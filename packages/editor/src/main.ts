
// if(MODE !== 'build') {
      // import "webpack-hot-middleware/client?reload=true"
      // import 'core-js/stable';
      // import 'regenerator-runtime/runtime';
      // import "babel-runtime/regenerator"
// }


console.log("main.   ts");


// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import renobitMainPlugin from "./wemb/wv/vue/RenobitMainPlugin";
Vue.use(renobitMainPlugin);

import router from './router';
import {http} from "./wemb/http/Http";
import api = http.api;
import puremvc from "puremvc";

Vue.config.productionTip = false;
window.Vue = Vue;
/*
2018.10.01(ckkim)
- 확장요소에서 동적으로 puremvc 관련 요소를 추가할때 사용.
   이를 위해서 puremvc를 window에 등록.
*/
window.puremvc = puremvc;
window.http = api;
////////////////////

import ElementUI from "element-ui";
import VModal from 'vue-js-modal';
import './assets/style/style.css';

import locale from 'element-ui/lib/locale/lang/en'
Vue.use(ElementUI, { locale });
Vue.use(VModal);

Vue.$loading = Vue.prototype.$loading;
Vue.$msgbox = Vue.prototype.$msgbox;
Vue.$alert = Vue.prototype.$alert;
Vue.$confirm = Vue.prototype.$confirm;
Vue.$notify = Vue.prototype.$notify;
Vue.$message = Vue.prototype.$message;


import { ContainerMixin, ElementMixin, HandleDirective } from 'vue-slicksort';
window.slicksort = {
      "ContainerMixin": ContainerMixin,
      "ElementMixin": ElementMixin,
      "HandleDirective" : HandleDirective
};


import { VueColorpicker } from 'vue-pop-colorpicker';
Vue.use(VueColorpicker);
Vue.component('color-picker', VueColorpicker);

/**/
//only editor
import DatasetPreviewMainComponent from "./wemb/wv/components/datasetPreview/DatasetPreviewMainComponent";
import TransferMainComponent from "./wemb/wv/components/transfer/TransferMainComponent";
import BasicTableComponent from "./wemb/wv/components/table/BasicTableComponent";
import SelectablePageTreeComponent from "./wemb/wv/components/selectablePageTree/SelectablePageTreeComponent";


Vue.component("dataset-preview-main-component", DatasetPreviewMainComponent);
Vue.component("transfer-main-component", TransferMainComponent);
Vue.component("basic-table-component", BasicTableComponent);
Vue.component("selectable-page-tree-component", SelectablePageTreeComponent);

// 메인 메뉴
import * as VueMenu from '@hscmap/vue-menu'
Vue.use(VueMenu)

import App from "./App";
import i18n from "./lang";
import ConfigManager from "./wemb/wv/managers/ConfigManager";
import LocaleManager from "./wemb/wv/managers/LocaleManager";
import ExtensionInstanceManager from "./wemb/wv/managers/ExtensionInstanceManager";


import VueSplit from 'vue-split-panel';
Vue.use(VueSplit);
Vue.component('split', VueSplit.Split);
Vue.component('split-area', VueSplit.SplitArea);

Vue.$i18n = i18n;


//////////////////////////////////////////////////////////////////



/* 20190212 세션처리를 위한 전역 함수
* ie에서 window.opener.객체 접근시 속도가 느려지는 현상이 있어, 객체를 참조하지 않는 전역 메서드를 정의하여 사용한다.
* 데이터셋,
*/

let locationStr = window.location.host;
window._setSessionUpdateTime = function(time){
      localStorage.setItem("__renobit_session__"+locationStr, time.toString());
}

window._getSessionUpdateTime = function(){
      return parseInt(localStorage.getItem("__renobit_session__"+locationStr));
}







//////////////////////////////////////////////////////////////////
/*
시작 순서
1. 환경 설정 정보 구하기
2. socket 생성
3. locale 정보 설정 및 다국어 파일 정보 읽기
4. 확장 정보 로드하기
5. App 태그 생성 시작

 */
async function start(){
      console.log("__START 1, ################# start ##############");
      console.log("__START 1, 준비 시작");


      // 1. 환경 설정 정보 구하기
      console.log('\t__START 1, step01. config정보 읽기 시작');
      let success= await ConfigManager.getInstance().startLoading();
      console.log('\t__START 1, step01. config정보 읽기 완료', success);
      if(success!=true){
            alert("config file error");
            return;
      }

      // 2. socket 생성
      createSocketConnector();

      // 3. locale 정보 설정 및 다국어 파일 정보 읽기
      success = await initLocale();
      if(success!=true){
             return;
      }

      // 4. 확장 정보 로드하기
      success = await loadExtensionInfo();
      if(success!=true)
            return;

      console.log("__START 1, 준비 완료\n\n\n");
      console.log("__START 2, ue Main 생성 시작!");
      // 5. 정상적인 경우 App생성 및 시작
      createApp();

}



import Socket from "./wemb/ws/Socket";
import {THREE} from "./wemb/core/utils/reference";
/*
node인 경우 소켓 생성하기
 */
function createSocketConnector(){
      if(ConfigManager.getInstance().serverType === "node") {
            console.log('\t__START 1, step02. RENOBIT data 관련 socket 연결 시도');
            window.socket = io.connect({
                  path: '/socket.io',
                  transports: ['websocket'],
                  secure: true,
            });

            window.socket.on('connected', function (data) {
                  console.log('\t__START 1, RENOBIT data 관련 socket 연결', data.id);

            });
            console.log('\t__START 1, step02. RENOBIT data 관련 socket 연결 완료');
      } else if(ConfigManager.getInstance().serverType === "java") {
            let socketURL = ConfigManager.getInstance().serverUrl;
            socketURL = socketURL.replace("https", "ws");
            socketURL = socketURL.replace("http", "ws");
            socketURL += "/socket";

            window.socket = new Socket(socketURL);

            // window.socket = new WebSocket(socketURL);
            // window.socket.onopen = (message)=> {
            //       console.log('\t__START 1, RENOBIT data 관련 socket 연결', message);
            // };
            // //웹 소켓이 닫혔을 때 호출되는 이벤트
            // window.socket.onclose = (message)=> {
            //       console.log(message);
            // };
            // //웹 소켓이 에러가 났을 때 호출되는 이벤트
            // window.socket.onerror = (message)=> {
            //       console.log(message);
            // };

            // window.socket.onmessage = (message)=> {
            //       console.log(JSON.parse(message.data));
            // };

            // console.log('\t__START 1, step02. RENOBIT data 관련 socket 연결 완료');
      }
}




/*
서버에 설정된

1. locale 정보 설정
2. 다국어 정보 읽기
 */
async function initLocale(){

      console.log('\t__START 1, step03. locale 정보 설정 및 다국어 파일 정보 읽기 시작');

      let success = await LocaleManager.getInstance().updateLocaleToLocalStorage();
      if(success==false){
            alert("setServerLocale error");
            return false;
      }
      console.log("\t__START 1, step03. 현재 locale = ", LocaleManager.getInstance().locale);
      /*
      0번째 언어 정보 구하기
      0번째 언어를 시작으로 처리
       */
      success = await LocaleManager.getInstance().load();
      if(success==false){
            alert("Can not read multilingual files.");
            return false;
      }

      console.log('\t__START 1, step03. locale 정보 설정 및 다국어 파일 정보 읽기 완료');
      return true;
}


async function loadExtensionInfo(){

      console.log('\t__START 1, step04. 확장 인스턴스 관리자 정보 로드 시작');
      /*
      확장 인스턴스 관리자 정보 로드
       */
      let success = await ExtensionInstanceManager.getInstance().startLoading();
      if(success==false){
            alert("ExtensionInstanceManager data loading error");
            return false;
      }

      console.log('\t__START 1, step04. 확장 인스턴스 관리자 정보 로드 완료');

      return true;
}


function createApp(){

      new Vue({
            el: '#app',
            router,
            i18n: i18n,
            components: {App},
            template: `
                  <App>
                  </App>
                  `
      })

      THREE.Cache.enabled = true;
}


start();

/*
2018.10.10 (ckkim)
마우스 우클릭 막기
 */
$(document).ready(function(){
      $(document).on("contextmenu",function(e){
            return false;
      });
});

// todo: window에 변수를 등록하는 절차가 분산되어 있어 한군데로 합쳐야됩니다. 
import ScriptUtil from './wemb/core/utils/ScriptUtil';
window.ScriptUtil = ScriptUtil;