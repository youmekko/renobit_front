import * as puremvc from "puremvc";
import EditorMainComponent from "./EditorMainComponent.vue";
import EditorProxy from "../model/EditorProxy";
import EditorStatic from "../controller/editor/EditorStatic";
import {EDITOR_APP_STATE} from "../../wemb/wv/data/Data";


export class EditorMainMediator extends puremvc.Mediator {
	public static NAME: string = "EditorMainMediator";
	private view: EditorMainComponent = null;

	constructor(name: string, viewComponent: EditorMainComponent) {
		super(name, viewComponent);
		this.view = viewComponent;
	}

	public getView(): EditorMainComponent {
		return this.viewComponent;
	}

	public onRegister() {
		this.view.$on(EditorMainComponent.EVENT_OPEN_PAGE, (pageId:string)=>{
			this.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageId);
		})
	}


	public listNotificationInterests(): string[] {
		return [
			EditorProxy.NOTI_UPDATE_EDITOR_STATE,
			EditorProxy.NOTI_CLOSED_PAGE,
			EditorProxy.NOTI_OPEN_PAGE,
			EditorProxy.NOTI_CHANGE_VISIBLE_ASIDE_PANEL,
                  EditorProxy.NOTI_CREATE_DYNAMIC_PANEL
		]
	}


	public toggleLeftAside(){
		this.view.toggleLeftAside();
	}

	public toggleRightAside(){
		this.view.toggleRightAside();
	}

	public handleNotification(note: puremvc.Notification): void {
		switch (note.name) {
			case EditorProxy.NOTI_UPDATE_EDITOR_STATE :
				this.executeUpdateEditorState(note.getBody());
				break;
                case EditorProxy.NOTI_CLOSED_PAGE:
					this.view.noti_closePage();
					break;
                case EditorProxy.NOTI_OPEN_PAGE:
					this.view.noti_openPage();
					break;
                case EditorProxy.NOTI_CHANGE_VISIBLE_ASIDE_PANEL:
                       this.view.toggleAsidePanel(note.getBody());
                    break;
                case EditorProxy.NOTI_CREATE_DYNAMIC_PANEL :
                      this.view.noti_createDynamicPanel(note.getBody());
                      break;

		}
	}


	private executeUpdateEditorState(state:EDITOR_APP_STATE){
		switch(state){
			case EDITOR_APP_STATE.READY_START :
				break;
			case EDITOR_APP_STATE.READY_COMPLETED :
				this.view.noti_readyCompleted();
				break;



            }
	}
}
