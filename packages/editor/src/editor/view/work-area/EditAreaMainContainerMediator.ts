import Vue from "vue";
import * as puremvc from "puremvc";

import EditAreaMainContainer from "./EditAreaMainContainer.vue";
import EditorProxy from "../../model/EditorProxy";
import LayerToolbarMediator from "../toolbar/layerToolbar/LayerToolbarMediator";
import WVComponent from "../../../wemb/core/component/WVComponent";
import EditorStatic from "../../controller/editor/EditorStatic";
import SelectProxy from "../../model/SelectProxy";
import {LayerVisibleVO} from "../../model/vo/ValueObjects";
import {Rectangle} from "../../../wemb/core/geom/interfaces/GeomInterfaces";


export default class EditAreaMainContainerMediator extends puremvc.Mediator {
	public static NAME: string = "EditAreaMainContainerMediator";
	private view: EditAreaMainContainer = null;

	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <EditAreaMainContainer>viewComponent;
	}

	public getView(): EditAreaMainContainer {
		return this.viewComponent;
	}

	public onRegister() {
		this.view.$on(EditAreaMainContainer.EVENT_NEW_SELECTED_COMPONENT_INSTANCE, (comInstance:WVComponent)=>{
			this.sendNotification(EditorStatic.CMD_NEW_SELECTED_COMPONENT_INSTANCE, comInstance);
		})

		this.view.$on(EditAreaMainContainer.EVENT_ADD_SELECTED_COMPONENT_INSTANCE, (comInstance:WVComponent)=>{

			this.sendNotification(EditorStatic.CMD_ADD_SELECTED, comInstance);
		})

		this.view.$on(EditAreaMainContainer.EVENT_REMOVE_SELECTED_COMPONENT_INSTANCE, (comInstance:WVComponent)=>{
			this.sendNotification(EditorStatic.CMD_REMOVE_SELECTED, comInstance);
		})


		this.view.$on(EditAreaMainContainer.EVENT_CLEAR_ALL_SELECTED_COMPONENT_INSTANCE, ()=>{
			this.sendNotification(EditorStatic.CMD_CLEAR_ALL_SELECTED);
		})

		this.view.$on(EditAreaMainContainer.EVENT_TRANSFORM_COMPLETED, ()=>{
		// undo redo
			var proxy =  this.facade.retrieveProxy(EditorProxy.NAME) as EditorProxy;
			proxy.setHistoryEditing(null,"edit");
			this.sendNotification(SelectProxy.NOTI_UPDATE_PROPERTIES);
		})

		this.view.$on(EditAreaMainContainer.EVENT_CALCULATE_SELECTED_COMPONENT_INSTANCE_IN_DRAG_AREA, (area:Rectangle)=>{
				this.sendNotification(EditorStatic.CMD_CALCULATE_SELECTED_COMPONENT_INSTANCE_IN_DRAG_AREA, area);
		})
	}


	public listNotificationInterests(): string[] {
		return [
			EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
			EditorProxy.NOTI_ADD_COM_INSTANCE,
			EditorProxy.NOTI_REMOVE_COM_INSTANCE,
			EditorProxy.NOTI_CLOSED_PAGE,
                  EditorProxy.NOTI_SET_ACTIVE_SHORT_CUT_STATE,
			LayerToolbarMediator.NOTI_UPDATE_LAYER_VISIBLE,
			SelectProxy.NOTI_CLEAR_ALL_SELECTED_INSTANCE,
			SelectProxy.NOTI_ADD_SELECTED_INSTANCE,
			SelectProxy.NOTI_REMOVE_SELECTED_INSTANCE,
                  SelectProxy.NOTI_UPDATE_SELECTED_INFO,
                  SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE,


		]
	}


	public handleNotification(note: puremvc.Notification): void {
		switch (note.name) {
			case EditorProxy.NOTI_CLOSED_PAGE:
				this.view.noti_closedPage();
				break;
			case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :

				this.view.noti_changeActiveLayer(note.getBody());
				break;
			case EditorProxy.NOTI_ADD_COM_INSTANCE :
				this.view.noti_addComInstance(note.getBody());
				break;
			case EditorProxy.NOTI_REMOVE_COM_INSTANCE :
				this.view.noti_removeComInstance(note.getBody());
				break;

                  case EditorProxy.NOTI_SET_ACTIVE_SHORT_CUT_STATE :
                        this.view.noti_setActiveShortCutState(note.getBody());
                        break;
			case LayerToolbarMediator.NOTI_UPDATE_LAYER_VISIBLE :
				this.view.noti_updateLayerVisible(note.getBody() as LayerVisibleVO);
				break;

			case  SelectProxy.NOTI_CLEAR_ALL_SELECTED_INSTANCE :
				this.view.noti_clearAllSelectedInstance(note.getBody());
				break;

                  case SelectProxy.NOTI_UPDATE_SELECTED_INFO :

				let selectProxy:SelectProxy = this.facade.retrieveProxy(SelectProxy.NAME) as SelectProxy;
				let selectedComList:Array<WVComponent> =  selectProxy.selectedTwoThreeMasterComInstanceListList;

				this.view.noti_updateSelectedInfo(selectedComList);
                        break;
			case SelectProxy.NOTI_REMOVE_SELECTED_INSTANCE :
				this.view.noti_removeSelectedInstance(note.getBody() as WVComponent);
				break;

			case SelectProxy.NOTI_ADD_SELECTED_INSTANCE :
                        break;

                  case SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE :
                        let selectProxy2:SelectProxy = this.facade.retrieveProxy(SelectProxy.NAME) as SelectProxy;
                        if(selectProxy2.editableProperties){
                              this.view.noti_syncTransformLocationSize(selectProxy2.editableProperties.info.category);
                        }

                        break;
		}
	}
}
