import Vue from "vue";
import * as puremvc from "puremvc";

import WorkAreaToolbar from "./WorkAreaToolbar.vue";
import EditorProxy from "../../../model/EditorProxy";
import SelectProxy from "../../../model/SelectProxy";
import CopyPasteProxy from "../../../model/CopyPasteProxy";
import EditorStatic from "../../../controller/editor/EditorStatic";
import {EDITOR_APP_STATE} from "../../../../wemb/wv/data/Data";


export default class WorkAreaToolbarMediator extends puremvc.Mediator {
	public static NAME: string = "WorkAreaToolbarMediator";
	private view: WorkAreaToolbar = null;
      private _selectProxy:SelectProxy =  null;
      private _copyProxy:CopyPasteProxy = null;
	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <WorkAreaToolbar>viewComponent;
	}



	public getView(): WorkAreaToolbar {
		return this.viewComponent;
	}

	public onRegister() {



		this.view.$on(WorkAreaToolbar.EVENT_COPY_SELECTED_COM_INSTANCE, ()=>{
			this.sendNotification(EditorStatic.CMD_COPY_SELECTED_COM_INSTANCE);
		})
		this.view.$on(WorkAreaToolbar.EVENT_CUT_SELECTED_COM_INSTANCE, ()=>{
			this.sendNotification(EditorStatic.CMD_CUT_SELECTED_COM_INSTANCE);
		})
		this.view.$on(WorkAreaToolbar.EVENT_PASTE_COM_INSTANCE, ()=>{
			this.sendNotification(EditorStatic.CMD_PASTE_COM_INSTANCE);
		})
		this.view.$on(WorkAreaToolbar.EVENT_DELETE_SELECTED_COM_INSTANCE, ()=>{
			this.sendNotification(EditorStatic.CMD_REMOVE_SELECTED_COM_INSTANCE);
		})


	}





	public listNotificationInterests(): string[] {
		return [
                  EditorProxy.NOTI_UPDATE_EDITOR_STATE,
                  EditorProxy.NOTI_OPEN_PAGE,
                  EditorProxy.NOTI_CLOSED_PAGE,
                  EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
                  SelectProxy.NOTI_UPDATE_SELECTED_INFO,
                  SelectProxy.NOTI_CLEAR_ALL_SELECTED_INSTANCE,
                  CopyPasteProxy.NOTI_ADD_PASTE_COMPONENT_INSTANCE,
                  CopyPasteProxy.NOTI_ADD_COPY_COMPONENT_INSTANCE
		]
	}


	public handleNotification(note: puremvc.Notification): void {

		switch(note.name){
                  case EditorProxy.NOTI_UPDATE_EDITOR_STATE :
                        if(note.getBody()==EDITOR_APP_STATE.READY_COMPLETED){
                              this._selectProxy= this.facade.retrieveProxy(SelectProxy.NAME) as SelectProxy;
                              this._copyProxy= this.facade.retrieveProxy(CopyPasteProxy.NAME) as CopyPasteProxy;
                        }
                        break;
                  case EditorProxy.NOTI_OPEN_PAGE :
                        break;
                  case EditorProxy.NOTI_CLOSED_PAGE :
                        this.view.noti_closedPage();
                        break;

                  case SelectProxy.NOTI_CLEAR_ALL_SELECTED_INSTANCE :
                  case SelectProxy.NOTI_UPDATE_SELECTED_INFO :
                  case CopyPasteProxy.NOTI_ADD_COPY_COMPONENT_INSTANCE :
                  case CopyPasteProxy.NOTI_ADD_PASTE_COMPONENT_INSTANCE :
                        if(this._selectProxy) {
                              this.view.noti_updateSelectedInfo(this._selectProxy.selectedTwoThreeMasterComInstanceListList, this._copyProxy.instanceMetaList);
                        }
		}


	}
}
