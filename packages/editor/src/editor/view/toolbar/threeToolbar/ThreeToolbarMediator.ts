import Vue from "vue";
import * as puremvc from "puremvc";

import ThreeToolbar from "./ThreeToolbar.vue";
import EditorProxy from "../../../model/EditorProxy";
import SelectProxy from "../../../model/SelectProxy";
import WVComponent from "../../../../wemb/core/component/WVComponent";
import EditorStatic from "../../../controller/editor/EditorStatic";



export default class ThreeToolbarMediator extends puremvc.Mediator{
    public static NAME:string = "ThreeToolbarMediator";
    private view:ThreeToolbar = null;
    constructor(name:string, viewComponent:Vue){
        super(name, viewComponent);
        this.view = <ThreeToolbar>viewComponent;
    }

    public getView():ThreeToolbar{
        return this.viewComponent;
    }

    public onRegister(){
            this.view.$on(ThreeToolbar.EVENT_CHANGE_THREE_TRANSFORM_CONTROL_MODE, (mode:string)=>{
                  this.sendNotification(EditorStatic.CMD_CHANGE_THREE_TRANSFORM_CONTROL_MODE, mode);
            })

            this.view.$on(ThreeToolbar.EVENT_TOGGLE_DIRECTION_VIEW,(value)=>{
                  this.sendNotification(EditorStatic.CMD_TOGGLE_DIRECTION_VIEW, value);
            })

          this.view.$on(ThreeToolbar.EVENT_TOGGLE_GRID_HELPER,(value)=>{
                this.sendNotification(EditorStatic.CMD_TOGGLE_GRID_HELPER, value);
          })


          this.view.$on(ThreeToolbar.EVENT_SHOW_THREE_ALIGN_MODAL,()=>{
                this.sendNotification(EditorStatic.CMD_SHOW_THREE_ALIGN_MODAL);
          })
    }


    public listNotificationInterests():string[] {
          return [
                SelectProxy.NOTI_UPDATE_SELECTED_INFO
          ]
    }


    public handleNotification(note:puremvc.Notification):void{
        //console.log("ThreeToolbarMediator handleNotification note = ", note);
          switch (note.name) {
                case SelectProxy.NOTI_UPDATE_SELECTED_INFO :
                      let selectProxy:SelectProxy = this.facade.retrieveProxy(SelectProxy.NAME) as SelectProxy;
                      let selectedComList:Array<WVComponent> =  selectProxy.selectedTwoThreeMasterComInstanceListList;
                      this.view.noti_updateSelectedInfo(selectedComList);
          }

    }
}
