import Vue from "vue";
import * as puremvc from "puremvc";

import MainToolbarContainer from "./MainToolbarContainer.vue";
import EditorProxy from "../../model/EditorProxy";
import EditorStatic from "../../controller/editor/EditorStatic";


export default class MainToolbarContainerMediator extends puremvc.Mediator {
    public static NAME: string = "MainToolbarContainerMediator";
    private view: MainToolbarContainer = null;

    constructor(name: string, viewComponent: Vue) {
        super(name, viewComponent);
        this.view = <MainToolbarContainer>viewComponent;
    }

    public getView(): MainToolbarContainer {
        return this.viewComponent;
    }

    public onRegister() {
        this.view.$on(MainToolbarContainer.EVENT_GOTO_VIEWER, () => {
            if (window.wemb.pageManager.currentPageInfo.type != "page"){
                alert(Vue.$i18n.messages.wv.extension.menu.openViewerMasterError);
                return;
            }
              this.sendNotification(EditorStatic.CMD_GOTO_VIEWER_BY_ID, window.wemb.pageManager.currentPageInfo.id);
        });

        this.view.$on(MainToolbarContainer.EVENT_GOTO_ADMIN, () => {
            this.sendNotification(EditorStatic.CMD_GOTO_ADMIN);
        });

        this.view.$on(MainToolbarContainer.EVENT_GOTO_LOGOUT, () => {
            this.sendNotification(EditorStatic.CMD_GOTO_LOGOUT);
        });

        this.view.$on(MainToolbarContainer.EVENT_CHANGE_LOCALE, () => {
            this.sendNotification(EditorStatic.CMD_CHANGE_LOCALE);
        });

        this.view.$on(MainToolbarContainer.EVENT_GOTO_MULTILINGUAL, () => {
            this.sendNotification(EditorStatic.CMD_SHOW_MULTILINGUAL_MANAGER);
        });
    }


    public listNotificationInterests(): string[] {
        return [
            EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
            EditorProxy.NOTI_ADD_COM_INSTANCE,
            EditorProxy.NOTI_CLOSED_PAGE,
            EditorProxy.NOTI_OPEN_PAGE
        ]
    }


    public handleNotification(note: puremvc.Notification): void {
        //console.log("MainToolbarContainerMediator handleNotification note = ", note.name);

        switch (note.name) {
            case EditorProxy.NOTI_OPEN_PAGE:
                this.view.noti_openPage();
                break;
            case EditorProxy.NOTI_CLOSED_PAGE:
                this.view.noti_closedPage();
                break;
            case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :
                this.view.noti_changeActiveLayer(note.getBody());
                break;
        }
    }
}
