import Vue from "vue";
import * as puremvc from "puremvc";

import TwoToolbar from "./TwoToolbar.vue";
import EditorProxy from "../../../model/EditorProxy";
import SelectProxy from "../../../model/SelectProxy";
import EditorStatic from "../../../controller/editor/EditorStatic";
import WVComponent from "../../../../wemb/core/component/WVComponent";


export default class TwoToolbarMediator extends puremvc.Mediator {
	public static NAME: string = "TwoToolbarMediator";
	private view: TwoToolbar = null;


	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <TwoToolbar>viewComponent;
	}

	public getView(): TwoToolbar {
		return this.viewComponent;
	}

	public onRegister() {
            this.view.$on(TwoToolbar.EVENT_ALIGN, (command:string)=>{
                  this.sendNotification(EditorStatic.CMD_ALIGN_SELECTED_COM_INSTANCE, command);
            })

            this.view.$on(TwoToolbar.EVENT_ARRANGE, (command:string)=>{
                  this.sendNotification(EditorStatic.CMD_ARRANGE_SELECTED_COM_INSTANCE, command);
            })

            this.view.$on(TwoToolbar.EVENT_SIZE, (command:string)=>{
                  this.sendNotification(EditorStatic.CMD_SIZE_SELECTED_COM_INSTANCE, command);
            })

            this.view.$on(TwoToolbar.EVENT_DISTRIBUTE, (command:string)=>{
                  this.sendNotification(EditorStatic.CMD_DISTRIBUTE_SELECTED_COM_INSTANCE, command);
            })


	}


	public listNotificationInterests(): string[] {
		return [
			EditorProxy.NOTI_OPEN_PAGE,
			EditorProxy.NOTI_CLOSED_PAGE,
			EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
                  SelectProxy.NOTI_UPDATE_SELECTED_INFO
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		switch (note.name) {
			case EditorProxy.NOTI_OPEN_PAGE :
				this.view.noti_openPage();
				break;
			case EditorProxy.NOTI_CLOSED_PAGE :
				this.view.noti_closedPage();
				break;
			case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :
				this.view.noti_changeActiveLayer(note.getBody());
				break;
                  case SelectProxy.NOTI_UPDATE_SELECTED_INFO :
				let selectProxy:SelectProxy = this.facade.retrieveProxy(SelectProxy.NAME) as SelectProxy;
				let selectedComList:Array<WVComponent> =  selectProxy.selectedTwoThreeMasterComInstanceListList;
				this.view.noti_updateSelectedInfo(selectedComList);
		}


	}
}
