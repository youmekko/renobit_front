import Vue from "vue";
import * as puremvc from "puremvc";

import MainMenubar from "./MainMenubar.vue";
import EditorProxy from "../../../model/EditorProxy";
import SelectProxy from "../../../model/SelectProxy";
import {WORK_LAYERS} from "../../../../wemb/wv/layer/Layer";

import LayerToolbarMediator from "../layerToolbar/LayerToolbarMediator";
import {LayerVisibleVO} from "../../../model/vo/ValueObjects";
import CopyPasteProxy from "../../../model/CopyPasteProxy";
import {EDITOR_APP_STATE} from "../../../../wemb/wv/data/Data";
import EditorStatic from "../../../controller/editor/EditorStatic";

export default class MainMenubarMediator extends puremvc.Mediator {
    public static NAME: string = "MainMenubarMediator";
    private view: MainMenubar = null;

    private _copyProxy:CopyPasteProxy = null;
    private _selectProxy:SelectProxy =  null;

    constructor(name: string, viewComponent: Vue) {
        super(name, viewComponent);
        this.view = <MainMenubar>viewComponent;
    }

    public getView(): MainMenubar {
        return this.viewComponent;
    }

    public onRegister() {

        this.view.$on(MainMenubar.EVENT_CUSTOM_MAIN_MENU, (command: string, param: any = null) => {
            this.sendNotification(command, param);
        });

        this.view.$on(MainMenubar.EVENT_GOTO_VIEWER, ()=>{

            this.sendNotification(EditorStatic.CMD_GOTO_VIEWER_BY_NAME, window.wemb.pageManager.currentPageInfo.name);
        });

    }


    public listNotificationInterests(): string[] {
        return [
            EditorProxy.NOTI_UPDATE_EDITOR_STATE,
            EditorProxy.NOTI_CLOSED_PAGE,
            EditorProxy.NOTI_OPEN_PAGE,
            EditorProxy.NOTI_CHANGE_MODIFIED,
            SelectProxy.NOTI_UPDATE_SELECTED_INFO,
            EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
            LayerToolbarMediator.NOTI_UPDATE_LAYER_VISIBLE,
            CopyPasteProxy.NOTI_ADD_PASTE_COMPONENT_INSTANCE,
            CopyPasteProxy.NOTI_ADD_COPY_COMPONENT_INSTANCE
        ]
    }


    public handleNotification(note: puremvc.Notification): void {
        switch (note.name) {

            case EditorProxy.NOTI_UPDATE_EDITOR_STATE :
                this._chagneEditorState(note.getBody());

                if(note.getBody()==EDITOR_APP_STATE.READY_COMPLETED){
                    this._selectProxy= this.facade.retrieveProxy(SelectProxy.NAME) as SelectProxy;
                    this._copyProxy= this.facade.retrieveProxy(CopyPasteProxy.NAME) as CopyPasteProxy;
                }

                break;

            case EditorProxy.NOTI_CLOSED_PAGE :
                this.view.noti_closedPage();
                 break;

            case EditorProxy.NOTI_OPEN_PAGE :
                this.view.noti_openPage();
                break;

            case EditorProxy.NOTI_CHANGE_MODIFIED :
                this.view.noti_changeModified(note.getBody() as boolean)
                break;

            case SelectProxy.NOTI_UPDATE_SELECTED_INFO :
                let list = (this.facade.retrieveProxy(SelectProxy.NAME) as SelectProxy).selectedTwoThreeMasterComInstanceListList;
                this.view.noti_updateSelectedInfo(list);
                break;

            case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :
                this.view.noti_changeActiveLayer(note.getBody() as WORK_LAYERS);
                break;

            case LayerToolbarMediator.NOTI_UPDATE_LAYER_VISIBLE :
                this.view.noti_updateLayerVisible(note.getBody() as LayerVisibleVO);
                break;

            case CopyPasteProxy.NOTI_ADD_COPY_COMPONENT_INSTANCE :
            case CopyPasteProxy.NOTI_ADD_PASTE_COMPONENT_INSTANCE :
                if (this._selectProxy) {
                    this.view.noti_updateCopyInfo(this._selectProxy.selectedTwoThreeMasterComInstanceListList);
                }

        }


    }

    private _chagneEditorState(state: EDITOR_APP_STATE) {
        switch (state) {
            case EDITOR_APP_STATE.READY_COMPLETED :
                this.view.startMainMenubar(window.wemb.mainMenuManager.getMenuItemList());
                break;
            case EDITOR_APP_STATE.LOCALE_CHANGED :
            this.view.startMainMenubar(window.wemb.mainMenuManager.getMenuItemList());
                break;

        }
    }


}
