import Vue from "vue";
import * as puremvc from "puremvc";

import LayerToolbar from "./LayerToolbar.vue";
import EditorProxy from "../../../model/EditorProxy";
import EditorStatic from "../../../controller/editor/EditorStatic";
import {LayerVisibleVO} from "../../../model/vo/ValueObjects";
import {WORK_LAYERS} from "../../../../wemb/wv/layer/Layer";



export default class LayerToolbarMediator extends puremvc.Mediator {
	public static readonly NAME: string = "LayerToolbarMediator";
	public static readonly NOTI_UPDATE_LAYER_VISIBLE:string="notification/updateLayerVisible";
	private view: LayerToolbar = null;

	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <LayerToolbar>viewComponent;
	}



	public getView(): LayerToolbar {
		return this.viewComponent;
	}

	public onRegister() {
		this.view.$on(LayerToolbar.EVENT_SET_LAYER_VISIBLE,(vo:LayerVisibleVO)=>{
			this.sendNotification(LayerToolbarMediator.NOTI_UPDATE_LAYER_VISIBLE, vo);
                  this.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);
		})

		this.view.$on(LayerToolbar.EVENT_CHANGE_ACTIVE_LAYER,(layerName:WORK_LAYERS)=>{
			this.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, layerName);
		})

		this.view.$on(LayerToolbar.EVENT_TOGGLE_LEFT_ASIDE_VISIBLE,()=>{
			this.sendNotification(EditorStatic.CMD_TOGGLE_LEFT_ASIDE_VISIBLE);
		})

		this.view.$on(LayerToolbar.EVENT_TOGGLE_RIGHT_ASIDE_VISIBLE,()=>{
			this.sendNotification(EditorStatic.CMD_TOGGLE_RIGHT_ASIDE_VISIBLE);
		})
	}

	public listNotificationInterests(): string[] {
		return [
			EditorProxy.NOTI_OPEN_PAGE,
			EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
                  LayerToolbarMediator.NOTI_UPDATE_LAYER_VISIBLE,
			EditorProxy.NOTI_CHANGE_VISIBLE_ASIDE_PANEL
		]
	}


	public handleNotification(note: puremvc.Notification): void {

		switch(note.name){
			case EditorProxy.NOTI_OPEN_PAGE :
				this._openPage(note.getBody());
				break;
			case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :
				this.view.noti_changeActiveLayer(note.getBody());
				break;
                  case LayerToolbarMediator.NOTI_UPDATE_LAYER_VISIBLE:
                        this.view.noti_changeLayerVisible(note.getBody());
                        break;
			case EditorProxy.NOTI_CHANGE_VISIBLE_ASIDE_PANEL:
				this.view.toggleAsidePanelIcon(note.getBody());
				break;
		}
	}

	private _openPage(page){
		this.view.noti_openPage(page);
	}



}
