import Vue from "vue";
import * as puremvc from "puremvc";

import PageToolbar from "./PageToolbar.vue";
import EditorProxy from "../../../model/EditorProxy";
import SelectProxy from "../../../model/SelectProxy";
import EditorStatic from "../../../controller/editor/EditorStatic";


export default class PageToolbarMediator extends puremvc.Mediator {
      public static NAME: string = "PageToolbarMediator";
      private view: PageToolbar = null;

      constructor(name: string, viewComponent: Vue) {
            super(name, viewComponent);
            this.view = <PageToolbar>viewComponent;
      }

      public getView(): PageToolbar {
            return this.viewComponent;
      }

      public onRegister() {
            this.view.$on(PageToolbar.EVENT_NEW_PAGE, () => {
                  this.sendNotification(EditorStatic.CMD_SHOW_NEW_PAGE_MODAL);
            })

            this.view.$on(PageToolbar.EVENT_SAVE_PAGE, () => {
                  this.sendNotification(EditorStatic.CMD_SAVE_PAGE);
            })
            this.view.$on(PageToolbar.EVENT_SAVE_AS_PAGE, () => {
                  this.sendNotification(EditorStatic.CMD_SHOW_SAVE_AS_PAGE_MODAL);
            })
            this.view.$on(PageToolbar.EVENT_GOTO_VIEWER, () => {
                  this.sendNotification(EditorStatic.CMD_GOTO_VIEWER_BY_NAME, window.wemb.pageManager.currentPageInfo.name);
            })
      }


      public listNotificationInterests(): string[] {
            return [
                  EditorProxy.NOTI_OPEN_PAGE,
                  EditorProxy.NOTI_CLOSED_PAGE,
                  EditorProxy.NOTI_CHANGE_MODIFIED
            ]
      }


      public handleNotification(note: puremvc.Notification): void {
            switch (note.name) {
                  case EditorProxy.NOTI_OPEN_PAGE :
                        this.view.noti_openPage();
                        break;
                  case EditorProxy.NOTI_CLOSED_PAGE :
                        this.view.noti_closedPage();
                        break;
                  case EditorProxy.NOTI_CHANGE_MODIFIED :
                        this.view.noti_changeModified(note.getBody() as boolean)
                        break;

            }

      }
}
