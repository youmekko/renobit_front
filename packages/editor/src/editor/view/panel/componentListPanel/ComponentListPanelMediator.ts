import Vue from "vue";
import * as puremvc from "puremvc";

import ComponentListPanel from "./ComponentListPanel.vue";


import EditorProxy from "../../../model/EditorProxy";
import EditorStatic from "../../../controller/editor/EditorStatic";
import {ComponentInstanceInfoVO} from "../../../model/vo/ValueObjects";


export default class ComponentListPanelMediator extends puremvc.Mediator {
	public static NAME: string = "ComponentListPanelMediator";
	private view: ComponentListPanel = null;

	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <ComponentListPanel>viewComponent;
	}

	public getView(): ComponentListPanel {
		return this.view;
	}

	public onRegister() {
		this.view.$on(ComponentListPanel.EVENT_ADD_COM_INSTANCE_BY_NAME, (vo:ComponentInstanceInfoVO)=>{
			this.sendNotification(EditorStatic.CMD_ADD_COM_INSTANCE_BY_NAME, vo);
		})
	}


	public listNotificationInterests(): string[] {
		return [
			EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
			EditorProxy.NOTI_OPEN_PAGE,
                  EditorProxy.NOTI_CLOSED_PAGE
		]
	}


	public handleNotification(noti: puremvc.Notification): void {
		//console.log("ComponentListPanelMediator handleNotification note = ", noti.name);
		switch (noti.name) {


			case EditorProxy.NOTI_OPEN_PAGE :
				this.view.noti_openPage();
				break;

                  case EditorProxy.NOTI_CLOSED_PAGE :
                        this.view.noti_closedPage();
                        break;

			case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :
				this.view.noti_setActiveWorkLayer(noti.getBody());
				break;
		}
	}


}
