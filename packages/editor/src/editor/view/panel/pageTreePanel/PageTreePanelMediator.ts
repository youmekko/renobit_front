import Vue from "vue";
import * as puremvc from "puremvc";

import PageTreePanel from "./PageTreePanel.vue";
import EditorProxy from "../../../model/EditorProxy";
import EditorStatic from "../../../controller/editor/EditorStatic";
import {EDITOR_APP_STATE} from "../../../../wemb/wv/data/Data";


export default class PageTreePanelMediator extends puremvc.Mediator {

    public static NAME: string = "PageTreePanelMediator";
    private view: PageTreePanel = null;

    constructor(name: string, viewComponent: Vue) {
        super(name, viewComponent);
        this.view = <PageTreePanel>viewComponent;
    }

    public getView(): PageTreePanel {
        return this.viewComponent;
    }

    public onRegister() {
        this.view.$on(PageTreePanel.EVENT_OPEN_PAGE, (pageId) => {
            this.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageId);
        })

        this.view.$on(PageTreePanel.EVENT_NEW_PAGE, (type) => {
            this.sendNotification(EditorStatic.CMD_SHOW_NEW_PAGE_MODAL, type);
        })


        this.view.$on(PageTreePanel.EVENT_CHANGE_PAGE_NAME, (pageName:any) => {
            this.sendNotification(EditorStatic.CMD_CHANGE_PAGE_NAME, pageName);
        })

        this.view.$on(PageTreePanel.EVENT_DELETE_PAGE_TREE_ITEMS, (items:Array<any>) => {
            this.sendNotification(EditorStatic.CMD_DELETE_PAGE_TREE_ITEMS, items);
        })
        
        this.view.$on(PageTreePanel.EVENT_UPDATE_PAGE_TREE_ITEMS, (items:Array<any>) => {
            
        })
        
    }

    public getSelectedTreeIds(): Array<string> {
        let selectedAllTreeIds: Array<string> = this.view.getSelectedTreeIds();//window.wemb.pageTreeDataManager.getSelectedTreeIds(this.view.getSelectedTreeIds());//tree 하위까지 검색
        return selectedAllTreeIds;
    }

    public getOpenTargetPageId(): string {
        let selectedAllTreeIds: Array<string> = this.view.getSelectedTreeIds();//window.wemb.pageTreeDataManager.getSelectedTreeIds(this.view.getSelectedTreeIds());//tree 하위까지 검색
        let result = null;

        if (selectedAllTreeIds && selectedAllTreeIds.length == 1) {
            let filterPageIds = window.wemb.pageTreeDataManager.getfilterPageTypeIds(selectedAllTreeIds);
            if (filterPageIds && filterPageIds.length == 1) {
                result = filterPageIds[0];
            }
        }

        return result;
    }


    public listNotificationInterests(): string[] {
        return [
            EditorProxy.NOTI_OPEN_PAGE,
            EditorProxy.NOTI_CLOSED_PAGE,
            EditorProxy.NOTI_UPDATE_EDITOR_STATE
        ]
    }


    public handleNotification(note: puremvc.Notification): void {
        switch (note.name) {
            case EditorProxy.NOTI_OPEN_PAGE :
                this.view.noti_openPage(note.getBody());
                break;
              case EditorProxy.NOTI_CLOSED_PAGE :
                    this.view.noti_closedPage();
                    break;
            case EditorProxy.NOTI_UPDATE_EDITOR_STATE :
                this.view.noti_updateEditorState(note.getBody() as EDITOR_APP_STATE);
        }
    }
}
