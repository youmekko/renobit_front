import Vue from "vue";
import * as puremvc from "puremvc";

import StagePropertyPanel from "./StagePropertyPanel.vue";
import StageProxy from "../../../model/StageProxy";
import EditorStatic from "../../../controller/editor/EditorStatic";
import EditorProxy from "../../../model/EditorProxy";

export default class StagePropertyPanelMediator extends puremvc.Mediator {
      public static NAME: string = "StagePropertyPanelMediator";

      private view: StagePropertyPanel = null;

      constructor(name: string, viewComponent: Vue) {
            super(name, viewComponent);
            this.view = <StagePropertyPanel>viewComponent;
      }

      private get stageProxy(): StageProxy {
            return <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
      }

      public getView(): StagePropertyPanel {
            return this.viewComponent;
      }

      public onRegister() {

            /*
	  값 자체는 바인딩에 의해서 싱크 되기 때문에 따로 변경할 필요가 없지만
	  bg layer등은 vue객체가 아닌 일반 객체이기 때문에 업데이트를 따로 해줘야 함.
	  값은 이미 바인딩에 의해서 변경돼 있기 때문에 proxy에 저장된 값을 bglayer등에 보내기만 하면 된다.
	 */
            this.view.$on(StagePropertyPanel.UPDATE_STAGE_BACKGROUND_INFO, (event, param) => {
                  //console.log("bg stage");
                  this.stageProxy.updateStageBackgroundInfo();

                  this.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);
            });

            this.view.$on(StagePropertyPanel.UPDATE_MASTER_BACKGROUND_INFO, (event, param) => {
                  //console.log("bg master");
                  this.stageProxy.updatePageBackgroundInfo();
                  this.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);
            });

            this.view.$on(StagePropertyPanel.UPDATE_PAGE_BACKGROUND_INFO, (event, param) => {
                  //console.log("bg page");
                  this.stageProxy.updatePageBackgroundInfo();
                  this.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);
            });


            this.view.$on(StagePropertyPanel.EVENT_SET_ACTIVE_SHORTCUT_STATE, (value:boolean)=>{
                  this.sendNotification(EditorStatic.CMD_SET_ACTIVE_SHORT_CUT_STATE, value);
            });
      }


      public listNotificationInterests(): string[] {
            return [
                  StageProxy.NOTI_BACKGROUND_INFO,
                  EditorProxy.NOTI_OPEN_PAGE,
                  EditorProxy.NOTI_CLOSED_PAGE,

            ]
      }


      /*
	배경 값의 경우 바인딩으로 사용하기 때문에 따로 뷰에 업데이트해줄 필요가 없읎ㅇ.ㅁ
	 */
      public handleNotification(note: puremvc.Notification): void {

            switch (note.name) {
                  case StageProxy.NOTI_BACKGROUND_INFO :
                        this.view.noti_backgroundInfo(note.getBody());
                        break;

                  case EditorProxy.NOTI_OPEN_PAGE:
                        this.view.noti_openPage();
                        break;
                  case EditorProxy.NOTI_CLOSED_PAGE :
                        this.view.noti_closedPage();
                        break;
            }
      }
}
