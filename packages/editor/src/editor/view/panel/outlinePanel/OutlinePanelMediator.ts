import Vue from "vue";
import * as puremvc from "puremvc";

import OutlinePanel from "./OutlinePanel.vue";
import EditorProxy from "../../../model/EditorProxy";
import WVComponent from "../../../../wemb/core/component/WVComponent";
import EditorStatic from "../../../controller/editor/EditorStatic";
import SelectProxy from "../../../model/SelectProxy";
import {WORK_LAYERS} from "../../../../wemb/wv/layer/Layer";


export default class OutlinePanelMediator extends puremvc.Mediator {
	public static NAME: string = "OutlinePanelMediator";
	private view: OutlinePanel = null;

      private _deferredID:number = 0;
      private _isFirst:boolean = false;
	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <OutlinePanel>viewComponent;


	}

	public getView(): OutlinePanel {
		return this.viewComponent;
	}

	public onRegister() {
		this.view.$on(OutlinePanel.EVENT_NEW_SELECTED_COMPONENT_INSTANCE, (comInstance:WVComponent)=>{
			this.sendNotification(EditorStatic.CMD_NEW_SELECTED_COMPONENT_INSTANCE, comInstance);
		});

		this.view.$on(OutlinePanel.EVENT_ADD_SELECTED_COMPONENT_INSTANCE, (comInstance:WVComponent)=>{
			this.sendNotification(EditorStatic.CMD_ADD_SELECTED, comInstance);
		});

		this.view.$on(OutlinePanel.EVENT_REMOVE_SELECTED_COMPONENT_INSTANCE, (comInstance:WVComponent)=>{
			this.sendNotification(EditorStatic.CMD_REMOVE_SELECTED, comInstance);
		});

		this.view.$on(OutlinePanel.EVENT_CHANGE_PROPERTY_VALUE, ()=>{
			this.sendNotification(SelectProxy.NOTI_UPDATE_PROPERTIES);
                  this.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE)
		});
		this.view.$on(OutlinePanel.EVENT_ADD_SELECTED_COMPONENT_LIST, (instanceList : Array<WVComponent>)=>{
		      this.sendNotification(EditorStatic.CMD_ADD_SELECTED_LIST, { list : instanceList, outlist : null, clear : false})
            });

		this.view.$on(OutlinePanel.EVENT_SHOW_SCRIPT_EDITOR, ()=>{
            this.sendNotification(EditorStatic.CMD_SHOW_WSCRIPT_EDITOR);
        })
	}


	public listNotificationInterests(): string[] {
		return [
			EditorProxy.NOTI_OPEN_PAGE,
			EditorProxy.NOTI_CLOSED_PAGE,
			EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
                  EditorProxy.NOTI_UPDATE_COM_INSTANCE

		]
	}


	public handleNotification(note: puremvc.Notification): void {
		//console.log("\n\n\nOutlinePanelMediator, handleNotification note = ", note);
            let editorProxy = null;
		switch (note.name) {
			case EditorProxy.NOTI_OPEN_PAGE :
				this.view.noti_openPage();
				break;
			case EditorProxy.NOTI_CLOSED_PAGE :
				this.view.noti_closedPage();
				break;
                  case EditorProxy.NOTI_UPDATE_COM_INSTANCE :
                        if(this._isFirst==false)
                              return;

                        this._deferredCall(()=>{
                              editorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
                              /*
                              instanceListGroup의 경우 호출할때마다 그룹 정보를 만들기 때문에 여러 번 호출하지 말아야함.
                               */
                              let list = editorProxy.activeLayerInfo.instanceListGroup;
                              if (list)
                                    this.view.updateComInstanceListGroup(list);
                        });
                        break;
			case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :
			      this._isFirst = true;
                        this._deferredCall(()=> {
                              let editorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
                              this.view.noti_changeActiveLayer(note.getBody(), editorProxy.activeLayerInfo.instanceListGroup);
                        });
				break;

		}

	}


      /*
      추가 삭제가 빈번히 일어나는 경우 한번에 처리하기 위해 deffered 추가
       */

      private _clearDeferred(): void {
            if (this._deferredID) {
                  clearTimeout(this._deferredID);
                  this._deferredID = 0;
            }
      }

      private _deferredCall(method:Function, args:any=null){
            this._clearDeferred();
            this._deferredID=setTimeout(()=>{
                  this._deferredID=0;
                  method.apply(this, args);
            },10);
      }


}


























