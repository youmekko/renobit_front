import Vue from "vue";
import * as puremvc from "puremvc";

import ComponentPropertyPanel from "./ComponentPropertyPanel.vue";
import SelectProxy from "../../../model/SelectProxy";
import {UpdatePropertyVO, LayerVisibleVO} from "../../../model/vo/ValueObjects";
import EditorStatic from "../../../controller/editor/EditorStatic";
import {PropertyPanelInfo} from "../../../data/Data";
import EditorProxy from "../../../model/EditorProxy";


export default class ComponentPropertyPanelMediator extends puremvc.Mediator {
      public static NAME: string = "ComponentPropertyPanelMediator";
      private view: ComponentPropertyPanel = null;


      constructor(name: string, viewComponent: Vue) {
            super(name, viewComponent);
            this.view = <ComponentPropertyPanel>viewComponent;
      }

      public getView(): ComponentPropertyPanel {
            return this.viewComponent;
      }

      public onRegister() {

            this.view.$on(ComponentPropertyPanel.EVENT_UPDATE_PROPERTIES, this.onUpdateProperty.bind(this));
            this.view.$on(ComponentPropertyPanel.EVENT_UPDATE_PROPERTIES, ()=>{
                  this.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);
            });
            this.view.$on(ComponentPropertyPanel.EVENT_SET_ACTIVE_SHORTCUT_STATE, (value:boolean)=>{
                  this.sendNotification(EditorStatic.CMD_SET_ACTIVE_SHORT_CUT_STATE, value);
            });

      }


      private onUpdateProperty(vo: UpdatePropertyVO) {
            let selectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
            selectProxy.setGroupPropertyValue(vo.groupName, vo.propertyName, vo.value);

      }


      public listNotificationInterests(): string[] {
            return [
                  SelectProxy.NOTI_UPDATE_SELECTED_INFO,
                  SelectProxy.NOTI_UPDATE_PROPERTIES,
                  EditorProxy.NOTI_OPEN_PAGE,
                  EditorProxy.NOTI_CLOSED_PAGE
            ]
      }


      public handleNotification(noti: puremvc.Notification): void {
            switch (noti.name) {
                  case SelectProxy.NOTI_UPDATE_PROPERTIES :
                  case SelectProxy.NOTI_UPDATE_SELECTED_INFO :
                        this._noti_setPropertyInfo();
                        break;

                  case EditorProxy.NOTI_OPEN_PAGE:
                        this.view.noti_openPage();
                        break;
                  case EditorProxy.NOTI_CLOSED_PAGE :
                        this.view.noti_closedPage();
                        break;



            }
      }


      private _noti_setPropertyInfo(){
            let selectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
            let info: PropertyPanelInfo = {
                  editableProperties: null,
                  propertyPanelDescription: null,
                  existExtensionProperties:false,
                  componentInstance:null
            }
            //console.log("\n\n 편접 기능 프로퍼티와 프로퍼티 정보(description) 요청 시작");
            info.editableProperties = selectProxy.editableProperties;
            info.propertyPanelDescription = selectProxy.propertyPanelDescription;
            let comInstanceList = selectProxy.currentPropertyManager.getInstanceList();

            if(comInstanceList.length==1){
                  if(comInstanceList[0].getExtensionProperties()){
                        info.existExtensionProperties = true;
                  }
            }
            info.componentInstance = comInstanceList[0];

            //console.log("편접 기능 프로퍼티와 프로퍼티 정보(description) 요청 완료\n\n ");
            this.view.noti_setPropertyInfo(info);
      }
}
