import { Component, Emit, Inject, Model, Prop, Provide, Vue, Watch } from 'vue-property-decorator'
import ComponentPropertyPanel from "../ComponentPropertyPanel.vue";
import {IWVComponent} from "../../../../../wemb/core/component/interfaces/ComponentInterfaces";

export default class CoreTemplate extends Vue {

	@Prop()
      model:any;

	@Prop()
      oldModel:any;

	@Prop()
      instance:IWVComponent;

	@Prop()
      eventBus:Vue;

      @Prop()
      propertyInfo:any;

      @Prop()
      propertyPanel:ComponentPropertyPanel;


      get primary(){
            return this.model.primary;
      }
      get setter() {
		return this.model.setter;
	}

	get style() {
	      return this.model.style;
      }


      get label() {
            return this.model.label;
      }

      get info(){
	      return this.model.info;
      }

      get editorMode(){
            return this.model.editorMode;
      }

      get version(){
            return "1.0.0";
      }

      get textShadow() {
            return this.model.textShadow;
      }

      get tooltip() {
            return this.model.tooltip;
      }

      get gradient() {
            return this.model.gradient;
      }

      get stroke() {
            return this.model.stroke;
      }

      get contentBorder() {
            return this.model.contentBorder;
      }

      get tableBackground() {
            return this.model.tableBackground;
      }

      get strokeStyle() {
            return this.model.strokeStyle;
      }

      get shadow() {
            return this.model.shadow;
      }

      constructor(){
            super();
      }


      /*
      하위 자식에서 재정의 해야함.
       */
      updatePropertyInfo(){

      }


	// 지원 타입 목록
	checkValidateType(type) {
		let typeList = ["number", "string", "object", "color", "checkbox", "slider", "select", "textarea"];
		if (typeList.indexOf(type) == -1) {
			return false;
		}
		return true;
	}

      checkValidateInputType(type) {
            let typeList = ["number", "color", "slider","checkbox", "select","textarea"];
            if (typeList.indexOf(type) == -1) {
                  return false;
            }
            return true;
      }

	// input 컴포넌트 구하기
	getInputComponent(type) {

		if (this.checkValidateInputType(type) == false) {
			return "normal-input";
		} else {
			return type + "-input";
		}
	}


	hasGroupProperties(groupName:string, propertyName:string){
		if(this.model.hasOwnProperty(groupName)==true){
			let group = this.model[groupName];
			if(group.hasOwnProperty(propertyName)==true){
				return true;
			}
		}
		return false;
	}

	onChange(groupName:string, propertyName: string, event: any) {
            let value = event;
            if(value.hasOwnProperty("originalEvent"))
                  value=event.value;

	      this.dispatchChangeEvent(groupName, propertyName,value)
	}

	/*
      onGroupPropertyChange(groupName:string, propertyName: string, value: any) {
            console.log("PrimaryTemplate propertyName =  ", propertyName, value);

            this.$emit("change", {groupName, propertyName, value})
      }
      */


      onChange3DProperty(propertyFullName, propertyName, childPropertyName, value, target=null){
            value =parseInt(value);

            if(target){
                  /// 유효성 처리
                  //let min = parseInt(target.getAttribute("min") || 0);
                  //let max = parseInt(target.getAttribute("max") || 0);

                  // if(value<min){
                  //       value=min;
                  // }
                  // if(value>max){
                  //       value=max;
                  // }
            }


            let ownerValue = Object.assign({}, this.model.props.normal[propertyName]);
            ownerValue[childPropertyName] = value;

            this.dispatchChangeEvent(propertyFullName, propertyName, ownerValue);

      }

      dispatchChangeEvent(groupName, propertyName, value){
            this.propertyPanel.onChange(groupName,propertyName,value);
      }

}


/*
2018.10.18(ckkim)
사용자 정의 템플릿 추가를 위한 es5용 코어 템플릿 추가
사용자 정의 템플릿에서 이 클래스를 확장해서 사용.
 */
window.CoreTemplate ={
      props:["model", "oldModel", "instance", "eventBus", "propertyInfo", "propertyPanel"],
      computed:{

            primary:function(){
                  return this.model.primary;
            },
            setter:function() {
                  return this.model.setter;
            },

            style:function() {
                  return this.model.style;
            },


            label:function() {
                  return this.model.label;
            },

            info:function(){
                  return this.model.info;
            },

            editorMode:function(){
                  return this.model.editorMode;
            },

            version:function(){
                  return "1.0.0";
            },

            textShadow:function(){
                  return this.model.textShadow;
            },

            gradient:function(){
                  return this.model.gradient;
            },

            stroke:function(){
                  return this.model.stroke;
            },

            contentBorder : function() {
                  return this.model.contentBorder;
            },

            tableBackground : function() {
                  return this.model.tableBackground;
            },

            strokeStyle:function(){
                  return this.model.strokeStyle;
            },

            shadow: function() {
                  return this.model.shadow;
            }

      },
      methods:{
            /*
하위 자식에서 재정의 해야함.
 */
            updatePropertyInfo:function(){

            },


            // 지원 타입 목록
            checkValidateType:function(type) {
                  let typeList = ["number", "string", "object", "color", "checkbox", "slider", "select", "textarea"];
                  if (typeList.indexOf(type) == -1) {
                        return false;
                  }
                  return true;
            },

            checkValidateInputType:function(type) {
                  let typeList = ["number", "color", "slider","checkbox", "select","textarea"];
                  if (typeList.indexOf(type) == -1) {
                        return false;
                  }
                  return true;
            },

            // input 컴포넌트 구하기
            getInputComponent:function(type) {

                  if (this.checkValidateInputType(type) == false) {
                        return "normal-input";
                  } else {
                        return type + "-input";
                  }
            },


            hasGroupProperties:function(groupName, propertyName){
                  if(this.model.hasOwnProperty(groupName)==true){
                        let group = this.model[groupName];
                        if(group.hasOwnProperty(propertyName)==true){
                              return true;
                        }
                  }
                  return false;
            },

            onChange:function(groupName, propertyName, event) {
                  let value = event;
                  if(value.hasOwnProperty("originalEvent"))
                        value=event.value;

                  this.dispatchChangeEvent(groupName, propertyName,value)
            },
            dispatchChangeEvent(groupName, propertyName, value){
                  this.propertyPanel.onChange(groupName,propertyName,value);
            }
      }
};

