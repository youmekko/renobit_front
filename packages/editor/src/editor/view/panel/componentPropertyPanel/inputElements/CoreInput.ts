	import {Component, Prop, Vue } from 'vue-property-decorator'
      @Component({})
	export default class CoreInput extends Vue {

		@Prop()
		value:string|number;


            @Prop()
            eventBus:Vue;

		@Prop()
            propertyInfo:any;

            @Prop()
            model:any;

            // 입력 시 ESC키를 눌렀는지지 유무 판단, ESC키를 누르는 경우 CHANGE 이벤를 발생시키지 않기 위해 사용.
            protected isCancel: boolean = false;
            // 최초 focus를 가지게 되는 경우 값, 즉 v-model 값이 됨, 입력 취소(ESC)키를 누르는 경우 이 값으로 되돌려짐.
            protected oldValue: string|number;
            // 가장 최근에 정상적으로 입력한 값, 유효성에 맞지 않는 값이 입력되는 경우 이 값으로 되돌려짐.
            protected tempValue: string|number;

		updateValue(event, value){
                  this.tempValue = value;
                  this.$emit("input", value);
		}


		onFocus(){

			this.tempValue = this.oldValue = this.value;
			this.isCancel = false;
                  if(this.eventBus){
                        this.eventBus.$emit("inputFocus",{
                              target:event.target
                        })
                  }
		}

            /*
            change event는
              - ESC키를 누른 상태에서 포커스를 잃어버리는 경우 X
              - FOUC가 있는 상태에서 focus를 잃어버리는 경우 O
              - FOCUS가 있는 상태에서 enter키를 누른 경우 O
           */

            onBlur(event) {
                  /*
			ESC키가 아닌 포커스가 나가게 되는 경우
			 현재 입력된 값으로 change 이벤트 발생 시키기.
			 */
                  if (this.isCancel == false) {
                        this.$emit("change", {originalEvent: event, target: event.target, value: this.value});
                  }
                  this.isCancel = false;

                  if (this.eventBus) {
                        this.eventBus.$emit("inputBlur", {
                              target: event.target
                        })
                  }
            }


            onKeyUp(event) {
                  //  입력 취소 처리
                  if (event.key == "Escape") {
                        this.isCancel = true;

                        // ESC키를 누르는 경우 최초 초기 값으로
                        this.$emit("input", this.oldValue);
                        // 이후 blur처리
                        this.$refs.input["blur"]();

                  }

                  // 입력 완료 처리
                  if (event.key == "Enter") {
                        // Enter키를 입력하는 경우 input 이벤트 발생
                        //this.inputValue = event.target.value;

                        this.$emit("input", event.target.value);

                        this.$refs.input["blur"]();
                  }
            }

	}
