import WVComponent from "../../wemb/core/component/WVComponent";


export type PropertyPanelInfo={
      editableProperties:any;
      propertyPanelDescription:any;
      existExtensionProperties:boolean,
      componentInstance:WVComponent

}
