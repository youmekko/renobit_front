import Vue from "vue";
import axios from "axios";


import ComponentLibaryManager from "../../wemb/wv/managers/ComponentLibraryManager";
import PageManager from "../../wemb/wv/managers/PageManager";
import ConfigManager from "../../wemb/wv/managers/ConfigManager";
import MainMenuManager from "../../wemb/wv/managers/MainMenuManager";

import PageTreeDataManager from "../../wemb/wv/managers/PageTreeDataManager";
import ILoadingModal from "../../common/modal/ILoadingModal";
import EditorAppFacade from "../EditorAppFacade";
import KeyboardCommandManager from "../../wemb/wv/managers/KeyboardCommandManager";
import WindowManager from "../../wemb/wv/managers/WindowsManager";

import EditorPageComponent from "../../wemb/wv/components/EditorPageComponent";
import EditorProxy from "../model/EditorProxy";
import DataService from "../../wemb/wv/managers/DataService";
import LocaleManager from "../../wemb/wv/managers/LocaleManager";

declare function httpVueLoader(url: string, name: string);

import CreatePageModal from "../view/modal/CreatePageModal.vue";
import ReferencePagesModal from "../view/modal/ReferencePagesModal.vue";
import ExportPagesModal from "../view/modal/ExportPagesModal/ExportPagesModal.vue";
import ResourceManagerModal from "../view/manager/resource-manager/ResourceManagerModal.vue";
import RestoreProjectModal from "../view/modal/RestoreProjectModal.vue";
import MenusetManagerModal from "../view/manager/menuset-manager/MenusetManagerModal.vue";
import TemplateManagerModal from "../view/manager/template-manager/TemplateManagerModal.vue";


import ExportResourcesManagerModal from "../view/manager/export-resource-manager/ExportResourcesManagerModal.vue";

// 2019.02.12 추가
import ThreeAlignModal from "../view/modal/ThreeAlignModal.vue";

import DynamicModalComponent from "../../wemb/wv/components/DynamicModalComponent.vue";
import ProductInfoModal from "../view/modal/ProductInfoModal.vue"
import ShortcutsInfoModal from "../view/modal/ShortcutsInfoModal.vue"
import {IBackgroundElement, IThreeExtensionElements} from "../../wemb/core/component/interfaces/ComponentInterfaces";
import {EXE_MODE} from "../../wemb/wv/data/Data";
import WVComponentEvent from "../../wemb/core/events/WVComponentEvent";
import ExtensionInstanceManager from "../../wemb/wv/managers/ExtensionInstanceManager";
import UserInfoManager from "../../wemb/wv/managers/UserInfoManager";
import MenusetDataManager from "../../wemb/wv/managers/MenusetDataManager";
import TemplateDataManager from "../../wemb/wv/managers/TemplateDataManager";
import EditorStatic from "../controller/editor/EditorStatic";


import HookManager from "../../wemb/wv/managers/HookManager";
import {ObjectUtil} from "../../wemb/utils/Util";

import EditMainAreaResourceLoadingComponent from "../../wemb/wv/components/EditMainAreaResourceLoadingComponent.vue";
import {ShowRestoreProjectCommand} from "../controller/main/ShowRestoreProjectCommand";

declare function httpVueLoader(url: string, name: string);

export default class WeMB {
      public static WVComponentEvent = WVComponentEvent;
      public static ObjectUtil = ObjectUtil;
      public $http: any;
      public $globalBus: Vue;
      public $componentBus: Vue;
      public $httpVueLoader = httpVueLoader;
      public $modalManager: DynamicModalComponent;
      public $defaultLoadingModal: ILoadingModal;
      public $editMainAreaResourceLoadingComponent: EditMainAreaResourceLoadingComponent

      public componentLibraryManager: ComponentLibaryManager;
      public pageManager: PageManager;
      public pageTreeDataManager: PageTreeDataManager;
      public configManager: ConfigManager;
      public extensionInstanceManager: ExtensionInstanceManager;
      public windowsManager: WindowManager;
      public dataService: DataService;
      public localeManager: LocaleManager;
      public menusetDataManager: MenusetDataManager;
      public templateDataManager: TemplateDataManager;
      public userInfo: UserInfoManager;

      public mainMenuManager: MainMenuManager;
      public viewComponentMap: Map<string, Vue> = new Map();
      public editorFacade: EditorAppFacade;
      public editorProxy: EditorProxy;
      public keyboardCommandManager: KeyboardCommandManager;


      public mainControls: any;
      public personControls: any;

      public mainPageComponent: EditorPageComponent;
      public stageBackgroundElement: IBackgroundElement;
      public pageBackgroundElement: IBackgroundElement;


      public $createPageModal: CreatePageModal;
      public $referencePagesModal: ReferencePagesModal;
      public $exportPagesModal: ExportPagesModal;
      public $resourceManagerModal: ResourceManagerModal;
      public $restoreProjectModal: RestoreProjectModal;
      public $menusetManagerModal: MenusetManagerModal;
      public $templateManagerModal: TemplateManagerModal;


      public $exportResourceManagerModal: ExportResourcesManagerModal;
      public $productInfoModal: ProductInfoModal;
      public $shortcutsInfoModal: ShortcutsInfoModal;

      //2019.02.12 추가
      public $threeAlignModal: ThreeAlignModal;


      public domEvents: any;
      public camera: any;
      public renderer: any;
      public scene: any;
      public raycaster: any;


      /*timer*/
      public setInterval: Function;
      public setTimeout: Function;
      public clearTimeout: Function;
      public clearInterval: Function;

      private _threeCoreElements: IThreeExtensionElements = null;
      public globalStore: Map<string, any>;


      get threeElements() {
            return this._threeCoreElements;
      }

      get hookManager(): HookManager {
            return HookManager.getInstance();
      }


      constructor() {
            this.$http = axios;
            this.$globalBus = new Vue();
            this.$componentBus = new Vue();
            this.$httpVueLoader = httpVueLoader;

            this.pageManager = new PageManager();
            this.menusetDataManager = MenusetDataManager.getInstance();


            //this.configManager = new ConfigManager(EXE_MODE.EDITOR);
            this.configManager = ConfigManager.getInstance();
            this.configManager.exeMode = EXE_MODE.EDITOR;
            this.userInfo = UserInfoManager.getInstance();

            this.templateDataManager = TemplateDataManager.getInstance();

            this.extensionInstanceManager = ExtensionInstanceManager.getInstance();

            this.pageTreeDataManager = new PageTreeDataManager();

            //popup windowsManager
            this.windowsManager = WindowManager.getInstance();

            // 컴포넌트 라이브러리 관리자
            this.componentLibraryManager = new ComponentLibaryManager(this.configManager.exeMode);

            // 단축키 관리자
            this.keyboardCommandManager = new KeyboardCommandManager();

            this.localeManager = LocaleManager.getInstance();

            this.mainMenuManager = new MainMenuManager();

            this.globalStore = new Map();


      }

      /*

      call : 3d container에서 호출, 추후 호출 위치를 획일화 시켜야 함.
       */
      setThreeElements(threeElements: IThreeExtensionElements) {
            this.camera = threeElements.camera;
            this.mainControls = threeElements.mainControls;
            this.personControls = threeElements.personControls;
            this.domEvents = threeElements.domEvents;
            this.renderer = threeElements.renderer;
            this.scene = threeElements.scene;
            this.raycaster = threeElements.raycaster;

            this._threeCoreElements = threeElements;
      }


      /*
      2018.11.05(ckkim)
      외부 모달을 동적으로 활성화 처리


      @modalName: 활성화 모달에 붙일 이름
      @fileName: 모달이 구현된 파일
      @opener: 모달을 활성화 시킨 대상
      @data: 데이터
      @closedCallback: 모달이 닫힐때 실행할 callback function

      call : editor에서만 사용.

       */
      public showExternalModal(modalName: string, fileName: string, opener: any = null, data: any = null, closedCallback: Function = null) {
            this.editorFacade.sendNotification(EditorStatic.CMD_SHOW_MODAL, {
                  modalName: modalName,
                  fileName: fileName,
                  opener: opener,
                  data: data,
                  closed: closedCallback
            })
      }
}
