import * as puremvc from "puremvc";
import ScriptEditorProxy from "../model/ScriptEditorProxy";

export class CopySnippetCodeToClipboardCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let scriptEditorProxy: ScriptEditorProxy = <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
		//scriptEditorProxy.test();
		let id:string = <string>note.getBody();
		scriptEditorProxy.copyToClipboard(id);
    }
}
