import * as puremvc from "puremvc";
import ScriptEditorProxy from "../model/ScriptEditorProxy";
import { dto } from "../model/dto/SnippetDTO";
import SnippetTreeItemDTO = dto.SnippetTreeItemDTO;

export class UpdateCustomSnippetTreeCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let scriptEditorProxy: ScriptEditorProxy = <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
		let tree:Array<SnippetTreeItemDTO> = note.getBody();
		scriptEditorProxy.updateCustomSnippets(tree);
    }
}
