export default class ScriptEditorStatic {
	public static CMD_LOAD_GLOBAL_SNIPPETS: string = "command/loadGolobalSnippets";
	public static CMD_LOAD_FAVORITE_SNIPPETS: string = "command/loadFavoriteSnippets";
	public static CMD_LOAD_CUSTOM_SNIPPETS: string = "command/loadCustomSnippets";

	public static CMD_ADD_FAVORITE_SNIPPET: string = "command/addFavoriteSnippet";
	public static CMD_REMOVE_FAVORITE_SNIPPET: string = "command/removeFavoriteSnippet";
	public static CMD_INSERT_SNIPPET_CODE: string = "command/insertSnippetCode";
	public static CMD_COPY_SNIPPET_CODE_TO_CLIPBOARD: string = "command/copySnippetCodeToClipboard";

	public static CMD_ADD_CUSTOM_SNIPPET: string = "command/addCustomSnippet";
	public static CMD_MODIFY_CUSTOM_SNIPPET: string = "command/modifyCustomSnippet";
	public static CMD_UPDATE_CUSTOM_SNIPPET_TREE: string = "command/updateCustomSnippetTree";
}