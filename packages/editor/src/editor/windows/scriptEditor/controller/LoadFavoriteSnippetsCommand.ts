import * as puremvc from "puremvc";
import ScriptEditorProxy from "../model/ScriptEditorProxy";

export class LoadFavoriteSnippetsCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let scriptEditorProxy: ScriptEditorProxy = <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
		scriptEditorProxy.loadFavoriteSnippets();
    }
}
