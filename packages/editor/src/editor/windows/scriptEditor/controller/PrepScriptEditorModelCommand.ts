import * as puremvc from "puremvc";
import ScriptEditorProxy from "../model/ScriptEditorProxy";

export class PrepScriptEditorModelCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		this.facade.registerProxy( new ScriptEditorProxy() );
	}
}