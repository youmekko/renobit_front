import * as puremvc from "puremvc";
import ScriptEditorProxy from "../model/ScriptEditorProxy";

export class InsertSnippetCodeCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let scriptEditorProxy: ScriptEditorProxy = <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
		let id:string = <string>note.getBody();
		scriptEditorProxy.insertCode(id);
    }
}
