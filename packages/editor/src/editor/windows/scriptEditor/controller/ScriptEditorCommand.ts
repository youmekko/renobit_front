import * as puremvc from "puremvc";
import ScriptEditorStatic from "./ScriptEditorStatic";
import EditorStatic from "../../../controller/editor/EditorStatic";
import { LoadGlobalSnippetsCommand } from "./LoadGlobalSnippetsCommand";
import { AddFavoriteSnippetCommand } from "./AddFavoriteSnippetCommand";
import { RemoveFavoriteSnippetCommand } from "./RemoveFavoriteSnippetCommand";
import { InsertSnippetCodeCommand } from "./InsertSnippetCodeCommand";
import { CopySnippetCodeToClipboardCommand } from "./CopySnippetCodeToClipboardCommand";
import { LoadFavoriteSnippetsCommand } from "./LoadFavoriteSnippetsCommand";
import { LoadCustomSnippetsCommand } from "./LoadCustomSnippetsCommand";
import { UpdateCustomSnippetTreeCommand } from "./UpdateCustomSnippetTreeCommand";
import { AddCustomSnippetCommand } from "./AddCustomSnippetCommand";
import { ModifyCustomSnippetCommand } from "./ModifyCustomSnippetCommand";
import { ShowManualCommand} from "../../../controller/main/ShowManualCommand";

export class ScriptEditorCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		this.facade.registerCommand(ScriptEditorStatic.CMD_LOAD_GLOBAL_SNIPPETS, LoadGlobalSnippetsCommand);
		this.facade.registerCommand(ScriptEditorStatic.CMD_LOAD_FAVORITE_SNIPPETS, LoadFavoriteSnippetsCommand);
		this.facade.registerCommand(ScriptEditorStatic.CMD_LOAD_CUSTOM_SNIPPETS, LoadCustomSnippetsCommand);
		
		this.facade.registerCommand(ScriptEditorStatic.CMD_ADD_FAVORITE_SNIPPET, AddFavoriteSnippetCommand);
		this.facade.registerCommand(ScriptEditorStatic.CMD_REMOVE_FAVORITE_SNIPPET, RemoveFavoriteSnippetCommand);
		this.facade.registerCommand(ScriptEditorStatic.CMD_INSERT_SNIPPET_CODE, InsertSnippetCodeCommand);
		this.facade.registerCommand(ScriptEditorStatic.CMD_COPY_SNIPPET_CODE_TO_CLIPBOARD, CopySnippetCodeToClipboardCommand);
		this.facade.registerCommand(ScriptEditorStatic.CMD_UPDATE_CUSTOM_SNIPPET_TREE, UpdateCustomSnippetTreeCommand);
	
		this.facade.registerCommand(ScriptEditorStatic.CMD_ADD_CUSTOM_SNIPPET, AddCustomSnippetCommand);
		this.facade.registerCommand(ScriptEditorStatic.CMD_MODIFY_CUSTOM_SNIPPET, ModifyCustomSnippetCommand);
            this.facade.registerCommand(EditorStatic.CMD_SHOW_MANUAL, ShowManualCommand);
	}
}
