import * as puremvc from "puremvc";
import { PrepScriptEditorModelCommand } from "./PrepScriptEditorModelCommand";
import { PrepScriptEditorViewCommand } from "./PrepScriptEditorViewCommand";
import { ScriptEditorCommand } from "./ScriptEditorCommand";


export default class ScriptEditorStartUpCommand extends puremvc.MacroCommand {
	initializeMacroCommand() {
		console.log("step 02, StartUpCommand 실행 view, model, command 등록");
		this.addSubCommand(PrepScriptEditorModelCommand);
		this.addSubCommand(PrepScriptEditorViewCommand);
		this.addSubCommand(ScriptEditorCommand);
	}
}