import * as puremvc from "puremvc";
import ScriptEditorProxy from "../model/ScriptEditorProxy";
import { dto } from "../model/dto/SnippetDTO";
import SnippetFavoriteItemDTO = dto.SnippetFavoriteItemDTO;

export class AddFavoriteSnippetCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let scriptEditorProxy: ScriptEditorProxy = <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
		//scriptEditorProxy.test();
		let item = <SnippetFavoriteItemDTO>note.getBody();
		scriptEditorProxy.addFavoriteSnippet(item);
    }
}
