import * as puremvc from "puremvc";
import ScriptEditorMainMediator from "../view/ScriptEditorMainMediator";
import ScriptEditAreaMediator from "../view/ScriptEditAreaMediator";
import CodeSnippetsMainMediator from "../view/code-snippets/CodeSnippetsMainMediator";
import CustomSnippetsTreeMediator from "../view/code-snippets/custom/CustomSnippetsTreeMediator";
import CreateCustomSnippetModalMediator from "../view/code-snippets/custom/CreateCustomSnippetModalMediator";
import ComponentInfoMediator from "../view/component-info/ComponentInfoMediator";

export class PrepScriptEditorViewCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let scriptEditorMainMediator: puremvc.IMediator = new ScriptEditorMainMediator(ScriptEditorMainMediator.NAME, note.getBody());
		this.facade.registerMediator(scriptEditorMainMediator);
		this.facade.registerMediator(new ScriptEditAreaMediator(ScriptEditAreaMediator.NAME, window.wemb.viewComponentMap.get(ScriptEditAreaMediator.NAME)));
		this.facade.registerMediator(new CodeSnippetsMainMediator(CodeSnippetsMainMediator.NAME, window.wemb.viewComponentMap.get(CodeSnippetsMainMediator.NAME)));
		this.facade.registerMediator(new CustomSnippetsTreeMediator(CustomSnippetsTreeMediator.NAME, window.wemb.viewComponentMap.get(CustomSnippetsTreeMediator.NAME)));
		// 신규 추가(ddandongne), 2018.06.13
        this.facade.registerMediator(new ComponentInfoMediator(ComponentInfoMediator.NAME, window.wemb.viewComponentMap.get(ComponentInfoMediator.NAME)));
		this.facade.registerMediator(new CreateCustomSnippetModalMediator(CreateCustomSnippetModalMediator.NAME, window.wemb.viewComponentMap.get(CreateCustomSnippetModalMediator.NAME)));
	}
}
