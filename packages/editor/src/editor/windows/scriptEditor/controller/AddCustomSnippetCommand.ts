import * as puremvc from "puremvc";
import ScriptEditorProxy from "../model/ScriptEditorProxy";
import { dto } from "../model/dto/SnippetDTO";
import CustomSnippetItemDTO = dto.CustomSnippetItemDTO;

export class AddCustomSnippetCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let scriptEditorProxy: ScriptEditorProxy = <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
		//scriptEditorProxy.test();
		let item = <CustomSnippetItemDTO>note.getBody();
		scriptEditorProxy.addCustomSnippet(item);
    }
}
