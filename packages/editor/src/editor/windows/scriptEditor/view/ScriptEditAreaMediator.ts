import * as puremvc from "puremvc";
import ScriptEditAreaComponent from "./ScriptEditAreaComponent.vue";
import Vue from "vue";
import ScriptEditorProxy from "../model/ScriptEditorProxy";
import ScriptEditorFacade from "../ScriptEditorFacade";

export default class ScriptEditAreaMediator extends puremvc.Mediator {
	public static NAME: string = "ScriptEditAreaMediator";
    private view: ScriptEditAreaComponent = null;

	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <ScriptEditAreaComponent>viewComponent;
    }

	private get proxy():ScriptEditorProxy{

		return <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
	}

	public getView(): ScriptEditAreaComponent {
		return this.viewComponent;
	}

	public onRegister() {
	    //컴포넌트 선택되었을 때 NOTI, 이 NOTI로 하단 프로퍼티 정보가 업데이트됨
		this.view.$on(ScriptEditAreaComponent.EVENT_COMPONENT_FOCUS,(component)=>{
	        this.sendNotification(ScriptEditorFacade.NOTI_COMPONENT_FOCUS, component);
        })
	}

	

	public listNotificationInterests(): string[] {
		return [
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		
	}
}
