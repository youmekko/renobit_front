import * as puremvc from "puremvc";
import CodeSnippetsMainComponent from "./CodeSnippetsMainComponent.vue";
import Vue from "vue";
import { dto } from '../../model/dto/SnippetDTO';
import SnippetTreeItemDTO = dto.SnippetTreeItemDTO;
import SnippetFavoriteItemDTO = dto.SnippetFavoriteItemDTO;
import CustomSnippetItemDTO = dto.CustomSnippetItemDTO;
import ScriptEditorStatic from "../../controller/ScriptEditorStatic";
import ScriptEditorProxy from "../../model/ScriptEditorProxy";
import FileManager from "../../../../../wemb/wv/managers/FileManager";
import EditorStatic from "../../../../controller/editor/EditorStatic";

export default class CodeSnippetsMainMediator extends puremvc.Mediator {
	public static NAME: string = "CodeSnippetsMainMediator";
    private view: CodeSnippetsMainComponent = null;
	private globalTree:Array<SnippetTreeItemDTO> = [];
	private favoriteSnippets: Array<SnippetFavoriteItemDTO> = [];
	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);		
		this.view = <CodeSnippetsMainComponent>viewComponent;
    }

	private get proxy():ScriptEditorProxy{		
		return <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
	}

	public getView(): CodeSnippetsMainComponent {
		return this.viewComponent;
	}

	/*사용자 스니펫 가져오기 처리*/
    private importCustomSnippetListAsFile(file:any){
		let self = this;
		var locale_msg = Vue.$i18n.messages.wv;
		var reader = new FileReader();

		if (file.name.search(/.json$/) === -1) {
                  Vue.$message.error(locale_msg.common.errorFormat);
            } else {
                  reader.onloadend = function(evt:any) {
                        //if (evt.target.readyState == state) {
                        try {
                              let data = JSON.parse(evt.target.result);
                              self.proxy.mergeCustomSnippets( data );
                              Vue.$message(locale_msg.common.file + locale_msg.common.successImport);
                        } catch (error) {
                              Vue.$message.error( locale_msg.common.failImport );
                        }
                        //}
                  };

                  reader.readAsText(file);
            }
    }

	/*사용자 스니펫 내보내기 처리*/
	exportCustomSnippetListAsFile() {	
        let jsonStr = JSON.stringify(this.proxy.getCustomSnippetsJSON());
        let fileManager = new FileManager();
        fileManager.saveTextFile("customSnippets.json", jsonStr);
        fileManager.onDestroy();
    }
	

	public onRegister() {
		let self = this;

		//즐겨찾기 추가
        this.view.$on(CodeSnippetsMainComponent.EVENT_ADD_FAVORITE, (item:SnippetFavoriteItemDTO)=>{
			this.sendNotification( ScriptEditorStatic.CMD_ADD_FAVORITE_SNIPPET, item );
		})

		//즐겨찾기 삭제
		this.view.$on(CodeSnippetsMainComponent.EVENT_REMOVE_FAVORITE, (codeId:string)=>{
			this.sendNotification( ScriptEditorStatic.CMD_REMOVE_FAVORITE_SNIPPET, codeId );
		})

		//클립보드에 코드 복사
		this.view.$on(CodeSnippetsMainComponent.EVENT_COPY_TO_CLIPBOARD, (codeId:string)=>{
			this.sendNotification( ScriptEditorStatic.CMD_COPY_SNIPPET_CODE_TO_CLIPBOARD, codeId );
		})

		//코드 삽입
		this.view.$on(CodeSnippetsMainComponent.EVENT_INSERT_CODE, (codeId:string)=>{
			this.sendNotification( ScriptEditorStatic.CMD_INSERT_SNIPPET_CODE, codeId );
		})

		///사용자 코드 수정
		this.view.$on(CodeSnippetsMainComponent.EVENT_MODIFY_CUSTOM_SNIPPET, (item:CustomSnippetItemDTO)=>{
			this.sendNotification( ScriptEditorStatic.CMD_MODIFY_CUSTOM_SNIPPET, item );
		})

		//사용자 코드 수정
		this.view.$on(CodeSnippetsMainComponent.EVENT_ADD_CUSTOM_SNIPPET, (item:CustomSnippetItemDTO)=>{
			this.sendNotification( ScriptEditorStatic.CMD_ADD_CUSTOM_SNIPPET, item );
		})

		//사용자 코드 추가/수정 모달 띄우기
		this.view.$on(CodeSnippetsMainComponent.EVENT_SHOW_CUSTOM_SNIPPET_MODAL, (item:CustomSnippetItemDTO)=>{
			if(item != null){
				item.code = this.proxy.getCodeById(item.id);//수정일경우..	
			}
			
			this.view.showCreateCustomSnippetModal( item );
		})

		//파일가져오기
		this.view.$on(CodeSnippetsMainComponent.EVENT_IMPORT_CUSTOM_SNIPPETS, (file)=>{
			this.importCustomSnippetListAsFile(file);
		})

		//파일내보내기
		this.view.$on(CodeSnippetsMainComponent.EVENT_EXPORT_CUSTOM_SNIPPETS, ()=>{
			this.exportCustomSnippetListAsFile();
		})

            //매뉴얼 가기
            this.view.$on(CodeSnippetsMainComponent.EVENT_GOTO_MANUAL, (path:string)=>{
                  this.sendNotification(EditorStatic.CMD_SHOW_MANUAL, path)
            })
      }

	public listNotificationInterests(): string[] {
		return [			
			ScriptEditorProxy.NOTI_LOAD_GLOBAL_SNIPPETS,
			ScriptEditorProxy.NOTI_UPDATE_FAVORITE_SNIPPETS,
			ScriptEditorProxy.NOTI_LOAD_FAVORITE_SNIPPETS,
			ScriptEditorProxy.NOTI_CHANGE_FOCUS_STATE
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		switch(note.name){
            case ScriptEditorProxy.NOTI_LOAD_GLOBAL_SNIPPETS:
				this.globalTree = note.body;
				this.view.setGlobalSnippets(this.globalTree);
            break;
			case ScriptEditorProxy.NOTI_LOAD_FAVORITE_SNIPPETS:
				this.favoriteSnippets = note.body;
				this.view.setFavoriteSnippets(this.favoriteSnippets);
            break;
			case ScriptEditorProxy.NOTI_UPDATE_FAVORITE_SNIPPETS:
				this.favoriteSnippets = note.body;
				this.view.setFavoriteSnippets(this.favoriteSnippets)
            break;
			case ScriptEditorProxy.NOTI_CHANGE_FOCUS_STATE:
				let isFocus = this.proxy.isEditFocus;
				this.view.isFocus = isFocus;
			break;
        }
	}

	

}
