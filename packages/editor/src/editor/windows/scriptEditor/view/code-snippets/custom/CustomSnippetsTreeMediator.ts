import * as puremvc from "puremvc";
import CustomSnippetsTreeComponent from "./CustomSnippetsTreeComponent.vue";
import Vue from "vue";
import { dto } from '../../../model/dto/SnippetDTO';
import SnippetTreeItemDTO = dto.SnippetTreeItemDTO;
import SnippetFavoriteItemDTO = dto.SnippetFavoriteItemDTO;
import CustomSnippetItemDTO = dto.CustomSnippetItemDTO;
import ScriptEditorStatic from "../../../controller/ScriptEditorStatic";
import ScriptEditorProxy from "../../../model/ScriptEditorProxy";

export default class CustomSnippetsTreeMediator extends puremvc.Mediator {
	public static NAME: string = "CustomSnippetsTreeMediator";
    private view: CustomSnippetsTreeComponent = null;

	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);		
		this.view = <CustomSnippetsTreeComponent>viewComponent;
        
        //console.log("proxy", this.proxy.getGlobalSnippetsTreeData)
    }

	private get proxy():ScriptEditorProxy{		
		return <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
	}

	public getView(): CustomSnippetsTreeComponent {
		return this.viewComponent;
	}

	public onRegister() {
		let self = this;
		
       this.view.$on(CustomSnippetsTreeComponent.EVENT_UPDATE_CUSTOM_SNIPPET_TREE, (treeData:Array<SnippetTreeItemDTO>)=>{
			this.sendNotification( ScriptEditorStatic.CMD_UPDATE_CUSTOM_SNIPPET_TREE, treeData );
		}) 

	}

	public listNotificationInterests(): string[] {
		return [		
			ScriptEditorProxy.NOTI_LOAD_CUSTOM_SNIPPETS,	
			ScriptEditorProxy.NOTI_ADD_CUSTOM_SNIPPETS,
			ScriptEditorProxy.NOTI_MODIFY_CUSTOM_SNIPPETS,
			ScriptEditorProxy.NOTI_REMOVE_CUSTOM_SNIPPETS,
			ScriptEditorProxy.NOTI_IMPORT_CUSTOM_SNIPPETS,
			ScriptEditorProxy.NOTI_CHANGE_FOCUS_STATE
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		let treeData:any;
		switch(note.name){
			case ScriptEditorProxy.NOTI_LOAD_CUSTOM_SNIPPETS:
				treeData = note.body;	
				this.view.setTreeData(treeData);
			break;
            case ScriptEditorProxy.NOTI_ADD_CUSTOM_SNIPPETS:
				let addItem:CustomSnippetItemDTO = note.body;		
				this.view.addTreeItem(addItem);
            break;
			case ScriptEditorProxy.NOTI_MODIFY_CUSTOM_SNIPPETS:
				let modifyItem:CustomSnippetItemDTO = note.body;		
				this.view.modifyTreeItem(modifyItem);
			break;
			case ScriptEditorProxy.NOTI_REMOVE_CUSTOM_SNIPPETS:
            break;
			case ScriptEditorProxy.NOTI_IMPORT_CUSTOM_SNIPPETS:
				treeData = note.body;	
				this.view.redrawTree(treeData);
            break;
			case ScriptEditorProxy.NOTI_CHANGE_FOCUS_STATE:
				let isFocus = this.proxy.isEditFocus;
				this.view.isFocus = isFocus;
			break;
        }
	}
}