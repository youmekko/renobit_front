import * as puremvc from "puremvc";
import CreateCustomSnippetModal from "./CreateCustomSnippetModal.vue";
import Vue from "vue";
import { dto } from '../../../model/dto/SnippetDTO';
import CustomSnippetItemDTO = dto.CustomSnippetItemDTO;
import ScriptEditorStatic from "../../../controller/ScriptEditorStatic";
import ScriptEditorProxy from "../../../model/ScriptEditorProxy";

export default class CreateCustomSnippetModalMediator extends puremvc.Mediator {
	public static NAME: string = "CreateCustomSnippetModalMediator";
    private view: CreateCustomSnippetModal = null;

	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);		
		this.view = <CreateCustomSnippetModal>viewComponent;
    }

	private get proxy():ScriptEditorProxy{		
		return <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
	}

	public getView(): CreateCustomSnippetModal {
		return this.viewComponent;
	}

	public onRegister() {
        this.view.$on(CreateCustomSnippetModal.EVENT_MODIFY_CUSTOM_SNIPPET , (item:CustomSnippetItemDTO)=>{
			this.modifyCustomSnippet(item);
		})

		this.view.$on(CreateCustomSnippetModal.EVENT_ADD_CUSTOM_SNIPPET , (item:CustomSnippetItemDTO)=>{
			this.addCustomSnippet(item);
		})

	}

	private modifyCustomSnippet(item:CustomSnippetItemDTO){
		if(this.validationSnippetData(item)){
			this.sendNotification( ScriptEditorStatic.CMD_MODIFY_CUSTOM_SNIPPET, item );
			this.view.hide();
		}
	}

	private addCustomSnippet(item:CustomSnippetItemDTO){
		if(this.validationSnippetData(item)){
			this.sendNotification( ScriptEditorStatic.CMD_ADD_CUSTOM_SNIPPET, item );
			this.view.hide();
		}
	}

	private validationSnippetData(item:CustomSnippetItemDTO):boolean{
		if(!item.title.length){
			Vue.$message.error('Please enter title.');
			return false;
		}else if( !item.code.length ){
			Vue.$message.error('Please enter code.');
			return false;
		}
		return true;
	}

	public listNotificationInterests(): string[] {
		return [			
			ScriptEditorProxy.NOTI_ADD_CUSTOM_SNIPPETS,
			ScriptEditorProxy.NOTI_MODIFY_CUSTOM_SNIPPETS
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		switch(note.name){			
            case ScriptEditorProxy.NOTI_ADD_CUSTOM_SNIPPETS:
			case ScriptEditorProxy.NOTI_MODIFY_CUSTOM_SNIPPETS:
				//let item = note.body;	
				
            break;
		
        }
	}
}