import * as puremvc from "puremvc";
import Vue from "vue";
import ComponentInfoComponent from "./ComponentInfoComponent.vue";
import ScriptEditorProxy from "../../model/ScriptEditorProxy";
import ScriptEditorFacade from "../../ScriptEditorFacade";

export default class ComponentInfoMediator extends puremvc.Mediator {
	public static NAME: string = "ComponentInfosMediator";
    private view: ComponentInfoComponent = null;
	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <ComponentInfoComponent>viewComponent;
    }

	public getView(): ComponentInfoComponent {
		return this.viewComponent;
	}

	public onRegister() {
		this.view.$on(ComponentInfoComponent.EVENT_INSERT_CODE, (code:string)=>{
				this.sendNotification(ScriptEditorProxy.NOTI_INSERT_CODE, code);
		})
	}

	public listNotificationInterests(): string[] {
		return [
            ScriptEditorFacade.NOTI_COMPONENT_FOCUS
		]
	}

	public handleNotification(note: puremvc.Notification): void {
            switch(note.name){
                  case ScriptEditorFacade.NOTI_COMPONENT_FOCUS :
                        this.view.noti_componentFocus(note.getBody());
            }
	}
}
