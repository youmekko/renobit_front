import * as puremvc from "puremvc";
import ScriptEditorMainComponent from "./ScriptEditorMainComponent.vue";
import Vue from "vue";
import ScriptEditorProxy from "../model/ScriptEditorProxy";
import ScriptEditorWorker from "../ScriptEditorWorker";
import ScriptEditorStatic from "../controller/ScriptEditorStatic";

/**
 * ScriptEditorMainMediator
 * channel통신(타겟 변경, 이벤트 핸들러 내용 업데이트),
 * snippets 시작(main Editor에서 serverUrl을 받은 후) 처리
 * server url은 onRegister에서 channel을 통해 MainEditor(ScriptEditorWorker)에 ServerUrl정보를 달라고 메세지로 요청한다 - ScriptEditorWorker.CMD_GET_SERVER_URL
 * opener.wemb.configManager에 접근할경우 ie에서 상당히 느려지기 때문에 Channel연결을 확인 한 후 요청하여 channel을 통해 받고 이후 데이터 통신이 필요한 codesnippet을 시작한다.
*/
export default class ScriptEditorMainMediator extends puremvc.Mediator {
	public static NAME: string = "ScriptEditorMainMediator";
	private currentTarget: any = {type: "", id: ""};

	private editorChannel: MessageChannel = null;
	private workerChannel: MessageChannel = null;
	private tempHandler: any = null;



	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
    }

	private get proxy():ScriptEditorProxy{
		return <ScriptEditorProxy>this.facade.retrieveProxy(ScriptEditorProxy.NAME);
	}

	public getView(): ScriptEditorMainComponent {
		return this.viewComponent;

	}

	/**
	 * window.opener에서 channel저장
 	 * workerChannel edit main(ScriptEditorWorker)쪽에 전송하고 받아야할 메세지가 있는 경우 이 Channel을 사용
	 * editorChannel를 Script Editor쪽에 전송하고 받아야할 메세지가 있는 경우 이 Channel을 사용
	*/
	public onRegister() {
		this.workerChannel = <MessageChannel>window.opener.__scriptWorkerChannel__;
		this.editorChannel =  <MessageChannel>window.opener.__scriptEditorChannel__;

		let self = this;
		this.tempHandler =  this.rcvMessage.bind(this);
		this.editorChannel.port1.addEventListener("message", this.tempHandler);

		$(document).on("keydown", function(e){
			if( ( e.key=="s" || e.key == "ㄴ" || e.key=="S" ) && e.ctrlKey){
				e.preventDefault();
				e.stopPropagation();
				self.requestPageSave();
			}
		});

		/**이벤트 핸들러 코드가 변경되어 업데이트 이벤트 처리*/
		this.viewComponent.$on( ScriptEditorMainComponent.EVENT_UPDATE_FOCUS_TARGET_EVENTS, (data:any)=>{
                                    self.updateFocusTargetEvents(data);
                              })

                              /** 왼쪽 stage, common, page항목을 눌렀을 이벤트 처리*/
                              this.viewComponent.$on( ScriptEditorMainComponent.EVENT_SELECTED_LIST_ITEM, (type:string)=>{

                                    let scriptEditorProxy:ScriptEditorProxy = this.facade.retrieveProxy(ScriptEditorProxy.NAME) as ScriptEditorProxy;

                                    scriptEditorProxy.setGlobalState(type);
                                    if(type == "stage"){
                                          Vue.$message.warning("Deactived");
                                          return;
                }

                if(type=="common"){
                    Vue.$message.warning("Deactived");
			        return;
                }
			//page
                  self.sendMessage(ScriptEditorWorker.CMD_EDIT_CLEAR_SELECT_ALL);
		})


		this.requestSendServerUrl();
	}

	//Editor Main(ScriptEditorWorker)에 server url 요청(wemb.configManager에 바로 접근시 ie에서 심각한 속도 저하 발생으로..)
	requestSendServerUrl(){
		this.sendMessage( ScriptEditorWorker.CMD_GET_SERVER_URL );
	}

	/*
	 * Editor Main(ScriptEditorWorker)에 전달해야될 메세지가 있을 경우 사용
	 */
	sendMessage(command, value=null) {
		let data = {
			command: command,
			data: value || {}
		}
		let strData = JSON.stringify(data);
		this.workerChannel.port2.postMessage(strData);
	}

	/*
	 * Editor Main(ScriptEditorWorker)에서 전달 받은 메세지 처리
	 */
	rcvMessage(event){
		this.eventActionHandler(this.getJSON(event.data));
	}


	getJSON(strData):any{
		let data = JSON.parse(strData);
		return data;
	}

	/**
	 * EditorMain(ScriptEditorWorker)에서 보내오는 정보는 이쪽에서 받게 됨.
	 * server url을 받으면서 code snippets시작
	*/
	eventActionHandler( data ){
		let event = data.event;
		let value = data.data;

		switch (event) {
			case ScriptEditorWorker.EVENT_SET_SERVER_URL:
				window.__serverUrl__ = value;
				this.startCodeSnippets();
				//Vue.$WScriptService = new WScriptService();
				break;
			case ScriptEditorWorker.EVENT_CHANGE_FOCUS:
				this.changeFocusTarget(value.type, value.data);
				break;
		}
	}

	private startCodeSnippets(){
		this.viewComponent.isReady = true;
		this.sendNotification( ScriptEditorStatic.CMD_LOAD_GLOBAL_SNIPPETS );
		this.sendNotification( ScriptEditorStatic.CMD_LOAD_FAVORITE_SNIPPETS );
		this.sendNotification( ScriptEditorStatic.CMD_LOAD_CUSTOM_SNIPPETS );
	}


	changeFocusTarget(type, data){
		switch( type ){
			case "component":
			case "page":
				this.focus(type, data);
			break;
			case "group":
				this.blur();
			break;
		}

	}

	private focus(type, data){
		this.viewComponent.focus( type, data );
		this.updateCurrentTarget(type, data.id);
		this.proxy.isEditFocus = true;
	}

	/*Main Editor에서 2개 이상의 컴포넌트를 선택한 경우 박스처리*/
	private blur(){
		this.viewComponent.blur();
		this.updateCurrentTarget("", "");
		this.proxy.isEditFocus = false;

	}

	private updateCurrentTarget(type, id){
		id = id || "";
		this.currentTarget.type = type;
		this.currentTarget.id = id;
		this.viewComponent.updateCurrentTarget(this.currentTarget.type, this.currentTarget.id);
	}

	private updateFocusTargetEvents(data){
		switch( this.currentTarget.type ){
			case "component":
				this.sendMessage(ScriptEditorWorker.CMD_UPDATE_COMPONENT_EVENTS, data);
				//parent.saveComponentEventProperties(data, isPageSave);
			      break;
			case "page":
				this.sendMessage(ScriptEditorWorker.CMD_UPDATE_PAGE_EVENTS, data);
				//parent.saveCurrentPageEventProperties(data, isPageSave);
			      break;
			case "stage":
			      break;
			case "common":
			      break;
		}

	}


	private requestPageSave(){
		let data = this.viewComponent.getCurrentFocusTargetData();
		if( data ){
			this.updateFocusTargetEvents(data);
		}

		this.sendMessage( ScriptEditorWorker.CMD_SAVE_PAGE );
	}


	public listNotificationInterests(): string[] {
		return [
			ScriptEditorProxy.NOTI_INSERT_CODE
		]
	}


	public handleNotification(note: puremvc.Notification): void {
            if (note.name === ScriptEditorProxy.NOTI_INSERT_CODE) {
                  let code: string = note.body;
                  this.viewComponent.insertCode( code );
            }
	}
}
