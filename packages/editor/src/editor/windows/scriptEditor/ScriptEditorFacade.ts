import * as puremvc from "puremvc";
import ScriptEditorMainComponent from "./view/ScriptEditorMainComponent.vue";
import ScriptEditorStartUpCommand from "./controller/ScriptEditorStartUpCommand";

export default class ScriptEditorFacade extends puremvc.Facade {
    public static CMD_STARTUP: string = 'command/startup';
    public static NOTI_COMPONENT_FOCUS:string="notification/componentFocus";


    public static NAME:string = "ScriptEditor";


    constructor(key:string){
        super(key);
    }

    /** @override */
    initializeController() {
        ScriptEditorStartUpCommand
        super.initializeController();
        this.registerCommand(ScriptEditorFacade.CMD_STARTUP, ScriptEditorStartUpCommand);
    }

    /** @override */
    initializeModel() {
        super.initializeModel();
    }

    /** @override */
    initializeView() {
        super.initializeView();
    }

    /*

     */
    startup(app:ScriptEditorMainComponent) {
        this.sendNotification(ScriptEditorFacade.CMD_STARTUP, app);

    }

    static getInstance(multitonKey:string):ScriptEditorFacade {
        const instanceMap:any = puremvc.Facade.instanceMap;
        if (!instanceMap[multitonKey]) {
            instanceMap[multitonKey] = new ScriptEditorFacade(multitonKey);
        }
        return instanceMap[multitonKey] as ScriptEditorFacade;
    }
}
