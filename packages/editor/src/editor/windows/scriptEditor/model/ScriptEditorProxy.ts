import * as puremvc from "puremvc";
import codeSnippetsApi from "../../../../common/api/codeSnippetsApi";
import { dto } from "./dto/SnippetDTO";
import SnippetTreeItemDTO = dto.SnippetTreeItemDTO;
import SnippetFavoriteItemDTO = dto.SnippetFavoriteItemDTO;
import CustomSnippetItemDTO = dto.CustomSnippetItemDTO;
import { ObjectUtil } from "../../../../wemb/utils/Util";

export default class ScriptEditorProxy extends puremvc.Proxy {
    public static readonly NAME: string = "ScriptEditorProxy";

    public static readonly NOTI_LOAD_GLOBAL_SNIPPETS:string =       "notification/loadGlobalSnippets";
    public static readonly NOTI_LOAD_FAVORITE_SNIPPETS:string =     "notification/loadFavoriteSnippets";
    public static readonly NOTI_LOAD_CUSTOM_SNIPPETS:string =       "notification/loadCustomSnippets";

    public static readonly NOTI_UPDATE_CUSTOM_SNIPPETS:string =     "notification/updateCustomSnippets";
    public static readonly NOTI_UPDATE_FAVORITE_SNIPPETS:string =   "notification/updateFavoriteSnippets";
    public static readonly NOTI_INSERT_CODE:string =                "notification/insertCode";

    public static readonly NOTI_ADD_CUSTOM_SNIPPETS:string =        "notification/addCustomSnippets";
    public static readonly NOTI_MODIFY_CUSTOM_SNIPPETS:string =     "notification/modifyCustomSnippets";

    public static readonly NOTI_REMOVE_CUSTOM_SNIPPETS:string =     "notification/removeCustomSnippets";
    public static readonly NOTI_IMPORT_CUSTOM_SNIPPETS:string =     "notification/addImportSnippets";

    public static readonly NOTI_CHANGE_FOCUS_STATE: string = "notification/changeFocusState";

	private _customCodes:any;
	private _customTree:Array<SnippetTreeItemDTO> = [];
    private _favoriteList:Array<SnippetFavoriteItemDTO>=[];
	private _tree:Array<SnippetTreeItemDTO> = [];
	private _codes:any = {};

    private _isEditorFocus:boolean = false;


    constructor() {
        super(ScriptEditorProxy.NAME);
	}

	private _customRootTreeData:SnippetTreeItemDTO = {
		type: "root",
		id: "wv_root",
		text: "Custom Snippets",
		parent: "#"
	};

    getCustomSnippetsJSON():any{
        return {
            tree: this._customTree,
            codes: this._customCodes
        }
    }

    getGlobalSnippetsTreeData(){
        return this._tree;
    }

    set isEditFocus(bool: boolean){
        if(bool == this._isEditorFocus){return;}
        this._isEditorFocus=bool;
        this.sendNotification( ScriptEditorProxy.NOTI_CHANGE_FOCUS_STATE, bool );
    }

    get isEditFocus(){
        return this._isEditorFocus;
    }

    loadCustomSnippets(){
        let self = this;
        codeSnippetsApi.getUserCodeSnippetList().then((result) => {
            result = result || {};
            let data = result.data || {};
            if (!data.tree || !data.tree.length) {
                data.tree = [this._customRootTreeData]
            }
            this._customCodes = data.codes || {};
            this._customTree = data.tree;
            this.sendNotification( ScriptEditorProxy.NOTI_LOAD_CUSTOM_SNIPPETS,  this._customTree );
        })
    }
    /**
     *  load favorite snippets
     */
    loadFavoriteSnippets() {
        codeSnippetsApi.getFavoriteList().then((result) => {
            if (!result) {
                result = { data: [] };
            }

            this._favoriteList = result.data || [];
            this.sendNotification(ScriptEditorProxy.NOTI_LOAD_FAVORITE_SNIPPETS, this._favoriteList);
        })
    }

      /**
     *  update favorite snippets
     */
    updateFavoriteSnippets(list:Array<SnippetFavoriteItemDTO>) {
        codeSnippetsApi.setFavoriteList(list).then((result) => {
            console.log("updateFavoriteSnippets", typeof result);
            if(result){
                this._favoriteList = list;
                this.sendNotification(ScriptEditorProxy.NOTI_UPDATE_FAVORITE_SNIPPETS, this._favoriteList);
            }
        });
    }

    /**
     *  load basic snippets
     */
    loadGlobalSnippets() {
        codeSnippetsApi.getCodeSnippetList().then((result:any) => {
            if (result) {
                this._codes = result.codes;
                this._tree = result.tree;
				this.sendNotification(ScriptEditorProxy.NOTI_LOAD_GLOBAL_SNIPPETS, this._tree);
            }
            return;
        }, (error) => {
            console.log("error");
            return error;
        });
    }

    addFavoriteSnippet(item:SnippetFavoriteItemDTO){
        let findItem:SnippetFavoriteItemDTO = this._favoriteList.find(x => x.id == item.id);
        let list =  this._favoriteList.concat();
        if (!findItem) {
            list.unshift(item);
        }
        this.updateFavoriteSnippets(list);
    }

    removeFavoriteSnippet(id:string){
        let findItem = this._favoriteList.find(x => x.id == id);
        let list:Array<SnippetFavoriteItemDTO> = this._favoriteList.concat();
        if (findItem) {
            list.splice(this._favoriteList.indexOf(findItem), 1);
            this.updateFavoriteSnippets(list);
        }
    }

    insertCode(id:string){
        let code = this.getCodeById(id);
        this.sendNotification(ScriptEditorProxy.NOTI_INSERT_CODE, code);
    }

    copyToClipboard(id:string){
        let code:string = this.getCodeById(id);
        ObjectUtil.copyToClipboard(code);
    }

    getCodeById(id:string):string {
        let code:string = this._codes[id];
        if (!code) {
            code = this._customCodes[id];
        }
        return code || "";
    }

	mergeCustomSnippets(data:any) {
        if (data.tree && data.codes) {
            this._customCodes = this.__mergeCodes(this._customCodes, data.codes);
            this._customTree = this.__mergeTree(this._customTree, data.tree);

            //this.$bus.$emit("redrawCustomSnippets", this._customTree);
            this.updateCustomSnippets(this._customTree, ScriptEditorProxy.NOTI_IMPORT_CUSTOM_SNIPPETS);
        }
    }

	private __mergeCodes(data1:any, data2:any) {
        let data = $.extend(true, data1);
        for (let key in data2) {
            data[key] = data2[key];
        }

        return data;
    }

    private __mergeTree(data1:any, data2:any) {
        let ary = data1.concat();
        for (let i = 0, len = data2.length; i < len; i++) {
            let item = data2[i];
            let findItem = ary.find(x => x.id == item.id);
            if (findItem) {
                findItem.text = item.text;
                findItem.type = item.type;
                findItem.parent = item.parent;
            } else {
                ary.push(item);
            }
        }

        return ary;
    }

    addCustomSnippet(data:CustomSnippetItemDTO) {
        /// emit으로 tree update-> 다시 updateCustomSnippets요청/저장(tree정보와 함께 저장돼야하기 때문에..)
        data.id = ObjectUtil.generateID();
        this._customCodes[data.id] = data.code;
        this.sendNotification(ScriptEditorProxy.NOTI_ADD_CUSTOM_SNIPPETS, data);

        //this.$bus.$emit("addCustomSnippet", data);
    }

    modifyCustomSnippet(data:CustomSnippetItemDTO) {
        this._customCodes[data.id] = data.code;
        this.sendNotification(ScriptEditorProxy.NOTI_MODIFY_CUSTOM_SNIPPETS, data);
        //this.$bus.$emit("modifyCustomSnippet", data);
    }

	updateCustomSnippets(treeData:Array<SnippetTreeItemDTO> = null, noti=""):Promise<any> {
        let self = this;
        this._customCodes = this.getSyncCustomCodes(treeData);
        if(treeData != null){
            this._customTree = treeData.map(x => {
                return {
                    id: x.id,
                    text: x.text,
                    parent: x.parent,
                    type: x.type
                }
            });
        }

        return codeSnippetsApi.setUserCodeSnippetList({
            tree: this._customTree,
            codes: this._customCodes
        }).then((result) => {
            if( result ){
                //this.loadCustomSnippets();
                let syncFavoriteList = this.getSyncFavoriteList(treeData);
                if ( syncFavoriteList != null) {
                    //사용자 스니펫이 삭제/수정되면 즐겨찾기도 업데이트
                    this.updateFavoriteSnippets(syncFavoriteList);
                }
                if( noti ){
                    //옵션이 있을경우면 send..
                    this.sendNotification(noti, this._customTree);
                }
            }

            return result;
        });


    }

    getSyncFavoriteList(customTreeData:Array<SnippetTreeItemDTO>):Array<SnippetFavoriteItemDTO> {
        let tree:Array<SnippetTreeItemDTO> = this._tree.concat(customTreeData);
        let isSync:boolean = true;
        let newFoavoriteList:Array<SnippetFavoriteItemDTO> = [];
        for (let i = 0, len = this._favoriteList.length; i < len; i++) {
            let fItem:SnippetFavoriteItemDTO = this._favoriteList[i];
            let findItem:SnippetTreeItemDTO = tree.find(x=>x.id == fItem.id);

            if (findItem) {
                if (fItem.text != findItem.text) {
                    isSync = false;
                    fItem.text = findItem.text;
                }

                newFoavoriteList.push(fItem);
            } else {
                isSync = false;
            }
        }

        if( isSync ){
            return null;
        }else{
            return newFoavoriteList;
        }
    }


    private getSyncCustomCodes(tree:Array<SnippetTreeItemDTO>):any {
        let codes:any = this._customCodes;
        let newData = {};
        for (let i:number = 0, len:number = tree.length; i < len; i++) {
            let treeItem = tree[i];
            if (treeItem.type == "item") {
                let id:string = treeItem.id;
                let code = codes[id] || "";
                newData[id] = code;
            }
        }
        return newData;
    }


    /*
    2018.09.09 : ckkim
    page, common 상태 유무
     */
    private _globalState=GlobalState.PAGE;
    public setGlobalState(state){
          this._globalState = state;
    }

    public getGlobalState(){
          return this._globalState;
    }

}

export enum GlobalState {
      PAGE="page",
      COMMON="common"
}
