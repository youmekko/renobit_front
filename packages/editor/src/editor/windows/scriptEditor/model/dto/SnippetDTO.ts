

export namespace  dto {

	export class SnippetTreeItemDTO {
		public type:string = "";
		public id:string = "";
		public text:string = "";
		public parent:string = "";
	}

	export class SnippetFavoriteItemDTO {
		public id:string = "";
		public text:string = "";
	}

	export class CustomSnippetItemDTO{
		public id: string = "";
		public title: string = "";
		public code: string = ""
	}
}