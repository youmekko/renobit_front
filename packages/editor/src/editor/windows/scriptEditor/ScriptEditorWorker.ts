import Vue from "vue";
import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";
import ComponentListPanel from "../../view/panel/componentListPanel/ComponentListPanel";
import SelectProxy from "../../model/SelectProxy";

import { WORK_LAYERS } from "../../../wemb/wv/layer/Layer";
import EditorStatic from "../../controller/editor/EditorStatic";
import WVComponent, { GROUP_NAME_LIST } from "../../../wemb/core/component/WVComponent";
import {VirtualPropertyManager} from "../../model/VirtualPropertyManager";
import ScriptEditorMainMediator from "./view/ScriptEditorMainMediator";

/**
 * PrepViewCommand에서 생성된다. - Editor에서 사용 유무를 떠나 무조건 생성하여 참조
 * workerChannel과 editorChannel를 생성하여 window에 참조할 수 있게 저장한다. 
 * - scriptEditor에서 window.opener로 접근하여 이 channel을 참조한다.
 * workerChannel Worker(this)쪽에 전송하고 받아야할 메세지가 있는 경우 이 Channel을 사용
 * editorChannel를 Script Editor쪽에 전송하고 받아야할 메세지가 있는 경우 이 Channel을 사용
 * 
 * 참고 : 직접 opener를 통해 부모의 객체에 접근할 경우 ie에서 상당히 느려지는 현상이 있어 이를 우회하기 위해 channel을 사용한다. 
 * 
 * 첫 시작 순서 :
 * 1. 에디터 시작시 PrepViewCommand에서 ScriptEditorWorker 생성
 * 2. ScriptEditor가 열리고 mounted실행시 ScriptEditorWorker에 serverUrl달라고 요청 : ScriptEditorWorker.CMD_GET_SERVER_URL
 * 3. ScriptEditorWorker에서는 serverUrl을 보내고, 현재 포커스 타겟 정보를 전달하면서 시작 : loadedScriptEditor()
 */
export default class ScriptEditorWorker extends puremvc.Mediator {
      public static NAME: string = "ScriptEditorWorker";

      /*command*/
      public static CMD_GET_SERVER_URL:string = "command/getServerUrl";
      public static CMD_UPDATE_COMPONENT_EVENTS:string = "command/updateCompnentEvents";
      public static CMD_UPDATE_PAGE_EVENTS:string = "command/updatePageEvents";
      public static CMD_SAVE_PAGE:string = "command/savePage";
      public static CMD_EDIT_CLEAR_SELECT_ALL = "command/editClearSelectAll";

      /*event*/
      public static EVENT_SET_SERVER_URL:string = "event/setServerUrl";
      public static EVENT_CHANGE_FOCUS:string = "event/changeFocus";

      private editorChannel:any = null;
      private selectTimer:number;
      private workerChannel:any = null;
      private tempHandler: Function = null;

      viewComponent:ComponentListPanel = null;

      constructor(name: string, viewComponent: Vue) {
            super(name, viewComponent);
            this.viewComponent = <ComponentListPanel>viewComponent;
            this.init();
      }

      private get proxy():SelectProxy{
            return <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
      }

      private get editorProxy():EditorProxy{
            return <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
      }


      public getView(): ComponentListPanel {
            return this.viewComponent;
      }

      /**
       * 첫 시작시 채널을 생성하고, 채널 이벤트를 바인딩하면서 시작
       */
      init() {
            this.creatworkerChannel();
            this.bindEvent();
      }

      unbindEvent() {

      }

      creatworkerChannel() {
             /** 
             * Worker(this)쪽에 전송하고 받아야할 메세지가 있는 경우 이 Channel을 사용
            */
            this.workerChannel = new MessageChannel();
            this.workerChannel.port1.start();
            this.workerChannel.port2.start();
            window.__scriptWorkerChannel__ = this.workerChannel;

            /** 
             * Script Editor쪽에 전송하고 받아야할 메세지가 있는 경우 이 Channel을 사용
            */
            this.editorChannel = new MessageChannel();
            this.editorChannel.port1.start();
            this.editorChannel.port2.start();
            window.__scriptEditorChannel__ = this.editorChannel;
      }

      destroy() {
            /*Editor에서 무조건 생성 후 참조 사용, 삭제하지 않음*/
      }

      channelEventHandler(event) {
            this.executeCommand(this.getJSON(event.data));
      }

      bindEvent() {
            //unbind처리를 위해 tempHandler에 저장하였으나.. ScriptEditorWorker를 소멸시키는 코드가 삭제되면서 사용하지 않게 됨
            this.tempHandler = this.channelEventHandler.bind(this);
            this.workerChannel.port1.addEventListener("message", this.tempHandler);
      }

      //Editor에서 데이터를 받을 준비가 되었을 경우 호출 
      // server url정보와 현재 target정보를 전송함 
      loadedScriptEditor() {
            this.sendMessage(ScriptEditorWorker.EVENT_SET_SERVER_URL, window.wemb.configManager.serverUrl);
            //셋팅후 현재 정보 전송
            this.sendTargetData();
      }


      /*
      에디터에서 선택 정보가 변경되는 실행
      - 배치 컴포넌트가 변경되거나
      - 배경을 선택했을때 실행.
       */
      sendTargetData() {
            clearTimeout(this.selectTimer);
            this.selectTimer = setTimeout(() => {
                  let targetData:any;
                  let currentPropertyManager:VirtualPropertyManager = this.proxy.currentPropertyManager;

                  if(currentPropertyManager.layerName == WORK_LAYERS.BG_LAYER){
                        targetData = { type: "page", data: this.getInstanceEventInfo( currentPropertyManager,currentPropertyManager.layerName ) };

                  }else if( currentPropertyManager.getInstanceLength() == 1 ){
                        /*
                        2018.06.19
                        컴포넌트가 선택되어 있는 경우 layerName을
                        현재 활성화 되어 있는 레이어 사용.
                         */
                        let layerName:string =   this.editorProxy.activeLayerInfo.name;
                        targetData = { type: "component", data: this.getInstanceEventInfo( currentPropertyManager,layerName ) }
                  }else{
                        targetData = {type: "group"};
                  }
                  this.sendMessage(ScriptEditorWorker.EVENT_CHANGE_FOCUS, targetData);
            }, 10);
      }

      /*
      선택한 인스턴스 이벤트 목록 구하기
       */
      private getInstanceEventInfo( propertyManager:VirtualPropertyManager, currentLayerName:string ):any{
            let properties = propertyManager.getEditableProperties();

            let eventsInfo:Array<any> = propertyManager.getEventDescription();
            let editableEvents = propertyManager.getEditableProperties().events;
            let sendEvents: any = {};

            for( let i=0, len=eventsInfo.length; i<len; i++ ){
                  let info = eventsInfo[i];
                  try {
                        sendEvents[info.name] = editableEvents[info.name] || "";
                  }catch(error){
                        console.log("getInstanceEventInfo error ", error);
                  }
            }


            let data = {
                  id: properties.primary.id,
                  layer: currentLayerName,
                  name: properties.primary.name,
                  eventsInfo: eventsInfo,
                  eventProps: sendEvents,
                  propertyInfo:propertyManager.getPropertyDescription(),
                  methodInfo:propertyManager.getMethodDescription()
            }
            return data;
      }

      /**
       * 이벤트 정보 페이지/컴포넌트에 업데이트하기
      */
      updateCurrentTargetEventProperties = function(data) {
            let instanceList: Array<WVComponent>;
            switch (data.layer) {
                  case WORK_LAYERS.TWO_LAYER:
                        instanceList = this.editorProxy.twoLayerInstanceList;
                        break;
                  case WORK_LAYERS.THREE_LAYER:
                        instanceList = this.editorProxy.threeLayerInstanceList;
                        break;
                  case WORK_LAYERS.MASTER_LAYER:
                        instanceList = this.editorProxy.masterLayerInstanceList;
                        break;
                  case WORK_LAYERS.BG_LAYER:
                        ///page
                        instanceList = this.editorProxy.bgLayerInstanceList;
                        break;

            }

            if (!instanceList) { return false; }
            let findItem:WVComponent = instanceList.find(x => x.id == data.id);
            if (findItem) {
                  findItem.setGroupProperties( GROUP_NAME_LIST.EVENTS, data.eventProps );
                  return true;
            } else {
                  return false;
            }
      };


      savePage() {
            this.facade.sendNotification(EditorStatic.CMD_SAVE_PAGE);
      };

      getJSON(strData) {
            let data = JSON.parse(strData);
            return data;
      }

      /**
       * Script Editor에서 전송한 command 처리 
      */
      executeCommand(data) {
            let command = data.command;
            switch (command) {
                  case ScriptEditorWorker.CMD_GET_SERVER_URL:
                        //ScriptEditor에서 ServerUrl을 요청 
                        this.loadedScriptEditor();
                        break;
                  case ScriptEditorWorker.CMD_EDIT_CLEAR_SELECT_ALL:
                        //Script Editor에서 page를 선택함
                        this.sendNotification( EditorStatic.CMD_CLEAR_ALL_SELECTED );
                        break;
                  case ScriptEditorWorker.CMD_UPDATE_COMPONENT_EVENTS:
                  case ScriptEditorWorker.CMD_UPDATE_PAGE_EVENTS:
                        //Script Editor에서 이벤트 핸들러에 내용을 변경함
                        this.updateCurrentTargetEventProperties(data.data);
                        break;
                  case ScriptEditorWorker.CMD_SAVE_PAGE:
                        //Script Editor에서 페이지 저장을 요청함
                        this.savePage();
                        break;
            }
      }

      /**
       * 메세지 전송을 간편하게 하기 위한 메소드 
      */
      sendMessage(event, value) {
            let data = {
                  event: event,
                  data: value
            }

            let strData = JSON.stringify(data);
            this.editorChannel.port2.postMessage(strData);
      }

      public onRegister() {

      }

      public listNotificationInterests(): string[] {
            return [
                  SelectProxy.NOTI_UPDATE_SELECTED_INFO,
                  EditorProxy.NOTI_CLOSED_PAGE,
                  EditorProxy.NOTI_OPEN_PAGE
            ]
      }

      public handleNotification(note: puremvc.Notification): void {
            switch(note.name){
                  case SelectProxy.NOTI_UPDATE_SELECTED_INFO:
                        // 선택 정보가 변경되면 실행.
                        this.sendTargetData();
                        break;
            }
      }
}

