import * as puremvc from "puremvc";
import MultilingualEditorProxy from "../model/MultilingualEditorProxy";

export class UpdateLocaleItemCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let proxy: MultilingualEditorProxy = <MultilingualEditorProxy>this.facade.retrieveProxy(MultilingualEditorProxy.NAME);
		let data:any = note.getBody();
		proxy.updateLocaleItem(data.key, data.value);		
    }
}
