import * as puremvc from "puremvc";
import MultilingualEditorProxy from "../model/MultilingualEditorProxy";

export class PrepMultilingualEditorModelCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		this.facade.registerProxy( new MultilingualEditorProxy() );
	}
}