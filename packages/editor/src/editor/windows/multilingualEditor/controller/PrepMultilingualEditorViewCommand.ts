import * as puremvc from "puremvc";
import MultilingualEditorMainMediator from "../view/MultilingualEditorMainMediator";
import MultilingualFormMediator from "../view/MultilingualFormMediator";

export class PrepMultilingualEditorViewCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {		
		let multilingualEditorMainMediator: puremvc.IMediator = new MultilingualEditorMainMediator(MultilingualEditorMainMediator.NAME, note.getBody());
		this.facade.registerMediator(multilingualEditorMainMediator);		
	}
}