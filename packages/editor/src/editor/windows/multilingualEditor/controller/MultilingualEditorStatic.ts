export default class MultilingualEditorStatic {
	public static CMD_ADD_LOCALE_ITEM: string = "command/addLocaleItem";
	public static CMD_REMOVE_LOCALE_ITEMS: string = "command/removeLocaleItems";
      public static CMD_REMOVE_ALL_LOCALE: string = "command/removeAllLocale";
	public static CMD_UPDATE_LOCALE_ITEM: string = "command/updateLocaleItem";
	
}
