import * as puremvc from "puremvc";
import MultilingualEditorProxy from "../model/MultilingualEditorProxy";

export class RemoveLocaleItemsCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let proxy: MultilingualEditorProxy = <MultilingualEditorProxy>this.facade.retrieveProxy(MultilingualEditorProxy.NAME);
		let keys:Array<string> = note.getBody();
		proxy.removeLocaleItems(keys);		
    }
}
