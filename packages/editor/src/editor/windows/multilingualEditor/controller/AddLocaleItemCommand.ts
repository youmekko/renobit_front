import * as puremvc from "puremvc";
import MultilingualEditorProxy from "../model/MultilingualEditorProxy";

export class AddLocaleItemCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let proxy: MultilingualEditorProxy = <MultilingualEditorProxy>this.facade.retrieveProxy(MultilingualEditorProxy.NAME);
		let data:any = note.getBody();
		proxy.addLocaleItem(data.key, data.value);		
    }
}
