import * as puremvc from "puremvc";
import MultilingualEditorStatic from "./MultilingualEditorStatic";
import { AddLocaleItemCommand } from "./AddLocaleItemCommand";
import { UpdateLocaleItemCommand } from "./UpdateLocaleItemCommand";
import { RemoveLocaleItemsCommand } from "./RemoveLocaleItemsCommand";
import { RemoveAllLocaleCommand } from "./RemoveAllLocaleCommand";

export class MultilingualEditorCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		
		this.facade.registerCommand(MultilingualEditorStatic.CMD_ADD_LOCALE_ITEM, AddLocaleItemCommand);
		this.facade.registerCommand(MultilingualEditorStatic.CMD_REMOVE_LOCALE_ITEMS, RemoveLocaleItemsCommand);
		this.facade.registerCommand(MultilingualEditorStatic.CMD_UPDATE_LOCALE_ITEM, UpdateLocaleItemCommand);
            this.facade.registerCommand(MultilingualEditorStatic.CMD_REMOVE_ALL_LOCALE, RemoveAllLocaleCommand);
	}
}
