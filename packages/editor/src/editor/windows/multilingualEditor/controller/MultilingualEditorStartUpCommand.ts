import * as puremvc from "puremvc";
import { PrepMultilingualEditorModelCommand } from "./PrepMultilingualEditorModelCommand";
import { PrepMultilingualEditorViewCommand } from "./PrepMultilingualEditorViewCommand";
import { MultilingualEditorCommand } from "./MultilingualEditorCommand";


export default class MultilingualEditorStartUpCommand extends puremvc.MacroCommand {
	initializeMacroCommand() {
		console.log("step 02, StartUpCommand 실행 view, model, command 등록");
		this.addSubCommand(PrepMultilingualEditorModelCommand);
		this.addSubCommand(PrepMultilingualEditorViewCommand);
		this.addSubCommand(MultilingualEditorCommand);

	}
}
