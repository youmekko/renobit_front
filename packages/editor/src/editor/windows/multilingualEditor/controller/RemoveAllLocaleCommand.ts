import * as puremvc from "puremvc";
import MultilingualEditorProxy from "../model/MultilingualEditorProxy";

export class RemoveAllLocaleCommand extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            let proxy: MultilingualEditorProxy = <MultilingualEditorProxy>this.facade.retrieveProxy(MultilingualEditorProxy.NAME);
            proxy.removeAllLocale();
      }
}
