import * as puremvc from "puremvc";
import Vue from "vue";
import multilingualApi from "../../../../common/api/multilingualApi";
import { ObjectUtil } from "../../../../wemb/utils/Util";
import FileManager from "../../../../wemb/wv/managers/FileManager";
import ConfigManager from "../../../../wemb/wv/managers/ConfigManager";

/**
 * 다국어 관리자
 * Config.json에서 설정한 Locale정보를 가져와 이를 key로 다국어 정보를 등록할 수 있게 제공
 * 클립보드 복사 기능에서는 key정보 앞에 "__locale__."를 접두어로 붙여서 클립보드에 복사함
*/
export default class MultilingualEditorProxy extends puremvc.Proxy {
    public static readonly NAME: string = "MultilingualEditorProxy";
    public static readonly NOTI_LOAD_LOCALE_DATA:string =  "notification/loadLocaleData";
    public static readonly NOTI_ADDED_LOCALE_ITEM:string =  "notification/addedLocaleItem";
    public static readonly NOTI_REMOVED_LOCALE_ITEMS:string =  "notification/removedLocaleItems";
    public static readonly NOTI_REMOVED_ALL_LOCALE:string =  "notification/removeAllLocale";
    public static readonly NOTI_UPDATE_LOCALE_ITEM:string =  "notification/updateLocaleItem";

    private localeItemsJson:any={};
    private localeItems:Array<any> = [];
    private _locales:Array<any> = [];//["ko-KR", "en-US", "zh-CN","zh-HK"];

    constructor() {
        super(MultilingualEditorProxy.NAME);
        this.setLocales();
	}


    private setLocales(){
        let languages = ConfigManager.getInstance().languages || [];
        this._locales = languages.map(x=>x.locale);
    }

    getLocaleItemsJson(){
        return this.localeItemsJson;
    }

    get locales(){
        return this._locales;
    }

    /**
     *
     */
    public loadLocaleJson() {
        //this.updateLocaleJson({});
        //return;
        multilingualApi.getMultilingualData().then((result) => {
            let json = result || {};
            this.updateLocaleItems(json);
        })
    }


    updateLocaleJson(json:any):Promise<any>{
       return multilingualApi.setMultilingualData(json).then(x=>{
            this.localeItemsJson = json;
            this.syncLocaleData();
        });
    }

    private updateLocaleItems(json){
        this.localeItemsJson = json;
        this.localeItems = this.convertToLocaleData(this.localeItemsJson);
        this.sendNotification(MultilingualEditorProxy.NOTI_LOAD_LOCALE_DATA, this.localeItems);
    }

    /*목록 업데이트시 통신 결과와 로컬 데이터 동기화 - 동시 편집일 경우 결과를 반영하기 위함*/
    private syncLocaleData(){
        this.localeItems = this.localeItems.filter((x)=>{
            if(this.localeItemsJson[x.key]){ return true}
        });

        for(let key in this.localeItemsJson){
            let itemValue = this.localeItemsJson[key];
            let findItem = this.localeItems.find(x=>x.key == key);
            if(findItem){
                $.extend(true, findItem, itemValue);
            }else{
                let data = JSON.parse( JSON.stringify(itemValue));
                data.key = key;
                data.editMode = false;
                this.localeItems.unshift(data);
            }
        }
    }

    private convertToLocaleData(jsonData:any):Array<any>{
        let ary = [];
        for(let key in jsonData){
            let data = JSON.parse( JSON.stringify(jsonData[key]));
            data.key = key;
            data.editMode = false;
            ary.push(data);
        }

        return ary;
    }

    public addLocaleItem(key:string, value:any){
        if( this.localeItemsJson[key] ){
            alert("이미 등록된 locale 정보가 있습니다.");
            return;
        }

        let tempJson = this.getCopyLocaleJson();
        tempJson[key] = value;
        this.updateLocaleJson(tempJson).then(()=>{
            this.sendNotification(MultilingualEditorProxy.NOTI_ADDED_LOCALE_ITEM, {key: key, items: this.localeItems});
        });
    }

    public removeLocaleItems(keys:Array<string>){
        let tempJson = this.getCopyLocaleJson();
        for(let index in keys ){
            let key = keys[index];
            delete tempJson[key];
        }

        this.updateLocaleJson(tempJson).then(()=>{
            this.sendNotification(MultilingualEditorProxy.NOTI_REMOVED_LOCALE_ITEMS, {keys: keys, items: this.localeItems});
        });
    }

    public removeAllLocale(){
        this.updateLocaleJson({}).then(()=>{
            this.sendNotification(MultilingualEditorProxy.NOTI_REMOVED_ALL_LOCALE, {keys: [], items: []});
        });
    }

    public updateLocaleItem(key:string, value:any){
        let tempJson = this.getCopyLocaleJson();
        if(tempJson[key]){
            tempJson[key] = value;
        }else{
            alert("조회된 Locale 정보가 없습니다.")
            return;
        }

        this.updateLocaleJson(tempJson).then(()=>{
            this.sendNotification(MultilingualEditorProxy.NOTI_UPDATE_LOCALE_ITEM, {key: key, items: this.localeItems});
        });
    }


    getCopyLocaleJson(){
        return JSON.parse( JSON.stringify(this.localeItemsJson));
    }

    public mergeLocaleItems(data){
        let currentItemsJson = this.getCopyLocaleJson();

        let isConfirmOverride:boolean = null;
        for(let key in data){
            if( currentItemsJson[key] ){
                if(confirm("동일한 ID의 데이터셋이 있습니다. 덮어 쓰시겠습니까?")){
                    isConfirmOverride = true;
                }else{
                    isConfirmOverride = false;
                }
                break;
            }
        }

        let tempJson;
        if( isConfirmOverride ){
            tempJson = $.extend(true, currentItemsJson, data);
        }else{
            tempJson = $.extend(true, data, currentItemsJson);
        }

        //temp
        this.updateLocaleJson(tempJson).then(()=>{
            this.updateLocaleItems(tempJson);
        });
	}

}
