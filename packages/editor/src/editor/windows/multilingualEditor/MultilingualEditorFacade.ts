import * as puremvc from "puremvc";
import MultilingualEditorMainComponent from "./view/MultilingualEditorMainComponent.vue";
import MultilingualEditorStartUpCommand from "./controller/MultilingualEditorStartUpCommand";

/**
 * 컨텐츠 영역 다국어를 처리하기 위해 다국어 데이터를 등록하는 에디터
 * 작성된 내용은 json파일로 저장됨 이중화에서 문제가 되어 DB로 저장해달라는 요청이 있었음
 * 
 * Config.json에서 설정한 Locale정보를 가져와 이를 key로 다국어 정보를 등록할 수 있게 제공
 * 클립보드 복사 기능에서는 key정보 앞에 "__locale__."를 접두어로 붙여서 클립보드에 복사함
 * 클립보드에서 복사한 텍스트를 다국어를 지원하는 컴포넌트 속성에 대입시 다국어 정보로 컨버팅하여 노출함(사용자 매뉴얼 1.2.0 참조)
*/
export default class MultilingualEditorFacade extends puremvc.Facade {
    public static CMD_STARTUP: string = 'command/startup';
    public static NAME:string = "MultilingualEditor";
    constructor(key:string){
        super(key);
    }

    /** @override */
    initializeController() {
        super.initializeController();
        this.registerCommand(MultilingualEditorFacade.CMD_STARTUP, MultilingualEditorStartUpCommand);
    }

    /** @override */
    initializeModel() {
        super.initializeModel();
    }

    /** @override */
    initializeView() {
        super.initializeView();
    }

    /*

     */
    startup(app:MultilingualEditorMainComponent) {
        this.sendNotification(MultilingualEditorFacade.CMD_STARTUP, app);
       

    }

    static getInstance(multitonKey:string):MultilingualEditorFacade {
        const instanceMap:any = puremvc.Facade.instanceMap;
        if (!instanceMap[multitonKey]) {
            instanceMap[multitonKey] = new MultilingualEditorFacade(multitonKey);
        }
        return instanceMap[multitonKey] as MultilingualEditorFacade;
    }
}
