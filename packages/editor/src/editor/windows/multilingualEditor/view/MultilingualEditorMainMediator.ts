import * as puremvc from "puremvc";
import MultilingualEditorMainComponent from "./MultilingualEditorMainComponent.vue";
import Vue from "vue";
import { POPUP } from "../../../../wemb/wv/managers/WindowsManager";
import MultilingualEditorProxy from "../model/MultilingualEditorProxy";
import MultilingualEditorStatic from "../controller/MultilingualEditorStatic";
import FileManager from "../../../../wemb/wv/managers/FileManager";
import { ObjectUtil } from "../../../../wemb/utils/Util";


export default class MultilingualEditorMainMediator extends puremvc.Mediator {
	public static PREFIX: string = "__locale__.";

	public static NAME: string = "MultilingualEditorMainMediator";
    private view: MultilingualEditorMainComponent = null;
	private windowName: string = POPUP.SCRIPT_EDITOR.NAME;
	private currentTarget: any = {type: "", id: ""};

	private editorChannel: MessageChannel = null;
	private workerChannel: MessageChannel = null;
	private tempHandler: any = null
	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.viewComponent = <MultilingualEditorMainComponent>viewComponent;

    }

	private get proxy():MultilingualEditorProxy{
		return <MultilingualEditorProxy>this.facade.retrieveProxy(MultilingualEditorProxy.NAME);
	}

	public getView(): MultilingualEditorMainComponent {
		return this.viewComponent;
		
	}

	public onRegister() {
		this.proxy.loadLocaleJson();
		this.viewComponent.init(this.proxy.locales);
		
		let self = this;
       
		this.viewComponent.$on( MultilingualEditorMainComponent.EVENT_ADD_LOCALE_ITEM, (item:any)=>{
			this.sendNotification( MultilingualEditorStatic.CMD_ADD_LOCALE_ITEM, item );
		})

		this.viewComponent.$on( MultilingualEditorMainComponent.EVENT_UPDATE_LOCALE_ITEM, (item:any)=>{
			this.sendNotification( MultilingualEditorStatic.CMD_UPDATE_LOCALE_ITEM, item );
		})

		this.viewComponent.$on( MultilingualEditorMainComponent.EVENT_REMOVE_LOCALE_ITEMS, (keys:any)=>{
			if( confirm("선택한 파일을 삭제하시겠습니까?") ){
				this.sendNotification( MultilingualEditorStatic.CMD_REMOVE_LOCALE_ITEMS, keys );
			}
		})

            this.viewComponent.$on( MultilingualEditorMainComponent.EVENT_REMOVE_ALL_LOCALE, ()=>{
                  if( confirm("모든 Locale을 삭제하시겠습니까?") ) {
                        this.sendNotification(MultilingualEditorStatic.CMD_REMOVE_ALL_LOCALE)
                  }
            })

		this.viewComponent.$on( MultilingualEditorMainComponent.EVENT_IMPORT_FILE, (file)=>{
			this.importLocaleItemsAsFile(file);
		})

		this.viewComponent.$on( MultilingualEditorMainComponent.EVENT_EXPORT_FILE, ()=>{
			this.exportLocaleItemsAsFile();
		})

		this.viewComponent.$on( MultilingualEditorMainComponent.EVENT_COPY_CLIPBOARD, (key:string)=>{
			this.copyPrefixKey(key);
		})

	}

	copyPrefixKey(key:string){
        let str:string = MultilingualEditorMainMediator.PREFIX + key;
        ObjectUtil.copyToClipboard(str);
		Vue.$notify({
          message: 'copied',
          type: 'success'
        });
    }

	importLocaleItemsAsFile(file:any){
		let self = this;
		var reader = new FileReader();

		if (file.name.search(/.json$/) === -1) {
			Vue.$message.error(Vue.$i18n.messages.wv.common.errorFormat);
		} else {
			reader.onloadend = function(evt:any) {
				try {
					let data = JSON.parse(evt.target.result).data;
					if(data === undefined) throw new Error();

					self.proxy.mergeLocaleItems( data );
					Vue.$message(Vue.$i18n.messages.wv.common.file + Vue.$i18n.messages.wv.common.successImport);

				} catch (error) {
					Vue.$message.error(Vue.$i18n.messages.wv.common.failImport);
				}
			};

			reader.readAsText(file);
		}
	}

	exportLocaleItemsAsFile(){		
		let jsonStr = JSON.stringify({data:  this.proxy.getLocaleItemsJson() });
		let fileManager = new FileManager();
		fileManager.saveTextFile("locales.json", jsonStr);
		fileManager.onDestroy();
	}

	public listNotificationInterests(): string[] {
		return [			
			MultilingualEditorProxy.NOTI_LOAD_LOCALE_DATA,
			MultilingualEditorProxy.NOTI_ADDED_LOCALE_ITEM,
			MultilingualEditorProxy.NOTI_REMOVED_LOCALE_ITEMS,
			MultilingualEditorProxy.NOTI_UPDATE_LOCALE_ITEM,
                  MultilingualEditorProxy.NOTI_REMOVED_ALL_LOCALE,
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		let data:any = note.body;
		switch(note.name){
			case MultilingualEditorProxy.NOTI_LOAD_LOCALE_DATA:
				this.viewComponent.setLocaleItems(data);
			break;  
			case MultilingualEditorProxy.NOTI_UPDATE_LOCALE_ITEM:
				let findItem = data.items.find(x=>x.key == data.key);
				if(findItem){
					findItem.editMode = false;
				}
				this.viewComponent.setLocaleItems(data.items);
                  break;
			case MultilingualEditorProxy.NOTI_REMOVED_LOCALE_ITEMS:
				this.viewComponent.setLocaleItems(data.items);
			break;
                  case  MultilingualEditorProxy.NOTI_REMOVED_ALL_LOCALE:
                        this.viewComponent.setLocaleItems(data.items);
                  break;
			case MultilingualEditorProxy.NOTI_ADDED_LOCALE_ITEM:
				this.viewComponent.resetFormField();
				this.viewComponent.setLocaleItems(data.items);
			break;
		}
	}
}
