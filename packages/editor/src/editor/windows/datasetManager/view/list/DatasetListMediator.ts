import * as puremvc from "puremvc";
import DatasetListComponent from "./DatasetListComponent.vue";
import Vue from "vue";
import DatasetStatic from "../../controller/DatasetStatic";
import EditorStatic from "../../../../../editor/controller/editor/EditorStatic";
import DatasetProxy from "../../model/DatasetProxy";
import FileManager from "../../../../../wemb/wv/managers/FileManager";
import {dto} from "../../model/dto/DatasetDTO";
import DatasetItemDTO = dto.DatasetItemDTO;
import DataSourceDTO = dto.DataSourceDTO;


export default class DatasetListMediator extends puremvc.Mediator {
      public static NAME: string = "DatasetListMediator";

      private view:DatasetListComponent  = null;
      private currentSearchStr: string = '';
      constructor(name: string, viewComponent: Vue) {
            super(name, viewComponent);
            this.view = <DatasetListComponent>viewComponent;
      }

      private get proxy():DatasetProxy{
            return <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
      }

      /**
       * 파일 가져오기, merget처리는 DatasetProxy에서 처리
       */
      importDatasetListAsFile(file:any){
            let self = this;
            var locale_msg = Vue.$i18n.messages.wv;
            var reader = new FileReader();

            if (file.name.search(/.json$/) === -1) {
                  Vue.$message.error(locale_msg.common.errorFormat);
            } else {
                  reader.onloadend = function(evt:any) {
                        //if (evt.target.readyState == state) {
                        try {
                              let result = JSON.parse(evt.target.result);
                              self.view.showImportPopup(result);
                              // self.proxy.mergeDatasetList( data );
                              // Vue.$message(locale_msg.common.file + locale_msg.common.successImport);
                              //console.log(data);
                        } catch (error) {
                              Vue.$message.error( locale_msg.common.failImport );
                        }
                        //}
                  };

                  reader.readAsText(file);
            }

      }

      saveDatasetList({ datasetList, dataSourceList }) {
            var locale_msg = Vue.$i18n.messages.wv;
            let datasourceListInMemory = this.proxy.getDataSourceList();
            datasetList.forEach(dataset => {
                  let datasourceId = dataset.datasource;
                  if (datasourceId &&
                        !datasourceListInMemory.some(item => item.id === datasourceId)) {
                        let dataSourceInfo = dataSourceList.find(item => item.id === datasourceId);

                        if (dataSourceInfo) {
                              dataSourceInfo = JSON.parse(JSON.stringify(dataSourceInfo));
                              dataSourceInfo.user_id = encypt(dataSourceInfo.user_id);
                              dataSourceInfo.url = encypt(dataSourceInfo.url);
                              this.proxy.addDataSource(dataSourceInfo);
                        }
                  }
            });
            this.proxy.mergeDatasetList( datasetList );
            Vue.$message(locale_msg.common.file + locale_msg.common.successImport);
            console.log(datasetList);
            this.view.closeImportPopup();
      }


      /**
       * FileManager를 통해 현재 데이터셋 json파일로 내보내기
       */
      exportDatasetListAsFile(selectedDatasetList){
            let datasetList:Array<DatasetItemDTO> = this.proxy.getDatasetList().filter(dataset => {
                  return selectedDatasetList.includes(dataset.dataset_id);
            });
            let datasourceList = this.proxy.getDataSourceList();
            let datasourceInfoList:Array<DataSourceDTO> = datasetList.reduce((array:Array<DataSourceDTO>, item:DatasetItemDTO) => {
                  let datasourceTemp;
                  if (item.datasource &&
                        !array.some(temp => temp.id === item.datasource) &&
                        (datasourceTemp = datasourceList.find(datasource => datasource.id === item.datasource))) {
                        array.push(datasourceTemp)
                  }

                  return array;
            }, []);
            let jsonStr = JSON.stringify({data: datasetList, datasource: datasourceInfoList });
            let fileManager = new FileManager();
            fileManager.saveTextFile("datasetList.json", jsonStr);
            fileManager.onDestroy();
      }

      public getView(): DatasetListComponent {
            return this.viewComponent;
      }

      public onRegister() {
            this.view.$on(DatasetListComponent.CHANGE_SEARCH_STR, (str:string)=>{
                  this.currentSearchStr = str;
                  this.setSearchResult(str);
            })

            this.view.$on(DatasetListComponent.CHANGE_EXPORT_SEARCH_STR, (str:string)=>{
                  this.currentSearchStr = str;
                  this.setExportSearchResult(str);
            })

            this.view.$on(DatasetListComponent.DELETE_DATASET, (datasetId:string)=>{
                  this.sendNotification(DatasetStatic.CMD_REMOVE_DATASET, datasetId);
            })

            //데이터셋 복사
            this.view.$on(DatasetListComponent.DUPLICATION_DATASET, (datasetId:string)=>{
                  this.sendNotification(DatasetStatic.CMD_DUPLICATE_DATASET, datasetId);
            })


            //데이터셋 수정
            this.view.$on(DatasetListComponent.MODIFY_DATASET, (datasetId:string)=>{
                  this.proxy.setModifyDatasetById(datasetId);
            })

            //파일 내보내기
            this.view.$on(DatasetListComponent.EXPORT_DATASET_LIST, (datasetList)=>{
                  this.exportDatasetListAsFile(datasetList);
            })

            //파일 가져오기
            this.view.$on(DatasetListComponent.IMPORT_DATASET_LIST, (file)=>{
                  this.importDatasetListAsFile(file);
            })

            //파일 저장하기
            this.view.$on(DatasetListComponent.SAVE_DATASET_LIST,(datasetInfo) => {
                  this.saveDatasetList(datasetInfo);
            });
            //매뉴얼 이동
            this.view.$on(DatasetListComponent.EVENT_GOTO_MANUAL, (path:string) => {
                  this.sendNotification(EditorStatic.CMD_SHOW_MANUAL, path)
            })
      }

      private setSearchResult(str){
            let searchList = this.proxy.getDatasetSearchList(str);
            this.view.setSearchResult(searchList);
      }

      private setExportSearchResult(str){
            let searchList = this.proxy.getDatasetSearchList(str);
            this.view.setExportSearchResult(searchList);
      }

      public listNotificationInterests(): string[] {
            return [
                  DatasetProxy.NOTI_UPDATE_DATASET_LIST,
                  DatasetListComponent.CHANGE_SEARCH_STR,
                  DatasetProxy.NOTI_ADD_DATASET,
                  DatasetProxy.NOTI_MODIFY_DATASET,
                  DatasetProxy.NOTI_REMOVE_DATASET,
                  DatasetStatic.CMD_VIEW_QUERY,
                  DatasetProxy.NOTI_NO_EXIST_DATASET
            ]
      }


      public handleNotification(note: puremvc.Notification): void {
            switch (note.name) {
                  case DatasetProxy.NOTI_NO_EXIST_DATASET :
                        this.view.noExistDataset();
                        break;
                  case DatasetProxy.NOTI_UPDATE_DATASET_LIST :
                        this.view.setDatasetList( note.body );
                        this.view.setExportSearchResult( note.body );
                        this.setSearchResult(this.currentSearchStr);
                        break;
                  case DatasetStatic.CMD_VIEW_QUERY:
                        this.view.viewQuery(note.getBody());
                        break;
                  case DatasetProxy.NOTI_ADD_DATASET:
                  case DatasetProxy.NOTI_MODIFY_DATASET:
                  case DatasetProxy.NOTI_REMOVE_DATASET:
                        this.sendNotification( DatasetStatic.CMD_LOAD_DATASET_LIST );
                        break;
            }
      }
}
