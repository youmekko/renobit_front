import * as puremvc from "puremvc";
import DatasetManagerMainComponent from "./DatasetManagerMainComponent.vue";
import Vue from "vue";
import DatasetProxy from "../model/DatasetProxy";


export default class DatasetManagerMainMediator extends puremvc.Mediator {
	public static NAME: string = "DatasetManagerMainMediator";
	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);		
    }

	private get proxy():DatasetProxy{
		return <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
	}

	public getView(): DatasetManagerMainComponent {
		return this.viewComponent;
	}

	public onRegister() {
		window.__serverUrl__ = window.opener.window.__serverUrl__;
		//config에서 remote로 다른 서버를 설정할 수 있기 때문에  configManager에서 설정한 serverUrl을 따라야한다
		//window.opener에서 객체(configManager 직접접근)를 접근할경우 ie에서 심각한 속도 저하가 있기 때문에 window 변수로 main Editor에서 저장한 window.__serverUrl__을 가져다 사용한다.
	}

	

	public listNotificationInterests(): string[] {
		return [			
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		
	}
}