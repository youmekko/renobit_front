import * as puremvc from "puremvc";
import Vue from "vue";
import {dto} from "../../../model/dto/DatasetDTO";
import DatasetStatic from "../../../controller/DatasetStatic";
import DataSourceSettingsComponent from "./DataSourceSettingsComponent.vue";
import {ObjectUtil} from "../../../../../../wemb/utils/Util";
import DataSourceDTO = dto.DataSourceDTO;
import DatasetProxy from "../../../model/DatasetProxy";

export default class DataSourceSettingsMediator extends puremvc.Mediator {
      public static NAME: string = "DataSourceSettingsMediator";
      private view: DataSourceSettingsComponent = null;

      constructor(name: string, viewComponent: Vue) {
            super(name, viewComponent);
            this.view = <DataSourceSettingsComponent>viewComponent;
      }

      public getView(): DataSourceSettingsMediator {
            return this.viewComponent;
      }

      private get proxy():DatasetProxy{
            return <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
      }

      public onRegister() {
            this.view.$on(DataSourceSettingsComponent.ADD_DATASOURCE, (data: DataSourceDTO) => {
                  data.id = ObjectUtil.generateID();
                  this.sendNotification(DatasetStatic.CMD_ADD_DATASOURCE, data);
            })

            this.view.$on(DataSourceSettingsComponent.REMOVE_DATASOURCE, (data: DataSourceDTO) => {
                  this.sendNotification(DatasetStatic.CMD_REMOVE_DATASOURCE, data);
            })

            this.view.$on(DataSourceSettingsComponent.TEST_DATASOURCE, (data: DataSourceDTO) => {
                  this.sendNotification(DatasetStatic.CMD_TEST_DATASOURCE, data);
            })

            this.view.$on(DataSourceSettingsComponent.CHECK_DATASOURCE, (data: DataSourceDTO) => {
                  this.checkDataSource(data);
            })
      }

      private checkDataSource(datasource:DataSourceDTO) {
            let item = this.proxy.getDatasetByDatasource(datasource.id);
            if(item) {
                  this.view.alertNoDelete();
            } else {
                  this.view.deleteDatasource(datasource);
            }
      }


      public listNotificationInterests(): string[] {
            return [
                  DatasetProxy.NOTI_LOAD_DATA_SOURCE,
                  DatasetProxy.NOTI_TEST_DATA_SOURCE
            ]
      }


      public handleNotification(note: puremvc.Notification): void {
            switch (note.name) {
                  case DatasetProxy.NOTI_LOAD_DATA_SOURCE:
                        this.view.setList(note.getBody());
                  case DatasetProxy.NOTI_TEST_DATA_SOURCE:
                        this.view.connectResult();
                  break;
            }
      }

}
