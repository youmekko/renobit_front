import * as puremvc from "puremvc";
import DatasetTabsComponent from "./DatasetTabsComponent.vue";
import Vue from "vue";
import DatasetStatic from "../../controller/DatasetStatic";
import DatasetProxy from "../../model/DatasetProxy";
import DatasetModifyFormMediator from "./settings/DatasetModifyFormMediator";
import DatasetQueryTypeFieldsMediator from "./settings/query/DatasetQueryTypeFieldsMediator";
/**
 * 데이터셋 수정 탭은 동적으로 컴포넌트가 추가되고 삭제 되기 때문에,
 * DatasetModifyFormMediator를 동적으로 등록/삭제를 반복한다.
 * PrepDatasetManagerViewCommand에서는 DatasetModifyFormMediator를 등록하지 않는다.
 * 
 * 생성탭과 수정탭 Mediator기능이 거의 동일하므로 DatasetFormMediator를 상속받아 중복 코드를 제거한다.
*/
export default class DatasetTabsMediator extends puremvc.Mediator {
	public static NAME: string = "DatasetTabsMediator";    
	private view:DatasetTabsComponent  = null;

	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <DatasetTabsComponent>viewComponent;
	}

	public getView(): DatasetTabsComponent {
		return this.viewComponent;
	}

	public onRegister() {
		this.view.$on(DatasetTabsComponent.EVENT_OPEN_MODIFY_DATASET_TAB, ()=>{
			this.registerModifyDatasetFormMediator();
		});

		this.view.$on(DatasetTabsComponent.EVENT_CLOSE_MODIFY_DATASET_TAB, ()=>{
			this.removeModifyDatasetFormMediator();
		});
	}

	/*동적 DatasetModifyFormMediator 등록*/
	private registerModifyDatasetFormMediator(){		
		this.removeModifyDatasetFormMediator();
            this.facade.registerMediator(new DatasetModifyFormMediator(DatasetModifyFormMediator.NAME, window.wemb.viewComponentMap.get(DatasetModifyFormMediator.NAME)));
            this.facade.registerMediator(new DatasetQueryTypeFieldsMediator(DatasetQueryTypeFieldsMediator.MODIFY_NAME, window.wemb.viewComponentMap.get(DatasetQueryTypeFieldsMediator.MODIFY_NAME)));
	}

	/*동적 DatasetModifyFormMediator 삭제*/
	private removeModifyDatasetFormMediator(){
		if( this.facade.retrieveMediator(DatasetModifyFormMediator.NAME) ){
                  this.facade.removeMediator(DatasetModifyFormMediator.NAME);
		}
		if (this.facade.retrieveMediator(DatasetQueryTypeFieldsMediator.MODIFY_NAME)) {
                  this.facade.removeMediator(DatasetQueryTypeFieldsMediator.MODIFY_NAME);
            }

	}
	
	/*리스트에서 데이터셋 수정 버튼을 누르면 탭이 추가돼야하므로 Noti를 등록함*/
	public listNotificationInterests(): string[] {
		return [
			DatasetProxy.NOTI_CHANGE_MODIFY_DATASET
			//DatasetProxy.NOTI_UPDATE_DATASET_LIST,
			//DatasetTabsComponent.CHANGE_SEARCH_STR
		]
	}


	public handleNotification(note: puremvc.Notification): void {		
		switch (note.name) {
			case DatasetProxy.NOTI_CHANGE_MODIFY_DATASET :
				//데이터셋 수정탭 오픈 
				this.view.openEditTab( note.body );
				break;
				
		}
	}

}
