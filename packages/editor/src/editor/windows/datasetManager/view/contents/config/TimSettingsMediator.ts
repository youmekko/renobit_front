import * as puremvc from "puremvc";
import TimSettingsComponent from "./TimSettingsComponent.vue";
import Vue from "vue";
import {dto} from "../../../model/dto/DatasetDTO";
import TimItemDTO = dto.TimItemDTO;
import DatasetStatic from "../../../controller/DatasetStatic";
import DatasetProxy from "../../../model/DatasetProxy";
import {ObjectUtil} from "../../../../../../wemb/utils/Util";

export default class TimSettingsMediator extends puremvc.Mediator {
	public static NAME: string = "TimSettingsMediator";
	private view:TimSettingsComponent  = null;
	private list: Array<TimItemDTO> = [];
	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <TimSettingsComponent>viewComponent;
	}

	public getView(): TimSettingsComponent {
		return this.viewComponent;
	}

      public get proxy():DatasetProxy{
            return <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
      }

	public onRegister() {
		this.view.$on(TimSettingsComponent.UPDATE_TIM_URL, (list:Array<TimItemDTO>)=>{
			this.sendNotification( DatasetStatic.CMD_UPDATE_TIM_URL_LIST, list );
		})

		this.view.$on(TimSettingsComponent.REMOVE_TIM_URL, (id:string)=>{
			let list = this.list.filter(x=>x.id != id);
			this.sendNotification( DatasetStatic.CMD_UPDATE_TIM_URL_LIST, list );
		})

		this.view.$on(TimSettingsComponent.ADD_TIM_URL, (data:TimItemDTO)=>{
			var item = new TimItemDTO();
                item.id = ObjectUtil.generateID();
                item.title = data.title;
                item.url = data.url;

                let list = this.list.concat();
                list.unshift(item);
				this.sendNotification( DatasetStatic.CMD_UPDATE_TIM_URL_LIST, list );
		})

            this.view.$on(TimSettingsComponent.LOAD_TIM_WORKER_LIST, (url:string)=>{
                  this.proxy.loadTimWorkList(url).then(result=>{
                        this.view.addTimUrl();
                  })
            })

		/*console.log("@@@ searchInput", this.view, this.view.$refs, this.view.$refs["searchInput"]);
		this.view.$on(TimSettingsComponent.CHANGE_SEARCH_STR, (str)=>{
			console.log(str);
			let datasetProxy:DatasetProxy = <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
			let searchList = datasetProxy.getDatasetSearchList(str);
			this.view.setSearchResult(searchList);
		})*/
	}


	public listNotificationInterests(): string[] {
		return [
			DatasetProxy.NOTI_LOAD_TIM_URL_LIST,
			DatasetProxy.NOTI_LOAD_TIM_WORKER_LIST,
			DatasetProxy.NOTI_UPDATE_TIM_URL_LIST
			//DatasetProxy.NOTI_UPDATE_DATASET_LIST,
			//TimSettingsComponent.CHANGE_SEARCH_STR
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		switch (note.name) {
			case DatasetProxy.NOTI_LOAD_TIM_URL_LIST :
			case DatasetProxy.NOTI_UPDATE_TIM_URL_LIST :
				this.list = note.body;
				this.view.updateList( this.list );
				break;
		}
	}

}
