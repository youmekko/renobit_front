import * as puremvc from "puremvc";
import DatasetFormMediator from "./DatasetFormMediator";
import DatasetFormComponent from "./DatasetFormComponent.vue";
import Vue from "vue";
import DatasetStatic from "../../../controller/DatasetStatic";
import DatasetProxy from "../../../model/DatasetProxy";

export default class DatasetCreateFormMediator extends DatasetFormMediator {
	public static NAME: string = "DatasetCreateFormMediator";    
	
	constructor(name: string, viewComponent: Vue) {
		super(name, DatasetStatic.DATASET_FORM_CREATE_TYPE, viewComponent);	
		this.view = <DatasetFormComponent>viewComponent;
		
	}

	
	public addDataset(){		
		let data = this.getSendData();
		if( this.checkSaveCondition( data ) ){
			this.sendNotification( DatasetStatic.CMD_ADD_DATASET, data);

			/*Vue.$DatasetService.addDataset( data ).then((result) => {
				if( result.result == "ok" ){
					this.view.$message({ type: 'success',  message: '데이터셋이 등록되었습니다.' });
					//this.init();
				}else{
					this.view.$message({ type: 'error',  message: '[등록 실패] 다시 시도해주세요.' });
					//this.init();///임시처리
				}
			}, (error) => {
					//console.log("error");
				return error;
			})*/
		}
	}

	public onRegister() {    
		super.onRegister();   
        //중복체크 
        this.view.$on(DatasetStatic.EVENT_ADD_DATASET, ()=>{
			this.addDataset();
		});

        
	}

	public listNotificationInterests(): string[] {
		let notis = super.listNotificationInterests();

		return notis.concat([
			DatasetProxy.NOTI_ADD_DATASET
            //DatasetFormComponent.DOUBLE_CHECK
			//proxy.NOTI_UPDATE_DATASET_LIST,
			//DatasetFormComponent.CHANGE_SEARCH_STR
		])
	}

	public handleNotification(note: puremvc.Notification): void {
		var locale_msg = Vue.$i18n.messages.wv;
		super.handleNotification(note);
		switch (note.name) {
			case DatasetProxy.NOTI_ADD_DATASET :
				Vue.$message(locale_msg.common.successAdd);
			      this.view.reset();
				//this.view.setDatasetList( note.body.data );
				break;
		}/**/
	}
}
