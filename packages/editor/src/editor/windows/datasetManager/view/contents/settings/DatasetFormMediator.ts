import * as puremvc from "puremvc";
import DatasetFormComponent from "./DatasetFormComponent.vue";
import {dto} from "../../../model/dto/DatasetDTO"
import DatasetItemDTO = dto.DatasetItemDTO;
import TimItemDTO = dto.TimItemDTO;
import Vue from "vue";
import DatasetStatic from "../../../controller/DatasetStatic";
import DatasetProxy from "../../../model/DatasetProxy";
import { Dataset } from "../../../const/Dataset";
import { ObjectUtil } from "../../../../../../wemb/utils/Util";

export default class DatasetFormMediator extends puremvc.Mediator {
    public static NAME: string = "DatasetFormMediator";    
	protected view:DatasetFormComponent = null;
    protected type:string;

    /*private datasetInfo:any = {
        name: '',
        data_type: Dataset.DATA_TYPE.SQL,
        delivery_type: Dataset.DELIVERY_TYPE.POLLING,
        interval: 10,
        rest_api: '',
        queryStr: '',
        dataset_id: '',
        deliveryMinTime: 0,
        isDeliveryOnce: false
    }*/

    protected doubleCheckName: string = '';
    protected tempPreviewDataStr: string = '';
    protected tempDataStr: string = '';
    protected timUrlList: Array<TimItemDTO>

    private readonly NO_INCLUDE_QUERY_KEYWORD:Array<string> = ["create", "drop", "alter", "truncate"];
	constructor(name: string, type:string, viewComponent: Vue) {
		super(name, viewComponent);
        this.view = <DatasetFormComponent>viewComponent;
        this.type = type;
		//console.log("@@@@@@ EDIT");
        
	}

    public get proxy():DatasetProxy{        
         return <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
    }

	public getView(): DatasetFormComponent {
		return this.view;
	}

    init(){
        /*this.datasetInfo = {
            name: '',
            data_type: Dataset.DATA_TYPE.SQL,
            delivery_type: Dataset.DELIVERY_TYPE.POLLING,
            interval: 10,
            rest_api: '',
            queryStr: '',
            dataset_id: '',            
            deliveryMinTime: 0,
            isDeliveryOnce: false
        }

        
        if(this.$refs.previewComp){
            this.$refs.previewComp.init();
        }
        
        if(this.$refs.queryFieldComp){
            this.$refs.queryFieldComp.init();
        }

        if(this.$refs.timFieldComp){
            this.$refs.timFieldComp.init();
        }*/

        this.updateTempDataStr();
    }

    updateTempDataStr(){
        this.tempDataStr = JSON.stringify( this.getSendData() );
    }

    setTempPreviewDataStr(data){
        if( !data ){
            this.tempPreviewDataStr = '';
            return;
        }

        this.tempPreviewDataStr = JSON.stringify(data);
    }

    getFilterPreviewData(data:any){
        let previewData:any = {
            data_type: data.data_type,
            param_info: data.param_info,
            query: "",
            rest_api: ""
        };

        switch( data.data_type ){
            case Dataset.DATA_TYPE.SQL:
                previewData.query = data.query;
                previewData.query_type = data.query_type;
                previewData.datasource = data.datasource;
                previewData.dbType = data.dbType;
            break;
            case Dataset.DATA_TYPE.TIM:
            case Dataset.DATA_TYPE.REST:
                previewData.rest_api = data.rest_api;
            break;
        }

        return previewData;
    }

    compareTempPreviewData(){
        let currentData = JSON.stringify(this.getFilterPreviewData(this.getSendData()));
        if( currentData != this.tempPreviewDataStr ){
            return true;
        }
        return false;
    }

    loadPreviewData(isRestApi){       
        let previewData = this.getFilterPreviewData( this.getSendData() );
        if(!this.checkTypeContent(previewData)){
            this.setTempPreviewDataStr(null);
            return;
        }

        if(!isRestApi){
             this.proxy.loadPreviewData(previewData).then((result)=>{
                this.view.setPreviewData(result);
                previewData && (previewData.dbType = result.dbType);
                this.setTempPreviewDataStr(result ? previewData : null);
            }, (error) => {
                this.view.setPreviewData(null);
                this.setTempPreviewDataStr(null);
            });  
        }else{
            this.proxy.loadRestApiPreviewData(previewData).then((result)=>{
                this.view.setRestApiPreviewData(result);
                this.setTempPreviewDataStr(previewData);
            }, (error) => {
                this.view.setRestApiPreviewData(null);
                this.setTempPreviewDataStr(null);
            });       
        }            
    }


    getSendData():DatasetItemDTO{        
        return this.view.getDatasetInfo();
    }


    checkTypeContent(data):boolean{
        var msg = this.view.$i18n.messages.wv;
        switch( data.data_type ){
            case Dataset.DATA_TYPE.SQL:
                if( !data.query || !data.query.length ){
                    if(data.query_type === Dataset.DYNAMIC_TYPE.STATIC) {
                        Vue.$message({ type: 'error',  message: msg.dataset.errorMsg01 });
                    } else {
                        Vue.$message({ type: 'error',  message: msg.dataset.errorMsg11 });
                    }
                    return false;
                } else if (this.NO_INCLUDE_QUERY_KEYWORD.some(key => data.query.toLowerCase().includes(key.toLowerCase()))) {
                    Vue.$message({ type: 'error',  message: msg.dataset.noDDLQuery});
                    return false;
                }
            break;
            case Dataset.DATA_TYPE.TIM:
            case Dataset.DATA_TYPE.REST:
                if( !data.rest_api || !Object.keys(data.rest_api).length ){
                    Vue.$message({ type: 'error',  message: msg.dataset.errorMsg02 });
                    return false;
                }

                
            break;
        }

        return true;
    }

    checkSaveCondition(data){
        var msg = this.view.$i18n.messages.wv;
        if( !data.name.length ){
            Vue.$message({ type: 'error',  message: msg.dataset.errorMsg03 });
            return false;
        }

        if(!this.validCheckDatasetName(data.name)){
            return false;
        }

        
        
        if( this.doubleCheckName != data.name ){
            Vue.$message({ type: 'error',  message: msg.dataset.errorMsg04 });
            return false;
        }

        if( !data.dataset_id ){
            //신규 등록일경우만 
            if( this.proxy.doubleCheckDatasetName( data.name ) ){
                Vue.$message({ type: 'error',  message: msg.dataset.errorMsg05 });
                return false;
            }
        }   
        

        if(!this.checkTypeContent(data)){
            return false;
        }

        if( this.tempPreviewDataStr == null ){
            Vue.$message({ type: 'error',  message: msg.dataset.errorMsg06 });
            return false;
        }else if( this.compareTempPreviewData() ){
            Vue.$message({ type: 'error',  message: msg.dataset.errorMsg07 });
            return false;
        }

        return true;
    }

    //데이터셋 이름 중복체크
    protected doubleCheck( name:string ):boolean{
        var msg = this.view.$i18n.messages.wv;
         if(!name.length){
            Vue.$message({
                type: 'error',
                message: msg.dataset.errorMsg03
            })
            return false;
        }

       
        if( !this.validCheckDatasetName(name) ){
           
            return false;
        }

        if( this.proxy.doubleCheckDatasetName( name ) ){
            Vue.$message({
                type: 'error',
                message: msg.dataset.errorMsg08
            });

            return false;
        }else{
            Vue.$message({
                type: 'success',
                message: msg.dataset.successMsg01
            });

            this.doubleCheckName = name;
            return true;
        }
    }

    private validCheckDatasetName(name){
        var msg = this.view.$i18n.messages.wv;
        let nameReg = /[^a-zA-Z0-9_$]|^[0-9]+/;
        if( nameReg.test(name) ){
            Vue.$message({
                type: 'error',
                message: msg.dataset.errorMsg09
            });
            return false;
        }
        return true;
    }

	public onRegister() {      
        //중복체크 
        this.view.$on(DatasetStatic.EVENT_DOUBLE_CHECK, (name)=>{
            this.doubleCheck( name );
		});

        /**미리보기*/
        this.view.$on(DatasetStatic.EVENT_REQUEST_PREVIEW_DATA, (name)=>{
            this.loadPreviewData( false );
        });

        this.view.$on(DatasetStatic.EVENT_REQUEST_REST_API_PREVIEW_DATA, ()=>{
            this.loadPreviewData( true );
        });

        /**tim**/
        this.view.$on(DatasetStatic.LOAD_TIM_WORKER_LIST, (url:string)=>{
			this.proxy.loadTimWorkList(url).then((result)=>{
				//console.log("@@@@@@ list", result)                
                let data = result || [];
                this.view.updateTimWorkerList( data );
			
				return result;
			}, (error)=>{
				return error;
			})
		});

        this.view.$on(DatasetStatic.LOAD_TIM_WORKER_PREVIEW, (workerUrl:string)=>{
             let previewData:any ={
                data_type: Dataset.DATA_TYPE.TIM,
                query: "",
                rest_api: workerUrl
            };
            
			this.proxy.loadPreviewData(previewData).then((result)=>{
                this.view.setTimWorkerPreview(result);
                return result;
            }, (error)=>{
                return error;
            })
        })

        this.view.$on(DatasetStatic.EVENT_PREVIEW_COPY_TO_CLIPBOARD,(data:string)=>{
            ObjectUtil.copyToClipboard(data);
        })
	}


	public listNotificationInterests(): string[] {
		return [
            DatasetProxy.NOTI_UPDATE_TIM_URL_LIST,
			DatasetProxy.NOTI_LOAD_TIM_URL_LIST
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		switch (note.name) {
			case DatasetProxy.NOTI_UPDATE_TIM_URL_LIST:
			case DatasetProxy.NOTI_LOAD_TIM_URL_LIST:
                this.timUrlList = note.body;
				this.view.updateTimUrlList( note.body );
				break;
		}
	}
    

}
