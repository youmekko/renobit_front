import * as puremvc from "puremvc";
import Vue from "vue";
import {dto} from "../../../../model/dto/DatasetDTO";
import DatasetStatic from "../../../../controller/DatasetStatic";
import DatasetQueryTypeFieldsComponent from "./DatasetQueryTypeFieldsComponent.vue";
import {ObjectUtil} from "../../../../../../../wemb/utils/Util";
import DataSourceDTO = dto.DataSourceDTO;
import DatasetProxy from "../../../../model/DatasetProxy";

export default class DatasetQueryTypeFieldsMediator extends puremvc.Mediator {
      public static NAME:string = "DatasetQueryTypeFieldsMediator";
      public static MODIFY_NAME:string = "DatasetQueryTypeFieldsModifyMediator";
      private view:DatasetQueryTypeFieldsComponent = null;
      private currentSearchStr: string = '';
      private dynamicList:Array<any> = [];

      public get proxy():DatasetProxy{
            return <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
      }

      constructor(name: string, viewComponent: Vue) {
            super(name, viewComponent);
            this.view = <DatasetQueryTypeFieldsComponent>viewComponent;
      }

      public getView(): DatasetQueryTypeFieldsComponent {
            return this.view;
      }

      private setSearchResult(str) {
            let searchList = this.dynamicList.filter(sql => {
                  return sql.methodName.toLowerCase().trim().indexOf(str.toLowerCase().trim()) > -1;
            });
            this.view.updateDynamicSQL(searchList);
      }

      public onRegister(): void {
            this.view.$on(DatasetQueryTypeFieldsComponent.LOAD_DYNAMIC_SQL, (data: string, mode: boolean) => {
                  this.proxy.loadDynamicSQL(data).then(result => {
                        this.dynamicList = result.data;
                        if (mode) {
                              this.view.updateDynamicSQLMode(result.data);
                        } else {
                              this.view.updateDynamicSQL(result.data);
                        }
                  }, error => {
                        return error;
                  });
            });

            this.view.$on(DatasetQueryTypeFieldsComponent.CHANGE_SEARCH_STR, (data: string) => {
                  this.currentSearchStr = data;
                  this.setSearchResult(data);
            });

            this.view.$on(DatasetQueryTypeFieldsComponent.VIEW_QUERY, (data: string) => {
                  this.sendNotification(DatasetStatic.CMD_VIEW_QUERY, data);
            });

            this.view.$on(DatasetQueryTypeFieldsComponent.LOAD_DATASOURCE, () => {
                  this.proxy.loadDataSourceFromQueryTypeField().then(result => {
                        this.view.updateMultiDataSource(result);
                  }).catch(error => {
                        return error;
                  });
            });
      }

      public listNotificationInterests(): string[] {
            return [
                  DatasetProxy.NOTI_LOAD_DATA_SOURCE
            ];
      }

      public handleNotification(note: puremvc.Notification): void {
            switch (note.name) {
                  case DatasetProxy.NOTI_LOAD_DATA_SOURCE:
                        this.view.updateMultiDataSource(note.getBody());
                        break;
            }
      }
}
