import * as puremvc from "puremvc";
import Vue from "vue";
import DatasetFormMediator from "./DatasetFormMediator";
import DatasetFormComponent from "./DatasetFormComponent.vue";
import {dto} from "../../../model/dto/DatasetDTO"
import DatasetItemDTO = dto.DatasetItemDTO;
import DatasetStatic from "../../../controller/DatasetStatic";
import DatasetProxy from "../../../model/DatasetProxy";

export default class DatasetModifyFormMediator extends DatasetFormMediator {
	public static NAME: string = "DatasetModifyFormMediator";    

	private currentDatasetId:string = "";

	constructor(name: string, viewComponent: Vue) {
		super(name, DatasetStatic.DATASET_FORM_MODIFY_TYPE, viewComponent);
		setTimeout(()=>{
			this.view.updateTimUrlList( this.proxy.getTimUrlList() );
			//_timUrlList
		}, 20);
	}	

	
	editDataset( dataset:DatasetItemDTO ){
		if( this.currentDatasetId != "" ){
			if( this.tempDataStr != '' && this.isModifiedDataset()){
				var locale_msg = Vue.$i18n.messages.wv;
				Vue.$confirm(locale_msg.dataset.editWarning, locale_msg.common.notification, {
					confirmButtonText: 'OK',
					cancelButtonText: 'Cancel',
					type: 'warning'
				}).then(() => {
					this.setEditData(dataset);
				}).catch(() => {

				});
			}else{
				this.setEditData(dataset);
			}
		}else{
			this.setEditData(dataset);
		}
	}

	private setEditData(dataset: DatasetItemDTO){
		this.doubleCheckName = dataset.name;
		this.view.setEditData(dataset);
		this.currentDatasetId = dataset.dataset_id;
		this.updateTempDataStr();
	}

	isModifiedDataset(){
		if( this.tempDataStr != null &&
			this.tempDataStr == JSON.stringify( this.getSendData() )){
			return false;
		}else{
			return true;
		}
	}

	modifyDataset(){
		var locale_msg = Vue.$i18n.messages.wv;
		let data = this.getSendData();
		if( this.proxy.getDatasetById(data.dataset_id)){
			if( this.checkSaveCondition( data ) ){
				this.sendNotification( DatasetStatic.CMD_MODIFY_DATASET, data);
                        this.setTempPreviewDataStr(null);
			}
		}else{
			Vue.$message.error(locale_msg.common.errorValidate);
		}		
	}

	public onRegister() {    
		super.onRegister();   
        //중복체크 
        this.view.$on(DatasetStatic.EVENT_CHANGE_MODIFY_DATASET, (dataset)=>{
			this.editDataset(dataset);
		});

		this.view.$on(DatasetStatic.EVENT_MODIFY_DATASET, ()=>{
			this.modifyDataset();	
		})
	}

	public listNotificationInterests(): string[] {
		let notis= super.listNotificationInterests();

		return notis.concat([
			DatasetProxy.NOTI_MODIFY_DATASET			
		]);
	}

	public handleNotification(note: puremvc.Notification): void {
		super.handleNotification(note);
		switch (note.name) {
			case DatasetProxy.NOTI_MODIFY_DATASET :
				this.updateTempDataStr();
                        this.view.reset();
				break;			
		}
	}
}
