export default class DatasetStatic {
	public static readonly  CMD_ADD_DATASET:string = "command/addDataset";
    public static readonly  CMD_REMOVE_DATASET:string = "command/removeDataset";
    public static readonly  CMD_UPDATE_DATASET_LIST:string = "command/updateDatasetList";
    public static readonly  CMD_LOAD_DATASET_LIST:string = "command/loadDataset";
    public static readonly  CMD_MODIFY_DATASET:string = "command/modifyDataset";
    public static readonly  CMD_DUPLICATE_DATASET:string = "command/duplicateDataset";

    public static readonly  CMD_LOAD_TIM_URL_LIST:string = "command/loadTimUrlList";
    public static readonly  CMD_UPDATE_TIM_URL_LIST:string = "command/updateTimUrlList";
    public static readonly  CMD_LOAD_TIM_WORKER_LIST:string = "command/loadTimWorkerList";
    public static readonly  CMD_ADD_DATASOURCE:string = "command/addDataSource";
    public static readonly  CMD_LOAD_DATASOURCE:string = "command/loadDataSource";
    public static readonly  CMD_REMOVE_DATASOURCE:string = "command/removeDataSource";
    public static readonly  CMD_TEST_DATASOURCE:string = "command/testDataSource";
    public static readonly  CMD_VIEW_QUERY:string = "command/loadDynamicSQL";

    public static readonly EVENT_DOUBLE_CHECK: string = "event/doubleCheck";
    public static readonly EVENT_REQUEST_PREVIEW_DATA: string = "event/eventRequestPreviewData";
    public static readonly EVENT_REQUEST_REST_API_PREVIEW_DATA: string = "event/eventRequestRestApiPreviewData";
    public static readonly EVENT_ADD_DATASET: string = "event/addDataset";
    public static readonly EVENT_DELETE_DATASET: string = "event/deleteDataset";
    public static readonly EVENT_MODIFY_DATASET: string = "event/modifyDataset";
    public static readonly EVENT_CHANGE_MODIFY_DATASET: string = "event/changeModifyDataset";
    public static readonly EVENT_PREVIEW_COPY_TO_CLIPBOARD: string = "event/previewCopyToClipboard";

    public static readonly DATASET_FORM_CREATE_TYPE:string = "datasetFormCreateType";
    public static readonly DATASET_FORM_MODIFY_TYPE:string = "datasetFormModifyType";

    public static readonly LOAD_TIM_WORKER_LIST:string = "loadTimWorkerList";
    public static readonly LOAD_TIM_WORKER_PREVIEW:string = "loadTimWorkerPreview";

}
