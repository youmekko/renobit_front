import * as puremvc from "puremvc";
import DatasetProxy from "../model/DatasetProxy";
import {dto} from "../model/dto/DatasetDTO";
import DataSourceDTO = dto.DataSourceDTO;


export class TestDatasourceCommand extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            let datasetProxy: DatasetProxy = <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
            datasetProxy.testDataSource(<DataSourceDTO>note.getBody());
      }
}
