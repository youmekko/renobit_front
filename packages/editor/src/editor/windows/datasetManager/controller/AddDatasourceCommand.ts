import * as puremvc from "puremvc";
import {dto} from "../model/dto/DatasetDTO";
import DatasetProxy from "../model/DatasetProxy";
import DataSourceDTO = dto.DataSourceDTO;


export class AddDatasourceCommand extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            let datasetProxy: DatasetProxy = <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
            let data:DataSourceDTO = <DataSourceDTO>note.getBody();
            datasetProxy.addDataSource(data);
      }
}
