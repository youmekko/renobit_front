import * as puremvc from "puremvc";
import {PrepDatasetManagerModelCommand} from "./PrepDatasetManagerModelCommand";
import {PrepDatasetManagerViewCommand} from "./PrepDatasetManagerViewCommand";
import { DatasetCommand } from "./DatasetCommand";


export default class DatasetManagerStartUpCommand extends puremvc.MacroCommand {
	initializeMacroCommand() {
		//console.log("step 02, StartUpCommand 실행 view, model, command 등록");
		this.addSubCommand(PrepDatasetManagerModelCommand);
		this.addSubCommand(PrepDatasetManagerViewCommand);
		this.addSubCommand(DatasetCommand);
	}
}