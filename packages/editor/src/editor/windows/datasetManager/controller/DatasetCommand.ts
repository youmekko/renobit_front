import * as puremvc from "puremvc";
import DatasetStatic from "./DatasetStatic";
import EditorStatic from '../../../../editor/controller/editor/EditorStatic';
import { LoadDatasetListCommand } from "./LoadDatasetListCommand";
import { AddDatasetCommand } from "./AddDatasetCommand";
import { RemoveDatasetCommand } from "./RemoveDatasetCommand";
import { ModifyDatasetCommand } from "./ModifyDatasetCommand";
import { LoadTimUrlListCommand } from "./LoadTimUrlListCommand";
import { UpdateTimUrlListCommand } from "./UpdateTimUrlListCommand";
import { DuplicateDatasetCommand } from "./DuplicateDatasetCommand";
import {AddDatasourceCommand} from "./AddDatasourceCommand";
import {LoadDatasourceCommand} from "./LoadDatasourceCommand";
import {RemoveDatasourceCommand} from "./RemoveDatasourceCommand";
import {TestDatasourceCommand} from "./TestDatasourceCommand";
import { ShowManualCommand} from "../../../controller/main/ShowManualCommand";

export class DatasetCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		this.facade.registerCommand(DatasetStatic.CMD_LOAD_DATASET_LIST, LoadDatasetListCommand);
		this.facade.registerCommand(DatasetStatic.CMD_ADD_DATASET, AddDatasetCommand);
		this.facade.registerCommand(DatasetStatic.CMD_REMOVE_DATASET, RemoveDatasetCommand);
		this.facade.registerCommand(DatasetStatic.CMD_MODIFY_DATASET, ModifyDatasetCommand);
		
		this.facade.registerCommand(DatasetStatic.CMD_LOAD_TIM_URL_LIST, LoadTimUrlListCommand);
		this.facade.registerCommand(DatasetStatic.CMD_UPDATE_TIM_URL_LIST, UpdateTimUrlListCommand);
		this.facade.registerCommand(DatasetStatic.CMD_DUPLICATE_DATASET, DuplicateDatasetCommand);
            this.facade.registerCommand(DatasetStatic.CMD_ADD_DATASOURCE, AddDatasourceCommand);
            this.facade.registerCommand(DatasetStatic.CMD_LOAD_DATASOURCE, LoadDatasourceCommand);
            this.facade.registerCommand(DatasetStatic.CMD_REMOVE_DATASOURCE, RemoveDatasourceCommand);
            this.facade.registerCommand(DatasetStatic.CMD_TEST_DATASOURCE, TestDatasourceCommand);
            this.facade.registerCommand(EditorStatic.CMD_SHOW_MANUAL, ShowManualCommand);
	}
}
