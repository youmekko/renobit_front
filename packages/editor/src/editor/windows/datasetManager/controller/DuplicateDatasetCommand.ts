import * as puremvc from "puremvc";
import datasetApi from "../../../../common/api/datasetApi";
import DatasetProxy from "../model/DatasetProxy";
import DatasetStatic from "./DatasetStatic";


export class DuplicateDatasetCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let datasetProxy: DatasetProxy = <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
		let datasetId:string = <string>note.getBody();
		datasetProxy.duplicateDataset(datasetId);		
    }
}
