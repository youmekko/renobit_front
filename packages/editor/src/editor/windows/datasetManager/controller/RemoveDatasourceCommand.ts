import * as puremvc from "puremvc";
import DatasetProxy from "../model/DatasetProxy";


export class RemoveDatasourceCommand extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            let datasetProxy: DatasetProxy = <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
            datasetProxy.removeDataSource(note.getBody());
      }
}
