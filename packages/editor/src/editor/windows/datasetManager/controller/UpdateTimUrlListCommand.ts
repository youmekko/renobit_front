import * as puremvc from "puremvc";
import datasetApi from "../../../../common/api/datasetApi";
import { dto } from "../model/dto/DatasetDTO";
import TimItemDTO = dto.TimItemDTO;
import DatasetProxy from "../model/DatasetProxy";
import DatasetStatic from "./DatasetStatic";


export class UpdateTimUrlListCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let datasetProxy: DatasetProxy = <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
		let list:Array<TimItemDTO> = note.getBody();		
		datasetProxy.updateTimUrlList(list);
    }
}
