import * as puremvc from "puremvc";
import DatasetListMediator from "../view/list/DatasetListMediator";
import DatasetTabsMediator from "../view/contents/DatasetTabsMediator";
import DatasetCreateFormMediator from "../view/contents/settings/DatasetCreateFormMediator";
import TimSettingsMediator from "../view/contents/config/TimSettingsMediator";
import DatasetManagerMainMediator from "../view/DatasetManagerMainMediator";
import DataSourceSettingsMediator from "../view/contents/config/DataSourceSettingsMediator";
import DatasetQueryTypeFieldsMediator from "../view/contents/settings/query/DatasetQueryTypeFieldsMediator";


export class PrepDatasetManagerViewCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {		
		let datasetManagerMainMediator: puremvc.IMediator = new DatasetManagerMainMediator(DatasetManagerMainMediator.NAME, note.getBody());
		this.facade.registerMediator(datasetManagerMainMediator);

		this.facade.registerMediator(new DatasetListMediator(DatasetListMediator.NAME, window.wemb.viewComponentMap.get(DatasetListMediator.NAME)));
		this.facade.registerMediator(new DatasetTabsMediator(DatasetTabsMediator.NAME, window.wemb.viewComponentMap.get(DatasetTabsMediator.NAME)));
		this.facade.registerMediator(new DatasetCreateFormMediator(DatasetCreateFormMediator.NAME, window.wemb.viewComponentMap.get(DatasetCreateFormMediator.NAME)));
		this.facade.registerMediator(new TimSettingsMediator(TimSettingsMediator.NAME, window.wemb.viewComponentMap.get(TimSettingsMediator.NAME)));
            this.facade.registerMediator(new DataSourceSettingsMediator(DataSourceSettingsMediator.NAME, window.wemb.viewComponentMap.get(DataSourceSettingsMediator.NAME)));
            this.facade.registerMediator(new DatasetQueryTypeFieldsMediator(DatasetQueryTypeFieldsMediator.NAME, window.wemb.viewComponentMap.get(DatasetQueryTypeFieldsMediator.NAME)));
	}
}
