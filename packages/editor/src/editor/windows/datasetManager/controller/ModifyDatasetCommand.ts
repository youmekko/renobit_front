import * as puremvc from "puremvc";
import datasetApi from "../../../../common/api/datasetApi";
import { dto } from "../model/dto/DatasetDTO";
import DatasetItemDTO = dto.DatasetItemDTO;
import DatasetProxy from "../model/DatasetProxy";
import DatasetStatic from "./DatasetStatic";


export class ModifyDatasetCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let datasetProxy: DatasetProxy = <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
		let data:DatasetItemDTO = <DatasetItemDTO>note.getBody();
		datasetProxy.modifyDatset(data);		
    }
}
