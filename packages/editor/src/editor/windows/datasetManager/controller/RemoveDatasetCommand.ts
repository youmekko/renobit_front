import * as puremvc from "puremvc";
import datasetApi from "../../../../common/api/datasetApi";
import { dto } from "../model/dto/DatasetDTO";
import DatasetItemDTO = dto.DatasetItemDTO;
import DatasetProxy from "../model/DatasetProxy";
import DatasetStatic from "./DatasetStatic";

export class RemoveDatasetCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let datasetProxy: DatasetProxy = <DatasetProxy>this.facade.retrieveProxy(DatasetProxy.NAME);
		let datasetId:string = <string>note.getBody();
		datasetProxy.removeDataset(datasetId);		
    }
}
