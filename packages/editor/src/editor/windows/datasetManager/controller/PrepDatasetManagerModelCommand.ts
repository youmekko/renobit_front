import * as puremvc from "puremvc";
import DatasetProxy from "../model/DatasetProxy";

export class PrepDatasetManagerModelCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		this.facade.registerProxy( new DatasetProxy() );
	}
}