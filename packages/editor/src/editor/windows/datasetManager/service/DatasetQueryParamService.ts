import Vue from "vue";
/**
 * DataType이 DB Query인 경우 파라메터 등록시 사용되는 객체로
 * #{paramValue}와 같이 query문에 파라메터를 추가하는 문법이 추가될 경우
 * 파라메터 목록을 업데이트 하고( ==> 드롭박스에서 목록으로 노출 )
 * 드롭박스에 추가된 항목을 다시 파라메터로 등록하는 경우 소진처리( ==> 드롭박스에서 삭제)를 하는 기능을 한다. 
*/
export default class DatasetQueryParamService {
	private fieldList:Array<string> = [];
    private paramList:Array<any> = [];
    private availableFieldList:Array<string> = [];
    private _$bus:Vue = new Vue();

    get $bus() {
        return this._$bus;
    }

    constructor() {
        this.updateAvailableFieldList();
    }

    public init():void {
        this.fieldList.splice(0, this.fieldList.length);
        this.availableFieldList.splice(0, this.availableFieldList.length);
        this.paramList.splice(0, this.paramList.length);
        this.emitChangeParam();
    }

    

    public setParamList(list):void {
        this.paramList.splice(0, this.paramList.length);
        for (let i = 0, len = list.length; i < len; i++) {
            let item = JSON.parse(JSON.stringify(list[i]));
            this.paramList.push(item);
        }

        this.emitChangeParam();
        this.updateAvailableFieldList();
    }

    private parseSqlStr(str):Array<Object> {
        let a:Array<Object> = this.__getParseSqlStr(str, "#");
        let b:Array<Object> = this.__getParseSqlStr(str, "$");
        let c:Array<Object> = this.__getParseDynamicSqlStr(str, "collection");
        let d:Array<Object> = this.__getParseDynamicSqlStr(str, "test");

        return [...a, ...b, ...c, ...d];
    }

    private __getParseSqlStr(str, prefix):Array<Object> {
        let ary = new Map();
        let startIndex = str.indexOf(prefix+"{");
        str = str.substr(startIndex);

        let closeStrIndex = str.indexOf("}");

        if (startIndex == -1 || closeStrIndex == -1) { return []; }

        str = str.substr(2);

        let splitResult = str.split(prefix+"{");
        splitResult = splitResult.map(x => x.split("}")[0].trim());
        splitResult.forEach(b => {
                if (!ary.get(b)) {
                      ary.set(b, {
                          "field_name": b,
                          "type": "empty"
                    });
                }
            });

        return Array.from(ary.values());
    }

    private __getParseDynamicSqlStr(str = '', prefix = ''):Array<Object> {
          let newList = new Map();
          let reg = new RegExp(`${prefix}="([0-9A-za-z$_]*)"`, "g");
          let match, item;

          while ((match = reg.exec(str)) != null) {
                item = match[1];
                if (!newList.get(item)) {
                      newList.set(item, {
                            "field_name": item,
                            "type": "Array"
                      });
                }
          }

          return Array.from(newList.values());
    }

    private emitChangeParam():void {
        this._$bus.$emit("CHANGE_PARAM", this.paramList);
    }

    public getAvailableFieldList():Array<string> {
        return this.availableFieldList;
    }

    public addParam(param):void {
        ////console.log("@@add param", param);
        this.paramList.push(param);
        this.updateAvailableFieldList();
        this.emitChangeParam();
    }

    public deleteParam(param):void {
        let index = this.paramList.indexOf(param);
        this.paramList.splice(index, 1);
        this.updateAvailableFieldList();
        this.emitChangeParam();
    }

    public getParamJsonData():string {
        return JSON.stringify(this.paramList);
    }

    public getAvailableParamList():Array<any> {
        return this.paramList.filter(x => x.param_name != "");
    }

    public getParamList():Array<any> {
        return this.paramList;
    }

    public setFieldList(list):void {
        this.fieldList.splice(0, this.fieldList.length);
        this.fieldList.push(...list);

        this.updateAvailableFieldList();
    }

    public updateSqlStr(str):void {
        let ary = this.parseSqlStr(str);
        this.setFieldList(ary);
    }

    public updateAvailableFieldList():void {
        let oldData = this.availableFieldList.toString();

        this.availableFieldList.splice(0, this.availableFieldList.length);
        let self = this;

        this.fieldList.forEach((name) => {
            let item = self.paramList.find(x => x.param_name == name);
            if (!item) {
                self.availableFieldList.push(name);
            }
            return name;
        });

        this._$bus.$emit("UPDATE_AVAILABLE_FIELDS", this.availableFieldList);
    }
}
