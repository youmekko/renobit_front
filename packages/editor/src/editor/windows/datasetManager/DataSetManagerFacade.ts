import * as puremvc from "puremvc";
import DatasetManagerStartUpCommand from "./controller/DatasetManagerStartUpCommand";
import DatasetManagerMainComponent from "./view/DatasetManagerMainComponent.vue";
import DatasetStatic from "./controller/DatasetStatic";

/**
 * 데이터셋 관리자는 별도의 window로 실행되는 도구로 별도의 facade가 존재.
 * 데이터셋 리스트는 DB에 저장되고 TIM URL 리스트는 output 폴더에 json으로 저장되어 관리된다. 
 * 여기서 등록된 데이터셋은 WScript로 DataService를 통해 실행된다.
 * Dataset 서버규약 정적 정보 = const/Dataset.ts
 * DatasetDTO = model/dto/DatasetDTO
 * 
 * 자세한 데이터셋 관리자 사용법은 RENOBIT 사용자 매뉴얼 1.2.0참조 
 * TODO : 데이터셋 선택 내보내기 
 */
export default class DatasetManagerFacade extends puremvc.Facade {
    public static CMD_STARTUP: string = 'command/startup';
    public static NAME:string = "DatasetManager";
    constructor(key:string){
        super(key);
    }

    /** @override */
    initializeController() {
        super.initializeController();
        this.registerCommand(DatasetManagerFacade.CMD_STARTUP, DatasetManagerStartUpCommand);
    }

    /** @override */
    initializeModel() {
        super.initializeModel();
    }

    /** @override */
    initializeView() {
        super.initializeView();
    }

    startup(app:DatasetManagerMainComponent) {
        //console.log("step 02, startup() 메서드 호출 ");
        this.sendNotification(DatasetManagerFacade.CMD_STARTUP, app);
        this.sendNotification(DatasetStatic.CMD_LOAD_DATASET_LIST);
        this.sendNotification(DatasetStatic.CMD_LOAD_TIM_URL_LIST);
        this.sendNotification(DatasetStatic.CMD_LOAD_DATASOURCE);
    }

    static getInstance(multitonKey:string):DatasetManagerFacade {
        const instanceMap:any = puremvc.Facade.instanceMap;
        if (!instanceMap[multitonKey]) {
            instanceMap[multitonKey] = new DatasetManagerFacade(multitonKey);
            //console.log("step 02, DatasetManagerFacade 인스턴스 생성");
        }
        return instanceMap[multitonKey] as DatasetManagerFacade;
    }
}
