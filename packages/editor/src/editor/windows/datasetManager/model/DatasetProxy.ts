import * as puremvc from "puremvc";
import Vue from "vue";
import datasetApi from "../../../../common/api/datasetApi";
import {dto} from "./dto/DatasetDTO"
import DatasetItemDTO = dto.DatasetItemDTO;
import TimItemDTO = dto.TimItemDTO;
import DatasetStatic from "../controller/DatasetStatic";
import DataSourceDTO = dto.DataSourceDTO;

/**
 * 데이터셋 목록, TIM LIST URL 목록이 관리됨.
 * - 데이터셋 추가/복제/삭제/업데이트/미리보기/merge하기
 * - TIM 목록 업데이터/worker list 조회
 */
export default class DatasetProxy extends puremvc.Proxy {
      public static readonly NAME: string = "DatasetProxy";
      public static readonly NOTI_UPDATE_DATASET_LIST: string = "notification/updateDatasetList";

      public static readonly NOTI_CHANGE_MODIFY_DATASET: string = "notification/changeModifyDataset";
      public static readonly NOTI_ADD_DATASET: string = "notification/addDataset";
      public static readonly NOTI_REMOVE_DATASET: string = "notification/removeDataset";
      public static readonly NOTI_LOAD_DATASET_LIST: string = "notification/loadDatasetList";
      public static readonly NOTI_MODIFY_DATASET: string = "notification/modifyDataset";

      public static readonly NOTI_LOAD_TIM_URL_LIST: string = "notification/loadTimUrlList";
      public static readonly NOTI_LOAD_TIM_WORKER_LIST: string = "notification/loadTimWorkerList";
      public static readonly NOTI_UPDATE_TIM_URL_LIST: string = "notification/loadTimUrlList";
      public static readonly NOTI_LOAD_DATA_SOURCE: string = "notification/loadDatasource";
      public static readonly NOTI_TEST_DATA_SOURCE: string = "notification/testDatasource";

      public static readonly NOTI_NO_EXIST_DATASET: string="notification/noExistDataset"
      /*******************
       * dataset data
       *******************/
      private _datasetList: Array<DatasetItemDTO> = [];
      private _timUrlList: Array<TimItemDTO> = [];
      private _dataSourceList:Array<DataSourceDTO> = [];


      constructor() {
            super(DatasetProxy.NAME);
      }

      public doubleCheckDatasetName(name) {
            let findItem = this._datasetList.find((x) => x.name == name);
            if (findItem) {
                  return true;
            } else {
                  return false;
            }
      }


      public getDatasetSearchList(searchStr): Array<DatasetItemDTO> {
            return this._datasetList.filter((x) => x.name.toLowerCase().indexOf(searchStr.toLowerCase()) != -1);
      }

      public getDatasetById(id): DatasetItemDTO {
            return this._datasetList.find(x => x.dataset_id == id);
      }

      public getDatasetList(): Array<DatasetItemDTO> {
            return this._datasetList;
      }

      public getDatasetByDatasource(datasourceId):DatasetItemDTO {
            return this._datasetList.find(x=> x.datasource == datasourceId)
      }

      /**데이터셋 리스트를 로드하는 메소드, 데이터셋 추가/삭제/수정 등 변동시마다 호출됨.*/
      public loadDatasetList(): void {
            var locale_msg = Vue.$i18n.messages.wv;
            datasetApi.getDatasetList('').then((result) => {
                  if (result.result == "ok") {
                        this._datasetList = result.data;
                        this.sendNotification(DatasetProxy.NOTI_UPDATE_DATASET_LIST, this._datasetList);
                  } else {
                        Vue.$message.error(locale_msg.common.errorLoad + locale_msg.common.retry);
                  }
            })
      }

      /**
       * 미리보기 시도시 datatype이 query, tim일 경우 아래 메소드를 통해 실행
       */
      public loadPreviewData(previewData): Promise<any> {
            return datasetApi.getPreviewData(previewData).then((result) => {
                  return result;
            }, (error) => {
                  return Promise.reject(error);
            })
      }

      /**
       * 미리보기 시도시 datatype이 rest api 일 경우 아래 메소드를 통해 실행
       * - get/post방식에 따라 파라메터 타입이 디름 get=object, post=string
       */
      public loadRestApiPreviewData(previewData): Promise<any> {
            return new Promise((resolve, reject) => {
                  let options;
                  try {
                        options = JSON.parse(previewData.rest_api);
                        options.method = options.method || "GET";
                        if (options.data && options.method.toLowerCase() == "post" && typeof options.data == "object") {
                              options.data = JSON.stringify(options.data);
                              if (options.data == "{}") {
                                    delete options.data;
                              }
                        }

                  } catch (error) {
                        reject();
                        return false;
                  }

                  $.ajax(options)
                        .done(function (result, textStatus, jqXHR) {
                              let data;
                              if (typeof result == "string") {
                                    data = result;
                              } else {
                                    data = JSON.stringify(result);
                              }

                              resolve(data);
                        })
                        .fail(function () {
                              reject();
                        })
            });
      }


      public addDataset(sendData: DatasetItemDTO) {
            var locale_msg = Vue.$i18n.messages.wv;
            datasetApi.addDataset(sendData).then((result) => {
                  if (result.result == "ok") {
                        this.sendNotification(DatasetProxy.NOTI_ADD_DATASET, sendData);
                        return result;
                  } else {
                        if (result.result.errorCode == "DS_SIZE") {
                              Vue.$message.error(locale_msg.common.errorAdd + locale_msg.common.retry);
                        } else {
                              Vue.$message.error(locale_msg.common.errorAdd + "(oversize)" + locale_msg.common.retry);
                        }
                        return result.errorMsg;
                  }
            }, (error) => {
                  Vue.$message.error(locale_msg.common.errorAdd + locale_msg.common.retry);
                  return error;
            });
      }

      /* 데이터셋 복제시 이름이 겹치지 않게 새이름을 만들어서 추가*/
      public duplicateDataset(datasetId: string) {
            //console.log("@@ duplicate Dataset", datasetId);
            let dataset = this.getDatasetById(datasetId);

            if (dataset.query_detail_info && !dataset.query_detail_info.type) {
                  this.sendNotification(DatasetProxy.NOTI_NO_EXIST_DATASET);
                  return;
            }

            if (dataset) {
                  let newData = $.extend(true, {}, dataset);
                  newData.dataset_id = "";
                  newData.name = this.getDuplicateDatasetName(newData.name);
                  this.addDataset(newData);
            }
      }

      /*데이터셋 복사시 겹치지 않게 새로운 이름 부여*/
      private getDuplicateDatasetName(targetName: string) {
            let name = targetName + "_copy";
            let i = 1;
            while (this._datasetList.find((x) => x.name == name)) {
                  i++;
                  name = targetName + "_copy" + i;
            }

            return name;
      }

      public removeDataset(datasetId: string) {
            var locale_msg = Vue.$i18n.messages.wv;
            datasetApi.removeDataset(datasetId).then((result) => {
                  //console.log("proxy removeDataset", datasetId);
                  if (result.result == "ok") {
                        Vue.$message(locale_msg.common.successDelete);
                        this.sendNotification(DatasetProxy.NOTI_REMOVE_DATASET);
                  } else {
                        Vue.$message.error(locale_msg.common.errorDelete + locale_msg.common.retry);
                  }

                  return result;
            }, (error) => {
                  Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  return error;
            });
      }

      public modifyDatset(dataset: DatasetItemDTO) {
            var locale_msg = Vue.$i18n.messages.wv;
            datasetApi.modifyDataset(dataset).then((result) => {
                  //console.log("proxy modifyDatset", dataset);
                  if (result.result == "ok") {
                        Vue.$message(locale_msg.common.successModify);
                        this.sendNotification(DatasetProxy.NOTI_MODIFY_DATASET);
                  } else {
                        if (result.result.errorCode == "DS_SIZE") {
                              Vue.$message.error(locale_msg.common.errorModify + locale_msg.common.retry);
                        } else {
                              Vue.$message.error(locale_msg.common.errorModify + "(oversize)" + locale_msg.common.retry);
                        }
                  }

                  return result;
            }, (error) => {
                  Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  return error;
            });
      }

      /**
       * 업데이트시 업데이트가 필요한 모든 리스트를 한꺼번에 보내는데,
       * 서버 API에서는 기존에 없던 데이터를 보내면 데이터셋 추가, 기존에 있던(동일 ID)를 보내면 업데이트 처리가 됨
       */
      public updateDatasetList(list: Array<DatasetItemDTO>) {
            var locale_msg = Vue.$i18n.messages.wv;
            datasetApi.updateDatasetList(list).then((result) => {
                  if (result.result == "ok") {
                        this.loadDatasetList();
                  } else {
                        Vue.$message.error(locale_msg.common.errorSave + locale_msg.common.retry);
                  }
                  return result;
            }, (error) => {
                  Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  return error;
            });
      }

      public setModifyDatasetById(datasetId) {
            let dataset = this._datasetList.find(x => x.dataset_id == datasetId);

            if ((dataset.query_detail_info && !dataset.query_detail_info.type) ||
                  (dataset.db_type && !this._dataSourceList.some(datasource => {return datasource.db_type === dataset.db_type}))) {
                  this.sendNotification(DatasetProxy.NOTI_NO_EXIST_DATASET);
                  return;
            }

            if (dataset) {
                  this.sendNotification(DatasetProxy.NOTI_CHANGE_MODIFY_DATASET, dataset);
            }
      }

      /**
       * 데이터셋 가져오기시 아래 메소드 호출. 같은 ID의 데이터셋이 있을 경우 overwrite할 것인이 confirm창 띄움
       * 업데이트시 업데이트가 필요한 모든 리스트를 한꺼번에 보내는데,
       * 서버 API에서는 기존에 없던 데이터를 보내면 데이터셋 추가, 기존에 있던(동일 ID)를 보내면 업데이트 처리가 됨
       * TODO : 데이터셋 이름이 같은 경우 시나리오X
       */
      public mergeDatasetList(ary: Array<DatasetItemDTO>) {
            var locale_msg = Vue.$i18n.messages.wv;
            let list: Array<DatasetItemDTO> = ary.concat();
            let addList: Array<DatasetItemDTO> = [];
            let isConfirmOverride: boolean = null;

            for (let i: number = 0, len: number = list.length; i < len; i++) {
                  let dataset: DatasetItemDTO = list[i];
                  let findDataset = this._datasetList.find(x => x.dataset_id == dataset.dataset_id);
                  if (findDataset) {
                        //같은 아이디의 데이터셋이 있다면 덮어 쓸것인지 최초 1회만 묻고 결과에 따라 처리
                        if (isConfirmOverride === null) {
                              if (confirm(locale_msg.common.overwrite)) {
                                    isConfirmOverride = true;
                              } else {
                                    isConfirmOverride = false;
                              }
                        }

                        if (isConfirmOverride) {
                              addList.push(dataset);
                        }
                  } else {
                        addList.push(dataset);
                  }
            }

            if (!addList.length) {
                  return;
            }

            addList = addList.filter((x) => {
                  if (x.name && x.data_type && x.delivery_type && x.dataset_id) {
                        return x;
                  }
            });

            this.updateDatasetList(addList);
      }

      /*******************************
       * TIM
       * ****************************/

      /*
     * TIM URL List는 json파일로 관리되는데, 최초에는 json파일이 없으므로 무조건 error결과를 받음
     * 이 에러 메세지가 이유없이 띄워져서 메세지 출력부분을 삭제함
     */

      /*환경설정에서 등록한 TIM Worker List를  조회할 수 있는 URL 리스트를 조회(json)*/
      public loadTimUrlList() {
            var locale_msg = Vue.$i18n.messages.wv;
            datasetApi.loadTimUrlList().then((result) => {
                  if (result.result == "ok") {
                        let data = JSON.parse(result.data);
                        if (data && data.data) {
                              this._timUrlList = data.data;
                        } else {
                              this._timUrlList = [];
                        }

                        this.sendNotification(DatasetProxy.NOTI_LOAD_TIM_URL_LIST, this._timUrlList);
                  } else {
                        //Vue.$message.error(locale_msg.common.errorLoad + locale_msg.common.retry);
                  }
                  return result;
            }, (error) => {
                  //Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  return error;
            });
      }


      /*환경설정에서 TIM Worker List 변경시 호출*/
      public updateTimUrlList(list: Array<TimItemDTO>) {
            var locale_msg = Vue.$i18n.messages.wv;
            datasetApi.updateTimUrlList(list).then((result) => {
                  if (result.result == "ok") {
                        this._timUrlList = list;
                        this.sendNotification(DatasetProxy.NOTI_UPDATE_TIM_URL_LIST, this._timUrlList);
                  } else {
                        Vue.$message.error(locale_msg.common.errorSave + locale_msg.common.retry);
                  }

                  return result;
            }, (error) => {
                  Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  return error;
            });
      }

      /**환경설정에서 등록힌 TIM Worker List중 선택한 항목의 URL로 Worker 조회*/
      public loadTimWorkList(url: string): Promise<any> {
            return datasetApi.loadTimWorkList(url).then((result) => {
                  return result;
            }, (error) => {
                  let locale_msg = Vue.$i18n.messages.wv;
                  Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  console.error("RENOBIT datasetApi.loadTimWorkList() ", error);
                  return Promise.reject(error);
            });
      }

      public getTimUrlList(): Array<TimItemDTO> {
            return this._timUrlList;
      }

      public addDataSource(data: DataSourceDTO) {
            let locale_msg = Vue.$i18n.messages.wv;
            datasetApi.addDataSource(data).then((result) => {
                  this.loadDataSource()
                  Vue.$message(locale_msg.common.successAdd);
                  return result;
            }, (error) => {
                  //Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  return error;
            });
      }

      public loadDataSource() {
            datasetApi.getDatasourceList().then((result) => {
                  if (result.result == "ok") {
                        this._dataSourceList = result.data;
                        this.sendNotification(DatasetProxy.NOTI_LOAD_DATA_SOURCE, this._dataSourceList);
                  } else {
                  }
            })
      }

      public loadDataSourceFromQueryTypeField() {
            return datasetApi.getDatasourceList().then((result) => {
                  if (result.result == "ok") {
                        this._dataSourceList = result.data;
                        return  this._dataSourceList;
                  } else {
                  }
            })
      }

      public removeDataSource(item:DataSourceDTO) {
            let locale_msg = Vue.$i18n.messages.wv;
            datasetApi.removeDataSource(item).then((result) => {
                  this.loadDataSource()
                  Vue.$message(locale_msg.common.successDelete);
                  return result;
            }, (error) => {
                  //Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  return error;
            });
      }

      public testDataSource(item:DataSourceDTO) {
            let locale_msg = Vue.$i18n.messages.wv;
            datasetApi.testDataSource(item).then(result=>{
                  if(result.data && result.data[0] == 'error') {
                        Vue.$message.error(result.data[1]);
                        console.error(result.data[1]);
                  } else {
                        Vue.$message(locale_msg.common.successExecute);
                        this.sendNotification(DatasetProxy.NOTI_TEST_DATA_SOURCE, result);
                  }
            }, (error) => {
                  //Vue.$message.error(locale_msg.common.errorMsg + locale_msg.common.retry);
                  return error;
            });
      }

      public loadDynamicSQL(dataSouraces: string) {
            return datasetApi.loadDynamicSQL(dataSouraces).then(result => {
                  return result;
            }, error => {
                  return error;
            });
      }

      public getDataSourceList() {
            return this._dataSourceList;
      }
}
