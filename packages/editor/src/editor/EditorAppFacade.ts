import * as puremvc from "puremvc";
import StartUpCommand from "./controller/StartUpCommand";
import EditorMainComponent from "./view/EditorMainComponent.vue";
import ArchitectStatic from "./controller/architect/ArchitectStatic";

export default class EditorAppFacade extends puremvc.Facade {
    public static CMD_STARTUP: string = 'command/startup';
    public static NAME:string = "EDITOR_APP";
    constructor(key:string){
        super(key);
    }

    /** @override */
    initializeController() {
        super.initializeController();
        this.registerCommand(EditorAppFacade.CMD_STARTUP, StartUpCommand);


    }

    /** @override */
    initializeModel() {
        super.initializeModel();
    }

    /** @override */
    initializeView() {
        super.initializeView();
    }

    /*

     */
    startup(app:EditorMainComponent) {
        console.log("step 02, startup() 메서드 호출 ");
        this.sendNotification(EditorAppFacade.CMD_STARTUP, app);
        // 전역 변수 등록을 시작으로!
        this.sendNotification(ArchitectStatic.CMD_REGISTER_GLOBAL_VARIABLE_EDITOR, app);
    }

    static getInstance(multitonKey:string):EditorAppFacade {
        const instanceMap:any = puremvc.Facade.instanceMap;
        if (!instanceMap[multitonKey]) {
            instanceMap[multitonKey] = new EditorAppFacade(multitonKey);
            console.log("step 02, EditorAppFacade 인스턴스 생성");
        }
        return instanceMap[multitonKey] as EditorAppFacade;
    }
}
