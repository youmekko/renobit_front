import * as puremvc from "puremvc";
import CommonStatic from "./CommonStatic";
import Vue from "vue";
import FPSManager from "../../../wemb/wv/managers/FPSManager";

/*
페이지 권한이 없는 경우
 */
export class ActiveFPSCommand  extends puremvc.SimpleCommand{
    execute( note:puremvc.INotification )
    {
      FPSManager.getInstance().toggle();
    }


}
