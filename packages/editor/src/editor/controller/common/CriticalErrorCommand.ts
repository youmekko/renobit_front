import * as puremvc from "puremvc";
import CommonStatic from "./CommonStatic";
import Vue from "vue";


export class CriticalErrorCommand  extends puremvc.SimpleCommand{
    execute( note:puremvc.INotification )
    {
        console.log("CriticalErrorCommand 에러 페이지로 이동", note.getBody());
    }


}
