import * as puremvc from "puremvc";
import CommonStatic from "./CommonStatic";
import Vue from "vue";

/*
페이지가 없는 경우
 */
export class Critical404ErrorCommand  extends puremvc.SimpleCommand{
    execute( note:puremvc.INotification )
    {

          alert(note.getBody());
          location.href=window.wemb.configManager.serverUrl+"/404Error.do";
    }
}
