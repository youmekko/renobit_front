export default class ArchitectStatic {
	/*
	실행순서
	CMD_REGISTER_GLOBAL_VARIABLE_EDITOR
	CMD_READY_EDITOR
	CMD_START_EDITOR

	 */
	public static CMD_REGISTER_GLOBAL_VARIABLE_EDITOR: string = "command/globalVariableEditor";
	public static CMD_READY_EDITOR: string = "command/readyEditor";
	public static CMD_START_EDITOR: string = "command/startEditor";


}