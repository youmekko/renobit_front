import * as puremvc from "puremvc";
import Vue from "vue";
import EditorStatic from "../editor/EditorStatic";
import EditorProxy from "../../model/EditorProxy";

import CommanderStatic from "../common/CommonStatic";
import pageApi from "../../../common/api/pageApi";

export class StartEditorCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            var locale_msg = Vue.$i18n.messages.wv;
		console.log("\n\n\n\n\n############################################################\nstep 04, StartEditorCommand 실행", "초기 페이지 읽기 시작");
		console.log("4-1. 전체 페이지 개수 = ", window.wemb.pageManager.getPageLength())


            let loading;

		if (window.wemb.pageManager.getPageLength() == 0) {
                  /* [RNB-29] 초기 페이지 생성 */
                  Vue.$confirm(locale_msg.page.initSamplePage, "", {
                        confirmButtonText: 'OK',
                        cancelButtonText: 'Cancel',
                        type: 'info'
                  }).then(()=>{
                        loading = Vue.$loading({
                              lock: true,
                              text: 'Loading',
                              spinner: 'el-icon-loading',
                              background: 'rgba(255, 255, 255, 0.7)'
                        });

                        return pageApi.restoreSamplePage()
                  }).then(({ data }) => {
                        loading.close();
                        if (data.result === "SUCCESS") {
                              location.reload()
                        }
                  }).catch(err => {
                        alert('Failed to get sample pages.');
                        console.log('init sample page error', err);
                  });

		}else {
			/*
			 두 가지 경우
			 1. 페이지가 없는 경우

			 2. 페이지가 있는 경우
			 */
                  let pageID:string = "";
                  let pageName:string="";
                  console.log("4-1. window.wemb.configManager.startPageName = ", window.wemb.configManager.startPageName)

                  if(window.wemb.configManager.startPageName.length>0){

				// 페이지 열기
                        pageID=window.wemb.pageManager.getPageIdByName(window.wemb.configManager.startPageName);
                        console.log("4-2. 시작페이지가 있는 경우 = ",pageID,  window.wemb.configManager.startPageName);
                  }else {
                        pageID = window.wemb.configManager.getStartPageId();
                        // 서버에 시작 페이지가 설정되지 않은 경우 0번째 페이지로 선택
                        if(pageID==null){
                              pageID= window.wemb.pageManager.getPageIdByIndex(0);
                        }

                        if(pageID!=null) {
                              pageName = window.wemb.pageManager.getPageInfoBy(pageID).name;
                        }
                        console.log("4-3. 시작페이지가 없는 경우 tree의 0번째 페이지 읽기 = ", pageID, pageName);
                  }


                  if(pageID==null){
                        // 2018.11.17(ckkim) 화면을 뒤덮는 로딩 효과 숨기기
                        //window.wemb.$defaultLoadingModal.hide();
                        this.sendNotification(CommanderStatic.CMD_CRITICAL_404_ERROR_COMMAND, "페이지가 존재하지 않습니다.");
				return;
                  }

                  this.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageID);
		}

	}
}
