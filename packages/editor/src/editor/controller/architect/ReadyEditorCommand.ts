import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";
import ArchitectStatic from "./ArchitectStatic";
import pageApi from "../../../common/api/pageApi";
import {event} from "../../../wemb/wv/events/LoadingEvent";
import LoadingEvent = event.LoadingEvent;
import PublicProxy from "../../model/PublicProxy";
import {ProjectInfoData} from "../../../wemb/wv/page/Page";
import StageProxy from "../../model/StageProxy";
import {EDITOR_APP_STATE} from "../../../wemb/wv/data/Data";
import EditorStatic from "../editor/EditorStatic";


export class ReadyEditorCommand extends puremvc.SimpleCommand {
	private editorProxy: EditorProxy;
	private publicProxy:PublicProxy;
	private stageProxy: StageProxy;

	execute(note: puremvc.INotification) {
		console.log("step 03, ReadyEditorCommand, 1. 준비 시작");

		this.editorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		this.publicProxy = <PublicProxy>this.facade.retrieveProxy(PublicProxy.NAME);
		this.stageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);

		this.editorProxy.updateAppState(EDITOR_APP_STATE.READY_START);
		window.wemb.$defaultLoadingModal.setMaxLength(4);
		window.wemb.$defaultLoadingModal.show(window.wemb.$defaultLoadingModal.$i18n.messages.wv.common.preparing);
		this._startMasterPageInfoLoad();

	}

	_startMasterPageInfoLoad() {
		/*
		마스터 페이지 정보 읽기
			- 페이지 리스트 정보
			- 스테이지 정보
			- 마스터 정보
				- 마스터 프로퍼티 정보
				- 마스터 레이어 정보
		 */
		console.log("step 03, ReadyEditorCommand, 2. 마스터 정보 읽기 시작");
		pageApi.loadMasterPageInfo().then((projectInfo:ProjectInfoData)=>{
			/*
			 json 페이지 정보를 파싱해서  PageInfo 정보로 만들기.
			 */
			window.wemb.pageManager.attachJsonPageInfoListToPageInfoList(projectInfo.page_list);


                  this.stageProxy.setMasterBackgroundInfo(projectInfo.master_info.background);
			this.stageProxy.setStageBackgroundInfo(projectInfo.stage_info.background);

			// 마스터 레이어 정보 설정하기
			this.publicProxy.setComInstanceInfoListInMasterLayer(projectInfo.master_info.master_layer);

			window.wemb.$defaultLoadingModal.setProgress(1);
			window.wemb.$defaultLoadingModal.addMessage("페이지 정보 로딩 완료");
			this._startPageTreeInfoLoad();
		})
	}


	_startPageTreeInfoLoad() {
		console.log("step 03, ReadyEditorCommand, 3.트리 정보 읽기 시작");
		window.wemb.$defaultLoadingModal.addMessage("페이지 트리정보 로딩 시작",false);

		window.wemb.pageTreeDataManager.startLoading().then((success) => {
			window.wemb.$defaultLoadingModal.setProgress(2);
			window.wemb.$defaultLoadingModal.addMessage("페이지 트리정보 로딩 완료");
			this._startMainMenuInfoLoad();
		})
	}


	_startMainMenuInfoLoad() {
		console.log("step 03, ReadyEditorCommand, 4.메인 메뉴 정보 로딩 시작");

		window.wemb.$defaultLoadingModal.addMessage("메인 메뉴 정보 로딩 시작",false);
		window.wemb.mainMenuManager.startLoading().then((success) => {
			window.wemb.$defaultLoadingModal.setProgress(3);
			window.wemb.$defaultLoadingModal.addMessage("메인 메뉴 정보 로딩 완료");
			this._startComponentPanelInfo();
		})
	}


      async _startComponentPanelInfo(){

            console.log("step 03, ReadyEditorCommand, 5.컴포넌트 패널 정보 로딩 시작");
            window.wemb.$defaultLoadingModal.addMessage("컴포넌트 패널 정보 로딩 시작",false);

            try {
                  let devComponentPanelInfoList = await window.wemb.componentLibraryManager.getComponentPanelInfoList();
                  window.wemb.$defaultLoadingModal.setProgress(4);
                  window.wemb.$defaultLoadingModal.addMessage("컴포넌트 패널 정보 로딩 완료");
                  this._startComponentLoad();

            }catch(error){

            }


      }

	_startComponentLoad() {

		console.log("step 03, ReadyEditorCommand, 6.컴포넌트 라이브러리 로딩 시작 ");

		window.wemb.$defaultLoadingModal.addMessage("컴포넌트 라이브러리 로딩 시작  ", false);


		window.wemb.componentLibraryManager.$on(LoadingEvent.LOAD_START, (event)=>{
			window.wemb.$defaultLoadingModal.addMessage("\t\t   -라이브러리 로딩 시작", false);
			window.wemb.$defaultLoadingModal.addMessage(`    -라이브러리 로딩 개수  ${event.data.loadLength}`, false);
		});

		window.wemb.componentLibraryManager.$on(LoadingEvent.PROGRESS, (event)=>{
			window.wemb.$defaultLoadingModal.addMessage(`    -${event.data.currentIndex}. ${event.data.componentInfo.name} 로딩 완료`, false);
		});

		window.wemb.componentLibraryManager.$on(LoadingEvent.COMPLETED, (event)=>{
			console.log("step 03, ReadyEditorCommand, 6.컴포넌트 라이브러리 로딩 완료 ");
			window.wemb.$defaultLoadingModal.completed();
			window.wemb.$defaultLoadingModal.addMessage("컴포넌트 라이브러리 로딩 완료");
                  console.log("########################################에디터 초기화 완료 ############################\n\n\n\n\n");

                  /*
                  2018.11.21(ckkim)

                  주의!!!!!!!!!!!!
                   RENOBIT 상태 업데이트 처리가 동적패널, 동적플러그인보다 앞서서 실행되면 안됨.

                   */

			// 동적으로 컴포넌트 패널 만들기.
                  this.sendNotification(EditorStatic.CMD_CREATE_DYNAMIC_PANEL);
                  // 동적으로 컴포넌트 패널 만들기.
                  this.sendNotification(EditorStatic.CMD_CREATE_DYNAMIC_PLUGIN);

                  /* 2018.12.19(ckkim)
                  확장요소(plugin,  panel)이 모두 로드 완료 된 후 실행
                   */
                  this.sendNotification(EditorStatic.CMD_COMPLETED_EXTENSION);


                  // 에디터 상태 업데이트 처리(동적 패널, 동적 플러그인을 먼저 실행 한 후 최종 상태를 변경해야함.
                  this.editorProxy.updateAppState(EDITOR_APP_STATE.READY_COMPLETED);

			// 에디터 시작
                  // 타이머를 사용하는 이유는 부드럽게 페이지/모달 전환 처리를 위해 사용했을뿐 다른 이유는 없음.
                  setTimeout(()=>{
                        // 2018.11.17(ckkim) 화면을 뒤덮는 로딩 효과 숨기기
                        window.wemb.$defaultLoadingModal.hide();
                        this.sendNotification(ArchitectStatic.CMD_START_EDITOR);
                  },500)


		});


		window.wemb.componentLibraryManager.startLoading();
	}

}
