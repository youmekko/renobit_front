import * as puremvc from "puremvc";
import Vue from "vue";

import ArchitectStatic from "./ArchitectStatic";
import ILoadingModal from "../../../common/modal/ILoadingModal";
import EditorAppFacade from "../../EditorAppFacade";
import IMediator = puremvc.IMediator;
import EditAreaMainContainerMediator from "../../view/work-area/EditAreaMainContainerMediator";
import EditorProxy from "../../model/EditorProxy";
import { SessionScheduler } from "../../../wemb/wv/managers/SessionScheduler";
import {CONFIG_MODE} from "../../../wemb/wv/data/Data";
import CommanderStatic from "../common/CommonStatic";
import MenusetDataManager from "../../../wemb/wv/managers/MenusetDataManager";
import TemplateDataManager from "../../../wemb/wv/managers/TemplateDataManager";
export class RegisterGlobalVariableCommand extends puremvc.SimpleCommand {
	async execute(note: puremvc.INotification) {
		console.log("step 02, RegisterGlobalVariableCommand 실행", "전역 변수 등록");

		// 에디터 메인 컴포넌트(router정보등을 가지고 있음)
		let editorMainComponent:any = <Vue>note.getBody();
		// 환경설정 정보 설정하기
		// test, pid, layer 정보 설정하기
		window.wemb.configManager.initConfigValue(editorMainComponent.$route.query);
            // 라우터
		Vue.$router = editorMainComponent.$router;
		Vue.$route = editorMainComponent.$route;

		/*
		DefaultLoadingModal 대신 ILoadingModal을 사용하는 경우 에러
		 */
		window.wemb.$defaultLoadingModal = <ILoadingModal>editorMainComponent.$refs["defaultLoadingModal"];
            window.wemb.$editMainAreaResourceLoadingComponent = editorMainComponent.$refs["editMainAreaResourceLoadingComponent"];

            window.wemb.$createPageModal = editorMainComponent.$refs["createPageModal"];
            window.wemb.$referencePagesModal = editorMainComponent.$refs["referencePagesModal"];
		window.wemb.$importPagesModal = editorMainComponent.$refs["importPagesModal"];
		window.wemb.$exportPagesModal = editorMainComponent.$refs["exportPagesModal"];
            window.wemb.$resourceManagerModal = editorMainComponent.$refs["resourceManagerModal"];
            window.wemb.$restoreProjectModal = editorMainComponent.$refs["restoreProjectModal"];
            window.wemb.$menusetManagerModal = editorMainComponent.$refs["menusetManagerModal"];
            window.wemb.$templateManagerModal = editorMainComponent.$refs["templateManagerModal"];
		window.wemb.$exportResourceManagerModal = editorMainComponent.$refs["exportResourceManagerModal"];
            window.wemb.$modalManager = editorMainComponent.$refs["modalManager"];
            window.wemb.$productInfoModal = editorMainComponent.$refs["productInfoModal"];
            window.wemb.$shortcutsInfoModal = editorMainComponent.$refs["shortcutsInfoModal"];
            window.wemb.$threeAlignModal = editorMainComponent.$refs["threeAlignModal"];

		window.wemb.editorFacade = <EditorAppFacade>EditorAppFacade.getInstance(EditorAppFacade.NAME);
            window.wemb.currentFacade =window.wemb.editorFacade;
            window.wemb.editorProxy = window.wemb.editorFacade.retrieveProxy(EditorProxy.NAME) as EditorProxy;

            // 에디터 편집영역 main container
		let editAreaMainContainerMediator:IMediator = window.wemb.editorFacade.retrieveMediator(EditAreaMainContainerMediator.NAME);
            // 실제 레이어를 가지고 있는 container
		window.wemb.mainPageComponent =  editAreaMainContainerMediator.getViewComponent().mainPageComponent;


		/////////////////////
            // main 영역의 three 정보 설정하기
            // 이 값은 three component에 주입됨.

            /*
	            2018.05.08, ddandongne
	            3D layer 사용 유무를 config로 변경해야함.
		*/
            let mainThreeLayer = window.wemb.mainPageComponent.getMainThreeLayer();

            if(mainThreeLayer!=null){
                  if(mainThreeLayer.threeElements==null) {
                        alert(Vue.$i18n.messages.wv.etc.etc01);
                        return;
                  }
                  window.wemb.setThreeElements(mainThreeLayer.threeElements);

            }else {
                  console.log("RENOBIT INFO, Preferences without 3DLayer")
            }



		// 배경 처리용 요소들(스테이지 영역, 페이지 배경영역)
            window.wemb.stageBackgroundElement = editAreaMainContainerMediator.getViewComponent();

            window.wemb.pageBackgroundElement = window.wemb.mainPageComponent.bgLayer;

		// 키보드 단축키 관리자 실행(내부에서 command등을 실행하기 때문에 facade를 넘겨줘야함.
		window.wemb.keyboardCommandManager.start(window.wemb.editorFacade);

		if(window.wemb.configManager.configMode == CONFIG_MODE.LOCAL) {
                  let result = await window.wemb.userInfo.setUserConfigInfo();
                  if(!result) {
                        alert("setUserConfigInfo error");
                        return;
                  }

                  /*[ 에디터 권한 여부 판별 ]*/
                  // 추후 개인별 에디터 권한 여부 판별로 변경 예정
                  if(!wemb.userInfo.hasAdminRole) {
                        this.sendNotification(CommanderStatic.CMD_CRITICAL_404_ERROR_COMMAND, "할당된 에디터 권한이 없습니다.");
                        return;
                  }
            }

            let menusetLoadSuccess = await MenusetDataManager.getInstance().startLoading();
            if(menusetLoadSuccess==false){
                  console.warn("RENOBIT RegisterGlobalVariableCommand, Loading menu set information failed.");
            }


            let templateLoadSuccess = await TemplateDataManager.getInstance().startLoading();
            if(templateLoadSuccess==false){
                  console.warn("RENOBIT RegisterGlobalVariableCommand, Loading template manager information failed.");
            }

            SessionScheduler.getInstance().setSessionTime(wemb.configManager.sessionTime);

		// 에디터 시작
		this.sendNotification(ArchitectStatic.CMD_READY_EDITOR);
	}

}


