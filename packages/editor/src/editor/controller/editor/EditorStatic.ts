import {ChangeThreeTrasnformControlModeCommand} from "./ChangeThreeTrasnformControlModeCommand";
import {ShowExtensionPopupCommand} from "./ShowExtensionPopupCommand";
import EditorProxy from "../../model/EditorProxy";

export default class EditorStatic {
	public static readonly  CMD_OPEN_PAGE:string = "command/openPage";
	public static readonly  CMD_OPEN_PAGE_BY_ID:string = "command/openPageById";
      public static readonly  CMD_CLOSE_PAGE:string = "command/closePage";


	public static readonly  CMD_CHANGE_ACTIVE_LAYER:string = "command/changeActvieLayer";
      public static readonly  CMD_CHANGE_PAGE_NAME:string = "command/changePageName";
	public static readonly  CMD_ADD_COM_INSTANCE:string = "command/addComponentInstance";
	public static readonly  CMD_ADD_COM_INSTANCE_BY_NAME:string = "command/addComponentInstanceByName";
	public static readonly  CMD_REMOVE_COM_INSTANCE:string = "command/removeComponentInstance";
	public static readonly  CMD_REMOVE_SELECTED_COM_INSTANCE:string = "command/removeSelectedComponentInstance";

	public static readonly  CMD_NEW_SELECTED_COMPONENT_INSTANCE:string = "command/newSelectedComponentInstance";
	public static readonly  CMD_ADD_SELECTED:string = "command/addSelected";
	public static readonly  CMD_ADD_ALL_SELECTED:string = "command/addAllSelected";
	public static readonly  CMD_ADD_SELECTED_LIST:string = "command/addSelectedList";
	public static readonly  CMD_REMOVE_SELECTED:string = "command/removeSelected";

	public static readonly  CMD_CLEAR_ALL_SELECTED:string = "command/clearAllSelected";




	public static readonly  CMD_COPY_SELECTED_COM_INSTANCE:string = "command/copySelectedComponentInstance";
	public static readonly  CMD_CUT_SELECTED_COM_INSTANCE:string = "command/cutSelectedComponentInstance";
	public static readonly  CMD_PASTE_COM_INSTANCE:string = "command/pasteComponentInstance";


	public static readonly  CMD_SYNC_LOCATION_AND_SIZE_ELEMENT_PROPS_TO_COMPONENT_PROPS:string = "command/locationAndSizeElementPropsToCompoentProps";
      public static readonly  CMD_SYNC_REAL_CAMERA_POSITION_TO_COMPONENT_POSITION:string = "command/SyncRealCameraPositionToComponentPositionCommand";


      public static readonly  CMD_ALIGN_SELECTED_COM_INSTANCE:string = "command/alignSelecedComponentInstance";
      public static readonly  CMD_ARRANGE_SELECTED_COM_INSTANCE:string = "command/arrangeSelecedComponentInstance";
      public static readonly  CMD_SIZE_SELECTED_COM_INSTANCE:string  = "command/sizeSelectedComponentInstance";
      public static readonly  CMD_DISTRIBUTE_SELECTED_COM_INSTANCE:string  = "command/distributeSelectedComponentInstance";




      public static readonly  CMD_SET_MODIFY_STATE:string  = "command/setModifyState";
      public static readonly  CMD_SET_ACTIVE_SHORT_CUT_STATE:string  = "command/setActiveShortCutState";



	public static readonly  SELECTED_CLASS_NAME:string = "wv-component-selected";
	public static readonly  LOCK_CLASS_NAME:string = "lock";









	public static readonly CMD_SAVE_PAGE:string = "command/savePage";

      public static readonly CMD_SAVE_AS_PAGE:string = "command/saveAsPage";
      public static readonly CMD_DELETE_PAGE_TREE_ITEMS:string = "command/deletePageTreeItems";
      public static readonly CMD_DELETED_CURRENT_PAGE:string = "command/deletedCurrentPage";

      public static readonly CMD_NEW_PAGE_SAVE:string = "command/newPageSave";

      public static readonly CMD_NEW_PAGE:string = "command/newPage";
      public static readonly CMD_NEW_PAGE_GROUP:string = "command/newPageGroup";
      public static readonly CMD_SHOW_NEW_PAGE_MODAL:string = "command/showNewPageModal";
      public static readonly CMD_SHOW_SAVE_AS_PAGE_MODAL:string = "command/showSaveAsPageModal";


      public static readonly CMD_EXPORT_PAGE:string = "command/exportPage";
      public static readonly CMD_IMPORT_PAGE:string = "command/importPage";
      public static readonly CMD_IMPORT_PAGES:string = "command/importPages";
      public static readonly CMD_UPDATE_MASTER_INFO:string = "command/updateMasterInfo";
      public static readonly CMD_SHOW_EXPORT_PAGES_MODAL:string = "command/showExportPagesModal";

      public static readonly CMD_EXPORT_RESOURCES:string = "command/exportResources";
      public static readonly CMD_IMPORT_RESOURCES:string = "command/importResources";

      public static readonly CMD_SHOW_EXPORT_RESOURCES_MANAGER:string = "command/showExportResourcesManager";

      public static readonly CMD_PROJECT_RESTORE:string = "command/projectRestore";
      public static readonly CMD_PROJECT_BACKUP:string = "command/projectBackup";

      public static readonly CMD_SHOW_PRODUCT_INFO:string = "command/showProductInfo";
      public static readonly CMD_SHOW_SHORTCUTS_INFO:string = "command/showShortcutsInfo";


      public static readonly CMD_SHOW_RESOURCE_MANAGER:string = "command/showResourceManager";
      public static readonly CMD_SHOW_MENUSET_MANAGER:string = "command/showMenusetManager";
      public static readonly CMD_SHOW_TEMPLATE_MANAGER:string = "command/showTemplateManager";
      public static readonly CMD_SHOW_MANUAL:string = "command/showManual";

      public static readonly CMD_SHOW_WSCRIPT_EDITOR:string = "command/showWScriptEditor";
      public static readonly CMD_SHOW_DATATSET_MANAGER:string = "command/showDatasetManager";
      public static readonly CMD_SHOW_MULTILINGUAL_MANAGER:string = "command/showMultilingual";
      public static readonly CMD_OPEN_POPUP:string = "command/openPopupUrl"



      public static readonly  CMD_GOTO_VIEWER_BY_NAME:string="command/gotoViewerByName";
      public static readonly  CMD_GOTO_VIEWER_BY_ID:string="command/gotoViewerById";
      public static readonly  CMD_GOTO_VIEWER_BY_CURRENT_PAGE_NAME:string="command/gotoViewerByCurrentPageName";
    public static readonly  CMD_GOTO_ADMIN:string="command/gotoAdmin";
    public static readonly  CMD_GOTO_LOGOUT:string="command/gotoLogout";
    public static readonly  CMD_CHANGE_LOCALE:string="command/changeLocale";




    public static readonly  CMD_MOVE_SELECTED_COMPONENT_INSTANCE="command/moveSelectedComponentInstance";


      public static readonly  CMD_CHANGE_THREE_TRANSFORM_CONTROL_MODE="command/changeThreeTransformControlMode";



    /*
    관리자용임.
     */



      public static readonly  CMD_RESET_PAGE_LIST="command/resetPageList";


      public static readonly CMD_TOGGLE_LEFT_ASIDE_VISIBLE="command/toggleLeftAsideVisible";
      public static readonly CMD_TOGGLE_RIGHT_ASIDE_VISIBLE="command/toggleRightAsideVisible";

      public static readonly  CMD_SHOW_EXTENSION_POPUP="command/showExtensionPopup";




      public static readonly CMD_CALCULATE_SELECTED_COMPONENT_INSTANCE_IN_DRAG_AREA="command/calculateSelectedComponentInstanceInDragArea";

      public static readonly  CMD_CREATE_DYNAMIC_PANEL="command/createDynamicPanel";
      public static readonly  CMD_CREATE_DYNAMIC_PLUGIN="command/createDynamicPlugin";



      public static readonly CMD_SAVE_DATA_TO_PAGE:string = "command/saveDataToPage";



      /* 2018.09.20(ckkim) */
      public static readonly  CMD_TOGGLE_DIRECTION_VIEW:string="command/toggleDirectionView";
      public static readonly  CMD_TOGGLE_GRID_HELPER:string="command/toggleGridHelper";



      public static readonly  CMD_SHOW_EXTERNAL_MANAGER:string="command/showExternalManager";

      /*
      2018.11.10(ckkim)
      동적 모달 활성화
       */
      public static readonly  CMD_SHOW_MODAL:string ="command/showModal";


      public static readonly  CMD_EXECUTE_SCRIPT:string ="command/executeScript";




      /*
      2018.12.19(ckkim)
      plugin, panel 정보가 모두 생성 완료된 상태
       */

      public static readonly  CMD_COMPLETED_EXTENSION:string ="command/completedExtension";


      // 2019.02.12 추가
      public static readonly CMD_SHOW_THREE_ALIGN_MODAL:string = "command/showThreeAlignModal";

}

/*
2018.08.25
      - 자바스크립트에서도 사용할 숭 ㅣㅆㄲ ㅔ추가
 */
window.EditorStatic = EditorStatic;
