import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import WVComponent from "../../../wemb/core/component/WVComponent";
import EditorStatic from "./EditorStatic";
import EditorProxy from "../../model/EditorProxy";

// 사용않함(2018.12.02), 삭제 예정
export class RemoveSelectedCommand extends puremvc.SimpleCommand {

	/*
		신규 선택시

		1. 레이어 선택
	 */
	execute(note: puremvc.INotification) {
	      console.log("@@ CHECK RemoveSelectedCommand");
		// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
		let proxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
		let instance:WVComponent = <WVComponent>note.getBody();


		// 선택 제거
		proxy.removeSelectedInstance(instance);

		// 선택이 없는 경우 clearAll 처리 한다.
		if(proxy.currentPropertyManager.getInstanceLength()==0){
			this.sendNotification(EditorStatic.CMD_CLEAR_ALL_SELECTED);
		}

		proxy.updateSelectedInfo();
            this.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE)
	}
}
