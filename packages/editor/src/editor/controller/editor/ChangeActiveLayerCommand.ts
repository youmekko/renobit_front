import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";
import SelectProxy from "../../model/SelectProxy";
import {WORK_LAYERS} from "../../../wemb/wv/layer/Layer";



export class ChangeActiveLayerCommand extends puremvc.SimpleCommand {


	execute(note: puremvc.INotification) {

		// 레이어 활성화
		let proxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            proxy.setActiveLayerByName(note.getBody())


		// 선택 프락시 취소 처리
		let selectedProxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
		console.log("### 기존 선택 정보 제거");
		selectedProxy.clearAllSelectedInstance();
		console.log("### 선택 정보 처리, 배경레이어 선택");
		selectedProxy.setSelectedLayer(WORK_LAYERS.BG_LAYER);
		console.log("### 선택 정보 처리, 선택 정보 업데이트 메서드 실행");
		selectedProxy.updateSelectedInfo();

	}
}
