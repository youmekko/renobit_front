import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import WVComponent from "../../../wemb/core/component/WVComponent";
import {WORK_LAYERS} from "../../../wemb/wv/layer/Layer";
import EditorProxy from "../../model/EditorProxy";


export class AddSelectedCommand extends puremvc.SimpleCommand {

	/*
		신규 선택시

		1. 레이어 선택
	 */
	execute(note: puremvc.INotification) {
		// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
		let proxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
		let instance:WVComponent = <WVComponent>note.getBody();
		let layerName = <WORK_LAYERS>instance.layerName;

		// 레이어 변경 처리
		/*
		추가 선택 인스턴스가 0에서 1로 늘어나는 경우 레이어를 변경처리 해야함.
		 */
		if(layerName!=proxy.currentPropertyManager.layerName){
			proxy.setSelectedLayer(layerName);
		}

		// 선택 추가
		proxy.addSelectedInstance(instance);
		proxy.updateSelectedInfo();

            this.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE)
	}
}
