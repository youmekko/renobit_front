import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import {Rectangle} from "../../../wemb/core/geom/interfaces/GeomInterfaces";
import EditorProxy from "../../model/EditorProxy";
import WV2DComponent from "../../../wemb/core/component/2D/WV2DComponent";


export class CalculateSelectedComponentInstanceInDragAreaCommand extends puremvc.SimpleCommand {

	/*
		신규 선택시

		1. 레이어 선택
	 */
	execute(note: puremvc.INotification) {
		// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
		let editorProxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		let area:Rectangle = <Rectangle>note.getBody();
            let comInstanceListAry = editorProxy.activeLayerInfo.instanceList;

            let selectedComInstanceListAry = [];
            if(comInstanceListAry.length>0) {

                  comInstanceListAry.forEach((instance:WV2DComponent) => {
                        // 안보이는 객체는 선택에서 제외
                        if (!instance.visible) {
                              return;
                        }
                        if (area.x <= instance.x && area.y <= instance.y) {
                              let right = instance.x + instance.width;
                              let bottom = instance.y + instance.height;
                              if (area.x + area.width >= right && area.y + area.height >= bottom) {
                                    selectedComInstanceListAry.push(instance);
                              }
                        }
                  });

                  let selectProxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
                  if(selectedComInstanceListAry.length>0) {
                        // 레이어 변경 처리
                        /*
				추가 선택 인스턴스가 0에서 1로 늘어나는 경우 레이어를 변경처리 해야함.
				 */
                        if (editorProxy.activeLayerInfo.name != selectProxy.currentPropertyManager.layerName) {
                              selectProxy.setSelectedLayer(editorProxy.activeLayerInfo.name);
                        }

                        selectedComInstanceListAry.forEach((instance)=>{
                              // 선택 추가
                              selectProxy.addSelectedInstance(instance);
                        })

                        selectProxy.updateSelectedInfo();
                  }
                  this.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE);
            }


	}
}
