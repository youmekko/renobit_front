import * as puremvc from "puremvc";
import WVComponent from "../../../wemb/core/component/WVComponent";
import EditorStatic from "./EditorStatic";

/*
2018.11.27(ckkim)
정리
 */
export class ExecuteScriptCommand extends puremvc.SimpleCommand {

      execute(note: puremvc.INotification) {
            let script = note.getBody().script;
            eval(script);
      }
}
