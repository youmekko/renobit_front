import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import ComponentSizeHelper from "../../../wemb/wv/helpers/ComponentSizeHelper";


export class SizeSelectedComponentInstanceCommand extends puremvc.SimpleCommand {

    execute(note: puremvc.INotification) {
        console.log("SizeSelectedComponentInstanceCommand ");
        let selectProxy: SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
        if (selectProxy.currentPropertyManager.getInstanceLength() > 0) {
            ComponentSizeHelper.execute(note.getBody(), selectProxy.currentPropertyManager.getInstanceList());

            this.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);
        } else {
            console.log("복사 내용이 없음. ")
        }
    }
}
