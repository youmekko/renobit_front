import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import EditorProxy from "../../model/EditorProxy";
import ComponentArrangeHelper from "../../../wemb/wv/helpers/ComponentArrangeHelper";


export class ArrangeSelectedComponentInstanceCommand extends puremvc.SimpleCommand {

    execute(note: puremvc.INotification) {
        console.log("AlignSelectedComponentInstanceCommand ");
        let selectProxy: SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
        let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);

        console.log("selectProxy.currentPropertyManager.getInstanceLength()", selectProxy.currentPropertyManager.getInstanceLength());

        if (selectProxy.currentPropertyManager.getInstanceLength() > 0) {
            ComponentArrangeHelper.execute(note.getBody(), selectProxy.currentPropertyManager.getInstanceList(), editorProxy.activeLayerInfo.instanceList);
        } else {
            console.log("복사 내용이 없음. ")
        }
    }
}
