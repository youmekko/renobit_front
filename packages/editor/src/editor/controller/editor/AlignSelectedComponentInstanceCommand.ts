import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import ComponentAlignHelper from "../../../wemb/wv/helpers/ComponentAlignHelper";
import {IWVComponent} from "../../../wemb/core/component/interfaces/ComponentInterfaces";
import EditorStatic from "./EditorStatic";


export class AlignSelectedComponentInstanceCommand extends puremvc.SimpleCommand {

    execute(note: puremvc.INotification) {
        console.log("AlignSelectedComponentInstanceCommand ");
        let selectProxy: SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
        let selectedList: Array<IWVComponent> = selectProxy.selectedTwoThreeMasterComInstanceListList;
        if (selectedList.length > 0) {
            ComponentAlignHelper.execute(note.getBody(), selectedList);

            this.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);
        } else {
            console.log("복사 내용이 없음. ")
        }
    }
}
