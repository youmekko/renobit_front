import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import {WVCOMPONENT_CATEGORY} from "../../../wemb/core/component/interfaces/ComponentInterfaces";


export class MoveSelectedComponentInstanceCommand extends puremvc.SimpleCommand {

	execute(note: puremvc.INotification) {

            let selectProxy: SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);


            if (selectProxy.currentPropertyManager.getInstanceLength() > 0) {

                  let editableProperties = selectProxy.editableProperties;
                  let info = note.getBody();

                  if(editableProperties.info.category==WVCOMPONENT_CATEGORY.TWO){
                        let value = parseFloat(editableProperties.setter[info.propertyName])+info.step;
                        selectProxy.setGroupPropertyValue("setter", info.propertyName, value);
                  }else {
                        let position = editableProperties.setter.position;
                        position[info.propertyName]=parseFloat(position[info.propertyName])+info.step;
                        selectProxy.setGroupPropertyValue("setter", "position", position);
                  }



            } else {
                  console.log("선택 내용이 없음. ")
            }



	}
}
