import * as puremvc from "puremvc";
import WVComponent from "../../../wemb/core/component/WVComponent";
import EditorStatic from "./EditorStatic";


export class ShowExtensionPopupCommand extends puremvc.SimpleCommand {

      execute(note: puremvc.INotification) {

            let componentInstance:WVComponent = note.getBody() as WVComponent;

            console.log('componentInstance',componentInstance);

            if(componentInstance){
                  let componentName:string = componentInstance.componentName;

                  if(componentInstance.getExtensionProperties()==true){
                        const componentPath = window.wemb.componentLibraryManager.getComponentPath(componentInstance.componentName);
                        const defaultModalPath = `${componentPath}/popup.vue`;
                        const { modalPath } = componentInstance.info;
// 컴포넌트 모달 띄우기
                        window.wemb.$modalManager.loadModal(componentName, modalPath ? modalPath : defaultModalPath).then((result:any)=>{
                              this.sendNotification(EditorStatic.CMD_SET_ACTIVE_SHORT_CUT_STATE, false)
                              result.modal.setComponent(componentInstance);
                              result.modal.show();

                              result.modal.$on("closed", (updateState)=>{
                                    this.sendNotification(EditorStatic.CMD_SET_ACTIVE_SHORT_CUT_STATE, true)
                              })

                        }).catch ((error)=>{
                              console.log("errror", error);
                        });
                  }
            }



      }
}
