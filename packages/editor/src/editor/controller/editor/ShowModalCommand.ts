import * as puremvc from "puremvc";
import WVComponent from "../../../wemb/core/component/WVComponent";
import EditorStatic from "./EditorStatic";

/*
2018.11.05(ckkim)
정리
 */
export class ShowModalCommand extends puremvc.SimpleCommand {

      execute(note: puremvc.INotification) {
            let info = note.getBody();
            window.wemb.$modalManager.loadModal(info.modalName, info.fileName).then((result:any)=>{
                  //
                  this.sendNotification(EditorStatic.CMD_SET_ACTIVE_SHORT_CUT_STATE, false);


                  if(info.opener)
                        result.modal.setOpener(info.opener);
                  if(info.data)
                        result.modal.setData(info.data);

                  if(info.closed)
                        result.modal.setClosedCallback(info.closed);


                  result.modal.show();

                  result.modal.$on("closed", (updateState)=>{
                        this.sendNotification(EditorStatic.CMD_SET_ACTIVE_SHORT_CUT_STATE, true)
                  })

            }).catch ((error)=>{
                  console.log("errror", error);
            });
      }
}
