import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import EditorProxy from "../../model/EditorProxy";


export class  AddAllSelectedCommand extends puremvc.SimpleCommand {

	/*
		신규 선택시

		1. 레이어 선택
	 */
	execute(note: puremvc.INotification) {


		let editorProxy:EditorProxy = <EditorProxy> this.facade.retrieveProxy(EditorProxy.NAME);
		// 활성화된 레이어에서 인스턴스 목록 구하기
		let instanceList = editorProxy.activeLayerInfo.instanceList;

		if(instanceList.length>0) {
			// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
			let selectProxy: SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);


			// 레이어 변경 처리
			/*
			중요!!!
			추가 선택 인스턴스가 0에서 1로 늘어나는 경우 레이어를 변경처리 해야함.

			즉, 선택처리가 배경인 경우 selectProxy 선택처리를 현재 활성화된 레이어로 변경해야함.
			 */
			if (selectProxy.currentPropertyManager.layerName != editorProxy.activeLayerInfo.name) {
				selectProxy.setSelectedLayer(editorProxy.activeLayerInfo.name);
			}
                  // 선택 추가
                  instanceList.forEach((instance) => {
                        selectProxy.addSelectedInstance(instance);

                  });


                  selectProxy.updateSelectedInfo();



		}else {
			console.log("배치 정보가 없음.")
		}


	}
}
