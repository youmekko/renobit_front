import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";

import EditorStatic from "./EditorStatic";
import SelectProxy from "../../model/SelectProxy";
import WVComponent from "../../../wemb/core/component/WVComponent";



/*
선택 컴포넌트 삭제하기

 */
export class RemoveSelectedComponentInstanceCommand extends puremvc.SimpleCommand {


	execute(note: puremvc.INotification) {

		let selectProxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);


		let comList:WVComponent[] = selectProxy.selectedTwoThreeMasterComInstanceListList;

            //console.log("remove 1 ", comList.length);

		if(comList.length>0) {


			// 삭제 정보를 복사 해놓기
			let instanceList = comList.slice(0);

			// 선택 정보 취소 처리
			this.sendNotification(EditorStatic.CMD_CLEAR_ALL_SELECTED);

			// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
			let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
			instanceList.forEach((instance)=>{
				// undo, redo
				console.log('selected remove' , instance);
				editorProxy.setHistoryEditing(instance as WVComponent, "remove");
				
				editorProxy.removeComInstance(instance);
			})

			instanceList =null;

                  this.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE);
			// 수정 상태 변경
			this.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);

		}

	}
}
