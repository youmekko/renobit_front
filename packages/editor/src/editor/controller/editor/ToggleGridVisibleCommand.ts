import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";

export class ToggleGridVisibleCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
	      try {
                  console.log("@@ wemb.threeElements.threeLayer.getGridHelper().visible1 = ", wemb.threeElements.threeLayer.getGridHelper().visible);
                  console.log("@@ wemb.threeElements.threeLayer.getGridHelper().visible2 = ", note.getBody());
                  wemb.threeElements.threeLayer.getGridHelper().visible=note.getBody();
            }catch(error){
	            console.log("ToggleGridVisibleCommand error", error);
	      }
      }


}
