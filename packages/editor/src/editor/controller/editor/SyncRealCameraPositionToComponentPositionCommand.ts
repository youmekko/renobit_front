import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";
import SelectProxy from "../../model/SelectProxy";
import EditorStatic from "./EditorStatic";


/*
에디터에서  oribit과 같은 control에 의해서 camera위치가 변경되는 경우
이 값을 실제 페이지 컴포넌트의 카메라 프로퍼티와 동일하게 만들어야 함.


 */
export class SyncRealCameraPositionToComponentPositionCommand extends puremvc.SimpleCommand {

	execute(note: puremvc.INotification) {

		// 선택 프락시 취소 처리
		let editorProxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		if(editorProxy.isOpenPage==true) {

                  let vo = note.getBody();

                  let cameraPosition = editorProxy.mainPageComponent.properties.setter.threeLayer.camera;

                  cameraPosition.x = vo.x;
                  cameraPosition.y = vo.y;
                  cameraPosition.z = vo.z;

                  //console.log("camera 2 , SyncRealCameraPositionToComponentPositionCommand ", editorProxy.isOpenPage);

                  this.sendNotification(SelectProxy.NOTI_UPDATE_PROPERTIES);
			editorProxy.setHistoryEditing(null,"edit");
                  // 수정 상태로 변경
                  this.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);
            }
      }
}
