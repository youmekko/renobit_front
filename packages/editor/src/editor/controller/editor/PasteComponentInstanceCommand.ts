import * as puremvc from "puremvc";
import CopyPasteProxy from "../../model/CopyPasteProxy";
import EditorProxy from "../../model/EditorProxy";
import HookManager from "../../../wemb/wv/managers/HookManager";
import {IComponentMetaProperties} from "../../../wemb/core/component/interfaces/ComponentInterfaces";


export class PasteComponentInstanceCommand extends puremvc.SimpleCommand {

	async execute(note: puremvc.INotification) {

		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		let copyPasteProxy: CopyPasteProxy = <CopyPasteProxy>this.facade.retrieveProxy(CopyPasteProxy.NAME);




            // 2018.11.26(ckkim) // Array<IComponentMetaProperties>
            if(await window.wemb.hookManager.executeAction(HookManager.HOOK_BEFORE_PASTE_COMPONENT_INSTANCE, copyPasteProxy.instanceMetaList)==false){
                  console.warn("컴포넌트 붙여넣는 과정에서 취소 되었습니다.");
                  //alert("페이지 저장이 취소 되었습니다.");
                  return;
            }

		copyPasteProxy.pasteInstanceList(editorProxy.activeLayerInfo.name);
	}
}
