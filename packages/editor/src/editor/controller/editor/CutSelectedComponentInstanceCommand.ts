import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import CopyPasteProxy from "../../model/CopyPasteProxy";
import WVComponent from "../../../wemb/core/component/WVComponent";
import EditorStatic from "./EditorStatic";


export class CutSelectedComponentInstanceCommand extends puremvc.SimpleCommand {

	execute(note: puremvc.INotification) {

		let selectProxy: SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);


            let comList:WVComponent[] = selectProxy.selectedTwoThreeMasterComInstanceListList;

		if(comList.length>0) {
			// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
			let copyPasteProxy: CopyPasteProxy = <CopyPasteProxy>this.facade.retrieveProxy(CopyPasteProxy.NAME);
			copyPasteProxy.cutInstanceList(selectProxy.currentPropertyManager.layerName, comList);


			this.sendNotification(EditorStatic.CMD_REMOVE_SELECTED_COM_INSTANCE);


		}else {
			console.log("오리기 내용이 없음. ")
		}


	}
}
