import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import {WORK_LAYERS} from "../../../wemb/wv/layer/Layer";
import EditorProxy from "../../model/EditorProxy";


export class ClearAllSelectedCommand extends puremvc.SimpleCommand {

	/*
		신규 선택시

		1. 레이어 선택
	 */
	execute(note: puremvc.INotification) {
		// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
		let proxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);


		proxy.clearAllSelectedInstance();
		proxy.setSelectedLayer(WORK_LAYERS.BG_LAYER);
		proxy.updateSelectedInfo();
            this.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE)

	}
}
