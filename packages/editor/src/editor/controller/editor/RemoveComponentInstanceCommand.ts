import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";
import WVComponent from "../../../wemb/core/component/WVComponent";
import EditorStatic from "./EditorStatic";


// 사용않함(2018.12.02), 삭제 예정
export class RemoveComponentInstanceCommand extends puremvc.SimpleCommand {


	execute(note: puremvc.INotification) {
            console.log("@@ CHECK RemoveComponentInstanceCommand");
		// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
		let proxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		let comInstance:WVComponent = note.getBody();

		// 선택 항목이 있는 경우 먼저 선택 제거
		if(comInstance.selected) {
			this.sendNotification(EditorStatic.CMD_REMOVE_SELECTED, comInstance);
		}

		proxy.removeComInstance(comInstance);

	}
}
