import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import WVComponent from "../../../wemb/core/component/WVComponent";
import {WORK_LAYERS} from "../../../wemb/wv/layer/Layer";
import EditorProxy from "../../model/EditorProxy";



export class NewSelectedComponentInstanceCommand extends puremvc.SimpleCommand {

	/*
		신규 선택시

		1. 레이어 선택
	 */
	execute(note: puremvc.INotification) {

		// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
		let selectProxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
		let instance:WVComponent = <WVComponent>note.getBody();


		// 기존 선택 내용 제거
            selectProxy.clearAllSelectedInstance();
            selectProxy.setSelectedLayer(<WORK_LAYERS>instance.layerName);
            selectProxy.addSelectedInstance(instance)
            selectProxy.updateSelectedInfo();

            this.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE)


      }
}
