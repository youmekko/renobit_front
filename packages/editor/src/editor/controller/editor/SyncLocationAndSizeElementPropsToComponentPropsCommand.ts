import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";
import SelectProxy from "../../model/SelectProxy";
import EditorStatic from "./EditorStatic";


/*
2d, 3d controller에 의해서 위치 및 크기가 변경되는 경우
element의 속성과 컴포넌트 속성을 동기화 시켜주는 작업이 필요.



 */
export class SyncLocationAndSizeElementPropsToComponentPropsCommand extends puremvc.SimpleCommand {

	execute(note: puremvc.INotification) {

		// 선택 프락시 취소 처리
		let selectedProxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);

            console.log("SyncLocationAndSizeElementPropsToComponentPropsCommand");
		selectedProxy.currentPropertyManager.syncLocationSizeElementPropsComponentProps();


		this.sendNotification(SelectProxy.NOTI_UPDATE_PROPERTIES);
		var editorProxy =  this.facade.retrieveProxy(EditorProxy.NAME) as EditorProxy;
		editorProxy.setHistoryEditing(null,"edit");

            // 수정 상태로 변경
            this.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);

      }
}
