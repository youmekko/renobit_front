import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";


export class SetActiveShortcutStateCommand extends puremvc.SimpleCommand {

	execute(note: puremvc.INotification) {

		let proxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);

		// 변경하고 NOTI 발생
		proxy.setActiveShortcut(note.getBody());

	}
}
