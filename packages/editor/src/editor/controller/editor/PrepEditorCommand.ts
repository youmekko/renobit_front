import * as puremvc from "puremvc";


import EditorStatic from "./EditorStatic";
import {ChangeActiveLayerCommand} from "./ChangeActiveLayerCommand";
import {AddComponentInstanceByNameCommand} from "./AddComponentInstanceByNameCommand";
import {NewSelectedComponentInstanceCommand} from "./NewSelectedComponentInstanceCommand";
import {AddSelectedCommand} from "./AddSelectedCommand";
import {ClearAllSelectedCommand} from "./ClearAllSelectedCommand";
import {RemoveComponentInstanceCommand} from "./RemoveComponentInstanceCommand";
import {RemoveSelectedComponentInstanceCommand} from "./RemoveSelectedComponentInstanceCommand";
import {AddAllSelectedCommand} from "./AddAllSelectedCommand";
import {RemoveSelectedCommand} from "./RemoveSelectedCommand";

import {SyncLocationAndSizeElementPropsToComponentPropsCommand} from "./SyncLocationAndSizeElementPropsToComponentPropsCommand";
import {AlignSelectedComponentInstanceCommand} from "./AlignSelectedComponentInstanceCommand";
import {ArrangeSelectedComponentInstanceCommand} from "./ArrangeSelectedComponentInstanceCommand";
import {SizeSelectedComponentInstanceCommand} from "./SizeSelectedComponentInstanceCommand";
import {DistributeSelectedComponentInstanceCommand} from "./DistributeSelectedComponentInstanceCommand";
import {SetModifyStateCommand} from "./SetModifyStateCommand";
import {CutSelectedComponentInstanceCommand} from "./CutSelectedComponentInstanceCommand";
import {CopySelectedComponentInstanceCommand} from "./CopySelectedComponentInstanceCommand";
import {PasteComponentInstanceCommand} from "./PasteComponentInstanceCommand";
import {SetActiveShortcutStateCommand} from "./SetActiveShortcutStateCommand";
import {SyncRealCameraPositionToComponentPositionCommand} from "./SyncRealCameraPositionToComponentPositionCommand";
import {MoveSelectedComponentInstanceCommand} from "./MoveSelectedComponentInstanceCommand";
import {ChangeThreeTrasnformControlModeCommand} from "./ChangeThreeTrasnformControlModeCommand";
import { ToggleLeftAsideVisibleCommand } from "./ToggleLeftAsideVisibleCommand";
import { ToggleRightAsideVisibleCommand } from "./ToggleRightAsideVisibleCommand";
import {ShowExtensionPopupCommand} from "./ShowExtensionPopupCommand";
import {CalculateSelectedComponentInstanceInDragAreaCommand} from "./CalculateSelectedComponentInstanceInDragAreaCommand";
import {ToggleDirectionViewVisibleCommand} from "./ToggleDirectionViewVisibleCommand";
import {ToggleGridVisibleCommand} from "./ToggleGridVisibleCommand";
import {ShowModalCommand} from "./ShowModalCommand";
import {ExecuteScriptCommand} from "./ExecuteScriptCommand";
import {AddSelectedListCommand} from "./AddSelectedListCommand";

export class PrepEditorCommand extends puremvc.SimpleCommand {


	execute(note: puremvc.INotification) {

		console.log("step 02, PrepEditorCommand");


		this.facade.registerCommand(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, ChangeActiveLayerCommand);
		this.facade.registerCommand(EditorStatic.CMD_ADD_COM_INSTANCE_BY_NAME, AddComponentInstanceByNameCommand);

		// 사용않함(2018.12.02)
		this.facade.registerCommand(EditorStatic.CMD_REMOVE_COM_INSTANCE, RemoveComponentInstanceCommand);
		this.facade.registerCommand(EditorStatic.CMD_REMOVE_SELECTED_COM_INSTANCE, RemoveSelectedComponentInstanceCommand);
		this.facade.registerCommand(EditorStatic.CMD_ADD_ALL_SELECTED, AddAllSelectedCommand);
            this.facade.registerCommand(EditorStatic.CMD_ADD_SELECTED_LIST, AddSelectedListCommand);

		this.facade.registerCommand(EditorStatic.CMD_NEW_SELECTED_COMPONENT_INSTANCE, NewSelectedComponentInstanceCommand);
		this.facade.registerCommand(EditorStatic.CMD_ADD_SELECTED, AddSelectedCommand);
		this.facade.registerCommand(EditorStatic.CMD_REMOVE_SELECTED, RemoveSelectedCommand);
		this.facade.registerCommand(EditorStatic.CMD_CLEAR_ALL_SELECTED, ClearAllSelectedCommand);


		this.facade.registerCommand(EditorStatic.CMD_COPY_SELECTED_COM_INSTANCE, CopySelectedComponentInstanceCommand);
            this.facade.registerCommand(EditorStatic.CMD_CUT_SELECTED_COM_INSTANCE, CutSelectedComponentInstanceCommand);

		this.facade.registerCommand(EditorStatic.CMD_PASTE_COM_INSTANCE, PasteComponentInstanceCommand);

		this.facade.registerCommand(EditorStatic.CMD_SYNC_LOCATION_AND_SIZE_ELEMENT_PROPS_TO_COMPONENT_PROPS, SyncLocationAndSizeElementPropsToComponentPropsCommand);
            this.facade.registerCommand(EditorStatic.CMD_SYNC_REAL_CAMERA_POSITION_TO_COMPONENT_POSITION, SyncRealCameraPositionToComponentPositionCommand);



		this.facade.registerCommand(EditorStatic.CMD_ALIGN_SELECTED_COM_INSTANCE, AlignSelectedComponentInstanceCommand);
            this.facade.registerCommand(EditorStatic.CMD_ARRANGE_SELECTED_COM_INSTANCE, ArrangeSelectedComponentInstanceCommand);
            this.facade.registerCommand(EditorStatic.CMD_SIZE_SELECTED_COM_INSTANCE, SizeSelectedComponentInstanceCommand);
            this.facade.registerCommand(EditorStatic.CMD_DISTRIBUTE_SELECTED_COM_INSTANCE, DistributeSelectedComponentInstanceCommand);



            this.facade.registerCommand(EditorStatic.CMD_SET_MODIFY_STATE, SetModifyStateCommand);
            this.facade.registerCommand(EditorStatic.CMD_SET_ACTIVE_SHORT_CUT_STATE, SetActiveShortcutStateCommand);



            this.facade.registerCommand(EditorStatic.CMD_MOVE_SELECTED_COMPONENT_INSTANCE, MoveSelectedComponentInstanceCommand);


            this.facade.registerCommand(EditorStatic.CMD_CHANGE_THREE_TRANSFORM_CONTROL_MODE, ChangeThreeTrasnformControlModeCommand);


		this.facade.registerCommand(EditorStatic.CMD_TOGGLE_LEFT_ASIDE_VISIBLE, ToggleLeftAsideVisibleCommand);
		this.facade.registerCommand(EditorStatic.CMD_TOGGLE_RIGHT_ASIDE_VISIBLE, ToggleRightAsideVisibleCommand);
            this.facade.registerCommand(EditorStatic.CMD_SHOW_EXTENSION_POPUP, ShowExtensionPopupCommand);
            this.facade.registerCommand(EditorStatic.CMD_CALCULATE_SELECTED_COMPONENT_INSTANCE_IN_DRAG_AREA, CalculateSelectedComponentInstanceInDragAreaCommand);



            /*
            toggle direction view
             */
            this.facade.registerCommand(EditorStatic.CMD_TOGGLE_DIRECTION_VIEW, ToggleDirectionViewVisibleCommand);
            this.facade.registerCommand(EditorStatic.CMD_TOGGLE_GRID_HELPER, ToggleGridVisibleCommand);



            this.facade.registerCommand(EditorStatic.CMD_SHOW_MODAL, ShowModalCommand);

            this.facade.registerCommand(EditorStatic.CMD_EXECUTE_SCRIPT, ExecuteScriptCommand);



	}
}
