import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import EditorProxy from "../../model/EditorProxy";
import {Rectangle} from "../../../wemb/core/geom/interfaces/GeomInterfaces";
import ComponentDistributeHelper from "../../../wemb/wv/helpers/ComponentDistributeHelper";


export class DistributeSelectedComponentInstanceCommand extends puremvc.SimpleCommand {

    execute(note: puremvc.INotification) {
        console.log("DistributeSelectedComponentInstanceCommand ");
        let selectProxy: SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
        let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
        if (selectProxy.currentPropertyManager.getInstanceLength() > 0) {

            let size: Rectangle = {
                width: window.wemb.mainPageComponent.width,
                height: window.wemb.mainPageComponent.height,
                x: 0,
                y: 0
            };
            ComponentDistributeHelper.execute(note.getBody(), selectProxy.currentPropertyManager.getInstanceList(), editorProxy.activeLayerInfo.instanceList, size);

            this.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);
        } else {
            console.log("선택 내용이 없음. ");
        }
    }
}
