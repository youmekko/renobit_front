import * as puremvc from "puremvc";
import SelectProxy from "../../model/SelectProxy";
import WVComponent from "../../../wemb/core/component/WVComponent";
import {WORK_LAYERS} from "../../../wemb/wv/layer/Layer";
import EditorProxy from "../../model/EditorProxy";
import {ArrayUtil} from "../../../wemb/utils/Util";

export class AddSelectedListCommand extends puremvc.SimpleCommand {
      // 다중 선택시 리스트를 받아서 넣는다
      execute(note : puremvc.INotification){
            let selectProxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
            let editorProxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            let data:{list : Array<WVComponent>, clear : boolean } = note.getBody();
            let layerName = <WORK_LAYERS>data.list[0].layerName;
            if(layerName!=selectProxy.currentPropertyManager.layerName){
                  selectProxy.setSelectedLayer(layerName);
            }

            if(data.clear){
                  selectProxy.clearAllSelectedInstance();
            }
            data.list.forEach((instance)=>{
                  selectProxy.addSelectedInstance(editorProxy.mainPageComponent.getComInstanceByName(instance.name))
            });

            selectProxy.updateSelectedInfo();
            this.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE)
      }
}
