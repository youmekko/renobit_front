import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";

export class ToggleLeftAsideVisibleCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		editorProxy.toggleLeftAside();
	}
}
