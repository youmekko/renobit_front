import * as puremvc from "puremvc";


export class ChangeThreeTrasnformControlModeCommand extends puremvc.SimpleCommand {


	execute(note: puremvc.INotification) {

		// 레이어 활성화
		//let proxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);

		window.wemb.mainPageComponent.threeLayer.setTransformControlMode(note.getBody())
	}
}
