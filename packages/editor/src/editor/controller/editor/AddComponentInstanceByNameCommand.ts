import * as puremvc from "puremvc";
import Vue from "vue";
import EditorProxy from "../../model/EditorProxy";
import EditorStatic from "./EditorStatic";
import {ComponentInstanceInfoVO} from "../../model/vo/ValueObjects";
import {ObjectUtil} from "../../../wemb/utils/Util";
import WVEvent from "../../../wemb/core/events/WVEvent";
import {WORK_LAYERS} from "../../../wemb/wv/layer/Layer";
import {
      IComponentMetaProperties,
      IWVComponent,
      WVCOMPONENT_METHOD_INFO
} from "../../../wemb/core/component/interfaces/ComponentInterfaces";
import WVComponent from "../../../wemb/core/component/WVComponent";
import WVComponentEvent from "../../../wemb/core/events/WVComponentEvent";
import HookManager from "../../../wemb/wv/managers/HookManager";
import ComponentCounter from "../../../../../renobit_client/src/wemb/core/utils/ComponentCounter";



export class AddComponentInstanceByNameCommand extends puremvc.SimpleCommand {

	/*
	1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
	2. 인스턴스 생성
	3. element및 프로퍼티 생성
	4. 화면에 붙이기
	 */
	async execute(note: puremvc.INotification) {
		// 1. ComponentInstanceInfoVO바탕으로 componentProperites 만들기
		let proxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		let vo:ComponentInstanceInfoVO = note.getBody();


            let tempInitMetaProperties:any = $.extend(true, {

                  props: {
                        setter: {
                              depth: 0
                        }
                  }
            },vo.initProperties);



            if(vo.hasOwnProperty("id")){
                  tempInitMetaProperties.id=vo.id;
            }else {
                  tempInitMetaProperties.id=ObjectUtil.generateID();
            }


            if(vo.hasOwnProperty("name")){
                  tempInitMetaProperties.name=vo.name;
            }else {
                  tempInitMetaProperties.name =  proxy.getMaxInstanceName(vo.componentName);
            }




            let initMetaProperties:IComponentMetaProperties =  $.extend(true, {
                  componentName:vo.componentName,
                  layerName: proxy.activeLayerInfo.name,
                  label:vo.label

		}, tempInitMetaProperties);




            initMetaProperties.props.setter.depth=  proxy.getMaxZIndexTo(proxy.activeLayerInfo.name);



            // 2018.11.21(ckkim)
            if(await window.wemb.hookManager.executeAction(HookManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE, initMetaProperties)==false){
                  console.warn("컴포넌트 생성 전 취소 AddComponentInstanceByNameCommand");
                  //alert("페이지 저장이 취소 되었습니다.");
                  return;
            }



            //2. 인스턴스 생성
            // Compass component는 페이지당 1개로 한정
            if(ComponentCounter.shouldBeOneAndPositionedComponent(vo.componentName)){
                  console.warn('allowed only one compass component');
                  alert('Allowed only one compass component!');
                  return;
            }


		let comInstance:IWVComponent = window.wemb.componentLibraryManager.createComponentInstance(vo.componentName);
            comInstance.exeMode = window.wemb.configManager.exeMode;

            let layerName:string = proxy.activeLayerInfo.name as string;
            if(layerName == WORK_LAYERS.THREE_LAYER) {
                  comInstance.setExtensionElements(window.wemb.threeElements);
            }




		let self = this;
            let $resourceEventBus = new Vue();




            function onCreated(){
                  comInstance.removeEventListener(WVEvent.CREATED, onCreated);

                  //4. 화면에 붙이기
                  proxy.addComInstance(comInstance as WVComponent);

                  // undo ,  redo
                  console.log('undo/redo :', comInstance as WVComponent);
                  proxy.setHistoryEditing(comInstance as WVComponent, "add");
            }

		function onAddComponent(){
		      comInstance.removeEventListener(WVEvent.ADDED_CONTAINER, onAddComponent);

		      if(comInstance.isResourceComponent==false){
                        completed();
                        return;
                  }


                  comInstance.setResourceBus($resourceEventBus);
                  $resourceEventBus.$on(WVComponentEvent.LOADED_RESOURCE, onLoadedResource);
                  comInstance.startLoadResource();
            }




            function onLoadedResource(comInstance:WVComponent){
                  //console.log("리소스 로드 완료 ");
                  $resourceEventBus.$off(WVComponentEvent.LOADED_RESOURCE, onLoadedResource);
                  if(comInstance==null){
                        console.log("리소스 완료시 반드시 컴포넌트 인스턴스를 넘겨줘야 합니다.");
                        return null;
                  }


                  // 체크하기
                  //console.log("리소스 로딩 완료 컴포넌트 = ", comInstance.id);
                  completed();
            }


            function completed(){
                  // onLoadPage를 가지고 있는 컴포넌트만 onLoadPage()메서드 호출
                  if(comInstance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]){
                        comInstance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]();
                  }


                  self.sendNotification(EditorStatic.CMD_NEW_SELECTED_COMPONENT_INSTANCE, comInstance);
                  self.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);
                  self.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE);
            }


            comInstance.addEventListener(WVEvent.CREATED, onCreated );
            comInstance.addEventListener(WVEvent.ADDED_CONTAINER, onAddComponent );
            //3. element및 프로퍼티 생성
            comInstance.create(initMetaProperties);
            console.log(initMetaProperties);
	}
}
