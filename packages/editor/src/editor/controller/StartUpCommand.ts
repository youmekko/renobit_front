import * as puremvc from "puremvc";
import {PrepViewCommand} from "./PrepViewCommand";
import {PrepModelCommand} from "./PrepModelCommand";
import {PrepEditorArchitectCommand} from "./PrepEditorArchitectCommand";
import {PrepEditorCommand} from "./editor/PrepEditorCommand";
import {PrepCommonCommand} from "./PrepCommonCommand";
import {PrepEditorMainCommand} from "./main/PrepEditorMainCommand";
import {CreateDynamicPanelCommand} from "./main/CreateDynamicPanelCommand";


export default class StartUpCommand extends puremvc.MacroCommand {
	initializeMacroCommand() {
		console.log("step 02, StartUpCommand 실행 view, model, command 등록");
		this.addSubCommand(PrepEditorCommand);
            this.addSubCommand(PrepEditorMainCommand);

		this.addSubCommand(PrepViewCommand);
		this.addSubCommand(PrepModelCommand);

		this.addSubCommand(PrepCommonCommand);
		this.addSubCommand(PrepEditorArchitectCommand);


		console.log("step 02, StartUpCommand 실행 view, model, command 완료");
	}
}
