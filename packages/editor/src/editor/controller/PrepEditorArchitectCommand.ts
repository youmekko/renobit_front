import * as puremvc from "puremvc";


import {RegisterGlobalVariableCommand} from "./architect/RegisterGlobalVariableCommand";
import {ReadyEditorCommand} from "./architect/ReadyEditorCommand";
import ArchitectStatic from "./architect/ArchitectStatic";
import {StartEditorCommand} from "./architect/StartEditorCommand";

export class PrepEditorArchitectCommand extends puremvc.SimpleCommand {


    execute(note: puremvc.INotification) {

	  console.log("step 02, PrepEditorArchitectCommand 실행");
        this.facade.registerCommand(ArchitectStatic.CMD_REGISTER_GLOBAL_VARIABLE_EDITOR, RegisterGlobalVariableCommand);
        this.facade.registerCommand(ArchitectStatic.CMD_READY_EDITOR, ReadyEditorCommand);
        this.facade.registerCommand(ArchitectStatic.CMD_START_EDITOR, StartEditorCommand);
    }
}
