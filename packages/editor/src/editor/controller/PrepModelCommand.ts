import * as puremvc from "puremvc";
import EditorProxy from "../model/EditorProxy";
import PublicProxy from "../model/PublicProxy";
import SelectProxy from "../model/SelectProxy";
import CopyPasteProxy from "../model/CopyPasteProxy";
import StageProxy from "../model/StageProxy";

export class PrepModelCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		let selectProxy:SelectProxy = new SelectProxy();
		this.facade.registerProxy(selectProxy);
		this.facade.registerProxy(new PublicProxy());

		this.facade.registerProxy(new EditorProxy(selectProxy));

		this.facade.registerProxy(new CopyPasteProxy());

		this.facade.registerProxy(new StageProxy());


	}
}
