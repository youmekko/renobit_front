import * as puremvc from "puremvc";


import {EditorMainMediator} from "../view/EditorMainMediator";
import MainMenubarMediator from "../view/toolbar/mainMenubar/MainMenubarMediator";

import PageToolbarMediator from "../view/toolbar/pageToolbar/PageToolbarMediator";
import LayerToolbarMediator from "../view/toolbar/layerToolbar/LayerToolbarMediator";
import WorkAreaToolbarMediator from "../view/toolbar/workAreaToolbar/WorkAreaToolbarMediator";

import TwoToolbarMediator from "../view/toolbar/twoToolbar/TwoToolbarMediator";
import ThreeToolbarMediator from "../view/toolbar/threeToolbar/ThreeToolbarMediator";
import ComponentListPanelMediator from "../view/panel/componentListPanel/ComponentListPanelMediator";
import ComponentPropertyPanelMediator from "../view/panel/componentPropertyPanel/ComponentPropertyPanelMediator";
import OutlinePanelMediator from "../view/panel/outlinePanel/OutlinePanelMediator";
import StagePropertyPanelMediator from "../view/panel/stagePropertyPanel/StagePropertyPanelMediator";
import EditAreaMainContainerMediator from "../view/work-area/EditAreaMainContainerMediator";
import MainToolbarContainerMediator from "../view/toolbar/MainToolbarContainerMediator";
import ScriptEditorWorker from "../windows/scriptEditor/ScriptEditorWorker";
import PageTreePanelMediator from "../view/panel/pageTreePanel/PageTreePanelMediator";

export class PrepViewCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {

		console.log("step 02, preViewCommand");
		let editorMainMediator: puremvc.IMediator = new EditorMainMediator(EditorMainMediator.NAME, note.getBody());
		this.facade.registerMediator(editorMainMediator);

		this.facade.registerMediator(new MainMenubarMediator(MainMenubarMediator.NAME, window.wemb.viewComponentMap.get(MainMenubarMediator.NAME)));
		this.facade.registerMediator(new EditAreaMainContainerMediator(EditAreaMainContainerMediator.NAME, window.wemb.viewComponentMap.get(EditAreaMainContainerMediator.NAME)));


		this.facade.registerMediator(new MainToolbarContainerMediator(MainToolbarContainerMediator.NAME, window.wemb.viewComponentMap.get(MainToolbarContainerMediator.NAME)));
		this.facade.registerMediator(new PageToolbarMediator(PageToolbarMediator.NAME, window.wemb.viewComponentMap.get(PageToolbarMediator.NAME)));
		this.facade.registerMediator(new LayerToolbarMediator(LayerToolbarMediator.NAME, window.wemb.viewComponentMap.get(LayerToolbarMediator.NAME)));
		this.facade.registerMediator(new WorkAreaToolbarMediator(WorkAreaToolbarMediator.NAME, window.wemb.viewComponentMap.get(WorkAreaToolbarMediator.NAME)));
		this.facade.registerMediator(new TwoToolbarMediator(TwoToolbarMediator.NAME, window.wemb.viewComponentMap.get(TwoToolbarMediator.NAME)));
		this.facade.registerMediator(new ThreeToolbarMediator(ThreeToolbarMediator.NAME, window.wemb.viewComponentMap.get(ThreeToolbarMediator.NAME)));


		this.facade.registerMediator(new ComponentListPanelMediator(ComponentListPanelMediator.NAME, window.wemb.viewComponentMap.get(ComponentListPanelMediator.NAME)));
		this.facade.registerMediator(new ComponentPropertyPanelMediator(ComponentPropertyPanelMediator.NAME, window.wemb.viewComponentMap.get(ComponentPropertyPanelMediator.NAME)));
		this.facade.registerMediator(new OutlinePanelMediator(OutlinePanelMediator.NAME, window.wemb.viewComponentMap.get(OutlinePanelMediator.NAME)));
		
		this.facade.registerMediator(new PageTreePanelMediator(PageTreePanelMediator.NAME, window.wemb.viewComponentMap.get(PageTreePanelMediator.NAME)));
		this.facade.registerMediator(new StagePropertyPanelMediator(StagePropertyPanelMediator.NAME, window.wemb.viewComponentMap.get(StagePropertyPanelMediator.NAME)));
		
		/*임시*/
		this.facade.registerMediator(new ScriptEditorWorker(ScriptEditorWorker.NAME, window.wemb.viewComponentMap.get(ComponentListPanelMediator.NAME)));
	}
}