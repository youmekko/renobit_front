import * as puremvc from "puremvc";
import EditorStatic from "../editor/EditorStatic";



/*
페이지를 닫고 비활성화 상태로만 만들기.
 */
export class GotoViewerByCurrentPageNameCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
	      this.facade.sendNotification(EditorStatic.CMD_GOTO_VIEWER_BY_ID, window.wemb.pageManager.currentPageInfo.id);
	}
}
