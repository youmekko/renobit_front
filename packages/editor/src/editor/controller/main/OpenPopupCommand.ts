import Vue from "vue";
import * as puremvc from "puremvc";
import {POPUP} from "../../../wemb/wv/managers/WindowsManager";



export class OpenPopupCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
	      let param = note.body;

	      if (param === "codeCompiler") {
                  window.wemb.windowsManager.open(POPUP.CODE_COMPILER);
            } else {
                  window.wemb.windowsManager.open(param);
            }

	}
}
