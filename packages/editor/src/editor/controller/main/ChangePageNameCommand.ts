import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";
import SelectProxy from "../../model/SelectProxy";

export class ChangePageNameCommand extends puremvc.SimpleCommand {
    execute(note: puremvc.INotification) {

          let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
          // 페이지 이름 변경

          let info:PageVO = note.getBody();

          if(editorProxy.mainPageComponent.name==info.oldName) {
                editorProxy.mainPageComponent.setDynamicGroupPropertyValue("primary", "name", info.newName);
                let selectProxy:SelectProxy = <SelectProxy>this.facade.retrieveProxy(SelectProxy.NAME);
                selectProxy.updateSelectedInfo();

                //아래내용 업데이트 되지 않아 임시로 추가 
                window.wemb.pageManager.currentPageInfo.name = info.newName;
          }


    }
}

type PageVO={
      oldName:string;
      newName:string
}
