import * as puremvc from "puremvc";

import EditorProxy from "../../model/EditorProxy";
import {EditorMainMediator} from "../../view/EditorMainMediator";
import CustomMediator from "../../../wemb/wv/extension/CustomMediator";
import CustomPanel from "@/wemb/wv/extension/CustomPanel.vue";




/*
페이지를 닫고 비활성화 상태로만 만들기.
call :

 */
export class CreateDynamicPanelCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {

            if(window.wemb.extensionInstanceManager.usingExtension !=true) {
                  console.log("##### 확장 기능이 활성화되지 않았음.");
                  return;
            }


            let editorModeInstanceList = window.wemb.extensionInstanceManager.editorModeInstanceList;
	      if(editorModeInstanceList) {

	            let mainPageMediator: EditorMainMediator = this.facade.retrieveMediator(EditorMainMediator.NAME) as EditorMainMediator;
                  /*
                  1. 추가할 left 목록 구하기
                  2. 패널 만큼 메디에디터와 뷰컴포넌트를 동적으로 만들기.
                  3. facade에 등록
                  4. 화면에 나타날 수 있게 패널 정보를 vue의 컴포넌트에 전달.
                        신규 추가 갯수만큼 동적으로 패널이 만들어짐(component is 문법 사용)

                   */

                  // 1. 추가할 left 목록 구하기
                  /*
                  let leftPanelList = editorModeInstanceList["panels"].filter((panelInfo)=>{
                        return panelInfo.direction=="left"
                  });
                  */

                  let leftPanelList = editorModeInstanceList["panels"];
                  if(leftPanelList && leftPanelList.length>0) {
                        let mediatorViewList = [];
                        // 2. 패널 만큼 메디에디터와 뷰컴포넌트를 동적으로 만들기.
                        leftPanelList.forEach((panelInfo) => {
                              try {

                                    let ExtensionClass:any =  eval(panelInfo.extension_name);
                                    let mediatorView: any = new ExtensionClass();
                                    let mediatorName = panelInfo.label;
                                    let mediator = new CustomMediator(mediatorName, mediatorView, mediatorView.notificationList);
                                    this.facade.registerMediator(mediator);


                                    // 기본 값을 left로 설정
                                    let panelDirection = "left";
                                    if(panelInfo.hasOwnProperty("direction")==true){
                                          panelDirection= panelInfo.direction;
                                          if(panelInfo.direction!="right"){
                                                panelDirection= "left";
                                          }
                                    }


                                    // 3. facade에 등록
                                    mediatorView.setFacade(this.facade);

                                  /*  try {
                                          // 플러그인 시작
                                          mediatorView.start();
                                    }catch(error){

                                    }*/


                                    mediatorViewList.push({
                                          name: mediatorName,
                                          panelClass:CustomPanel,
                                          childView: mediatorView,
                                          direction:panelDirection
                                    });




                              }catch(error){
                                    console.log("###################");
                                    console.log("@@ extension error ", panelInfo.extension_name +" 이라는 확장 정보가 존재하지 않습니다.");
                                    console.log("###################");
                                    return;
                              }
                        });

                        if(mediatorViewList.length>0){
                              // 4. 화면에 나타날 수 있게 패널 정보를 vue의 컴포넌트에 전달.
                              // 이 갯수만큼 동적으로 패널이 만들어짐.
                              mainPageMediator.sendNotification(EditorProxy.NOTI_CREATE_DYNAMIC_PANEL, mediatorViewList);

                        }
                  }
            }else {
	            console.log("###################");
	            console.log("확장 정보가 존재하지 않습니다.");
                  console.log("###################");
            }
	}
}
