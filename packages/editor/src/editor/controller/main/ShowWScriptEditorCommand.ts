import * as puremvc from "puremvc";
import {POPUP} from "../../../wemb/wv/managers/WindowsManager";



export class ShowWScriptEditorCommand  extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            window.wemb.windowsManager.open(POPUP.SCRIPT_EDITOR);
      }
}
