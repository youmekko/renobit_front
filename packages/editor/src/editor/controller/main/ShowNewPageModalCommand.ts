import Vue from "vue";
import * as puremvc from "puremvc";


/*
페이지 생성 팝업.
 */

export class ShowNewPageModalCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
	      let createType:string = note.getBody();


		window.wemb.$createPageModal.showNewPage(createType);

	}
}
