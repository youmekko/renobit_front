import * as puremvc from "puremvc";
import pageApi from "../../../common/api/pageApi";
import {ProjectInfoData} from "../../../wemb/wv/page/Page";
import PublicProxy from "../../model/PublicProxy";
import StageProxy from "../../model/StageProxy";

/*
2018.09.09 ckkim
현재 사용안되고 있음.
 */
export class UpdateMasterInfoCommand extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {

            let projectInfo = <ProjectInfoData>note.getBody();
            let publicProxy = <PublicProxy>this.facade.retrieveProxy(PublicProxy.NAME);
            let stageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);

            // 마스터 정보 설정하기
            stageProxy.setMasterBackgroundInfo(projectInfo.master_info.background);
            stageProxy.setStageBackgroundInfo(projectInfo.stage_info.background);
            //2018.09.09
            stageProxy.setScripts(projectInfo.master_info.scripts);

            // 마스터 레이어 정보 설정하기
            publicProxy.setComInstanceInfoListInMasterLayer(projectInfo.master_info.master_layer);


	}

}

