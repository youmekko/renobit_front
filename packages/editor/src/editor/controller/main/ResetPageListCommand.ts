import * as puremvc from "puremvc";
import pageApi from "../../../common/api/pageApi";
import Vue from "vue";



/*
DB에 있는 페이지 리스트와 트리 json 정보를 모두 지우기.
일반적으로는 사용하지 않음.
테스트 용으로만 사용함.
 */
export class ResetPageListCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		console.log("openpage, ClosePageCommand 실행");
            let result=window.confirm(Vue.$i18n.message.wv.etc.etc07);
            if(result) {
                  pageApi.resetPageList().then((result)=>{
                        console.log(result);
                        //window.location.reload();
                  });
            }


	}
}
