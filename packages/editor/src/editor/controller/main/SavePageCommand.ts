import * as puremvc from "puremvc";

import EditorProxy from "../../model/EditorProxy";
import PublicProxy from "../../model/PublicProxy";
import {SavePageData} from "../../../wemb/wv/page/Page";
import pageApi from "../../../common/api/pageApi";
import Vue from "vue";
import StageProxy from "../../model/StageProxy";
import EditorStatic from "../editor/EditorStatic";
import HookManager from "../../../wemb/wv/managers/HookManager";


export class SavePageCommand extends puremvc.SimpleCommand {
	async execute(note: puremvc.INotification) {



		/*

		로딩 모달 활성화
		단축키 비활성화
		편집 비활성화
		페이지 열기 시작
			페이지 정보 유효성 검사
			기존 편집 정보 모두 삭제

			페이지 프로퍼티 정보 설정

			레이어별 컴포넌트 정보 생성
				- 마스터 레이어 컴포넌트 정보 생성(단, 마스터 정보가 출력이 안된 경우)
				- 2D 레이어 컴포넌트 정보 생성
				- 3D 레이어 컴포넌트 정보 생성

			레이어별 컴포넌트 생성
				- 마스터 레이어 컴포넌트 생성(단, 마스터 정보가 출력이 안된 경우)
				- 2D 레이어 컴포넌트 생성
				- 3D 레이어 컴포넌트 생성


			* 레이어 활성화(환경설정 값으로)

			* 컴포넌트 목록은 현재 활성화된 레이어에 배치할 수 있는 컴포넌트 목록이 출력
			* 프로퍼티는 페이지 프로퍼티 정보가 출력
			* 스테이지에는 현재 스테이지 정보가 출력
			* 아웃라인에는 현재 활성화된 레이어의 배치 정보가 출력



		 */

		let stageProxy: StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            if(editorProxy.isOpenPage==false){
                  console.log("페이지가 열리지 않은 상태에서 실행")
                  return;
            }


            /*
            2018.11.17(ckkim)
            action hook mehtod 실행
             */
            if(await window.wemb.hookManager.executeAction(HookManager.HOOK_BEFORE_SAVE_PAGE)==false){
                  console.warn("페이지 저장이 취소 되었습니다.");
                  //alert("페이지 저장이 취소 되었습니다.");
                  return;
            }




		// 에디터 기능 잠그기
            editorProxy.setEditable(false);

            // 단축키 잠그기
            editorProxy.setActiveShortcut(false);



            // 페이지 저장 데이터 생성
            let savePageData:SavePageData = new SavePageData();

            savePageData.data.page_info = editorProxy.mainPageComponent.serializePageMetaProperties();


            // 마스터 정보 저장.
            savePageData.data.master_info.background = stageProxy.backgroundInfo.master;
            savePageData.data.master_info.scripts  = stageProxy.getScripts();
            savePageData.data.stage_info.background = stageProxy.backgroundInfo.stage;

            // 레이어 정보 저장
            editorProxy.masterLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.master_info.master_layer.push(comInstance.serializeMetaProperties());
            });

            // 2D 영역
            editorProxy.twoLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.content_info.two_layer.push(comInstance.serializeMetaProperties());
            });

            //3D 영역
            editorProxy.threeLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.content_info.three_layer.push(comInstance.serializeMetaProperties());
            });

            let id = savePageData.data.page_info.id;
            pageApi.checkExistPage( id ).then((bool)=>{
                  /*
                  페이지 저장 실행
                  */
                  pageApi.savePage(savePageData).then(async(result:any)=>{
                        console.log("SavePageCommand 완료 데이터 ", result);
                        var locale_msg = Vue.$i18n.messages.wv;
                        if (result.data.result != "ok") {

                              Vue.$alert('error code: []<br>'+locale_msg.common.errorSave+' '+locale_msg.common.contact, locale_msg.common.error, {
                                    confirmButtonText: 'OK',
                                    type: "error",
                                    dangerouslyUseHTMLString: true
                              });

                              editorProxy.setEditable(true);
                              editorProxy.setActiveShortcut(true);

                              return;
                        }

                        Vue.$message({
                              showClose: true,
                              message: locale_msg.common.successSave,
                              type: 'success'
                        });

                        editorProxy.setEditable(true);
                        editorProxy.setActiveShortcut(true);
                        editorProxy.setModified(false);

                        /*
                        2018.09.30(ckkim)
                        - 페이지가 저장된 후 발생하는 NOTI
                         */
                        this.sendNotification(EditorProxy.NOTI_SAVED_PAGE, id);

                        /*
                       2018.11.26(ckkim)
                       action hook mehtod 실행
		            */
                        if(await window.wemb.hookManager.executeAction(HookManager.HOOK_AFTER_SAVE_PAGE)==false){
                              console.warn("페이지 저장 후 처리가 취소 되었습니다.");
                              //alert("페이지 저장이 취소 되었습니다.");
                              return;
                        }

                  })
            }).catch( (error)=>{
                  if( error === false ){
                        this.facade.sendNotification(EditorStatic.CMD_DELETED_CURRENT_PAGE);
                  }
            })




      }
}
