import * as puremvc from "puremvc";

export class ShowResourceManagerCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            window.wemb.$resourceManagerModal.show({onlyEditMode: true, type: 'all'});
	}
}
