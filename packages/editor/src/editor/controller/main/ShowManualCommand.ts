import * as puremvc from "puremvc";

export class ShowManualCommand  extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {

            let path = note.getBody();
            let context = window.opener ? window.opener.wemb.configManager.context : window.wemb.configManager.context;
            let manualURL = location.origin + context + path;

            // window focus를 위한 타임아웃
            setTimeout(function() {
                  window.open(manualURL, "_blank");
            },100)
      }
}
