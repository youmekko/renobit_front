import * as puremvc from "puremvc";
import FileManager from "../../../wemb/wv/managers/FileManager";

export class ImportResourcesCommand extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            let fileManager = new FileManager();

            fileManager.loadZipFile((success, data) => {
                  if(success) {
                        console.log(success, data);
                  }
            })
      }
}
