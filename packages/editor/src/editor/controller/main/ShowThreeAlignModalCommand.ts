import * as puremvc from "puremvc";

export class ShowThreeAlignModalCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            window.wemb.$threeAlignModal.show();
	}
}
