import Vue from "vue";
import * as puremvc from "puremvc";
import {SavePageData} from "../../../wemb/wv/page/Page";
import EditorProxy from "../../model/EditorProxy";
import StageProxy from "../../model/StageProxy";
import FileManager from "../../../wemb/wv/managers/FileManager";
import {ObjectUtil} from "../../../wemb/utils/Util";
import pageApi from "../../../common/api/pageApi";
import EditorStatic from "../editor/EditorStatic";


/*
페이지 생성 팝업.
 */

export class ImportPageCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            let fileManager = new FileManager();
            fileManager.loadTextFile((success, data) => {
                  if (success == true) {

                        console.log("pageData 1 = ", data);
                        let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
                        let stageProxy:StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
                        ///////////////////
                        // 페이지 정보 생성 시작
                        let pageData:any = JSON.parse(data).data;



                        console.log("pageData 2 = ", pageData);
                        if(pageData.hasOwnProperty("page_info")==false || pageData.hasOwnProperty("content_info")==false){
                              alert(Vue.$i18n.messages.wv.etc.etc03);
                              return;
                        }

                        // 기존 페이지가 존재하는지 확인하기
                        if (window.wemb.pageManager.hasPageInfoBy(pageData.page_info.id) == true) {

                              //alert("가져오려는 페이지는 이미 존재하는 페이지 입니다. 페이지id이름을 변경하고 다시 시도해주세요.");
                              //return;

                              if (window.confirm(Vue.$i18n.messages.wv.etc.etc06) == false)
                                    return;
                        }

                        // 페이지 저장 데이터 생성
                        let savePageData:SavePageData = new SavePageData();

                        savePageData.data.page_info = editorProxy.mainPageComponent.serializePageMetaProperties();

                        // 마스터 정보 저장.
                        savePageData.data.master_info.background = stageProxy.backgroundInfo.stage;
                        savePageData.data.stage_info.background = stageProxy.backgroundInfo.master;



                        if(window.wemb.pageManager.hasPageInfoByName(pageData.page_info.name)==true){
                              console.log("페이지가 존재!!!!!");
                              pageData.page_info.name+="_1";
                        }
                        savePageData.data.page_info = pageData.page_info;
                        //savePageData.data.content_info = pageData.content_info;

                        // 신규 아이디 발급.
                        savePageData.data.page_info.id= ObjectUtil.generateID();


                        // 2D 영역
                        savePageData.data.content_info.two_layer =[];
                        pageData.content_info.two_layer.forEach((comInstanceProperties) => {
                              comInstanceProperties.id=ObjectUtil.generateID();
                              savePageData.data.content_info.two_layer.push(comInstanceProperties);
                        });

                        //3D 영역
                        savePageData.data.content_info.three_layer = [];
                        pageData.content_info.three_layer.forEach((comInstanceProperties) => {
                              comInstanceProperties.id=ObjectUtil.generateID();
                              savePageData.data.content_info.three_layer.push(comInstanceProperties);
                        });


                        // 페이지 정보 생성 끝
                        //////////////////////////

                        console.log("pageVO_________ ", pageData.page_info.name, savePageData.data);
                        
                        // 2. 서버에 신규 페이지 생성하기

                        

                        //pageApi.saveNewPage(savePageData).then((result) => {
                        
                        window.wemb.pageTreeDataManager.addPage( pageData.page_info.name, savePageData.data ) .then((result) => {     console.log("SaveAsPageCommand 완료 데이터 ", result);
                              //window.wemb.pageTreeManager.addPage(savePageData.data.page_info.id, savePageData.data.page_info.name);
                              //tree 처리 추가 작업 필요 
                              //window.wemb.pageManager.addPageInfo(savePageData.data.page_info);
                              this.sendNotification(EditorStatic.CMD_OPEN_PAGE, savePageData.data.page_info.id);

                        }).catch((error)=>{
                              console.log("IMPORT 페이지 저장 중 에러 발생");                              
                              var locale_msg = Vue.$i18n.messages.wv;
                              Vue.$alert('error code: []<br>'+locale_msg.common.errorSave+' '+locale_msg.common.contact, locale_msg.common.error, {
                                    confirmButtonText: 'OK',
                                    type: "error",
                                    dangerouslyUseHTMLString: true
                              });

                              editorProxy.setEditable(true);
                              editorProxy.setActiveShortcut(true);

                              return;
                        })

                  } else {
                        console.log("페이지 정보를 가져오는 도중에 에러 발생", data)
                  }
            });
	}
}
