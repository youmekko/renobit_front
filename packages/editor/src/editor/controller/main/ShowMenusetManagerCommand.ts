import * as puremvc from "puremvc";

export class ShowMenusetManagerCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		window.wemb.$menusetManagerModal.show('', true);
	}
}
