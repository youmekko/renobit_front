import * as puremvc from "puremvc";
import snapshotApi from '../../../common/api/snapshotApi';
import Vue from "vue";

export class BackupProjectCommand  extends puremvc.SimpleCommand {
      async execute(note: puremvc.INotification) {
            let locale_msg = Vue.$i18n.messages.wv;

            const loading = Vue.$loading({
                  lock: true,
                  text: locale_msg.snapshot.loading,
                  spinner: 'el-icon-loading',
                  background: 'rgba(255, 255, 255, 0.7)'
            });

            let response = await snapshotApi.backupProject();

            if (response.data.result === "SUCCESS") {
                  Vue.$message({
                        showClose: true,
                        message: locale_msg.snapshot.successExport,
                        type: 'success'
                  });
                  window.wemb.$globalBus.$emit('snapshot_load')
            } else {
                  Vue.$message.error(locale_msg.snapshot.errorExport);
            }

            loading.close();
      }
}
