import * as puremvc from "puremvc";

import {CONFIG_PROPERTY_GROUP_NAME} from "../../../wemb/wv/managers/ConfigManager";
import CustomMediator from "@/wemb/wv/extension/CustomMediator";
import {EditorMainMediator} from "../../view/EditorMainMediator";


/*
플러그인 생성
플러그인은 puremvc mediator view로 등록됨.
 */
export class CreateExtensionCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {

	      console.log("CreateExtensionCommand execute ");
            if(window.wemb.extensionInstanceManager.usingExtension !=true)
                  return;


            let editorModeInstanceList = window.wemb.extensionInstanceManager.editorModeInstanceList;
	      if(editorModeInstanceList) {

	            let mainPageMediator: EditorMainMediator = this.facade.retrieveMediator(EditorMainMediator.NAME) as EditorMainMediator;
                  /*
                  1. 추가할 플러그인 목록 구하기
                  2. 패널 만큼 메디에디터와 뷰컴포넌트를 동적으로 만들기.
                  3. facade에 등록


                   */

                  // 1. 추가할 플러그인 목록 구하기
                  let pluginList = editorModeInstanceList["plugins"];
                  let count = 1;
                  console.log("@@ EXTENSION extension start, maxcount= ", pluginList.length);
                  if(pluginList && pluginList.length>0) {
                        pluginList.forEach((extensionInfo) => {
                              try {
                                    let PluginClass:any = eval(extensionInfo.extension_name);
                                    let mediatorView: any = new PluginClass();
                                    let mediatorName = extensionInfo.label;
                                    let mediator = new CustomMediator(mediatorName, mediatorView, mediatorView.notificationList);
                                    this.facade.registerMediator(mediator);


                                    mediatorView.setFacade(this.facade);

                                    console.log("@@ EXTENSION extension create ", (count), mediatorName);

                              }catch(error){
                                    console.log("###################");
                                    console.log("error CreateExtensionCommand ", extensionInfo.extension_name +" 이라는 확장 정보가 존재하지 않습니다.");
                                    console.log("###################");
                              }

                              count++;
                        });

                        console.log("@@ EXTENSION extension = end ");
                  }
            }else {
	            console.log("###################");
	            console.log("확장 정보가 존재하지 않습니다.");
                  console.log("###################");
            }
	}
}
