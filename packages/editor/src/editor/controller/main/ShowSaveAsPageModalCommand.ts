import * as puremvc from "puremvc";
import EditorProxy from "../../model/EditorProxy";


/*
페이지 생성 팝업.
 */

export class ShowSaveAsPageModalCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
	      let editorProxy:EditorProxy = this.facade.retrieveProxy(EditorProxy.NAME) as EditorProxy;
		window.wemb.$createPageModal.showSaveAsPage(editorProxy.mainPageComponent.properties);

	}
}
