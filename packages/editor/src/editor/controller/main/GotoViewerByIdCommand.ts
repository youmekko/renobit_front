import * as puremvc from "puremvc";


export class GotoViewerByIdCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {

	      let pageId:string =  note.getBody();

            let viewerURL = wemb.configManager._getFullPathName() + "/visualViewer.do#/pid=" + pageId;

            // window focus를 위한 타임아웃
            setTimeout(function() {
                window.open(viewerURL, "_blank");
            },100)
	}
}
