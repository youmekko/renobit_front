import Vue from "vue";
import * as puremvc from "puremvc";
import {POPUP} from "../../../wemb/wv/managers/WindowsManager";



export class ShowDatasetManagerCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            window.wemb.windowsManager.open(POPUP.DATASET_MANAGER);
	}
}
