import * as puremvc from "puremvc";

import EditorProxy from "../../model/EditorProxy";
import PublicProxy from "../../model/PublicProxy";
import pageApi from "../../../common/api/pageApi";
import {OpenPageData} from "../../../wemb/wv/page/Page";
import StageProxy from "../../model/StageProxy";
import Vue from "vue";
import WVComponent from "../../../wemb/core/component/WVComponent";
import {WVCOMPONENT_METHOD_INFO} from "../../../wemb/core/component/interfaces/ComponentInterfaces";
import {ComponentResourceLoader, ComponentResourceLoaderEvent} from "../../../wemb/wv/helpers/ComponentResourceLoader";
import HookManager from "../../../wemb/wv/managers/HookManager";
import {LayerInfo, WORK_LAYERS} from "../../../wemb/wv/layer/Layer";
import CPUtil from "../../../wemb/core/utils/CPUtil";
import NLoaderManager from "../../../wemb/core/component/3D/manager/NLoaderManager";
import {THREE} from "../../../wemb/core/utils/reference";
import {load} from "dotenv";

/*
순서
01. 로딩 모들 활성화
02. 메인 페이지 마스터, 3D, 2D 레이어 숨기기(한 번에 딱! 나오는 효과를 위해)
03. 페이지 닫기
      - 단축키 비활성화
      - 에디트 모드 비활성화
      - 레이어 정보 초기화
      - 선택 정보 비활성화
      - 복사 정보 모두 삭제

04. 페이지 정보 읽기
05. 페이지 정보 설정하기
      - 크기, 배경 등등

06. 리소스 로딩바 활성화

07. 배치할 컴포넌트 정보 읽기
      * 컴포넌트 정보 생성
            * 마스터 레이어 정보 추가
            * 2D 레이어 정보 추가
            * 3D 레이어 정보 추가
      * 컴포넌트 인스턴스 생성
            * 마스터 정보 출력(단, 마스터 정바가 출력이 안된 경우)
            * 2D 정보 출력
            * 3D 정보 출력

08. 컴포넌트 정보를 컴포넌트 인스턴스로 변경 후 레이어에 붙이기
09. 컴포넌트에 연결되어 잇는 리소스 읽기
 */
export class OpenPageCommand extends puremvc.SimpleCommand {
      private _componentResourceLoader:ComponentResourceLoader = null;
      private publicProxy             :PublicProxy;
      private isCreateMasterLayer     :boolean = false;
      private workerDirector          :any;
      private running                 :boolean = false;
      private cumilative              :number = 0;


	public async execute(note: puremvc.INotification) {
		console.log("OpenPageCommand 실행, page id = ", note.getBody());

		if(window.wemb.mainPageComponent.isLoading==true){
                  console.warn("You are already loading the page. It can be executed after loading is completed.");
		      return;
            }

            wemb.hookManager._actionHookMap.set(HookManager.HOOK_BEFORE_OPEN_PAGE, []);
            // 페이지 로딩 상태를 true로 만든다.
            window.wemb.mainPageComponent.setLoadingState(true);
            // 메인 페이지 컴포넌트 로딩 완료 유무 처리하기
            window.wemb.mainPageComponent.isLoaded=false;

            console.log("3-1. 리소스 로딩바 활성화");
            window.wemb.$editMainAreaResourceLoadingComponent.show();
            window.wemb.$editMainAreaResourceLoadingComponent.increase(0, 'Init Loading...');


            // 2018.11.21(ckkim)
            if(await window.wemb.hookManager.executeAction(HookManager.HOOK_BEFORE_LOAD_PAGE, note.getBody())==false){
                  console.warn("페이지 오픈 전 취소 되었습니다.");
                  window.wemb.mainPageComponent.setLoadingState(false);
                  window.wemb.$editMainAreaResourceLoadingComponent.hide();
                  //alert("페이지 저장이 취소 되었습니다.");

                  return;
            }




		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		let stageProxy:StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
            this.publicProxy = <PublicProxy>this.facade.retrieveProxy(PublicProxy.NAME);

		// 2018.11.17(ckkim) 화면을 뒤덮는 로딩 효과 숨기기
            //로딩 모달 활성화

            console.log("1-2. 로딩전 레이어 숨기기");
            window.wemb.mainPageComponent.hiddenMTTLayer();



            /*
		페이지 닫기
			- 단축키 비활성화
			- 에디트 모드 비활성화
			- 레이어 정보 초기화
			- 선택 정보 비활성화
			- 복사 정보 모두 삭제
		 */
            editorProxy.closedPage();

            try {
                  let pageId = note.getBody();
                  console.log("페이지 정보 읽기 시작, 페이지ID = ", pageId);
                  let pageData: OpenPageData = await pageApi.openPageById(pageId);
                  console.log("페이지 정보 읽기 완료");

                  // 신규로 읽어들인 페이지 정보를 최신 페이지 정보로 설정.
                  window.wemb.pageManager.currentPageInfo = pageData.page_info;
                  editorProxy.setPagePropertyInfo(pageData.page_info);

                  // 페이지 벼경 설정.
                  let bgInfo = window.wemb.mainPageComponent.getGroupProperties("setter.background");
                  stageProxy.setPageBackgroundInfo(bgInfo);




                  // 로딩 바 활성화
                  console.log("3-0. 화면에 붙일 컴포넌트 인스턴스 정보 생성, 시작");


                  console.log("3-2. 화면에 붙일 컴포넌트 인스턴스 정보 생성, 시작");




                  // 레이어별 컴포넌트 정보 생성

                  /*
			페이지 정보 설정
			* 컴포넌트 정보 생성
				* 마스터 레이어 정보 추가
				* 2D 레이어 정보 추가
				* 3D 레이어 정보 추가
			* 컴포넌트 인스턴스 생성
				* 마스터 정보 출력(단, 마스터 정바가 출력이 안된 경우)
				* 2D 정보 출력
				* 3D 정보 출력
			 */

                  let comInstanceInfoList: Array<any> = [];

                  // 마스터 컴포넌트 인스턴스 메타 정보 생성
                  this.isCreateMasterLayer = this.publicProxy.getCreateMasterLayer();
                  if (this.isCreateMasterLayer == false) {
                        // comInstanceInfoList = comInstanceInfoList.concat(this.publicProxy.getComInstanceInfoListInMasterLayer());
                        // 단 한번만 마스터 컴포넌트 로딩 처리를 위해 로딩 완료 상태를 true로 변경
                        this.publicProxy.completeCreateMasterLayer();
                  }

                  if(pageData.page_info.type !== 'master'){
                        this.publicProxy.setComInstanceInfoListInMasterLayer(pageData.content_info.master_layer);
                        comInstanceInfoList = comInstanceInfoList.concat(this.publicProxy.getComInstanceInfoListInMasterLayer());
                  }

                  // 2D 컴포넌트 인스턴스 메타 정보 생성
                  comInstanceInfoList = comInstanceInfoList.concat(pageData.content_info.two_layer);


                  /*
                  2018.11.04(ckkim)
                        3D 컴포넌트 인스턴스 메타 정보 생성
                        3D 레이어를 사용하는 경우에만 실행.
                   */
                  if(window.wemb.configManager.using3D==true)
                        comInstanceInfoList = comInstanceInfoList.concat(pageData.content_info.three_layer);

                  console.log("화면에 붙일 컴포넌트 인스턴스 정보 생성 완료, 개수 = ", comInstanceInfoList.length);
                  // 레이어별 컴포넌트 생성
                  window.wemb.$defaultLoadingModal.setMaxLength(comInstanceInfoList.length);




                  if(await window.wemb.hookManager.executeAction(HookManager.HOOK_AFTER_LOAD_PAGE,comInstanceInfoList)==false){
                        console.warn("페이지 오픈이 취소 되었습니다.");
                        //alert("페이지 저장이 취소 되었습니다.");
                        return;
                  }

                  window.wemb.$editMainAreaResourceLoadingComponent.increase(30, 'Open Page Loading...');

                  /***
                   * test resource loading ( webworker : only chrome)
                   */

                  /*
                  컴포넌트 인스턴스 생성 후
                  layer에 컴포넌트 인스턴스가 추가된 상태.
                  단, 이때 리소스는 로드되지 않은 시점
                  즉, READY 상태임.
                   */
                  console.log("3-4. 화면에 인스턴스 붙이기, 시작!");
                  let success =await editorProxy.startConvertingComponentInfoToComponentInstance(comInstanceInfoList);
                  console.log("3-5. 화면에 인스턴스 붙이기, 완료 = ", success);
                  window.wemb.$editMainAreaResourceLoadingComponent.increase(35, 'Resource Loading Start...');

                  function convertServerPath( str ){
                        if(str.indexOf("http")<= -1){
                              return wemb.configManager.serverUrl  + str;
                        }
                  }



                  const loadObj = function loadObj(objs){
                        console.time("OBJ")
                        if(objs.length === 0 ){
                              // 로드해 둘게 없으면 그냥 리소스 로딩을 진행한다.
                              this._startResourceLoading();
                        }else{
                              this.workerDirector = new THREE.LoaderSupport.WorkerDirector(THREE.OBJLoader2);
                              this.workerDirector.setLogging( false, false );
                              this.workerDirector.setCrossOrigin( 'anonymous' );
                              this.workerDirector.setForceWorkerDataCopy( true );

                              // 로드 될 때마다 불려지는 콜백
                              var callbackOnLoad = async function ( event ) {
                                    NLoaderManager.setMeshLoadedPool(event.detail.modelName, event.detail.loaderRootNode);
                                    window.wemb.$editMainAreaResourceLoadingComponent.increase(0, `Resource Loading...\n${this.cumilative}/${preps.length}`);
                                    this.cumilative += 1;

                                    if ( this.workerDirector.objectsCompleted + 1 === objs.length ) {
                                          this.workerDirector.tearDown();
                                          this.workerDirector = null;
                                          console.timeEnd("OBJ");
                                          console.time("OBJtexture")
                                          // 여기서 다음 과정인 텍스처 로드를 진행한다.
                                          let textures = await this._componentResourceLoader.createTexturePool();
                                          textures
                                                .filter((texture)=> texture.image !== undefined)
                                                .forEach((texture)=>{
                                                      NLoaderManager.registTexturePool(texture.image.src, texture);
                                                });

                                          this.cumilative = 0;
                                          console.timeEnd("OBJtexture")
                                          this._startResourceLoading();
                                    }
                              }.bind(this);

                              var callbacks = new THREE.LoaderSupport.Callbacks();
                              callbacks.setCallbackOnLoad(callbackOnLoad);

                              this.workerDirector.prepareWorkers(callbacks, objs.length, 16);

                              objs.forEach((pr)=>{
                                    this.workerDirector.enqueueForRun(pr);
                              });
                              this.workerDirector.processQueue();
                        }
                  }.bind(this);
                  if(success){
                        this._componentResourceLoader = new ComponentResourceLoader();
                        let preps = this._componentResourceLoader.prepObjLoad(editorProxy.comInstanceList);
                        let objs = preps.filter((prep)=> prep.modelName.includes('.obj'))
                        let gltf = preps.filter((prep)=> prep.modelName.includes('.gltf'))
                        let gltfCnt = 0;
                        if(!gltf.length) loadObj(objs);
                        else{
                              console.time("GLTF");
                              for(let i = 0 ; i < gltf.length; i++){
                                    let loader = new THREE.GLTFLoader();
                                    loader.setCrossOrigin('anonymous');
                                    loader.setDRACOLoader(new THREE.DRACOLoader());
                                    const url =  gltf[i].resources[0].url;
                                    loader.load(url, function(data){
                                          NLoaderManager.setMeshLoadedPool(url, data.scene);
                                          gltfCnt++;
                                          if(gltfCnt === gltf.length) {
                                                console.timeEnd('GLTF');
                                                loadObj(objs);
                                          }
                                    }.bind(this))
                              }
                        }
                  }else {
                        console.log("컴포넌트 생성이 정상적으로 이뤄지지 않았습니다.")
                  }

            }catch(error){
                  console.log("페이지 열기 후, 컴포넌트 생성 시 에러 발생", error);
            }
	}

      private async _startResourceLoading(){
            console.log("4-2. 컴포넌트 리소스 읽기, 시작");
            let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            let $resourceEventBus = new Vue();
            let comInstanceListMap:Map<string, WVComponent> = new Map();

            this._componentResourceLoader = new ComponentResourceLoader();
            this._componentResourceLoader.$on(ComponentResourceLoaderEvent.COMPLETED_ALL, ()=>{
                  this._completedLoadAllResource();
                  // 로드가 다 되면 메쉬 + 텍스처 가 convertServerPath된 path를 key로 저장되어 있기 때문에
                  // 메쉬와 텍스처를 담아 두었던 풀은 지운다.
                  // NLoaderManager.clearMeshTextureLoaderPool();
            });
            this._componentResourceLoader.startResourceLoading(editorProxy.comInstanceList);
      }

      /*
      리소스가 모두 로디된 후

      1. 리소스 로더 파괴

      2. 메인 페이지 컴포넌트 로딩 완료 유무 처리하기

      3. 일반 레이어 컴포넌트인 경우, 컴포넌트 ON_LAOAD_PAGE() 메서드 호출

      4. 마스터 컴포넌트인경우, 이미 마스터 컴포넌트가 존재하는 경우
            마스터 컴포넌트.ON_OPEN_PAGE() 실행

      5. 한번에 출력되는 효과를 구현하기 위해
            숨긴 레이어 활성화+

       */
      private async _completedLoadAllResource(){
            //리소스 다 불러옴
            console.log("4-2. 컴포넌트 리소스 읽기, 완료");

            // 리로스 로더 파괴
            if(this._componentResourceLoader) {
                  this._componentResourceLoader.destroy();
                  this._componentResourceLoader = null;
            }

            console.log("4-3. 화면에 배치된 모든 컴포넌트에 complete wscript 이벤트 발생");
            console.log("4-4. 화면에 배치된 모든 컴포넌트의 onLoadPage 메서도 호출");





            // onLoadPage() 메서드 호출하기
            /*
            페이지가 처음 호출되는 경우
                  마스터,2D,3D의 컴포넌트의 onLoadPage() 메서드 호출
            아닌 경우
                  2D,3D 컴포넌트의 onLoadPage() 메서드 호출
             */
            let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            editorProxy.comInstanceList.forEach((instance:WVComponent)=>{
                  // 마스터가 로드되지 않은 상태인경우 마스터 실행
                  if(this.isCreateMasterLayer==true)
                        if(instance.layerName==WORK_LAYERS.MASTER_LAYER)
                              return;

                  // onLoadPage를 가지고 있는 컴포넌트만 onLoadPage()메서드 호출
                  if(instance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]){
                        instance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]();
                  }
            });



            /*
             2018.10.29(ckkim)
            마스터 컴포넌트의 onLoadPage() 메서드 호출 하기
            마스터 컴포넌트가 이미 생성된 경우이기 때문에 onOpenPage()만 호출
             */
            if(this.isCreateMasterLayer==true){
                  let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
                  editorProxy.masterLayerInstanceList.forEach((instance:WVComponent)=>{

                        // onLoadPage를 가지고 있는 컴포넌트만 onOpenPage()메서드 호출
                        if(instance[WVCOMPONENT_METHOD_INFO.ON_OPEN_PAGE]){
                              instance[WVCOMPONENT_METHOD_INFO.ON_OPEN_PAGE]();
                        }
                  });
            }




            /*
                  2018.11.21(ckkim)
                  컴포넌트 활성화(보이게) 전에 훅 호출

             */
            if(await window.wemb.hookManager.executeAction(HookManager.HOOK_BEFORE_ACITVE_PAGE)==false){
                  console.warn("페이지 활성화 전에 에러 발생");
                  return;
            }

            if(await window.wemb.hookManager.executeAction(HookManager.HOOK_BEFORE_OPEN_PAGE)==false){
                  return;
            }

            /*
		    2018.09.14
		    Master, Two, Three Layer 보이기
		     페이지 로딩 후 모든 내용을 한번에 출력되는 효과를 만들기 위해 사용
		     주의:
		      타이머를 사용한 이유는?
		            - 컴포넌트가 보이는 도중에 layer가 활성화 되는 경우 "틱!" 튀기는 현상이 발생함.
		            - 이를 막기 위해 타이머 사용.

	     */
            window.wemb.$editMainAreaResourceLoadingComponent.increase(35, 'Completed!');

            setTimeout(async ()=>{
                  // 메인 페이지 컴포넌트 로딩 완료 유무 처리하기
                  window.wemb.mainPageComponent.isLoaded=true;
                  window.wemb.mainPageComponent.setLoadingState(false);
                  // 로딩바 숨기기
                  window.wemb.mainPageComponent.showMTTLayer();
                  window.wemb.$editMainAreaResourceLoadingComponent.hide();
                  this.sendNotification(EditorProxy.NOTI_OPENED_PAGE);
                  if(await window.wemb.hookManager.executeAction(HookManager.HOOK_AFTER_ACITVE_PAGE)==false){
                        console.warn("페이지 활성화 후 에러 발생");
                        return;
                  }
            },500);

            console.log("모든 컴포넌트 onLoadPage() 메서드 호출");
      }

}
