import * as puremvc from "puremvc";

export class ShowRestoreProjectCommand  extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            window.wemb.$restoreProjectModal.show();
      }
}
