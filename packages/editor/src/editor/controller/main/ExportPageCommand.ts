import Vue from "vue";
import * as puremvc from "puremvc";
import {SavePageData} from "../../../wemb/wv/page/Page";
import EditorProxy from "../../model/EditorProxy";
import StageProxy from "../../model/StageProxy";
import FileManager from "../../../wemb/wv/managers/FileManager";


/*
페이지 생성 팝업.
 */

export class ExportPageCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            let stageProxy: StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
            let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            if(editorProxy.isOpenPage==false){
                  console.log("페이지가 열리지 않은 상태에서 실행")
                  return;
            }

            // 페이지 저장 데이터 생성
            let savePageData:SavePageData = new SavePageData();

            savePageData.data.page_info = editorProxy.mainPageComponent.serializePageMetaProperties();



            // 마스터 정보 저장.
            savePageData.data.master_info.background = stageProxy.backgroundInfo.stage;
            savePageData.data.stage_info.background = stageProxy.backgroundInfo.master;

            // 레이어 정보 저장
            editorProxy.masterLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.master_info.master_layer.push(comInstance.serializeMetaProperties());
            });

            // 2D 영역
            editorProxy.twoLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.content_info.two_layer.push(comInstance.serializeMetaProperties());
            });

            //3D 영역
            editorProxy.threeLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.content_info.three_layer.push(comInstance.serializeMetaProperties());
            });

            console.log("SavePageCommand 저장 전 데이터 = ", JSON.stringify(savePageData));


            // 파일 저장하기
            let fileManager = new FileManager();
            let pageName:string = savePageData.data.page_info["name"];

            let temp:any  = $.extend(true, {}, savePageData);
            delete temp.master_info;

            let json = {"page_info_list":[ temp.data ]};
            
            fileManager.saveTextFile( pageName + ".json", JSON.stringify(json));
            fileManager.onDestroy();
	}
}
