import Vue from "vue";
import * as puremvc from "puremvc";

import EditorProxy from "../../model/EditorProxy";

import {SavePageData} from "../../../wemb/wv/page/Page";
import pageApi from "../../../common/api/pageApi";
import StageProxy from "../../model/StageProxy";
import PageInfoProperties from "../../../wemb/wv/components/PageInfoProperties";


export class NewPageSaveCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		console.log("NewSavePageCommand 실행, page id = ", note.getBody());
            let pageInfoProperties:PageInfoProperties = note.getBody() as PageInfoProperties;

            // 페이지 메타 정보에서 이름 존재유무 판단.
            pageInfoProperties.name = pageInfoProperties.name.trim();
            if (pageInfoProperties.name.length == 0) {
                  //console.log("#### 페이지 이름이 설정되지 않았습니다..");
                  let promise = new Promise((resolve, reject) => {
                        reject("페이지 이름이 설정되지 않았습니다.!!!");
                  });
                  return promise;
            }


            let savePageData:SavePageData = new SavePageData();

            /*
            신규 페이지 정보 생성
             */
            let editorProxy:EditorProxy = this.facade.retrieveProxy(EditorProxy.NAME) as EditorProxy;
            savePageData.data.page_info = pageInfoProperties.serializeMetaProperties();



            // 마스터 정보 저장.
            savePageData.data.master_info.background = editorProxy.stageBackgroundInfo;
            savePageData.data.stage_info.background = editorProxy.masterBackgroundInfo;

            // 레이어 정보 저장
            editorProxy.masterLayerInstanceList.forEach((comInstance) => {

                  savePageData.data.master_info.master_layer.push(comInstance.serializeMetaProperties());
            });




            console.log("ACTION_MAIN_NEW_PAGE_SAVE 페이지 생성 시작");
            console.log("ACTION_MAIN_NEW_PAGE_SAVE 페이지 생성 시작", savePageData.getJSON());

            var locale_msg = Vue.$i18n.messages.wv;
            // 2. 서버에 신규 페이지 생성하기
            pageApi.saveNewPage(savePageData).then((result) => {
                  if (result.data.result != "ok") {
                        Vue.$alert('error code: []<br>'+locale_msg.common.errorSave+' '+locale_msg.common.contact, locale_msg.common.error, {
                              confirmButtonText: 'OK',
                              type: "error",
                              dangerouslyUseHTMLString: true
                        });

                        return;
                  }

                  /* 테스트 용 */
                  if (window.wemb.configManager.test) {
                        pageInfoProperties.id = "page_000" + (window.wemb.pageManager.getPageLength() + 1);
                  }


                  /*
                  페이지 닫기
                        단축키 비활성화
                        편집 비활성화
                        수정 상태를 false로
                        기존 편접 정보 무두 삭제
                        선택 정보 삭제
                        복사 정보 삭제
             */
                  editorProxy.closedPage();



                  //3. 페이지 트리 관리자에 신규 페이지 추가
                  console.log("ACTION_MAIN_NEW_PAGE_SAVE 생성시작!!!!")
                  //window.wemb.pageTreeManager.addPage(pageInfoProperties.id, pageInfoProperties.name);
                  //window.wemb.pageManager.addPageInfo(savePageData.data.page_info);
                  console.log("ACTION_MAIN_NEW_PAGE_SAVE 생성시작 완료");



                  // //4. 신규 페이지 정보를 페이지 컴포넌트에 적용하기
                   window.wemb.pageManager.currentPageInfo = savePageData.data.page_info;
                   editorProxy.setPagePropertyInfo(savePageData.data.page_info);

                  let bgInfo = window.wemb.mainPageComponent.getGroupProperties("setter.background");


                  let stageProxy:StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
                  stageProxy.setPageBackgroundInfo(bgInfo);

                  /*
                  completePageReady() 내부에 아래와 같은소스가 들어 잇음.
                  this.setPageOpenState(true)
                  this.setEditable(true);
                  this.setActiveShortcut(true);


                  this.sendNotification(EditorProxy.NOTI_OPEN_PAGE);
                  this.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, window.wemb.configManager.startWorkLayerName);
                   */
                  editorProxy.completePageReady();
                  this.sendNotification(EditorProxy.NOTI_OPENED_PAGE);
                  window.wemb.$createPageModal.hide();



            });

	}
}
