import * as puremvc from "puremvc";
import EditorStatic from "../editor/EditorStatic";
import PageTreePanelMediator from "../../view/panel/pageTreePanel/PageTreePanelMediator";

export class OpenPageByIdCommand extends puremvc.SimpleCommand {
    execute(note: puremvc.INotification) {
        
        let pageTreePanelMediator:PageTreePanelMediator = <PageTreePanelMediator>this.facade.retrieveMediator(PageTreePanelMediator.NAME);
        let pageId = pageTreePanelMediator.getOpenTargetPageId();

        if(!pageId) return;

        this.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageId);
    }
}
