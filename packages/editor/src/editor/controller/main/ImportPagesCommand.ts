import Vue from "vue";
import * as puremvc from "puremvc";
import {SavePageData} from "../../../wemb/wv/page/Page";
import EditorProxy from "../../model/EditorProxy";
import StageProxy from "../../model/StageProxy";
import FileManager from "../../../wemb/wv/managers/FileManager";
import {ObjectUtil} from "../../../wemb/utils/Util";
import pageApi from "../../../common/api/pageApi";
import EditorStatic from "../editor/EditorStatic";


/*
페이지 생성 팝업.
 */

export class ImportPagesCommand  extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            let fileManager = new FileManager();
            fileManager.loadJsonFile((success, str) => {
                  if (success == true) {
                        try{
                              let data = JSON.parse(str);
                              let validData = this.convertToNewFormat(data);

                              if (this.existNotPageInfoList(validData)) {
                                    Vue.$message.error(Vue.$i18n.messages.wv.importPages.noPages);
                                    return;
                              }

                              window.wemb.$importPagesModal.show(validData);
                        }catch(error){
                              Vue.$message.error(Vue.$i18n.messages.wv.common.noData);
                        }
                  }
            });
      }

      convertToNewFormat(data){
            if(data.data && data.data.page_info && data.data.content_info){
                  return {"page_info_list": [data.data]};
            }else{
                  return data;
            }
      }

      existNotPageInfoList(validData) {
            return !validData.page_info_list;
      }
}
