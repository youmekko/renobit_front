import * as puremvc from "puremvc";

import EditorProxy from "../../model/EditorProxy";
import {SavePageData} from "../../../wemb/wv/page/Page";
import pageApi from "../../../common/api/pageApi";
import Vue from "vue";
import StageProxy from "../../model/StageProxy";
import EditorStatic from "../editor/EditorStatic";
import {IComponentMetaProperties} from "../../../wemb/core/component/interfaces/ComponentInterfaces";
import {ObjectUtil} from "../../../wemb/utils/Util";
import PageInfoProperties from "../../../wemb/wv/components/PageInfoProperties";


export class SaveAsPageCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {


		/*

		로딩 모달 활성화
		단축키 비활성화
		편집 비활성화
		페이지 열기 시작
			페이지 정보 유효성 검사
			기존 편집 정보 모두 삭제

			페이지 프로퍼티 정보 설정

			레이어별 컴포넌트 정보 생성
				- 마스터 레이어 컴포넌트 정보 생성(단, 마스터 정보가 출력이 안된 경우)
				- 2D 레이어 컴포넌트 정보 생성
				- 3D 레이어 컴포넌트 정보 생성

			레이어별 컴포넌트 생성
				- 마스터 레이어 컴포넌트 생성(단, 마스터 정보가 출력이 안된 경우)
				- 2D 레이어 컴포넌트 생성
				- 3D 레이어 컴포넌트 생성


			* 레이어 활성화(환경설정 값으로)

			* 컴포넌트 목록은 현재 활성화된 레이어에 배치할 수 있는 컴포넌트 목록이 출력
			* 프로퍼티는 페이지 프로퍼티 정보가 출력
			* 스테이지에는 현재 스테이지 정보가 출력
			* 아웃라인에는 현재 활성화된 레이어의 배치 정보가 출력



		 */

            let pageInfoProperties:PageInfoProperties = note.getBody() as PageInfoProperties;

            // 페이지 메타 정보에서 이름 존재유무 판단.
            pageInfoProperties.name = pageInfoProperties.name.trim();
            if (pageInfoProperties.name.length == 0) {
                  console.log("#### 페이지 이름이 설정되지 않았습니다..");
                  let promise = new Promise((resolve, reject) => {
                        reject("페이지 이름이 설정되지 않았습니다.!!!");
                  });
                  return promise;
            }





		let stageProxy: StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            if(editorProxy.isOpenPage==false){
                  console.log("페이지가 열리지 않은 상태에서 실행")
                  return;
            }


            // 페이지 저장 데이터 생성
            let savePageData:SavePageData = new SavePageData();
            savePageData.data.page_info = pageInfoProperties.serializeMetaProperties();


            // 마스터 정보 저장.
            savePageData.data.master_info.background = stageProxy.backgroundInfo.stage;
            savePageData.data.stage_info.background = stageProxy.backgroundInfo.master;

            // 레이어 정보 저장
            editorProxy.masterLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.master_info.master_layer.push(comInstance.serializeMetaProperties());
            });

            // 2D 영역
            editorProxy.twoLayerInstanceList.forEach((comInstance) => {
                  let metaProperties:IComponentMetaProperties = comInstance.serializeMetaProperties();
                  metaProperties.id = ObjectUtil.generateID();
                  savePageData.data.content_info.two_layer.push(metaProperties);
            });

            //3D 영역
            editorProxy.threeLayerInstanceList.forEach((comInstance) => {
                  let metaProperties:IComponentMetaProperties = comInstance.serializeMetaProperties();
                  metaProperties.id = ObjectUtil.generateID();
                  savePageData.data.content_info.three_layer.push(metaProperties);
            });


            window.wemb.$createPageModal.hide();

            var locale_msg = Vue.$i18n.messages.wv;
            window.wemb.pageTreeDataManager.addPage( pageInfoProperties.name, savePageData.data )
            .then((result) => {

                  Vue.$message({
                        showClose: true,
                        message: locale_msg.common.successAdd,
                        type: 'success'
                  });
                  //window.wemb.pageManager.addPageInfo(savePageData.data.page_info);
                  this.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageInfoProperties.id);

            }).catch((error)=>{
                  Vue.$alert('error code: []<br>'+locale_msg.common.errorSave+' '+locale_msg.common.contact, locale_msg.common.error, {
                        confirmButtonText: 'OK',
                        type: "error",
                        dangerouslyUseHTMLString: true
                  });

                  editorProxy.setEditable(true);
                  editorProxy.setActiveShortcut(true);
            });
            /*
            페이지 저장 실행
            pageApi.saveNewPage(savePageData).then((result:any)=>{
                  if (result.data.result != "ok") {

                        Vue.$alert('error code: []<br>'+locale_msg.common.errorSave+' '+locale_msg.common.contact, locale_msg.common.error, {
                              confirmButtonText: 'OK',
                              type: "error",
                              dangerouslyUseHTMLString: true
                        });

                        editorProxy.setEditable(true);
                        editorProxy.setActiveShortcut(true);

                        return;
                  }


                  window.wemb.pageTreeManager.addPage(pageInfoProperties.id, pageInfoProperties.name);
                  window.wemb.pageManager.addPageInfo(savePageData.data.page_info);
                  this.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageInfoProperties.id);
            })
             */


      }
}
