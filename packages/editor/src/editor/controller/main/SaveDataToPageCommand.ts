import * as puremvc from "puremvc";

import EditorProxy from "../../model/EditorProxy";
import {SavePageData} from "../../../wemb/wv/page/Page";
import pageApi from "../../../common/api/pageApi";
import Vue from "vue";
import StageProxy from "../../model/StageProxy";
import EditorStatic from "../editor/EditorStatic";
import {IComponentMetaProperties} from "../../../wemb/core/component/interfaces/ComponentInterfaces";
import {ObjectUtil} from "../../../wemb/utils/Util";
import PageInfoProperties from "../../../wemb/wv/components/PageInfoProperties";


export class SaveDataToPageCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {


            let saveDataOption: any = note.getBody();

            // 기본값은 true
            let isNewPage: boolean = true;
            if (saveDataOption.hasOwnProperty("newPage") == true) {
                  if(!saveDataOption.newPage) {
                        isNewPage = false;
                  }
            }


            if (saveDataOption.hasOwnProperty("pageData") == false) {
                  alert("페이지 정보가 존재하지 않거나 형식이 맞질 않습니다.");
                  return;
            }

            if(isNewPage) {
                  this._executeNewPage(saveDataOption.pageData)
            }else {
                  this._executeSavePage(saveDataOption.pageData)
            }


      }

      private _executeNewPage(pageData){





            let newName = this.getNewPageName(pageData.page_info.name);


            let newId =  ObjectUtil.generateID();
            // 페이지 저장 데이터 생성
            let savePageData:SavePageData = new SavePageData();
            savePageData.data.page_info = pageData.page_info;
            savePageData.data.page_info.name= newName;
            savePageData.data.page_info.id = newId;




		let stageProxy: StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            if(editorProxy.isOpenPage==false){
                  console.log("페이지가 열리지 않은 상태에서 실행")
                  return;
            }


            // 마스터 정보 저장.
            savePageData.data.master_info.background = stageProxy.backgroundInfo.stage;
            savePageData.data.stage_info.background = stageProxy.backgroundInfo.master;

            // 레이어 정보 저장
            editorProxy.masterLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.master_info.master_layer.push(comInstance.serializeMetaProperties());
            });

            // 2D 영역
            pageData.content_info.two_layer.forEach((comInstance) => {
                  comInstance.id = ObjectUtil.generateID();
            });

            //3D 영역
            pageData.content_info.three_layer.forEach((comInstance) => {
                  comInstance.id = ObjectUtil.generateID();
            });


            savePageData.data.content_info.two_layer = pageData.content_info.two_layer;
            savePageData.data.content_info.three_layer = pageData.content_info.three_layer;



            window.wemb.pageTreeDataManager.addPage( newName, savePageData.data )
            .then((result) => {
                  this.sendNotification(EditorProxy.NOTI_SAVED_PAGE, newId);
                  this.sendNotification(EditorStatic.CMD_OPEN_PAGE, newId);

            }).catch((error)=>{
                  var locale_msg = Vue.$i18n.messages.wv;
                  Vue.$alert('error code: []<br>'+locale_msg.common.errorSave+' '+locale_msg.common.contact, locale_msg.common.error, {
                        confirmButtonText: 'OK',
                        type: "error",
                        dangerouslyUseHTMLString: true
                  });

                  editorProxy.setEditable(true);
                  editorProxy.setActiveShortcut(true);
            });

      }

      private _executeSavePage(pageData){



            // 페이지 저장 데이터 생성
            let savePageData:SavePageData = new SavePageData();




            let stageProxy: StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
            let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            savePageData.data.page_info = editorProxy.mainPageComponent.serializePageMetaProperties();


            // 마스터 정보 저장.
            savePageData.data.master_info.background = stageProxy.backgroundInfo.master;
            savePageData.data.master_info.scripts  = stageProxy.getScripts();
            savePageData.data.stage_info.background = stageProxy.backgroundInfo.stage;



            // 마스터 정보 저장.
            savePageData.data.master_info.background = stageProxy.backgroundInfo.stage;
            savePageData.data.stage_info.background = stageProxy.backgroundInfo.master;

            // 레이어 정보 저장
            editorProxy.masterLayerInstanceList.forEach((comInstance) => {
                  savePageData.data.master_info.master_layer.push(comInstance.serializeMetaProperties());
            });

            // 2D 영역
            pageData.content_info.two_layer.forEach((comInstance) => {
                  comInstance.id = ObjectUtil.generateID();
            });

            //3D 영역
            pageData.content_info.three_layer.forEach((comInstance) => {
                  comInstance.id = ObjectUtil.generateID();
            });


            savePageData.data.content_info.two_layer = pageData.content_info.two_layer;
            savePageData.data.content_info.three_layer = pageData.content_info.three_layer;

            let pageId = savePageData.data.page_info.id;

            pageApi.checkExistPage( pageId ).then((bool)=> {
                  /*
		     페이지 저장 실행
		     */
                  pageApi.savePage(savePageData).then(async (result: any) => {
                        console.log("SavePageCommand 완료 데이터 ", result);
                        var locale_msg = Vue.$i18n.messages.wv;
                        if (result.data.result != "ok") {

                              Vue.$alert('error code: []<br>'+locale_msg.common.errorSave+' '+locale_msg.common.contact, locale_msg.common.error, {
                                    confirmButtonText: 'OK',
                                    type: "error",
                                    dangerouslyUseHTMLString: true
                              });

                              editorProxy.setEditable(true);
                              editorProxy.setActiveShortcut(true);

                              return;
                        }



                        Vue.$message({
                              showClose: true,
                              message: locale_msg.common.successSave,
                              type: 'success'
                        });

                        editorProxy.setEditable(true);
                        editorProxy.setActiveShortcut(true);
                        editorProxy.setModified(false);

                        this.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageId);


                  })
            }).catch( (error)=>{

                  var locale_msg = Vue.$i18n.messages.wv;
                  Vue.$alert('error code: []<br>'+locale_msg.common.errorSave+' '+locale_msg.common.contact, locale_msg.common.error, {
                        confirmButtonText: 'OK',
                        type: "error",
                        dangerouslyUseHTMLString: true
                  });

                  editorProxy.setEditable(true);
                  editorProxy.setActiveShortcut(true);
            });

      }

      /*
      페이지가 이미 존재하는 경우 _*(숫자) 식으로 신규 이름 만들기.
       */
      getNewPageName(newPageName){
            let maxPageCount=0;
            // 페이지 이름이 존재하는 경우
            // 최대 page count 개수를 구하기
            if(window.wemb.pageManager.hasPageInfoByName(newPageName)==true){


                  window.wemb.pageManager.getPageInfoList().forEach((pageInfo)=>{
                        console.log("pageInfo.name ", pageInfo.name);

                        // 동일한 페이지 인 경우 패스
                        if(newPageName==pageInfo.name)
                              return;

                        if(pageInfo.name.indexOf(newPageName)!=-1){

                              let index = pageInfo.name.lastIndexOf("_");
                              console.log("lastIndexOf =  ", index);
                              if(index !=-1){
                                    let pageCount = parseInt(pageInfo.name.slice(index+1, pageInfo.name.length));
                                    console.log("pageCount  =  ", pageCount, maxPageCount);
                                    if(maxPageCount<pageCount){
                                          maxPageCount = pageCount;
                                    }
                              }

                        }
                  })

                  newPageName = `${newPageName}_${maxPageCount+1}`;
            }


            return newPageName;
      }
}
