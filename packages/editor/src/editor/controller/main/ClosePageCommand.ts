import * as puremvc from "puremvc";

import EditorProxy from "../../model/EditorProxy";


/*
페이지를 닫고 비활성화 상태로만 만들기.
 */
export class ClosePageCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		console.log("openpage, ClosePageCommand 실행, page id = ", note.getBody());

            window.wemb.mainPageComponent.isLoaded=false;
		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);


		/*
		페이지 닫기
			- 단축키 비활성화
			- 에디트 모드 비활성화
			- 레이어 정보 초기화
			- 선택 정보 비활성화
			- 복사 정보 모두 삭제
		 */
		editorProxy.closedPage();




	}
}
