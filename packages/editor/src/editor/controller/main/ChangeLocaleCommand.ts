import * as puremvc from "puremvc";

import EditorProxy from "../../model/EditorProxy";
import {EDITOR_APP_STATE} from "../../../wemb/wv/data/Data";

/*
페이지를 닫고 비활성화 상태로만 만들기.
 */
export class ChangeLocaleCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		console.log("openpage, ChangeLocaleCommand 실행");

		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);

		editorProxy.updateAppState(EDITOR_APP_STATE.LOCALE_CHANGED);
	}
}
