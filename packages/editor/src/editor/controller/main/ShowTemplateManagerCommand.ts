import * as puremvc from "puremvc";

export class ShowTemplateManagerCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		window.wemb.$templateManagerModal.show('', true);
	}
}
