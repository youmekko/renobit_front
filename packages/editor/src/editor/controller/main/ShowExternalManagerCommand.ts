import * as puremvc from "puremvc";

import {POPUP} from "../../../wemb/wv/managers/WindowsManager";
import EditorProxy from "../../model/EditorProxy";
import ExternalWindowManager from "../../../wemb/wv/managers/ExternalWindowManager";



/*
외부 관리자 활성화 command
- 외부 관리자 실행 위치는 모두 packName/main/index.html
 */
export class ShowExternalManagerCommand  extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            console.log("@@ note 4", note.getBody());

            ExternalWindowManager.getInstance().open({
                  ORIGIN:true,
                  NAME:"tet1",
                  URL:note.getBody(),
                  SIZE:"width=1000,height=800"
            });

            /*
            window["tempWindow"]=window.open(note.getBody(), "_blank", "left=0,top=0,scrollbars=1,resizable=1");
            //window.wemb.windowsManager.open(POPUP.SCRIPT_EDITOR);

            let proxy = this.facade.retrieveProxy(EditorProxy.NAME) as EditorProxy;

            setTimeout(()=>{
            console.log("@@ note 4 window ", window["tempWindow"]);
            console.log("@@ note 5 window ", window["tempWindow"].testCall);
            window["tempWindow"].testCall(proxy.comInstanceList)
            },3000)
            */
      }
}
