import * as puremvc from "puremvc";


export class NewPageGroupCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            let groupName:string = note.getBody() as string;
           //window.wemb.pageTreeManager.addPageGroup( groupName );
           window.wemb.pageTreeDataManager.addPageGroup( groupName );
           
	}
}
