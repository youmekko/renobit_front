import Vue from "vue";
import * as puremvc from "puremvc";
import {POPUP} from "../../../wemb/wv/managers/WindowsManager";



export class ShowMultilingualManagerCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
			let url = location.origin + location.pathname + "#/multilingualEditor";
            window.open(url, "_blank");
	}
}
