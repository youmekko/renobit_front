import * as puremvc from "puremvc";

export class ShowShortcutsInfoCommand extends puremvc.SimpleCommand {
    execute(note: puremvc.INotification) {
        console.log("ShowShortcutsInfoCommand !");

        window.wemb.$shortcutsInfoModal.show();
    }
}
