import * as puremvc from "puremvc";



/*
페이지를 닫고 비활성화 상태로만 만들기.
 */
export class GotoViewerByNameCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            //viewerURL += "&lang=" + localStorage.getItem("__renobit_locale__");
	      let pageName:string =  note.getBody();

            let viewerURL = wemb.configManager._getFullPathName() + "/visualViewer.do#/pname=" + pageName;

            // window focus를 위한 타임아웃
            setTimeout(function() {
                window.open(viewerURL, "_blank");
            },100)
	}
}
