import Vue from "vue";
import * as puremvc from "puremvc";

export class ShowExportPagesModalCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {       
		window.wemb.$exportPagesModal.show();
	}
}
