import * as puremvc from "puremvc";

export class ShowExportResourcesManagerCommand  extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
		window.wemb.$exportResourceManagerModal.show();
	}
}
