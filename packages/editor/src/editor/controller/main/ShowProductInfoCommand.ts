import * as puremvc from "puremvc";

export class ShowProductInfoCommand extends puremvc.SimpleCommand {

    execute(note: puremvc.INotification) {
        console.log("ShowProductInfoCommand !");
        window.wemb.$productInfoModal.show();
    }

}
