import * as puremvc from "puremvc";


export class GotoAdminCommand extends puremvc.SimpleCommand{
    execute(note: puremvc.INotification) {
        let viewerURL = window.wemb.configManager.serverUrl + "/manager.do";
        window.open(viewerURL, "_blank");
    }
}
