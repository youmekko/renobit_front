import * as puremvc from "puremvc";
import Vue from "vue";
import EditorStatic from "../editor/EditorStatic";
import PageTreePanelMediator from "../../view/panel/pageTreePanel/PageTreePanelMediator";

/*
페이지 생성 팝업.
 */

export class DeletePageTreeItemsCommand  extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            let pageTreePanelMediator:PageTreePanelMediator = <PageTreePanelMediator>this.facade.retrieveMediator(PageTreePanelMediator.NAME)
            let ids = pageTreePanelMediator.getSelectedTreeIds();
            let treeItems = [];

            for( let index in ids ){
                let id = ids[index];
                let item = window.wemb.pageTreeDataManager.treeData.find(x=>x.id == id);
                treeItems.push( {type: item.type, id: id} );
            }
            
            if(!treeItems || !treeItems.length){
                  Vue.$message(Vue.$i18n.messages.wv.page.noDeletePage);
                  return;
            }

            this.confirmDeletePages(treeItems);
      }

      private confirmDeletePages(treeItems){
            let locale_msg = Vue.$i18n.messages.wv;           
            if (treeItems.length) {
                  let msg = "";
                  if (treeItems.length > 1) {
                        msg = locale_msg.page.warningDelete + " ";
                  }
                  msg += locale_msg.pageTree.confirmDelete;

                  let self = this;
                  Vue.$confirm( msg, {
                        confirmButtonText: locale_msg.common.delete,
                        cancelButtonText: locale_msg.common.cancel,
                        type: 'warning'
                  }).then(() => {                        
                        this.removeTreeItems( treeItems );
                  }).catch(() => {
                        
                  });
            } else {
                  Vue.$message({
                        type: 'warning',
                        message: locale_msg.page.unkownPage
                  });
            }

      }

      private removeTreeItems( items ){

            const loading = Vue.$loading({
                  lock: true,
                  text: "Delete Tree Processing...",
                  spinner: 'el-icon-loading',
                  background: 'rgba(255, 255, 255, 0.7)'
            });

            let locale_msg = Vue.$i18n.messages.wv;
            window.wemb.pageTreeDataManager.removeTreeItems(items).then((result)=>{

                  Vue.$message({
                        showClose: true,
                        message: locale_msg.common.successDelete,
                        type: 'success'
                  });

                  var currentPageInfo = window.wemb.pageManager.currentPageInfo;
                  if(currentPageInfo.hasOwnProperty("master") && currentPageInfo.master){
                        //현재 페이지의 Master 페이지가 삭제 리스트에 존재한다면 화면 새로 고침. 
                        var usedMasterPage = items.find(deletePage => deletePage.type == 'master' && deletePage.id == currentPageInfo.master);
                        if(usedMasterPage){
                              this.sendNotification(EditorStatic.CMD_OPEN_PAGE, currentPageInfo.id);
                        }
                  }
            }).finally(() => {
                  loading.close();
            });;
      }
      /*
      페이지 삭제에 따른 다음 작업 진행
      1. 페이지가 0이 되는 경우

      2. 열려있는 페이지가 삭제되는 경우
      */
      private updatePageList(removedItems) {
            var locale_msg = Vue.$i18n.messages.wv;                  
            let pageIds: Array<string> = removedItems.map(x=>x.id);
            //window.wemb.pageTreeDataManager.getPageIds();
            // 페이지 삭제 후 페이지가 0인 경우
            if(pageIds.length == 0){
            //if (window.wemb.pageManager.getPageLength() == 0) {
                  this.sendNotification(EditorStatic.CMD_CLOSE_PAGE);

                  setTimeout(()=>{
                        alert(locale_msg.page.noPage + locale_msg.page.newCreate);
                  },100);
                  return;
            }
            // 삭제 페이지가 포함되어 있는 경우!!!! 0번째 페이지 열기
            // 삭제 페이지가 현재 열린 페이지인 경우 마지막 페이지를 자동으로 열기
            if(window.wemb.pageManager.currentPageInfo==null){
                  console.log("트리에서 페이지 목록 삭제 후 처리 부분, 열려있는 페이지가 삭제되는 경우 자동으로 0번째 페이지를 열게되는데, 이때 열려있는 페이지가 존재 하지 않아 처리 할 수 없음.");
                  return;
            }

            
            let findId = pageIds.find((x) => x == window.wemb.pageManager.currentPageInfo.id);
            if (findId) {
                  //let pageId = window.wemb.pageManager.getPageIdByIndex(0);
                  let pageId = window.wemb.configManager.getStartPageId();
                  if (pageId != null) {
                        console.log("페이지 삭제 후 열게되는 페이지 ID= ", pageId);
                        this.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageId);
                  } else {
                        this.sendNotification(EditorStatic.CMD_CLOSE_PAGE);
                        alert(Vue.$i18n.messages.wv.page.noPage);
                  }
            }
      } 
}
