import * as puremvc from "puremvc";
import Vue from "vue";
import EditorStatic from "../editor/EditorStatic";
import EditorProxy from "../../model/EditorProxy";
import PageInfoProperties from "../../../wemb/wv/components/PageInfoProperties";


export class DeletedCurrentPageCommand  extends puremvc.SimpleCommand {
      execute(note: puremvc.INotification) {
            let editorProxy:EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
            let pageComponentProperties = editorProxy.mainPageComponent.properties;

            let pageInfoProperties: PageInfoProperties = new PageInfoProperties();
            pageInfoProperties.setPageInfoProperties(pageComponentProperties);

            // 그리드 정보 가져오기
            let gridSizeInfo = window.wemb.mainPageComponent.getDefaultProperties().setter.threeLayer.grid;

            pageInfoProperties.grid = {
                  x: gridSizeInfo.x,
                  z: gridSizeInfo.z
            }
5
            Vue.$confirm(Vue.$i18n.messages.wv.page.deletedCurrentPageNoti, 'Notification', {
                  showClose: false,
                  closeOnClickModal: false,
                  closeOnPressEscape: false,
                  confirmButtonText: 'Export page',
                  cancelButtonText: 'Save as page',
                  type: 'warning'
            }).then(() => {
                  this.facade.sendNotification(EditorStatic.CMD_EXPORT_PAGE);
                  this.facade.sendNotification(EditorStatic.CMD_CLOSE_PAGE);
            }).catch(() => {
                  this.facade.sendNotification(EditorStatic.CMD_SAVE_AS_PAGE, pageInfoProperties);
            });
      }
}
