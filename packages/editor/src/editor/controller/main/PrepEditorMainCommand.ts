import * as puremvc from "puremvc";
import EditorStatic from "../editor/EditorStatic";
import {ShowResourceManagerCommand} from "./ShowResourceManagerCommand";
import {SavePageCommand} from "./SavePageCommand";
import {NewPageSaveCommand} from "./NewPageSaveCommand";
import {OpenPageCommand} from "./OpenPageCommand";
import {ShowNewPageModalCommand} from "./ShowNewPageModalCommand";
import {SaveAsPageCommand} from "./SaveAsPageCommand";
import {ShowSaveAsPageModalCommand} from "./ShowSaveAsPageModalCommand";
import {ExportPageCommand} from "./ExportPageCommand";
import {ImportPagesCommand} from "./ImportPagesCommand";
import {ClosePageCommand} from "./ClosePageCommand";
import {ShowProductInfoCommand} from "./ShowProductInfoCommand";
import {GotoViewerByNameCommand} from "./GotoViewerByNameCommand";
import {GotoViewerByIdCommand} from "./GotoViewerByIdCommand";
import {OpenPageByIdCommand} from "./OpenPageByIdCommand";
import {GotoAdminCommand} from "./GotoAdminCommand";
import {GotoLogoutCommand} from "./GotoLogoutCommand";
import {NewPageGroupCommand} from "./NewPageGroupCommand";
import {ShowWScriptEditorCommand} from "./ShowWScriptEditorCommand";
import {ShowDatasetManagerCommand} from "./ShowDatasetManagerCommand";
import {GotoViewerByCurrentPageNameCommand} from "./GotoViewerByCurrentPageNameCommand";
import {ResetPageListCommand} from "./ResetPageListCommand";
import {ShowShortcutsInfoCommand} from "./ShowShortcutsInfoCommand";
import {ChangeLocaleCommand} from "./ChangeLocaleCommand";
import {CreateDynamicPanelCommand} from "./CreateDynamicPanelCommand";
import {ShowMultilingualManagerCommand} from "./ShowMultilingualManagerCommand";
import {ChangePageNameCommand} from "./ChangePageNameCommand";
import {ShowExportResourcesManagerCommand} from "./ShowExportResourcesManagerCommand";
import {NewPageCommand} from "./NewPageCommand";
import {DeletePageTreeItemsCommand} from "./DeletePageTreeItemsCommand";
import {ShowExportPagesModalCommand} from "./ShowExportPagesModalCommand";
import {DeletedCurrentPageCommand} from "./DeletedCurrentPageCommand";
import {ImportResourcesCommand} from "./ImportResourcesCommand";
import {UpdateMasterInfoCommand} from "./UpdateMasterInfoCommand";
import {SaveDataToPageCommand} from "./SaveDataToPageCommand";
import { ShowMenusetManagerCommand } from "./ShowMenusetManagerCommand";
import { ShowTemplateManagerCommand } from "./ShowTemplateManagerCommand";
import { ShowManualCommand } from './ShowManualCommand';
import {CreateExtensionCommand} from "./CreateExtensionCommand";
import {ShowExternalManagerCommand} from "./ShowExternalManagerCommand";
import {OpenPopupCommand} from "./OpenPopupCommand";
import {CompletedExtensionCommand} from "./CompletedExtensionCommand";
import {ShowThreeAlignModalCommand} from "./ShowThreeAlignModalCommand";
import {BackupProjectCommand} from "./BackupProjectCommand";
import {ShowRestoreProjectCommand} from "./ShowRestoreProjectCommand";

export class PrepEditorMainCommand extends puremvc.SimpleCommand {


      execute(note: puremvc.INotification) {

            //page 관련 커맨드
            this.facade.registerCommand(EditorStatic.CMD_OPEN_PAGE, OpenPageCommand);
            this.facade.registerCommand(EditorStatic.CMD_OPEN_PAGE_BY_ID, OpenPageByIdCommand);
            this.facade.registerCommand(EditorStatic.CMD_CLOSE_PAGE, ClosePageCommand);
            this.facade.registerCommand(EditorStatic.CMD_SAVE_PAGE, SavePageCommand);
            this.facade.registerCommand(EditorStatic.CMD_SAVE_AS_PAGE, SaveAsPageCommand);
            this.facade.registerCommand(EditorStatic.CMD_SAVE_DATA_TO_PAGE, SaveDataToPageCommand);
            this.facade.registerCommand(EditorStatic.CMD_DELETE_PAGE_TREE_ITEMS, DeletePageTreeItemsCommand);
            this.facade.registerCommand(EditorStatic.CMD_DELETED_CURRENT_PAGE, DeletedCurrentPageCommand);

            this.facade.registerCommand(EditorStatic.CMD_EXPORT_PAGE, ExportPageCommand);
            this.facade.registerCommand(EditorStatic.CMD_IMPORT_PAGES, ImportPagesCommand);
            this.facade.registerCommand(EditorStatic.CMD_UPDATE_MASTER_INFO, UpdateMasterInfoCommand);

            this.facade.registerCommand(EditorStatic.CMD_EXPORT_PAGE, ExportPageCommand);
            this.facade.registerCommand(EditorStatic.CMD_IMPORT_PAGES, ImportPagesCommand);

            this.facade.registerCommand(EditorStatic.CMD_NEW_PAGE, NewPageCommand);
            this.facade.registerCommand(EditorStatic.CMD_NEW_PAGE_SAVE, NewPageSaveCommand);
            this.facade.registerCommand(EditorStatic.CMD_NEW_PAGE_GROUP, NewPageGroupCommand);
            this.facade.registerCommand(EditorStatic.CMD_CHANGE_PAGE_NAME, ChangePageNameCommand);

            //모달 관련 커맨드
            this.facade.registerCommand(EditorStatic.CMD_SHOW_EXPORT_PAGES_MODAL, ShowExportPagesModalCommand);
            this.facade.registerCommand(EditorStatic.CMD_SHOW_EXPORT_PAGES_MODAL, ShowExportPagesModalCommand); //duplicate

            //리소스 관련 커맨드
            this.facade.registerCommand(EditorStatic.CMD_SHOW_EXPORT_RESOURCES_MANAGER, ShowExportResourcesManagerCommand);
            this.facade.registerCommand(EditorStatic.CMD_IMPORT_RESOURCES, ImportResourcesCommand);

            // 스냅샷 관련 커맨드
            this.facade.registerCommand(EditorStatic.CMD_PROJECT_BACKUP, BackupProjectCommand);
            this.facade.registerCommand(EditorStatic.CMD_PROJECT_RESTORE, ShowRestoreProjectCommand);

            //this.facade.registerCommand(EditorStatic.CMD_NEW_PAGE_SAVE, NewPageSaveCommand);


            //페이지 모달 관련
            this.facade.registerCommand(EditorStatic.CMD_SHOW_NEW_PAGE_MODAL, ShowNewPageModalCommand);
            this.facade.registerCommand(EditorStatic.CMD_SHOW_SAVE_AS_PAGE_MODAL, ShowSaveAsPageModalCommand);

            this.facade.registerCommand(EditorStatic.CMD_SHOW_PRODUCT_INFO, ShowProductInfoCommand);
            this.facade.registerCommand(EditorStatic.CMD_SHOW_SHORTCUTS_INFO, ShowShortcutsInfoCommand);

            //리소스 매니저
            this.facade.registerCommand(EditorStatic.CMD_SHOW_RESOURCE_MANAGER, ShowResourceManagerCommand);
            this.facade.registerCommand(EditorStatic.CMD_SHOW_MENUSET_MANAGER , ShowMenusetManagerCommand);

            //템플릿 매니저
            this.facade.registerCommand(EditorStatic.CMD_SHOW_TEMPLATE_MANAGER , ShowTemplateManagerCommand);

            //스크립트 에디터
            this.facade.registerCommand(EditorStatic.CMD_SHOW_WSCRIPT_EDITOR, ShowWScriptEditorCommand);

            //데이터셋 매니저
            this.facade.registerCommand(EditorStatic.CMD_SHOW_DATATSET_MANAGER, ShowDatasetManagerCommand);

            //다국어 매니저
            this.facade.registerCommand(EditorStatic.CMD_SHOW_MULTILINGUAL_MANAGER, ShowMultilingualManagerCommand);

            //매뉴얼
            this.facade.registerCommand(EditorStatic.CMD_SHOW_MANUAL, ShowManualCommand);


            //뷰어로 가는 커맨드
            this.facade.registerCommand(EditorStatic.CMD_GOTO_VIEWER_BY_NAME, GotoViewerByNameCommand);
            this.facade.registerCommand(EditorStatic.CMD_GOTO_VIEWER_BY_ID, GotoViewerByIdCommand);
            this.facade.registerCommand(EditorStatic.CMD_GOTO_VIEWER_BY_CURRENT_PAGE_NAME, GotoViewerByCurrentPageNameCommand);

            //어드민
            this.facade.registerCommand(EditorStatic.CMD_GOTO_ADMIN, GotoAdminCommand);
            //로그아웃
            this.facade.registerCommand(EditorStatic.CMD_GOTO_LOGOUT, GotoLogoutCommand);
            //로케일 체인지
            this.facade.registerCommand(EditorStatic.CMD_CHANGE_LOCALE, ChangeLocaleCommand);





            /*
            관리자용
            */
            this.facade.registerCommand(EditorStatic.CMD_RESET_PAGE_LIST, ResetPageListCommand);
            this.facade.registerCommand(EditorStatic.CMD_CREATE_DYNAMIC_PANEL, CreateDynamicPanelCommand);
            this.facade.registerCommand(EditorStatic.CMD_CREATE_DYNAMIC_PLUGIN, CreateExtensionCommand)


            this.facade.registerCommand(EditorStatic.CMD_SHOW_EXTERNAL_MANAGER, ShowExternalManagerCommand)

            //팝업 여는 커맨드
            this.facade.registerCommand(EditorStatic.CMD_OPEN_POPUP, OpenPopupCommand);


            /*
            2018.12.19(ckkim)
             */
            //플러그인 관련
            this.facade.registerCommand(EditorStatic.CMD_COMPLETED_EXTENSION, CompletedExtensionCommand);



            // 2019.02.12
            //3D 정렬 관련
            this.facade.registerCommand(EditorStatic.CMD_SHOW_THREE_ALIGN_MODAL, ShowThreeAlignModalCommand);

      }
}
