import * as puremvc from "puremvc";


import WVComponent from "../../wemb/core/component/WVComponent";

import SelectProxy from "./SelectProxy";
import EditorStatic from "../controller/editor/EditorStatic";
import {ArrayUtil} from "../../wemb/utils/Util";
import {BackgroundInfo, MasterBackgroundInfo, PageInfo, StageBackgroundInfo} from "../../wemb/wv/page/Page";
import {IWVEvent} from "../../wemb/core/events/interfaces/EventInterfaces";
import WVEvent from "../../wemb/core/events/WVEvent";
import {IThreeExtensionElements, IWVComponent} from "../../wemb/core/component/interfaces/ComponentInterfaces";
import WVComponentEvent from "../../wemb/core/events/WVComponentEvent";
import {EDITOR_APP_STATE, EXE_MODE} from "../../wemb/wv/data/Data";
import EditorPageComponent from "../../wemb/wv/components/EditorPageComponent";
import {
      default as ComponentInstanceLoader,
      IComponentInstanceLoaderContainer
} from "../../wemb/wv/managers/ComponentInstanceLoader";
import {LayerInfo, WORK_LAYERS} from "../../wemb/wv/layer/Layer";
import _default from "@hscmap/vue-menu/lib/types/separator.vue";
import Vue from "vue";

class EditorProxy extends puremvc.Proxy implements  IComponentInstanceLoaderContainer{
	public static readonly NAME: string = "EditorProxy";

	public static readonly NOTI_UPDATE_EDITOR_STATE:string="notification/updateEditorState";

	// 편집 모드가 변경 될때
	public static readonly NOTI_CHANGE_EDITABLE_STATE:string = "notification/changeEditableState";
	// 작업 레이어가 변경 될때
	public static readonly NOTI_CHANGE_ACTIVE_LAYER: string = "notification/changeActiveLayer";

	public static readonly NOTI_ADD_COM_INSTANCE:string = "notification/addComponentInstance";
      public static readonly NOTI_BEOFRE_REMOVE_COM_INSTANCE:string = "notification/beforeRemoveComponentInstance";
	public static readonly NOTI_REMOVE_COM_INSTANCE:string = "notification/removeComponentInstance";
	// 컴포넌트가 추가/삭제 될때
      // 만약 추가, 삭제가 여러 개인 경우 한번만 발생함.
      public static readonly NOTI_UPDATE_COM_INSTANCE:string = "notification/updateComponentInstance";




	public static readonly NOTI_OPEN_PAGE:string = "notification/openPage";
	// 페이지가 열린 후 발생하는 이벤트(리소스 로드까지 완료된 상태)
      public static readonly NOTI_OPENED_PAGE:string = "notification/openedPage";
	public static readonly NOTI_CLOSED_PAGE:string = "notification/closedPage";
      public static readonly NOTI_CHANGE_MODIFIED:string="notification/changeModified";
	public static readonly NOTI_CHANGE_VISIBLE_ASIDE_PANEL:string="notification/changeVisibleAsidePanel";
      public static readonly  NOTI_SET_ACTIVE_SHORT_CUT_STATE="notification/setActiveShortCutState";

      public static readonly  NOTI_CREATE_DYNAMIC_PANEL="notification/createDynamicPanel";



      public static readonly NOTI_CHANGE_PAGE_LOADING_STATE:string="notification/changPageLoadingState";


      /*
      2018.09.30(ckkim)
      페이지 저장 후 실행되는 NOTI
       */
      public static readonly NOTI_SAVED_PAGE:string="notification/savedPage";

      /*
      2018.09.30(ckkim)
      확장요소 데이터가 업데이트 되고 난 후 실행 실행
       */
      public static readonly NOTI_UPDATE_EXTENSION_DATA:string="notification/updateExtensionData";





	/////////////////////////////////////////////////

	private _selectProxy:SelectProxy =  null;




	/////////////////////////////////////////////////
	private _appState:EDITOR_APP_STATE;

	// 환경설정 정보까지 준비 완료가 되면 true가 됨. (오직 한번만 초기화 됨.
	private _readyCompleted:boolean = false;
	private _isActiveShortcut:boolean =true;





	private _activeLayerInfo:LayerInfo = null;
	private _masterLayerInfo:LayerInfo = new LayerInfo();
	private _twoLayerInfo:LayerInfo = new LayerInfo();
	private _threeLayerInfo:LayerInfo = new LayerInfo();

	private _layerInfoMap:Map<string, LayerInfo> = new Map();



	private asideState:any = { left:true, right:true}

	private _masterBackgroundInfo:BackgroundInfo =new BackgroundInfo();

	get masterBackgroundInfo():BackgroundInfo{
	      return this._masterBackgroundInfo;
      }

	private _stageBackgroundInfo:BackgroundInfo = new BackgroundInfo();
      get stageBackgroundInfo():BackgroundInfo{
            return this._stageBackgroundInfo;
      }

	/*
	값이 closed가 되는 경우 모든 내용을 삭제
	 */
	private _isOpenPage:boolean = false;




	public get isOpenPage(){
	      return this._isOpenPage;
      }
	/*
	값이 유지된 상태에서 편집 기능이 안되는 기능
	 */
	private _isEditable:boolean =false;
	public get isEditable(){
	      return this._isEditable;
      }

	private _isModifed:boolean = false;
      public get isModifed(){
            return this._isModifed;
      }



	public get masterLayerInstanceList(){
		return this._masterLayerInfo.instanceList;
	}

	public get twoLayerInstanceList(){
		return this._twoLayerInfo.instanceList;
	}


	public get threeLayerInstanceList(){
		return this._threeLayerInfo.instanceList;
	}

	public get bgLayerInstanceList(){
            // 메인페이지 컴포넌트 자체가 bglayer의 인스턴스 목록이 됨.
	      return [window.wemb.mainPageComponent];
      }


      /*
      전체 컴포넌트 목록이 담긴 컴포넌트로 페이지가 로드될때 채워지고
      닫힐때  비워짐
       */
      private _comInstanceList:WVComponent[] = [];
      public get comInstanceList(){
            return this._comInstanceList;
      }

      public get mainPageComponent():EditorPageComponent{
	      return window.wemb.mainPageComponent;
      }


      // 프로퍼티가 변경되는 경우 실행되는 이벤트 헨들러, 속성이 변경되면
      private _func_changePropertyHandler:Function =null;
      private _func_lockHandler:Function = null;
      private _func_syncTransformSizeToElementSizeHandler = null;





	constructor(selectProxy:SelectProxy) {
		super(EditorProxy.NAME);
		this._resetLayers(true);
		this._layerInfoMap.set(WORK_LAYERS.MASTER_LAYER, this._masterLayerInfo);
		this._layerInfoMap.set(WORK_LAYERS.TWO_LAYER, this._twoLayerInfo);
		this._layerInfoMap.set(WORK_LAYERS.THREE_LAYER, this._threeLayerInfo);
		this._selectProxy = selectProxy;

		/*
	      컴포넌트 포로퍼티 변경시 이벤트 처리를 위한 헨들러
	      chanagedPropertyHandler
		 */
		this._func_changePropertyHandler= this._changePropertyHandler.bind(this);

		// lock => unlock으로 되는 경우 transform 업데이트를 위해 메서드
		this._func_lockHandler = this._onLockHandler.bind(this);


		this._func_syncTransformSizeToElementSizeHandler = this._onSyncTransformSizeToElementSizeHandler.bind(this);
	}




	public setSelectProxy(proxy:SelectProxy){
		this._selectProxy = proxy;
	}
	public onRegister() {

	}
	/////////////////////////////////////////////////////////////////////




	/////////////////////////////////////////////////////////////////////
	public updateAppState(state:EDITOR_APP_STATE){
		this._appState = state;

		if(state==EDITOR_APP_STATE.READY_COMPLETED){
			this._readyCompleted = true;
		}

		this.sendNotification(EditorProxy.NOTI_UPDATE_EDITOR_STATE, state);
	}



	public getReadyCompleted():boolean{
		return this._readyCompleted;
	}


	public setEditable(isEditMode:boolean){

		this._isEditable = isEditMode;
		this.sendNotification(EditorProxy.NOTI_CHANGE_EDITABLE_STATE, isEditMode);
	}




	public setModified(modified:boolean){
	      this._isModifed = modified;
            this.sendNotification(EditorProxy.NOTI_CHANGE_MODIFIED, this._isModifed);
      }

	public setActiveShortcut(active){
		this._isActiveShortcut = active;

		window.wemb.keyboardCommandManager.setActive(active);
            this.sendNotification(EditorProxy.NOTI_SET_ACTIVE_SHORT_CUT_STATE, active);

	}



      public setPageOpenState(isState:boolean){
            this._isOpenPage = isState;
      }
	/*
	현재 열려 있는 페이지가 있다면 닫기
		단축키 비활성화
		편집 비활성화
		수정 상태를 false로
		기존 편접 정보 무두 삭제
		선택 정보 삭제
		복사 정보 삭제
	 */
	public closedPage(){
	      if(this._isOpenPage==true) {
                  this._comInstanceList = [];
                  //this._comInstanceList.push(...this._masterLayerInfo.instanceList);

                  // 단축키 멈춤
                  this.setActiveShortcut(false);
                  // 페이지 닫힌 상태로
                  this.setPageOpenState(false);
                  this.setEditable(false);
                  this.setModified(false);

                  // 레이어 정보 초기화
                  this._resetLayers(true);

                  // 선택 정보 초기화
                  this._selectProxy.clear();

                  this.sendNotification(EditorProxy.NOTI_CLOSED_PAGE);
            }
	}


	/*
	레이어 정보 초기화
	 */
	private _resetLayers(includeMasterLayer:boolean=false){
		if(includeMasterLayer){
                  this._masterLayerInfo.instanceList.forEach((comInstance:IWVComponent)=>{
                        comInstance.removeEventListener(WVEvent.UPDATE_PROPERTY, this._func_changePropertyHandler);
                  })

                  this._masterLayerInfo.name = WORK_LAYERS.MASTER_LAYER;
			ArrayUtil.clear(this._masterLayerInfo.instanceList);


			this._masterLayerInfo.startZIndex = 30000;
			this._masterLayerInfo.instanceName = "com_master_";
		}

		this._twoLayerInfo.name = WORK_LAYERS.TWO_LAYER;
            this._twoLayerInfo.instanceList.forEach((comInstance:IWVComponent)=>{
                  comInstance.removeEventListener(WVEvent.UPDATE_PROPERTY, this._func_changePropertyHandler);
            })

            ArrayUtil.clear(this._twoLayerInfo.instanceList);

		this._twoLayerInfo.startZIndex = 10000;
		this._twoLayerInfo.instanceName = "com_two_";


		this._threeLayerInfo.name = WORK_LAYERS.THREE_LAYER;
            this._threeLayerInfo.instanceList.forEach((comInstance:IWVComponent)=>{
                  comInstance.removeEventListener(WVEvent.UPDATE_PROPERTY, this._func_changePropertyHandler);
            })

		ArrayUtil.clear(this._threeLayerInfo.instanceList);
		this._threeLayerInfo.startZIndex = 0;
		this._threeLayerInfo.instanceName = "com_three_";
	}



	/*
	페이지 프로퍼티 정보 설정 및 다시  그리기
            1. 페이지 정보 재 설정
            2. 페이지 정보 즉서 적용
            3. resize 이벤트 발생
	 */
	public setPagePropertyInfo(pageInfo:any){
            window.wemb.mainPageComponent.resetProperties(pageInfo);

            this.dispatchWindowResizeEvent();
	}

	private dispatchWindowResizeEvent(){
		let event;
            if(typeof(Event) === 'function') {
                  event = new Event('resize');
            }else{
                  event = document.createEvent('Event');
                  event.initEvent('resize', true, true);
            }

            window.dispatchEvent(event);
	}


	/////////////////////////////////////////////


	/*
	페이지 정보 설정
	* 컴포넌트 정보 생성
		* 마스터 레이어 정보 추가
		* 2D 레이어 정보 추가
		* 3D 레이어 정보 추가
	* 컴포넌트 인스턴스 생성
		* 마스터 정보 출력(단, 마스터 정바가 출력이 안된 경우)
		* 2D 정보 출력
		* 3D 정보 출력
	 */
	// public openPage(pageInfo:PageInfo, comInstanceInfoList:Array<any>){
	//
	// 	console.log("openpage, 페이지 정보 설정 시작");
	// 	this.setPagePropertyInfo(pageInfo);
	// 	console.log("openpage, 페이지 정보 설정 완료");
	//
	// 	console.log("openpage, EditorProxy.openPage 생성할 인스턴스 생성 시작, 개수 =", comInstanceInfoList.length);
	// 	ComponentInstanceLoader.start(comInstanceInfoList, this).then(()=>{
	// 		console.log("openpage, EditorProxy.openPage 생성할 인스턴스 생성 완료\n\n\n");
	// 		this._isOpenPage = true;
	//
	// 		console.log("openpage, 에디터 상태 활성화로 변경");
	// 		this.setEditable(true);
	// 		console.log("openpage, 단축키 처리 활성화");
	// 		this.setActiveShortcut(true);
	//
	//
	// 		console.log("openpage, 페이지 열기 NOTI 전송");
	// 		this.sendNotification(EditorProxy.NOTI_OPEN_PAGE);
	// 		console.log("openpage, 시작 레이어 활성화\n\n\n");
	// 		this.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, window.wemb.configManager.startWorkLayerName);
	//
	// 	})
	//
	// }
      /*
		* 컴포넌트 정보 생성
			* 마스터 레이어 정보 추가
			* 2D 레이어 정보 추가
			* 3D 레이어 정보 추가
		* 컴포넌트 인스턴스 생성
			* 마스터 정보 출력(단, 마스터 정바가 출력이 안된 경우)
			* 2D 정보 출력
			* 3D 정보 출력
		 */
      public async startConvertingComponentInfoToComponentInstance(comInstanceInfoList:Array<any>):Promise<any>{
            let success = await ComponentInstanceLoader.startInstanceLoader(comInstanceInfoList, window.wemb.$defaultLoadingModal, this, EXE_MODE.EDITOR);
            if(success) {
                  this.completePageReady();
                  return true;
            }

            return false;



      }

      /*
      페이지 열기 준비 완료
       */
      public completePageReady(){
            this.setPageOpenState(true)
            this.setEditable(true);
            this.setActiveShortcut(true);
            this.sendNotification(EditorProxy.NOTI_OPEN_PAGE, window.wemb.mainPageComponent);
            this.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, window.wemb.configManager.startWorkLayerName);
      }


	/////////////////////////////////////////////





	/////////////////////////////////////////////
	public get activeLayerInfo(){
		return this._activeLayerInfo;
	}
	public setActiveLayerByName(name:WORK_LAYERS){
			switch (name) {
				case WORK_LAYERS.MASTER_LAYER :
					this._activeLayerInfo = this._masterLayerInfo;
					break;
				case WORK_LAYERS.TWO_LAYER :
					this._activeLayerInfo = this._twoLayerInfo;
					break;
				case WORK_LAYERS.THREE_LAYER :
					this._activeLayerInfo = this._threeLayerInfo;
					break;
			}

			this.sendNotification(EditorProxy.NOTI_CHANGE_ACTIVE_LAYER, name);

	}
	public addComInstance(comInstance:WVComponent, type:String=""){
            if(comInstance.layerName==null)
                  return;


		this._comInstanceList.push(comInstance);


            // 컴포넌트의 layerName에 해당하는 레이어를 찾는다.
            let layerInstance  = this._layerInfoMap.get(comInstance.layerName);
            if(layerInstance) {
                  this._layerInfoMap.get(comInstance.layerName).instanceList.push(comInstance);

                  comInstance.addEventListener(WVEvent.UPDATE_PROPERTY, this._func_changePropertyHandler);
                  comInstance.addEventListener(WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE, this._func_syncTransformSizeToElementSizeHandler);
			comInstance.addEventListener(WVComponentEvent.LOCKED, this._func_lockHandler);

                  this.sendNotification(EditorProxy.NOTI_ADD_COM_INSTANCE, comInstance);


                  return true;
            }else {
                  console.log("layer에 컴포넌트 추가 실패", comInstance.name)
                  return false;
            }
	}

	public removeComInstance(comInstance:WVComponent, type:String=""){
            comInstance.removeEventListener(WVEvent.UPDATE_PROPERTY, this._func_changePropertyHandler);

            comInstance.removeEventListener(WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE, this._func_syncTransformSizeToElementSizeHandler);
		comInstance.removeEventListener(WVComponentEvent.LOCKED, this._func_lockHandler);
            this.sendNotification(EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE, comInstance);

            ArrayUtil.delete(this._layerInfoMap.get(comInstance.layerName).instanceList, "id", comInstance.id);
		/* 2018.12.13(ckkim), 삭제 추가 */
		ArrayUtil.delete(this._comInstanceList, "id", comInstance.id);
		this.sendNotification(EditorProxy.NOTI_REMOVE_COM_INSTANCE, comInstance);
		console.log('undo/redo : remove instance')


            /*
            여기에서 삭제할 것인지는 고민해야함.
             */
            //comInstance.onDestroy(null);
	}

	private historyIndex = -1;
	private _comInstanceListHistory:any[] = [];
	public setHistoryEditing(comInstance:WVComponent, type:String="") {
		var me = this;
		if(type === "edit") {
			this._comInstanceListHistory.forEach(function(meta_info) {
				var origin = me._comInstanceList.find(function(d) { return d.id === meta_info.data.id});
				if(origin) meta_info.data.initProperties.props = origin.properties;
			})
		} else {
			var metaInfo = {};
			metaInfo["data"] = {
				x:0,
				y:0,
				sendSelectedNotification: false,
				componentName:comInstance.componentName,
				layerName:comInstance.layerName,
				label:"",
				initProperties:{
					props:comInstance.properties
				},
				id:comInstance.properties.primary.id,
				name:comInstance.properties.primary.name
			}
			metaInfo["type"] = type;
			this._comInstanceListHistory = this._comInstanceListHistory.splice(0, this.historyIndex);
			this._comInstanceListHistory.push(metaInfo);
			this.historyIndex = this._comInstanceListHistory.length;
		}
	}

	public undoComInstances() {
		this.sendNotification(EditorStatic.CMD_CLEAR_ALL_SELECTED);
		if(this.historyIndex > 0) {
			var action_info = this._comInstanceListHistory[this.historyIndex - 1];
			if(action_info.type === "add") {
				var delete_obj = this._comInstanceList.find(function(d) { return d.id === action_info.data.id});
				this.removeComInstance(delete_obj);
			} else {
				var instance = wemb.componentLibraryManager.createComponentInstance(action_info.data.componentName);
				// 인스턴스에 대한 라이프사이클 재정의 및 코드 정리 필요
				if(action_info.data.layerName === "threeLayer") {
					instance.setExtensionElements(window.wemb.threeElements);
				}
				instance.create(action_info.data.initProperties);
				if(instance.isResourceComponent) {
					instance.setResourceBus(new Vue());
                        	instance.startLoadResource();
				}
				this.addComInstance(instance);
			}
			this.historyIndex--;
		}
	}

	public redoComInstances() {
		this.sendNotification(EditorStatic.CMD_CLEAR_ALL_SELECTED);
		if(this.historyIndex >= 0 && this._comInstanceListHistory.length > this.historyIndex) {
			var action_info = this._comInstanceListHistory[this.historyIndex];
			if(action_info.type === "add") {
				var instance = wemb.componentLibraryManager.createComponentInstance(action_info.data.componentName);
				// 인스턴스에 대한 라이프사이클 재정의 및 코드 정리 필요
				if(action_info.data.layerName === "threeLayer") {
					instance.setExtensionElements(window.wemb.threeElements);
				}
				instance.create(action_info.data.initProperties);
				if(instance.isResourceComponent) {
					instance.setResourceBus(new Vue());
                        	instance.startLoadResource();
				}
				this.addComInstance(instance);
			} else {
				var delete_obj = this._comInstanceList.find(function(d) { return d.id === action_info.data.id});
				this.removeComInstance(delete_obj);
			}
			this.historyIndex++;
		}
	}

	// public redoComInstances() {
	// 	// 현재 실행 undo index의 다음 실행
	// 	// add일 경우 addComInstance 호출
	// 	// remove일 경우 addComInstance 호출
	// 	// modify일 경우 modifyComInstance 호출
	// 	if(this._comInstanceHistoryList[this.history_index + 1]) {
	// 		var history_info = this._comInstanceHistoryList[this.history_index + 1];
	// 		switch(history_info.type) {
	// 			case 'add':
	// 				this.addComInstance(history_info.instance, "redo");
	// 			break;
	// 			case 'remove' :
	// 				this.removeComInstance(history_info.instance, "redo");
	// 			break;
	// 		}
	// 		this.history_index++;
	// 	}
	// }

	/*
	최대 인스턴스 이름 구하기
	1.  컴포넌트에 해당하는(컴포넌트 이름을 가지고) 기본 인스턴스 이름 구하기
	    기본 인스턴스 이름은 라이브러리 로딩할때 등록됨.
	    등로된 정보를 사용함.
	 */
	public getMaxInstanceName(componentName:string){
	      let separator:any =  window.wemb.componentLibraryManager.getInstanceSeparator(componentName);
		if(separator!=undefined){

			let newName:string = separator+"_"+componentInstanceUtil.getMaxNameCounter(separator, this);
			return newName;
		}

		// 만약 컴포넌트가 없는 경우 랜덤하게 name명을 만들기.
		return "com_"+Math.floor(Math.random()*10000);
	}

	public getNextInstanceCount(componentName:string){
            let separator:any =  window.wemb.componentLibraryManager.getInstanceSeparator(componentName);
            if(separator!=undefined){
                  return componentInstanceUtil.getMaxNameCounter(separator, this);
            }

            return 1;
      }

	public getMaxZIndexTo(layerName:string){
	      let layerInfo:LayerInfo = this._layerInfoMap.get(layerName) as LayerInfo;
            return componentInstanceUtil.getMaxZIndexTo(layerInfo);
      }


      public getLayerInfo(layerName:WORK_LAYERS){
	      return this._layerInfoMap.get(layerName.toString());
      }
      //////////////////////////////////////////////////






      //////////////////////////////////////////////////
      /*
      프로퍼티 변경시 수정 처리로 변경
       */
      private _count =0;
      private _changePropertyHandler(event:IWVEvent){
            this._count++;
	      this.setModified(true);
      }


      /*
      lock이 풀리는 경우 transform을 업데이트 처리 해야함.
       */
      private _onLockHandler(event:IWVEvent){
      	this.sendNotification(SelectProxy.NOTI_UPDATE_SELECTED_INFO);
	}


	/*
	trasnfrom 크기를 element size와 동일하게 처리하기
	 */
	private _onSyncTransformSizeToElementSizeHandler(event:IWVEvent){
            this.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);
      }

	public toggleLeftAside(){
		this.asideState.left = !this.asideState.left;
		this.sendNotification(EditorProxy.NOTI_CHANGE_VISIBLE_ASIDE_PANEL, this.asideState);
	}

	public toggleRightAside(){
		this.asideState.right = !this.asideState.right;
		this.sendNotification(EditorProxy.NOTI_CHANGE_VISIBLE_ASIDE_PANEL, this.asideState);
		this.dispatchWindowResizeEvent();
	}





      //////////////////////////////////////////////////
      // ComponnentIntanceLoader에 의해서 컴포넌트 인스턴스가 모두 로드(생성)된 경우 호출
      // 현재 사용하지 않음.
      public loadCompleted(){

      }

      public getThreeExtensionElements():IThreeExtensionElements{
	      return window.wemb.threeElements;
      }



      //////////////////////////////////////////////////
}


export default EditorProxy;



/*
컴포넌트 관련 유틸
 */
const componentInstanceUtil = {

	getMaxNameCounter(comInstanceSeparator, editorProxy:EditorProxy){


		let counter =1;
		let separator = comInstanceSeparator+"_";
		counter = Math.max(counter,this._getMaxCounter(separator, editorProxy.masterLayerInstanceList));
		counter = Math.max(counter,this._getMaxCounter(separator, editorProxy.twoLayerInstanceList));
		counter = Math.max(counter,this._getMaxCounter(separator, editorProxy.threeLayerInstanceList));
		return counter;
	},

	_getMaxCounter(separator, instanceList:Array<WVComponent>){
		let counter =1;
		if (instanceList.length > 0) {
			instanceList.forEach((instance)=>{
				let name = instance.name;
				let pos = name.indexOf(separator);
				if(pos!=-1){
					let tempValue = name.slice(separator.length, name.length);
					let tempCounter = parseInt(tempValue);
					if(isNaN(tempCounter)==false){
						counter = Math.max(counter, tempCounter+1);
					}
				}
			})
		}
		return counter;
	},

      // zIndex값 구하기
      _generateZIndexTo(layer) {

            let zIndex = layer.startZIndex;
            if (layer.instanceList.length > 0) {
                  for (let componentInstance of layer.instanceList) {
                        let comZIndex = componentInstance.depth;
                        if (zIndex < comZIndex) {
                              zIndex = comZIndex;
                        }

                  }
            }
            return zIndex + 1;
      },
      getMaxZIndexTo(layer) {
            let zIndex = layer.startZIndex;
            if (layer.instanceList.length > 0) {
                  for (let componentInstance of layer.instanceList) {
                        let comZIndex = componentInstance.depth;

                        if (zIndex < comZIndex) {
                              zIndex = comZIndex;
                        }

                  }
            }
            return zIndex + 1;

      }
}

/*
2018.05.30
      - 자바스크립트에서도 사용할 숭 ㅣㅆㄲ ㅔ추가
 */
window.EditorProxy = EditorProxy;
