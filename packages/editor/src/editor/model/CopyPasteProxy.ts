import * as puremvc from "puremvc";
import EditorProxy from "./EditorProxy";
import EditorStatic from "../controller/editor/EditorStatic";
import {WORK_LAYERS} from "../../wemb/wv/layer/Layer";
import {ObjectUtil} from "../../wemb/utils/Util";
import {
      IComponentMetaProperties,
      IWVComponent,
      WVCOMPONENT_CATEGORY, WVCOMPONENT_METHOD_INFO
} from "../../wemb/core/component/interfaces/ComponentInterfaces";
import SelectProxy from "./SelectProxy";
import WVEvent from "../../wemb/core/events/WVEvent";
import Vue from "vue";
import WVComponent from "../../wemb/core/component/WVComponent";
import WVComponentEvent from "../../wemb/core/events/WVComponentEvent";
import ComponentCounter from "../../../../renobit_client/src/wemb/core/utils/ComponentCounter";

export default class CopyPasteProxy extends puremvc.Proxy {
	public static readonly NAME: string = "CopyPasteProxy";
      public static readonly NOTI_ADD_COPY_COMPONENT_INSTANCE: string = "notification/copyComponentInstance";
      public static readonly NOTI_ADD_PASTE_COMPONENT_INSTANCE: string = "notification/pasteComponentInstance";

	private _targetLayerName: WORK_LAYERS;
	private _targetInstanceMetaPropertiesList: Array<IComponentMetaProperties> = [];
	private _copyCounter = 0;


	public get instanceMetaList():Array<IComponentMetaProperties>{
	      return this._targetInstanceMetaPropertiesList;
      }
      public get layerName():WORK_LAYERS{
	      return this._targetLayerName;
      }


      private _isCoping:boolean = false;
      public get isCoping(){
            return this._isCoping;
      }

      public setCopying(state:boolean){
            this._isCoping = state;
      }



	constructor() {
		super(CopyPasteProxy.NAME);
	}


	public onRegister() {
	}

      /*
      페이지 이동 시 호출
      EditorProxy.closedPage()
       */
      public clear(){
            this._isCoping=false;
      }

	private _call_copyTargetComInstanceMetaProperties(list:Array<IWVComponent>){
            this._targetInstanceMetaPropertiesList = [];
            this._copyCounter = 0;

            if(list.length>0) {
                  list.forEach((comInstance:IWVComponent)=>{
                        this._targetInstanceMetaPropertiesList.push(comInstance.serializeMetaProperties());
                  })
                  return true;

            }else {
                  console.log("복사 내용이 없음.");
                  return false;
            }
      }
	public copyInstanceList(layerName: WORK_LAYERS, list: Array<IWVComponent>) {
		this._targetLayerName = layerName;

		this._call_copyTargetComInstanceMetaProperties(list);
            // 현재 선택 정보 제거하기
            this.sendNotification(CopyPasteProxy.NOTI_ADD_COPY_COMPONENT_INSTANCE, this._targetInstanceMetaPropertiesList);
	}

	public cutInstanceList(layerName: WORK_LAYERS, list: Array<IWVComponent>) {
		this._targetLayerName = layerName;
            this._call_copyTargetComInstanceMetaProperties(list);
            // 현재 선택 정보 제거하기
            this.sendNotification(CopyPasteProxy.NOTI_ADD_PASTE_COMPONENT_INSTANCE, this._targetInstanceMetaPropertiesList);
	}





	public pasteInstanceList(activeLayerName: WORK_LAYERS) {

            if(this._isCoping){
                  Vue.$message(Vue.$i18n.messages.wv.editor.duplicatedCopy);
                  return;
            }

            this.setCopying(true);

		// 1. 복수 가능 유무  판단하기.
		this._copyCounter++;

            ////////////////////
            // 붙이기 가능 유무 체크
            // - 마스터에서 2D, 마스터로 옮길 수 있지만 3D로 옮기질 못함.
		if (activeLayerName == WORK_LAYERS.MASTER_LAYER || activeLayerName == WORK_LAYERS.TWO_LAYER) {
			if (this._targetLayerName == WORK_LAYERS.THREE_LAYER) {

			      // 예외 발생시 복사 상태 초기화
                        this.setCopying(false);
			      alert(Vue.$i18n.messages.wv.etc.etc04);
				return;
			}

                  // 붙여넣기 할 때 depth 정렬 (2d, master만)
                  this._targetInstanceMetaPropertiesList.sort((a,b)=>{
                        return Number(a.props.setter.depth) - Number(b.props.setter.depth);
                  })
		}

		if (activeLayerName == WORK_LAYERS.THREE_LAYER) {
			if (this._targetLayerName != WORK_LAYERS.THREE_LAYER) {

                        // 예외 발생시 복사 상태 초기화
                        this.setCopying(false);
				alert(Vue.$i18n.messages.wv.etc.etc05);
				return;
			}
		}
            ////////////////////




            /*
            선택 정보 취소
             */
		this.sendNotification(EditorStatic.CMD_CLEAR_ALL_SELECTED);





		////////////
		let editorProxy: EditorProxy = <EditorProxy>this.facade.retrieveProxy(EditorProxy.NAME);
		let moveStep = this._copyCounter * 5;

		let loadLength = this._targetInstanceMetaPropertiesList.length;
		let loadedCount = 0;


		this._targetInstanceMetaPropertiesList.forEach((targetMetaProperties: IComponentMetaProperties) => {
		      if(ComponentCounter.shouldBeOneAndPositionedComponent(targetMetaProperties.componentName)){
                        this.setCopying(false);
                        return;
                  }

                  /*
                  1. 카겟 프로퍼티 정보 복사하기.
                   */

                  let initProperties:any ={
                        id: ObjectUtil.generateID(),
                        componentName:targetMetaProperties.componentName,
                        name: editorProxy.getMaxInstanceName(targetMetaProperties.componentName),
                        layerName: activeLayerName,
                        props:{}

                  };

                  $.extend(true, initProperties.props, targetMetaProperties.props);

                  // 2. 위치 조절하기
                  if(activeLayerName==WORK_LAYERS.THREE_LAYER){
                        initProperties.props.setter.position.x = parseInt(initProperties.props.setter.position.x) + moveStep;


                  }else {
                        initProperties.props.setter.x = parseInt(initProperties.props.setter.x) + moveStep;
                        initProperties.props.setter.y = parseInt(initProperties.props.setter.y) + moveStep;
                  }
                  initProperties.props.setter.depth = editorProxy.getMaxZIndexTo(activeLayerName);


			//3. 인스턴스 생성하기
			let comInstance = window.wemb.componentLibraryManager.createComponentInstance(initProperties.componentName);

                  /*
                  2018.10.18 ckkim
                  create() 메서드 호출후에 다음 내용을 호출하면 에러 발생
                        즉, setExtensionElements() 메서드 호출에 의해서 domEvent 객체가 설정되며
                        이후 create()에서 domevent 객체를 사용함.

                   */
                  if(activeLayerName==WORK_LAYERS.THREE_LAYER) {
                        comInstance.setExtensionElements(window.wemb.threeElements);
                  }



                  comInstance.create(initProperties);




                  let self = this;
                  let $resourceEventBus = new Vue();




                  function onCreated(){
                        comInstance.removeEventListener(WVEvent.CREATED, onAddComponent);

                        //4. 화면에 붙이기
                        editorProxy.addComInstance(comInstance as WVComponent);

                        // undo ,  redo
                        console.log('undo/redo :', comInstance as WVComponent);
                        editorProxy.setHistoryEditing(comInstance as WVComponent, "add");
                  }

                  function onAddComponent(){
                        console.log("화면에 컴포넌츠 추가, 리소스 컴포넌트 유무  ", comInstance.isResourceComponent);
                        comInstance.removeEventListener(WVEvent.ADDED_CONTAINER, onAddComponent);

                        if(comInstance.isResourceComponent==false){
                              completed();
                              return;
                        }


                        comInstance.setResourceBus($resourceEventBus);
                        $resourceEventBus.$on(WVComponentEvent.LOADED_RESOURCE, onLoadedResource);
                        comInstance.startLoadResource();
                  }



                  function onLoadedResource(comInstance:WVComponent){
                        //console.log("리소스 로드 완료 ");
                        $resourceEventBus.$off(WVComponentEvent.LOADED_RESOURCE, onLoadedResource);
                        if(comInstance==null){
                              console.log("리소스 완료시 반드시 컴포넌트 인스턴스를 넘겨줘야 합니다.");
                              completed();
                              return null;
                        }

                        // onLoadPage를 가지고 있는 컴포넌트만 onLoadPage()메서드 호출
                        if(comInstance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]){
                              comInstance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]();
                        }

                        // 체크하기
                        //console.log("리소스 로딩 완료 컴포넌트 = ", comInstance.id);
                        completed();
                  }


                  function completed(){
                        self.sendNotification(EditorStatic.CMD_ADD_SELECTED, comInstance);
                        loadedCount++;
                        if(loadedCount>=loadLength){
                              completedAll();
                        }
                  }

                  function completedAll(){
                        self.sendNotification(EditorStatic.CMD_SET_MODIFY_STATE, true);
                        self.sendNotification(EditorProxy.NOTI_UPDATE_COM_INSTANCE);
                        self.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);

                        self.setCopying(false);
                  }


                  comInstance.addEventListener(WVEvent.CREATED, onCreated)
                  comInstance.addEventListener(WVEvent.ADDED_CONTAINER, onAddComponent)
                  //3. element및 프로퍼티 생성
                  comInstance.create(initProperties);

		})




	}

	/////////////////////////////////////////////////////////////////////

}



/*
    let vo = {
	    componentName:"",
	    initProperties:{
		    props:{
			    setter:{
				    x:0,
				    y:0
			    },
			    position:{
				    x:0,
				    y:0,
				    z:0
			    }
		    }
	    },
	    sendSelectedNotification:false
    }

    vo.componentName = targetMetaProperties.componentName;
    $.extend(true, vo.initProperties.props, targetMetaProperties.props);

    // 2. 위치 조절하기
    if(activeLayerName==WORK_LAYERS.THREE_LAYER){
	    vo.initProperties.props.setter["position"].x = parseInt(vo.initProperties.props.setter["position"].x) + moveStep;

    }else {
	    vo.initProperties.props.setter.x = parseInt(vo.initProperties.props.setter.x+"") + moveStep;
	    vo.initProperties.props.setter.y = parseInt(vo.initProperties.props.setter.y+"") + moveStep;
    }

    this.sendNotification(EditorStatic.CMD_ADD_COM_INSTANCE_BY_NAME, vo);

*/

