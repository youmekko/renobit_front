import * as puremvc from "puremvc";


import WVComponent from "../../wemb/core/component/WVComponent";

import EditAreaMainContainerMediator from "../view/work-area/EditAreaMainContainerMediator";
import EditAreaMainContainer from "../view/work-area/EditAreaMainContainer.vue";

import {WORK_LAYERS} from "../../wemb/wv/layer/Layer";
import {
      VirtualBackgroundManager, VirtualPropertyManager, VirtualThreePropertyManager,
      VirtualTwoPropertyManager
} from "./VirtualPropertyManager";
import EditorProxy from "./EditorProxy";





export default class SelectProxy extends puremvc.Proxy {
	public static readonly NAME: string = "SelectProxy";

	public static readonly NOTI_CLEAR_ALL_SELECTED_INSTANCE: string = "notification/clearAllSelectedInstance";
	public static readonly NOTI_ADD_SELECTED_INSTANCE: string = "notification/addSelectedInstance";
	public static readonly NOTI_REMOVE_SELECTED_INSTANCE: string = "notification/removeSelectedInstance";
	public static readonly NOTI_UPDATE_SELECTED_INFO:string = "notification/updateSelectedInfo";
	public static readonly NOTI_UPDATE_PROPERTIES:string = "notification/updateProperties";
	public static readonly NOTI_SYNC_TRANSFORM_LOCATION_SIZE:string = "notification/syncTransformLocationSize";


	/////////////////////////////////////////////////


	private _mainPageComponent:WVComponent = null;

      private _bgPropertyManager:VirtualPropertyManager = null;
	private _twoPropertyManager:VirtualPropertyManager = null;
	private _threePropertyManager:VirtualPropertyManager = null;
	private _currentPropertyManager:VirtualPropertyManager = null;





	public get editableProperties():any {
		return this._currentPropertyManager.getEditableProperties();
	}


	public get propertyPanelDescription(){
		return this._currentPropertyManager.getPropertyPanelDescription();
	}

	public get currentPropertyManager():VirtualPropertyManager{
		return this._currentPropertyManager;
	}

	/*

	중요!
	toolbar, mainmenu에서 two, three, master layer의 컴포넌트만 해당되며
	backgroundlayer에 있는 editPageContainer의 컴포넌트는 선택 대상이 안됨.
	이를 처리하기 위해서  이 부분을 추가함.

	two, three, master 영역에 해당하는 컴포넌트 목록 구하기
	 */
	public get selectedTwoThreeMasterComInstanceListList(){
		let layerName:WORK_LAYERS = this._currentPropertyManager.layerName as WORK_LAYERS;

		if(layerName==WORK_LAYERS.MASTER_LAYER ||
			layerName==WORK_LAYERS.TWO_LAYER ||
			layerName==WORK_LAYERS.THREE_LAYER
		){
			return this._currentPropertyManager.getInstanceList()
		}

		return [];
	}



	constructor() {
		super(SelectProxy.NAME);

		this._bgPropertyManager = new VirtualBackgroundManager(WORK_LAYERS.BG_LAYER);
		this._twoPropertyManager = new VirtualTwoPropertyManager(WORK_LAYERS.TWO_LAYER);
		this._threePropertyManager = new VirtualThreePropertyManager(WORK_LAYERS.THREE_LAYER);
		this._currentPropertyManager = this._bgPropertyManager;
	}


	public onRegister() {
		// 컴포넌트가 선택 안된 경우 자동으로 선택되는 배경 담당 컴포넌트.
		this._mainPageComponent =<WVComponent>(this.facade.retrieveMediator(EditAreaMainContainerMediator.NAME).getViewComponent() as EditAreaMainContainer).mainPageComponent;
		// 배경관리자에서 배경 컴포넌트 추가.
		this._bgPropertyManager.addSelectedInstance(this._mainPageComponent);
	}

	/*
	call :
		EditorProxy.closedPage()에서 호출
	 */
	public clear(){
		this._currentPropertyManager.clearSelectedInstance();
	}
	/////////////////////////////////////////////////////////////////////







	/////////////////////////////////////////////////////////////////////
	public setSelectedLayer(layerName:WORK_LAYERS){
		switch(layerName){
			case WORK_LAYERS.TWO_LAYER :
			case WORK_LAYERS.MASTER_LAYER :
				this._currentPropertyManager = this._twoPropertyManager;
				break;
			case WORK_LAYERS.THREE_LAYER :
				this._currentPropertyManager = this._threePropertyManager;
				break;
			case WORK_LAYERS.BG_LAYER :
				this._currentPropertyManager = this._bgPropertyManager;
				break;

		}
	}



	/////////////////////////////////////////////////////////////////////
	/*
	선택 정보 취소 처리
	1. 선택된 내용이 없는 경우 보내지 않기
	2. 선택된 내용이 있을때만 취소 정보 보내기
	 */
	public clearAllSelectedInstance(){
		if(this._currentPropertyManager.layerName==WORK_LAYERS.BG_LAYER)
			return;

		if(this._currentPropertyManager.getInstanceLength()!=0) {
			this.sendNotification(SelectProxy.NOTI_CLEAR_ALL_SELECTED_INSTANCE, this._currentPropertyManager.getInstanceList());
			this._currentPropertyManager.clearSelectedInstance();
		}
	}


	public addSelectedInstance(instance: WVComponent) {
            if(this._currentPropertyManager.addSelectedInstance(instance)==true) {
			this.sendNotification(SelectProxy.NOTI_ADD_SELECTED_INSTANCE, instance);
		}

	}


	public removeSelectedInstance(instance:WVComponent){
		if(this._currentPropertyManager.removeSelectedInstance(instance)==true){
			this.sendNotification(SelectProxy.NOTI_REMOVE_SELECTED_INSTANCE, instance);
		}

	}


	public updateSelectedInfo(){
		this.sendNotification(SelectProxy.NOTI_UPDATE_SELECTED_INFO, this._currentPropertyManager);
	}


	public setGroupPropertyValue(groupName:string, propertyName:string, value){
		if(this._currentPropertyManager.setGroupPropertyValue(groupName, propertyName, value)){

		      /*
		      2018.04.24
		      setGrupPropertyValue()에 의해서 값이 변경되는 경우
		      EditorProxy의 _changePropertyHBandler()가 실행됨. 즉, 아래와 같은 구문이 실행됨
		            private _changePropertyHandler(event:IWVEvent){
                              this._count++;
                              this.setModified(true);
                        }

                  위의 코드에서 확인 할 수 있는 것처럼 값 수정 처리만 하기 때문에
                  아래 코드를 호출해서
                        속성 변경으 프로퍼티 패널에 알리기 - NOTI_UPDATE_PROPERTIES
                        transform와 location 크기를 동기화 시키기 - NOTI_SYNC_TRANSFORM_LOCATION_SIZE
		       */

                  /*
                  타이머를 사용한 이유
                  만약 값이 변경되면 validation 기능에 의해서 내부에서 타이머가 실행되어 값보다
                  update()기능이 먼저 실행되게 됨.
                  싱크를 맞게 하기 위해 타이머 사용.
                   */
                  setTimeout(()=>{
                        this.sendNotification(SelectProxy.NOTI_UPDATE_PROPERTIES);
						this.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);
						var editorProxy = this.facade.retrieveProxy(EditorProxy.NAME) as EditorProxy;
						editorProxy.setHistoryEditing(null, "edit");
                  },0);



		}
	}

	/*
	      transform 크기를 element 크기로  동기화 시키기
	      주로 component에서 리소스를 로드하는 등의 작업으로
	      element 크기가 변경되는 경우 transform 크기를 동일하게 맞춰줘야함.
	 */
	public syncTransformSizeToElementSize(){

            this.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);
      }


}
/*
2018.05.30
      - 자바스크립트에서도 사용할 숭 ㅣㅆㄲ ㅔ추가
 */
window.SelectProxy = SelectProxy;
