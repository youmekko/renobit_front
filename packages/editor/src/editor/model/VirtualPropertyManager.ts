import * as puremvc from "puremvc";

import {default as WV3DComponent, WVVector} from "../../wemb/core/component/3D/WV3DComponent";
import WVComponent from "../../wemb/core/component/WVComponent";
import WVDOMComponent from "../../wemb/core/component/2D/WVDOMComponent";
import {WORK_LAYERS} from "../../wemb/wv/layer/Layer";
import {ArrayUtil} from "../../wemb/utils/Util";
import {
	IWVComponentProperties, PRIMARY_WVCOMPONENT
} from "../../wemb/core/component/interfaces/ComponentInterfaces";





/*
컴포넌트 프로퍼티 생성 관리자
- 주요 기능

1. 컴포넌트가 선택되지 않은 경우 페이지(EditorPageComponent)의 프로퍼티
2. 컴포넌트가 선택된 경우
	- 컴포넌트가 하나인  경우 (단일)
		- 해당 컴포넌트의 인스턴스 사용

	- 컴포넌트가 여러 개인 경우(다중)
		- 중복된 프로퍼티만 처리



 */
	export interface IVirtualPropertyManager {

		setGroupPropertyValue(groupName:string, propertyName: string, value: any);

		getEditableProperties(): any;

		getPropertyPanelDescription(): any;
            getPropertyDescription(): any;

		getInstanceList(): Array<WVComponent>;
            getInstanceLength():number;

            getEventDescription():Array<any>;
            getMethodDescription():Array<any>;
            getExtensionProperties():any;

            readonly layerName:string;

	}


	export abstract class VirtualPropertyManager implements IVirtualPropertyManager {
		private _layerName: WORK_LAYERS = WORK_LAYERS.NONE_LAYER;
		get layerName() {
			return this._layerName;
		}

		protected _instanceList: Array<WVComponent|WV3DComponent> = [];
		protected _propertySetterMap: Map<string, Function> = new Map();

		protected _properties:any;
		protected _oldProperties:any;

		constructor(layerName: WORK_LAYERS) {
			this._layerName = layerName;
			this.onRegister();
		}


		protected onRegister() {

		}

		/*
		선택 취소 처리

		 */
		clearSelectedInstance() {
			this._instanceList.forEach((instance: WVComponent) => {
				instance.selected = false;
				instance.tempUpdateData = Math.random().toString();

			})

			ArrayUtil.clear(this._instanceList);
		}

		/*
		선택 컴포넌트 추가
		 */
		addSelectedInstance(instance: WVComponent) {
			//console.log("VirtualPropertyManager, addSelectedInstance(), 선택처리 시작 ", instance.name);
			if (ArrayUtil.has(this._instanceList, "id", instance.id) == false) {
				this._instanceList.push(instance);
				//console.log("VirtualPropertyManager, addSelectedInstance(), 선택처리 완료 ", instance.name);
				instance.selected = true;
				instance.tempUpdateData = Math.random().toString();
				return true;
			}

			return false;
		}
		removeSelectedInstance(instance:WVComponent){
			//console.log("VirtualPropertyManager, removeSelectedInstance(), 선택 제거 처리 시작 ", instance.name);
			if (ArrayUtil.has(this._instanceList, "id", instance.id) == true) {
				ArrayUtil.delete(this._instanceList, "id", instance.id);
				//console.log("VirtualPropertyManager, addSelectedInstance(), 선택 제거 처리 완료 ", instance.name);
				instance.selected = false;
				instance.tempUpdateData = Math.random().toString();
				return true;
			}

			return false;
		}

		getInstanceList(): Array<WVComponent> {
			return this._instanceList;
		}

		getInstanceLength(): number {
			return this._instanceList.length;
		}


		/*
		element의 위치/크기 값이 변경되었지만 컴포넌트 프로퍼티에는 적용되지 않은 상태임.
		이때 두 개의 내용을 싱크해야하는 경우가 발생
		이를 위해 존재
		 */
		syncLocationSizeElementPropsComponentProps() {
			this._instanceList.forEach((intance)=>{
				intance.syncLocationSizeElementPropsComponentProps();
			})
		}
		///////////////////////////////////////////////////////////////////////








		///////////////////////////////////////////////////////////////////////
		/*
		call : SelectProxy에서 호출
		그룹 속성 정보 변경
		 */
		setGroupPropertyValue(groupName:string, propertyName: string, value: any) {
			if (this._instanceList.length == 0)
				return false;


			if (this._instanceList.length == 1) {
				return this._setGroupPropertyFirstComponent(groupName, propertyName, value);

			}

			return this._setGroupPropertyManyComponent(groupName, propertyName, value);
		}


		protected _setGroupPropertyFirstComponent(groupName:string, propertyName: string, value: any) {

			let instance: WVComponent = this.getFirst();
			instance.tempUpdateData = Date.now().toString();
                  return instance.setGroupPropertyValue(groupName, propertyName, value);
		}

		protected _setGroupPropertyManyComponent(groupName:string, propertyName: string, value: any) {
			/*
			setter로 처리해야하는 프로퍼티인 경우
			 */
			if (this._propertySetterMap.has(propertyName) == true) {
				let setFunction: Function = this._propertySetterMap.get(propertyName)
				return setFunction(value);
			}

			/*
			value 값을 동일하게 적용해야하는 프로퍼티인 경우
			 */
			let changed=false;
			this._instanceList.forEach((instance: WVComponent) => {
                        changed=instance.setGroupPropertyValue(groupName, propertyName, value);
			})

                  return changed;
		}
		///////////////////////////////////////////////////////////////////////












		///////////////////////////////////////////////////////////////////////
		// 편집 가능한 프로퍼티 정보 구하기.
		getEditableProperties(): any {

			if (this._instanceList.length == 0) {
				this._properties = null;
				this._oldProperties = null;
				return this._properties;
			}

			let tempProperties = null;
			if (this._instanceList.length == 1) {
				tempProperties = this._getEditablePropertiesFirstComponent()
			}else {
				tempProperties = this._getEditablePropertiesManyComponent();
			}

			this._oldProperties = $.extend(true, {}, tempProperties);
			this._properties = $.extend(true, {}, tempProperties);
			return this._properties;

		}

		_getEditablePropertiesFirstComponent(): any {
			return this.getFirst().getEditableProperties();
		}


		protected abstract _getEditablePropertiesManyComponent(): any

		////////////////////////////////////////////////






		getPropertyPanelDescription(): any {
			if (this._instanceList.length == 0)
				return null;

			if (this._instanceList.length == 1) {
				return this._getPropertyPanelDescriptionFirstComponent();
			}

			return this._getPropertyPanelDescriptionManyComponent();
		}


		_getPropertyPanelDescriptionFirstComponent() {
			return this.getFirst().getPropertyPanelDescription();
		}

		protected  abstract _getPropertyPanelDescriptionManyComponent()


            ///////////
            // 프로퍼티 정보
            getPropertyDescription(): any {
                  if (this._instanceList.length == 0)
                        return null;

                  return this.getFirst().getPropertyDescription();
            }

            ///////////

		protected getFirst(): WVComponent {
			return <WVComponent>this._instanceList[0];
		}


		/*
		x,y,width,height 값 구하기
		 */
		protected _calculateLocation():any{
			let info = {
				left: 10000,
				top: 10000,
				right: -10000,
				bottom: -10000,
			};

			// left 값 구학
			this._instanceList.forEach((instance:WVDOMComponent) => {
				// 작은 left 값 구하기
				info.left = Math.min(instance.x, info.left);
				// 작은 top 값 구하기
				info.top = Math.min(instance.y, info.top);
				// 큰 right값 구하기
				info.right = Math.max(instance.x + instance.width, info.right);
				info.bottom = Math.max(instance.y + instance.height, info.bottom);

			});


			return {
				x:info.left,
				y:info.top,
				width:info.right - info.left,
				height:info.bottom - info.top
			}

		}


		//////////////////////////////////////////////////////////////////////////////////////////
		/*
		동일 컴포넌트의 인스턴스인경우 처리
		 */

		protected _checkAllSameType() {
			let firstInstance = this._instanceList[0];
			let nextInstance = null;

			// 1. 동일한 컴포넌트 인지 확인하기
			var isSameType = true;
			for( let nextInstance of this._instanceList){
				if ((nextInstance instanceof firstInstance.constructor) == false) {
					isSameType = false;
				}
			}

			return isSameType;
		}

           public getEventDescription():Array<any>{
                 if(this._instanceList.length==0)
		            return [];

                 return this.getFirst().getEventDescription();

           }
           public getMethodDescription():Array<any>{
                 if(this._instanceList.length==0)
                       return [];

                 return this.getFirst().getMethodDescription();

            }
            public getExtensionProperties():any{
                  if(this._instanceList.length==0)
                        return [];

                  return this.getFirst().getExtensionProperties();
              }
      }


	export class VirtualTwoPropertyManager extends VirtualPropertyManager {

		constructor(layerName: WORK_LAYERS) {
			super(layerName);
		}

		onRegister() {
			this._propertySetterMap.set("x", this._x.bind(this));
			this._propertySetterMap.set("y", this._y.bind(this));
			this._propertySetterMap.set("width", this._width.bind(this));
			this._propertySetterMap.set("height", this._height.bind(this));
		}

		private _x(value: number) {
			if(value==this._oldProperties.setter.x)
				return false;


			let gapValue:number = value - this._oldProperties.setter.x;
			this._instanceList.forEach((instance:WVDOMComponent) => {
				instance.x = instance.x + gapValue;
			});
			this._oldProperties.setter.x = value;
			return true;
		}

		private _y(value: number) {
			if(value==this._oldProperties.setter.y)
				return false;


			let gapValue: number = value - this._oldProperties.setter.y;
			this._instanceList.forEach((instance: WVDOMComponent) => {
				instance.y = instance.y + gapValue;
			});
			this._oldProperties.setter.y = value;

			return true;
		}


		private _width(value: number) {

			if(value==this._oldProperties.setter.width)
				return false;

			// 늘어난 %율 구하기
			let percent = value /this._oldProperties.setter.width;

			// 복사
			let tempInstanceArray = this._instanceList.slice(0);

			// 소팅
			tempInstanceArray = tempInstanceArray.sort((a:WVDOMComponent,b:WVDOMComponent)=>{
				return a.x-b.x;
			});


			let firstInstance = tempInstanceArray[0] as WVDOMComponent;
			for(let i=0;i<tempInstanceArray.length;i++){
				let instance:WVDOMComponent=  tempInstanceArray[i] as WVDOMComponent;

				instance.width = Math.ceil(instance.width*percent);
				instance.x = firstInstance.x+Math.ceil((instance.x-firstInstance.x)*percent);
			}

			this._oldProperties.setter.width=value;

			return true;
		}

		private _height(value: number) {
			if(value==this._oldProperties.setter.height)
				return false;


			// 늘어난 %율 구하기
			let percent = value /this._oldProperties.setter.height;

			// 복사
			let tempInstanceArray = this._instanceList.slice(0);

			// 소팅
			tempInstanceArray = tempInstanceArray.sort((a:WVDOMComponent,b:WVDOMComponent)=>{
				return a.y-b.y;
			});


			let firstInstance = tempInstanceArray[0] as WVDOMComponent;
			for(let i=0;i<tempInstanceArray.length;i++){
				let instance=  tempInstanceArray[i] as WVDOMComponent;
				instance.height = Math.ceil(instance.height*percent);
				instance.y = firstInstance.y+Math.ceil((instance.y-firstInstance.y)*percent);
			}

			this._oldProperties.setter.height=value;


			return true;
		}

		/*
		진행순서.

		경우1. 공통된 컴포넌트가 선택된 경우
			1. 첫 번째 컴포넌트 프로퍼티를 복사한다.
			2. componentName을 group으로 변경한다.

		경우2. 공통된 컴포넌트가 아닌 경우
			1. 컴포넌트 프로퍼티 정보 중 공통된 프로퍼티 패널 정보 구하기
			2. 프로퍼티 만들기.
				2-1. 프로퍼티 그룹 정보 만들기.

		 */
		protected _getEditablePropertiesManyComponent(){


			let tempProperties:IWVComponentProperties = {
				primary:{
					id:"",
					name:"",
					layerName:""
				},
				info:{}
			}

			let firstProperties:IWVComponentProperties =  this.getFirst().properties;
			// 선택한 컴포넌트가 동일 컴포넌트인지 판단후 맞다면 properties와 propertyInfo를 해당 컴포넌트로 설정하기
			if (this._checkAllSameType()) {

				tempProperties = $.extend(true, {}, firstProperties);

			}else {
				/*
				1. 컴포넌트 프로퍼티 정보 중 공통된 프로퍼티 패널 정보 구하기
				 */

				let propertyPanelDescription = this._getPropertyPanelDescriptionManyComponent();

				/*
				컴포넌트 포러퍼티 정보는 아래와 같은 구조로 되어 있음.
				 property_panel_info = [{
						name:"primary",
						label: "일반 속성",
						template: "primary",
						children: [{
							owner:"primary",
							name: "name",
							type: "string",
							label: "이름",
							writable: true,
							show: true,
							description: "인스턴스 이름"
						}]
					}, {
						name:"pos-size-2d",
						label: "위치 크기 정보",
						template: "pos-size-2d",
						children: [{
							owner:"setter",
							name: "lock",
							type: "checkbox",
							label: "잠금",
							show: true,
							writable: true,
							description: "잠금 유무"
						}, {
							owner:"setter",
							name: "x",
							type: "number",
							label: "x",
							tag: 'px',
							show: true,
							writable: true,
							description: "x 위치 값"
						}, .......
					}
				]
				 */



				////////////////////////////////
				/*
				// propertyPanelDescription 에 맞게 가상 속성 만들기
				// 기본값은 첫 번째 값으로 만들기.
				결과는 IWVComponentProperties 스타일에 맞게 나와야 함.
					let tempProperties:IWVComponentProperties = {
						primary:{
							id:"",
							name:"",
							layerName:""
						},
						setter:{
						},
						style:{},
						info:{}
					}

				 */

				for(let i=0;i<propertyPanelDescription.length;i++){
					let propertyInfo = propertyPanelDescription[i];
					if(propertyInfo.children==null)
					      continue;

                              let count = 0;
					for(let childProperty of  propertyInfo.children) {
					      count++;
						// 2-2. 속성을 만들 프로퍼티 구릅 구하기.
						if (childProperty.hasOwnProperty("owner") == false)
							continue;


						// 2-3. 그룹이 없는 경우 만들기
						if (tempProperties.hasOwnProperty(childProperty.owner) == false) {
							tempProperties[childProperty.owner] = $.extend(true, {}, firstProperties[childProperty.owner]);
						}
					}

				}
			}

			try {

				tempProperties.primary.id = "";
				tempProperties.primary.name = "";
				tempProperties.primary.layerName = firstProperties.primary.layerName

				tempProperties.info.componentName = PRIMARY_WVCOMPONENT.VIRTUAL_GROUP_COMPONENT_NAME;
				tempProperties.info.category = firstProperties.info.category;
				tempProperties.info.version = "";


				// 위치값 설정하기
				let rectInfo = this._calculateLocation();
				if(tempProperties.setter==null)
					tempProperties.setter={};

				tempProperties.setter.x = rectInfo.x;
				tempProperties.setter.y = rectInfo.y;
				tempProperties.setter.width = rectInfo.width;
				tempProperties.setter.height = rectInfo.height;

				return tempProperties;

			}catch{
				console.warn("가상 프로퍼티 정보를 만드는 도중 에러가 발생 ");
				return null;
			}



		}

		/*
		여러 컴포넌트 프로퍼티 정보에서 공통된 프로퍼티 정보 구하기.

		 */
		protected _getPropertyPanelDescriptionManyComponent(){
			// 선택한 컴포넌트가 동일 컴포넌트인지 판단후 맞다면 properties와 propertyInfo를 해당 컴포넌트로 설정하기
			if (this._checkAllSameType()) {
				let firstInstance:WVComponent = this.getFirst();
				let propertyPanelDescription:Array<any> = firstInstance.getPropertyPanelDescription();

				//////////////
				return propertyPanelDescription;
			}else {

                        // 인스턴스에 공통으로 들어 있는 그룹 목록 구하기.
                        let propertyPanelDescription = this._getDuplicateDescriptionGroupList();


                        // 그룹 목록을 검사해 중복된 프로퍼티만 남긴다. 이 기능은 추후 추가
                        this._filterDuplicateDescriptionGroupProperty(propertyPanelDescription);
				return propertyPanelDescription;
				////////////


			}
		}

      /*
      각각의 컴포넌트의 description()에서 label에 중복되는 그룹만 구하기.
       */
		private _getDuplicateDescriptionGroupList(){

                  let tempResultMap = new Map();
                  this._instanceList.forEach((instance:WVComponent)=>{
                        let tempPropertyDescription = instance.getPropertyPanelDescription();
                        for(let groupItem of tempPropertyDescription){

                              if(groupItem.template=="vertical")
                                    continue;

                              if(groupItem.template==null)
                                    continue;

                              /*
                              2018.05.03(ddandongne)
                              컴포넌트에 작성된 description에 template정보만 가지고 있는 컴포넌트가 있음.
                              template에는 아래와 같은 정보가 반드시 있어야 함.
                              {
						name:"pos-size-2d",
						label: "위치 크기 정보",
						template: "pos-size-2d",
						children: [{
							owner:"setter",
							name: "lock",
							type: "checkbox",
							label: "잠금",
							show: true,
							writable: true,
							description: "잠금 유무"
						}, {
							owner:"setter",
							name: "x",
							type: "number",
							label: "x",
							tag: 'px',
							show: true,
							writable: true,
							description: "x 위치 값"
						}, .......
					}

					추후 수정해야함.

                               */

                              if(groupItem.template=="primary" || groupItem.template=="pos-size-2d") {

                                    // 기존 저장한 내용이 있다면
                                    if (tempResultMap.has(groupItem.template)) {
                                          let targetItem = tempResultMap.get(groupItem.template);
                                          targetItem.count++;
                                    } else {
                                          //let temp = $.extend(true, {}, groupItem);
                                          // 없다면 신규로 생성.
                                          tempResultMap.set(groupItem.template, {
                                                count: 1,
                                                value: groupItem
                                          })
                                    }
                              }
                        }
                  });

                  // 모든 인스턴스 동일하게 가지고 있는 프로퍼티 정보 그룹만을 골란다
                  let instanceSize = this._instanceList.length;
                  let propertyPanelDescription = [];		// <--- 이곳에 공통 그룹만 담기게 됨.
                  tempResultMap.forEach((tempItem)=>{
                        // 인스턴스 개수와 = tempResultMap의 항목의 count와 동일하면 해당 그룹은 모든 인스턴스에 들어 있다고 판단.
                        if(tempItem.count==instanceSize){
                              propertyPanelDescription.push($.extend(true,{}, tempItem.value));
                        }
                  });
                  return propertyPanelDescription;
            }


            /*
            2018.05.04(ddandongne)
            그룹리스트에서 인스턴스별로 중복해서 가지고 있는 항목만 남기고 삭제하기
            이 부분은 추후 추가해야함.


             */
            public _filterDuplicateDescriptionGroupProperty(groupList:Array<any>){

                  // tempResultMap.clear();
                  //
                  // ////////////
                  // // 공통 그룹에서 공통 속성 정보 구하기;
                  // // 프로퍼티 패널 만들기
                  // for (let groupItem of propertyPanelDescription) {
                  // 	//2. 그룹의 속성 구하기(그룹 속성에서 완전 중복되는 속성만을 구하기
                  // 	tempResultMap.clear();  // tempResultMap 재사용.
                  // 	this._instanceList.forEach((instance)=> {
                  // 		let tempPropertyDescription:any = instance.getPropertyPanelDescription();
                  //
                  // 		// 컴포넌트의 프로퍼티 속성 중 찾은 프로퍼티 정보의 자식 정보를 찾는다.
                  // 		let tempChildren = null;
                  // 		for(let tempGroupItem of tempPropertyDescription){
                  //                  console.log("propertyPanelDescription  45 = ",tempGroupItem.label, " == ", groupItem.label);
                  // 			if(tempGroupItem.label==groupItem.label){
                  //                        console.log("propertyPanelDescription  46 = 찾은 대상 ", tempGroupItem.label);
                  // 				tempChildren = tempGroupItem.children;
                  // 				break;
                  // 			}
                  // 		}
                  //
                  // 		// 그룹의 자식 프로퍼티 목록에서 중복 소스 정보를 만든다.
                  // 		for (let item of tempChildren) {
                  // 			// name은 제외
                  // 			//if(item.name=="name")
                  // 			//	continue;
                  //
                  // 			if (tempResultMap.has(item.name)) {
                  // 				let targetItem = tempResultMap.get(item.name);
                  // 				targetItem.count++;
                  // 			} else {
                  // 				tempResultMap.set(item.name, {
                  // 					count: 1,
                  // 					value: item
                  // 				})
                  // 			}
                  //
                  //                  console.log("propertyPanelDescription  47 = 찾은 대상 ", tempResultMap.get(item.name).count);
                  // 		}
                  // 	});
                  //
                  //
                  //
                  // 	// 그룹에서 전체 중복되는 속성만을 찾아낸다.
                  // 	// 찾는 방법은 속성 카운터가 = 그룹
                  // 	groupItem.children = [];
                  // 	tempResultMap.forEach((item) => {
                  // 	      console.log("propertyPanelDescription 56 ", item.count, item.value.name);
                  // 		if (item.count == this._instanceList.length) {
                  // 		      let child = $.extend(true, {}, item.value);
                  // 			groupItem.children.push(child);
                  //                  console.log("propertyPanelDescription 57 ", item.count, item.value.name, groupItem.children.length);
                  // 		}
                  // 	})
                  //
                  //      console.log("propertyPanelDescription 58 ", groupItem.label, groupItem);
                  // }
                  //

            }

	}

	export class VirtualThreePropertyManager extends VirtualPropertyManager {

		constructor(layerName: WORK_LAYERS) {
			super(layerName);
		}

		onRegister() {
			this._propertySetterMap.set("position", this._position.bind(this));
			this._propertySetterMap.set("size", this._size.bind(this));
			this._propertySetterMap.set("rotation", this._rotation.bind(this));
		}

		private _position(value: WVVector) {
			let newPosition:WVVector = new WVVector();
			newPosition.copy(value);
			this._instanceList.forEach((instance:WV3DComponent) => {
				let tempPosition = new WVVector();
				tempPosition.copy(this.removeZeroValue(instance.position, newPosition));
				instance.position = tempPosition;
			});

			this._oldProperties.setter.position = JSON.parse(JSON.stringify(newPosition));
		}

		private _size(value: WVVector) {
			let newValue:WVVector = new WVVector();
			newValue.copy(value);
			this._instanceList.forEach((instance:WV3DComponent) => {
			      // todo
				let tempValue = new WVVector();
				tempValue.copy(this.removeZeroValue(instance.size, newValue));
				instance.size = tempValue;
			});

			this._oldProperties.setter.position = JSON.parse(JSON.stringify(newValue));
		}

		private _rotation(value: WVVector) {
			let newValue:WVVector = new WVVector();
			newValue.copy(value);
			this._instanceList.forEach((instance:WV3DComponent) => {
				let tempValue = new WVVector();
				tempValue.copy(this.removeZeroValue(instance.rotation, newValue));
				instance.rotation = tempValue;
			});

			this._oldProperties.setter.rotation = JSON.parse(JSON.stringify(newValue));
		}

		private removeZeroValue(source, value):WVVector {
		      let stats = ['x', 'y', 'z'];

		      stats.forEach((stat) => {
		            if (value[stat] === 0) {
                              value[stat] = source[stat];
                        }
                  })

		      return value;
            }

		/*
		x,y,width,height 값 구하기
		 */
		protected _calculateLocation():any{
			let info = {
				position:{x:0,y:0,z:0},
				size:{x:0,y:0,z:0},
				rotation:{x:0,y:0,z:0}
			}
			let targetInstance = null;
			if(this._instanceList.length==1){
				targetInstance = this._instanceList[0];
				// tempGroup = 선택 매시를 묶고 있는 그룹 매시
				let position = targetInstance.position;
				info.position.x = position.x;
				info.position.y = position.y;
				info.position.z = position.z;

				let size = targetInstance.size;
				info.size.x = size.x;
				info.size.y = size.y;
				info.size.z = size.z;

				let rotation = targetInstance.rotation;
				info.rotation.x = rotation.x;
				info.rotation.y = rotation.y;
				info.rotation.z = rotation.z;

			}
			return info;
		}

		protected _getEditablePropertiesManyComponent(){

			let tempProperties ={
				setter:{
					componentName:"",
					id:"",
					name:"",
					category:"",
					layerName:"",
					position:{
						x:0,
						y:0,
						z:0
					},
					size:{
						x:0,
						y:0,
						z:0
					},
					rotation:{
						x:0,
						y:0,
						z:0
					},
					opacity: 0
				},
				info:{},
				primary:{}
			}

			let firstProperties:IWVComponentProperties =  this.getFirst().properties;
			// 선택한 컴포넌트가 동일 컴포넌트인지 판단후 맞다면 properties와 propertyInfo를 해당 컴포넌트로 설정하기
			if (this._checkAllSameType()) {

				//console.log("컴포넌트가 동일한 경우 갯수 = ", this.componentListAry.size)
				tempProperties = $.extend(true, {}, this.getFirst().properties);

			}else {


				let propertyPanelDescription = this._getPropertyPanelDescriptionManyComponent();




				////////////////////////////////
				// 가상 속성 값 만들기.
				/*
				주의 !!!
				this._properties를 직접사용하는 경우 바인딩이 정상적으로 동작하지 않는 문제 발생.
				아마도 this.properties가 이미 바인딩에 사용하고 있기 때문에 완전히 새롭게 object를 만들어서 사용해야함.
				 */

				// propertyPanelDescription 에 맞게 프로퍼티 속성 만들기
				for(let propertyInfo of propertyPanelDescription){
					for(let childProperty of  propertyInfo.children) {
						let ownerProperty = tempProperties;


						if(childProperty.hasOwnProperty("owner")){
							// 그룹이 존재하지 않으면 신규로 만든다.
							if (tempProperties.hasOwnProperty(childProperty.owner) == false) {
								tempProperties[childProperty.owner] = {}
							}

							ownerProperty = tempProperties[childProperty.owner];
						}


						// 그룹에 자식 속성 값을 만든다.
						switch (childProperty.type) {

							case "checkbox":
								ownerProperty[childProperty.name] = false;
								break;
							case "color":
								ownerProperty[childProperty.name] = "#ff0000";
								break;

							case "number":
								ownerProperty[childProperty.name] = 1;
								break;
							case "range":
								ownerProperty[childProperty.name] = 1;
								break;
							case "string":
							default :
								ownerProperty[childProperty.name] = "";
								break;
						}

					}
				}
			}


			tempProperties.primary.id = "";
			tempProperties.primary.name = "";
			tempProperties.primary.layerName = firstProperties.primary.layerName

			tempProperties.info.componentName = PRIMARY_WVCOMPONENT.VIRTUAL_GROUP_COMPONENT_NAME;
			tempProperties.info.category = firstProperties.info.category;
			tempProperties.info.version = "";

			// 위치값 설정하기
			let rectInfo = this._calculateLocation();
			tempProperties.setter.componentName = "group";
			tempProperties.setter.id="";
			tempProperties.setter.name="";
			tempProperties.setter.position = rectInfo.position;
			tempProperties.setter.size = rectInfo.size;
			tempProperties.setter.rotation = rectInfo.rotation;
			tempProperties.setter.opacity = firstProperties.setter.opacity;

			if (firstProperties.label && tempProperties.label) {
                        for (let label_key in firstProperties.label) {
                              if (!(label_key in tempProperties.label)) {
                                    tempProperties.label[label_key] = firstProperties.label[label_key];
                              }
                        }
                  }

			return tempProperties;
		}
		protected _getPropertyPanelDescriptionManyComponent(){
			// 선택한 컴포넌트가 동일 컴포넌트인지 판단후 맞다면 properties와 propertyInfo를 해당 컴포넌트로 설정하기
			if (this._checkAllSameType()) {
				let firstInstance:WVComponent = this.getFirst();
				let propertyPanelDescription:Array<any> = firstInstance.getPropertyPanelDescription();

				//////////////
				return propertyPanelDescription;
			}else {

				// 1. 그룹 구하기
				let tempResultMap = new Map();
				this._instanceList.forEach((instance:WVComponent)=>{
					let tempPropertyDescription = instance.getPropertyPanelDescription();

					for(let groupItem of tempPropertyDescription){

						// 기존 저장한 내용이 있다면
						if(tempResultMap.has(groupItem.label)){
							let targetItem = tempResultMap.get(groupItem.label);
							targetItem.count++;
						}else {
							// 없다면 신규로 생성.
							tempResultMap.set(groupItem.label,{
								count:1,
								value:groupItem
							})
						}
					}
				});

				// 모든 인스턴스 동일하게 가지고 있는 프로퍼티 정보 그룹만을 골란다
				let instanceSize = this._instanceList.length;
				let propertyPanelDescription = [];		// <--- 이곳에 공통 그룹만 담기게 됨.
				tempResultMap.forEach((tempItem)=>{
					// 인스턴스 개수와 = tempResultMap의 항목의 count와 동일하면 해당 그룹은 모든 인스턴스에 들어 있다고 판단.
					if(tempItem.count==instanceSize){
						propertyPanelDescription.push(tempItem.value);
					}
				});
				////////////





				tempResultMap.clear();

				////////////
				// 공통 그룹에서 공통 속성 정보 구하기;
				// 프로퍼티 패널 만들기
				for (let groupItem of propertyPanelDescription) {
					//2. 그룹의 속성 구하기(그룹 속성에서 완전 중복되는 속성만을 구하기
					tempResultMap.clear();  // tempResultMap 재사용.
					this._instanceList.forEach((instance)=> {
						let tempPropertyDescription:any = instance.getPropertyPanelDescription();

						// 컴포넌트의 프로퍼티 속성 중 찾은 프로퍼티 정보의 자식 정보를 찾는다.
						let tempChildren = null;
						for(let tempGroupItem of tempPropertyDescription){
							if(tempGroupItem.label==groupItem.label){
								tempChildren = tempGroupItem.children;
								break;
							}
						}

						// 그룹의 자식 프로퍼티 목록에서 중복 소스 정보를 만든다.
						if(tempChildren){
                                          for (let item of tempChildren) {
                                                // name, textInfo 제외
                                                if(item.name === 'name' || item.name === 'text')
                                                      continue;

                                                if (tempResultMap.has(item.name)) {
                                                      let targetItem = tempResultMap.get(item.name);
                                                      targetItem.count++;
                                                } else {
                                                      tempResultMap.set(item.name, {
                                                            count: 1,
                                                            value: item
                                                      })
                                                }
                                          }
                                    }
					});



					// 그룹에서 전체 중복되는 속성만을 찾아낸다.
					// 찾는 방법은 속성 카운터가 = 그룹
					groupItem.children = [];
					tempResultMap.forEach((item) => {

						if (item.count == this._instanceList.length) {
							groupItem.children.push(Object.assign({}, item.value));
						}
					})
				}

				return propertyPanelDescription;
			}
		}
	}

	export class VirtualBackgroundManager extends VirtualPropertyManager {
		constructor(layerName: WORK_LAYERS) {
			super(layerName);
		}

		clearSelectedInstance(force: boolean = false) {
			if (force) {
				super.clearSelectedInstance();
			}
		}

		getInstanceLength(): number {
			return 0;
		}

		protected _getEditablePropertiesManyComponent(){
			return {}
		}
		protected _getPropertyPanelDescriptionManyComponent(){
			return {};
		}
	}
