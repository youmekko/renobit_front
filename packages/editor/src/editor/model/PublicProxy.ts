import * as puremvc from "puremvc";

class PublicProxy extends puremvc.Proxy {
	public static readonly NAME: string = "PublicProxy";

	private _comInstanceInfoListInMasterLayer:Array<any>;
	private _isCreateMasterLayer:boolean=false;
	constructor() {
		super(PublicProxy.NAME);
	}

	public onRegister() {

	}
	/////////////////////////////////////////////////////////////////////

	public setComInstanceInfoListInMasterLayer(layerInfoList:Array<any>){
		this._comInstanceInfoListInMasterLayer = layerInfoList;
	}

	public getComInstanceInfoListInMasterLayer(){
		return this._comInstanceInfoListInMasterLayer;
	}

	public getCreateMasterLayer(){
		return this._isCreateMasterLayer;
	}

	public completeCreateMasterLayer(){
		this._isCreateMasterLayer = true;
	}

	/////////////////////////////////////////////////////////////////////

}

export default PublicProxy;

