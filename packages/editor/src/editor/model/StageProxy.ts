import * as puremvc from "puremvc";
import {BackgroundInfo} from "../../wemb/wv/page/Page";

export default class StageProxy extends puremvc.Proxy {
      public static readonly NAME: string = "StageProxy";

      // 스테이지 초기정보 주입 ( 스테이지, 배경 정보 )
      public static readonly NOTI_STAGE_INFO_STATE: string = "notification/stageInfoState";
      public static readonly NOTI_MASTER_INFO_STATE: string = "notification/masterInfoState";
      public static readonly NOTI_PAGE_INFO_STATE: string = "notification/pageInfoState";

      public static readonly NOTI_BACKGROUND_INFO: string = "notification/backgroundInfo";


      public static readonly UPDATE_STAGE_BACKGROUND_INFO: string = "updateStageBackgroundInfo";
      public static readonly UPDATE_MASTER_BACKGROUND_INFO: string = "updateMasterBackgroundInfo";
      public static readonly UPDATE_PAGE_BACKGROUND_INFO: string = "updatePageBackgroundInfo";


      public backgroundInfo: any = {
            "stage": {"color": "#ff0000", "path": "", "image": ""},
            "master": {"color": "#ffffff", "path": "", "image": ""},
            "page": {"color": "#eeeeee", "path": "", "image": "", using: false},
      };

      private _scripts:any = {}

      constructor() {
            super(StageProxy.NAME);

      }

      public onRegister() {

      }

      public setPageBackgroundInfo(pageBGInfo: any) {
            this.backgroundInfo.page = pageBGInfo;
            this.sendNotification(StageProxy.NOTI_BACKGROUND_INFO, this.backgroundInfo);

            this.updatePageBackgroundInfo();

      }



      public setMasterBackgroundInfo(backgroundInfo: BackgroundInfo) {
            // 마스터 정보가 설정되는 경우 저장만 하고 배경을 업데이트 하지 않는다.
            // 이유는? 마스터와 페이지 배경 둘을 비교해서 사용하기 때문에
            this.backgroundInfo.master = backgroundInfo;
      }


      public setStageBackgroundInfo(backgroundInfo: BackgroundInfo) {
            this.backgroundInfo.stage = backgroundInfo;
            this.updateStageBackgroundInfo();
      }


      public updatePageBackgroundInfo() {
            if (this.backgroundInfo.page.using == true) {
                  window.wemb.pageBackgroundElement.setBackgroundPropertyValue("masterBackground", this.backgroundInfo.page);
            } else {
                  this.backgroundInfo.master.using = true;
                  window.wemb.pageBackgroundElement.setBackgroundPropertyValue("masterBackground", this.backgroundInfo.master);
            }
      }



      public updateStageBackgroundInfo() {
            this.backgroundInfo.stage.using = true;
            window.wemb.stageBackgroundElement.setBackgroundPropertyValue("stageBackground", this.backgroundInfo.stage);
      }


      public setGroupPropertyValue(groupName: string, propertyName: string, value: any) {
            let groupOwner = this.backgroundInfo[groupName];
            groupOwner[propertyName] = value;
      }


      public setScripts(scripts:object){
            //console.log("@@ scripts ", scripts);
            if(scripts==null){
                  scripts={
                        master:"var test1=100; //삭제 예정"
                  }
            }
            this._scripts = scripts;
      }
      public getScripts(){
            return this._scripts;
      }

}
