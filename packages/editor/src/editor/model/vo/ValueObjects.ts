import {WORK_LAYERS} from "../../../wemb/wv/layer/Layer";


export class ComponentInstanceInfoVO {
		public componentName:string;
		public label:string;
		public x:number=0;
		public y:number=0;
		public id?:string;
		public name?:string;
		public initProperties?:any;
            public sendSelectedNotification:boolean=false


}

	export class LayerVisibleVO {
		public layerName:WORK_LAYERS;
		public visible:boolean;
	}

	export class UpdatePropertyVO {
		public groupName:string="";
		public propertyName:string="";
		public value:any=null;
	}
