import PageMetaProperties from "../../../wemb/wv/components/PageMetaProperties";


export default class SavePageVO {

      private _data:any = {
            page_info:{},
            master_info:{
                  background:{
                        stage:{
                              color:"",
                              image:"",
                              path:""
                        },
                        master:{
                              color:"",
                              image:"",
                              path:""
                        }
                  }
            },
            content_info:{
                  two_layer:[],
                  three_layer:[]
            }

      };
      set page_info(value){
            this._data.page_info = value;
      }
      get page_info():PageMetaProperties{
            return this._data.page_info;
      }

      get master_info(){
            return this._data.master_info;
      }


      get content_info(){
            return this._data.content_info;
      }


      constructor(){
            /*
		this.data = {
			page_info: {
				id: "",
				name: "",
				secret:"N",
				props: {
					normal: {
						width: 1920,
						height: 1080,
						mode: true,
						type: "page",
						scene_info: "",
						template_yn: "N",
						camera: {
							x: -100,
							y: 100,
							z: 100
						}
					},
					background: {
						using: false,
						type: "color",
						data: {
							color: "#ff0000"
						}
					},
					events: {
						loadstart: "",
						loadcomplete: "",
						ready: "",
						unload: ""
					}
				}
			},
			master_info:{
				background:{
				      master:{
				            color:
				            image:
				            path:
				      },
				      stage:{
				            color:
				            image:
				            path:
				      }
				},
				master: [],
			},
			content_info: {
				two: [],
				three: []
			}
		}
		*/


            //  프로퍼티 속성 저보는 PageUIComponent 내용 그대로 사용하기
            // PageProperitesVO에서도 PageUIComponent가 사용됨

      }

      getPageData():any{
            return this._data;
      }
      getJSON(){
            return JSON.stringify(this._data);
      }


      // 페이지 저장용 json 구하기 (type:two_three인 경우 2d, 3d 내용 만
      getPageToJSON(){
            let temp:any  = $.extend(true, {}, this._data);
            delete temp.master_info;
            return JSON.stringify({type:"two_three", data:temp});

      }

      getMasterPageInfoToJSON(){
            let temp:any  = $.extend(true, {}, this._data);
            delete temp.page_info;
            return JSON.stringify({type:"master", data:temp});
      }

}
