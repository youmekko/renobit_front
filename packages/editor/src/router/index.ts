import Vue from 'vue'
import Router from 'vue-router'

import EditorMainComponent from '../editor/view/EditorMainComponent';
import DatasetManagerMainComponent from "../editor/windows/datasetManager/view/DatasetManagerMainComponent";
import ScriptEditorMainComponent from "../editor/windows/scriptEditor/view/ScriptEditorMainComponent";
import MultilingualEditorMainComponent from "../editor/windows/multilingualEditor/view/MultilingualEditorMainComponent";
import CodeCompilerComponent from '../editor/windows/codeCompiler/CodeCompiler';
import ErrorPageComponent from "../wemb/ErrorPageComponent.vue";

/*
const EditorMainComponent = () => import("../editor/view/EditorMainComponent");
const ViewerMainComponent = () => import("../viewer/view/ViewerMainComponent.vue");
const DatasetManagerMainComponent = () => import("../editor/windows/datasetManager/view/DatasetManagerMainComponent");
const ScriptEditorMainComponent = () => import("../editor/windows/scriptEditor/view/ScriptEditorMainComponent");
const MultilingualEditorMainComponent = () => import("../editor/windows/multilingualEditor/view/MultilingualEditorMainComponent");

//const Home = () => import("./Home.vue");

*/

Vue.use(Router);

export default new Router({
      routes: [
            {
                  path: '/',
                  name: 'editor',
                  component: EditorMainComponent
            }, {
                  path: '/editor',
                  component: EditorMainComponent
            },{
                  path: "/datasetMananger",
                  component: DatasetManagerMainComponent
            },{
                  path: "/scriptEditor",
                  component: ScriptEditorMainComponent
            },{
                  path: "/multilingualEditor",
                  component: MultilingualEditorMainComponent
            }, {
                  path: "/error/:code",
                  component: ErrorPageComponent
            },{
                  path: "/codeCompiler",
                  component: CodeCompilerComponent
            }
      ]
})
