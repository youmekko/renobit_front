"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var LGRangeDatePickerComponent = function(_WVDOMComponent) {
    _inherits(LGRangeDatePickerComponent, _WVDOMComponent);

    function LGRangeDatePickerComponent() {
        _classCallCheck(this, LGRangeDatePickerComponent);

        var _this = _possibleConstructorReturn(this, (LGRangeDatePickerComponent.__proto__ || Object.getPrototypeOf(LGRangeDatePickerComponent)).call(this));

        _this.dateFormat = "yy/mm/dd (D)";
        _this._oldStartDate = "";
        _this._oldEndDate = "";

        _this.$startPicker = null;
        _this.$endPicker = null;
        return _this;
    }

    _createClass(LGRangeDatePickerComponent, [{
        key: "_onCreateProperties",
        value: function _onCreateProperties() {}

        //element 생성

    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            $(this._element).append('<div class="lgu-datepicker-field start-field"><input type="text" class="lgu-single-picker" readonly size="30"></div> <div class="lgu-datepicker-field  end-field"><input type="text" class="lgu-single-picker" readonly size="30"></div>');
            this.createCalender();
        }
    }, {
        key: "createCalender",
        value: function createCalender() {
            $(this._element).addClass("lgu-datepicker lgu-date-range");

            var options = {
                showOn: "button",
                buttonImage: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTdDMjQyQTBBOUQ3MTFFODgxMUVDQ0Q2NjQ1MEJBOUYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QTdDMjQyQTFBOUQ3MTFFODgxMUVDQ0Q2NjQ1MEJBOUYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBN0MyNDI5RUE5RDcxMUU4ODExRUNDRDY2NDUwQkE5RiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBN0MyNDI5RkE5RDcxMUU4ODExRUNDRDY2NDUwQkE5RiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PqQkIqoAAAFxSURBVHjarNTPKwRxGMfx3W2xfrRsYZ0UF9yUqwslt00c5B8gF6UkeyDhJlc/blx2U1KOJC4kJVwUJ7GFpL0orYMf76c+U9O0w6489Wpnnn2+M89859kNHgynAoomXOELbbgP5I9WnOPT6rrWhjKWDLkKqlCOClQH/KNWdZWocZJhTCKOdc+CZYx6citIuROHI+klPh6toznd4a9haxesozLU4xlHevYnd9uuiOFWdR/WidaWhVWQwBb6dN6DgTwX6seOpy7h7JFFCdIFPEapX10o8E8Rdh3P4KTI9d1IejvawD6i6MCLNrVdjpWz76IMotWu5uvIiXF06q3Z3Cwqv4tBTOkG236P5oQV5XCBV+wpbyNxqa7PftojJ5Ke817X8aYU9NZsnrIYcw2qaVQuq5pfO4prghsQQZ3yUeViqvG9kHM8obuf4kHTbHGtt2R7d6dcxFkc5P8op9/bDTJFzlGzvFsXs5hGixQbb5j/FmAAjN9Lh09Z7wUAAAAASUVORK5CYII=",
                buttonImageOnly: true,
                showMonthAfterYear: true,
                yearSuffix: "년",
                monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
                dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                dayNamesShort: ["일", "월", "화", "수", "목", "금", "토"],
                dateFormat: this.dateFormat
            };

            var self = this;
            this.$startPicker = $(this._element).find(".start-field input").datepicker($.extend(true, {
                beforeShow: function beforeShow() {
                    $("#ui-datepicker-div").addClass("ll-skin-lugo");
                },
                onClose: function() {
                    this.changedDate();
                }.bind(this)
            }, options));

            this.$endPicker = $(this._element).find(".end-field input").datepicker($.extend(true, {
                beforeShow: function beforeShow() {
                    $("#ui-datepicker-div").addClass("ll-skin-lugo");
                },
                onClose: function() {
                    this.changedDate();
                }.bind(this)
            }, options));
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.$startPicker.datepicker("destroy");
            this.$startPicker = null;
            this.$endPicker.datepicker("destroy");
            this.$endPicker = null;

            this._oldStartDate = null;
            this._oldEndDate = null;

            _get(LGRangeDatePickerComponent.prototype.__proto__ || Object.getPrototypeOf(LGRangeDatePickerComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "changedDate",
        value: function changedDate() {
            if (this._oldStartDate == this.startDate && this._oldEndDate == this.endDate) {
                return;
            }
            this._oldStartDate = this.startDate;
            this._oldEndDate = this.endDate;

            this.$endPicker.datepicker("option", "minDate", this.startDate);
            this.$startPicker.datepicker("option", "maxDate", this.endDate);

            this.dispatchWScriptEvent("changeDate", {
                startDate: this.startDate,
                endDate: this.endDate
            });
        }
    }, {
        key: "startPicker",
        get: function get() {
            return this.$startPicker;
        }
    }, {
        key: "endPicker",
        get: function get() {
            return this.$endPicker;
        }
    }, {
        key: "startDate",
        set: function set(date) {
            this.$startPicker.datepicker("setDate", date);
            this.changedDate();
        },
        get: function get() {
            return this.$startPicker.datepicker("getDate");
        }
    }, {
        key: "endDate",
        set: function set(date) {
            this.$endPicker.datepicker("setDate", date);
            this.changedDate();
        },
        get: function get() {
            return this.$endPicker.datepicker("getDate");
        }
    }]);

    return LGRangeDatePickerComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(LGRangeDatePickerComponent, {
    "info": {
        "componentName": "LGRangeDatePickerComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 388,
        "height": 25,
        "date": ""
    },
    "label": {
        "label_using": "N",
        "label_text": "LGRangeDatePickerComponent Component"
    }
});

LGRangeDatePickerComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}];

WVPropertyManager.add_event(LGRangeDatePickerComponent, {
    name: "changeDate",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "date",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.remove_property_group_info(LGRangeDatePickerComponent, "label");
WVPropertyManager.remove_property_group_info(LGRangeDatePickerComponent, "background");
WVPropertyManager.remove_property_group_info(LGRangeDatePickerComponent, "border");



WVPropertyManager.add_property_group_info(LGRangeDatePickerComponent, {
    label: "LGRangeDatePickerComponent 고유 속성",
    children: [{
        name: "startDate",
        type: "Date",
        show: true,
        writable: true,
        defaultValue: 'new Date()',
        description: "선택한 from date 정보입니다."
    }, {
        name: "endDate",
        type: "Date",
        show: true,
        writable: true,
        defaultValue: 'new Date()',
        description: "선택한 to date 정보입니다."
    }, {
        name: "startPicker",
        type: "$datepicker",
        show: true,
        writable: false,
        description: "생성된 from Datepicker instance(jQuery)객체 입니다."
    }, {
        name: "endPicker",
        type: "$datepicker",
        show: true,
        writable: false,
        description: "생성된 to Datepicker instance(jQuery)객체 입니다."
    }]
});