class SampleDomComponent extends WVDOMComponent {

    constructor() {
        super();
        console.log("SampleDomComponent 시작!");
    }



      onLoadPage(){
          console.log("SampleDomComponent");
      }
}

WVPropertyManager.attach_default_component_infos(SampleDomComponent, {
    "info": {
        "componentName": "SampleDomComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100
    },

    "label": {
        "label_text": "Frame Component"
    },

    "style": {
        "border": "1px solid #000000",
        "backgroundColor": "#eeeeee",
        "borderRadius": 5,
        "cursor": "default"
    }
});

SampleDomComponent.property_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    template: "background"
}, {
    template: "border"
}];
