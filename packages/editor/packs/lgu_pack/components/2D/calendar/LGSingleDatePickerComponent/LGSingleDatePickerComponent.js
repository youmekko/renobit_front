class LGSingleDatePickerComponent extends WVDOMComponent {

    constructor() {
        super();
        this._oldDate = "";
        this.$datepicker = null;
    }

    _onCreateProperties() {}

    //element 생성
    _onCreateElement() {

        $(this._element).append('<div class="lgu-datepicker-field"><input type="text" class="lgu-single-picker" readonly size="30"></div>');
        this.createCalender();

    }

    createCalender() {
        $(this._element).addClass("lgu-datepicker");
        this.$datepicker = $(this._element).find(".lgu-single-picker").datepicker({
            beforeShow: function() { $("#ui-datepicker-div").addClass("ll-skin-lugo") },
            showOn: "button",
            buttonImage: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTdDMjQyQTBBOUQ3MTFFODgxMUVDQ0Q2NjQ1MEJBOUYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QTdDMjQyQTFBOUQ3MTFFODgxMUVDQ0Q2NjQ1MEJBOUYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBN0MyNDI5RUE5RDcxMUU4ODExRUNDRDY2NDUwQkE5RiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBN0MyNDI5RkE5RDcxMUU4ODExRUNDRDY2NDUwQkE5RiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PqQkIqoAAAFxSURBVHjarNTPKwRxGMfx3W2xfrRsYZ0UF9yUqwslt00c5B8gF6UkeyDhJlc/blx2U1KOJC4kJVwUJ7GFpL0orYMf76c+U9O0w6489Wpnnn2+M89859kNHgynAoomXOELbbgP5I9WnOPT6rrWhjKWDLkKqlCOClQH/KNWdZWocZJhTCKOdc+CZYx6citIuROHI+klPh6toznd4a9haxesozLU4xlHevYnd9uuiOFWdR/WidaWhVWQwBb6dN6DgTwX6seOpy7h7JFFCdIFPEapX10o8E8Rdh3P4KTI9d1IejvawD6i6MCLNrVdjpWz76IMotWu5uvIiXF06q3Z3Cwqv4tBTOkG236P5oQV5XCBV+wpbyNxqa7PftojJ5Ke817X8aYU9NZsnrIYcw2qaVQuq5pfO4prghsQQZ3yUeViqvG9kHM8obuf4kHTbHGtt2R7d6dcxFkc5P8op9/bDTJFzlGzvFsXs5hGixQbb5j/FmAAjN9Lh09Z7wUAAAAASUVORK5CYII=",
            buttonImageOnly: true,
            showMonthAfterYear: true,
            yearSuffix: "년",
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
            dayNamesShort: ["일", "월", "화", "수", "목", "금", "토"],
            dateFormat: "yy/mm/dd (D)"
        }).on("change", function() {
            this.changedDate();
        }.bind(this));
    }

    _onDestroy() {
        this.$datepicker.datepicker("destroy");
        this.$datepicker = null;
        super._onDestroy();
    }

    changedDate() {
        if (this._oldDate == this.date) { return; }
        this._oldDate = this.date;
        this.dispatchWScriptEvent("changeDate", {
            date: this.date
        });
    }

    get datepicker() {
        return this.$datepicker;
    }

    set date(date) {
        this.$datepicker.datepicker("setDate", date);
        this.changedDate();
    }

    get date() {
        return this.$datepicker.datepicker("getDate");
    }
}

WVPropertyManager.attach_default_component_infos(LGSingleDatePickerComponent, {
    "info": {
        "componentName": "LGSingleDatePickerComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 181,
        "height": 25,
        "date": ""
    },
    "label": {
        "label_using": "N",
        "label_text": "LGSingleDatePickerComponent Component"
    }
});

LGSingleDatePickerComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}]


WVPropertyManager.add_event(LGSingleDatePickerComponent, {
    name: "changeDate",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "date",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.remove_property_group_info(LGSingleDatePickerComponent, "label");
WVPropertyManager.remove_property_group_info(LGSingleDatePickerComponent, "background");
WVPropertyManager.remove_property_group_info(LGSingleDatePickerComponent, "border");



WVPropertyManager.add_property_group_info(LGSingleDatePickerComponent, {
    label: "LGSingleDatePickerComponent 고유 속성",
    children: [{
        name: "date",
        type: "Date",
        show: true,
        writable: true,
        defaultValue: 'new Date()',
        description: "선택한 date 정보입니다."
    }, {
        name: "datepicker",
        type: "$datepicker",
        show: true,
        writable: false,
        description: "생성된 jQuery Datepicker instance객체 입니다."
    }]
});