class LGDataGridTemplateComponent extends WVDOMComponent {
    constructor() {
        super();
        this._header = [];
        this._dataProvider = [];
        this.app = null;
    }

    set header(data) {
        this._dataProvider.splice(0, this.dataProvider.length);
        this._header.splice(0, this.header.length);
        this._header.push(...data);
    }

    get header() {
        return this._header;
    }

    set dataProvider(data) {
        // if(this.isEditorMode==true)
        //     return;

        this._dataProvider.splice(0, this._dataProvider.length);
        this._dataProvider.push(...data);
    }

    get dataProvider() {
        return this._dataProvider;
    }

    _onCreateElement() {
        this.header = [
            { key: "d_date", label: "일자", width: "150px" },
            { key: "detection", label: "탐지건수", width: "150px" },
            { key: "avg", label: "3개월 평균", width: "150px" },
            { key: "inc", label: "증감(%)", upDown: true }
        ];

        if (this.isEditorMode == true) { // 에디터모드에서 초기 값
            this.dataProvider = [
                { "d_date": "--/--/--", "detection": 0, "avg": 0, "inc": 0 },
                { "d_date": "--/--/--", "detection": 0, "avg": 0, "inc": 0 },
                { "d_date": "--/--/--", "detection": 0, "avg": 0, "inc": 0 },
                { "d_date": "--/--/--", "detection": 0, "avg": 0, "inc": 0 }
            ];
        }

        var grid =
            `<div class="table-wrap">
            <div class="header">
                <span v-for="(item, index) in header" :class="{flex: !item.width}" :style="{width:(item.hasOwnProperty('width') ? item.width:'')}">{{item.label}}</span>
            </div>
            <div class="table-body flex">
            <ul v-if="items.length > 0">
                <li v-for="(item, index) in items" class="list-wrap">
                    <div v-for="(header_item) in header" class="list" @click="rowClick(index)" @dblclick="rowDoubleClick(index)" :class="{flex: !header_item.width}" :style="{width:(header_item.hasOwnProperty('width') ? header_item.width:'')}">
                        <div v-if="header_item.upDown == true">
                            <span v-if="item[header_item.key] < 0"  class="color-blue">▼ {{Math.abs(item[header_item.key])}}</span>
                            <span v-else-if="item[header_item.key] === 0 ">{{item[header_item.key]}}</span>
                            <span v-else class="color-red">▲ {{item[header_item.key]}}</span>
                        </div>
                        
                        <div v-else>
                            <span>{{item[header_item.key]}}</span>
                        </div>
                    </div>
                </li>
            </ul>
            <div v-else class="no-data"><h3>No Data</h3></div>
            </div>
        </div>
        `;

        let temp = $(grid);
        $(this._element).append(temp);

        let dataProvider = this._dataProvider;
        let header = this._header;
        let self = this;
        this.app = new Vue({
            el: temp.get(0),
            data: {
                items: dataProvider,
                header: header
            },
            mounted() {
                $(this.$el).find(".table-body").addClass("scrollbar-inner").scrollbar();
            },
            methods: {
                rowClick: function(index) {
                    var rowData = self._dataProvider[index];
                    self.dispatchWScriptEvent("itemClick", {
                        value: rowData
                    })
                },
                rowDoubleClick: function(index) {
                    var rowData = self._dataProvider[index];
                    self.dispatchWScriptEvent("itemDoubleClick", {
                        value: rowData
                    })
                }
            }
        });
    }


    // 에디터/뷰어 레이어에 컴포넌트가 추가되면 호출되는 메서드
    _onImmediateUpdateDisplay() {

    }

    _onCommitProperties() {

    }

    _onDestroy() {
        // 생성한 객체 삭제
        if (this.app) {
            this.app.$destroy();
            this.app = null;
        }

        this._dataProvider = null;
        this._header = null;
        this.app = null;

        super._onDestroy();
    }
}

WVPropertyManager.attach_default_component_infos(LGDataGridTemplateComponent, {
    "info": {
        "componentName": "LGDataGridTemplateComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 600, // 기본 크기
        "height": 130, // 기본 크기
    }
});

WVPropertyManager.add_event(LGDataGridTemplateComponent, {
    name: "itemClick",
    label: "item click 이벤트",
    description: "item click 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "click한 row data 값입니다."
    }]
});

WVPropertyManager.add_event(LGDataGridTemplateComponent, {
    name: "itemDoubleClick",
    label: "item double click 이벤트",
    description: "item double click 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "double click한 row data 값입니다."
    }]
});
