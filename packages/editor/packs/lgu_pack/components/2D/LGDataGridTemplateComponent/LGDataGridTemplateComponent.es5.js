"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LGDataGridTemplateComponent = function (_WVDOMComponent) {
    _inherits(LGDataGridTemplateComponent, _WVDOMComponent);

    function LGDataGridTemplateComponent() {
        _classCallCheck(this, LGDataGridTemplateComponent);

        var _this = _possibleConstructorReturn(this, (LGDataGridTemplateComponent.__proto__ || Object.getPrototypeOf(LGDataGridTemplateComponent)).call(this));

        _this._header = [];
        _this._dataProvider = [];
        _this.app = null;
        return _this;
    }

    _createClass(LGDataGridTemplateComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.header = [{ key: "d_date", label: "일자", width: "150px" }, { key: "detection", label: "탐지건수", width: "150px" }, { key: "avg", label: "3개월 평균", width: "150px" }, { key: "inc", label: "증감(%)", upDown: true }];

            if (this.isEditorMode == true) {
                // 에디터모드에서 초기 값
                this.dataProvider = [{ "d_date": "--/--/--", "detection": 0, "avg": 0, "inc": 0 }, { "d_date": "--/--/--", "detection": 0, "avg": 0, "inc": 0 }, { "d_date": "--/--/--", "detection": 0, "avg": 0, "inc": 0 }, { "d_date": "--/--/--", "detection": 0, "avg": 0, "inc": 0 }];
            }

            var grid = "<div class=\"table-wrap\">\n            <div class=\"header\">\n                <span v-for=\"(item, index) in header\" :class=\"{flex: !item.width}\" :style=\"{width:(item.hasOwnProperty('width') ? item.width:'')}\">{{item.label}}</span>\n            </div>\n            <div class=\"table-body flex\">\n            <ul v-if=\"items.length > 0\">\n                <li v-for=\"(item, index) in items\" class=\"list-wrap\">\n                    <div v-for=\"(header_item) in header\" class=\"list\" @click=\"rowClick(index)\" @dblclick=\"rowDoubleClick(index)\" :class=\"{flex: !header_item.width}\" :style=\"{width:(header_item.hasOwnProperty('width') ? header_item.width:'')}\">\n                        <div v-if=\"header_item.upDown == true\">\n                            <span v-if=\"item[header_item.key] < 0\"  class=\"color-blue\">\u25BC {{Math.abs(item[header_item.key])}}</span>\n                            <span v-else-if=\"item[header_item.key] === 0 \">{{item[header_item.key]}}</span>\n                            <span v-else class=\"color-red\">\u25B2 {{item[header_item.key]}}</span>\n                        </div>\n                        \n                        <div v-else>\n                            <span>{{item[header_item.key]}}</span>\n                        </div>\n                    </div>\n                </li>\n            </ul>\n            <div v-else class=\"no-data\"><h3>No Data</h3></div>\n            </div>\n        </div>\n        ";

            var temp = $(grid);
            $(this._element).append(temp);

            var dataProvider = this._dataProvider;
            var header = this._header;
            var self = this;
            this.app = new Vue({
                el: temp.get(0),
                data: {
                    items: dataProvider,
                    header: header
                },
                mounted: function mounted() {
                    $(this.$el).find(".table-body").addClass("scrollbar-inner").scrollbar();
                },

                methods: {
                    rowClick: function rowClick(index) {
                        var rowData = self._dataProvider[index];
                        self.dispatchWScriptEvent("itemClick", {
                            value: rowData
                        });
                    },
                    rowDoubleClick: function rowDoubleClick(index) {
                        var rowData = self._dataProvider[index];
                        self.dispatchWScriptEvent("itemDoubleClick", {
                            value: rowData
                        });
                    }
                }
            });
        }

        // 에디터/뷰어 레이어에 컴포넌트가 추가되면 호출되는 메서드

    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {}
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {}
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            // 생성한 객체 삭제
            if (this.app) {
                this.app.$destroy();
                this.app = null;
            }

            this._dataProvider = null;
            this._header = null;
            this.app = null;

            _get(LGDataGridTemplateComponent.prototype.__proto__ || Object.getPrototypeOf(LGDataGridTemplateComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "header",
        set: function set(data) {
            var _header;

            this._dataProvider.splice(0, this.dataProvider.length);
            this._header.splice(0, this.header.length);
            (_header = this._header).push.apply(_header, _toConsumableArray(data));
        },
        get: function get() {
            return this._header;
        }
    }, {
        key: "dataProvider",
        set: function set(data) {
            var _dataProvider;

            // if(this.isEditorMode==true)
            //     return;

            this._dataProvider.splice(0, this._dataProvider.length);
            (_dataProvider = this._dataProvider).push.apply(_dataProvider, _toConsumableArray(data));
        },
        get: function get() {
            return this._dataProvider;
        }
    }]);

    return LGDataGridTemplateComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(LGDataGridTemplateComponent, {
    "info": {
        "componentName": "LGDataGridTemplateComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 600, // 기본 크기
        "height": 130 // 기본 크기
    }
});

WVPropertyManager.add_event(LGDataGridTemplateComponent, {
    name: "itemClick",
    label: "item click 이벤트",
    description: "item click 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "click한 row data 값입니다."
    }]
});

WVPropertyManager.add_event(LGDataGridTemplateComponent, {
    name: "itemDoubleClick",
    label: "item double click 이벤트",
    description: "item double click 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "double click한 row data 값입니다."
    }]
});
