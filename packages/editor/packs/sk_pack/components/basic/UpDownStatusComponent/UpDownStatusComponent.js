class UpDownStatusComponent extends WVDOMComponent {

    constructor() {
        super();
        this._$contents;

        this._$img;
        this._$text;

        this._isResourceComponent = true;
        this._invalidatePropertyStatus = false;
        this._invalidatePropertyValue = false;

    }


    onLoadPage() {

    }


    _onCreateElement() {
        let $el = $(this._element);

        let temp = "<div class='comp-wrap'><img src='' alt='icon'><span class='txt'>TEST</span></div>";

        $el.append(temp);

        this._$contents = $el.find(".comp-wrap");
        this._$img = $el.find("img");
        this._$text = $el.find(".txt");
    }




    _onImmediateUpdateDisplay() {
        this._validateUpDownValue();
        this._validateUpDownStatus();
        this._validateFontStyleProperty();
    }



    _validateUpDownStatus() {




        let margin1 = this.getGroupProperties("upDown").margin;

        let margin2 = this.getGroupPropertyValue("upDown", "margin");



        this._$img.css("margin-right", this.getGroupPropertyValue("upDown", "margin"));

        if (this.status == "up") {
            this._$text.css("color", this.getGroupPropertyValue("upDown", "upColor"));

        } else if (this.status == "down") {
            this._$text.css("color", this.getGroupPropertyValue("upDown", "downColor"));

        } else if (this.status == "same") {
            this._$text.css("color", this.getGroupPropertyValue("upDown", "sameColor"));
        }
    }


    _onCommitProperties() {
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty);
        }

        if (this._invalidatePropertyStatus || this._updatePropertiesMap.get("upDown")) {
            this._validateUpDownStatus();
            this.loadImage().then(() => {

            }, (error) => {

            });


            this._invalidatePropertyStatus = false;
        }

        if (this._invalidatePropertyValue) {
            this.validateCallLater(this._validateUpDownValue);
            this._invalidatePropertyValue = false;
        }
    }

    _validateUpDownValue() {

        let value = this.getGroupPropertyValue("setter", "value");

        if (!value) {
            this._$text.text("-");
        } else {
            this._$text.text(value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }

        var upDownValues = this.getGroupProperties("setter");
        this.dispatchWScriptEvent("change", {
            status: upDownValues.status,
            value: upDownValues.value
        });
    }




    async startLoadResource() {
        try {
            let success = await this.loadImage();
        } catch (error) {
            console.log("error ", error);
        } finally {
            this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
        }
    }

    getImageByStatus(status) {
        let res = this.getGroupPropertyValue("upDown", status + "Img");

        let path = "";
        if (res) {
            path = res.path;
        }

        return path;
    }

    getImgUrl(path) {
        if (path.indexOf("data:image/jpg;base64") != -1) {
            return path;
        } else {
            return wemb.configManager.serverUrl + path;
        }
    }

    loadImage() {

        let url = this.getImgUrl(this.getImageByStatus(this.status));

        return new Promise((resolve, reject) => {
            if (url == "") {
                this._$img.hide();
                reject("error");
            } else {
                this._$img.show();
                this._$img.off("error").on("error", () => {
                    this._$img.hide();
                    reject("error");
                }).attr("src", url);

                this._$img.off("load").on("load", () => {
                    resolve(true);

                })
            }
        });
    }


    _validateFontStyleProperty() {
        var fontProps = this.font;
        var $txt = $(this._element).find(".txt");
        $txt.css({
            "fontFamily": fontProps.font_type,
            "fontSize": fontProps.font_size,
            "fontWeight": fontProps.font_weight,
            "fontColor": fontProps.font_color,
            "textAlign": fontProps.text_align
        });
    }

    set status(status) {
        this._invalidatePropertyStatus = this._checkUpdateGroupPropertyValue("setter", "status", status);
    }

    get status() {
        return this.getGroupPropertyValue("setter", "status");
    }

    set value(value) {
        this._invalidatePropertyValue = this._checkUpdateGroupPropertyValue("setter", "value", value);
    }

    get value() {
        return this.getGroupPropertyValue("setter", "value");
    }

    get font() {
        return this.getGroupProperties("font");
    }


    _onDestroy() {
        this._$contents.remove();
        this._$contents = null;
        this._$img = null;
        this._$text = null;
        super._onDestroy();
    }


}


WVPropertyManager.attach_default_component_infos(UpDownStatusComponent, {
    "info": {
        "componentName": "UpDownStatusComponent",
        "version": "1.0.0"
    },

    "label": {
        "label_using": "N",
        "label_text": "UpDownStatus Component"
    },

    "setter": {
        "width": 100,
        "height": 25,
        "status": "up", //same, down
        "value": 0,
    },

    "font": {
        "font_type": "inherit",
        "font_size": 14,
        "font_weight": "normal",
        "font_color": "#333333",
        "text_align": "left"
    },

    "upDown": {
        "margin": 10,
        "upColor": "#FF0000",
        "upImg": {
            "path": 'data:image/jpg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMxaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzEzOCA3OS4xNTk4MjQsIDIwMTYvMDkvMTQtMDE6MDk6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE3IChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMwNzNBMzY5MjRDRjExRTg4OUI5Q0E5NENBNEUxNkYyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMwNzNBMzZBMjRDRjExRTg4OUI5Q0E5NENBNEUxNkYyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzA3M0EzNjcyNENGMTFFODg5QjlDQTk0Q0E0RTE2RjIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzA3M0EzNjgyNENGMTFFODg5QjlDQTk0Q0E0RTE2RjIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCAAoADIDAREAAhEBAxEB/8QAjAAAAwADAQAAAAAAAAAAAAAAAAUHAQMGBAEAAwEBAQAAAAAAAAAAAAAAAAMFBAECEAABAwICCAIGCwAAAAAAAAACAQMEAAYRBSExEtITo1QWQXHwUWGBIhWx0eEyYiODs6RVNhEAAgEDAgUDBQEAAAAAAAAAAAECUQMUIbERMUESBIGhIvAyUhMjM//aAAwDAQACEQMRAD8Au9rWtkM7IYsqVF4j7nE2z4jg47LhCmgSRNSVu8jyJxm0noY7FiEoJtDbsi1+i5ru/Scq5XYbjQoHZFr9FzXd+jKuV2DGhQOyLX6Lmu79GVcrsGNCgdkWv0XNd36Mq5XYMaFA7Itfoua7v0ZVyuwY0KEpquSyrWR/l4X6v7p1I8r/AEf10KnjfYhnHzODJlPxWHhN+NhxgTwx+n20qUGkm+o1TTfBdD014PQUAcVIHKJM/NSz2STMqO4SQwUyDYZRMWyaFFTaJa3x7lGPYtHzMT7W33vU6O23pj2Rw3ZmKyCDElLWqYrsqvmOC1lvpKb4cjTZbcVxI/VokHc29PnS8kiZNlaq26KGs2YqaGQJ0lRB9Zkmr0wwXoKM3KXoqm21JuKjH1Y3k24WXjElZG2izInwuNkSJx2yX40MlwTHxx+ykxvd3FT5P2HSs9vBx5r3MuZ3nsE25GbQ2m8uNdlw2CJw2lXUR+Gz5VxWoS0i/kDuSjrJaDVvOsmcMQbnxzMlwERdBVVV8ETGku1JdGNVyNUbn4MKQYuPx23TD7hmAkqeSqmiuKbXJnXFPmjdXk9ENq+RCqWM2A2zFIRRCNXCNUTSqo4SYr7kRKkeU/6MqeMvgh9WceYMAcAgMUMCRUISTFFRdaKi0Jg0L37cyF5omigMChJgpA2IEnkQoipTVemnzYt2YPojwNt3LlK8CO183hYfk7bgtPN/hIi0ElMbtz1fxYtKcNF8kbPm90f0H8tr6q5+u3+Xszv7J/j7kpquSygWtdOQwchixZUrhvt8TbDhuFhtOESaRFU1LU7yPHnKbaWhvsX4Rgk2Nu97X63lO7lJxblNhuTCod72v1vKd3KMW5TYMmFQ73tfreU7uUYtymwZMKh3va/W8p3coxblNgyYVDve1+t5Tu5Ri3KbBkwqSmq5LP/Z'
        },
        "downColor": "#0890FF",
        "downImg": "",
        "sameColor": "#0B8812",
        "sameImg": ""
    },

    "style": {
        "cursor": "default",
    }
});


UpDownStatusComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "font",
    label: "font"
}, {
    label: "Up & Down",
    owner: "vertical",
    children: [{
            owner: "setter",
            name: "status",
            type: "select",
            options: {
                items: [
                    { label: "Up", value: "up" },
                    { label: "Down", value: "down" },
                    { label: "Same", value: "same" },
                ]
            },
            label: "status",
            show: true,
            writable: true,
            description: "상태 정보"
        },
        {
            owner: "setter",
            name: "value",
            type: "number",
            label: "value",
            show: true,
            writable: true,
            description: "실제값"
        },
        {
            owner: "upDown",
            name: "margin",
            type: "number",
            label: "gap",
            show: true,
            writable: true,
            description: "이미지 오른쪽 마진"
        }, {
            owner: "upDown",
            name: "upColor",
            type: "color",
            label: "upColor",
            show: true,
            writable: true,
            description: "업 색상"

        }, {
            owner: "upDown",
            name: "downColor",
            type: "color",
            label: "downColor",
            show: true,
            writable: true,
            description: "다운 색상"

        }, {
            owner: "upDown",
            name: "sameColor",
            type: "color",
            label: "sameColor",
            show: true,
            writable: true,
            description: "동일시 색상"

        }
    ]
}, {
    label: "resource",
    template: "resource",
    children: [{
        owner: "upDown",
        name: "upImg",
        type: "resource",
        label: "upImg",
        resource_options: {
            minLength: 1,
            maxLenght: 1,
            type: "image"
        },
        show: true,
        writable: true,
        description: "업 이미지 리소스 선택"
    }, {
        owner: "upDown",
        name: "downImg",
        type: "resource",
        label: "downImg",
        resource_options: {
            minLength: 1,
            maxLenght: 1,
            type: "image"
        },
        show: true,
        writable: true,
        description: "다운 이미지 리소스 선택"
    }, {
        owner: "upDown",
        name: "sameImg",
        type: "resource",
        label: "sameImg",
        resource_options: {
            minLength: 1,
            maxLenght: 1,
            type: "image"
        },
        show: true,
        writable: true,
        description: "same 이미지 리소스 선택"
    }]
}]



WVPropertyManager.add_event(UpDownStatusComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다",
    properties: [{
            name: "stauts",
            type: "select",
            default: "",
            description: "새로운 상태"
        },
        {
            name: "value",
            type: "number",
            default: "",
            description: "새로운 값"
        }
    ]
});



WVPropertyManager.remove_property_group_info(UpDownStatusComponent, "label");
WVPropertyManager.remove_property_group_info(UpDownStatusComponent, "background");
WVPropertyManager.remove_property_group_info(UpDownStatusComponent, "border");

WVPropertyManager.add_property_group_info(UpDownStatusComponent, {
    label: "UpDownStatusComponent 고유 속성",
    children: [{
        name: "value",
        type: "string",
        show: true,
        writable: true,
        description: "라벨에 출력할 값 입니다."
    }, {
        name: "status",
        type: "string",
        show: true,
        writable: true,
        description: "상태 값 입니다. up/down/same 중 하나의 값을 가집니다. "
    }]
});