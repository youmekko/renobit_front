"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IFrameComponent = function(_WVDOMComponent) {
    _inherits(IFrameComponent, _WVDOMComponent);

    function IFrameComponent() {
        _classCallCheck(this, IFrameComponent);

        var _this = _possibleConstructorReturn(this, (IFrameComponent.__proto__ || Object.getPrototypeOf(IFrameComponent)).call(this));

        _this.invalidateLinkUrl = false;
        _this.invalidateFrameScroll = false;

        return _this;
    }

    _createClass(IFrameComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this._element.classList.add("iframe-component");

            if (this.isViewerMode == true) {
                this._iframe = document.createElement("iframe");
                this._iframe.style.width = "100%";
                this._iframe.style.height = "100%";
                this._element.appendChild(this._iframe);
            } else {
                $(this._element).addClass("isEditor");
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            if (this.isEditorMode == false && this._iframe) {
                $(this._iframe).off();
                this._element.removeChild(this._iframe);
                this._iframe = null;
            }

            _get(IFrameComponent.prototype.__proto__ || Object.getPrototypeOf(IFrameComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            if (this.isViewerMode == true) {
                this._validateLinkUrl();
                this._validateFrameScroll();
            }
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {

            if (this.invalidateLinkUrl) {
                this.validateCallLater(this._validateLinkUrl);
                this.invalidateLinkUrl = false;
            }

            if (this.invalidateFrameScroll) {
                this.validateCallLater(this._validateFrameScroll);
                this.invalidateFrameScroll = false;
            }
        }
    }, {
        key: "_validateLinkUrl",
        value: function _validateLinkUrl() {
            if (this.isViewerMode == true) {
                this._iframe.src = this.link_url;
            }
        }
    }, {
        key: "_validateFrameScroll",
        value: function _validateFrameScroll() {
            if (this.isViewerMode == true) {
                this._iframe.scrolling = this.link_scrolling;
            }
        }
    }, {
        key: "link_url",
        set: function set(url) {
            if (this._checkUpdateGroupPropertyValue("link", "link_url", url)) {
                this.invalidateLinkUrl = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("link", "link_url");
        }
    }, {
        key: "link_scrolling",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("link", "invalidateFrameScroll", value)) {
                this.invalidateFrameScroll = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("link", "link_scrolling");
        }
    }]);

    return IFrameComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(IFrameComponent, {
    "info": {
        "componentName": "IFrameComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100
    },

    "label": {
        "label_using": "N",
        "label_text": "IFrame Component"
    },

    "style": {
        "border": "1px dashed #808080",
        "backgroundColor": "rgba(255, 255, 255, 0.6)",
        "borderRadius": 0
    },

    "link": {
        "link_url": "",
        "link_scrolling": "auto"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
IFrameComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    template: "background"
}, {
    template: "border"
}, {
    label: "Link",
    template: "vertical",
    children: [{
        owner: "link",
        name: "link_url",
        type: "string",
        label: "url",
        show: true,
        writable: true,
        description: "링크입력"
    }, {
        owner: "link",
        name: "link_scrolling",
        type: "select",
        label: "scroll",
        show: true,
        writable: true,
        description: "스크롤 옵션",
        options: {
            items: [{ label: "auto", value: "auto" }, { label: "hidden", value: "hidden" }, { label: "scroll", value: "scroll" }]
        }
    }]
}];

WVPropertyManager.add_property_group_info(IFrameComponent, {
    label: "IFrameComponent 고유 속성",
    children: [{
        name: "link_url",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'url..'",
        description: "IFrame에 적용할 src 정보입니다."
    }]
});