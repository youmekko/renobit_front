class DigitalClockComponent extends WVDOMComponent {

    constructor() {
        super();

        this.$el;
        this.$txt;
        this.clockData = {
            intervalTime: 1000,
            dist: 0, //서버와 로컬 시간 차
            currentTime: 0, //현재 시간
            currentDate: null,
            intervalID: null,
            initFlag: false
        };

        this._oldTime = "";
        this.invalidateDateFormat = false;
        this._isDestroyed = false;
    }

    getExtensionProperties() {
        return true;
    }


    _onDestroy() {
        this.clearTime();
        this.$el = null;
        this.$txt.remove();
        this.$txt = null;
        this._isDestroyed = true;
        this.clockData = null;
        super._onDestroy();
    }

    _onImmediateUpdateDisplay() {
        this.display(new Date());
        this.loadServerTime();
        this._validateFontStyleProperty();
    }

    _onCreateElement() {
        this.$el = $(this._element);
        this.$el.css({
            "text-align": "center",
            "display": "flex",
            "align-items": "center",
            "justify-content": "center"
        });
        this.$el.append('<span class="date-txt"></span>');
        this.$txt = this.$el.find(".date-txt");
    }



    _onCommitProperties() {
        if (this.invalidateDateFormat) {
            this.validateCallLater(this._validateDateFormat);
        }

        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty)
        }
    }

    _validateFontStyleProperty() {
        var fontProps = this.font;
        this.$txt.css({
            "fontFamily": fontProps.font_type,
            "fontSize": fontProps.font_size,
            "fontWeight": fontProps.font_weight,
            "color": fontProps.font_color
        })
    }

    _validateDateFormat() {
        this.invalidateDateFormat = false;
        this.display(this.getDate());
    }

    set dateFormat(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "dateFormat", value)) {
            this.invalidateDateFormat = true;
        }
    }


    get dateFormat() {
        return this.getGroupPropertyValue("setter", "dateFormat");
    }


    get font() {
        return this.getGroupProperties("font");
    }






    /* 서버시간 기준으로 로컬 시간 설정
     * @param date 서버시간 Date객체
     */

    setTime(date) {
        this.clockData.dist = date.getTime() - (new Date()).getTime();
        this.clockData.currentTime = this.getTime();
    }

    /**
     * 계산된 현재 시간 Date 객체 전달
     * @return Date
     */
    getDate() {
        var currentTime = (new Date()).getTime() + this.clockData.dist;
        var date = new Date(currentTime);
        return date;
    }


    /**
     * 계산된 현재 시간 Time값 전달
     * @return Integer
     */
    getTime() {
        return this.getDate().getTime();
    }


    loadServerTime() {
        let self = this;
        self.clockData.initFlag = true;

        let PATH = wemb.configManager.serverUrl + "/editor.do";
        let data = {
            "id": "editorService.getCurrentTime",
            "params": {}
        };

        return http.post(PATH, data).then((result) => {
            if (result && result.data && result.data.currentTime && !self._isDestroyed) {
                self.initTime(new Date(result.data.currentTime));
            } else {
                self.initTime(new Date());
            }
        }, (result) => {
            if (!self._isDestroyed) {
                self.initTime(new Date());
            }
        });
    }


    initTime(date) {
        this.setTime(date);
        this.start();
    }

    /**
     * 서버시간 기준 로컬 시간 계산 interval 실행
     */
    start() {
        this.clearTime();
        this.clockData.intervalID = setInterval(this.updateTime.bind(this), this.clockData.intervalTime);
    }


    clearTime() {
        if (this.clockData.intervalID) {
            clearInterval(this.clockData.intervalID);
            this.clockData.intervalID = null;
        }
    }

    updateTime() {
        this.clockData.currentTime += this.clockData.intervalTime;
        var distance = this.clockData.currentTime - this.getTime();
        if (!this.clockData.initFlag) {
            return;
        }

        if (Math.abs(distance) > 1000) {
            //시간이 안맞으면(중간에 사용자가 시간을 변경하거나 부하로 인한)
            this.clockData.initFlag = false;
            this.clearTime();
            this.loadServerTime();
        } else {
            ///시간 변동시 tirgger
            var currentDate = this.getDate();
            if (currentDate.getDate() != this.clockData.currentDate) {
                //날짜 변동시
                this.clockData.currentDate = currentDate.getDate();
            }

            this.display(currentDate);
        }
    }

    getDigitNum(no) {
        no = parseInt(no);
        if (no < 10) {
            no = "0" + no;
        }
        return no;
    }

    display(date) {
        let newTime = date.toString();
        this.$txt.html(DateFormat.format.date(date, this.dateFormat));
        this._oldTime = newTime;
    }

}


WVPropertyManager.attach_default_component_infos(DigitalClockComponent, {
    "info": {
        "componentName": "Digital Clock Component",
        "version": "1.0.0"
    },

    "setter": {
        "width": 214,
        "height": 32,
        "dateFormat": "yyyy-MM-dd HH:mm:ss"
    },

    "label": {
        "label_using": "N",
        "label_text": "Frame Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 20,
        "font_weight": "normal"
    },

    "style": {
        "display": "flex",
        "border": "1px none #000000",
        "backgroundColor": "rgba(255, 255, 255, 0)",
        "borderRadius": 0
    }
});


DigitalClockComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        template: "label"
    },
    {
        template: "font"
    }, {
        label: "Date Format",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "dateFormat",
            type: "string",
            label: "format",
            show: true,
            writable: true,
            description: "date format 설정"
        }]
    }
];


WVPropertyManager.add_property_group_info(DigitalClockComponent, {
    label: "DigitalClockComponent 고유 속성",
    children: [{
        name: "dateFormat",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'yyyy-MM-dd HH:mm:ss'",
        description: "Date 정보를 표기하기 위한 format(확장팝업 참조)입니다.\n ex) 'yyyy-MM-dd HH:mm:ss' "
    }]
});


//  추후 추가 예정
WVPropertyManager.add_method_info(DigitalClockComponent, {
    name: "getDate",
    description: "현재 표기되고 있는 Date정보를 반환합니다. type은 Date객체입니다."
});