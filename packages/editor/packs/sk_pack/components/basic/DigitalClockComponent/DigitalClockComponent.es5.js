"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DigitalClockComponent = function(_WVDOMComponent) {
    _inherits(DigitalClockComponent, _WVDOMComponent);

    function DigitalClockComponent() {
        _classCallCheck(this, DigitalClockComponent);

        var _this = _possibleConstructorReturn(this, (DigitalClockComponent.__proto__ || Object.getPrototypeOf(DigitalClockComponent)).call(this));

        _this.$el;
        _this.$txt;
        _this.clockData = {
            intervalTime: 1000,
            dist: 0, //서버와 로컬 시간 차
            currentTime: 0, //현재 시간
            currentDate: null,
            intervalID: null,
            initFlag: false
        };

        _this._oldTime = "";
        _this.invalidateDateFormat = false;
        _this._isDestroyed = false;
        return _this;
    }

    _createClass(DigitalClockComponent, [{
        key: "getExtensionProperties",
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.clearTime();
            this.$el = null;
            this.$txt.remove();
            this.$txt = null;
            this._isDestroyed = true;
            this.clockData = null;
            _get(DigitalClockComponent.prototype.__proto__ || Object.getPrototypeOf(DigitalClockComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.display(new Date());
            this.loadServerTime();
            this._validateFontStyleProperty();
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.$el = $(this._element);
            this.$el.css({
                "text-align": "center",
                "display": "flex",
                "align-items": "center",
                "justify-content": "center"
            });
            this.$el.append('<span class="date-txt"></span>');
            this.$txt = this.$el.find(".date-txt");
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this.invalidateDateFormat) {
                this.validateCallLater(this._validateDateFormat);
            }

            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this._validateFontStyleProperty);
            }
        }
    }, {
        key: "_validateFontStyleProperty",
        value: function _validateFontStyleProperty() {
            var fontProps = this.font;
            this.$txt.css({
                "fontFamily": fontProps.font_type,
                "fontSize": fontProps.font_size,
                "fontWeight": fontProps.font_weight,
                "color": fontProps.font_color
            });
        }
    }, {
        key: "_validateDateFormat",
        value: function _validateDateFormat() {
            this.invalidateDateFormat = false;
            this.display(this.getDate());
        }
    }, {
        key: "setTime",


        /* 서버시간 기준으로 로컬 시간 설정
         * @param date 서버시간 Date객체
         */

        value: function setTime(date) {
            this.clockData.dist = date.getTime() - new Date().getTime();
            this.clockData.currentTime = this.getTime();
        }

        /**
         * 계산된 현재 시간 Date 객체 전달
         * @return Date
         */

    }, {
        key: "getDate",
        value: function getDate() {
            var currentTime = new Date().getTime() + this.clockData.dist;
            var date = new Date(currentTime);
            return date;
        }

        /**
         * 계산된 현재 시간 Time값 전달
         * @return Integer
         */

    }, {
        key: "getTime",
        value: function getTime() {
            return this.getDate().getTime();
        }
    }, {
        key: "loadServerTime",
        value: function loadServerTime() {
            var self = this;
            self.clockData.initFlag = true;

            var PATH = wemb.configManager.serverUrl + "/editor.do";
            var data = {
                "id": "editorService.getCurrentTime",
                "params": {}
            };

            return http.post(PATH, data).then(function(result) {
                if (result && result.data && result.data.currentTime && !self._isDestroyed) {
                    self.initTime(new Date(result.data.currentTime));
                } else {
                    self.initTime(new Date());
                }
            }, function(result) {
                if (!self._isDestroyed) {
                    self.initTime(new Date());
                }
            });
        }
    }, {
        key: "initTime",
        value: function initTime(date) {
            this.setTime(date);
            this.start();
        }

        /**
         * 서버시간 기준 로컬 시간 계산 interval 실행
         */

    }, {
        key: "start",
        value: function start() {
            this.clearTime();
            this.clockData.intervalID = setInterval(this.updateTime.bind(this), this.clockData.intervalTime);
        }
    }, {
        key: "clearTime",
        value: function clearTime() {
            if (this.clockData.intervalID) {
                clearInterval(this.clockData.intervalID);
                this.clockData.intervalID = null;
            }
        }
    }, {
        key: "updateTime",
        value: function updateTime() {
            this.clockData.currentTime += this.clockData.intervalTime;
            var distance = this.clockData.currentTime - this.getTime();
            if (!this.clockData.initFlag) {
                return;
            }

            if (Math.abs(distance) > 1000) {
                //시간이 안맞으면(중간에 사용자가 시간을 변경하거나 부하로 인한)
                this.clockData.initFlag = false;
                this.clearTime();
                this.loadServerTime();
            } else {
                ///시간 변동시 tirgger
                var currentDate = this.getDate();
                if (currentDate.getDate() != this.clockData.currentDate) {
                    //날짜 변동시
                    this.clockData.currentDate = currentDate.getDate();
                }

                this.display(currentDate);
            }
        }
    }, {
        key: "getDigitNum",
        value: function getDigitNum(no) {
            no = parseInt(no);
            if (no < 10) {
                no = "0" + no;
            }
            return no;
        }
    }, {
        key: "display",
        value: function display(date) {
            var newTime = date.toString();
            this.$txt.html(DateFormat.format.date(date, this.dateFormat));
            this._oldTime = newTime;
        }
    }, {
        key: "dateFormat",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "dateFormat", value)) {
                this.invalidateDateFormat = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "dateFormat");
        }
    }, {
        key: "font",
        get: function get() {
            return this.getGroupProperties("font");
        }
    }]);

    return DigitalClockComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DigitalClockComponent, {
    "info": {
        "componentName": "Digital Clock Component",
        "version": "1.0.0"
    },

    "setter": {
        "width": 214,
        "height": 32,
        "dateFormat": "yyyy-MM-dd HH:mm:ss"
    },

    "label": {
        "label_using": "N",
        "label_text": "Frame Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 20,
        "font_weight": "normal"
    },

    "style": {
        "display": "flex",
        "border": "1px none #000000",
        "backgroundColor": "rgba(255, 255, 255, 0)",
        "borderRadius": 0
    }
});

DigitalClockComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    template: "font"
}, {
    label: "Date Format",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "dateFormat",
        type: "string",
        label: "format",
        show: true,
        writable: true,
        description: "date format 설정"
    }]
}];

WVPropertyManager.add_property_group_info(DigitalClockComponent, {
    label: "DigitalClockComponent 고유 속성",
    children: [{
        name: "dateFormat",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'yyyy-MM-dd HH:mm:ss'",
        description: "Date 정보를 표기하기 위한 format(확장팝업 참조)입니다.\n ex) 'yyyy-MM-dd HH:mm:ss' "
    }]
});

//  추후 추가 예정
WVPropertyManager.add_method_info(DigitalClockComponent, {
    name: "getDate",
    description: "현재 표기되고 있는 Date정보를 반환합니다. type은 Date객체입니다."
});