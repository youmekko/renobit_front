class GroupButtonComponent extends WVDOMComponent {
    constructor() {
        super();
        this._invalidatePropertyTabs = false;
        this._invalidatePropertyActive = false;
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateElement() {
        var self = this;
        var str =
            '<div class="renobit-btn-group">' +
            '<div class="item" v-for="(item, index) in compSetter.items"><span>{{getTranslateItemName(item)}}</span></div>' +
            '</div>';

        let temp = $(str);
        $(this._element).append(temp);

        var self = this;
        var app = new Vue({
            el: temp.get(0),
            data: function() {
                return {
                    compSetter: self.getGroupProperties("setter")
                }
            },

            methods: {
                getTranslateItemName(str) {
                    return wemb.localeManager.translatePrefixStr(str);
                }
            }

        })
    }

    bindEvent() {
        if (this.isEditorMode) { return; }
        var self = this;
        $(this._element).on("click", ".renobit-btn-group .item", function(e) {
            e.preventDefault();
            var index = $(self._element).find(".item").index($(this));
            self.dispatchWScriptEvent("select", {
                index: index
            })
        })
    }

    _onDestroy() {
        $(this._element).off("click");
        super._onDestroy();
    }

    getExtensionProperties() {
        return true;
    }

    _onImmediateUpdateDisplay() {
        this.updateComponentStyle();
        this.bindEvent();
        this._validateFontStyleProperty();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this._updateStyle);
        }

        if (this._invalidatePropertyActive) {
            this.validateCallLater(this.changeActiveIndex);
            this._invalidatePropertyActive = false;
        }

        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty);
        }
    }

    _validateFontStyleProperty() {
        var fontProps = this.font;
        $(this._element).css("fontFamily", fontProps.font_type);
    }


    _updateStyle() {
        this.updateComponentStyle();
    }

    updateComponentStyle() {
        var $el = $(this._element);
        $el.find("[style-id='" + this.id + "']").remove();

        var style = this.getGroupPropertyValue("extension", "style");
        var trimStr = style.trim();
        if (!trimStr) { return ''; }

        var selectorStr = "[id='" + this.id + "'] ";
        trimStr = trimStr.replace(/}/gi, "} " + selectorStr);
        trimStr = selectorStr + trimStr;

        var lastIndex = trimStr.lastIndexOf(selectorStr);
        trimStr = trimStr.substr(0, lastIndex);

        var tagStr = "<style style-id='" + this.id + "'>" + trimStr + '</style>';
        $el.append(tagStr);
    }



    set items(items) {
        if (this._checkUpdateGroupPropertyValue("setter", "items", items)) {
            this._invalidatePropertyTabs = true;
        }
    }

    get items() {
        return this.getGroupPropertyValue("setter", "items");
    }

    get font() {
        return this.getGroupProperties("font");
    }

}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(GroupButtonComponent, {
    "info": {
        "componentName": "GroupButtonComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 400,
        "height": 40,
        "items": ["item01"]
    },

    "label": {
        "label_using": "N",
        "label_text": "Group Button"
    },

    "font": {
        "font_type": "inherit"
    },

    "extension": {
        "style": "div.renobit-btn-group{\n    display: flex; \n    width:100%; \n    height: 100%;\n    font-family: inherit !important;\n}\n\ndiv.renobit-btn-group *{\n    font-family: inherit !important;\n}\n\ndiv.renobit-btn-group .item{\n    flex:1;\n    color: #fff;\n    background-color: #7ebeff;\n    border-color: #409eff;\n    border-radius: 5px;\n    cursor:pointer;\n    overflow: hidden;\n    padding:5px 10px;\n}\n\ndiv.renobit-btn-group .item:not(:last-child){\n    margin-right: 4px;\n}\n\ndiv.renobit-btn-group .item:hover{\n    background-color: #409eff;\n}\n\ndiv.renobit-btn-group .item>span{\n    display:flex; \n    align-items: center; \n    justify-content: center;\n    height: 100%;    \n}"
    },

    "style": {
        "backgroundColor": "rgba(255, 255, 255, 0)",
        "border": "1px none #000000",
        "borderRadius": "0",
        "cursor": "default"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
GroupButtonComponent.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    },
    {
        template: "label",
        label: "label"
    }, {
        template: "background",
        label: "background"
    }, {
        template: "border",
        label: "border"
    }, {
        template: "font-type",
        label: "font-type"
    }
];


// 이벤트 정보
WVPropertyManager.remove_event(GroupButtonComponent, "dblclick");
WVPropertyManager.remove_event(GroupButtonComponent, "click");

// 이벤트 정보
WVPropertyManager.add_event(GroupButtonComponent, {
    name: "select",
    label: "버튼 클릭 이벤트",
    description: "버튼 클릭 이벤트 입니다.",
    properties: [{
        name: "index",
        type: "Number",
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }]
});


WVPropertyManager.add_property_group_info(GroupButtonComponent, {
    label: "GroupButtonComponent 고유 속성",
    children: [{
        name: "items",
        type: "Array<string>",
        show: true,
        writable: true,
        defaultValue: "['button name1', 'button name2', 'button name3']",
        description: "버튼 이름 리스트\nex) this.item = ['button name1', 'button name2', 'button name3']"
    }]
});