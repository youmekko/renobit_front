class TemplateComponent extends WVDOMComponent {
    constructor() {
        super();
        this.$el;
        this.$wrap;
        this.targetSelector;
        this._dataProvider = null;
        this.isCreated = false;
        this.timer;
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateElement() {
        this.$el = $(this._element);
        this.$el.attr("data-id", this.id).addClass("template-comp");
        this.$el.append('<div class="tmpl-wrap"></div>');
        this.$wrap = this.$el.find(".tmpl-wrap");
        this.targetSelector = "[data-id='" + this.id + "'] .tmpl-wrap";

        this.createTransform();

        ///////////////////////////////////////////////////////////////////////

        if (this.isEditorMode) {
            this.setNodateStyle();
        }
        this.isCreated = true;
    }

    _onImmediateUpdateDisplay() {
        this.render();
    }

    _onDestroy() {
        clearTimeout(this.timer);
        this.$el = null;
        this.$wrap.remove();
        this.$wrap = null;
        this._dataProvider = null;
        super._onDestroy();
    }

    dispatchRegisterEvent() {
        super.dispatchRegisterEvent();
        if (this.isEditorMode) {
            this.render();
        }
    }

    setNodateStyle() {
        if (this.isEditorMode) {
            this.$el.addClass("no-data");
            this.$wrap.html("<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAOCAYAAABpcp9aAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MEZGNTdDREUyNTVEMTFFODg5QjlDQTk0Q0E0RTE2RjIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MEZGNTdDREYyNTVEMTFFODg5QjlDQTk0Q0E0RTE2RjIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowRkY1N0NEQzI1NUQxMUU4ODlCOUNBOTRDQTRFMTZGMiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowRkY1N0NERDI1NUQxMUU4ODlCOUNBOTRDQTRFMTZGMiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpTnVn0AAAH2SURBVHjaxFbLUcMwELU9uRMqQBw4x1RAaACcCnAHmBNHnCMn3AGmAw8NYCqIOXOIU0FMBWHfzHNmR2MlYJLwZnYUSav1vv1I8VerlfdTPL5+ZjI091dnKefeHhFzzF0K4ocX9DBae4dBrEg4ETgibUQia20sw5FIyTlSlx6ITCIy3EpAnBqyTOYioaULQh+StppkACMypnGjzhhKiyH19Fqo9EPuuxAx84mTgDjVlgfGu7bOFcZt9AVvHG/4O+S5QqRiAObUx9qSenM6A2Tcxzdn3G86AtcSgP4T7a/JBoimCAw9s2GMOJ/ZJSXDiM6geXxuTUV8ReyEZXWKbIlc8IOYH4ssFAGPDse0cc61rrJslN2GZAv4NeAGPnQpjpWOFILx14Z9jUKdWaooexx1GdWKfMUA3m6wXdMufH7AmYB19Q5WwihntLtSWP6y8ZoezbrtTEg/4PwLCAcS1UoErCZkVwmJFA3tqP//gGF2Zpyj3GJcKIF6FAoRw/QkbccLkZDXZ+GIyC4RsXds5AziOsjOd4ANbJTDMLoAW0sVH7pmZuKeDl+wrjM6NXI0cWr5tMbA8UQ3imXkiH6sbpTK8UJPrfW8Iwi5KtFElarWdZavv+m/EPsAN8kEJdax/5dyKVV/9QL+Cw22KDS8o/eBahdGvgUYAH0jt3P7TWSdAAAAAElFTkSuQmCC'/>");
        }
    }

    render() {
        var data = this.getTemplateData();
        clearTimeout(this.timer);

        var self = this;
        this.timer = setTimeout(function() {
            self.$wrap.empty();
            if (!self.extension.tmpl) {
                if (self.isEditorMode) {
                    self.setNodateStyle();
                }
                return;
            }

            self.$el.removeClass("no-data");
            try {
                $.tmpl(self.extension.tmpl, data).appendTo(self.targetSelector);
            } catch (error) {
                console.warn("template error", error);
            }

            var style = self.getInstanceStyle();
            self.$wrap.append('<style>' + style + '</style>');
        }, 5);

    }

    getTemplateData() {
        var data = { data: this.dataProvider };
        if (!this.dataProvider) {
            data = { data: {} };
        }

        return data;
    }

    getInstanceStyle() {
        //let trimStr = this.styleStr.replace(/\s/gi, "");
        var trimStr = this.extension.tmpl_style.trim();
        if (!trimStr) {
            return '';
        }

        var prefix = this.targetSelector + " ";
        trimStr = trimStr.replace(/}/gi, "} " + prefix);
        trimStr = prefix + trimStr;

        var lastIndex = trimStr.lastIndexOf(prefix);
        trimStr = trimStr.substr(0, lastIndex);

        return trimStr;
    }

    getTestData() {
        var trimDataStr = this.extension.tmpl_test_data.trim();

        if (!trimDataStr) {
            trimDataStr = "{}";
        }

        var str = '{"data" :' + trimDataStr + "}";
        var json = JSON.parse(str);
        return json.data;
    }

    show() {
        this.$el.show();
    }

    hide() {
        this.$el.hide();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension") && this.isCreated) {
            this.validateCallLater(this._validateSetterProperty);
        }
    }

    _validateSetterProperty() {
        this.render();
    }



    set dataProvider(data) {
        //if (this._sortData.length) { return; }
        var oldValue = this.dataProvider;
        this._dataProvider = data;

        if (!this.isEditorMode && data != oldValue) {
            this.dispatchWScriptEvent("change", {
                value: data
            })
        }
    }

    get dataProvider() {
        if (this.isEditorMode) {
            return this.getTestData();
        }

        return this._dataProvider;
    }

    get extension() {
        return this.getGroupProperties("extension");
    }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(TemplateComponent, {
    "info": {
        "componentName": "TemplateComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 100,
        "height": 100,
    },

    "label": {
        "label_using": "N",
        "label_text": "Template Component"
    },
    "extension": {
        "tmpl": "",
        "tmpl_style": "",
        "tmpl_test_data": ""
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
TemplateComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        template: "label"
    }
];


// 이벤트 정보
WVPropertyManager.add_event(TemplateComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(TemplateComponent, {
    label: "TemplateComponent 고유 속성",
    children: [{
        name: "dataProvider",
        type: "any",
        show: true,
        writable: true,
        description: "template data입니다."
    }]
});

//  추후 추가 예정
WVPropertyManager.add_method_info(TemplateComponent, {
    name: "render",
    description: "저장된 dataProvider를 작성된 template포맷에 맞추어 화면에 출력합니다."
});