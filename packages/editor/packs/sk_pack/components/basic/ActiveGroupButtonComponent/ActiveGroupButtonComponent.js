class ActiveGroupButtonComponent extends WVDOMComponent {
    constructor() {
        super();
        this._invalidatePropertyTabs = false;
        this._invalidatePropertyActive = false;
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateElement() {
        var self = this;
        var str = '<ul class="renobit-active-group">' +
            '<template v-for="(item, index) in compSetter.items" >' +
            '<li  :class="{active: compSetter.active==index }" ><a class="item" nohref>{{getTranslateItemName(item)}}</a></li>' +
            '</template>' +
            '</ul>';

        let temp = $(str);
        $(this._element).append(temp);

        var self = this;
        var app = new Vue({
            el: temp.get(0),
            data: function() {
                return {
                    compSetter: self.getGroupProperties("setter")
                }
            },

            methods: {
                getTranslateItemName(str) {
                    return wemb.localeManager.translatePrefixStr(str);
                }
            }
        })
    }

    bindEvent() {
        if (this.isEditorMode) { return; }
        var self = this;
        $(this._element).on("click", ".renobit-active-group a", function(e) {
            e.preventDefault();
            var index = $(self._element).find("li").index($(this).parent());

            self.dispatchWScriptEvent("select", {
                index: index
            })

            self.active = index;
        })
    }

    _onDestroy() {
        $(this._element).off("click");
        super._onDestroy();
    }

    getExtensionProperties() {
        return true;
    }

    _onImmediateUpdateDisplay() {
        this.updateComponentStyle();
        this.bindEvent();
        this._validateFontStyleProperty();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this._updateStyle);
        }

        if (this._invalidatePropertyActive) {
            this.validateCallLater(this.changeActiveIndex);
            this._invalidatePropertyActive = false;
        }

        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty);
        }
    }

    _validateFontStyleProperty() {
        var fontProps = this.font;
        $(this._element).css("fontFamily", fontProps.font_type);
    }

    changeActiveIndex() {
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                index: this.active
            })
        }
    }

    _updateStyle() {
        this.updateComponentStyle();
    }

    updateComponentStyle() {
        var $el = $(this._element);
        $el.find("[style-id='" + this.id + "']").remove();

        var style = this.getGroupPropertyValue("extension", "style");
        var trimStr = style.trim();
        if (!trimStr) { return ''; }

        var selectorStr = "[id='" + this.id + "'] ";
        trimStr = trimStr.replace(/}/gi, "} " + selectorStr);
        trimStr = selectorStr + trimStr;

        var lastIndex = trimStr.lastIndexOf(selectorStr);
        trimStr = trimStr.substr(0, lastIndex);

        var tagStr = "<style style-id='" + this.id + "'>" + trimStr + '</style>';
        $el.append(tagStr);
    }


    set active(index) {
        if (this._checkUpdateGroupPropertyValue("setter", "active", index)) {
            this._invalidatePropertyActive = true;
        }
    }

    get active() {
        return this.getGroupPropertyValue("setter", "active");
    }

    set items(items) {
        if (this._checkUpdateGroupPropertyValue("setter", "items", items)) {
            this._invalidatePropertyTabs = true;
        }
    }

    get items() {
        return this.getGroupPropertyValue("setter", "items");
    }

    get font() {
        return this.getGroupProperties("font");
    }

}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(ActiveGroupButtonComponent, {
    "info": {
        "componentName": "ActiveGroupButtonComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 400,
        "height": 40,
        "items": ["item01"],
        "active": 0
    },

    "label": {
        "label_using": "N",
        "label_text": "Active Group Button"
    },

    "font": {
        "font_type": "inherit"
    },

    "extension": {
        "style": "ul.renobit-active-group {\n    display: flex; \n    width:100%; \n    height: 100%;\n    font-family: inherit !important; \n}\n\nul.renobit-active-group *{\n    font-family: inherit !important;\n}\n\nul.renobit-active-group li{\n    flex:1; \n    border: 1px solid #e4e7ed; \n    border-bottom: 3px solid #e4e7ed; \n    border-top-left-radius: 5px; \n    border-top-right-radius: 5px;\n    overflow: hidden;\n}\n\nul.renobit-active-group li.active{\n    border-bottom: none;\n}\n\nul.renobit-active-group li:hover{\n    border-bottom: none;\n}\n\nul.renobit-active-group li>a{\n    display:flex; \n    align-items: center; \n    justify-content: center; \n    padding:5px 10px;\n    text-decoration: none; \n    color: #333; \n    height: 100%; \n    font-size: 14px; \n    background-color: #fff;\n    cursor:pointer;\n}\n\nul.renobit-active-group li:not(:last-child){\n    border-right:none;\n}\n\nul.renobit-active-group li.active a{\n    border-bottom: 3px solid #409eff;\n}\n\nul.renobit-active-group li:not(.active):hover a{\n    border-bottom: 3px solid rgba(64,158,255,.5);\n}"
    },

    "style": {
        "backgroundColor": "rgba(255, 255, 255, 0)",
        "border": "1px none #000000",
        "borderRadius": "0",
        "cursor": "default"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
ActiveGroupButtonComponent.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    },
    {
        template: "label",
        label: "label"
    }, {
        template: "background",
        label: "background"
    }, {
        template: "border",
        label: "border"
    }, {
        template: "font-type",
        label: "font-type"
    }
];


//  추후 추가 예정
ActiveGroupButtonComponent.method_info = [];
WVPropertyManager.remove_event(ActiveGroupButtonComponent, "dblclick");
WVPropertyManager.remove_event(ActiveGroupButtonComponent, "click");
// 이벤트 정보
WVPropertyManager.add_event(ActiveGroupButtonComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "index",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

// 이벤트 정보
WVPropertyManager.add_event(ActiveGroupButtonComponent, {
    name: "select",
    label: "버튼 클릭 이벤트",
    description: "버튼 클릭 이벤트 입니다.",
    properties: [{
        name: "index",
        type: "Number",
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }]
});

WVPropertyManager.add_property_group_info(ActiveGroupButtonComponent, {
    label: "ActiveGroupButtonComponent 고유 속성",
    children: [{
        name: "items",
        type: "Array<string>",
        show: true,
        writable: true,
        defaultValue: "['button name1', 'button name2', 'button name3']",
        description: "버튼 이름 리스트\nex) this.item = ['button name1', 'button name2', 'button name3']"
    }, {
        name: "active",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: 0,
        description: "선택할 버튼 index"
    }]
});