class FrameComponent extends WVDOMComponent {

    constructor() {
        super();
    }

    onLoadPage() {}

    _onImmediateUpdateDisplay() {
        this.updateBackground();
    }

    _onCreateProperties() {
        /*이전버전 호환처리*/
        if (this.getGroupPropertyValue("background", "type") == "") {
            this.setGroupPropertyValue("background", "type", "solid");
            let color = this.getGroupPropertyValue("style", "backgroundColor");
            if (color) {
                this.setGroupPropertyValue("background", "color1", color);
            }
        }
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("background")) {
            this.validateCallLater(this.updateBackground);
        }
    }

    updateBackground() {
        let bgData = this.getGroupProperties("background");
        let style = this._styleManager.getBackgroundStyle(bgData, `[id='${this.id}']`);
        $(this._element).find("style").remove();
        $(this._element).append(style);
    }
}

WVPropertyManager.attach_default_component_infos(FrameComponent, {
    "info": {
        "componentName": "FrameComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100
    },

    "label": {
        "label_using": "N",
        "label_text": "Frame Component"
    },

    "background": {
        "type": "",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#eee",
        "color2": "#000",
        "text": ''
    },

    "style": {
        "border": "1px solid #000000",
        "backgroundColor": "#eee",
        "borderRadius": 1,
        "cursor": "default"
    }
});

FrameComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    template: "background-gradient",
    owner: "background",
    label: "Background"
}, {
    template: "border"
}];