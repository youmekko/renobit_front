class EventBrowserComponent extends WVDOMComponent {

    constructor() {
        super();
    }

    // 확장 팝업 사용 여부
    getExtensionProperties() {
        return true;
    }

    _onCreateProperties() {
        // header
        this.header = [
            { key: "severity", label: "등급" },
            { key: "host", label: "호스트명" },
            { key: "type", label: "이벤트타입" },
            { key: "stime", label: "발생시간" },
            { key: "dupl", label: "중복" },
            { key: "msg", label: "메세지" }
        ];

        this.foldedHeight = 35;
        this.ackList = [];
        this.activeSeverity = [];
        this._sortInfo = { key: "", state: "" };

        // slide String 값
        this.upStr = "▲";
        this.downStr = "▼";

        // 에디터에서 보여질 preview DataProvider
        this.previewData = [{
                "type": "이벤트",
                "msg": "샘플 데이터 입니다.",
                "host": "test1",
                "severity": "major",
                "dupl": "N",
                "id": 1,
                "stime": "09:05:02"
            },
            { "type": "이벤트", "msg": "샘플 데이터 입니다.", "host": "test2", "severity": "critical", "dupl": "N", "id": 2, "stime": "09:05:03" },
            { "type": "이벤트", "msg": "샘플 데이터 입니다.", "host": "test3", "severity": "minor", "dupl": "N", "id": 3, "stime": "09:05:04" },
            { "type": "이벤트", "msg": "샘플 데이터 입니다.", "host": "test4", "severity": "critical", "dupl": "N", "id": 4, "stime": "09:05:05" }
        ]

    }

    _onDestroy() {
        if (this._dataProviderWorker) {
            this._dataProviderWorker.clear();
            this._dataProviderWorker = null;
        }

        if (this._countWorker) {
            this._countWorker.clear();
            this._countWorker = null;
        }

        super._onDestroy();
    }

    _onCreateElement() {
        $(this._element).addClass("event-browser");

        var $container = $("<div class='container'></div>");
        this.$toolArea = $("<div class='tool-area'></div>");
        this.$contentsArea = $("<div class='contents-area'></div>");

        $container.append(this.$toolArea, this.$contentsArea);
        $(this._element).append($container);

        this._setToolArea();
        this._setHeaderArea();
        this._setStyle();
    }

    _onImmediateUpdateDisplay() {
        if (this.isEditorMode) {
            this._setDataProvider();
            this._setSeverityCount();
        } else {
            // 데이터셋 호출 (dataProvider, Count)
            this._callDataset();
        }
    }

    _setDatasetParam(paramInfo) {
        var obj = {};
        if (paramInfo) {
            paramInfo.map((value, index) => {
                if (value.isDirect == 0) {
                    obj[value.param_name] = value.default_value;
                } else if (value.isDirect == 1) {
                    obj[value.param_name] = (this.activeSeverity.indexOf(value.isSeverityValue) > -1) ? 'Y' : 'N';
                } else if (value.isDirect == -1) {
                    obj[value.param_name] = value.value
                }
            })
        }

        return obj;
    }

    _callDataset() {
        var config = this.getGroupPropertyValue("extension", "configInfo");
        if (!config) {
            this._setNoData();
            return;
        } else {
            this.executeDataProvider(config.dpInfo);
            this.executeCount(config.countInfo);
        }
    }

    /**
     * Toolbar 영역 엘리먼트 생성 및 이벤트 바인딩
     *
     *  ( title, auto버튼, slide 버튼, ack버튼, severity 버튼)
     * @private
     */
    _setToolArea() {
        var $titleArea = $("<div style='min-width: 400px;'></div>");
        var $title = $("<span class='title'>" + this.title + "</span>");
        var autoTitle = $("<span style='padding-left: 4px; padding-right: 3px;'>AUTO</span>");
        var $btnAuto = $("<input class='auto-skewed' id=" + this.name + " type='checkbox'><label class='btn-auto' data-off='OFF' data-on='ON' for=" + this.name + "></label>");
        var $btnSlide = $("<div class='btn btn-slide' mode='up'>" + this.downStr + "</div>");
        var $btnAck = $("<div class='btn btn-ack'>ACK</div>");

        $titleArea.append($title, autoTitle, $btnAuto, $btnSlide);

        var $severityArea = $("<div></div>");
        var severityEle = '';
        for (var i = 0; i < this.severity.length; i++) {
            severityEle += '<div class="severity" data-severity="' + this.severity[i] + '">' +
                '   <div class="bullet ' + this.severity[i] + '"></div>' +
                '   <span>-</span>' +
                '</div>';
        }

        $severityArea.append($btnAck, severityEle);
        this.$toolArea.append($titleArea, $severityArea);

        if (!this.isEditorMode) {
            this._btnBindEvent();

            if (this.getGroupPropertyValue("extension", "isFolded")) {
                this._btnSlideClick($btnSlide);
            }
            if (this.getGroupPropertyValue("extension", "isAuto")) {
                $btnAuto.trigger("click");
            }
        }
    }

    /**
     * 이벤트 브라우저 헤더 영역 엘리먼트 생성
     * @private
     */
    _setHeaderArea() {
        var $header = $("<div class='header'></div>");
        var self = this;

        for (var i = 0; i < this.header.length; i++) {
            var $headerItem = $("<div>" + this.header[i].label + "<i></i></div>");
            $headerItem.attr("data-key", this.header[i].key);
            $header.append($headerItem);

            if (!this.isEditorMode) {
                $headerItem.click(function(event) {
                    self._headerClick(event);
                });
            }
        }

        this.$events = $("<div class='event-area'></div>");
        this.$events.addClass("scrollbar-inner");
        this.$contentsArea.append($header, this.$events);
    }

    /**
     * dataProvider 적용
     * @private
     */
    _setDataProvider() {
        this.$events.find("div").remove();
        /*[ 데이터가 없을 경우 no data 표시 ]*/
        if (this.dataProvider.length < 1 && !this.isEditorMode) {
            this._setNoData();
            return;
        }

        /*[ auto 모드일 경우 이벤트 브라우저 패널 up ]*/
        if (this.isAutoMode) {
            var $btnSlide = this.$toolArea.find(".btn-slide");
            $btnSlide.attr("mode", "down");
            this._btnSlideClick($btnSlide);
        }

        /*[ severity 별로 데이터 filter ]*/
        var self = this;
        var provider = this.isEditorMode ? this.previewData : this.sort(this._sortInfo.key, this._sortInfo.state);
        provider.map(function(value, index) {
            if (index >= self.getGroupPropertyValue("extension", "maxRowCount")) {
                return;
            }

            var $contents = $("<div class='contents'></div>");
            $contents.attr({ "data-seq": index, "data-id": value.id });

            if (self.ackList && self.ackList.indexOf(value.id + "") >= 0) {
                $contents.addClass("active");
            }

            for (var i = 0; i < self.header.length; i++) {
                var event = $('<div>' + value[self.header[i].key] + '</div>');
                event.attr(self.header[i].key, value[self.header[i].key]);
                $contents.append(event);
            }

            if (!self.isEditorMode) {
                $contents.click(function(event) {
                    self._contentsClick(event);
                });

                $contents.dblclick(function(event) {
                    self._contentsDoubleClick(event);
                });
            }

            self.$events.append($contents);
        });

        this.$events.scrollbar();
    }

    /**
     * severity 카운트 적용 메소드
     * @private
     */
    _setSeverityCount() {
        var self = this;
        this.severity.map(function(value) {
            var severityValue = self.count[value];
            self.$toolArea.find(".severity[data-severity=" + value + "] > span").text(severityValue);
        });
    }

    /**
     * 데이터 없을경우 메세지 표시 메소드
     * @private
     */
    _setNoData() {
        this.$events.find("div").remove();
        this.$events.append($("<div class='no-data'>No data.</div>"));
    }

    _setStyle() {
        this._validateHeaderStyle();
        this._validateContentStyle();
    }

    /**
     * 버튼 클릭 이벤트 바인드 메소드
     * (auto버튼, slide 버튼, ack버튼, severity 버튼)
     * @private
     */
    _btnBindEvent() {
        var $btnSlide = this.$toolArea.find(".btn-slide");
        var $btnAck = this.$toolArea.find(".btn-ack");
        var $btnAuto = this.$toolArea.find(".btn-auto");
        var $severity = this.$toolArea.find(".severity");
        var self = this;

        $btnSlide.click(function(event) {
            self._btnSlideClick($btnSlide);
        });

        $btnAck.click(function(event) {
            self._btnAckClick();
        });

        $btnAuto.click(function(event) {
            self._btnAutoClick(event);
        });

        $severity.click(function(event) {
            self._btnSeverityClick(event);
        });
    }

    _btnSlideClick(target) {
        if (target.attr("mode") == 'up') {
            $(this._element).animate({ "height": this.foldedHeight, "top": this.y + this.height - this.foldedHeight });
            target.text(this.upStr);
            target.attr("mode", "down");
        } else {
            $(this._element).animate({ "height": this.height, "top": this.y });
            target.text(this.downStr);
            target.attr("mode", "up");
        }
    }

    _btnAckClick() {
        //=========================================
        /*[ ack 처리 이벤트 ]*/
        //=========================================
        this.dispatchWScriptEvent("eventAck", {
            ackList: this.ackList.concat()
        });

        if (this._dataProviderWorker) {
            this._dataProviderWorker.pause();
        }

        if (this._countWorker) {
            this._countWorker.pause();
        }
    }

    _btnSeverityClick(event) {
        var $severity = $(event.currentTarget);
        var value = $(event.currentTarget).attr("data-severity");

        if ($severity.hasClass("active")) {
            $severity.removeClass("active");
            this.activeSeverity.splice(this.activeSeverity.indexOf(value), 1);
        } else {
            $severity.addClass("active");
            this.activeSeverity.push(value);
        }

        //==========================================
        //  dataset Call
        //==========================================
        if (this._dataProviderWorker && this._dataProviderWorker.isActive) {
            var config = this.getGroupPropertyValue("extension", "configInfo");

            if (!config) {
                this._setNoData();
                return;
            } else {
                var info = this.activeSeverity.length == 0 ? config.dpInfo : config.severityInfo;
                this.executeDataProvider(info);
            }
        }
    }

    _btnAutoClick(event) {
        this.isAutoMode = !this.isAutoMode;
    }

    sort(key, state) {
        let data = this.dataProvider.concat();
        if (state != "") {
            data.sort(function(a, b) {
                var aValue = a[key].toUpperCase();
                var bValue = b[key].toUpperCase();
                return aValue < bValue ? -1 : aValue > bValue ? 1 : 0;
            });

            if (state == "descending") {
                data.reverse();
            }
        }

        this._sortInfo = { key: key, state: state };

        return data;
    }

    _headerClick(event) {
        var $target = $(event.currentTarget);
        let key = $target.attr("data-key");

        let state = "";
        if ($target.find("i").hasClass("ascending")) {
            state = "descending";
        } else if ($target.find("i").hasClass("descending")) {
            state = "";
        } else {
            state = "ascending";
        }

        $target.parent().find("i").removeClass("ascending descending");
        $target.find("i").addClass(state);

        this.dataProvider = this.sort(key, state);
    }

    /**
     * 컨텐츠 클릭 메소드
     *
     * ack 처리를 위해 선택 리스트 저장
     * @param event
     * @private
     */
    _contentsClick(event) {
        event.preventDefault();
        event.stopPropagation();

        var $target = $(event.currentTarget);
        var event_id = $target.attr("data-id");

        if ($target.hasClass("active")) {
            $target.removeClass("active");
            this.ackList.splice(this.ackList.indexOf(event_id), 1);

        } else {
            $target.addClass("active");
            this.ackList.push(event_id);
        }
    }

    /**
     * 컨텐츠 더블 클릭 메소드
     *
     * itemDoubleClick 으로 dispatch
     *
     * @param event
     * @private
     */
    _contentsDoubleClick(event) {
        var $target = $(event.currentTarget);
        var sequence = $target.attr("data-seq");
        var item = this.dataProvider[sequence];

        //=========================================
        /*[ item 더블 클릭 이벤트 ]*/
        //=========================================
        this.dispatchWScriptEvent("itemDoubleClick", {
            item: item
        })
    }

    _onCommitProperties() {
        if (this._invalidateDataProvider) {
            this.validateCallLater(this._validateDataProvider);
            this._invalidateDataProvider = false;
        }

        if (this._invalidateCount) {
            this.validateCallLater(this._validateCount);
            this._invalidateCount = false;
        }

        if (this._invalidateTitle) {
            this.validateCallLater(this._validateTitle);
            this._invalidateTitle = false;
        }

        if (this._updatePropertiesMap.has("headerStyle")) {
            this.validateCallLater(this._validateHeaderStyle);
        }

        if (this._updatePropertiesMap.has("contentStyle")) {
            this.validateCallLater(this._validateContentStyle);
        }
    }

    _validateDataProvider() {
        this._setDataProvider();
    }

    _validateCount() {
        this._setSeverityCount();
    }

    /**
     * 타이틀 적용 메소드
     * @private
     */
    _validateTitle() {
        var titleEle = this.$toolArea.find(".title");
        $(titleEle).text(this.title);
    }

    /**
     * 이벤트 브라우저 헤더 스타일 적용 메소드
     * @private
     */
    _validateHeaderStyle() {
        var headerStyle = this.getGroupProperties("headerStyle");

        this.$contentsArea.find(".header").css({
            "background-color": headerStyle.bgColor,
            "color": headerStyle.color,
            "fontSize": headerStyle.fontSize
        });
    }

    /**
     * 이벤트 브라우저 컨텐츠 스타일 적용 메소드
     * @private
     */
    _validateContentStyle() {
        var contentStyle = this.getGroupProperties("contentStyle");

        var style = $('<style>' +
            '[id="' + this.id + '"] .event-area > .contents { ' +
            '       background-color: ' + contentStyle.bgColor + '; ' +
            '       color: ' + contentStyle.color + '; ' +
            '       font-size: ' + contentStyle.fontSize + 'px;} ' +
            '[id="' + this.id + '"] .event-area > .contents:hover { ' +
            '       background-color: ' + contentStyle.hoverBg + '; ' +
            '       color: ' + contentStyle.hoverFont + '} ' +
            '[id="' + this.id + '"] .event-area > .contents.active { ' +
            '       background-color: ' + contentStyle.activeBg + '; ' +
            '       color: ' + contentStyle.activeFont + '} ' +
            '</style>');

        this.$contentsArea.append(style);
    }

    /**
     * dataProvider 적용을 위한 데이터셋 콜 메소드
     * @param info
     */
    executeDataProvider(info) {
        if (!info || !info.datasetId) {
            return;
        }

        // 현재 실행중인 dataSet 끄기
        var self = this;
        if (this._dataProviderWorker) {
            this._dataProviderWorker.clear();
        }

        this._dataProviderWorker = this.page.dataService.callById(info.datasetId, this._setDatasetParam(info.param));
        if (this._dataProviderWorker && this._dataProviderWorker.item) {
            this._dataProviderWorker.on("error", function(event) {
                console.warn("dataset call error", event);
            });

            this._dataProviderWorker.on("success", function(event) {
                self.dataProvider = event.rowData;
            });
        } else {
            console.warn("dataset is not defined component id : ", this.id);
        }
    }

    /**
     * count 적용을 위한 데이터셋 콜 메소드
     * @param info
     */
    executeCount(info) {
        if (!info || !info.datasetId) {
            return;
        }

        var self = this;
        if (this._countWorker) {
            this._countWorker.clear();
        }

        this._countWorker = this.page.dataService.callById(info.datasetId, this._setDatasetParam(info.param));
        if (this._countWorker && this._countWorker.item) {
            this._countWorker.on("error", function(event) {
                console.warn("dataset call error", event);
            });

            this._countWorker.on("success", function(event) {
                self.count = event.rowData[0];
            });
        } else {
            console.warn("dataset is not defined component id : ", this.id);
        }
    }


    /**
     * ack 처리후 반드시 실행 시켜주어야 하는 메소드
     *
     *  1. dataProvider, count 데이터셋 start
     *  2. 초기화 처리
     */
    ackComplete() {
        if (this._dataProviderWorker) {
            this._dataProviderWorker.start();
        }

        if (this._countWorker) {
            this._countWorker.start();
        }

        this.$events.find(".active").removeClass("active");
        this.ackList = [];
    }

    set dataProvider(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "dataProvider", value)) {
            this._invalidateDataProvider = true;
        }
    }

    get dataProvider() {
        return this.getGroupPropertyValue("setter", "dataProvider");
    }

    set count(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "count", value)) {
            this._invalidateCount = true;
        }
    }

    get count() {
        return this.getGroupPropertyValue("setter", "count");
    }

    set title(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "title", value)) {
            this._invalidateTitle = true;
        }
    }

    get title() {
        return this.getGroupPropertyValue("setter", "title");
    }

    get severity() {
        return this.getGroupPropertyValue("extension", "severity");
    }

}

WVPropertyManager.attach_default_component_infos(EventBrowserComponent, {
    "info": {
        "componentName": "EventBrowserComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 800,
        "height": 200,
        "dataProvider": "",
        "title": "이벤트 브라우저",
        "count": {}
    },

    "extension": {
        "maxRowCount": 100,
        "isAuto": false,
        "isFolded": false,
        "configInfo": null,
        "severity": ["critical", "major", "warning", "minor", "normal"]
    },

    "label": {
        "label_using": "N",
        "label_text": "EventBrowser Component"
    },

    "style": {
        "backgroundColor": "rgba(34, 38, 41, 0.88)",
        "cursor": "default"
    },

    "headerStyle": {
        "bgColor": "#3b323e",
        "color": "#dad8db",
        "fontSize": 13
    },

    "contentStyle": {
        "fontSize": 12,
        "bgColor": "#2d3033",
        "color": "#dad8db",
        "activeBg": "#645d67",
        "activeFont": "#ffffff",
        "hoverBg": "#424243",
        "hoverFont": "#dad8db"
    }
});

WVPropertyManager.add_event(EventBrowserComponent, {
    name: "eventAck",
    label: "eventAck 이벤트",
    description: "eventAck 이벤트 입니다.",
    properties: []
});
WVPropertyManager.add_event(EventBrowserComponent, {
    name: "itemDoubleClick",
    label: "itemDoubleClick 이벤트",
    description: "itemDoubleClick 이벤트 입니다.",
    properties: []
});


EventBrowserComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    template: "background"
}, {
    label: "config",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "title",
        type: "string",
        label: "title",
        show: true,
        writable: true,
        description: "title"
    }, {
        owner: "extension",
        name: "maxRowCount",
        type: "number",
        label: "maxRowCnt",
        show: true,
        writable: true,
        description: "maxRowCount"
    }, {
        owner: "extension",
        name: "isAuto",
        type: "checkbox",
        label: "isAuto",
        show: true,
        writable: true,
        description: "isAuto"
    }, {
        owner: "extension",
        name: "isFolded",
        type: "checkbox",
        label: "isFolded",
        show: true,
        writable: true,
        description: "isFolded"
    }]
}, {
    label: "Header Style",
    owner: "vertical",
    children: [{
        owner: "headerStyle",
        name: "fontSize",
        type: "number",
        label: "fontSize",
        show: true,
        writable: true,
        description: "fontSize"
    }, {
        owner: "headerStyle",
        name: "bgColor",
        type: "color",
        label: "bgColor",
        show: true,
        writable: true,
        description: "bgColor"
    }, {
        owner: "headerStyle",
        name: "color",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "color"
    }]
}, {
    label: "Content Style",
    owner: "vertical",
    children: [{
        owner: "contentStyle",
        name: "fontSize",
        type: "number",
        label: "fontSize",
        show: true,
        writable: true,
        description: "fontSize"
    }, {
        owner: "contentStyle",
        name: "bgColor",
        type: "color",
        label: "bgColor",
        show: true,
        writable: true,
        description: "bgColor"
    }, {
        owner: "contentStyle",
        name: "color",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "contentStyle",
        name: "activeBg",
        type: "color",
        label: "activeBg",
        show: true,
        writable: true,
        description: "activeBg"
    }, {
        owner: "contentStyle",
        name: "activeFont",
        type: "color",
        label: "activeFont",
        show: true,
        writable: true,
        description: "activeFont"
    }, {
        owner: "contentStyle",
        name: "hoverBg",
        type: "color",
        label: "hoverBg",
        show: true,
        writable: true,
        description: "hoverBg"
    }, {
        owner: "contentStyle",
        name: "hoverFont",
        type: "color",
        label: "hoverFont",
        show: true,
        writable: true,
        description: "hoverFont"
    }]
}];


WVPropertyManager.add_property_group_info(EventBrowserComponent, {
    label: "EventBrowserComponent 고유 속성",
    children: [{
        name: "title",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "이벤트 브라우저",
        description: "이벤트 브라우저 Title값 입니다."
    }, {
        name: "dataProvider",
        type: "array",
        show: true,
        writable: true,
        defaultValue: "[]",
        description: "이벤트 브라우저 데이터값 입니다."
    }, {
        name: "count",
        type: "object",
        show: true,
        writable: true,
        defaultValue: "{}",
        description: "severity 별로 count값을 나타내는 값입니다. (ex : {critical : 10, normal: 20}"
    }]
});



WVPropertyManager.add_method_info(EventBrowserComponent, {
    name: "ackComplete",
    description: "이벤트 브라우저 ack 처리후, 반드시 실행 시켜주어야 하는 메소드입니다."
});