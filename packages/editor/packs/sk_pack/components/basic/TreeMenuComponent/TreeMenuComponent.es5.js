"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TreeMenuComponent = function(_WVDOMComponent) {
    _inherits(TreeMenuComponent, _WVDOMComponent);

    function TreeMenuComponent() {
        _classCallCheck(this, TreeMenuComponent);

        var _this = _possibleConstructorReturn(this, (TreeMenuComponent.__proto__ || Object.getPrototypeOf(TreeMenuComponent)).call(this));

        _this._invalidatePropertyData = false;
        _this._invalidatePropertyActiveColor = false;
        _this._invalidatePropertyActiveNode = false;
        return _this;
    }

    _createClass(TreeMenuComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            // 에디터모드일때만!

            if (this.isEditorMode) {
                this.dataProvider = [{ "id": "root", "text": "root", "parent": "#" }, {
                    "id": "group1",
                    "text": "group1",
                    "parent": "root",
                    "state": {
                        "opened": true,
                        "selected": true
                    }
                }, { "id": "item1", "text": "item1", "parent": "group1" }, { "id": "item2", "text": "item2", "parent": "group1" }];

                var str = '<div class="myTree"></div>';
                var $el = $(this._element);
                var temp = $(str);
                $el.append(temp);

                this.setTreeHTML(this.dataProvider);
                this.updateStyle();
            } else {}
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            _get(TreeMenuComponent.prototype.__proto__ || Object.getPrototypeOf(TreeMenuComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.bindEvent();
            this.updateStyle();
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {
            this.updateStyle();
        }
    }, {
        key: "getMyData",
        value: function getMyData(myId) {
            var needle = myId;

            var found;

            for (var i = 0; i < this.dataProvider.length; i++) {
                if (this.dataProvider[i].id == needle) {
                    found = this.dataProvider[i];
                }
            }
            return found;
        }
    }, {
        key: "bindEvent",
        value: function bindEvent() {
            var single_click_timeout;

            if (this.isEditorMode) {
                return;
            }
            var self = this;

            $(this._element).on("click", ".jstree-node > a", function(e) {
                var my = this;

                e.preventDefault();

                clearTimeout(single_click_timeout);
                single_click_timeout = setTimeout(function() {
                    e.preventDefault();

                    var index = $(self._element).find("li").index($(this).parent());
                    var found = self.getMyData($(my).parent().attr("id"));

                    self.dispatchWScriptEvent("select", {
                        index: index,
                        node: found
                    });

                    self.activeNode = $("#" + $(self).attr('id') + " .myTree").jstree('get_selected', true)[0];

                    $("#" + $(self._element).attr('id') + " .myTree").jstree(true).redraw(true);

                    self.active = index;
                }, 300);
            });

            $(this._element).on("dblclick", ".jstree-node > .jstree-clicked", function(e) {
                var my = this;
                e.preventDefault();
                e.stopPropagation();

                clearTimeout(single_click_timeout);

                var index = $(self._element).find("li").index($(this).parent());
                var found = self.getMyData($(my).parent().attr("id"));

                self.dispatchWScriptEvent("dblclick", {
                    index: index,
                    node: found
                });

                self.active = index;
            });

            $(this._element).on("click", ".jstree-closed > .jstree-icon", function(e) {
                var my = this;
                e.preventDefault();
                var index = $(self._element).find("li").index($(this).parent());
                var found = self.getMyData($(my).parent().attr("id"));

                self.dispatchWScriptEvent("collapse", {
                    index: index,
                    node: found
                });

                self.active = index;
            });

            $(this._element).on("click", ".jstree-open > .jstree-icon", function(e) {
                var my = this;
                e.preventDefault();
                var index = $(self._element).find("li").index($(this).parent());
                var found = self.getMyData($(my).parent().attr("id"));

                self.dispatchWScriptEvent("expand", {
                    index: index,
                    node: found
                });

                self.active = index;
            });
        }
    }, {
        key: "setTreeHTML",
        value: function setTreeHTML(myData) {
            var self = this;

            $(this._element).find('.myTree').on('ready.jstree', function(e, data) {
                self.activeNode = $("#" + $(self._element).attr('id') + " .myTree").jstree('get_selected', true)[0];
                self.updateStyle();
            }).jstree({
                'core': {
                    'data': myData
                },
                "plugins": ["wholerow"]

            });
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._invalidatePropertyData) {
                this.validateCallLater(this._validateData);
                this._invalidatePropertyData = false;
            }
            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this.updateStyle);
            }

            if (this._invalidatePropertyActiveColor) {
                this.validateCallLater(this.updateStyle);
                this._invalidatePropertyActiveColor = false;
            }

            if (this._invalidatePropertyActiveNode) {
                this.validateCallLater(this._validateActiveNode);
                this._invalidatePropertyActiveNode = false;
            }
        }
    }, {
        key: "_validateActiveNode",
        value: function _validateActiveNode() {
            // $("#" + $(this._element).attr('id') + " .myTree").jstree(true).redraw(true);
            // console.log("this.activeNode :"+this.activeNode );
        }
    }, {
        key: "_validateData",
        value: function _validateData() {
            var str = '<div class="myTree"></div>';
            var $el = $(this._element);
            var temp = $(str);
            $el.append(temp);

            this.setTreeHTML(this.dataProvider);
            this.updateStyle();
        }
    }, {
        key: "updateStyle",
        value: function updateStyle() {
            //let bgData = this.getGroupProperties("background");
            //let style = this._styleManager.getBackgroundStyle(bgData, `[id='${this.id}']`);
            var style = "<style>\n\t\t\t\t\t[id='" + this.id + "'] .jstree-default .jstree-anchor .jstree-icon:empty { display: none;}\n\t\t\t\t\t[id='" + this.id + "'] .jstree-node[aria-selected=\"true\"] >.jstree-wholerow,\n\t\t\t\t\t[id='" + this.id + "'] .jstree-wholerow.jstree-wholerow-hovered { background: " + this.activeColor + ";}\n\t\t\t\t\t[id='" + this.id + "'] .jstree-node[aria-selected=\"true\"]>a,\n\t\t\t\t\t[id='" + this.id + "'] .jstree-anchor.jstree-hovered{ color: " + this.activeTextColor + ";}\n\t\t\t\t\t[id='" + this.id + "'] *{ \n\t\t\t\t\t\t\t\t\t\t\t\t\tfont-family: " + this.font.font_type + ";\n\t\t\t\t\t\t\t\t\t\t\t\t\tfont-size: " + this.font.font_size + ";\n\t\t\t\t\t\t\t\t\t\t\t\t\tcolor: " + this.font.font_color + ";\n\t\t\t\t\t\t\t\t\t\t\t\t\tfont-weight: " + this.font.font_weight + ";\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t}\n\t\t\t\t\t</style>";
            $(this._element).find("style").remove();
            $(this._element).append(style);
        }
    }, {
        key: "font",
        get: function get() {
            return this.getGroupProperties("font");
        }
    }, {
        key: "activeColor",
        get: function get() {
            return this.getGroupPropertyValue("setter", "activeColor");
        },
        set: function set(color) {
            if (this._checkUpdateGroupPropertyValue("setter", "activeColor", color)) {
                this._invalidatePropertyActiveColor = true;
            }
        }
    }, {
        key: "activeTextColor",
        get: function get() {
            return this.getGroupPropertyValue("setter", "activeTextColor");
        },
        set: function set(color) {
            if (this._checkUpdateGroupPropertyValue("setter", "activeTextColor", color)) {
                this._invalidatePropertyActiveColor = true;
            }
        }
    }, {
        key: "dataProvider",
        get: function get() {
            return this.getGroupPropertyValue("setter", "dataProvider");
        },
        set: function set(data) {
            if (this._checkUpdateGroupPropertyValue("setter", "dataProvider", data)) {
                this._invalidatePropertyData = true;
            }
        }
    }, {
        key: "activeNode",
        get: function get() {
            return this.getGroupPropertyValue("setter", "activeNode");
        },
        set: function set(data) {
            if (this._checkUpdateGroupPropertyValue("setter", "activeNode", data)) {
                this._invalidatePropertyActiveNode = true;
            }
        }
    }]);

    return TreeMenuComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(TreeMenuComponent, {
    "info": {
        "componentName": "TreeMenuComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 200,
        "height": 200,
        "activeColor": "#545C64",
        "activeTextColor": "#ffffff",
        "dataProvider": [],
        "activeNode": ""
    },

    "label": {
        "label_text": "TreeMenu Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },

    "style": {
        "border": "1px none #dddddd",
        "borderRadius": "0",
        "cursor": "default"
    }
});

TreeMenuComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    template: "font",
    label: "font"
}, {
    label: "Active Style",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "activeColor",
        type: "color",
        label: "bg color",
        show: true,
        writable: true,
        description: "active Text Color"
    }, {
        owner: "setter",
        name: "activeTextColor",
        type: "color",
        label: "text color",
        show: true,
        writable: true,
        description: "activeColor"
    }]
}];

TreeMenuComponent.method_info = [];
WVPropertyManager.remove_event(TreeMenuComponent, "dblclick");
WVPropertyManager.remove_event(TreeMenuComponent, "click");

WVPropertyManager.add_property_group_info(TreeMenuComponent, {
    label: "TreeMenuComponent 고유 속성",
    children: [{
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        defaultValue: "\n[{ \"id\": \"root\", \"text\": \"root\", \"parent\": \"#\" },\n  {\n      \"id\": \"group1\",\n      \"text\": \"group1\",\n      \"parent\": \"root\",\n      \"state\": {\n          \"opened\": true,\n          \"selected\": true\n      }\n  },\n  { \"id\": \"item1\", \"text\": \"item1\", \"parent\": \"group1\" },\n  { \"id\": \"item2\", \"text\": \"item2\", \"parent\": \"group1\" }]",
        description: "트리를 구성하는 데이터입니다. 펼쳐진 상태나 선택된 상태를 설정하려면 state정보를 추가합니다(예제코드 참조) \nex) this.dataProvider = \n" + "[{ \"id\": \"root\", \"text\": \"root\", \"parent\": \"#\" },\n  {\n      \"id\": \"group1\",\n      \"text\": \"group1\",\n      \"parent\": \"root\",\n      \"state\": {\n          \"opened\": true,\n          \"selected\": true\n      }\n  },\n  { \"id\": \"item1\", \"text\": \"item1\", \"parent\": \"group1\" },\n  { \"id\": \"item2\", \"text\": \"item2\", \"parent\": \"group1\" }];"
    }]
});

// 이벤트 정보
WVPropertyManager.add_event(TreeMenuComponent, {
    name: "select",
    label: "버튼 클릭 이벤트",
    description: "버튼 클릭 이벤트 입니다.",
    properties: [{
        name: "index",
        type: "Number",
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }, {
        name: "data",
        type: "Object",
        default: "",
        description: "클릭한 버튼의 데이터 입니다."
    }]
});

// 이벤트 정보
WVPropertyManager.add_event(TreeMenuComponent, {
    name: "collapse",
    label: "트리 접힘 이벤트",
    description: "트리 접힘 이벤트입니다.",
    properties: [{
        name: "index",
        type: "Number",
        data: [],
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }, {
        name: "data",
        type: "Object",
        default: "",
        description: "클릭한 버튼의 데이터 입니다."
    }]
});

// 이벤트 정보
WVPropertyManager.add_event(TreeMenuComponent, {
    name: "expand",
    label: "트리 펼침 이벤트",
    description: "트리 펼침 이벤트입니다.",
    properties: [{
        name: "index",
        type: "Number",
        data: [],
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }, {
        name: "data",
        type: "Object",
        default: "",
        description: "클릭한 버튼의 데이터 입니다."
    }]
});

// 이벤트 정보
WVPropertyManager.add_event(TreeMenuComponent, {
    name: "dblclick",
    label: "더블클릭 이벤트",
    description: "더블클릭 이벤트입니다.",
    properties: [{
        name: "index",
        type: "Number",
        data: [],
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }, {
        name: "data",
        type: "Object",
        default: "",
        description: "클릭한 버튼의 데이터 입니다."
    }]
});