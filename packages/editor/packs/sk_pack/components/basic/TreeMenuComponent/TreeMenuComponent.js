class TreeMenuComponent extends WVDOMComponent {
    constructor() {
        super();

        this._invalidatePropertyData = false;
        this._invalidatePropertyActiveColor = false;
        this._invalidatePropertyActiveNode = false;
    }


    _onCreateElement() {
        // 에디터모드일때만!

        if (this.isEditorMode) {
            this.dataProvider = [{ "id": "root", "text": "root", "parent": "#" },
                {
                    "id": "group1",
                    "text": "group1",
                    "parent": "root",
                    "state": {
                        "opened": true,
                        "selected": true
                    }
                },
                { "id": "item1", "text": "item1", "parent": "group1" },
                { "id": "item2", "text": "item2", "parent": "group1" }
            ];

            var str = '<div class="myTree"></div>';
            let $el = $(this._element);
            let temp = $(str);
            $el.append(temp);

            this.setTreeHTML(this.dataProvider);
            this.updateStyle();
        } else {

        }
    }

    _onDestroy() {
        super._onDestroy();

    }

    _onImmediateUpdateDisplay() {
        this.bindEvent();
        this.updateStyle();
    }

    onLoadPage() {
        this.updateStyle();
    }

    getMyData(myId) {
        var needle = myId;

        var found;

        for (var i = 0; i < this.dataProvider.length; i++) {
            if (this.dataProvider[i].id == needle) {
                found = this.dataProvider[i];
            }
        }
        return found;
    }

    bindEvent() {
        var single_click_timeout;

        if (this.isEditorMode) {
            return;
        }
        var self = this;

        $(this._element).on("click", ".jstree-node > a", function(e) {
            var my = this;

            e.preventDefault();

            clearTimeout(single_click_timeout);
            single_click_timeout = setTimeout(function() {
                e.preventDefault();

                var index = $(self._element).find("li").index($(this).parent());
                var found = self.getMyData($(my).parent().attr("id"));

                self.dispatchWScriptEvent("select", {
                    index: index,
                    node: found
                })

                self.activeNode = $("#" + $(self).attr('id') + " .myTree").jstree('get_selected', true)[0];

                $("#" + $(self._element).attr('id') + " .myTree").jstree(true).redraw(true);

                self.active = index;
            }, 300);

        })

        $(this._element).on("dblclick", ".jstree-node > .jstree-clicked", function(e) {
            var my = this;
            e.preventDefault();
            e.stopPropagation();

            clearTimeout(single_click_timeout);

            var index = $(self._element).find("li").index($(this).parent());
            var found = self.getMyData($(my).parent().attr("id"));

            self.dispatchWScriptEvent("dblclick", {
                index: index,
                node: found
            })

            self.active = index;
        })

        $(this._element).on("click", ".jstree-closed > .jstree-icon", function(e) {
            var my = this;
            e.preventDefault();
            var index = $(self._element).find("li").index($(this).parent());
            var found = self.getMyData($(my).parent().attr("id"));


            self.dispatchWScriptEvent("collapse", {
                index: index,
                node: found
            })

            self.active = index;
        })

        $(this._element).on("click", ".jstree-open > .jstree-icon", function(e) {
            var my = this;
            e.preventDefault();
            var index = $(self._element).find("li").index($(this).parent());
            var found = self.getMyData($(my).parent().attr("id"));

            self.dispatchWScriptEvent("expand", {
                index: index,
                node: found
            })

            self.active = index;
        })

    }


    setTreeHTML(myData) {
        var self = this;

        $(this._element).find('.myTree')
            .on('ready.jstree', function(e, data) {
                self.activeNode = $("#" + $(self._element).attr('id') + " .myTree").jstree('get_selected', true)[0];
                self.updateStyle();
            }).jstree({
                'core': {
                    'data': myData
                },
                "plugins": ["wholerow"]

            })
    }

    _onCommitProperties() {
        if (this._invalidatePropertyData) {
            this.validateCallLater(this._validateData);
            this._invalidatePropertyData = false;
        }
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this.updateStyle);
        }

        if (this._invalidatePropertyActiveColor) {
            this.validateCallLater(this.updateStyle);
            this._invalidatePropertyActiveColor = false;
        }

        if (this._invalidatePropertyActiveNode) {
            this.validateCallLater(this._validateActiveNode);
            this._invalidatePropertyActiveNode = false;
        }

    }

    _validateActiveNode() {
        // $("#" + $(this._element).attr('id') + " .myTree").jstree(true).redraw(true);
        // console.log("this.activeNode :"+this.activeNode );
    }

    _validateData() {
        var str = '<div class="myTree"></div>';
        let $el = $(this._element);
        let temp = $(str);
        $el.append(temp);

        this.setTreeHTML(this.dataProvider);
        this.updateStyle();
    }


    updateStyle() {
        //let bgData = this.getGroupProperties("background");
        //let style = this._styleManager.getBackgroundStyle(bgData, `[id='${this.id}']`);
        let style = `<style>
					[id='${this.id}'] .jstree-default .jstree-anchor .jstree-icon:empty { display: none;}
					[id='${this.id}'] .jstree-node[aria-selected="true"] >.jstree-wholerow,
					[id='${this.id}'] .jstree-wholerow.jstree-wholerow-hovered { background: ${this.activeColor};}
					[id='${this.id}'] .jstree-node[aria-selected="true"]>a,
					[id='${this.id}'] .jstree-anchor.jstree-hovered{ color: ${this.activeTextColor};}
					[id='${this.id}'] *{ 
													font-family: ${this.font.font_type};
													font-size: ${this.font.font_size};
													color: ${this.font.font_color};
													font-weight: ${this.font.font_weight};													
									}
					</style>`;
        $(this._element).find("style").remove();
        $(this._element).append(style);
    }

    get font() {
        return this.getGroupProperties("font");
    }

    get activeColor() {
        return this.getGroupPropertyValue("setter", "activeColor");
    }

    set activeColor(color) {
        if (this._checkUpdateGroupPropertyValue("setter", "activeColor", color)) {
            this._invalidatePropertyActiveColor = true;
        }
    }

    get activeTextColor() {
        return this.getGroupPropertyValue("setter", "activeTextColor");
    }

    set activeTextColor(color) {
        if (this._checkUpdateGroupPropertyValue("setter", "activeTextColor", color)) {
            this._invalidatePropertyActiveColor = true;
        }
    }

    get dataProvider() {
        return this.getGroupPropertyValue("setter", "dataProvider");
    }

    set dataProvider(data) {
        if (this._checkUpdateGroupPropertyValue("setter", "dataProvider", data)) {
            this._invalidatePropertyData = true;
        }
    }

    get activeNode() {
        return this.getGroupPropertyValue("setter", "activeNode");
    }

    set activeNode(data) {
        if (this._checkUpdateGroupPropertyValue("setter", "activeNode", data)) {
            this._invalidatePropertyActiveNode = true;
        }
    }
}

WVPropertyManager.attach_default_component_infos(TreeMenuComponent, {
    "info": {
        "componentName": "TreeMenuComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 200,
        "height": 200,
        "activeColor": "#545C64",
        "activeTextColor": "#ffffff",
        "dataProvider": [],
        "activeNode": ""
    },

    "label": {
        "label_text": "TreeMenu Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },

    "style": {
        "border": "1px none #dddddd",
        "borderRadius": "0",
        "cursor": "default"
    }
});

TreeMenuComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    template: "font",
    label: "font"
}, {
    label: "Active Style",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "activeColor",
        type: "color",
        label: "bg color",
        show: true,
        writable: true,
        description: "active Text Color"
    }, {
        owner: "setter",
        name: "activeTextColor",
        type: "color",
        label: "text color",
        show: true,
        writable: true,
        description: "activeColor"
    }]
}];


TreeMenuComponent.method_info = [];
WVPropertyManager.remove_event(TreeMenuComponent, "dblclick");
WVPropertyManager.remove_event(TreeMenuComponent, "click");

WVPropertyManager.add_property_group_info(TreeMenuComponent, {
    label: "TreeMenuComponent 고유 속성",
    children: [{
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        defaultValue: "\n[{ \"id\": \"root\", \"text\": \"root\", \"parent\": \"#\" },\n  {\n      \"id\": \"group1\",\n      \"text\": \"group1\",\n      \"parent\": \"root\",\n      \"state\": {\n          \"opened\": true,\n          \"selected\": true\n      }\n  },\n  { \"id\": \"item1\", \"text\": \"item1\", \"parent\": \"group1\" },\n  { \"id\": \"item2\", \"text\": \"item2\", \"parent\": \"group1\" }]",
        description: "트리를 구성하는 데이터입니다. 펼쳐진 상태나 선택된 상태를 설정하려면 state정보를 추가합니다(예제코드 참조) \nex) this.dataProvider = \n" + "[{ \"id\": \"root\", \"text\": \"root\", \"parent\": \"#\" },\n  {\n      \"id\": \"group1\",\n      \"text\": \"group1\",\n      \"parent\": \"root\",\n      \"state\": {\n          \"opened\": true,\n          \"selected\": true\n      }\n  },\n  { \"id\": \"item1\", \"text\": \"item1\", \"parent\": \"group1\" },\n  { \"id\": \"item2\", \"text\": \"item2\", \"parent\": \"group1\" }];"
    }]
});

// 이벤트 정보
WVPropertyManager.add_event(TreeMenuComponent, {
    name: "select",
    label: "버튼 클릭 이벤트",
    description: "버튼 클릭 이벤트 입니다.",
    properties: [{
        name: "index",
        type: "Number",
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }, {
        name: "data",
        type: "Object",
        default: "",
        description: "클릭한 버튼의 데이터 입니다."
    }]
});


// 이벤트 정보
WVPropertyManager.add_event(TreeMenuComponent, {
    name: "collapse",
    label: "트리 접힘 이벤트",
    description: "트리 접힘 이벤트입니다.",
    properties: [{
        name: "index",
        type: "Number",
        data: [],
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }, {
        name: "data",
        type: "Object",
        default: "",
        description: "클릭한 버튼의 데이터 입니다."
    }]
});


// 이벤트 정보
WVPropertyManager.add_event(TreeMenuComponent, {
    name: "expand",
    label: "트리 펼침 이벤트",
    description: "트리 펼침 이벤트입니다.",
    properties: [{
        name: "index",
        type: "Number",
        data: [],
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }, {
        name: "data",
        type: "Object",
        default: "",
        description: "클릭한 버튼의 데이터 입니다."
    }]
});


// 이벤트 정보
WVPropertyManager.add_event(TreeMenuComponent, {
    name: "dblclick",
    label: "더블클릭 이벤트",
    description: "더블클릭 이벤트입니다.",
    properties: [{
        name: "index",
        type: "Number",
        data: [],
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }, {
        name: "data",
        type: "Object",
        default: "",
        description: "클릭한 버튼의 데이터 입니다."
    }]
});