"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function() { var gen = fn.apply(this, arguments); return new Promise(function(resolve, reject) {
            function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function(value) { step("next", value); }, function(err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ImageButtonComponent = function(_WVDOMComponent) {
    _inherits(ImageButtonComponent, _WVDOMComponent);

    function ImageButtonComponent() {
        _classCallCheck(this, ImageButtonComponent);

        var _this = _possibleConstructorReturn(this, (ImageButtonComponent.__proto__ || Object.getPrototypeOf(ImageButtonComponent)).call(this));

        _this.$el;
        _this.$wrap;

        _this._isResourceComponent = true;
        return _this;
    }

    _createClass(ImageButtonComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.$el = $(this._element).attr("data-id", this.id);
            this.$el.addClass("image-button-comp up");
            this.$el.prepend("<a class='no-img' nohref> </a>");
            this.$wrap = this.$el.find("a");

            if (!this.isEditorMode) {
                this.$el.addClass("viewer");
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.$wrap.off();
            this.$wrap.find(".img-up").off();
            this.$wrap.remove();
            this.$wrap = null;
            this.$el = null;
            _get(ImageButtonComponent.prototype.__proto__ || Object.getPrototypeOf(ImageButtonComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension")) {
                if (this._updatePropertiesMap.has("extension.upState")) {
                    this.isSelectUpState = true;
                    this.validateCallLater(this.startLoadResource);
                }
            }
        }
    }, {
        key: "startLoadResource",
        value: function() {
            var _ref = _asyncToGenerator( /*#__PURE__*/ regeneratorRuntime.mark(function _callee() {
                var success;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _context.next = 3;
                                return this.loadResource();

                            case 3:
                                success = _context.sent;

                                if (success && !this.isEditorMode) {
                                    this.bindEvent();
                                    this._setOverStyle();
                                    this._setDownStyle();
                                    this._setDisableStyle();
                                }

                                if (!this.isSelectUpState) {
                                    this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                }

                                this.isSelectUpState = false;
                                _context.next = 12;
                                break;

                            case 9:
                                _context.prev = 9;
                                _context.t0 = _context["catch"](0);

                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);

                            case 12:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [
                    [0, 9]
                ]);
            }));

            function startLoadResource() {
                return _ref.apply(this, arguments);
            }

            return startLoadResource;
        }()
    }, {
        key: "loadResource",
        value: function loadResource() {
            var _this2 = this;

            return new Promise(function(resolve, reject) {
                if (_this2.extension.upState && _this2.extension.upState.path) {
                    _this2.$wrap.empty().removeClass("no-img");
                    var upStateImgUrl = _this2.getImageUrl(_this2.extension.upState.path);

                    _this2.$wrap.prepend("<img class='img-up img-over img-down img-disable' alt='up state image' >");
                    var self = _this2;
                    var $img = _this2.$wrap.find(".img-up");

                    $img.off("error").on("error", function() {
                        self._setNoDataStyle();
                        reject("error");
                    }).attr("src", upStateImgUrl);

                    $img.off("load").on("load", function(e) {
                        //기본 이미지가 바뀔때에는 사이즈 원본사이즈로 조정
                        if (self.isSelectUpState) {
                            $img.width("auto");
                            $img.height("auto");

                            self.width = parseInt($img.width());
                            self.height = parseInt($img.height());
                            $img.width('');
                            $img.height('');
                            if (self.isEditorMode) {
                                self.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
                            }

                            self.$wrap.find(".img-up").css("visibility", "visible");
                        } else {
                            self.$wrap.find(".img-up").css("visibility", "hidden");
                        }

                        resolve(true);
                    });
                } else {
                    _this2._setNoDataStyle();
                    resolve(false);
                }
            });
        }
    }, {
        key: "bindEvent",
        value: function bindEvent() {
            var self = this;
            this.$wrap.off();
            this.$wrap.on("mouseover, mouseenter", function() {
                self._setStateClass("over");
            });

            this.$wrap.on("mouseout, mouseleave", function(e) {
                self._setStateClass("up");
            });
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {
            this.$wrap.find(".img-up").css({
                "visibility": "visible"
            });
        }
    }, {
        key: "_setStateClass",
        value: function _setStateClass(name) {
            this.$el.removeClass("up over down disable");
            this.$el.addClass(name);
        }
    }, {
        key: "_setOverStyle",
        value: function _setOverStyle() {
            if (this.extension.overState && this.extension.overState.path) {
                var overStateImgUrl = this.getImageUrl(this.extension.overState.path);
                this.$wrap.find(".img-up").removeClass("img-over");
                this.$wrap.prepend("<img class='img-over' alt='over state image' src='" + overStateImgUrl + "'>");
            }
        }
    }, {
        key: "_setDownStyle",
        value: function _setDownStyle() {
            if (this.extension.downState && this.extension.downState.path) {
                var downStateImgUrl = this.getImageUrl(this.extension.downState.path);
                this.$wrap.find(".img-up").removeClass("img-down");
                this.$wrap.prepend("<img class='img-down' alt='down state image' src='" + downStateImgUrl + "'>");
            }
        }
    }, {
        key: "_setDisableStyle",
        value: function _setDisableStyle() {
            if (this.extension.disableState && this.extension.disableState.path) {
                var disableStateImgUrl = this.getImageUrl(this.extension.disableState.path);
                this.$wrap.find(".img-up").removeClass("img-disable");
                this.$wrap.prepend("<img class='img-disable' alt='disable state image' src='" + disableStateImgUrl + "'>");
            }
        }
    }, {
        key: "_setNoDataStyle",
        value: function _setNoDataStyle() {
            this.$wrap.empty().addClass('no-img');
            this.$wrap.prepend("<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDlBNTQ1RDUyNDQwMTFFODg5QjlDQTk0Q0E0RTE2RjIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDlBNTQ1RDQyNDQwMTFFODg5QjlDQTk0Q0E0RTE2RjIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCRDE2NTAyMkUzMEYxMUU3QUUzQUZBQTdGOTAzMDQwMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpCRDE2NTAyM0UzMEYxMUU3QUUzQUZBQTdGOTAzMDQwMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuB5AwIAAAidSURBVHja7FpZc1NlGH7TNG267226UCi77AgCssumqOACwgA6OOMMM44/gAvvvHDGa8cbL7xwxBXZRZFFgbKDViqLLdACbVrSNN2TJs3i83w5JwZJoU1bGh2+mTPn5OvJOd+7PM/7vF9qCAQC8n8YcfI/GU8MibURH/7hk59rAl1ur8Q6bAwG4Rqt29eOK45oSGe3V5ZMyhVLRmJMG9LY5pbjV+1FvUaEY0R2khRnm2PaEJMx7uGpxdHj86uzx+uXpg6PxAo9G5BPeWkJkhAfF1rjQw3Rhw3h23GqLqYisWVBiZTkJD0a7JHGtuWjxGwaenKLg8f9vUS/u8cvnx6t7TtrRRrJiUZJjB96Q9xYbFKCMbKRcYaB15GAf+gxYgcWv0Qauzy+qNcwbAXR2tItP/x+T13nAsQb5xeriHTDmL0XG6Td5Y2+ID7OkYKULc0NAvfPu+3S0OqWkfg8piAF52RQrOG/YUhGskmm4jh+zS4VtW2SmWJSBj2HgjxjVMbj0Vpe5Kyzl3zuy7h8p10qbreJG7XqbHWLrJqWL1sXl8rcsVly8nqzuufUXw65ZXMOXUQaW7vlIHKbxXKsJUVemF6g0qQ/g0XNrwGYuqnHFwgVY52hEkH58XGGoTGkzuGSb89YkcfJsmxKnhypbJKvT9fJ63OKJAup0dcxsSg1dL1gfI4cu9IkZ6ocirVWTM1T87NHZw5NapFldp6zyuj8ZHn5aYuMykuWTai09C6NuQcl0J/h6PTIxVutsmBCtqzB86aVpiuHPFWchnRziNPtG1xDuNAGGMHFjs5PkbWzLWJEyJnfP1bcQ2rlIxoJsqO8Tu7YXWGaLaAi2IwFRxoeb0BaunrUNZlqIgwgi/F7Dsx7+1m/ek8tQ1A21NqdChNlMOKlmQVqjkqCx/SRGZIO5lk9I19OAKS7zltlzSyL5KMN2HexUepbXMoRS57KlZn/YiJLZiKOYBo1A28H8I6N84rEjFry4oyCgTVW99sR1D67zjeoQvXaM4Whv91tdsmpqmbZNL9E9l1qlMklabJ4Yo5creuQ/b81KpnN77yzdKRinsOVNvHBw73lfQ4K4tbFIwavQ4w0yoAJLrILOZsEJiGr0ONLwfcdaMTmgTJN8Dpl9ttLSuV31ARbu1vWId/JZlwku86jfzYxyDILxjAtyxFB4orG5aQmwDEO1TosGJ8thVnmwasjAQnmaFGmWfLSE+WbM/XKGA6q4ULMf3fWKlmpJqltcirWKYCBXNxKME84JTeAsqcCzMevNctp3LcT35uBtEw1x6uIMq34mRnw1el6qWlyPtCLRB+RQJDjGRHWjHVzCiGnfUo6MCqtzh411+r0KgZT9yESr4IMuEC9OWsFcF+aaZEeXHOeBY9Uff5mi1yr71BYsXe4obsaVVE8+VezMpSpzGiytqSb4wfGWgR2R7dPjiEtKClOXneIFZqIgu5wZXDuNF5MKuW9h/6wqTmjVsjacN9Pl22SnhQvF0C1GTgTV+vnFins8F4ufnZZpnpmG5zzPKo8JQqx9vmJOzjuysEKW4iAomItApRp9MrsQqnBi8lOxAW9ux4YYAqsnJqvMMCNizfmFau5EvT8nXCABz3GRswx9RaiXrDnYPpkAxM8J2m9d6LWuJFxec+z47JU3ZpVBlZMMskeqGGdgKKuI/V4II1hbrPBqgNjVTV2qpdzzhRvUMr1urVDzRErXtQCLuRqvTaHAsc8Z025VNMWcq5eKfSSwZSlUxydPUraTxmRrmrLhnnF9+E2KoyMwoOYy1sWlmCxnQoPrA1XQLXsoasaOqUILDMWRY3qdTPmqhu7JD89QamASswxnW5gjqlEtquEaIzcJfokJQGpCRyumpan0i0N+CDWBowRl8cfUqF3UBypeplyN+91aXMu5UUfUuWGNsc6w0WwaNKAYGRdCgMc1YhopHEAuCBrVcFhey8GMbLrglXSgK3ta8dFaYiGEXqDFZ1ptBxCsUdLm7Wo4Jzjhh5zlwa9CiwxtYgHptQtW5diH6bW3LHZyruMIDVVpEGSYBaQ0fjdNOCDjmBBvQ0n6ooiqoKoh7UDHmaE+Jm7GvrOZFCG+0P9NslA11p6z0Lgh+YeIgZJ0/Q+d0zGW1KVs76DUCXR1GhZQVG5ZFJOPyISCKYWub8cFEt5TVWqWtScJFXcGCEqWGKGwu/4VbuaY9NEME8oTJVfMbdscq7CDw2eBDnzyxV7xFeG76JQLRD8CdoOziQYMB7PI17rmrv7FxFS5Ldn60Md278HDXtg7kbLA3PntLk5Y7JUK6D37CGfaenCVPZqu4j6HpdPo7TlcKQTkWVqOiIo6l4NoZ56c9GIEEeS+ogFnQL1a53b1bVSxg/OsRZcQN5T1rPYsU58UV4X5n2DwhTraLLZpK71SPDMz7TLG9ZVPtIQfYOYDyjOGpzN7EOo7txgYBRIFGyXiTWzKRiVp1HZp5dmqHdyke+uKAu1vCzCfn9wPTrr9Qnsd+G1SJvEAxmU9xSUrDHElL7BYEzRFoFFh/fniWFbtMqxfdgSuM8QAvvENfug/9BDIygeKe/nQ6Zz0JgB+svaqyHvrSozyBCMj/ZVr8BpP2S/CYLRSGOo16q1Atrfca665UMUyPcf+5YpXnoEp83AhQ+s5WdxYyFdNCEnqucB9L5h2/uFMbtx2kBj0D4HaMyEsG2hvoywnx06h3UTG8bsxekNGOP9/pw1UNvk7Nf3bf9sOd0e9t14zZh1SA/37gsNfTaGhbHidjtpmdaUx8TPCjBmP7JkHWjeQ2Nq+mBMOZQFW2mw0cf4fn3M/D6CxRyEMUwzD2ULFa8uOMMHFw95H9Dk0ClE5oOIYn24f7UFNa+GfP8MC7SgEPrQBvtMRkMcl+X2+o3AhV4S9uDYBgc0xaQhmjHsZd/C8QqOKTiStU7YpuHhK431em+fnvyb0xNDhmb8LcAA905h+YYwrJEAAAAASUVORK5CYII=' alt='no image'>");
        }
    }, {
        key: "getImageUrl",
        value: function getImageUrl(str) {
            return wemb.configManager.serverUrl + str;
        }
    }, {
        key: "extension",
        get: function get() {
            return this.getGroupProperties("extension");
        }
    }]);

    return ImageButtonComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(ImageButtonComponent, {
    "info": {
        "componentName": "ImageButtonComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 100,
        "height": 100
    },
    "label": {
        "label_using": "N",
        "label_text": "Image Button"
    },

    "style": {
        "cursor": "pointer"
    },

    "extension": {
        "overState": "",
        "downState": "",
        "disableState": "",
        "upState": ""
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
ImageButtonComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    label: "upState 설정",
    template: "resource",
    children: [{
        owner: "extension",
        name: "upState",
        type: "resource",
        label: "Up Img",
        options: {
            type: "buttons"
        },
        show: true,
        writable: true,
        description: "기본 상태 이미지"
    }, {
        owner: "extension",
        name: "overState",
        type: "resource",
        label: "Over Img",
        options: {
            type: "buttons"
        },
        show: true,
        writable: true,
        description: "over 이미지"
    }]
}];

//  추후 추가 예정
ImageButtonComponent.method_info = [];