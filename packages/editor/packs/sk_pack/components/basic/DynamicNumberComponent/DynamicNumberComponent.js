class DynamicNumberComponent extends WVDOMComponent {
    constructor() {
        super();

        this._invalidateProperty = false;
    }

    _onCreateElement() {
        $(this._element).append('<div class="odometer"></div>');
    }

    _onImmediateUpdateDisplay() {
        this._validateProperty();
        this._validateFontProperty();
    }

    _onDestroy() {
        //
        this.odometer = null;
        super._onDestroy();
    }


    _onCommitProperties() {
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontProperty);
        }

        if (this._invalidateProperty || this._updatePropertiesMap.has("normal.format")) {
            this.validateCallLater(this._validateSetterProperty);
        }
    }


    _validateProperty() {
        let $odometer = $(this._element).find(".odometer");
        $odometer.empty();
        var oOdometer = $odometer.get(0);

          if(!this.odometer){
                this.odometer = new Odometer({
                      el: oOdometer,
                      theme: 'minimal',
                      auto: false,
                      format: this.format
                });

          }
          this.odometer.options.format = this.format;
          this.odometer.resetFormat();

          this.odometer.update(this.value);

    }

    _validateFontProperty() {
        let fontProps = this.getGroupProperties("font");
        let $el = $(this._element);
        $el.css({
            "fontFamily": fontProps.font_type,
            "fontSize": fontProps.font_size,
            "fontWeight": fontProps.font_weight,
            "color": fontProps.font_color,
            "textAlign": fontProps.text_align
        });


        let styleStr =
            '<style>' +
            '[id="' + this.id + '"] .odometer-digit *{font-family: ' + fontProps.font_type + '}' +
            '</style>';
        $el.find("style").remove();
        $el.append(styleStr);
    }

    _validateSetterProperty() {
        this._validateProperty();
        this._validateFontProperty();

        this._invalidateProperty = false;
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: this.value
            })
        }
    }


    get format() {
        return this.getGroupPropertyValue("normal", "format");
    }

    set value(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "value", value)) {
            this._invalidateProperty = true;
        }
    }

    get value() {
        return this.getGroupPropertyValue("setter", "value");
    }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(DynamicNumberComponent, {
    "info": {
        "componentName": "DynamicNumberComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 158,
        "height": 53,
        "value": "123456"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 40,
        "font_weight": "normal",
        "text_align": "left"
    },

    "normal": {
        "format": "(,ddd)"
    },

    "label": {
        "label_using": "N",
        "label_text": "Dynamic Number Component"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
DynamicNumberComponent.property_panel_info = [{
    label: "일반 속성",
    template: "primary"
}, {
    label: "위치 크기 정보",
    template: "pos-size-2d"
}, {
    label: "레이블 속성",
    template: "label"
}, {
    label: "폰트 속성",
    template: "font"
}, {
    label: "Format",
    children: [{
        owner: "normal",
        name: "format",
        type: "select",
        options: {
            items: [
                { label: "(,ddd)", value: "(,ddd)" },
                { label: "(,ddd).dd", value: "(,ddd).dd" },
                { label: "( ddd).dd", value: "( ddd).dd" },
                { label: "d", value: "d" },
            ]
        },
        label: "type",
        writable: true,
        show: true,
        description: "format",
    }, {
        owner: "setter",
        name: "value",
        type: "string",
        label: "value",
        writable: true,
        show: true,
        description: "value"
    }]
}];


// 이벤트 정보
WVPropertyManager.add_event(DynamicNumberComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.remove_property_group_info(DynamicNumberComponent, "background");
WVPropertyManager.add_property_group_info(DynamicNumberComponent, {
    label: "DynamicNumberComponent 고유 속성",
    children: [{
        name: "value",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: "12345",
        description: "표기할 값입니다."
    }]
});
