"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DynamicNumberComponent = function(_WVDOMComponent) {
    _inherits(DynamicNumberComponent, _WVDOMComponent);

    function DynamicNumberComponent() {
        _classCallCheck(this, DynamicNumberComponent);

        var _this = _possibleConstructorReturn(this, (DynamicNumberComponent.__proto__ || Object.getPrototypeOf(DynamicNumberComponent)).call(this));

        _this._invalidateProperty = false;
        return _this;
    }

    _createClass(DynamicNumberComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            $(this._element).append('<div class="odometer"></div>');
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this._validateProperty();
            this._validateFontProperty();
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            //
            this.odometer = null;
            _get(DynamicNumberComponent.prototype.__proto__ || Object.getPrototypeOf(DynamicNumberComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this._validateFontProperty);
            }

            if (this._invalidateProperty || this._updatePropertiesMap.has("normal.format")) {
                this.validateCallLater(this._validateSetterProperty);
            }
        }
    }, {
        key: "_validateProperty",
        value: function _validateProperty() {
            var $odometer = $(this._element).find(".odometer");
            $odometer.empty();
            var oOdometer = $odometer.get(0);

            if(!this.odometer){
                  this.odometer = new Odometer({
                        el: oOdometer,
                        theme: 'minimal',
                        auto: false,
                        format: this.format
                  });

            }
            this.odometer.options.format = this.format;
            this.odometer.resetFormat();

            this.odometer.update(this.value);
        }
    }, {
        key: "_validateFontProperty",
        value: function _validateFontProperty() {
            var fontProps = this.getGroupProperties("font");
            var $el = $(this._element);
            $el.css({
                "fontFamily": fontProps.font_type,
                "fontSize": fontProps.font_size,
                "fontWeight": fontProps.font_weight,
                "color": fontProps.font_color,
                "textAlign": fontProps.text_align
            });

            var styleStr = '<style>' + '[id="' + this.id + '"] .odometer-digit *{font-family: ' + fontProps.font_type + '}' + '</style>';
            $el.find("style").remove();
            $el.append(styleStr);
        }
    }, {
        key: "_validateSetterProperty",
        value: function _validateSetterProperty() {
            this._validateProperty();
            this._validateFontProperty();

            this._invalidateProperty = false;
            if (!this.isEditorMode) {
                this.dispatchWScriptEvent("change", {
                    value: this.value
                });
            }
        }
    }, {
        key: "format",
        get: function get() {
            return this.getGroupPropertyValue("normal", "format");
        }
    }, {
        key: "value",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "value", value)) {
                this._invalidateProperty = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "value");
        }
    }]);

    return DynamicNumberComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(DynamicNumberComponent, {
    "info": {
        "componentName": "DynamicNumberComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 158,
        "height": 53,
        "value": "123456"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 40,
        "font_weight": "normal",
        "text_align": "left"
    },

    "normal": {
        "format": "(,ddd).dd"
    },

    "label": {
        "label_using": "N",
        "label_text": "Dynamic Number Component"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
DynamicNumberComponent.property_panel_info = [{
    label: "일반 속성",
    template: "primary"
}, {
    label: "위치 크기 정보",
    template: "pos-size-2d"
}, {
    label: "레이블 속성",
    template: "label"
}, {
    label: "폰트 속성",
    template: "font"
}, {
    label: "Format",
    children: [{
        owner: "normal",
        name: "format",
        type: "select",
        options: {
            items: [{ label: "(,ddd)", value: "(,ddd)" }, { label: "(,ddd).dd", value: "(,ddd).dd" }, { label: "(ddd).dd", value: "(ddd).dd" }, { label: "d", value: "d" }]
        },
        label: "type",
        writable: true,
        show: true,
        description: "format"
    }, {
        owner: "setter",
        name: "value",
        type: "string",
        label: "value",
        writable: true,
        show: true,
        description: "value"
    }]
}];

// 이벤트 정보
WVPropertyManager.add_event(DynamicNumberComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.remove_property_group_info(DynamicNumberComponent, "background");
WVPropertyManager.add_property_group_info(DynamicNumberComponent, {
    label: "DynamicNumberComponent 고유 속성",
    children: [{
        name: "value",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: "12345",
        description: "표기할 값입니다."
    }]
});
