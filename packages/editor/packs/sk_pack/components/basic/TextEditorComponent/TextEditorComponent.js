class TextEditorComponent extends WVDOMComponent {
    constructor() {
        super();
        this.textElement;
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateElement() {
        this.textElement = document.createElement("div");
        this._element.appendChild(this.textElement);
    }

    _onImmediateUpdateDisplay() {
        this._updateTextValue();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this._updateTextValue);
        }
    }

    _onDestroy() {
        this.textElement = null;
        super._onDestroy();
    }

    _updateTextValue() {
        if (this.textElement) {
            this.textElement.innerHTML = this.text;
        }
    }

    get text() {
        return this.getGroupPropertyValue("extension", "text");
    }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(TextEditorComponent, {
    "info": {
        "componentName": "TextEditorComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 300,
        "height": 300
    },
    "style": {
        "backgroundColor": "#eeeeee",
        "border": "1px solid #000000",
        "borderRadius": 3
    },

    "label": {
        "label_using": "N",
        "label_text": "Text Component"
    },

    "extension": {
        "text": "Text Editor Component"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
TextEditorComponent.property_panel_info = [{
    label: "일반 속성",
    template: "primary"
}, {
    label: "위치 크기 정보",
    template: "pos-size-2d"
}, {
    label: "레이블 속성",
    template: "label"
}, {
    label: "배경",
    template: "background"
}, {
    label: "외각선",
    template: "border"
}];

WVPropertyManager.add_property_group_info(TextEditorComponent, {
    label: "TextEditorComponent 고유 속성",
    children: [{
        name: "text",
        type: "string",
        show: true,
        writable: false,
        description: "Text Editor를 통해 작성된 HTML형식의 데이터입니다."
    }]
});