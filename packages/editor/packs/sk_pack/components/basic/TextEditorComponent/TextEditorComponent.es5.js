"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextEditorComponent = function(_WVDOMComponent) {
    _inherits(TextEditorComponent, _WVDOMComponent);

    function TextEditorComponent() {
        _classCallCheck(this, TextEditorComponent);

        var _this = _possibleConstructorReturn(this, (TextEditorComponent.__proto__ || Object.getPrototypeOf(TextEditorComponent)).call(this));

        _this.textElement;
        return _this;
    }

    _createClass(TextEditorComponent, [{
        key: "getExtensionProperties",
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.textElement = document.createElement("div");
            this._element.appendChild(this.textElement);
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this._updateTextValue();
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension")) {
                this.validateCallLater(this._updateTextValue);
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.textElement = null;
            _get(TextEditorComponent.prototype.__proto__ || Object.getPrototypeOf(TextEditorComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_updateTextValue",
        value: function _updateTextValue() {
            if (this.textElement) {
                this.textElement.innerHTML = this.text;
            }
        }
    }, {
        key: "text",
        get: function get() {
            return this.getGroupPropertyValue("extension", "text");
        }
    }]);

    return TextEditorComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(TextEditorComponent, {
    "info": {
        "componentName": "TextEditorComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 300,
        "height": 300
    },
    "style": {
        "backgroundColor": "#eeeeee",
        "border": "1px solid #000000",
        "borderRadius": 3
    },

    "label": {
        "label_using": "N",
        "label_text": "Text Component"
    },

    "extension": {
        "text": "Text Editor Component"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
TextEditorComponent.property_panel_info = [{
    label: "일반 속성",
    template: "primary"
}, {
    label: "위치 크기 정보",
    template: "pos-size-2d"
}, {
    label: "레이블 속성",
    template: "label"
}, {
    label: "배경",
    template: "background"
}, {
    label: "외각선",
    template: "border"
}];

WVPropertyManager.add_property_group_info(TextEditorComponent, {
    label: "TextEditorComponent 고유 속성",
    children: [{
        name: "text",
        type: "string",
        show: true,
        writable: false,
        description: "Text Editor를 통해 작성된 HTML형식의 데이터입니다."
    }]
});