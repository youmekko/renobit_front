/*
실행 조건

1. 시작시
      autoPlay가 true인 경우

      - 롤링에 포함된 페이지 목록에서 현재 열린 페이지 index와 pageInfo를 찾는다.

      - 만약 존재한다면
            index와 pageInfo를 설정 한 후 autoPlay가 true인 경우 자동 실행 시작.

      - 존재하지 않는다면
            index= -1, pageInfo = null, autoPlay = false로 설정


2. 시작 후
      - 존재하는 페이지에서 존재하지 않는 페이지로 이동하면
            index= -1, pageInfo = null, autoPlay = false로 설정

      - 존재하지 않는 페이지에서 존재하는 페이지로 이동하면
            index = 자동 구하기, pageInfo = 자동구학, autoPlay = false


 */
class PageHistoryComponent extends WVDOMComponent {


      constructor() {
            super();

            this.pageInfoList = [{
                  name: "test1"
            }, {
                  name: "test2"
            }, {
                  name: "test3"
            }]

            this._app = null;
      }

      _onCreateElement() {
            /* 추후 추가 예정
		 <select @change="on_gotoPageAt($event)" v-model="currentIndex">
			  <option v-for="pageInfo in pageInfoList" :value="pageInfo.idx" >{{currentIndex}}{{pageInfo.idx}}{{pageInfo.name}}</option>
		  </select>

		  */
            let str = '';
            if (this.isEditorMode) {
                  str = `
            <div class="page-history-btn-wrap">
                  <a :style="{'color':btnStyle.normalColor, 'border-color':btnStyle.normalColor}"  class="prev" nohref><i class="fa fa-arrow-left"></i></a>
                  <a :style="{'color':btnStyle.normalColor, 'border-color':btnStyle.normalColor}" class="next"  nohref><i class="fa fa-arrow-right"></i></a>
            </div>
            `
            } else {
                  str = `
            <div class="page-history-btn-wrap">
                  <a :style="{'color':btnStyle.normalColor, 'border-color':btnStyle.normalColor}" :class="{'disabled':(isFirstHistory==true)}" class="prev" @click="on_gotoPrevPage" nohref><i class="fa fa-arrow-left"></i></a>
                  <a :style="{'color':btnStyle.normalColor, 'border-color':btnStyle.normalColor}"  :class="{'disabled':(isLastHistory==true)}" class="next" @click="on_gotoNextPage" nohref><i class="fa fa-arrow-right"></i></a>
            </div>
            `
            }

            let self = this;
            var $temp = $(str);
            var app;
            $(this.element).append($temp);
            if (this.isEditorMode) {
                  app = new Vue({
                        el: $temp[0],
                        data() {
                              return {
                                    btnStyle: self.getGroupProperties("btnStyle")
                              }
                        }
                  })
            } else {
                  this._app = new Vue({
                        el: $temp[0],
                        data() {
                              return {
                                    pageInfoList: window.wemb.pageHistoryManager.pageInfoList,
                                    currentIndex: 0,
                                    btnStyle: self.getGroupProperties("btnStyle"),
                                    isFirstHistory:false,
                                    isLastHistory:false

                              }
                        },

                        methods: {
                              on_gotoPrevPage: function() {
                                    window.wemb.pageHistoryManager.gotoPrevPage();
                              },
                              on_gotoNextPage: function() {
                                    window.wemb.pageHistoryManager.gotoNextPage();
                              },
                              on_gotoPageAt: function(event) {
                                    window.wemb.pageHistoryManager.gotoPageAt(this.currentIndex);
                              },
                              updateHistoryInfo(){
                                    if(window.wemb.pageHistoryManager) {
                                          this.currentIndex = window.wemb.pageHistoryManager.currentIndex;
                                          this.isFirstHistory = window.wemb.pageHistoryManager.isFirstHistory;
                                          this.isLastHistory = window.wemb.pageHistoryManager.isLastHistory;
                                    }

                              }

                        }
                  })
            }


      }





      _onDestroy() {
            if (this.isViewerMode == true){
                  window.wemb.pageHistoryManager.removeCallback(this.name);
                  this._app.$destroy();
                  this._app=null;
            }
            super._onDestroy();

      }



      onLoadPage() {
            // editor모드에서는 실행되지 않음.
            if (this.isEditorMode == true)
                  return;


      }


      /*
	 컴포넌트가 container에 추가되면 자동으로 호출
	  */
      _onImmediateUpdateDisplay() {

            if (this.isViewerMode == true){
                  window.wemb.pageHistoryManager.addUpdateCallback(this.name, ()=>{
                        this._app.updateHistoryInfo();
                  })
                  if(this._app)
                        this._app.updateHistoryInfo();
            }
      }


      _onCommitProperties() {

      }





      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       기본 처리 끝.
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */


      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       메서드 추가
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       **/



}

WVPropertyManager.attach_default_component_infos(PageHistoryComponent, {
      "info": {
            "version": "1.0.0",
            "componentName": "PageHistoryComponent"
      },

      "setter": {
            "width": 58,
            "height": 28
      },

      "style": {
            "overflow": "hidden"
      },

      "btnStyle": {
            "normalColor": "#c5c4be"
      }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
PageHistoryComponent["property_panel_info"] = [{
      template: "primary"
},
      {
            template: "pos-size-2d"
      }, {
            label: "Button Color",
            template: "vertical",
            children: [{
                  owner: "btnStyle",
                  name: "normalColor",
                  label: "normal",
                  type: "color",
                  writable: true,
                  show: true,
                  description: "normal color"
            }]
      }
];
