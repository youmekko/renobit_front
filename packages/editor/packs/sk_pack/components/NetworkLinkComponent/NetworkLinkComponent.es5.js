"use strict";

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _set = function set(object, property, value, receiver) { var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent !== null) { set(parent, property, value, receiver); } } else if ("value" in desc && desc.writable) { desc.value = value; } else { var setter = desc.set; if (setter !== undefined) { setter.call(receiver, value); } } return value; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NetworkLinkComponent = function (_WVDOMComponent) {
      _inherits(NetworkLinkComponent, _WVDOMComponent);

      _createClass(NetworkLinkComponent, [{
            key: "width",
            set: function set(nValue) {
                  _set(NetworkLinkComponent.prototype.__proto__ || Object.getPrototypeOf(NetworkLinkComponent.prototype), "width", this._HIT_AREA_WIDTH, this);
            },
            get: function get() {
                  return this._HIT_AREA_WIDTH;
            }
      }, {
            key: "height",
            set: function set(nValue) {
                  _set(NetworkLinkComponent.prototype.__proto__ || Object.getPrototypeOf(NetworkLinkComponent.prototype), "height", this._HIT_AREA_HEIGHT, this);
            },
            get: function get() {
                  return this._HIT_AREA_HEIGHT;
            }
      }, {
            key: "toTarget",
            get: function get() {
                  var network = this.getGroupProperties("network");
                  return network.to.target;
            }
      }, {
            key: "fromTarget",
            get: function get() {
                  var network = this.getGroupProperties("network");
                  return network.from.target;
            }
      }]);

      function NetworkLinkComponent() {
            _classCallCheck(this, NetworkLinkComponent);

            var _this = _possibleConstructorReturn(this, (NetworkLinkComponent.__proto__ || Object.getPrototypeOf(NetworkLinkComponent)).call(this));

            _this._$line = null;
            _this._$hitArea = null;
            _this._$from = null;
            _this._$to = null;
            _this._$element = null;
            _this._HIT_AREA_WIDTH = 50;
            _this._HIT_AREA_HEIGHT = 50;
            _this._updateLinkTimerID = 0;

            // 위치값을 구하기 위한 목적으로 만들어지는 dom
            _this._$tempPosition = null;

            return _this;
      }

      _createClass(NetworkLinkComponent, [{
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  var _this2 = this;

                  var info = "            \n            <div class='line'>\n                  <span class='from'>from</span>\n                  <span class='to'>to</span>\n            </div>\n        ";

                  this._$line = $(info);
                  this._$from = this._$line.children(".from");
                  this._$to = this._$line.children(".to");
                  this._$element = $(this._element);
                  this._$element.append(this._$line);

                  if (this.isEditorMode) {
                        this._$hitArea = $("<div class=\"hitArea\"></div>");
                        this._$element.append(this._$hitArea);

                        this._$line.mousedown(function (e) {
                              _this2._$element.trigger("mousedown");
                        });
                        this._$hitArea.mousedown(function (e) {
                              _this2._$element.trigger("mousedown");
                        });
                  }

                  // 위치값을 구하기 위한 목적으로 만들어지는 dom
                  this._$tempPosition = $("<div style='visibility:hidden;width:1px;height:1px;position:absolute;pointer-events: none' class='_temp_" + this.id + "'></div>");
                  $("#_twoLayer").append(this._$tempPosition);
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {

                  if (this._$line) {
                        this._$line.off();
                        this._$line.remove();
                        this._$line = null;
                        this._$from = null;
                        this._$to = null;
                  }

                  if (this._$hitArea) {
                        this._$hitArea.off();
                        this._$hitArea.remove();
                        this._$hitArea = null;
                  }

                  if (this._$element) {
                        this._$element = null;
                  }

                  if (this._$tempPosition) {
                        this._$tempPosition.remove();
                        this._$tempPosition = null;
                  }

                  this._clearUpdateTimer();

                  _get(NetworkLinkComponent.prototype.__proto__ || Object.getPrototypeOf(NetworkLinkComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {
                  if (this._updatePropertiesMap.has("network")) {
                        this.validateCallLater(this._validateNetwork);
                  }
            }
      }, {
            key: "_onImmediateUpdateDisplay",
            value: function _onImmediateUpdateDisplay() {
                  /*
                  연결된 컴포넌트가 생성 후 처리해야하기 때문에
                  선그리는 링크 처리는 onloadPage()에서 처리
                  */
            }
      }, {
            key: "_validateNetwork",
            value: function _validateNetwork() {
                  var networkInfo = this.getGroupProperties("network");
                  var fromComInstance = null;
                  var toComInstance = null;

                  var proxy = null;
                  if (wemb.configManager.isEditorMode) {
                        proxy = wemb.editorProxy;
                  } else {
                        proxy = wemb.viewerProxy;
                  }

                  if (networkInfo.from.target) {
                        fromComInstance = proxy.twoLayerInstanceList.find(function (comInstance) {
                              if (networkInfo.from.target == comInstance.name) {
                                    return true;
                              } else {
                                    return false;
                              }
                        });
                  }

                  if (networkInfo.to.target) {
                        toComInstance = proxy.twoLayerInstanceList.find(function (comInstance) {
                              if (networkInfo.to.target == comInstance.name) {
                                    return true;
                              } else {
                                    return false;
                              }
                        });
                  }

                  this._drawUpdateLinkLine(fromComInstance, toComInstance, Number(networkInfo.gap));

                  if (networkInfo.from.port) this._$from.text(networkInfo.from.port);else this._$from.text("from");

                  if (networkInfo.to.port) this._$to.text(networkInfo.to.port);else this._$to.text("to");
            }

            /*
            실제 라인 그리는 메서드
             실행 순서
            1. from의 rect 구하기
            2. to의 rect 구하기
            3. from, to 간의 각도 구하기
            4.
              */

      }, {
            key: "_drawUpdateLinkLine",
            value: function _drawUpdateLinkLine(fromComInstance, toComInstance) {
                  var _this3 = this;

                  var gap = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

                  $(this._element).css({
                        visibility: "hidden"
                  });

                  this._clearUpdateTimer();

                  /*
                  이동하는 내용이 보이기 때문에 한번에 모아서 처리
                   */
                  this._updateLinkTimerID = setTimeout(function () {
                        _this3._updateLinkTimerID = 0;
                        // fromComInstance의 rect 구하기, fromComInstance 컴포넌트인 경우 이후 프로퍼티는 사용하지 않음.
                        var fromRect = Rectangle.getRectangle(fromComInstance, _this3.x, _this3.y, -100);
                        // toComInstance의 rect 구하기, toComInstance 컴포넌트인 경우 이후 프로퍼티는 사용하지 않음.
                        var toRect = Rectangle.getRectangle(toComInstance, _this3.x, _this3.y, 100);

                        var angle = Math.atan2(toRect.y - fromRect.y, toRect.x - fromRect.x) * 180 / Math.PI;

                        // 회전 위치에 따라
                        var newFromPoint = Rectangle.getIntersectionPoint(fromRect, {
                              x: fromRect.x,
                              y: fromRect.y
                        }, { x: toRect.x, y: toRect.y }, _this3._HIT_AREA_WIDTH);

                        var newToPoint = Rectangle.getIntersectionPoint(toRect, { x: fromRect.x, y: fromRect.y }, {
                              x: toRect.x,
                              y: toRect.y
                        }, _this3._HIT_AREA_HEIGHT);

                        // 두 점 사이의 중간 위치 구하기
                        var anglePoint = {
                              x: newToPoint.x - newFromPoint.x,
                              y: newToPoint.y - newFromPoint.y
                        };

                        var centerPoint = {
                              x: newFromPoint.x + anglePoint.x / 2,
                              y: newFromPoint.y + anglePoint.y / 2

                              // 회전 시 line 크기 구하기
                        };var lineWidth = Math.sqrt(Math.pow(anglePoint.x, 2) + Math.pow(anglePoint.y, 2));

                        // 0~360도 각도로 변경
                        var newAngle = angle;
                        if (angle >= -180 && angle <= -1) {
                              newAngle = 180 + (180 + angle);
                        }

                        var newPoint = _this3._getMatrixPosition(newAngle, centerPoint.x, centerPoint.y, gap);

                        var tx = -(lineWidth / 2) + _this3._HIT_AREA_WIDTH / 2;
                        var ty = +(_this3._HIT_AREA_HEIGHT / 2);

                        if (newAngle > 90 && newAngle <= 270) {
                              // 라인 시작 위치

                              _this3._$line.css({
                                    width: lineWidth,
                                    transform: "translate(" + tx + "px, " + ty + "px) rotate(" + angle + "deg)"
                              });

                              _this3._$from.css("transform", "rotate(180deg) translate(0,-100%)");
                              _this3._$to.css("transform", "rotate(180deg) translate(0,-100%)");

                              if (_this3._$hitArea) {
                                    _this3._$hitArea.css({
                                          transform: "rotate(" + angle + "deg)"
                                    });
                              }
                        } else {
                              // 라인 시작 위치
                              _this3._$line.css({
                                    width: lineWidth,
                                    transform: "translate(" + tx + "px, " + ty + "px) rotate(" + angle + "deg)"
                              });

                              _this3._$from.css("transform", "rotate(0deg)");
                              _this3._$to.css("transform", "rotate(0deg)");

                              if (_this3._$hitArea) {
                                    _this3._$hitArea.css({
                                          transform: "rotate(" + angle + "deg)"
                                    });
                              }
                        }

                        /*
                        현재 컴포넌트 위치 구하기
                        centerPoint 중심에 와야하기 때문에
                        this._HIT_AREA_WIDTH /2 , this._HIT_AREA_HEIGHT/2를 빼줌
                        */
                        //this.x = centerPoint.x - (this._HIT_AREA_WIDTH / 2);
                        //this.y = centerPoint.y - (this._HIT_AREA_HEIGHT / 2);

                        _this3.x = newPoint.x;
                        _this3.y = newPoint.y;

                        if (wemb.configManager.isEditorMode) {
                              // transform update 시키기
                              wemb.editorProxy.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);
                        }

                        $(_this3._element).css({
                              visibility: "visible"
                        });
                  }, 0);
            }

            /*
            외부에서 호출
            */

      }, {
            key: "updateLinkLine",
            value: function updateLinkLine() {
                  this._validateNetwork();
            }
      }, {
            key: "onLoadPage",
            value: function onLoadPage() {

                  this._validateNetwork();
            }

            /*
            업데이트를 위해 사용하는 타이머 제거하기
             */

      }, {
            key: "_clearUpdateTimer",
            value: function _clearUpdateTimer() {
                  clearInterval(this._updateLinkTimerID);
                  this._updateLinkTimerID = 0;
            }

            /*
             css3 기능을 활용해
             tx,ty의 위치 위치를 이동 및 회전 그리고 gap 만큼 이동한 위치 값 구하기.
             */

      }, {
            key: "_getMatrixPosition",
            value: function _getMatrixPosition(angle, tx, ty, gap) {

                  if (angle > 90 && angle <= 270) {
                        // 라인 시작 위치

                        this._$tempPosition.css({
                              transform: "translate(" + tx + "px, " + ty + "px) rotate(" + angle + "deg) translate(0, " + -gap + "px)"
                        });
                  } else {
                        // 라인 시작 위치
                        this._$tempPosition.css({
                              transform: "translate(" + tx + "px, " + ty + "px) rotate(" + angle + "deg) translate(0, " + gap + "px)"
                        });
                  }

                  var point = {
                        x: this._$tempPosition.position().left - 25,
                        y: this._$tempPosition.position().top - 25
                  };

                  return point;
            }
      }]);

      return NetworkLinkComponent;
}(WVDOMComponent);

var Rectangle = function () {
      function Rectangle() {
            _classCallCheck(this, Rectangle);
      }

      _createClass(Rectangle, null, [{
            key: "getRectangle",
            value: function getRectangle(wvComInstance) {
                  var parentX = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
                  var parentY = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
                  var startLeftMargin = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 100;
                  var defWidth = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 50;
                  var defHeight = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 50;

                  var rect = {
                        left: 0,
                        top: 0,
                        width: defWidth,
                        height: defWidth,
                        right: 0,
                        bottom: 0,
                        x: 0,
                        y: 0
                  };

                  if (wvComInstance) {
                        var $fromPoint = $(wvComInstance.element);

                        rect.left = $fromPoint.position().left;
                        rect.top = $fromPoint.position().top;
                        rect.width = $fromPoint.width();
                        rect.height = $fromPoint.height();
                        rect.right = rect.left + rect.width;
                        rect.bottom = rect.top + rect.height;
                  } else {
                        rect.left = parentX + startLeftMargin;
                        rect.top = parentY;
                        rect.right = rect.left + rect.width;
                        rect.bottom = rect.top + rect.height;
                  }

                  rect.x = rect.left + rect.width / 2;
                  rect.y = rect.top + rect.height / 2;

                  return rect;
            }
      }, {
            key: "lineIntersect",
            value: function lineIntersect(p1, p2, p3, p4) {

                  var x1 = p1.x;
                  var y1 = p1.y;
                  var x2 = p2.x;
                  var y2 = p2.y;
                  var x3 = p3.x;
                  var y3 = p3.y;
                  var x4 = p4.x;
                  var y4 = p4.y;

                  // Check if none of the lines are of length 0
                  if (x1 === x2 && y1 === y2 || x3 === x4 && y3 === y4) {
                        return false;
                  }

                  var denominator = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);

                  // Lines are parallel
                  if (denominator === 0) {
                        return false;
                  }

                  var ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator;
                  var ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator;

                  // is the intersection along the segments
                  if (ua < 0 || ua > 1 || ub < 0 || ub > 1) {
                        return false;
                  }

                  // Return a object with the x and y coordinates of the intersection
                  var x = x1 + ua * (x2 - x1);
                  var y = y1 + ua * (y2 - y1);

                  return { x: x, y: y };
            }
      }, {
            key: "getIntersectionPoint",
            value: function getIntersectionPoint(target, p1, p2) {

                  var t = new Point(target.left, target.top);
                  var tr = new Point(target.right, target.top);
                  var b = new Point(target.left, target.bottom);
                  var br = new Point(target.right, target.bottom);

                  var results = Rectangle.lineIntersect(t, tr, p1, p2);
                  if (results) {
                        return results;
                  }
                  var results = Rectangle.lineIntersect(p1, p2, tr, br);
                  if (results) {
                        return results;
                  }
                  var results = Rectangle.lineIntersect(p1, p2, b, br);
                  if (results) {
                        return results;
                  }
                  var results = Rectangle.lineIntersect(p1, p2, t, b);
                  if (results) {
                        return results;
                  }
                  return null;
            }
      }]);

      return Rectangle;
}();

var Point = function Point(x, y) {
      _classCallCheck(this, Point);

      this.x = x || 0;
      this.y = y || 0;
};

WVPropertyManager.attach_default_component_infos(NetworkLinkComponent, {
      "info": {
            "componentName": "NetworkLinkComponent",
            "version": "1.7.0(2019.01.03)"
      },

      "label": {
            "label_using": "Y",
            "label_text": ""
      },

      "network": {
            "gap": 0,
            "from": {
                  "ip": "",
                  "hostName": "",
                  "port": "",
                  "target": ""
            },
            "to": {
                  "ip": "",
                  "hostName": "",
                  "port": "",
                  "target": ""
            }
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
NetworkLinkComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}];

WVPropertyManager.add_property_panel_group_info(NetworkLinkComponent, {
      template: "network-link",
      children: [{
            owner: "network",
            name: "from",
            type: "object",
            label: "from",
            show: true,
            writable: true,
            defaultValue: {
                  ip: "0.0.0.0",
                  hostName: "",
                  port: "",
                  target: ""
            },
            description: "from 정보입니다."
      }, {
            owner: "network",
            name: "to",
            type: "object",
            label: "to",
            show: true,
            writable: true,
            defaultValue: {
                  ip: "1.0.0.0",
                  hostName: "",
                  port: "",
                  target: ""
            },
            description: "to 정보입니다."
      }]
});
