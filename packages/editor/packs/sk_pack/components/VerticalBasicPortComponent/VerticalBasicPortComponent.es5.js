"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VerticalBasicPortComponent = function (_WVDOMComponent) {
	_inherits(VerticalBasicPortComponent, _WVDOMComponent);

	function VerticalBasicPortComponent() {
		_classCallCheck(this, VerticalBasicPortComponent);

		var _this = _possibleConstructorReturn(this, (VerticalBasicPortComponent.__proto__ || Object.getPrototypeOf(VerticalBasicPortComponent)).call(this));

		console.log("VerticalBasicPortComponent 시작");
		_this.$portFromCircle = null;
		_this.$portLine = null;
		_this.$portToCircle = null;
		_this.$portFromText = null;
		_this.$portToText = null;
		return _this;
	}

	_createClass(VerticalBasicPortComponent, [{
		key: "_onCreateElement",
		value: function _onCreateElement() {
			this.$portLine = $("<span class=\"port-line\"></span>");
			this.$portFromCircle = $("<span class=\"port-from-circle\"></span>");
			this.$portToCircle = $("<span class=\"port-to-circle\"></span>");
			this.$portFromText = $("<span class=\"port-from-text\">FromText</span>");
			this.$portToText = $("<span class=\"port-to-text\">ToText</span>");

			$(this._element).append(this.$portLine);
			$(this._element).append(this.$portFromCircle);
			$(this._element).append(this.$portToCircle);
			// $(this._element).append(this.$portFromText);
			// $(this._element).append(this.$portToText);
		}
	}, {
		key: "_onCommitProperties",
		value: function _onCommitProperties() {

			if (this._updatePropertiesMap.has("portInfo")) {
				this.validateCallLater(this._validatePortInfo);
				this.validateCallLater(this._validateSizeProperty);
			}

			if (this.invalidateSize) {
				this.validateCallLater(this._validateSizeProperty);
			}
		}

		// _onCommitProperties() {
		//     if (this._updatePropertiesMap.has("portInfo")) { //1
		//         this.changeOffset(this.getGroupProperties("label").label_position);
		//     }
		//
		// }

	}, {
		key: "_validatePortInfo",
		value: function _validatePortInfo() {
			var portInfoProperties = this.getGroupProperties("portInfo");
			var borderStyleValue = portInfoProperties.lineStyle;

			this.$portLine.css({
				borderStyle: borderStyleValue
			});

			this.$portFromText.text(portInfoProperties.fromText);
			this.$portToText.text(portInfoProperties.toText);
		}
	}, {
		key: "_changeCssWithJquery",
		value: function _changeCssWithJquery(instance, property, value) {
			instance.css(property, value);
		}
	}, {
		key: "_validateSizeProperty",
		value: function _validateSizeProperty() {
			var CIRCLE_SIZE_HALF = this.$portFromCircle.width() / 2;
			var centerValue = this.width / 2; // 가운데 값 구함

			// 센터 값에서 서클 값 의 반만큼 이동한다.
			this._changeCssWithJquery(this.$portFromCircle, "left", centerValue - CIRCLE_SIZE_HALF);
			this._changeCssWithJquery(this.$portFromCircle, "top", 0);

			this._changeCssWithJquery(this.$portToCircle, "left", centerValue - CIRCLE_SIZE_HALF);
			this._changeCssWithJquery(this.$portToCircle, "bottom", 0);

			this._changeCssWithJquery(this.$portLine, "left", centerValue - 1);
			this._changeCssWithJquery(this.$portLine, "height", this.height - 1);

			this._directionCheck();
			this._positionCheck();
		}
	}, {
		key: "_validateMarginInfo",
		value: function _validateMarginInfo() {
			var textTopMargin = this.getGroupPropertyValue("portInfo", "textTopMargin");

			this._changeCssWithJquery(this.$portFromText, "bottom", "");
			this._changeCssWithJquery(this.$portFromText, "top", textTopMargin);

			var textBottomMargin = this.getGroupPropertyValue("portInfo", "textBottomMargin");
			this._changeCssWithJquery(this.$portToText, "top", "");
			this._changeCssWithJquery(this.$portToText, "bottom", textBottomMargin);
		}
	}, {
		key: "_directionCheck",
		value: function _directionCheck() {
			var directionValue = this.getGroupPropertyValue("portInfo", "direction");
			var $from = null;
			var $to = null;

			if (directionValue === "forward") {
				$from = this.$portFromText;
				$to = this.$portToText;
			} else {
				$from = this.$portToText;
				$to = this.$portFromText;
			}
			// 값 초기화
			var textTopMargin = this.getGroupPropertyValue("portInfo", "textTopMargin");
			var textBottomMargin = this.getGroupPropertyValue("portInfo", "textBottomMargin");

			// 값 초기화
			this._changeCssWithJquery($from, "bottom", "");
			this._changeCssWithJquery($from, "top", textTopMargin);

			this._changeCssWithJquery($to, "top", "");
			this._changeCssWithJquery($to, "bottom", textBottomMargin);
		}
	}, {
		key: "_positionCheck",
		value: function _positionCheck() {
			var centerValue = this.width / 2; // 가운데 값 구함
			var postionValue = this.getGroupPropertyValue("portInfo", "position");
			var fromPositionValue = 0;
			var toPositionValue = 0;

			if (postionValue === "right") {
				fromPositionValue = centerValue + 8;
				toPositionValue = centerValue + 8;
			} else {
				var fromTextLengthValue = this.getGroupPropertyValue("portInfo", "fromText");
				var toTextLengthValue = this.getGroupPropertyValue("portInfo", "toText");

				var fromSize = this.getLabelSize(fromTextLengthValue, 11);
				var toSize = this.getLabelSize(toTextLengthValue, 11);

				fromPositionValue = centerValue - fromSize.width - 10;
				toPositionValue = centerValue - toSize.width - 10;
			}

			this._changeCssWithJquery(this.$portFromText, "left", fromPositionValue);
			this._changeCssWithJquery(this.$portToText, "left", toPositionValue);
		}
	}, {
		key: "_onImmediateUpdateDisplay",
		value: function _onImmediateUpdateDisplay() {
			this._validatePortInfo();
			this._validateSizeProperty();
			this._validateMarginInfo();
			this._directionCheck();
		}
	}, {
		key: "_onDestroy",
		value: function _onDestroy() {
			_get(VerticalBasicPortComponent.prototype.__proto__ || Object.getPrototypeOf(VerticalBasicPortComponent.prototype), "_onDestroy", this).call(this);
		}
	}]);

	return VerticalBasicPortComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(VerticalBasicPortComponent, {
	"info": {
		"componentName": "VerticalBasicPortComponent",
		"version": "1.0.0"
	},
	"setter": {
		"width": 100,
		"height": 100,
		"min-width": 20,
		"min-height": 20
	},
	"style": {
		"cursor": "default",
		"min-width": "20px",
		"min-height": "20px"
	},
	"portInfo": {
		"lineStyle": "solid"
	}
});

VerticalBasicPortComponent.property_panel_info = [{
	template: "primary"
}, {
	template: "pos-size-2d"
}, {
	template: "cursor"
}, {
	template: "label"
}, {
	template: "background"
}, {
	template: "border"
}];