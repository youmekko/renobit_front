"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

/**
 * circle엘리먼트를 이용할 경우 중심점이 center로 고정되는 관계로 path를 이용해
 * 좌측 상단으로 그림.
 * https://developer.mozilla.org/ko/docs/Web/SVG/Tutorial/Paths
 * */
var SVGCircleComponent = function(_WVSVGComponent) {
    _inherits(SVGCircleComponent, _WVSVGComponent);

    function SVGCircleComponent() {
        _classCallCheck(this, SVGCircleComponent);

        return _possibleConstructorReturn(this, (SVGCircleComponent.__proto__ || Object.getPrototypeOf(SVGCircleComponent)).call(this));
    }

    _createClass(SVGCircleComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this._element = CPUtil.getInstance().createSVGElement("path");
            this.appendElement.appendChild(this.element);
        }
    }, {
        key: "getDrawPath",
        value: function getDrawPath() {
            var w = this.width;
            var h = this.height;
            var path = [
                ["M", 0, h / 2],
                ["a", w / 2, h / 2, 0, 1, 0, w, 0],
                ["a", w / 2, h / 2, 0, 1, 0, -w, 0]
            ];
            return Snap.parsePathString(path);
        }
    }, {
        key: "getTransformValue",
        value: function getTransformValue() {
            return "translate( " + this.x + ", " + this.y + " ) rotate( " + this.rotation + " 0 0 )";
        }
    }, {
        key: "_validatePosition",
        value: function _validatePosition() {
            $(this.element).attr({ "transform": this.getTransformValue() });
        }
    }, {
        key: "_validateSize",
        value: function _validateSize() {
            $(this.element).attr({ "d": this.getDrawPath() });
        }
    }, {
        key: "_validateRotation",
        value: function _validateRotation() {
            $(this.element).attr({ transform: this.getTransformValue() });
        }
    }]);

    return SVGCircleComponent;
}(WVSVGComponent);

WVPropertyManager.attach_default_component_infos(SVGCircleComponent, {
    "info": {
        "componentName": "SVGCircleComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100
    },
    "style": {
        "fill": "#0000ff",
        "stroke": "#ff0000",
        "stroke-width": 1,
        "stroke-dasharray": "0"
    }
});

SVGCircleComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    label: "Fill",
    template: "vertical",
    children: [{
        owner: "style",
        name: "fill",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "Fill Color"
    }]
}, {
    label: "Stroke",
    template: "vertical",
    children: [{
        owner: "style",
        name: "stroke",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "Stroke Color"
    }, {
        owner: "style",
        name: "stroke-width",
        type: "number",
        label: "width",
        show: true,
        writable: true,
        description: "Stroke Width"
    }, {
        owner: "style",
        name: "stroke-dasharray",
        type: "select",
        label: "style",
        show: true,
        writable: true,
        description: "border style",
        options: {
            items: [{ label: "solid", value: "0" }, { label: "dashed", value: "5 5" }]
        }
    }]
}];

WVPropertyManager.remove_property_group_info(SVGCircleComponent, "label");
WVPropertyManager.remove_property_group_info(SVGCircleComponent, "background");


WVPropertyManager.add_property_group_info(SVGCircleComponent, {
    label: "SVGCircleComponent 고유 속성",
    children: [{
        name: "fill",
        type: "string",
        owner: "style",
        show: true,
        writable: true,
        description: "fill 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
    }, {
        name: "stroke",
        type: "string",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
    }, {
        name: "stroke-width",
        type: "number",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 두께를 설정합니다."
    }]
});