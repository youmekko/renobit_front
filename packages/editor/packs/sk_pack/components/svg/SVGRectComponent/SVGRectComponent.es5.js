"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var SVGRectComponent = function(_WVSVGComponent) {
    _inherits(SVGRectComponent, _WVSVGComponent);

    function SVGRectComponent() {
        _classCallCheck(this, SVGRectComponent);

        return _possibleConstructorReturn(this, (SVGRectComponent.__proto__ || Object.getPrototypeOf(SVGRectComponent)).call(this));
    }

    _createClass(SVGRectComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this._element = CPUtil.getInstance().createSVGElement("rect");
            this.appendElement.appendChild(this.element);
        }
    }, {
        key: "_validatePosition",
        value: function _validatePosition() {
            $(this.element).attr({ x: this.x, y: this.y });
            if (this.rotation != 0) {
                this._validateRotation();
            }
        }
    }, {
        key: "_validateSize",
        value: function _validateSize() {
            $(this.element).attr({ width: this.width, height: this.height });
        }
    }, {
        key: "_validateRotation",
        value: function _validateRotation() {
            $(this.element).attr({ transform: "rotate(" + this.rotation + " " + this.x + " " + this.y + ")" });
        }
    }]);

    return SVGRectComponent;
}(WVSVGComponent);

WVPropertyManager.attach_default_component_infos(SVGRectComponent, {
    "info": {
        "componentName": "SVGRectComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100
    },

    "style": {
        "fill": "#00ffcc",
        "stroke": "#ff0000",
        "stroke-width": 1,
        "rx": 0,
        "stroke-dasharray": "0"
    }
});

SVGRectComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    label: "Fill",
    template: "vertical",
    children: [{
        owner: "style",
        name: "fill",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "Fill Color"
    }]
}, {
    label: "Stroke",
    template: "vertical",
    children: [{
        owner: "style",
        name: "stroke",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "Stroke Color"
    }, {
        owner: "style",
        name: "stroke-width",
        type: "number",
        label: "width",
        show: true,
        writable: true,
        description: "Stroke Width"
    }, {
        owner: "style",
        name: "stroke-dasharray",
        type: "select",
        label: "style",
        show: true,
        writable: true,
        description: "border style",
        options: {
            items: [{ label: "solid", value: "0" }, { label: "dashed", value: "5 5" }]
        }
    }]
}, {
    label: "Border Radius",
    template: "vertical",
    children: [{
        owner: "style",
        name: "rx",
        type: "number",
        label: "size",
        show: true,
        writable: true,
        description: "Broder radius"
    }]
}];


WVPropertyManager.remove_property_group_info(SVGRectComponent, "label");
WVPropertyManager.remove_property_group_info(SVGRectComponent, "background");

WVPropertyManager.add_property_group_info(SVGRectComponent, {
    label: "SVGRectComponent 고유 속성",
    children: [{
        name: "fill",
        type: "string",
        owner: "style",
        show: true,
        writable: true,
        description: "fill 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
    }, {
        name: "rx",
        type: "string",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 라운드 속성 값입니다."
    }, {
        name: "stroke",
        type: "string",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
    }, {
        name: "stroke-width",
        type: "number",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 두께를 설정합니다."
    }]
});