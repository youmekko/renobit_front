class SVGStreamLineComponent extends WVSVGStreamLineComponent {

    constructor() {
        super();
    }

    get paths() {
        let path = [];
        let pointList = this.points.concat();
        let currentPoint = pointList.shift();
        path.push(this.moveTo(currentPoint));
        for (let i = 0; i < pointList.length; i++) {
            path.push(this.lineTo(pointList[i]));
        }
        return Snap.parsePathString(path);
    }


    _createDefaultPoints() {
        return [new Point(this.x, this.y), new Point(this.x + this.width, this.y + this.height)];
    }

    handleTransformList() {
        let results = [];
        let idx;
        for (idx = 0; idx < this.points.length; idx++) {
            results.push("point-anchor");
        }
        return results;
    }

    _notifyInserPoint(point) {
        this.notifyComponentEvent(new WVPointComponentEvent(
            WVPointComponentEvent.INSERT_POINT, this, {
                point: point,
                type: "point-anchor"
            }
        ));
    }

    _notifyRemovePoint(point) {
        this.notifyComponentEvent(new WVPointComponentEvent(
            WVPointComponentEvent.REMOVE_POINT, this, {
                point: point
            }
        ));
    }


}


WVPropertyManager.attach_default_component_infos(SVGStreamLineComponent, {
    "info": {
        "componentName": "SVGStreamLineComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "startMarker": false,
        "endMarker": false,
        "pointCount": 2,
        "preview": true
    },

    "style": {
        "fill": "none",
        "stroke": "#AAffEE",
        "stroke-width": 4
    },

    "stateInfo": {
        "state": "normal",
        "direction": "left",
        "normal": "#ffffff",
        "minor": "#CCCCCC",
        "major": "#ffcc00",
        "warning": "#AA0000",
        "critical": "#FF0000"
    },

    "tweenInfo": {
        "duration": 1000,
        "streamSize": 20
    }
});

WVPropertyManager.remove_property_group_info(SVGStreamLineComponent, "label");
WVPropertyManager.remove_property_group_info(SVGStreamLineComponent, "background");



WVPropertyManager.add_property_group_info(SVGStreamLineComponent, {
    label: "SVGStreamLineComponent 고유 속성",
    children: [{
            name: "pointCount",
            type: "number",
            show: true,
            writable: true,
            description: "라인을 그릴 때 사용할 포인트 수입니다."
        }, {
            name: "starMarker",
            type: "boolean",
            show: true,
            writable: true,
            description: "라인 시작점에 화살표 마커 사용여부를 설정합니다."
        }, {
            name: "endMarker",
            type: "boolean",
            show: true,
            writable: true,
            description: "라인 끝점에 화살표 마커 사용여부를 설정합니다."
        }, {
            name: "stroke",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
        }, {
            name: "stroke-width",
            type: "number",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 두께를 설정합니다."
        }, {
            name: "state",
            type: "string",
            owner: "stateInfo",
            defaultValue: "'normal'",
            show: true,
            writable: true,
            description: "흐름선의 상태를 설정합니다.(상태는 normal, minor, major, warning, critical중 하나 입니다)"
        },
        {
            name: "direction",
            type: "string",
            owner: "stateInfo",
            defaultValue: "'left'",
            show: true,
            writable: true,
            description: "흐름선의 방향을 설정합니다.(상태는 left, right, biDirectional 중에 하나 입니다)"
        },
        {
            name: "duration",
            type: "number",
            owner: "tweenInfo",
            defaultValue: "1000",
            show: true,
            writable: true,
            description: "흐름선의 속도를 설정합니다.(단위는 밀리초를 이용합니다)"
        },
        {
            name: "streamSize",
            type: "number",
            owner: "tweenInfo",
            defaultValue: "20",
            show: true,
            writable: true,
            description: "흐름선의 길이를 설정합니다.(전체 흐름선 길이에 비례합니다)"
        }
    ]
});

SVGStreamLineComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        label: "Stroke",
        template: "vertical",
        children: [{
            owner: "style",
            name: "stroke",
            type: "color",
            label: "color",
            show: true,
            writable: true,
            description: "Stroke Color"
        }, {
            owner: "style",
            name: "stroke-width",
            type: "number",
            label: "width",
            show: true,
            writable: true,
            description: "Stroke Width"
        }]
    }, {
        label: "Arrow",
        template: "vertical",
        children: [{
                owner: "setter",
                name: "startMarker",
                type: "checkbox",
                label: "start",
                show: true,
                writable: true,
                description: "Start Arrow Icon"
            },
            {
                owner: "setter",
                name: "endMarker",
                type: "checkbox",
                label: "end",
                show: true,
                writable: true,
                description: "Start Arrow Icon"
            }
        ]
    }, {

        label: "Point",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "length",
            show: true,
            writable: true,
            description: "point"
        }]

    },
    {
        label: "Stream Preview",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "preview",
            type: "checkbox",
            label: "preview",
            show: true,
            writable: true,
            description: "preview",
        }]
    },
    {
        label: "Stream Info",
        template: "vertical",
        children: [{
                owner: "stateInfo",
                name: "state",
                type: "select",
                label: "state",
                show: true,
                writable: true,
                description: "state",
                options: {
                    items: [
                        { label: "normal", value: "normal" },
                        { label: "minor", value: "minor" },
                        { label: "major", value: "major" },
                        { label: "warning", value: "warning" },
                        { label: "critical", value: "critical" }
                    ]
                }
            },
            {
                owner: "stateInfo",
                name: "direction",
                type: "select",
                label: "direction",
                show: true,
                writable: true,
                description: "direction",
                options: {
                    items: [
                        { label: "left", value: "left" },
                        { label: "right", value: "right" },
                        { label: "biDirectional", value: "biDirectional" }
                    ]
                }
            },
            {
                owner: "stateInfo",
                name: "normal",
                type: "color",
                label: "normal",
                show: true,
                writable: true,
                description: "normal state color"
            },
            {
                owner: "stateInfo",
                name: "minor",
                type: "color",
                label: "minor",
                show: true,
                writable: true,
                description: "minor state color"
            },
            {
                owner: "stateInfo",
                name: "major",
                type: "color",
                label: "major",
                show: true,
                writable: true,
                description: "major state color"
            },
            {
                owner: "stateInfo",
                name: "warning",
                type: "color",
                label: "warning",
                show: true,
                writable: true,
                description: "warning state color"
            },
            {
                owner: "stateInfo",
                name: "critical",
                type: "color",
                label: "critical",
                show: true,
                writable: true,
                description: "critical state color"
            }
        ]
    },
    {
        label: "Tween Info",
        template: "vertical",
        children: [{
                owner: "tweenInfo",
                name: "duration",
                type: "number",
                label: "duration",
                show: true,
                writable: true,
                description: "duration"
            },
            {
                owner: "tweenInfo",
                name: "streamSize",
                type: "number",
                label: "streamSize",
                show: true,
                writable: true,
                description: "streamSize"
            }
        ]
    }
]