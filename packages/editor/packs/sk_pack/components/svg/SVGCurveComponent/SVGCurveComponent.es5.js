"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var SVGCurveComponent = function(_WVPointComponent) {
    _inherits(SVGCurveComponent, _WVPointComponent);

    function SVGCurveComponent() {
        _classCallCheck(this, SVGCurveComponent);

        return _possibleConstructorReturn(this, (SVGCurveComponent.__proto__ || Object.getPrototypeOf(SVGCurveComponent)).call(this));
    }

    _createClass(SVGCurveComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this._element = CPUtil.getInstance().createSVGElement("path");
            this.appendElement.appendChild(this.element);
        }
    }, {
        key: "_createDefaultPoints",
        value: function _createDefaultPoints() {
            var sp = new Point(this.x, this.y);
            var ep = new Point(this.x + this.width, this.y + this.height);
            var cp = sp.interpolate(ep, 0.5);
            return [sp, cp, ep];
        }
    }, {
        key: "_notifyInserPoint",
        value: function _notifyInserPoint(point) {
            this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.INSERT_POINT, this, {
                point: point,
                type: "curve-control"
            }));
        }
    }, {
        key: "_notifyRemovePoint",
        value: function _notifyRemovePoint(point) {
            this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.REMOVE_POINT, this, {
                point: point
            }));
        }
    }, {
        key: "handleTransformList",
        value: function handleTransformList() {
            var results = [];
            var idx = void 0;
            var max = this.points.length;
            for (idx = 0; idx < max; idx++) {
                if (idx == 0 || idx == max - 1) {
                    results.push("point-anchor");
                } else {
                    results.push("curve-control");
                }
            }
            return results;
        }
    }, {
        key: "controlPaths",
        get: function get() {
            var path = [];
            var pointList = this.points.concat();
            var max = pointList.length;
            var p1 = void 0,
                p2 = void 0;
            for (var i = 1; i < max; i++) {
                p1 = pointList[i - 1];
                p2 = pointList[i];
                path.push(this.moveTo(p1));
                path.push(this.lineTo(p2));
            }
            return Snap.parsePathString(path);
        }
    }, {
        key: "paths",
        get: function get() {
            var path = [];
            var pointList = this.points.concat();
            var max = pointList.length;
            var p1 = void 0,
                p2 = void 0,
                cp = void 0,
                prevPoint = void 0;

            for (var i = 1; i < max; i++) {
                p1 = pointList[i - 1];
                p2 = pointList[i];
                cp = p1.interpolate(p2, 0.5);
                if (prevPoint) {
                    path.push(this.curveTo(p1, cp));
                } else {
                    path.push(this.moveTo(p1));
                    path.push(this.lineTo(cp));
                }
                prevPoint = cp;
            }
            path.push(this.lineTo(this.points[max - 1]));
            return Snap.parsePathString(path);
        }
    }]);

    return SVGCurveComponent;
}(WVPointComponent);

WVPropertyManager.attach_default_component_infos(SVGCurveComponent, {
    "info": {
        "componentName": "SVGCurveComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "startMarker": false,
        "endMarker": false,
        "pointCount": 3
    },

    "style": {
        "fill": "none",
        "stroke": "#AAffEE",
        "stroke-width": 1,
        "stroke-dasharray": "0"
    }

});

SVGCurveComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    label: "Stroke",
    template: "vertical",
    children: [{
        owner: "style",
        name: "stroke",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "Stroke Color"
    }, {
        owner: "style",
        name: "stroke-width",
        type: "number",
        label: "width",
        show: true,
        writable: true,
        description: "Stroke Width"
    }, {
        owner: "style",
        name: "stroke-dasharray",
        type: "select",
        label: "style",
        show: true,
        writable: true,
        description: "border style",
        options: {
            items: [{ label: "solid", value: "0" }, { label: "dashed", value: "5 5" }]
        }
    }]
}, {
    label: "Arrow",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "startMarker",
        type: "checkbox",
        label: "start",
        show: true,
        writable: true,
        description: "Start Arrow Icon"
    }, {
        owner: "setter",
        name: "endMarker",
        type: "checkbox",
        label: "end",
        show: true,
        writable: true,
        description: "Start Arrow Icon"
    }]
}, {

    label: "Point",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "pointCount",
        type: "number",
        label: "length",
        show: true,
        writable: true,
        description: "point"
    }]

}];

WVPropertyManager.remove_property_group_info(SVGCurveComponent, "label");
WVPropertyManager.remove_property_group_info(SVGCurveComponent, "background");



WVPropertyManager.add_property_group_info(SVGCurveComponent, {
    label: "SVGCurveComponent 고유 속성",
    children: [{
        name: "pointCount",
        type: "number",
        show: true,
        writable: true,
        description: "라인을 그릴 때 사용할 포인트 수 입니다."
    }, {
        name: "starMarker",
        type: "boolean",
        show: true,
        writable: true,
        description: "라인 시작점에 화살표 마커 사용여부를 설정합니다."
    }, {
        name: "endMarker",
        type: "boolean",
        show: true,
        writable: true,
        description: "라인 끝점에 화살표 마커 사용여부를 설정합니다."
    }, {
        name: "stroke",
        type: "string",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
    }, {
        name: "stroke-width",
        type: "number",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 두께를 설정합니다."
    }]
});