class SVGLineComponent extends WVPointComponent {

    constructor() {
        super();
    }

    get paths() {
        let path = [];
        let pointList = this.points.concat();
        let currentPoint = pointList.shift();
        path.push(this.moveTo(currentPoint));
        for (let i = 0; i < pointList.length; i++) {
            path.push(this.lineTo(pointList[i]));
        }
        return Snap.parsePathString(path);
    }

    _createDefaultPoints() {
        return [new Point(this.x, this.y), new Point(this.x + this.width, this.y + this.height)];
    }

    _onCreateElement() {
        this._element = CPUtil.getInstance().createSVGElement("path");
        this.appendElement.appendChild(this.element);
    }


    handleTransformList() {
        let results = [];
        let idx;
        for (idx = 0; idx < this.points.length; idx++) {
            results.push("point-anchor");
        }
        return results;
    }


    _notifyInserPoint(point) {
        this.notifyComponentEvent(new WVPointComponentEvent(
            WVPointComponentEvent.INSERT_POINT, this, {
                point: point,
                type: "point-anchor"
            }
        ));
    }

    _notifyRemovePoint(point) {
        this.notifyComponentEvent(new WVPointComponentEvent(
            WVPointComponentEvent.REMOVE_POINT, this, {
                point: point
            }
        ));
    }

    _onDestroy() {
        super._onDestroy();
    }
}

WVPropertyManager.attach_default_component_infos(SVGLineComponent, {
    "info": {
        "componentName": "SVGLineComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 100,
        "height": 100,
        "startMarker": false,
        "endMarker": false,
        "pointCount": 2
    },
    "style": {
        "fill": "none",
        "stroke": "#ff00ff",
        "stroke-width": 1,
        "stroke-dasharray": "0"
    }
});

WVPropertyManager.add_property_group_info(SVGLineComponent, {
    label: "SVGLineComponent 고유 속성",
    children: [{
        name: "pointCount",
        type: "number",
        show: true,
        writable: true,
        description: "라인을 그릴 때 사용할 포인트 수 입니다."
    }, {
        name: "starMarker",
        type: "boolean",
        show: true,
        writable: true,
        description: "라인 시작점에 화살표 마커 사용여부를 설정합니다."
    }, {
        name: "endMarker",
        type: "boolean",
        show: true,
        writable: true,
        description: "라인 끝점에 화살표 마커 사용여부를 설정합니다."
    }, {
        name: "stroke",
        type: "string",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
    }, {
        name: "stroke-width",
        type: "number",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 두께를 설정합니다."
    }]
});

SVGLineComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },

    {
        label: "Stroke",
        template: "vertical",
        children: [{
            owner: "style",
            name: "stroke",
            type: "color",
            label: "color",
            show: true,
            writable: true,
            description: "Stroke Color"
        }, {
            owner: "style",
            name: "stroke-width",
            type: "number",
            label: "width",
            show: true,
            writable: true,
            description: "Stroke Width"
        }, {
            owner: "style",
            name: "stroke-dasharray",
            type: "select",
            label: "style",
            show: true,
            writable: true,
            description: "border style",
            options: {
                items: [
                    { label: "solid", value: "0" },
                    { label: "dashed", value: "5 5" },
                ]
            }
        }]
    }, {
        label: "Arrow",
        template: "vertical",
        children: [{
                owner: "setter",
                name: "startMarker",
                type: "checkbox",
                label: "start",
                show: true,
                writable: true,
                description: "Start Arrow Icon"
            },
            {
                owner: "setter",
                name: "endMarker",
                type: "checkbox",
                label: "end",
                show: true,
                writable: true,
                description: "Start Arrow Icon"
            }
        ]
    }, {
        label: "Point",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "length",
            show: true,
            writable: true,
            description: "point"
        }]

    }
]

WVPropertyManager.remove_property_group_info(SVGLineComponent, "label");
WVPropertyManager.remove_property_group_info(SVGLineComponent, "background");