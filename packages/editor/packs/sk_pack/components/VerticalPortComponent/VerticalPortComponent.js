class VerticalPortComponent extends WVDOMComponent {
	constructor() {
		super();
		console.log("VerticalPortComponent 시작");
		this.$portFromCircle = null;
		this.$portLine = null;
		this.$portToCircle = null;
		this.$portFromText = null;
		this.$portToText = null;
	}

	_onCreateElement() {
		this.$portLine = $(`<span class="port-line"></span>`);
		this.$portFromCircle = $(`<span class="port-from-circle"></span>`);
		this.$portToCircle = $(`<span class="port-to-circle"></span>`);
		this.$portFromText = $(`<span class="port-from-text">FromText</span>`);
		this.$portToText = $(`<span class="port-to-text">ToText</span>`);

		$(this._element).append(this.$portLine);
		$(this._element).append(this.$portFromCircle);
		$(this._element).append(this.$portToCircle);
		$(this._element).append(this.$portFromText);
		$(this._element).append(this.$portToText);
	}

	_onCommitProperties() {

		if (this._updatePropertiesMap.has("portInfo")) {
			this.validateCallLater(this._validatePortInfo);
			this.validateCallLater(this._validateSizeProperty);
		}

		if (this.invalidateSize) {
			this.validateCallLater(this._validateSizeProperty);
		}
	}

	// _onCommitProperties() {
	//     if (this._updatePropertiesMap.has("portInfo")) { //1
	//         this.changeOffset(this.getGroupProperties("label").label_position);
	//     }
	//
	// }

	_validatePortInfo() {
		let portInfoProperties = this.getGroupProperties("portInfo");
		let borderStyleValue = portInfoProperties.lineStyle;

		this.$portLine.css({
			borderStyle: borderStyleValue
		})

		this.$portFromText.text(portInfoProperties.fromText);
		this.$portToText.text(portInfoProperties.toText);
	}

	_changeCssWithJquery(instance, property, value) {
		instance.css(property, value);
	}

	_validateSizeProperty() {
		const CIRCLE_SIZE_HALF = (this.$portFromCircle.width() / 2);
		let centerValue = (this.width / 2); // 가운데 값 구함

		// 센터 값에서 서클 값 의 반만큼 이동한다.
		this._changeCssWithJquery(this.$portFromCircle, "left", centerValue - CIRCLE_SIZE_HALF);
		this._changeCssWithJquery(this.$portFromCircle, "top", 0);

		this._changeCssWithJquery(this.$portToCircle, "left", centerValue - CIRCLE_SIZE_HALF);
		this._changeCssWithJquery(this.$portToCircle, "bottom", 0);

		this._changeCssWithJquery(this.$portLine, "left", centerValue - 1);
		this._changeCssWithJquery(this.$portLine, "height", this.height - 1);

		this._directionCheck();
		this._positionCheck();
	}

	_validateMarginInfo() {
		let textTopMargin = this.getGroupPropertyValue("portInfo", "textTopMargin");

		this._changeCssWithJquery(this.$portFromText, "bottom", "");
		this._changeCssWithJquery(this.$portFromText, "top", textTopMargin);

		let textBottomMargin = this.getGroupPropertyValue("portInfo", "textBottomMargin");
		this._changeCssWithJquery(this.$portToText, "top", "");
		this._changeCssWithJquery(this.$portToText, "bottom", textBottomMargin);
	}

	_directionCheck() {
		let directionValue = this.getGroupPropertyValue("portInfo", "direction");
		let $from = null;
		let $to = null;

		if (directionValue === "forward") {
			$from = this.$portFromText;
			$to = this.$portToText;
		} else {
			$from = this.$portToText;
			$to = this.$portFromText;
		}
		// 값 초기화
		let textTopMargin = this.getGroupPropertyValue("portInfo", "textTopMargin");
		let textBottomMargin = this.getGroupPropertyValue("portInfo", "textBottomMargin");

		// 값 초기화
		this._changeCssWithJquery($from, "bottom", "");
		this._changeCssWithJquery($from, "top", textTopMargin);

		this._changeCssWithJquery($to, "top", "");
		this._changeCssWithJquery($to, "bottom", textBottomMargin);
	}

	_positionCheck() {
		let centerValue = (this.width / 2); // 가운데 값 구함
		let postionValue = this.getGroupPropertyValue("portInfo", "position");
		let fromPositionValue = 0;
		let toPositionValue = 0;

		if (postionValue === "right") {
			fromPositionValue = centerValue + 8;
			toPositionValue = centerValue + 8;
		} else {
			let fromTextLengthValue = this.getGroupPropertyValue("portInfo", "fromText");
			let toTextLengthValue = this.getGroupPropertyValue("portInfo", "toText");

			let fromSize = this.getLabelSize(fromTextLengthValue, 11);
			let toSize = this.getLabelSize(toTextLengthValue, 11);

			fromPositionValue = centerValue - fromSize.width - 10;
			toPositionValue = centerValue - toSize.width - 10;
		}

		this._changeCssWithJquery(this.$portFromText, "left", fromPositionValue);
		this._changeCssWithJquery(this.$portToText, "left", toPositionValue);

	}

	_onImmediateUpdateDisplay() {
		this._validatePortInfo();
		this._validateSizeProperty();
		this._validateMarginInfo();
		this._directionCheck();
	}

	_onDestroy() {
		super._onDestroy();
	}

}

WVPropertyManager.attach_default_component_infos(VerticalPortComponent, {
	"info": {
		"componentName": "VerticalPortComponent",
		"version": "1.0.0"
	},
	"setter": {
		"width": 100,
		"height": 100,
		"min-width": 50,
		"min-height": 50
	},
	"style": {
		"cursor": "default",
		"min-width": "50px",
		"min-height": "50px"
	},
	"portInfo": {
		"direction": "reverse",
		"position": "right",
		"lineStyle": "solid",
		"fromText": "from",
		"fromHost": "fromHost",
		"fromIP":"fromIP",
		"toText": "to",
		"toHost": "toHost",
		"toIP": "toIP",
		"textTopMargin": 0,
		"textBottomMargin": 0
	}
});

VerticalPortComponent.property_panel_info = [{
	template: "primary"
}, {
	template: "pos-size-2d"
}, {
	template: "cursor"
}, {
	template: "label"
}, {
	template: "background"
}, {
	template: "border"
}, {
	label: "Port Infomation",
	template: "vertical",
	children: [{
		owner: "portInfo",
		name: "position",
		type: "select",
		label: "position",
		show: true,
		writable: true,
		description: "text 위치",
		options: {
			items: [
				{label: "left", value: "left"},
				{label: "right", value: "right"},
			]
		}
	}, {
		owner: "portInfo",
		name: "direction",
		type: "select",
		label: "direction",
		show: true,
		writable: true,
		description: "from to 방향",
		options: {
			items: [
				{label: "forward", value: "forward"},
				{label: "reverse", value: "reverse"},
			]
		}
	}, {
		owner: "portInfo",
		name: "lineStyle",
		type: "select",
		label: "line style",
		show: true,
		writable: true,
		description: "라인 스타일",
		options: {
			items: [
				{label: "solid", value: "solid"},
				{label: "dashed", value: "dashed"},
			]
		}
	}, {
		owner: "portInfo",
		name: "fromHost",
		type: "string",
		label: "fromHost",
		show: true,
		writable: false,
		description: "from HostName"
	}, {
		owner: "portInfo",
		name: "fromIP",
		type: "string",
		label: "fromIP",
		show: true,
		writable: false,
		description: "from IP"
	}, {
		owner: "portInfo",
		name: "fromText",
		type: "string",
		label: "fromPort",
		show: true,
		writable: true,
		description: "from Port"
	}, {
		owner: "portInfo",
		name: "toHost",
		type: "string",
		label: "toHost",
		show: true,
		writable: false,
		description: "to HostName"
	}, {
		owner: "portInfo",
		name: "toIP",
		type: "string",
		label: "toIP",
		show: true,
		writable: false,
		description: "to IP"
	}, {
		owner: "portInfo",
		name: "toText",
		type: "string",
		label: "toPort",
		show: true,
		writable: true,
		description: "to Port"
	}, {
		owner: "portInfo",
		name: "textTopMargin",
		type: "number",
		label: "top margin",
		show: true,
		writable: true,
		description: "from value Text"
	}, {
		owner: "portInfo",
		name: "textBottomMargin",
		type: "number",
		label: "bottom margin",
		show: true,
		writable: true,
		description: "to value Text"
	}]
}];
