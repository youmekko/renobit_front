class HorizontalPortComponent extends WVDOMComponent {
	constructor() {
		super();
		console.log("HorizontalPortComponent 시작");
		this.$portFromCircle = null;
		this.$portLine = null;
		this.$portToCircle = null;
		this.$portFromText = null;
		this.$portToText = null;
	}

	_onCreateElement() {
		this.$portLine = $(`<span class="port-line"></span>`);
		this.$portFromCircle = $(`<span class="port-from-circle"></span>`);
		this.$portToCircle = $(`<span class="port-to-circle"></span>`);
		this.$portFromText = $(`<span class="port-from-text">FromText</span>`);
		this.$portToText = $(`<span class="port-to-text">ToText</span>`);

		$(this._element).append(this.$portLine);
		$(this._element).append(this.$portFromCircle);
		$(this._element).append(this.$portToCircle);
		$(this._element).append(this.$portFromText);
		$(this._element).append(this.$portToText);
	}

	_onCommitProperties() {
		if (this._updatePropertiesMap.has("portInfo")) {
			this.validateCallLater(this._validatePortInfo);
			this.validateCallLater(this._validateSizeProperty);
		}

		if (this.invalidateSize) {
			this.validateCallLater(this._validateSizeProperty);
		}
	}

	_validatePortInfo() {
		let portInfoProperties = this.getGroupProperties("portInfo");
		let borderStyleValue = portInfoProperties.lineStyle;

		this.$portLine.css({
			borderStyle: borderStyleValue
		})

		this.$portFromText.text(portInfoProperties.fromText);
		this.$portToText.text(portInfoProperties.toText);
	}

	_changeCssWithJquery(instance, property, value) {
		instance.css(property, value);
	}

	_validateSizeProperty() {
		const CIRCLE_SIZE_HALF = (this.$portFromCircle.width() / 2);
		let centerValue = (this.height / 2); // 가운데 값 구함

		// 센터 값에서 서클 값 의 반만큼 이동한다.
		this._changeCssWithJquery(this.$portFromCircle, "top", centerValue - CIRCLE_SIZE_HALF);
		this._changeCssWithJquery(this.$portFromCircle, "left", 0);

		this._changeCssWithJquery(this.$portToCircle, "top", centerValue - CIRCLE_SIZE_HALF);
		this._changeCssWithJquery(this.$portToCircle, "right", 0);

		this._changeCssWithJquery(this.$portLine, "top", centerValue - 1);
		this._changeCssWithJquery(this.$portLine, "width", this.width - 1);

		this._directionCheck();
		this._positionCheck();
	}

	_validateMarginInfo() {
		let textLeftMargin = this.getGroupPropertyValue("portInfo", "textLeftMargin");

		this._changeCssWithJquery(this.$portFromText, "right", "");
		this._changeCssWithJquery(this.$portFromText, "left", textLeftMargin);

		let textRightMargin = this.getGroupPropertyValue("portInfo", "textRightMargin");
		this._changeCssWithJquery(this.$portToText, "left", "");
		this._changeCssWithJquery(this.$portToText, "right", textRightMargin);
	}

	_directionCheck() {
		let directionValue = this.getGroupPropertyValue("portInfo", "direction");
		let $from = null;
		let $to = null;

		if (directionValue === "forward") {
			$from = this.$portFromText;
			$to = this.$portToText;
		} else {
			$from = this.$portToText;
			$to = this.$portFromText;
		}

		// 값 초기화
		let textLeftMargin = this.getGroupPropertyValue("portInfo", "textLeftMargin");
		let textRightMargin = this.getGroupPropertyValue("portInfo", "textRightMargin");

		this._changeCssWithJquery($from, "right", "");
		this._changeCssWithJquery($from, "left", textLeftMargin);

		this._changeCssWithJquery($to, "left", "");
		this._changeCssWithJquery($to, "right", textRightMargin);
	}

	_positionCheck() {
		let centerValue = (this.height / 2); // 가운데 값 구함
		let postionValue = this.getGroupPropertyValue("portInfo", "position");
		let fromPositionValue = 0;
		let toPositionValue = 0;

		if (postionValue === "bottom") {
			fromPositionValue = centerValue + 8;
			toPositionValue = centerValue + 8;
		} else {
			fromPositionValue = centerValue - 18;
			toPositionValue = centerValue - 18;
		}

		this._changeCssWithJquery(this.$portFromText, "top", fromPositionValue);
		this._changeCssWithJquery(this.$portToText, "top", toPositionValue);
	}

	_onImmediateUpdateDisplay() {
		this._validatePortInfo();
		this._validateSizeProperty();
		this._validateMarginInfo();
		this._directionCheck();
	}

	_onDestroy() {
		super._onDestroy();
	}
}

WVPropertyManager.attach_default_component_infos(HorizontalPortComponent, {
	"info": {
		"componentName": "HorizontalPortComponent",
		"version": "1.0.0"
	},
	"setter": {
		"width": 140,
		"height": 100,
		"min-width": 50,
		"min-height": 50
	},
	"style": {
		"cursor": "default",
		"min-width": "50px",
		"min-height": "50px"
	},
	"portInfo": {
		"direction": "forward",
		"position": "bottom",
		"lineStyle": "solid",
		"fromText": "from",
		"fromHost": "fromHost",
		"fromIP": "fromIP",
		"toText": "to",
		"toHost": "toHost",
		"toIP": "toIP",
		"textLeftMargin": 0,
		"textRightMargin": 0
	}
});

HorizontalPortComponent.property_panel_info = [{
	template: "primary"
}, {
	template: "pos-size-2d"
}, {
	template: "cursor"
}, {
	template: "label"
}, {
	template: "background"
}, {
	template: "border"
}, {
	label: "Port Infomation",
	template: "vertical",
	children: [{
		owner: "portInfo",
		name: "position",
		type: "select",
		label: "position",
		show: true,
		writable: true,
		description: "방향 값",
		options: {
			items: [
				{label: "up", value: "up"},
				{label: "bottom", value: "bottom"},
			]
		}
	}, {
		owner: "portInfo",
		name: "direction",
		type: "select",
		label: "direction",
		show: true,
		writable: true,
		description: "from to 방향",
		options: {
			items: [
				{label: "forward", value: "forward"},
				{label: "reverse", value: "reverse"},
			]
		}
	}, {
		owner: "portInfo",
		name: "lineStyle",
		type: "select",
		label: "line style",
		show: true,
		writable: true,
		description: "라인 스타일",
		options: {
			items: [
				{label: "solid", value: "solid"},
				{label: "dashed", value: "dashed"},
			]
		}
	},{
		owner: "portInfo",
		name: "fromHost",
		type: "string",
		label: "fromHost",
		show: true,
		writable: false,
		description: "from HostName"
	}, {
		owner: "portInfo",
		name: "fromIP",
		type: "string",
		label: "fromIP",
		show: true,
		writable: false,
		description: "from IP"
	}, {
		owner: "portInfo",
		name: "fromText",
		type: "string",
		label: "fromPort",
		show: true,
		writable: true,
		description: "from Port"
	}, {
		owner: "portInfo",
		name: "toHost",
		type: "string",
		label: "toHost",
		show: true,
		writable: false,
		description: "to HostName"
	}, {
		owner: "portInfo",
		name: "toIP",
		type: "string",
		label: "toIP",
		show: true,
		writable: false,
		description: "to IP"
	}, {
		owner: "portInfo",
		name: "toText",
		type: "string",
		label: "toPort",
		show: true,
		writable: true,
		description: "to Port"
	}, {
		owner: "portInfo",
		name: "textLeftMargin",
		type: "number",
		label: "left margin",
		show: true,
		writable: true,
		description: "From Left Margin"
	}, {
		owner: "portInfo",
		name: "textRightMargin",
		type: "number",
		label: "right margin",
		show: true,
		writable: true,
		description: "To right Margin"
	}]
}];
