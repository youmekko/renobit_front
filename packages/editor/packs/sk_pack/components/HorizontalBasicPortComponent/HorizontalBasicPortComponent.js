class HorizontalBasicPortComponent extends WVDOMComponent {
	constructor() {
		super();
		console.log("HorizontalBasicPortComponent 시작");
		this.$portFromCircle = null;
		this.$portLine = null;
		this.$portToCircle = null;
		this.$portFromText = null;
		this.$portToText = null;
	}

	_onCreateElement() {
		this.$portLine = $(`<span class="port-line"></span>`);
		this.$portFromCircle = $(`<span class="port-from-circle"></span>`);
		this.$portToCircle = $(`<span class="port-to-circle"></span>`);
		this.$portFromText = $(`<span class="port-from-text">FromText</span>`);
		this.$portToText = $(`<span class="port-to-text">ToText</span>`);

		$(this._element).append(this.$portLine);
		$(this._element).append(this.$portFromCircle);
		$(this._element).append(this.$portToCircle);
		// $(this._element).append(this.$portFromText);
		// $(this._element).append(this.$portToText);
	}

	_onCommitProperties() {
		if (this._updatePropertiesMap.has("portInfo")) {
			this.validateCallLater(this._validatePortInfo);
			this.validateCallLater(this._validateSizeProperty);
		}

		if (this.invalidateSize) {
			this.validateCallLater(this._validateSizeProperty);
		}
	}

	_validatePortInfo() {
		let portInfoProperties = this.getGroupProperties("portInfo");
		let borderStyleValue = portInfoProperties.lineStyle;

		this.$portLine.css({
			borderStyle: borderStyleValue
		})

		this.$portFromText.text(portInfoProperties.fromText);
		this.$portToText.text(portInfoProperties.toText);
	}

	_changeCssWithJquery(instance, property, value) {
		instance.css(property, value);
	}

	_validateSizeProperty() {
		const CIRCLE_SIZE_HALF = (this.$portFromCircle.width() / 2);
		let centerValue = (this.height / 2); // 가운데 값 구함

		// 센터 값에서 서클 값 의 반만큼 이동한다.
		this._changeCssWithJquery(this.$portFromCircle, "top", centerValue - CIRCLE_SIZE_HALF);
		this._changeCssWithJquery(this.$portFromCircle, "left", 0);

		this._changeCssWithJquery(this.$portToCircle, "top", centerValue - CIRCLE_SIZE_HALF);
		this._changeCssWithJquery(this.$portToCircle, "right", 0);

		this._changeCssWithJquery(this.$portLine, "top", centerValue - 1);
		this._changeCssWithJquery(this.$portLine, "width", this.width - 1);

		this._directionCheck();
		this._positionCheck();
	}

	_validateMarginInfo() {
		let textLeftMargin = this.getGroupPropertyValue("portInfo", "textLeftMargin");

		this._changeCssWithJquery(this.$portFromText, "right", "");
		this._changeCssWithJquery(this.$portFromText, "left", textLeftMargin);

		let textRightMargin = this.getGroupPropertyValue("portInfo", "textRightMargin");
		this._changeCssWithJquery(this.$portToText, "left", "");
		this._changeCssWithJquery(this.$portToText, "right", textRightMargin);
	}

	_directionCheck() {
		let directionValue = this.getGroupPropertyValue("portInfo", "direction");
		let $from = null;
		let $to = null;

		if (directionValue === "forward") {
			$from = this.$portFromText;
			$to = this.$portToText;
		} else {
			$from = this.$portToText;
			$to = this.$portFromText;
		}

		// 값 초기화
		let textLeftMargin = this.getGroupPropertyValue("portInfo", "textLeftMargin");
		let textRightMargin = this.getGroupPropertyValue("portInfo", "textRightMargin");

		this._changeCssWithJquery($from, "right", "");
		this._changeCssWithJquery($from, "left", textLeftMargin);

		this._changeCssWithJquery($to, "left", "");
		this._changeCssWithJquery($to, "right", textRightMargin);
	}

	_positionCheck() {
		let centerValue = (this.height / 2); // 가운데 값 구함
		let postionValue = this.getGroupPropertyValue("portInfo", "position");
		let fromPositionValue = 0;
		let toPositionValue = 0;

		if (postionValue === "bottom") {
			fromPositionValue = centerValue + 8;
			toPositionValue = centerValue + 8;
		} else {
			fromPositionValue = centerValue - 18;
			toPositionValue = centerValue - 18;
		}

		this._changeCssWithJquery(this.$portFromText, "top", fromPositionValue);
		this._changeCssWithJquery(this.$portToText, "top", toPositionValue);
	}

	_onImmediateUpdateDisplay() {
		this._validatePortInfo();
		this._validateSizeProperty();
		this._validateMarginInfo();
		this._directionCheck();
	}

	_onDestroy() {
		super._onDestroy();
	}
}

WVPropertyManager.attach_default_component_infos(HorizontalBasicPortComponent, {
	"info": {
		"componentName": "HorizontalBasicPortComponent",
		"version": "1.0.0"
	},
	"setter": {
		"width": 140,
		"height": 100,
		"min-width": 20,
		"min-height": 20
	},
	"style": {
		"cursor": "default",
		"min-width": "20px",
		"min-height": "20px"
	},
	"portInfo": {
		"lineStyle": "solid"
	}
});

HorizontalBasicPortComponent.property_panel_info = [{
	template: "primary"
}, {
	template: "pos-size-2d"
}, {
	template: "cursor"
}, {
	template: "label"
}, {
	template: "background"
}, {
	template: "border"
}];
