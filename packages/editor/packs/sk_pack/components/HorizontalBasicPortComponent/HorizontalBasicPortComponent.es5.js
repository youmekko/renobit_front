"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HorizontalBasicPortComponent = function (_WVDOMComponent) {
	_inherits(HorizontalBasicPortComponent, _WVDOMComponent);

	function HorizontalBasicPortComponent() {
		_classCallCheck(this, HorizontalBasicPortComponent);

		var _this = _possibleConstructorReturn(this, (HorizontalBasicPortComponent.__proto__ || Object.getPrototypeOf(HorizontalBasicPortComponent)).call(this));

		console.log("HorizontalBasicPortComponent 시작");
		_this.$portFromCircle = null;
		_this.$portLine = null;
		_this.$portToCircle = null;
		_this.$portFromText = null;
		_this.$portToText = null;
		return _this;
	}

	_createClass(HorizontalBasicPortComponent, [{
		key: "_onCreateElement",
		value: function _onCreateElement() {
			this.$portLine = $("<span class=\"port-line\"></span>");
			this.$portFromCircle = $("<span class=\"port-from-circle\"></span>");
			this.$portToCircle = $("<span class=\"port-to-circle\"></span>");
			this.$portFromText = $("<span class=\"port-from-text\">FromText</span>");
			this.$portToText = $("<span class=\"port-to-text\">ToText</span>");

			$(this._element).append(this.$portLine);
			$(this._element).append(this.$portFromCircle);
			$(this._element).append(this.$portToCircle);
			// $(this._element).append(this.$portFromText);
			// $(this._element).append(this.$portToText);
		}
	}, {
		key: "_onCommitProperties",
		value: function _onCommitProperties() {
			if (this._updatePropertiesMap.has("portInfo")) {
				this.validateCallLater(this._validatePortInfo);
				this.validateCallLater(this._validateSizeProperty);
			}

			if (this.invalidateSize) {
				this.validateCallLater(this._validateSizeProperty);
			}
		}
	}, {
		key: "_validatePortInfo",
		value: function _validatePortInfo() {
			var portInfoProperties = this.getGroupProperties("portInfo");
			var borderStyleValue = portInfoProperties.lineStyle;

			this.$portLine.css({
				borderStyle: borderStyleValue
			});

			this.$portFromText.text(portInfoProperties.fromText);
			this.$portToText.text(portInfoProperties.toText);
		}
	}, {
		key: "_changeCssWithJquery",
		value: function _changeCssWithJquery(instance, property, value) {
			instance.css(property, value);
		}
	}, {
		key: "_validateSizeProperty",
		value: function _validateSizeProperty() {
			var CIRCLE_SIZE_HALF = this.$portFromCircle.width() / 2;
			var centerValue = this.height / 2; // 가운데 값 구함

			// 센터 값에서 서클 값 의 반만큼 이동한다.
			this._changeCssWithJquery(this.$portFromCircle, "top", centerValue - CIRCLE_SIZE_HALF);
			this._changeCssWithJquery(this.$portFromCircle, "left", 0);

			this._changeCssWithJquery(this.$portToCircle, "top", centerValue - CIRCLE_SIZE_HALF);
			this._changeCssWithJquery(this.$portToCircle, "right", 0);

			this._changeCssWithJquery(this.$portLine, "top", centerValue - 1);
			this._changeCssWithJquery(this.$portLine, "width", this.width - 1);

			this._directionCheck();
			this._positionCheck();
		}
	}, {
		key: "_validateMarginInfo",
		value: function _validateMarginInfo() {
			var textLeftMargin = this.getGroupPropertyValue("portInfo", "textLeftMargin");

			this._changeCssWithJquery(this.$portFromText, "right", "");
			this._changeCssWithJquery(this.$portFromText, "left", textLeftMargin);

			var textRightMargin = this.getGroupPropertyValue("portInfo", "textRightMargin");
			this._changeCssWithJquery(this.$portToText, "left", "");
			this._changeCssWithJquery(this.$portToText, "right", textRightMargin);
		}
	}, {
		key: "_directionCheck",
		value: function _directionCheck() {
			var directionValue = this.getGroupPropertyValue("portInfo", "direction");
			var $from = null;
			var $to = null;

			if (directionValue === "forward") {
				$from = this.$portFromText;
				$to = this.$portToText;
			} else {
				$from = this.$portToText;
				$to = this.$portFromText;
			}

			// 값 초기화
			var textLeftMargin = this.getGroupPropertyValue("portInfo", "textLeftMargin");
			var textRightMargin = this.getGroupPropertyValue("portInfo", "textRightMargin");

			this._changeCssWithJquery($from, "right", "");
			this._changeCssWithJquery($from, "left", textLeftMargin);

			this._changeCssWithJquery($to, "left", "");
			this._changeCssWithJquery($to, "right", textRightMargin);
		}
	}, {
		key: "_positionCheck",
		value: function _positionCheck() {
			var centerValue = this.height / 2; // 가운데 값 구함
			var postionValue = this.getGroupPropertyValue("portInfo", "position");
			var fromPositionValue = 0;
			var toPositionValue = 0;

			if (postionValue === "bottom") {
				fromPositionValue = centerValue + 8;
				toPositionValue = centerValue + 8;
			} else {
				fromPositionValue = centerValue - 18;
				toPositionValue = centerValue - 18;
			}

			this._changeCssWithJquery(this.$portFromText, "top", fromPositionValue);
			this._changeCssWithJquery(this.$portToText, "top", toPositionValue);
		}
	}, {
		key: "_onImmediateUpdateDisplay",
		value: function _onImmediateUpdateDisplay() {
			this._validatePortInfo();
			this._validateSizeProperty();
			this._validateMarginInfo();
			this._directionCheck();
		}
	}, {
		key: "_onDestroy",
		value: function _onDestroy() {
			_get(HorizontalBasicPortComponent.prototype.__proto__ || Object.getPrototypeOf(HorizontalBasicPortComponent.prototype), "_onDestroy", this).call(this);
		}
	}]);

	return HorizontalBasicPortComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(HorizontalBasicPortComponent, {
	"info": {
		"componentName": "HorizontalBasicPortComponent",
		"version": "1.0.0"
	},
	"setter": {
		"width": 140,
		"height": 100,
		"min-width": 20,
		"min-height": 20
	},
	"style": {
		"cursor": "default",
		"min-width": "20px",
		"min-height": "20px"
	},
	"portInfo": {
		"lineStyle": "solid"
	}
});

HorizontalBasicPortComponent.property_panel_info = [{
	template: "primary"
}, {
	template: "pos-size-2d"
}, {
	template: "cursor"
}, {
	template: "label"
}, {
	template: "background"
}, {
	template: "border"
}];