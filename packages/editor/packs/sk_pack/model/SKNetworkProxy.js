class SKNetworkProxy {
      static get ROW_SHEET_NAME() {
            return "구성정보";
      }

      static get PORT_INFO_SHEET_NAME() {
            return "포트정보";
      }

      static get START_DATA_ROW() {
            return 4;
      }

      constructor() {
            this._savePageName = "";
            this._templateTypeName = "";

            this._wholeElementInfoMap = new Map();
            this._wholeElementInfoList = [];

            //  헤더 정보만 생성하기
            this._headerElementInfoMap = new Map();
            this._headerElementInfoList = [];

            this._bodyElementInfoList = [];
            this._badElementInfoList = [];

            this._wholePortInfoList = [];

            this._headerPortInfoList = [];
            this._headerPortInfoLinkList = [];
            this._bodyPortInfoList = [];
            this._badPortInfoList = [];
            this._isNewPage = true;
      }


      async createPageToFile(file, isNewPage = true) {
            try {
                  // 파싱 정보 가져오기
                  let workbook = await this._parseExcelFile(file);
                  await this.createPageToWorkbook(workbook, isNewPage)
            } catch (error) {
                  console.log("error ", error);
            }
      }


      async _parseExcelFile(file) {
            let reader = new FileReader();

            if (reader == null) {
                  alert("FileReader를 지원하지 않는 브라우저입니다.");
                  return;
            }

            let fileName = file.name;

            return new Promise((resolve, reject) => {
                  reader.onload = (e) => {
                        let data = null;
                        /*
				    e.target.result가 있는 경우는 기본
				    content가 있는 경우 IE11 override한 readAsBinaryString()이 실행되는 경우임.
				*/
                        if (e.target.result)
                              data = e.target.result;
                        else
                              data = e.target.content;

                        // 이진 파일 유무 및 sheets 전조 유무 확인
                        let workbook = XLSX.read(data, {type: 'binary'});
                        if (workbook == null || workbook.hasOwnProperty("Sheets") == false) {
                              reject(new Error("엑셀 파일이 정사적이지 않습니다. 확인 후 다시 실행해주세요."));
                              return;
                        }

                        resolve(workbook);


                  }; //end onload

                  reader.readAsBinaryString(file);
            });
      }


      /*
      1. 정보 리셋
      2. 저장할 페이지명과 상요할 템플릿 타입 정보 생서하기
      3. elementInfo, 정보 생성하기
      4. portInfo 정보 생성하기
      5. 사용할 템플릿 페이지 명 생성
      6. 실제 페이지 생성하기

       */
      async createPageToWorkbook(workbook, isNewPage = true) {
            this._resetInfos();

            this._isNewPage = isNewPage;


            this._createSheet(workbook);
            this._attachPageNameAndTemplateTypeName();

            // element 정보 생성하기
            this._createElementInfoList();

            // 포트 정보 생성하기
            this._createPortInfoList();
	      this._bodyPortInfoListAddType();
            // 포트 정보에 gap 정보 투이기
            this._attachPortInfoGap();

            // 템플릿 페이지 명 선택
            // this._attachTemplatePageName();
            this._showInfo();

            // 편집 정보 생성하기
           this._executeCreatePageEditData();
      }


      /*
      데이터 생성에 필요한  sheet 정보 생성하기
       */
      _createSheet(workbook) {
            // 구성정보 sheet 존재 유무 확인.
            this._elementSheet = workbook.Sheets[SKNetworkProxy.ROW_SHEET_NAME];
            this._elementFormulate = XLSX.utils.sheet_to_formulae(this._elementSheet, {range: 1});

            // 구성정보 sheet 존재 유무 확인.
            this._portSheet = workbook.Sheets[SKNetworkProxy.PORT_INFO_SHEET_NAME];
            this._portFormulate = XLSX.utils.sheet_to_formulae(this._portSheet, {range: 1});
      }


      /*
      생성할 페이지 이름과, 템플릿 타입 생ㅅ어하기
       */

      _attachPageNameAndTemplateTypeName() {
            // 생성할 페이지 정보
            this._savePageName = this._getCellData(this._elementFormulate, "B1");
            // 템플릿 이름 구하기
            let tempPageType = this._getCellData(this._elementFormulate, "B2");
            // 가로/세로 템플릿 타입명 구하기
            this._templateTypeName = this._getTemplateTypeName(tempPageType);
      }

      ////////////////////////////////////////////////////////////////


      ////////////////////////////////////////////////////////////////

      /*
		전체 데이터 _wholeElementInfoList
		헤더 정보 : _headerElementInfoMap
		body 정보 : _bodyElementInfoList
	 */
      _createElementInfoList() {

            // // row정보 출력
            let headerInfo = Object.values(SKNetworkProxy.ExcelHeader);
            // 원본 데이터 (헤더와 body가 모두 포함되어 있는 정보
            let rowElementInfoList = XLSX.utils.sheet_to_json(this._elementSheet, {
                  range: SKNetworkProxy.START_DATA_ROW,
                  raw: true,
                  header: headerInfo
            });

            this._wholeElementInfoList = rowElementInfoList.map((rowElementInfo) => {
                  return new ElementInfoVO(rowElementInfo);
            })


            let tempMap = new Map();

            // 타입과 column 생성하기
            this._wholeElementInfoList.forEach((elementInfo) => {
                  let row = elementInfo.row;
                  elementInfo.type = row == "A" ? "body" : "header";
                  elementInfo.bad = false;
                  elementInfo.column = 1;


                  // 기본 bad 체크하기
                  if (!elementInfo.ip || !elementInfo.ip.trim()) {
                        elementInfo.bad = true;
                        elementInfo.badInfo = "no ip";
                        this._badElementInfoList.push(elementInfo);
                        return;

                  }

                  // ip 중복 체크
                  if (this._wholeElementInfoMap.has(elementInfo.ip) == true) {
                        elementInfo.bad = true;
                        elementInfo.badInfo = "duplicateIp";
                        this._badElementInfoList.push(elementInfo);
                        return;
                  }


                  // 만약 header인 경우 column 생성
                  if (elementInfo.type == "header") {
                        elementInfo.row = parseInt(elementInfo.row);

	                  // row 체크 하기 3이상 넘어가면 베드
                        if (elementInfo.row >= 3) {
                              elementInfo.bad = true;
                              elementInfo.badInfo = "row가 3이상"
                              this._badElementInfoList.push(elementInfo);
                              return;
                        }


                        // column이 3이상 넘어가면 오류\
                        if (this._headerElementInfoMap.has(row) == false) {
                              tempMap.set(row, 0);
                              this._headerElementInfoMap.set(row, [])
                        }

                        elementInfo.column = tempMap.get(row) + 1; // 기존 카운터 정보에 1을 더함.
	                  tempMap.set(row, elementInfo.column);

                        if (elementInfo.column >= 3) {
                              elementInfo.bad = true;
                              elementInfo.badInfo = "column이 3이상"
                              this._badElementInfoList.push(elementInfo);
                              return;
                        }

                        // headerMap 에 추가
                        this._headerElementInfoMap.get(row).push(elementInfo);
                        this._headerElementInfoList.push(elementInfo);

                  } else {
                        this._bodyElementInfoList.push(elementInfo);
                  }


                  this._wholeElementInfoMap.set(elementInfo.ip, elementInfo);

            })


            tempMap.clear();
            tempMap = null;
      }

      ////////////////////////////////////////////////////////////////


      //////////////////////////////////////////////////////////////
      /*
                  전체 데이터 _wholeElementInfoList
                  헤더 정보 : _headerElementInfoMap
                  body 정보 : _bodyElementInfoList
             */
      _createPortInfoList() {
            // console.log("OKOK");
            // // row정보 출력
            // 원본 데이터 (헤더와 body가 모두 포함되어 있는 정보
            let rowInfoList = XLSX.utils.sheet_to_json(this._portSheet, {
                  range: 1,
                  raw: true
            });

            this._wholePortInfoList = rowInfoList.map((rowPortInfo) => {
                  return new PortInfoVO(rowPortInfo);
            })


            this._wholePortInfoList.forEach((portInfoVO) => {
                  let fromElementInfoVO = this._wholeElementInfoMap.get(portInfoVO.from.ip);
                  let toElementInfoVO = this._wholeElementInfoMap.get(portInfoVO.to.ip);

                  // hostName 적용하기
                  if(fromElementInfoVO){
                        portInfoVO.from.hostName = fromElementInfoVO.hostName;
                        portInfoVO.from.target = fromElementInfoVO.ip.replace(/\./g, "_");
                  }

                  if(toElementInfoVO && fromElementInfoVO){
                        portInfoVO.to.hostName = toElementInfoVO.hostName;
                        portInfoVO.to.target = fromElementInfoVO.ip.replace(/\./g, "_");
                  }


                  // 정상적인 경우
                  if (fromElementInfoVO && toElementInfoVO) {
                        // 헤더 link인 경우
                        if (fromElementInfoVO.type == "header" && toElementInfoVO.type == "header") {
                              this._headerPortInfoList.push(portInfoVO);
                              return;
                        }


                        // body link인 경우
                        this._bodyPortInfoList.push(portInfoVO);
                        return;
                  }

                  // 그외인 경우
                  portInfoVO.bad = false;
                  portInfoVO.badInfo = "port에 맞는 정보가 존재하지 않습니다."
	            this._badPortInfoList.push(portInfoVO);
            })
      }

 	/* Port Type 추가 */
	_bodyPortInfoListAddType() {
		let ATypeIP = "";
		let BTypeIP = "";

		let deleteArr = [];
		let headerElementLength = this._headerElementInfoList.length;
		if(this._headerElementInfoList.length<=1){
			ATypeIP    =this._headerElementInfoList[0].ip;
		} else {
			ATypeIP    =this._headerElementInfoList[headerElementLength-2].ip;
			BTypeIP    =this._headerElementInfoList[headerElementLength-1].ip;
		}

            let tempBadMap = new Map();

		this._bodyPortInfoList.some((portInfo, index) => {
			if(tempBadMap.has(portInfo.from.ip+"_"+portInfo.to.ip)){
				portInfo.bad=true;
				portInfo.badInfo="중첩";
				deleteArr.push(index);
                        // let test = this._bodyPortInfoList.splice(index,1)[0]
                        // console.log("$$$$$$$$$#@#@#", test);
				this._badPortInfoList.push(portInfo);
				return false;
			}

			if(portInfo.from.ip == ATypeIP) {
			      portInfo.type = "A";
			      tempBadMap.set(portInfo.from.ip+"_"+portInfo.to.ip, 1);
	            }else{
			      portInfo.type = "B";
			      tempBadMap.set(portInfo.from.ip+"_"+portInfo.to.ip, 1);
                  }

	      })

		deleteArr = deleteArr.reverse();
		deleteArr.forEach((item) => {
		      this._bodyPortInfoList.splice(item, 1);
            })


		tempBadMap.clear();
		tempBadMap=null;
      }

	/* gap 추가 */
      _attachPortInfoGap() {
            // 링크 gap 처리를 위해 사용
            let tempLinkList = [];
            this._headerPortInfoList.forEach((portInfoVO) => {
                  tempLinkList.push({
                        key:portInfoVO.from.ip+"_"+portInfoVO.to.ip,
                        value:portInfoVO
                  })
            })

            ////////////////////////////////



            ////////////////////////////////
            // 동일한 선에 따라 gap 추가하기
            /*
            0부터 시작해서 동일한 선이 존재하는 지 확인하기
           동일한선
                  - from to가 동일한 경우
                  - 역으로 to, from이 동일한 경우

            gap 추가 방법
                  - 동일한 선이 2개 이상인 경우
                        0 = -20
                        1이상 = 20(gap)씩 증가
             */
            let count=0;
            while(tempLinkList.length!=0){
                  let linkInfo = tempLinkList[0];
                  let tempResultList = [];
                  tempResultList.push({
                        index:0,
                        value:linkInfo.value
                  });
                  // 배열 0은 타겟이기 때문에 1부터 검색 요소로 사용함.
                  let tempCopyList = tempLinkList.slice(1);
                  for(var i=0;i<tempCopyList.length;i++){
                        let tempLinkInfo = tempCopyList[i];

                        // 동일한 선이 있는지 확인 하기
                        if(tempLinkInfo.key.indexOf(linkInfo.value.from.ip)!=-1){
                              if(tempLinkInfo.key.indexOf(linkInfo.value.to.ip)!=-1){
                                    // 주의! i+1을 하는 이유는 tempLinkList에서 index에해당하는 요소를 삭제해야 하기때문에 tempLinkList의 index 값으로 치환시켜 주기 위해 1을 더해준다.
                                    tempResultList.push({index:i+1,value:tempLinkInfo.value})
                              }
                        }
                  }

                  // 동일한 선이 2개 이상인 경우
                  if(tempResultList.length>=2){

                        tempResultList.forEach((linkInfo, index)=>{

                              if(index==0){
                                    linkInfo.value.gap=-10;
                                    return;
                              }
                              if(index>=1){
                                    linkInfo.value.gap=index*10;
                              }
                        })


	                  for(let i=tempResultList.length-1;i>=0;i--){
		                  let temp = tempResultList[i];
		                  tempLinkList.splice(temp.index,1);
	                  }

                        // 원본 배열에서 검색된 내용 제거하기
                  }else {
                        tempLinkList.splice(0,1);
                  }
            }

            tempLinkList = null;
      }
      //////////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////
      /*
      페이지를 생성할 페이지 생성자 생성하기
       */
      _createPageCreator(templateTypeName) {

            if(templateTypeName=="vertical_page"){
                  return new VerticalPageCreator();
            }

            if(templateTypeName=="horizontal_page"){
                  return new HorizontalPageCreator();
            }
      }


      /*
     pageCreator를 이용해
       */
      async _executeCreatePageEditData() {

            let pageData = null;
            try {

                  pageData = $.extend(true,{},SKNetworkProxy.defaultPageInfo);

            } catch (error) {
                  console.log("error ", error);
                  return false;
            }

            // 생성할 페이지 이름
            pageData.page_info.name = this._savePageName;

            // 페이지 정보 생성하기
            let pageCreator = this._createPageCreator(this._templateTypeName);
            pageCreator.createPage(pageData, this);

            // 페이지 정보를 db 저장시에 사용.
            window.__PAGE_DATA__ = pageData;

            let pageDataOption = {
                  newPage: this._isNewPage,
                  pageData: pageData
            }

            wemb.editorProxy.sendNotification(EditorStatic.CMD_SAVE_DATA_TO_PAGE, pageDataOption);
      }


      _resetInfos() {
            this._wholeElementInfoMap.clear();
            this._wholeElementInfoList = [];

            //  헤더 정보만 생성하기
            this._headerElementInfoMap.clear();
            this._headerElementInfoList = [];

            this._bodyElementInfoList = [];
            this._badElementInfoList = [];


            this._elementSheet = null;
            this._elementFormulate = null;


            ///////////////////////
            this._portSheet = null;
            this._portFormulate = null;


            this._wholePortInfoList = [];
            this._headerPortInfoList = [];
            // 연결 정보(한줄인지, 두 줄인지 판단하기 위해 사용)
            this._headerPortInfoLinkList = [];
            this._bodyPortInfoList = [];

            this._badPortInfoList = [];
      }

      /*
      삭제 예정
      _attachTemplatePageName() {
            return "horizontal_page_rows5";
            let length = this._bodyElementInfoList.length;
            if (this._savePageName == "vertical_page") {
                  if (length <= 5) {
                        return "vertical_page_rows5";
                  } else if (length == 6) {
                        return "vertical_page_rows6";
                  } else if (length > 6 && length <= 9) {
                        return "vertical_page_rows9";
                  } else if (length > 9 && length <= 12) {
                        return "vertical_page_rows12";
                  } else if (length > 12 && length <= 15) {
                        return "vertical_page_rows15";
                  } else if (length > 15 && length <= 20) {
                        return "vertical_page_rows20";
                  } else if (length > 20 && length <= 25) {
                        return "vertical_page_rows25";
                  } else if (length > 25 && length <= 30) {
                        return "vertical_page_rows30";
                  } else {
                        return "vertical_page_rows42";
                  }
            } else if (this._savePageName == "horizontal_page") {
                  if (length <= 5) {
                        return "horizontal_page_rows5";
                  } else if (length > 5 && length <= 10) {
                        return "horizontal_page_rows10";
                  } else if (length > 10 && length <= 15) {
                        return "horizontal_page_rows15";
                  } else if (length > 10 && length <= 20) {
                        return "horizontal_page_rows20";
                  } else {
                        return "horizontal_page_rows40";
                  }
            }
      }
      */
      //////////////////////////////////////////////////////////////


      _getCellData(formulae, selName) {
            let selData = formulae.find(function (data) {
                  return data.indexOf(selName + "='") != -1;
            })

            if (selData != undefined) {
                  return selData.replace(selName + "='", "");
            }

            return selData;
      }


      // 확장자를 제외시킨 파일 이름만 구하기.
      getFileName(name) {
            let index = name.lastIndexOf(".");
            if (index != -1) {
                  return name.slice(0, index);
            }

            return name;
      }

      _showInfo() {
            console.log("@@showInfo _wholeElementInfoMap ", this._wholeElementInfoMap);
            console.log("@@showInfo _wholeElementInfoList ", this._wholeElementInfoList);
            console.log("@@showInfo _headerElementInfoMap ", this._headerElementInfoMap);
            console.log("@@showInfo _bodyElementInfoList ", this._bodyElementInfoList);
            console.log("@@showInfo _badElementInfoList ", this._badElementInfoList);
            console.log("---------------------------");

            console.log("@@showInfo _wholePortInfoList ", this._wholePortInfoList);
            console.log("@@showInfo _headerPortInfoList ", this._headerPortInfoList);
            console.log("@@showInfo _bodyPortInfoList ", this._bodyPortInfoList);
            console.log("@@showInfo _badPortInfoList ", this._badPortInfoList);
      }

      _getTemplateTypeName(type) {
            switch (type) {
                  case "세로형":
                        return "vertical_page";
                        break;
                  case "가로형":
                        return "horizontal_page";
            }
      }


      getHeaderRowCount(){
            return this._headerElementInfoMap.size;
      }

      getHeaderColumnCount(){
            let count = 0;
            this._headerElementInfoMap.forEach((row, index)=>{
                  count = Math.max(count, row.length);
            })

            return count;
      }


      /*
      링크 카운트 구하기
      방식
      _headerPortInfoLinkList에 저장된 fromIP_toIP 배열 목록에서
      1. fromIP가 포함되어 있는지 확인하기
      2. toIP가 포함되어 있는지 확인하기
      1,2번인 경우 링크 카운트에 해당함.

       */
      getHeaderPortLinkCount(targetPortInfoVO){
            let count =0;
            this._headerPortInfoLinkList.forEach((fromIP_toIP)=>{
                  let index = fromIP_toIP.indexOf(targetPortInfoVO.from.ip);
                  if(index!=-1){
                        index = fromIP_toIP.indexOf(targetPortInfoVO.to.ip);
                        if(index!=-1){
                              count++;
                        }
                  }
            })

            return count;

      }

}

SKNetworkProxy.ExcelHeader = {
      EQUIPMENT_NAME: "장비명",
      IP: "IP",
      EQUIPMENT_TYPE: "장비종류",
      MANUFACTURER: "제조사",
      MODEL_NAME: "모델명",
      SN: "S/N",
      ROW: "ROW",
      CID: "CID"
}


class ElementInfoVO {
      constructor(rowElementInfo) {
            this.hostName = rowElementInfo[SKNetworkProxy.ExcelHeader.EQUIPMENT_NAME] || "";
            this.ip = rowElementInfo[SKNetworkProxy.ExcelHeader.IP] || "";
            this.equipmentType = rowElementInfo[SKNetworkProxy.ExcelHeader.EQUIPMENT_TYPE] || "";
            this.manufacturer = rowElementInfo[SKNetworkProxy.ExcelHeader.MANUFACTURER] || "";
            this.modelName = rowElementInfo[SKNetworkProxy.ExcelHeader.MODEL_NAME] || "";
            this.sn = rowElementInfo[SKNetworkProxy.ExcelHeader.SN] || "";
            this.row = rowElementInfo[SKNetworkProxy.ExcelHeader.ROW] || "";
            this.row = this.row.toString().toUpperCase();
            this.cid = rowElementInfo[SKNetworkProxy.ExcelHeader.CID] || "";
            this.column = 0;
            this.bad = false;
            this.badInfo = "";

      }
}


class PortInfoVO {
      constructor(rowPortInfo) {
            this.gap=0;
            this.from = {};
            this.from.ip = rowPortInfo.IP || "";
            this.from.hostName = "";
            this.from.port = rowPortInfo.PORT || "";
            this.from.target = "";

            this.to = {};
            this.to.ip = rowPortInfo.IP_1 || "";
            this.to.hostName = "";
            this.to.port = rowPortInfo.PORT_1 || "";
            this.to.target = "";
      }
}

if (!FileReader.prototype.readAsBinaryString) {

      FileReader.prototype.readAsBinaryString = function (fileData) {
            let binary = '';
            let pk = this;
            let reader = new FileReader();

            reader.onload = function (e) {
                  let bytes = new Uint8Array(reader.result);
                  let length = bytes.byteLength;
                  for (let i = 0; i < length; i++) {
                        let a = bytes[i];

                        let b = String.fromCharCode(a)
                        binary += b;
                  }

                  /*
		pk.content를 pk.result로 하지 않는 이유는?
		FileReader 내부에서 result를 초기화 시킴
		 */
                  pk.content = binary;

                  let event = document.createEvent("Event");
                  event.initEvent("load", false, true);
                  pk.dispatchEvent(event)
            }

            reader.readAsArrayBuffer(fileData);
      }
}


SKNetworkProxy.defaultPageInfo =
{
  "page_info": {
    "props": {
      "setter": {
        "width": 1920.0,
        "height": 970.0,
        "cameraView": {
          "save": false,
          "cameraMatrix": "[1, 0, -0, 0, -0, 0.8944271909999159, -0.447213595499958, 0, 0, 0.447213595499958, 0.8944271909999159, 0, 0, 100, 100, 1]",
          "target": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0
          }
        }
      },
      "page_info": {
        "description": ""
      },
      "settings_3d": {
        "using_shadow": false,
        "using_light": false,
        "camera": {
          "x": -100.0,
          "y": 100.0,
          "z": 100.0
        },
        "cameraView": {
          "save": false,
          "cameraMatrix": "",
          "target": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0
          }
        },
        "grid": {
          "x": 100.0,
          "z": 100.0
        }
      },
      "background": {
        "using": true,
        "color": "rgb(255, 255, 255)",
        "path": "",
        "image": ""
      },
      "events": {
        "beforeLoad": "",
        "loaded": "",
        "beforeUnLoad": "",
        "unLoaded": "",
        "ready": ""
      },
      "script": {
        "fileName": ""
      }
    },
    "type": "page",
    "name": "",
    "id": "",
    "secret": "N",
    "version": "1.0.0"
  },
  "content_info": {
    "three_layer": [],
    "two_layer": []
  }
}