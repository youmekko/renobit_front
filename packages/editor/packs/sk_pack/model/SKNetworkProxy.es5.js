"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SKNetworkProxy = function () {
	_createClass(SKNetworkProxy, null, [{
		key: "ROW_SHEET_NAME",
		get: function get() {
			return "구성정보";
		}
	}, {
		key: "PORT_INFO_SHEET_NAME",
		get: function get() {
			return "포트정보";
		}
	}, {
		key: "START_DATA_ROW",
		get: function get() {
			return 4;
		}
	}]);

	function SKNetworkProxy() {
		_classCallCheck(this, SKNetworkProxy);

		this._savePageName = "";
		this._templateTypeName = "";

		this._wholeElementInfoMap = new Map();
		this._wholeElementInfoList = [];

		//  헤더 정보만 생성하기
		this._headerElementInfoMap = new Map();
		this._headerElementInfoList = [];

		this._bodyElementInfoList = [];
		this._badElementInfoList = [];

		this._wholePortInfoList = [];

		this._headerPortInfoList = [];
		this._headerPortInfoLinkList = [];
		this._bodyPortInfoList = [];
		this._badPortInfoList = [];
		this._isNewPage = true;
	}

	_createClass(SKNetworkProxy, [{
		key: "createPageToFile",
		value: function () {
			var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(file) {
				var isNewPage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
				var workbook;
				return regeneratorRuntime.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								_context.prev = 0;
								_context.next = 3;
								return this._parseExcelFile(file);

							case 3:
								workbook = _context.sent;
								_context.next = 6;
								return this.createPageToWorkbook(workbook, isNewPage);

							case 6:
								_context.next = 11;
								break;

							case 8:
								_context.prev = 8;
								_context.t0 = _context["catch"](0);

								console.log("error ", _context.t0);

							case 11:
							case "end":
								return _context.stop();
						}
					}
				}, _callee, this, [[0, 8]]);
			}));

			function createPageToFile(_x) {
				return _ref.apply(this, arguments);
			}

			return createPageToFile;
		}()
	}, {
		key: "_parseExcelFile",
		value: function () {
			var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(file) {
				var reader, fileName;
				return regeneratorRuntime.wrap(function _callee2$(_context2) {
					while (1) {
						switch (_context2.prev = _context2.next) {
							case 0:
								reader = new FileReader();

								if (!(reader == null)) {
									_context2.next = 4;
									break;
								}

								alert("FileReader를 지원하지 않는 브라우저입니다.");
								return _context2.abrupt("return");

							case 4:
								fileName = file.name;
								return _context2.abrupt("return", new Promise(function (resolve, reject) {
									reader.onload = function (e) {
										var data = null;
										/*
										e.target.result가 있는 경우는 기본
										content가 있는 경우 IE11 override한 readAsBinaryString()이 실행되는 경우임.
										*/
										if (e.target.result) data = e.target.result;else data = e.target.content;

										// 이진 파일 유무 및 sheets 전조 유무 확인
										var workbook = XLSX.read(data, { type: 'binary' });
										if (workbook == null || workbook.hasOwnProperty("Sheets") == false) {
											reject(new Error("엑셀 파일이 정사적이지 않습니다. 확인 후 다시 실행해주세요."));
											return;
										}

										resolve(workbook);
									}; //end onload

									reader.readAsBinaryString(file);
								}));

							case 6:
							case "end":
								return _context2.stop();
						}
					}
				}, _callee2, this);
			}));

			function _parseExcelFile(_x3) {
				return _ref2.apply(this, arguments);
			}

			return _parseExcelFile;
		}()

		/*
		1. 정보 리셋
		2. 저장할 페이지명과 상요할 템플릿 타입 정보 생서하기
		3. elementInfo, 정보 생성하기
		4. portInfo 정보 생성하기
		5. 사용할 템플릿 페이지 명 생성
		6. 실제 페이지 생성하기
		  */

	}, {
		key: "createPageToWorkbook",
		value: function () {
			var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(workbook) {
				var isNewPage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
				return regeneratorRuntime.wrap(function _callee3$(_context3) {
					while (1) {
						switch (_context3.prev = _context3.next) {
							case 0:
								this._resetInfos();

								this._isNewPage = isNewPage;

								this._createSheet(workbook);
								this._attachPageNameAndTemplateTypeName();

								// element 정보 생성하기
								this._createElementInfoList();

								// 포트 정보 생성하기
								this._createPortInfoList();
								this._bodyPortInfoListAddType();
								// 포트 정보에 gap 정보 투이기
								this._attachPortInfoGap();

								// 템플릿 페이지 명 선택
								// this._attachTemplatePageName();
								this._showInfo();

								// 편집 정보 생성하기
								this._executeCreatePageEditData();

							case 10:
							case "end":
								return _context3.stop();
						}
					}
				}, _callee3, this);
			}));

			function createPageToWorkbook(_x4) {
				return _ref3.apply(this, arguments);
			}

			return createPageToWorkbook;
		}()

		/*
		데이터 생성에 필요한  sheet 정보 생성하기
		 */

	}, {
		key: "_createSheet",
		value: function _createSheet(workbook) {
			// 구성정보 sheet 존재 유무 확인.
			this._elementSheet = workbook.Sheets[SKNetworkProxy.ROW_SHEET_NAME];
			this._elementFormulate = XLSX.utils.sheet_to_formulae(this._elementSheet, { range: 1 });

			// 구성정보 sheet 존재 유무 확인.
			this._portSheet = workbook.Sheets[SKNetworkProxy.PORT_INFO_SHEET_NAME];
			this._portFormulate = XLSX.utils.sheet_to_formulae(this._portSheet, { range: 1 });
		}

		/*
		생성할 페이지 이름과, 템플릿 타입 생ㅅ어하기
		 */

	}, {
		key: "_attachPageNameAndTemplateTypeName",
		value: function _attachPageNameAndTemplateTypeName() {
			// 생성할 페이지 정보
			this._savePageName = this._getCellData(this._elementFormulate, "B1");
			// 템플릿 이름 구하기
			var tempPageType = this._getCellData(this._elementFormulate, "B2");
			// 가로/세로 템플릿 타입명 구하기
			this._templateTypeName = this._getTemplateTypeName(tempPageType);
		}

		////////////////////////////////////////////////////////////////


		////////////////////////////////////////////////////////////////

		/*
		전체 데이터 _wholeElementInfoList
		헤더 정보 : _headerElementInfoMap
		body 정보 : _bodyElementInfoList
		*/

	}, {
		key: "_createElementInfoList",
		value: function _createElementInfoList() {
			var _this = this;

			// // row정보 출력
			var headerInfo = Object.values(SKNetworkProxy.ExcelHeader);
			// 원본 데이터 (헤더와 body가 모두 포함되어 있는 정보
			var rowElementInfoList = XLSX.utils.sheet_to_json(this._elementSheet, {
				range: SKNetworkProxy.START_DATA_ROW,
				raw: true,
				header: headerInfo
			});

			this._wholeElementInfoList = rowElementInfoList.map(function (rowElementInfo) {
				return new ElementInfoVO(rowElementInfo);
			});

			var tempMap = new Map();

			// 타입과 column 생성하기
			this._wholeElementInfoList.forEach(function (elementInfo) {
				var row = elementInfo.row;
				elementInfo.type = row == "A" ? "body" : "header";
				elementInfo.bad = false;
				elementInfo.column = 1;

				// 기본 bad 체크하기
				if (!elementInfo.ip || !elementInfo.ip.trim()) {
					elementInfo.bad = true;
					elementInfo.badInfo = "no ip";
					_this._badElementInfoList.push(elementInfo);
					return;
				}

				// ip 중복 체크
				if (_this._wholeElementInfoMap.has(elementInfo.ip) == true) {
					elementInfo.bad = true;
					elementInfo.badInfo = "duplicateIp";
					_this._badElementInfoList.push(elementInfo);
					return;
				}

				// 만약 header인 경우 column 생성
				if (elementInfo.type == "header") {
					elementInfo.row = parseInt(elementInfo.row);

					// row 체크 하기 3이상 넘어가면 베드
					if (elementInfo.row >= 3) {
						elementInfo.bad = true;
						elementInfo.badInfo = "row가 3이상";
						_this._badElementInfoList.push(elementInfo);
						return;
					}

					// column이 3이상 넘어가면 오류\
					if (_this._headerElementInfoMap.has(row) == false) {
						tempMap.set(row, 0);
						_this._headerElementInfoMap.set(row, []);
					}

					elementInfo.column = tempMap.get(row) + 1; // 기존 카운터 정보에 1을 더함.
					tempMap.set(row, elementInfo.column);

					if (elementInfo.column >= 3) {
						elementInfo.bad = true;
						elementInfo.badInfo = "column이 3이상";
						_this._badElementInfoList.push(elementInfo);
						return;
					}

					// headerMap 에 추가
					_this._headerElementInfoMap.get(row).push(elementInfo);
					_this._headerElementInfoList.push(elementInfo);
				} else {
					_this._bodyElementInfoList.push(elementInfo);
				}

				_this._wholeElementInfoMap.set(elementInfo.ip, elementInfo);
			});

			tempMap.clear();
			tempMap = null;
		}

		////////////////////////////////////////////////////////////////


		//////////////////////////////////////////////////////////////
		/*
				전체 데이터 _wholeElementInfoList
				헤더 정보 : _headerElementInfoMap
				body 정보 : _bodyElementInfoList
			 */

	}, {
		key: "_createPortInfoList",
		value: function _createPortInfoList() {
			var _this2 = this;

			// console.log("OKOK");
			// // row정보 출력
			// 원본 데이터 (헤더와 body가 모두 포함되어 있는 정보
			var rowInfoList = XLSX.utils.sheet_to_json(this._portSheet, {
				range: 1,
				raw: true
			});

			this._wholePortInfoList = rowInfoList.map(function (rowPortInfo) {
				return new PortInfoVO(rowPortInfo);
			});

			this._wholePortInfoList.forEach(function (portInfoVO) {
				var fromElementInfoVO = _this2._wholeElementInfoMap.get(portInfoVO.from.ip);
				var toElementInfoVO = _this2._wholeElementInfoMap.get(portInfoVO.to.ip);

				// hostName 적용하기
				if (fromElementInfoVO) {
					portInfoVO.from.hostName = fromElementInfoVO.hostName;
					portInfoVO.from.target = fromElementInfoVO.ip.replace(/\./g, "_");
				}

				if (toElementInfoVO && fromElementInfoVO) {
					portInfoVO.to.hostName = toElementInfoVO.hostName;
					portInfoVO.to.target = fromElementInfoVO.ip.replace(/\./g, "_");
				}

				// 정상적인 경우
				if (fromElementInfoVO && toElementInfoVO) {
					// 헤더 link인 경우
					if (fromElementInfoVO.type == "header" && toElementInfoVO.type == "header") {
						_this2._headerPortInfoList.push(portInfoVO);
						return;
					}

					// body link인 경우
					_this2._bodyPortInfoList.push(portInfoVO);
					return;
				}

				// 그외인 경우
				portInfoVO.bad = false;
				portInfoVO.badInfo = "port에 맞는 정보가 존재하지 않습니다.";
				_this2._badPortInfoList.push(portInfoVO);
			});
		}

		/* Port Type 추가 */

	}, {
		key: "_bodyPortInfoListAddType",
		value: function _bodyPortInfoListAddType() {
			var _this3 = this;

			var ATypeIP = "";
			var BTypeIP = "";

			var deleteArr = [];
			var headerElementLength = this._headerElementInfoList.length;
			if (this._headerElementInfoList.length <= 1) {
				ATypeIP = this._headerElementInfoList[0].ip;
			} else {
				ATypeIP = this._headerElementInfoList[headerElementLength - 2].ip;
				BTypeIP = this._headerElementInfoList[headerElementLength - 1].ip;
			}

			var tempBadMap = new Map();

			this._bodyPortInfoList.some(function (portInfo, index) {
				if (tempBadMap.has(portInfo.from.ip + "_" + portInfo.to.ip)) {
					portInfo.bad = true;
					portInfo.badInfo = "중첩";
					deleteArr.push(index);
					// let test = this._bodyPortInfoList.splice(index,1)[0]
					// console.log("$$$$$$$$$#@#@#", test);
					_this3._badPortInfoList.push(portInfo);
					return false;
				}

				if (portInfo.from.ip == ATypeIP) {
					portInfo.type = "A";
					tempBadMap.set(portInfo.from.ip + "_" + portInfo.to.ip, 1);
				} else {
					portInfo.type = "B";
					tempBadMap.set(portInfo.from.ip + "_" + portInfo.to.ip, 1);
				}
			});

			deleteArr = deleteArr.reverse();
			deleteArr.forEach(function (item) {
				_this3._bodyPortInfoList.splice(item, 1);
			});

			tempBadMap.clear();
			tempBadMap = null;
		}

		/* gap 추가 */

	}, {
		key: "_attachPortInfoGap",
		value: function _attachPortInfoGap() {
			// 링크 gap 처리를 위해 사용
			var tempLinkList = [];
			this._headerPortInfoList.forEach(function (portInfoVO) {
				tempLinkList.push({
					key: portInfoVO.from.ip + "_" + portInfoVO.to.ip,
					value: portInfoVO
				});
			});

			////////////////////////////////


			////////////////////////////////
			// 동일한 선에 따라 gap 추가하기
			/*
			0부터 시작해서 동일한 선이 존재하는 지 확인하기
			동일한선
				- from to가 동일한 경우
				- 역으로 to, from이 동일한 경우
			 gap 추가 방법
				- 동일한 선이 2개 이상인 경우
					0 = -20
					1이상 = 20(gap)씩 증가
			 */
			var count = 0;
			while (tempLinkList.length != 0) {
				var linkInfo = tempLinkList[0];
				var tempResultList = [];
				tempResultList.push({
					index: 0,
					value: linkInfo.value
				});
				// 배열 0은 타겟이기 때문에 1부터 검색 요소로 사용함.
				var tempCopyList = tempLinkList.slice(1);
				for (var i = 0; i < tempCopyList.length; i++) {
					var tempLinkInfo = tempCopyList[i];

					// 동일한 선이 있는지 확인 하기
					if (tempLinkInfo.key.indexOf(linkInfo.value.from.ip) != -1) {
						if (tempLinkInfo.key.indexOf(linkInfo.value.to.ip) != -1) {
							// 주의! i+1을 하는 이유는 tempLinkList에서 index에해당하는 요소를 삭제해야 하기때문에 tempLinkList의 index 값으로 치환시켜 주기 위해 1을 더해준다.
							tempResultList.push({ index: i + 1, value: tempLinkInfo.value });
						}
					}
				}

				// 동일한 선이 2개 이상인 경우
				if (tempResultList.length >= 2) {

					tempResultList.forEach(function (linkInfo, index) {

						if (index == 0) {
							linkInfo.value.gap = -10;
							return;
						}
						if (index >= 1) {
							linkInfo.value.gap = index * 10;
						}
					});

					for (var _i = tempResultList.length - 1; _i >= 0; _i--) {
						var temp = tempResultList[_i];
						tempLinkList.splice(temp.index, 1);
					}

					// 원본 배열에서 검색된 내용 제거하기
				} else {
					tempLinkList.splice(0, 1);
				}
			}

			tempLinkList = null;
		}
		//////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////
		/*
		페이지를 생성할 페이지 생성자 생성하기
		 */

	}, {
		key: "_createPageCreator",
		value: function _createPageCreator(templateTypeName) {

			if (templateTypeName == "vertical_page") {
				return new VerticalPageCreator();
			}

			if (templateTypeName == "horizontal_page") {
				return new HorizontalPageCreator();
			}
		}

		/*
		pageCreator를 이용해
		 */

	}, {
		key: "_executeCreatePageEditData",
		value: function () {
			var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
				var pageData, pageCreator, pageDataOption;
				return regeneratorRuntime.wrap(function _callee4$(_context4) {
					while (1) {
						switch (_context4.prev = _context4.next) {
							case 0:
								pageData = null;
								_context4.prev = 1;


								pageData = $.extend(true, {}, SKNetworkProxy.defaultPageInfo);

								_context4.next = 9;
								break;

							case 5:
								_context4.prev = 5;
								_context4.t0 = _context4["catch"](1);

								console.log("error ", _context4.t0);
								return _context4.abrupt("return", false);

							case 9:

								// 생성할 페이지 이름
								pageData.page_info.name = this._savePageName;

								// 페이지 정보 생성하기
								pageCreator = this._createPageCreator(this._templateTypeName);

								pageCreator.createPage(pageData, this);

								// 페이지 정보를 db 저장시에 사용.
								window.__PAGE_DATA__ = pageData;

								pageDataOption = {
									newPage: this._isNewPage,
									pageData: pageData
								};


								wemb.editorProxy.sendNotification(EditorStatic.CMD_SAVE_DATA_TO_PAGE, pageDataOption);

							case 15:
							case "end":
								return _context4.stop();
						}
					}
				}, _callee4, this, [[1, 5]]);
			}));

			function _executeCreatePageEditData() {
				return _ref4.apply(this, arguments);
			}

			return _executeCreatePageEditData;
		}()
	}, {
		key: "_resetInfos",
		value: function _resetInfos() {
			this._wholeElementInfoMap.clear();
			this._wholeElementInfoList = [];

			//  헤더 정보만 생성하기
			this._headerElementInfoMap.clear();
			this._headerElementInfoList = [];

			this._bodyElementInfoList = [];
			this._badElementInfoList = [];

			this._elementSheet = null;
			this._elementFormulate = null;

			///////////////////////
			this._portSheet = null;
			this._portFormulate = null;

			this._wholePortInfoList = [];
			this._headerPortInfoList = [];
			// 연결 정보(한줄인지, 두 줄인지 판단하기 위해 사용)
			this._headerPortInfoLinkList = [];
			this._bodyPortInfoList = [];

			this._badPortInfoList = [];
		}

		/*
		삭제 예정
		_attachTemplatePageName() {
			return "horizontal_page_rows5";
			let length = this._bodyElementInfoList.length;
			if (this._savePageName == "vertical_page") {
				if (length <= 5) {
					return "vertical_page_rows5";
				} else if (length == 6) {
					return "vertical_page_rows6";
				} else if (length > 6 && length <= 9) {
					return "vertical_page_rows9";
				} else if (length > 9 && length <= 12) {
					return "vertical_page_rows12";
				} else if (length > 12 && length <= 15) {
					return "vertical_page_rows15";
				} else if (length > 15 && length <= 20) {
					return "vertical_page_rows20";
				} else if (length > 20 && length <= 25) {
					return "vertical_page_rows25";
				} else if (length > 25 && length <= 30) {
					return "vertical_page_rows30";
				} else {
					return "vertical_page_rows42";
				}
			} else if (this._savePageName == "horizontal_page") {
				if (length <= 5) {
					return "horizontal_page_rows5";
				} else if (length > 5 && length <= 10) {
					return "horizontal_page_rows10";
				} else if (length > 10 && length <= 15) {
					return "horizontal_page_rows15";
				} else if (length > 10 && length <= 20) {
					return "horizontal_page_rows20";
				} else {
					return "horizontal_page_rows40";
				}
			}
		}
		*/
		//////////////////////////////////////////////////////////////


	}, {
		key: "_getCellData",
		value: function _getCellData(formulae, selName) {
			var selData = formulae.find(function (data) {
				return data.indexOf(selName + "='") != -1;
			});

			if (selData != undefined) {
				return selData.replace(selName + "='", "");
			}

			return selData;
		}

		// 확장자를 제외시킨 파일 이름만 구하기.

	}, {
		key: "getFileName",
		value: function getFileName(name) {
			var index = name.lastIndexOf(".");
			if (index != -1) {
				return name.slice(0, index);
			}

			return name;
		}
	}, {
		key: "_showInfo",
		value: function _showInfo() {
			console.log("@@showInfo _wholeElementInfoMap ", this._wholeElementInfoMap);
			console.log("@@showInfo _wholeElementInfoList ", this._wholeElementInfoList);
			console.log("@@showInfo _headerElementInfoMap ", this._headerElementInfoMap);
			console.log("@@showInfo _bodyElementInfoList ", this._bodyElementInfoList);
			console.log("@@showInfo _badElementInfoList ", this._badElementInfoList);
			console.log("---------------------------");

			console.log("@@showInfo _wholePortInfoList ", this._wholePortInfoList);
			console.log("@@showInfo _headerPortInfoList ", this._headerPortInfoList);
			console.log("@@showInfo _bodyPortInfoList ", this._bodyPortInfoList);
			console.log("@@showInfo _badPortInfoList ", this._badPortInfoList);
		}
	}, {
		key: "_getTemplateTypeName",
		value: function _getTemplateTypeName(type) {
			switch (type) {
				case "세로형":
					return "vertical_page";
					break;
				case "가로형":
					return "horizontal_page";
			}
		}
	}, {
		key: "getHeaderRowCount",
		value: function getHeaderRowCount() {
			return this._headerElementInfoMap.size;
		}
	}, {
		key: "getHeaderColumnCount",
		value: function getHeaderColumnCount() {
			var count = 0;
			this._headerElementInfoMap.forEach(function (row, index) {
				count = Math.max(count, row.length);
			});

			return count;
		}

		/*
		링크 카운트 구하기
		방식
		_headerPortInfoLinkList에 저장된 fromIP_toIP 배열 목록에서
		1. fromIP가 포함되어 있는지 확인하기
		2. toIP가 포함되어 있는지 확인하기
		1,2번인 경우 링크 카운트에 해당함.
		  */

	}, {
		key: "getHeaderPortLinkCount",
		value: function getHeaderPortLinkCount(targetPortInfoVO) {
			var count = 0;
			this._headerPortInfoLinkList.forEach(function (fromIP_toIP) {
				var index = fromIP_toIP.indexOf(targetPortInfoVO.from.ip);
				if (index != -1) {
					index = fromIP_toIP.indexOf(targetPortInfoVO.to.ip);
					if (index != -1) {
						count++;
					}
				}
			});

			return count;
		}
	}]);

	return SKNetworkProxy;
}();

SKNetworkProxy.ExcelHeader = {
	EQUIPMENT_NAME: "장비명",
	IP: "IP",
	EQUIPMENT_TYPE: "장비종류",
	MANUFACTURER: "제조사",
	MODEL_NAME: "모델명",
	SN: "S/N",
	ROW: "ROW",
	CID: "CID"
};

var ElementInfoVO = function ElementInfoVO(rowElementInfo) {
	_classCallCheck(this, ElementInfoVO);

	this.hostName = rowElementInfo[SKNetworkProxy.ExcelHeader.EQUIPMENT_NAME] || "";
	this.ip = rowElementInfo[SKNetworkProxy.ExcelHeader.IP] || "";
	this.equipmentType = rowElementInfo[SKNetworkProxy.ExcelHeader.EQUIPMENT_TYPE] || "";
	this.manufacturer = rowElementInfo[SKNetworkProxy.ExcelHeader.MANUFACTURER] || "";
	this.modelName = rowElementInfo[SKNetworkProxy.ExcelHeader.MODEL_NAME] || "";
	this.sn = rowElementInfo[SKNetworkProxy.ExcelHeader.SN] || "";
	this.row = rowElementInfo[SKNetworkProxy.ExcelHeader.ROW] || "";
	this.row = this.row.toString().toUpperCase();
	this.cid = rowElementInfo[SKNetworkProxy.ExcelHeader.CID] || "";
	this.column = 0;
	this.bad = false;
	this.badInfo = "";
};

var PortInfoVO = function PortInfoVO(rowPortInfo) {
	_classCallCheck(this, PortInfoVO);

	this.gap = 0;
	this.from = {};
	this.from.ip = rowPortInfo.IP || "";
	this.from.hostName = "";
	this.from.port = rowPortInfo.PORT || "";
	this.from.target = "";

	this.to = {};
	this.to.ip = rowPortInfo.IP_1 || "";
	this.to.hostName = "";
	this.to.port = rowPortInfo.PORT_1 || "";
	this.to.target = "";
};

if (!FileReader.prototype.readAsBinaryString) {

	FileReader.prototype.readAsBinaryString = function (fileData) {
		var binary = '';
		var pk = this;
		var reader = new FileReader();

		reader.onload = function (e) {
			var bytes = new Uint8Array(reader.result);
			var length = bytes.byteLength;
			for (var i = 0; i < length; i++) {
				var a = bytes[i];

				var b = String.fromCharCode(a);
				binary += b;
			}

			/*
			pk.content를 pk.result로 하지 않는 이유는?
			FileReader 내부에서 result를 초기화 시킴
			*/
			pk.content = binary;

			var event = document.createEvent("Event");
			event.initEvent("load", false, true);
			pk.dispatchEvent(event);
		};

		reader.readAsArrayBuffer(fileData);
	};
}

SKNetworkProxy.defaultPageInfo = {
	"page_info": {
		"props": {
			"setter": {
				"width": 1920.0,
				"height": 970.0,
				"cameraView": {
					"save": false,
					"cameraMatrix": "[1, 0, -0, 0, -0, 0.8944271909999159, -0.447213595499958, 0, 0, 0.447213595499958, 0.8944271909999159, 0, 0, 100, 100, 1]",
					"target": {
						"x": 0.0,
						"y": 0.0,
						"z": 0.0
					}
				}
			},
			"page_info": {
				"description": ""
			},
			"settings_3d": {
				"using_shadow": false,
				"using_light": false,
				"camera": {
					"x": -100.0,
					"y": 100.0,
					"z": 100.0
				},
				"cameraView": {
					"save": false,
					"cameraMatrix": "",
					"target": {
						"x": 0.0,
						"y": 0.0,
						"z": 0.0
					}
				},
				"grid": {
					"x": 100.0,
					"z": 100.0
				}
			},
			"background": {
				"using": true,
				"color": "rgb(255, 255, 255)",
				"path": "",
				"image": ""
			},
			"events": {
				"beforeLoad": "",
				"loaded": "",
				"beforeUnLoad": "",
				"unLoaded": "",
				"ready": ""
			},
			"script": {
				"fileName": ""
			}
		},
		"type": "page",
		"name": "",
		"id": "",
		"secret": "N",
		"version": "1.0.0"
	},
	"content_info": {
		"three_layer": [],
		"two_layer": []
	}
};