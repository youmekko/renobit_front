
class HeaderLayoutHelper {

      constructor(){
            this._startX = 0;
            this._startY = 0;
            this._rowWidth =0;
            this._rowHeight=0;
            this._rowCount = 1;
            this._columnCount =2;
            this._columnWidth =0;
            this._columnHalfWidth = 0;
            this.setLayout(500,100,1,2);
      }




      /*
      name 생성하기
       */
      _getNextStateInstanceName(componentLabelName="Backbone"){
            let name=null;
            try {

                  let comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("two_layer", componentLabelName);
                  if (comPanelInfo) {
                        let separator = window.wemb.componentLibraryManager.getInstanceSeparator(comPanelInfo.name);
                        let count = wemb.editorProxy.getNextInstanceCount(comPanelInfo.name);

                        name = separator + "_" + count;


                  } else {
                        console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.")
                  }
            }
            catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
            }

            return name;
      }



      // 컬럼에 해당하는 x 위치 값 구하기
      _getX(column, stateWidth){
            // column 중앙 위치
            let x = this._startX+ (this._rowWidth*(column))+this._columnHalfWidth;

            // stateWidth가 컬럼의 중앙에 올수 있게 stateWidth의 반절 값을 빼준다.
            x = x-(stateWidth/2);

            return x;
      }

      // 컬럼에 해당하는 y 위치 값 구하기
      _getY(row){
            // column 중앙 위치
            let y = this._startY+ (this._rowHeight*(row));

            return y;
      }




      setStartXY(startX, startY){
            this._startX = startX;
            this._startY = startY;
      }

      setLayout(rowWidth, rowHeight, rowCount,columnCount){
            this._rowWidth = rowWidth;
            this._rowHeight = rowHeight;
            this._rowCount = rowCount;
            this._columnCount = columnCount;
            this._columnWidth = rowWidth/columnCount;
            this._columnHalfWidth = this._columnWidth/2;
      }

      /*
      false가 리턴되는 경우
       */
      createStateComInstanceVO(row, column, elementInfoVO, comPanelLabelName="Backbone"){
            let componentVO = null;

            try {

                  let comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("two_layer", comPanelLabelName);
                  if (comPanelInfo) {
                        componentVO = {};
                        componentVO.componentName = comPanelInfo.name;
                        componentVO.label = comPanelInfo.label;
                        componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                        componentVO.initProperties.props.setter.x = this._getX(column, componentVO.initProperties.props.setter.width);
                        componentVO.initProperties.props.setter.y = this._getY(row);

                        componentVO.id = WeMB.ObjectUtil.generateID();
                        componentVO.name = elementInfoVO.ip;
                        //let separator = window.wemb.componentLibraryManager.getInstanceSeparator(comPanelInfo.name);
                        //let count = wemb.editorProxy.getNextInstanceCount(comPanelInfo.name);

                        //componentVO.name = separator + "_" + count;
                        //await this._addComponent(componentVO);



                  } else {
                        console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.")
                  }
            }
            catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
            }

            return componentVO;
      }

      async createLineComInstanceVO(portInfoVO, gap=0){

            let componentVO = null;

            try {
                  let comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("two_layer", "NetworkLinkComponent");
                  if (comPanelInfo) {
                        componentVO = {}
                        componentVO.componentName = comPanelInfo.name;
                        componentVO.label = comPanelInfo.label;
                        componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                        componentVO.initProperties.props.network = {
                              "gap": gap,
                              "from": {
                                    "ip": "",
                                    "hostName": "",
                                    "port": "",
                                    "target": fromComInstanceName
                              },
                              "to": {
                                    "ip": "",
                                    "hostName":"" ,
                                    "port": "",
                                    "target": toComInstanceName
                              }
                        }

                        componentVO.initProperties.props.network.gap = portInfoVO.gap;
                        componentVO.initProperties.props.network.from = portInfoVO.from;
                        componentVO.initProperties.props.network.to = portInfoVO.to;


                  } else {
                        console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.")
                  }
            }catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
            }

            return componentVO;
      }

      async addCircle(firstLineComInstanceInfo, secondLineComInstanceInfo){
            let componentVO = null;

            try {
                  let comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("two_layer", "SVGCircleComponent");
                  let firstProps = firstLineComInstanceInfo.initProperties.props;
                  let secondProps = secondLineComInstanceInfo.initProperties.props;


                  let width=30;
                  let height=60;
                  let x = firstProps.setter.x+((secondProps.setter.x-firstProps.setter.x+firstProps.setter.width)/2);
                  x-=(width/2);
                  let y = firstProps.setter.y+(firstProps.setter.height/2);
                  y-=(height/2);

                  if (comPanelInfo) {
                        componentVO = {}
                        componentVO.componentName = comPanelInfo.name;
                        componentVO.label = comPanelInfo.label;
                        componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                        componentVO.initProperties.props.setter = {
                              x:x,
                              y:y,
                              width:width,
                              height:height
                        }

                        componentVO.initProperties.props.style={
                              stroke:"rgb(0,0,0)",
                              fill:"rgba(255,255,255,0)",
                              "stroke-width":2
                        }


                        await this._addComponent(componentVO);


                  } else {
                        console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.")
                  }
            }catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
            }

            return componentVO;
      }
      async draw(){

      }

}



