"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HeaderGroupExcelCreator = function () {
	function HeaderGroupExcelCreator() {
		_classCallCheck(this, HeaderGroupExcelCreator);
	}

	/*
 false가 리턴되는 경우
  */


	_createClass(HeaderGroupExcelCreator, [{
		key: "addState",
		value: function addState(row, column, info) {
			var comPanelLabelName = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "Switch01";

			var componentVO = null;
			try {
				var comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("two_layer", comPanelLabelName);

				if (comPanelInfo) {
					componentVO = $.extend(true, {}, HeaderGroupExcelCreator.NetworkStateMetaInfo); // 생성.
					componentVO.componentName = comPanelInfo.name;
					componentVO.label = comPanelInfo.label;

					componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
					componentVO.props.setter.info = $.extend(true, {}, HeaderGroupExcelCreator.EquipmentTypeToComponent[comPanelLabelName]);

					componentVO.props.setter.width = componentVO.props.setter.info.width;
					componentVO.props.setter.height = componentVO.props.setter.info.height;
					componentVO.props.network = $.extend(true, {}, info);
					componentVO.id = WeMB.ObjectUtil.generateID();
					componentVO.name = info.ip.replace(/\./g, "_"); // 치환

					if(componentVO.props.network.type=="header")
						componentVO.props.router_component=true;


					console.log("SW 01 ", componentVO);
				} else {
					console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.");
				}
			} catch (error) {
				console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
			}

			return componentVO;
		}
	}, {
		key: "addLine",
		value: function addLine(fromComInstance, toComInstance, index) {
			var gap = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

			var componentVO = null;
			var temp1 = $.extend(true, {}, fromComInstance);
			var temp2 = $.extend(true, {}, toComInstance);

			try {
				var comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("two_layer", "NetworkLinkComponent");
				if (comPanelInfo) {
					componentVO = $.extend(true, {}, HeaderGroupExcelCreator.NetworkLinkMetaInfo); // 생성.
					componentVO.componentName = comPanelInfo.name;
					componentVO.label = comPanelInfo.label;
					componentVO.name = "net_link_" + index;
					componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);

					componentVO.props.network = {
						"gap": gap,
						"from": temp1,
						"to": temp2
					};
				} else {
					console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.");
				}
			} catch (error) {
				console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
			}

			return componentVO;
		}
	}, {
		key: "addElementPort",
		value: function addElementPort(row, portInfo, index) {
			var type = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "horizontal";

			var portVO = null;

			try {
				if (type == "vertical") {
					portVO = $.extend(true, {}, HeaderGroupExcelCreator.VerticalPortMetaInfo);
				} else {
					portVO = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalPortMetaInfo);

					portVO.props.setter.width = 120;

					portVO.props.portInfo.textLeftMargin = "20px";
					portVO.props.portInfo.textRightMargin = "5px";
				}
				if (row == "BAD") {
					portVO.name = "bad_port_" + index;
				}

				portVO.props.portInfo.fromHost = portInfo.from.hostName;
				portVO.props.portInfo.fromIP = portInfo.from.ip;
				portVO.props.portInfo.fromText = portInfo.from.port;

				portVO.props.portInfo.toHost = portInfo.to.hostName;
				portVO.props.portInfo.toIP = portInfo.to.ip;
				portVO.props.portInfo.toText = portInfo.to.port;
			} catch (err) {
				console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", err);
			}

			return portVO;
		}
	}]);

	return HeaderGroupExcelCreator;
}();

HeaderGroupExcelCreator.EquipmentTypeToComponent = {
	"Backbone": {
		"name": "network009",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat009/res/",
		"width": 52,
		"height": 70
	},
	"Firewall": {
		"name": "firewall001",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/FirewallFlat001/res/",
		"width": 50,
		"height": 50
	},
	"Switch": {
		"name": "network005",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat005/res/",
		"width": 76,
		"height": 38
	},
	"Router": {
		"name": "router001",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/RouterFlat001/res/",
		"width": 64,
		"height": 40
	},
	"Server": {
		"name": "server001",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/ServerFlat001/res/",
		"width": 52,
		"height": 76
	},
	"Building": {
		"name": "building",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/BuildingFlat/res/",
		"width": 50,
		"height": 50
	},
	"Switch01": {
		"name": "Switch01",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat001/res/",
		"width": 50,
		"height": 50
	},
	"Switch02": {
		"name": "Switch02",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat002/res/",
		"width": 72,
		"height": 50
	},
	"Switch03": {
		"name": "Switch03",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat003/res/",
		"width": 72,
		"height": 50
	},
	"Switch04": {
		"name": "Switch04",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat004/res/",
		"width": 50,
		"height": 50
	},
	"Switch05": {
		"name": "Switch05",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat006/res/",
		"width": 52,
		"height": 70
	},
	"Switch06": {
		"name": "Switch06",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat007/res/",
		"width": 52,
		"height": 70
	},
	"Switch07": {
		"name": "Switch07",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat008/res/",
		"width": 52,
		"height": 76
	}
};

HeaderGroupExcelCreator.NetworkStateMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10053,
			"x": 51,
			"y": 102,
			"visible": true,
			"opacity": 1,
			"width": 76,
			"height": 36,
			"rotation": 0,
			"selectItem": "",
			"severity": "normal",
			"isSaved": true,
			"info": {
				"name": "NetworkFlat005",
				"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat005/res/"
			}
		},
		"label": {
			"label_using": "Y",
			"label_text": "<div class='label-box'>hostname<br />ip</div>",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": "",
			"change": "",
			"change2": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"network": {
			"hostName": "hostname",
			"ip": "ip",
			"equipmentType": "",
			"manufacturer": "",
			"modelName": "",
			"row": "",
			"cid": "",
			"column": "",
			"bad": "",
			"badInfo": "",
			"sn": "",
			"type": ""
		},
		"componentName": "NetworkStateComponent",
		"name": "network_symbol"
	},
	"id": "4afdd952-f5e0-4966-8b1c-4aaa8df522d0",
	"name": "network_symbol",
	"layerName": "twoLayer",
	"componentName": "NetworkStateComponent",
	"version": "1.0.0",
	"category": "2D"
};

HeaderGroupExcelCreator.NetworkLinkMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10054,
			"x": 446,
			"y": 334,
			"visible": true,
			"opacity": 1,
			"width": 50,
			"height": 50,
			"rotation": 0
		},
		"label": {
			"label_using": "Y",
			"label_text": "",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"network": {
			"gap": "",
			"from": {
				"ip": "",
				"hostName": "",
				"port": "",
				"target": ""
			},
			"to": {
				"ip": "",
				"hostName": "",
				"port": "",
				"target": ""
			}
		},
		"componentName": "NetworkLinkComponent",
		"name": "net_link"
	},
	"id": "d9e1f182-9f34-4bcc-896d-5194f8cd06cf",
	"name": "net_link",
	"layerName": "twoLayer",
	"componentName": "NetworkLinkComponent",
	"version": "1.5.0",
	"category": "2D"
};

HeaderGroupExcelCreator.HorizontalPortMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10177,
			"x": 400,
			"y": 450,
			"visible": true,
			"opacity": 1,
			"width": 1400,
			"height": 20,
			"rotation": 0
		},
		"label": {
			"label_using": "N",
			"label_text": "Frame Component",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0,
			"cursor": "default",
			"min-width": "20px",
			"min-height": "20px"
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"portInfo": {
			"direction": "forward",
			"position": "top",
			"lineStyle": "solid",
			"fromText": "",
			"fromHost": "fromHost",
			"fromIP": "fromIP",
			"toText": "",
			"toHost": "toHost",
			"toIP": "toIP",
			"textTopMargin": 0,
			"textBottomMargin": 0
		},
		"componentName": "HorizontalPortComponent",
		"name": "horizontal_line"
	},
	"id": "4c0eecaf-76e7-4df1-b222-a6a57d26a7a7",
	"name": "horizontal_line",
	"layerName": "twoLayer",
	"componentName": "HorizontalPortComponent",
	"version": "1.0.0",
	"category": "2D"
};

HeaderGroupExcelCreator.VerticalPortMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10178,
			"x": 592,
			"y": 249,
			"visible": true,
			"opacity": 1,
			"width": 60,
			"height": 86,
			"rotation": 0
		},
		"label": {
			"label_using": "N",
			"label_text": "Frame Component",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0,
			"cursor": "default",
			"min-width": "20px",
			"min-height": "20px"
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"portInfo": {
			"direction": "reverse",
			"position": "right",
			"lineStyle": "solid",
			"fromText": "",
			"fromHost": "fromHost",
			"fromIP": "fromIP",
			"toText": "",
			"toHost": "toHost",
			"toIP": "toIP",
			"textTopMargin": 0,
			"textBottomMargin": 0
		}
	},
	"id": "a11c2679-7db0-482a-9921-09231a62f8ff",
	"name": "vertical_line",
	"layerName": "twoLayer",
	"componentName": "VerticalPortComponent",
	"version": "1.0.0",
	"category": "2D"
};

HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10179,
			"x": 409,
			"y": 302,
			"visible": true,
			"opacity": 1,
			"width": 1400,
			"height": 20,
			"rotation": 0,
			"min-width": 20,
			"min-height": 20
		},
		"label": {
			"label_using": "N",
			"label_text": "HorizontalBasicPortComponent",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0,
			"cursor": "default",
			"min-width": "20px",
			"min-height": "20px"
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"portInfo": {
			"lineStyle": "solid"
		}
	},
	"id": "dacb2139-009a-40d9-9b21-97c752918dd3",
	"name": "horizontal_basic_port",
	"layerName": "twoLayer",
	"componentName": "HorizontalBasicPortComponent",
	"version": "1.0.0",
	"category": "2D"
};

HeaderGroupExcelCreator.VerticalBasicPortMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10179,
			"x": 421,
			"y": 205,
			"visible": true,
			"opacity": 1,
			"width": 42,
			"height": 122,
			"rotation": 0,
			"min-width": 30,
			"min-height": 30
		},
		"label": {
			"label_using": "N",
			"label_text": "Frame Component",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0,
			"cursor": "default",
			"min-width": "30px",
			"min-height": "30px"
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"portInfo": {
			"lineStyle": "solid"
		}
	},
	"id": "c0b2b7a9-3fa4-4069-a0f4-507d381d08f4",
	"name": "net_port",
	"layerName": "twoLayer",
	"componentName": "VerticalBasicPortComponent",
	"version": "1.0.0",
	"category": "2D"
};