let HEADER_GROUP= {
      SINGLE_L2_LINK:"SingleL2Link",
      DUAL_L2_LINK:"DualL2Link",
      SINGLE_L3_LINK:"SingleL3Link",
      DUAL_L3_LINK:"DualL3Link",
      TWO_TIER_L2_LINK_SQUARE:"2TierL2LinkSquare",
      TWO_TIER_L2_LINK_MESH:"2TierL2LinkMesh",
      TWO_TIER_L3_LINK_SQUARE:"2TierL3LinkSquare",
      TWO_TIER_L3_LINK_MESH:"2TierL3LinkMesh",
}


/*
plugin은 모두 mediator의 view로 등록됨.
 */
class SKNetworkManager extends ExtensionPluginCore {

      get linkComponentName(){
            return this._linkComponentName;
      }

      get targetComponentName(){
            return this._targetComponentName;
      }
      get targetStateIconName(){
            return this._targetStateIconName;
      }
      constructor() {
            super();
            this._$bus = new Vue();
            this._linkComponentName = "NetworkLinkComponent";
            // LinkComponent에 연결할 수 있는 컴포넌트 타입(이름)
            this._targetComponentName = "NetworkStateComponent";
            this._targetStateIconName="router_component";
            this._selectProxy = null;
      }


      start() {
            window.wemb.skNetworkManager = SKNetworkManager._instance;

            /*
		에디터 모드에서만 템플릿 등록하기
		 */
            if (wemb.configManager.isEditorMode) {
                  ForeignPackTemplateRegister.regist();
                  window.wemb.hookManager.addAction(HookManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE, this.onCreateNetworkGroup.bind(this));
            }
      }


      setFacade(facade) {
            super.setFacade(facade);
            this._selectProxy = this._facade.retrieveProxy(SelectProxy.NAME);
      }

      get $bus() {
            return this._$bus;
      }

      $on(eventName, data) {
            return this._$bus.$on(eventName, data);
      }


      destroy() {
      }

      get notificationList() {
            return [
                  SelectProxy.NOTI_UPDATE_PROPERTIES
            ]
      }


      /*
	  noti가 온 경우 실행
	  call : mediator에서 실행
	   */
      handleNotification(note) {
            switch (note.name) {
                  // 편집 페이지가 열릴때마다 실행
                  case SelectProxy.NOTI_UPDATE_PROPERTIES:
                        this.noti_updateProperties();
            }

      }


      initProperties() {
      }

      /*
	컴포넌트 프로퍼티가 업데이트되면 실행.

	컴포넌트 중 링크 컴포넌트에 연결된 컴포넌트 위치가 변경되는 경우에만
	링크 컴포넌트를 찾아 업데이트 처리한다.
	방법:
		단계01: 현재 선택되어 있는 컴포넌트 중에서 링크 컴포넌트에 연결할 수 있는 컴포넌트만 찾는다.
		단계02: 단계01에서 찾은 타겟요소가 속한  LinkComponent를 찾는다.
		단계03: 단계02에서 찾은 LinkComponent를 업데이트 처리한다.


	 */
      noti_updateProperties() {

            // 단계01: 현재 선택되어 있는 컴포넌트 중에서 링크 컴포넌트에 연결할 수 있는 컴포넌트만 찾는다.
            let tempTargetComInstanceList = this._selectProxy.currentPropertyManager.getInstanceList().filter((comInstance)=>{
                  if(comInstance.componentName==this._targetComponentName){
                        return true;
                  }

                  return false;
            })


            // 선택한 컴포넌트 중 타겟 컴포넌트 인스턴스가 0이면 취소 처리
            if(tempTargetComInstanceList.length<=0){
                  return;
            }




            // 단계02: 링크 컴포넌트만들 골라 낸다.
            let linkComInstanceList = wemb.editorProxy.twoLayerInstanceList.filter((comInstance)=>{

                  if(comInstance.componentName==this._linkComponentName){
                        return true;
                  }else {
                        return false;
                  }
            })

            if(linkComInstanceList.length<=0){
                  return;
            }




            // 단계03: 단계02중 단계01을 가진 컴포넌트너 골라내기
            let tempLinkComInstanceList = linkComInstanceList.filter((linkComInstance)=>{

                  let linkCom=tempTargetComInstanceList.find((targetComInstance)=>{
                        if(linkComInstance.toTarget == targetComInstance.name)
                              return true;

                        if(linkComInstance.fromTarget == targetComInstance.name)
                              return true;

                        return false;

                  })

                  if(linkCom)
                        return true;

                  return false;

            })


            // 연결된 링크 컴포넌트만 업데이트 처리
            if(tempLinkComInstanceList){
                  tempLinkComInstanceList.forEach((linkComInstance)=>{
                        linkComInstance.updateLinkLine();
                  });
                  wemb.editorProxy.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE)
            }
      }
      /////////////////////////////////////////////////////////////







      /////////////////////////////////////////////////////////////
      /*
	initProperties = {
		componentName: string,
		label:string,
		version: string,
		category: string,

		id: string,
		name: string,
		layerName: string,

		props?: any
	}
	 */
      async onCreateNetworkGroup(comVO){
            try {

                  if(comVO.hasOwnProperty("group_key")==false){
                        return true;
                  }

                  let groupKey = comVO.group_key;
                  let drawManager = null;

                  switch(groupKey){
                        case HEADER_GROUP.SINGLE_L2_LINK :
                              drawManager = new SingleL2LinkHeader();
                              break;
                        case HEADER_GROUP.DUAL_L2_LINK :
                              drawManager = new DualL2LinkHeader();
                              break;
                        case HEADER_GROUP.SINGLE_L3_LINK :
                              drawManager = new SingleL3LinkHeader();
                              break;
                        case HEADER_GROUP.DUAL_L3_LINK :
                              drawManager = new DualL3LinkHeader();
                              break;


                        case HEADER_GROUP.TWO_TIER_L2_LINK_SQUARE :
                              drawManager = new TwoTierL2LinkSquareHeader();
                              break;

                        case HEADER_GROUP.TWO_TIER_L2_LINK_MESH :
                              drawManager = new TwoTierL2LinkMeshHeader();
                              break;

                        case HEADER_GROUP.TWO_TIER_L3_LINK_SQUARE :
                              drawManager = new TwoTierL3LinkSquareHeader();
                              break;

                        case HEADER_GROUP.TWO_TIER_L3_LINK_MESH :
                              drawManager = new TwoTierL3LinkMeshHeader();
                              break;

                  }



                  if(drawManager){
                        drawManager.setStartXY(comVO.props.setter.x, comVO.props.setter.y);
                        let result = await drawManager.draw();
                        if(result==true)
                              return false;
                  }



            }catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
                  return true;
            }

            return true;

      }

}


class HeaderGroupCreator {

      constructor(){
            this._startX = 0;
            this._startY = 0;
            this._rowWidth =0;
            this._rowHeight=0;
            this._rowCount = 1;
            this._columnCount =2;
            this._columnWidth =0;
            this._columnHalfWidth = 0;
            this.setLayout(500,100,1,2);
      }





      _getNextStateInstanceName(){

            let name=null;
            try {

                  let comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("two_layer", "Backbone");
                  if (comPanelInfo) {
                        let separator = window.wemb.componentLibraryManager.getInstanceSeparator(comPanelInfo.name);
                        let count = wemb.editorProxy.getNextInstanceCount(comPanelInfo.name);

                        name = separator + "_" + count;


                  } else {
                        console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.")
                  }
            }
            catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
            }

            return name;
      }



      // 컬럼에 해당하는 x 위치 값 구하기
      _getX(column, stateWidth){
            // column 중앙 위치
            let x = this._startX+ (this._rowWidth*(column))+this._columnHalfWidth;

            // stateWidth가 컬럼의 중앙에 올수 있게 stateWidth의 반절 값을 빼준다.
            x = x-(stateWidth/2);

            return x;
      }

      // 컬럼에 해당하는 y 위치 값 구하기
      _getY(row){
            // column 중앙 위치
            let y = this._startY+ (this._rowHeight*(row));

            return y;
      }



      _addComponent(comVO){
            window.wemb.editorFacade.sendNotification(EditorStatic.CMD_ADD_COM_INSTANCE_BY_NAME, comVO);
            return new Promise((resolve, reject)=>{
                  setTimeout(()=>{
                        resolve(true)
                  },100)
            });

      }


      setStartXY(startX, startY){
            this._startX = startX;
            this._startY = startY;
      }

      setLayout(rowWidth, rowHeight, rowCount,columnCount){
            this._rowWidth = rowWidth;
            this._rowHeight = rowHeight;
            this._rowCount = rowCount;
            this._columnCount = columnCount;
            this._columnWidth = rowWidth/columnCount;
            this._columnHalfWidth = this._columnWidth/2;
      }

      /*
	false가 리턴되는 경우
	 */
      async addState(row, column, comPanelLabelName="Backbone"){
            let componentVO = null;

            try {

                  let comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("two_layer", comPanelLabelName);
                  if (comPanelInfo) {
                        componentVO = {};
                        componentVO.componentName = comPanelInfo.name;
                        componentVO.label = comPanelInfo.label;
                        componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                        componentVO.initProperties.props.setter.x = this._getX(column, componentVO.initProperties.props.setter.width);
                        componentVO.initProperties.props.setter.y = this._getY(row);

                        componentVO.id = WeMB.ObjectUtil.generateID();
                        let separator = window.wemb.componentLibraryManager.getInstanceSeparator(comPanelInfo.name);
                        let count = wemb.editorProxy.getNextInstanceCount(comPanelInfo.name);

                        componentVO.name = separator + "_" + count;

                        await this._addComponent(componentVO);



                  } else {
                        console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.")
                  }
            }
            catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
            }

            return componentVO;
      }

      async addLine(fromComInstanceName, toComInstanceName, gap=0){

            let componentVO = null;

            try {
                  let comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("two_layer", "NetworkLinkComponent");
                  if (comPanelInfo) {
                        componentVO = {}
                        componentVO.componentName = comPanelInfo.name;
                        componentVO.label = comPanelInfo.label;
                        componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                        componentVO.initProperties.props.network = {
                              "gap": gap,
                              "from": {
                                    "ip": "",
                                    "hostName": "",
                                    "port": "",
                                    "target": fromComInstanceName
                              },
                              "to": {
                                    "ip": "",
                                    "hostName":"" ,
                                    "port": "",
                                    "target": toComInstanceName
                              }
                        }
                        // componentVO.initProperties.props.network.from.target = fromComInstanceName;
                        //componentVO.initProperties.props.network.to.target = toComInstanceName;

                        await this._addComponent(componentVO);


                  } else {
                        console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.")
                  }
            }catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
            }

            return componentVO;
      }

      async addCircle(firstLineComInstanceInfo, secondLineComInstanceInfo){
            let componentVO = null;

            try {
                  let comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("two_layer", "SVGCircleComponent");
                  let firstProps = firstLineComInstanceInfo.initProperties.props;
                  let secondProps = secondLineComInstanceInfo.initProperties.props;


                  let width=30;
                  let height=60;
                  let x = firstProps.setter.x+((secondProps.setter.x-firstProps.setter.x+firstProps.setter.width)/2);
                  x-=(width/2);
                  let y = firstProps.setter.y+(firstProps.setter.height/2);
                  y-=(height/2);

                  if (comPanelInfo) {
                        componentVO = {}
                        componentVO.componentName = comPanelInfo.name;
                        componentVO.label = comPanelInfo.label;
                        componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                        componentVO.initProperties.props.setter = {
                              x:x,
                              y:y,
                              width:width,
                              height:height
                        }

                        componentVO.initProperties.props.style={
                              stroke:"rgb(0,0,0)",
                              fill:"rgba(255,255,255,0)",
                              "stroke-width":2
                        }


                        await this._addComponent(componentVO);


                  } else {
                        console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.")
                  }
            }catch(error){
                  console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
            }

            return componentVO;
      }
      async draw(){

      }

}



class SingleL2LinkHeader extends HeaderGroupCreator {

      constructor(){
            super();
      }
      async draw(){
            let formVO = await this.addState(0,0, "Switch01");
            if(formVO==null){
                  return false;
            }

            let toVO = await this.addState(0,1, "Switch01");
            if(toVO==null){
                  return false;
            }
            if(await this.addLine(formVO.name, toVO.name)==null){
                  return false;
            }

            return true;
      }
}


class DualL2LinkHeader extends HeaderGroupCreator {

      constructor(){
            super();
      }
      async draw(){
            let formVO = await this.addState(0,0,"Switch01");
            if(formVO==null){
                  return false;
            }

            let toVO = await this.addState(0,1,"Switch01");
            if(toVO==null){
                  return false;
            }

            let firstLineVO=await this.addLine(formVO.name, toVO.name, -10);
            if(firstLineVO==null){
                  return false;
            }

            let secondLineVO = await this.addLine(toVO.name, formVO.name, 10);
            if(secondLineVO==null){
                  return false;
            }

            if(await this.addCircle(formVO, toVO)==null){
                  return false;
            }


            return true;
      }
}




class SingleL3LinkHeader extends HeaderGroupCreator {

      constructor(){
            super();
      }
      async draw(){
            let formVO = await this.addState(0,0, "Switch02");
            if(formVO==null){
                  return false;
            }

            let toVO = await this.addState(0,1, "Switch02");
            if(toVO==null){
                  return false;
            }
            if(await this.addLine(formVO.name, toVO.name)==null){
                  return false;
            }

            return true;
      }
}


class DualL3LinkHeader extends HeaderGroupCreator {

      constructor(){
            super();
      }
      async draw(){
            let formVO = await this.addState(0,0,"Switch02");
            if(formVO==null){
                  return false;
            }

            let toVO = await this.addState(0,1,"Switch02");
            if(toVO==null){
                  return false;
            }

            let firstLineVO=await this.addLine(formVO.name, toVO.name, -10);
            if(firstLineVO==null){
                  return false;
            }

            let secondLineVO = await this.addLine(toVO.name, formVO.name, 10);
            if(secondLineVO==null){
                  return false;
            }

            if(await this.addCircle(formVO, toVO)==null){
                  return false;
            }


            return true;
      }
}

class TwoTierL2LinkSquareHeader extends HeaderGroupCreator {

      constructor(){
            super();
            this.setLayout(500,200,2,2);
      }
      async draw(){
            let state0_0 = await this.addState(0,0, "Switch01");
            if(state0_0==null){
                  return false;
            }

            let state0_1 = await this.addState(0,1, "Switch01");
            if(state0_1==null){
                  return false;
            }



            let state1_0 = await this.addState(1,0, "Switch01");
            if(state1_0==null){
                  return false;
            }

            let state1_1 = await this.addState(1,1, "Switch01");
            if(state1_1==null){
                  return false;
            }




            /*
		상위 2선 + O 추가
		 */
            if(await this.addLine(state0_0.name, state0_1.name, -10)==null){
                  return false;
            }

            if(await this.addLine(state0_1.name, state0_0.name, 10)==null){
                  return false;
            }

            if(await this.addCircle(state0_0, state0_1)==null){
                  return false;
            }


            /*
		하위 2선 + O 추가
		 */
            if(await this.addLine(state1_0.name, state1_1.name, -10)==null){
                  return false;
            }

            if(await this.addLine(state1_1.name, state1_0.name, 10)==null){
                  return false;
            }

            if(await this.addCircle(state1_0, state1_1)==null){
                  return false;
            }







            /*
		| | 표시
		*/
            if(await this.addLine(state0_0.name, state1_0.name)==null){
                  return false;
            }

            if(await this.addLine(state0_1.name, state1_1.name)==null){
                  return false;
            }
            return true;
      }
}



class TwoTierL2LinkMeshHeader extends HeaderGroupCreator {

      constructor(){
            super();
            this.setLayout(500,200,2,2);
      }
      async draw(){

            let state0_0 = await this.addState(0,0, "Switch01");
            if(state0_0==null){
                  return false;
            }

            let state0_1 = await this.addState(0,1, "Switch01");
            if(state0_1==null){
                  return false;
            }



            let state1_0 = await this.addState(1,0, "Switch01");
            if(state1_0==null){
                  return false;
            }

            let state1_1 = await this.addState(1,1, "Switch01");
            if(state1_1==null){
                  return false;
            }




            /*
		상위 2선 + O 추가
		 */
            if(await this.addLine(state0_0.name, state0_1.name, -10)==null){
                  return false;
            }

            if(await this.addLine(state0_1.name, state0_0.name, 10)==null){
                  return false;
            }

            if(await this.addCircle(state0_0, state0_1)==null){
                  return false;
            }


            /*
		하위 2선 + O 추가
		 */
            if(await this.addLine(state1_0.name, state1_1.name, -10)==null){
                  return false;
            }

            if(await this.addLine(state1_1.name, state1_0.name, 10)==null){
                  return false;
            }

            if(await this.addCircle(state1_0, state1_1)==null){
                  return false;
            }







            /*
		| | 표시
		*/
            if(await this.addLine(state0_0.name, state1_0.name)==null){
                  return false;
            }

            if(await this.addLine(state0_1.name, state1_1.name)==null){
                  return false;
            }


            /*
		x표시
		 */
            if(await this.addLine(state0_0.name, state1_1.name)==null){
                  return false;
            }

            if(await this.addLine(state1_0.name, state0_1.name)==null){
                  return false;
            }
            return true;


      }
}




class TwoTierL3LinkSquareHeader extends HeaderGroupCreator {

      constructor(){
            super();
            this.setLayout(500,200,2,2);
      }
      async draw(){
            let state0_0 = await this.addState(0,0, "Switch02");
            if(state0_0==null){
                  return false;
            }

            let state0_1 = await this.addState(0,1, "Switch02");
            if(state0_1==null){
                  return false;
            }



            let state1_0 = await this.addState(1,0, "Switch02");
            if(state1_0==null){
                  return false;
            }

            let state1_1 = await this.addState(1,1, "Switch02");
            if(state1_1==null){
                  return false;
            }




            /*
		상위 2선 + O 추가
		 */
            if(await this.addLine(state0_0.name, state0_1.name, -10)==null){
                  return false;
            }

            if(await this.addLine(state0_1.name, state0_0.name, 10)==null){
                  return false;
            }

            if(await this.addCircle(state0_0, state0_1)==null){
                  return false;
            }


            /*
		하위 2선 + O 추가
		 */
            if(await this.addLine(state1_0.name, state1_1.name, -10)==null){
                  return false;
            }

            if(await this.addLine(state1_1.name, state1_0.name, 10)==null){
                  return false;
            }

            if(await this.addCircle(state1_0, state1_1)==null){
                  return false;
            }







            /*
		| | 표시
		*/
            if(await this.addLine(state0_0.name, state1_0.name)==null){
                  return false;
            }

            if(await this.addLine(state0_1.name, state1_1.name)==null){
                  return false;
            }
            return true;
      }
}



class TwoTierL3LinkMeshHeader extends HeaderGroupCreator {

      constructor(){
            super();
            this.setLayout(500,200,2,2);
      }
      async draw(){

            let state0_0 = await this.addState(0,0, "Switch02");
            if(state0_0==null){
                  return false;
            }

            let state0_1 = await this.addState(0,1, "Switch02");
            if(state0_1==null){
                  return false;
            }



            let state1_0 = await this.addState(1,0, "Switch02");
            if(state1_0==null){
                  return false;
            }

            let state1_1 = await this.addState(1,1, "Switch02");
            if(state1_1==null){
                  return false;
            }




            /*
		상위 2선 + O 추가
		 */
            if(await this.addLine(state0_0.name, state0_1.name, -10)==null){
                  return false;
            }

            if(await this.addLine(state0_1.name, state0_0.name, 10)==null){
                  return false;
            }

            if(await this.addCircle(state0_0, state0_1)==null){
                  return false;
            }


            /*
		하위 2선 + O 추가
		 */
            if(await this.addLine(state1_0.name, state1_1.name, -10)==null){
                  return false;
            }

            if(await this.addLine(state1_1.name, state1_0.name, 10)==null){
                  return false;
            }

            if(await this.addCircle(state1_0, state1_1)==null){
                  return false;
            }







            /*
		| | 표시
		*/
            if(await this.addLine(state0_0.name, state1_0.name)==null){
                  return false;
            }

            if(await this.addLine(state0_1.name, state1_1.name)==null){
                  return false;
            }


            /*
		x표시
		 */
            if(await this.addLine(state0_0.name, state1_1.name)==null){
                  return false;
            }

            if(await this.addLine(state1_0.name, state0_1.name)==null){
                  return false;
            }
            return true;


      }
}
