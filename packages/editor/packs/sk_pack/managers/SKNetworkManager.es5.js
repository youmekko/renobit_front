"use strict";

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HEADER_GROUP = {
      SINGLE_L2_LINK: "SingleL2Link",
      DUAL_L2_LINK: "DualL2Link",
      SINGLE_L3_LINK: "SingleL3Link",
      DUAL_L3_LINK: "DualL3Link",
      TWO_TIER_L2_LINK_SQUARE: "2TierL2LinkSquare",
      TWO_TIER_L2_LINK_MESH: "2TierL2LinkMesh",
      TWO_TIER_L3_LINK_SQUARE: "2TierL3LinkSquare",
      TWO_TIER_L3_LINK_MESH: "2TierL3LinkMesh"

      /*
 plugin은 모두 mediator의 view로 등록됨.
  */
};
var SKNetworkManager = function (_ExtensionPluginCore) {
      _inherits(SKNetworkManager, _ExtensionPluginCore);

      _createClass(SKNetworkManager, [{
            key: "linkComponentName",
            get: function get() {
                  return this._linkComponentName;
            }
      }, {
            key: "targetComponentName",
            get: function get() {
                  return this._targetComponentName;
            }
      }, {
            key: "targetStateIconName",
            get: function get() {
                  return this._targetStateIconName;
            }
      }]);

      function SKNetworkManager() {
            _classCallCheck(this, SKNetworkManager);

            var _this = _possibleConstructorReturn(this, (SKNetworkManager.__proto__ || Object.getPrototypeOf(SKNetworkManager)).call(this));

            _this._$bus = new Vue();
            _this._linkComponentName = "NetworkLinkComponent";
            // LinkComponent에 연결할 수 있는 컴포넌트 타입(이름)
            _this._targetComponentName = "NetworkStateComponent";
            _this._targetStateIconName = "router_component";
            _this._selectProxy = null;
            return _this;
      }

      _createClass(SKNetworkManager, [{
            key: "start",
            value: function start() {
                  window.wemb.skNetworkManager = SKNetworkManager._instance;

                  /*
   에디터 모드에서만 템플릿 등록하기
    */
                  if (wemb.configManager.isEditorMode) {
                        ForeignPackTemplateRegister.regist();
                        window.wemb.hookManager.addAction(HookManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE, this.onCreateNetworkGroup.bind(this));
                  }
            }
      }, {
            key: "setFacade",
            value: function setFacade(facade) {
                  _get(SKNetworkManager.prototype.__proto__ || Object.getPrototypeOf(SKNetworkManager.prototype), "setFacade", this).call(this, facade);
                  this._selectProxy = this._facade.retrieveProxy(SelectProxy.NAME);
            }
      }, {
            key: "$on",
            value: function $on(eventName, data) {
                  return this._$bus.$on(eventName, data);
            }
      }, {
            key: "destroy",
            value: function destroy() {}
      }, {
            key: "handleNotification",


            /*
    noti가 온 경우 실행
    call : mediator에서 실행
     */
            value: function handleNotification(note) {
                  switch (note.name) {
                        // 편집 페이지가 열릴때마다 실행
                        case SelectProxy.NOTI_UPDATE_PROPERTIES:
                              this.noti_updateProperties();
                  }
            }
      }, {
            key: "initProperties",
            value: function initProperties() {}

            /*
  컴포넌트 프로퍼티가 업데이트되면 실행.
	  컴포넌트 중 링크 컴포넌트에 연결된 컴포넌트 위치가 변경되는 경우에만
	 링크 컴포넌트를 찾아 업데이트 처리한다.
	 방법:
		 단계01: 현재 선택되어 있는 컴포넌트 중에서 링크 컴포넌트에 연결할 수 있는 컴포넌트만 찾는다.
		 단계02: 단계01에서 찾은 타겟요소가 속한  LinkComponent를 찾는다.
		 단계03: 단계02에서 찾은 LinkComponent를 업데이트 처리한다.

   */

      }, {
            key: "noti_updateProperties",
            value: function noti_updateProperties() {
                  var _this2 = this;

                  // 단계01: 현재 선택되어 있는 컴포넌트 중에서 링크 컴포넌트에 연결할 수 있는 컴포넌트만 찾는다.
                  var tempTargetComInstanceList = this._selectProxy.currentPropertyManager.getInstanceList().filter(function (comInstance) {
                        if (comInstance.componentName == _this2._targetComponentName) {
                              return true;
                        }

                        return false;
                  });

                  // 선택한 컴포넌트 중 타겟 컴포넌트 인스턴스가 0이면 취소 처리
                  if (tempTargetComInstanceList.length <= 0) {
                        return;
                  }

                  // 단계02: 링크 컴포넌트만들 골라 낸다.
                  var linkComInstanceList = wemb.editorProxy.twoLayerInstanceList.filter(function (comInstance) {

                        if (comInstance.componentName == _this2._linkComponentName) {
                              return true;
                        } else {
                              return false;
                        }
                  });

                  if (linkComInstanceList.length <= 0) {
                        return;
                  }

                  // 단계03: 단계02중 단계01을 가진 컴포넌트너 골라내기
                  var tempLinkComInstanceList = linkComInstanceList.filter(function (linkComInstance) {

                        var linkCom = tempTargetComInstanceList.find(function (targetComInstance) {
                              if (linkComInstance.toTarget == targetComInstance.name) return true;

                              if (linkComInstance.fromTarget == targetComInstance.name) return true;

                              return false;
                        });

                        if (linkCom) return true;

                        return false;
                  });

                  // 연결된 링크 컴포넌트만 업데이트 처리
                  if (tempLinkComInstanceList) {
                        tempLinkComInstanceList.forEach(function (linkComInstance) {
                              linkComInstance.updateLinkLine();
                        });
                        wemb.editorProxy.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE);
                  }
            }
            /////////////////////////////////////////////////////////////


            /////////////////////////////////////////////////////////////
            /*
  initProperties = {
	  componentName: string,
	  label:string,
	  version: string,
	  category: string,
		  id: string,
	  name: string,
	  layerName: string,
		  props?: any
  }
   */

      }, {
            key: "onCreateNetworkGroup",
            value: function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(comVO) {
                        var groupKey, drawManager, result;
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                _context.prev = 0;

                                                if (!(comVO.hasOwnProperty("group_key") == false)) {
                                                      _context.next = 3;
                                                      break;
                                                }

                                                return _context.abrupt("return", true);

                                          case 3:
                                                groupKey = comVO.group_key;
                                                drawManager = null;
                                                _context.t0 = groupKey;
                                                _context.next = _context.t0 === HEADER_GROUP.SINGLE_L2_LINK ? 8 : _context.t0 === HEADER_GROUP.DUAL_L2_LINK ? 10 : _context.t0 === HEADER_GROUP.SINGLE_L3_LINK ? 12 : _context.t0 === HEADER_GROUP.DUAL_L3_LINK ? 14 : _context.t0 === HEADER_GROUP.TWO_TIER_L2_LINK_SQUARE ? 16 : _context.t0 === HEADER_GROUP.TWO_TIER_L2_LINK_MESH ? 18 : _context.t0 === HEADER_GROUP.TWO_TIER_L3_LINK_SQUARE ? 20 : _context.t0 === HEADER_GROUP.TWO_TIER_L3_LINK_MESH ? 22 : 24;
                                                break;

                                          case 8:
                                                drawManager = new SingleL2LinkHeader();
                                                return _context.abrupt("break", 24);

                                          case 10:
                                                drawManager = new DualL2LinkHeader();
                                                return _context.abrupt("break", 24);

                                          case 12:
                                                drawManager = new SingleL3LinkHeader();
                                                return _context.abrupt("break", 24);

                                          case 14:
                                                drawManager = new DualL3LinkHeader();
                                                return _context.abrupt("break", 24);

                                          case 16:
                                                drawManager = new TwoTierL2LinkSquareHeader();
                                                return _context.abrupt("break", 24);

                                          case 18:
                                                drawManager = new TwoTierL2LinkMeshHeader();
                                                return _context.abrupt("break", 24);

                                          case 20:
                                                drawManager = new TwoTierL3LinkSquareHeader();
                                                return _context.abrupt("break", 24);

                                          case 22:
                                                drawManager = new TwoTierL3LinkMeshHeader();
                                                return _context.abrupt("break", 24);

                                          case 24:
                                                if (!drawManager) {
                                                      _context.next = 31;
                                                      break;
                                                }

                                                drawManager.setStartXY(comVO.props.setter.x, comVO.props.setter.y);
                                                _context.next = 28;
                                                return drawManager.draw();

                                          case 28:
                                                result = _context.sent;

                                                if (!(result == true)) {
                                                      _context.next = 31;
                                                      break;
                                                }

                                                return _context.abrupt("return", false);

                                          case 31:
                                                _context.next = 37;
                                                break;

                                          case 33:
                                                _context.prev = 33;
                                                _context.t1 = _context["catch"](0);

                                                console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", _context.t1);
                                                return _context.abrupt("return", true);

                                          case 37:
                                                return _context.abrupt("return", true);

                                          case 38:
                                          case "end":
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this, [[0, 33]]);
                  }));

                  function onCreateNetworkGroup(_x) {
                        return _ref.apply(this, arguments);
                  }

                  return onCreateNetworkGroup;
            }()
      }, {
            key: "$bus",
            get: function get() {
                  return this._$bus;
            }
      }, {
            key: "notificationList",
            get: function get() {
                  return [SelectProxy.NOTI_UPDATE_PROPERTIES];
            }
      }]);

      return SKNetworkManager;
}(ExtensionPluginCore);

var HeaderGroupCreator = function () {
      function HeaderGroupCreator() {
            _classCallCheck(this, HeaderGroupCreator);

            this._startX = 0;
            this._startY = 0;
            this._rowWidth = 0;
            this._rowHeight = 0;
            this._rowCount = 1;
            this._columnCount = 2;
            this._columnWidth = 0;
            this._columnHalfWidth = 0;
            this.setLayout(500, 100, 1, 2);
      }

      _createClass(HeaderGroupCreator, [{
            key: "_getNextStateInstanceName",
            value: function _getNextStateInstanceName() {

                  var name = null;
                  try {

                        var comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("two_layer", "Backbone");
                        if (comPanelInfo) {
                              var separator = window.wemb.componentLibraryManager.getInstanceSeparator(comPanelInfo.name);
                              var count = wemb.editorProxy.getNextInstanceCount(comPanelInfo.name);

                              name = separator + "_" + count;
                        } else {
                              console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.");
                        }
                  } catch (error) {
                        console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", error);
                  }

                  return name;
            }

            // 컬럼에 해당하는 x 위치 값 구하기

      }, {
            key: "_getX",
            value: function _getX(column, stateWidth) {
                  // column 중앙 위치
                  var x = this._startX + this._rowWidth * column + this._columnHalfWidth;

                  // stateWidth가 컬럼의 중앙에 올수 있게 stateWidth의 반절 값을 빼준다.
                  x = x - stateWidth / 2;

                  return x;
            }

            // 컬럼에 해당하는 y 위치 값 구하기

      }, {
            key: "_getY",
            value: function _getY(row) {
                  // column 중앙 위치
                  var y = this._startY + this._rowHeight * row;

                  return y;
            }
      }, {
            key: "_addComponent",
            value: function _addComponent(comVO) {
                  window.wemb.editorFacade.sendNotification(EditorStatic.CMD_ADD_COM_INSTANCE_BY_NAME, comVO);
                  return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                              resolve(true);
                        }, 100);
                  });
            }
      }, {
            key: "setStartXY",
            value: function setStartXY(startX, startY) {
                  this._startX = startX;
                  this._startY = startY;
            }
      }, {
            key: "setLayout",
            value: function setLayout(rowWidth, rowHeight, rowCount, columnCount) {
                  this._rowWidth = rowWidth;
                  this._rowHeight = rowHeight;
                  this._rowCount = rowCount;
                  this._columnCount = columnCount;
                  this._columnWidth = rowWidth / columnCount;
                  this._columnHalfWidth = this._columnWidth / 2;
            }

            /*
  false가 리턴되는 경우
   */

      }, {
            key: "addState",
            value: function () {
                  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(row, column) {
                        var comPanelLabelName = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "Backbone";
                        var componentVO, comPanelInfo, separator, count;
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                              while (1) {
                                    switch (_context2.prev = _context2.next) {
                                          case 0:
                                                componentVO = null;
                                                _context2.prev = 1;
                                                comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("two_layer", comPanelLabelName);

                                                if (!comPanelInfo) {
                                                      _context2.next = 18;
                                                      break;
                                                }

                                                componentVO = {};
                                                componentVO.componentName = comPanelInfo.name;
                                                componentVO.label = comPanelInfo.label;
                                                componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                                                componentVO.initProperties.props.setter.x = this._getX(column, componentVO.initProperties.props.setter.width);
                                                componentVO.initProperties.props.setter.y = this._getY(row);

                                                componentVO.id = WeMB.ObjectUtil.generateID();
                                                separator = window.wemb.componentLibraryManager.getInstanceSeparator(comPanelInfo.name);
                                                count = wemb.editorProxy.getNextInstanceCount(comPanelInfo.name);


                                                componentVO.name = separator + "_" + count;

                                                _context2.next = 16;
                                                return this._addComponent(componentVO);

                                          case 16:
                                                _context2.next = 19;
                                                break;

                                          case 18:
                                                console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.");

                                          case 19:
                                                _context2.next = 24;
                                                break;

                                          case 21:
                                                _context2.prev = 21;
                                                _context2.t0 = _context2["catch"](1);

                                                console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", _context2.t0);

                                          case 24:
                                                return _context2.abrupt("return", componentVO);

                                          case 25:
                                          case "end":
                                                return _context2.stop();
                                    }
                              }
                        }, _callee2, this, [[1, 21]]);
                  }));

                  function addState(_x2, _x3) {
                        return _ref2.apply(this, arguments);
                  }

                  return addState;
            }()
      }, {
            key: "addLine",
            value: function () {
                  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(fromComInstanceName, toComInstanceName) {
                        var gap = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
                        var componentVO, comPanelInfo;
                        return regeneratorRuntime.wrap(function _callee3$(_context3) {
                              while (1) {
                                    switch (_context3.prev = _context3.next) {
                                          case 0:
                                                componentVO = null;
                                                _context3.prev = 1;
                                                comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("two_layer", "NetworkLinkComponent");

                                                if (!comPanelInfo) {
                                                      _context3.next = 13;
                                                      break;
                                                }

                                                componentVO = {};
                                                componentVO.componentName = comPanelInfo.name;
                                                componentVO.label = comPanelInfo.label;
                                                componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                                                componentVO.initProperties.props.network = {
                                                      "gap": gap,
                                                      "from": {
                                                            "ip": "",
                                                            "hostName": "",
                                                            "port": "",
                                                            "target": fromComInstanceName
                                                      },
                                                      "to": {
                                                            "ip": "",
                                                            "hostName": "",
                                                            "port": "",
                                                            "target": toComInstanceName
                                                      }
                                                      // componentVO.initProperties.props.network.from.target = fromComInstanceName;
                                                      //componentVO.initProperties.props.network.to.target = toComInstanceName;

                                                };_context3.next = 11;
                                                return this._addComponent(componentVO);

                                          case 11:
                                                _context3.next = 14;
                                                break;

                                          case 13:
                                                console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.");

                                          case 14:
                                                _context3.next = 19;
                                                break;

                                          case 16:
                                                _context3.prev = 16;
                                                _context3.t0 = _context3["catch"](1);

                                                console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", _context3.t0);

                                          case 19:
                                                return _context3.abrupt("return", componentVO);

                                          case 20:
                                          case "end":
                                                return _context3.stop();
                                    }
                              }
                        }, _callee3, this, [[1, 16]]);
                  }));

                  function addLine(_x5, _x6) {
                        return _ref3.apply(this, arguments);
                  }

                  return addLine;
            }()
      }, {
            key: "addCircle",
            value: function () {
                  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(firstLineComInstanceInfo, secondLineComInstanceInfo) {
                        var componentVO, comPanelInfo, firstProps, secondProps, width, height, x, y;
                        return regeneratorRuntime.wrap(function _callee4$(_context4) {
                              while (1) {
                                    switch (_context4.prev = _context4.next) {
                                          case 0:
                                                componentVO = null;
                                                _context4.prev = 1;
                                                comPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("two_layer", "SVGCircleComponent");
                                                firstProps = firstLineComInstanceInfo.initProperties.props;
                                                secondProps = secondLineComInstanceInfo.initProperties.props;
                                                width = 30;
                                                height = 60;
                                                x = firstProps.setter.x + (secondProps.setter.x - firstProps.setter.x + firstProps.setter.width) / 2;

                                                x -= width / 2;
                                                y = firstProps.setter.y + firstProps.setter.height / 2;

                                                y -= height / 2;

                                                if (!comPanelInfo) {
                                                      _context4.next = 22;
                                                      break;
                                                }

                                                componentVO = {};
                                                componentVO.componentName = comPanelInfo.name;
                                                componentVO.label = comPanelInfo.label;
                                                componentVO.initProperties = $.extend(true, {}, comPanelInfo.initProperties);
                                                componentVO.initProperties.props.setter = {
                                                      x: x,
                                                      y: y,
                                                      width: width,
                                                      height: height
                                                };

                                                componentVO.initProperties.props.style = {
                                                      stroke: "rgb(0,0,0)",
                                                      fill: "rgba(255,255,255,0)",
                                                      "stroke-width": 2
                                                };

                                                _context4.next = 20;
                                                return this._addComponent(componentVO);

                                          case 20:
                                                _context4.next = 23;
                                                break;

                                          case 22:
                                                console.log("컴포넌트 기본 생성 정보가 존재하지 않습니다.");

                                          case 23:
                                                _context4.next = 28;
                                                break;

                                          case 25:
                                                _context4.prev = 25;
                                                _context4.t0 = _context4["catch"](1);

                                                console.log("RENOBIT  SKNetworkManager PLUGIN ERROR ", _context4.t0);

                                          case 28:
                                                return _context4.abrupt("return", componentVO);

                                          case 29:
                                          case "end":
                                                return _context4.stop();
                                    }
                              }
                        }, _callee4, this, [[1, 25]]);
                  }));

                  function addCircle(_x8, _x9) {
                        return _ref4.apply(this, arguments);
                  }

                  return addCircle;
            }()
      }, {
            key: "draw",
            value: function () {
                  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                        return regeneratorRuntime.wrap(function _callee5$(_context5) {
                              while (1) {
                                    switch (_context5.prev = _context5.next) {
                                          case 0:
                                          case "end":
                                                return _context5.stop();
                                    }
                              }
                        }, _callee5, this);
                  }));

                  function draw() {
                        return _ref5.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return HeaderGroupCreator;
}();

var SingleL2LinkHeader = function (_HeaderGroupCreator) {
      _inherits(SingleL2LinkHeader, _HeaderGroupCreator);

      function SingleL2LinkHeader() {
            _classCallCheck(this, SingleL2LinkHeader);

            return _possibleConstructorReturn(this, (SingleL2LinkHeader.__proto__ || Object.getPrototypeOf(SingleL2LinkHeader)).call(this));
      }

      _createClass(SingleL2LinkHeader, [{
            key: "draw",
            value: function () {
                  var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                        var formVO, toVO;
                        return regeneratorRuntime.wrap(function _callee6$(_context6) {
                              while (1) {
                                    switch (_context6.prev = _context6.next) {
                                          case 0:
                                                _context6.next = 2;
                                                return this.addState(0, 0, "Switch01");

                                          case 2:
                                                formVO = _context6.sent;

                                                if (!(formVO == null)) {
                                                      _context6.next = 5;
                                                      break;
                                                }

                                                return _context6.abrupt("return", false);

                                          case 5:
                                                _context6.next = 7;
                                                return this.addState(0, 1, "Switch01");

                                          case 7:
                                                toVO = _context6.sent;

                                                if (!(toVO == null)) {
                                                      _context6.next = 10;
                                                      break;
                                                }

                                                return _context6.abrupt("return", false);

                                          case 10:
                                                _context6.next = 12;
                                                return this.addLine(formVO.name, toVO.name);

                                          case 12:
                                                _context6.t0 = _context6.sent;

                                                if (!(_context6.t0 == null)) {
                                                      _context6.next = 15;
                                                      break;
                                                }

                                                return _context6.abrupt("return", false);

                                          case 15:
                                                return _context6.abrupt("return", true);

                                          case 16:
                                          case "end":
                                                return _context6.stop();
                                    }
                              }
                        }, _callee6, this);
                  }));

                  function draw() {
                        return _ref6.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return SingleL2LinkHeader;
}(HeaderGroupCreator);

var DualL2LinkHeader = function (_HeaderGroupCreator2) {
      _inherits(DualL2LinkHeader, _HeaderGroupCreator2);

      function DualL2LinkHeader() {
            _classCallCheck(this, DualL2LinkHeader);

            return _possibleConstructorReturn(this, (DualL2LinkHeader.__proto__ || Object.getPrototypeOf(DualL2LinkHeader)).call(this));
      }

      _createClass(DualL2LinkHeader, [{
            key: "draw",
            value: function () {
                  var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                        var formVO, toVO, firstLineVO, secondLineVO;
                        return regeneratorRuntime.wrap(function _callee7$(_context7) {
                              while (1) {
                                    switch (_context7.prev = _context7.next) {
                                          case 0:
                                                _context7.next = 2;
                                                return this.addState(0, 0, "Switch01");

                                          case 2:
                                                formVO = _context7.sent;

                                                if (!(formVO == null)) {
                                                      _context7.next = 5;
                                                      break;
                                                }

                                                return _context7.abrupt("return", false);

                                          case 5:
                                                _context7.next = 7;
                                                return this.addState(0, 1, "Switch01");

                                          case 7:
                                                toVO = _context7.sent;

                                                if (!(toVO == null)) {
                                                      _context7.next = 10;
                                                      break;
                                                }

                                                return _context7.abrupt("return", false);

                                          case 10:
                                                _context7.next = 12;
                                                return this.addLine(formVO.name, toVO.name, -10);

                                          case 12:
                                                firstLineVO = _context7.sent;

                                                if (!(firstLineVO == null)) {
                                                      _context7.next = 15;
                                                      break;
                                                }

                                                return _context7.abrupt("return", false);

                                          case 15:
                                                _context7.next = 17;
                                                return this.addLine(toVO.name, formVO.name, 10);

                                          case 17:
                                                secondLineVO = _context7.sent;

                                                if (!(secondLineVO == null)) {
                                                      _context7.next = 20;
                                                      break;
                                                }

                                                return _context7.abrupt("return", false);

                                          case 20:
                                                _context7.next = 22;
                                                return this.addCircle(formVO, toVO);

                                          case 22:
                                                _context7.t0 = _context7.sent;

                                                if (!(_context7.t0 == null)) {
                                                      _context7.next = 25;
                                                      break;
                                                }

                                                return _context7.abrupt("return", false);

                                          case 25:
                                                return _context7.abrupt("return", true);

                                          case 26:
                                          case "end":
                                                return _context7.stop();
                                    }
                              }
                        }, _callee7, this);
                  }));

                  function draw() {
                        return _ref7.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return DualL2LinkHeader;
}(HeaderGroupCreator);

var SingleL3LinkHeader = function (_HeaderGroupCreator3) {
      _inherits(SingleL3LinkHeader, _HeaderGroupCreator3);

      function SingleL3LinkHeader() {
            _classCallCheck(this, SingleL3LinkHeader);

            return _possibleConstructorReturn(this, (SingleL3LinkHeader.__proto__ || Object.getPrototypeOf(SingleL3LinkHeader)).call(this));
      }

      _createClass(SingleL3LinkHeader, [{
            key: "draw",
            value: function () {
                  var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                        var formVO, toVO;
                        return regeneratorRuntime.wrap(function _callee8$(_context8) {
                              while (1) {
                                    switch (_context8.prev = _context8.next) {
                                          case 0:
                                                _context8.next = 2;
                                                return this.addState(0, 0, "Switch02");

                                          case 2:
                                                formVO = _context8.sent;

                                                if (!(formVO == null)) {
                                                      _context8.next = 5;
                                                      break;
                                                }

                                                return _context8.abrupt("return", false);

                                          case 5:
                                                _context8.next = 7;
                                                return this.addState(0, 1, "Switch02");

                                          case 7:
                                                toVO = _context8.sent;

                                                if (!(toVO == null)) {
                                                      _context8.next = 10;
                                                      break;
                                                }

                                                return _context8.abrupt("return", false);

                                          case 10:
                                                _context8.next = 12;
                                                return this.addLine(formVO.name, toVO.name);

                                          case 12:
                                                _context8.t0 = _context8.sent;

                                                if (!(_context8.t0 == null)) {
                                                      _context8.next = 15;
                                                      break;
                                                }

                                                return _context8.abrupt("return", false);

                                          case 15:
                                                return _context8.abrupt("return", true);

                                          case 16:
                                          case "end":
                                                return _context8.stop();
                                    }
                              }
                        }, _callee8, this);
                  }));

                  function draw() {
                        return _ref8.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return SingleL3LinkHeader;
}(HeaderGroupCreator);

var DualL3LinkHeader = function (_HeaderGroupCreator4) {
      _inherits(DualL3LinkHeader, _HeaderGroupCreator4);

      function DualL3LinkHeader() {
            _classCallCheck(this, DualL3LinkHeader);

            return _possibleConstructorReturn(this, (DualL3LinkHeader.__proto__ || Object.getPrototypeOf(DualL3LinkHeader)).call(this));
      }

      _createClass(DualL3LinkHeader, [{
            key: "draw",
            value: function () {
                  var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
                        var formVO, toVO, firstLineVO, secondLineVO;
                        return regeneratorRuntime.wrap(function _callee9$(_context9) {
                              while (1) {
                                    switch (_context9.prev = _context9.next) {
                                          case 0:
                                                _context9.next = 2;
                                                return this.addState(0, 0, "Switch02");

                                          case 2:
                                                formVO = _context9.sent;

                                                if (!(formVO == null)) {
                                                      _context9.next = 5;
                                                      break;
                                                }

                                                return _context9.abrupt("return", false);

                                          case 5:
                                                _context9.next = 7;
                                                return this.addState(0, 1, "Switch02");

                                          case 7:
                                                toVO = _context9.sent;

                                                if (!(toVO == null)) {
                                                      _context9.next = 10;
                                                      break;
                                                }

                                                return _context9.abrupt("return", false);

                                          case 10:
                                                _context9.next = 12;
                                                return this.addLine(formVO.name, toVO.name, -10);

                                          case 12:
                                                firstLineVO = _context9.sent;

                                                if (!(firstLineVO == null)) {
                                                      _context9.next = 15;
                                                      break;
                                                }

                                                return _context9.abrupt("return", false);

                                          case 15:
                                                _context9.next = 17;
                                                return this.addLine(toVO.name, formVO.name, 10);

                                          case 17:
                                                secondLineVO = _context9.sent;

                                                if (!(secondLineVO == null)) {
                                                      _context9.next = 20;
                                                      break;
                                                }

                                                return _context9.abrupt("return", false);

                                          case 20:
                                                _context9.next = 22;
                                                return this.addCircle(formVO, toVO);

                                          case 22:
                                                _context9.t0 = _context9.sent;

                                                if (!(_context9.t0 == null)) {
                                                      _context9.next = 25;
                                                      break;
                                                }

                                                return _context9.abrupt("return", false);

                                          case 25:
                                                return _context9.abrupt("return", true);

                                          case 26:
                                          case "end":
                                                return _context9.stop();
                                    }
                              }
                        }, _callee9, this);
                  }));

                  function draw() {
                        return _ref9.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return DualL3LinkHeader;
}(HeaderGroupCreator);

var TwoTierL2LinkSquareHeader = function (_HeaderGroupCreator5) {
      _inherits(TwoTierL2LinkSquareHeader, _HeaderGroupCreator5);

      function TwoTierL2LinkSquareHeader() {
            _classCallCheck(this, TwoTierL2LinkSquareHeader);

            var _this7 = _possibleConstructorReturn(this, (TwoTierL2LinkSquareHeader.__proto__ || Object.getPrototypeOf(TwoTierL2LinkSquareHeader)).call(this));

            _this7.setLayout(500, 200, 2, 2);
            return _this7;
      }

      _createClass(TwoTierL2LinkSquareHeader, [{
            key: "draw",
            value: function () {
                  var _ref10 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
                        var state0_0, state0_1, state1_0, state1_1;
                        return regeneratorRuntime.wrap(function _callee10$(_context10) {
                              while (1) {
                                    switch (_context10.prev = _context10.next) {
                                          case 0:
                                                _context10.next = 2;
                                                return this.addState(0, 0, "Switch01");

                                          case 2:
                                                state0_0 = _context10.sent;

                                                if (!(state0_0 == null)) {
                                                      _context10.next = 5;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 5:
                                                _context10.next = 7;
                                                return this.addState(0, 1, "Switch01");

                                          case 7:
                                                state0_1 = _context10.sent;

                                                if (!(state0_1 == null)) {
                                                      _context10.next = 10;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 10:
                                                _context10.next = 12;
                                                return this.addState(1, 0, "Switch01");

                                          case 12:
                                                state1_0 = _context10.sent;

                                                if (!(state1_0 == null)) {
                                                      _context10.next = 15;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 15:
                                                _context10.next = 17;
                                                return this.addState(1, 1, "Switch01");

                                          case 17:
                                                state1_1 = _context10.sent;

                                                if (!(state1_1 == null)) {
                                                      _context10.next = 20;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 20:
                                                _context10.next = 22;
                                                return this.addLine(state0_0.name, state0_1.name, -10);

                                          case 22:
                                                _context10.t0 = _context10.sent;

                                                if (!(_context10.t0 == null)) {
                                                      _context10.next = 25;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 25:
                                                _context10.next = 27;
                                                return this.addLine(state0_1.name, state0_0.name, 10);

                                          case 27:
                                                _context10.t1 = _context10.sent;

                                                if (!(_context10.t1 == null)) {
                                                      _context10.next = 30;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 30:
                                                _context10.next = 32;
                                                return this.addCircle(state0_0, state0_1);

                                          case 32:
                                                _context10.t2 = _context10.sent;

                                                if (!(_context10.t2 == null)) {
                                                      _context10.next = 35;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 35:
                                                _context10.next = 37;
                                                return this.addLine(state1_0.name, state1_1.name, -10);

                                          case 37:
                                                _context10.t3 = _context10.sent;

                                                if (!(_context10.t3 == null)) {
                                                      _context10.next = 40;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 40:
                                                _context10.next = 42;
                                                return this.addLine(state1_1.name, state1_0.name, 10);

                                          case 42:
                                                _context10.t4 = _context10.sent;

                                                if (!(_context10.t4 == null)) {
                                                      _context10.next = 45;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 45:
                                                _context10.next = 47;
                                                return this.addCircle(state1_0, state1_1);

                                          case 47:
                                                _context10.t5 = _context10.sent;

                                                if (!(_context10.t5 == null)) {
                                                      _context10.next = 50;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 50:
                                                _context10.next = 52;
                                                return this.addLine(state0_0.name, state1_0.name);

                                          case 52:
                                                _context10.t6 = _context10.sent;

                                                if (!(_context10.t6 == null)) {
                                                      _context10.next = 55;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 55:
                                                _context10.next = 57;
                                                return this.addLine(state0_1.name, state1_1.name);

                                          case 57:
                                                _context10.t7 = _context10.sent;

                                                if (!(_context10.t7 == null)) {
                                                      _context10.next = 60;
                                                      break;
                                                }

                                                return _context10.abrupt("return", false);

                                          case 60:
                                                return _context10.abrupt("return", true);

                                          case 61:
                                          case "end":
                                                return _context10.stop();
                                    }
                              }
                        }, _callee10, this);
                  }));

                  function draw() {
                        return _ref10.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return TwoTierL2LinkSquareHeader;
}(HeaderGroupCreator);

var TwoTierL2LinkMeshHeader = function (_HeaderGroupCreator6) {
      _inherits(TwoTierL2LinkMeshHeader, _HeaderGroupCreator6);

      function TwoTierL2LinkMeshHeader() {
            _classCallCheck(this, TwoTierL2LinkMeshHeader);

            var _this8 = _possibleConstructorReturn(this, (TwoTierL2LinkMeshHeader.__proto__ || Object.getPrototypeOf(TwoTierL2LinkMeshHeader)).call(this));

            _this8.setLayout(500, 200, 2, 2);
            return _this8;
      }

      _createClass(TwoTierL2LinkMeshHeader, [{
            key: "draw",
            value: function () {
                  var _ref11 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
                        var state0_0, state0_1, state1_0, state1_1;
                        return regeneratorRuntime.wrap(function _callee11$(_context11) {
                              while (1) {
                                    switch (_context11.prev = _context11.next) {
                                          case 0:
                                                _context11.next = 2;
                                                return this.addState(0, 0, "Switch01");

                                          case 2:
                                                state0_0 = _context11.sent;

                                                if (!(state0_0 == null)) {
                                                      _context11.next = 5;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 5:
                                                _context11.next = 7;
                                                return this.addState(0, 1, "Switch01");

                                          case 7:
                                                state0_1 = _context11.sent;

                                                if (!(state0_1 == null)) {
                                                      _context11.next = 10;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 10:
                                                _context11.next = 12;
                                                return this.addState(1, 0, "Switch01");

                                          case 12:
                                                state1_0 = _context11.sent;

                                                if (!(state1_0 == null)) {
                                                      _context11.next = 15;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 15:
                                                _context11.next = 17;
                                                return this.addState(1, 1, "Switch01");

                                          case 17:
                                                state1_1 = _context11.sent;

                                                if (!(state1_1 == null)) {
                                                      _context11.next = 20;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 20:
                                                _context11.next = 22;
                                                return this.addLine(state0_0.name, state0_1.name, -10);

                                          case 22:
                                                _context11.t0 = _context11.sent;

                                                if (!(_context11.t0 == null)) {
                                                      _context11.next = 25;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 25:
                                                _context11.next = 27;
                                                return this.addLine(state0_1.name, state0_0.name, 10);

                                          case 27:
                                                _context11.t1 = _context11.sent;

                                                if (!(_context11.t1 == null)) {
                                                      _context11.next = 30;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 30:
                                                _context11.next = 32;
                                                return this.addCircle(state0_0, state0_1);

                                          case 32:
                                                _context11.t2 = _context11.sent;

                                                if (!(_context11.t2 == null)) {
                                                      _context11.next = 35;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 35:
                                                _context11.next = 37;
                                                return this.addLine(state1_0.name, state1_1.name, -10);

                                          case 37:
                                                _context11.t3 = _context11.sent;

                                                if (!(_context11.t3 == null)) {
                                                      _context11.next = 40;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 40:
                                                _context11.next = 42;
                                                return this.addLine(state1_1.name, state1_0.name, 10);

                                          case 42:
                                                _context11.t4 = _context11.sent;

                                                if (!(_context11.t4 == null)) {
                                                      _context11.next = 45;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 45:
                                                _context11.next = 47;
                                                return this.addCircle(state1_0, state1_1);

                                          case 47:
                                                _context11.t5 = _context11.sent;

                                                if (!(_context11.t5 == null)) {
                                                      _context11.next = 50;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 50:
                                                _context11.next = 52;
                                                return this.addLine(state0_0.name, state1_0.name);

                                          case 52:
                                                _context11.t6 = _context11.sent;

                                                if (!(_context11.t6 == null)) {
                                                      _context11.next = 55;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 55:
                                                _context11.next = 57;
                                                return this.addLine(state0_1.name, state1_1.name);

                                          case 57:
                                                _context11.t7 = _context11.sent;

                                                if (!(_context11.t7 == null)) {
                                                      _context11.next = 60;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 60:
                                                _context11.next = 62;
                                                return this.addLine(state0_0.name, state1_1.name);

                                          case 62:
                                                _context11.t8 = _context11.sent;

                                                if (!(_context11.t8 == null)) {
                                                      _context11.next = 65;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 65:
                                                _context11.next = 67;
                                                return this.addLine(state1_0.name, state0_1.name);

                                          case 67:
                                                _context11.t9 = _context11.sent;

                                                if (!(_context11.t9 == null)) {
                                                      _context11.next = 70;
                                                      break;
                                                }

                                                return _context11.abrupt("return", false);

                                          case 70:
                                                return _context11.abrupt("return", true);

                                          case 71:
                                          case "end":
                                                return _context11.stop();
                                    }
                              }
                        }, _callee11, this);
                  }));

                  function draw() {
                        return _ref11.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return TwoTierL2LinkMeshHeader;
}(HeaderGroupCreator);

var TwoTierL3LinkSquareHeader = function (_HeaderGroupCreator7) {
      _inherits(TwoTierL3LinkSquareHeader, _HeaderGroupCreator7);

      function TwoTierL3LinkSquareHeader() {
            _classCallCheck(this, TwoTierL3LinkSquareHeader);

            var _this9 = _possibleConstructorReturn(this, (TwoTierL3LinkSquareHeader.__proto__ || Object.getPrototypeOf(TwoTierL3LinkSquareHeader)).call(this));

            _this9.setLayout(500, 200, 2, 2);
            return _this9;
      }

      _createClass(TwoTierL3LinkSquareHeader, [{
            key: "draw",
            value: function () {
                  var _ref12 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
                        var state0_0, state0_1, state1_0, state1_1;
                        return regeneratorRuntime.wrap(function _callee12$(_context12) {
                              while (1) {
                                    switch (_context12.prev = _context12.next) {
                                          case 0:
                                                _context12.next = 2;
                                                return this.addState(0, 0, "Switch02");

                                          case 2:
                                                state0_0 = _context12.sent;

                                                if (!(state0_0 == null)) {
                                                      _context12.next = 5;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 5:
                                                _context12.next = 7;
                                                return this.addState(0, 1, "Switch02");

                                          case 7:
                                                state0_1 = _context12.sent;

                                                if (!(state0_1 == null)) {
                                                      _context12.next = 10;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 10:
                                                _context12.next = 12;
                                                return this.addState(1, 0, "Switch02");

                                          case 12:
                                                state1_0 = _context12.sent;

                                                if (!(state1_0 == null)) {
                                                      _context12.next = 15;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 15:
                                                _context12.next = 17;
                                                return this.addState(1, 1, "Switch02");

                                          case 17:
                                                state1_1 = _context12.sent;

                                                if (!(state1_1 == null)) {
                                                      _context12.next = 20;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 20:
                                                _context12.next = 22;
                                                return this.addLine(state0_0.name, state0_1.name, -10);

                                          case 22:
                                                _context12.t0 = _context12.sent;

                                                if (!(_context12.t0 == null)) {
                                                      _context12.next = 25;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 25:
                                                _context12.next = 27;
                                                return this.addLine(state0_1.name, state0_0.name, 10);

                                          case 27:
                                                _context12.t1 = _context12.sent;

                                                if (!(_context12.t1 == null)) {
                                                      _context12.next = 30;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 30:
                                                _context12.next = 32;
                                                return this.addCircle(state0_0, state0_1);

                                          case 32:
                                                _context12.t2 = _context12.sent;

                                                if (!(_context12.t2 == null)) {
                                                      _context12.next = 35;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 35:
                                                _context12.next = 37;
                                                return this.addLine(state1_0.name, state1_1.name, -10);

                                          case 37:
                                                _context12.t3 = _context12.sent;

                                                if (!(_context12.t3 == null)) {
                                                      _context12.next = 40;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 40:
                                                _context12.next = 42;
                                                return this.addLine(state1_1.name, state1_0.name, 10);

                                          case 42:
                                                _context12.t4 = _context12.sent;

                                                if (!(_context12.t4 == null)) {
                                                      _context12.next = 45;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 45:
                                                _context12.next = 47;
                                                return this.addCircle(state1_0, state1_1);

                                          case 47:
                                                _context12.t5 = _context12.sent;

                                                if (!(_context12.t5 == null)) {
                                                      _context12.next = 50;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 50:
                                                _context12.next = 52;
                                                return this.addLine(state0_0.name, state1_0.name);

                                          case 52:
                                                _context12.t6 = _context12.sent;

                                                if (!(_context12.t6 == null)) {
                                                      _context12.next = 55;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 55:
                                                _context12.next = 57;
                                                return this.addLine(state0_1.name, state1_1.name);

                                          case 57:
                                                _context12.t7 = _context12.sent;

                                                if (!(_context12.t7 == null)) {
                                                      _context12.next = 60;
                                                      break;
                                                }

                                                return _context12.abrupt("return", false);

                                          case 60:
                                                return _context12.abrupt("return", true);

                                          case 61:
                                          case "end":
                                                return _context12.stop();
                                    }
                              }
                        }, _callee12, this);
                  }));

                  function draw() {
                        return _ref12.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return TwoTierL3LinkSquareHeader;
}(HeaderGroupCreator);

var TwoTierL3LinkMeshHeader = function (_HeaderGroupCreator8) {
      _inherits(TwoTierL3LinkMeshHeader, _HeaderGroupCreator8);

      function TwoTierL3LinkMeshHeader() {
            _classCallCheck(this, TwoTierL3LinkMeshHeader);

            var _this10 = _possibleConstructorReturn(this, (TwoTierL3LinkMeshHeader.__proto__ || Object.getPrototypeOf(TwoTierL3LinkMeshHeader)).call(this));

            _this10.setLayout(500, 200, 2, 2);
            return _this10;
      }

      _createClass(TwoTierL3LinkMeshHeader, [{
            key: "draw",
            value: function () {
                  var _ref13 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
                        var state0_0, state0_1, state1_0, state1_1;
                        return regeneratorRuntime.wrap(function _callee13$(_context13) {
                              while (1) {
                                    switch (_context13.prev = _context13.next) {
                                          case 0:
                                                _context13.next = 2;
                                                return this.addState(0, 0, "Switch02");

                                          case 2:
                                                state0_0 = _context13.sent;

                                                if (!(state0_0 == null)) {
                                                      _context13.next = 5;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 5:
                                                _context13.next = 7;
                                                return this.addState(0, 1, "Switch02");

                                          case 7:
                                                state0_1 = _context13.sent;

                                                if (!(state0_1 == null)) {
                                                      _context13.next = 10;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 10:
                                                _context13.next = 12;
                                                return this.addState(1, 0, "Switch02");

                                          case 12:
                                                state1_0 = _context13.sent;

                                                if (!(state1_0 == null)) {
                                                      _context13.next = 15;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 15:
                                                _context13.next = 17;
                                                return this.addState(1, 1, "Switch02");

                                          case 17:
                                                state1_1 = _context13.sent;

                                                if (!(state1_1 == null)) {
                                                      _context13.next = 20;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 20:
                                                _context13.next = 22;
                                                return this.addLine(state0_0.name, state0_1.name, -10);

                                          case 22:
                                                _context13.t0 = _context13.sent;

                                                if (!(_context13.t0 == null)) {
                                                      _context13.next = 25;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 25:
                                                _context13.next = 27;
                                                return this.addLine(state0_1.name, state0_0.name, 10);

                                          case 27:
                                                _context13.t1 = _context13.sent;

                                                if (!(_context13.t1 == null)) {
                                                      _context13.next = 30;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 30:
                                                _context13.next = 32;
                                                return this.addCircle(state0_0, state0_1);

                                          case 32:
                                                _context13.t2 = _context13.sent;

                                                if (!(_context13.t2 == null)) {
                                                      _context13.next = 35;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 35:
                                                _context13.next = 37;
                                                return this.addLine(state1_0.name, state1_1.name, -10);

                                          case 37:
                                                _context13.t3 = _context13.sent;

                                                if (!(_context13.t3 == null)) {
                                                      _context13.next = 40;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 40:
                                                _context13.next = 42;
                                                return this.addLine(state1_1.name, state1_0.name, 10);

                                          case 42:
                                                _context13.t4 = _context13.sent;

                                                if (!(_context13.t4 == null)) {
                                                      _context13.next = 45;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 45:
                                                _context13.next = 47;
                                                return this.addCircle(state1_0, state1_1);

                                          case 47:
                                                _context13.t5 = _context13.sent;

                                                if (!(_context13.t5 == null)) {
                                                      _context13.next = 50;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 50:
                                                _context13.next = 52;
                                                return this.addLine(state0_0.name, state1_0.name);

                                          case 52:
                                                _context13.t6 = _context13.sent;

                                                if (!(_context13.t6 == null)) {
                                                      _context13.next = 55;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 55:
                                                _context13.next = 57;
                                                return this.addLine(state0_1.name, state1_1.name);

                                          case 57:
                                                _context13.t7 = _context13.sent;

                                                if (!(_context13.t7 == null)) {
                                                      _context13.next = 60;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 60:
                                                _context13.next = 62;
                                                return this.addLine(state0_0.name, state1_1.name);

                                          case 62:
                                                _context13.t8 = _context13.sent;

                                                if (!(_context13.t8 == null)) {
                                                      _context13.next = 65;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 65:
                                                _context13.next = 67;
                                                return this.addLine(state1_0.name, state0_1.name);

                                          case 67:
                                                _context13.t9 = _context13.sent;

                                                if (!(_context13.t9 == null)) {
                                                      _context13.next = 70;
                                                      break;
                                                }

                                                return _context13.abrupt("return", false);

                                          case 70:
                                                return _context13.abrupt("return", true);

                                          case 71:
                                          case "end":
                                                return _context13.stop();
                                    }
                              }
                        }, _callee13, this);
                  }));

                  function draw() {
                        return _ref13.apply(this, arguments);
                  }

                  return draw;
            }()
      }]);

      return TwoTierL3LinkMeshHeader;
}(HeaderGroupCreator);
