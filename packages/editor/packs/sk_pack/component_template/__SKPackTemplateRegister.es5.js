"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SKPackTemplateRegister = function () {
	function SKPackTemplateRegister() {
		_classCallCheck(this, SKPackTemplateRegister);
	}

	_createClass(SKPackTemplateRegister, null, [{
		key: "regist",
		value: function regist() {
			if (SKPackTemplateRegister.init == true) return;

			SKPackTemplateRegister.init = true;
			SKPackTemplateRegister._createAssetTypeGroupListTemplate();

			console.log("SKPackTemplateRegister ");
		}
	}, {
		key: "_createAssetTypeGroupListTemplate",
		value: function _createAssetTypeGroupListTemplate() {
			wemb.componentLibraryManager.addComponentPropertyTemplate("network-link");
			Vue.component('network-link-template', {
				extends: CoreTemplate,
				template: " \n                  <div class=\"prop-group\">\n                        \n                        <h5 class=\"header\">network link info</h5>\n                        <div class=\"body\">\n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">\uACF5\uD1B5 \uC18D\uC131</span>\n                              </div>\n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">\uC0C1\uD558\uC5EC\uBC31</span>\n                                     <normal-input v-model=\"model.network.gap\"\n                                            @change=\"onChange('network', 'gap', $event.value)\"\n                                            class=\"flex\"\n                                            :eventBus=\"eventBus\">\n                                  </normal-input>\n                              </div>\n                        </div>\n                        \n                        <hr class=\"body\">\n                        \n                        <div class=\"body\">\n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">from</span>\n                              </div>\n                              \n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">instance</span>\n                                    <div class=\"flex d-flex\" v-if=\"instanceList && instanceList.length\">\n                                          <el-select class=\"select flex\" \n                                          v-bind:value=\"model.network.from.target\" \n                                          @change=\"onChangeFromTargetValue($event)\">\n                                              <el-option v-for=\"instanceName of instanceList\" \n                                                    :value=\"instanceName\" :label=\"instanceName\"\n                                                    :key=\"instanceName\">                                         \n                                              </el-option>\n                                          </el-select> \n                                    </div>\n                              </div>\n                               <div class=\"d-flex row\">\n                                    <span class=\"label\">hostName</span>\n                                    <div class=\"flex d-flex\">\n                                         <normal-input v-model=\"model.network.from.hostName\"\n                                                  @change=\"onChangeValue('from', 'hostName', $event.value)\"\n                                                  class=\"flex\"\n                                                  :eventBus=\"eventBus\">\n                                        </normal-input>\n                                    </div>\n                              </div>\n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">ip</span>\n                                    <div class=\"flex d-flex\">\n                                         <normal-input v-model=\"model.network.from.ip\"\n                                                  @change=\"onChangeValue('from', 'ip', $event.value)\"\n                                                  class=\"flex\"\n                                                  :eventBus=\"eventBus\">\n                                        </normal-input>\n                                    </div>\n                              </div>\n                               <div class=\"d-flex row\">\n                                    <span class=\"label\">port</span>\n                                    <div class=\"flex d-flex\">\n                                         <normal-input v-model=\"model.network.from.port\"\n                                                  @change=\"onChangeValue('from', 'port', $event.value)\"\n                                                  class=\"flex\"\n                                                  :eventBus=\"eventBus\">\n                                        </normal-input>\n                                    </div>\n                              </div>\n                        \n                        </div>\n                        <hr class=\"body\">\n                        <div class=\"body\">\n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">to</span>\n                              </div>\n                              \n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">instance</span>\n                                    <div class=\"flex d-flex\" v-if=\"instanceList && instanceList.length\">\n                                          <el-select class=\"select flex\" \n                                          v-model=\"model.network.to.target\" \n                                         @change=\"onChangeToTargetValue($event)\">\n                                              <el-option v-for=\"instanceName of instanceList\" \n                                                    :value=\"instanceName\" :label=\"instanceName\"\n                                                    :key=\"instanceName\">                                         \n                                              </el-option>\n                                          </el-select>\n                                    </div>\n                              </div>\n                               <div class=\"d-flex row\">\n                                    <span class=\"label\">hostName</span>\n                                    <div class=\"flex d-flex\">\n                                         <normal-input v-model=\"model.network.to.hostName\"\n                                                  @change=\"onChangeValue('to', 'hostName', $event.value)\"\n                                                  class=\"flex\"\n                                                  :eventBus=\"eventBus\">\n                                        </normal-input>\n                                    </div>\n                              </div>\n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">ip</span>\n                                    <div class=\"flex d-flex\">\n                                         <normal-input v-model=\"model.network.to.ip\"\n                                                  @change=\"onChangeValue('to', 'ip', $event.value)\"\n                                                  class=\"flex\"\n                                                  :eventBus=\"eventBus\">\n                                        </normal-input>\n                                    </div>\n                              </div>\n                               <div class=\"d-flex row\">\n                                    <span class=\"label\">port</span>\n                                    <div class=\"flex d-flex\">\n                                         <normal-input v-model=\"model.network.to.port\"\n                                                  @change=\"onChangeValue('to', 'port', $event.value)\"\n                                                  class=\"flex\"\n                                                  :eventBus=\"eventBus\">\n                                        </normal-input>\n                                    </div>\n                              </div>                        \n                        </div>\n                        <hr class=\"body\">\n                        <div class=\"body\">\n                              <div class=\"d-flex row\" style=\"justify-content:center; \">\n                                    <el-button @click=\"onClickAutoSync()\" size=\"small\" style=\"background-color:rgb(51, 51, 51);color:#fff\">auto sync</el-button>\n                              </div>\n                        </div>\n                    </div>\n            ",

				mounted: function mounted() {},
				data: function data() {
					return {
						typeId: "",
						comInstanceList: []
					};
				},

				computed: {
					instanceList: function instanceList() {
						var list = [];

						list.push("");
						wemb.editorProxy.twoLayerInstanceList.forEach(function (comInstance) {
							if (comInstance.componentName == window.wemb.skNetworkManager.targetComponentName) {
								console.log("comInstance.info2 ", comInstance.setter);
								if (comInstance.setter.info.name == window.wemb.skNetworkManager.targetStateIconName) list.push(comInstance.name);
							}
						});

						return list;
					}
				},

				methods: {
					updatePropertyInfo: function updatePropertyInfo() {},


					onChangeTypeGroupName: function onChangeTypeGroupName(value) {
						//this.dispatchChangeEvent("typeInfo", "typeGroupName", value);
					},

					onChangeValue: function onChangeValue(direction, property, value) {
						var directionValue = this.model.network[direction];
						directionValue[property] = value;
						this.dispatchChangeEvent("network", direction, directionValue);
					},

					onChangeFromTargetValue: function onChangeFromTargetValue(fromTarget) {
						var toTarget = this.model.network.to.target.trim();

						if (fromTarget) {
							if (fromTarget == toTarget) {
								alert("from, to에 동일한 요소가 선택 될 수 없습니다.");
								return;
							}
						}
						var directionValue = this.model.network.from;
						directionValue.target = fromTarget;
						this.dispatchChangeEvent("network", "from", directionValue);
					},

					onChangeToTargetValue: function onChangeToTargetValue(toTarget) {
						var fromTarget = this.model.network.from.target.trim();

						if (fromTarget) {
							if (fromTarget == toTarget) {
								alert("from, to에 동일한 요소가 선택 될 수 없습니다.");
								return;
							}
						}
						var directionValue = this.model.network.to;
						directionValue.target = toTarget;
						this.dispatchChangeEvent("network", "to", directionValue);
					},

					onClickAutoSync: function onClickAutoSync() {

						this._executeSyncNetworkInfo("from");
						this._executeSyncNetworkInfo("to");
					},

					_executeSyncNetworkInfo: function _executeSyncNetworkInfo(directionName) {
						var directionProperty = this.model.network[directionName];

						// 값 초기화
						directionProperty.ip = "";
						directionProperty.hostName = "";

						//  컴포넌트 찾기
						var comInstanceName = directionProperty.target;
						if (comInstanceName) {
							var comInstance = wemb.editorProxy.twoLayerInstanceList.find(function (comInstance) {
								if (comInstanceName == comInstance.name) {
									return true;
								} else {
									return false;
								}
							});

							// 인스턴스가 존재하는 경우 값 적용하기
							if (comInstance) {
								directionProperty.ip = comInstance.properties.network.ip;
								directionProperty.hostName = comInstance.properties.network.hostName;
							}
						}

						this.dispatchChangeEvent("network", directionName, directionProperty);
					}
				}
			});
		}
	}]);

	return SKPackTemplateRegister;
}();

SKPackTemplateRegister.init = false;