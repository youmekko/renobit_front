class SKPackTemplateRegister {
      static regist() {
            if (SKPackTemplateRegister.init == true)
                  return;

            SKPackTemplateRegister.init = true;
            SKPackTemplateRegister._createAssetTypeGroupListTemplate();

            console.log("SKPackTemplateRegister ")
      }


      static _createAssetTypeGroupListTemplate() {
            wemb.componentLibraryManager.addComponentPropertyTemplate("network-link");
            Vue.component('network-link-template', {
                  extends: CoreTemplate,
                  template: ` 
                  <div class="prop-group">
                        
                        <h5 class="header">network link info</h5>
                        <div class="body">
                              <div class="d-flex row">
                                    <span class="label">공통 속성</span>
                              </div>
                              <div class="d-flex row">
                                    <span class="label">상하여백</span>
                                     <normal-input v-model="model.network.gap"
                                            @change="onChange('network', 'gap', $event.value)"
                                            class="flex"
                                            :eventBus="eventBus">
                                  </normal-input>
                              </div>
                        </div>
                        
                        <hr class="body">
                        
                        <div class="body">
                              <div class="d-flex row">
                                    <span class="label">from</span>
                              </div>
                              
                              <div class="d-flex row">
                                    <span class="label">instance</span>
                                    <div class="flex d-flex" v-if="instanceList && instanceList.length">
                                          <el-select class="select flex" 
                                          v-bind:value="model.network.from.target" 
                                          @change="onChangeFromTargetValue($event)">
                                              <el-option v-for="instanceName of instanceList" 
                                                    :value="instanceName" :label="instanceName"
                                                    :key="instanceName">                                         
                                              </el-option>
                                          </el-select> 
                                    </div>
                              </div>
                               <div class="d-flex row">
                                    <span class="label">hostName</span>
                                    <div class="flex d-flex">
                                         <normal-input v-model="model.network.from.hostName"
                                                  @change="onChangeValue('from', 'hostName', $event.value)"
                                                  class="flex"
                                                  :eventBus="eventBus">
                                        </normal-input>
                                    </div>
                              </div>
                              <div class="d-flex row">
                                    <span class="label">ip</span>
                                    <div class="flex d-flex">
                                         <normal-input v-model="model.network.from.ip"
                                                  @change="onChangeValue('from', 'ip', $event.value)"
                                                  class="flex"
                                                  :eventBus="eventBus">
                                        </normal-input>
                                    </div>
                              </div>
                               <div class="d-flex row">
                                    <span class="label">port</span>
                                    <div class="flex d-flex">
                                         <normal-input v-model="model.network.from.port"
                                                  @change="onChangeValue('from', 'port', $event.value)"
                                                  class="flex"
                                                  :eventBus="eventBus">
                                        </normal-input>
                                    </div>
                              </div>
                        
                        </div>
                        <hr class="body">
                        <div class="body">
                              <div class="d-flex row">
                                    <span class="label">to</span>
                              </div>
                              
                              <div class="d-flex row">
                                    <span class="label">instance</span>
                                    <div class="flex d-flex" v-if="instanceList && instanceList.length">
                                          <el-select class="select flex" 
                                          v-model="model.network.to.target" 
                                         @change="onChangeToTargetValue($event)">
                                              <el-option v-for="instanceName of instanceList" 
                                                    :value="instanceName" :label="instanceName"
                                                    :key="instanceName">                                         
                                              </el-option>
                                          </el-select>
                                    </div>
                              </div>
                               <div class="d-flex row">
                                    <span class="label">hostName</span>
                                    <div class="flex d-flex">
                                         <normal-input v-model="model.network.to.hostName"
                                                  @change="onChangeValue('to', 'hostName', $event.value)"
                                                  class="flex"
                                                  :eventBus="eventBus">
                                        </normal-input>
                                    </div>
                              </div>
                              <div class="d-flex row">
                                    <span class="label">ip</span>
                                    <div class="flex d-flex">
                                         <normal-input v-model="model.network.to.ip"
                                                  @change="onChangeValue('to', 'ip', $event.value)"
                                                  class="flex"
                                                  :eventBus="eventBus">
                                        </normal-input>
                                    </div>
                              </div>
                               <div class="d-flex row">
                                    <span class="label">port</span>
                                    <div class="flex d-flex">
                                         <normal-input v-model="model.network.to.port"
                                                  @change="onChangeValue('to', 'port', $event.value)"
                                                  class="flex"
                                                  :eventBus="eventBus">
                                        </normal-input>
                                    </div>
                              </div>                        
                        </div>
                        <hr class="body">
                        <div class="body">
                              <div class="d-flex row" style="justify-content:center; ">
                                    <el-button @click="onClickAutoSync()" size="small" style="background-color:rgb(51, 51, 51);color:#fff">auto sync</el-button>
                              </div>
                        </div>
                    </div>
            `,

                  mounted: function() {},
                  data: function() {
                        return {
                              typeId: "",
                              comInstanceList:[]
                        };
                  },

                  computed: {
                        instanceList:function(){
                              let list = [];

                              list.push("");
                              wemb.editorProxy.twoLayerInstanceList.forEach((comInstance)=>{
                                    if(comInstance.componentName==window.wemb.skNetworkManager.targetComponentName){
                                          console.log("comInstance.info2 ", comInstance.setter);
                                          if(comInstance.setter.info.name==window.wemb.skNetworkManager.targetStateIconName)
                                                list.push(comInstance.name);
                                    }
                              });

                              return list;
                        }
                  },

                  methods: {
                        updatePropertyInfo() {

                        },

                        onChangeTypeGroupName: function(value) {
                              //this.dispatchChangeEvent("typeInfo", "typeGroupName", value);
                        },

                        onChangeValue:function(direction, property, value){
                              let directionValue = this.model.network[direction];
                              directionValue[property] = value;
                              this.dispatchChangeEvent("network", direction,directionValue)
                        },


                        onChangeFromTargetValue:function(fromTarget){
                              let toTarget = this.model.network.to.target.trim();

                              if(fromTarget){
                                    if(fromTarget==toTarget){
                                          alert("from, to에 동일한 요소가 선택 될 수 없습니다.");
                                          return;
                                    }
                              }
                              let directionValue = this.model.network.from;
                              directionValue.target = fromTarget;
                              this.dispatchChangeEvent("network", "from",directionValue)


                        },

                        onChangeToTargetValue:function(toTarget){
                              let fromTarget = this.model.network.from.target.trim();

                              if(fromTarget){
                                    if(fromTarget==toTarget){
                                          alert("from, to에 동일한 요소가 선택 될 수 없습니다.");
                                          return;
                                    }
                              }
                              let directionValue = this.model.network.to;
                              directionValue.target = toTarget;
                              this.dispatchChangeEvent("network", "to",directionValue)

                        },

                        onClickAutoSync:function(){

                              this._executeSyncNetworkInfo("from");
                              this._executeSyncNetworkInfo("to");


                        },

                        _executeSyncNetworkInfo(directionName){
                              let directionProperty = this.model.network[directionName];

                              // 값 초기화
                              directionProperty.ip = "";
                              directionProperty.hostName = "";

                              //  컴포넌트 찾기
                              let comInstanceName = directionProperty.target;
                              if (comInstanceName) {
                                    let comInstance = wemb.editorProxy.twoLayerInstanceList.find((comInstance) => {
                                          if (comInstanceName == comInstance.name) {
                                                return true;
                                          } else {
                                                return false;
                                          }
                                    })

                                    // 인스턴스가 존재하는 경우 값 적용하기
                                    if(comInstance){
                                          directionProperty.ip = comInstance.properties.network.ip;
                                          directionProperty.hostName = comInstance.properties.network.hostName;
                                    }
                              }

                              this.dispatchChangeEvent("network", directionName, directionProperty);
                        }


                  }
            })
      }


}

SKPackTemplateRegister.init = false;
