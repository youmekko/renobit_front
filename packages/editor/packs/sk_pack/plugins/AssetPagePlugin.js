class AssetPagePlugin extends ExtensionPluginCore {
	constructor() {
		super();
		this.editorProxy = null;
		this.networkStateComponentList = [];
		this.pageComponentList = [];
		console.log("@@ AssetPagePlugin 2222");
	}

	async setFacade(facade) {
		super.setFacade(facade);
		this.editorProxy = wemb.editorProxy;
	}

	get notificationList() {
		return [
			EditorProxy.NOTI_SAVED_PAGE
		]
	}
	/*
    noti가 온 경우 실행
    call : mediator에서 실행
     */
	handleNotification(note) {
		switch (note.name) {
			case EditorProxy.NOTI_SAVED_PAGE:
				this.noti_savedPage(note.body);
				break;
		}
	}

	initProperties() {
	}

	destroy() {
	}

	/*
    1. 자산 배치 정보 저장하기
    2. 자산 배치 정보 업데이트 정보 날리기 (NOTI_UPDATE_EXTENSION_DATA);
     */
	async noti_savedPage(page_id) {
		let networkPageComponentList = []; // 페이지 전체 데이터를 가지고 있는 배열
		let networkComponentDataList = []; // 서버에 보낼 전체 ROW데이터를 JSON 형태로 가지고 있는 배열

		let portDataList = []; // A,B 배열 합친 배열.
		console.log("1. 자산 배치 정보 저장하기 시작 dcimAssetProxy.saveAssetInfo()");
		networkPageComponentList = await this.getnetworkPageComponentList(); // 페이지에 있는 컴포넌트 전부를 읽어온다..

		networkPageComponentList = networkPageComponentList.filter((component) =>{
			return component.hasOwnProperty("componentName");
		});

		let bodyPortInfoList  = networkPageComponentList.filter((component) => {
			if((component.componentName.indexOf("VerticalPortComponent") !=  -1) || (component.componentName.indexOf("HorizontalPortComponent") !=  -1)){
				return component;
			}
		})

		let headerPortInfoList  = networkPageComponentList.filter((component) => {
			return component.componentName.indexOf("NetworkLinkComponent") != -1;
		})

		let networkStateComponentList = networkPageComponentList.filter((component) => {
			return component.componentName.indexOf("NetworkStateComponent") != -1;
		})

		bodyPortInfoList = this._getBodyPortList(page_id, bodyPortInfoList);
		headerPortInfoList = this._getLinkPortList(page_id, headerPortInfoList);
		networkStateComponentList = this._getNetworkStateList(page_id, networkStateComponentList);

		let networkPortList = headerPortInfoList.concat(bodyPortInfoList);
		let jsonDataObjList = {
			"list": networkStateComponentList,
			"port": networkPortList,
		}

		let jsonArray = JSON.stringify(jsonDataObjList); // 문자열로 변환하여 저장한다.

		$.ajax({
			url: "/renobit/saveNetworkTempData.jsp",
			type: "post",
			dataType: "JSON",
			data: {
				jsonArray: jsonArray
			},
			success: function (result) {
				console.log("성공 :", result);
				jsonDataObjList = null;
			}, error: function (result) {
				console.log("실패 :", result);
				jsonDataObjList = null;
			}
		})
	}

	_getBodyPortList(page_id, networkPortList) {
		let jsonPortList = [];

		networkPortList.forEach((portVO) => {
			// console.log("portVOportVOportVO", portVO);
			let portVOPortinfo = portVO.portInfo;
			jsonPortList.push({
				"PAGE_ID": page_id,
				"SRC_HOST": portVOPortinfo.fromHost,
				"SRC_IP": portVOPortinfo.fromIP,
				"SRC_PORT": portVOPortinfo.fromText,
				"DESC_HOST": portVOPortinfo.toHost,
				"DESC_IP": portVOPortinfo.toIP,
				"DESC_PORT": portVOPortinfo.toText
			})
		})
		return jsonPortList;
	}

	_getLinkPortList(page_id, networkLinkPortList){
		let jsonPortList = [];

		networkLinkPortList.forEach((portVO) => {
			let portVOPortinfo = portVO.network;
			jsonPortList.push({
				"PAGE_ID": page_id,
				"SRC_HOST": portVOPortinfo.from.hostName,
				"SRC_IP": portVOPortinfo.from.ip,
				"SRC_PORT": portVOPortinfo.from.port,
				"DESC_HOST": portVOPortinfo.to.hostName,
				"DESC_IP": portVOPortinfo.to.ip,
				"DESC_PORT": portVOPortinfo.to.port
			})
		})

		return jsonPortList;
	}

	_getNetworkStateList(page_id, networkStateList) {
		let jsonNetworkStateList = [];
		networkStateList.forEach((networkState) => {
			console.log("networkStatenetworkStatenetworkStatenetworkState", networkState);
			let networkStateInfo = networkState.network
			jsonNetworkStateList.push({
				"PAGE_ID": page_id,
				"HOSTNAME": networkStateInfo.hostName,
				"IP_ADDRESS": networkStateInfo.ip,
				"EQUIP_TYPE": networkStateInfo.equipmentType,
				"MANUFACTURER": networkStateInfo.manufacturer,
				"MODEL": networkStateInfo.modelName,
				"SN": networkStateInfo.sn,
				"ROW_VALUE": networkStateInfo.row,
				"CID": networkStateInfo.cid
			});
		})

		return jsonNetworkStateList;

	}

	async getnetworkPageComponentList() {
		// 처음 시작한 것이라면, 엑셀 문서에 있는 내용으로 저장 작업을 시작한다.
		// 처음 시작한 것이 아니라면, 지금 열고있는 페이지를 불러와 저장한다.
		let networkPageComponentList = [];
		if (window.hasOwnProperty("__PAGE_DATA__")) {
			console.log("@@실행 1");
			networkPageComponentList = window.__PAGE_DATA__.content_info.two_layer.map((item) => {
				if (item.componentName === "VerticalPortComponent") {
					item.props.componentName = item.componentName;
					item.props.name = item.name;
				}

				if (item.componentName === "HorizontalPortComponent") {
					item.props.componentName = item.componentName;
					item.props.name = item.name;
				}

				return item.props;
			});
			window.__PAGE_DATA__ = null;
			delete window.__PAGE_DATA__;
		} else {
			networkPageComponentList = this.editorProxy.twoLayerInstanceList.map((item) => {
				item.properties.componentName = item.componentName;
				item.properties.name = item.name;
				return item.properties;
			});
		}

		return networkPageComponentList;
	}

	_getNetworkComponentName(name) { //  type 이름 보정
		let temp = "";
		switch (name) {
			case "NetworkFlat005":
				temp = "Switch";
				return temp;
				break;
			case "BuildingFlat":
				temp = "Building";
				return temp;
				break;
			case "FirewallFlat001":
				temp = "Firewall";
				return temp;
				break;
			case "network009":
				temp = "Backbone";
				return temp;
				break;
			case "router001":
				temp = "Router";
				return temp;
				break;
			case "Switch01":
				temp = "Switch01";
				return temp;
				break;
			case "Switch02":
				temp = "Switch02";
				return temp;
				break;
			case "Switch03":
				temp = "Switch03";
				return temp;
				break;
			case "Switch04":
				temp = "Switch04";
				return temp;
				break;
			case "Switch05":
				temp = "Switch05";
				return temp;
				break;
			case "Switch06":
				temp = "Switch06";
				return temp;
				break;
			case "Switch07":
				temp = "Switch07";
				return temp;
				break;
			default:
				temp = "Server";
				return temp;
				break;
		}
	}
}
