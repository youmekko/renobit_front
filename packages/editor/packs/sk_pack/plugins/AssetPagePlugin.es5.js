"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AssetPagePlugin = function (_ExtensionPluginCore) {
	_inherits(AssetPagePlugin, _ExtensionPluginCore);

	function AssetPagePlugin() {
		_classCallCheck(this, AssetPagePlugin);

		var _this = _possibleConstructorReturn(this, (AssetPagePlugin.__proto__ || Object.getPrototypeOf(AssetPagePlugin)).call(this));

		_this.editorProxy = null;
		_this.networkStateComponentList = [];
		_this.pageComponentList = [];
		console.log("@@ AssetPagePlugin 2222");
		return _this;
	}

	_createClass(AssetPagePlugin, [{
		key: "setFacade",
		value: function () {
			var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(facade) {
				return regeneratorRuntime.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								_get(AssetPagePlugin.prototype.__proto__ || Object.getPrototypeOf(AssetPagePlugin.prototype), "setFacade", this).call(this, facade);
								this.editorProxy = wemb.editorProxy;

							case 2:
							case "end":
								return _context.stop();
						}
					}
				}, _callee, this);
			}));

			function setFacade(_x) {
				return _ref.apply(this, arguments);
			}

			return setFacade;
		}()
	}, {
		key: "handleNotification",

		/*
     noti가 온 경우 실행
     call : mediator에서 실행
      */
		value: function handleNotification(note) {
			console.log("@@ template 2 , handleNotification", note.name, note);
			switch (note.name) {
				case EditorProxy.NOTI_SAVED_PAGE:
					this.noti_savedPage(note.body);
					break;
			}
		}
	}, {
		key: "initProperties",
		value: function initProperties() {}
	}, {
		key: "destroy",
		value: function destroy() {}

		/*
     1. 자산 배치 정보 저장하기
     2. 자산 배치 정보 업데이트 정보 날리기 (NOTI_UPDATE_EXTENSION_DATA);
      */

	}, {
		key: "noti_savedPage",
		value: function () {
			var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(page_id) {
				var networkPageComponentList, networkComponentDataList, portDataList, bodyPortInfoList, headerPortInfoList, networkStateComponentList, networkPortList, jsonDataObjList, jsonArray;
				return regeneratorRuntime.wrap(function _callee2$(_context2) {
					while (1) {
						switch (_context2.prev = _context2.next) {
							case 0:
								networkPageComponentList = []; // 페이지 전체 데이터를 가지고 있는 배열

								networkComponentDataList = []; // 서버에 보낼 전체 ROW데이터를 JSON 형태로 가지고 있는 배열

								portDataList = []; // A,B 배열 합친 배열.

								console.log("1. 자산 배치 정보 저장하기 시작 dcimAssetProxy.saveAssetInfo()");
								_context2.next = 6;
								return this.getnetworkPageComponentList();

							case 6:
								networkPageComponentList = _context2.sent;
								// 페이지에 있는 컴포넌트 전부를 읽어온다..

								networkPageComponentList = networkPageComponentList.filter(function (component) {
									return component.hasOwnProperty("componentName");
								});

								bodyPortInfoList = networkPageComponentList.filter(function (component) {
									if (component.componentName.indexOf("VerticalPortComponent") != -1 || component.componentName.indexOf("HorizontalPortComponent") != -1) {
										return component;
									}
								});
								headerPortInfoList = networkPageComponentList.filter(function (component) {
									return component.componentName.indexOf("NetworkLinkComponent") != -1;
								});
								networkStateComponentList = networkPageComponentList.filter(function (component) {
									return component.componentName.indexOf("NetworkStateComponent") != -1;
								});


								bodyPortInfoList = this._getBodyPortList(page_id, bodyPortInfoList);
								headerPortInfoList = this._getLinkPortList(page_id, headerPortInfoList);
								networkStateComponentList = this._getNetworkStateList(page_id, networkStateComponentList);

								networkPortList = headerPortInfoList.concat(bodyPortInfoList);
								jsonDataObjList = {
									"list": networkStateComponentList,
									"port": networkPortList
								};
								jsonArray = JSON.stringify(jsonDataObjList); // 문자열로 변환하여 저장한다.

								$.ajax({
									url: "/renobit/saveNetworkTempData.jsp",
									type: "post",
									dataType: "JSON",
									data: {
										jsonArray: jsonArray
									},
									success: function success(result) {
										console.log("성공 :", result);
										jsonDataObjList = null;
									}, error: function error(result) {
										console.log("실패 :", result);
										jsonDataObjList = null;
									}
								});

							case 18:
							case "end":
								return _context2.stop();
						}
					}
				}, _callee2, this);
			}));

			function noti_savedPage(_x2) {
				return _ref2.apply(this, arguments);
			}

			return noti_savedPage;
		}()
	}, {
		key: "_getBodyPortList",
		value: function _getBodyPortList(page_id, networkPortList) {
			var jsonPortList = [];

			networkPortList.forEach(function (portVO) {
				// console.log("portVOportVOportVO", portVO);
				var portVOPortinfo = portVO.portInfo;
				jsonPortList.push({
					"PAGE_ID": page_id,
					"SRC_HOST": portVOPortinfo.fromHost,
					"SRC_IP": portVOPortinfo.fromIP,
					"SRC_PORT": portVOPortinfo.fromText,
					"DESC_HOST": portVOPortinfo.toHost,
					"DESC_IP": portVOPortinfo.toIP,
					"DESC_PORT": portVOPortinfo.toText
				});
			});
			return jsonPortList;
		}
	}, {
		key: "_getLinkPortList",
		value: function _getLinkPortList(page_id, networkLinkPortList) {
			var jsonPortList = [];

			networkLinkPortList.forEach(function (portVO) {
				var portVOPortinfo = portVO.network;
				jsonPortList.push({
					"PAGE_ID": page_id,
					"SRC_HOST": portVOPortinfo.from.hostName,
					"SRC_IP": portVOPortinfo.from.ip,
					"SRC_PORT": portVOPortinfo.from.port,
					"DESC_HOST": portVOPortinfo.to.hostName,
					"DESC_IP": portVOPortinfo.to.ip,
					"DESC_PORT": portVOPortinfo.to.port
				});
			});

			return jsonPortList;
		}
	}, {
		key: "_getNetworkStateList",
		value: function _getNetworkStateList(page_id, networkStateList) {
			var jsonNetworkStateList = [];
			networkStateList.forEach(function (networkState) {
				console.log("networkStatenetworkStatenetworkStatenetworkState", networkState);
				var networkStateInfo = networkState.network;
				jsonNetworkStateList.push({
					"PAGE_ID": page_id,
					"HOSTNAME": networkStateInfo.hostName,
					"IP_ADDRESS": networkStateInfo.ip,
					"EQUIP_TYPE": networkStateInfo.equipmentType,
					"MANUFACTURER": networkStateInfo.manufacturer,
					"MODEL": networkStateInfo.modelName,
					"SN": networkStateInfo.sn,
					"ROW_VALUE": networkStateInfo.row,
					"CID": networkStateInfo.cid
				});
			});

			return jsonNetworkStateList;
		}
	}, {
		key: "getnetworkPageComponentList",
		value: function () {
			var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
				var networkPageComponentList;
				return regeneratorRuntime.wrap(function _callee3$(_context3) {
					while (1) {
						switch (_context3.prev = _context3.next) {
							case 0:
								// 처음 시작한 것이라면, 엑셀 문서에 있는 내용으로 저장 작업을 시작한다.
								// 처음 시작한 것이 아니라면, 지금 열고있는 페이지를 불러와 저장한다.
								networkPageComponentList = [];

								if (window.hasOwnProperty("__PAGE_DATA__")) {
									console.log("@@실행 1");
									networkPageComponentList = window.__PAGE_DATA__.content_info.two_layer.map(function (item) {
										if (item.componentName === "VerticalPortComponent") {
											item.props.componentName = item.componentName;
											item.props.name = item.name;
										}

										if (item.componentName === "HorizontalPortComponent") {
											item.props.componentName = item.componentName;
											item.props.name = item.name;
										}

										return item.props;
									});
									window.__PAGE_DATA__ = null;
									delete window.__PAGE_DATA__;
								} else {
									networkPageComponentList = this.editorProxy.twoLayerInstanceList.map(function (item) {
										item.properties.componentName = item.componentName;
										item.properties.name = item.name;
										return item.properties;
									});
								}

								return _context3.abrupt("return", networkPageComponentList);

							case 3:
							case "end":
								return _context3.stop();
						}
					}
				}, _callee3, this);
			}));

			function getnetworkPageComponentList() {
				return _ref3.apply(this, arguments);
			}

			return getnetworkPageComponentList;
		}()
	}, {
		key: "_getNetworkComponentName",
		value: function _getNetworkComponentName(name) {
			//  type 이름 보정
			var temp = "";
			switch (name) {
				case "NetworkFlat005":
					temp = "Switch";
					return temp;
					break;
				case "BuildingFlat":
					temp = "Building";
					return temp;
					break;
				case "FirewallFlat001":
					temp = "Firewall";
					return temp;
					break;
				case "network009":
					temp = "Backbone";
					return temp;
					break;
				case "router001":
					temp = "Router";
					return temp;
					break;
				case "Switch01":
					temp = "Switch01";
					return temp;
					break;
				case "Switch02":
					temp = "Switch02";
					return temp;
					break;
				case "Switch03":
					temp = "Switch03";
					return temp;
					break;
				case "Switch04":
					temp = "Switch04";
					return temp;
					break;
				case "Switch05":
					temp = "Switch05";
					return temp;
					break;
				case "Switch06":
					temp = "Switch06";
					return temp;
					break;
				case "Switch07":
					temp = "Switch07";
					return temp;
					break;
				default:
					temp = "Server";
					return temp;
					break;
			}
		}
	}, {
		key: "notificationList",
		get: function get() {
			console.log("@@ template 21 , notificationList");
			return [EditorProxy.NOTI_SAVED_PAGE];
		}
	}]);

	return AssetPagePlugin;
}(ExtensionPluginCore);