class SKNetworkPanel extends ExtensionPanelCore {
      constructor() {
            super();

            this._app = null;
            this._sknetworkProxy = new SKNetworkProxy();
      }

      /*
    call :
	  인스턴스 생성 후 실행
	  단, 아직 화면에 붙지 않은 상태임.

     */
      create() {
            let template =
                  `
           <div style="overflow: auto;padding:8px;">
                <p>
                  <button class="el-button el-button--primary" style="width:100%;" @click="on_selectExcelFile(true)"> 새 페이지 만들기</button>
                  </p>
                   <p>
                  <button class="el-button el-button--primary" style="width:100%;" @click="on_selectExcelFile(false)"> 자산 정보 업데이트 </button>
                    </p>
                 <form id="FILE_FORM" method="post" enctype="multipart/form-data" action="">
                      <input type="file" name="file" id="excel-file-input" @change="on_change_file($event)" style="display:none"/> 
                </form>
            </div>
      `
            //  <!--<button class="el-button el-button&#45;&#45;primary" style="width:100%;" @click="on_ServerExcelFile()"> Server 파일 선택하기</button>-->
            // <!--fileUpload.do에서는 name=file로 받음-->
            this._$element = $(template);

            if (this._$parentView == null) {
                  console.log("Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.")
                  return;
            }

            /*
		중요:
		Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.
		 */
            this._$parentView.append(this._$element);

            // 트리 목록에서 그룹 목록만 구해오기
            this._app = new Vue({
                  el: this._$element[0],
                  data: {
                        parent: this,
                        isNewPage: true
                  },
                  methods: {
                        on_selectExcelFile(isNewPage) {
                              if (isNewPage == false) {
                                    var check = window.confirm("현재 페이지에 엑셀 파일 정보로 덮어쓰기 하겠습니까?");
                                    if (check == false) {
                                          return;
                                    }
                              }

                              this.isNewPage = isNewPage;
                              let $importFile = $("#excel-file-input");

                              if ($importFile.length == 0) {
                                    alert("excel-file-input 아이디를 가진 input 태그가 존재하지 않습니다. ");
                                    return;
                              }

                              $importFile.attr("accept", ".xlsx, xlsx/*,.xls, xls/*");
                              // 기존 저장된 이벤트 제거
                              $importFile.off();
                              $importFile.val("");
                              let that = this;

                              // 생성되는 경우
                              $importFile.on("change", async (event) => {
                                    try {
                                          let form = $("#FILE_FORM")[0];
                                          let formData = new FormData(form);

                                          // url: "/renobit/excelUpload.do", // 하이닉스
                                           $.ajax({
								  url: "/renobit/fileUpload.jsp",  // RND
								  processData: false,
								  contentType: false,
								  data: formData,
								  type: 'POST',
								  success: function (result) {
									  console.log("success@#@#", result);
									  that.on_ServerExcelFile(result);
									  // that.on_ServerExcelFile(result.path);  //"/renobit/excelUpload.do", 에서는 result가 객체로와서  result.path로 넣어야 함
								  }, error: function (result) {
									  alert('파일 업로드 실패');
									  console.log("파일 업로드 실패 ", result);
								  }
							  });

                                          // 임시로 열어 둠
                                          // this.parent._sknetworkProxy.createPageToFile(event.target.files[0], this.isNewPage); // 서버 타지 않고 작업할 때

                                          // 임시로 열어둠
                                    } catch (error) {
                                          console.log("error = ", error);
                                    }
                              });

                              $importFile.trigger("click");
                        },
                        on_ServerExcelFile(result) {
                              /* set up XMLHttpRequest */

                              // "D:/wemb/product/renobit/uploads/row1_hor_5.xlsx" // 가정값
                              // result = "D:/wemb/product/renobit/uploads/row1_hor_5.xlsx"; //가정값

                              let that = this;
                              let tmpArr = [];

                              // 슬래쉬, 역슬래시 구분 값
                              if (result.indexOf("/") != -1) {
                                    tmpArr = result.split("/");
                              } else if (result.indexOf("\\") != -1) {
                                    tmpArr = result.split("\\");
                              }

                              let filename = tmpArr[tmpArr.length - 1];
                              let url = "/renobit/uploads/" + filename;

                              var oReq = new XMLHttpRequest();
                              oReq.open("GET", url, true);
                              oReq.responseType = "arraybuffer";

                              oReq.onload = function (e) {
                                    var arraybuffer = oReq.response;

                                    /* convert data to binary string */
                                    var data = new Uint8Array(arraybuffer);
                                    var arr = new Array();
                                    for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                                    var bstr = arr.join("");

                                    /* Call XLSX */
                                    var workbook = XLSX.read(bstr, {type: "binary"});

                                    // /* DO SOMETHING WITH workbook HERE */
	                              that.parent._sknetworkProxy.createPageToWorkbook(workbook, that.isNewPage);
                              }
                              oReq.send();
                        },
                        on_change_file(event) {
                              console.log("@#@#@#@ event ", event);
                        },
                  }
            })

            return this._app;
      }

}
