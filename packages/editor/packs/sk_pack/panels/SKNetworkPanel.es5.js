"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SKNetworkPanel = function (_ExtensionPanelCore) {
	_inherits(SKNetworkPanel, _ExtensionPanelCore);

	function SKNetworkPanel() {
		_classCallCheck(this, SKNetworkPanel);

		var _this = _possibleConstructorReturn(this, (SKNetworkPanel.__proto__ || Object.getPrototypeOf(SKNetworkPanel)).call(this));

		_this._app = null;
		_this._sknetworkProxy = new SKNetworkProxy();
		return _this;
	}

	/*
	call :
	인스턴스 생성 후 실행
	단, 아직 화면에 붙지 않은 상태임.
	*/


	_createClass(SKNetworkPanel, [{
		key: "create",
		value: function create() {
			var template = "\n           <div style=\"overflow: auto;padding:8px;\">\n                <p>\n                  <button class=\"el-button el-button--primary\" style=\"width:100%;\" @click=\"on_selectExcelFile(true)\"> \uC0C8 \uD398\uC774\uC9C0 \uB9CC\uB4E4\uAE30</button>\n                  </p>\n                   <p>\n                  <button class=\"el-button el-button--primary\" style=\"width:100%;\" @click=\"on_selectExcelFile(false)\"> \uC790\uC0B0 \uC815\uBCF4 \uC5C5\uB370\uC774\uD2B8 </button>\n                    </p>\n                 <form id=\"FILE_FORM\" method=\"post\" enctype=\"multipart/form-data\" action=\"\">\n                      <input type=\"file\" name=\"file\" id=\"excel-file-input\" @change=\"on_change_file($event)\" style=\"display:none\"/> \n                </form>\n            </div>\n      ";
			//  <!--<button class="el-button el-button&#45;&#45;primary" style="width:100%;" @click="on_ServerExcelFile()"> Server 파일 선택하기</button>-->
			// <!--fileUpload.do에서는 name=file로 받음-->
			this._$element = $(template);

			if (this._$parentView == null) {
				console.log("Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.");
				return;
			}

			/*
			중요:
			Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.
			*/
			this._$parentView.append(this._$element);

			// 트리 목록에서 그룹 목록만 구해오기
			this._app = new Vue({
				el: this._$element[0],
				data: {
					parent: this,
					isNewPage: true
				},
				methods: {
					on_selectExcelFile: function on_selectExcelFile(isNewPage) {
						var _this2 = this;

						if (isNewPage == false) {
							var check = window.confirm("현재 페이지에 엑셀 파일 정보로 덮어쓰기 하겠습니까?");
							if (check == false) {
								return;
							}
						}

						this.isNewPage = isNewPage;
						var $importFile = $("#excel-file-input");

						if ($importFile.length == 0) {
							alert("excel-file-input 아이디를 가진 input 태그가 존재하지 않습니다. ");
							return;
						}

						$importFile.attr("accept", ".xlsx, xlsx/*,.xls, xls/*");
						// 기존 저장된 이벤트 제거
						$importFile.off();
						$importFile.val("");
						var that = this;

						// 생성되는 경우
						$importFile.on("change", function () {
							var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(event) {
								var form, formData;
								return regeneratorRuntime.wrap(function _callee$(_context) {
									while (1) {
										switch (_context.prev = _context.next) {
											case 0:
												try {
													form = $("#FILE_FORM")[0];
													formData = new FormData(form);

													// url: "/renobit/excelUpload.do", // 하이닉스

													$.ajax({
														url: "/renobit/fileUpload.jsp", // RND
														processData: false,
														contentType: false,
														data: formData,
														type: 'POST',
														success: function success(result) {
															console.log("success@#@#", result);
															that.on_ServerExcelFile(result);
															// that.on_ServerExcelFile(result.path);  //"/renobit/excelUpload.do", 에서는 result가 객체로와서  result.path로 넣어야 함
														}, error: function error(result) {
															alert('파일 업로드 실패');
															console.log("파일 업로드 실패 ", result);
														}
													});

													// 임시로 열어 둠
													// this.parent._sknetworkProxy.createPageToFile(event.target.files[0], this.isNewPage); // 서버 타지 않고 작업할 때

													// 임시로 열어둠
												} catch (error) {
													console.log("error = ", error);
												}

											case 1:
											case "end":
												return _context.stop();
										}
									}
								}, _callee, _this2);
							}));

							return function (_x) {
								return _ref.apply(this, arguments);
							};
						}());

						$importFile.trigger("click");
					},
					on_ServerExcelFile: function on_ServerExcelFile(result) {
						/* set up XMLHttpRequest */

						// "D:/wemb/product/renobit/uploads/row1_hor_5.xlsx" // 가정값
						// result = "D:/wemb/product/renobit/uploads/row1_hor_5.xlsx"; //가정값

						var that = this;
						var tmpArr = [];

						// 슬래쉬, 역슬래시 구분 값
						if (result.indexOf("/") != -1) {
							tmpArr = result.split("/");
						} else if (result.indexOf("\\") != -1) {
							tmpArr = result.split("\\");
						}

						var filename = tmpArr[tmpArr.length - 1];
						var url = "/renobit/uploads/" + filename;

						var oReq = new XMLHttpRequest();
						oReq.open("GET", url, true);
						oReq.responseType = "arraybuffer";

						oReq.onload = function (e) {
							var arraybuffer = oReq.response;

							/* convert data to binary string */
							var data = new Uint8Array(arraybuffer);
							var arr = new Array();
							for (var i = 0; i != data.length; ++i) {
								arr[i] = String.fromCharCode(data[i]);
							}var bstr = arr.join("");

							/* Call XLSX */
							var workbook = XLSX.read(bstr, { type: "binary" });

							// /* DO SOMETHING WITH workbook HERE */
							that.parent._sknetworkProxy.createPageToWorkbook(workbook, that.isNewPage);
						};
						oReq.send();
					},
					on_change_file: function on_change_file(event) {
						console.log("@#@#@#@ event ", event);
					}
				}
			});

			return this._app;
		}
	}]);

	return SKNetworkPanel;
}(ExtensionPanelCore);