"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HorizontalPageCreator = function (_PageCreator) {
	_inherits(HorizontalPageCreator, _PageCreator);

	function HorizontalPageCreator() {
		_classCallCheck(this, HorizontalPageCreator);

		var _this = _possibleConstructorReturn(this, (HorizontalPageCreator.__proto__ || Object.getPrototypeOf(HorizontalPageCreator)).call(this));

		_this.positionMap = new Map(); // element 개수에 따라 body line 위치 변경 및 엘리먼트 위치 변경 값을 담는 맵
		return _this;
	}

	_createClass(HorizontalPageCreator, [{
		key: "createPage",
		value: function createPage(pageData, networkProxy) {
			_get(HorizontalPageCreator.prototype.__proto__ || Object.getPrototypeOf(HorizontalPageCreator.prototype), "createPage", this).call(this, pageData, networkProxy);
		}
	}, {
		key: "_createHeader",
		value: function _createHeader() {
			var _this2 = this;

			var layoutManager = new HeaderGroupExcelCreator();
			var comInstanceInfoMap = new Map();
			var rows = this._networkProxy._headerElementInfoMap.size;
			var columns = 0;

			this._networkProxy._headerElementInfoMap.forEach(function (elementInfoList, row) {
				var comInstanceVO1 = layoutManager.addState(parseInt(row) - 1, 0, elementInfoList[0], elementInfoList[0].equipmentType);
				comInstanceInfoMap.set(elementInfoList[0].ip, comInstanceVO1);

				var comInstanceVO2 = layoutManager.addState(parseInt(row) - 1, 1, elementInfoList[1], elementInfoList[1].equipmentType);
				comInstanceInfoMap.set(elementInfoList[1].ip, comInstanceVO2);
			});

			var temp2 = [];
			comInstanceInfoMap.forEach(function (elementInfo) {
				temp2.push(elementInfo);
				_this2._pageDataList.content_info.two_layer.push(elementInfo);
			});

			var temp = [];
			this._networkProxy._headerPortInfoList.forEach(function (item, index) {
				item.from.target = item.from.ip.replace(/\./g, "_"); // 치환
				item.to.target = item.to.ip.replace(/\./g, "_"); // 치환

				var comInstanceVO = layoutManager.addLine(item.from, item.to, index, item.gap);
				_this2._pageDataList.content_info.two_layer.push(comInstanceVO);

				temp.push(comInstanceVO);
			});

			this._networkLinkSetPosition(temp2, this._networkProxy._bodyElementInfoList.length);
			this._createHeaderToBodyPort();
			this._setPositionMap();
		}
	}, {
		key: "_networkLinkSetPosition",
		value: function _networkLinkSetPosition(instanceList, accessDataListLength) {
			if (instanceList.length == 1) {
				// 하나일 경우 위치
				instanceList[0].props.setter.x = 340;
				instanceList[0].props.setter.y = 350;
			}

			if (instanceList.length == 2) {
				// 정상 위치
				instanceList[0].props.setter.x = 340;
				instanceList[0].props.setter.y = 350;
				instanceList[0].props.label.label_position = "CT";
				instanceList[0].props.label.label_offset_y = "-40";

				instanceList[1].props.setter.x = 340;
				instanceList[1].props.setter.y = 680;
			}
		}

		// 링크와 바디를 잇는 port 생성하기

	}, {
		key: "_createHeaderToBodyPort",
		value: function _createHeaderToBodyPort() {
			var horizontalLineTop = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo);
			var horizontalLineBottom = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo);

			horizontalLineTop.name = "header_to_body_1";
			horizontalLineBottom.name = "header_to_body_2";
			horizontalLineTop.props.setter.width = 40;
			horizontalLineTop.props.setter.height = 20;
			horizontalLineBottom.props.setter.width = 50;
			horizontalLineBottom.props.setter.height = 20;

			horizontalLineTop.props.setter.x = 396;
			horizontalLineTop.props.setter.y = 366;
			horizontalLineBottom.props.setter.x = 396;
			horizontalLineBottom.props.setter.y = 696;

			horizontalLineBottom.props.portInfo.lineStyle = "dashed";

			this._pageDataList.content_info.two_layer.push(horizontalLineTop);
			this._pageDataList.content_info.two_layer.push(horizontalLineBottom);

			var verticalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);
			var verticalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);

			verticalSolidPort.name = "header_to_body_3";
			verticalDashedPort.name = "header_to_body_4";
			verticalSolidPort.props.setter.x = 417;
			verticalSolidPort.props.setter.y = 174;
			verticalSolidPort.props.setter.width = 30;
			verticalSolidPort.props.setter.height = 760;
			verticalDashedPort.props.setter.x = 427;
			verticalDashedPort.props.setter.y = 174;
			verticalDashedPort.props.setter.width = 30;
			verticalDashedPort.props.setter.height = 760;
			verticalDashedPort.props.portInfo.lineStyle = "dashed";

			this._pageDataList.content_info.two_layer.push(verticalSolidPort);
			this._pageDataList.content_info.two_layer.push(verticalDashedPort);
		}

		// NetWorkState 개수에 따라 시작위치를 정하고, width, height도 같이 정하여 준다.

	}, {
		key: "_setPositionMap",
		value: function _setPositionMap() {
			// startPositionX : NetworkState X 시작 위치가 됩니다.
			// startPositionX :  NetworkState Y 시작 위치가 됩니다.
			// width :  NetworkState의 width 간격이 됩니다.
			// width :  NetworkState의 height 간격이 됩니다.
			var positionObj = {
				"startPositionX": 490,
				"startPositionY": 430,
				"width": 230,
				"height": 200
			};
			this.positionMap.set(5, positionObj);
			// 5는 NetworkState 5개 이하일 경우의 위치입니다.

			positionObj = {
				"startPositionX": 490,
				"startPositionY": 320,
				"width": 230,
				"height": 200
			};
			this.positionMap.set(10, positionObj);
			// 10는 NetworkState 10개 이하일 경우의 위치입니다.

			positionObj = {
				"startPositionX": 490,
				"startPositionY": 250,
				"width": 230,
				"height": 200
			};
			this.positionMap.set(15, positionObj);

			positionObj = {
				"startPositionX": 490,
				"startPositionY": 220,
				"width": 230,
				"height": 180
			};
			this.positionMap.set(20, positionObj);

			positionObj = {
				"startPositionX": 490,
				"startPositionY": 250,
				"width": 140,
				"height": 200
			};
			this.positionMap.set(30, positionObj);

			positionObj = {
				"startPositionX": 490,
				"startPositionY": 220,
				"width": 140,
				"height": 180
			};
			this.positionMap.set(40, positionObj);
		}
	}, {
		key: "_createBody",
		value: function _createBody() {
			var _this3 = this;

			// NetworkState 개수에 따라 시작 위치를 변경하여 준다.
			// 위에서 설정해 놓았던 시작위치를 element개수에 따라 불러옵니다.
			var positionMap = [];
			if (this._networkProxy._bodyElementInfoList.length <= 5) {
				positionMap = this.positionMap.get(5);
			} else if (this._networkProxy._bodyElementInfoList.length <= 10) {
				positionMap = this.positionMap.get(10);
			} else if (this._networkProxy._bodyElementInfoList.length <= 15) {
				positionMap = this.positionMap.get(15);
			} else if (this._networkProxy._bodyElementInfoList.length <= 20) {
				positionMap = this.positionMap.get(20);
			} else if (this._networkProxy._bodyElementInfoList.length <= 30) {
				positionMap = this.positionMap.get(30);
			} else {
				positionMap = this.positionMap.get(40);
			}

			// 시작위치 설정
			this.startPositionX = positionMap.startPositionX;
			this.startPositionY = positionMap.startPositionY;
			this.width = positionMap.width;
			this.height = positionMap.height;
			var comInstanceInfoMap = new Map();

			var layoutManager = new HeaderGroupExcelCreator();
			var tempRowsValue = this._networkProxy._bodyElementInfoList.length;

			/*
   	한칸에 배치되는 columns의 수를 변경하고 싶다면 column을 변경,
   	columns 수를 변경하면 _setPositionMap() 함수의 width, height도 맞게 변경
   	Network State 는 기본적으로 5개의 colmun으로 생성되지만, 20개가 넘어가면 10로 변경
    */
			var columns = 5; //
			var height = 760;
			var rows = tempRowsValue / columns;

			var column = 0;
			var row = 0;

			if (this._networkProxy._bodyElementInfoList.length > 20) {
				columns = 10; // 20개가 넘어가면 10로 변경
			}

			var nextPositionY = 0;
			// 별찍기 로직
			this._networkProxy._bodyElementInfoList.forEach(function (elementInfo) {
				var comInstanceVO = layoutManager.addState("A", column, elementInfo, elementInfo.equipmentType);
				comInstanceVO.props.label.label_position = "CT"; // 라벨 위치 변경
				comInstanceVO.props.label.label_offset_y = "-37"; // 라벨 위치 변경

				_this3._setBodyElement(comInstanceVO, row, column);
				nextPositionY = _this3.startPositionY;
				_this3._pageDataList.content_info.two_layer.push(comInstanceVO);
				if (comInstanceInfoMap.has(row) == false) {
					comInstanceInfoMap.set(row, []);
				}

				var tempArr = comInstanceInfoMap.get(row); // row에 column 개수를 1로 푸쉬하여, 줄을 그릴 때 사용한다.
				tempArr.push(1);

				column++;
				if (columns <= column) {
					nextPositionY = nextPositionY + _this3.height * row;
					column = 0;
					row++;
				}
			});

			comInstanceInfoMap.forEach(function (valueArr, index) {
				var widthVal = _this3.width * valueArr.length;
				_this3._setBodyElementVerticalPort(index, widthVal);
			});
		}

		//저장된 comInstanceInfoMap에 있는 값으로 선을 그려준다.

	}, {
		key: "_setBodyElementVerticalPort",
		value: function _setBodyElementVerticalPort(index, width) {
			var horizontalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo);
			var horizontalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo);

			horizontalSolidPort.name = "body_solid_line_" + index;
			horizontalSolidPort.props.setter.width = width;
			horizontalSolidPort.props.setter.height = 20;
			horizontalSolidPort.props.setter.x = this.startPositionX - 62;
			horizontalSolidPort.props.setter.y = this.startPositionY + 86 + index * this.height;

			horizontalDashedPort.name = "body_dashed_line_" + index;
			horizontalDashedPort.props.setter.width = width;
			horizontalDashedPort.props.setter.height = 20;
			horizontalDashedPort.props.setter.x = this.startPositionX - 52;
			horizontalDashedPort.props.setter.y = this.startPositionY + 96 + index * this.height;
			horizontalDashedPort.props.portInfo.lineStyle = "dashed";

			this._pageDataList.content_info.two_layer.push(horizontalSolidPort);
			this._pageDataList.content_info.two_layer.push(horizontalDashedPort);
		}
	}, {
		key: "_setBodyElement",
		value: function _setBodyElement(comInstanceVO, row, column) {
			var _this4 = this;

			// horizontal의 경우 동일한 위치로 했을 때 network state 의 위치가 제각각으로 나와서 보정을 해준다.
			var equipmentType = comInstanceVO.props.network.equipmentType;
			if (equipmentType === "Backbone") {
				//  560 250
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 12;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 34;
			} else if (equipmentType === "Router") {
				//  560 250
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 2;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 6;
			} else if (equipmentType === "Firewall") {
				//  560 250
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 10;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 7;
			} else if (equipmentType === "Server") {
				//  560 250
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 10;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 37;
			} else if (equipmentType === "Building") {
				//  560 250
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 10;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 5;
			} else if (equipmentType === "Switch01") {
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 12;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 14;
			} else if (equipmentType === "Switch02") {
				// 770 220
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 5;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 15;
			} else if (equipmentType === "Switch03") {
				// 770 220
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 5;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 15;
			} else if (equipmentType === "Switch04") {
				// 1195 205
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 10;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 5;
			} else if (equipmentType === "Switch05") {
				// 1335 205
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 10;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 34;
			} else if (equipmentType === "Switch06") {
				//
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 10;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 34;
			} else if (equipmentType === "Switch07") {
				//
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column + 10;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row - 40;
			} else {
				// 스위치인 경우
				comInstanceVO.props.setter.x = this.startPositionX + this.width * column;
				comInstanceVO.props.setter.y = this.startPositionY + this.height * row;
			}

			// body부분 해당 network state의 ip로 된 port가 있으면, port를 추가하여 준다..
			var comInstancePorts = this._networkProxy._bodyPortInfoList.filter(function (portInfo) {
				return portInfo.to.ip.indexOf(comInstanceVO.props.network.ip) != -1;
			});

			if (comInstancePorts) {
				comInstancePorts.forEach(function (portInfo, index) {
					if (portInfo.type == "A") {
						var verticalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalPortMetaInfo);
						verticalSolidPort.props.setter.width = 20;
						verticalSolidPort.props.setter.height = 60;

						verticalSolidPort.props.setter.x = _this4.startPositionX + _this4.width * column + 20;
						verticalSolidPort.props.setter.y = _this4.startPositionY + _this4.height * row + 40;

						verticalSolidPort.props.portInfo.position = "left";
						verticalSolidPort.props.portInfo.textLeftMargin = "0px";

						verticalSolidPort.props.portInfo.textTopMargin = "5px";
						verticalSolidPort.props.portInfo.textBottomMargin = "10px";

						verticalSolidPort.props.portInfo.fromHost = portInfo.from.hostName;
						verticalSolidPort.props.portInfo.fromIP = portInfo.from.ip;
						verticalSolidPort.props.portInfo.fromText = portInfo.from.port;

						verticalSolidPort.props.portInfo.toHost = portInfo.to.hostName;
						verticalSolidPort.props.portInfo.toIP = portInfo.to.ip;
						verticalSolidPort.props.portInfo.toText = portInfo.to.port;
						verticalSolidPort.name = portInfo.to.ip + "_port_a";

						_this4._pageDataList.content_info.two_layer.push(verticalSolidPort);
					} else {
						var verticalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalPortMetaInfo);
						verticalDashedPort.props.setter.width = 20;
						verticalDashedPort.props.setter.height = 70;

						verticalDashedPort.props.setter.x = _this4.startPositionX + _this4.width * column + 30;
						verticalDashedPort.props.setter.y = _this4.startPositionY + _this4.height * row + 40;

						verticalDashedPort.props.portInfo.position = "right";
						verticalDashedPort.props.portInfo.lineStyle = "dashed";

						verticalDashedPort.props.portInfo.textTopMargin = "5px";
						verticalDashedPort.props.portInfo.textBottomMargin = "20px";

						verticalDashedPort.props.portInfo.fromHost = portInfo.from.hostName;
						verticalDashedPort.props.portInfo.fromIP = portInfo.from.ip;
						verticalDashedPort.props.portInfo.fromText = portInfo.from.port;

						verticalDashedPort.props.portInfo.toHost = portInfo.to.hostName;
						verticalDashedPort.props.portInfo.toIP = portInfo.to.ip;
						verticalDashedPort.props.portInfo.toText = portInfo.to.port;
						verticalDashedPort.name = portInfo.to.ip + "_port_b";

						_this4._pageDataList.content_info.two_layer.push(verticalDashedPort);
					}
				});
			}
		}

		//bad 추가 부분

	}, {
		key: "_createBad",
		value: function _createBad() {
			var _this5 = this;

			var layoutManager = new HeaderGroupExcelCreator();
			var badLeft = 1800;
			var badTop = 170;

			this._networkProxy._badElementInfoList.forEach(function (elementInfo) {
				var comInstanceVO = layoutManager.addState("BAD", -1, elementInfo, elementInfo.equipmentType);
				badTop = _this5._badComponentNextPosition(badTop);
				comInstanceVO.props.setter.x = badLeft;
				comInstanceVO.props.setter.y = badTop;
				_this5._pageDataList.content_info.two_layer.push(comInstanceVO);
			});

			this._networkProxy._badPortInfoList.forEach(function (portInfo, index) {
				var portVO = layoutManager.addElementPort("BAD", portInfo, index);
				badTop = _this5._badComponentNextPosition(badTop);
				portVO.props.setter.x = badLeft;
				portVO.props.setter.y = badTop;
				_this5._pageDataList.content_info.two_layer.push(portVO);
			});
		}
	}, {
		key: "_badComponentNextPosition",
		value: function _badComponentNextPosition(badTop) {
			var emptySpace = 20;
			var limitSize = 1000;

			badTop += emptySpace;
			if (badTop > limitSize) {
				badTop = limitSize;
			}

			return badTop;
		}
	}]);

	return HorizontalPageCreator;
}(PageCreator);