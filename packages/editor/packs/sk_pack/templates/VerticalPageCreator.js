class VerticalPageCreator extends PageCreator {
	constructor() {
		super();

		this.startPositionX = 560;
		this.startPositionY = 500;
		this.height = 100;
		this.width = 280;
	}

	createPage(pageData, networkProxy) {
		super.createPage(pageData, networkProxy);
	}

	_createHeader() {
		let layoutManager = new HeaderGroupExcelCreator();
		let comInstanceInfoMap = new Map();
		let rows = this._networkProxy._headerElementInfoMap.size;
		let columns = 0;
		this._networkProxy._headerElementInfoMap.forEach((value, key) => {
			columns = Math.max(columns, value.length);
		})

		this._networkProxy._headerElementInfoMap.forEach((elementInfoList, row) => {
			try {
				let comInstanceVO1 = layoutManager.addState(parseInt(row) - 1, 0, elementInfoList[0], elementInfoList[0].equipmentType);
				comInstanceVO1.props.label.label_position = "LC";
				this.get_label_size(comInstanceVO1);
				comInstanceInfoMap.set(elementInfoList[0].ip, comInstanceVO1);

				let comInstanceVO2 = layoutManager.addState(parseInt(row) - 1, 1, elementInfoList[1], elementInfoList[1].equipmentType);
				comInstanceVO2.props.label.label_position = "RC";
				this.get_label_size(comInstanceVO2);
				comInstanceInfoMap.set(elementInfoList[1].ip, comInstanceVO2);
			} catch (e) {
				console.log("header 구현부에서 에러발생", e);
			}
		})

		let temp = [];
		let temp2 = [];
		comInstanceInfoMap.forEach((elementInfo, row) => {
			temp2.push(elementInfo);
			this._pageDataList.content_info.two_layer.push(elementInfo);
		})

		this._networkProxy._headerPortInfoList.forEach((item, index) => {
			item.from.target = item.from.ip.replace(/\./g, "_"); // 치환
			item.to.target = item.to.ip.replace(/\./g, "_"); // 치환

			let comInstanceVO = layoutManager.addLine(item.from, item.to, index, item.gap);
			this._pageDataList.content_info.two_layer.push(comInstanceVO);

			temp.push(comInstanceVO);
			// addLine
		})

		this._networkLinkSetPosition(temp2, this._networkProxy._bodyElementInfoList.length);

		this._createHeaderToBodyPort();
	}

	get_label_size(instance) {
		var hostNameLength = instance.props.network.hostName.toString().length;
		var ipValueLength = instance.props.network.ip.toString().length;

		if (hostNameLength > ipValueLength) {
			hostNameLength = hostNameLength * 7;
		} else {
			hostNameLength = ipValueLength * 7;
		}

		if ((instance.props.label.label_position == "LT") || (instance.props.label.label_position == "LC") || (instance.props.label.label_position == "LB")) {
			hostNameLength *= 1;
			instance.props.label.label_offset_x = -hostNameLength;
		} else if ((instance.props.label.label_position == "RT") || (instance.props.label.label_position == "RC") || (instance.props.label.label_position == "RB")) {
			instance.props.label.label_offset_x = hostNameLength;
		}
	}

	_createHeaderToBodyPort() {
		let verticalLineLeft = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);
		let verticalLineRight = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);

		verticalLineLeft.name = "header_to_body_1";
		verticalLineRight.name = "header_to_body_2";
		verticalLineLeft.props.setter.width = 20;
		verticalLineLeft.props.setter.height = 40;
		verticalLineRight.props.setter.width = 30;
		verticalLineRight.props.setter.height = 50;

		verticalLineLeft.props.setter.x = 932;
		verticalLineLeft.props.setter.y = 424;
		verticalLineRight.props.setter.x = 1310;
		verticalLineRight.props.setter.y = 424;
		verticalLineRight.props.portInfo.lineStyle = "dashed";

		this._pageDataList.content_info.two_layer.push(verticalLineLeft);
		this._pageDataList.content_info.two_layer.push(verticalLineRight);

		let horizontalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo);
		let horizontalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo);

		horizontalSolidPort.name = "header_to_body_3";
		horizontalDashedPort.name = "header_to_body_4";
		horizontalSolidPort.props.setter.x = 400;
		horizontalSolidPort.props.setter.y = 450;
		horizontalDashedPort.props.setter.x = 400;
		horizontalDashedPort.props.setter.y = 460;
		horizontalDashedPort.props.portInfo.lineStyle = "dashed";

		this._pageDataList.content_info.two_layer.push(horizontalSolidPort);
		this._pageDataList.content_info.two_layer.push(horizontalDashedPort);
	}

	_createBody() {
		let bodyLength = this._networkProxy._bodyElementInfoList.length;
		if (bodyLength < 26) {
			this._createBody25();
		} else {
			this._createBody42();
		}
	}

	_createBody25() {
		// 바디 부분 구현
		let layoutManager = new HeaderGroupExcelCreator();
		let comInstanceInfoMap = new Map();
		let columns = 5;

		let column = 0;
		let row = 0;

		this._networkProxy._bodyElementInfoList.forEach((elementInfo) => {
			let comInstanceVO = layoutManager.addState("A", column, elementInfo, elementInfo.equipmentType);
			this._setBodyElement(comInstanceVO, row, column);

			this._pageDataList.content_info.two_layer.push(comInstanceVO);
			if (comInstanceInfoMap.has(column) == false) {
				comInstanceInfoMap.set(column, []);
			}

			let tempArr = comInstanceInfoMap.get(column);
			tempArr.push(1);

			column++;
			if (column >= columns) {
				row += 1;
				column = 0;
			}
		})

		comInstanceInfoMap.forEach((valueArr, index) => {
			let heightVal = (this.height * valueArr.length);
			this._setBodyElementVerticalPort(index, heightVal);
		})
	}

	_setBodyElementVerticalPort(index, height) {
		let verticalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);
		let verticalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);

		verticalSolidPort.name = "body_solid_line_" + index;
		verticalSolidPort.props.setter.width = 20;
		verticalSolidPort.props.setter.height = height;
		verticalSolidPort.props.setter.x = this.startPositionX - 136 + (index * this.width);
		verticalSolidPort.props.setter.y = this.startPositionY - 44;

		verticalDashedPort.name = "body_dashed_line_" + index;
		verticalDashedPort.props.setter.width = 20;
		verticalDashedPort.props.setter.height = height;
		verticalDashedPort.props.setter.x = this.startPositionX - 126 + (index * this.width);
		verticalDashedPort.props.setter.y = this.startPositionY - 34;
		verticalDashedPort.props.portInfo.lineStyle = "dashed";
		if (this._networkProxy._bodyElementInfoList.length > 25) {
			verticalSolidPort.props.setter.y = this.startPositionY - 30;
			verticalDashedPort.props.setter.y = this.startPositionY - 24;
		}

		this._pageDataList.content_info.two_layer.push(verticalSolidPort);
		this._pageDataList.content_info.two_layer.push(verticalDashedPort);
	}

	/*
	      NetworkState, HorizontalPort 셋팅
	 */

	_setBodyElement(comInstanceVO, row, column) {
		let comInstancePorts = this._networkProxy._bodyPortInfoList.filter((portInfo) => {
			return portInfo.to.ip.indexOf(comInstanceVO.props.network.ip) != -1;
		})

		comInstanceVO.props.setter.x = this.startPositionX + (this.width * column);
		comInstanceVO.props.setter.y = this.startPositionY + (this.height * row);

		if (comInstancePorts) {
			comInstancePorts.forEach((portInfo) => {
				if (portInfo.type == "A") {
					let horizontalSolidPort = $.extend(true, {}, PageCreator.HorizontalPortMetaInfo);
					horizontalSolidPort.props.setter.width = 120;
					horizontalSolidPort.props.setter.x = (this.startPositionX + (this.width * column)) - 130;
					horizontalSolidPort.props.setter.y = (this.startPositionY + (this.height * row));

					horizontalSolidPort.props.portInfo.textLeftMargin = "20px";
					horizontalSolidPort.props.portInfo.textRightMargin = "5px";

					horizontalSolidPort.props.portInfo.fromHost = portInfo.from.hostName;
					horizontalSolidPort.props.portInfo.fromIP = portInfo.from.ip;
					horizontalSolidPort.props.portInfo.fromText = portInfo.from.port;

					horizontalSolidPort.props.portInfo.toHost = portInfo.to.hostName;
					horizontalSolidPort.props.portInfo.toIP = portInfo.to.ip;
					horizontalSolidPort.props.portInfo.toText = portInfo.to.port;
					horizontalSolidPort.name = portInfo.to.ip + "_port_a";

					this._pageDataList.content_info.two_layer.push(horizontalSolidPort);
				} else {
					let horizontalDashedPort = $.extend(true, {}, PageCreator.HorizontalPortMetaInfo);
					horizontalDashedPort.props.setter.width = 110;

					horizontalDashedPort.props.setter.x = (this.startPositionX + (this.width * column)) - 120;
					horizontalDashedPort.props.setter.y = (this.startPositionY + (this.height * row)) + 10;

					horizontalDashedPort.props.portInfo.textLeftMargin = "10px";
					horizontalDashedPort.props.portInfo.textRightMargin = "5px";

					horizontalDashedPort.props.portInfo.lineStyle = "dashed";
					horizontalDashedPort.props.portInfo.position = "bottom";

					horizontalDashedPort.props.portInfo.fromHost = portInfo.from.hostName;
					horizontalDashedPort.props.portInfo.fromIP = portInfo.from.ip;
					horizontalDashedPort.props.portInfo.fromText = portInfo.from.port;

					horizontalDashedPort.props.portInfo.toHost = portInfo.to.hostName;
					horizontalDashedPort.props.portInfo.toIP = portInfo.to.ip;
					horizontalDashedPort.props.portInfo.toText = portInfo.to.port;
					horizontalDashedPort.name = portInfo.to.ip + "_port_b";

					this._pageDataList.content_info.two_layer.push(horizontalDashedPort);
				}
			})
		}
	}

	_createBody42() {
		this.width = 230;
		this.height = 72;
		this.startPositionX = 550;
		this.startPositionY = 476;

		let lines = this._pageDataList.content_info.two_layer.filter((item) => {
			return item.name.indexOf("header_to_body_") != -1;
		})

		lines.forEach((item) => {
			item.props.setter.y = item.props.setter.y - 10;
		})

		let layoutManager = new HeaderGroupExcelCreator();
		let comInstanceInfoMap = new Map();
		let columns = 6;

		let column = 0;
		let row = 0;

		this._networkProxy._bodyElementInfoList.forEach((elementInfo) => {
			let comInstanceVO = layoutManager.addState("A", column, elementInfo, elementInfo.equipmentType);
			this._setBodyElement(comInstanceVO, row, column);

			this._pageDataList.content_info.two_layer.push(comInstanceVO);
			if (comInstanceInfoMap.has(column) == false) {
				comInstanceInfoMap.set(column, []);
			}

			let tempArr = comInstanceInfoMap.get(column);
			tempArr.push(1);

			column++;
			if (column >= columns) {
				row += 1;
				column = 0;
			}
		})

		comInstanceInfoMap.forEach((valueArr, index) => {
			let heightVal = (this.height * valueArr.length);
			this._setBodyElementVerticalPort(index, heightVal);
		})
	}

	_setBodyElementVerticalPort42(row, column) {
		let width = 230;
		let height = 72;
		let startPositionX = 550;
		let startPositionY = 476;

		let verticalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);
		let verticalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);

		verticalSolidPort.name = "body_solid_line_" + column;
		verticalSolidPort.props.setter.width = 20;
		verticalSolidPort.props.setter.height = height * row;
		verticalSolidPort.props.setter.x = startPositionX - 130 - 6 + (column * width);
		verticalSolidPort.props.setter.y = startPositionY - 30;

		verticalDashedPort.name = "body_dashed_line_" + column;
		verticalDashedPort.props.setter.width = 20;
		verticalDashedPort.props.setter.height = height * row;
		verticalDashedPort.props.setter.x = startPositionX - 120 - 6 + (column * width);
		verticalDashedPort.props.setter.y = startPositionY - 20;
		verticalDashedPort.props.portInfo.lineStyle = "dashed";

		this._pageDataList.content_info.two_layer.push(verticalSolidPort);
		this._pageDataList.content_info.two_layer.push(verticalDashedPort);
	}

	_createBad() {
		let layoutManager = new HeaderGroupExcelCreator();
		let badLeft = 1800;
		let badTop = 170;

		this._networkProxy._badElementInfoList.forEach((elementInfo) => {
			let comInstanceVO = layoutManager.addState("BAD", -1, elementInfo, elementInfo.equipmentType);
			badTop = this._badComponentNextPosition(badTop);
			comInstanceVO.props.setter.x = badLeft;
			comInstanceVO.props.setter.y = badTop;
			comInstanceVO.name = "bad_" + comInstanceVO.name;
			this._pageDataList.content_info.two_layer.push(comInstanceVO);
		})

		this._networkProxy._badPortInfoList.forEach((portInfo, index) => {
			let portVO = layoutManager.addElementPort("BAD", portInfo, index);
			badTop = this._badComponentNextPosition(badTop);
			portVO.props.setter.x = badLeft;
			portVO.props.setter.y = badTop;
			this._pageDataList.content_info.two_layer.push(portVO);
		});
	}

	_badComponentNextPosition(badTop) {
		let emptySpace = 20;
		let limitSize = 1000;

		badTop += emptySpace;
		if (badTop > limitSize) {
			badTop = limitSize;
		}

		return badTop;
	}

	_networkLinkSetPosition(instanceList, accessDataListLength) {

		if (instanceList.length == 1) { // 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
			if (accessDataListLength < 6) {
				instanceList[0].props.setter.x = 886;
				instanceList[0].props.setter.y = 432;
			} else if (accessDataListLength < 31) {
				instanceList[0].props.setter.x = 892;
				instanceList[0].props.setter.y = 312;
			} else {
				instanceList[0].props.setter.x = 876;
				instanceList[0].props.setter.y = 464;
			}
		}

		if (instanceList.length == 2) { // 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
			instanceList[0].props.setter.x = 920;
			instanceList[0].props.setter.y = 344;

			instanceList[1].props.setter.x = 1302;
			instanceList[1].props.setter.y = 344;
		}

		if (instanceList.length == 3) {
			if (accessDataListLength < 6) {
				instanceList[0].props.setter.x = 886;
				instanceList[0].props.setter.y = 260;

				instanceList[1].props.setter.x = 1270;
				instanceList[1].props.setter.y = 260;

				instanceList[2].props.setter.x = 886;
				instanceList[2].props.setter.y = 430;

			} else if (accessDataListLength < 31) {
				instanceList[0].props.setter.x = 892;
				instanceList[0].props.setter.y = 145;

				instanceList[1].props.setter.x = 1276;
				instanceList[1].props.setter.y = 145;

				instanceList[2].props.setter.x = 892;
				instanceList[2].props.setter.y = 315;

			} else {
				instanceList[0].props.setter.x = 876;
				instanceList[0].props.setter.y = 464;

				instanceList[1].props.setter.x = 1260;
				instanceList[1].props.setter.y = 464;

				instanceList[2].props.setter.x = 876;
				instanceList[2].props.setter.y = 464;

			}
		}

		if (instanceList.length == 4) {
			instanceList[0].props.setter.x = 920;
			instanceList[0].props.setter.y = 170;

			instanceList[1].props.setter.x = 1302;
			instanceList[1].props.setter.y = 170;

			instanceList[2].props.setter.x = 920;
			instanceList[2].props.setter.y = 344;

			instanceList[3].props.setter.x = 1302;
			instanceList[3].props.setter.y = 344;
		}
	}

}
