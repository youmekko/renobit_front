"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var vertical_page = function (_basic_page) {
	_inherits(vertical_page, _basic_page);

	function vertical_page() {
		_classCallCheck(this, vertical_page);

		return _possibleConstructorReturn(this, (vertical_page.__proto__ || Object.getPrototypeOf(vertical_page)).call(this));
	}

	_createClass(vertical_page, [{
		key: "createPage",
		value: function createPage(pageData, headerPortDataList, excelRowDataList, excelPortDataList) {
			_get(vertical_page.prototype.__proto__ || Object.getPrototypeOf(vertical_page.prototype), "createPage", this).call(this, pageData, headerPortDataList, excelRowDataList, excelPortDataList);
		}

		//@Override

	}, {
		key: "_attachLink",
		value: function _attachLink() {
			var linkDataList = [];
			var instanceList = [];

			var firstLinkDataList = this._excelRowDataList.get("1"); // 1단 불러옴
			var secondLinkDataList = this._excelRowDataList.get("2"); // 2단 불러옴
			var accessDataList = this._excelRowDataList.get("A"); // a단 불러옴

			var positionLeft = 0;
			var positionTop = 0;
			var metaInfo = null;

			linkDataList = linkDataList.concat(firstLinkDataList); // 1단 추가
			if (secondLinkDataList) {
				// 2단이 있으면
				linkDataList = linkDataList.concat(secondLinkDataList); // 2단 추가
			}

			// console.log("_pageDataList%$%", firstLinkDataList);
			// console.log("$%$%", secondLinkDataList);
			// console.log("$%$%", accessDataList);
			// console.log("$%$%", this.DefaultRowVO);
			// console.log("linkDataList", linkDataList);

			if (this._headerPortDataList) {
				this._headerPortDataList.forEach(function (networkState, index) {
					var newInstanceInfo = $.extend(true, {}, basic_page.NetworkStateMetaInfo); // link Instance
					newInstanceInfo.props.name = "network_link_" + index;
					// let networkVO =  $.extend({}, this.DefaultRowVO); // network value
					// networkVO.
					var equipmentType = networkState["장비종류"];
					var comVO = basic_page.EquipmentTypeToComponent[equipmentType];
					// Backbone
					newInstanceInfo.name = networkState.IP.replace(/\./g, "_"); // 치환
					newInstanceInfo.props.setter.info.name = comVO.name;
					newInstanceInfo.props.setter.info.path = comVO.path;
					// alias + ip 출력
					newInstanceInfo.props.network.manufacturer = networkState["제조사"];
					// newInstanceInfo.props.network.company_label = networkState["제조사"];
					newInstanceInfo.props.network.ip = networkState["IP"];
					newInstanceInfo.props.network.hostName = networkState["장비명"];
					//network 속성 설정
					newInstanceInfo.props.network.row = networkState["ROW"] + ""; // int로 되는 경우가 있어서 뒤에 +""

					if (networkState["serial_number"]) {
						newInstanceInfo.props.network.serial_number = networkState["serial_number"];
					}

					if (networkState["model"]) {
						newInstanceInfo.props.network.model = networkState["model"];
					}

					if (networkState["cid"]) {
						newInstanceInfo.props.network.cid = networkState["cid"];
					}

					// Network State 마다 적정 크기가 달라서 info 에 width, height가 있을 경우에만 사이즈를 변경해준다.
					if (comVO.hasOwnProperty("width")) {
						newInstanceInfo.props.setter.width = comVO.width;
					}

					if (comVO.hasOwnProperty("height")) {
						newInstanceInfo.props.setter.height = comVO.height;
					}

					instanceList.push(newInstanceInfo);
					console.log("newInstanceInfonewInstanceInfo", newInstanceInfo);
					// this._pageDataList.content_info.two_layer.push(newInstanceInfo);
				});
			}

			this._networkLinkSetPosition(instanceList, accessDataList.length);
			this._networkLinkPortSetting();
		}

		// 포지션은 1,2,3,4 라고 생각한다.

	}, {
		key: "_networkLinkSetPosition",
		value: function _networkLinkSetPosition(instanceList, accessDataListLength) {
			var _this2 = this;

			if (instanceList.length == 1) {
				// 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
				if (accessDataListLength < 6) {
					instanceList[0].props.setter.x = 886;
					instanceList[0].props.setter.y = 432;
				} else if (accessDataListLength < 31) {
					instanceList[0].props.setter.x = 892;
					instanceList[0].props.setter.y = 312;
				} else {
					instanceList[0].props.setter.x = 876;
					instanceList[0].props.setter.y = 464;
				}
			}

			if (instanceList.length == 2) {
				// 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
				if (accessDataListLength < 6) {
					instanceList[0].props.setter.x = 886;
					instanceList[0].props.setter.y = 432;

					instanceList[1].props.setter.x = 1268;
					instanceList[1].props.setter.y = 432;
				} else if (accessDataListLength < 31) {
					instanceList[0].props.setter.x = 892;
					instanceList[0].props.setter.y = 312;

					instanceList[1].props.setter.x = 1278;
					instanceList[1].props.setter.y = 312;
				} else {
					instanceList[0].props.setter.x = 876;
					instanceList[0].props.setter.y = 464;

					instanceList[1].props.setter.x = 1260;
					instanceList[1].props.setter.y = 464;
				}
			}

			if (instanceList.length == 3) {
				if (accessDataListLength < 6) {
					instanceList[0].props.setter.x = 886;
					instanceList[0].props.setter.y = 260;

					instanceList[1].props.setter.x = 1270;
					instanceList[1].props.setter.y = 260;

					instanceList[2].props.setter.x = 886;
					instanceList[2].props.setter.y = 430;
				} else if (accessDataListLength < 31) {
					instanceList[0].props.setter.x = 892;
					instanceList[0].props.setter.y = 145;

					instanceList[1].props.setter.x = 1276;
					instanceList[1].props.setter.y = 145;

					instanceList[2].props.setter.x = 892;
					instanceList[2].props.setter.y = 315;
				} else {
					instanceList[0].props.setter.x = 876;
					instanceList[0].props.setter.y = 464;

					instanceList[1].props.setter.x = 1260;
					instanceList[1].props.setter.y = 464;

					instanceList[2].props.setter.x = 876;
					instanceList[2].props.setter.y = 464;
				}
			}

			if (instanceList.length == 4) {
				if (accessDataListLength < 6) {
					instanceList[0].props.setter.x = 886;
					instanceList[0].props.setter.y = 260;

					instanceList[1].props.setter.x = 1270;
					instanceList[1].props.setter.y = 260;

					instanceList[2].props.setter.x = 886;
					instanceList[2].props.setter.y = 430;

					instanceList[3].props.setter.x = 1270;
					instanceList[3].props.setter.y = 430;
				} else if (accessDataListLength < 31) {
					instanceList[0].props.setter.x = 892;
					instanceList[0].props.setter.y = 145;

					instanceList[1].props.setter.x = 1276;
					instanceList[1].props.setter.y = 145;

					instanceList[2].props.setter.x = 892;
					instanceList[2].props.setter.y = 315;

					instanceList[3].props.setter.x = 1276;
					instanceList[3].props.setter.y = 315;
				} else {
					instanceList[0].props.setter.x = 876;
					instanceList[0].props.setter.y = 464;

					instanceList[1].props.setter.x = 1260;
					instanceList[1].props.setter.y = 464;

					instanceList[2].props.setter.x = 876;
					instanceList[2].props.setter.y = 464;

					instanceList[3].props.setter.x = 1260;
					instanceList[3].props.setter.y = 464;
				}
			}

			instanceList.forEach(function (item) {
				_this2._pageDataList.content_info.two_layer.push(item);
			});
		}
	}, {
		key: "_networkLinkPortSetting",
		value: function _networkLinkPortSetting() {
			var _this3 = this;

			console.log("headerPortDataListdd", this._headerPortDataList);
			// console.log("headerPortDataListdd", this._sourceCompositionData); // 구성정보
			var positionX = 40;
			var positionY = 100;

			var excelData = this._excelPortDataList.slice();

			excelData.some(function (portItem, index) {
				var portInstance = null;
				var toNetworkState = null;
				var fromNetworkState = null;
				toNetworkState = _this3._headerPortDataList.find(function (item) {
					// Link 리스트에 To IP로 된 networkState를 찾는다.
					return item["IP"].indexOf(portItem["IP_1"]) != -1;
				});

				if (toNetworkState) {
					// Link 리스트에 To state가 있을 경우에만 from을 찾음.
					fromNetworkState = _this3._headerPortDataList.find(function (item) {
						return item["IP"].indexOf(portItem["IP"]) != -1;
					});
					portInstance = $.extend(true, {}, basic_page.NetworkLinkMetaInfo);
					portInstance.name = "net_link_" + index;
					portInstance.componentName = "NetworkLinkComponent";
				} else {
					return false;
				}

				if (fromNetworkState) {
					portInstance.props.network.from.ip = fromNetworkState["IP"];
					portInstance.props.network.from.hostName = fromNetworkState["장비명"];
					portInstance.props.network.from.port = portItem["PORT"];
					portInstance.props.network.from.target = fromNetworkState["IP"].replace(/\./g, "_");
				}

				if (toNetworkState) {
					portInstance.props.network.to.ip = toNetworkState["IP"];
					portInstance.props.network.to.hostName = toNetworkState["장비명"];
					portInstance.props.network.to.port = portItem["PORT_1"];
					portInstance.props.network.to.target = toNetworkState["IP"].replace(/\./g, "_");
				}

				if (portInstance) {
					// null이 아닐 경우에만 추가
					_this3._pageDataList.content_info.two_layer.push(portInstance);
				}
			});
		}

		// @Override
		// _changeNetworkSymbolInfo(newInstance, networkState) {
		// 	super._changeNetworkSymbolInfo(newInstance, networkState);
		//
		// 	// var hostNameLength = instance.props.network.hostName.toString().length;
		// 	// var ipValueLength = instance.props.network.ip.toString().length;
		// 	//
		// 	// if (hostNameLength > ipValueLength) {
		// 	// 	hostNameLength = hostNameLength * 8
		// 	// } else {
		// 	// 	hostNameLength = ipValueLength * 8;
		// 	// }
		// 	//
		// 	// if ((instance.props.label.label_position == "LT") || (instance.props.label.label_position == "LC") || (instance.props.label.label_position == "LB")) {
		// 	// 	hostNameLength *= 1;
		// 	// 	instance.props.label.label_offset_x = -hostNameLength;
		// 	// } else if ((instance.props.label.label_position == "RT") || (instance.props.label.label_position == "RC") || (instance.props.label.label_position == "RB")) {
		// 	// 	instance.props.label.label_offset_x = hostNameLength;
		// 	// }
		// }

	}, {
		key: "_getStartPosition",
		value: function _getStartPosition(rowInfo) {
			var position = {};

			if (rowInfo.length <= 5) {
				position = { // 5개
					x: 668,
					y: 586
				};
			} else if (rowInfo.length > 5 && rowInfo.length <= 9) {
				// 7~9
				position = {
					x: 605,
					y: 470
				};
			} else if (rowInfo.length > 9 && rowInfo.length <= 12) {
				// 10~12
				position = {
					x: 625,
					y: 450
				};
			} else if (rowInfo.length > 12 && rowInfo.length <= 15) {
				// 13~15
				position = {
					x: 625,
					y: 450
				};
			} else if (rowInfo.length > 15 && rowInfo.length <= 20) {
				// 20
				position = {
					x: 510,
					y: 448
				};
			} else if (rowInfo.length > 20 && rowInfo.length <= 25) {
				position = {
					x: 350,
					y: 448
				};
			} else if (rowInfo.length > 25 && rowInfo.length <= 30) {
				position = {
					x: 335,
					y: 448
				};
			} else {
				position = {
					x: 320,
					y: 436
				};
			}
			return position;
		}
	}, {
		key: "_getNetworkComponentNextPosition",
		value: function _getNetworkComponentNextPosition(rowInfo, colIndex, position) {
			if (rowInfo.length <= 5) {
				position.x += this.templateRect.width; //5개 이하 포지션 변경
			} else {
				position.y += this.templateRect.height; //5개 이상(42개) 포지션 변경
			}

			if (rowInfo.length == 6) {
				if (colIndex % 2 == 0) {
					// 10개
					position.x += 300;
					position.y = 470;
				}
			} else if (rowInfo.length > 6 && rowInfo.length <= 9) {
				//6~9
				// this.baiscLineList[0].props
				if (colIndex % 7 == 0) {
					this.drawLine(position.x + 20, rowInfo.length, 7, 29);
				}

				if (colIndex % 3 == 0) {
					// 9개
					position.x += 300;
					position.y = 470;
				}
			} else if (rowInfo.length > 9 && rowInfo.length <= 12) {
				//12개
				if (colIndex % 10 == 0) {
					this.drawLine(position.x, rowInfo.length, 9, 29);
				}

				if (colIndex % 4 == 0) {
					position.x += 300;
					position.y = 450;
				}
			} else if (rowInfo.length > 12 && rowInfo.length <= 15) {
				//15개
				if (colIndex % 11 == 0) {
					this.drawLine(position.x, rowInfo.length, 11, 29);
				}

				if (colIndex % 5 == 0) {
					position.x += 300;
					position.y = 450;
				}
			} else if (rowInfo.length > 15 && rowInfo.length <= 20) {
				//20개
				if (colIndex % 16 == 0) {
					this.drawLine(position.x, rowInfo.length, 16, 29);
				}

				if (colIndex % 5 == 0) {
					// 20개
					position.x += 300;
					position.y = 448;
				}
			} else if (rowInfo.length > 20 && rowInfo.length <= 25) {
				//25개
				if (colIndex % 21 == 0) {
					this.drawLine(position.x, rowInfo.length, 21, 29);
				}

				if (colIndex % 5 == 0) {
					// 24개
					position.x += 300;
					position.y = 448;
				}
			} else if (rowInfo.length > 25 && rowInfo.length <= 30) {
				//30개
				if (colIndex % 26 == 0) {
					this.drawLine(position.x, rowInfo.length, 26, 14);
				}

				if (colIndex % 5 == 0) {
					// 30개
					position.x += 265;
					position.y = 448;
				}
			} else {

				if (colIndex % 37 == 0) {
					this.drawLine(position.x, rowInfo.length, 37, 14);
				}

				if (colIndex % 6 == 0) {
					// 42개
					position.x += 225;
					position.y = 436;
				}
			}

			return position;
		}
	}, {
		key: "drawLine",
		value: function drawLine(positionX, length, lineNum, value) {
			// 보정 값:  받는 값이 tmp_wrapper안에 있는 x공백임. 추후 계산해서 변경
			positionX += value;

			// 페이지에서 circle 아무거나 찾아온다.
			var circle = this._pageDataList.content_info.two_layer.find(function (comInstanceInfo) {
				return comInstanceInfo.name.indexOf("svg_cir") == 0;
			});

			var solidCircleStart = $.extend(true, {}, circle);
			var dashedCircleStart = $.extend(true, {}, circle);

			var solidCircleEnd = $.extend(true, {}, circle);
			var dashedCircleEnd = $.extend(true, {}, circle);

			var positionY1 = this.baiscLineList.solidLine.props.setter.y;
			var positionY2 = null;

			var solidLine = $.extend(true, {}, this.baiscLineList.solidLine);
			var dashedLine = $.extend(true, {}, this.baiscLineList.dashedLine);

			var temp = 0;

			if (length) {
				temp = length % lineNum;
				temp++;
				positionY2 = parseInt(this.templateRect.height * temp);
			}

			var solidLineEndPosition = positionY1 + positionY2;

			// solid line
			var position1 = { x: positionX, y: positionY1 };
			var position2 = { x: positionX, y: solidLineEndPosition };

			var points = [position1, position2];
			solidLine.props.points = JSON.stringify(points);
			solidLine.name = "solid_line";
			// solid line

			// dashed line +10
			position1 = { x: positionX + 10, y: positionY1 + 10 };
			position2 = { x: positionX + 10, y: solidLineEndPosition + 10 };
			points = [position1, position2];
			dashedLine.props.points = JSON.stringify(points);
			dashedLine.name = "dashed_line";
			// dashed line

			var x = positionX - 4;
			var y = positionY1 - 4;
			// solid circle Start
			solidCircleStart.props.setter.x = x;
			solidCircleStart.props.setter.y = y;
			solidCircleStart.name = "solid_circle_0";
			// solid circle Start

			// dashed circle Start
			dashedCircleStart.props.setter.x = x + 10;
			dashedCircleStart.props.setter.y = y + 10;
			dashedCircleStart.name = "dashed_circle_0";
			// dashed circle Start

			// solid circle End
			solidCircleEnd.props.setter.x = x;
			solidCircleEnd.props.setter.y = solidLineEndPosition;
			solidCircleEnd.name = "solid_circle_1";
			// solid circle End

			// dashed circle End
			dashedCircleEnd.props.setter.x = x + 10;
			dashedCircleEnd.props.setter.y = solidLineEndPosition + 10;
			dashedCircleEnd.name = "dashed_circle_1";
			// dashed circle End

			this._pageDataList.content_info.two_layer.push(solidLine);
			this._pageDataList.content_info.two_layer.push(dashedLine);
			this._pageDataList.content_info.two_layer.push(solidCircleStart);
			this._pageDataList.content_info.two_layer.push(solidCircleEnd);
			this._pageDataList.content_info.two_layer.push(dashedCircleStart);
			this._pageDataList.content_info.two_layer.push(dashedCircleEnd);
		}
	}]);

	return vertical_page;
}(basic_page);