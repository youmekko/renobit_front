class basic_page {
	constructor() {
		this.ExcelHeader = basic_page.ExcelHeader;
		this.DefaultRowVO = basic_page.DefaultRowVO; // ROW 객체
		this.EquipmentTypeToComponent = basic_page.EquipmentTypeToComponent;

		this._pageDataList = null;
		this._headerPortDataList = null;
		this._excelRowDataList = null;
		this._sourceCompositionData = null;
		this.templateRect = {};  // tmp_wrap_position 기록하는 리터럴
		this.tempArray = [];

		this._templateInstanceList = [];
		this.baiscLineList = {}; // 라인을 동적으로 그릴 때 사용
		this.tempArray = []; // 사용처?
		this.newInstance= null;
	}

	/*
	1. 템플릿 복사하기
	2. 헤더영역 생성하기
	3. row 내용 생성하기
	4. 페이지 크기 설정하기
	 */

	createPage(pageData, headerPortDataList, excelRowDataList, excelPortDataList) {
		// 1. data 받아오기
		//pageData는 복사할 page를 가져온다. vertical = vertical_page, horizontal = horizontal_page
		this._pageDataList = pageData;
		// sk pannel page에서 정리된 1,2단 부분 port 데이터를 받는다.
		this._headerPortDataList = headerPortDataList;
		// Map으로 정리된 NetworkState 정보
		this._excelRowDataList = excelRowDataList;
		this._excelPortDataList = excelPortDataList;
		window._excelPortDataList = excelPortDataList;
		//
		// this._sourceCompositionData = sourceCompositionData;

		this._attachLink();
		this._attachTemplate();
		this._getLineList();
		this._createRows();
	}

	//name에 해당하는 컴포넌트 정보 구하기
	_getComponentInstanceByName(name) {
		return this._pageDataList.content_info.two_layer.find((comInstanceInfo) => {
			return comInstanceInfo.name == name;
		})
	}

	_getComponentInstanceByLink(name) {
		return this._pageDataList.content_info.two_layer.find((comInstanceInfo) => {
			return comInstanceInfo.name.indexOf(name) != -1;
		})
	}

	_attachLink() { // horizontal, vertical 에서 각자 오버라이드

	}

	_attachTemplate() {
		this.getTemplateRectPosition();         // 1. 기준 위치가 되는 wrapper 가져오기
		this.deleteTmpTemplate();            //  3. 템플릿 영역으로 사용하는 객체만 삭제하기

		if (this.getTemplateInstanceList() === false) {
			alert("템플릿 정보가 존재하지 않습니다. 확인 후 다시 실행해주세요.");
			return;
		}

		this.tmpStampPositionReset();
	}

	getTemplateRectPosition() {
		let wrapper = this._getComponentInstanceByName("tmp_wrapper");
		// 2. 템플릿 영역 설정하기
		this.templateRect = {
			x: wrapper.props.setter.x,
			y: wrapper.props.setter.y,
			width: wrapper.props.setter.width,
			height: wrapper.props.setter.height
		}
	}

	deleteTmpTemplate() {
		//  3. 템플릿 영역으로 사용하는 객체만 삭제하기
		this._pageDataList.content_info.two_layer.forEach((item, index) => {
			if (item != null) {
				if (item.name == "tmp_wrapper") {
					this._pageDataList.content_info.two_layer.splice(index, 1);
				}
			}
		})
	}

	getTemplateInstanceList() {
		// 4. 템플릿 페이지에서 tmp가 있는 컴포넌트들만 return 받는다.
		// 복사할 도장을 만들기 위함
		this._templateInstanceList = this._pageDataList.content_info.two_layer.filter((instance) => {
			return instance.name.indexOf("tmp_") != -1;
		})

		// 복사할 컴포넌트가 없으면 종료
		if (this._templateInstanceList.length <= 0) return false;
	}

	tmpStampPositionReset() {
		// 4. tmp_wrapper의 시작위치를 기준으로 0,0을 기준으로 원점 처리
		var byX = this.templateRect.x;
		var byY = this.templateRect.y;

		this._templateInstanceList.forEach((instance) => {
			instance.props.setter.x = instance.props.setter.x - byX;
			instance.props.setter.y = instance.props.setter.y - byY;

			if (instance.componentName == "SVGLineComponent") {
				var points = eval(instance.props.points);

				points.forEach((point) => {
					point.x = point.x - byX;
					point.y = point.y - byY;
				})

				instance.props.points = JSON.stringify(points);
			}
		})

		// 5. 템플릿 위치를 0,0으로 설정하기
		this.templateRect.x = 0;
		this.templateRect.y = 0;
	}

	_getLineList() {
		this._pageDataList.content_info.two_layer.forEach((item, index) => {
			if (item.name == "basic_solid_line") {
				this.baiscLineList.solidLine = item;
			}

			if (item.name == "basic_dashed_line") {
				this.baiscLineList.dashedLine = item;
			}
		});
	}

	/*
	    A단의 excel Data를 가져와서 페이지에 뿌려준다.
	 */
	_createRows() {
		// A단에 있는 정보를 불러와서 인스턴스로 만들고, 화면에 배치해준다.
		let rowInfo = this._excelRowDataList.get("A"); // excel에서 a단 불러오기

		// 해당 networkState 찍힐 위치를 받아온다.
		let position = this._getStartPosition(rowInfo);

		// 복사 시작
		let colIndex = 0;
		let instance = {};
		rowInfo.forEach((networkState, index) => {
			instance = this._startStampSeal(networkState, position); //NetworkState 와 계산된 position만 넘겨준다.
			colIndex += 1;  // 마지막 내용은 증가하지 않게
			position = this._getNetworkComponentNextPosition(rowInfo, colIndex, position, index); // 다음 NSC position위치를 얻어온다.
		});
	}

	_startStampSeal(networkState, position) {
		// let networkState = $.extend({}, basic_page.DefaultRowVO, temp); // network
		// networkState.name = networkState.IP.replace(/\./g, "_"); // 치환

		this._templateInstanceList.forEach((tmpInstance) => { // 템플릿 요소를 하나씩 추가한다.
			let newInstanceInfo = $.extend(true, {}, tmpInstance);

			if (tmpInstance.componentName == "VerticalPortComponent" || tmpInstance.componentName == "HorizontalPortComponent") {

				if (!networkState.hasOwnProperty("ports")) {
					return;
				}

				if (networkState.ports.a === null) { // 해당 networkState에 port a 가 없으면 _startStampSeal 종료
					if (newInstanceInfo.name.indexOf("tmp_port_a") != -1) {
						return;
					}

				} else {
					if (newInstanceInfo.name === "tmp_port_a") {
						newInstanceInfo.props.portInfo.fromText = networkState.ports.a.from;
						newInstanceInfo.props.portInfo.toText = networkState.ports.a.to;
					}
				}

				if (networkState.ports.b === null) { // 해당 networkState에 port a 가 없으면 _startStampSeal 종료
					if (newInstanceInfo.name.indexOf("tmp_port_b") != -1) {
						return;
					}

				} else {
					if (newInstanceInfo.name === "tmp_port_b") {
						newInstanceInfo.props.portInfo.fromText = networkState.ports.b.from;
						newInstanceInfo.props.portInfo.toText = networkState.ports.b.to;
					}
				}

			}

			if(newInstanceInfo.componentName == "NetworkStateComponent") {
				// this._changeNetworkSymbolInfo(newInstanceInfo, networkState);

				let equipmentType = networkState["장비종류"];
				let comVO = basic_page.EquipmentTypeToComponent[equipmentType];

				console.log("comVOcomVOcomVO", comVO);
				//
				// // Backbone

				newInstanceInfo.props.setter.info.name = comVO.name;
				newInstanceInfo.props.setter.info.path = comVO.path;
				// this.tempArray.push(newInstance.name);
				// // alias + ip 출력
				newInstanceInfo.props.network.manufacturer = networkState["제조사"];
				// newInstanceInfo.props.network.company_label = networkState["제조사"];
				newInstanceInfo.props.network.ip = networkState["IP"];
				newInstanceInfo.props.network.hostName = networkState["장비명"];
				// //network 속성 설정
				newInstanceInfo.props.network.row = networkState["ROW"]+""; // int로 되는 경우가 있어서 뒤에 +""
				if(networkState["serial_number"]) {
					newInstanceInfo.props.network.serial_number = networkState["serial_number"];
				}

				if(networkState["model"]) {
					newInstanceInfo.props.network.serial_number = networkState["model"];
				}

				if(networkState["cid"]) {
					newInstanceInfo.props.network.serial_number = networkState["cid"];
				}
				//
				// Network State 마다 적정 크기가 달라서 info 에 width, height가 있을 경우에만 사이즈를 변경해준다.
				if (comVO.hasOwnProperty("width")) {
					newInstanceInfo.props.setter.width = comVO.width;
				}

				if (comVO.hasOwnProperty("height")) {
					newInstanceInfo.props.setter.height = comVO.height;
				}
			}

			newInstanceInfo.props.setter.x = position.x + tmpInstance.props.setter.x;
			newInstanceInfo.props.setter.y = position.y + tmpInstance.props.setter.y;

			// newInstanceInfo.name = ; // 치환
			newInstanceInfo.name = newInstanceInfo.name.replace("tmp", networkState.IP.replace(/\./g, "_"));
			newInstanceInfo.name = newInstanceInfo.name.replace("_symbol", ""); // _symbol 제거 추후 변경

			// newInstanceInfo
			if(newInstanceInfo.componentName == "NetworkStateComponent") {
				this.tempArray.push(newInstanceInfo);
			}

			this._pageDataList.content_info.two_layer.push(newInstanceInfo);
		});
	}


	drawLine() {
		// OverRide
	}

	_getStartPosition() {
		// OverRide
	}

	_getNetworkComponentNextPosition() {
		// OverRide
	}
}

basic_page.ExcelHeader = {
	EQUIPMENT_NAME: "장비명",
	IP: "IP",
	EQUIPMENT_TYPE: "장비종류",
	MANUFACTURER: "제조사",
	MODEL_NAME: "모델명",
	SN: "S/N",
	ROW: "ROW",
	CID: "CID"
}

basic_page.DefaultRowVO = {
	"company_label": "",
	"hostName": "",
	"ip": "",
	"manufacturer": "",
	"model": "",
	"row": "",
	"serial_number": "",
	"cid": "",
}

/*
"장비명": "",
	"IP": "",
	"장비종류": "",
	"제조사": "",
	"모델명": "",
	"S/N": "",
	"ROW": "",
	"CID": ""
 */
basic_page.EquipmentTypeToComponent = {
	"Backbone": {
		"name": "network009",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat009/res/",
		"width": 52,
		"height": 70
	},
	"Firewall": {
		"name": "firewall001",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/FirewallFlat001/res/"
	},
	"Switch": {
		"name": "network005",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat005/res/",
		"width": 76,
		"height": 38
	},
	"Router": {
		"name": "router001",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/RouterFlat001/res/",
		"width": 72,
		"height": 46
	},
	"Server": {
		"name": "server001",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/ServerFlat001/res/"
	},
	"Switch01": {
		"name": "Switch01",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat001/res/",
		"width": 50,
		"height": 50
	},
	"Switch02": {
		"name": "Switch02",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat002/res/",
		"width": 72,
		"height": 50
	},
	"Switch03": {
		"name": "Switch03",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat003/res/",
		"width": 76,
		"height": 38
	},
	"Switch04": {
		"name": "Switch04",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat004/res/",
		"width": 50,
		"height": 50
	},
	"Switch05": {
		"name": "Switch05",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat006/res/",
		"width": 76,
		"height": 38
	},
	"Switch06": {
		"name": "Switch06",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat007/res/",
		"width": 76,
		"height": 38
	},
	"Switch07": {
		"name": "Switch07",
		"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat008/res/",
		"width": 76,
		"height": 38
	}
};

basic_page.NetworkStateMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10053,
			"x": 51,
			"y": 102,
			"visible": true,
			"opacity": 1,
			"width": 76,
			"height": 36,
			"rotation": 0,
			"selectItem": "",
			"severity": "normal",
			"isSaved": true,
			"info": {
				"name": "NetworkFlat005",
				"path": "/custom/packs/sk_pack/components/NetworkStateComponent/state/NetworkFlat005/res/"
			}
		},
		"label": {
			"label_using": "Y",
			"label_text": "<div class='label-box'>hostname<br />ip</div>",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": "",
			"change": "",
			"change2": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"network": {
			"ip": "ip",
			"hostName": "hostname",
			"manufacturer": "",
			"serial_number": "",
			"model": "",
			"row": "",
			"cid": "",
			"company_label": "",
			"company": ""
		},
		"componentName": "NetworkStateComponent",
		"name": "network_symbol"
	},
	"id": "4afdd952-f5e0-4966-8b1c-4aaa8df522d0",
	"name": "network_symbol",
	"layerName": "twoLayer",
	"componentName": "NetworkStateComponent",
	"version": "1.0.0",
	"category": "2D"
}

basic_page.NetworkLinkMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10054,
			"x": 446,
			"y": 334,
			"visible": true,
			"opacity": 1,
			"width": 50,
			"height": 50,
			"rotation": 0
		},
		"label": {
			"label_using": "Y",
			"label_text": "",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"network": {
			"from": {
				"ip": "",
				"hostName": "",
				"port": "",
				"target": ""
			},
			"to": {
				"ip": "",
				"hostName": "",
				"port": "",
				"target": ""
			}
		},
		"componentName": "NetworkLinkComponent",
		"name": "net_link",
	},
	"id": "d9e1f182-9f34-4bcc-896d-5194f8cd06cf",
	"name": "net_link",
	"layerName": "twoLayer",
	"componentName": "NetworkLinkComponent",
	"version": "1.5.0",
	"category": "2D"
}