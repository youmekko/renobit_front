"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var VerticalPageCreator = function (_PageCreator) {
	_inherits(VerticalPageCreator, _PageCreator);

	function VerticalPageCreator() {
		_classCallCheck(this, VerticalPageCreator);

		var _this = _possibleConstructorReturn(this, (VerticalPageCreator.__proto__ || Object.getPrototypeOf(VerticalPageCreator)).call(this));

		_this.startPositionX = 560;
		_this.startPositionY = 500;
		_this.height = 100;
		_this.width = 280;
		return _this;
	}

	_createClass(VerticalPageCreator, [{
		key: "createPage",
		value: function createPage(pageData, networkProxy) {
			_get(VerticalPageCreator.prototype.__proto__ || Object.getPrototypeOf(VerticalPageCreator.prototype), "createPage", this).call(this, pageData, networkProxy);
		}
	}, {
		key: "_createHeader",
		value: function _createHeader() {
			var _this2 = this;

			var layoutManager = new HeaderGroupExcelCreator();
			var comInstanceInfoMap = new Map();
			var rows = this._networkProxy._headerElementInfoMap.size;
			var columns = 0;
			this._networkProxy._headerElementInfoMap.forEach(function (value, key) {
				columns = Math.max(columns, value.length);
			});

			this._networkProxy._headerElementInfoMap.forEach(function (elementInfoList, row) {
				try {
					var comInstanceVO1 = layoutManager.addState(parseInt(row) - 1, 0, elementInfoList[0], elementInfoList[0].equipmentType);
					comInstanceVO1.props.label.label_position = "LC";
					_this2.get_label_size(comInstanceVO1);
					comInstanceInfoMap.set(elementInfoList[0].ip, comInstanceVO1);

					var comInstanceVO2 = layoutManager.addState(parseInt(row) - 1, 1, elementInfoList[1], elementInfoList[1].equipmentType);
					comInstanceVO2.props.label.label_position = "RC";
					_this2.get_label_size(comInstanceVO2);
					comInstanceInfoMap.set(elementInfoList[1].ip, comInstanceVO2);
				} catch (e) {
					console.log("header 구현부에서 에러발생", e);
				}
			});

			var temp = [];
			var temp2 = [];
			comInstanceInfoMap.forEach(function (elementInfo, row) {
				temp2.push(elementInfo);
				_this2._pageDataList.content_info.two_layer.push(elementInfo);
			});

			this._networkProxy._headerPortInfoList.forEach(function (item, index) {
				item.from.target = item.from.ip.replace(/\./g, "_"); // 치환
				item.to.target = item.to.ip.replace(/\./g, "_"); // 치환

				var comInstanceVO = layoutManager.addLine(item.from, item.to, index, item.gap);
				_this2._pageDataList.content_info.two_layer.push(comInstanceVO);

				temp.push(comInstanceVO);
				// addLine
			});

			this._networkLinkSetPosition(temp2, this._networkProxy._bodyElementInfoList.length);

			this._createHeaderToBodyPort();
		}
	}, {
		key: "get_label_size",
		value: function get_label_size(instance) {
			var hostNameLength = instance.props.network.hostName.toString().length;
			var ipValueLength = instance.props.network.ip.toString().length;

			if (hostNameLength > ipValueLength) {
				hostNameLength = hostNameLength * 7;
			} else {
				hostNameLength = ipValueLength * 7;
			}

			if (instance.props.label.label_position == "LT" || instance.props.label.label_position == "LC" || instance.props.label.label_position == "LB") {
				hostNameLength *= 1;
				instance.props.label.label_offset_x = -hostNameLength;
			} else if (instance.props.label.label_position == "RT" || instance.props.label.label_position == "RC" || instance.props.label.label_position == "RB") {
				instance.props.label.label_offset_x = hostNameLength;
			}
		}
	}, {
		key: "_createHeaderToBodyPort",
		value: function _createHeaderToBodyPort() {
			var verticalLineLeft = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);
			var verticalLineRight = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);

			verticalLineLeft.name = "header_to_body_1";
			verticalLineRight.name = "header_to_body_2";
			verticalLineLeft.props.setter.width = 20;
			verticalLineLeft.props.setter.height = 40;
			verticalLineRight.props.setter.width = 30;
			verticalLineRight.props.setter.height = 50;

			verticalLineLeft.props.setter.x = 932;
			verticalLineLeft.props.setter.y = 424;
			verticalLineRight.props.setter.x = 1310;
			verticalLineRight.props.setter.y = 424;
			verticalLineRight.props.portInfo.lineStyle = "dashed";

			this._pageDataList.content_info.two_layer.push(verticalLineLeft);
			this._pageDataList.content_info.two_layer.push(verticalLineRight);

			var horizontalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo);
			var horizontalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.HorizontalBasicPortMetaInfo);

			horizontalSolidPort.name = "header_to_body_3";
			horizontalDashedPort.name = "header_to_body_4";
			horizontalSolidPort.props.setter.x = 400;
			horizontalSolidPort.props.setter.y = 450;
			horizontalDashedPort.props.setter.x = 400;
			horizontalDashedPort.props.setter.y = 460;
			horizontalDashedPort.props.portInfo.lineStyle = "dashed";

			this._pageDataList.content_info.two_layer.push(horizontalSolidPort);
			this._pageDataList.content_info.two_layer.push(horizontalDashedPort);
		}
	}, {
		key: "_createBody",
		value: function _createBody() {
			var bodyLength = this._networkProxy._bodyElementInfoList.length;
			if (bodyLength < 26) {
				this._createBody25();
			} else {
				this._createBody42();
			}
		}
	}, {
		key: "_createBody25",
		value: function _createBody25() {
			var _this3 = this;

			// 바디 부분 구현
			var layoutManager = new HeaderGroupExcelCreator();
			var comInstanceInfoMap = new Map();
			var columns = 5;

			var column = 0;
			var row = 0;

			this._networkProxy._bodyElementInfoList.forEach(function (elementInfo) {
				var comInstanceVO = layoutManager.addState("A", column, elementInfo, elementInfo.equipmentType);
				_this3._setBodyElement(comInstanceVO, row, column);

				_this3._pageDataList.content_info.two_layer.push(comInstanceVO);
				if (comInstanceInfoMap.has(column) == false) {
					comInstanceInfoMap.set(column, []);
				}

				var tempArr = comInstanceInfoMap.get(column);
				tempArr.push(1);

				column++;
				if (column >= columns) {
					row += 1;
					column = 0;
				}
			});

			comInstanceInfoMap.forEach(function (valueArr, index) {
				var heightVal = _this3.height * valueArr.length;
				_this3._setBodyElementVerticalPort(index, heightVal);
			});
		}
	}, {
		key: "_setBodyElementVerticalPort",
		value: function _setBodyElementVerticalPort(index, height) {
			var verticalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);
			var verticalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);

			verticalSolidPort.name = "body_solid_line_" + index;
			verticalSolidPort.props.setter.width = 20;
			verticalSolidPort.props.setter.height = height;
			verticalSolidPort.props.setter.x = this.startPositionX - 136 + index * this.width;
			verticalSolidPort.props.setter.y = this.startPositionY - 44;

			verticalDashedPort.name = "body_dashed_line_" + index;
			verticalDashedPort.props.setter.width = 20;
			verticalDashedPort.props.setter.height = height;
			verticalDashedPort.props.setter.x = this.startPositionX - 126 + index * this.width;
			verticalDashedPort.props.setter.y = this.startPositionY - 34;
			verticalDashedPort.props.portInfo.lineStyle = "dashed";
			if (this._networkProxy._bodyElementInfoList.length > 25) {
				verticalSolidPort.props.setter.y = this.startPositionY - 30;
				verticalDashedPort.props.setter.y = this.startPositionY - 24;
			}

			this._pageDataList.content_info.two_layer.push(verticalSolidPort);
			this._pageDataList.content_info.two_layer.push(verticalDashedPort);
		}

		/*
        NetworkState, HorizontalPort 셋팅
   */

	}, {
		key: "_setBodyElement",
		value: function _setBodyElement(comInstanceVO, row, column) {
			var _this4 = this;

			var comInstancePorts = this._networkProxy._bodyPortInfoList.filter(function (portInfo) {
				return portInfo.to.ip.indexOf(comInstanceVO.props.network.ip) != -1;
			});

			comInstanceVO.props.setter.x = this.startPositionX + this.width * column;
			comInstanceVO.props.setter.y = this.startPositionY + this.height * row;

			if (comInstancePorts) {
				comInstancePorts.forEach(function (portInfo) {
					if (portInfo.type == "A") {
						var horizontalSolidPort = $.extend(true, {}, PageCreator.HorizontalPortMetaInfo);
						horizontalSolidPort.props.setter.width = 120;
						horizontalSolidPort.props.setter.x = _this4.startPositionX + _this4.width * column - 130;
						horizontalSolidPort.props.setter.y = _this4.startPositionY + _this4.height * row;

						horizontalSolidPort.props.portInfo.textLeftMargin = "20px";
						horizontalSolidPort.props.portInfo.textRightMargin = "5px";

						horizontalSolidPort.props.portInfo.fromHost = portInfo.from.hostName;
						horizontalSolidPort.props.portInfo.fromIP = portInfo.from.ip;
						horizontalSolidPort.props.portInfo.fromText = portInfo.from.port;

						horizontalSolidPort.props.portInfo.toHost = portInfo.to.hostName;
						horizontalSolidPort.props.portInfo.toIP = portInfo.to.ip;
						horizontalSolidPort.props.portInfo.toText = portInfo.to.port;
						horizontalSolidPort.name = portInfo.to.ip + "_port_a";

						_this4._pageDataList.content_info.two_layer.push(horizontalSolidPort);
					} else {
						var horizontalDashedPort = $.extend(true, {}, PageCreator.HorizontalPortMetaInfo);
						horizontalDashedPort.props.setter.width = 110;

						horizontalDashedPort.props.setter.x = _this4.startPositionX + _this4.width * column - 120;
						horizontalDashedPort.props.setter.y = _this4.startPositionY + _this4.height * row + 10;

						horizontalDashedPort.props.portInfo.textLeftMargin = "10px";
						horizontalDashedPort.props.portInfo.textRightMargin = "5px";

						horizontalDashedPort.props.portInfo.lineStyle = "dashed";
						horizontalDashedPort.props.portInfo.position = "bottom";

						horizontalDashedPort.props.portInfo.fromHost = portInfo.from.hostName;
						horizontalDashedPort.props.portInfo.fromIP = portInfo.from.ip;
						horizontalDashedPort.props.portInfo.fromText = portInfo.from.port;

						horizontalDashedPort.props.portInfo.toHost = portInfo.to.hostName;
						horizontalDashedPort.props.portInfo.toIP = portInfo.to.ip;
						horizontalDashedPort.props.portInfo.toText = portInfo.to.port;
						horizontalDashedPort.name = portInfo.to.ip + "_port_b";

						_this4._pageDataList.content_info.two_layer.push(horizontalDashedPort);
					}
				});
			}
		}
	}, {
		key: "_createBody42",
		value: function _createBody42() {
			var _this5 = this;

			this.width = 230;
			this.height = 72;
			this.startPositionX = 550;
			this.startPositionY = 476;

			var lines = this._pageDataList.content_info.two_layer.filter(function (item) {
				return item.name.indexOf("header_to_body_") != -1;
			});

			lines.forEach(function (item) {
				item.props.setter.y = item.props.setter.y - 10;
			});

			var layoutManager = new HeaderGroupExcelCreator();
			var comInstanceInfoMap = new Map();
			var columns = 6;

			var column = 0;
			var row = 0;

			this._networkProxy._bodyElementInfoList.forEach(function (elementInfo) {
				var comInstanceVO = layoutManager.addState("A", column, elementInfo, elementInfo.equipmentType);
				_this5._setBodyElement(comInstanceVO, row, column);

				_this5._pageDataList.content_info.two_layer.push(comInstanceVO);
				if (comInstanceInfoMap.has(column) == false) {
					comInstanceInfoMap.set(column, []);
				}

				var tempArr = comInstanceInfoMap.get(column);
				tempArr.push(1);

				column++;
				if (column >= columns) {
					row += 1;
					column = 0;
				}
			});

			comInstanceInfoMap.forEach(function (valueArr, index) {
				var heightVal = _this5.height * valueArr.length;
				_this5._setBodyElementVerticalPort(index, heightVal);
			});
		}
	}, {
		key: "_setBodyElementVerticalPort42",
		value: function _setBodyElementVerticalPort42(row, column) {
			var width = 230;
			var height = 72;
			var startPositionX = 550;
			var startPositionY = 476;

			var verticalSolidPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);
			var verticalDashedPort = $.extend(true, {}, HeaderGroupExcelCreator.VerticalBasicPortMetaInfo);

			verticalSolidPort.name = "body_solid_line_" + column;
			verticalSolidPort.props.setter.width = 20;
			verticalSolidPort.props.setter.height = height * row;
			verticalSolidPort.props.setter.x = startPositionX - 130 - 6 + column * width;
			verticalSolidPort.props.setter.y = startPositionY - 30;

			verticalDashedPort.name = "body_dashed_line_" + column;
			verticalDashedPort.props.setter.width = 20;
			verticalDashedPort.props.setter.height = height * row;
			verticalDashedPort.props.setter.x = startPositionX - 120 - 6 + column * width;
			verticalDashedPort.props.setter.y = startPositionY - 20;
			verticalDashedPort.props.portInfo.lineStyle = "dashed";

			this._pageDataList.content_info.two_layer.push(verticalSolidPort);
			this._pageDataList.content_info.two_layer.push(verticalDashedPort);
		}
	}, {
		key: "_createBad",
		value: function _createBad() {
			var _this6 = this;

			var layoutManager = new HeaderGroupExcelCreator();
			var badLeft = 1800;
			var badTop = 170;

			this._networkProxy._badElementInfoList.forEach(function (elementInfo) {
				var comInstanceVO = layoutManager.addState("BAD", -1, elementInfo, elementInfo.equipmentType);
				badTop = _this6._badComponentNextPosition(badTop);
				comInstanceVO.props.setter.x = badLeft;
				comInstanceVO.props.setter.y = badTop;
				comInstanceVO.name = "bad_" + comInstanceVO.name;
				_this6._pageDataList.content_info.two_layer.push(comInstanceVO);
			});

			this._networkProxy._badPortInfoList.forEach(function (portInfo, index) {
				var portVO = layoutManager.addElementPort("BAD", portInfo, index);
				badTop = _this6._badComponentNextPosition(badTop);
				portVO.props.setter.x = badLeft;
				portVO.props.setter.y = badTop;
				_this6._pageDataList.content_info.two_layer.push(portVO);
			});
		}
	}, {
		key: "_badComponentNextPosition",
		value: function _badComponentNextPosition(badTop) {
			var emptySpace = 20;
			var limitSize = 1000;

			badTop += emptySpace;
			if (badTop > limitSize) {
				badTop = limitSize;
			}

			return badTop;
		}
	}, {
		key: "_networkLinkSetPosition",
		value: function _networkLinkSetPosition(instanceList, accessDataListLength) {

			if (instanceList.length == 1) {
				// 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
				if (accessDataListLength < 6) {
					instanceList[0].props.setter.x = 886;
					instanceList[0].props.setter.y = 432;
				} else if (accessDataListLength < 31) {
					instanceList[0].props.setter.x = 892;
					instanceList[0].props.setter.y = 312;
				} else {
					instanceList[0].props.setter.x = 876;
					instanceList[0].props.setter.y = 464;
				}
			}

			if (instanceList.length == 2) {
				// 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
				instanceList[0].props.setter.x = 920;
				instanceList[0].props.setter.y = 344;

				instanceList[1].props.setter.x = 1302;
				instanceList[1].props.setter.y = 344;
			}

			if (instanceList.length == 3) {
				if (accessDataListLength < 6) {
					instanceList[0].props.setter.x = 886;
					instanceList[0].props.setter.y = 260;

					instanceList[1].props.setter.x = 1270;
					instanceList[1].props.setter.y = 260;

					instanceList[2].props.setter.x = 886;
					instanceList[2].props.setter.y = 430;
				} else if (accessDataListLength < 31) {
					instanceList[0].props.setter.x = 892;
					instanceList[0].props.setter.y = 145;

					instanceList[1].props.setter.x = 1276;
					instanceList[1].props.setter.y = 145;

					instanceList[2].props.setter.x = 892;
					instanceList[2].props.setter.y = 315;
				} else {
					instanceList[0].props.setter.x = 876;
					instanceList[0].props.setter.y = 464;

					instanceList[1].props.setter.x = 1260;
					instanceList[1].props.setter.y = 464;

					instanceList[2].props.setter.x = 876;
					instanceList[2].props.setter.y = 464;
				}
			}

			if (instanceList.length == 4) {
				instanceList[0].props.setter.x = 920;
				instanceList[0].props.setter.y = 170;

				instanceList[1].props.setter.x = 1302;
				instanceList[1].props.setter.y = 170;

				instanceList[2].props.setter.x = 920;
				instanceList[2].props.setter.y = 344;

				instanceList[3].props.setter.x = 1302;
				instanceList[3].props.setter.y = 344;
			}
		}
	}]);

	return VerticalPageCreator;
}(PageCreator);