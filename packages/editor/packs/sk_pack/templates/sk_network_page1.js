
class sk_network_page1{


      constructor(){
            this._pageData = null;
            this.configInfo = null;
            this.excelRowData = null;
            this.symbolPoint = {
                  x:0,
                  y:0
            }

            this.ExcelHeader = this.constructor.ExcelHeader;
            this.TextComponentInfo =this.constructor.TextComponentInfo;
            this.EquipmentTypeToComponent = this.constructor.EquipmentTypeToComponent;

      }


      /*
      시작 페이지 정보 설정 후 시작.
       */
      createPage(pageData, configInfo, excelRowData){
            this._pageData = pageData;
            this.configInfo = configInfo;

            // 헤더 정보 생성하기.
            this.excelHeaderData = excelRowData.get("0");
            // row정보에서 헤더 정보 삭제
            excelRowData.delete("0");

            // row정보만 신규로 설정하기
            this.excelRowData =excelRowData;



            // 헤더영역 생성하기.
            this.createHeaderComponent();

            // ROW 내용 생성하기
            this.createRows();
      }



      /*
      헤더 정보 생성하기.


       */
      createHeaderComponent(){
            let headerInstanceName = "row_0";
            let headerConfigs = this.configInfo[headerInstanceName];
            if(headerConfigs==null)
                  return;

            let rowInfoList = this.excelHeaderData;
            if(headerConfigs==null)
                  return;



            console.log("@@ rowInfoList ", rowInfoList);

            // header 부분 처리
            /*
                  1.  Excel 장비 종류 정보에 설정되어 있는 컴포넌트의 인스턴스 초기값 구하기
                  2. 컴포넌트 인스턴스 생성
                  3. 컴포넌트 초기화 처리
                  4. 2D 레이어에 컴포넌트 추가
                  5. IP/EQUIPMENT_ALIAS 정보를 담는 텍스트 컴포넌트 생성.

             */
            rowInfoList.forEach((rowInfo, index)=>{
                  ///////////////////
                  //1.  Excel 장비 종류 정보에 설정되어 있는 컴포넌트의 인스턴스 초기값 구하기
                  let equipmentType = rowInfo[this.ExcelHeader.EQUIPMENT_TYPE];
                  // equipmentType에 해당하는 컴포넌트 정보 구하기
                  var comVO= this.EquipmentTypeToComponent[equipmentType];

                  //2. 컴포넌트 인스턴스 생성
                  var componentInstance = null;
                  if(comVO==null)
                        componentInstance = wemb.componentLibraryManager.createComponentInstance("FrameComponent");
                  else
                        componentInstance = wemb.componentLibraryManager.createComponentInstance(comVO.name);


                  //3. 컴포넌트 초기화 처리
                  var config = headerConfigs[index];
                  var symbolName =`${headerInstanceName}_com_${index+1}`;
                  // 컴포넌트 메타 프로퍼티 생성
                  let tempInitMetaProperties = $.extend(true, {
                        name:symbolName,
                        id:"__", //아이디 값은  command에서 다시 설정됨.
                        layerName:"twoLayer",
                        props:{
                              setter:{
                                    x:config.x,
                                    y:config.y
                              }
                        }
                  },comVO.initProperties);
                  // 컴포넌트에 프로퍼티 적용하기
                  componentInstance.create(tempInitMetaProperties);
                  componentInstance._onImmediateUpdateDisplay();


                  //4. 2D 레이어에 컴포넌트 추가
                  this._pageData.content_info.two_layer.push(componentInstance.serializeMetaProperties());
                  ///////////////////






                  ///////////////////
                  //5. IP/EQUIPMENT_ALIAS 정보를 담는 텍스트 컴포넌트 생성.
                  var y = tempInitMetaProperties.props.setter.y-this.TextComponentInfo.HEIGHT;
                  if(index==1){
                        y = tempInitMetaProperties.props.setter.y + tempInitMetaProperties.props.setter.height
                  }
                  var textInfo = {
                        x:config.x -Math.floor(this.TextComponentInfo.WIDTH/2)+(tempInitMetaProperties.props.setter.width/2),
                        y:y,
                        width:this.TextComponentInfo.WIDTH,
                        height:this.TextComponentInfo.HEIGHT,
                        text:rowInfo[this.ExcelHeader.EQUIPMENT_ALIAS]+"\n"+rowInfo[this.ExcelHeader.IP]
                  }
                  this._createTextComponent(symbolName+"_text", textInfo);
                  ///////////////////

            });
      }


      /*
      텍스트 생성하기
       */
      _createTextComponent(comName, textInfo){
            var componentInstance = wemb.componentLibraryManager.createComponentInstance("TextComponent");
            // 컴포넌트 메타 프로퍼티 생성


            let tempInitMetaProperties ={
                  name:comName,
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              x:textInfo.x,
                              y:textInfo.y,
                              width:textInfo.width,
                              height:textInfo.height,
                              text:textInfo.text
                        },
                        font: {
                              "text_align": "center",
                              font_size:10,
                              font_height:10,
                              font_weight:"bold"
                        }
                  }
            };

            // 컴포넌트 생성
            componentInstance.create(tempInitMetaProperties);
            componentInstance._onImmediateUpdateDisplay();
            this._pageData.content_info.two_layer.push(componentInstance.serializeMetaProperties())
      }





      async createRows(){
            this.excelRowData.forEach((rowInfoList,rowIndex)=>{
                  // rowIndex에 해당하는 config 정보 구하기.
                  let rowConfig = this.configInfo["row_"+rowIndex];
                  if(rowConfig==null)
                        return;

                  // main 부분 처리
                  var startPoint = {
                        x:rowConfig.x,
                        y:rowConfig.y
                  }

                  /*
                        rowInfo 개수만큼 for문 돌며 컴포넌트 인스턴스 생성하기
                        순서는 config 시작위치를 기준으로
                        1. 첫번째 라인 그리기(실선)
                        2. 두번째 라인 그리기(점선)
                        3. 심볼 그리기
                        4. 텍스트 그리기 (심볼 위로)
                        5. 다음 위치 설정하기
                   */
                  rowInfoList.forEach((rowInfo, index)=>{
                        let lineName = `row_${rowIndex}_svg_col_${index+1}`;
                        var LINE_GAB = 8;

                        //1. 첫번째 라인 그리기(실선)
                        this.drawSVGVLine(lineName+"_1", rowConfig.x-LINE_GAB,rowConfig.y-rowConfig.line_height, rowConfig.line_height);

                        //2. 두번째 라인 그리기(점선)
                        this.drawSVGVLine(lineName+"_2", rowConfig.x+LINE_GAB,rowConfig.y-rowConfig.line_height, rowConfig.line_height+15, true);


                        //3. 심볼 그리기
                        let symbolName = `row_${rowIndex}_com_${index+1}`;
                        let comMetaInfo = this.createSymbolComponent(symbolName,  rowInfo, rowConfig);

                        // 4. 텍스트 그리기 (심볼 위로)
                        // 50은 textcomponent height값.
                        var textInfo = {
                              x:rowConfig.x - Math.floor(this.TextComponentInfo.WIDTH/2),
                              y:rowConfig.y-(rowConfig.line_height+comMetaInfo.props.setter.height+this.TextComponentInfo.HEIGHT),
                              width:this.TextComponentInfo.WIDTH,
                              height:this.TextComponentInfo.HEIGHT,
                              text:rowInfo[this.ExcelHeader.EQUIPMENT_ALIAS]+"\n"+rowInfo[this.ExcelHeader.IP]
                        }

                        this._createTextComponent(symbolName+"_text", textInfo)







                        console.log("@@ rowInfo ",  rowInfo.ports);

                        var port1 = rowInfo.ports[0];
                        // from
                        var portTextInfo1 = {
                              x:rowConfig.x - LINE_GAB-50,
                              y:rowConfig.y-20,
                              width:50,
                              height:20,
                              text:port1.from
                        }

                        this._createTextComponent(symbolName+"_port1_from", portTextInfo1)
                        // to
                        var portTextInfo2 = {
                              x:rowConfig.x - LINE_GAB-50,
                              y:rowConfig.y-rowConfig.line_height,
                              width:50,
                              height:20,
                              text:port1.to
                        }

                        this._createTextComponent(symbolName+"_port1_to", portTextInfo2);



                        var port2 = rowInfo.ports[1];
                        // from
                        var portTextInf3 = {
                              x:rowConfig.x + LINE_GAB,
                              y:rowConfig.y-20,
                              width:50,
                              height:20,
                              text:port2.from
                        }

                        this._createTextComponent(symbolName+"_port2_from", portTextInf3)
                        // to
                        var portTextInfo4 = {
                              x:rowConfig.x +LINE_GAB,
                              y:rowConfig.y-rowConfig.line_height,
                              width:50,
                              height:20,
                              text:port2.to
                        }

                        this._createTextComponent(symbolName+"_port2_to", portTextInfo4)




                        //5. 다음 위치 설정하기
                        // line x 위치 설정
                        rowConfig.x+=rowConfig.step;


                  })

            })



      }






      drawSVGVLine(comName, startX, startY, strokeHeight, dotType=false){

            var startDotSvgComInstance = wemb.componentLibraryManager.createComponentInstance("SVGCircleComponent");
            var circleSize=8;
            // 컴포넌트 메타 프로퍼티 생성
            let startDotMetaProperty = {
                  name:comName+"_start",
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              width:circleSize,
                              height:circleSize,
                              x:startX-(circleSize/2),
                              y:startY-(circleSize/2)
                        },
                        style:{
                              "fill":"rgb(0,0,0)",
                              "stroke-width":0
                        }
                  }
            };

            // 컴포넌트 생성
            startDotSvgComInstance.create(startDotMetaProperty);
            this._pageData.content_info.two_layer.push(startDotSvgComInstance.serializeMetaProperties())



            // 실선 그리기
            var svgLineComponentInstance = wemb.componentLibraryManager.createComponentInstance("SVGLineComponent");
            // 컴포넌트 메타 프로퍼티 생성
            let tempInitMetaProperties2 = {
                  name:comName,
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              width:0,
                              height:strokeHeight,
                              x:startX,
                              y:startY,
                              points:[{
                                    x:startX,
                                    y:startY
                              },{
                                    x:startX,
                                    y:startY+strokeHeight
                              }]

                        },
                        style:{
                              "stroke-width":3,
                              "stroke":"rgb(0, 0, 0)",

                        }
                  }
            };
            if(dotType){
                  tempInitMetaProperties2.props.style["stroke-dasharray"]="5 5"
            }
            // 컴포넌트 생성
            svgLineComponentInstance.create(tempInitMetaProperties2);
            svgLineComponentInstance._onImmediateUpdateDisplay();
            var serializeProperties = svgLineComponentInstance.serializeMetaProperties();
            this._pageData.content_info.two_layer.push(serializeProperties)



            var endDotSvgComInstance = wemb.componentLibraryManager.createComponentInstance("SVGCircleComponent");
            // 컴포넌트 메타 프로퍼티 생성
            let endDotMetaProperty = {
                  name:comName+"_end",
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              width:circleSize,
                              height:circleSize,
                              x:startX-(circleSize/2),
                              y:startY+strokeHeight-(circleSize/2)
                        },
                        style:{
                              "fill":"rgb(0,0,0)",
                              "stroke-width":0
                        }
                  }
            };

            // 컴포넌트 생성
            endDotSvgComInstance.create(endDotMetaProperty);
            this._pageData.content_info.two_layer.push(endDotSvgComInstance.serializeMetaProperties())
      }

      createSymbolComponent(comName, rowInfo, rowConfig){

            // rowInfo에서 장비종류 값 구하기.
            let equipmentType = rowInfo[this.ExcelHeader.EQUIPMENT_TYPE];

            // componentVO 생성
            var comVO= this.EquipmentTypeToComponent[equipmentType];

            // componentInstance 생성
            var componentInstance = null;
            if(comVO==null)
                  componentInstance = wemb.componentLibraryManager.createComponentInstance("FrameComponent");
            else
                  componentInstance = wemb.componentLibraryManager.createComponentInstance(comVO.name);

            var severityList = ["normal", "critical", "major", "minor", "warning"];
            var selectedSeverity = severityList[Math.floor(Math.random()*5)];
            // 컴포넌트 메타 프로퍼티 생성
            let tempInitMetaProperties = $.extend(true, {
                  name:comName,
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              x:(rowConfig.x-(comVO.initProperties.props.setter.width/2)),
                              y:rowConfig.y-(rowConfig.line_height+comVO.initProperties.props.setter.height),
                              severity:selectedSeverity
                        }

                  }
            },comVO.initProperties);

            // 컴포넌트 생성
            componentInstance.create(tempInitMetaProperties);
            this._pageData.content_info.two_layer.push(componentInstance.serializeMetaProperties())

            return tempInitMetaProperties;
      }

}


sk_network_page1.ExcelHeader = {
      EQUIPMENT_NAME:"장비명",
      EQUIPMENT_ALIAS:"장비Alias",
      IP:"IP",
      EQUIPMENT_TYPE:"장비종류",
      MANUFACTURER:"제조사",
      MODEL_NAME:"모델명",
      ROW:"ROW"

}



sk_network_page1.EquipmentTypeToComponent = {
      "백본 스위치":{
            "name": "BasicStateClipComponent",
            "initProperties": {
                  "props": {
                        "setter": {
                              "width":66,
                              "height":90,
                              "info": {
                                    "name": "network007",
                                    "path": "/client/packs/2d_pack/components/symbol/network_flat_theme/NetworkFlat007/res/"
                              },
                              "isSaved":true
                        }
                  }
            }
      },
      "스위치":{
            "name": "BasicStateClipComponent",
            "initProperties": {
                  "props": {
                        "setter": {
                              "width":100,
                              "height":50,
                              "info": {
                                    "name": "network005",
                                    "path": "/client/packs/2d_pack/components/symbol/network_flat_theme/NetworkFlat005/res/"
                              },
                              "isSaved":true
                        }
                  }
            }
      }
}


sk_network_page1.TextComponentInfo = {
      WIDTH:140,
      HEIGHT:50
}

