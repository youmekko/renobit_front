"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var horizontal_page = function (_basic_page) {
	_inherits(horizontal_page, _basic_page);

	function horizontal_page() {
		_classCallCheck(this, horizontal_page);

		return _possibleConstructorReturn(this, (horizontal_page.__proto__ || Object.getPrototypeOf(horizontal_page)).call(this));
	}

	_createClass(horizontal_page, [{
		key: "createPage",
		value: function createPage(pageData, headerPortDataList, excelRowDataList, excelPortDataList) {
			_get(horizontal_page.prototype.__proto__ || Object.getPrototypeOf(horizontal_page.prototype), "createPage", this).call(this, pageData, headerPortDataList, excelRowDataList, excelPortDataList);
		}

		//@Override

	}, {
		key: "_attachLink",
		value: function _attachLink() {
			var linkDataList = [];
			var instanceList = [];

			var firstLinkDataList = this._excelRowDataList.get("1"); // 1단 불러옴
			var secondLinkDataList = this._excelRowDataList.get("2"); // 2단 불러옴
			var accessDataList = this._excelRowDataList.get("A"); // a단 불러옴

			var positionLeft = 0;
			var positionTop = 0;
			var metaInfo = null;

			linkDataList = linkDataList.concat(firstLinkDataList); // 1단 추가
			if (secondLinkDataList) {
				// 2단이 있으면
				linkDataList = linkDataList.concat(secondLinkDataList); // 2단 추가
			}

			// console.log("_pageDataList%$%", firstLinkDataList);
			// console.log("$%$%", secondLinkDataList);
			// console.log("$%$%", accessDataList);
			// console.log("$%$%", this.DefaultRowVO);
			// console.log("linkDataList", linkDataList);

			if (this._headerPortDataList) {
				this._headerPortDataList.forEach(function (networkState, index) {
					var newInstanceInfo = $.extend(true, {}, basic_page.NetworkStateMetaInfo); // link Instance
					newInstanceInfo.props.name = "network_link_" + index;
					// let networkVO =  $.extend({}, this.DefaultRowVO); // network value
					// networkVO.
					var equipmentType = networkState["장비종류"];
					var comVO = basic_page.EquipmentTypeToComponent[equipmentType];
					// Backbone
					newInstanceInfo.name = networkState.IP.replace(/\./g, "_"); // 치환
					newInstanceInfo.props.setter.info.name = comVO.name;
					newInstanceInfo.props.setter.info.path = comVO.path;
					// alias + ip 출력
					newInstanceInfo.props.network.manufacturer = networkState["제조사"];
					// newInstanceInfo.props.network.company_label = networkState["제조사"];
					newInstanceInfo.props.network.ip = networkState["IP"];
					newInstanceInfo.props.network.hostName = networkState["장비명"];
					//network 속성 설정
					newInstanceInfo.props.network.row = networkState["ROW"] + ""; // int로 되는 경우가 있어서 뒤에 +""

					if (networkState["serial_number"]) {
						newInstanceInfo.props.network.serial_number = networkState["serial_number"];
					}

					if (networkState["model"]) {
						newInstanceInfo.props.network.model = networkState["model"];
					}

					if (networkState["cid"]) {
						newInstanceInfo.props.network.cid = networkState["cid"];
					}

					// Network State 마다 적정 크기가 달라서 info 에 width, height가 있을 경우에만 사이즈를 변경해준다.
					if (comVO.hasOwnProperty("width")) {
						newInstanceInfo.props.setter.width = comVO.width;
					}

					if (comVO.hasOwnProperty("height")) {
						newInstanceInfo.props.setter.height = comVO.height;
					}

					instanceList.push(newInstanceInfo);
					console.log("newInstanceInfonewInstanceInfo", newInstanceInfo);
					// this._pageDataList.content_info.two_layer.push(newInstanceInfo);
				});
			}

			this._networkLinkSetPosition(instanceList, accessDataList.length);
			this._networkLinkPortSetting();
		}

		// 포지션은 1,2

	}, {
		key: "_networkLinkSetPosition",
		value: function _networkLinkSetPosition(instanceList, accessDataListLength) {
			var _this2 = this;

			if (instanceList.length == 1) {
				// 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
				if (accessDataListLength <= 5) {
					instanceList[0].props.setter.x = 350;
					instanceList[0].props.setter.y = 380;
				} else if (accessDataListLength <= 10) {
					instanceList[0].props.setter.x = 350;
					instanceList[0].props.setter.y = 390;
				} else if (accessDataListLength <= 15) {
					instanceList[0].props.setter.x = 350;
					instanceList[0].props.setter.y = 380;
				} else {
					instanceList[0].props.setter.x = 350;
					instanceList[0].props.setter.y = 380;
				}
			}

			if (instanceList.length == 2) {
				// 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
				if (accessDataListLength <= 5) {
					instanceList[0].props.setter.x = 350;
					instanceList[0].props.setter.y = 380;

					instanceList[1].props.setter.x = 350;
					instanceList[1].props.setter.y = 628;
				} else if (accessDataListLength <= 10) {
					instanceList[0].props.setter.x = 350;
					instanceList[0].props.setter.y = 390;

					instanceList[1].props.setter.x = 350;
					instanceList[1].props.setter.y = 638;
				} else if (accessDataListLength <= 15) {
					instanceList[0].props.setter.x = 350;
					instanceList[0].props.setter.y = 380;

					instanceList[1].props.setter.x = 350;
					instanceList[1].props.setter.y = 628;
				} else {
					instanceList[0].props.setter.x = 350;
					instanceList[0].props.setter.y = 380;

					instanceList[1].props.setter.x = 350;
					instanceList[1].props.setter.y = 628;
				}
			}

			instanceList.forEach(function (item) {
				_this2._pageDataList.content_info.two_layer.push(item);
			});
		}
	}, {
		key: "_networkLinkPortSetting",
		value: function _networkLinkPortSetting() {
			var _this3 = this;

			console.log("headerPortDataListdd", this._headerPortDataList);
			// console.log("headerPortDataListdd", this._sourceCompositionData); // 구성정보
			var positionX = 40;
			var positionY = 100;

			var excelData = this._excelPortDataList.slice();

			excelData.some(function (portItem, index) {
				var portInstance = null;
				var toNetworkState = null;
				var fromNetworkState = null;
				toNetworkState = _this3._headerPortDataList.find(function (item) {
					// Link 리스트에 To IP로 된 networkState를 찾는다.
					return item["IP"].indexOf(portItem["IP_1"]) != -1;
				});

				if (toNetworkState) {
					// Link 리스트에 To state가 있을 경우에만 from을 찾음.
					fromNetworkState = _this3._headerPortDataList.find(function (item) {
						return item["IP"].indexOf(portItem["IP"]) != -1;
					});
					portInstance = $.extend(true, {}, basic_page.NetworkLinkMetaInfo);
					portInstance.name = "net_link_" + index;
					portInstance.componentName = "NetworkLinkComponent";
				} else {
					return false;
				}

				if (fromNetworkState) {
					portInstance.props.network.from.ip = fromNetworkState["IP"];
					portInstance.props.network.from.hostName = fromNetworkState["장비명"];
					portInstance.props.network.from.port = portItem["PORT"];
					portInstance.props.network.from.target = fromNetworkState["IP"].replace(/\./g, "_");
				}

				if (toNetworkState) {
					portInstance.props.network.to.ip = toNetworkState["IP"];
					portInstance.props.network.to.hostName = toNetworkState["장비명"];
					portInstance.props.network.to.port = portItem["PORT_1"];
					portInstance.props.network.to.target = toNetworkState["IP"].replace(/\./g, "_");
				}

				if (portInstance) {
					// null이 아닐 경우에만 추가
					_this3._pageDataList.content_info.two_layer.push(portInstance);
				}
			});
		}

		// window._excelPortDataLis

	}, {
		key: "_getStartPosition",
		value: function _getStartPosition(rowInfo) {
			var position = {};

			if (rowInfo.length <= 5) {
				position = { // 5개
					x: 500,
					y: 384
				};
			} else if (rowInfo.length > 5 && rowInfo.length <= 10) {
				position = { //10개
					x: 500,
					y: 290
				};
			} else if (rowInfo.length > 10 && rowInfo.length <= 15) {
				position = { //15개
					x: 500,
					y: 255
				};
			} else if (rowInfo.length > 15 && rowInfo.length <= 20) {
				position = { //20개
					x: 500,
					y: 195
				};
			} else {
				position = { // 40개 시작 위치
					x: 480,
					y: 195
				};
			}
			return position;
		}
	}, {
		key: "_getNetworkComponentNextPosition",
		value: function _getNetworkComponentNextPosition(rowInfo, colIndex, position, index) {
			position.x += this.templateRect.width;

			if (rowInfo.length <= 5) {
				if (index == rowInfo.length - 1) {
					// 마지막에 한번 실행
					this.drawLine(position.x);
				}
			} else if (rowInfo.length > 5 && rowInfo.length <= 10) {
				if (index == rowInfo.length - 1) {
					// 마지막에 한번 실행
					this.drawLine(position.x);
				}

				if (colIndex % 5 == 0) {
					// 10개
					position.x = 500;
					position.y += 190;
				}
			} else if (rowInfo.length > 10 && rowInfo.length <= 15) {
				if (index == rowInfo.length - 1) {
					// 마지막에 한번 실행
					this.drawLine(position.x);
				}

				if (colIndex % 5 == 0) {
					// 20개
					position.x = 500;
					position.y += 180;
				}
			} else if (rowInfo.length > 15 && rowInfo.length <= 20) {
				if (index == rowInfo.length - 1) {
					// 마지막에 한번 실행
					this.drawLine(position.x);
				}

				if (colIndex % 5 == 0) {
					// 20개
					position.x = 500;
					position.y += 180;
				}
			} else {
				//40개
				if (index == rowInfo.length - 1) {
					// 마지막에 한번 실행
					this.drawLine(position.x);
				}

				if (colIndex % 10 == 0) {
					// 40개
					position.x = 480;
					position.y += 180;
				}
			}

			return position;
		}

		//@OverRide, loading

	}, {
		key: "drawLine",
		value: function drawLine(lineValue) {

			// 페이지에서 circle 아무거나 찾아온다.
			var solidLine = $.extend(true, {}, this.baiscLineList.solidLine);
			var dashedLine = $.extend(true, {}, this.baiscLineList.dashedLine);
			var solidCircleStart = this._getComponentInstanceByName("solid_circle_0");

			var solidCircleEnd = $.extend(true, {}, solidCircleStart);
			var dashedCircleEnd = $.extend(true, {}, solidCircleStart);

			var lineX = solidCircleStart.props.setter.x;
			var lineY = solidCircleStart.props.setter.y + 4;

			// solid line
			var position1 = { x: lineX, y: lineY };
			var position2 = { x: lineValue, y: lineY };
			var points = [position1, position2];
			solidLine.props.points = JSON.stringify(points);
			solidLine.props.setter.height = 0;
			solidLine.name = "solid_line";
			// solid line

			// dashed line +10
			position1 = { x: lineX + 10, y: lineY + 10 };
			position2 = { x: lineValue + 10, y: lineY + 10 };
			points = [position1, position2];
			dashedLine.props.points = JSON.stringify(points);
			dashedLine.props.setter.height = 0;
			dashedLine.name = "dashed_line";
			// dashed line

			var tempY = solidCircleStart.props.setter.y;

			// solid circle End
			solidCircleEnd.props.setter.x = lineValue;
			solidCircleEnd.props.setter.y = tempY;
			solidCircleEnd.name = "solid_circle_1";
			// solid circle End

			// dashed circle End
			dashedCircleEnd.props.setter.x = lineValue + 10;
			dashedCircleEnd.props.setter.y = tempY + 10;
			dashedCircleEnd.name = "dashed_circle_1";
			// dashed circle End

			this._pageDataList.content_info.two_layer.push(solidLine);
			this._pageDataList.content_info.two_layer.push(dashedLine);
			this._pageDataList.content_info.two_layer.push(solidCircleEnd);
			this._pageDataList.content_info.two_layer.push(dashedCircleEnd);
		}
	}]);

	return horizontal_page;
}(basic_page);