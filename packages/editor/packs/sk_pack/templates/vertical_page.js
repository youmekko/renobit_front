class vertical_page extends basic_page {
	constructor() {
		super();
	}

	createPage(pageData, headerPortDataList, excelRowDataList, excelPortDataList) {
		super.createPage(pageData, headerPortDataList, excelRowDataList, excelPortDataList);
	}

	//@Override
	_attachLink() {
		let linkDataList = [];
		let instanceList = [];

		let firstLinkDataList = this._excelRowDataList.get("1"); // 1단 불러옴
		let secondLinkDataList = this._excelRowDataList.get("2"); // 2단 불러옴
		let accessDataList = this._excelRowDataList.get("A"); // a단 불러옴
		let badDataList = this._excelRowDataList.get("BAD"); // a단 불러옴

		let positionLeft = 0;
		let positionTop = 0;
		let metaInfo = null;

		linkDataList = linkDataList.concat(firstLinkDataList); // 1단 추가
		if (secondLinkDataList) { // 2단이 있으면
			linkDataList = linkDataList.concat(secondLinkDataList); // 2단 추가
		}

		// console.log("_pageDataList%$%", firstLinkDataList);
		// console.log("$%$%", secondLinkDataList);
		// console.log("$%$%", accessDataList);
		// console.log("$%$%", this.DefaultRowVO);
		console.log("badDataList", badDataList);

		if (this._headerPortDataList) {
			this._headerPortDataList.forEach((networkState, index) => {
				let newInstanceInfo = $.extend(true, {}, basic_page.NetworkStateMetaInfo); // link Instance
				newInstanceInfo.props.name = "network_link_" + index;
				// let networkVO =  $.extend({}, this.DefaultRowVO); // network value
				// networkVO.
				let equipmentType = networkState["장비종류"];
				let comVO = basic_page.EquipmentTypeToComponent[equipmentType];
				// Backbone
				newInstanceInfo.name = networkState.IP.replace(/\./g, "_"); // 치환
				newInstanceInfo.props.setter.info.name = comVO.name;
				newInstanceInfo.props.setter.info.path = comVO.path;
				// alias + ip 출력
				newInstanceInfo.props.network.manufacturer = networkState["제조사"];
				// newInstanceInfo.props.network.company_label = networkState["제조사"];
				newInstanceInfo.props.network.ip = networkState["IP"];
				newInstanceInfo.props.network.hostName = networkState["장비명"];
				//network 속성 설정
				newInstanceInfo.props.network.row = networkState["ROW"] + ""; // int로 되는 경우가 있어서 뒤에 +""

				if (networkState["serial_number"]) {
					newInstanceInfo.props.network.serial_number = networkState["serial_number"];
				}

				if (networkState["model"]) {
					newInstanceInfo.props.network.model = networkState["model"];
				}

				if (networkState["cid"]) {
					newInstanceInfo.props.network.cid = networkState["cid"];
				}

				// Network State 마다 적정 크기가 달라서 info 에 width, height가 있을 경우에만 사이즈를 변경해준다.
				if (comVO.hasOwnProperty("width")) {
					newInstanceInfo.props.setter.width = comVO.width;
				}

				if (comVO.hasOwnProperty("height")) {
					newInstanceInfo.props.setter.height = comVO.height;
				}

				instanceList.push(newInstanceInfo);
				console.log("newInstanceInfonewInstanceInfo", newInstanceInfo);
				// this._pageDataList.content_info.two_layer.push(newInstanceInfo);
			})
		}

		this._networkLinkSetPosition(instanceList, accessDataList.length);
		this._networkLinkPortSetting();
	}

	// 포지션은 1,2,3,4 라고 생각한다.
	_networkLinkSetPosition(instanceList, accessDataListLength) {
		if (instanceList.length == 1) { // 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
			if (accessDataListLength < 6) {
				instanceList[0].props.setter.x = 886;
				instanceList[0].props.setter.y = 432;
			} else if (accessDataListLength < 31) {
				instanceList[0].props.setter.x = 892;
				instanceList[0].props.setter.y = 312;
			} else {
				instanceList[0].props.setter.x = 876;
				instanceList[0].props.setter.y = 464;
			}
		}

		if (instanceList.length == 2) { // 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
			if (accessDataListLength < 6) {
				instanceList[0].props.setter.x = 886;
				instanceList[0].props.setter.y = 432;

				instanceList[1].props.setter.x = 1268;
				instanceList[1].props.setter.y = 432;
			} else if (accessDataListLength < 31) {
				instanceList[0].props.setter.x = 892;
				instanceList[0].props.setter.y = 312;

				instanceList[1].props.setter.x = 1278;
				instanceList[1].props.setter.y = 312;
			} else {
				instanceList[0].props.setter.x = 876;
				instanceList[0].props.setter.y = 464;

				instanceList[1].props.setter.x = 1260;
				instanceList[1].props.setter.y = 464;
			}
		}

		if (instanceList.length == 3) {
			if (accessDataListLength < 6) {
				instanceList[0].props.setter.x = 886;
				instanceList[0].props.setter.y = 260;

				instanceList[1].props.setter.x = 1270;
				instanceList[1].props.setter.y = 260;

				instanceList[2].props.setter.x = 886;
				instanceList[2].props.setter.y = 430;

			} else if (accessDataListLength < 31) {
				instanceList[0].props.setter.x = 892;
				instanceList[0].props.setter.y = 145;

				instanceList[1].props.setter.x = 1276;
				instanceList[1].props.setter.y = 145;

				instanceList[2].props.setter.x = 892;
				instanceList[2].props.setter.y = 315;

			} else {
				instanceList[0].props.setter.x = 876;
				instanceList[0].props.setter.y = 464;

				instanceList[1].props.setter.x = 1260;
				instanceList[1].props.setter.y = 464;

				instanceList[2].props.setter.x = 876;
				instanceList[2].props.setter.y = 464;

			}
		}

		if (instanceList.length == 4) {
			if (accessDataListLength < 6) {
				instanceList[0].props.setter.x = 886;
				instanceList[0].props.setter.y = 260;

				instanceList[1].props.setter.x = 1270;
				instanceList[1].props.setter.y = 260;

				instanceList[2].props.setter.x = 886;
				instanceList[2].props.setter.y = 430;

				instanceList[3].props.setter.x = 1270;
				instanceList[3].props.setter.y = 430;
			} else if (accessDataListLength < 31) {
				instanceList[0].props.setter.x = 892;
				instanceList[0].props.setter.y = 145;

				instanceList[1].props.setter.x = 1276;
				instanceList[1].props.setter.y = 145;

				instanceList[2].props.setter.x = 892;
				instanceList[2].props.setter.y = 315;

				instanceList[3].props.setter.x = 1276;
				instanceList[3].props.setter.y = 315;
			} else {
				instanceList[0].props.setter.x = 876;
				instanceList[0].props.setter.y = 464;

				instanceList[1].props.setter.x = 1260;
				instanceList[1].props.setter.y = 464;

				instanceList[2].props.setter.x = 876;
				instanceList[2].props.setter.y = 464;

				instanceList[3].props.setter.x = 1260;
				instanceList[3].props.setter.y = 464;
			}
		}

		instanceList.forEach((item) => {
			this._pageDataList.content_info.two_layer.push(item);
		});
	}

	_networkLinkPortSetting() {
		console.log("headerPortDataListdd", this._headerPortDataList);
		// console.log("headerPortDataListdd", this._sourceCompositionData); // 구성정보
		let positionX = 40;
		let positionY = 100;

		let excelData = this._excelPortDataList.slice();

		excelData.some((portItem, index) => {
			let portInstance = null;
			let toNetworkState = null;
			let fromNetworkState = null;
			toNetworkState = this._headerPortDataList.find((item) => { // Link 리스트에 To IP로 된 networkState를 찾는다.
				return item["IP"].indexOf(portItem["IP_1"]) != -1;
			})

			if (toNetworkState) { // Link 리스트에 To state가 있을 경우에만 from을 찾음.
				fromNetworkState = this._headerPortDataList.find((item) => {
					return item["IP"].indexOf(portItem["IP"]) != -1;
				});
				portInstance = $.extend(true, {}, basic_page.NetworkLinkMetaInfo);
				portInstance.name = "net_link_" + index;
				portInstance.componentName = "NetworkLinkComponent";
			} else {
				return false;
			}

			if (fromNetworkState) {
				portInstance.props.network.from.ip = fromNetworkState["IP"];
				portInstance.props.network.from.hostName = fromNetworkState["장비명"];
				portInstance.props.network.from.port = portItem["PORT"];
				portInstance.props.network.from.target = fromNetworkState["IP"].replace(/\./g, "_");
			}

			if (toNetworkState) {
				portInstance.props.network.to.ip = toNetworkState["IP"];
				portInstance.props.network.to.hostName = toNetworkState["장비명"];
				portInstance.props.network.to.port = portItem["PORT_1"];
				portInstance.props.network.to.target = toNetworkState["IP"].replace(/\./g, "_");
			}

			if (portInstance) { // null이 아닐 경우에만 추가
				this._pageDataList.content_info.two_layer.push(portInstance);
			}

		})
	}


	// @Override
	// _changeNetworkSymbolInfo(newInstance, networkState) {
	// 	super._changeNetworkSymbolInfo(newInstance, networkState);
	//
	// 	// var hostNameLength = instance.props.network.hostName.toString().length;
	// 	// var ipValueLength = instance.props.network.ip.toString().length;
	// 	//
	// 	// if (hostNameLength > ipValueLength) {
	// 	// 	hostNameLength = hostNameLength * 8
	// 	// } else {
	// 	// 	hostNameLength = ipValueLength * 8;
	// 	// }
	// 	//
	// 	// if ((instance.props.label.label_position == "LT") || (instance.props.label.label_position == "LC") || (instance.props.label.label_position == "LB")) {
	// 	// 	hostNameLength *= 1;
	// 	// 	instance.props.label.label_offset_x = -hostNameLength;
	// 	// } else if ((instance.props.label.label_position == "RT") || (instance.props.label.label_position == "RC") || (instance.props.label.label_position == "RB")) {
	// 	// 	instance.props.label.label_offset_x = hostNameLength;
	// 	// }
	// }

	_getStartPosition(rowInfo) {
		var position = {};

		if (rowInfo.length <= 5) {
			position = { // 5개
				x: 668,
				y: 586
			}
		} else if ((rowInfo.length > 5) && (rowInfo.length <= 9)) { // 7~9
			position = {
				x: 605,
				y: 470
			}
		} else if ((rowInfo.length > 9) && (rowInfo.length <= 12)) { // 10~12
			position = {
				x: 625,
				y: 450
			}
		} else if ((rowInfo.length > 12) && (rowInfo.length <= 15)) { // 13~15
			position = {
				x: 625,
				y: 450
			}
		} else if ((rowInfo.length > 15) && (rowInfo.length <= 20)) { // 20
			position = {
				x: 510,
				y: 448
			}
		} else if ((rowInfo.length > 20) && (rowInfo.length <= 25)) {
			position = {
				x: 350,
				y: 448
			}
		} else if ((rowInfo.length > 25) && (rowInfo.length <= 30)) {
			position = {
				x: 335,
				y: 448
			}
		} else {
			position = {
				x: 320,
				y: 436
			}
		}
		return position;
	}

	_getNetworkComponentNextPosition(rowInfo, colIndex, position) {
		if (rowInfo.length <= 5) {
			position.x += this.templateRect.width; //5개 이하 포지션 변경
		} else {
			position.y += this.templateRect.height; //5개 이상(42개) 포지션 변경
		}

		if (rowInfo.length == 6) {
			if (colIndex % 2 == 0) { // 10개
				position.x += 300;
				position.y = 470;
			}
		} else if ((rowInfo.length > 6) && (rowInfo.length <= 9)) { //6~9
			// this.baiscLineList[0].props
			if (colIndex % 7 == 0) {
				this.drawLine(position.x + 20, rowInfo.length, 7, 29);
			}

			if (colIndex % 3 == 0) { // 9개
				position.x += 300;
				position.y = 470;
			}

		} else if ((rowInfo.length > 9) && (rowInfo.length <= 12)) { //12개
			if (colIndex % 10 == 0) {
				this.drawLine(position.x, rowInfo.length, 9, 29);
			}

			if (colIndex % 4 == 0) {
				position.x += 300;
				position.y = 450;
			}
		} else if ((rowInfo.length > 12) && (rowInfo.length <= 15)) { //15개
			if (colIndex % 11 == 0) {
				this.drawLine(position.x, rowInfo.length, 11, 29);
			}

			if (colIndex % 5 == 0) {
				position.x += 300;
				position.y = 450;
			}
		} else if ((rowInfo.length > 15) && (rowInfo.length <= 20)) { //20개
			if (colIndex % 16 == 0) {
				this.drawLine(position.x, rowInfo.length, 16, 29);
			}

			if (colIndex % 5 == 0) { // 20개
				position.x += 300;
				position.y = 448;
			}
		} else if ((rowInfo.length > 20) && (rowInfo.length <= 25)) { //25개
			if (colIndex % 21 == 0) {
				this.drawLine(position.x, rowInfo.length, 21, 29);
			}

			if (colIndex % 5 == 0) { // 24개
				position.x += 300;
				position.y = 448;
			}
		} else if ((rowInfo.length > 25) && (rowInfo.length <= 30)) { //30개
			if (colIndex % 26 == 0) {
				this.drawLine(position.x, rowInfo.length, 26, 14);
			}

			if (colIndex % 5 == 0) { // 30개
				position.x += 265;
				position.y = 448;
			}
		} else {

			if (colIndex % 37 == 0) {
				this.drawLine(position.x, rowInfo.length, 37, 14);
			}

			if (colIndex % 6 == 0) { // 42개
				position.x += 225;
				position.y = 436;
			}
		}

		return position;
	}

	drawLine(positionX, length, lineNum, value) {
		// 보정 값:  받는 값이 tmp_wrapper안에 있는 x공백임. 추후 계산해서 변경
		positionX += value;

		// 페이지에서 circle 아무거나 찾아온다.
		let circle = this._pageDataList.content_info.two_layer.find((comInstanceInfo) => {
			return comInstanceInfo.name.indexOf("svg_cir") == 0;
		})

		let solidCircleStart = $.extend(true, {}, circle);
		let dashedCircleStart = $.extend(true, {}, circle);

		let solidCircleEnd = $.extend(true, {}, circle);
		let dashedCircleEnd = $.extend(true, {}, circle);

		let positionY1 = this.baiscLineList.solidLine.props.setter.y;
		let positionY2 = null;

		let solidLine = $.extend(true, {}, this.baiscLineList.solidLine);
		let dashedLine = $.extend(true, {}, this.baiscLineList.dashedLine);

		let temp = 0;

		if (length) {
			temp = length % lineNum;
			temp++;
			positionY2 = parseInt(this.templateRect.height * temp);
		}

		let solidLineEndPosition = positionY1 + positionY2;

		// solid line
		let position1 = {x: positionX, y: positionY1};
		let position2 = {x: positionX, y: solidLineEndPosition};

		let points = [position1, position2];
		solidLine.props.points = JSON.stringify(points);
		solidLine.name = "solid_line";
		// solid line

		// dashed line +10
		position1 = {x: positionX + 10, y: positionY1 + 10};
		position2 = {x: positionX + 10, y: solidLineEndPosition + 10};
		points = [position1, position2];
		dashedLine.props.points = JSON.stringify(points);
		dashedLine.name = "dashed_line";
		// dashed line

		let x = positionX - 4;
		let y = positionY1 - 4;
		// solid circle Start
		solidCircleStart.props.setter.x = x;
		solidCircleStart.props.setter.y = y;
		solidCircleStart.name = "solid_circle_0";
		// solid circle Start

		// dashed circle Start
		dashedCircleStart.props.setter.x = x + 10;
		dashedCircleStart.props.setter.y = y + 10;
		dashedCircleStart.name = "dashed_circle_0";
		// dashed circle Start

		// solid circle End
		solidCircleEnd.props.setter.x = x;
		solidCircleEnd.props.setter.y = solidLineEndPosition;
		solidCircleEnd.name = "solid_circle_1";
		// solid circle End

		// dashed circle End
		dashedCircleEnd.props.setter.x = x + 10;
		dashedCircleEnd.props.setter.y = solidLineEndPosition + 10;
		dashedCircleEnd.name = "dashed_circle_1";
		// dashed circle End

		this._pageDataList.content_info.two_layer.push(solidLine);
		this._pageDataList.content_info.two_layer.push(dashedLine);
		this._pageDataList.content_info.two_layer.push(solidCircleStart);
		this._pageDataList.content_info.two_layer.push(solidCircleEnd);
		this._pageDataList.content_info.two_layer.push(dashedCircleStart);
		this._pageDataList.content_info.two_layer.push(dashedCircleEnd);
	}
}