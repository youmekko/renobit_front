"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PageCreator = function () {
	function PageCreator() {
		_classCallCheck(this, PageCreator);

		this._networkProxy = null;
		/* 템플릿 그릴때 필요한 좌표들 */
		this.startPositionX = 560;
		this.startPositionY = 500;
		this.height = 100;
		this.width = 280;
	}

	/*
 	1. 템플릿 복사하기
 	2. 헤더영역 생성하기
 	3. row 내용 생성하기
 	4. 페이지 크기 설정하기
 		1. NetworkState 추가 후 위치 설정
 	2. NeworkState 값 설정
 	3. Port 추가 후 위치 설정
 	4. Port 프로퍼티 값 설정
  */

	_createClass(PageCreator, [{
		key: "createPage",
		value: function createPage(pageData, networkProxy) {
			// 1. data 받아오기
			//pageData는 복사할 page를 가져온다. vertical = vertical_page, horizontal = horizontal_page
			this._pageDataList = pageData;
			this._networkProxy = networkProxy;

			this._createHeader();
			this._createBody();
			this._createBad();
		}
	}, {
		key: "_createHeader",
		value: function _createHeader() {}
	}, {
		key: "_createHeaderToBodyPort",
		value: function _createHeaderToBodyPort() {}
	}, {
		key: "_createBody",
		value: function _createBody() {}
	}, {
		key: "_setBodyElementVerticalPort",
		value: function _setBodyElementVerticalPort(rows, columns) {}
	}, {
		key: "_setBodyElement",
		value: function _setBodyElement(comInstanceVO, row, column) {}
	}, {
		key: "_createBad",
		value: function _createBad() {}
	}, {
		key: "_networkLinkSetPosition",
		value: function _networkLinkSetPosition(instanceList, accessDataListLength) {}
	}]);

	return PageCreator;
}();

PageCreator.HorizontalPortMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10177,
			"x": 400,
			"y": 450,
			"visible": true,
			"opacity": 1,
			"width": 1400,
			"height": 20,
			"rotation": 0
		},
		"label": {
			"label_using": "N",
			"label_text": "Frame Component",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0,
			"cursor": "default",
			"min-width": "20px",
			"min-height": "20px"
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"portInfo": {
			"direction": "forward",
			"position": "top",
			"lineStyle": "solid",
			"fromText": "",
			"fromHost": "fromHost",
			"fromIP": "fromIP",
			"toText": "",
			"toHost": "toHost",
			"toIP": "toIP",
			"textTopMargin": 0,
			"textBottomMargin": 0
		},
		"componentName": "HorizontalPortComponent",
		"name": "horizontal_line"
	},
	"id": "4c0eecaf-76e7-4df1-b222-a6a57d26a7a7",
	"name": "horizontal_line",
	"layerName": "twoLayer",
	"componentName": "HorizontalPortComponent",
	"version": "1.0.0",
	"category": "2D"
};

PageCreator.VerticalPortMetaInfo = {
	"props": {
		"setter": {
			"mouseEnabled": true,
			"depth": 10178,
			"x": 592,
			"y": 249,
			"visible": true,
			"opacity": 1,
			"width": 60,
			"height": 86,
			"rotation": 0
		},
		"label": {
			"label_using": "N",
			"label_text": "Frame Component",
			"label_position": "CB",
			"label_offset_x": 0,
			"label_offset_y": 0,
			"label_color": "#333333",
			"label_font_type": "inherit",
			"label_font_size": 11,
			"label_border": "1px none #000000",
			"label_background_color": "#eeeeee",
			"label_border_radius": 0,
			"label_opacity": 1
		},
		"style": {
			"border": "1px none #000000",
			"backgroundColor": "rgba(255,255,255,0)",
			"borderRadius": 0,
			"cursor": "default",
			"min-width": "20px",
			"min-height": "20px"
		},
		"events": {
			"click": "",
			"dblclick": "",
			"register": "",
			"complete": ""
		},
		"editorMode": {
			"lock": false,
			"visible": true
		},
		"portInfo": {
			"direction": "reverse",
			"position": "right",
			"lineStyle": "solid",
			"fromText": "",
			"fromHost": "fromHost",
			"fromIP": "fromIP",
			"toText": "",
			"toHost": "toHost",
			"toIP": "toIP",
			"textTopMargin": 0,
			"textBottomMargin": 0
		}
	},
	"id": "a11c2679-7db0-482a-9921-09231a62f8ff",
	"name": "vertical_line",
	"layerName": "twoLayer",
	"componentName": "VerticalPortComponent",
	"version": "1.0.0",
	"category": "2D"
};