class horizontal_page extends basic_page {
	constructor() {
		super();
	}

	createPage(pageData, headerPortDataList, excelRowDataList, excelPortDataList) {
		super.createPage(pageData, headerPortDataList, excelRowDataList, excelPortDataList);
	}

	//@Override
	_attachLink() {
		let linkDataList = [];
		let instanceList = [];

		let firstLinkDataList = this._excelRowDataList.get("1"); // 1단 불러옴
		let secondLinkDataList = this._excelRowDataList.get("2"); // 2단 불러옴
		let accessDataList = this._excelRowDataList.get("A"); // a단 불러옴

		let positionLeft = 0;
		let positionTop = 0;
		let metaInfo = null;

		linkDataList = linkDataList.concat(firstLinkDataList); // 1단 추가
		if (secondLinkDataList) { // 2단이 있으면
			linkDataList = linkDataList.concat(secondLinkDataList); // 2단 추가
		}

		// console.log("_pageDataList%$%", firstLinkDataList);
		// console.log("$%$%", secondLinkDataList);
		// console.log("$%$%", accessDataList);
		// console.log("$%$%", this.DefaultRowVO);
		// console.log("linkDataList", linkDataList);

		if (this._headerPortDataList) {
			this._headerPortDataList.forEach((networkState, index) => {
				let newInstanceInfo = $.extend(true, {}, basic_page.NetworkStateMetaInfo); // link Instance
				newInstanceInfo.props.name = "network_link_" + index;
				// let networkVO =  $.extend({}, this.DefaultRowVO); // network value
				// networkVO.
				let equipmentType = networkState["장비종류"];
				let comVO = basic_page.EquipmentTypeToComponent[equipmentType];
				// Backbone
				newInstanceInfo.name = networkState.IP.replace(/\./g, "_"); // 치환
				newInstanceInfo.props.setter.info.name = comVO.name;
				newInstanceInfo.props.setter.info.path = comVO.path;
				// alias + ip 출력
				newInstanceInfo.props.network.manufacturer = networkState["제조사"];
				// newInstanceInfo.props.network.company_label = networkState["제조사"];
				newInstanceInfo.props.network.ip = networkState["IP"];
				newInstanceInfo.props.network.hostName = networkState["장비명"];
				//network 속성 설정
				newInstanceInfo.props.network.row = networkState["ROW"] + ""; // int로 되는 경우가 있어서 뒤에 +""

				if (networkState["serial_number"]) {
					newInstanceInfo.props.network.serial_number = networkState["serial_number"];
				}

				if (networkState["model"]) {
					newInstanceInfo.props.network.model = networkState["model"];
				}

				if (networkState["cid"]) {
					newInstanceInfo.props.network.cid = networkState["cid"];
				}

				// Network State 마다 적정 크기가 달라서 info 에 width, height가 있을 경우에만 사이즈를 변경해준다.
				if (comVO.hasOwnProperty("width")) {
					newInstanceInfo.props.setter.width = comVO.width;
				}

				if (comVO.hasOwnProperty("height")) {
					newInstanceInfo.props.setter.height = comVO.height;
				}

				instanceList.push(newInstanceInfo);
				console.log("newInstanceInfonewInstanceInfo", newInstanceInfo);
				// this._pageDataList.content_info.two_layer.push(newInstanceInfo);
			})
		}

		this._networkLinkSetPosition(instanceList, accessDataList.length);
		this._networkLinkPortSetting();
	}

	// 포지션은 1,2
	_networkLinkSetPosition(instanceList, accessDataListLength) {
		if (instanceList.length == 1) { // 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
			if (accessDataListLength <= 5) {
				instanceList[0].props.setter.x = 350;
				instanceList[0].props.setter.y = 380;
			} else if (accessDataListLength <= 10) {
				instanceList[0].props.setter.x = 350;
				instanceList[0].props.setter.y = 390;
			}else if (accessDataListLength <= 15) {
				instanceList[0].props.setter.x = 350;
				instanceList[0].props.setter.y = 380;
			} else {
				instanceList[0].props.setter.x = 350;
				instanceList[0].props.setter.y = 380;
			}
		}

		if (instanceList.length == 2) { // 구성정보의 network State가 하나일 경우. 3 위치에 배치한다.
			if (accessDataListLength <= 5) {
				instanceList[0].props.setter.x = 350;
				instanceList[0].props.setter.y = 380;

				instanceList[1].props.setter.x = 350;
				instanceList[1].props.setter.y = 628;
			} else if (accessDataListLength <= 10) {
				instanceList[0].props.setter.x = 350;
				instanceList[0].props.setter.y = 390;

				instanceList[1].props.setter.x = 350;
				instanceList[1].props.setter.y = 638;
			}else if (accessDataListLength <= 15) {
				instanceList[0].props.setter.x = 350;
				instanceList[0].props.setter.y = 380;

				instanceList[1].props.setter.x = 350;
				instanceList[1].props.setter.y = 628;
			} else {
				instanceList[0].props.setter.x = 350;
				instanceList[0].props.setter.y = 380;

				instanceList[1].props.setter.x = 350;
				instanceList[1].props.setter.y = 628;
			}
		}

		instanceList.forEach((item) => {
			this._pageDataList.content_info.two_layer.push(item);
		});
	}

	_networkLinkPortSetting() {
		console.log("headerPortDataListdd", this._headerPortDataList);
		// console.log("headerPortDataListdd", this._sourceCompositionData); // 구성정보
		let positionX = 40;
		let positionY = 100;

		let excelData = this._excelPortDataList.slice();

		excelData.some((portItem, index) => {
			let portInstance = null;
			let toNetworkState = null;
			let fromNetworkState = null;
			toNetworkState = this._headerPortDataList.find((item) => { // Link 리스트에 To IP로 된 networkState를 찾는다.
				return item["IP"].indexOf(portItem["IP_1"]) != -1;
			})

			if (toNetworkState) { // Link 리스트에 To state가 있을 경우에만 from을 찾음.
				fromNetworkState = this._headerPortDataList.find((item) => {
					return item["IP"].indexOf(portItem["IP"]) != -1;
				});
				portInstance = $.extend(true, {}, basic_page.NetworkLinkMetaInfo);
				portInstance.name = "net_link_" + index;
				portInstance.componentName = "NetworkLinkComponent";
			} else {
				return false;
			}

			if (fromNetworkState) {
				portInstance.props.network.from.ip = fromNetworkState["IP"];
				portInstance.props.network.from.hostName = fromNetworkState["장비명"];
				portInstance.props.network.from.port = portItem["PORT"];
				portInstance.props.network.from.target = fromNetworkState["IP"].replace(/\./g, "_");
			}

			if (toNetworkState) {
				portInstance.props.network.to.ip = toNetworkState["IP"];
				portInstance.props.network.to.hostName = toNetworkState["장비명"];
				portInstance.props.network.to.port = portItem["PORT_1"];
				portInstance.props.network.to.target = toNetworkState["IP"].replace(/\./g, "_");
			}

			if (portInstance) { // null이 아닐 경우에만 추가
				this._pageDataList.content_info.two_layer.push(portInstance);
			}

		})
	}

	// window._excelPortDataLis
	_getStartPosition(rowInfo) {
		var position = {};

		if (rowInfo.length <= 5) {
			position = { // 5개
				x: 500,
				y: 384
			}
		} else if ((rowInfo.length > 5) && (rowInfo.length <= 10)) {
			position = { //10개
				x: 500,
				y: 290
			}
		} else if ((rowInfo.length > 10) && (rowInfo.length <= 15)) {
			position = { //15개
				x: 500,
				y: 255
			}
		} else if ((rowInfo.length > 15) && (rowInfo.length <= 20)) {
			position = { //20개
				x: 500,
				y: 195
			}
		} else {
			position = { // 40개 시작 위치
				x: 480,
				y: 195
			}
		}
		return position;
	}

	_getNetworkComponentNextPosition(rowInfo, colIndex, position, index) {
		position.x += this.templateRect.width;

		if (rowInfo.length <= 5) {
			if (index == rowInfo.length - 1) { // 마지막에 한번 실행
				this.drawLine(position.x);
			}
		} else if ((rowInfo.length > 5) && (rowInfo.length <= 10)) {
			if (index == rowInfo.length - 1) { // 마지막에 한번 실행
				this.drawLine(position.x);
			}

			if (colIndex % 5 == 0) { // 10개
				position.x = 500;
				position.y += 190;
			}
		} else if ((rowInfo.length > 10) && (rowInfo.length <= 15)) {
			if (index == rowInfo.length - 1) { // 마지막에 한번 실행
				this.drawLine(position.x);
			}

			if (colIndex % 5 == 0) { // 20개
				position.x = 500;
				position.y += 180;
			}
		} else if ((rowInfo.length > 15) && (rowInfo.length <= 20)) {
			if (index == rowInfo.length - 1) { // 마지막에 한번 실행
				this.drawLine(position.x);
			}

			if (colIndex % 5 == 0) { // 20개
				position.x = 500;
				position.y += 180;
			}
		} else { //40개
			if (index == rowInfo.length - 1) { // 마지막에 한번 실행
				this.drawLine(position.x);
			}

			if (colIndex % 10 == 0) { // 40개
				position.x = 480;
				position.y += 180;
			}
		}

		return position;
	}

	//@OverRide, loading
	drawLine(lineValue) {

		// 페이지에서 circle 아무거나 찾아온다.
		let solidLine = $.extend(true, {}, this.baiscLineList.solidLine);
		let dashedLine = $.extend(true, {}, this.baiscLineList.dashedLine);
		let solidCircleStart = this._getComponentInstanceByName(`solid_circle_0`);

		let solidCircleEnd = $.extend(true, {}, solidCircleStart);
		let dashedCircleEnd = $.extend(true, {}, solidCircleStart);

		let lineX = solidCircleStart.props.setter.x;
		let lineY = solidCircleStart.props.setter.y + 4;

		// solid line
		let position1 = {x: lineX, y: lineY};
		let position2 = {x: lineValue, y: lineY};
		let points = [position1, position2];
		solidLine.props.points = JSON.stringify(points);
		solidLine.props.setter.height = 0;
		solidLine.name = "solid_line";
		// solid line

		// dashed line +10
		position1 = {x: lineX + 10, y: lineY + 10};
		position2 = {x: lineValue + 10, y: lineY + 10};
		points = [position1, position2];
		dashedLine.props.points = JSON.stringify(points);
		dashedLine.props.setter.height = 0;
		dashedLine.name = "dashed_line";
		// dashed line

		let tempY = solidCircleStart.props.setter.y;

		// solid circle End
		solidCircleEnd.props.setter.x = lineValue;
		solidCircleEnd.props.setter.y = tempY;
		solidCircleEnd.name = "solid_circle_1";
		// solid circle End

		// dashed circle End
		dashedCircleEnd.props.setter.x = lineValue + 10;
		dashedCircleEnd.props.setter.y = tempY + 10;
		dashedCircleEnd.name = "dashed_circle_1";
		// dashed circle End

		this._pageDataList.content_info.two_layer.push(solidLine);
		this._pageDataList.content_info.two_layer.push(dashedLine);
		this._pageDataList.content_info.two_layer.push(solidCircleEnd);
		this._pageDataList.content_info.two_layer.push(dashedCircleEnd);
	}
}