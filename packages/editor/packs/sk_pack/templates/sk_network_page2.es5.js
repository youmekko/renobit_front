"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var sk_network_page2 = function () {
      function sk_network_page2() {
            _classCallCheck(this, sk_network_page2);

            this._pageData = null;
            this.excelRowData = null;

            this.ExcelHeader = this.constructor.ExcelHeader;
            this.EquipmentTypeToComponent = this.constructor.EquipmentTypeToComponent;
      }

      /*
      1. 템플릿 복사하기
      2. 헤더영역 생성하기
      3. row 내용 생성하기
      4. 페이지 크기 설정하기
         */


      _createClass(sk_network_page2, [{
            key: "createPage",
            value: function createPage(pageData, excelRowData) {
                  this._pageData = pageData;
                  // row정보만 신규로 설정하기
                  this.excelRowData = excelRowData;

                  // 1. 템플릿 복사하기
                  if (this._attachTemplate() == false) {
                        alert("템플릿 정보가 존재하지 않습니다. 확인 후 다시 실행해주세요.");
                        return;
                  }

                  // 2. 헤더영역 생성하기.
                  this._createRow("0");
                  this._createRow("1");

                  // 3. row 생성하기
                  this._createRows();

                  // 4. 페이지 크기 재설정하기
                  //this._resetPageSize();
            }

            /*
            template page에서 템플릿 복사 하기
                  1. 기준 위치가 되는 wrapper 가져오기
                  2. 템플릿 영역 설정하기
                  3. 템플릿 페이지 정보에서 템플릿 정보만 구하기
                  4. tmp_wrapper의 시작위치를 기준으로 0,0을 기준으로 원점 처리
                  5. 템플릿 위치를 0,0으로 설정하기
            */

      }, {
            key: "_attachTemplate",
            value: function _attachTemplate() {

                  // 1. 기준 위치가 되는 wrapper 가져오기
                  var wrapper = this._getComponentInstanceByName("tmp_wrapper");

                  // 2. 템플릿 영역 설정하기
                  this.templateRect = {
                        x: wrapper.props.setter.x,
                        y: wrapper.props.setter.y,
                        width: wrapper.props.setter.width,
                        height: wrapper.props.setter.height

                        // 템플릿 영역으로 사용하는 객체 삭제하기
                  };var list = this._pageData.content_info.two_layer;
                  for (var i = 0; i < list.length; i++) {
                        var item = list[i];
                        if (item.name == "tmp_wrapper") {
                              this._pageData.content_info.two_layer.splice(i, 1);
                        }
                  }

                  // 3. 템플릿 페이지 정보에서 템플릿 정보만 구하기
                  this.templateInstanceList = this._pageData.content_info.two_layer.filter(function (comInstanceInfo) {
                        return comInstanceInfo.name.indexOf("tmp_") != -1;
                  });

                  if (this.templateInstanceList.length <= 0) return false;

                  // 4. tmp_wrapper의 시작위치를 기준으로 0,0을 기준으로 원점 처리
                  var byX = this.templateRect.x;
                  var byY = this.templateRect.y;

                  this.templateInstanceList.forEach(function (instance) {
                        instance.props.setter.x = instance.props.setter.x - byX;
                        instance.props.setter.y = instance.props.setter.y - byY;

                        if (instance.name.indexOf("tmp_line") != -1) {

                              var points = eval(instance.props.points);

                              points.forEach(function (point) {
                                    point.x = point.x - byX;
                                    point.y = point.y - byY;
                              });

                              instance.props.points = JSON.stringify(points);
                        }
                  });

                  // 5. 템플릿 위치를 0,0으로 설정하기
                  this.templateRect.x = 0;
                  this.templateRect.y = 0;

                  return true;
            }

            /*
            헤더 정보 생성하기.
             */

      }, {
            key: "_createRow",
            value: function _createRow(row) {
                  var headerInstanceName = "row_" + row;

                  // 0 - 0번째
                  var rowInfoList = this.excelRowData.get(row.toString());
                  var rowInfo = rowInfoList[0];

                  //1. 심볼 변경하기
                  var equipmentType = rowInfo[this.ExcelHeader.EQUIPMENT_TYPE];
                  var comVO = this.EquipmentTypeToComponent[equipmentType];
                  var infoInstance = this._getComponentInstanceByName(headerInstanceName + "_symbol_0");
                  infoInstance.props.setter.info.name = comVO.name;
                  infoInstance.props.setter.info.path = comVO.path;

                  // alias + ip 출력
                  var txtInfo = rowInfo[this.ExcelHeader.EQUIPMENT_ALIAS] + "\n" + rowInfo[this.ExcelHeader.IP];
                  infoInstance = this._getComponentInstanceByName(headerInstanceName + "_0_info");
                  infoInstance.props.setter.text = txtInfo;

                  // port 값 설정하기
                  // 0 - 0 - 0 번째
                  var portInfo = rowInfo.ports;
                  var portInstance = this._getComponentInstanceByName(headerInstanceName + "_0_0_from");
                  portInstance.props.setter.text = portInfo[0].from;
                  portInstance = this._getComponentInstanceByName(headerInstanceName + "_0_0_to");
                  portInstance.props.setter.text = portInfo[0].to;

                  // 0 - 0 - 1 번째
                  portInstance = this._getComponentInstanceByName(headerInstanceName + "_0_1_from");
                  portInstance.props.setter.text = portInfo[1].from;
                  portInstance = this._getComponentInstanceByName(headerInstanceName + "_0_1_to");
                  portInstance.props.setter.text = portInfo[1].to;

                  // 0 - 1번째
                  rowInfo = rowInfoList[1];

                  //1. 심볼 변경하기
                  equipmentType = rowInfo[this.ExcelHeader.EQUIPMENT_TYPE];
                  comVO = this.EquipmentTypeToComponent[equipmentType];
                  infoInstance = this._getComponentInstanceByName(headerInstanceName + "_symbol_1");
                  infoInstance.props.setter.info.name = comVO.name;
                  infoInstance.props.setter.info.path = comVO.path;

                  // alias + ip 출력
                  txtInfo = rowInfo[this.ExcelHeader.EQUIPMENT_ALIAS] + "\n" + rowInfo[this.ExcelHeader.IP];
                  infoInstance = this._getComponentInstanceByName(headerInstanceName + "_1_info");
                  infoInstance.props.setter.text = txtInfo;

                  // port 값 설정하기
                  // 0 - 0 - 0 번째
                  portInfo = rowInfo.ports;
                  portInstance = this._getComponentInstanceByName(headerInstanceName + "_1_0_from");
                  portInstance.props.setter.text = portInfo[0].from;
                  portInstance = this._getComponentInstanceByName(headerInstanceName + "_1_0_to");
                  portInstance.props.setter.text = portInfo[0].to;

                  // 0 - 0 - 1 번째
                  portInstance = this._getComponentInstanceByName(headerInstanceName + "_1_1_from");
                  portInstance.props.setter.text = portInfo[1].from;
                  portInstance = this._getComponentInstanceByName(headerInstanceName + "_1_1_to");
                  portInstance.props.setter.text = portInfo[1].to;
            }

            /*
            rwo 개수/2 만큼 템플릿 복사하기
             */

      }, {
            key: "_createRows",
            value: function _createRows() {

                  var rowIndex = "2";
                  var rowInfoList = this.excelRowData.get(rowIndex);

                  // 시작 위치
                  var position = {
                        x: 100,
                        y: 434

                        // 복사 시작 (2개씩 쌍으로)
                  };var colIndex = 0;
                  for (var index = 0; index < rowInfoList.length; index += 2) {
                        var instanceName = "row_" + rowIndex + "_col_" + colIndex;
                        this._copyTemplate(this.templateInstanceList, instanceName, position, rowInfoList[index], rowInfoList[index + 1]);

                        // 마지막 내용은 증가하지 않게
                        colIndex++;
                        position.x += this.templateRect.width;
                  }
            }

            /*
            페이지 사이즈 설정하기
            사용안함.
             */

      }, {
            key: "_resetPageSize",
            value: function _resetPageSize() {
                  var sourcePageWidth = this._pageData.page_info.props.setter.width;
                  var sourcePageHeight = this._pageData.page_info.props.setter.height;

                  var pageWidth = sourcePageWidth;
                  var pageHeight = sourcePageHeight;

                  this._pageData.content_info.two_layer.forEach(function (instance) {
                        var setter = instance.props.setter;
                        if (pageWidth < setter.x + setter.width) pageWidth = setter.x + setter.width;

                        if (pageHeight < setter.y + setter.height) pageHeight = setter.y + setter.height;
                  });

                  if (sourcePageWidth != pageWidth) this._pageData.page_info.props.setter.width = pageWidth + 100;

                  if (sourcePageHeight != pageHeight) this._pageData.page_info.props.setter.height = pageHeight + 100;
            }
      }, {
            key: "_copyTemplate",
            value: function _copyTemplate(template, instanceName, position, info0, info1) {
                  var _this = this;

                  var newInstanceInfoMap = new Map();
                  template.forEach(function (instanceInfo) {
                        // 템플릿 내용 복사
                        var newInstanceInfo = $.extend(true, {}, instanceInfo);

                        // 위치 설정
                        newInstanceInfo.props.setter.x = position.x + instanceInfo.props.setter.x;
                        newInstanceInfo.props.setter.y = position.y + instanceInfo.props.setter.y;
                        var name = newInstanceInfo.name;

                        // line인 경우 points 설정
                        if (name.indexOf("tmp_line") != -1) {

                              var points = eval(newInstanceInfo.props.points);

                              points.forEach(function (point) {
                                    point.x = position.x + point.x;
                                    point.y = position.y + point.y;
                              });

                              newInstanceInfo.props.points = JSON.stringify(points);
                        }

                        // 신규 이름 적용을 위해 tmp를 instatnceName으로 변경
                        newInstanceInfo.name = newInstanceInfo.name.replace("tmp", instanceName);
                        newInstanceInfoMap.set(newInstanceInfo.name, newInstanceInfo);

                        _this._pageData.content_info.two_layer.push(newInstanceInfo);
                  });

                  // 0번째 포트 정보 설정

                  //1. 심볼 변경하기
                  var equipmentType = info0[this.ExcelHeader.EQUIPMENT_TYPE];
                  var comVO = this.EquipmentTypeToComponent[equipmentType];
                  var infoInstance = this._getComponentInstanceByName(instanceName + "_symbol_0");
                  infoInstance.props.setter.info.name = comVO.name;
                  infoInstance.props.setter.info.path = comVO.path;

                  newInstanceInfoMap.get(instanceName + "_port_0_0_from").props.setter.text = info0.ports[0].from;
                  newInstanceInfoMap.get(instanceName + "_port_0_0_to").props.setter.text = info0.ports[0].to;
                  newInstanceInfoMap.get(instanceName + "_port_0_1_from").props.setter.text = info0.ports[1].from;
                  newInstanceInfoMap.get(instanceName + "_port_0_1_to").props.setter.text = info0.ports[1].to;

                  //1. 심볼 변경하기
                  equipmentType = info1[this.ExcelHeader.EQUIPMENT_TYPE];
                  comVO = this.EquipmentTypeToComponent[equipmentType];
                  infoInstance = this._getComponentInstanceByName(instanceName + "_symbol_1");
                  infoInstance.props.setter.info.name = comVO.name;
                  infoInstance.props.setter.info.path = comVO.path;

                  // 1번째 포트 정보 설정
                  newInstanceInfoMap.get(instanceName + "_port_1_0_from").props.setter.text = info1.ports[0].from;
                  newInstanceInfoMap.get(instanceName + "_port_1_0_to").props.setter.text = info1.ports[0].to;
                  newInstanceInfoMap.get(instanceName + "_port_1_1_from").props.setter.text = info1.ports[1].from;
                  newInstanceInfoMap.get(instanceName + "_port_1_1_to").props.setter.text = info1.ports[1].to;

                  newInstanceInfoMap.get(instanceName + "_info_0").props.setter.text = info0[this.ExcelHeader.EQUIPMENT_ALIAS] + "\n" + info0[this.ExcelHeader.IP];
                  newInstanceInfoMap.get(instanceName + "_info_1").props.setter.text = info1[this.ExcelHeader.EQUIPMENT_ALIAS] + "\n" + info1[this.ExcelHeader.IP];

                  newInstanceInfoMap.clear();
            }

            /*
            name에 해당하는 컴포넌트 정보 구하기
             */

      }, {
            key: "_getComponentInstanceByName",
            value: function _getComponentInstanceByName(name) {
                  return this._pageData.content_info.two_layer.find(function (comInstanceInfo) {
                        return comInstanceInfo.name == name;
                  });
            }
      }]);

      return sk_network_page2;
}();

sk_network_page2.ExcelHeader = {
      EQUIPMENT_NAME: "장비명",
      EQUIPMENT_ALIAS: "장비Alias",
      IP: "IP",
      EQUIPMENT_TYPE: "장비종류",
      MANUFACTURER: "제조사",
      MODEL_NAME: "모델명",
      ROW: "ROW"

};

sk_network_page2.EquipmentTypeToComponent = {
      "백본 스위치": {
            "name": "network007",
            "path": "/client/packs/2d_pack/components/symbol/network_flat_theme/NetworkFlat007/res/"
      },
      "스위치": {
            "name": "network005",
            "path": "/client/packs/2d_pack/components/symbol/network_flat_theme/NetworkFlat005/res/"
      }
};
