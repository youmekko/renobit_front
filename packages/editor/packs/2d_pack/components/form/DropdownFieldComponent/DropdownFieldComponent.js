class DropdownFieldComponent extends WVDOMComponent {

    constructor() {
        super();
        this._invalidateOptions = false;
        this._invalidateValue = false;
        this._invalidateCaretColor = false;
    }

    //element 생성
    _onCreateElement() {
        // search multiple은 추후 제공
        // this.$dropdown_container = $('<div class="ui fluid selection dropdown">' +
        //                                 '<input name="tags" type="hidden">'+
        //                                 '<div class="default text">Select</div>'+
        //                                 '<i class="dropdown icon"></i>'+
        //                             '</div>');
        this.$container = $('<div class="select-container"></div>')
        this.$dropdown = $('<select></select>');
        // this.$dropdown_container.append(this.$items)

        $(this._element).append(this.$container.append(this.$dropdown));
        this.bindEvent();
    }

    bindEvent() {
        if (this.isEditorMode) { return; }
        var self = this;
        this.$dropdown.on("change", function(e) {
            if (!self.isEditorMode) {
                e.stopPropagation();
                self.value = e.target.value;
                self.dispatchWScriptEvent("change", {
                    value: e.target.value
                })
            }
        })
    }


    _onCreateProperties() {
        if (this.isEditorMode) {
            this.options = [{ text: "option1", value: "option1" }];
        }
        /*이전버전 호환처리*/
        if (this.getGroupPropertyValue("background", "type") == "") {
            this.setGroupPropertyValue("background", "type", "solid");
            let color = this.getGroupPropertyValue("style", "backgroundColor");
            if (color) {
                this.setGroupPropertyValue("background", "color1", color);
            }
        }
    }

    getExtensionProperties() {
        return true;
    }

    updateBackground() {
        let bgData = this.getGroupProperties("background");
        let style = this._styleManager.getBackgroundStyle(bgData, `[id='${this.id}'], [id='${this.id}'] select`);
        $(this._element).find("style").remove();
        $(this._element).append(style);
    }

    ///페이지 로드가 다 끝났을때
    onLoadPage() {

    }

    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {

        this._updateOptions();

        this.$container.css({
            color: this.caret_color
        });
        this._updateStyle();
        this.setFontStyle();
        this.updateBackground();

    }

    _updateOptions() {
        this.$dropdown.empty();
        this.options.forEach((v, k) => {
            if (this.value.toString() === v.value) {
                this.$dropdown.append('<option selected value="' + v.value + '">' + v.text + '</option>')
            } else {
                this.$dropdown.append('<option value="' + v.value + '">' + v.text + '</option>')
            }
        });
    }

    setFontStyle() {
        if (this.font) {
            var fontProps = this.font;
            this.$container.css({
                "fontSize": fontProps.font_size
            });
            this.$dropdown.css({
                "fontFamily": fontProps.font_type,
                "fontSize": fontProps.font_size,
                "color": fontProps.font_color,
                "textAlign": fontProps.text_align
            });
        }
    }

    _updateStyle() {
        let style = this.getGroupProperties("style");
    }

    //properties 변경시
    _onCommitProperties() {
        // validation check 필수! setter를 통해 flag형태로 작업
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this.setFontStyle)
        }

        if (this._invalidateOptions) {
            this._updateOptions();
            this._invalidateOptions = false;
        }

        if (this._invalidateValue) {
            this._updateOptions();
            this._invalidateValue = false;
        }

        if (this._invalidateCaretColor) {
            this.$container.css({
                color: this.caret_color
            });
            this._invalidateCaretColor = false;
        }

        if (this._updatePropertiesMap.has("background")) {
            this.validateCallLater(this.updateBackground);
        }

        this._updateStyle();
    }

    _onDestroy() {
        this.$dropdown.off("change");
        super._onDestroy();
    }

    get font() {
        return this.getGroupProperties("font");
    }

    set options(value) {
        this._invalidateOptions = this._checkUpdateGroupPropertyValue("setter", "options", value);
    }

    get options() {
        return this.getGroupPropertyValue("setter", "options");
    }

    set value(value) {
        this._invalidateValue = this._checkUpdateGroupPropertyValue("setter", "value", value);
    }

    get value() {
        return this.getGroupPropertyValue("setter", "value");
    }

    set caret_color(value) {
        this._invalidateCaretColor = this._checkUpdateGroupPropertyValue("setter", "caret_color", value);
    }

    get caret_color() {
        return this.getGroupPropertyValue("setter", "caret_color");
    }
}

WVPropertyManager.attach_default_component_infos(DropdownFieldComponent, {
    "info": {
        "componentName": "DropdownFieldComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 200,
        "height": 30,
        "options": [],
        "value": "",
        "caret_color": "#333333"
    },

    "label": {
        "label_using": "N",
        "label_text": "Search Field Component"
    },

    "background": {
        "type": "",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#eee",
        "color2": "#000",
        "text": ''
    },

    "style": {
        "border": "1px none #000000",
        "backgroundColor": "rgba(255,255,255,1)",
        "borderRadius": 0
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 11,
        "text_align": "left"
    }
});

DropdownFieldComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "background-gradient",
    owner: "background",
    label: "Background"
}, {
    template: "label"
}, {
    template: "border"
}, {
    template: "font",
    label: "font"
}, {
    label: "Style",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "caret_color",
        type: "color",
        label: "Caret Color",
        show: true,
        writable: true,
        description: "normal caret color"
    }]
}]


WVPropertyManager.add_event(DropdownFieldComponent, {
    name: "change",
    label: "값 선택 이벤트",
    description: "값 선택 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.remove_event(DropdownFieldComponent, "dblclick");
WVPropertyManager.remove_event(DropdownFieldComponent, "click");

WVPropertyManager.add_property_group_info(DropdownFieldComponent, {
    label: "DropdownFieldComponent 고유 속성",
    children: [{
        name: "options",
        type: "Array<any>",
        show: true,
        writable: true,
        defaultValue: "[{text:'선택1', value:'option1' }, {text:'선택2', value:'option2'}]",
        description: "options 속성입니다.\nex) this.options = [{text:'선택1', value:'option1' }, {text:'선택2', value:'option2'}]"
    }, {
        name: "value",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'option1'",
        description: "선택된 value입니다."
    }]
});