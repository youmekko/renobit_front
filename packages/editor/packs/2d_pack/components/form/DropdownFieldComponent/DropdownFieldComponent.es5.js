'use strict';

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DropdownFieldComponent = function(_WVDOMComponent) {
    _inherits(DropdownFieldComponent, _WVDOMComponent);

    function DropdownFieldComponent() {
        _classCallCheck(this, DropdownFieldComponent);

        var _this = _possibleConstructorReturn(this, (DropdownFieldComponent.__proto__ || Object.getPrototypeOf(DropdownFieldComponent)).call(this));

        _this._invalidateOptions = false;
        _this._invalidateValue = false;
        _this._invalidateCaretColor = false;
        return _this;
    }

    //element 생성


    _createClass(DropdownFieldComponent, [{
        key: '_onCreateElement',
        value: function _onCreateElement() {
            // search multiple은 추후 제공
            // this.$dropdown_container = $('<div class="ui fluid selection dropdown">' +
            //                                 '<input name="tags" type="hidden">'+
            //                                 '<div class="default text">Select</div>'+
            //                                 '<i class="dropdown icon"></i>'+
            //                             '</div>');
            this.$container = $('<div class="select-container"></div>');
            this.$dropdown = $('<select></select>');
            // this.$dropdown_container.append(this.$items)
            $(this._element).append(this.$container.append(this.$dropdown));
            this.bindEvent();
        }
    }, {
        key: 'bindEvent',
        value: function bindEvent() {
            if (this.isEditorMode) {
                return;
            }
            var self = this;
            this.$dropdown.on("change", function(e) {
                if (!self.isEditorMode) {
                    e.stopPropagation();
                    self.value = e.target.value;
                    self.dispatchWScriptEvent("change", {
                        value: e.target.value
                    });
                }
            });
        }
    }, {
        key: '_onCreateProperties',
        value: function _onCreateProperties() {
            if (this.isEditorMode) {
                //this.options = [{ text: "option1", value: "option1" }];
            }
            /*이전버전 호환처리*/
            if (this.getGroupPropertyValue("background", "type") == "") {
                this.setGroupPropertyValue("background", "type", "solid");
                var color = this.getGroupPropertyValue("style", "backgroundColor");
                if (color) {
                    this.setGroupPropertyValue("background", "color1", color);
                }
            }
        }
    }, {
        key: 'getExtensionProperties',
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: 'updateBackground',
        value: function updateBackground() {
            var bgData = this.getGroupProperties("background");
            var style = this._styleManager.getBackgroundStyle(bgData, '[id=\'' + this.id + '\'], [id=\'' + this.id + '\'] select');
            $(this._element).find("style").remove();
            $(this._element).append(style);
        }

        ///페이지 로드가 다 끝났을때

    }, {
        key: 'onLoadPage',
        value: function onLoadPage() {}

        ///화면에 붙였을때

    }, {
        key: '_onImmediateUpdateDisplay',
        value: function _onImmediateUpdateDisplay() {
            this._element.style.backgroundColor = "";
            this._updateOptions();

            this.$container.css({
                color: this.caret_color
            });
            this._updateStyle();
            this.setFontStyle();
            this.updateBackground();
        }
    }, {
        key: '_updateOptions',
        value: function _updateOptions() {
            var _this2 = this;

            this.$dropdown.empty();
            this.options.forEach(function(v, k) {
                if (_this2.value.toString() === v.value) {
                    _this2.$dropdown.append('<option selected value="' + v.value + '">' + v.text + '</option>');
                } else {
                    _this2.$dropdown.append('<option value="' + v.value + '">' + v.text + '</option>');
                }
            });
        }
    }, {
        key: 'setFontStyle',
        value: function setFontStyle() {
            if (this.font) {
                var fontProps = this.font;
                this.$container.css({
                    "fontSize": fontProps.font_size
                });
                this.$dropdown.css({
                    "fontFamily": fontProps.font_type,
                    "fontSize": fontProps.font_size,
                    "color": fontProps.font_color,
                    "textAlign": fontProps.text_align
                });
            }
        }
    }, {
        key: '_updateStyle',
        value: function _updateStyle() {
            var style = this.getGroupProperties("style");
        }

        //properties 변경시

    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            // validation check 필수! setter를 통해 flag형태로 작업
            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this.setFontStyle);
            }

            if (this._invalidateOptions) {
                this._updateOptions();
                this._invalidateOptions = false;
            }

            if (this._invalidateValue) {
                this._updateOptions();
                this._invalidateValue = false;
            }

            if (this._invalidateCaretColor) {
                this.$container.css({
                    color: this.caret_color
                });
                this._invalidateCaretColor = false;
            }

            if (this._updatePropertiesMap.has("background")) {
                this.validateCallLater(this.updateBackground);
            }

            this._updateStyle();
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            this.$dropdown.off("change");
            _get(DropdownFieldComponent.prototype.__proto__ || Object.getPrototypeOf(DropdownFieldComponent.prototype), '_onDestroy', this).call(this);
        }
    }, {
        key: 'font',
        get: function get() {
            return this.getGroupProperties("font");
        }
    }, {
        key: 'options',
        set: function set(value) {
            this._invalidateOptions = this._checkUpdateGroupPropertyValue("setter", "options", value);
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "options");
        }
    }, {
        key: 'value',
        set: function set(value) {
            this._invalidateValue = this._checkUpdateGroupPropertyValue("setter", "value", value);
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "value");
        }
    }, {
        key: 'caret_color',
        set: function set(value) {
            this._invalidateCaretColor = this._checkUpdateGroupPropertyValue("setter", "caret_color", value);
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "caret_color");
        }
    }]);

    return DropdownFieldComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DropdownFieldComponent, {
    "info": {
        "componentName": "DropdownFieldComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 200,
        "height": 30,
        "options": [],
        "value": "",
        "caret_color": "#333333"
    },

    "label": {
        "label_using": "N",
        "label_text": "Search Field Component"
    },

    "background": {
        "type": "",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#eee",
        "color2": "#000",
        "text": ''
    },

    "style": {
        "border": "1px none #000000",
        "backgroundColor": "rgba(255,255,255,1)",
        "borderRadius": 0
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 11,
        "text_align": "left"
    }
});

DropdownFieldComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "background-gradient",
    owner: "background",
    label: "Background"
}, {
    template: "label"
}, {
    template: "border"
}, {
    template: "font",
    label: "font"
}, {
    label: "Style",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "caret_color",
        type: "color",
        label: "Caret Color",
        show: true,
        writable: true,
        description: "normal caret color"
    }]
}];

WVPropertyManager.add_event(DropdownFieldComponent, {
    name: "change",
    label: "값 선택 이벤트",
    description: "값 선택 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.remove_event(DropdownFieldComponent, "dblclick");
WVPropertyManager.remove_event(DropdownFieldComponent, "click");

WVPropertyManager.add_property_group_info(DropdownFieldComponent, {
    label: "DropdownFieldComponent 고유 속성",
    children: [{
        name: "options",
        type: "Array<any>",
        show: true,
        writable: true,
        defaultValue: "[{text:'선택1', value:'option1' }, {text:'선택2', value:'option2'}]",
        description: "options 속성입니다.\nex) this.options = [{text:'선택1', value:'option1' }, {text:'선택2', value:'option2'}]"
    }, {
        name: "value",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'option1'",
        description: "선택된 value입니다."
    }]
});