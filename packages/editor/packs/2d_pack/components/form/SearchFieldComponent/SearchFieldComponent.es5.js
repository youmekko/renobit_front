"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var SearchFieldComponent =
/*#__PURE__*/
function (_WVDOMComponent) {
  _inherits(SearchFieldComponent, _WVDOMComponent);

  function SearchFieldComponent() {
    var _this;

    _classCallCheck(this, SearchFieldComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(SearchFieldComponent).call(this));
    _this._invalidateValue = false;
    _this._invalidatePlaceHolder = false;
    return _this;
  } //element 생성


  _createClass(SearchFieldComponent, [{
    key: "_onCreateElement",
    value: function _onCreateElement() {
      this.$searchField = $("<input type='search'/>");
      this.$searchField.css({
        "width": "100%",
        "height": "100%",
        "background": "none",
        "border": "none",
        "outline": "none",
        "padding": "5px 25px 5px 32px"
      });
      this.$searchIcon = $('<i class="fa fa-search"></i>');
      this.$searchIcon.css({
        "position": "absolute",
        "top": "calc(50% - 7.5px)",
        "left": "10px"
      });
      this.$cancelBtn = $('<i class="fa fa-remove"></i>');
      this.$cancelBtn.css({
        "position": "absolute",
        "top": "calc(50% - 7.5px)",
        "left": "calc(100% - 15px)",
        "cursor": "pointer"
      });
      this.$searchField.attr("placeholder", this.setter.placeHolder);
      $(this._element).append(this.$searchIcon).append(this.$searchField).append(this.$cancelBtn);
      this.bindEvent();
      this.$cancelBtn.hide();
    }
  }, {
    key: "bindEvent",
    value: function bindEvent() {
      if (this.isEditorMode) {
        return;
      }

      var self = this;
      this.$searchField.on("keyup", function (e) {
        self.value = this.value;

        if (e.keyCode === 13) {
          self._enterEvent();
        }
      });
      this.$searchField.on("change", function (e) {
        e.stopPropagation();
      });
      this.$cancelBtn.on("click", function (e) {
        self.value = "";
        self.$searchField.val("");
      });
    }
  }, {
    key: "_onCreateProperties",
    value: function _onCreateProperties() {
      /*이전버전 호환처리*/
      if (this.getGroupPropertyValue("background", "type") == "") {
        this.setGroupPropertyValue("background", "type", "solid");
        var color = this.getGroupPropertyValue("style", "backgroundColor");

        if (color) {
          this.setGroupPropertyValue("background", "color1", color);
        }
      }
    } ///페이지 로드가 다 끝났을때

  }, {
    key: "onLoadPage",
    value: function onLoadPage() {} ///화면에 붙였을때

  }, {
    key: "_onImmediateUpdateDisplay",
    value: function _onImmediateUpdateDisplay() {
      this._validateFontStyleProperty();

      this.updateBackground();
    }
  }, {
    key: "updateBackground",
    value: function updateBackground() {
      var bgData = this.getGroupProperties("background");

      var style = this._styleManager.getBackgroundStyle(bgData, "[id='".concat(this.id, "']"));

      $(this._element).find("style").remove();
      $(this._element).append(style);
    } //properties 변경시

  }, {
    key: "_onCommitProperties",
    value: function _onCommitProperties() {
      if (this._updatePropertiesMap.has("font")) {
        this.validateCallLater(this._validateFontStyleProperty);
      }

      if (this._invalidateValue) {
        this.validateCallLater(this._updateValue);

        if (this.setter.value !== "") {
          this.$cancelBtn.show();
        } else {
          this.$cancelBtn.hide();
        }

        console.log('this.$searchField', this.$searchField);
        this.$searchField[0].value = this.getGroupPropertyValue("setter", "value");
        this._invalidateValue = false;
      }

      if (this._invalidatePlaceHolder) {
        this.$searchField.attr("placeholder", this.setter.placeHolder);
        this._invalidatePlaceHolder = false;
      }

      if (this._updatePropertiesMap.has("background")) {
        this.validateCallLater(this.updateBackground);
      }
    }
  }, {
    key: "_updateValue",
    value: function _updateValue() {
      if (!this.isEditorMode) {
        this.dispatchWScriptEvent("change", {
          value: this.getGroupPropertyValue("setter", "value")
        });
      }
    }
  }, {
    key: "_validateFontStyleProperty",
    value: function _validateFontStyleProperty() {
      if (this.font) {
        var fontProps = this.font;
        this.$searchField.css({
          "fontFamily": fontProps.font_type,
          "fontSize": fontProps.font_size,
          "fontWeight": fontProps.font_weight,
          "color": fontProps.font_color,
          "textAlign": fontProps.text_align,
          "paddingLeft": parseInt(fontProps.font_size) + 15 + "px"
        });
        this.$searchIcon.css({
          "color": fontProps.font_color,
          "fontSize": fontProps.font_size,
          "top": "calc(50% - " + parseInt(fontProps.font_size) / 2 + "px)"
        });
        this.$cancelBtn.css({
          "color": fontProps.font_color,
          "fontSize": fontProps.font_size,
          "top": "calc(50% - " + parseInt(fontProps.font_size) / 2 + "px)",
          "left": "calc(100% - " + (parseInt(fontProps.font_size) + 7.5) + "px)"
        });
      }
    }
  }, {
    key: "_enterEvent",
    value: function _enterEvent() {
      if (!this.isEditorMode) {
        this.dispatchWScriptEvent("enter", {
          value: this.getGroupPropertyValue("setter", "value")
        });
      }
    }
  }, {
    key: "_onDestroy",
    value: function _onDestroy() {
      this.$searchField.off("keyup");
      this.$searchField.off("change");
      this.$cancelBtn.off("click");

      _get(_getPrototypeOf(SearchFieldComponent.prototype), "_onDestroy", this).call(this);
    }
  }, {
    key: "value",
    set: function set(value) {
      this._invalidateValue = this._checkUpdateGroupPropertyValue("setter", "value", value);
    },
    get: function get() {
      //그룹으로 가져올때 this.getGroupProperties("setter");
      return this.getGroupPropertyValue("setter", "value");
    }
  }, {
    key: "placeHolder",
    set: function set(value) {
      this._invalidatePlaceHolder = this._checkUpdateGroupPropertyValue("setter", "placeHolder", value);
    }
  }, {
    key: "font",
    get: function get() {
      return this.getGroupProperties("font");
    }
  }]);

  return SearchFieldComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(SearchFieldComponent, {
  "info": {
    "componentName": "SearchFieldComponent",
    "version": "1.0.0"
  },
  ///외부에서 접근하는 property는 이곳에 선언
  "setter": {
    "width": 200,
    "height": 30,
    "value": "",
    "placeHolder": ""
  },
  "background": {
    "type": "",
    "direction": "left",
    //top, left, diagonal1, diagonal2, radial
    "color1": "#eee",
    "color2": "#000",
    "text": ''
  },
  "label": {
    "label_using": "N",
    "label_text": "Search Field Component"
  },
  "style": {
    "border": "1px none #000000",
    "backgroundColor": "rgba(255,255,255,1)",
    "borderRadius": 0
  },
  "font": {
    "font_type": "inherit",
    "font_color": "#333333",
    "font_size": 14,
    "font_weight": "normal",
    "text_align": "left"
  }
});
SearchFieldComponent.property_panel_info = [{
  template: "primary"
}, {
  template: "pos-size-2d"
}, {
  template: "background-gradient",
  owner: "background",
  label: "Background"
}, {
  template: "label"
}, {
  template: "border"
}, {
  template: "font",
  label: "font"
}, {
  label: "Placeholder",
  template: "vertical",
  children: [{
    owner: "setter",
    name: "placeHolder",
    type: "string",
    label: "Text",
    show: true,
    writable: true,
    description: "Place Holder"
  }]
}];
WVPropertyManager.add_event(SearchFieldComponent, {
  name: "change",
  label: "값 체인지 이벤트",
  description: "값 체인지 이벤트 입니다.",
  properties: [{
    name: "value",
    type: "string",
    default: "",
    description: "새로운 값입니다."
  }]
});
WVPropertyManager.add_event(SearchFieldComponent, {
  name: "enter",
  label: "엔터키 이벤트",
  description: "엔터키 이벤트 입니다.",
  properties: [{
    name: "value",
    type: "string",
    default: "",
    description: "새로운 값입니다."
  }]
});
WVPropertyManager.add_property_group_info(SearchFieldComponent, {
  label: "SearchFieldComponent 고유 속성",
  children: [{
    name: "value",
    type: "string",
    show: true,
    writable: true,
    defaultValue: "'text'",
    description: "value입니다."
  }, {
    name: "placeHolder",
    type: "string",
    show: true,
    writable: true,
    defaultValue: "'값을 입력하세요.'",
    description: "placeholder 문자열 입니다."
  }]
});