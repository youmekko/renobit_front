class InputFieldComponent extends WVDOMComponent {

    constructor() {
        super();
        this._invalidateValue = false;
        this._invalidatePlaceHolder = false;
    }

    _onCreateProperties() {
        /*이전버전 호환처리*/
        if (this.getGroupPropertyValue("background", "type") == "") {
            this.setGroupPropertyValue("background", "type", "solid");
            let color = this.getGroupPropertyValue("style", "backgroundColor");
            if (color) {
                this.setGroupPropertyValue("background", "color1", color);
            }
        }
    }

    //element 생성
    _onCreateElement() {
        this.$inputField = $("<input/>");
        this.$inputField.css({
            "width": "100%",
            "height": "100%",
            "background": "none",
            "border": "none",
            "outline": "none",
            "padding": "5px"
        });

        this.$inputField.attr("placeholder", this.setter.placeHolder);
        $(this._element).append(this.$inputField);
        this.bindEvent();
    }


    bindEvent() {
        if (this.isEditorMode) { return; }
        var self = this;
        this.$inputField.on("keyup", function(e) {
            self.value = this.value;
        })
    }

    ///페이지 로드가 다 끝났을때
    onLoadPage() {

    }

    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {
        this._validateFontStyleProperty();
        this.updateBackground();
    }

    //properties 변경시
    _onCommitProperties() {
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty)
        }
        if (this._invalidateValue) {
            this.validateCallLater(this._updateValue);
            this.$inputField[0].value = this.getGroupPropertyValue("setter", "value");
            this._invalidateValue = false;
        }
        if (this._invalidatePlaceHolder) {
            this.$inputField.attr("placeholder", this.setter.placeHolder);
            this._invalidatePlaceHolder = false;
        }

        if (this._updatePropertiesMap.has("background")) {
            this.validateCallLater(this.updateBackground);
        }
    }

    updateBackground() {
        let bgData = this.getGroupProperties("background");
        let style = this._styleManager.getBackgroundStyle(bgData, `[id='${this.id}']`);
        $(this._element).find("style").remove();
        $(this._element).append(style);
    }

    _validateFontStyleProperty() {
        if (this.font) {
            var fontProps = this.font;
            this.$inputField.css({
                "fontFamily": fontProps.font_type,
                "fontSize": fontProps.font_size,
                "fontWeight": fontProps.font_weight,
                "color": fontProps.font_color,
                "textAlign": fontProps.text_align
            });
        }
    }

    _updateValue() {
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: this.getGroupPropertyValue("setter", "value")
            })
        }
    }

    _enterEvent() {
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("enter", {
                value: this.getGroupPropertyValue("setter", "value")
            })
        }
    }

    _onDestroy() {
        //삭제 처리
        this.$inputField.off("keyup");
        super._onDestroy();
    }

    set value(value) {
        this._invalidateValue = this._checkUpdateGroupPropertyValue("setter", "value", value);
    }

    get value() {
        //그룹으로 가져올때 this.getGroupProperties("setter");
        return this.getGroupPropertyValue("setter", "value");
    }

    set placeHolder(value) {
        this._invalidatePlaceHolder = this._checkUpdateGroupPropertyValue("setter", "placeHolder", value);
    }

    get font() {
        return this.getGroupProperties("font");
    }
}

WVPropertyManager.attach_default_component_infos(InputFieldComponent, {
    "info": {
        "componentName": "InputFieldComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 200,
        "height": 30,
        "value": "",
        "placeHolder": ""
    },

    "label": {
        "label_using": "N",
        "label_text": "Search Field Component"
    },

    "background": {
        "type": "",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#eee",
        "color2": "#000",
        "text": ''
    },

    "style": {
        "border": "1px none #000000",
        "backgroundColor": "rgba(255,255,255,1)",
        "borderRadius": 0
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal",
        "text_align": "left"
    }
});

InputFieldComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "background-gradient",
    owner: "background",
    label: "Background"
}, {
    template: "label"
}, {
    template: "border"
}, {
    template: "font",
    label: "font"
}, {
    label: "Placeholder",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "placeHolder",
        type: "string",
        label: "Text",
        show: true,
        writable: true,
        description: "Place Holder",
    }]
}]


WVPropertyManager.add_event(InputFieldComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(InputFieldComponent, {
    label: "InputFieldComponent 고유 속성",
    children: [{
        name: "value",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'text'",
        description: "value입니다."
    }, {
        name: "placeHolder",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'값을 입력하세요.'",
        description: "placeholder 문자열 입니다."
    }]
});
