class EmptyHighChartComponent extends WVDOMComponent {
    constructor() {
        super();
        this._isAdded = false;
        this._chart = null;
        this._timer = null;
        this._config = null;
        this.chartId = "";
        this._dataProvider = [];
        this._xAxisField = "";
        this._invaldatePreivewProperty = false;
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateProperties() {
        this.chartId = "id-" + this.id;
        let config = this.getParseConfigData();
        if (config) {
            this.config = config;
        }
    }

    _onCreateElement() {
        $(this._element).append('<div style="width:100%;height:100%;" class="chart-area" id="' + this.chartId + '"></div>');
    }

    _onImmediateUpdateDisplay() {
        this._isAdded = true;
        this.setPreviewMode();
    }

    onLoadPage() {}

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this._updateChartConfig);
        }

        if (this._updatePropertiesMap.has("preview")) {
            this.validateCallLater(this.setPreviewMode);
        }
        if (this._chart) {
            this.validateCallLater(this.setSize);
        }
    }

    setSize() {
        if (!this._chart) return;
        if (!this._timer) {
            this._timer = setTimeout(() => { this._timer = null;this._chart.setSize(this._properties.setter.width, this._properties.setter.height) },100) 
        } 
  }

    setPreviewMode() {
        if (this.isEditorMode && !this.getGroupPropertyValue("preview", "use")) {
            $(this._element).removeClass("no-data").addClass("editor");
            if (this._chart) {
                this._chart.destroy();
                this._chart = null;
            }
        } else {
            $(this._element).removeClass("editor");
            this.render();
        }
    }


    _updateChartConfig() {
        let config = this.getParseConfigData();
        if (config) {
            this.config = config;
            this.render();
        }
    }


    _onDestroy() {
        if(this._chart)
            this._chart.destroy();
        super._onDestroy();
    }

    makeChart() {
        if (!this._isAdded || $(this._element).hasClass("editor")) {
            return;
        }
        if (this.config) {
            this._chart = Highcharts.chart(this.chartId, this.config);
        }
    }

    render() {
        if (!this._chart) {
            this.makeChart();
        }
    }

    getParseConfigData() {
        let data = null;
        try {
            let configStr = this.getGroupPropertyValue("extension", "configStr");
            data = window.ScriptUtil.eval(configStr);
            return data;
        } catch (error) {
            return null;
        }
    }

    validateData() {
        this._chart.update(this._config);
    }

    set config(config) {
        this._config = config;
    }

    get config() {
        return this._config;
    }

    get chart() {
        return this._chart;
    }

    set dataProvider(data) {
        this._dataProvider = data;
        if(this._dataProvider && this._config) {
            if(this._xAxisField !== "") {
                var categories = this._dataProvider.map(function(b) { return b[this._xAxisField] }.bind(this))
                if(!this._config.xAxis) {
                    this._config["xAxis"] = {
                        categories : categories
                    }
                } else {
                    this._config.xAxis.categories = categories
                }
            }
            this._config.series.forEach(function(d) {
                d.data = this._dataProvider.map(function(b) { return b[d.name] })
            }.bind(this));
        }
        this.validateData();
    }

    get dataProvider() {
        return this._dataProvider;
    }

    set xAxisField(data) {
        this._xAxisField = data;
    }

    get xAxisField() {
        return this._xAxisField;
    }
}

// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(EmptyHighChartComponent, {
    "info": {
        "componentName": "EmptyHighChartComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "autoExecute": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Empty Chart"
    },

    "extension": {
        "configStr": '',
    },

    "preview": {
        "use": false
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
EmptyHighChartComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
      template: "label"
}, {
    label: "Preview",
    template: "vertical",
    children: [{
        owner: "preview",
        name: "use",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "미리보기 설정",
        options: {
            label: "사용"
        }
    }],
}];

WVPropertyManager.add_property_group_info(EmptyHighChartComponent, {
    label: "EmptyHighChartComponent 고유 속성",
    children: [{
        name: "config",
        type: "Object",
        show: true,
        writable: true,
        description: "Chart 설정 정보 입니다."
    }, {
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        description: "Chart를 구성할 데이터(row data)입니다."
    }, {
        name: "chart",
        type: "HighCharts Instance",
        show: true,
        writable: false,
        description: "생성된 chart(관련 설정은 highcharts 참조)객체 입니다. 데이터가 없을경우 null입니다."
    }]
});


WVPropertyManager.remove_property_group_info(EmptyHighChartComponent, "label");
