"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EmptyHighChartComponent = function (_WVDOMComponent) {
    _inherits(EmptyHighChartComponent, _WVDOMComponent);

    function EmptyHighChartComponent() {
        _classCallCheck(this, EmptyHighChartComponent);

        var _this = _possibleConstructorReturn(this, (EmptyHighChartComponent.__proto__ || Object.getPrototypeOf(EmptyHighChartComponent)).call(this));

        _this._isAdded = false;
        _this._chart = null;
        _this._timer = null;
        _this._config = null;
        _this.chartId = "";
        _this._dataProvider = [];
        _this._xAxisField = "";
        _this._invaldatePreivewProperty = false;
        return _this;
    }

    _createClass(EmptyHighChartComponent, [{
        key: "getExtensionProperties",
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: "_onCreateProperties",
        value: function _onCreateProperties() {
            this.chartId = "id-" + this.id;
            var config = this.getParseConfigData();
            if (config) {
                this.config = config;
            }
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            $(this._element).append('<div style="width:100%;height:100%;" class="chart-area" id="' + this.chartId + '"></div>');
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this._isAdded = true;
            this.setPreviewMode();
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension")) {
                this.validateCallLater(this._updateChartConfig);
            }

            if (this._updatePropertiesMap.has("preview")) {
                this.validateCallLater(this.setPreviewMode);
            }
            if (this._chart) {
                this.validateCallLater(this.setSize);
            }
        }
    }, {
        key: "setSize",
        value: function setSize() {
            var _this2 = this;

            if (!this._chart) return;
            if (!this._timer) {
                this._timer = setTimeout(function () {
                    _this2._timer = null;
                    _this2._chart.setSize(_this2._properties.setter.width, _this2._properties.setter.height);
                }, 100);
            }
        }
    }, {
        key: "setPreviewMode",
        value: function setPreviewMode() {
            if (this.isEditorMode && !this.getGroupPropertyValue("preview", "use")) {
                $(this._element).removeClass("no-data").addClass("editor");
                if (this._chart) {
                    this._chart.destroy();
                    this._chart = null;
                }
            } else {
                $(this._element).removeClass("editor");
                this.render();
            }
        }
    }, {
        key: "_updateChartConfig",
        value: function _updateChartConfig() {
            var config = this.getParseConfigData();
            if (config) {
                this.config = config;
                this.render();
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            if (this._chart) this._chart.destroy();
            _get(EmptyHighChartComponent.prototype.__proto__ || Object.getPrototypeOf(EmptyHighChartComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "makeChart",
        value: function makeChart() {
            if (!this._isAdded || $(this._element).hasClass("editor")) {
                return;
            }
            if (this.config) {
                this._chart = Highcharts.chart(this.chartId, this.config);
            }
        }
    }, {
        key: "render",
        value: function render() {
            if (!this._chart) {
                this.makeChart();
            }
        }
    }, {
        key: "getParseConfigData",
        value: function getParseConfigData() {
            var data = null;
            try {
                var configStr = this.getGroupPropertyValue("extension", "configStr");
                data = window.ScriptUtil.eval(configStr);
                return data;
            } catch (error) {
                return null;
            }
        }
    }, {
        key: "validateData",
        value: function validateData() {
            this._chart.update(this._config);
        }
    }, {
        key: "config",
        set: function set(config) {
            this._config = config;
        },
        get: function get() {
            return this._config;
        }
    }, {
        key: "chart",
        get: function get() {
            return this._chart;
        }
    }, {
        key: "dataProvider",
        set: function set(data) {
            this._dataProvider = data;
            if (this._dataProvider && this._config) {
                if (this._xAxisField !== "") {
                    var categories = this._dataProvider.map(function (b) {
                        return b[this._xAxisField];
                    }.bind(this));
                    if (!this._config.xAxis) {
                        this._config["xAxis"] = {
                            categories: categories
                        };
                    } else {
                        this._config.xAxis.categories = categories;
                    }
                }
                this._config.series.forEach(function (d) {
                    d.data = this._dataProvider.map(function (b) {
                        return b[d.name];
                    });
                }.bind(this));
            }
            this.validateData();
        },
        get: function get() {
            return this._dataProvider;
        }
    }, {
        key: "xAxisField",
        set: function set(data) {
            this._xAxisField = data;
        },
        get: function get() {
            return this._xAxisField;
        }
    }]);

    return EmptyHighChartComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(EmptyHighChartComponent, {
    "info": {
        "componentName": "EmptyHighChartComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "autoExecute": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Empty Chart"
    },

    "extension": {
        "configStr": ''
    },

    "preview": {
        "use": false
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
EmptyHighChartComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Preview",
    template: "vertical",
    children: [{
        owner: "preview",
        name: "use",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "미리보기 설정",
        options: {
            label: "사용"
        }
    }]
}];

WVPropertyManager.add_property_group_info(EmptyHighChartComponent, {
    label: "EmptyHighChartComponent 고유 속성",
    children: [{
        name: "config",
        type: "Object",
        show: true,
        writable: true,
        description: "Chart 설정 정보 입니다."
    }, {
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        description: "Chart를 구성할 데이터(row data)입니다."
    }, {
        name: "chart",
        type: "HighCharts Instance",
        show: true,
        writable: false,
        description: "생성된 chart(관련 설정은 highcharts 참조)객체 입니다. 데이터가 없을경우 null입니다."
    }]
});

WVPropertyManager.remove_property_group_info(EmptyHighChartComponent, "label");