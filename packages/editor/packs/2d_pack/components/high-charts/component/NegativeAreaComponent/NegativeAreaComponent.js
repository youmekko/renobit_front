class NegativeAreaComponent extends ChartBasicComponent {
    constructor() {
        super();
    }
}

WVPropertyManager.attach_default_component_infos(NegativeAreaComponent, {
    "info": {
        "componentName": "NegativeAreaComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "autoExecute": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Negative Area"
    },

    "extension": {
        "configStr":'',
        "datasetId":''
    },

    "preview": {
        "use": true
    }
});


NegativeAreaComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
      template: "label"
}, {
    label: "Preview",
    template: "vertical",
    children: [{
        owner: "preview",
        name: "use",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "미리보기 설정",
        options: {
            label: "사용"
        }
    }],
}];

WVPropertyManager.add_property_group_info(NegativeAreaComponent, {
    label: "NegativeAreaComponent 고유 속성",
    children: [{
        name: "config",
        type: "Object",
        show: true,
        writable: true,
        description: "Chart 설정 정보 입니다."
    }, {
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        description: "Chart를 구성할 데이터(row data)입니다."
    }, {
        name: "chart",
        type: "HighCharts Instance",
        show: true,
        writable: false,
        description: "생성된 chart(관련 설정은 highcharts 참조)객체 입니다. 데이터가 없을경우 null입니다."
    }]
});


WVPropertyManager.remove_property_group_info(NegativeAreaComponent, "label");
