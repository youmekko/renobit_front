"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SplineAreaComponent = function (_ChartBasicComponent) {
    _inherits(SplineAreaComponent, _ChartBasicComponent);

    function SplineAreaComponent() {
        _classCallCheck(this, SplineAreaComponent);

        return _possibleConstructorReturn(this, (SplineAreaComponent.__proto__ || Object.getPrototypeOf(SplineAreaComponent)).call(this));
    }
    return SplineAreaComponent;
}(ChartBasicComponent);


WVPropertyManager.attach_default_component_infos(SplineAreaComponent, {
  "info": {
    "componentName": "SplineAreaComponent",
    "version": "1.0.0"
  },
  "setter": {
    "width": 591.14,
    "height": 412.61,
    "autoExecute": true
  },
  "label": {
    "label_using": "N",
    "label_text": "Spline Area"
  },
  "extension": {
    "configStr":'',
    "datasetId":''
},
  "preview": {
    "use": true
  }
}); 

SplineAreaComponent.property_panel_info = [{
  template: "primary"
}, {
  template: "pos-size-2d"
}, {
  template: "label"
}, {
  label: "Preview",
  template: "vertical",
  children: [{
    owner: "preview",
    name: "use",
    type: "checkbox",
    label: "Use",
    show: true,
    writable: true,
    description: "미리보기 설정",
    options: {
      label: "사용"
    }
  }]
}];
WVPropertyManager.add_property_group_info(SplineAreaComponent, {
  label: "SplineAreaComponent 고유 속성",
  children: [{
    name: "config",
    type: "Object",
    show: true,
    writable: true,
    description: "Chart 설정 정보 입니다."
  }, {
    name: "dataProvider",
    type: "Array<any>",
    show: true,
    writable: true,
    description: "Chart를 구성할 데이터(row data)입니다."
  }, {
    name: "chart",
    type: "HighCharts Instance",
    show: true,
    writable: false,
    description: "생성된 chart(관련 설정은 highcharts 참조)객체 입니다. 데이터가 없을경우 null입니다."
  }]
});
WVPropertyManager.remove_property_group_info(SplineAreaComponent, "label");
