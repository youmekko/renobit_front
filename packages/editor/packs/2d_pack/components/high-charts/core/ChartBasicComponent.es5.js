'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

// basicArea,groupCol,percentArea,splineSymbol, stackedArea -> default json function   -> controlling ineach js file
// each vue file making
Vue.component('popupComponent', {
    template: '<modal\n    class="w-modal modal-tabs"\n    id="empty-chart"\n    name="empty-chart"\n    @opened="modalOpened"\n    @before-close="beforeModalClosed"\n        :click-to-close="false"\n    :draggable="false"\n    :resizable="true"\n    :width="1020"\n    :height="670"\n    :min-width="700"\n    :min-height="370"\n    >\n    <div class="modal-header"><h4>High Chart Settings</h4><a class="close-modal-btn" role="button" @click="_onHidden"><i class="el-icon-error"></i></a></div>\n        <div class="modal-body">\n               <div class="wrap" v-show="stepLevel==0">\n                      <p>Highcharts Demo Site : <a href="https://www.highcharts.com/demo/" target="_blank">https://www.highcharts.com/demo/</a></p>\n                     <div class="cont-wrap" >\n                <div class="cont">\n                    <h5>Preview</h5>\n                    <div class="preview">\n                        <div id="saved-chart-preview" style="width: 100%; height: 80%;" ></div>\n                    </div>\n                </div>\n                <div class="cont">\n                    <h5>&lt;/&gt; Code</h5>\n                    <div class="code">\n                                      <textarea\n                                                  :placeholder="placeHolder"\n                                                  v-model="defaultConfigStr" class="txt" @change="changeConfigStr">\n                                      </textarea>\n                                </div>\n                </div>\n            </div>\n            <div class="btn-wrap">\n                          <el-button class="bottom-btn" type="primary" size="small" @click="_onHidden">{{$t(\'common.cancel\')}}</el-button>\n                          <el-button class="bottom-btn" size="small" @click="nextStep1" v-if="supportable">{{$t(\'common.next\')}}</el-button> \n            </div>\n              </div>\n              <div class="wrap" v-show="stepLevel==1" style="overflow:auto">\n                    <div class="cont-wrap">\n                    <div class="cont dataset">\n                          <dataset-preview-main-component ref="datasetPreviewComp"></dataset-preview-main-component>\n                    </div>\n                    </div>\n                    <div class="btn-wrap">\n                          <el-button class="bottom-btn" type="primary" size="small" @click="prevStep">{{$t(\'common.prev\')}}</el-button>\n                          <el-button class="bottom-btn" size="small" @click="nextStep2" v-if="supportable">{{$t(\'common.next\')}}</el-button> \n            </div>\n              </div>\n              <div class="wrap" v-show="stepLevel==2">\n                    <div class="cont-wrap">\n                    <div class="cont">\n                          <transfer-main-component ref="transferComp" @change="changeColumnData" ></transfer-main-component>\n                    </div>\n                    <div class="cont">\n                    <h5>Preview</h5>\n                    <div class="preview">\n                        <div id="second-chart-preview" style="width: 100%; height: 80%;" ></div>\n                    </div>\n                    </div>\n                    </div>\n                    <div class="btn-wrap">\n                          <el-button class="bottom-btn" type="primary" size="small" @click="prevStep">{{$t(\'common.prev\')}}</el-button>\n                          <el-button class="bottom-btn" size="small" @click="nextStep3" v-if="supportable">{{$t(\'common.next\')}}</el-button> \n            </div>\n              </div>\n              <div class="wrap" v-show="stepLevel==3">\n                     <div class="cont-wrap" >\n                <div class="cont">\n                    <h5>Preview</h5>\n                    <div class="preview">\n                        <div id="new-chart-preview" style="width: 100%; height: 80%;" ></div>\n                    </div>\n                </div>\n                <div class="cont">\n                    <h5>&lt;/&gt; Code</h5>\n                    <div class="code">\n                                      <textarea\n                                                  :placeholder="placeHolder"\n                                                  v-model="configStr" class="txt" @change="changeConfigStr">\n                                      </textarea>\n                                </div>\n                </div>\n            </div>\n            <div class="btn-wrap">\n                          <el-button class="bottom-btn" type="primary" size="small" @click="prevStep">{{$t(\'common.prev\')}}</el-button>\n                          <el-button class="bottom-btn" size="small" @click="_onOk" v-if="supportable">{{$t(\'common.save\')}}</el-button> \n            </div>\n              </div>\n    </div>\n  </modal>',

    props: ['componentInstance', 'isCategory', 'isSemipie', 'needStartPoint'],
    data: function data() {
        return {
            placeHolder: '',
            configStr: '',
            chart: null,
            timer: null,
            supportable: true,
            tranferData: {
                mainData: {
                    title: "Data Column List",
                    data: []
                },

                targetListData: [{
                    title: '',
                    data: []
                }, {
                    title: "Series Data",
                    data: []
                }]
            },
            currentDataset: null,
            datasetResultData: null,
            datasetID: null,
            isResetDataset: false,
            stepLevel: 0,
            series: [],
            categories: [],
            parsedConfigStr: null,
            defaultConfigStr: null

        };
    },

    methods: {
        prevStep: function prevStep() {
            var _this2 = this;

            // refactoring
            this.stepLevel -= 1;
            if (this.stepLevel == 0) {
                setTimeout(function () {
                    _this2.render();
                }, 100);
            }
            if (this.stepLevel == 2) {
                this.parsedConfigStr = window.ScriptUtil.eval(this.configStr);
                this.categories = [];
                this.series = [];
                setTimeout(function () {
                    _this2.render();
                }, 100);
            }
        },
        nextStep1: function nextStep1() {
            this.stepLevel = 1;
        },
        nextStep2: function nextStep2() {
            var _this3 = this;

            this.tranferData.targetListData[0].title = this.isCategory ? 'Category' : 'No Category';

            this.currentDataset = this.$refs.datasetPreviewComp.getDataset();
            if (this.datasetID && this.currentDataset && this.datasetID == this.currentDataset.dataset_id) {
                var datasetPreviewData = this.$refs.datasetPreviewComp.getPreviewData();
                if (datasetPreviewData == null || datasetPreviewData.length == 0 || datasetPreviewData.keys.join('') == this.datasetResultData.keys.join('')) {
                    this.stepLevel = 2;
                } else {
                    this._initTransferData();
                    this.stepLevel = 2;
                }
            } else {
                if (this.$refs.datasetPreviewComp.validationPreviewData()) {
                    this.datasetResultData = this.$refs.datasetPreviewComp.getPreviewData();
                    this.datasetID = this.currentDataset.dataset_id;
                    this._initTransferData();
                    this.stepLevel = 2;
                } else {
                    return;
                }
            }
            setTimeout(function () {
                _this3.render();
            }, 100);
        },
        nextStep3: function nextStep3() {
            var _this4 = this;

            if (!this.checkData()) {
                var locale_msg = Vue.$i18n.messages.wv;
                Vue.$message(locale_msg.common.setData);
                return;
            }
            this.stepLevel = 3;
            this.configStr = JSON.stringify(this.parsedConfigStr, null, 4);
            setTimeout(function () {
                _this4.render();
            }, 100);
        },
        insertData: function insertData(targetList, _this, isCategory, isSemipie, needStartPoint) {
            targetList.data.forEach(function (eachCol) {
                var eachObj;
                eachObj = isCategory ? null : { name: eachCol, data: [] };
                if (isSemipie) {
                    eachObj.type = 'pie';eachObj.innerSize = '50%';
                }
                if (needStartPoint) {
                    eachObj.pointStart = 1;
                }
                _this.datasetResultData.data.forEach(function (eachData) {
                    if (eachData[eachCol] !== undefined) {
                        isCategory ? _this.categories.push(eachData[eachCol]) : eachObj.data.push(eachData[eachCol]);
                    }
                });
                if (!isCategory) _this.series.push(eachObj);
            });
        },
        changeColumnData: function changeColumnData(data) {
            // refactoring
            console.log('changeColumnData');
            var self = this;
            this.parsedConfigStr = window.ScriptUtil.eval(this.configStr);
            this.tranferData = data;
            this.isResetDataset = true;

            if (this.isCategory) {
                this.categories = [];
                var categoryList = this.tranferData.targetListData[0];
                this.insertData(categoryList, self, self.isCategory);
                var ArrOrNot = Array.isArray(this.parsedConfigStr.xAxis);
                ArrOrNot ? this.parsedConfigStr.xAxis[0].categories = [].concat(_toConsumableArray(self.categories)) : this.parsedConfigStr.xAxis.categories = [].concat(_toConsumableArray(self.categories));
            }

            this.series = [];
            var seriesDataList = this.tranferData.targetListData[1];
            this.insertData(seriesDataList, self, false, self.isSemipie, self.needStartPoint);
            this.parsedConfigStr.series = [].concat(_toConsumableArray(self.series));

            this.configStr = JSON.stringify(this.parsedConfigStr, null, 4);
            this.render();
        },
        _initTransferData: function _initTransferData() {
            this.datasetResultData = this.$refs.datasetPreviewComp.getPreviewData();
            this.tranferData.mainData.data = this.datasetResultData.keys;
            this.tranferData.targetListData[0].data = [];
            this.tranferData.targetListData[1].data = [];
            this.$refs.transferComp.setData(this.tranferData);
        },
        checkData: function checkData() {
            var targetListData = this.tranferData.targetListData;
            return targetListData[1].data.length;
        },

        paintChart: function paintChart(whereTo) {
            if (this.stepLevel == 0) return this.chart = Highcharts.chart(whereTo, window.ScriptUtil.eval(this.defaultConfigStr));
            this.chart = Highcharts.chart(whereTo, window.ScriptUtil.eval(this.configStr));
        },
        render: function render() {
            if (!this.configStr) return;
            var whereToPaint;
            if (this.stepLevel == 0) whereToPaint = 'saved-chart-preview';
            if (this.stepLevel == 2) whereToPaint = 'second-chart-preview';
            if (this.stepLevel == 3) whereToPaint = 'new-chart-preview';
            if (this.chart) {
                this.chart.destroy();
                this.chart = null;
            }
            try {
                this.paintChart(whereToPaint);
            } catch (error) {
                return this.$message("지원하지 않는 차트 형식 입니다.");
            }
        },
        changeConfigStr: function changeConfigStr() {
            console.log('changeConfig');
            if (this.configStr && this.configStr.startsWith('Highcharts')) {
                this.configStr = this.configStr.substring(this.configStr.indexOf("{"), this.configStr.lastIndexOf("}") + 1);
            }
            this.render();
        },
        show: function show() {
            this.$modal.show('empty-chart');
        },
        modalOpened: function modalOpened() {
            this.configStr = this.componentInstance.getGroupPropertyValue("extension", "configStr");
            this.defaultConfigStr = this.configStr;
            console.log(this.defaultConfigStr);
            this.render();
        },
        _onOk: function _onOk() {
            var self = this;
            var locale_msg = Vue.$i18n.messages.wv;
            this.$confirm(locale_msg.common.confirmSave, locale_msg.common.notification, {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'info'
            }).then(function () {
                self.componentInstance.setGroupPropertyValue("extension", "configStr", self.configStr);
                self.componentInstance.setGroupPropertyValue("extension", "datasetId", self.datasetID);
                self.$modal.hide('empty-chart');
                self.$emit('closed', true);
            });
        },
        beforeModalClosed: function beforeModalClosed() {},
        _onHidden: function _onHidden() {
            var self = this;
            var locale_msg = Vue.$i18n.messages.wv;
            this.$confirm(locale_msg.common.confirmExitWindow, locale_msg.common.notification, {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning'
            }).then(function () {
                self.$modal.hide('empty-chart');
                self.$emit("closed", false);
            });
        }
    },
    destroyed: function destroyed() {
        if (this.chart) {
            this.chart.destroy();
        }
    }
});

var ChartBasicComponent = function (_WVDOMComponent) {
    _inherits(ChartBasicComponent, _WVDOMComponent);

    function ChartBasicComponent() {
        _classCallCheck(this, ChartBasicComponent);

        var _this5 = _possibleConstructorReturn(this, (ChartBasicComponent.__proto__ || Object.getPrototypeOf(ChartBasicComponent)).call(this));

        _this5._isAdded = false;
        _this5._chart = null;
        _this5._config = null;
        _this5.chartId = "";
        _this5._dataProvider = [];
        _this5._xAxisField = "";
        _this5._invaldatePreivewProperty = false;
        _this5._timer = null;
        return _this5;
    }

    _createClass(ChartBasicComponent, [{
        key: 'getExtensionProperties',
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: '_onCreateProperties',
        value: function _onCreateProperties() {
            this.chartId = "id-" + this.id;
            var config = this.getParseConfigData();
            if (config) {
                this.config = config;
            }
        }
    }, {
        key: '_onCreateElement',
        value: function _onCreateElement() {
            $(this._element).append('<div style="width:100%;height:100%;" class="chart-area" id="' + this.chartId + '"></div>');
        }
    }, {
        key: '_onImmediateUpdateDisplay',
        value: function _onImmediateUpdateDisplay() {
            this._isAdded = true;
            this.setPreviewMode();
        }
    }, {
        key: 'onLoadPage',
        value: function onLoadPage() {
            if (!this.isEditorMode && this.getGroupPropertyValue("setter", "autoExecute")) {
                this.execute({});
            }
        }
    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension")) {
                this.validateCallLater(this._updateChartConfig);
            }

            if (this._updatePropertiesMap.has("preview")) {
                this.validateCallLater(this.setPreviewMode);
            }

            if (this._updatePropertiesMap.has("setter") && this._chart.hasRendered) {
                this.validateCallLater(this.setSize);
            }
        }
    }, {
        key: 'setSize',
        value: function setSize() {
            var _this6 = this;

            if (!this._timer) {
                this._timer = setTimeout(function () {
                    _this6._timer = null;_this6._chart.setSize(_this6._properties.setter.width, _this6._properties.setter.height);
                }, 100);
            }
        }
    }, {
        key: 'setPreviewMode',
        value: function setPreviewMode() {
            if (this.isEditorMode && !this.getGroupPropertyValue("preview", "use")) {
                $(this._element).removeClass("no-data").addClass("editor").addClass('customComponent');
                if (this._chart) {
                    this._chart.destroy();
                }
            } else {
                $(this._element).removeClass("editor").removeClass('customComponent');
                this.render();
            }
        }
    }, {
        key: '_updateChartConfig',
        value: function _updateChartConfig() {
            var config = this.getParseConfigData();
            if (config) {
                this.config = config;
                this.render();
            }
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            if (this._chart) this._chart.destroy();
            _get(ChartBasicComponent.prototype.__proto__ || Object.getPrototypeOf(ChartBasicComponent.prototype), '_onDestroy', this).call(this);
        }
    }, {
        key: 'makeChart',
        value: function makeChart() {
            var _this7 = this;
            if (!this._isAdded || $(this._element).hasClass("editor")) {
                return;
            }
            if (!this.config) {
                $.getJSON('./custom/packs/2d_pack/components/high-charts/component/' + this.componentName + '/defaultData.json', function (json) {
                    _this7._properties.extension.configStr = JSON.stringify(json, null, 4);
                    _this7._chart = Highcharts.chart(_this7.chartId, json);
                });
            } else {
                this._chart = Highcharts.chart(this.chartId, this.config);
            }
        }
    }, {
        key: 'render',
        value: function render() {
            this.makeChart();
            // if (!this._chart || !this._chart.hasRendered) return this.makeChart(); else this._chart.update(this.config)
        }
    }, {
        key: 'getParseConfigData',
        value: function getParseConfigData() {
            var data = null;
            try {
                var configStr = this.getGroupPropertyValue("extension", "configStr");

                data = window.ScriptUtil.eval(configStr);
                return data;
            } catch (error) {
                return null;
            }
        }
    }, {
        key: 'validateData',
        value: function validateData() {
            this._chart.update(this._config);
        }
    }, {
        key: 'execute',
        value: function execute(param) {
            var _this8 = this;

            console.log(this.getGroupPropertyValue("extension", "datasetId"));
            if (this.datasetId) {
                this.datasetWorker = this.page.dataService.callById(this.datasetId, { "param": param });
                if (this.datasetWorker && this.datasetWorker.item) {
                    this.datasetWorker.on("error", function (event) {
                        console.warn("dataset call error", event);
                    });
                    this.datasetWorker.on("success", function (event) {
                        var keys = _this8.config.series.map(function (eachData) {
                            return eachData.name;
                        });
                        keys.forEach(function (key) {
                            var chooseIndex = _this8.config.series.findIndex(function (seriesData) {
                                return seriesData.name == key;
                            });
                            _this8.config.series[chooseIndex].data = event.rowData.map(function (data) {
                                return data[key];
                            });
                        });
                        _this8._chart = Highcharts.chart(_this8.chartId, _this8.config);
                    });
                } else {
                    console.warn("dataset is not defined component id : ", this.id);
                }
            } else {
                alert('datasetId does not exist ');
                this._chart = Highcharts.chart(this.chartId, {});
                //   this.makeChartData(null);
            }
        }
    }, {
        key: 'config',
        set: function set(config) {

            this._config = config;
        },
        get: function get() {
            return this._config;
        }
    }, {
        key: 'chart',
        get: function get() {
            return this._chart;
        }
    }, {
        key: 'dataProvider',
        set: function set(data) {
            this._dataProvider = data;
            if (this._dataProvider && this._config) {
                if (this._xAxisField !== "") {
                    var categories = this._dataProvider.map(function (b) {
                        return b[this._xAxisField];
                    }.bind(this));
                    if (!this._config.xAxis) {
                        this._config["xAxis"] = {
                            categories: categories
                        };
                    } else {
                        this._config.xAxis.categories = categories;
                    }
                }
                this._config.series.forEach(function (d) {
                    d.data = this._dataProvider.map(function (b) {
                        return b[d.name];
                    });
                }.bind(this));
            }
            this.validateData();
        },
        get: function get() {
            return this._dataProvider;
        }
    }, {
        key: 'xAxisField',
        set: function set(data) {
            this._xAxisField = data;
        },
        get: function get() {
            return this._xAxisField;
        }
    }, {
        key: 'datasetId',
        get: function get() {
            return this.getGroupPropertyValue("extension", "datasetId");
        }
    }]);

    return ChartBasicComponent;
}(WVDOMComponent);