// basicArea,groupCol,percentArea,splineSymbol, stackedArea -> default json function   -> controlling ineach js file
// each vue file making
Vue.component('popupComponent', {
    template:`<modal
    class="w-modal modal-tabs"
    id="empty-chart"
    name="empty-chart"
    @opened="modalOpened"
    @before-close="beforeModalClosed"
        :click-to-close="false"
    :draggable="false"
    :resizable="true"
    :width="1020"
    :height="670"
    :min-width="700"
    :min-height="370"
    >
    <div class="modal-header"><h4>High Chart Settings</h4><a class="close-modal-btn" role="button" @click="_onHidden"><i class="el-icon-error"></i></a></div>
        <div class="modal-body">
               <div class="wrap" v-show="stepLevel==0">
                      <p>Highcharts Demo Site : <a href="https://www.highcharts.com/demo/" target="_blank">https://www.highcharts.com/demo/</a></p>
                     <div class="cont-wrap" >
                <div class="cont">
                    <h5>Preview</h5>
                    <div class="preview">
                        <div id="saved-chart-preview" style="width: 100%; height: 80%;" ></div>
                    </div>
                </div>
                <div class="cont">
                    <h5>&lt;/&gt; Code</h5>
                    <div class="code">
                                      <textarea
                                                  :placeholder="placeHolder"
                                                  v-model="defaultConfigStr" class="txt" @change="changeConfigStr">
                                      </textarea>
                                </div>
                </div>
            </div>
            <div class="btn-wrap">
                          <el-button class="bottom-btn" type="primary" size="small" @click="_onHidden">{{$t('common.cancel')}}</el-button>
                          <el-button class="bottom-btn" size="small" @click="nextStep1" v-if="supportable">{{$t('common.next')}}</el-button> 
            </div>
              </div>
              <div class="wrap" v-show="stepLevel==1" style="overflow:auto">
                    <div class="cont-wrap">
                    <div class="cont dataset">
                          <dataset-preview-main-component ref="datasetPreviewComp"></dataset-preview-main-component>
                    </div>
                    </div>
                    <div class="btn-wrap">
                          <el-button class="bottom-btn" type="primary" size="small" @click="prevStep">{{$t('common.prev')}}</el-button>
                          <el-button class="bottom-btn" size="small" @click="nextStep2" v-if="supportable">{{$t('common.next')}}</el-button> 
            </div>
              </div>
              <div class="wrap" v-show="stepLevel==2">
                    <div class="cont-wrap">
                    <div class="cont">
                          <transfer-main-component ref="transferComp" @change="changeColumnData" ></transfer-main-component>
                    </div>
                    <div class="cont">
                    <h5>Preview</h5>
                    <div class="preview">
                        <div id="second-chart-preview" style="width: 100%; height: 80%;" ></div>
                    </div>
                    </div>
                    </div>
                    <div class="btn-wrap">
                          <el-button class="bottom-btn" type="primary" size="small" @click="prevStep">{{$t('common.prev')}}</el-button>
                          <el-button class="bottom-btn" size="small" @click="nextStep3" v-if="supportable">{{$t('common.next')}}</el-button> 
            </div>
              </div>
              <div class="wrap" v-show="stepLevel==3">
                     <div class="cont-wrap" >
                <div class="cont">
                    <h5>Preview</h5>
                    <div class="preview">
                        <div id="new-chart-preview" style="width: 100%; height: 80%;" ></div>
                    </div>
                </div>
                <div class="cont">
                    <h5>&lt;/&gt; Code</h5>
                    <div class="code">
                                      <textarea
                                                  :placeholder="placeHolder"
                                                  v-model="configStr" class="txt" @change="changeConfigStr">
                                      </textarea>
                                </div>
                </div>
            </div>
            <div class="btn-wrap">
                          <el-button class="bottom-btn" type="primary" size="small" @click="prevStep">{{$t('common.prev')}}</el-button>
                          <el-button class="bottom-btn" size="small" @click="_onOk" v-if="supportable">{{$t('common.save')}}</el-button> 
            </div>
              </div>
    </div>
  </modal>`,

  props:['componentInstance', 'isCategory', 'isSemipie','needStartPoint'],
  data() {
    return {
          placeHolder: '',
          configStr:'',
          chart: null,
          timer: null,
          supportable: true,
          tranferData: {
                mainData:{
                      title: "Data Column List",
                      data: []
                },

                targetListData:[{
                      title: '',
                      data: []
                },{
                      title: "Series Data",
                      data: []
                }]
          },
          currentDataset:null,
          datasetResultData: null,
          datasetID:null,
          isResetDataset:false,
          stepLevel:0,
          series:[],
          categories:[],
          parsedConfigStr:null,
          defaultConfigStr:null
          
    };
},
    methods: {
                  prevStep:function() { // refactoring
                        this.stepLevel -= 1
                        if (this.stepLevel == 0) { setTimeout(() => { this.render() }, 100) }
                        if (this.stepLevel == 2) {
                              this.parsedConfigStr = window.ScriptUtil.eval(this.configStr)
                              this.categories = [];
                              this.series = [];
                              setTimeout(() => { this.render() }, 100);
                         }
                  },
                  nextStep1:function(){
                        this.stepLevel = 1
                  },
                  nextStep2:function() {
                         this.tranferData.targetListData[0].title = this.isCategory ? 'Category' : 'No Category';

                         this.currentDataset = this.$refs.datasetPreviewComp.getDataset();
                              if (this.datasetID && this.currentDataset && this.datasetID == this.currentDataset.dataset_id) {
                                    var datasetPreviewData = this.$refs.datasetPreviewComp.getPreviewData();
                                    if( datasetPreviewData == null || datasetPreviewData.length == 0 || datasetPreviewData.keys.join('') == this.datasetResultData.keys.join('')){
                                          this.stepLevel = 2;
                                    }else{
                                          this._initTransferData();
                                          this.stepLevel = 2;
                                    }
                              } else {
                                    if (this.$refs.datasetPreviewComp.validationPreviewData()) {
                                          this.datasetResultData = this.$refs.datasetPreviewComp.getPreviewData();
                                          this.datasetID = this.currentDataset.dataset_id;
                                          this._initTransferData();
                                          this.stepLevel = 2;
                                    }else{
                                          return;
                                    }
                        }
                        setTimeout(() => { this.render() }, 100);
                  },
                  nextStep3:function() {
                         if (!this.checkData()) {
                              var locale_msg = Vue.$i18n.messages.wv;
                              Vue.$message(locale_msg.common.setData);
                              return;
                              }
                              this.stepLevel = 3;
                              this.configStr = JSON.stringify(this.parsedConfigStr,null,4)
                              setTimeout(() => { this.render() }, 100);
                  },
                  insertData:function(targetList, _this, isCategory, isSemipie, needStartPoint) {
                        targetList.data.forEach(function(eachCol) {
                              var eachObj;
                              eachObj = isCategory ? null : { name:eachCol, data:[] }
                              if (isSemipie) { eachObj.type = 'pie'; eachObj.innerSize = '50%'}
                              if (needStartPoint) {eachObj.pointStart= 1}
                              _this.datasetResultData.data.forEach(function(eachData) {
                                    if (eachData[eachCol] !== undefined) {
                                        isCategory ?  _this.categories.push(eachData[eachCol]) : eachObj.data.push(eachData[eachCol])
                                    }
                              })
                              if(!isCategory) _this.series.push(eachObj);
                        })
                  },
                  changeColumnData:function(data){  // refactoring
                        console.log('changeColumnData')
                        var self = this;
                        this.parsedConfigStr = window.ScriptUtil.eval(this.configStr)
				        this.tranferData = data;
                        this.isResetDataset = true;

                        if (this.isCategory) {
                            this.categories = [];
                            var categoryList = this.tranferData.targetListData[0];
                            this.insertData(categoryList, self, self.isCategory);
                            var ArrOrNot = Array.isArray(this.parsedConfigStr.xAxis)
                            ArrOrNot ? this.parsedConfigStr.xAxis[0].categories = [...self.categories] : this.parsedConfigStr.xAxis.categories = [...self.categories]
                        }

                        this.series = [];
                        var seriesDataList = this.tranferData.targetListData[1];
                        this.insertData(seriesDataList, self, false, self.isSemipie,self.needStartPoint);
                        this.parsedConfigStr.series = [...self.series]
                        
                        this.configStr = JSON.stringify(this.parsedConfigStr,null,4)
                        this.render();
                  },
                  _initTransferData: function(){
                        this.datasetResultData = this.$refs.datasetPreviewComp.getPreviewData();
                        this.tranferData.mainData.data = this.datasetResultData.keys;
                        this.tranferData.targetListData[0].data = [];
                        this.tranferData.targetListData[1].data = [];
                        this.$refs.transferComp.setData( this.tranferData );
                  },
                  checkData: function() {
                        var targetListData = this.tranferData.targetListData;
                        return targetListData[1].data.length;
                  },

                  paintChart:function(whereTo) {
                        if (this.stepLevel == 0 ) return this.chart = Highcharts.chart(whereTo, window.ScriptUtil.eval(this.defaultConfigStr));
                        this.chart = Highcharts.chart(whereTo, window.ScriptUtil.eval(this.configStr));

                  },
                  render: function render() { 
                        if (!this.configStr) return;
                        var whereToPaint;
                        if (this.stepLevel == 0) whereToPaint = 'saved-chart-preview';
                        if (this.stepLevel == 2) whereToPaint = 'second-chart-preview';
                        if (this.stepLevel == 3) whereToPaint = 'new-chart-preview';
                        if (this.chart) {
                              this.chart.destroy();
                              this.chart = null;
                        } 
                        try { this.paintChart(whereToPaint) } 
                        catch (error) { return this.$message("지원하지 않는 차트 형식 입니다."); }
                       
                       
                  },
                  changeConfigStr: function changeConfigStr() {
                        console.log('changeConfig')
                        if (this.configStr && this.configStr.startsWith('Highcharts')) {
                              this.configStr =this.configStr.substring(this.configStr.indexOf("{"), this.configStr.lastIndexOf("}") + 1);
                        }
                        this.render();
                  },
                  show: function show() {
                        this.$modal.show('empty-chart');
                  },
                  modalOpened: function modalOpened() {
                        this.configStr = this.componentInstance.getGroupPropertyValue("extension", "configStr");
                        this.defaultConfigStr = this.configStr
                        console.log(this.defaultConfigStr)
                        this.render();
                  },
                  _onOk: function _onOk() {
                        var self = this;
                        var locale_msg = Vue.$i18n.messages.wv;
                        this.$confirm(locale_msg.common.confirmSave, locale_msg.common.notification, {
                              confirmButtonText: 'OK',
                              cancelButtonText: 'Cancel',
                              type: 'info'
                        }).then(function () {
                              self.componentInstance.setGroupPropertyValue("extension", "configStr", self.configStr);
                              self.componentInstance.setGroupPropertyValue("extension", "datasetId", self.datasetID);
                              self.$modal.hide('empty-chart');
                              self.$emit('closed', true);
                        });
                  },
                   beforeModalClosed: function beforeModalClosed() {},
                  _onHidden: function _onHidden() {
                        var self = this;
                        var locale_msg = Vue.$i18n.messages.wv;
                        this.$confirm(locale_msg.common.confirmExitWindow, locale_msg.common.notification, {
                              confirmButtonText: 'OK',
                              cancelButtonText: 'Cancel',
                              type: 'warning'
                        }).then(function () {
                              self.$modal.hide('empty-chart');
                              self.$emit("closed", false);
                        });
                  }
            },
            destroyed: function()  {
                  if(this.chart) {
                        this.chart.destroy();
                  }
            }
})



class ChartBasicComponent extends WVDOMComponent {
    constructor() {
        super();
        this._isAdded = false;
        this._chart = null;
        this._config = null;
        this.chartId = "";
        this._dataProvider = [];
        this._xAxisField = "";
        this._invaldatePreivewProperty = false;
        this._timer = null;
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateProperties() {
        this.chartId = "id-" + this.id;
        let config = this.getParseConfigData();
        if (config) {
            this.config = config;
        }
    }

    _onCreateElement() {
        $(this._element).append('<div style="width:100%;height:100%;" class="chart-area" id="' + this.chartId + '"></div>');
    }

    _onImmediateUpdateDisplay() {
        this._isAdded = true;
        this.setPreviewMode();
    }

    onLoadPage() {
        if (!this.isEditorMode && this.getGroupPropertyValue("setter", "autoExecute")) {
              this.execute({});
        }
  }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this._updateChartConfig);
        }

        if (this._updatePropertiesMap.has("preview")) {
            this.validateCallLater(this.setPreviewMode);
        }

        if (this._updatePropertiesMap.has("setter") && this._chart.hasRendered) {
            this.validateCallLater(this.setSize)
        }
    }

    setSize() {
        if (!this._timer) {
            this._timer = setTimeout(() => { this._timer = null;this._chart.setSize(this._properties.setter.width, this._properties.setter.height) },100) 
        }  
      }

    setPreviewMode() {
        if (this.isEditorMode && !this.getGroupPropertyValue("preview", "use")) {
            $(this._element).removeClass("no-data").addClass("editor").addClass('customComponent');
            if (this._chart) {
                this._chart.destroy();
            }
        } else {
            $(this._element).removeClass("editor").removeClass('customComponent')
            this.render();
        }
    }


    _updateChartConfig() {
        let config = this.getParseConfigData();
        if (config) {
            this.config = config;
            this.render();
        }
    }

    _onDestroy() {
        if(this._chart)
            this._chart.destroy();
        super._onDestroy();
    }

    makeChart() {
        if (!this._isAdded || $(this._element).hasClass("editor")) {
            return;
        }
        if (!this.config) {
            $.getJSON('./custom/packs/2d_pack/components/high-charts/component/' + this.componentName + '/defaultData.json',(json) => {
                this._properties.extension.configStr = JSON.stringify(json,null,4)
                this._chart = Highcharts.chart(this.chartId, json)
                })
        } else {
            this._chart = Highcharts.chart(this.chartId, this.config)
        }
       
    }

    render() {
        this.makeChart();
        // if (!this._chart || !this._chart.hasRendered) return this.makeChart(); else this._chart.update(this.config)
    }

    getParseConfigData() {
        let data = null;
        try {
            let configStr = this.getGroupPropertyValue("extension", "configStr");
            
            data = window.ScriptUtil.eval(configStr);
            return data;
        } catch (error) {
            return null;
        }
    }


    validateData() { 
        this._chart.update(this._config);
    }


    execute(param) {
        console.log(this.getGroupPropertyValue("extension", "datasetId"))
        if (this.datasetId) {
                  this.datasetWorker = this.page.dataService.callById(this.datasetId, {"param": param});
                  if (this.datasetWorker && this.datasetWorker.item) {
                        this.datasetWorker.on("error", (event) => {
                              console.warn("dataset call error", event);
                        })
                        this.datasetWorker.on("success", (event) => {
                            let keys = this.config.series.map(eachData => eachData.name)
                            keys.forEach(key => {
                               var chooseIndex = this.config.series.findIndex( seriesData => seriesData.name == key )
                                this.config.series[chooseIndex].data = event.rowData.map(data => data[key])
                            })
                            this._chart = Highcharts.chart(this.chartId, this.config);
                            
                        });
                  } else {
                        console.warn("dataset is not defined component id : ", this.id);
                  }

        } else {
            alert('datasetId does not exist ')
            this._chart = Highcharts.chart(this.chartId, {});
            //   this.makeChartData(null);
        }
  }
  
    set config(config) {
      
        this._config = config;
    }

    get config() {
        return this._config;
    }

    get chart() {
        return this._chart;
    }

    set dataProvider(data) {
        this._dataProvider = data;
        if(this._dataProvider && this._config) {
            if(this._xAxisField !== "") {
                var categories = this._dataProvider.map(function(b) { return b[this._xAxisField] }.bind(this))
                if(!this._config.xAxis) {
                    this._config["xAxis"] = {
                        categories : categories
                    }
                } else {
                    this._config.xAxis.categories = categories
                }
            }
            this._config.series.forEach(function(d) {
                d.data = this._dataProvider.map(function(b) { return b[d.name] })
            }.bind(this));
        }
        this.validateData();
    }

    get dataProvider() {
        return this._dataProvider;
    }

    set xAxisField(data) {
        this._xAxisField = data;
    }

    get xAxisField() {
        return this._xAxisField;
    }

    get datasetId() {
        return this.getGroupPropertyValue("extension", "datasetId");
    }
}



