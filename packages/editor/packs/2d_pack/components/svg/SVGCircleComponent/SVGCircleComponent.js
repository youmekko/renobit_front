/**
 * circle엘리먼트를 이용할 경우 중심점이 center로 고정되는 관계로 path를 이용해
 * 좌측 상단으로 그림.
 * https://developer.mozilla.org/ko/docs/Web/SVG/Tutorial/Paths
 * */
class SVGCircleComponent extends WVSVGComponent {

    constructor() {
        super();
    }

    getDrawPath() {
        let w = this.width;
        let h = this.height;
        let path = [
            ["M", 0, h / 2],
            ["a", w / 2, h / 2, 0, 1, 0, w, 0],
            ["a", w / 2, h / 2, 0, 1, 0, -w, 0]
        ];
        return Snap.parsePathString(path);
    }

    getTransformValue() {
        return "translate( " + this.x + ", " + this.y + " ) rotate( " + this.rotation + " 0 0 )";
    }

    _validatePosition() {
        $(this.element).attr({ "transform": this.getTransformValue() });
    }

    _validateSize() {
        $(this.element).attr({ "d": this.getDrawPath() });
    }

    _validateRotation() {
        $(this.element).attr({ transform: this.getTransformValue() });
    }

    _onCreateElement() {
          this._element = CPUtil.getInstance().createSVGElement("path");
          this.appendElement.appendChild(this.element);
    }


}

WVPropertyManager.attach_default_component_infos(SVGCircleComponent, {
      "info": {
            "componentName": "SVGCircleComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 100,
            "height": 100
      },
      "background" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#fff",
            "color2": "#000",
            "text": '',
            "info" : "background"
      },
      "strokeStyle" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#fff",
            "color2": "#000",
            "text": '',
            "width" : 1,
            "stroke" : "#fff",
            "info" : "stroke"
      },

      "shadow": {
            "xPosition": '0',
            "yPosition": '0',
            "blur": '0',
            "color": 'rgb(255,255,255)'
      }
});

SVGCircleComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      label: "Fill",
      template: "background-gradient",
      owner : "background"
}, {
      label: "Stroke",
      template: "stroke-gradient",
      owner : "strokeStyle"
}, {
      label : "Shadow",
      template : "shadow"
}];

WVPropertyManager.remove_property_group_info(SVGCircleComponent, "label");
WVPropertyManager.remove_property_group_info(SVGCircleComponent, "background");


WVPropertyManager.add_property_group_info(SVGCircleComponent, {
      label: "SVGCircleComponent 고유 속성",
      children: [{
            name: "fill",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "fill 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
      }, {
            name: "stroke",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
      }, {
            name: "stroke-width",
            type: "number",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 두께를 설정합니다."
      }]
});
