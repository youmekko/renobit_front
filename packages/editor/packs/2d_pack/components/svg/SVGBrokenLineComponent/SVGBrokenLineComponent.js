class SVGBrokenLineComponent extends WVPointComponent {

constructor() {
      super();
}


get paths() {
      let path = [];
      let pointList = this.points.concat();
      let p1 = pointList[0];
      let p2;
      let max = pointList.length;
      let idx;
      path.push(this.moveTo(p1));
      for (idx = 1; idx < max; idx++) {
            p1 = pointList[idx - 1];
            p2 = pointList[idx];
            if (idx > 1) {
                  path.push(this.lineTo(new Point(p1.x, p2.y)));
                  path.push(this.lineTo(new Point(p2.x, p2.y)));
            } else {
                  path.push(this.lineTo(new Point(p2.x, p1.y)));
                  path.push(this.lineTo(new Point(p2.x, p2.y)));
            }

      }

      return Snap.parsePathString(path);
}


_createDefaultPoints() {
      let sp = new Point(this.x, this.y);
      let ep = new Point(this.x + this.width, this.y + this.height);
      let cp = sp.interpolate(ep, 0.5);
      return [sp, cp, ep];
}

_onCreateElement() {
      this._element = CPUtil.getInstance().createSVGElement("path");
      this.appendElement.appendChild(this.element);
}

handleTransformList() {
      let results = [];
      let idx;
      let max = this.points.length;
      for (idx = 0; idx < max; idx++) {
            results.push("broken-anchor-point");
      }
      return results;
}


_notifyInserPoint(point) {
      this.notifyComponentEvent(new WVPointComponentEvent(
            WVPointComponentEvent.INSERT_POINT, this, {
                  point: point,
                  type: "broken-anchor-point"
            }
      ));
}

_notifyRemovePoint(point) {
      this.notifyComponentEvent(new WVPointComponentEvent(
            WVPointComponentEvent.REMOVE_POINT, this, {
                  point: point
            }
      ));
}
}

WVPropertyManager.attach_default_component_infos(SVGBrokenLineComponent, {
"info": {
      "componentName": "SVGBrokenLineComponent",
      "version": "1.0.0"
},

"setter": {
      "width": 100,
      "height": 100,
      "pointCount": 3,
      "starMarker": false,
      "endMarker": false
},

"strokeStyle" : {
      "type": "solid",
      "direction": "left", //top, left, diagonal1, diagonal2, radial
      "color1": "#ff00ff",
      "color2": "#000",
      "text": '',
      "width" : 1,
      "stroke" : "#ff00ff",
      "info" : "stroke"
},

"shadow": {
      "xPosition": '0',
      "yPosition": '0',
      "blur": '0',
      "color": 'rgb(255,255,255)'
}

});

WVPropertyManager.remove_property_group_info(SVGBrokenLineComponent, "label");
WVPropertyManager.remove_property_group_info(SVGBrokenLineComponent, "background");

WVPropertyManager.add_property_group_info(SVGBrokenLineComponent, {
label: "SVGBrokenLineComponent 고유 속성",
children: [{
      name: "pointCount",
      type: "number",
      show: true,
      writable: true,
      description: "라인을 그릴 때 사용할 포인트 수 입니다."
}, {
      name: "starMarker",
      type: "boolean",
      show: true,
      writable: true,
      description: "라인 시작점에 화살표 마커 사용여부를 설정합니다."
}, {
      name: "endMarker",
      type: "boolean",
      show: true,
      writable: true,
      description: "라인 끝점에 화살표 마커 사용여부를 설정합니다."
}, {
      name: "stroke",
      type: "string",
      owner: "style",
      show: true,
      writable: true,
      description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
}, {
      name: "stroke-width",
      type: "number",
      owner: "style",
      show: true,
      writable: true,
      description: "라인 두께를 설정합니다."
}]
});

SVGBrokenLineComponent.property_panel_info = [{
template: "primary"
}, {
template: "pos-size-2d"
},
{
      label: "Stroke",
      template: "stroke-gradient",
      owner : "strokeStyle",
}, {
      label: "Arrow",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "startMarker",
            type: "checkbox",
            label: "Start",
            show: true,
            writable: true,
            description: "Start Arrow Icon"
      }, {
            owner: "setter",
            name: "endMarker",
            type: "checkbox",
            label: "End",
            show: true,
            writable: true,
            description: "Start Arrow Icon"
      }]
}, {

      label: "Point",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "Length",
            show: true,
            writable: true,
            description: "point",
            options: {
                  min: 2,
                  max: 50
            }
      }]

}, {
      label : "Shadow",
      template : "shadow"
}];
