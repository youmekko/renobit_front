"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SVGBrokenLineComponent = function (_WVPointComponent) {
      _inherits(SVGBrokenLineComponent, _WVPointComponent);

      function SVGBrokenLineComponent() {
            _classCallCheck(this, SVGBrokenLineComponent);

            return _possibleConstructorReturn(this, (SVGBrokenLineComponent.__proto__ || Object.getPrototypeOf(SVGBrokenLineComponent)).call(this));
      }

      _createClass(SVGBrokenLineComponent, [{
            key: "_createDefaultPoints",
            value: function _createDefaultPoints() {
                  var sp = new Point(this.x, this.y);
                  var ep = new Point(this.x + this.width, this.y + this.height);
                  var cp = sp.interpolate(ep, 0.5);
                  return [sp, cp, ep];
            }
      }, {
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  this._element = CPUtil.getInstance().createSVGElement("path");
                  this.appendElement.appendChild(this.element);
            }
      }, {
            key: "handleTransformList",
            value: function handleTransformList() {
                  var results = [];
                  var idx = void 0;
                  var max = this.points.length;
                  for (idx = 0; idx < max; idx++) {
                        results.push("broken-anchor-point");
                  }
                  return results;
            }
      }, {
            key: "_notifyInserPoint",
            value: function _notifyInserPoint(point) {
                  this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.INSERT_POINT, this, {
                        point: point,
                        type: "broken-anchor-point"
                  }));
            }
      }, {
            key: "_notifyRemovePoint",
            value: function _notifyRemovePoint(point) {
                  this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.REMOVE_POINT, this, {
                        point: point
                  }));
            }
      }, {
            key: "paths",
            get: function get() {
                  var path = [];
                  var pointList = this.points.concat();
                  var p1 = pointList[0];
                  var p2 = void 0;
                  var max = pointList.length;
                  var idx = void 0;
                  path.push(this.moveTo(p1));
                  for (idx = 1; idx < max; idx++) {
                        p1 = pointList[idx - 1];
                        p2 = pointList[idx];
                        if (idx > 1) {
                              path.push(this.lineTo(new Point(p1.x, p2.y)));
                              path.push(this.lineTo(new Point(p2.x, p2.y)));
                        } else {
                              path.push(this.lineTo(new Point(p2.x, p1.y)));
                              path.push(this.lineTo(new Point(p2.x, p2.y)));
                        }
                  }

                  return Snap.parsePathString(path);
            }
      }]);

      return SVGBrokenLineComponent;
}(WVPointComponent);

WVPropertyManager.attach_default_component_infos(SVGBrokenLineComponent, {
      "info": {
            "componentName": "SVGBrokenLineComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 100,
            "height": 100,
            "pointCount": 3,
            "starMarker": false,
            "endMarker": false
      },

      "strokeStyle" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#ff00ff",
            "color2": "#000",
            "text": '',
            "width" : 1,
            "stroke" : "#ff00ff",
            "info" : "stroke"
      },

      "shadow": {
            "xPosition": '0',
            "yPosition": '0',
            "blur": '0',
            "color": 'rgb(255,255,255)'
      }

});

WVPropertyManager.remove_property_group_info(SVGBrokenLineComponent, "label");
WVPropertyManager.remove_property_group_info(SVGBrokenLineComponent, "background");

WVPropertyManager.add_property_group_info(SVGBrokenLineComponent, {
      label: "SVGBrokenLineComponent 고유 속성",
      children: [{
            name: "pointCount",
            type: "number",
            show: true,
            writable: true,
            description: "라인을 그릴 때 사용할 포인트 수 입니다."
      }, {
            name: "starMarker",
            type: "boolean",
            show: true,
            writable: true,
            description: "라인 시작점에 화살표 마커 사용여부를 설정합니다."
      }, {
            name: "endMarker",
            type: "boolean",
            show: true,
            writable: true,
            description: "라인 끝점에 화살표 마커 사용여부를 설정합니다."
      }, {
            name: "stroke",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
      }, {
            name: "stroke-width",
            type: "number",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 두께를 설정합니다."
      }]
});

SVGBrokenLineComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
},
      {
            label: "Stroke",
            template: "stroke-gradient",
            owner : "strokeStyle",
      }, {
      label: "Arrow",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "startMarker",
            type: "checkbox",
            label: "Start",
            show: true,
            writable: true,
            description: "Start Arrow Icon"
      }, {
            owner: "setter",
            name: "endMarker",
            type: "checkbox",
            label: "End",
            show: true,
            writable: true,
            description: "Start Arrow Icon"
      }]
}, {

      label: "Point",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "Length",
            show: true,
            writable: true,
            description: "point",
            options: {
                  min: 2,
                  max: 50
            }
      }]

}, {
            label : "Shadow",
            template : "shadow"
}];
