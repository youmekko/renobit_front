"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var SVGStreamCurveComponent = function(_WVSVGStreamLineCompo) {
    _inherits(SVGStreamCurveComponent, _WVSVGStreamLineCompo);

    function SVGStreamCurveComponent() {
        _classCallCheck(this, SVGStreamCurveComponent);

        return _possibleConstructorReturn(this, (SVGStreamCurveComponent.__proto__ || Object.getPrototypeOf(SVGStreamCurveComponent)).call(this));
    }

    _createClass(SVGStreamCurveComponent, [{
        key: "_createDefaultPoints",
        value: function _createDefaultPoints() {
            var sp = new Point(this.properties.setter.x, this.properties.setter.y);
            var ep = new Point(this.properties.setter.x + this.properties.setter.width, this.properties.setter.y + this.properties.setter.height);
            var cp = sp.interpolate(ep, 0.5);
            return [sp, cp, ep];
        }
    }, {
        key: "handleTransformList",
        value: function handleTransformList() {
            var results = [];
            var idx = void 0;
            var max = this.points.length;
            for (idx = 0; idx < max; idx++) {
                if (idx == 0 || idx == max - 1) {
                    results.push("point-anchor");
                } else {
                    results.push("curve-control");
                }
            }
            return results;
        }
    }, {
        key: "_notifyInserPoint",
        value: function _notifyInserPoint(point) {
            this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.INSERT_POINT, this, {
                point: point,
                type: "curve-control"
            }));
        }
    }, {
        key: "_notifyRemovePoint",
        value: function _notifyRemovePoint(point) {
            this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.REMOVE_POINT, this, {
                point: point
            }));
        }
    }, {
        key: "controlPaths",
        get: function get() {
            var path = [];
            var pointList = this.points.concat();
            var max = pointList.length;
            var p1 = void 0,
                p2 = void 0;
            for (var i = 1; i < max; i++) {
                p1 = pointList[i - 1];
                p2 = pointList[i];
                path.push(this.moveTo(p1));
                path.push(this.lineTo(p2));
            }
            return Snap.parsePathString(path);
        }
    }, {
        key: "paths",
        get: function get() {
            var path = [];
            var pointList = this.points.concat();
            var max = pointList.length;
            var p1 = void 0,
                p2 = void 0,
                cp = void 0,
                prevPoint = void 0;

            for (var i = 1; i < max; i++) {
                p1 = pointList[i - 1];
                p2 = pointList[i];
                cp = p1.interpolate(p2, 0.5);
                if (prevPoint) {
                    path.push(this.curveTo(p1, cp));
                } else {
                    path.push(this.moveTo(p1));
                    path.push(this.lineTo(cp));
                }
                prevPoint = cp;
            }
            path.push(this.lineTo(this.points[max - 1]));
            return Snap.parsePathString(path);
        }
    }]);

    return SVGStreamCurveComponent;
}(WVSVGStreamLineComponent);

WVPropertyManager.attach_default_component_infos(SVGStreamCurveComponent, {
    "info": {
        "componentName": "SVGStreamCurveComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "startMarker": false,
        "endMarker": false,
        "pointCount": 3,
        "preview": true
    },

      "strokeStyle" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#AAffEE",
            "color2": "#000",
            "text": '',
            "width" : 4,
            "stroke" : "#AAffEE",
            "info" : "stroke"
      },

    "stateInfo": {
        "state": "normal",
        "direction": "left",
        "normal": "#ffffff",
        "minor": "#CCCCCC",
        "major": "#ffcc00",
        "warning": "#AA0000",
        "critical": "#FF0000"
    },

    "tweenInfo": {
        "duration": 1000,
        "streamSize": 20
    },

      "shadow": {
            "xPosition": '0',
            "yPosition": '0',
            "blur": '0',
            "color": 'rgb(255,255,255)'
      }
});


WVPropertyManager.remove_property_group_info(SVGStreamCurveComponent, "label");
WVPropertyManager.remove_property_group_info(SVGStreamCurveComponent, "background");



WVPropertyManager.add_property_group_info(SVGStreamCurveComponent, {
    label: "SVGStreamCurveComponent 고유 속성",
    children: [{
        name: "pointCount",
        type: "number",
        show: true,
        writable: true,
        description: "라인을 그릴 때 사용할 포인트 수 입니다."
    }, {
        name: "starMarker",
        type: "boolean",
        show: true,
        writable: true,
        description: "라인 시작점에 화살표 마커 사용여부를 설정 합니다."
    }, {
        name: "endMarker",
        type: "boolean",
        show: true,
        writable: true,
        description: "라인 끝점에 화살표 마커 사용여부를 설정 합니다."
    }, {
        name: "stroke",
        type: "string",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 컬러에 적용할 hex값 입니다."
    }, {
        name: "stroke-width",
        type: "number",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 두께를 설정 합니다."
    }, {
        name: "state",
        type: "string",
        owner: "stateInfo",
        defaultValue: "'normal'",
        show: true,
        writable: true,
        description: "흐름선의 상태를 설정합니다.(상태는 normal, minor, major, warning, critical중 하나 입니다)"
    }, {
        name: "direction",
        type: "string",
        owner: "stateInfo",
        defaultValue: "'left'",
        show: true,
        writable: true,
        description: "흐름선의 방향을 설정합니다.(상태는 left, right, biDirectional 중에 하나 입니다)"
    }, {
        name: "duration",
        type: "number",
        owner: "tweenInfo",
        defaultValue: "1000",
        show: true,
        writable: true,
        description: "흐름선의 속도를 설정합니다.(단위는 밀리초를 이용합니다)"
    }, {
        name: "streamSize",
        type: "number",
        owner: "tweenInfo",
        defaultValue: "20",
        show: true,
        writable: true,
        description: "흐름선의 길이를 설정합니다.(전체 흐름선 길이에 비례합니다)"
    }]
});

SVGStreamCurveComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
      label: "Stroke",
      template: "stroke-gradient",
      owner : "strokeStyle",
}, {
    label: "Arrow",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "startMarker",
        type: "checkbox",
        label: "Start",
        show: true,
        writable: true,
        description: "Start Arrow Icon"
    }, {
        owner: "setter",
        name: "endMarker",
        type: "Checkbox",
        label: "end",
        show: true,
        writable: true,
        description: "Start Arrow Icon"
    }]
}, {

    label: "Point",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "pointCount",
        type: "number",
        label: "Length",
        show: true,
        writable: true,
        description: "point",
          options: {
                min: 2,
                max: 50
          }
    }]

}, {
    label: "Stream Preview",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "preview",
        type: "checkbox",
        label: "Preview",
        show: true,
        writable: true,
        description: "preview"
    }]
}, {
    label: "Stream Info",
    template: "vertical",
    children: [{
        owner: "stateInfo",
        name: "state",
        type: "select",
        label: "State",
        show: true,
        writable: true,
        description: "state",
        options: {
            items: [{ label: "normal", value: "normal" }, { label: "minor", value: "minor" }, { label: "major", value: "major" }, { label: "warning", value: "warning" }, { label: "critical", value: "critical" }]
        }
    }, {
        owner: "stateInfo",
        name: "direction",
        type: "select",
        label: "Direction",
        show: true,
        writable: true,
        description: "direction",
        options: {
            items: [{ label: "left", value: "left" }, { label: "right", value: "right" }, { label: "biDirectional", value: "biDirectional" }]
        }
    }, {
        owner: "stateInfo",
        name: "normal",
        type: "color",
        label: "Normal",
        show: true,
        writable: true,
        description: "normal state color"
    }, {
        owner: "stateInfo",
        name: "minor",
        type: "color",
        label: "Minor",
        show: true,
        writable: true,
        description: "minor state color"
    }, {
        owner: "stateInfo",
        name: "major",
        type: "color",
        label: "Major",
        show: true,
        writable: true,
        description: "major state color"
    }, {
        owner: "stateInfo",
        name: "warning",
        type: "color",
        label: "Warning",
        show: true,
        writable: true,
        description: "warning state color"
    }, {
        owner: "stateInfo",
        name: "critical",
        type: "color",
        label: "Critical",
        show: true,
        writable: true,
        description: "critical state color"
    }]
}, {
    label: "Tween Info",
    template: "vertical",
    children: [{
        owner: "tweenInfo",
        name: "duration",
        type: "number",
        label: "Duration",
        show: true,
        writable: true,
        description: "duration"
    }, {
        owner: "tweenInfo",
        name: "streamSize",
        type: "number",
        label: "StreamSize",
        show: true,
        writable: true,
        description: "streamSize"
    }]
}, {
      label : "Shadow",
      template : "shadow"
}];
