function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var SVGStreamLineComponent =
      /*#__PURE__*/
      function (_WVSVGStreamLineCompo) {
            "use strict";

            _inherits(SVGStreamLineComponent, _WVSVGStreamLineCompo);

            function SVGStreamLineComponent() {
                  _classCallCheck(this, SVGStreamLineComponent);

                  return _possibleConstructorReturn(this, _getPrototypeOf(SVGStreamLineComponent).call(this));
            }

            _createClass(SVGStreamLineComponent, [{
                  key: "_createDefaultPoints",
                  value: function _createDefaultPoints() {
                        return [new Point(this.x, this.y), new Point(this.x + this.width, this.y + this.height)];
                  }
            }, {
                  key: "handleTransformList",
                  value: function handleTransformList() {
                        var results = [];
                        var idx;

                        for (idx = 0; idx < this.points.length; idx++) {
                              results.push("point-anchor");
                        }

                        return results;
                  }
            }, {
                  key: "_notifyInserPoint",
                  value: function _notifyInserPoint(point) {
                        this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.INSERT_POINT, this, {
                              point: point,
                              type: "point-anchor"
                        }));
                  }
            }, {
                  key: "_notifyRemovePoint",
                  value: function _notifyRemovePoint(point) {
                        this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.REMOVE_POINT, this, {
                              point: point
                        }));
                  }
            }, {
                  key: "paths",
                  get: function get() {
                        var path = [];
                        var pointList = this.points.concat();
                        var currentPoint = pointList.shift();
                        path.push(this.moveTo(currentPoint));

                        for (var i = 0; i < pointList.length; i++) {
                              path.push(this.lineTo(pointList[i]));
                        }

                        return Snap.parsePathString(path);
                  }
            }]);

            return SVGStreamLineComponent;
      }(WVSVGStreamLineComponent);

WVPropertyManager.attach_default_component_infos(SVGStreamLineComponent, {
      "info": {
            "componentName": "SVGStreamLineComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 100,
            "height": 100,
            "startMarker": false,
            "endMarker": false,
            "pointCount": 2,
            "preview": true
      },
      "strokeStyle" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#AAffEE",
            "color2": "#000",
            "text": '',
            "width" : 4,
            "stroke" : "#AAffEE",
            "info" : "stroke"
      },
      "stateInfo": {
            "state": "normal",
            "direction": "left",
            "normal": "#ffffff",
            "minor": "#CCCCCC",
            "major": "#ffcc00",
            "warning": "#AA0000",
            "critical": "#FF0000"
      },
      "tweenInfo": {
            "duration": 1000,
            "streamSize": 20
      },

      "shadow": {
            "xPosition": '0',
            "yPosition": '0',
            "blur": '0',
            "color": 'rgb(255,255,255)'
      }
});
WVPropertyManager.remove_property_group_info(SVGStreamLineComponent, "label");
WVPropertyManager.remove_property_group_info(SVGStreamLineComponent, "background");
WVPropertyManager.add_property_group_info(SVGStreamLineComponent, {
      label: "SVGStreamLineComponent 고유 속성",
      children: [{
            name: "pointCount",
            type: "number",
            show: true,
            writable: true,
            description: "라인을 그릴 때 사용할 포인트 수입니다."
      }, {
            name: "starMarker",
            type: "boolean",
            show: true,
            writable: true,
            description: "라인 시작점에 화살표 마커 사용여부를 설정합니다."
      }, {
            name: "endMarker",
            type: "boolean",
            show: true,
            writable: true,
            description: "라인 끝점에 화살표 마커 사용여부를 설정합니다."
      }, {
            name: "stroke",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
      }, {
            name: "stroke-width",
            type: "number",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 두께를 설정합니다."
      }, {
            name: "state",
            type: "string",
            owner: "stateInfo",
            defaultValue: "'normal'",
            show: true,
            writable: true,
            description: "흐름선의 상태를 설정합니다.(상태는 normal, minor, major, warning, critical중 하나 입니다)"
      }, {
            name: "direction",
            type: "string",
            owner: "stateInfo",
            defaultValue: "'left'",
            show: true,
            writable: true,
            description: "흐름선의 방향을 설정합니다.(상태는 left, right, biDirectional 중에 하나 입니다)"
      }, {
            name: "duration",
            type: "number",
            owner: "tweenInfo",
            defaultValue: "1000",
            show: true,
            writable: true,
            description: "흐름선의 속도를 설정합니다.(단위는 밀리초를 이용합니다)"
      }, {
            name: "streamSize",
            type: "number",
            owner: "tweenInfo",
            defaultValue: "20",
            show: true,
            writable: true,
            description: "흐름선의 길이를 설정합니다.(전체 흐름선 길이에 비례합니다)"
      }]
});
SVGStreamLineComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      label: "Stroke",
      template: "stroke-gradient",
      owner : "strokeStyle",
}, {
      label: "Arrow",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "startMarker",
            type: "checkbox",
            label: "Start",
            show: true,
            writable: true,
            description: "Start Arrow Icon"
      }, {
            owner: "setter",
            name: "endMarker",
            type: "checkbox",
            label: "End",
            show: true,
            writable: true,
            description: "Start Arrow Icon"
      }]
}, {
      label: "Point",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "Length",
            show: true,
            writable: true,
            description: "point",
            options: {
                  min: 2,
                  max: 50
            }
      }]
}, {
      label: "Stream Preview",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "preview",
            type: "checkbox",
            label: "Preview",
            show: true,
            writable: true,
            description: "preview"
      }]
}, {
      label: "Stream Info",
      template: "vertical",
      children: [{
            owner: "stateInfo",
            name: "state",
            type: "select",
            label: "State",
            show: true,
            writable: true,
            description: "state",
            options: {
                  items: [{
                        label: "normal",
                        value: "normal"
                  }, {
                        label: "minor",
                        value: "minor"
                  }, {
                        label: "major",
                        value: "major"
                  }, {
                        label: "warning",
                        value: "warning"
                  }, {
                        label: "critical",
                        value: "critical"
                  }]
            }
      }, {
            owner: "stateInfo",
            name: "direction",
            type: "select",
            label: "Direction",
            show: true,
            writable: true,
            description: "direction",
            options: {
                  items: [{
                        label: "left",
                        value: "left"
                  }, {
                        label: "right",
                        value: "right"
                  }, {
                        label: "biDirectional",
                        value: "biDirectional"
                  }]
            }
      }, {
            owner: "stateInfo",
            name: "normal",
            type: "color",
            label: "Normal",
            show: true,
            writable: true,
            description: "normal state color"
      }, {
            owner: "stateInfo",
            name: "minor",
            type: "color",
            label: "Minor",
            show: true,
            writable: true,
            description: "minor state color"
      }, {
            owner: "stateInfo",
            name: "major",
            type: "color",
            label: "Major",
            show: true,
            writable: true,
            description: "major state color"
      }, {
            owner: "stateInfo",
            name: "warning",
            type: "color",
            label: "Warning",
            show: true,
            writable: true,
            description: "warning state color"
      }, {
            owner: "stateInfo",
            name: "critical",
            type: "color",
            label: "Critical",
            show: true,
            writable: true,
            description: "critical state color"
      }]
}, {
      label: "Tween Info",
      template: "vertical",
      children: [{
            owner: "tweenInfo",
            name: "duration",
            type: "number",
            label: "Duration",
            show: true,
            writable: true,
            description: "duration"
      }, {
            owner: "tweenInfo",
            name: "streamSize",
            type: "number",
            label: "StreamSize",
            show: true,
            writable: true,
            description: "streamSize"
      }]
}, {
      label : "Shadow",
      template : "shadow"
}];
