class SVGCurveComponent extends WVPointComponent {

    constructor() {
        super();
    }

    get controlPaths() {
        let path = [];
        let pointList = this.points.concat();
        let max = pointList.length;
        let p1, p2;
        for (let i = 1; i < max; i++) {
            p1 = pointList[i - 1];
            p2 = pointList[i];
            path.push(this.moveTo(p1));
            path.push(this.lineTo(p2));
        }
        return Snap.parsePathString(path);
    }

    get paths() {
        let path = [];
        let pointList = this.points.concat();
        let max = pointList.length;
        let p1, p2, cp, prevPoint;

        for (let i = 1; i < max; i++) {
            p1 = pointList[i - 1];
            p2 = pointList[i];
            cp = p1.interpolate(p2, 0.5);
            if (prevPoint) {
                path.push(this.curveTo(p1, cp));
            } else {
                path.push(this.moveTo(p1));
                path.push(this.lineTo(cp));
            }
            prevPoint = cp;
        }
        path.push(this.lineTo(this.points[max - 1]));
        return Snap.parsePathString(path);
    }

    _onCreateElement() {
        this._element = CPUtil.getInstance().createSVGElement("path");
        this.appendElement.appendChild(this.element);
    }

    _createDefaultPoints() {
        let sp = new Point(this.x, this.y);
        let ep = new Point(this.x + this.width, this.y + this.height);
        let cp = sp.interpolate(ep, 0.5);
        return [sp, cp, ep];
    }

    _notifyInserPoint(point) {
        this.notifyComponentEvent(new WVPointComponentEvent(
            WVPointComponentEvent.INSERT_POINT, this, {
                point: point,
                type: "curve-control"
            }
        ));
    }

    _notifyRemovePoint(point) {
        this.notifyComponentEvent(new WVPointComponentEvent(
            WVPointComponentEvent.REMOVE_POINT, this, {
                point: point
            }
        ));
    }

    handleTransformList() {
        let results = [];
        let idx;
        let max = this.points.length;
        for (idx = 0; idx < max; idx++) {
            if (idx == 0 || idx == max - 1) {
                results.push("point-anchor");
            } else {
                results.push("curve-control");
            }
        }
        return results;
    }

}


WVPropertyManager.attach_default_component_infos(SVGCurveComponent, {
      "info": {
            "componentName": "SVGCurveComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 100,
            "height": 100,
            "startMarker": false,
            "endMarker": false,
            "pointCount": 3
      },

      "strokeStyle" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#AAffEE",
            "color2": "#fe8033",
            "text": '',
            "width" : 1,
            "stroke" : "#AAffEE",
            "info" : "stroke"
      },

      "shadow": {
            "xPosition": '0',
            "yPosition": '0',
            "blur": '0',
            "color": 'rgb(255,255,255)'
      }

});

SVGCurveComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      label: "Stroke",
      template: "stroke-gradient",
      owner : "strokeStyle",
}, {
      label: "Arrow",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "startMarker",
            type: "checkbox",
            label: "Start",
            show: true,
            writable: true,
            description: "Start Arrow Icon"
      }, {
            owner: "setter",
            name: "endMarker",
            type: "checkbox",
            label: "End",
            show: true,
            writable: true,
            description: "Start Arrow Icon"
      }]
}, {

      label: "Point",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "Length",
            show: true,
            writable: true,
            description: "point",
            options: {
                  min: 2,
                  max: 50
            }
      }]

}, {
      label : "Shadow",
      template : "shadow"
}];

WVPropertyManager.remove_property_group_info(SVGCurveComponent, "label");
WVPropertyManager.remove_property_group_info(SVGCurveComponent, "background");



WVPropertyManager.add_property_group_info(SVGCurveComponent, {
      label: "SVGCurveComponent 고유 속성",
      children: [{
            name: "pointCount",
            type: "number",
            show: true,
            writable: true,
            description: "라인을 그릴 때 사용할 포인트 수 입니다."
      }, {
            name: "starMarker",
            type: "boolean",
            show: true,
            writable: true,
            description: "라인 시작점에 화살표 마커 사용여부를 설정합니다."
      }, {
            name: "endMarker",
            type: "boolean",
            show: true,
            writable: true,
            description: "라인 끝점에 화살표 마커 사용여부를 설정합니다."
      }, {
            name: "stroke",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
      }, {
            name: "stroke-width",
            type: "number",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 두께를 설정합니다."
      }]
});
