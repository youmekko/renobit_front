class SVGRectComponent extends WVSVGComponent {

    constructor() { super(); }

    _onCreateElement() {
        this._element = CPUtil.getInstance().createSVGElement("rect");
        this.appendElement.appendChild(this.element);
    }


    _validatePosition() {
        $(this.element).attr({ x: this.x, y: this.y });
        if (this.rotation != 0) { this._validateRotation(); }
    }

    _validateSize() {
        $(this.element).attr({ width: this.width, height: this.height });
    }

    _validateRotation() {
        $(this.element).attr({ transform: "rotate(" + this.rotation + " " + this.x + " " + this.y + ")" });
    }
}

WVPropertyManager.attach_default_component_infos(SVGRectComponent, {
      "info": {
            "componentName": "SVGRectComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 100,
            "height": 100,
      },
      "style": {
            "rx": 0,
      },

      "strokeStyle" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#fff",
            "color2": "#000",
            "text": '',
            "width" : 1,
            "stroke" : "#fff",
            "info" : "stroke"
      },
      "background" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#fff",
            "color2": "#000",
            "text": '',
            "info" : "background"
      },

      "shadow": {
            "xPosition": '0',
            "yPosition": '0',
            "blur": '0',
            "color": 'rgb(255,255,255)'
      }
});


SVGRectComponent.property_panel_info = [{
      template: "primary"
},
      {
            template: "pos-size-2d"
      },
      {
            label: "Fill",
            template: "background-gradient",
            owner : "background"
      },
      {
            label: "Stroke",
            template: "stroke-gradient",
            owner : "strokeStyle",
      }, {
            label: "Border Radius",
            template: "vertical",
            children: [{
                  owner: "style",
                  name: "rx",
                  type: "number",
                  label: "Size",
                  show: true,
                  writable: true,
                  description: "Broder radius"
            }]
      }, {
            label : "Shadow",
            template : "shadow"
      }
];


WVPropertyManager.remove_property_group_info(SVGRectComponent, "label");
WVPropertyManager.remove_property_group_info(SVGRectComponent, "background");


WVPropertyManager.add_property_group_info(SVGRectComponent, {
      label: "SVGRectComponent 고유 속성",
      children: [{
            name: "fill",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "fill 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
      }, {
            name: "rx",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 라운드 속성 값입니다."
      }, {
            name: "stroke",
            type: "string",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
      }, {
            name: "stroke-width",
            type: "number",
            owner: "style",
            show: true,
            writable: true,
            description: "라인 두께를 설정합니다."
      }]
});
