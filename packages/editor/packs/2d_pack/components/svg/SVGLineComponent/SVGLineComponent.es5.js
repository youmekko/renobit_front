"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var SVGLineComponent = function(_WVPointComponent) {
    _inherits(SVGLineComponent, _WVPointComponent);

    function SVGLineComponent() {
        _classCallCheck(this, SVGLineComponent);

        return _possibleConstructorReturn(this, (SVGLineComponent.__proto__ || Object.getPrototypeOf(SVGLineComponent)).call(this));
    }

    _createClass(SVGLineComponent, [{
        key: "_createDefaultPoints",
        value: function _createDefaultPoints() {
            return [new Point(this.x, this.y), new Point(this.x + this.width, this.y + this.height)];
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this._element = CPUtil.getInstance().createSVGElement("path");
            this.appendElement.appendChild(this.element);
        }
    }, {
        key: "handleTransformList",
        value: function handleTransformList() {
            var results = [];
            var idx = void 0;
            for (idx = 0; idx < this.points.length; idx++) {
                results.push("point-anchor");
            }
            return results;
        }
    }, {
        key: "_notifyInserPoint",
        value: function _notifyInserPoint(point) {
            this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.INSERT_POINT, this, {
                point: point,
                type: "point-anchor"
            }));
        }
    }, {
        key: "_notifyRemovePoint",
        value: function _notifyRemovePoint(point) {
            this.notifyComponentEvent(new WVPointComponentEvent(WVPointComponentEvent.REMOVE_POINT, this, {
                point: point
            }));
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            _get(SVGLineComponent.prototype.__proto__ || Object.getPrototypeOf(SVGLineComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "paths",
        get: function get() {
            var path = [];
            var pointList = this.points.concat();
            var currentPoint = pointList.shift();
            path.push(this.moveTo(currentPoint));
            for (var i = 0; i < pointList.length; i++) {
                path.push(this.lineTo(pointList[i]));
            }
            return Snap.parsePathString(path);
        }
    }]);

    return SVGLineComponent;
}(WVPointComponent);

WVPropertyManager.attach_default_component_infos(SVGLineComponent, {
    "info": {
        "componentName": "SVGLineComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "startMarker": false,
        "endMarker": false,
        "pointCount": 2
    },

      "strokeStyle" : {
            "type": "solid",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#ff00ff",
            "color2": "#000",
            "text": '',
            "width" : 1,
            "stroke" : "#ff00ff",
            "info" : "stroke"
      },

      "shadow": {
            "xPosition": '0',
            "yPosition": '0',
            "blur": '0',
            "color": 'rgb(255,255,255)'
      }
});

WVPropertyManager.add_property_group_info(SVGLineComponent, {
    label: "SVGLineComponent 고유 속성",
    children: [{
        name: "pointCount",
        type: "number",
        show: true,
        writable: true,
        description: "라인을 그릴 때 사용할 포인트 수 입니다."
    }, {
        name: "starMarker",
        type: "boolean",
        show: true,
        writable: true,
        description: "라인 시작점에 화살표 마커 사용여부를 설정합니다."
    }, {
        name: "endMarker",
        type: "boolean",
        show: true,
        writable: true,
        description: "라인 끝점에 화살표 마커 사용여부를 설정합니다."
    }, {
          name: "stroke",
          type: "string",
          owner: "style",
          show: true,
          writable: true,
          description: "라인 컬러에 적용할 hex값입니다(rgb, rgba형식 사용가능)."
    }, {
        name: "stroke-width",
        type: "number",
        owner: "style",
        show: true,
        writable: true,
        description: "라인 두께를 설정합니다."
    }]
});


SVGLineComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
      label: "Stroke",
      template: "stroke-gradient",
      owner : "strokeStyle",
}, {
    label: "Arrow",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "startMarker",
        type: "checkbox",
        label: "Start",
        show: true,
        writable: true,
        description: "Start Arrow Icon"
    }, {
        owner: "setter",
        name: "endMarker",
        type: "checkbox",
        label: "End",
        show: true,
        writable: true,
        description: "Start Arrow Icon"
    }]
}, {
      label: "Point",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "Length",
            show: true,
            writable: true,
            description: "point",
            options: {
                  min: 2,
                  max: 50
            }
      }]
}, {
      label : "Shadow",
      template : "shadow"
}];

WVPropertyManager.remove_property_group_info(SVGLineComponent, "label");
WVPropertyManager.remove_property_group_info(SVGLineComponent, "background");
