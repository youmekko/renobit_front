"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SeverityComponent004 = function(_WVDOMComponent) {
    _inherits(SeverityComponent004, _WVDOMComponent);

    function SeverityComponent004() {
        _classCallCheck(this, SeverityComponent004);

        return _possibleConstructorReturn(this, (SeverityComponent004.__proto__ || Object.getPrototypeOf(SeverityComponent004)).call(this));
    }

    _createClass(SeverityComponent004, [{
        key: "_onDestroy",
        value: function _onDestroy() {
            _get(SeverityComponent004.prototype.__proto__ || Object.getPrototypeOf(SeverityComponent004.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            $(this._element).append("<div class='wrapper'></div>");

            this.$wrapper = $(this._element).find(".wrapper");

            this.imgWidth = 300;
            this.imgHeight = 155;
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._invalidateSeverity) {
                this.validateCallLater(this._validateSeverity);
                this._invalidateSeverity = false;
            }

            if (this._updatePropertiesMap.has("setter.width") || this._updatePropertiesMap.has("setter.height")) {
                this.validateCallLater(this._validatecCalcScale);
            }
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this._validateSeverity();
            this._validatecCalcScale();
        }
    }, {
        key: "_validateSeverity",
        value: function _validateSeverity() {
            this.$wrapper.empty().attr("class", "wrapper " + this.severity);

            if (this.severity == 'critical') {
                this.$wrapper.append("<div class='bullet'></div>");
            }
        }
    }, {
        key: "_validatecCalcScale",
        value: function _validatecCalcScale() {
            var scaleX = 1 / this.imgWidth * this.width;
            var scaleY = 1 / this.imgHeight * this.height;

            this.$wrapper.css({
                "transform": "scale(" + scaleX + "," + scaleY + ")",
                "width": this.imgWidth,
                "height": this.imgHeight,
                "background-position": "0 0"
            });
        }
    }, {
        key: "severity",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "severity", value)) {
                this._invalidateSeverity = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "severity");
        }
    }]);

    return SeverityComponent004;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(SeverityComponent004, {
    "info": {
        "componentName": "SeverityComponent004",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 155,
        "severity": "critical"
    },

    "label": {
        "label_using": "N",
        "label_text": "Severity Box Component"
    }
});

SeverityComponent004.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    label: "Status",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [{ label: "critical", value: "critical" }, { label: "major", value: "major" }, { label: "minor", value: "minor" }, { label: "warning", value: "warning" }, { label: "normal", value: "normal" }]
        }
    }]
}];

WVPropertyManager.add_property_group_info(SeverityComponent004, {
    label: "SeverityComponent004 고유 속성",
    children: [{
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
