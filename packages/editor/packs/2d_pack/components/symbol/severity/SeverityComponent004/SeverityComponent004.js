class SeverityComponent004 extends WVDOMComponent {

    constructor() {
        super();
    }

    _onDestroy() {
        super._onDestroy();
    }

    _onCreateElement() {
        $(this._element).append("<div class='wrapper'></div>");

        this.$wrapper = $(this._element).find(".wrapper");

        this.imgWidth = 300;
        this.imgHeight = 155;
    }

    _onCommitProperties() {
        if (this._invalidateSeverity) {
            this.validateCallLater(this._validateSeverity);
            this._invalidateSeverity = false;
        }

        if (this._updatePropertiesMap.has("setter.width") ||
            this._updatePropertiesMap.has("setter.height")) {
            this.validateCallLater(this._validatecCalcScale);
        }
    }

    _onImmediateUpdateDisplay() {
        this._validateSeverity();
        this._validatecCalcScale();
    }

    _validateSeverity() {
        this.$wrapper.empty().attr("class", "wrapper " + this.severity);

        if (this.severity == 'critical') {
            this.$wrapper.append("<div class='bullet'></div>");
        }
    }

    _validatecCalcScale() {
        var scaleX = 1 / this.imgWidth * this.width;
        var scaleY = 1 / this.imgHeight * this.height;

        this.$wrapper.css({
            "transform": "scale(" + scaleX + "," + scaleY + ")",
            "width": this.imgWidth,
            "height": this.imgHeight,
            "background-position": "0 0"
        });
    }

    set severity(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "severity", value)) {
            this._invalidateSeverity = true;
        }
    }

    get severity() {
        return this.getGroupPropertyValue("setter", "severity");
    }
}

WVPropertyManager.attach_default_component_infos(SeverityComponent004, {
    "info": {
        "componentName": "SeverityComponent004",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 155,
        "severity": "critical"
    },

    "label": {
        "label_using": "N",
        "label_text": "Severity Box Component"
    }
});

SeverityComponent004.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    label: "Status",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [
                { label: "critical", value: "critical" },
                { label: "major", value: "major" },
                { label: "minor", value: "minor" },
                { label: "warning", value: "warning" },
                { label: "normal", value: "normal" }
            ]
        }
    }]
}];


WVPropertyManager.add_property_group_info(SeverityComponent004, {
    label: "SeverityComponent004 고유 속성",
    children: [{
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
