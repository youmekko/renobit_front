class SeverityComponent002 extends WVDOMComponent {

    constructor() {
        super();
    }

    _onDestroy() {
        this.$title = null;
        super._onDestroy();
    }

    _onCreateElement() {
        this._element.innerHTML = "<div class='container'>" +
            "<div class='gradient'></div>" +
            "<div class='title-area'>" +
            "<p>" + this.title + "</p>" +
            "</div>" +
            "</div>";

        this.$title = $(this._element).find("p");
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFont);
        }

        if (this._invalidateSeverity) {
            this.validateCallLater(this._validateSeverity);
            this._invalidateSeverity = false;
        }

        if (this._invalidateTitle) {
            this.validateCallLater(this._validateTitle);
            this._invalidateTitle = false;
        }

        if (this._updatePropertiesMap.has("setter.height")) {
            this.validateCallLater(this._validateHeight);
        }
    }

    _onImmediateUpdateDisplay() {
        this._validateSeverity();
        this._validateFont();
    }

    _validateSeverity() {
        var severityColor = this.getGroupPropertyValue("colors", this.severity);
        $(this._element).find(".title-area").css("background-color", severityColor);

        $(this._element).find(".gradient")
            .css("background", '-webkit-linear-gradient(transparent, ' + severityColor + ')')
            .css("background", '-ms-linear-gradient(transparent, ' + severityColor + ')');
    }

    _validateTitle() {
        this.$title.text(this.title);
    }

    _validateFont() {
        var font = this.getGroupProperties("font");

        this.$title.css({
            "font-size": font.font_size,
            "color": font.font_color,
            "font-weight": font.font_weight,
            "fontFamily": font.font_type
        });
    }

    _validateHeight() {
        $(this._element).find(".title-area").css({
            "top": (this.height * 0.6).toFixed(0),
            "height": (this.height * 0.4).toFixed(0)
        });

        $(this._element).find(".gradient").css({
            "height": (this.height * 0.6).toFixed(0)
        });
    }

    set severity(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "severity", value)) {
            this._invalidateSeverity = true;
        }
    }

    get severity() {
        return this.getGroupPropertyValue("setter", "severity");
    }

    set title(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "title", value)) {
            this._invalidateTitle = true;
        }
    }

    get title() {
        return this.getGroupPropertyValue("setter", "title");
    }
}

WVPropertyManager.attach_default_component_infos(SeverityComponent002, {
    "info": {
        "componentName": "SeverityComponent002",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "severity": "critical",
        "title": "SeverityComponent002"
    },

    "label": {
        "label_using": "N",
        "label_text": "Severity Box Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },
    "colors": {
        "critical": "#ff0000",
        "major": "#FF9900",
        "minor": "#ffff00",
        "warning": "#0000ff",
        "normal": "#00ff00",
    }
});

SeverityComponent002.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    template: "font"
}, {
    label: "Status",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "title",
        type: "string",
        label: "Title",
        description: "title"
    }, {
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [
                { label: "critical", value: "critical" },
                { label: "major", value: "major" },
                { label: "minor", value: "minor" },
                { label: "warning", value: "warning" },
                { label: "normal", value: "normal" }
            ]
        }
    }]
}, {
    label: "Severity Color",
    owner: "vertical",
    children: [{
        owner: "colors",
        name: "critical",
        type: "color",
        label: "Critical",
        description: "critical"
    }, {
        owner: "colors",
        name: "major",
        type: "color",
        label: "Major",
        description: "major"
    }, {
        owner: "colors",
        name: "minor",
        type: "color",
        label: "Minor",
        description: "minor"
    }, {
        owner: "colors",
        name: "warning",
        type: "color",
        label: "Warning",
        description: "warning"
    }, {
        owner: "colors",
        name: "normal",
        type: "color",
        label: "Normal",
        description: "normal"
    }]
}];


WVPropertyManager.add_property_group_info(SeverityComponent002, {
    label: "SeverityComponent002 고유 속성",
    children: [{
        name: "title",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'title'",
        description: "컴포넌트 내에 출력되는 텍스트 정보입니다."
    }, {
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
