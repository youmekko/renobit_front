"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SeverityComponent002 = function(_WVDOMComponent) {
    _inherits(SeverityComponent002, _WVDOMComponent);

    function SeverityComponent002() {
        _classCallCheck(this, SeverityComponent002);

        return _possibleConstructorReturn(this, (SeverityComponent002.__proto__ || Object.getPrototypeOf(SeverityComponent002)).call(this));
    }

    _createClass(SeverityComponent002, [{
        key: "_onDestroy",
        value: function _onDestroy() {
            this.$title = null;
            _get(SeverityComponent002.prototype.__proto__ || Object.getPrototypeOf(SeverityComponent002.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this._element.innerHTML = "<div class='container'>" + "<div class='gradient'></div>" + "<div class='title-area'>" + "<p>" + this.title + "</p>" + "</div>" + "</div>";

            this.$title = $(this._element).find("p");
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this._validateFont);
            }

            if (this._invalidateSeverity) {
                this.validateCallLater(this._validateSeverity);
                this._invalidateSeverity = false;
            }

            if (this._invalidateTitle) {
                this.validateCallLater(this._validateTitle);
                this._invalidateTitle = false;
            }

            if (this._updatePropertiesMap.has("setter.height")) {
                this.validateCallLater(this._validateHeight);
            }
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this._validateSeverity();
            this._validateFont();
        }
    }, {
        key: "_validateSeverity",
        value: function _validateSeverity() {
            var severityColor = this.getGroupPropertyValue("colors", this.severity);
            $(this._element).find(".title-area").css("background-color", severityColor);

            $(this._element).find(".gradient").css("background", '-webkit-linear-gradient(transparent, ' + severityColor + ')').css("background", '-ms-linear-gradient(transparent, ' + severityColor + ')');
        }
    }, {
        key: "_validateTitle",
        value: function _validateTitle() {
            this.$title.text(this.title);
        }
    }, {
        key: "_validateFont",
        value: function _validateFont() {
            var font = this.getGroupProperties("font");

            this.$title.css({
                "font-size": font.font_size,
                "color": font.font_color,
                "font-weight": font.font_weight,
                "fontFamily": font.font_type
            });
        }
    }, {
        key: "_validateHeight",
        value: function _validateHeight() {
            $(this._element).find(".title-area").css({
                "top": (this.height * 0.6).toFixed(0),
                "height": (this.height * 0.4).toFixed(0)
            });

            $(this._element).find(".gradient").css({
                "height": (this.height * 0.6).toFixed(0)
            });
        }
    }, {
        key: "severity",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "severity", value)) {
                this._invalidateSeverity = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "severity");
        }
    }, {
        key: "title",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "title", value)) {
                this._invalidateTitle = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "title");
        }
    }]);

    return SeverityComponent002;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(SeverityComponent002, {
    "info": {
        "componentName": "SeverityComponent002",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "severity": "critical",
        "title": "SeverityComponent002"
    },

    "label": {
        "label_using": "N",
        "label_text": "Severity Box Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },
    "colors": {
        "critical": "#ff0000",
        "major": "#FF9900",
        "minor": "#ffff00",
        "warning": "#0000ff",
        "normal": "#00ff00"
    }
});

SeverityComponent002.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    template: "font"
}, {
    label: "Status",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "title",
        type: "string",
        label: "Title",
        description: "title"
    }, {
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [{ label: "critical", value: "critical" }, { label: "major", value: "major" }, { label: "minor", value: "minor" }, { label: "warning", value: "warning" }, { label: "normal", value: "normal" }]
        }
    }]
}, {
    label: "Severity Color",
    owner: "vertical",
    children: [{
        owner: "colors",
        name: "critical",
        type: "color",
        label: "Critical",
        description: "critical"
    }, {
        owner: "colors",
        name: "major",
        type: "color",
        label: "Major",
        description: "major"
    }, {
        owner: "colors",
        name: "minor",
        type: "color",
        label: "Minor",
        description: "minor"
    }, {
        owner: "colors",
        name: "warning",
        type: "color",
        label: "Warning",
        description: "warning"
    }, {
        owner: "colors",
        name: "normal",
        type: "color",
        label: "Normal",
        description: "normal"
    }]
}];

WVPropertyManager.add_property_group_info(SeverityComponent002, {
    label: "SeverityComponent002 고유 속성",
    children: [{
        name: "title",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'title'",
        description: "컴포넌트 내에 출력되는 텍스트 정보입니다."
    }, {
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
