"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SeverityComponent006 = function(_SeverityCanvas) {
    _inherits(SeverityComponent006, _SeverityCanvas);

    function SeverityComponent006() {
        _classCallCheck(this, SeverityComponent006);

        var _this = _possibleConstructorReturn(this, (SeverityComponent006.__proto__ || Object.getPrototypeOf(SeverityComponent006)).call(this));

        _this.center = null;
        return _this;
    }

    _createClass(SeverityComponent006, [{
        key: "_drawCircle",
        value: function _drawCircle(center) {
            var startAngle = 90 * Math.PI / 180;
            var lineWidth = center * 0.1;
            var blurValue = 8;
            this.ctx.save();
            this.ctx.beginPath();
            var colorInfo = CPUtil.getInstance().hex2rgba(this._severityVO.colors[this._severityVO.state]);
            this.ctx.fillStyle = "rgba(" + colorInfo.r + "," + colorInfo.g + "," + colorInfo.b + ", 0.5)";
            this.ctx.arc(center, center, center - lineWidth, startAngle, startAngle + Math.PI);
            this.ctx.fill();

            this.ctx.beginPath();
            this.ctx.strokeStyle = "rgb(" + colorInfo.r + "," + colorInfo.g + "," + colorInfo.b + ")";
            this.ctx.lineWidth = lineWidth;
            this.ctx.shadowBlur = blurValue;
            this.ctx.shadowColor = "rgb(" + colorInfo.r + "," + colorInfo.g + "," + colorInfo.b + ")";
            this.ctx.arc(center, center, center - this.ctx.lineWidth, 0, Math.PI * 2);
            this.ctx.stroke();
            this.ctx.closePath();
            this.ctx.restore();
        }
    }, {
        key: "_render",
        value: function _render() {
            this.clear();
            var center = (this.width - 1) / 2;
            this._drawCircle(center);
        }
    }]);

    return SeverityComponent006;
}(SeverityCanvas);

WVPropertyManager.attach_default_component_infos(SeverityComponent006, {
    "info": {
        "componentName": "SeverityComponent006",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "severity": "critical"
    },

    "label": {
        "label_using": "N",
        "label_text": "SeverityComponent006"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },
    "colors": {
        "critical": "#ff0000",
        "major": "#FF9900",
        "minor": "#ffff00",
        "warning": "#0000ff",
        "normal": "#00ff00"
    }
});

SeverityComponent006.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    label: "Status",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [{ label: "critical", value: "critical" }, { label: "major", value: "major" }, { label: "minor", value: "minor" }, { label: "warning", value: "warning" }, { label: "normal", value: "normal" }]
        }
    }]
}, {
    label: "Severity Color",
    owner: "vertical",
    children: [{
        owner: "colors",
        name: "critical",
        type: "color",
        label: "Critical",
        description: "critical"
    }, {
        owner: "colors",
        name: "major",
        type: "color",
        label: "Major",
        description: "major"
    }, {
        owner: "colors",
        name: "minor",
        type: "color",
        label: "Minor",
        description: "minor"
    }, {
        owner: "colors",
        name: "warning",
        type: "color",
        label: "Warning",
        description: "warning"
    }, {
        owner: "colors",
        name: "normal",
        type: "color",
        label: "Normal",
        description: "normal"
    }]
}];

WVPropertyManager.add_property_group_info(SeverityComponent006, {
    label: "SeverityComponent006 고유 속성",
    children: [{
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
