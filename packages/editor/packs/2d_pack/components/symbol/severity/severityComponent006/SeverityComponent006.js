class SeverityComponent006 extends SeverityCanvas {
    constructor() {
        super();
        this.center = null;
    }

    _drawCircle(center) {
        let startAngle = 90 * Math.PI / 180;
        let lineWidth = center * 0.1;
        let blurValue = 8;
        this.ctx.save();
        this.ctx.beginPath();
        let colorInfo = CPUtil.getInstance().hex2rgba(this._severityVO.colors[this._severityVO.state]);
        this.ctx.fillStyle = `rgba(${colorInfo.r},${colorInfo.g},${colorInfo.b}, 0.5)`;
        this.ctx.arc(center, center, center - lineWidth, startAngle, startAngle + Math.PI);
        this.ctx.fill();

        this.ctx.beginPath();
        this.ctx.strokeStyle = `rgb(${colorInfo.r},${colorInfo.g},${colorInfo.b})`;
        this.ctx.lineWidth = lineWidth;
        this.ctx.shadowBlur = blurValue;
        this.ctx.shadowColor = "rgb(" + colorInfo.r + "," + colorInfo.g + "," + colorInfo.b + ")";
        this.ctx.arc(center, center, center - (this.ctx.lineWidth), 0, Math.PI * 2);
        this.ctx.stroke();
        this.ctx.closePath();
        this.ctx.restore();
    }



    _render() {
        this.clear();
        let center = (this.width - 1) / 2;
        this._drawCircle(center);
    }




}


WVPropertyManager.attach_default_component_infos(SeverityComponent006, {
    "info": {
        "componentName": "SeverityComponent006",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "severity": "critical",
    },

    "label": {
        "label_using": "N",
        "label_text": "SeverityComponent006"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },
    "colors": {
        "critical": "#ff0000",
        "major": "#FF9900",
        "minor": "#ffff00",
        "warning": "#0000ff",
        "normal": "#00ff00",
    }
});

SeverityComponent006.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    label: "Status",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [
                { label: "critical", value: "critical" },
                { label: "major", value: "major" },
                { label: "minor", value: "minor" },
                { label: "warning", value: "warning" },
                { label: "normal", value: "normal" }
            ]
        }
    }]
}, {
    label: "Severity Color",
    owner: "vertical",
    children: [{
        owner: "colors",
        name: "critical",
        type: "color",
        label: "Critical",
        description: "critical"
    }, {
        owner: "colors",
        name: "major",
        type: "color",
        label: "Major",
        description: "major"
    }, {
        owner: "colors",
        name: "minor",
        type: "color",
        label: "Minor",
        description: "minor"
    }, {
        owner: "colors",
        name: "warning",
        type: "color",
        label: "Warning",
        description: "warning"
    }, {
        owner: "colors",
        name: "normal",
        type: "color",
        label: "Normal",
        description: "normal"
    }]
}];


WVPropertyManager.add_property_group_info(SeverityComponent006, {
    label: "SeverityComponent006 고유 속성",
    children: [{
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
