"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SeverityComponent005 = function(_SeverityCanvas) {
    _inherits(SeverityComponent005, _SeverityCanvas);

    function SeverityComponent005() {
        _classCallCheck(this, SeverityComponent005);

        var _this = _possibleConstructorReturn(this, (SeverityComponent005.__proto__ || Object.getPrototypeOf(SeverityComponent005)).call(this));

        _this.tween = null;
        _this.spinnerAngle = 40 * Math.PI / 180;
        _this.center = null;
        return _this;
    }

    _createClass(SeverityComponent005, [{
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            _get(SeverityComponent005.prototype.__proto__ || Object.getPrototypeOf(SeverityComponent005.prototype), "_onCommitProperties", this).call(this);
            if (this._updatePropertiesMap.has("setter.duration")) {
                this._render();
            }
        }
    }, {
        key: "stop",
        value: function stop() {
            if (this.tween != null && this.tween.isActive()) {
                this.tween.pause();
            }
        }
    }, {
        key: "play",
        value: function play() {
            var _this2 = this;

            this.stop();
            var tweenObj = {
                value: 0
            };
            this.tween = TweenMax.to(tweenObj, parseInt(this.duration) / 1000, {
                value: Math.PI * 2,
                onUpdate: function onUpdate() {
                    var center = _this2.width / 2;
                    _this2.clear();
                    _this2._drawCircle(center);
                    _this2._drawSpinnerWithAngle(center, tweenObj.value);
                },
                ease: Power0.easeNone,
                repeat: -1
            });
        }
    }, {
        key: "_drawCircle",
        value: function _drawCircle(center) {
            this.ctx.save();
            this.ctx.beginPath();
            this.ctx.fillStyle = "#333";
            this.ctx.arc(center, center, center, 0, Math.PI * 2);
            this.ctx.fill();
            this.ctx.beginPath();
            this.ctx.fillStyle = this._severityVO.colors[this._severityVO.state];
            this.ctx.arc(center, center, center * 0.8, 0, Math.PI * 2);
            this.ctx.fill();
            this.ctx.closePath();
            this.ctx.restore();
        }
    }, {
        key: "_drawSpinnerWithAngle",
        value: function _drawSpinnerWithAngle(center, angle) {
            this.ctx.save();
            var lineWidth = center * 0.2;
            this.ctx.strokeStyle = "#ffffff";
            this.ctx.lineWidth = lineWidth;
            this.ctx.beginPath();
            this.ctx.arc(center, center, center - lineWidth / 2, angle, angle + this.spinnerAngle);
            this.ctx.stroke();
            this.ctx.closePath();
            this.ctx.restore();
        }
    }, {
        key: "_render",
        value: function _render() {
            this.clear();
            var center = (this.width - 1) / 2;
            this._drawCircle(center);
            this._drawSpinnerWithAngle(center, 0);
            if (this.isViewerMode) {
                this.play();
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.stop();
            this.tween = null;
            _get(SeverityComponent005.prototype.__proto__ || Object.getPrototypeOf(SeverityComponent005.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "duration",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "duration", value)) {}
        },
        get: function get() {
            return parseInt(this.getGroupPropertyValue("setter", "duration"));
        }
    }]);

    return SeverityComponent005;
}(SeverityCanvas);

WVPropertyManager.attach_default_component_infos(SeverityComponent005, {
    "info": {
        "componentName": "SeverityComponent005",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "severity": "critical",
        "duration": 2000
    },

    "label": {
        "label_using": "N",
        "label_text": "SeverityComponent005"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },
    "colors": {
        "critical": "#ff0000",
        "major": "#FF9900",
        "minor": "#ffff00",
        "warning": "#0000ff",
        "normal": "#00ff00"
    }
});

SeverityComponent005.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    label: "Status",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "duration",
        label: "Duration",
        type: "number"
    }, {
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [{ label: "critical", value: "critical" }, { label: "major", value: "major" }, { label: "minor", value: "minor" }, { label: "warning", value: "warning" }, { label: "normal", value: "normal" }]
        }
    }]
}, {
    label: "Severity Color",
    owner: "vertical",
    children: [{
        owner: "colors",
        name: "critical",
        type: "color",
        label: "Critical",
        description: "critical"
    }, {
        owner: "colors",
        name: "major",
        type: "color",
        label: "Major",
        description: "major"
    }, {
        owner: "colors",
        name: "minor",
        type: "color",
        label: "Minor",
        description: "minor"
    }, {
        owner: "colors",
        name: "warning",
        type: "color",
        label: "Warning",
        description: "warning"
    }, {
        owner: "colors",
        name: "normal",
        type: "color",
        label: "Normal",
        description: "normal"
    }]
}];

WVPropertyManager.add_property_group_info(SeverityComponent005, {
    label: "SeverityComponent005 고유 속성",
    children: [{
        name: "duration",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "2000",
        description: "외각선이 한바퀴를 회전하는 속도를 설정합니다.(단위는 밀리초를 이용합니다)"

    }, {
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
