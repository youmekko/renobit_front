class SeverityComponent005 extends SeverityCanvas {
    constructor() {
        super();
        this.tween = null;
        this.spinnerAngle = 40 * Math.PI / 180;
        this.center = null;
    }

    set duration(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "duration", value)) {

        }
    }

    get duration() {
        return parseInt(this.getGroupPropertyValue("setter", "duration"));
    }

    _onCommitProperties() {
        super._onCommitProperties();
        if (this._updatePropertiesMap.has("setter.duration")) {
            this._render();
        }
    }

    stop() {
        if (this.tween != null && this.tween.isActive()) {
            this.tween.pause();
        }
    }

    play() {
        this.stop();
        var tweenObj = {
            value: 0
        };
        this.tween = TweenMax.to(
            tweenObj, parseInt(this.duration) / 1000, {
                value: Math.PI * 2,
                onUpdate: () => {
                    let center = this.width / 2;
                    this.clear();
                    this._drawCircle(center);
                    this._drawSpinnerWithAngle(center, tweenObj.value);
                },
                ease: Power0.easeNone,
                repeat: -1
            }
        );
    }

    _drawCircle(center) {
        this.ctx.save();
        this.ctx.beginPath();
        this.ctx.fillStyle = "#333";
        this.ctx.arc(center, center, center, 0, Math.PI * 2);
        this.ctx.fill();
        this.ctx.beginPath();
        this.ctx.fillStyle = this._severityVO.colors[this._severityVO.state];
        this.ctx.arc(center, center, center * 0.8, 0, Math.PI * 2);
        this.ctx.fill();
        this.ctx.closePath();
        this.ctx.restore();
    }

    _drawSpinnerWithAngle(center, angle) {
        this.ctx.save();
        let lineWidth = center * 0.2;
        this.ctx.strokeStyle = "#ffffff";
        this.ctx.lineWidth = lineWidth;
        this.ctx.beginPath();
        this.ctx.arc(center, center, center - (lineWidth / 2), angle, angle + this.spinnerAngle);
        this.ctx.stroke();
        this.ctx.closePath();
        this.ctx.restore();
    }

    _render() {
        this.clear();
        let center = (this.width - 1) / 2;
        this._drawCircle(center);
        this._drawSpinnerWithAngle(center, 0);
        if (this.isViewerMode) {
            this.play();
        }
    }

    _onDestroy() {
        this.stop();
        this.tween = null;
        super._onDestroy();
    }


}


WVPropertyManager.attach_default_component_infos(SeverityComponent005, {
    "info": {
        "componentName": "SeverityComponent005",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "severity": "critical",
        "duration": 2000
    },

    "label": {
        "label_using": "N",
        "label_text": "SeverityComponent005"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },
    "colors": {
        "critical": "#ff0000",
        "major": "#FF9900",
        "minor": "#ffff00",
        "warning": "#0000ff",
        "normal": "#00ff00",
    }
});

SeverityComponent005.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    label: "Status",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "duration",
        label: "Duration",
        type: "number"
    }, {
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [
                { label: "critical", value: "critical" },
                { label: "major", value: "major" },
                { label: "minor", value: "minor" },
                { label: "warning", value: "warning" },
                { label: "normal", value: "normal" }
            ]
        }
    }]
}, {
    label: "Severity Color",
    owner: "vertical",
    children: [{
        owner: "colors",
        name: "critical",
        type: "color",
        label: "Critical",
        description: "critical"
    }, {
        owner: "colors",
        name: "major",
        type: "color",
        label: "Major",
        description: "major"
    }, {
        owner: "colors",
        name: "minor",
        type: "color",
        label: "Minor",
        description: "minor"
    }, {
        owner: "colors",
        name: "warning",
        type: "color",
        label: "Warning",
        description: "warning"
    }, {
        owner: "colors",
        name: "normal",
        type: "color",
        label: "Normal",
        description: "normal"
    }]
}];


WVPropertyManager.add_property_group_info(SeverityComponent005, {
    label: "SeverityComponent005 고유 속성",
    children: [{
        name: "duration",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "2000",
        description: "외각선이 한바퀴를 회전하는 속도를 설정합니다.(단위는 밀리초를 이용합니다)"

    }, {
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
