class SeverityComponent008 extends SeverityCanvas {
    constructor() {
        super();
        this.center = null;
    }

    _drawShape(center) {
        this.ctx.save();
        let colorInfo = CPUtil.getInstance().hex2rgba(this._severityVO.colors[this._severityVO.state]);
        let blurValue = 8;
        let lineWidth = center * 0.1;
        let lineColor = `rgb(${colorInfo.r},${colorInfo.g},${colorInfo.b})`;
        this.ctx.beginPath();
        this.ctx.fillStyle = `rgba(${colorInfo.r},${colorInfo.g},${colorInfo.b}, 0.5)`;
        this.ctx.fillRect(lineWidth, lineWidth, center - lineWidth, this.height - (lineWidth * 2));
        this.ctx.closePath();
        this.ctx.beginPath();
        this.ctx.strokeStyle = lineColor;
        this.ctx.lineWidth = lineWidth;
        this.ctx.shadowBlur = blurValue;
        this.ctx.shadowColor = lineColor;
        this.ctx.strokeRect((lineWidth + blurValue) / 2, (lineWidth + blurValue) / 2, this.width - (lineWidth + blurValue), this.height - (lineWidth + blurValue));
        this.ctx.closePath();
        this.ctx.restore();
    }



    _render() {
        this.clear();
        let center = (this.width - 1) / 2;
        this._drawShape(center);
    }




}


WVPropertyManager.attach_default_component_infos(SeverityComponent008, {
    "info": {
        "componentName": "SeverityComponent008",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "severity": "critical",
    },

    "label": {
        "label_using": "N",
        "label_text": "SeverityComponent008"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },
    "colors": {
        "critical": "#ff0000",
        "major": "#FF9900",
        "minor": "#ffff00",
        "warning": "#0000ff",
        "normal": "#00ff00",
    }
});

SeverityComponent008.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    label: "Default Setting",
    owner: "vertical",
    children: [{
        owner: "setter",
        name: "severity",
        label: "Severity",
        type: "select",
        options: {
            items: [
                { label: "critical", value: "critical" },
                { label: "major", value: "major" },
                { label: "minor", value: "minor" },
                { label: "warning", value: "warning" },
                { label: "normal", value: "normal" }
            ]
        }
    }]
}, {
    label: "Severity Color",
    owner: "vertical",
    children: [{
        owner: "colors",
        name: "critical",
        type: "color",
        label: "Critical",
        description: "critical"
    }, {
        owner: "colors",
        name: "major",
        type: "color",
        label: "Major",
        description: "major"
    }, {
        owner: "colors",
        name: "minor",
        type: "color",
        label: "Minor",
        description: "minor"
    }, {
        owner: "colors",
        name: "warning",
        type: "color",
        label: "Warning",
        description: "warning"
    }, {
        owner: "colors",
        name: "normal",
        type: "color",
        label: "Normal",
        description: "normal"
    }]
}];


WVPropertyManager.add_property_group_info(SeverityComponent008, {
    label: "SeverityComponent008 고유 속성",
    children: [{
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
});
