class SeverityCanvas extends WVSeverityComponent {
    constructor() {
        super();
        this.canvas = null;
        this.ctx = null;
    }

    _onCreateElement() {
        this.canvas = document.createElement("canvas");
        this.ctx = this.canvas.getContext("2d");
        $(this.element).append(this.canvas);
    }

    clear() {
        this.ctx.clearRect(0, 0, this.width, this.height);
    }

    _validateSize() {
        super._validateSize();
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this._render();
    }

    _onDestroy(){
          this.ctx    = null;
          this.canvas = null;
          super._onDestroy();
    }

}
