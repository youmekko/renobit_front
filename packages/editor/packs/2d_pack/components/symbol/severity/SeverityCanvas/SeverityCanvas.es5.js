"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SeverityCanvas = function (_WVSeverityComponent) {
      _inherits(SeverityCanvas, _WVSeverityComponent);

      function SeverityCanvas() {
            _classCallCheck(this, SeverityCanvas);

            var _this = _possibleConstructorReturn(this, (SeverityCanvas.__proto__ || Object.getPrototypeOf(SeverityCanvas)).call(this));

            _this.canvas = null;
            _this.ctx = null;
            return _this;
      }

      _createClass(SeverityCanvas, [{
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  this.canvas = document.createElement("canvas");
                  this.ctx = this.canvas.getContext("2d");
                  $(this.element).append(this.canvas);
            }
      }, {
            key: "clear",
            value: function clear() {
                  this.ctx.clearRect(0, 0, this.width, this.height);
            }
      }, {
            key: "_validateSize",
            value: function _validateSize() {
                  _get(SeverityCanvas.prototype.__proto__ || Object.getPrototypeOf(SeverityCanvas.prototype), "_validateSize", this).call(this);
                  this.canvas.width = this.width;
                  this.canvas.height = this.height;
                  this._render();
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  this.ctx = null;
                  this.canvas = null;
                  _get(SeverityCanvas.prototype.__proto__ || Object.getPrototypeOf(SeverityCanvas.prototype), "_onDestroy", this).call(this);
            }
      }]);

      return SeverityCanvas;
}(WVSeverityComponent);
