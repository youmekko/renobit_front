class StateClipComponent extends WVDOMComponent {

    constructor() {
        super();
        this.isResourceLoaded = false;
        this.isSettingsReady = false;
        this.$container = null;
        this.animateClip = null;
        this.selectItem = null;
        this.spriteList = {};

        this.settingObj = {
            width: "",
            height: "",
            fps: 24,
            loop: true,
            columns: "",
            autoplay: false,
            totalFrames: ""
        };

        this.publicMotionOptions = null;
        this._invalidateSelectItem = false;
        this._invalidateServerity = false;
        this._isDestroyed = false;
    }

    _onImmediateUpdateDisplay() {
        //   this.isSettingsReady = true;
        this._validateSelectProperty();
    }

    _onCreateElement() {
        $(this._element).append('<div class="state-clip-wrap" style="width:100%;height:100%;overflow:hidden;"></div>');

        if (!this.$container) {
            this.createContainer();
        }
    }

    onLoadPage() {
          if (this.autoPlay && !this.isEditorMode) {
                this.loadedPage = true;
                this._validateSeverityProperty();
          }
    }

    createContainer() {
        if (this.$container) {
            this.$container.remove();
        }

        if (this.animateClip) {
            this.animateClip.destroy();
        }


        $(this._element).find(".state-clip-wrap").empty();

        this.$container = $("<div><div class='sprite-image'></div></div>");
        this.$container.css({
            "position": "relative",
            "width": "100%",
            "height": "100%",
            "background-image": "url(" + StateClipComponent.DEFAULT_IMAGE + ")",
            "background-position": "center center",
            "background-repeat": "no-repeat",
            "background-color": "rgba(255, 255, 255, .6)",
            "border": "1px dashed #808080",
            "transform-origin": "left top",
            "overflow": "hidden"
        });


        this.$container.find(".sprite-image").css({
            "position": "absolute",
            "background-position": "0 0",
            "background-repeat": "no-repeat",
            "pointer-events": "none",
            "width": 5000,
            "height": 5000
        })

        if (this.settingObj.width != "") {
            this.$container.css({
                "width": this.settingObj.width,
                "height": this.settingObj.height,
                "background-position": "0 0"
            });



            this.calcScale();
        }

        $(this._element).find(".state-clip-wrap").append(this.$container);
    }

    _onDestroy() {
        this._isDestroyed = true;
        if (this.animateClip) {
            this.animateClip.destroy();
            this.animateClip = null;
        }
        this.$container.remove();
        this.$container = null;
        this.spriteList = null;
        this.settingObj = null;
        super._onDestroy();
    }


    _onCommitProperties() {
        if (this._invalidateSelectItem) {
            this._invalidateSelectItem = false;
            this.validateCallLater(this._validateSelectProperty);
        }

        if (this.invalidateSize || this.invalidateVisible) {
            this.validateCallLater(this.calcScale);
        }

        if (this._invalidateServerity) {
            this._invalidateServerity = false;
            this.validateCallLater(this._validateSeverityProperty);
        }

    }

    _validateSeverityProperty() {
        if (this.spriteList[this.severity] && this.spriteList[this.severity].type === "motion") {
            this.excuteAnimate();
        } else {
            if (!this.$container) {
                this.createContainer();
            };

            this.$container.css({
                "backgroundImage": "none",
                "backgroundColor": "transparent",
                "border": "none"
            });

            this.$container.find(".sprite-image").css({
                "backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + this.severity + ".png')"
            });


            this.calcScale();
        }

        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: this.severity
            })
        }

    }


    excuteAnimate() {
        if (this.severity) {
            if (!this.$container) {
                this.createContainer();
            };

            this.$container.css({
                "backgroundImage": "none",
                "backgroundColor": "transparent",
                "border": "none"
            });

            this.$container.find(".sprite-image").css({
                "backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + this.severity + ".png')"
            });

            this.calcScale();

            var currentState = this.spriteList[this.severity];
            if (currentState.type != undefined && currentState.type == "motion") {
                var motionOps;

                if (currentState.motionOptions) {
                    motionOps = (currentState.motionOptions != undefined) ? currentState.motionOptions : this.publicMotionOptions;
                } else {
                    motionOps = this.publicMotionOptions;
                }

                this.settingObj.totalFrames = motionOps.totalFrames;
                this.settingObj.columns = motionOps.columns;

                if (!this.settingObj.totalFrames || !this.settingObj.columns) {
                    throw this.exceptionSymbolInfo("totalFrames || columns");
                }

                if (!this.isEditorMode) {
                    if (this.animateClip) {
                        this.animateClip.destroy();
                    }

                    if (this.loadedPage) {
                          this.play();
                    }
                }
            }

        } else {
            throw this.exceptionSymbolInfo("this.severity");
        }

    }

    play() {
        this.animateClip = new AnimateSpriteClip(this.$container.find(".sprite-image").get(0), this.settingObj);
        if (!this.isEditorMode) {
            this.animateClip.play();
        }
    }

    stop() {
        if (this.animateClip) {
            this.animateClip.stop();
        }
    }


    _validateSelectProperty() {
        var info = this.selectItem;
        if (!info) {
            this.settingObj.width = "";
            this.settingObj.height = "";
            this.isSettingsReady = false;
            this.createContainer();
            //    this.savedResourceLoaded();
            return;
        }

        this.createContainer();

        this.setAnimateInfo(info).then(() => {
            //   this.savedResourceLoaded();
            this.excuteAnimate();
        }, (error) => {

        });

    }

    procAnimateInfo(filePath) {
        var locale_msg = Vue.$i18n.messages.wv;
        return new Promise((resolve, reject) => {
            wemb.$http.get(wemb.configManager.serverUrl + filePath + "info.json").then((result) => {
                resolve(result.data);
            }, (error) => {
                let msg = this.name + ":" + filePath + 'info.json Data Format Error';
                /*Vue.$notify.error({
                    title: 'Error',
                    message: msg
                });*/
                console.error(msg);
                reject();
            })
        })
    }

    setSettingObj(result) {
        if (this._isDestroyed) { return; }

        this.settingObj.width = result.width;
        this.settingObj.height = result.height;

        if (this.isEditorMode && this.isSettingsReady) {
            this.width = this.settingObj.width;
            this.height = this.settingObj.height;
            //selectItem validate 프로세스중에 size변경을 시도하여 _validateSize호출되지 않아 _validateSize강제 호출..
            this._validateSize();

            if (this.isEditorMode) {
                this.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
            }
        }

        if (result.motionOptions != undefined) {
            if (!result.motionOptions.totalFrames || !result.motionOptions.columns) throw this.exceptionSymbolInfo("공용 motionOptions 구성 값을 확인해주세요.");
            this.publicMotionOptions = result.motionOptions;
        }

        if (!this.severity) this.severity = result.default;

        if (!this.settingObj.width || !this.settingObj.height) {
            throw this.exceptionSymbolInfo("width || height");
        }

        for (var state in result.data) {
            this.spriteList[state] = result.data[state];
        }
    }

    setAnimateInfo(info) {
        return new Promise((resolve, reject) => {
            if (StateClipComponent.stateClipPool.has(info.path)) {
                this.setSettingObj(StateClipComponent.stateClipPool.get(info.path));
                resolve();
            } else {
                this.procAnimateInfo(info.path).then((result) => {
                    if (result) {
                        StateClipComponent.stateClipPool.set(info.path, result);
                        this.setSettingObj(result);
                        resolve();
                    } else {
                        reject();
                    }
                }, (error) => {
                    reject();
                });
            }
        });
    }

    /*컴포넌트 로드 완료시점 체크 */
    savedResourceLoaded() {
        if (!this.isResourceLoaded) {
            this.validateResource();
            this.isResourceLoaded = true;
        }
    }

    exceptionSymbolInfo(type) {
        console.log("StateClipComponent _ 에러가 발생하였습니다. [" + type + " ]");
    }

    calcScale() {
        if (this.$container && this.settingObj.width != "") {
            var oriWidth = parseInt(this.settingObj.width);
            var scale = 1 / oriWidth * this.width;
            this.$container.css({
                "transform": "scale(" + scale + "," + scale + ")",
                "width": this.settingObj.width,
                "height": this.settingObj.height,
                "background-position": "0 0"
            });
        }
    }

    set severity(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "severity", value)) {
            this._invalidateServerity = true;
        }
    }

    get severity() {
        return this.getGroupPropertyValue("setter", "severity");
    }

    set selectItem(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
            this._invalidateSelectItem = true;

            // 초기 셋팅 플래그값. true일 경우 사이즈를 json에서 받아온 값으로 셋
            this.isSettingsReady = true;
        }
    }

    get selectItem() {
        return this.getGroupPropertyValue("setter", "selectItem");
    }

    set autoPlay(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "autoPlay", value)) {}
    }

    get autoPlay() {
        return this.getGroupPropertyValue("setter", "autoPlay");
    }
}

StateClipComponent.DEFAULT_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAK0AAACtAB0IQVDAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA0qSURBVGiB7ZprkFzFdcd/3ffeec++l9U+JO1KSOiBJIQsZBsCGDuxIS5TJganKgkggoOxkDDPqsQfsuWKHYJBYITiMjgSckiqDElhQyrGIZjEPIUeXgmh56LHSqt9aXZ33nPn3tudD7Oa2dcMKxD+kPL/y8w5t/v0+Xf39Dl97sD/E4jzbfCJ9VsaPVihlaxTghoAIchpdEpqjiQaThzo7Ox0z/e4H5vI43c/3aQ84zqF+CNpGF/QntdQeUThaSG6hNIvKE+89MCPbtn7cX2Aj0HkkW9tXS0k30aIm9DaFFKilQJAV1fjLlqAHBjEON5D8jOr8fX24TvZi9AaLSVohdCgDXlQeuqHXsD45wceuTn9OyPy2Ld+ulgZ6iG0/gqmiW/BXHwdbWTf2Ys3GsdZuRxv6SIQAnPHbsz9hxi+4csAGKMJIjt2I1NpUosXIDxNoPc0ZiqNNowYrnfPfZtvfVYg9CdG5LkbnzNONqX/BkSnkEL6l19EcM1yRMBP4vmXcfvP4HzuCry21mKfyUQAhOtS9Zu3MeIJhteswqmvw9/XT+RQN2Y6g5byHUuIr9/9w5t7zoWInEmjx+9+uqlnVuYN4LvW3GZZvfYGQp9bgwgFyW7fg3t6EGfNqgkkykGbJsnL16ACfmq69oHrkmuZxZmrP0vy4kUIwacdrQ9uXLftq+eVyGN3bpnnYe0QiE8HL7+U6Ff/EFkVAcAdGia7/T289rl4Cy+c8aDK7yO9eiUyZ1P1/oGCUkjS7XM4c/kavFAwiND//ui6LXeeFyI/uOunHcoytiPk7KobvkDwsuUgSrsx+/pOsEzcNZfOmMRZOA312O1tBHr7sBLJot6tihK7Yg12Q61AiH985K4t93wsIk+u21YvJa8KKRqiX/k85pyWiY6c7MM5cRr34sXoQOCciQBkli4GaRA5eGSCXpsmI6tXkq+vRSA2bly39U8/zJb593f+S+1kZVS60pb6BZTqsBZdiDl71pSOud37we/DW7TgI5EA0H4f+Y45+LuPYSbTuNFw6ZlhkFy8kPq3d6AVzz66YUvPfU/c9lY5W9OeWhvXb31Aax6W0TAqmUZGQgQuXYp/yXxEMIBKZxl9+nncJQtwP1V+W013ak2GTGeo/q9fk+loJ7lkIaDxxUYJHz2Ob3AIpIE2JNJV/VJ6i+95fO3otGNNVjy8/pn5CL5ntTYR/doXyXf3kNveReY3O8i8sQtrbgvCskAr1LyOsg7OFCocwquvI9jbB2iCpweQuRzaMMh0tJOe345vZJSanV2zlObvgLumszNlRR7dsPXnAnl91V9cj1FXXVBqcHr7yR8+jnPkOCqTK6jDYXRtDbq2GlVbjY5G0T4fWD6038Lc1VVcEeE4SMdF5B1E3saIJzASKcx4AplMIjyFFgKnvpZcyyxyzU0oyyr6VfvubvyDMS00y+7dfOv7FYk8tm7LpZ4Qu0KrlxG8YtW0M6gzWUZ+/DNUUyMEgojhEUQqBfqcgzHaZ+FWVaGiYfzHekhf2EGyzG/OyGRpfO0NNDx3/6Zbvz75+YStpYR4UEqJf+WSsoO7/WcA8JYswpvTNqZ0ESNxZDqFsPOQsxF2HtE/gBwZJdcxF+230JYP7bNQgQCqKoIXDBbtWgNDWKPxsuN6oSC55lkET/ff+Mi6nzx4/+bbT0xL5Ml12+ptwZ9YC9qR4eBUS2Nw+oYKpC8Yl+SaJrqxHq+xfkJbc8du5MgomZXLyto7C7euDqtvoJBUiukzp3THbAKn+4TEvAX47vhnxThiS3U9WpmBFRdVHNAbjKEj4Y8cO8rBratBeC5GJlO2jVNbg1sVRRn8+eRnpYCouU74fZjNF1Qc0BtNoquiH8PlMnYjhRhipLMV29kXNCCUXvDYt7e2j9eXiEjxebO1CWSFhFgpVDz1iRIxU5WvJPm6QvxWrr5ivF4CbPzmtlaUrjFbKq+GVhphGqiGypfAIsJhVDg0o6Y6FET5/ehKEwnka2vQQqA1l4zXmwDK0PMFYNRUnmlhGvhvu5FsbmZHrbvkIuw5LTCD5lpKRq69Bsf1KrezTLTPh7Tt+eP1EkAK3QYgggHcviFwSrUBt3cAxq6waI03ECvFDKWQA0PjPHeRQ7GiKPJ5ZLyU2UrbxkgkSnImgzFuK5npDDKXK8mJJDKfL8rWaBzpuiifDy3E7ClEFCICIKQk8bP/JPd+IRv1RhIknvsl+e7CZc3pHcT5+a+Qg4VYYvScwvfyfyOSBWfNI0fx/fIVxNjg5r6DRP7njeJgwX0Hiby9syiHu/YR3tlVknd0Ub3vUFGue3c3kUMfFCZFaere2kHoWA/KZyKEqJlCRGhlwNgO0Bp9dkXGPrU7Jk/+9ArbQIxvpzXaGytCeC5i/FZRHtIrycLzQJWX8byCDtBaI5RCeB7aMECIUv7C2YAoRRYNAo1v0TystkLabtRG8S1ox5zVWJAbapHz5qDPnhwN9ai5c9DRwo1Rz2rC65gLAX/heUsztm0XB3Oam1ChUqqeb20uOgqQn92CMy6/ys5uJV9XmHghJdnZrdiNDfjODINWpT16loiAmAZ0Lk/k2itLTy2LyJevLooyEsL3pavIpMdWoLqK/NWXlya8sR7V+NmS3NpMrrYKxlYo39YKbSXzuXnt430hc9ECHMcpysklpeCsBcRXLC34kc+D1gPj+xbiiOY4gPqQYIQG961dyNFE5XZjEGdi+A91z6gtQOj9Q1jxyraF1kg7D4oJuZYEMEXyMEK4bt/Q9L3HoF0Ht2s/8ujxGTlmHDtB4L39M2orbZvg4W78Q2cqtjOTqcLvROh9E/oDbNi0wUawx+0dmL73GIRlIQL+4ik1E8y01GakCjnW+Ix4Olix4YJdyfbx+lKKotTL3mgClSqftEEhaIpkambenQNkqmDTDVXOBHyxEYSUmUjO2TGh/9kvSvMSgL2/8p42aquR8XgpSJ4nGPEEWpRyrukg83n8g2dQSr14x1N3OBOenf3ywObbtmspjtjvHQZVfj+YLReA6yFjI2XbCMdBpDOIfGEsI50ZO2nKkzdjI3jhCMqaUkYoItjTi1AK0M9M6T9BUnqTSqSecI6fwpo3e3LbEhFAnjoNjoMcGUGOjCKG44hMGvL5KblV9a9+XfyuDQPt9+FVRXCrq1FVUdxoBHM0TnZ2+ZKr0JrQyV60EMdS9SdeqUgkms//Uyrg70y/sauuur0VISfW77zhOPahY4WOe8cdGqaFqq9BNbQVig8+C3w+ZM8pZN8A6ZXLC6vkuEjHQdg5zHgSc+AMYtxd30wkCfQPkmtqnFDRBAidOImRziAQ3+vs7JyytBOI3PHUHZmNd239joqN/sje+R6By1aAUuQPHiXbdRBvYOxotCxwHJzPXIZqvgAdiU5bITOTKWTfAHbHnOln2VPIZJJQ1z7MkVHMRJKanV0oyyLb2kxm/ly8YBCZyxE50A1SHkjUHds2na0pGzLRcOKpSKz9tuz2vasBcu8dQSVSGHU1hK5YhW/BXLzRJMkXCqurox/9kqUNiYpGMBNJ7KZG4pdcTGAwhr+vn9CJk4R6TpJrbcbI5ZGepwV8o9xruym1387OTqWE/jPt6Wzmzd8iLIvIdVdTffP1BFYvQ9ZUYc1pQUbDmIdnHrXLwXeqD+G6ZOe0oU2TbEsTo6tWELv6cuyWZoKn+vENnUEL/fA9T976Zjk70xaxH3xi7RGh1e0aEOEgvnltE/esFPiXLUTEhpH9gx+ZhNAQOPIBXjBIflIFxg2HsOtrC9k0crtF8m8r2Spbjb/3ybX/Cvy123Oa5C9eLaX2YwhcshgR8GN27ZvRDXA6WKf7MBJJUgvnTSkBhU72Ur1nP0h5NKD54w2bNthlzFQmAnD/k2sfEprvOyf7SP7by6hEKaILv4/gpy5GDgxgHDt+ziSE6xLaux8vHCLb1lzSa03kcDdVe/ajBR9oL3/NXZtviVUw9eFEAO7dvPY7CP1Ntz+m4s++SP5IKen0r1qK0ViH9e5uRLpyajMZoT37kNkc8eVLQRTckDmb2rd3ETl8FKR4R5ruZZMrih+ZCMB9m277sYQrtev2pv7jNVIvvopXyHmIXHsVKA/rtdeLV9wPQ6D7GP4Tp0hf2F54meN5hLuP0vC/b+IfHtECHorkclfe+9jtwzMyyDm+nn5i/bNVjnIeRvINENK3aB6BZQvRdp7kS6+ha2uwr/kDGEv8Jr8fERoCh7oJHDiIPauJxNKLCPb2Ez56AmnbaCkPSq3+6t5Na18/F7/OmchZ/ODObcsNw3sI5Jc0WsiqCMKy8GIj6KAfb8UyvPkdGL/dWyRiDI8QOnAYa2AIDEm+phpreBShNULKU9rzvj97MPLUTc/fVLkedD6JnMXD65+Zb2j+Uku+JpSe+D7AMNCmgbDz6EAAMa7MA4CUGZT+hUZtSzX0vDJd2nEuOG9/qvmH9U+3GVhXofQKIbgEqEOKehBSa5UVmjiaI0KyB/S7ifqeNz+JP9f8Hr/Hecb/AQvdsKb7Z4awAAAAAElFTkSuQmCC';

StateClipComponent.stateClipPool = new Map();

WVPropertyManager.attach_default_component_infos(StateClipComponent, {
    "info": {
        "componentName": "StateClipComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "selectItem": "",
        "severity": "normal",
        "autoPlay": true
    },

    "label": {
        "label_using": "N",
        "label_text": "State Clip"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
StateClipComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label",
}, {
    label: "Resource",
    template: "resource",
    children: [{
        owner: "setter",
        name: "selectItem",
        type: "resource",
        label: "Resource",
        show: true,
        writable: true,
        description: "stateClip",
        options: {
            type: "stateClip"
        }
    }]
}, {
    label: "Status",
    template: "vertical",
    children: [{
        name: "severity",
        type: "select",
        label: "Severity",
        owner: "setter",
        show: true,
        writable: true,
        description: "상태 값",
        options: {
            items: [
                { label: "critical", value: "critical" },
                { label: "major", value: "major" },
                { label: "minor", value: "minor" },
                { label: "warning", value: "warning" },
                { label: "normal", value: "normal" }
            ]
        }
    }]
},{
      label: "Auto Play",
      owner: "vertical",
      children: [{
            owner: "setter",
            name: "autoPlay",
            type: "checkbox",
            label: "Auto",
            show: true,
            writable: true,
            description: "자동 재생"
      }]
}];

WVPropertyManager.add_event(StateClipComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.add_property_group_info(StateClipComponent, {
    label: "StateClipComponent 고유 속성",
    children: [{
        name: "severity",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'normal'",
        description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
    }]
})
