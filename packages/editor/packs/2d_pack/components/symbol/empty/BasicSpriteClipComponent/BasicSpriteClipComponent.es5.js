"use strict";

var _createClass = function() {
      function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                  var descriptor = props[i];
                  descriptor.enumerable = descriptor.enumerable || false;
                  descriptor.configurable = true;
                  if ("value" in descriptor) descriptor.writable = true;
                  Object.defineProperty(target, descriptor.key, descriptor);
            }
      }
      return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
      if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
      subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
      if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var BasicSpriteClipComponent = function(_WVDOMComponent) {
      _inherits(BasicSpriteClipComponent, _WVDOMComponent);

      function BasicSpriteClipComponent() {
            _classCallCheck(this, BasicSpriteClipComponent);

            var _this = _possibleConstructorReturn(this, (BasicSpriteClipComponent.__proto__ || Object.getPrototypeOf(BasicSpriteClipComponent)).call(this));

            _this.loadedPage = false;
            _this.invalidateResource = false;
            _this.isResourceLoaded = false;
            _this.isSettingsReady = false;
            _this.$container = null;
            _this.animateClip = null;
            _this.settingObj = {
                  width: "",
                  height: "",
                  fps: 24,
                  loop: true,
                  columns: "",
                  autoplay: false,
                  totalFrames: ""
            };
            return _this;
      }

      _createClass(BasicSpriteClipComponent, [{
            key: "_onDestroy",
            value: function _onDestroy() {
                  if (this.animateClip) {
                        this.animateClip.destroy();
                        this.animateClip = null;
                  }
                  this.$container.remove();
                  this.$container = null;
                  this.settingObj = null;
                  _get(BasicSpriteClipComponent.prototype.__proto__ || Object.getPrototypeOf(BasicSpriteClipComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  var $el = $(this._element);
                  $el.find(".comp-wrap").remove();
                  this.$container = $("<div><div class='sprite-image'></div></div>");
                  this.$container.css({
                        "width": "100%",
                        "height": "100%",
                        "background-color": "rgba(255, 255, 255, .6)",
                        "border": "1px dashed #808080",
                        "background-image": "url(" + BasicSpriteClipComponent.DEFAULT_IMAGE + ")",
                        "background-position": "center center",
                        "background-repeat": "no-repeat",
                        "transform-origin": "left top",
                        "overflow": "hidden"
                  });

                  this.$container.find(".sprite-image").css({
                        "position": "absolute",
                        "background-position": "0 0",
                        "background-repeat": "no-repeat",
                        "width": 5000,
                        "height": 5000
                  });

                  $el.append('<div class="comp-wrap" style="width:100%;height:100%;overflow:hidden;"></div>').find("div").append(this.$container);
                  this.isSettingsReady = false;
            }
      }, {
            key: "onLoadPage",
            value: function onLoadPage() {
                  var _this2 = this;

                  if (this.autoPlay && !this.isEditorMode) {
                        _this2.loadedPage = true;
                        _this2.play();
                  }
            }
      }, {
            key: "_onImmediateUpdateDisplay",
            value: function _onImmediateUpdateDisplay() {
                  this.selectProperty();
            }
      }, {
            key: "setAnimateInfo",
            value: function setAnimateInfo() {
                  var currentSprite = this.selectItem;
                  this.settingObj.width = currentSprite.data.width;
                  this.settingObj.height = currentSprite.data.height;
                  this.settingObj.columns = currentSprite.data.columns;
                  this.settingObj.totalFrames = currentSprite.data.totalFrames;

                  if (!this.settingObj.width || !this.settingObj.height) {
                        throw this.exceptionSymbolInfo("width || height");
                  }

                  this.isSettingsReady = true;

                  this.$container.css({
                        "backgroundImage": "none",
                        "background-color": "transparent",
                        "border": "none"
                  });

                  var path = this.selectItem.path.replace("static/components/2D/symbol/", "client/packs/2d_pack/components/symbol/");
                  this.$container.find(".sprite-image").css({

                        "backgroundImage": "url('" + wemb.configManager.serverUrl + path + "')",
                        "visibility": "visible"
                  });

                  this.calcScale();
            }
      }, {
            key: "calcScale",
            value: function calcScale() {
                  if (this.$container && this.selectItem) {
                        var oriWidth = parseInt(this.settingObj.width);
                        var scale = 1 / oriWidth * this.width;
                        this.$container.css({
                              "transform": "scale(" + scale + "," + scale + ")",
                              width: this.settingObj.width,
                              height: this.settingObj.height
                        });
                  }
            }
      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {
                  if (this.invalidateSize || this.invalidateVisible) {
                        this.validateCallLater(this.calcScale);
                  }

                  /////////////////////////////////////////////////////
                  ////    추후 변경 예정 ( core로 이동 )
                  /////////////////////////////////////////////////////
                  if (this.invalidateResource) {
                        this.validateCallLater(this.dispatchSizeEvent);
                        this.invalidateResource = false;
                  }
            }
      }, {
            key: "dispatchSizeEvent",
            value: function dispatchSizeEvent() {
                  if (this.isEditorMode) {
                        this.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
                  }
                  this.setGroupPropertyValue("setter", "isSaved", true);
            }
      }, {
            key: "selectProperty",
            value: function selectProperty() {
                  var _this3 = this;

                  var info = this.getGroupPropertyValue("setter", "info");
                  if (!info) {
                        this.settingObj.width = "";
                        this.settingObj.height = "";
                        return;
                  }

                  this.procSelectItem(info).then(function(data) {

                        _this3.invalidateResource = true;
                        _this3.invalidateProperties();
                        _this3.setAnimateInfo();

                        _this3.isSaved = true;

                        if (!_this3.isEditorMode) {

                              if (_this3.animateClip) {
                                    _this3.animateClip.destroy();
                              }

                              if (_this3.loadedPage) {
                                    _this3.play();
                              }
                        }
                  }, function(error) {
                        console.log("basic sprite clip component error", error);
                  });
            }
      }, {
            key: "play",
            value: function play() {
                  this.animateClip = new AnimateSpriteClip(this.$container.find(".sprite-image").get(0), this.settingObj);
                  if(!this.isEditorMode) {
                        this.animateClip.play();
                  }
            }
      }, {
            key: "stop",
            value: function stop() {
                  var _this5 = this;
                  if (this.animateClip) {
                        _this5.animateClip.stop();
                  }
            }
      }, {
            key: "procSelectItem",
            value: function procSelectItem(info) {
                  var _this4 = this;

                  return new Promise(function(resolve, reject) {

                        var isResize = false;
                        if (_this4.isEditorMode && (!_this4.selectItem || !_this4.isSettingsReady)) {
                              isResize = true;
                        }

                        _this4.selectItem = info;

                        if (info) {
                              _this4.$container.css("background-position", "");
                              if (isResize && !_this4.isSaved) {
                                    _this4.width = _this4.selectItem.data.width;
                                    _this4.height = _this4.selectItem.data.height;
                              }
                        }

                        resolve();
                  });
            }
      }, {
            key: "exceptionSymbolInfo",
            value: function exceptionSymbolInfo(type) {
                  console.log("BasicSpriteClipComponent _ 에러가 발생하였습니다. [" + type + "]");
            }
      }, {
            key: "isSaved",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "isSaved", value)) {}
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "isSaved");
            }
      }, {
            key: "autoPlay",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "autoPlay", value)) {}
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "autoPlay");
            }
      }]);

      return BasicSpriteClipComponent;
}(WVDOMComponent);

BasicSpriteClipComponent.DEFAULT_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAARTQAAEU0BwDlgYwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAWzSURBVGiB7ZldbJNVGMd/5+3n2m2U0ZaxMWg3QOl0jmXKhmFuETZBEr50iTfGGy+Mt8ZIjImJMYCJCSpcaDSC0Uj0gkQdMIU4xDiYDvkQ2Ma6dWyMDtqO0e5773u8GJ1sittCOwrZ/+78z3POk19Onvac54VZzWpWs7qfJAAqdvy2S0qRP35KLhFCiUgp/YnpgQJ7D28r3gugB4hCKFAzthThElL6RcJ6vAS4omP9bXQ1h7cVvx0dl2+vLRWJ741J4QHRAwMiAMp31HYIsAvEYHRCIq0CRkhYT7Mi+aF626pNcKtG9EIZtFkNpnmpJlM08GbfMAadoksy6RLSa7/epw0MjdyIjvUAOoXrxQ/bszcULuR+0c4DF4Z8/rAvOn5gamQWJNE0C5JoemBA9JOHTE9vfHmaDJuZp/PSyV1ki/X2d1TMQSwmIxc6erjQ0YNep+B2JvPMigVxh4o5SMGydDprwwCMqBqt18LsPniTuckmHnPPpTAnjez0FMQk+0xXMQfJyUxDpwhUTWIy6Hht43LMRj2nW4LUt3RTc86Pw2bm2YJMVi6zxyxvzEEAnGlW/IEIL69dQpbdCsDa/AzW5mcQigzycXUz+2pakEBRjGDi8qu1Ymk6EphvM4/zr/UM8NUxH+3BXtLTkvmippXjF6/FJGdcQJZmzsOWbOKMr3ucf/JSkPZgP1tLlvNcqYdH3Hb2H2/jRFPgrnPG7X8kJ3Me9d5RkEB4kF/Od1H9ZycrPZlk2EeLvXSFG4/Lzv5ffbRd772rfHEDWZaVhq8rTCgyyOdHvRyo62B1XhYel2MsRgBlBW6WZdnZ9X3DXcHEDcRhs5JiMbLv51ZausKsL1pKXk76v+JGT8bF0qx5dwUTNxABPOFZSFPnTRw2K5mO1P+NHQdzLTLtfHG9a+W6HFQ8nk2wp48jf7SAvHOsAMpWuMjNdvL+dxe5dPXmtHLF/dL40CI7ZQUuGtoC/DgJDMCqR7LIdTv5qKqRps6pw8zI7TfX5eTpQjdN7UGOnGqd9GRWP7aYXLeT3QenDjNj13jPYgeVZR68V0JThvG4pg4zo+8R51wrm0sexnslxE/1k9dMSf7UYWb8YeW0jcK0dHZPC+ajqgaOX+i6Y6weQNVw1DYEaOoMj03caoqRZNJNyzObTHSHBzAadFjNhrG4iV6mPZnGy0F0iqCswH3Ha70AFjpSOOvt4uvjPo6e7SLVYsAf6jcysRs/IjVTMDI4GIoMxaCdGZ7O2uTzrdeVEVWjvDCH/6Lx+W9wuM7LhsJMbvQO8VtjQHZ1D/RLoZmAsdea/taOzQI+Pbyt6O3oRPn22prRVn58PSm0xqbLwRckwlJRmK27HcZ7JcTBk81sKcpiTd4CJKDTKRz7y2+RCK+QnI7G3vvmgyauqkKWXroc6PuxvkWN1sxECBg9sMonF4sSz3wUyJFCFka3ufcgQPXrxadUIUub2gJ9VXXNsrXzBodOellXkDEGEZUANhcvwmo2IIRYv27niaKoT/n22hqY+OmNlwT4mEFPKmKRMvpJTawvyGDD4/9uqg+rGnsONtHZPcB8e4r0dgTDw9rIo3oAIeRpKUW+BqX/LJF6hJIupZw5T5Nogm6dItKCkSGkBHFbzQwOq3xY1UgoMszzZR6SzAaxv6cvKRTufyfWzYyYqOLd2krFKD5bucRuebE0WxECBoZGIbp7h9ny1HKSk4wAnPN2cexsWyghamSiqt8s/kZDbj7ZHBD7fvbSNzTCB/8BAWBJMqBppCTkiURVsePEq4qQu61mPULRsbVkOalW07iYX8+1c6bZfzEhTySq6jeK9mjwSu+Aqi10zlFTLMZx86FwP39euqqqmtyd0CcS1br36p5Cyiq7LcmYl+M0WEwGroZ6OdV4RQXlkNXVtum+AAFYs/33bL1Q39IpbFQ1marXKw1DI9qeOdntn3xbWan+DUMbLYtgF8ajAAAAAElFTkSuQmCC';

WVPropertyManager.attach_default_component_infos(BasicSpriteClipComponent, {
      "info": {
            "componentName": "BasicSpriteClipComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 100,
            "height": 100,
            "isSaved": false,
            "autoPlay": true
      },

      "label": {
            "label_using": "N",
            "label_text": "Sprite Clip"
      }
});

// 기본 프로퍼티 정보입니다.

// 프로퍼티 패널에서 사용할 정보 입니다.
BasicSpriteClipComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}, {
      label: "Auto Play",
      owner: "vertical",
      children: [{
            owner: "setter",
            name: "autoPlay",
            type: "checkbox",
            label: "Auto",
            show: true,
            writable: true,
            description: "자동 재생"
      }]
}];
