function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var SpriteClipComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            "use strict";

            _inherits(SpriteClipComponent, _WVDOMComponent);

            function SpriteClipComponent() {
                  var _this;

                  _classCallCheck(this, SpriteClipComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(SpriteClipComponent).call(this));
                  _this.loadedPage = false;
                  _this.animateClip = null;
                  _this.$container = null;
                  _this.settingObj = {
                        width: "",
                        height: "",
                        fps: 24,
                        loop: true,
                        columns: "",
                        autoplay: false,
                        totalFrames: ""
                  };
                  _this.isInitSelectResource = false;
                  _this._invalidateSelectItem = false;
                  _this._isResourceComponent = true;
                  return _this;
            }

            _createClass(SpriteClipComponent, [{
                  key: "onLoadPage",
                  value: function onLoadPage() {
                        if (this.selectItem && this.selectItem.path) {
                              this.$container.find(".sprite-image").css({
                                    "backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + "')",
                                    "visibility": "visible"
                              });

                              if (this.autoPlay && !this.isEditorMode) {
                                    if (this.animateClip) {
                                          this.animateClip.destroy();
                                    }
                                    this.play();
                              }
                        } else {
                              this.$container.find(".sprite-image").css({
                                    "visibility": "hidden"
                              });
                        }
                  }
            }, {
                  key: "play",
                  value: function play() {
                        this.animateClip = new AnimateSpriteClip(this.$container.find(".sprite-image").get(0), this.settingObj);
                        this.animateClip.play();
                  }
            },{
                  key: "stop",
                  value: function stop() {
                        if (this.animateClip) {
                              this.animateClip.stop();
                        }
                  }
            },{
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this.createContainer();
                  }
            }, {
                  key: "createContainer",
                  value: function createContainer() {
                        var $el = $(this._element);
                        $el.find(".comp-wrap").remove();
                        this.$container = $("<div><div class='sprite-image'></div></div>");
                        this.$container.css({
                              "width": "100%",
                              "height": "100%",
                              "background-color": "rgba(255, 255, 255, .6)",
                              "background-image": "url(" + SpriteClipComponent.DEFAULT_IMAGE + ")",
                              "background-position": "center center",
                              "background-repeat": "no-repeat",
                              "transform-origin": "left top",
                              "overflow": "hidden"
                        });
                        this.$container.find(".sprite-image").css({
                              "position": "absolute",
                              "background-position": "0 0",
                              "background-repeat": "no-repeat",
                              "pointer-events": "none",
                              "width": 5000,
                              "height": 5000
                        });
                        $el.append('<div class="comp-wrap" style="width:100%;height:100%;overflow:hidden;"></div>').find("div").append(this.$container);
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        if (this.animateClip) {
                              this.animateClip.destroy();
                              this.animateClip = null;
                        }

                        this.$container.remove();
                        this.$container = null;
                        this.settingObj = null;
                        this._invalidateSelectItem = null;

                        _get(_getPrototypeOf(SpriteClipComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_setNoImageStyle",
                  value: function _setNoImageStyle() {
                        this.$container.css({
                              "background-color": "rgba(255, 255, 255, .6)",
                              "background-image": "url(" + SpriteClipComponent.DEFAULT_IMAGE + ")",
                              "background-position": "center center",
                              "background-repeat": "no-repeat",
                              "width": "100%",
                              "height": "100%"
                        });
                        this.$container.css("transform", "");
                        this.$container.find(".sprite-image").css("backgroundImage", "");
                  }
            }, {
                  key: "startLoadResource",
                  value: function () {
                        var _startLoadResource = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee() {
                                    var success;
                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                          while (1) {
                                                switch (_context.prev = _context.next) {
                                                      case 0:
                                                            _context.prev = 0;
                                                            _context.next = 3;
                                                            return this.loadSpriteResource();

                                                      case 3:
                                                            success = _context.sent;

                                                            if (success) {
                                                                  this._validateSelectProperty();

                                                                  if (this.isInitSelectResource) {
                                                                        this.$container.find(".sprite-image").css({
                                                                              "backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + "')",
                                                                              "visibility": "visible"
                                                                        });
                                                                  }
                                                            } // 에디터에서 리소스 선택한 경우, emit 날리지 않고


                                                            if (!this.isInitSelectResource) {
                                                                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                            }

                                                            _context.next = 12;
                                                            break;

                                                      case 8:
                                                            _context.prev = 8;
                                                            _context.t0 = _context["catch"](0);
                                                            this.exceptionSymbolInfo("startLoadResource error");

                                                            if (!this.isInitSelectResource) {
                                                                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                            }

                                                      case 12:
                                                      case "end":
                                                            return _context.stop();
                                                }
                                          }
                                    }, _callee, this, [[0, 8]]);
                              }));

                        function startLoadResource() {
                              return _startLoadResource.apply(this, arguments);
                        }

                        return startLoadResource;
                  }()
            }, {
                  key: "loadSpriteResource",
                  value: function loadSpriteResource() {
                        var _this2 = this;

                        return new Promise(function (resolve, reject) {
                              /*if (!this.getGroupPropertyValue("setter", "visible")) {
                                  resolve(false);
                                  return;
                              }*/
                              if (!_this2.selectItem) {
                                    _this2._setNoImageStyle();

                                    resolve(false);
                              } else {
                                    var self = _this2;
                                    var $image = $("<img class='temp-img'>");

                                    _this2.$container.append($image);

                                    $image.off("error").on("error", function () {
                                          self._setNoImageStyle();

                                          reject("error");
                                          self.$container.find(".temp-img").remove();
                                    }).attr("src", wemb.configManager.serverUrl + _this2.selectItem.path);
                                    $image.off("load").on("load", function () {
                                          resolve(true);
                                          self.$container.find(".temp-img").remove();
                                    });
                              }
                        });
                  }
            }, {
                  key: "setAnimateInfo",
                  value: function setAnimateInfo() {
                        var currentSprite = this.selectItem;
                        this.settingObj.width = currentSprite.data.width;
                        this.settingObj.height = currentSprite.data.height;
                        this.settingObj.columns = currentSprite.data.columns;
                        this.settingObj.totalFrames = currentSprite.data.totalFrames;

                        if (!this.settingObj.width || !this.settingObj.height) {
                              throw this.exceptionSymbolInfo("width || height");
                        }

                        this.calcScale();
                  }
            }, {
                  key: "calcScale",
                  value: function calcScale() {
                        if (this.$container && this.selectItem) {
                              var oriWidth = parseInt(this.settingObj.width);
                              var scale = 1 / oriWidth * this.width;
                              this.$container.css({
                                    "backgroundImage": "none",
                                    "background-color": "transparent",
                                    "border": "none",
                                    "transform": "scale(" + scale + "," + scale + ")",
                                    "width": this.settingObj.width,
                                    "height": this.settingObj.height
                              });
                        }
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._invalidateSelectItem) {
                              this._invalidateSelectItem = false;
                              this.validateCallLater(this.startLoadResource);
                        }

                        if (this.invalidateSize || this.invalidateVisible) {
                              this.validateCallLater(this.calcScale);
                        }
                  }
            }, {
                  key: "_validateSelectProperty",
                  value: function _validateSelectProperty() {
                        var info = this.selectItem;

                        if (!info) {
                              this.settingObj.width = "";
                              this.settingObj.height = "";
                              this.createContainer();
                              return;
                        }

                        var isResize = false;

                        if (this.isEditorMode && this.isInitSelectResource) {
                              isResize = true;
                        }

                        if (info) {
                              this.$container.css("background-position", "");

                              if (isResize) {
                                    this.width = this.selectItem.data.width;
                                    this.height = this.selectItem.data.height;

                                    if (this.isEditorMode) {
                                          this.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
                                    }
                              }

                              this.setAnimateInfo();
                        }
                  }
            }, {
                  key: "exceptionSymbolInfo",
                  value: function exceptionSymbolInfo(type) {
                        console.log("SpriteClipComponent _ 에러가 발생하였습니다. [" + type + "]");
                  }
            }, {
                  key: "selectItem",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                              this._invalidateSelectItem = true;
                              this.isInitSelectResource = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "selectItem");
                  }
            }, {
                  key: "autoPlay",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("setter", "autoPlay", value)) {}
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "autoPlay");
                  }
            }]);

            return SpriteClipComponent;
      }(WVDOMComponent);

SpriteClipComponent.DEFAULT_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAARTQAAEU0BwDlgYwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAWzSURBVGiB7ZldbJNVGMd/5+3n2m2U0ZaxMWg3QOl0jmXKhmFuETZBEr50iTfGGy+Mt8ZIjImJMYCJCSpcaDSC0Uj0gkQdMIU4xDiYDvkQ2Ma6dWyMDtqO0e5773u8GJ1sittCOwrZ/+78z3POk19Onvac54VZzWpWs7qfJAAqdvy2S0qRP35KLhFCiUgp/YnpgQJ7D28r3gugB4hCKFAzthThElL6RcJ6vAS4omP9bXQ1h7cVvx0dl2+vLRWJ741J4QHRAwMiAMp31HYIsAvEYHRCIq0CRkhYT7Mi+aF626pNcKtG9EIZtFkNpnmpJlM08GbfMAadoksy6RLSa7/epw0MjdyIjvUAOoXrxQ/bszcULuR+0c4DF4Z8/rAvOn5gamQWJNE0C5JoemBA9JOHTE9vfHmaDJuZp/PSyV1ki/X2d1TMQSwmIxc6erjQ0YNep+B2JvPMigVxh4o5SMGydDprwwCMqBqt18LsPniTuckmHnPPpTAnjez0FMQk+0xXMQfJyUxDpwhUTWIy6Hht43LMRj2nW4LUt3RTc86Pw2bm2YJMVi6zxyxvzEEAnGlW/IEIL69dQpbdCsDa/AzW5mcQigzycXUz+2pakEBRjGDi8qu1Ymk6EphvM4/zr/UM8NUxH+3BXtLTkvmippXjF6/FJGdcQJZmzsOWbOKMr3ucf/JSkPZgP1tLlvNcqYdH3Hb2H2/jRFPgrnPG7X8kJ3Me9d5RkEB4kF/Od1H9ZycrPZlk2EeLvXSFG4/Lzv5ffbRd772rfHEDWZaVhq8rTCgyyOdHvRyo62B1XhYel2MsRgBlBW6WZdnZ9X3DXcHEDcRhs5JiMbLv51ZausKsL1pKXk76v+JGT8bF0qx5dwUTNxABPOFZSFPnTRw2K5mO1P+NHQdzLTLtfHG9a+W6HFQ8nk2wp48jf7SAvHOsAMpWuMjNdvL+dxe5dPXmtHLF/dL40CI7ZQUuGtoC/DgJDMCqR7LIdTv5qKqRps6pw8zI7TfX5eTpQjdN7UGOnGqd9GRWP7aYXLeT3QenDjNj13jPYgeVZR68V0JThvG4pg4zo+8R51wrm0sexnslxE/1k9dMSf7UYWb8YeW0jcK0dHZPC+ajqgaOX+i6Y6weQNVw1DYEaOoMj03caoqRZNJNyzObTHSHBzAadFjNhrG4iV6mPZnGy0F0iqCswH3Ha70AFjpSOOvt4uvjPo6e7SLVYsAf6jcysRs/IjVTMDI4GIoMxaCdGZ7O2uTzrdeVEVWjvDCH/6Lx+W9wuM7LhsJMbvQO8VtjQHZ1D/RLoZmAsdea/taOzQI+Pbyt6O3oRPn22prRVn58PSm0xqbLwRckwlJRmK27HcZ7JcTBk81sKcpiTd4CJKDTKRz7y2+RCK+QnI7G3vvmgyauqkKWXroc6PuxvkWN1sxECBg9sMonF4sSz3wUyJFCFka3ufcgQPXrxadUIUub2gJ9VXXNsrXzBodOellXkDEGEZUANhcvwmo2IIRYv27niaKoT/n22hqY+OmNlwT4mEFPKmKRMvpJTawvyGDD4/9uqg+rGnsONtHZPcB8e4r0dgTDw9rIo3oAIeRpKUW+BqX/LJF6hJIupZw5T5Nogm6dItKCkSGkBHFbzQwOq3xY1UgoMszzZR6SzAaxv6cvKRTufyfWzYyYqOLd2krFKD5bucRuebE0WxECBoZGIbp7h9ny1HKSk4wAnPN2cexsWyghamSiqt8s/kZDbj7ZHBD7fvbSNzTCB/8BAWBJMqBppCTkiURVsePEq4qQu61mPULRsbVkOalW07iYX8+1c6bZfzEhTySq6jeK9mjwSu+Aqi10zlFTLMZx86FwP39euqqqmtyd0CcS1br36p5Cyiq7LcmYl+M0WEwGroZ6OdV4RQXlkNXVtum+AAFYs/33bL1Q39IpbFQ1marXKw1DI9qeOdntn3xbWan+DUMbLYtgF8ajAAAAAElFTkSuQmCC';
WVPropertyManager.attach_default_component_infos(SpriteClipComponent, {
      "info": {
            "componentName": "SpriteClipComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 100,
            "height": 100,
            "selectItem": "",
            "autoPlay": true
      },
      "label": {
            "label_using": "N",
            "label_text": "Sprite Clip"
      }
}); // 프로퍼티 패널에서 사용할 정보 입니다.

SpriteClipComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}, {
      label: "Resource",
      template: "resource",
      children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "spriteClip",
            options: {
                  type: "spriteClip"
            }
      }]
}, {
      label: "Auto Play",
      owner: "vertical",
      children: [{
            owner: "setter",
            name: "autoPlay",
            type: "checkbox",
            label: "Auto",
            show: true,
            writable: true,
            description: "자동 재생"
      }]
}];
