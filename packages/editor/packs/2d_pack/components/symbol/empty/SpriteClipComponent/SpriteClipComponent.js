/**
 * 애니메이션 처리 : common/libs/animateSprite/jquery.animateSprite 파일 사용
 * 초기 jquery.animateSprite를 사용하였으나, 속도 성능이슈로 아래 클래스를 작성하여 대체함.
 * TODO : Frame컨트롤하는 메소드 추가 - gotoAndPlay() gotoAndStop(), playOnce() 등.. 
 *        라이브러리파일정리..
 * 
 * 하나의 background Image에 애니메이션 이미지가 좌표에 맞추어 설정되어 있기 때문에 스케일 변경은 transform scale로 처리함  
 * 
 */
class SpriteClipComponent extends WVDOMComponent {

    constructor() {
        super();
        this.loadedPage = false;
        this.animateClip = null;
        this.$container = null;
        this.settingObj = {
            width: "",
            height: "",
            fps: 24,
            loop: true,
            columns: "",
            autoplay: false,
            totalFrames: ""
        };

        this.isInitSelectResource = false;
        this._invalidateSelectItem = false;
        this._isResourceComponent = true;
    }

    onLoadPage() {
        if (this.selectItem && this.selectItem.path) {
            this.$container.find(".sprite-image").css({
                "backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + "')",
                "visibility": "visible"
            })

            if (this.autoPlay && !this.isEditorMode) {
                if (this.animateClip) {
                    this.animateClip.destroy();
                }
                this.play();
            }
        } else {
            this.$container.find(".sprite-image").css({
                "visibility": "hidden"
            })
        }
    }

    play() {
          this.animateClip = new AnimateSpriteClip(this.$container.find(".sprite-image").get(0), this.settingObj);
          this.animateClip.play();
    }

    stop() {
          if (this.animateClip) {
                this.animateClip.stop();
          }
    }

    _onCreateElement() {
        this.createContainer();
    }

    createContainer() {
        let $el = $(this._element);
        $el.find(".comp-wrap").remove();
        this.$container = $("<div><div class='sprite-image'></div></div>");
        this.$container.css({
            "width": "100%",
            "height": "100%",
            "background-color": "rgba(255, 255, 255, .6)",
            "background-image": "url(" + SpriteClipComponent.DEFAULT_IMAGE + ")",
            "background-position": "center center",
            "background-repeat": "no-repeat",
            "transform-origin": "left top",
            "overflow": "hidden"
        });

        this.$container.find(".sprite-image").css({
            "position": "absolute",
            "background-position": "0 0",
            "background-repeat": "no-repeat",
            "pointer-events": "none",
            "width": 5000,
            "height": 5000
        })

        $el.append('<div class="comp-wrap" style="width:100%;height:100%;overflow:hidden;"></div>').find("div").append(this.$container);
    }

    _onDestroy() {
        if (this.animateClip) {
            this.animateClip.destroy();
            this.animateClip = null;
        }
        this.$container.remove();
        this.$container = null;
        this.settingObj = null;
        this._invalidateSelectItem = null;
        super._onDestroy();
    }

    _setNoImageStyle() {
        this.$container.css({
            "background-color": "rgba(255, 255, 255, .6)",
            "background-image": "url(" + SpriteClipComponent.DEFAULT_IMAGE + ")",
            "background-position": "center center",
            "background-repeat": "no-repeat",
            "width": "100%",
            "height": "100%"
        });

        this.$container.css("transform", "");
        this.$container.find(".sprite-image").css("backgroundImage", "");
    }

    async startLoadResource() {
        try {
            let success = await this.loadSpriteResource();
            if (success) {
                this._validateSelectProperty();

                if (this.isInitSelectResource) {
                    this.$container.find(".sprite-image").css({
                        "backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + "')",
                        "visibility": "visible"
                    });
                }
            }

            // 에디터에서 리소스 선택한 경우, emit 날리지 않고
            if (!this.isInitSelectResource) {
                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }
        } catch (error) {
            this.exceptionSymbolInfo("startLoadResource error");
            if (!this.isInitSelectResource) {
                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }
        }
    }

    loadSpriteResource() {
        return new Promise((resolve, reject) => {
            /*if (!this.getGroupPropertyValue("setter", "visible")) {
                resolve(false);
                return;
            }*/

            if (!this.selectItem) {
                this._setNoImageStyle();
                resolve(false);
            } else {
                var self = this;
                var $image = $("<img class='temp-img'>");
                this.$container.append($image);
                $image.off("error").on("error", function() {
                    self._setNoImageStyle();
                    reject("error");
                    self.$container.find(".temp-img").remove();
                }).attr("src", wemb.configManager.serverUrl + this.selectItem.path);

                $image.off("load").on("load", function() {
                    resolve(true);
                    self.$container.find(".temp-img").remove();
                })
            }
        });
    }

    setAnimateInfo() {
        let currentSprite = this.selectItem;
        this.settingObj.width = currentSprite.data.width;
        this.settingObj.height = currentSprite.data.height;
        this.settingObj.columns = currentSprite.data.columns;
        this.settingObj.totalFrames = currentSprite.data.totalFrames;

        if (!this.settingObj.width || !this.settingObj.height) {
            throw this.exceptionSymbolInfo("width || height");
        }

        this.calcScale();
    }

    calcScale() {
        if (this.$container && this.selectItem) {
            let oriWidth = parseInt(this.settingObj.width);
            let scale = 1 / oriWidth * this.width;
            this.$container.css({
                "backgroundImage": "none",
                "background-color": "transparent",
                "border": "none",
                "transform": "scale(" + scale + "," + scale + ")",
                "width": this.settingObj.width,
                "height": this.settingObj.height
            });
        }
    }

    _onCommitProperties() {
        if (this._invalidateSelectItem) {
            this._invalidateSelectItem = false;
            this.validateCallLater(this.startLoadResource);
        }

        if (this.invalidateSize || this.invalidateVisible) {
            this.validateCallLater(this.calcScale);
        }
    }

    _validateSelectProperty() {
        var info = this.selectItem;
        if (!info) {
            this.settingObj.width = "";
            this.settingObj.height = "";
            this.createContainer();
            return;
        }

        let isResize = false;
        if (this.isEditorMode && this.isInitSelectResource) {
            isResize = true;
        }

        if (info) {
            this.$container.css("background-position", "");
            if (isResize) {
                this.width = this.selectItem.data.width;
                this.height = this.selectItem.data.height;

                if (this.isEditorMode) {
                    this.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
                }
            }

            this.setAnimateInfo();
        }
    }

    exceptionSymbolInfo(type) {
        console.log("SpriteClipComponent _ 에러가 발생하였습니다. [" + type + "]");
    }

    set selectItem(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
            this._invalidateSelectItem = true;

            this.isInitSelectResource = true;
        }
    }

    get selectItem() {
        return this.getGroupPropertyValue("setter", "selectItem");
    }

    set autoPlay(value) {
          if (this._checkUpdateGroupPropertyValue("setter", "autoPlay", value)) {}
    }

    get autoPlay() {
          return this.getGroupPropertyValue("setter", "autoPlay");
    }

}

SpriteClipComponent.DEFAULT_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAARTQAAEU0BwDlgYwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAWzSURBVGiB7ZldbJNVGMd/5+3n2m2U0ZaxMWg3QOl0jmXKhmFuETZBEr50iTfGGy+Mt8ZIjImJMYCJCSpcaDSC0Uj0gkQdMIU4xDiYDvkQ2Ma6dWyMDtqO0e5773u8GJ1sittCOwrZ/+78z3POk19Onvac54VZzWpWs7qfJAAqdvy2S0qRP35KLhFCiUgp/YnpgQJ7D28r3gugB4hCKFAzthThElL6RcJ6vAS4omP9bXQ1h7cVvx0dl2+vLRWJ741J4QHRAwMiAMp31HYIsAvEYHRCIq0CRkhYT7Mi+aF626pNcKtG9EIZtFkNpnmpJlM08GbfMAadoksy6RLSa7/epw0MjdyIjvUAOoXrxQ/bszcULuR+0c4DF4Z8/rAvOn5gamQWJNE0C5JoemBA9JOHTE9vfHmaDJuZp/PSyV1ki/X2d1TMQSwmIxc6erjQ0YNep+B2JvPMigVxh4o5SMGydDprwwCMqBqt18LsPniTuckmHnPPpTAnjez0FMQk+0xXMQfJyUxDpwhUTWIy6Hht43LMRj2nW4LUt3RTc86Pw2bm2YJMVi6zxyxvzEEAnGlW/IEIL69dQpbdCsDa/AzW5mcQigzycXUz+2pakEBRjGDi8qu1Ymk6EphvM4/zr/UM8NUxH+3BXtLTkvmippXjF6/FJGdcQJZmzsOWbOKMr3ucf/JSkPZgP1tLlvNcqYdH3Hb2H2/jRFPgrnPG7X8kJ3Me9d5RkEB4kF/Od1H9ZycrPZlk2EeLvXSFG4/Lzv5ffbRd772rfHEDWZaVhq8rTCgyyOdHvRyo62B1XhYel2MsRgBlBW6WZdnZ9X3DXcHEDcRhs5JiMbLv51ZausKsL1pKXk76v+JGT8bF0qx5dwUTNxABPOFZSFPnTRw2K5mO1P+NHQdzLTLtfHG9a+W6HFQ8nk2wp48jf7SAvHOsAMpWuMjNdvL+dxe5dPXmtHLF/dL40CI7ZQUuGtoC/DgJDMCqR7LIdTv5qKqRps6pw8zI7TfX5eTpQjdN7UGOnGqd9GRWP7aYXLeT3QenDjNj13jPYgeVZR68V0JThvG4pg4zo+8R51wrm0sexnslxE/1k9dMSf7UYWb8YeW0jcK0dHZPC+ajqgaOX+i6Y6weQNVw1DYEaOoMj03caoqRZNJNyzObTHSHBzAadFjNhrG4iV6mPZnGy0F0iqCswH3Ha70AFjpSOOvt4uvjPo6e7SLVYsAf6jcysRs/IjVTMDI4GIoMxaCdGZ7O2uTzrdeVEVWjvDCH/6Lx+W9wuM7LhsJMbvQO8VtjQHZ1D/RLoZmAsdea/taOzQI+Pbyt6O3oRPn22prRVn58PSm0xqbLwRckwlJRmK27HcZ7JcTBk81sKcpiTd4CJKDTKRz7y2+RCK+QnI7G3vvmgyauqkKWXroc6PuxvkWN1sxECBg9sMonF4sSz3wUyJFCFka3ufcgQPXrxadUIUub2gJ9VXXNsrXzBodOellXkDEGEZUANhcvwmo2IIRYv27niaKoT/n22hqY+OmNlwT4mEFPKmKRMvpJTawvyGDD4/9uqg+rGnsONtHZPcB8e4r0dgTDw9rIo3oAIeRpKUW+BqX/LJF6hJIupZw5T5Nogm6dItKCkSGkBHFbzQwOq3xY1UgoMszzZR6SzAaxv6cvKRTufyfWzYyYqOLd2krFKD5bucRuebE0WxECBoZGIbp7h9ny1HKSk4wAnPN2cexsWyghamSiqt8s/kZDbj7ZHBD7fvbSNzTCB/8BAWBJMqBppCTkiURVsePEq4qQu61mPULRsbVkOalW07iYX8+1c6bZfzEhTySq6jeK9mjwSu+Aqi10zlFTLMZx86FwP39euqqqmtyd0CcS1br36p5Cyiq7LcmYl+M0WEwGroZ6OdV4RQXlkNXVtum+AAFYs/33bL1Q39IpbFQ1marXKw1DI9qeOdntn3xbWan+DUMbLYtgF8ajAAAAAElFTkSuQmCC';

WVPropertyManager.attach_default_component_infos(SpriteClipComponent, {
    "info": {
        "componentName": "SpriteClipComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "selectItem": "",
        "autoPlay": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Sprite Clip"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
SpriteClipComponent.property_panel_info = [{
        template: "primary",
    }, {
        template: "pos-size-2d"
    },
    {
        template: "label"
    }, {
        label: "Resource",
        template: "resource",
        children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resouce",
            show: true,
            writable: true,
            description: "spriteClip",
            options: {
                type: "spriteClip"
            }
        }]
    }, {
        label: "Auto Play",
        owner: "vertical",
        children: [{
            owner: "setter",
            name: "autoPlay",
            type: "checkbox",
            label: "Auto",
            show: true,
            writable: true,
            description: "자동 재생"
    }]
}];
