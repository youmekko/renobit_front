"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BasicStateClipComponent = function (_WVDOMComponent) {
      _inherits(BasicStateClipComponent, _WVDOMComponent);

      function BasicStateClipComponent() {
            _classCallCheck(this, BasicStateClipComponent);

            var _this = _possibleConstructorReturn(this, (BasicStateClipComponent.__proto__ || Object.getPrototypeOf(BasicStateClipComponent)).call(this));

            _this.isResourceLoaded = false;
            _this.isSettingsReady = false;
            _this.$container = null;
            _this.animateClip = null;
            _this.spriteList = {};
            _this.settingObj = {
                  width: "",
                  height: "",
                  fps: 24,
                  loop: true,
                  columns: "",
                  autoplay: false,
                  totalFrames: ""
            };

            _this.publicMotionOptions = null;
            _this._invalidateServerity = false;
            _this._isDestroyed = false;
            _this.loadedPage = false;
            return _this;
      }

      _createClass(BasicStateClipComponent, [{
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  $(this._element).append('<div class="state-clip-wrap" style="width:100%;height:100%;overflow:hidden;"></div>');

                  if (!this.$container) {
                        this.createContainer();
                  }
            }
      }, {
            key: "onLoadPage",
            value: function onLoadPage() {
                  var _this4 = this;
                  if (this.autoPlay && !this.isEditorMode) {
                        _this4.loadedPage = true;
                        _this4.selectProperty();
                  }
            }
      }, {
            key: "_onCreateProperties",
            value: function _onCreateProperties() {
                  var info = this.getGroupPropertyValue("setter", "info");
                  if (info && info.path) {
                        var newInfo = $.extend(true, {}, info);
                        newInfo.path = newInfo.path.replace("static/components/2D/symbol/", "client/packs/2d_pack/components/symbol/");
                        this.setGroupPropertyValue("setter", "info", newInfo);
                  }
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  this._isDestroyed = true;
                  //this.$container.animateSprite('destroy');
                  if (this.animateClip) {
                        this.animateClip.destroy();
                        this.animateClip = null;
                  }
                  this.$container.remove();
                  this.$container = null;
                  this.spriteList = null;
                  this.settingObj = null;
                  _get(BasicStateClipComponent.prototype.__proto__ || Object.getPrototypeOf(BasicStateClipComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "createContainer",
            value: function createContainer() {
                  var _$container$css;

                  if (this.$container) {
                        //this.$container.animateSprite('destroy');
                        this.$container.remove();
                  }

                  if (this.animateClip) {
                        this.animateClip.destroy();
                  }

                  $(this._element).find(".state-clip-wrap").empty();

                  this.$container = $("<div><div class='sprite-image'></div></div>");
                  this.$container.css((_$container$css = {
                        "position": "relative",
                        "width": "100%",
                        "height": "100%",
                        "background-image": "url(" + BasicStateClipComponent.DEFAULT_IMAGE + ")",
                        "background-position": "0 0",
                        "background-color": "rgba(255, 255, 255, .6)"
                  }, _defineProperty(_$container$css, "background-position", "center center"), _defineProperty(_$container$css, "border", "1px dashed #808080"), _defineProperty(_$container$css, "transform-origin", "left top"), _defineProperty(_$container$css, "overflow", "hidden"), _$container$css));

                  this.$container.find(".sprite-image").css({
                        "position": "absolute",
                        "background-position": "0 0",
                        "background-repeat": "no-repeat",
                        "pointer-events": "none",
                        "width": 5000,
                        "height": 5000
                  });
                  if (this.settingObj.width != "") {
                        this.$container.css({
                              "width": this.settingObj.width,
                              "height": this.settingObj.height
                        });

                        this.calcScale();
                  }

                  $(this._element).find(".state-clip-wrap").append(this.$container);
            }
      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {
                  if (this.invalidateSize || this.invalidateVisible) {
                        this.validateCallLater(this.calcScale);
                  }

                  if (this._invalidateServerity) {
                        this._invalidateServerity = false;
                        this.validateCallLater(this._validateSeverityProperty);
                  }

                  if (this.invalidateResource) {
                        this.validateCallLater(this.dispatchSizeEvent);
                        this.invalidateResource = false;
                  }
            }
      }, {
            key: "_onImmediateUpdateDisplay",
            value: function _onImmediateUpdateDisplay() {
                  this.selectProperty();
            }
      }, {
            key: "dispatchSizeEvent",
            value: function dispatchSizeEvent() {
                  if (this.isEditorMode) {
                        this.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
                  }

                  this.setGroupPropertyValue("setter", "isSaved", true);
            }

            /*_validateServerityProperty() {
                if (this.spriteList[this.severity] && this.spriteList[this.severity].type === "motion") {
                    this.excuteAnimate();
                } else {
                    if (!this.$container) {
                        this.createContainer();
                    };
                     this.$container.css({
                        "backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + this.severity + ".png')",
                        "backgroundColor": "transparent",
                        "border": "none"
                    });
                    this.calcScale();
                }
             }*/

      }, {
            key: "_validateSeverityProperty",
            value: function _validateSeverityProperty() {
                  this.selectProperty();

                  if (!this.isEditorMode) {
                        this.dispatchWScriptEvent("change", {
                              value: this.severity
                        });
                  }
            }
      }, {
            key: "excuteAnimate",
            value: function excuteAnimate() {
                  if (this.severity) {
                        if (!this.$container) {
                              this.createContainer();
                        };

                        this.$container.css({
                              "backgroundImage": "none",
                              "backgroundColor": "transparent",
                              "border": "none"
                        });

                        this.$container.find(".sprite-image").css({
                              "backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + this.severity + ".png')"
                        });

                        this.calcScale();
                  } else {
                        throw this.exceptionSymbolInfo("this.severity");
                  }
            }
      }, {
            key: "selectProperty",
            value: function selectProperty() {
                  var _this2 = this;

                  var info = this.getGroupPropertyValue("setter", "info");
                  this.selectItem = info;
                  this.selectItem.path = this.selectItem.path.replace("static/components/2D/symbol/", "client/packs/2d_pack/components/symbol/");

                  this.createContainer();

                  if (!info) {
                        this.settingObj.width = "";
                        this.settingObj.height = "";

                        //    this.savedResourceLoaded();
                        return;
                  }

                  this.setAnimateInfo(info).then(function () {
                        //    this.savedResourceLoaded();

                        var currentState = _this2.spriteList[_this2.severity];

                        if (currentState.type != undefined && currentState.type == "motion") {
                              var motionOps;

                              if (currentState.motionOptions) {
                                    motionOps = currentState.motionOptions != undefined ? currentState.motionOptions : _this2.publicMotionOptions;
                              } else {
                                    motionOps = _this2.publicMotionOptions;
                              }

                              _this2.settingObj.totalFrames = motionOps.totalFrames;
                              _this2.settingObj.columns = motionOps.columns;

                              if (!_this2.settingObj.totalFrames || !_this2.settingObj.columns) {
                                    throw _this2.exceptionSymbolInfo("totalFrames || columns");
                              }

                              if (!_this2.isEditorMode) {
                                    if (_this2.animateClip) {
                                          _this2.animateClip.destroy();
                                    }

                                    if (_this2.loadedPage) {
                                          _this2.play();
                                    }

                                    // _this2.animateClip = new AnimateSpriteClip(_this2.$container.find(".sprite-image").get(0), _this2.settingObj);
                                    // _this2.animateClip.play();
                                    //this.$container.animateSprite('destroy');
                                    //this.$container.animateSprite(JSON.parse(JSON.stringify(this.settingObj)));
                                    //this.$container.animateSprite('play');
                              }
                        }

                        _this2.invalidateResource = true;
                        _this2.invalidateProperties();
                  }, function(error) {
                        console.log("basic state clip component error", error);
                  });
            }
      }, {
            key: "play",
            value: function play() {
                  this.animateClip = new AnimateSpriteClip(this.$container.find(".sprite-image").get(0), this.settingObj);

                  if (!this.isEditorMode) {
                        this.animateClip.play();
                  }
            }
      }, {
            key: "stop",
            value: function stop() {
                  if (this.animateClip ) {
                        this.animateClip.stop();
                  }
            }
      }, {
            key: "procAnimateInfo",
            value: function procAnimateInfo(filePath) {
                  var locale_msg = Vue.$i18n.messages.wv;
                  return wemb.$http.get(wemb.configManager.serverUrl + filePath + "info.json").then(function (result) {
                        return result.data;
                  }, function (error) {
                        Vue.$alert(filePath + 'Info.json ' + locale_msg.common.errorFormat, 'Error', {
                              confirmButtonText: 'OK',
                              type: "error",
                              dangerouslyUseHTMLString: true
                        });
                  });
            }
      }, {
            key: "setSettingObj",
            value: function setSettingObj(result) {
                  if (this._isDestroyed) {
                        return;
                  }

                  this.settingObj.width = result.width;
                  this.settingObj.height = result.height;

                  if (this.isEditorMode && !this.isSaved) {
                        this.width = this.settingObj.width;
                        this.height = this.settingObj.height;
                  }

                  if (result.motionOptions != undefined) {
                        if (!result.motionOptions.totalFrames || !result.motionOptions.columns) throw this.exceptionSymbolInfo("공용 motionOptions 구성 값을 확인해주세요.");
                        this.publicMotionOptions = result.motionOptions;
                  }


                  if (!this.severity) this.severity = result.default;

                  if (!this.settingObj.width || !this.settingObj.height) {
                        throw this.exceptionSymbolInfo("width || height");
                  }

                  for (var state in result.data) {
                        this.spriteList[state] = result.data[state];
                  }

                  this.excuteAnimate();
            }
      }, {
            key: "setAnimateInfo",
            value: function setAnimateInfo(info) {
                  var _this3 = this;

                  return new Promise(function (resolve, reject) {

                        var path = info.path.replace("static/components/2D/symbol/", "client/packs/2d_pack/components/symbol/");

                        if (BasicStateClipComponent.stateClipPool.has(path)) {
                              _this3.setSettingObj(BasicStateClipComponent.stateClipPool.get(path));
                              resolve();
                        } else {

                              _this3.procAnimateInfo(path).then(function (result) {

                                    BasicStateClipComponent.stateClipPool.set(path, result);
                                    _this3.setSettingObj(result);

                                    resolve();
                              });
                        }
                  });
            }

            /*컴포넌트 로드 완료시점 체크 */

      }, {
            key: "savedResourceLoaded",
            value: function savedResourceLoaded() {
                  if (!this.isResourceLoaded) {
                        this.validateResource();
                        this.isResourceLoaded = true;
                  }
            }
      }, {
            key: "exceptionSymbolInfo",
            value: function exceptionSymbolInfo(type) {
                  console.log("BasicStateClipComponent _ 에러가 발생하였습니다. [" + type + " ]");
            }
      }, {
            key: "calcScale",
            value: function calcScale() {
                  if (this.$container && this.settingObj.width != "") {
                        var oriWidth = parseInt(this.settingObj.width);
                        var scale = 1 / oriWidth * this.width;
                        this.$container.css({
                              "transform": "scale(" + scale + "," + scale + ")",
                              "width": this.settingObj.width,
                              "height": this.settingObj.height,
                              "background-position": "0 0"
                        });
                  }
            }
      }, {
            key: "severity",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "severity", value)) {
                        this._invalidateServerity = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "severity");
            }
      }, {
            key: "isSaved",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "isSaved", value)) {}
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "isSaved");
            }
      },{
            key: "autoPlay",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "autoPlay", value)) {}
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "autoPlay");
            }
      }]);

      return BasicStateClipComponent;
}(WVDOMComponent);

BasicStateClipComponent.DEFAULT_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAK0AAACtAB0IQVDAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA0qSURBVGiB7ZprkFzFdcd/3ffeec++l9U+JO1KSOiBJIQsZBsCGDuxIS5TJganKgkggoOxkDDPqsQfsuWKHYJBYITiMjgSckiqDElhQyrGIZjEPIUeXgmh56LHSqt9aXZ33nPn3tudD7Oa2dcMKxD+kPL/y8w5t/v0+Xf39Dl97sD/E4jzbfCJ9VsaPVihlaxTghoAIchpdEpqjiQaThzo7Ox0z/e4H5vI43c/3aQ84zqF+CNpGF/QntdQeUThaSG6hNIvKE+89MCPbtn7cX2Aj0HkkW9tXS0k30aIm9DaFFKilQJAV1fjLlqAHBjEON5D8jOr8fX24TvZi9AaLSVohdCgDXlQeuqHXsD45wceuTn9OyPy2Ld+ulgZ6iG0/gqmiW/BXHwdbWTf2Ys3GsdZuRxv6SIQAnPHbsz9hxi+4csAGKMJIjt2I1NpUosXIDxNoPc0ZiqNNowYrnfPfZtvfVYg9CdG5LkbnzNONqX/BkSnkEL6l19EcM1yRMBP4vmXcfvP4HzuCry21mKfyUQAhOtS9Zu3MeIJhteswqmvw9/XT+RQN2Y6g5byHUuIr9/9w5t7zoWInEmjx+9+uqlnVuYN4LvW3GZZvfYGQp9bgwgFyW7fg3t6EGfNqgkkykGbJsnL16ACfmq69oHrkmuZxZmrP0vy4kUIwacdrQ9uXLftq+eVyGN3bpnnYe0QiE8HL7+U6Ff/EFkVAcAdGia7/T289rl4Cy+c8aDK7yO9eiUyZ1P1/oGCUkjS7XM4c/kavFAwiND//ui6LXeeFyI/uOunHcoytiPk7KobvkDwsuUgSrsx+/pOsEzcNZfOmMRZOA312O1tBHr7sBLJot6tihK7Yg12Q61AiH985K4t93wsIk+u21YvJa8KKRqiX/k85pyWiY6c7MM5cRr34sXoQOCciQBkli4GaRA5eGSCXpsmI6tXkq+vRSA2bly39U8/zJb593f+S+1kZVS60pb6BZTqsBZdiDl71pSOud37we/DW7TgI5EA0H4f+Y45+LuPYSbTuNFw6ZlhkFy8kPq3d6AVzz66YUvPfU/c9lY5W9OeWhvXb31Aax6W0TAqmUZGQgQuXYp/yXxEMIBKZxl9+nncJQtwP1V+W013ak2GTGeo/q9fk+loJ7lkIaDxxUYJHz2Ob3AIpIE2JNJV/VJ6i+95fO3otGNNVjy8/pn5CL5ntTYR/doXyXf3kNveReY3O8i8sQtrbgvCskAr1LyOsg7OFCocwquvI9jbB2iCpweQuRzaMMh0tJOe345vZJSanV2zlObvgLumszNlRR7dsPXnAnl91V9cj1FXXVBqcHr7yR8+jnPkOCqTK6jDYXRtDbq2GlVbjY5G0T4fWD6038Lc1VVcEeE4SMdF5B1E3saIJzASKcx4AplMIjyFFgKnvpZcyyxyzU0oyyr6VfvubvyDMS00y+7dfOv7FYk8tm7LpZ4Qu0KrlxG8YtW0M6gzWUZ+/DNUUyMEgojhEUQqBfqcgzHaZ+FWVaGiYfzHekhf2EGyzG/OyGRpfO0NNDx3/6Zbvz75+YStpYR4UEqJf+WSsoO7/WcA8JYswpvTNqZ0ESNxZDqFsPOQsxF2HtE/gBwZJdcxF+230JYP7bNQgQCqKoIXDBbtWgNDWKPxsuN6oSC55lkET/ff+Mi6nzx4/+bbT0xL5Ml12+ptwZ9YC9qR4eBUS2Nw+oYKpC8Yl+SaJrqxHq+xfkJbc8du5MgomZXLyto7C7euDqtvoJBUiukzp3THbAKn+4TEvAX47vhnxThiS3U9WpmBFRdVHNAbjKEj4Y8cO8rBratBeC5GJlO2jVNbg1sVRRn8+eRnpYCouU74fZjNF1Qc0BtNoquiH8PlMnYjhRhipLMV29kXNCCUXvDYt7e2j9eXiEjxebO1CWSFhFgpVDz1iRIxU5WvJPm6QvxWrr5ivF4CbPzmtlaUrjFbKq+GVhphGqiGypfAIsJhVDg0o6Y6FET5/ehKEwnka2vQQqA1l4zXmwDK0PMFYNRUnmlhGvhvu5FsbmZHrbvkIuw5LTCD5lpKRq69Bsf1KrezTLTPh7Tt+eP1EkAK3QYgggHcviFwSrUBt3cAxq6waI03ECvFDKWQA0PjPHeRQ7GiKPJ5ZLyU2UrbxkgkSnImgzFuK5npDDKXK8mJJDKfL8rWaBzpuiifDy3E7ClEFCICIKQk8bP/JPd+IRv1RhIknvsl+e7CZc3pHcT5+a+Qg4VYYvScwvfyfyOSBWfNI0fx/fIVxNjg5r6DRP7njeJgwX0Hiby9syiHu/YR3tlVknd0Ub3vUFGue3c3kUMfFCZFaere2kHoWA/KZyKEqJlCRGhlwNgO0Bp9dkXGPrU7Jk/+9ArbQIxvpzXaGytCeC5i/FZRHtIrycLzQJWX8byCDtBaI5RCeB7aMECIUv7C2YAoRRYNAo1v0TystkLabtRG8S1ox5zVWJAbapHz5qDPnhwN9ai5c9DRwo1Rz2rC65gLAX/heUsztm0XB3Oam1ChUqqeb20uOgqQn92CMy6/ys5uJV9XmHghJdnZrdiNDfjODINWpT16loiAmAZ0Lk/k2itLTy2LyJevLooyEsL3pavIpMdWoLqK/NWXlya8sR7V+NmS3NpMrrYKxlYo39YKbSXzuXnt430hc9ECHMcpysklpeCsBcRXLC34kc+D1gPj+xbiiOY4gPqQYIQG961dyNFE5XZjEGdi+A91z6gtQOj9Q1jxyraF1kg7D4oJuZYEMEXyMEK4bt/Q9L3HoF0Ht2s/8ujxGTlmHDtB4L39M2orbZvg4W78Q2cqtjOTqcLvROh9E/oDbNi0wUawx+0dmL73GIRlIQL+4ik1E8y01GakCjnW+Ix4Olix4YJdyfbx+lKKotTL3mgClSqftEEhaIpkambenQNkqmDTDVXOBHyxEYSUmUjO2TGh/9kvSvMSgL2/8p42aquR8XgpSJ4nGPEEWpRyrukg83n8g2dQSr14x1N3OBOenf3ywObbtmspjtjvHQZVfj+YLReA6yFjI2XbCMdBpDOIfGEsI50ZO2nKkzdjI3jhCMqaUkYoItjTi1AK0M9M6T9BUnqTSqSecI6fwpo3e3LbEhFAnjoNjoMcGUGOjCKG44hMGvL5KblV9a9+XfyuDQPt9+FVRXCrq1FVUdxoBHM0TnZ2+ZKr0JrQyV60EMdS9SdeqUgkms//Uyrg70y/sauuur0VISfW77zhOPahY4WOe8cdGqaFqq9BNbQVig8+C3w+ZM8pZN8A6ZXLC6vkuEjHQdg5zHgSc+AMYtxd30wkCfQPkmtqnFDRBAidOImRziAQ3+vs7JyytBOI3PHUHZmNd239joqN/sje+R6By1aAUuQPHiXbdRBvYOxotCxwHJzPXIZqvgAdiU5bITOTKWTfAHbHnOln2VPIZJJQ1z7MkVHMRJKanV0oyyLb2kxm/ly8YBCZyxE50A1SHkjUHds2na0pGzLRcOKpSKz9tuz2vasBcu8dQSVSGHU1hK5YhW/BXLzRJMkXCqurox/9kqUNiYpGMBNJ7KZG4pdcTGAwhr+vn9CJk4R6TpJrbcbI5ZGepwV8o9xruym1387OTqWE/jPt6Wzmzd8iLIvIdVdTffP1BFYvQ9ZUYc1pQUbDmIdnHrXLwXeqD+G6ZOe0oU2TbEsTo6tWELv6cuyWZoKn+vENnUEL/fA9T976Zjk70xaxH3xi7RGh1e0aEOEgvnltE/esFPiXLUTEhpH9gx+ZhNAQOPIBXjBIflIFxg2HsOtrC9k0crtF8m8r2Spbjb/3ybX/Cvy123Oa5C9eLaX2YwhcshgR8GN27ZvRDXA6WKf7MBJJUgvnTSkBhU72Ur1nP0h5NKD54w2bNthlzFQmAnD/k2sfEprvOyf7SP7by6hEKaILv4/gpy5GDgxgHDt+ziSE6xLaux8vHCLb1lzSa03kcDdVe/ajBR9oL3/NXZtviVUw9eFEAO7dvPY7CP1Ntz+m4s++SP5IKen0r1qK0ViH9e5uRLpyajMZoT37kNkc8eVLQRTckDmb2rd3ETl8FKR4R5ruZZMrih+ZCMB9m277sYQrtev2pv7jNVIvvopXyHmIXHsVKA/rtdeLV9wPQ6D7GP4Tp0hf2F54meN5hLuP0vC/b+IfHtECHorkclfe+9jtwzMyyDm+nn5i/bNVjnIeRvINENK3aB6BZQvRdp7kS6+ha2uwr/kDGEv8Jr8fERoCh7oJHDiIPauJxNKLCPb2Ez56AmnbaCkPSq3+6t5Na18/F7/OmchZ/ODObcsNw3sI5Jc0WsiqCMKy8GIj6KAfb8UyvPkdGL/dWyRiDI8QOnAYa2AIDEm+phpreBShNULKU9rzvj97MPLUTc/fVLkedD6JnMXD65+Zb2j+Uku+JpSe+D7AMNCmgbDz6EAAMa7MA4CUGZT+hUZtSzX0vDJd2nEuOG9/qvmH9U+3GVhXofQKIbgEqEOKehBSa5UVmjiaI0KyB/S7ifqeNz+JP9f8Hr/Hecb/AQvdsKb7Z4awAAAAAElFTkSuQmCC';

BasicStateClipComponent.stateClipPool = new Map();

WVPropertyManager.attach_default_component_infos(BasicStateClipComponent, {
      "info": {
            "componentName": "BasicStateClipComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 100,
            "height": 100,
            "selectItem": "",
            "severity": "normal",
            "isSaved": false,
            "autoPlay": true
      },

      "label": {
            "label_using": "N",
            "label_text": "State Clip"
      }
});

// 기본 프로퍼티 정보입니다.

// 프로퍼티 패널에서 사용할 정보 입니다.
BasicStateClipComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label",
}, {
      label: "Status",
      template: "vertical",
      children: [{
            name: "severity",
            type: "select",
            label: "Severity",
            owner: "setter",
            show: true,
            writable: true,
            description: "상태 값",
            options: {
                  items: [
                        { label: "critical", value: "critical" },
                        { label: "major", value: "major" },
                        { label: "minor", value: "minor" },
                        { label: "warning", value: "warning" },
                        { label: "normal", value: "normal" }
                  ]
            }
      }]
},{
      label: "Auto Playing",
      owner: "vertical",
      children: [{
            owner: "setter",
            name: "autoPlay",
            type: "checkbox",
            label: "Auto",
            show: true,
            writable: true,
            description: "자동 재생"
      }]
}];

// 이벤트 정보
WVPropertyManager.add_event(BasicStateClipComponent, {
      name: "change",
      label: "값 체인지 이벤트",
      description: "값 체인지 이벤트 입니다.",
      properties: [{
            name: "value",
            type: "string",
            default: "",
            description: "새로운 값입니다."
      }]
});

WVPropertyManager.add_property_group_info(BasicStateClipComponent, {
      label: "BasicStateClipComponent 고유 속성",
      children: [{
            name: "severity",
            type: "string",
            show: true,
            writable: true,
            defaultValue: "'normal'",
            description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
      }]
});
