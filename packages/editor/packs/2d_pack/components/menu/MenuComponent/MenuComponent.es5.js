"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var MenuComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(MenuComponent, _WVDOMComponent);

            function MenuComponent() {
                  var _this;

                  _classCallCheck(this, MenuComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(MenuComponent).call(this));
                  _this.userAccessiblePages = wemb.userInfo.accessiblePages || [];
                  _this.userAccessiblePages = _this.userAccessiblePages.map(function (x) {
                        return x.page_id;
                  }); //["7a46c63f-1012-45b6-822d-408f579d3b0f", "65f6597d-91d7-4c02-b14d-d9541c912fc7"] //

                  _this._invalidateProperty;
                  _this.clickMenuItemHandler = _this.clickMenuItem.bind(_assertThisInitialized(_this));
                  return _this;
            }

            _createClass(MenuComponent, [{
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        this.initMenuset();
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        var _this2 = this;

                        if (this.isEditorMode) {
                              window.wemb.menusetDataManager.$bus.$on("update_menuset_list", function (newList) {
                                    _this2.initMenuset();
                              });
                        }
                  }
            }, {
                  key: "openTemplate",
                  value: function openTemplate() {
                        var self = this;

                        if (this.isEditorMode) {
                              var menusetId = self.menusetId === null ? "" : self.menusetId;
                              window.wemb.$menusetManagerModal.show(menusetId).then(function (result) {
                                    self.setGroupPropertyValue("extension", "menuSetId", result.menuset.id);
                              }), function (error) {
                                    console.log("open template modal error", error);
                              };
                        }
                  }
            }, {
                  key: "onLoadPage",
                  value: function onLoadPage() {
                        this.activeMenu(wemb.pageManager.currentPageInfo.id);
                  }
            }, {
                  key: "onOpenPage",
                  value: function onOpenPage() {
                        this.activeMenu(wemb.pageManager.currentPageInfo.id);
                  }
            }, {
                  key: "initMenuset",
                  value: function initMenuset() {
                        var _this3 = this;

                        this.menuSet = window.wemb.menusetDataManager.menusetList;

                        if (!this.menuSet.length || !(this.menusetId && this.menuSet.find(function (x) {
                              return x.id == _this3.menusetId;
                        }))) {
                              this.makeEmptyMsg();
                              return;
                        }
                        /*if (!this.menusetId && this.menuSet.length) {
                            this.menusetId = data.menu[0].id;
                        }*/


                        this.initMenuSetData(this.menuSet);
                        this.setMenuSet(this.menusetId);
                  }
            }, {
                  key: "initMenuSetData",
                  value: function initMenuSetData() {
                        /*// 설정된 메뉴셋이 없을경우 메뉴셋 첫번째 아이템으로 선택
                        if (!this.menusetId && this.menuSet.length) {
                            this.menusetId = this.menuSet[0].id;
                        }*/
                        // 페이지명이 변경되었을 경우 저장된 메뉴셋의 페이지명 변경 (페이지명의 라벨을 비교하기 위해 필요)
                        var pageList = wemb.pageManager.getPageInfoList();
                        var self = this;
                        this.pageMap = {};
                        pageList.forEach(function (value) {
                              self.pageMap[value.id] = value.name;
                        });
                        this.setPageName(this.menuSet);
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        $(this._element).find("#nav").superfish("destroy");
                        $(this._element).find("#nav").remove();
                        this.clickMenuItemHandler = null;

                        _get(_getPrototypeOf(MenuComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._invalidateProperty) {
                              this.validateCallLater(this._validateProperty);
                        }

                        if (this._updatePropertiesMap.has("extension.menuSetId")) {
                              this.validateCallLater(this._validateProperty);
                        }

                        if (this._updatePropertiesMap.has("font")) {
                              this.validateCallLater(this._validateFontStyleProperty);
                        }
                  }
            }, {
                  key: "_validateProperty",
                  value: function _validateProperty() {
                        this.setMenuSet(this.menusetId);
                  }
            }, {
                  key: "_getAccessibleTreeItems",
                  value: function _getAccessibleTreeItems(treeItems) {
                        var validItems = $.extend(true, [], treeItems);
                        var list = [];

                        this._saveTempTreeId(list, validItems);

                        this._deleteInaccessiblePages(list, validItems);

                        return validItems;
                  }
            }, {
                  key: "_deleteInaccessiblePages",
                  value: function _deleteInaccessiblePages(list, treeItems) {
                        for (var index in treeItems) {
                              var treeItem = treeItems[index];

                              if (list.indexOf(treeItem.id) == -1) {
                                    delete treeItems[index];
                              } else {
                                    if (treeItem.children) {
                                          this._deleteInaccessiblePages(list, treeItem.children);
                                    }
                              }
                        }

                        var emptyFilter = treeItems.filter(function (x) {
                              return x;
                        });
                        treeItems.splice(0, treeItems.length);

                        if (emptyFilter.length) {
                              treeItems.push.apply(treeItems, _toConsumableArray(emptyFilter));
                        }
                  }
            }, {
                  key: "_saveTempTreeId",
                  value: function _saveTempTreeId(list, treeItems) {
                        for (var index in treeItems) {
                              var treeItem = treeItems[index];

                              if (treeItem.type != "page" || this.userAccessiblePages.indexOf(treeItem.id) != -1) {
                                    list.push(treeItem.id);
                              }

                              if (treeItem.children) {
                                    this._saveTempTreeId(list, treeItem.children);
                              }
                        }
                  }
            }, {
                  key: "makeEmptyMsg",
                  value: function makeEmptyMsg() {
                        var emptyMsg = $("<span>등록된 메뉴셋이 없습니다. 메뉴셋을 등록해주세요.</span>");
                        $(this._element).find("ul, span").remove();
                        $(this._element).append(emptyMsg);
                  }
            }, {
                  key: "setPageName",
                  value: function setPageName(children) {
                        var self = this;
                        children.forEach(function (value) {
                              if (self.pageMap[value.id]) {
                                    value.page_name = self.pageMap[value.id];
                              }

                              if (value.children) {
                                    this.setPageName(value.children);
                              }
                        });
                  }
            }, {
                  key: "setMenuSet",
                  value: function setMenuSet(setId) {
                        if (!this.menuSet || !this.menuSet || !setId) {
                              this.makeEmptyMsg();
                              return;
                        }

                        var allMenuSet = this.menuSet;
                        var menuStructure = null; // 선택한 메뉴셋 정보 가져오기

                        allMenuSet.forEach(function (menuObj) {
                              if (menuObj.id == setId) {
                                    menuStructure = menuObj.structure;
                              }
                        });

                        if (menuStructure) {
                              $(this._element).empty();
                              var navMenu = document.createElement("ul");
                              $(navMenu).attr("id", "nav").addClass('sf-menu'); // 메뉴셋에 따라 태그 만들어주기
                              //var items = this._getAccessibleTreeItems(menuStructure) //menuStructure; //

                              this.makeMenuItem(menuStructure, {
                                    element: navMenu
                              });

                              this._element.appendChild(navMenu);

                              if (this.isEditorMode) {//$(navMenu).css("pointer-events", "none");
                              } else {
                                    var $nav = $(this._element).find("#nav");
                                    $nav.superfish("destroy");
                                    $nav.superfish({
                                          speed: 'fast'
                                    });
                                    $(this._element).on("click", "a", this.clickMenuItemHandler);
                              }
                        }

                        this.setMenuStyle();
                  }
            }, {
                  key: "getTranslateText",
                  value: function getTranslateText(str) {
                        return wemb.localeManager.translatePrefixStr(str);
                  }
            }, {
                  key: "_validateFontStyleProperty",
                  value: function _validateFontStyleProperty() {
                        $(this._element).find("a").css({
                              "font-size": this.font.font_size,
                              "color": this.font.font_color,
                              "font-weight": this.font.font_weight,
                              "fontFamily": this.font.font_type
                        });
                  }
            }, {
                  key: "setMenuStyle",
                  value: function setMenuStyle() {
                        var bgColor = this.style.backgroundColor;
                        var bgHoverColor = this._properties.normal.background_hover_color;
                        var style = $('<style>' + '[id="' + this.id + '"] .sf-menu li:not(.disabled):hover { background: ' + bgHoverColor + '; } ' + '[id="' + this.id + '"] .sf-menu li{cursor:pointer;}' + '[id="' + this.id + '"] .sf-menu li.disabled{cursor:not-allowed;}' + '[id="' + this.id + '"] .sf-menu li.disabled>a { opacity: ' + 0.5 + ';} ' + '[id="' + this.id + '"] .sf-menu li{background:' + bgColor + ';}' + '[id="' + this.id + '"] .active {background-color: ' + this.active_color + ';}' + '</style>');
                        $(this._element).find("li").append(style).find("a").css({
                              "font-size": this.font.font_size,
                              "color": this.font.font_color,
                              "font-weight": this.font.font_weight,
                              "fontFamily": this.font.font_type
                        });
                  }
            }, {
                  key: "clickMenuItem",
                  value: function clickMenuItem(event) {
                        event.preventDefault();
                        var $target = $(event.target);
                        var type = $target.attr('type');
                        var url = $target.attr("data-url");
                        var isPopup = $target.attr("data-popup") || false;
                        var param = $target.attr("data-param");

                        if (type == "page" && this.userAccessiblePages.indexOf(url) == -1) {
                              console.warn("권한이 없는 페이지를 접근하셨습니다.");
                              return;
                        }

                        this.dispatchWScriptEvent("selectMenuItem", {
                              type: type,
                              isPopup: isPopup,
                              url: url,
                              item: $target.data("item"),
                              parent: $target.data("parent")
                        });

                        if (type == 'link') {
                              var target = "_self";

                              if (isPopup === true || isPopup == "true") {
                                    target = "_blank";
                              }

                              var _param = param || "";

                              window.open(url, target, _param);
                        } else if (type == 'page') {
                              var pageInfo = wemb.pageManager.getPageInfoBy(url);

                              if (isPopup == "true") {
                                    var _param2 = {};

                                    try {
                                          _param2 = JSON.parse(param);
                                    } catch (error) {}

                                    wemb.popupManager.open(pageInfo.name, _param2);
                              } else {
                                    $(this._element).find("li").removeClass("active");
                                    $target.parents('li').addClass('active');
                                    wemb.pageManager.openPageByName(pageInfo.name);
                              }
                        }
                  }
                  /**
                   ** 메뉴의 active 상태를 표현해주는
                   ** @param {페이지 아이디} pageId
                   * */

            }, {
                  key: "activeMenu",
                  value: function activeMenu(pageId) {
                        var items = $(this._element).find("[data-url=" + pageId + "]");
                        $(this._element).find("li").removeClass("active");

                        if (items) {
                              $(items[0]).parents('li').addClass("active");
                        }
                  }
            }, {
                  key: "makeMenuItem",
                  value: function makeMenuItem(itemList, parentObj) {
                        var self = this;
                        var parent = parentObj ? parentObj.element : null;
                        var parentItem = parentObj ? parentObj.menuItem : null;
                        var $parent = $(parent);

                        for (var i = 0; i < itemList.length; i++) {
                              var menuItem = itemList[i];
                              var locationInfo = '#';

                              if (self.isViewerMode == true) {
                                    if (menuItem.type == 'link') {
                                          locationInfo = menuItem.url.toString();
                                    } else if (menuItem.type == 'page') {
                                          locationInfo = menuItem.id.toString();
                                    }
                              }

                              var itemName = menuItem.name;

                              if (menuItem.type == "page" && menuItem.sync_page_name) {
                                    itemName = window.wemb.pageManager.getPageNameById(menuItem.id);
                              } else {
                                    itemName = self.getTranslateText(itemName);
                              }

                              var $a = $('<a></a>').attr("role", "button").attr("type", menuItem.type).attr("data-url", locationInfo).text(itemName);

                              if (menuItem.popup) {
                                    $a.attr("data-popup", menuItem.popup);

                                    if (menuItem.param) {
                                          var param = _typeof(menuItem.param) == "object" ? JSON.stringify(menuItem.param) : menuItem.param;
                                          $a.attr("data-param", param);
                                    }
                              }

                              $a.data("parent", parentItem);
                              $a.data("item", menuItem);
                              var $li = $('<li></li>').append($a);

                              if (self.isEditorMode == true) {
                                    $parent.append($li);
                                    continue;
                              }
                              /*
                               * 메뉴 항목 타입이 page 인 경우만  접근권한 체크하기
                               * 접근 권한이 없는  경우 메뉴  생성 X, 서브메뉴 생성도 X
                               */


                              if (menuItem.type == 'page' && self.userAccessiblePages.indexOf(locationInfo) == -1) {
                                    //접근권한  있는 경우만 추가
                                    $li.addClass("disabled");
                              }

                              var hasChildren = false;
                              /*
                               * 서브 메뉴 아이템이 있는 경우 재귀 호출
                               */

                              if (menuItem.children) {
                                    var $ul = $('<ul></ul>'); // 서브 메뉴 만들기 (재귀 호출)

                                    self.makeMenuItem(menuItem.children, {
                                          element: $ul,
                                          menuItem: menuItem
                                    });
                                    /*
                                     * $ul == 서브 메뉴 아이템 메인
                                     * $li == 부모의 현재 메뉴 아이템
                                     * $parent == 현재 부모(ul 태그, $li의 부모)
                                     * $ul의 자식이 있을 경우에만 추가하기
                                     */

                                    if ($ul.children().length > 0) {
                                          hasChildren = true;
                                          $li.append($ul);
                                    }
                              }

                              if (menuItem.type == "link" || hasChildren || menuItem.type != "page" && menuItem.always_visible || menuItem.type == "page" && self.userAccessiblePages.indexOf(locationInfo) != -1) {
                                    //외부 링크, 권한이 있는 페이지, 자식이 있는 Item, page가 아니고 항시노출 옵션이 true인 경우
                                    $parent.append($li);
                              }
                        }
                  }
            }, {
                  key: "background_hover_color",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("normal", "background_hover_color", value)) {
                              this._invalidateProperty = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("normal", "background_hover_color");
                  }
            }, {
                  key: "active_color",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("normal", "active_color", value)) {
                              this._invalidateProperty = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("normal", "active_color");
                  }
            }, {
                  key: "menusetId",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("extension", "menuSetId", value)) {}
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("extension", "menuSetId");
                  }
            }, {
                  key: "style",
                  get: function get() {
                        return this.getGroupProperties("style");
                  }
            }, {
                  key: "font",
                  get: function get() {
                        return this.getGroupProperties("font");
                  }
            }]);

            return MenuComponent;
      }(WVDOMComponent);


WVPropertyManager.attach_default_component_infos(MenuComponent, {
    "info": {
        "componentName": "MenuComponent",
        "version": "1.2.0"
    },
    "setter": {
        "width": 450,
        "height": 45
    },
    "normal": {
        "background_hover_color": "#434a50",
        "active_color": "#2db89a"
    },
    "style": {
        "backgroundColor": "#545c64"
    },
    "font": {
        "font_type": "inherit",
        "font_color": "#ffffff",
        "font_size": 12,
        "font_weight": "normal"
    },
    "label": {
        "label_using": "N",
        "label_text": "Menu Component"
    },
    "extension": {
        "menuSetId": null
    }
});
WVPropertyManager.add_event(MenuComponent, {
    name: "selectMenuItem",
    label: "selectMenuItem 이벤트",
    description: "selectMenuItem 이벤트 입니다.",
    properties: []
}); // 프로퍼티 패널에서 사용할 정보 입니다.

MenuComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    template: "background"
}, {
    template: "border"
}, {
    template: "font"
}, {
    label: "Status Color",
    owner: "vertical",
    children: [{
        owner: "normal",
        name: "background_hover_color",
        type: "color",
        label: "Over",
        show: true,
        writable: true,
        description: "마우스 오버 배경색 값"
    }, {
        owner: "normal",
        name: "active_color",
        type: "color",
        label: "Active",
        show: true,
        writable: true,
        description: "현재 메뉴 색상"
    }]
}, {
    label: "Menuset",
    template: "menuset",
    children: [{
        owner: "extension",
        name: "menuSetId",
        show: true,
        label: "Name",
        writable: true,
        description: "메뉴셋 선택"
    }]
}];
WVPropertyManager.add_property_group_info(MenuComponent, {
    label: "MenuComponent 고유 속성",
    children: [{
        name: "menusetId",
        type: "string",
        show: true,
        writable: true,
        description: "설정된 menuset ID 입니다."
    }]
}); //  추후 추가 예정

MenuComponent.method_info = [];
