class TriangleProgressComponent extends WVDOMComponent {
    constructor() {
        super();
        this._invalidateProperty = false;
    }

    _onImmediateUpdateDisplay() {
        this._validateProperty();
    }

    _onDestroy() {
        super._onDestroy();
    }


    _onCommitProperties() {
        if (this._updatePropertiesMap.has("childStyle") ||
            this._updatePropertiesMap.has("setter.width") ||
            this._updatePropertiesMap.has("setter.height")) {
            this.validateCallLater(this._validateProperty)
        }

        if (this._invalidateProperty) {
            this.validateCallLater(this._validateSetterProperty);
        }
    }

    _validateProperty() {
        var styleProperty = this.childStyle;
        var setter = this._properties.setter;
        var size = setter.width < setter.height ? setter.width : setter.height;

        $(this._element)
            .circleProgress({
                value: this.value * 0.01,
                size: size,
                emptyFill: styleProperty.emptyFill,
                fill: {
                    gradient: [styleProperty.gradientStart, styleProperty.gradientEnd]
                },
                lineCap: styleProperty.lineCap,
                thickness: styleProperty.thickness,
                drawFrame: function(v) {
                    var ctx = this.ctx,
                        s = this.size,
                        r = this.radius * 0.9,
                        t = this.thickness,
                        da = Math.PI / 3,
                        dx = r / 10,
                        dy = r / 10 + 25,
                        lc = this.lineCap,
                        lj = 'miter';

                    ctx.clearRect(0, 0, this.size, this.size);
                    ctx.lineWidth = t;
                    ctx.lineJoin = lj;
                    ctx.lineCap = lc;

                    function getX(angle) {
                        return r * (1 + Math.cos(angle));
                    }

                    function getY(angle) {
                        return r * (1 + Math.sin(angle));
                    }

                    var a1 = -Math.PI * 5 / 6 + da,
                        x1 = getX(a1) + dx,
                        y1 = getY(a1) + dy;

                    var a2 = -Math.PI / 6 + da,
                        x2 = getX(a2) + dx,
                        y2 = getY(a2) + dy;

                    var a3 = Math.PI / 2 + da,
                        x3 = getX(a3) + dx,
                        y3 = getY(a3) + dy;

                    // Draw "empty" path
                    ctx.save();
                    ctx.beginPath();
                    ctx.moveTo(x1, y1);
                    ctx.lineTo(x2, y2);
                    ctx.lineTo(x3, y3);
                    ctx.closePath();
                    ctx.strokeStyle = this.emptyFill;
                    ctx.stroke();
                    ctx.restore();

                    // Draw "filled" path
                    ctx.save();
                    ctx.beginPath();

                    var p0 = 0.96;
                    ctx.moveTo(
                        x1 * p0 + x3 * (1 - p0),
                        y1 * p0 + y3 * (1 - p0)
                    );

                    ctx.lineTo(x1, y1);

                    var p1 = v > 1 / 3 ? 1 : v * 3;
                    ctx.lineTo(
                        x2 * p1 + x1 * (1 - p1),
                        y2 * p1 + y1 * (1 - p1)
                    );

                    if (v > 1 / 3) {
                        var p2 = v > 2 / 3 ? 1 : v * 3 - 1;
                        ctx.lineTo(
                            x3 * p2 + x2 * (1 - p2),
                            y3 * p2 + y2 * (1 - p2)
                        );
                    }

                    if (v > 2 / 3) {
                        var p3 = v * 3 - 2;
                        ctx.lineTo(
                            x1 * p3 + x3 * (1 - p3),
                            y1 * p3 + y3 * (1 - p3)
                        );
                    }

                    if (v >= 1)
                        ctx.closePath();

                    var gr = this.fill.gradient;

                    if (gr.length == 1) {
                        this.arcFill = gr[0];
                    } else if (gr.length > 1) {
                        var ga = this.fill.gradientAngle || 0, // gradient direction angle; 0 by default
                            gd = this.fill.gradientDirection || [
                                this.size / 2 * (1 - Math.cos(ga)), // x0
                                this.size / 2 * (1 + Math.sin(ga)), // y0
                                this.size / 2 * (1 + Math.cos(ga)), // x1
                                this.size / 2 * (1 - Math.sin(ga)) // y1
                            ];

                        var lg = ctx.createLinearGradient.apply(ctx, gd);

                        for (var i = 0; i < gr.length; i++) {
                            var color = gr[i],
                                pos = i / (gr.length - 1);

                            if ($.isArray(color)) {
                                pos = color[1];
                                color = color[0];
                            }

                            lg.addColorStop(pos, color);
                        }

                        this.arcFill = lg;
                    }

                    ctx.strokeStyle = this.arcFill;
                    ctx.stroke();
                    ctx.restore();
                }
            });

        /*$(this._element).find("canvas").css("pointer-events", "none");*/
    }

    _validateSetterProperty() {
        $(this._element).circleProgress("value", this.value * 0.01);
        this._invalidateProperty = false;
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: this.value
            })
        }
    }


    set value(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "value", value)) {
            this._invalidateProperty = true;
        }
    }

    get value() {
        return this.getGroupPropertyValue("setter", "value");
    }

    get childStyle() {
        return this.getGroupProperties("childStyle");
    }
}

// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(TriangleProgressComponent, {
    "info": {
        "componentName": "TriangleProgressComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 300,
        "value": 90
    },

    "childStyle": {
        "lineCap": "round",
        "gradientStart": "#F02FC2",
        "gradientEnd": "#6094EA",
        "emptyFill": "rgba(0, 0, 0, .1)",
        "thickness": 15
    },

    "label": {
        "label_using": "N",
        "label_text": "Triangle Progress"
    },
});

// 기본 프로퍼티 정보입니다.
TriangleProgressComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        template: "label"
    }, {
        label: "Style",
        template: "vertical",
        children: [{
            owner: "childStyle",
            name: "emptyFill",
            type: "color",
            label: "Fill Color",
            show: true,
            writable: true,
            description: "빈 색상"
        }, {
            owner: "childStyle",
            name: "gradientStart",
            type: "color",
            label: "Start Color",
            show: true,
            writable: true,
            description: "그라데이션 시작 색상"
        }, {
            owner: "childStyle",
            name: "gradientEnd",
            type: "color",
            label: "End Color",
            show: true,
            writable: true,
            description: "그라데이션 끝 색상"
        }, {
            owner: "childStyle",
            name: "lineCap",
            type: "select",
            options: {
                items: [
                    { label: "round", value: "round" },
                    { label: "square", value: "square" }
                ]
            },
            label: "Line Cap",
            writable: true,
            show: true,
            description: "모양"
        }, {
            owner: "childStyle",
            name: "thickness",
            type: "number",
            label: "Thickness",
            show: true,
            writable: true,
            description: "thickness 값",
            options: {
                min: 0,
                max: 100
            }
        }]
    }
];


//  추후 추가 예정
TriangleProgressComponent.method_info = [];


// 이벤트 정보
WVPropertyManager.add_event(TriangleProgressComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.add_property_group_info(TriangleProgressComponent, {
    label: "TriangleProgressComponent 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "백분율 값입니다(0~100)."
    }]
});
