class SemiCircleProgressComponent extends WVDOMComponent {
    constructor() {
        super();
        this.hideMask = null;
        this.semiCircle = null;
        this._invalidateProperty = false;
        this.numberForCircle = 0.01; 
        this.numberForSemiCircle = this.numberForCircle / 2;
    }

    _onCreateElement() {
        this.hideMask = document.createElement('div');
        this.semiCircle = document.createElement('div');
        $(this.hideMask).css("overflow", "hidden");
        $(this.hideMask).append($(this.semiCircle));
        $(this._element).append($(this.hideMask));
    }
    
    _onImmediateUpdateDisplay() {
        this._validateProperty();
    }

    _onDestroy() {
        super._onDestroy();
    }

    _onCommitProperties() {
         const keys = ['childStyle', 'setter.width', 'setter.height'];
         if (keys.some(key => this._updatePropertiesMap.has(key))) {
            this.validateCallLater(this._validateProperty);
        }
        if (this._invalidateProperty) {
            this.validateCallLater(this._validateSetterProperty);
        }
    }

    _validateProperty() {
        var styleProperty = this.childStyle;
            var setter = this._properties.setter;
            var size = setter.width < setter.height ? setter.width : setter.height;
            var heightForHalfCircle = size / 2;

            $(this.hideMask).width(size);
            $(this.hideMask).height(heightForHalfCircle);
            $(this.semiCircle).circleProgress({
                value: this.value * this.numberForSemiCircle, 
                size: size,
                lineCap: styleProperty.lineCap,
                startAngle: Math.PI,
                fill: {
                    gradient: [styleProperty.gradientStart, styleProperty.gradientEnd]
                },
                emptyFill: styleProperty.emptyFill,
                thickness: styleProperty.thickness
            });

    }

    _validateSetterProperty() {
        $(this._element).circleProgress("value", this.value * this.numberForSemiCircle);
        this._invalidateProperty = false;
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: this.value
            })
        }
    }



    set value(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "value", value)) {
            this._invalidateProperty = true;
        }
    }

    get value() {
        return this.getGroupPropertyValue("setter", "value");
    }

    get childStyle() {
        return this.getGroupProperties("childStyle");
    }
}


WVPropertyManager.attach_default_component_infos(SemiCircleProgressComponent, {
    "info": {
        "componentName": "SemiCircleProgressComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 300,
        "value": 90
    },

    "childStyle": {
        "lineCap": "round",
        "gradientStart": "#F02FC2",
        "gradientEnd": "#6094EA",
        "emptyFill": "rgba(0, 0, 0, .1)",
        "thickness": 15
    },

    "label": {
        "label_using": "N",
        "label_text": "Semi Circle Progress"
    }
});



SemiCircleProgressComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        template: "label"
    }, {
        label: "Style",
        template: "vertical",
        children: [{
            owner: "childStyle",
            name: "emptyFill",
            type: "color",
            label: "Fill Color",
            show: true,
            writable: true,
            description: "빈 색상"
        }, {
            owner: "childStyle",
            name: "gradientStart",
            type: "color",
            label: "Start Color",
            show: true,
            writable: true,
            description: "그라데이션 시작 색상"
        }, {
            owner: "childStyle",
            name: "gradientEnd",
            type: "color",
            label: "End Color",
            show: true,
            writable: true,
            description: "그라데이션 끝 색상"
        }, {
            owner: "childStyle",
            name: "lineCap",
            label: "Line Cap",
            type: "select",
            options: {
                items: [
                    { label: "round", value: "round" },
                    { label: "square", value: "square" }
                ]
            },
            writable: true,
            show: true,
            description: "모양",
        }, {
            owner: "childStyle",
            name: "thickness",
            type: "number",
            label: "Thickness",
            show: true,
            writable: true,
            description: "thickness 값",
            options: {
                min: 0,
                max: 100
            }
        }]
    }
];



SemiCircleProgressComponent.method_info = [];



WVPropertyManager.add_event(SemiCircleProgressComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.add_property_group_info(SemiCircleProgressComponent, {
    label: "SemiCircleProgressComponent 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "백분율 값입니다(0~100)."
    }]
});
