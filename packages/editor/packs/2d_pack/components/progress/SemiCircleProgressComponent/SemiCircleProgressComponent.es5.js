'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SemiCircleProgressComponent = function (_WVDOMComponent) {
    _inherits(SemiCircleProgressComponent, _WVDOMComponent);

    function SemiCircleProgressComponent() {
        _classCallCheck(this, SemiCircleProgressComponent);

        var _this = _possibleConstructorReturn(this, (SemiCircleProgressComponent.__proto__ || Object.getPrototypeOf(SemiCircleProgressComponent)).call(this));

        _this.hideMask = null;
        _this.semiCircle = null;
        _this._invalidateProperty = false;
        _this.numberForCircle = 0.01;
        _this.numberForSemiCircle = _this.numberForCircle / 2;
        return _this;
    }

    _createClass(SemiCircleProgressComponent, [{
        key: '_onCreateElement',
        value: function _onCreateElement() {
            this.hideMask = document.createElement('div');
            this.semiCircle = document.createElement('div');
            $(this.hideMask).css("overflow", "hidden");
            $(this.hideMask).append($(this.semiCircle));
            $(this._element).append($(this.hideMask));
        }
    }, {
        key: '_onImmediateUpdateDisplay',
        value: function _onImmediateUpdateDisplay() {
            this._validateProperty();
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            _get(SemiCircleProgressComponent.prototype.__proto__ || Object.getPrototypeOf(SemiCircleProgressComponent.prototype), '_onDestroy', this).call(this);
        }
    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            var _this2 = this;

            var keys = ['childStyle', 'setter.width', 'setter.height'];
            if (keys.some(function (key) {
                return _this2._updatePropertiesMap.has(key);
            })) {
                this.validateCallLater(this._validateProperty);
            }
            if (this._invalidateProperty) {
                this.validateCallLater(this._validateSetterProperty);
            }
        }
    }, {
        key: '_validateProperty',
        value: function _validateProperty() {
            var styleProperty = this.childStyle;
            var setter = this._properties.setter;
            var size = setter.width < setter.height ? setter.width : setter.height;
            var heightForHalfCircle = size / 2;

            $(this.hideMask).width(size);
            $(this.hideMask).height(heightForHalfCircle);
            $(this.semiCircle).circleProgress({
                value: this.value * this.numberForSemiCircle,
                size: size,
                lineCap: styleProperty.lineCap,
                startAngle: Math.PI,
                fill: {
                    gradient: [styleProperty.gradientStart, styleProperty.gradientEnd]
                },
                emptyFill: styleProperty.emptyFill,
                thickness: styleProperty.thickness
            });
        }
    }, {
        key: '_validateSetterProperty',
        value: function _validateSetterProperty() {
            $(this._element).circleProgress("value", this.value * this.numberForSemiCircle);
            this._invalidateProperty = false;
            if (!this.isEditorMode) {
                this.dispatchWScriptEvent("change", {
                    value: this.value
                });
            }
        }
    }, {
        key: 'value',
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "value", value)) {
                this._invalidateProperty = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "value");
        }
    }, {
        key: 'childStyle',
        get: function get() {
            return this.getGroupProperties("childStyle");
        }
    }]);

    return SemiCircleProgressComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(SemiCircleProgressComponent, {
    "info": {
        "componentName": "SemiCircleProgressComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 300,
        "value": 90
    },

    "childStyle": {
        "lineCap": "round",
        "gradientStart": "#F02FC2",
        "gradientEnd": "#6094EA",
        "emptyFill": "rgba(0, 0, 0, .1)",
        "thickness": 15
    },

    "label": {
        "label_using": "N",
        "label_text": "Semi Circle Progress"
    }
});

SemiCircleProgressComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Style",
    template: "vertical",
    children: [{
        owner: "childStyle",
        name: "emptyFill",
        type: "color",
        label: "Fill Color",
        show: true,
        writable: true,
        description: "빈 색상"
    }, {
        owner: "childStyle",
        name: "gradientStart",
        type: "color",
        label: "Start Color",
        show: true,
        writable: true,
        description: "그라데이션 시작 색상"
    }, {
        owner: "childStyle",
        name: "gradientEnd",
        type: "color",
        label: "End Color",
        show: true,
        writable: true,
        description: "그라데이션 끝 색상"
    }, {
        owner: "childStyle",
        name: "lineCap",
        label: "Line Cap",
        type: "select",
        options: {
            items: [{ label: "round", value: "round" }, { label: "square", value: "square" }]
        },
        writable: true,
        show: true,
        description: "모양"
    }, {
        owner: "childStyle",
        name: "thickness",
        type: "number",
        label: "Thickness",
        show: true,
        writable: true,
        description: "thickness 값",
        options: {
            min: 0,
            max: 100
        }
    }]
}];

SemiCircleProgressComponent.method_info = [];

WVPropertyManager.add_event(SemiCircleProgressComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(SemiCircleProgressComponent, {
    label: "SemiCircleProgressComponent 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "백분율 값입니다(0~100)."
    }]
});