class BarProgressComponent extends WVDOMComponent {
    constructor() {
        super();
        this.barElement = null;
        this.$bar;
        this._invalidateProperty = false;

    }

    _onCreateElement() {
        this._element.style.boxSizing = "border-box";
        this._element.style.overflow = "hidden";
        this.barElement = document.createElement("div");
        this.barElement.style.width = 0;
        this.barElement.style.height = "100%";
        this.$bar = $(this.barElement);
        /*this.barElement.style.pointerEvents = "none";*/

        this._element.appendChild(this.barElement);
    }

    _onDestroy() {
        this.$bar.remove();
        this.$bar = null;
        this.barElement = null;
        super._onDestroy();
    }

    _onImmediateUpdateDisplay() {
        this._validateChildStyleProperty();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("childStyle")) {
            this.validateCallLater(this._validateChildStyleProperty)
        }

        if (this._invalidateProperty) {
            this.validateCallLater(this._validateSetterProperty);
        }
    }

    _validateChildStyleProperty() {
        var styleProps = this.childStyle;

        /**
         * 바 엘리먼트 스타일 적용
         */
        this.$bar.css("border-radius", styleProps.barRadius)
            .css("background", '-webkit-linear-gradient(right, ' + styleProps.gradientStart + ',' + styleProps.gradientEnd + ')')
            .css("background", '-ms-linear-gradient(right, ' + styleProps.gradientStart + ',' + styleProps.gradientEnd + ')')
            .animate({ width: this.value + "%" }, 1000);
    }

    _validateSetterProperty() {
        this.$bar.animate({ width: this.value + "%" }, 1000);
        this._invalidateProperty = false;
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: this.value
            })
        }
    }

    set value(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "value", value)) {
            this._invalidateProperty = true;
        }
    }

    get value() {
        return this.getGroupPropertyValue("setter", "value");
    }

    get childStyle() {
        return this.getGroupProperties("childStyle");
    }
}

/*WVPropertyManager.attach_default_component_infos(BarProgressComponent);*/

// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(BarProgressComponent, {
    "info": {
        "componentName": "BarProgressComponent",
        "version": "1.0.0",
        "category": "2D"
    },

    "setter": {
        "width": 500,
        "height": 25,
        "value": 80
    },

    "childStyle": {
        "barRadius": 20,
        "gradientStart": "#F02FC2",
        "gradientEnd": "#6094EA",
    },

    "label": {
        "label_using": "N",
        "label_text": "Bar Progress"
    },

    "style": {
        "backgroundColor": "rgba(0, 0, 0, .1)",
        "padding": 0,
        "border": "1px none #000000",
        "borderRadius": 20
    }
})

// 프로퍼티 패널에서 사용할 정보 입니다.
BarProgressComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        template: "label"
    }, {
        template: "border"
    }, {
        label: "Style",
        template: "vertical",
        children: [{
            owner: "style",
            name: "backgroundColor",
            type: "color",
            label: "Fill Color",
            show: true,
            writable: true,
            description: "배경색 값"
        }, {
            owner: "childStyle",
            name: "gradientStart",
            type: "color",
            label: "Start Color",
            show: true,
            writable: true,
            description: "그라데이션 시작 색상"
        }, {
            owner: "childStyle",
            name: "gradientEnd",
            type: "color",
            label: "End Color",
            show: true,
            writable: true,
            description: "그라데이션 끝 색상"
        }, {
            owner: "style",
            name: "padding",
            type: "number",
            label: "Padding",
            show: true,
            writable: true,
            description: "bgPadding 값",
            options: {
                min: 0,
                max: 1000
            }
        }, {
            owner: "childStyle",
            name: "barRadius",
            type: "number",
            label: "BarRadius",
            tag: "px",
            show: true,
            writable: true,
            description: "Radius 값",
            options: {
                min: 0,
                max: 1000
            }
        }]
    }
]


// 이벤트 정보
WVPropertyManager.add_event(BarProgressComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "number",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(BarProgressComponent, {
    label: "BarProgressComponent 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "백분율 값입니다(0~100)."
    }]
});
