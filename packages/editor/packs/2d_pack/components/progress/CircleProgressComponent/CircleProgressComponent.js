class CircleProgressComponent extends WVDOMComponent {
    constructor() {
        super();
        this._invalidateProperty = false;
    }

    _onImmediateUpdateDisplay() {
        this._validateProperty();
    }

    _onDestroy() {
        super._onDestroy();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("childStyle") ||
            this._updatePropertiesMap.has("setter.width") ||
            this._updatePropertiesMap.has("setter.height")) {
            this.validateCallLater(this._validateProperty)
        }

        if (this._invalidateProperty) {
            this.validateCallLater(this._validateSetterProperty);
        }
    }

    _validateProperty() {
        var styleProperty = this.childStyle;
        var setter = this._properties.setter;
        var size = setter.width < setter.height ? setter.width : setter.height;

        $(this._element)
            .circleProgress({
                value: this.value * 0.01,
                size: size,
                lineCap: styleProperty.lineCap,
                startAngle: (Math.PI + Math.PI / 2),
                fill: {
                    gradient: [
                        styleProperty.gradientStart,
                        styleProperty.gradientEnd
                    ]
                },
                emptyFill: styleProperty.emptyFill,
                thickness: styleProperty.thickness
            });

        /*$(this._element).find("canvas").css("pointer-events", "none");*/
    }

    _validateSetterProperty() {
        $(this._element).circleProgress("value", this.value * 0.01);
        this._invalidateProperty = false;
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: this.value
            })
        }
    }



    set value(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "value", value)) {
            this._invalidateProperty = true;
        }
    }

    get value() {
        return this.getGroupPropertyValue("setter", "value");
    }

    get childStyle() {
        return this.getGroupProperties("childStyle");
    }
}

// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(CircleProgressComponent, {
    "info": {
        "componentName": "CircleProgressComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 300,
        "value": 90
    },

    "childStyle": {
        "lineCap": "round",
        "gradientStart": "#F02FC2",
        "gradientEnd": "#6094EA",
        "emptyFill": "rgba(0, 0, 0, .1)",
        "thickness": 15
    },

    "label": {
        "label_using": "N",
        "label_text": "Circle Progress"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
CircleProgressComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        template: "label"
    }, {
        label: "Style",
        template: "vertical",
        children: [{
            owner: "childStyle",
            name: "emptyFill",
            type: "color",
            label: "Fill Color",
            show: true,
            writable: true,
            description: "빈 색상"
        }, {
            owner: "childStyle",
            name: "gradientStart",
            type: "color",
            label: "Start Color",
            show: true,
            writable: true,
            description: "그라데이션 시작 색상"
        }, {
            owner: "childStyle",
            name: "gradientEnd",
            type: "color",
            label: "End Color",
            show: true,
            writable: true,
            description: "그라데이션 끝 색상"
        }, {
            owner: "childStyle",
            name: "lineCap",
            label: "Line Cap",
            type: "select",
            options: {
                items: [
                    { label: "round", value: "round" },
                    { label: "square", value: "square" }
                ]
            },
            writable: true,
            show: true,
            description: "모양",
        }, {
            owner: "childStyle",
            name: "thickness",
            type: "number",
            label: "Thickness",
            show: true,
            writable: true,
            description: "thickness 값",
            options: {
                min: 0,
                max: 100
            }
        }]
    }
];


//  추후 추가 예정
CircleProgressComponent.method_info = [];


// 이벤트 정보
WVPropertyManager.add_event(CircleProgressComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.add_property_group_info(CircleProgressComponent, {
    label: "CircleProgressComponent 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "백분율 값입니다(0~100)."
    }]
});
