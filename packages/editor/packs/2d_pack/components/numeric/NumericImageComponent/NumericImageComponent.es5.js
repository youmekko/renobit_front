"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function() { var gen = fn.apply(this, arguments); return new Promise(function(resolve, reject) {
            function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function(value) { step("next", value); }, function(err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumericImageComponent = function(_NumericCoreComponent) {
    _inherits(NumericImageComponent, _NumericCoreComponent);

    function NumericImageComponent() {
        _classCallCheck(this, NumericImageComponent);

        var _this = _possibleConstructorReturn(this, (NumericImageComponent.__proto__ || Object.getPrototypeOf(NumericImageComponent)).call(this));

        _this._isResourceComponent = true;
        return _this;
    }

    _createClass(NumericImageComponent, [{
        key: "startLoadResource",
        value: function() {
            var _ref = _asyncToGenerator( /*#__PURE__*/ regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _context.next = 3;
                                return this.loadImage();

                            case 3:
                                this._dataRender();
                                _context.next = 9;
                                break;

                            case 6:
                                _context.prev = 6;
                                _context.t0 = _context["catch"](0);

                                console.log("error ", _context.t0);

                            case 9:
                                _context.prev = 9;

                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                return _context.finish(9);

                            case 12:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [
                    [0, 6, 9, 12]
                ]);
            }));

            function startLoadResource() {
                return _ref.apply(this, arguments);
            }

            return startLoadResource;
        }()
    }, {
        key: "getCompSVGPath",
        value: function getCompSVGPath() {
            var componentPath = window.wemb.componentLibraryManager.getComponentPath("NumericImageComponent");
            var src = componentPath + "/res/person.svg";
            return src;
        }
    }, {
        key: "loadImage",
        value: function loadImage() {
            var _this2 = this;

            var self = this;
            return new Promise(function(resolve, reject) {
                self.img = new Image();
                self.img.crossOrigin = "anonymous";
                self.img.src = _this2.getCompSVGPath();
                $(self.img).off("load").on('load', function() {
                    resolve();
                });
            });
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.canvas_controller = new CanvasController(this._element);
            this.canvas_controller.options.refresh = true;
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_dataRender",
        value: function _dataRender() {
            if (this.img) {
                this.canvas_controller.resize(this.width, this.height);
                this.drawImage();
            }
        }
    }, {
        key: "drawImage",
        value: function drawImage() {
            var self = this;
            self.canvas_controller.resize(this.width, this.height);
            self.canvas_controller.frame = [];

            var percent = _get(NumericImageComponent.prototype.__proto__ || Object.getPrototypeOf(NumericImageComponent.prototype), "getPercent", this).call(this);
            var unitPercent = percent / 100;
            for (var i = 30; i > 0; i--) {
                self.canvas_controller.frame.push({
                    shape: 'rectangle',
                    frame: true,
                    x: self.width / 2 - self.width * unitPercent / i / 2,
                    y: self.height - (self.height - self.height / 8) * unitPercent / i,
                    image: self.img,
                    width: self.width * unitPercent / i,
                    height: (self.height - self.height / 8) / i * unitPercent,
                    radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                    children: [{
                        shape: 'rectangle',
                        frame: true,
                        x: self.width / 2 - self.width * unitPercent / i / 2,
                        y: self.height - (self.height - self.height / 8) / i * unitPercent,
                        fill: this.color,
                        width: self.width * unitPercent / i,
                        height: (self.height - self.height / 8) / i * unitPercent,
                        radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                        composite: "source-in"
                    }, {
                        shape: 'text',
                        baseline: 'bottom',
                        x: 0,
                        y: self.height - (self.height - self.height / 8) / i * unitPercent - self.height / 8,
                        width: self.width,
                        height: self.height / 8,
                        text: percent,
                        font: self.height / 8,
                        fill: this.textColor,
                        frame: false
                    }]
                });
            }

            self.canvas_controller.draw();
            self.canvas_controller.animate(function() {});
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            var self = this;
            _get(NumericImageComponent.prototype.__proto__ || Object.getPrototypeOf(NumericImageComponent.prototype), "_onCommitProperties", this).call(this);
            if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
                self.drawImage();
            }

            if (this._updatePropertiesMap.has('extension')) {
                self.drawImage();
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.img = null;
            this.canvas_controller.dispose();
        }
    }, {
        key: "color",
        get: function get() {
            return this.getGroupPropertyValue("extension", "color");
        }
    }, {
        key: "textColor",
        get: function get() {
            return this.getGroupPropertyValue("extension", "textColor");
        }
    }]);

    return NumericImageComponent;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(NumericImageComponent, {
    "info": {
        "componentName": "NumericImageComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(109,181,255,1)",
        "textColor": "rgba(200,200,200,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Image Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericImageComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "textColor"
    }]
}];

WVPropertyManager.add_property_group_info(NumericImageComponent, {
    label: "NumericImageComponent 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericImageComponent, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
