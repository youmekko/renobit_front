class NumericImageComponent extends NumericCoreComponent {

    constructor() {
        super();
        this._isResourceComponent = true;
    }

    async startLoadResource() {
        try {
            await this.loadImage();
            this._dataRender();
        } catch (error) {
            console.log("error ", error);
        } finally {
            this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
        }
    }

    getCompSVGPath() {
        let componentPath = window.wemb.componentLibraryManager.getComponentPath("NumericImageComponent");
        let src = componentPath + "/res/person.svg";
        return src;
    }

    loadImage() {
        var self = this;
        return new Promise((resolve, reject) => {
            self.img = new Image();
            self.img.crossOrigin = "anonymous"
            self.img.src = this.getCompSVGPath();
            $(self.img).off("load").on('load', function() {
                resolve();
            })
        })
    }

    _onCreateElement() {
        this.canvas_controller = new CanvasController(this._element);
        this.canvas_controller.options.refresh = true;
    }

    onLoadPage() {

    }

    _dataRender() {
        if (this.img) {
            this.canvas_controller.resize(this.width, this.height);
            this.drawImage();
        }
    }

    drawImage() {
        var self = this;
        self.canvas_controller.resize(this.width, this.height);
        self.canvas_controller.frame = [];

        var percent = super.getPercent();
        var unitPercent = percent / 100;
        for (var i = 30; i > 0; i--) {
            self.canvas_controller.frame.push({
                shape: 'rectangle',
                frame: true,
                x: self.width / 2 - self.width * unitPercent / i / 2,
                y: self.height - (self.height - self.height / 8) * unitPercent / i,
                image: self.img,
                width: self.width * unitPercent / i,
                height: (self.height - self.height / 8) / i * unitPercent,
                radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                children: [{
                    shape: 'rectangle',
                    frame: true,
                    x: self.width / 2 - self.width * unitPercent / i / 2,
                    y: self.height - (self.height - self.height / 8) / i * unitPercent,
                    fill: this.color,
                    width: self.width * unitPercent / i,
                    height: (self.height - self.height / 8) / i * unitPercent,
                    radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                    composite: "source-in"
                }, {
                    shape: 'text',
                    baseline: 'bottom',
                    x: 0,
                    y: self.height - (self.height - self.height / 8) / i * unitPercent - self.height / 8,
                    width: self.width,
                    height: self.height / 8,
                    text: percent,
                    font: self.height / 8,
                    fill: this.textColor,
                    frame: false
                }]
            });
        }

        self.canvas_controller.draw();
        self.canvas_controller.animate(function() {

        });
    }

    _onCommitProperties() {
        var self = this;
        super._onCommitProperties();
        if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
            self.drawImage();
        }

        if (this._updatePropertiesMap.has('extension')) {
            self.drawImage();
        }
    }

    _onDestroy() {
        this.img = null;
        this.canvas_controller.dispose();
    }

    get color() {
        return this.getGroupPropertyValue("extension", "color");
    }

    get textColor() {
        return this.getGroupPropertyValue("extension", "textColor");
    }
}

WVPropertyManager.attach_default_component_infos(NumericImageComponent, {
    "info": {
        "componentName": "NumericImageComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(109,181,255,1)",
        "textColor": "rgba(200,200,200,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Image Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericImageComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "textColor"
    }]
}]



WVPropertyManager.add_property_group_info(NumericImageComponent, {
    label: "NumericImageComponent 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericImageComponent, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
