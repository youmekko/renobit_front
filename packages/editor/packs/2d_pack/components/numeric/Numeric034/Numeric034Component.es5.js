"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function() { var gen = fn.apply(this, arguments); return new Promise(function(resolve, reject) {
            function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function(value) { step("next", value); }, function(err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Numeric034Component = function(_NumericCoreComponent) {
    _inherits(Numeric034Component, _NumericCoreComponent);

    function Numeric034Component() {
        _classCallCheck(this, Numeric034Component);

        var _this = _possibleConstructorReturn(this, (Numeric034Component.__proto__ || Object.getPrototypeOf(Numeric034Component)).call(this));

        _this.loadedPage = false;
        _this.invalidateResource = false;
        _this.$container = null;
        _this.animateClip = null;
        _this.settingObj = {
            width: "",
            height: "",
            fps: 24,
            loop: true,
            columns: "",
            autoplay: false,
            totalFrames: ""
        };

        _this._isResourceComponent = true;
        return _this;
    }

    _createClass(Numeric034Component, [{
        key: "_onDestroy",
        value: function _onDestroy() {
            if (this.animateClip) {
                this.animateClip.destroy();
                this.animateClip = null;
            }
            this.$container.remove();
            this.$container = null;
            this.settingObj = null;
            _get(Numeric034Component.prototype.__proto__ || Object.getPrototypeOf(Numeric034Component.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            var $el = $(this._element);
            $el.find(".comp-wrap").remove();
            this.$container = $("<div class='circle-wrap'><div class='mask'><div class='sprite-image'></div></div></div>");
            this.$container.css({
                "width": "100%",
                "height": "100%",
                "background-position": "center center",
                "background-repeat": "no-repeat",
                "transform-origin": "left top",
                "overflow": "hidden"
            });

            this.$container.find(".sprite-image").css({
                "position": "absolute",
                "background-position": "0 0",
                "background-repeat": "no-repeat",
                "width": 5000,
                "height": 5000,
                "pointer-events": "none"
            });

            $el.append('<div class="comp-wrap" style="width:100%;height:100%;overflow:hidden;"></div>').find(".comp-wrap").append(this.$container);
        }
    }, {
        key: "startLoadResource",
        value: function() {
            var _ref = _asyncToGenerator( /*#__PURE__*/ regeneratorRuntime.mark(function _callee() {
                var success;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _context.next = 3;
                                return this.loadSpriteResource();

                            case 3:
                                success = _context.sent;

                                if (success) {
                                    this.selectProperty();
                                }

                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                _context.next = 12;
                                break;

                            case 8:
                                _context.prev = 8;
                                _context.t0 = _context["catch"](0);

                                this.exceptionSymbolInfo("startLoadResource error");
                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);

                            case 12:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [
                    [0, 8]
                ]);
            }));

            function startLoadResource() {
                return _ref.apply(this, arguments);
            }

            return startLoadResource;
        }()
    }, {
        key: "_setNoImageStyle",
        value: function _setNoImageStyle() {
            this.$container.css({
                "background-image": "url(" + Numeric034Component.DEFAULT_IMAGE + ")"
            });
        }
    }, {
        key: "loadSpriteResource",
        value: function loadSpriteResource() {
            var _this2 = this;

            return new Promise(function(resolve, reject) {
                /*if (!this.getGroupPropertyValue("setter", "visible")) {
                    resolve(false);
                    return;
                }*/

                _this2.selectItem = _this2.getGroupPropertyValue("setter", "info");

                if (!_this2.selectItem) {
                    _this2._setNoImageStyle();
                    resolve(false);
                } else {
                    //wemb.configManager.serverUrl + this.selectItem.path
                    var self = _this2;
                    var $image = $("<img style='visibility:hidden;' class='temp-img'>");
                    _this2.$container.append($image);
                    $image.off("error").on("error", function() {
                        self._setNoImageStyle();
                        reject("error");
                        self.$container.find(".temp-img").remove();
                    }).attr("src", _this2.getCompImagePath());

                    $image.off("load").on("load", function() {
                        self.$container.find(".temp-img").remove();
                        $(self._element).find(".mask").css("visibility", 'visible');
                        resolve(true);
                    });
                }
            });
        }
    }, {
        key: "getCompImagePath",
        value: function getCompImagePath() {
            var componentPath = window.wemb.componentLibraryManager.getComponentPath("Numeric034Component");
            var src = componentPath + "/res/numeric_034@145x148x7x85.png";
            return src;
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {
            this.$container.find(".sprite-image").css({
                "backgroundImage": "url('" + this.getCompImagePath() + "')",
                "visibility": "visible"
            });

            if (!this.isEditorMode) {
                if (this.animateClip) {
                    this.animateClip.destroy();
                }

                this.animateClip = new AnimateSpriteClip(this.$container.find(".sprite-image").get(0), this.settingObj);
                this.animateClip.play();
            }
            this._dataRender();
        }
    }, {
        key: "setAnimateInfo",
        value: function setAnimateInfo() {
            var currentSprite = this.selectItem;
            this.settingObj.width = currentSprite.data.width;
            this.settingObj.height = currentSprite.data.height;
            this.settingObj.columns = currentSprite.data.columns;
            this.settingObj.totalFrames = currentSprite.data.totalFrames;

            if (!this.settingObj.width || !this.settingObj.height) {
                throw this.exceptionSymbolInfo("width || height");
            }

            this.calcScale();
        }
    }, {
        key: "calcScale",
        value: function calcScale() {
            if (this.$container && this.selectItem) {
                var oriWidth = parseInt(this.settingObj.width);
                var scale = 1 / oriWidth * this.width;
                this.$container.css({
                    "transform": "scale(" + scale + "," + scale + ")",
                    width: this.settingObj.width,
                    height: this.settingObj.height
                });
            }
        }
    }, {
        key: "_dataRender",
        value: function _dataRender() {
            var hei = $(this._element).find(".circle-wrap").height();
            var per = this.getPercent();
            var top = hei - hei / 100 * per;
            $(this._element).find(".mask").css({ 'top': top });
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            _get(Numeric034Component.prototype.__proto__ || Object.getPrototypeOf(Numeric034Component.prototype), "_onCommitProperties", this).call(this);
            if (this.invalidateSize || this.invalidateVisible) {
                this.validateCallLater(this.calcScale);
            }

            if (this.invalidateResource) {
                this.validateCallLater(this.dispatchSizeEvent);
                this.invalidateResource = false;
            }
        }
    }, {
        key: "dispatchSizeEvent",
        value: function dispatchSizeEvent() {
            if (this.isEditorMode) {
                this.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
            }
            this.setGroupPropertyValue("setter", "isSaved", true);
        }
    }, {
        key: "selectProperty",
        value: function selectProperty() {
            var info = this.getGroupPropertyValue("setter", "info");
            if (!info) {
                this.settingObj.width = "";
                this.settingObj.height = "";
                return;
            }

            //    this.procSelectItem(info);
            this.invalidateResource = true;
            //    this.invalidateProperties();
            this.setAnimateInfo();

            this.isSaved = true;
        }
    }, {
        key: "exceptionSymbolInfo",
        value: function exceptionSymbolInfo(type) {
            console.log("Numeric034Component _ 에러가 발생하였습니다. [" + type + "]");
        }
    }, {
        key: "isSaved",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "isSaved", value)) {}
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "isSaved");
        }
    }]);

    return Numeric034Component;
}(NumericCoreComponent);

Numeric034Component.DEFAULT_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAARTQAAEU0BwDlgYwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAWzSURBVGiB7ZldbJNVGMd/5+3n2m2U0ZaxMWg3QOl0jmXKhmFuETZBEr50iTfGGy+Mt8ZIjImJMYCJCSpcaDSC0Uj0gkQdMIU4xDiYDvkQ2Ma6dWyMDtqO0e5773u8GJ1sittCOwrZ/+78z3POk19Onvac54VZzWpWs7qfJAAqdvy2S0qRP35KLhFCiUgp/YnpgQJ7D28r3gugB4hCKFAzthThElL6RcJ6vAS4omP9bXQ1h7cVvx0dl2+vLRWJ741J4QHRAwMiAMp31HYIsAvEYHRCIq0CRkhYT7Mi+aF626pNcKtG9EIZtFkNpnmpJlM08GbfMAadoksy6RLSa7/epw0MjdyIjvUAOoXrxQ/bszcULuR+0c4DF4Z8/rAvOn5gamQWJNE0C5JoemBA9JOHTE9vfHmaDJuZp/PSyV1ki/X2d1TMQSwmIxc6erjQ0YNep+B2JvPMigVxh4o5SMGydDprwwCMqBqt18LsPniTuckmHnPPpTAnjez0FMQk+0xXMQfJyUxDpwhUTWIy6Hht43LMRj2nW4LUt3RTc86Pw2bm2YJMVi6zxyxvzEEAnGlW/IEIL69dQpbdCsDa/AzW5mcQigzycXUz+2pakEBRjGDi8qu1Ymk6EphvM4/zr/UM8NUxH+3BXtLTkvmippXjF6/FJGdcQJZmzsOWbOKMr3ucf/JSkPZgP1tLlvNcqYdH3Hb2H2/jRFPgrnPG7X8kJ3Me9d5RkEB4kF/Od1H9ZycrPZlk2EeLvXSFG4/Lzv5ffbRd772rfHEDWZaVhq8rTCgyyOdHvRyo62B1XhYel2MsRgBlBW6WZdnZ9X3DXcHEDcRhs5JiMbLv51ZausKsL1pKXk76v+JGT8bF0qx5dwUTNxABPOFZSFPnTRw2K5mO1P+NHQdzLTLtfHG9a+W6HFQ8nk2wp48jf7SAvHOsAMpWuMjNdvL+dxe5dPXmtHLF/dL40CI7ZQUuGtoC/DgJDMCqR7LIdTv5qKqRps6pw8zI7TfX5eTpQjdN7UGOnGqd9GRWP7aYXLeT3QenDjNj13jPYgeVZR68V0JThvG4pg4zo+8R51wrm0sexnslxE/1k9dMSf7UYWb8YeW0jcK0dHZPC+ajqgaOX+i6Y6weQNVw1DYEaOoMj03caoqRZNJNyzObTHSHBzAadFjNhrG4iV6mPZnGy0F0iqCswH3Ha70AFjpSOOvt4uvjPo6e7SLVYsAf6jcysRs/IjVTMDI4GIoMxaCdGZ7O2uTzrdeVEVWjvDCH/6Lx+W9wuM7LhsJMbvQO8VtjQHZ1D/RLoZmAsdea/taOzQI+Pbyt6O3oRPn22prRVn58PSm0xqbLwRckwlJRmK27HcZ7JcTBk81sKcpiTd4CJKDTKRz7y2+RCK+QnI7G3vvmgyauqkKWXroc6PuxvkWN1sxECBg9sMonF4sSz3wUyJFCFka3ufcgQPXrxadUIUub2gJ9VXXNsrXzBodOellXkDEGEZUANhcvwmo2IIRYv27niaKoT/n22hqY+OmNlwT4mEFPKmKRMvpJTawvyGDD4/9uqg+rGnsONtHZPcB8e4r0dgTDw9rIo3oAIeRpKUW+BqX/LJF6hJIupZw5T5Nogm6dItKCkSGkBHFbzQwOq3xY1UgoMszzZR6SzAaxv6cvKRTufyfWzYyYqOLd2krFKD5bucRuebE0WxECBoZGIbp7h9ny1HKSk4wAnPN2cexsWyghamSiqt8s/kZDbj7ZHBD7fvbSNzTCB/8BAWBJMqBppCTkiURVsePEq4qQu61mPULRsbVkOalW07iYX8+1c6bZfzEhTySq6jeK9mjwSu+Aqi10zlFTLMZx86FwP39euqqqmtyd0CcS1br36p5Cyiq7LcmYl+M0WEwGroZ6OdV4RQXlkNXVtum+AAFYs/33bL1Q39IpbFQ1marXKw1DI9qeOdntn3xbWan+DUMbLYtgF8ajAAAAAElFTkSuQmCC';

WVPropertyManager.attach_default_component_infos(Numeric034Component, {
    "info": {
        "componentName": "Numeric034Component",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "isSaved": false,
        "value": 10,
        "min": 0,
        "max": 100
    },

    "label": {
        "label_using": "N",
        "label_text": "Sprite Clip"
    }
});

// 기본 프로퍼티 정보입니다.

// 프로퍼티 패널에서 사용할 정보 입니다.
Numeric034Component.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}];

WVPropertyManager.add_property_group_info(Numeric034Component, {
    label: "Numeric034Component 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(Numeric034Component, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
