'use strict';

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumericDonutComponent4 = function(_NumericCoreComponent) {
    _inherits(NumericDonutComponent4, _NumericCoreComponent);

    function NumericDonutComponent4() {
        _classCallCheck(this, NumericDonutComponent4);

        return _possibleConstructorReturn(this, (NumericDonutComponent4.__proto__ || Object.getPrototypeOf(NumericDonutComponent4)).call(this));
    }

    _createClass(NumericDonutComponent4, [{
        key: '_onCreateElement',
        value: function _onCreateElement() {
            this.canvas_controller = new CanvasController(this._element);
            //this.svg_controller = new SvgController(this._element);
        }
    }, {
        key: 'onLoadPage',
        value: function onLoadPage() {}
    }, {
        key: '_dataRender',
        value: function _dataRender() {
            //  var svg_tag = '<g id="Layer0_0_FILL">  <path fill="#57E2FB" stroke="none" d="  M 155.3 6.1  Q 153.3 4.9 148.65 3.75 145.35 2.9 141.8 2.3 123.8 -0.65 103.85 0.2 75.6 1.4 54.9 10.55 26.35 23.1 11.7 43.8 -2.15 63.35 0.4 83.8 2.45 100.2 16.25 114.15 21.7 119.6 30.7 124.95  L 30.7 119.55  Q 22.25 111.2 20.15 103.95 17.45 94.25 20.95 84.4 23.55 77.05 29.25 70.7 31.2 68.5 33.55 66.45 41.85 59 50.9 55 56.35 52.6 57.35 52.25 72.7 46.9 88.9 44.6 93.65 43.9 102.5 43.45 110.85 43.05 115.15 43.15  L 155.3 6.1 Z"/>  </g>';
            //  this.svg_controller.append(svg_tag);

            var self = this;

            // 캔버스 컨트롤러 초기화
            this.canvas_controller.frame = [];
            this.canvas_controller.resize(this.width, this.height);
            this.canvas_controller.options.refresh = true;

            // 퍼센트 정보 겟!
            var percent = _get(NumericDonutComponent4.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent4.prototype), 'getPercent', this).call(this);

            var radius = (self.width < self.height ? self.width : self.height) - 5;

            // 기본 도넛 모양
            var deco_x = self.width / 2 + radius / 4 * Math.cos(Math.PI * 5 / 6);
            var deco_y = self.height / 2 + radius / 4 * Math.sin(Math.PI * 5 / 6);
            var deco_x1 = self.width / 2 + radius / 2 * Math.cos(Math.PI * 5 / 6);
            var deco_y1 = self.height / 2 + radius / 2 * Math.sin(Math.PI * 5 / 6);
            var deco_x2 = self.width / 2 + radius / 4 * Math.cos(Math.PI * 5 / 6 + Math.PI * 8 / 6);
            var deco_y2 = self.height / 2 + radius / 4 * Math.sin(Math.PI * 5 / 6 + Math.PI * 8 / 6);
            var deco_x3 = self.width / 2 + radius / 2 * Math.cos(Math.PI * 5 / 6 + Math.PI * 8 / 6);
            var deco_y3 = self.height / 2 + radius / 2 * Math.sin(Math.PI * 5 / 6 + Math.PI * 8 / 6);
            this.canvas_controller.frame.push({
                shape: 'donut',
                x: self.width / 2,
                y: self.height / 2,
                outterRadius: radius / 2.2,
                innerRadius: radius / 4,
                sRadian: Math.PI * 5 / 6,
                eRadian: Math.PI * 13 / 6,
                fill: 'rgba(204,204,204,1)',
                frame: false
            });

            this.canvas_controller.frame.push({
                shape: 'text',
                baseline: 'top',
                x: 0,
                y: 0,
                width: self.width,
                height: self.height,
                fill: this.textColor,
                text: percent,
                font: radius / 4,
                frame: false
            });

            var unit = percent / 100 / 29;
            for (var i = 0; i < 30; i++) {
                var target_x = self.width / 2 + radius / 1.9 * Math.cos(Math.PI * 5 / 6 + Math.PI * 8 / 6 * unit * i);
                var target_y = self.height / 2 + radius / 1.9 * Math.sin(Math.PI * 5 / 6 + Math.PI * 8 / 6 * unit * i);
                this.canvas_controller.frame.push({
                    shape: 'donut',
                    x: self.width / 2,
                    y: self.height / 2,
                    fill: this.color,
                    outterRadius: radius / 2.2,
                    innerRadius: radius / 4,
                    sRadian: Math.PI * 5 / 6,
                    eRadian: Math.PI * 5 / 6 + Math.PI * 8 / 6 * unit * i,
                    children: [{
                        shape: 'path',
                        x: deco_x,
                        y: deco_y,
                        points: [{ type: 'line', x: deco_x1, y: deco_y1 }],
                        stroke: percent > 0 ? this.color : 'rgba(204,204,204,1)',
                        strokewidth: 4
                    }, {
                        shape: 'path',
                        x: deco_x2,
                        y: deco_y2,
                        points: [{ type: 'line', x: deco_x3, y: deco_y3 }],
                        stroke: unit * i === 1 ? this.color : 'rgba(204,204,204,1)',
                        strokewidth: 4
                    }, {
                        shape: 'circle',
                        x: self.width / 2,
                        y: self.height / 2,
                        width: radius / 22,
                        fill: 'rgba(85,85,85,1)',
                        children: [{
                            shape: 'path',
                            strokewidth: radius / 32,
                            stroke: 'rgba(85,85,85,1)',
                            x: self.width / 2,
                            y: self.height / 2,
                            points: [{ type: 'line', x: target_x, y: target_y }]
                        }]
                    }],
                    frame: true
                });
            }

            self.canvas_controller.draw();
            self.canvas_controller.animate(function() {});
        }
    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            var self = this;
            _get(NumericDonutComponent4.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent4.prototype), '_onCommitProperties', this).call(this);
            if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
                this._dataRender();
            }
            if (this._updatePropertiesMap.has('extension')) {
                this._dataRender();
            }
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            this.canvas_controller.dispose();
        }
    }, {
        key: 'color',
        get: function get() {
            return this.getGroupPropertyValue("extension", "color");
        }
    }, {
        key: 'textColor',
        get: function get() {
            return this.getGroupPropertyValue("extension", "textColor");
        }
    }]);

    return NumericDonutComponent4;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(NumericDonutComponent4, {
    "info": {
        "componentName": "NumericDonutComponent4",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(0,0,255,1)",
        "textColor": "rgba(200,200,200,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent4.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}];

WVPropertyManager.add_property_group_info(NumericDonutComponent4, {
    label: "NumericDonutComponent4 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent4, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
