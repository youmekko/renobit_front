'use strict';

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumericDonutComponent8 = function(_NumericCoreComponent) {
    _inherits(NumericDonutComponent8, _NumericCoreComponent);

    function NumericDonutComponent8() {
        _classCallCheck(this, NumericDonutComponent8);

        return _possibleConstructorReturn(this, (NumericDonutComponent8.__proto__ || Object.getPrototypeOf(NumericDonutComponent8)).call(this));
    }

    _createClass(NumericDonutComponent8, [{
        key: '_onCreateElement',
        value: function _onCreateElement() {
            this.canvas_controller = new CanvasController(this._element);
        }
    }, {
        key: 'onLoadPage',
        value: function onLoadPage() {}
    }, {
        key: '_dataRender',
        value: function _dataRender() {
            var self = this;

            // 캔버스 컨트롤러 초기화
            this.canvas_controller.frame = [];
            this.canvas_controller.resize(this.width, this.height);
            this.canvas_controller.options.refresh = true;

            // 퍼센트 정보 겟!
            var percent = _get(NumericDonutComponent8.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent8.prototype), 'getPercent', this).call(this);

            var radius = self.width > self.height ? self.width : self.height;

            self.canvas_controller.frame.push({
                shape: 'rectangle',
                x: 5,
                y: 5,
                width: self.width - 10,
                height: self.height - 10,
                fill: this.backColor,
                radius: { lt: radius / 10, lb: radius / 10, rt: radius / 10, rb: radius / 10 },
                frame: false
            });

            var unit = percent / 100 / 29;
            for (var i = 0; i < 30; i++) {
                this.canvas_controller.frame.push({
                    shape: 'rectangle',
                    x: 7,
                    y: 7,
                    width: self.width - 14,
                    height: self.height - 14,
                    fill: this.backColor,
                    radius: { lt: (radius - radius / 30) / 10, lb: (radius - radius / 30) / 10, rt: (radius - radius / 30) / 10, rb: (radius - radius / 30) / 10 },
                    children: [{
                        shape: 'arc',
                        x: self.width / 2,
                        y: self.height / 2,
                        width: radius,
                        sRadian: Math.PI * 1.5,
                        eRadian: Math.PI * 1.5 + Math.PI * 2 * unit * i,
                        fill: this.color,
                        composite: 'source-in'
                    }, {
                        shape: 'rectangle',
                        stroke: this.backColor,
                        strokewidth: 2,
                        x: radius / 8,
                        y: radius / 8,
                        width: self.width - radius / 4,
                        height: self.height - radius / 4,
                        fill: 'rgba(11,182,255,1)',
                        radius: { lt: (radius - radius / 4) / 10, lb: (radius - radius / 4) / 10, rt: (radius - radius / 4) / 10, rb: (radius - radius / 4) / 10 }
                    }, {
                        shape: 'text',
                        baseline: 'bottom',
                        x: 0,
                        y: 0,
                        width: self.width,
                        height: self.height,
                        fill: this.textColor,
                        text: percent,
                        font: radius / 4,
                        frame: false
                    }],
                    frame: true
                });
            }

            self.canvas_controller.draw();
            self.canvas_controller.animate(function() {});
        }
    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            var self = this;
            _get(NumericDonutComponent8.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent8.prototype), '_onCommitProperties', this).call(this);
            if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
                this._dataRender();
            }
            if (this._updatePropertiesMap.has('extension')) {
                this._dataRender();
            }
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            this.canvas_controller.dispose();
        }
    }, {
        key: 'color',
        get: function get() {
            return this.getGroupPropertyValue("extension", "color");
        }
    }, {
        key: 'textColor',
        get: function get() {
            return this.getGroupPropertyValue("extension", "textColor");
        }
    }, {
        key: 'backColor',
        get: function get() {
            return this.getGroupPropertyValue("extension", "backColor");
        }
    }]);

    return NumericDonutComponent8;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(NumericDonutComponent8, {
    "info": {
        "componentName": "NumericDonutComponent8",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(0,165,236,1)",
        "backColor": "rgba(255,255,255,1)",
        "textColor": "rgba(255,255,255,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent8.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "backColor",
        type: "color",
        label: "BackColor",
        show: true,
        writable: true,
        description: "background color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}];

WVPropertyManager.add_property_group_info(NumericDonutComponent8, {
    label: "NumericDonutComponent8 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent8, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
