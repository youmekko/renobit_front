class GridGaugeComponent1 extends NumericCoreComponent {

    constructor() {
        super();
    }

    _onCreateElement() {
        this.canvas_controller = new CanvasController(this._element);
        this.canvas_controller.options.refresh = false;
    }

    onLoadPage() {

    }

    _dataRender() {
        var self = this;
        this.canvas_controller.resize(this.width, this.height);
        var deco = this.canvas_controller.data_ctx.canvas.height / this.row * 2;
        var unit_width = this.canvas_controller.data_ctx.canvas.width / this.column - (self.margin * 2);
        var unit_height = (this.canvas_controller.data_ctx.canvas.height - deco) / this.row - (self.margin * 2);
        var rate = (this.column * this.row) / 100;
        var calculated_percent = super.getPercent();
        var i = this.direction === "top" ? self.row - 1 : 0;
        var j = this.direction === "left" ? self.column - 1 : 0;
        var flag = true;
        self.canvas_controller.frame = [];

        this.canvas_controller.frame.push({
            shape: 'rectangle',
            x: self.margin,
            y: self.height - deco + self.margin,
            width: self.width - (self.margin * 2),
            height: deco / 2 - self.margin,
            fill: self.gaugeColor,
            radius: { lt: self.radius, lb: self.radius, rt: self.radius, rb: self.radius },
            children: [{
                shape: 'path',
                strokewidth: 0,
                stroke: self.gridColor,
                fill: self.gridColor,
                x: 0,
                y: self.height - (deco / 2),
                points: [{ type: 'line', x: self.width / 2, y: self.height },
                    { type: 'line', x: self.width, y: self.height - (deco / 2) }
                ]
            }],
            frame: false
        })
        while (flag) {
            var position = 0;
            switch (self.direction) {
                case 'top':
                    position = (Math.abs(i - self.row + 1) * self.column) + j;
                    break;
                case 'bottom':
                    position = (i * self.column) + j;
                    break;
                case 'left':
                    position = (Math.abs(j - self.column + 1) * self.row) + i;
                    break;
                case 'right':
                    position = (j * self.row) + i;
                    break;
            }
            position = position / rate;

            this.canvas_controller.frame.push({
                shape: 'rectangle',
                x: unit_width * j + (self.margin * j * 2) + self.margin,
                y: unit_height * i + (self.margin * i * 2) + self.margin,
                width: unit_width,
                height: unit_height,
                fill: self.gridColor,
                radius: { lt: self.radius, lb: self.radius, rt: self.radius, rb: self.radius },
                frame: false
            })

            if (position < calculated_percent) {
                var percent = 1;
                if (Math.abs(position - calculated_percent) < (1 / rate) && Math.abs(position - calculated_percent) > 0) {
                    percent = Math.abs(position - calculated_percent) / (1 / rate);
                }
                self.canvas_controller.frame.push({
                    shape: 'rectangle',
                    x: (self.direction === "left" ? unit_width * j + unit_width * (1 - percent) : unit_width * j) + (self.margin * j * 2) + self.margin,
                    y: (self.direction === "top" ? unit_height * i + unit_height * (1 - percent) : unit_height * i) + (self.margin * i * 2) + self.margin,
                    width: (self.direction === "left" || self.direction === "right" ? unit_width * percent : unit_width),
                    height: (self.direction === "top" || self.direction === "bottom" ? unit_height * percent : unit_height),
                    fill: self.gaugeColor,
                    radius: { lt: self.radius, lb: self.radius, rt: self.radius, rb: self.radius },
                    frame: true
                })
            }

            switch (self.direction) {
                case 'top':

                    j++;
                    if (j >= self.column) {
                        j = 0;
                        i--;
                    }
                    break;
                case 'bottom':

                    j++;
                    if (j >= self.column) {
                        j = 0;
                        i++;
                    }
                    break;
                case 'left':

                    i++;
                    if (i >= self.row) {
                        i = 0;
                        j--;
                    }
                    break;
                case 'right':

                    i++;
                    if (i >= self.row) {
                        i = 0;
                        j++;
                    }
                    break;
            }

            if (!(i >= 0 && i < self.row && j >= 0 && j < self.column)) {
                flag = false;
            }
        }

        self.canvas_controller.draw();
        self.canvas_controller.animate(function(d) {
            // animation 완료 후 발생할 함수 처리.
        });
    }

    _onCommitProperties() {
        super._onCommitProperties();
        if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
            this._dataRender();
        }
        if (this._updatePropertiesMap.has('extension')) {
            this._dataRender();
        }
    }

    _onDestroy() {
        this.canvas_controller.dispose();
    }

    get row() {
        return this.getGroupPropertyValue("extension", "row");
    }

    get column() {
        return this.getGroupPropertyValue("extension", "column");
    }

    get gaugeColor() {
        return this.getGroupPropertyValue("extension", "gaugeColor");
    }

    get gridColor() {
        return this.getGroupPropertyValue("extension", "gridColor");
    }

    get margin() {
        return this.getGroupPropertyValue("extension", "margin");
    }

    get radius() {
        return this.getGroupPropertyValue("extension", "radius");
    }

    get direction() {
        return this.getGroupPropertyValue("extension", "direction");
    }
}

WVPropertyManager.attach_default_component_infos(GridGaugeComponent1, {
    "info": {
        "componentName": "GridGaugeComponent1",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "row": 20,
        "column": 5,
        "gaugeColor": "rgba(221,77,136,1)",
        "gridColor": "rgba(221,77,136,0.5)",
        "direction": "top",
        "margin": 1,
        "radius": 0
    },
    "label": {
        "label_using": "N",
        "label_text": "Grid Gauge Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

GridGaugeComponent1.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "background"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "guage value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "guage min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "guage max"
    }]
}, {
    label: "Grid Guage",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "margin",
        type: "number",
        label: "Margin",
        show: true,
        writable: true,
        description: "grid margin"
    }, {
        owner: "extension",
        name: "radius",
        type: "number",
        label: "Radius",
        show: true,
        writable: true,
        description: "grid radius"
    }]
}]


WVPropertyManager.add_property_group_info(GridGaugeComponent1, {
    label: "GridGaugeComponent1 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(GridGaugeComponent1, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
