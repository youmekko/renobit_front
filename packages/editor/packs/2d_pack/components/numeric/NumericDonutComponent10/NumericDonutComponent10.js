class NumericDonutComponent10 extends NumericCoreComponent {

    constructor() {
        super();
    }

    _onCreateElement() {
        this.svg_controller = new SvgController(this._element);
        this.svg_controller.svg.attr("viewBox", "0 0 204 88")
    }

    onLoadPage() {

    }

    _dataRender() {
        var self = this;
        self.svg_controller.svg.empty();
        var percent = super.getPercent();
        var empty_color = '#4e557e';
        var full_color = this.color;
        var svg_tag = '<g id="Layer0_0_FILL">    <path fill="{0}" transform="matrix( 1, 0, 0, 0.2138671875, -158.05,-13.7) " stroke="none" d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_1_FILL">    <path fill="{1}" transform="matrix( 0.9682464599609375, 0.0527801513671875, -0.2467803955078125, 0.20709228515625, -108.75,-26.25) " stroke="none" d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_2_FILL">    <path fill="{2}" transform="matrix( 0.8750457763671875, 0.1029205322265625, -0.4811859130859375, 0.1871490478515625, -45.7,-36) " stroke="none" d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_3_FILL">    <path fill="{3}" stroke="none" transform="matrix( 0.7285308837890625, 0.14599609375, -0.6826324462890625, 0.1558074951171875, 25.8,-41.95) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_4_FILL">    <path fill="{4}" stroke="none" transform="matrix( 0.5341644287109375, 0.180419921875, -0.8436279296875, 0.1142425537109375, 103,-44) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_5_FILL">    <path fill="{5}" stroke="none" transform="matrix( 0.30902099609375, 0.20318603515625, -0.950042724609375, 0.0660858154296875, 179.2,-41.9) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_6_FILL">    <path fill="{6}" stroke="none" transform="matrix( 0.0614776611328125, 0.2134246826171875, -0.997894287109375, 0.013153076171875, 251.45,-35.85) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_7_FILL">    <path fill="{7}" stroke="none" transform="matrix( -0.186737060546875, 0.2099761962890625, -0.9817962646484375, -0.0399322509765625, 313.2,-26.05) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_8_FILL">    <path fill="{8}" stroke="none" transform="matrix( -0.4234161376953125, 0.193450927734375, -0.904541015625, -0.090545654296875, 361.9,-13.4) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_9_FILL">    <path fill="{9}" stroke="none" transform="matrix( -0.63641357421875, 0.164520263671875, -0.769256591796875, -0.1361083984375, 394.7,1.6) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_10_FILL">    <path fill="{10}" stroke="none" transform="matrix( -0.8070831298828125, 0.1257171630859375, -0.587799072265625, -0.172607421875, 408.95,17.75) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_11_FILL">    <path fill="{11}" stroke="none" transform="matrix( -0.929046630859375, 0.078460693359375, -0.3668975830078125, -0.1986846923828125, 403.95,34.4) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_12_FILL">    <path fill="{12}" stroke="none" transform="matrix( -0.992034912109375, 0.0262603759765625, -0.12274169921875, -0.2121734619140625, 379.7,50.2) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_13_FILL">    <path fill="{13}" stroke="none" transform="matrix( -0.992034912109375, -0.0262603759765625, 0.12274169921875, -0.2121734619140625, 338.95,63.85) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_14_FILL">    <path fill="{14}" stroke="none" transform="matrix( -0.929046630859375, -0.078460693359375, 0.3668975830078125, -0.1986846923828125, 282.05,75.15) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_15_FILL">    <path fill="{15}" stroke="none" transform="matrix( -0.8070831298828125, -0.1257171630859375, 0.587799072265625, -0.172607421875, 213.7,83.1) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_16_FILL">    <path fill="{16}" stroke="none" transform="matrix( -0.63641357421875, -0.164520263671875, 0.769256591796875, -0.1361083984375, 139.2,87.1) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_17_FILL">    <path fill="{17}" stroke="none" transform="matrix( -0.4234161376953125, -0.193450927734375, 0.904541015625, -0.090545654296875, 61.4,87.1) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_18_FILL">    <path fill="{18}" stroke="none" transform="matrix( -0.186737060546875, -0.2099761962890625, 0.9817962646484375, -0.0399322509765625, -12.9,82.9) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_19_FILL">    <path fill="{19}" stroke="none" transform="matrix( 0.0614776611328125, -0.2134246826171875, 0.997894287109375, 0.013153076171875, -80,75) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_20_FILL">    <path fill="{20}" stroke="none" transform="matrix( 0.30902099609375, -0.20318603515625, 0.950042724609375, 0.0660858154296875, -136.3,63.6) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_21_FILL">    <path fill="{21}" stroke="none" transform="matrix( 0.5341644287109375, -0.180419921875, 0.8436279296875, 0.1142425537109375, -177.15,49.65) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_22_FILL">    <path fill="{22}" stroke="none" transform="matrix( 0.7285308837890625, -0.14599609375, 0.6826324462890625, 0.1558074951171875, -200.85,33.85) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_23_FILL">    <path fill="{23}" stroke="none" transform="matrix( 0.8750457763671875, -0.1029205322265625, 0.4811859130859375, 0.1871490478515625, -205.5,17.45) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <g id="Layer0_24_FILL">    <path fill="{24}" stroke="none" transform="matrix( 0.9682464599609375, -0.0527801513671875, 0.2467803955078125, 0.20709228515625, -190.8,1.1) " d=" M 358.45 183.9 Q 360 175.25 360 166.45 L 339.8 166.45 Q 339.8 173.5 338.6 180.4 L 358.45 183.9 Z"/> </g> <text transform="translate(102 12)" style="fill:' + this.textColor + ';font-size:44px;font-weight:bold;">    <tspan x="0" text-anchor="middle">' + percent + '</tspan> </text>';
        for (var i = 0; i < 25; i++) {
            if (Math.round(percent / 4) > i) {
                svg_tag = svg_tag.replace('{' + i + '}', full_color)
            } else {
                svg_tag = svg_tag.replace('{' + i + '}', empty_color)
            }
        }
        self.svg_controller.append(svg_tag).attr('transform', "translate(0 44)")
    }

    _onCommitProperties() {
        var self = this;
        super._onCommitProperties();
        if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
            this._dataRender();
        }
        if (this._updatePropertiesMap.has('extension')) {
            this._dataRender();
        }
    }

    _onDestroy() {

    }

    get color() {
        return this.getGroupPropertyValue("extension", "color");
    }

    get textColor() {
        return this.getGroupPropertyValue("extension", "textColor");
    }
}

WVPropertyManager.attach_default_component_infos(NumericDonutComponent10, {
    "info": {
        "componentName": "NumericDonutComponent10",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(0,255,255,1)",
        "textColor": "rgba(0,294,204,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent10.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}]


WVPropertyManager.add_property_group_info(NumericDonutComponent10, {
    label: "NumericDonutComponent10 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent10, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
