"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumericCoreComponent = function (_WVDOMComponent) {
    _inherits(NumericCoreComponent, _WVDOMComponent);

    function NumericCoreComponent() {
        var _this, _ret;

        _classCallCheck(this, NumericCoreComponent);

        return _ret = (_this = _possibleConstructorReturn(this, (NumericCoreComponent.__proto__ || _Object$getPrototypeOf(NumericCoreComponent)).call(this)), _this), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(NumericCoreComponent, [{
        key: "_onCreateProperties",
        value: function _onCreateProperties() {
            this._invalidateData = false;
            this._invalidateValue = false;
        }
    }, {
        key: "getPercent",
        value: function getPercent() {
            var percentage = (this.value - this.min) / (this.max - this.min) *10000;
            return Math.round(percentage)/100;
            //return !this.floatCheck(percentage) ? percentage.toFixed(2) : percentage;
        }
    }, {
        key: "floatCheck",
        value: function floatCheck(num) {
            var num_check = /^([0-9]*)[\.]?([0-9])?$/;
            if (!num_check.test(num)) {
                return false;
            }
            return true;
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this._dataRender();
        }
    }, {
        key: "_dataRender",
        value: function _dataRender() {}
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._invalidateData) {
                this.validateCallLater(this._validateValueProperty);
                this.validateCallLater(this._dataRender);
                this._invalidateData = false;
            }

            if (this._invalidateValue) {
                this.validateCallLater(this._dataRender);
                this._invalidateValue = false;
            }
        }
    }, {
        key: "_validateValueProperty",
        value: function _validateValueProperty() {
            this.value = Math.min(this.max, Math.max(this.min, this.value));
        }
    }, {
        key: "min",
        set: function set(value) {
            var _value = Math.min(this.max, value);
            this._invalidateData = this._checkUpdateGroupPropertyValue("setter", "min", parseInt(_value));
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "min");
        }
    }, {
        key: "max",
        set: function set(value) {
            var _value = Math.max(this.min, value);
            this._invalidateData = this._checkUpdateGroupPropertyValue("setter", "max", parseInt(_value));
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "max");
        }
    }, {
        key: "value",
        set: function set(value) {
            var _value = Math.min(this.max, Math.max(this.min, value));
            this._invalidateValue = this._checkUpdateGroupPropertyValue("setter", "value", parseInt(_value));
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "value");
        }
    }]);

    return NumericCoreComponent;
}(WVDOMComponent);
