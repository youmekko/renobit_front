class NumericCoreComponent extends WVDOMComponent {

    constructor() {
        return super();
    }

    _onCreateProperties(){
        this._invalidateData = false;
        this._invalidateValue = false;
    }

    getPercent(){
        var percentage = (this.value - this.min) / (this.max - this.min) *10000;
        return Math.round(percentage)/100;
        //return !this.floatCheck(percentage) ? percentage.toFixed(2) : percentage;
    }

    floatCheck(num){
        var num_check=/^([0-9]*)[\.]?([0-9])?$/;
        if(!num_check.test(num)){
           return false;
       }
       return true;
    }

    _onImmediateUpdateDisplay() {
        this._dataRender();
    }

    _dataRender() {

    }

    _onCommitProperties() {
        if(this._invalidateData) {
            this.validateCallLater(this._validateValueProperty);
            this.validateCallLater(this._dataRender);
            this._invalidateData = false;
        }

        if(this._invalidateValue) {
            this.validateCallLater(this._dataRender);
            this._invalidateValue = false;
        }
    }

    _validateValueProperty(){
        this.value = Math.min(this.max, Math.max(this.min, this.value));
    }

    set min(value) {
        let _value = Math.min(this.max, value);
        this._invalidateData = this._checkUpdateGroupPropertyValue("setter", "min", parseInt(_value));
    }

    get min() {
        return this.getGroupPropertyValue("setter", "min");
    }

    set max(value) {
        let _value = Math.max(this.min, value);
        this._invalidateData = this._checkUpdateGroupPropertyValue("setter", "max", parseInt(_value));
    }

    get max() {
        return this.getGroupPropertyValue("setter", "max");
    }

    set value(value) {
        let _value = Math.min(this.max, Math.max(this.min, value));
        this._invalidateValue = this._checkUpdateGroupPropertyValue("setter", "value", parseInt(_value));
    }

    get value() {
        return this.getGroupPropertyValue("setter", "value");
    }
}
