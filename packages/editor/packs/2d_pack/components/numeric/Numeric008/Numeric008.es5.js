'use strict';

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Numeric008 = function(_NumericCoreComponent) {
    _inherits(Numeric008, _NumericCoreComponent);

    function Numeric008() {
        _classCallCheck(this, Numeric008);

        var _this = _possibleConstructorReturn(this, (Numeric008.__proto__ || Object.getPrototypeOf(Numeric008)).call(this));

        _this.containier = null;
        _this.$bar = null;
        _this.$barlabel = null;
        _this._invalidatePropertyScolor = false;
        return _this;
    }

    _createClass(Numeric008, [{
        key: '_onCreateElement',
        value: function _onCreateElement() {
            var $el = $(this._element);

            $el.append('<div class="circle-face-num">' + '<div class="faceContainer">' + '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALkAAAC5CAIAAAD7zwkLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QjMzRUVBODI2QUVDMTFFOEIyNDRCMkQzNjEyMEUwNTgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QjMzRUVBODM2QUVDMTFFOEIyNDRCMkQzNjEyMEUwNTgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBNkVBNEZGRjZBRUMxMUU4QjI0NEIyRDM2MTIwRTA1OCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBNkVBNTAwMDZBRUMxMUU4QjI0NEIyRDM2MTIwRTA1OCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PtEnQYEAAAqkSURBVHja7J2tcyJLFMVhKsTEELMGTEzWYIiJ2Zj84TGLwQSzhpiYYIgIBkNE3im6iuJBILe7b3/OOSL13u5kp2f61+fe/pju7mq16uSnr6+vbrfboXJSk2exCApZocgKRVYoiqxQZIUiK63S19cXWaFEynPIgKwoNPc8bYCs5Njc09pANFLJCgMWWWFWq33fRvj7LQnJrc1qJfdthL/PyTyKMYgiK5R2JtQkuStVYibUJLlrTT0IxiD2IKiq85Vw1kLTqo2VcNZSn2k50M9+UEuzLkO/1e2ail9Hfdao/uqs/LLJ7XVQ2b46xiCKrFBkhakVWWF+QFaoWvyPrND/pLQ11bcGSou2pujWQDFfoeWQFVoOWaHaYKsNnZ+2Kqz3hs5fhzH43FFY74xBxRuDoSTCHckKuWwlK8y3yAp72mSFKspEyUqpEMQ3UbKSXevPNpKSleK7smSFiXZ2uvD55c1m8/r6ulgsdn/S7/eHw+GvX7/ISnwtl0tUx3q9Nv/b6/VQETc3N5eXlzr0u50fBIN9f3//+/fv5+fn8d+ifOPxWKuIlKTRzmYzgHL8VyDm4eFBpfU6xiCA8vT09C0oEAr98vLCKowmvO1vQYFQR6gpWE4aVuBycJTz1/z79+/t7Y21GEHz+Rxv+/w1qC94j2evzYWV6XR6ylEOLpOUjx1pH6HdIvr8eBnqS3LZ+Xy8cSic0NBQvv20l3LoIv1I1Y+OspO/zTe2Jba6JcNQUKpQL/JEBE334+PDtrr3/7uxLbEk+rhdTDmQtOshK1bHqX2NORZHBe4zp03oqDJYsRrV2b+Yi0tCyKo6+v1+VFas7udZOOpHDQYD4ZVXV1eeI+nWrOB+Nzc3wsINh0OVOMX4dUqoi16vJ7zSNwY5VMNoNJKUbzwea3URg8avokFE0727u5O029vbW19WHKoBN358fMTPM6Hnz58/tqaS5xhGILAUAYVh3N/fn2m9qClUh/9UruM8c2c7tzmdTvdHZlFc8AF+r6+vGR0i6+PjYzKZHA+3IKEBSSpz/u6s7IpocAG8KBbXISQUWi/q4mD9imK79WWFao84bkuRFYqsUGSFajcrusNcHL2tmRXd8VbOPjIGtddyyip2JawUajllFZu5LR2IrNSlnQMlhIasFOYKCcNWE/lR2+n5Oecl8idt4jxqcbS1p38uf9KGr55ivkLFZUUxdph/iuP09BVpGGIwqpaVVFVL+8mwW9TkWfG0n1Qv/8ybj8EKK76Ozn/DRkaRlZQOVyWUFyr/itnodr1ex/+CpNfr1frpGoDzhNt83XfmC1G7luZfu29vb8LdBqn4Ai6j0ej379/+dPrGoOVyOZlMCEq2MjtQntr91ipG+7Ii3weRuXBCqVSTFyug1WFD5oIqoI7ePl74equUrLjt3M3hliTE+2+z7sWKP6pUNCVmhSIrFv0x1kEpSpyvcP+mguQ/ItdSX2nhxFBiVsr1lRb2xfz3Gm7SokpFk3/DvvBkBWFIMsA/Go1OxQLPJr7ZbOJPWCa5afIYdOFPq6QzNhgMmAgH0nQ6lUz3+B+P2Vilfsd/LoyCKmczUt9KcoKUysEIjVXqd/znQrdQcWyuanOOhnJTP2MTvuO2MX2FE0nHEp4oKfeVMzbhywqAlYyyHM9zcicmFQkbocrZCArzQcKk6eCcTO7EpCLJ6aNozCqjG/FYYXobAhTJgIV/D0iNFaG/8azmVMmK1uE8CqxcbSW5Urjqs8SUIslNhcdfZ+QrzilLTSlF/JsKAxB6QFpTMTqsCE9ohGduNhvGDhUJTVrxdDgdVlAg4fqEOGGo+h71er0WJiv+R2IqsyLnt25WokWi+XwuuUwxAGmyIuR3tVqV0nnOdrTQfBEcOQBpsoL0NucwVNMeMHiBwg89FQNQR3cdv7Bk5iv56vsp4RB/eXkRdjh0F6NpsiL8wLpT+5etQRGXtzRdU1FmBRQLB1qSWEsFQqYibGaoC/Wz1JW/JZMfRE9rcRCiTypT0WdFvjMMrcXBVIRd5TJY6Zxehn2s5+dnEiAXnFje/QnxiYU+K0i/hZ3nxWJhO0PU2mWUy+VS2P2xaq6JWbm8vJR3iGazmdUMUQtPHLH14ECm0gm0TwIyXKG1IGWJnOSWONCCVyRf3B7IVEKxAmu5u7uT5/aSrxZaK7wceXMKZyqdcPuvWBV6MplwrcKpvs90OhVebLacDFeYgHv1jMdj4ZWIREhcSIZn9EGaGPQL84CsDIfDH4dxd5nm61YZ1lbCXBidRHnfB6YiHwjNjhWJtexnmkj1M0xcUuXCeBXy6AMhQUSaWDAr19fX8gj6+fnJxGU/TcELEboa/DvEQG1UVkz/WR5EkbgAl8qGRhyEl2DSFKGr3d/fRyhVcFZgjFZPslwu0aS+dZeKv2feNQPjKFZLBxHoz7RGxQYWY89SOKRV2oUk1ypUV6BdMxBuprJTv98/P0pe3llTyFqstgBZLBZtw8WAYvV1Jvo+Dw8P0YoXiRUTiaz2rdy5S0vSFFtHMS0w5pZ98fbNRp/oYOD/Rwjw7ubzeRu2XcFj2oIyGAzkc7SFsdLZDvzvd+0kEMxms+qDER7QdtgaAT1O30eZFasYgaTddu8yNLinp6cqx13wUHg0W0dBKAcooUfezrHinBZYxQg8IdIx2w230YesDxc8zmQycfiyDqAk2dSziTZ6sWMR6djj46Ptr69WK+BSzeoFPAgexwEUGLPbAn3/LkI31Z6+boMoMCQkyBHGs0M/+/Pzs8MhkXjw+GmKOyv+O10fRBaHX0QXIEnAVok7toMo+44SueOTi6/4uIsJZMBFa8eiODLTF25fuqR1lCxY6fiN6OMNorXlbzCwE/nBt3mCkgUrHachy4MMBlEpW2KcsxMjeKdDV6BaVjrbxYI+C/oPcl6HpEoxD9sPOqDE5w1nlZl18znbxH96GUnMaDTKoZeEZwH6nh/hZhJ6cmSlo7QaAR6Dt6y+UFliPIDDLJL1/1Q7ea8nd1Y62wXJZvmg/z9lVhbGSWVQbICutd/zaKvcEq9uhudrmWXJigXr9/uABujoDo0bF0FSorgluJnrUd86JV9WVE6mcx6z+tFsILPXt+3wDCAGH3hj+A/8VN8TxMweC4EOkYyX5ytanSNhO5bUzfGZNurKf6yom/kZj56DE0WolEmubv7ngaJB2y5tL0iIg4g7RZxenB0rp8LwfD6Xb2yU+bPs7ASdndw6xmX7yr7BIB7VcQ5R5nbyLeVerMRPxU1nZDablRuSwIfzeiX6imPO6z+IHj+HRcS5vb0tceVNwayU1UvKfzK8flaMEI/gMXlGpXymM8nK/4gxkzK2NhMo6zJTUcd5SZIkj6x8o81mg44SuNHaJcq2atHBMYgUMWTSalb2oTGzevgZOgVGOgJEzJ5n9SFSPyv7Aisgxkz4aaU1Zuqx3++rT1+TlUghQCJAg5xm/6fkt4AFejHmZ1nfD1TLSlapX7l5aIhHa7KDN6e6KQsUq69QHR6t6VDhK6YOsslKsoopbr8qssIAR1aocKxUsIVfa08ti81KBZ3DNuxCyBhEWbtmfBMlK8WEywPXjGmiprRNm98+w6VVaethhckKYxBVCyvsppIVOj/FGESRFYqsUGQlgJhoB9V/AgwAeyD+RpxL8WsAAAAASUVORK5CYII=\n">' + '</div>' + '<div class="txtContainer">' + '<div class="txt">' + '65' + '</div>' + '</div>' + '</div>');

            this.container = $el.find(".circle-face-num");
            this.$faceContainer = this.container.find(".circle-face-num .faceContainer");
            this.$txtContainer = this.container.find(".txtContainer");
            this.$txt = this.$txtContainer.find(".txt");
        }

        ///화면에 붙였을때

    }, {
        key: '_onImmediateUpdateDisplay',
        value: function _onImmediateUpdateDisplay() {
            this.setColors();
            this.changeFace();
            this.$txt.html(this.value);
        }
    }, {
        key: '_dataRender',
        value: function _dataRender() {
            this.setColors();
            this.changeFace();
            this.$txt.html(this.value);
        }
    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            _get(Numeric008.prototype.__proto__ || Object.getPrototypeOf(Numeric008.prototype), '_onCommitProperties', this).call(this); // for Numeric min-value-max

            if (this._invalidatePropertyScolor) {
                this.validateCallLater(this._validateSColorProp);
                this._invalidatePropertyScolor = false;
            }

            if (this._invalidatePropertyTcolor) {
                this.validateCallLater(this._validateTColorProp);
                this._invalidatePropertyTcolor = false;
            }

            if (this._invalidatePropertyTitleLabel) {
                this.validateCallLater(this._validateTitleLabelProp);
                this._invalidatePropertyTitleLabel = false;
            }

            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this._validateFontStyleProperty);
            }
        }
    }, {
        key: 'changeFace',
        value: function changeFace() {}
    }, {
        key: 'setColors',
        value: function setColors() {
            var barBackground = this.s_color;
            this.container.css("background", this.s_color);
        }
    }, {
        key: '_validateSColorProp',
        value: function _validateSColorProp() {
            this.setColors();
        }
    }, {
        key: '_validateFontStyleProperty',
        value: function _validateFontStyleProperty() {
            var fontProps = this.font;
            this.$txt.css({
                "fontFamily": fontProps.font_type,
                "fontSize": fontProps.font_size,
                "fontWeight": fontProps.font_weight,
                "color": fontProps.font_color,
                "textAlign": fontProps.text_align,
                "lineHeight": fontProps.line_height + "px"
            });

            //this.updateTxtElHeight();
        }
    }, {
        key: '_validateTColorProp',
        value: function _validateTColorProp() {
            this.setColors();
        }
    }, {
        key: '_validateTitleLabelProp',
        value: function _validateTitleLabelProp() {
            this.$barlabel.html(this.titleLabel);
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            this.containier = null;
            this.$bar = null;
            this.$barlabel = null;
            _get(Numeric008.prototype.__proto__ || Object.getPrototypeOf(Numeric008.prototype), '_onDestroy', this).call(this);
        }
    }, {
        key: 's_color',
        get: function get() {
            return this.getGroupPropertyValue("setter", "s_color");
        },
        set: function set(color) {
            if (this._checkUpdateGroupPropertyValue("setter", "s_color", color)) {
                this._invalidatePropertyScolor = true;
            }
        }
    }, {
        key: 't_color',
        get: function get() {
            return this.getGroupPropertyValue("setter", "t_color");
        },
        set: function set(color) {
            if (this._checkUpdateGroupPropertyValue("setter", "t_color", color)) {
                this._invalidatePropertyTcolor = true;
            }
        }
    }, {
        key: 'titleLabel',
        get: function get() {
            return this.getGroupPropertyValue("setter", "titleLabel");
        },
        set: function set(txt) {
            if (this._checkUpdateGroupPropertyValue("setter", "titleLabel", txt)) {
                this._invalidatePropertyTitleLabel = true;
            }
        }
    }, {
        key: 'font',
        get: function get() {
            return this.getGroupProperties("font");
        }
    }]);

    return Numeric008;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(Numeric008, {
    "info": {
        "componentName": "Numeric008",
        "version": "1.0.0"
    },

    "setter": {
        "width": 150, // 기본 크기
        "height": 50, // 기본 크기
        "s_color": "#3399FF",
        "t_color": "#FFF",
        "value": 10,
        "min": 0,
        "max": 100
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#FFF",
        "font_size": 24,
        "font_weight": "bold",
        "line_height": 14,
        "text_align": "center"
    },

    "style": {
        "border": "0px solid #000000",
        "borderRadius": 1,
        "cursor": "default"

    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
Numeric008.property_panel_info = [{
    template: "primary",
    label: "primary"
}, {
    template: "pos-size-2d",
    label: "pos-size-2d"
}, {
    template: "cursor",
    label: "pos-size-2d"
}, {
    template: "label",
    label: "label"
}, {
    template: "border",
    label: "border"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}];

WVPropertyManager.add_property_panel_group_info(Numeric008, {
    label: "Style",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "s_color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "Primary Color"
    }]
});

WVPropertyManager.add_property_group_info(Numeric008, {
    label: "Numeric008 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(Numeric008, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
