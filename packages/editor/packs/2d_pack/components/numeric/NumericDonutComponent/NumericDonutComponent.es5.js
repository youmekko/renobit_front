"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumericDonutComponent = function(_NumericCoreComponent) {
    _inherits(NumericDonutComponent, _NumericCoreComponent);

    function NumericDonutComponent() {
        _classCallCheck(this, NumericDonutComponent);

        return _possibleConstructorReturn(this, (NumericDonutComponent.__proto__ || Object.getPrototypeOf(NumericDonutComponent)).call(this));
    }

    _createClass(NumericDonutComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.canvas_controller = new CanvasController(this._element);
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_dataRender",
        value: function _dataRender() {
            var self = this;

            // 캔버스 컨트롤러 초기화
            this.canvas_controller.frame = [];
            this.canvas_controller.resize(this.width, this.height);
            this.canvas_controller.options.refresh = true;

            // 퍼센트 정보 겟!
            var percent = _get(NumericDonutComponent.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent.prototype), "getPercent", this).call(this);

            var radius = (self.width < self.height ? self.width : self.height) - 5;

            var rgb = this.color.substring(this.color.lastIndexOf("(") + 1, this.color.lastIndexOf(")")).split(",");
            var color_tempate = "rgba(" + rgb[0] + "," + rgb[1] + "," + rgb[2];
            var grd = this.canvas_controller.createGradient({
                type: 'radial',
                x0: self.width / 2,
                y0: self.height / 2,
                r0: radius / 5.5,
                x1: self.width / 2,
                y1: self.height / 2,
                r1: radius / 2,
                colors: ['rgba(0,0,0,0)', color_tempate + ", 0.2)", color_tempate + ", 0.5)", color_tempate + ", 1)"]
            });

            this.canvas_controller.frame.push({
                shape: 'text',
                baseline: 'bottom',
                x: 0,
                y: 0,
                width: self.width,
                height: self.height,
                fill: this.textColor,
                text: percent,
                font: radius / 4,
                frame: false
            });

            // 기본 도넛 모양

            this.canvas_controller.frame.push({
                shape: 'donut',
                x: self.width / 2,
                y: self.height / 2,
                outterRadius: radius / 2,
                innerRadius: radius / 2.2,
                sRadian: 0,
                eRadian: Math.PI * 2,
                fill: 'rgba(77,79,92,0.5)',
                frame: false
            });

            // 도넛 데이터 렌더 및 시계 바늘

            var unit = percent / 100 / 29;
            for (var i = 0; i < 30; i++) {
                var target_x = self.width / 2 + radius / 2 * Math.cos(Math.PI * 1.5 + Math.PI * 2 * unit * i);
                var target_y = self.height / 2 + radius / 2 * Math.sin(Math.PI * 1.5 + Math.PI * 2 * unit * i);
                this.canvas_controller.frame.push({
                    shape: 'arc',
                    x: self.width / 2,
                    y: self.height / 2,
                    width: radius / 2,
                    sRadian: Math.PI * 1.5,
                    eRadian: Math.PI * 1.5 + Math.PI * 2 * unit * i,
                    fill: grd,
                    frame: true
                });
            }

            self.canvas_controller.draw();
            self.canvas_controller.animate(function() {});
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            var self = this;
            _get(NumericDonutComponent.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent.prototype), "_onCommitProperties", this).call(this);
            if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
                this._dataRender();
            }
            if (this._updatePropertiesMap.has('extension')) {
                this._dataRender();
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.canvas_controller.dispose();
        }
    }, {
        key: "color",
        get: function get() {
            return this.getGroupPropertyValue("extension", "color");
        }
    }, {
        key: "textColor",
        get: function get() {
            return this.getGroupPropertyValue("extension", "textColor");
        }
    }]);

    return NumericDonutComponent;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(NumericDonutComponent, {
    "info": {
        "componentName": "NumericDonutComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(34,150,182,1)",
        "textColor": "rgba(200,200,200,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "text color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}];

WVPropertyManager.add_property_group_info(NumericDonutComponent, {
    label: "NumericDonutComponent 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
