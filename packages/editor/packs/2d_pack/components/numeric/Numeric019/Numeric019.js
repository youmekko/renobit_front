class Numeric019 extends NumericCoreComponent {
    constructor() {
        super();
        this.containier = null;
        this.$bar = null;
        this.$barlabel = null;
        this._invalidatePropertyScolor = false;
    }

    _onCreateElement() {
        let $el = $(this._element);
        $el.append('<div class="horizon-bar-pin"><div class="barContainer"><div class="bar1"></div><div class="bar2"></div><div class="bar3"></div><div class="bar4"></div></div><div class="label"><div class="labelArrow"></div></div></div>');

        this.container = $el.find(".horizon-bar-pin");
        this.$barlabel = this.container.find(".label");
        this.$barContainer = this.container.find(".barContainer");
        this.$bar = this.$barContainer.find(".bar");
        this.$bar1 = this.$barContainer.find(".bar1");
        this.$bar2 = this.$barContainer.find(".bar2");
        this.$bar3 = this.$barContainer.find(".bar3");
        this.$bar4 = this.$barContainer.find(".bar4");
        this.$labelArrow = this.container.find(".labelArrow");
        this.$bar.css({ "width": "0%" });
    }

    onLoadPage() {
        this.setColors();
        $(this.$bar).stop().animate({ "width": Math.round(this.getPercent()) + "%" }, 1000);

    }

    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {
        this.setColors();
        $(this.$barlabel).stop().animate({ "margin-left": Math.round(this.getPercent()) + "%" }, 1000);
    }

    _dataRender() {
        $(this.$barlabel).stop().animate({ "margin-left": Math.round(this.getPercent()) + "%" }, 1000);
    }

    _onCommitProperties() {
        super._onCommitProperties(); // for Numeric min-value-max

        if (this._invalidatePropertyScolor) {
            this.validateCallLater(this._validateSColorProp)
            this._invalidatePropertyScolor = false;
        }

        if (this._invalidatePropertyTcolor) {
            this.validateCallLater(this._validateTColorProp)
            this._invalidatePropertyTcolor = false;
        }

        if (this._invalidatePropertyTitleLabel) {
            this.validateCallLater(this._validateTitleLabelProp)
            this._invalidatePropertyTitleLabel = false;
        }
    }

    setColors() {
        //let barBackground = 'repeating-linear-gradient(-55deg, ' + this.s_color + ',  '+ this.s_color + ' 10px,  ' + this.t_color + ' 10px,  '+ this.t_color + ' 20px)'
        let barBackground = this.s_color;
        this.$bar.css("background", barBackground);
        this.$bar1.css("background", barBackground);
        this.$bar2.css("background", barBackground);
        this.$bar3.css("background", barBackground);
        this.$bar4.css("background", barBackground);
        //this.$barContainer.css("border-color", this.t_color);
        this.$barlabel.css("color", this.t_color);
        this.$labelArrow.css("borderTop", " 30px solid " + this.t_color);
    }

    _validateSColorProp() {
        this.setColors();
    }

    _validateTColorProp() {
        this.setColors();
    }

    _validateTitleLabelProp() {
        this.$barlabel.html(this.titleLabel);
    }

    _onDestroy() {
        this.containier = null;
        this.$bar = null;
        this.$barlabel = null;
        super._onDestroy();
    }

    get s_color() {
        return this.getGroupPropertyValue("setter", "s_color");

    }

    set s_color(color) {
        if (this._checkUpdateGroupPropertyValue("setter", "s_color", color)) {
            this._invalidatePropertyScolor = true;
        }
    }


    get t_color() {
        return this.getGroupPropertyValue("setter", "t_color");

    }

    set t_color(color) {
        if (this._checkUpdateGroupPropertyValue("setter", "t_color", color)) {
            this._invalidatePropertyTcolor = true;
        }
    }


    get titleLabel() {
        return this.getGroupPropertyValue("setter", "titleLabel");

    }

    set titleLabel(txt) {
        if (this._checkUpdateGroupPropertyValue("setter", "titleLabel", txt)) {
            this._invalidatePropertyTitleLabel = true;
        }
    }
}

WVPropertyManager.attach_default_component_infos(Numeric019, {
    "info": {
        "componentName": "Numeric019",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300, // 기본 크기
        "height": 30, // 기본 크기
        "s_color": "#0a6aa1",
        "t_color": "#FFF",
        "value": 10,
        "min": 0,
        "max": 100
    },
    "style": {
        "border": "0px solid #000000",

        "borderRadius": 1,
        "cursor": "default"

    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
Numeric019.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    }, {
        template: "cursor",
        label: "pos-size-2d"
    },
    {
        template: "label",
        label: "label"
    }, {
        template: "border",
        label: "border"
    }, {
        label: "Numeric",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "value",
            type: "number",
            label: "Value",
            show: true,
            writable: true,
            description: "value"
        }, {
            owner: "setter",
            name: "min",
            type: "number",
            label: "Min",
            show: true,
            writable: true,
            description: "min"
        }, {
            owner: "setter",
            name: "max",
            type: "number",
            label: "Max",
            show: true,
            writable: true,
            description: "max"
        }]
    }
];


WVPropertyManager.add_property_panel_group_info(Numeric019, {
    label: "Style",
    template: "vertical",
    children: [{
            owner: "setter",
            name: "s_color",
            type: "color",
            label: "Bar Color",
            show: true,
            writable: true,
            description: "선택 색상"
        },
        {
            owner: "setter",
            name: "t_color",
            type: "color",
            label: "Pin Color",
            show: true,
            writable: true,
            description: "선택 색상"
        }
    ]
})



WVPropertyManager.add_property_group_info(Numeric019, {
    label: "Numeric019 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(Numeric019, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
