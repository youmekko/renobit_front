'use strict';

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumericDonutComponent5 = function(_NumericCoreComponent) {
    _inherits(NumericDonutComponent5, _NumericCoreComponent);

    function NumericDonutComponent5() {
        _classCallCheck(this, NumericDonutComponent5);

        return _possibleConstructorReturn(this, (NumericDonutComponent5.__proto__ || Object.getPrototypeOf(NumericDonutComponent5)).call(this));
    }

    _createClass(NumericDonutComponent5, [{
        key: '_onCreateElement',
        value: function _onCreateElement() {
            this.canvas_controller = new CanvasController(this._element);
        }
    }, {
        key: 'onLoadPage',
        value: function onLoadPage() {}
    }, {
        key: '_dataRender',
        value: function _dataRender() {
            var self = this;

            // 캔버스 컨트롤러 초기화
            this.canvas_controller.frame = [];
            this.canvas_controller.resize(this.width, this.height);
            this.canvas_controller.options.refresh = true;

            // 퍼센트 정보 겟!
            var percent = _get(NumericDonutComponent5.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent5.prototype), 'getPercent', this).call(this);

            var radius = (self.width < self.height ? self.width : self.height) - 5;

            // 기본 도넛 모양
            this.canvas_controller.frame.push({
                shape: 'circle',
                x: self.width / 2,
                y: self.height / 2,
                width: radius / 2,
                fill: 'rgba(80,80,80,0.5)',
                children: [{
                    shape: 'circle',
                    x: self.width / 2,
                    y: self.height / 2,
                    width: radius / 2.2,
                    fill: this.color
                }, {
                    shape: 'circle',
                    x: self.width / 2,
                    y: self.height / 2,
                    width: radius / 2.3,
                    fill: 'rgba(255,255,255,1)'
                }],
                frame: false
            });

            this.canvas_controller.frame.push({
                shape: 'text',
                baseline: 'middle',
                x: 0,
                y: 0,
                width: self.width,
                height: self.height,
                fill: this.textColor,
                text: percent,
                font: radius / 5,
                frame: false
            });

            var unit = percent / 100 / 29;
            for (var i = 0; i < 30; i++) {
                this.canvas_controller.frame.push({
                    shape: 'donut',
                    x: self.width / 2,
                    y: self.height / 2,
                    outterRadius: radius / 2.5,
                    innerRadius: radius / 3.4,
                    sRadian: Math.PI * 1.5,
                    eRadian: Math.PI * 1.5 + Math.PI * 2 * unit * i,
                    fill: this.color,
                    frame: true
                });
            }

            self.canvas_controller.draw();
            self.canvas_controller.animate(function() {});
        }
    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            var self = this;
            _get(NumericDonutComponent5.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent5.prototype), '_onCommitProperties', this).call(this);
            if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
                this._dataRender();
            }
            if (this._updatePropertiesMap.has('extension')) {
                this._dataRender();
            }
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            this.canvas_controller.dispose();
        }
    }, {
        key: 'color',
        get: function get() {
            return this.getGroupPropertyValue("extension", "color");
        }
    }, {
        key: 'textColor',
        get: function get() {
            return this.getGroupPropertyValue("extension", "textColor");
        }
    }]);

    return NumericDonutComponent5;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(NumericDonutComponent5, {
    "info": {
        "componentName": "NumericDonutComponent5",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(0,153,255,1)",
        "textColor": "rgba(0,153,255,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent5.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}];

WVPropertyManager.add_property_group_info(NumericDonutComponent5, {
    label: "NumericDonutComponent5 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent5, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
