class NumericDonutComponent6 extends NumericCoreComponent {

    constructor() {
        super();
    }

    _onCreateElement() {
        this.canvas_controller = new CanvasController(this._element);
    }

    onLoadPage() {

    }

    _dataRender() {
        var self = this;

        // 캔버스 컨트롤러 초기화
        this.canvas_controller.frame = [];
        this.canvas_controller.resize(this.width, this.height);
        this.canvas_controller.options.refresh = true;

        // 퍼센트 정보 겟!
        var percent = super.getPercent();

        var radius = (self.width < self.height ? self.width : self.height) - 5;

        // 기본 도넛 모양

        var rgb = this.backColor.substring(this.backColor.lastIndexOf("(") + 1, this.backColor.lastIndexOf(")")).split(",");
        this.canvas_controller.frame.push({
            shape: 'circle',
            x: self.width / 2,
            y: self.height / 2,
            width: radius / 2,
            fill: "rgba(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ",1)",
            children: [{
                shape: 'circle',
                x: self.width / 2,
                y: self.height / 2,
                width: radius / 2.5,
                fill: "rgba(" + rgb[0] * 0.9 + "," + rgb[1] * 0.9 + "," + rgb[2] * 0.9 + ",1)",
            }, {
                shape: 'circle',
                x: self.width / 2,
                y: self.height / 2,
                width: radius / 3,
                fill: "rgba(" + rgb[0] * 0.5 + "," + rgb[1] * 0.5 + "," + rgb[2] * 0.5 + ",1)",
            }],
            frame: false
        });

        // 도넛 데이터 렌더 및 시계 바늘

        var unit = percent / 100 / 29;
        for (var i = 0; i < 30; i++) {
            this.canvas_controller.frame.push({
                shape: 'arc',
                x: self.width / 2,
                y: self.height / 2,
                width: radius / 2,
                sRadian: Math.PI * 1.5,
                eRadian: (Math.PI * 1.5) + (Math.PI * 2 * unit * i),
                fill: self.color,
                children: [{
                    shape: 'text',
                    baseline: 'bottom',
                    x: 0,
                    y: 0,
                    width: self.width,
                    height: self.height,
                    fill: this.textColor,
                    text: percent + '%',
                    font: radius / 5.5,
                    frame: false
                }],
                frame: true
            });
        }


        self.canvas_controller.draw();
        self.canvas_controller.animate(function() {

        });
    }

    _onCommitProperties() {
        var self = this;
        super._onCommitProperties();
        if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
            this._dataRender();
        }

        if (this._updatePropertiesMap.has('extension')) {
            this._dataRender();
        }
    }

    _onDestroy() {
        this.canvas_controller.dispose();
    }

    get color() {
        return this.getGroupPropertyValue("extension", "color");
    }

    get textColor() {
        return this.getGroupPropertyValue("extension", "textColor");
    }

    get backColor() {
        return this.getGroupPropertyValue("extension", "backColor");
    }
}

WVPropertyManager.attach_default_component_infos(NumericDonutComponent6, {
    "info": {
        "componentName": "NumericDonutComponent6",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(34,150,222,0.5)",
        "backColor": "rgba(245,187,0,1)",
        "textColor": "rgba(200,200,200,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent6.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "backColor",
        type: "color",
        label: "BackColor",
        show: true,
        writable: true,
        description: "background color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}]



WVPropertyManager.add_property_group_info(NumericDonutComponent6, {
    label: "NumericDonutComponent6 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent6, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
