CanvasRenderingContext2D.prototype.fillTextMultiLine = function(text,x,y,size) {
    var S = ("" + text).split("\n");
    for (var g = y - size, f = 0; f < S.length; f++) this.fillText(S[f], x, y), y += size;
}
class CanvasController {
    constructor(container) {
        var back_canvas = document.createElement('canvas');
        var data_canvas = document.createElement('canvas');
        $(data_canvas).css({
            width:'100%',
            height:'100%'
        });
        $(back_canvas).css({
            position:'absolute',
            left:0,top:0,
            width:'100%',
            height:'100%',
            zIndex:-10
        });
        $(container).append(back_canvas);
        $(container).append(data_canvas);
        this.data_ctx = data_canvas.getContext('2d');
        this.back_ctx = back_canvas.getContext('2d');
        this.frame = [];
        this.options = {
            refresh:false,
            back_loop:false
        }
        return this;
    }

    animate(callback) {
        var self = this;
        cancelAnimationFrame(self.req);
        var frame_count = 0;
        var draw_items = this.frame.filter(function(d){return d.frame});
        
        function loop() {
            if(self.options.refresh) {
                self.data_ctx.canvas.width = self.data_ctx.canvas.width;
                self.data_ctx.canvas.height = self.data_ctx.canvas.height;
            }
            // draw
            self.drawShape('data', draw_items[frame_count]);
            // update
            frame_count++;

            if(draw_items.length > frame_count) {
                self.req = requestAnimationFrame(loop);
            } else {
                callback();
            }
        }
        if(draw_items.length > 0) loop();
        else callback();
    }

    draw() {
        var self = this;
        cancelAnimationFrame(self.req_back);

        var frame_count = 0;
        var draw_items = this.frame.filter(function(a) {return !a.frame});
        function loop() {
            self.back_ctx.canvas.width = self.back_ctx.canvas.width;
            self.back_ctx.canvas.height = self.back_ctx.canvas.height;
            self.drawShape('back', draw_items[frame_count]);

            frame_count++
            if(draw_items.length <= frame_count) frame_count = 0;

            self.req_back = requestAnimationFrame(loop);
        }

        if(this.options.back_loop) {
            loop();
        } else {
            this.back_ctx.canvas.width = this.back_ctx.canvas.width;
            this.back_ctx.canvas.height = this.back_ctx.canvas.height;
            draw_items.forEach(function(d){
                self.drawShape('back',d);
            })
        }
    }

    resize(width,height) {
        this.back_ctx.canvas.width = width;
        this.back_ctx.canvas.height = height;
        this.data_ctx.canvas.width = width;
        this.data_ctx.canvas.height = height;
    }

    createGradient(config){
        var grd;
        if(config.type == 'linear') {
            grd = this.back_ctx.createLinearGradient(config.x0,config.y0,config.x1,config.y1)
        } else {
            grd = this.back_ctx.createRadialGradient(config.x0,config.y0,config.r0,config.x1,config.y1,config.r1)
        }
        config.colors.forEach(function(v,k){
            grd.addColorStop(k/(config.colors.length - 1),v);
        })

        return grd;
    }

    drawShape(type, config) {
        var self = this;
        var ctx = type == 'back' ? this.back_ctx : this.data_ctx;
        ctx.save();
        if(config.composite) ctx.globalCompositeOperation = config.composite;
        if(config.shadowBlur) {
            ctx.shadowBlur = config.shadowBlur;
            ctx.shadowColor = config.shadowColor;
        }
        switch(config.shape) {
            case "circle":
                ctx.beginPath();
                ctx.strokeStyle = config.stroke;
                ctx.lineWidth = config.strokewidth;
                ctx.arc(config.x, config.y, config.width, 0, Math.PI*2, true);
                ctx.closePath();
                ctx.fillStyle = config.fill;
                ctx.fill();
                if(config.strokewidth > 0) ctx.stroke();
            break;
            case "arc":
                ctx.beginPath();
                ctx.strokeStyle = config.stroke;
                ctx.lineWidth = config.strokewidth;
                ctx.moveTo(config.x, config.y);
                ctx.arc(config.x, config.y, config.width, config.sRadian, config.eRadian, false);
                ctx.closePath();
                ctx.fillStyle = config.fill;
                ctx.fill();
                if(config.strokewidth > 0) ctx.stroke();
            break;
            case "rectangle":
                ctx.strokeStyle = config.stroke;
                ctx.lineWidth = config.strokewidth;
                ctx.beginPath();
                ctx.moveTo(config.x + config.radius.lt, config.y);
                ctx.lineTo(config.x + config.width - config.radius.rt, config.y),
                ctx.quadraticCurveTo(config.x + config.width, config.y, config.x + config.width, config.y + config.radius.rt);
                ctx.lineTo(config.x + config.width, config.y + config.height - config.radius.rb),
                ctx.quadraticCurveTo(config.x + config.width, config.y + config.height, config.x + config.width - config.radius.rb, config.y + config.height);
                ctx.lineTo(config.x + config.radius.lb, config.y + config.height),
                ctx.quadraticCurveTo(config.x, config.y + config.height, config.x, config.y + config.height - config.radius.lb);
                ctx.lineTo(config.x, config.y + config.radius.lt), ctx.quadraticCurveTo(config.x, config.y, config.x + config.radius.lt, config.y);
                ctx.closePath();
                if(config.image) {
                    var srcImage = config.image;
                    ctx.drawImage(srcImage, config.x, config.y, config.width, config.height);
                } else {
                    ctx.fillStyle = config.fill;
                    ctx.fill();
                }
                if(config.strokewidth > 0) ctx.stroke();
            break;
            case "donut":
                ctx.beginPath();
                    ctx.arc(config.x, config.y, config.outterRadius, config.sRadian, config.eRadian, false);
                    ctx.arc(config.x, config.y, config.innerRadius, config.eRadian, config.sRadian, true);
                ctx.closePath();
                ctx.fillStyle = config.fill;
                ctx.fill();
            break;
            case "path" :
                ctx.strokeStyle = config.stroke;
                ctx.lineWidth = config.strokewidth;
                ctx.beginPath();
                ctx.moveTo(config.x, config.y);
                config.points.forEach(function(d){
                    if(d.type === 'line') {
                        ctx.lineTo(d.x, d.y);
                    } else if(d.type === 'beizer') {
                        ctx.bezierCurveTo(d.cp1x, d.cp1y, d.cp2x, d.cp2y, d.x, d.y);
                    } else {
                        ctx.quadraticCurveTo(d.cpx, d.cpy, d.x, d.y)
                    }
                });
                ctx.closePath();
                ctx.fillStyle = config.fill;
                ctx.fill();
                if(config.strokewidth > 0) ctx.stroke();
            break;
            case "text" :
                ctx.fillStyle = config.fill;
                ctx.textBaseline = config.baseline;
                ctx.textAlign = 'center';
                var tempText = config.text;
                ctx.font = config.font + "px Arial";
                //if(ctx.measureText(config.text).width > config.width) tempText = tempText.toFixed(2);
                if(config.text.toString().includes('\n')) {
                    ctx.fillTextMultiLine(tempText,config.x + (config.width / 2), config.y + (config.height/2), config.font)
                } else {
                    ctx.fillText(tempText, config.x + (config.width / 2), config.y + (config.height/2) + (config.font*1/2));
                }
            break;
        }
        ctx.restore();
        if(config.children && config.children.length > 0) {
            config.children.forEach(function(d){
                self.drawShape(type,d);
            })
        }
    }

    getItemByPoint (x,y) {
        return this.frame.find(function(d){
            return (x >= d.x && x <= d.x + d.width && y >= d.y && y <= d.y + d.height)
        });
    }

    dispose() {
        if(this.req) cancelAnimationFrame(this.req);
        if(this.req_back) cancelAnimationFrame(this.req_back);
        this.frame = null;
    }
}