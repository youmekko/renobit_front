class SvgController {
    constructor(container) {
        console.log(container);
        this.groups = [];
        this.svg = $(CPUtil.getInstance().createSVGElement('svg',{width:'100%',height:'100%'}));
        this.svg.attr("preserveAspectRatio","none")
        $(container).append(this.svg);
        return this;
    }

    SVG(tag) {
        return document.createElementNS('http://www.w3.org/2000/svg', tag);
    }

    append(data, container) {
        var self = this;
        var target = $(data);

        if(!container) {
            container = $(self.SVG('g'))            
            container.css({width:'100%',height:'100%'})
            container.appendTo(this.svg);
            this.groups.push(container);
        };
        target.each(function(d,k){
            if(k.tagName) {
                var tagName = k.tagName.toLowerCase();
                var svg_tag = $(self.SVG(tagName));
                if(k.children.length == 0 && k.innerText) {
                    svg_tag.text(k.innerText);
                }
                for(var i = 0; i < k.attributes.length; i++) {
                    var attr = k.attributes[i];
                    svg_tag.attr(attr.name, attr.value);
                }
                svg_tag.appendTo(container);
                if(k.children.length > 0) {
                    for(var i = 0; i < k.children.length; i++) {
                        self.append(k.children[i].outerHTML, svg_tag)
                    }
                }
            }
        });

        return container;
    }
}