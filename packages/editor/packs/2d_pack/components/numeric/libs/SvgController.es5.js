"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SvgController = function () {
    function SvgController(container) {
        _classCallCheck(this, SvgController);

        console.log(container);
        this.groups = [];
        this.svg = $(CPUtil.getInstance().createSVGElement('svg', { width: '100%', height: '100%' }));
        this.svg.attr("preserveAspectRatio", "none");
        $(container).append(this.svg);
        return this;
    }

    _createClass(SvgController, [{
        key: 'SVG',
        value: function SVG(tag) {
            return document.createElementNS('http://www.w3.org/2000/svg', tag);
        }
    }, {
        key: 'append',
        value: function append(data, container) {
            var self = this;
            var target = $(data);

            if (!container) {
                container = $(self.SVG('g'));
                container.css({ width: '100%', height: '100%' });
                container.appendTo(this.svg);
                this.groups.push(container);
            };
            target.each(function (d, k) {
                if (k.tagName) {
                    var tagName = k.tagName.toLowerCase();
                    var svg_tag = $(self.SVG(tagName));
                    if (k.children.length == 0 && k.innerText) {
                        svg_tag.text(k.innerText);
                    }
                    for (var i = 0; i < k.attributes.length; i++) {
                        var attr = k.attributes[i];
                        svg_tag.attr(attr.name, attr.value);
                    }
                    svg_tag.appendTo(container);
                    if (k.children.length > 0) {
                        for (var i = 0; i < k.children.length; i++) {
                            self.append(k.children[i].outerHTML, svg_tag);
                        }
                    }
                }
            });

            return container;
        }
    }]);

    return SvgController;
}();
