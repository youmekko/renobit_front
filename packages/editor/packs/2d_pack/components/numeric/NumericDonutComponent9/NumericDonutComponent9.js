class NumericDonutComponent9 extends NumericCoreComponent {

    constructor() {
        super();
        this._isResourceComponent = true;
    }

    async startLoadResource() {
        try {
            await this.loadImage();
            this._dataRender();
        } catch (error) {
            console.log("error ", error);
        } finally {
            this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
        }
    }



    getCompSVGPath() {
        let componentPath = window.wemb.componentLibraryManager.getComponentPath("NumericDonutComponent9");
        let src = componentPath + "/res/pattern.svg";
        return src;
    }

    loadImage() {
        var self = this;
        return new Promise((resolve, reject) => {
            self.img = new Image();
            self.img.crossOrigin = "anonymous"
            self.img.src = this.getCompSVGPath();
            $(self.img).one('load', function() {
                resolve();
            })
        })
    }

    _onCreateElement() {
        this.canvas_controller = new CanvasController(this._element);
    }

    onLoadPage() {

    }

    _dataRender() {
        var self = this;
        if (self.img) {
            this.canvas_controller.resize(this.width, this.height);
            self._drawDonut();
        }
    }

    _drawDonut() {
        var self = this;

        // 캔버스 컨트롤러 초기화
        this.canvas_controller.frame = [];
        this.canvas_controller.resize(this.width, this.height);
        this.canvas_controller.options.refresh = true;

        // 퍼센트 정보 겟!
        var percent = super.getPercent();

        var radius = (self.width < self.height ? self.width : self.height) - 5;

        this.canvas_controller.frame.push({
            shape: 'donut',
            x: self.width / 2,
            y: self.height / 2,
            outterRadius: radius / 2,
            innerRadius: radius / 2.9,
            sRadian: 0,
            eRadian: Math.PI * 2,
            fill: 'rgba(208,233,245,1)',
            children: [{
                shape: 'rectangle',
                x: self.width / 2 - radius / 2,
                y: self.height / 2 - radius / 2,
                image: self.img,
                width: radius,
                height: radius,
                radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                composite: "source-in"
            }, {
                shape: 'donut',
                x: self.width / 2,
                y: self.height / 2,
                outterRadius: radius / 2,
                innerRadius: radius / 2.9,
                sRadian: 0,
                eRadian: Math.PI * 2,
                fill: 'rgba(208,233,245,0.5)'
            }],
            frame: false
        });

        this.canvas_controller.frame.push({
            shape: 'text',
            baseline: 'bottom',
            x: 0,
            y: 0,
            width: self.width,
            height: self.height,
            fill: this.textColor,
            text: percent,
            font: radius / 5,
            frame: false
        });

        var unit = percent / 100 / 29;
        for (var i = 0; i < 30; i++) {
            this.canvas_controller.frame.push({
                shape: 'donut',
                x: self.width / 2,
                y: self.height / 2,
                outterRadius: radius / 2,
                innerRadius: radius / 2.9,
                sRadian: Math.PI * 1.5,
                eRadian: (Math.PI * 1.5) + (Math.PI * 2 * unit * i),
                fill: this.color,
                frame: true
            });
        }

        self.canvas_controller.draw();
        self.canvas_controller.animate(function() {});
    }

    _onCommitProperties() {
        var self = this;
        super._onCommitProperties();
        if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
            self._drawDonut();
        }
        if (this._updatePropertiesMap.has('extension')) {
            self._drawDonut();
        }
    }

    _onDestroy() {
        this.img = null;
        this.canvas_controller.dispose();
    }

    get color() {
        return this.getGroupPropertyValue("extension", "color");
    }

    get textColor() {
        return this.getGroupPropertyValue("extension", "textColor");
    }
}

WVPropertyManager.attach_default_component_infos(NumericDonutComponent9, {
    "info": {
        "componentName": "NumericDonutComponent9",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(19,136,216,0.6)",
        "textColor": "rgba(19,136,216,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent9.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}]



WVPropertyManager.add_property_group_info(NumericDonutComponent9, {
    label: "NumericDonutComponent9 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent9, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
