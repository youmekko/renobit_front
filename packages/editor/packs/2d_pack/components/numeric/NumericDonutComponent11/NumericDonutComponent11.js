class NumericDonutComponent11 extends NumericCoreComponent {

    constructor() {
        super();
        this._isResourceComponent = true;
    }

    async startLoadResource() {
        try {
            await this.loadImage();
            this._dataRender();
        } catch (error) {
            console.log("error ", error);
        } finally {
            this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
        }
    }

    getCompSVGPath() {
        let componentPath = window.wemb.componentLibraryManager.getComponentPath("NumericDonutComponent11");
        let src = componentPath + "/res/curv.svg";
        return src;
    }

    loadImage() {
        var self = this;
        return new Promise((resolve, reject) => {
            self.img = new Image();
            self.img.crossOrigin = "anonymous"
            self.img.src = this.getCompSVGPath();
            $(self.img).one('load', function() {
                resolve();
            })
        })
    }

    _onCreateElement() {
        this.canvas_controller = new CanvasController(this._element);
    }

    onLoadPage() {

    }

    _dataRender() {
        var self = this;
        if (self.img) {
            this.canvas_controller.resize(this.width, this.height);
            self._drawDonut();
        }
    }

    _drawDonut() {
        var self = this;

        // 캔버스 컨트롤러 초기화
        this.canvas_controller.frame = [];
        this.canvas_controller.resize(this.width, this.height);
        this.canvas_controller.options.refresh = true;

        // 퍼센트 정보 겟!
        var percent = super.getPercent();

        var radius = (self.width < self.height ? self.width : self.height) - 5;
        this.canvas_controller.frame.push({
            shape: 'rectangle',
            x: 0,
            y: 0,
            image: self.img,
            width: self.width,
            height: self.height,
            radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
            children: [{
                shape: 'rectangle',
                x: 0,
                y: 0,
                fill: 'rgba(255,255,255,0.2)',
                width: self.width,
                height: self.height,
                radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                composite: 'source-in'
            }],
            frame: false
        });

        this.canvas_controller.frame.push({
            shape: 'text',
            baseline: 'bottom',
            x: 0,
            y: self.height / 2,
            width: self.width,
            height: self.height / 2,
            fill: this.textColor,
            text: percent + '%',
            font: radius / 5,
            frame: false
        });

        var unitPercent = percent / 100 / 29;
        var startAngle = self.getAngle((self.width + (self.width * 30 / 156)) / 2,
            (self.height + (self.height * 6 / 125)) / 2, (self.width * 30 / 156), self.height);
        for (var i = 0; i < 30; i++) {
            this.canvas_controller.frame.push({
                shape: 'rectangle',
                x: 0,
                y: 0,
                image: self.img,
                width: self.width,
                height: self.height,
                radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                children: [{
                    shape: 'arc',
                    x: (self.width + (self.width * 30 / 156)) / 2,
                    y: (self.height + (self.height * 6 / 125)) / 2,
                    width: self.getDistance((self.width * 30 / 156), self.height, self.width, (self.height * 6 / 125)),
                    sRadian: startAngle,
                    eRadian: startAngle + Math.PI * unitPercent * i,
                    fill: this.color,
                    composite: "source-in"
                }],
                frame: true
            });
        }

        self.canvas_controller.draw();
        self.canvas_controller.animate(function() {});
    }

    getAngle(x1, y1, x2, y2) {
        var dx = x2 - x1;
        var dy = y2 - y1;

        return Math.atan2(dy, dx);
    }

    getDistance(x1, y1, x2, y2) {
        var dx = x2 - x1;
        var dy = y2 - y1;

        return Math.sqrt(Math.abs(dx * dx) + Math.abs(dy * dy));
    }

    _onCommitProperties() {
        var self = this;
        super._onCommitProperties();
        if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
            self._drawDonut();
        }
        if (this._updatePropertiesMap.has('extension')) {
            self._drawDonut();
        }
    }

    _onDestroy() {
        this.img = null;
        this.canvas_controller.dispose();
    }

    get color() {
        return this.getGroupPropertyValue("extension", "color");
    }

    get textColor() {
        return this.getGroupPropertyValue("extension", "textColor");
    }
}

WVPropertyManager.attach_default_component_infos(NumericDonutComponent11, {
    "info": {
        "componentName": "NumericDonutComponent11",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(19,136,216,0.6)",
        "textColor": "rgba(19,136,216,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent11.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}]


WVPropertyManager.add_property_group_info(NumericDonutComponent11, {
    label: "NumericDonutComponent11 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent11, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
