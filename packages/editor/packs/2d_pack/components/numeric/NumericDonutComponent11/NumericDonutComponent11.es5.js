"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function() { var gen = fn.apply(this, arguments); return new Promise(function(resolve, reject) {
            function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function(value) { step("next", value); }, function(err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumericDonutComponent11 = function(_NumericCoreComponent) {
    _inherits(NumericDonutComponent11, _NumericCoreComponent);

    function NumericDonutComponent11() {
        _classCallCheck(this, NumericDonutComponent11);

        var _this = _possibleConstructorReturn(this, (NumericDonutComponent11.__proto__ || Object.getPrototypeOf(NumericDonutComponent11)).call(this));

        _this._isResourceComponent = true;
        return _this;
    }

    _createClass(NumericDonutComponent11, [{
        key: "startLoadResource",
        value: function() {
            var _ref = _asyncToGenerator( /*#__PURE__*/ regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _context.next = 3;
                                return this.loadImage();

                            case 3:
                                this._dataRender();
                                _context.next = 9;
                                break;

                            case 6:
                                _context.prev = 6;
                                _context.t0 = _context["catch"](0);

                                console.log("error ", _context.t0);

                            case 9:
                                _context.prev = 9;

                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                return _context.finish(9);

                            case 12:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [
                    [0, 6, 9, 12]
                ]);
            }));

            function startLoadResource() {
                return _ref.apply(this, arguments);
            }

            return startLoadResource;
        }()
    }, {
        key: "getCompSVGPath",
        value: function getCompSVGPath() {
            var componentPath = window.wemb.componentLibraryManager.getComponentPath("NumericDonutComponent11");
            var src = componentPath + "/res/curv.svg";
            return src;
        }
    }, {
        key: "loadImage",
        value: function loadImage() {
            var _this2 = this;

            var self = this;
            return new Promise(function(resolve, reject) {
                self.img = new Image();
                self.img.crossOrigin = "anonymous";
                self.img.src = _this2.getCompSVGPath();
                $(self.img).one('load', function() {
                    resolve();
                });
            });
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.canvas_controller = new CanvasController(this._element);
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_dataRender",
        value: function _dataRender() {
            var self = this;
            if (self.img) {
                this.canvas_controller.resize(this.width, this.height);
                self._drawDonut();
            }
        }
    }, {
        key: "_drawDonut",
        value: function _drawDonut() {
            var self = this;

            // 캔버스 컨트롤러 초기화
            this.canvas_controller.frame = [];
            this.canvas_controller.resize(this.width, this.height);
            this.canvas_controller.options.refresh = true;

            // 퍼센트 정보 겟!
            var percent = _get(NumericDonutComponent11.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent11.prototype), "getPercent", this).call(this);

            var radius = (self.width < self.height ? self.width : self.height) - 5;
            this.canvas_controller.frame.push({
                shape: 'rectangle',
                x: 0,
                y: 0,
                image: self.img,
                width: self.width,
                height: self.height,
                radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                children: [{
                    shape: 'rectangle',
                    x: 0,
                    y: 0,
                    fill: 'rgba(255,255,255,0.2)',
                    width: self.width,
                    height: self.height,
                    radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                    composite: 'source-in'
                }],
                frame: false
            });

            this.canvas_controller.frame.push({
                shape: 'text',
                baseline: 'bottom',
                x: 0,
                y: self.height / 2,
                width: self.width,
                height: self.height / 2,
                fill: this.textColor,
                text: percent + '%',
                font: radius / 5,
                frame: false
            });

            var unitPercent = percent / 100 / 29;
            var startAngle = self.getAngle((self.width + self.width * 30 / 156) / 2, (self.height + self.height * 6 / 125) / 2, self.width * 30 / 156, self.height);
            for (var i = 0; i < 30; i++) {
                this.canvas_controller.frame.push({
                    shape: 'rectangle',
                    x: 0,
                    y: 0,
                    image: self.img,
                    width: self.width,
                    height: self.height,
                    radius: { lt: 0, lb: 0, rt: 0, rb: 0 },
                    children: [{
                        shape: 'arc',
                        x: (self.width + self.width * 30 / 156) / 2,
                        y: (self.height + self.height * 6 / 125) / 2,
                        width: self.getDistance(self.width * 30 / 156, self.height, self.width, self.height * 6 / 125),
                        sRadian: startAngle,
                        eRadian: startAngle + Math.PI * unitPercent * i,
                        fill: this.color,
                        composite: "source-in"
                    }],
                    frame: true
                });
            }

            self.canvas_controller.draw();
            self.canvas_controller.animate(function() {});
        }
    }, {
        key: "getAngle",
        value: function getAngle(x1, y1, x2, y2) {
            var dx = x2 - x1;
            var dy = y2 - y1;

            return Math.atan2(dy, dx);
        }
    }, {
        key: "getDistance",
        value: function getDistance(x1, y1, x2, y2) {
            var dx = x2 - x1;
            var dy = y2 - y1;

            return Math.sqrt(Math.abs(dx * dx) + Math.abs(dy * dy));
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            var self = this;
            _get(NumericDonutComponent11.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent11.prototype), "_onCommitProperties", this).call(this);
            if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
                self._drawDonut();
            }
            if (this._updatePropertiesMap.has('extension')) {
                self._drawDonut();
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.img = null;
            this.canvas_controller.dispose();
        }
    }, {
        key: "color",
        get: function get() {
            return this.getGroupPropertyValue("extension", "color");
        }
    }, {
        key: "textColor",
        get: function get() {
            return this.getGroupPropertyValue("extension", "textColor");
        }
    }]);

    return NumericDonutComponent11;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(NumericDonutComponent11, {
    "info": {
        "componentName": "NumericDonutComponent11",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "color": "rgba(19,136,216,0.6)",
        "textColor": "rgba(19,136,216,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent11.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "color",
        type: "color",
        label: "Color",
        show: true,
        writable: true,
        description: "color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}];

WVPropertyManager.add_property_group_info(NumericDonutComponent11, {
    label: "NumericDonutComponent11 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent11, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
