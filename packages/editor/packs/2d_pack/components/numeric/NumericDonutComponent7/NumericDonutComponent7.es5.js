'use strict';

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumericDonutComponent7 = function(_NumericCoreComponent) {
    _inherits(NumericDonutComponent7, _NumericCoreComponent);

    function NumericDonutComponent7() {
        _classCallCheck(this, NumericDonutComponent7);

        return _possibleConstructorReturn(this, (NumericDonutComponent7.__proto__ || Object.getPrototypeOf(NumericDonutComponent7)).call(this));
    }

    _createClass(NumericDonutComponent7, [{
        key: '_onCreateElement',
        value: function _onCreateElement() {
            this.canvas_controller = new CanvasController(this._element);
        }
    }, {
        key: 'onLoadPage',
        value: function onLoadPage() {}
    }, {
        key: '_dataRender',
        value: function _dataRender() {
            var self = this;

            // 캔버스 컨트롤러 초기화
            this.canvas_controller.frame = [];
            this.canvas_controller.resize(this.width, this.height);
            this.canvas_controller.options.refresh = true;
            this.canvas_controller.options.back_loop = true;

            // 퍼센트 정보 겟!
            var percent = _get(NumericDonutComponent7.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent7.prototype), 'getPercent', this).call(this);

            var radius = (self.width < self.height ? self.width : self.height) - 5;

            // 시계 모양
            var unit = 360 / 359;
            for (var i = 0; i < 350; i++) {
                var target_x = self.width / 2 + radius / 2 * Math.cos(Math.PI * 1.5 - Math.PI * 2 * unit * i);
                var target_y = self.height / 2 + radius / 2 * Math.sin(Math.PI * 1.5 - Math.PI * 2 * unit * i);
                var target_x1 = self.width / 2 + radius / 2 * Math.cos(Math.PI * 0.5 - Math.PI * 2 * unit * i);
                var target_y1 = self.height / 2 + radius / 2 * Math.sin(Math.PI * 0.5 - Math.PI * 2 * unit * i);
                var grd = this.canvas_controller.createGradient({
                    type: 'linear',
                    x0: target_x,
                    y0: target_y,
                    x1: target_x1,
                    y1: target_y1,
                    colors: [this.startColor, this.endColor]
                });
                this.canvas_controller.frame.push({
                    shape: 'circle',
                    x: self.width / 2,
                    y: self.height / 2,
                    width: radius / 2,
                    fill: this.backColor,
                    children: [{
                        shape: 'circle',
                        x: self.width / 2,
                        y: self.height / 2,
                        width: radius / 2.7,
                        fill: grd
                    }, {
                        shape: 'circle',
                        x: self.width / 2,
                        y: self.height / 2,
                        width: radius / 3.2,
                        fill: this.backColor
                    }, {
                        shape: 'text',
                        baseline: 'bottom',
                        x: 0,
                        y: 0,
                        width: self.width,
                        height: self.height,
                        fill: this.textColor,
                        text: percent + '%',
                        font: radius / 5.5,
                        frame: false
                    }],
                    frame: false
                });
            }

            var grd1 = this.canvas_controller.createGradient({
                type: 'linear',
                x0: 0,
                y0: 0,
                x1: self.width,
                y1: 0,
                colors: [this.startColor, this.endColor]
            });
            unit = percent / 100 / 29;
            for (var i = 0; i < 30; i++) {
                var target_x = self.width / 2 + radius / 2.1 * Math.cos(Math.PI * 1.5 + Math.PI * 2 * unit * i);
                var target_y = self.height / 2 + radius / 2.1 * Math.sin(Math.PI * 1.5 + Math.PI * 2 * unit * i);
                this.canvas_controller.frame.push({
                    shape: 'donut',
                    x: self.width / 2,
                    y: self.height / 2,
                    outterRadius: radius / 2.1,
                    innerRadius: radius / 2.6,
                    sRadian: Math.PI * 1.5,
                    eRadian: Math.PI * 1.5 + Math.PI * 2 * unit * i,
                    fill: grd1,
                    children: [{
                        shape: 'circle',
                        stroke: this.backColor,
                        strokewidth: 1,
                        fill: grd1,
                        x: target_x,
                        y: target_y,
                        width: radius / 2 - radius / 2.1
                    }],
                    frame: true
                });
            }

            self.canvas_controller.draw();
            self.canvas_controller.animate(function() {});
        }
    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            var self = this;
            _get(NumericDonutComponent7.prototype.__proto__ || Object.getPrototypeOf(NumericDonutComponent7.prototype), '_onCommitProperties', this).call(this);
            if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
                this._dataRender();
            }
            if (this._updatePropertiesMap.has('extension')) {
                this._dataRender();
            }
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            this.canvas_controller.dispose();
        }
    }, {
        key: 'startColor',
        get: function get() {
            return this.getGroupPropertyValue("extension", "startColor");
        }
    }, {
        key: 'endColor',
        get: function get() {
            return this.getGroupPropertyValue("extension", "endColor");
        }
    }, {
        key: 'textColor',
        get: function get() {
            return this.getGroupPropertyValue("extension", "textColor");
        }
    }, {
        key: 'backColor',
        get: function get() {
            return this.getGroupPropertyValue("extension", "backColor");
        }
    }]);

    return NumericDonutComponent7;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(NumericDonutComponent7, {
    "info": {
        "componentName": "NumericDonutComponent7",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "startColor": 'rgba(105,163,238,1)',
        "endColor": 'rgba(68,187,188,1)',
        "textColor": "rgba(200,200,200,1)",
        "backColor": "rgba(0,0,0,1)"
    },
    "label": {
        "label_using": "N",
        "label_text": "Numeric Donut Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

NumericDonutComponent7.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}, {
    label: "Extension",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "startColor",
        type: "color",
        label: "StartColor",
        show: true,
        writable: true,
        description: "start color"
    }, {
        owner: "extension",
        name: "endColor",
        type: "color",
        label: "EndColor",
        show: true,
        writable: true,
        description: "end color"
    }, {
        owner: "extension",
        name: "backColor",
        type: "color",
        label: "BackColor",
        show: true,
        writable: true,
        description: "background color"
    }, {
        owner: "extension",
        name: "textColor",
        type: "color",
        label: "TextColor",
        show: true,
        writable: true,
        description: "text color"
    }]
}];

WVPropertyManager.add_property_group_info(NumericDonutComponent7, {
    label: "NumericDonutComponent7 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(NumericDonutComponent7, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
