"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Numeric004 = function(_NumericCoreComponent) {
    _inherits(Numeric004, _NumericCoreComponent);

    function Numeric004() {
        _classCallCheck(this, Numeric004);

        var _this = _possibleConstructorReturn(this, (Numeric004.__proto__ || Object.getPrototypeOf(Numeric004)).call(this));

        _this.containier = null;
        _this.$bar = null;
        _this.$barlabel = null;
        _this._invalidatePropertyScolor = false;
        return _this;
    }

    _createClass(Numeric004, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            var $el = $(this._element);
            $el.append('<div class="horizon-bar-left-rounded-label"><div class="label"></div><div class="barContainer"><div class="bar"></div></div></div>');

            this.container = $el.find(".horizon-bar-left-rounded-label");
            this.$barlabel = this.container.find(".label");
            this.$barlabel.html(this.titleLabel);
            this.$barContainer = this.container.find(".barContainer");
            this.$bar = this.$barContainer.find(".bar");

            this.$bar.css({ "width": "0%" });
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {
            this.setColors();

            $(this.$bar).stop().animate({ "width": _get(Numeric004.prototype.__proto__ || Object.getPrototypeOf(Numeric004.prototype), "getPercent", this).call(this) + "%" }, 1000);
        }

        ///화면에 붙였을때

    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.setColors();
            $(this.$bar).stop().animate({ "width": _get(Numeric004.prototype.__proto__ || Object.getPrototypeOf(Numeric004.prototype), "getPercent", this).call(this) + "%" }, 1000);
            this._validateFontStyleProperty();
        }
    }, {
        key: "_dataRender",
        value: function _dataRender() {
            $(this.$bar).stop().animate({ "width": "0%" }, 0).stop().animate({ "width": this.getPercent() + "%" }, 1000);
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            _get(Numeric004.prototype.__proto__ || Object.getPrototypeOf(Numeric004.prototype), "_onCommitProperties", this).call(this); // for Numeric min-value-max

            if (this._invalidatePropertyScolor) {
                this.validateCallLater(this._validateSColorProp);
                this._invalidatePropertyScolor = false;
            }

            if (this._invalidatePropertyTcolor) {
                this.validateCallLater(this._validateTColorProp);
                this._invalidatePropertyTcolor = false;
            }

            if (this._invalidatePropertyTitleLabel) {
                this.validateCallLater(this._validateTitleLabelProp);
                this._invalidatePropertyTitleLabel = false;
            }

            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this._validateFontStyleProperty);
            }
        }
    }, {
        key: "setColors",
        value: function setColors() {
            var barBackground = this.s_color;
            this.$bar.css("background", barBackground);

            this.$barContainer.css("border-color", this.s_color);
            this.$barlabel.css("color", this.t_color);
        }
    }, {
        key: "_validateSColorProp",
        value: function _validateSColorProp() {
            this.setColors();
        }
    }, {
        key: "_validateFontStyleProperty",
        value: function _validateFontStyleProperty() {
            var fontProps = this.font;
            this.$barlabel.css({
                "fontFamily": fontProps.font_type,
                "fontSize": fontProps.font_size,
                "fontWeight": fontProps.font_weight,
                "color": fontProps.font_color,
                "textAlign": fontProps.text_align,
                "lineHeight": fontProps.line_height + "px"
            });

            //this.updateTxtElHeight();
        }
    }, {
        key: "_validateTColorProp",
        value: function _validateTColorProp() {
            var barBackground = this.s_color;
            this.$bar.css("background", barBackground);
            this.$barlabel.css("color", this.t_color);
        }
    }, {
        key: "_validateTitleLabelProp",
        value: function _validateTitleLabelProp() {
            this.$barlabel.html(this.titleLabel);
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.containier = null;
            this.$bar = null;
            this.$barlabel = null;
            _get(Numeric004.prototype.__proto__ || Object.getPrototypeOf(Numeric004.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "s_color",
        get: function get() {
            return this.getGroupPropertyValue("setter", "s_color");
        },
        set: function set(color) {
            if (this._checkUpdateGroupPropertyValue("setter", "s_color", color)) {
                this._invalidatePropertyScolor = true;
            }
        }
    }, {
        key: "t_color",
        get: function get() {
            return this.getGroupPropertyValue("setter", "t_color");
        },
        set: function set(color) {
            if (this._checkUpdateGroupPropertyValue("setter", "t_color", color)) {
                this._invalidatePropertyTcolor = true;
            }
        }
    }, {
        key: "titleLabel",
        get: function get() {
            return this.getGroupPropertyValue("setter", "titleLabel");
        },
        set: function set(txt) {
            if (this._checkUpdateGroupPropertyValue("setter", "titleLabel", txt)) {
                this._invalidatePropertyTitleLabel = true;
            }
        }
    }, {
        key: "font",
        get: function get() {
            return this.getGroupProperties("font");
        }
    }]);

    return Numeric004;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(Numeric004, {
    "info": {
        "componentName": "Numeric004",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300, // 기본 크기
        "height": 30, // 기본 크기
        "s_color": "#0a6aa1",
        "t_color": "#000",
        "titleLabel": "label",
        "value": 10,
        "min": 0,
        "max": 100
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 18,
        "font_weight": "bold",
        "line_height": 14,
        "text_align": "left"
    },

    "style": {
        "border": "0px solid #000000",
        "borderRadius": 1,
        "cursor": "default"

    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
Numeric004.property_panel_info = [{
    template: "primary",
    label: "primary"
}, {
    template: "pos-size-2d",
    label: "pos-size-2d"
}, {
    template: "cursor",
    label: "pos-size-2d"
}, {
    template: "label",
    label: "label"
}, {
    template: "border",
    label: "border"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "max"
    }]
}];

WVPropertyManager.add_property_panel_group_info(Numeric004, {
    label: "Style",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "s_color",
        type: "color",
        label: "Bar Color",
        show: true,
        writable: true,
        description: "Bar Color "
    }, {
        owner: "setter",
        name: "titleLabel",
        type: "normal",
        label: "Label",
        show: true,
        writable: true,
        description: "Text"
    }]
});

WVPropertyManager.add_property_group_info(Numeric004, {
    label: "Numeric004 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(Numeric004, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
