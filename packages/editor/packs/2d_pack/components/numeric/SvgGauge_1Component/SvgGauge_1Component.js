class SvgGauge_1Component extends NumericCoreComponent {

    constructor() {
        super();
        this.rad = Math.PI / 180;
        this.offset = 80;
        this._invalidateNeedleColor = false;
        this._invalidateOutLineColor = false;
        this._invalidateFillColor = false;
        this.drawPoints = [];
        this.oriWidth = null;
    }

    _onCreateElement() {
        this.gauge = CPUtil.getInstance().createSVGElement("svg", { "height": "100%", "width": "100%" });
        this.gaugeArea = CPUtil.getInstance().createSVGElement("g");

        var sacleGroup = CPUtil.getInstance().createSVGElement("g", { "class": "scale" });
        this.gaugeArea.appendChild(sacleGroup);

        this.outLine = CPUtil.getInstance().createSVGElement("path", { "class": "outline" });
        this.gaugeArea.appendChild(this.outLine);

        this.fill = CPUtil.getInstance().createSVGElement("path", { "class": "fill" });
        this.gaugeArea.appendChild(this.fill);

        this.needle = CPUtil.getInstance().createSVGElement("polygon", { "class": "needle", "points": "220,10 300,210 220,250 140,210" });
        this.gaugeArea.appendChild(this.needle);

        this.circle = CPUtil.getInstance().createSVGElement("circle", { "cx": "165", "cy": "155", "r": "25", "fill": "#DA3303" });
        this.gaugeArea.appendChild(this.circle);
        this.text = CPUtil.getInstance().createSVGElement("text", { "class": "output", "x": "165", "y": "160", "fill": "#ffffff" });
        this.gaugeArea.appendChild(this.text);

        this.gauge.appendChild(this.gaugeArea);

        this.element.appendChild(this.gauge);

        this.cx = 165;
        this.cy = 160;
        this.r1 = this.cx - this.offset;
        this.delta = Math.floor(this.r1 / 2);

        this.x1 = this.cx + this.r1, this.y1 = this.cy;
        this.r2 = this.r1 - this.delta;

        this.x2 = this.offset, this.y2 = this.cy;
        this.x3 = this.x1 - this.delta, this.y3 = this.cy;

        var temp = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

        for (var i = 0; i < temp.length; i++) {
            this.getPoints(temp[i]);
        }

        this.oriWidth = 330;
        this.calcScale();

        this._dataRender();
    }

    onLoadPage() {
        this.setStyle();
    }

    _dataRender() {
        var percent = super.getPercent();
        var pa = (percent * 1.8) - 180;
        var p = {};
        p.x = this.cx + this.r1 * Math.cos(pa * this.rad);
        p.y = this.cy + this.r1 * Math.sin(pa * this.rad);
        this.updateInput(p, this.cx, this.cy, this.r1, this.offset, this.delta);
    }

    _onCommitProperties() {

    }

    getPoints(value) {

        var pa = (value * 1.8) - 180;
        var p = {};
        p.x = this.cx + this.r1 * Math.cos(pa * this.rad);
        p.y = this.cy + this.r1 * Math.sin(pa * this.rad);

        var x = p.x;
        var y = p.y;
        var lx = this.cx - x;
        var ly = this.cy - y;

        var a = Math.atan2(ly, lx) / this.rad - 180;

        var nx1 = this.cx + 5 * Math.cos((a - 90) * this.rad);
        var ny1 = this.cy + 5 * Math.sin((a - 90) * this.rad);

        var nx2 = this.cx + (this.r1 - 10) * Math.cos(a * this.rad);
        var ny2 = this.cy + (this.r1 - 10) * Math.sin(a * this.rad);

        var nx3 = this.cx + 5 * Math.cos((a + 90) * this.rad);
        var ny3 = this.cy + 5 * Math.sin((a + 90) * this.rad);
        var pointsNe = nx1 + "," + ny1 + " " + nx2 + "," + ny2 + " " + nx3 + "," + ny3;

        this.drawPoints.push({ "value": value, "points": pointsNe });

    }

    drawScale() {
        this.sr1 = this.r1 + 5;
        this.sr2 = this.r2 - 5;
        this.srT = this.r1 + 15;
        var scale = $(this._element).find(".scale").get(0);
        this.clearRect(scale)
        var n = 0;
        for (var sa = -180; sa <= 0; sa += 18) {
            var sx1 = this.cx + this.sr1 * Math.cos(this.sa * this.rad);
            var sy1 = this.cy + this.sr1 * Math.sin(this.sa * this.rad);
            var sx2 = this.cx + this.sr2 * Math.cos(this.sa * this.rad);
            var sy2 = this.cy + this.sr2 * Math.sin(this.sa * this.rad);

            if (n == 0 || n == 5 || n == 10) {

                var temp = (n == 5) ? this.srT - 10 : this.srT;

                var scaleTextObj = {
                    "class": "scale",
                    "x": this.cx + temp * Math.cos(sa * this.rad),
                    "y": this.cy + temp * Math.sin(sa * this.rad),
                };
                var scaleText = CPUtil.getInstance().createSVGElement("text", scaleTextObj);

                scaleText.textContent = n * 10;

                scale.appendChild(scaleText);
            }

            n++

        }

    }

    drawInput(cx, cy, r1, offset, delta, a) {

        var d1 = this.getD1(cx, cy, r1, offset, delta);
        var d2 = this.getD2(cx, cy, r1, offset, delta, a);

        this.drawScale();

        this.outLine.setAttributeNS(null, "d", d1);
        this.fill.setAttributeNS(null, "d", d2);

        this.drawNeedle(cx, cy, r1, a);

    }

    updateInput(p, cx, cy, r1, offset, delta) {

        var x = p.x;
        var y = p.y;
        var lx = cx - x;
        var ly = cy - y;

        var a = Math.atan2(ly, lx) / this.rad - 180;

        this.drawInput(cx, cy, r1, offset, delta, a);
        this.text.textContent = Math.round((a + 180) / 1.8);
    }

    getD1(cx, cy, r1, offset, delta) {

        var x1 = cx + r1,
            y1 = cy;
        var x2 = offset,
            y2 = cy;
        var r2 = r1 - delta;
        var x3 = x1 - delta,
            y3 = cy;
        var d1 =
            "M " + x1 + ", " + y1 + " A" + r1 + "," + r1 + " 0 0 0 " + x2 + "," + y2 + " H" + (offset + delta) + " A" + r2 + "," + r2 + " 0 0 1 " + x3 + "," + y3 + " z";
        return d1;
    }

    getD2(cx, cy, r1, offset, delta, a) {
        a *= this.rad;
        var r2 = r1 - delta;
        var x2 = offset,
            y2 = cy;
        var x4 = cx + r1 * Math.cos(a);
        var y4 = cy + r1 * Math.sin(a);
        var x5 = cx + r2 * Math.cos(a);
        var y5 = cy + r2 * Math.sin(a);

        var d2 =
            "M " + x4 + ", " + y4 + " A" + r1 + "," + r1 + " 0 0 0 " + x2 + "," + y2 + " H" + (offset + delta) + " A" + r2 + "," + r2 + " 0 0 1 " + x5 + "," + y5 + " z";
        return d2;
    }

    drawNeedle(cx, cy, r1, a) {

        var nx1 = cx + 5 * Math.cos((a - 90) * this.rad);
        var ny1 = cy + 5 * Math.sin((a - 90) * this.rad);

        var nx2 = cx + (r1 - 10) * Math.cos(a * this.rad);
        var ny2 = cy + (r1 - 10) * Math.sin(a * this.rad);

        var nx3 = cx + 5 * Math.cos((a + 90) * this.rad);
        var ny3 = cy + 5 * Math.sin((a + 90) * this.rad);

        var pointsNe = nx1 + "," + ny1 + " " + nx2 + "," + ny2 + " " + nx3 + "," + ny3;

        var that = this;
        var percent = super.getPercent();
        var pointList = this.drawPoints.filter(function(obj) {
            return obj.value < percent;
        });
        pointList.push({ "value": percent, "points": pointsNe });

        var tl = new TimelineMax();
        for (var i = 0; i < pointList.length; i++) {
            if (i == 0) {
                this.needle.setAttributeNS(null, "points", pointList[i]["points"])
                continue;
            }
            tl.to(this.needle, 0.1, { attr: { points: pointList[i]["points"] } });
        }

    }

    clearRect(node) {
        while (node.firstChild) {
            node.removeChild(node.firstChild);
        }
    }

    setSVGAttributes(elmt, oAtt) {
        for (var prop in oAtt) {
            elmt.setAttributeNS(null, prop, oAtt[prop]);
        }
    }

    _onCommitProperties() {
        super._onCommitProperties();

        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this.setStyle);
        }

        if (this.invalidateSize || this.invalidateVisible) {
            this.validateCallLater(this.calcScale);
        }

    }

    calcScale() {
        var oriSize = parseInt(this.oriWidth);
        var scale = 1 / this.oriWidth * this.width;
        this.setSVGAttributes(this.gaugeArea, {
            "transform": "scale(" + scale + "," + scale + ")",
        })
    }

    setStyle() {
        this.fill.style.fill = this.fillColor;
        this.outLine.style.fill = this.outLineColor;
        this.needle.style.fill = this.needleColor;
        this.circle.style.fill = this.needleColor;
    }

    set fillColor(value) {
        if (this._checkUpdateGroupPropertyValue("extension", "fillColor", value)) {
            this._invalidateFillColor = true;
        }
    }

    get fillColor() {
        return this.getGroupPropertyValue("extension", "fillColor");
    }

    set outLineColor(value) {
        if (this._checkUpdateGroupPropertyValue("extension", "outLineColor", value)) {
            this._invalidateOutLineColor = true;
        }
    }

    get outLineColor() {
        return this.getGroupPropertyValue("extension", "outLineColor");
    }

    set needleColor(value) {
        if (this._checkUpdateGroupPropertyValue("extension", "needleColor", value)) {
            this._invalidateNeedleColor = true;
        }
    }

    get needleColor() {
        return this.getGroupPropertyValue("extension", "needleColor");
    }

    _onDestroy() {}

}

WVPropertyManager.attach_default_component_infos(SvgGauge_1Component, {
    "info": {
        "componentName": "SvgGauge_1Component",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "fillColor": "#e4ecef",
        "outLineColor": "#e4ecef",
        "needleColor": "#DA3303"
    },
    "label": {
        "label_using": "N",
        "label_text": "Grid Gauge Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

SvgGauge_1Component.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "guage value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "guage min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "guage max"
    }]
}, {
    label: "Color Options",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "needleColor",
        type: "color",
        label: "Needle",
        show: true,
        writable: true,
        description: "needleColor"
    }, {
        owner: "extension",
        name: "outLineColor",
        type: "color",
        label: "Outline",
        show: true,
        writable: true,
        description: "outLineColor"
    }, {
        owner: "extension",
        name: "fillColor",
        type: "color",
        label: "Fill",
        show: true,
        writable: true,
        description: "fillColor"
    }]
}]


WVPropertyManager.add_property_group_info(SvgGauge_1Component, {
    label: "SvgGauge_1Component 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(SvgGauge_1Component, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
