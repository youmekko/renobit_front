"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SvgGauge_1Component = function(_NumericCoreComponent) {
    _inherits(SvgGauge_1Component, _NumericCoreComponent);

    function SvgGauge_1Component() {
        _classCallCheck(this, SvgGauge_1Component);

        var _this = _possibleConstructorReturn(this, (SvgGauge_1Component.__proto__ || Object.getPrototypeOf(SvgGauge_1Component)).call(this));

        _this.rad = Math.PI / 180;
        _this.offset = 80;
        _this._invalidateNeedleColor = false;
        _this._invalidateOutLineColor = false;
        _this._invalidateFillColor = false;
        _this.drawPoints = [];
        _this.oriWidth = null;
        return _this;
    }

    _createClass(SvgGauge_1Component, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.gauge = CPUtil.getInstance().createSVGElement("svg", { "height": "100%", "width": "100%" });
            this.gaugeArea = CPUtil.getInstance().createSVGElement("g");

            var sacleGroup = CPUtil.getInstance().createSVGElement("g", { "class": "scale" });
            this.gaugeArea.appendChild(sacleGroup);

            this.outLine = CPUtil.getInstance().createSVGElement("path", { "class": "outline" });
            this.gaugeArea.appendChild(this.outLine);

            this.fill = CPUtil.getInstance().createSVGElement("path", { "class": "fill" });
            this.gaugeArea.appendChild(this.fill);

            this.needle = CPUtil.getInstance().createSVGElement("polygon", { "class": "needle", "points": "220,10 300,210 220,250 140,210" });
            this.gaugeArea.appendChild(this.needle);

            this.circle = CPUtil.getInstance().createSVGElement("circle", { "cx": "165", "cy": "155", "r": "25", "fill": "#DA3303" });
            this.gaugeArea.appendChild(this.circle);
            this.text = CPUtil.getInstance().createSVGElement("text", { "class": "output", "x": "165", "y": "160", "fill": "#ffffff" });
            this.gaugeArea.appendChild(this.text);

            this.gauge.appendChild(this.gaugeArea);

            this.element.appendChild(this.gauge);

            this.cx = 165;
            this.cy = 160;
            this.r1 = this.cx - this.offset;
            this.delta = Math.floor(this.r1 / 2);

            this.x1 = this.cx + this.r1, this.y1 = this.cy;
            this.r2 = this.r1 - this.delta;

            this.x2 = this.offset, this.y2 = this.cy;
            this.x3 = this.x1 - this.delta, this.y3 = this.cy;

            var temp = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

            for (var i = 0; i < temp.length; i++) {
                this.getPoints(temp[i]);
            }

            this.oriWidth = 330;
            this.calcScale();

            this._dataRender();
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {
            this.setStyle();
        }
    }, {
        key: "_dataRender",
        value: function _dataRender() {
            var percent = _get(SvgGauge_1Component.prototype.__proto__ || Object.getPrototypeOf(SvgGauge_1Component.prototype), "getPercent", this).call(this);
            var pa = percent * 1.8 - 180;
            var p = {};
            p.x = this.cx + this.r1 * Math.cos(pa * this.rad);
            p.y = this.cy + this.r1 * Math.sin(pa * this.rad);
            this.updateInput(p, this.cx, this.cy, this.r1, this.offset, this.delta);
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {}
    }, {
        key: "getPoints",
        value: function getPoints(value) {

            var pa = value * 1.8 - 180;
            var p = {};
            p.x = this.cx + this.r1 * Math.cos(pa * this.rad);
            p.y = this.cy + this.r1 * Math.sin(pa * this.rad);

            var x = p.x;
            var y = p.y;
            var lx = this.cx - x;
            var ly = this.cy - y;

            var a = Math.atan2(ly, lx) / this.rad - 180;

            var nx1 = this.cx + 5 * Math.cos((a - 90) * this.rad);
            var ny1 = this.cy + 5 * Math.sin((a - 90) * this.rad);

            var nx2 = this.cx + (this.r1 - 10) * Math.cos(a * this.rad);
            var ny2 = this.cy + (this.r1 - 10) * Math.sin(a * this.rad);

            var nx3 = this.cx + 5 * Math.cos((a + 90) * this.rad);
            var ny3 = this.cy + 5 * Math.sin((a + 90) * this.rad);
            var pointsNe = nx1 + "," + ny1 + " " + nx2 + "," + ny2 + " " + nx3 + "," + ny3;

            this.drawPoints.push({ "value": value, "points": pointsNe });
        }
    }, {
        key: "drawScale",
        value: function drawScale() {
            this.sr1 = this.r1 + 5;
            this.sr2 = this.r2 - 5;
            this.srT = this.r1 + 15;
            var scale = $(this._element).find(".scale").get(0);
            this.clearRect(scale);
            var n = 0;
            for (var sa = -180; sa <= 0; sa += 18) {
                var sx1 = this.cx + this.sr1 * Math.cos(this.sa * this.rad);
                var sy1 = this.cy + this.sr1 * Math.sin(this.sa * this.rad);
                var sx2 = this.cx + this.sr2 * Math.cos(this.sa * this.rad);
                var sy2 = this.cy + this.sr2 * Math.sin(this.sa * this.rad);

                if (n == 0 || n == 5 || n == 10) {

                    var temp = n == 5 ? this.srT - 10 : this.srT;

                    var scaleTextObj = {
                        "class": "scale",
                        "x": this.cx + temp * Math.cos(sa * this.rad),
                        "y": this.cy + temp * Math.sin(sa * this.rad)
                    };
                    var scaleText = CPUtil.getInstance().createSVGElement("text", scaleTextObj);

                    scaleText.textContent = n * 10;

                    scale.appendChild(scaleText);
                }

                n++;
            }
        }
    }, {
        key: "drawInput",
        value: function drawInput(cx, cy, r1, offset, delta, a) {

            var d1 = this.getD1(cx, cy, r1, offset, delta);
            var d2 = this.getD2(cx, cy, r1, offset, delta, a);

            this.drawScale();

            this.outLine.setAttributeNS(null, "d", d1);
            this.fill.setAttributeNS(null, "d", d2);

            this.drawNeedle(cx, cy, r1, a);
        }
    }, {
        key: "updateInput",
        value: function updateInput(p, cx, cy, r1, offset, delta) {

            var x = p.x;
            var y = p.y;
            var lx = cx - x;
            var ly = cy - y;

            var a = Math.atan2(ly, lx) / this.rad - 180;

            this.drawInput(cx, cy, r1, offset, delta, a);
            this.text.textContent = Math.round((a + 180) / 1.8);
        }
    }, {
        key: "getD1",
        value: function getD1(cx, cy, r1, offset, delta) {

            var x1 = cx + r1,
                y1 = cy;
            var x2 = offset,
                y2 = cy;
            var r2 = r1 - delta;
            var x3 = x1 - delta,
                y3 = cy;
            var d1 = "M " + x1 + ", " + y1 + " A" + r1 + "," + r1 + " 0 0 0 " + x2 + "," + y2 + " H" + (offset + delta) + " A" + r2 + "," + r2 + " 0 0 1 " + x3 + "," + y3 + " z";
            return d1;
        }
    }, {
        key: "getD2",
        value: function getD2(cx, cy, r1, offset, delta, a) {
            a *= this.rad;
            var r2 = r1 - delta;
            var x2 = offset,
                y2 = cy;
            var x4 = cx + r1 * Math.cos(a);
            var y4 = cy + r1 * Math.sin(a);
            var x5 = cx + r2 * Math.cos(a);
            var y5 = cy + r2 * Math.sin(a);

            var d2 = "M " + x4 + ", " + y4 + " A" + r1 + "," + r1 + " 0 0 0 " + x2 + "," + y2 + " H" + (offset + delta) + " A" + r2 + "," + r2 + " 0 0 1 " + x5 + "," + y5 + " z";
            return d2;
        }
    }, {
        key: "drawNeedle",
        value: function drawNeedle(cx, cy, r1, a) {

            var nx1 = cx + 5 * Math.cos((a - 90) * this.rad);
            var ny1 = cy + 5 * Math.sin((a - 90) * this.rad);

            var nx2 = cx + (r1 - 10) * Math.cos(a * this.rad);
            var ny2 = cy + (r1 - 10) * Math.sin(a * this.rad);

            var nx3 = cx + 5 * Math.cos((a + 90) * this.rad);
            var ny3 = cy + 5 * Math.sin((a + 90) * this.rad);

            var pointsNe = nx1 + "," + ny1 + " " + nx2 + "," + ny2 + " " + nx3 + "," + ny3;

            var that = this;
            var percent = _get(SvgGauge_1Component.prototype.__proto__ || Object.getPrototypeOf(SvgGauge_1Component.prototype), "getPercent", this).call(this);
            var pointList = this.drawPoints.filter(function(obj) {
                return obj.value < percent;
            });
            pointList.push({ "value": percent, "points": pointsNe });

            var tl = new TimelineMax();
            for (var i = 0; i < pointList.length; i++) {
                if (i == 0) {
                    this.needle.setAttributeNS(null, "points", pointList[i]["points"]);
                    continue;
                }
                tl.to(this.needle, 0.1, { attr: { points: pointList[i]["points"] } });
            }
        }
    }, {
        key: "clearRect",
        value: function clearRect(node) {
            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
        }
    }, {
        key: "setSVGAttributes",
        value: function setSVGAttributes(elmt, oAtt) {
            for (var prop in oAtt) {
                elmt.setAttributeNS(null, prop, oAtt[prop]);
            }
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            _get(SvgGauge_1Component.prototype.__proto__ || Object.getPrototypeOf(SvgGauge_1Component.prototype), "_onCommitProperties", this).call(this);

            if (this._updatePropertiesMap.has("extension")) {
                this.validateCallLater(this.setStyle);
            }

            if (this.invalidateSize || this.invalidateVisible) {
                this.validateCallLater(this.calcScale);
            }
        }
    }, {
        key: "calcScale",
        value: function calcScale() {
            var oriSize = parseInt(this.oriWidth);
            var scale = 1 / this.oriWidth * this.width;
            this.setSVGAttributes(this.gaugeArea, {
                "transform": "scale(" + scale + "," + scale + ")"
            });
        }
    }, {
        key: "setStyle",
        value: function setStyle() {
            this.fill.style.fill = this.fillColor;
            this.outLine.style.fill = this.outLineColor;
            this.needle.style.fill = this.needleColor;
            this.circle.style.fill = this.needleColor;
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {}
    }, {
        key: "fillColor",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extension", "fillColor", value)) {
                this._invalidateFillColor = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("extension", "fillColor");
        }
    }, {
        key: "outLineColor",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extension", "outLineColor", value)) {
                this._invalidateOutLineColor = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("extension", "outLineColor");
        }
    }, {
        key: "needleColor",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extension", "needleColor", value)) {
                this._invalidateNeedleColor = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("extension", "needleColor");
        }
    }]);

    return SvgGauge_1Component;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(SvgGauge_1Component, {
    "info": {
        "componentName": "SvgGauge_1Component",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "fillColor": "#e4ecef",
        "outLineColor": "#e4ecef",
        "needleColor": "#DA3303"
    },
    "label": {
        "label_using": "N",
        "label_text": "Grid Gauge Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

SvgGauge_1Component.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "guage value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "guage min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "guage max"
    }]
}, {
    label: "Color Options",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "needleColor",
        type: "color",
        label: "Needle",
        show: true,
        writable: true,
        description: "needleColor"
    }, {
        owner: "extension",
        name: "outLineColor",
        type: "color",
        label: "Outline",
        show: true,
        writable: true,
        description: "outLineColor"
    }, {
        owner: "extension",
        name: "fillColor",
        type: "color",
        label: "Fill",
        show: true,
        writable: true,
        description: "fillColor"
    }]
}];

WVPropertyManager.add_property_group_info(SvgGauge_1Component, {
    label: "SvgGauge_1Component 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(SvgGauge_1Component, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
