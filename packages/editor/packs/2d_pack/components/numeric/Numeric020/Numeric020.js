class Numeric020 extends NumericCoreComponent {
    constructor() {
        super();
        this.containier = null;
        this.$bar = null;
        this.$barlabel = null;
        this._invalidatePropertyScolor = false;
    }

    _onCreateElement() {
        let $el = $(this._element);
        $el.append('<div class="horizon-bar-battery"><div class="barContainer"><div class="bar"><div class="barGradient"></div></div></div><div class="barRight"></div><div class="label"><div class="labelBody"></div><div class="labelArrow"><div class="labelTxt">60%</div></div></div></div>');


        this.container = $el.find(".horizon-bar-battery");
        this.$barlabel = this.container.find(".label");
        this.$barlabelTxt = this.$barlabel.find(".labelTxt");
        this.$barlabelArrow = this.$barlabel.find(".labelArrow");
        this.$barlabelBody = this.container.find(".labelBody");
        this.$barContainer = this.container.find(".barContainer");
        this.$bar = this.$barContainer.find(".bar");

        this.$barGradient = this.$bar.find(".barGradient");

        this.$bar.css({ "width": "0%" });
    }

    onLoadPage() {
        this.setColors();
        $(this.$bar).stop().animate({ "width": Math.round(this.getPercent()) + "%" }, 1000);
    }

    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {
        this.setColors();
        //$(this.$bar).stop().animate({"width":Math.round(this.getPercent())+"%"},1000);

        // Horizontal Bar (Battery )
        this.$bar.stop().animate({ "width": "0%" }, 0);
        this.$bar.stop().animate({ "width": Math.round(this.getPercent()) + "%" }, 1000);
        this.$barlabelTxt.html(Math.round(this.getPercent()) + "%");


    }

    _dataRender() {
        this.setColors();
        //$(this.$bar).stop().animate({"width":Math.round(this.getPercent())+"%"},1000);

        // Horizontal Bar (Battery )
        this.$bar.stop().animate({ "width": "0%" }, 0);
        this.$bar.stop().animate({ "width": Math.round(this.getPercent()) + "%" }, 1000);
        this.$barlabelTxt.html(Math.round(this.getPercent()) + "%");

    }


    _onCommitProperties() {
        super._onCommitProperties(); // for Numeric min-value-max

        if (this._invalidatePropertyScolor) {
            this.validateCallLater(this._validateSColorProp)
            this._invalidatePropertyScolor = false;
        }

        if (this._invalidatePropertyTcolor) {
            this.validateCallLater(this._validateTColorProp)
            this._invalidatePropertyTcolor = false;
        }

        if (this._invalidatePropertyTitleLabel) {
            this.validateCallLater(this._validateTitleLabelProp)
            this._invalidatePropertyTitleLabel = false;
        }

        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty)
        }
    }

    setColors() {
        let barBackground = "linear-gradient(to right, " + this.s_color + " ," + this.t_color + ")";
        this.$bar.css("background", barBackground);
        let barBackgroundLabelArrow = "linear-gradient(to left, " + this.s_color + " ," + this.t_color + ")";
        this.$barlabelArrow.css("background", barBackgroundLabelArrow);
        this.$barlabelBody.css("borderRight", "30px solid " + this.t_color);
    }

    _validateSColorProp() {
        this.setColors();
    }

    _validateFontStyleProperty() {
        var fontProps = this.font;
        this.$barlabelTxt.css({
            "fontFamily": fontProps.font_type,
            "fontSize": fontProps.font_size,
            "fontWeight": fontProps.font_weight,
            "color": fontProps.font_color,
            "textAlign": fontProps.text_align,
            "lineHeight": fontProps.line_height + "px"
        });

        //this.updateTxtElHeight();
    }


    _validateTColorProp() {
        this.setColors();
    }

    _validateTitleLabelProp() {
        this.$barlabel.html(this.titleLabel);
    }


    _onDestroy() {
        this.containier = null;
        this.$bar = null;
        this.$barlabel = null;
        super._onDestroy();
    }

    get s_color() {
        return this.getGroupPropertyValue("setter", "s_color");

    }

    set s_color(color) {
        if (this._checkUpdateGroupPropertyValue("setter", "s_color", color)) {
            this._invalidatePropertyScolor = true;
        }
    }


    get t_color() {
        return this.getGroupPropertyValue("setter", "t_color");

    }

    set t_color(color) {
        if (this._checkUpdateGroupPropertyValue("setter", "t_color", color)) {
            this._invalidatePropertyTcolor = true;
        }
    }


    get titleLabel() {
        return this.getGroupPropertyValue("setter", "titleLabel");

    }

    set titleLabel(txt) {
        if (this._checkUpdateGroupPropertyValue("setter", "titleLabel", txt)) {
            this._invalidatePropertyTitleLabel = true;
        }
    }

    get font() {
        return this.getGroupProperties("font");
    }

}

WVPropertyManager.attach_default_component_infos(Numeric020, {
    "info": {
        "componentName": "Numeric020",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300, // 기본 크기
        "height": 50, // 기본 크기
        "s_color": "#47d5fe",
        "t_color": "#6863cc",
        "value": 10,
        "min": 0,
        "max": 100
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#FFF",
        "font_size": 18,
        "font_weight": "bold",
        "line_height": 14,
        "text_align": "center"
    },

    "style": {
        "border": "0px solid #000000",
        "borderRadius": 1,
        "cursor": "default"

    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
Numeric020.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    }, {
        template: "cursor",
        label: "pos-size-2d"
    },
    {
        template: "label",
        label: "label"
    }, {
        template: "border",
        label: "border"
    }, {
        label: "Numeric",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "value",
            type: "number",
            label: "Value",
            show: true,
            writable: true,
            description: "value"
        }, {
            owner: "setter",
            name: "min",
            type: "number",
            label: "Min",
            show: true,
            writable: true,
            description: "min"
        }, {
            owner: "setter",
            name: "max",
            type: "number",
            label: "Max",
            show: true,
            writable: true,
            description: "max"
        }]
    }
];


WVPropertyManager.add_property_panel_group_info(Numeric020, {
    label: "Gradient Colors",
    template: "vertical",
    children: [{
            owner: "setter",
            name: "s_color",
            type: "color",
            label: "Color1",
            show: true,
            writable: true,
            description: "선택 색상"
        },
        {
            owner: "setter",
            name: "t_color",
            type: "color",
            label: "Color2",
            show: true,
            writable: true,
            description: "선택 색상"
        }
    ]
})



WVPropertyManager.add_property_group_info(Numeric020, {
    label: "Numeric020 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(Numeric020, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
