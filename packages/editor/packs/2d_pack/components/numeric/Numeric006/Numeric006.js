class Numeric006 extends NumericCoreComponent {
    constructor() {
        super();
        this.containier = null;
        this.$bar = null;
        this.$barlabel = null;
        this._invalidatePropertyScolor = false;
    }

    _onCreateElement() {
        let $el = $(this._element);
        //$el.append('<div class="vertical-bar-center-balloon"><div class="barContainer"><div class="bar"><div class="barGradient"></div></div></div><div class="barRight"></div><div class="label"><div class="labelBody"></div><div class="labelArrow"><div class="labelTxt">60%</div></div></div></div>');


        $el.append('<div class="vertical-bar-center-balloon">' +
            '<div class="label">' +
            '<div class="labelBody">' +
            '<div class="labelTxt">' +
            '65' +
            '</div>' +
            '</div>' +
            '<div class="labelArrow"></div>' +
            '</div>' +
            '<div class="vertical-bar-center-balloon container">' +
            '<div class="barContainer">' +
            '<div class="bar">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');

        this.container = $el.find(".vertical-bar-center-balloon");
        this.$wholeContainer = $el.find(".vertical-bar-center-balloon.container");
        this.$barlabel = this.container.find(".label");
        this.$barlabelTxt = this.$barlabel.find(".labelTxt");
        this.$barlabelArrow = this.$barlabel.find(".labelArrow");
        this.$barlabelBody = this.container.find(".labelBody");
        this.$barContainer = this.container.find(".barContainer");
        this.$bar = this.$barContainer.find(".bar");

        this.$bar.css({ "height": "0%" });
    }

    onLoadPage() {
        this.setColors();
        $(this.$bar).stop().animate({ "height": Math.round(this.getPercent()) + "%" }, 1000);
    }

    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {
        this.setColors();
        //$(this.$bar).stop().animate({"width":Math.round(this.getPercent())+"%"},1000);

        // Horizontal Bar (Battery )
        this.$bar.stop().animate({ "height": "0%" }, 0);
        this.$bar.stop().animate({ "height": Math.round(this.getPercent()) + "%" }, 1000);
        this.$barlabelTxt.html(this.value);


    }
    _dataRender() {
        this.setColors();
        // Horizontal Bar (Battery )
        this.$bar.stop().animate({ "height": "0%" }, 0);
        this.$bar.stop().animate({ "height": Math.round(this.getPercent()) + "%" }, 1000);
        this.$barlabelTxt.html(this.value);


    }

    _onCommitProperties() {
        super._onCommitProperties(); // for Numeric min-value-max

        if (this._invalidatePropertyScolor) {
            this.validateCallLater(this._validateSColorProp)
            this._invalidatePropertyScolor = false;
        }

        if (this._invalidatePropertyTcolor) {
            this.validateCallLater(this._validateTColorProp)
            this._invalidatePropertyTcolor = false;
        }

        if (this._invalidatePropertyTitleLabel) {
            this.validateCallLater(this._validateTitleLabelProp)
            this._invalidatePropertyTitleLabel = false;
        }


        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty)
        }
    }

    setColors() {
        let barBackground = this.s_color;
        this.$bar.css("background", barBackground);
        let barBackgroundLabelArrow = this.s_color;
        this.$barlabelBody.css("background", barBackgroundLabelArrow);
        this.$barlabelArrow.css("background", barBackgroundLabelArrow);
        this.$wholeContainer.css("borderBottom", "2px solid " + barBackgroundLabelArrow);

        this.$barContainer.css("background", "repeating-linear-gradient(-45deg,  rgba(0,0,0,0) 0px, rgba(0,0,0,0) 4px, " + this.t_color + "5px, " + this.t_color + " 6px)")

    }

    _validateSColorProp() {
        this.setColors();
    }


    _validateFontStyleProperty() {
        var fontProps = this.font;
        this.$barlabelTxt.css({
            "fontFamily": fontProps.font_type,
            "fontSize": fontProps.font_size,
            "fontWeight": fontProps.font_weight,
            "color": fontProps.font_color,
            "textAlign": fontProps.text_align,
            "lineHeight": fontProps.line_height + "px"
        });
    }


    _validateTColorProp() {
        this.setColors();
    }

    _validateTitleLabelProp() {
        this.$barlabel.html(this.titleLabel);
    }

    _validatePerProp() {
        let newPer = Math.round(this.getPercent()) + "%";
        this.$bar.stop().animate({ "height": "0%" }, 0);
        this.$bar.stop().animate({ "height": newPer }, 1000);
        this.$barlabelTxt.html(this.value);
    }

    _onDestroy() {
        this.containier = null;
        this.$bar = null;
        this.$barlabel = null;
        super._onDestroy();
    }

    get s_color() {
        return this.getGroupPropertyValue("setter", "s_color");

    }

    set s_color(color) {
        if (this._checkUpdateGroupPropertyValue("setter", "s_color", color)) {
            this._invalidatePropertyScolor = true;
        }
    }


    get t_color() {
        return this.getGroupPropertyValue("setter", "t_color");

    }

    set t_color(color) {
        if (this._checkUpdateGroupPropertyValue("setter", "t_color", color)) {
            this._invalidatePropertyTcolor = true;
        }
    }


    get titleLabel() {
        return this.getGroupPropertyValue("setter", "titleLabel");

    }

    set titleLabel(txt) {
        if (this._checkUpdateGroupPropertyValue("setter", "titleLabel", txt)) {
            this._invalidatePropertyTitleLabel = true;
        }
    }


    get font() {
        return this.getGroupProperties("font");
    }

}

WVPropertyManager.attach_default_component_infos(Numeric006, {
    "info": {
        "componentName": "Numeric006",
        "version": "1.0.0"
    },

    "setter": {
        "width": 50, // 기본 크기
        "height": 200, // 기본 크기
        "s_color": "#3399FF",
        "t_color": "#FFF",
        "value": 10,
        "min": 0,
        "max": 100
    },

    "toggle": {
        "selected": true,
        "selectedColor": "#0EB795",
        "normalColor": "#000000",
        "selectedLabel": "",
        "normalLabel": "",
        "titleLabel": ""
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#FFF",
        "font_size": 14,
        "font_weight": "bold",
        "line_height": 14,
        "text_align": "center"
    },


    "style": {
        "border": "0px solid #000000",

        "borderRadius": 1,
        "cursor": "default"

    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
Numeric006.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    }, {
        template: "cursor",
        label: "pos-size-2d"
    },
    {
        template: "label",
        label: "label"
    }, {
        template: "border",
        label: "border"
    }, {
        label: "Numeric",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "value",
            type: "number",
            label: "Value",
            show: true,
            writable: true,
            description: "value"
        }, {
            owner: "setter",
            name: "min",
            type: "number",
            label: "Min",
            show: true,
            writable: true,
            description: "min"
        }, {
            owner: "setter",
            name: "max",
            type: "number",
            label: "Max",
            show: true,
            writable: true,
            description: "max"
        }]
    }
];


WVPropertyManager.add_property_panel_group_info(Numeric006, {
    label: "Style",
    template: "vertical",
    children: [{
            owner: "setter",
            name: "s_color",
            type: "color",
            label: "color",
            show: true,
            writable: true,
            description: "Primary Color"
        },
        {
            owner: "setter",
            name: "t_color",
            type: "color",
            label: "stripe",
            show: true,
            writable: true,
            description: "Background "
        }
    ]
})


WVPropertyManager.add_property_group_info(Numeric006, {
    label: "Numeric006 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(Numeric006, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
