"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GridGaugeComponent2 = function(_NumericCoreComponent) {
    _inherits(GridGaugeComponent2, _NumericCoreComponent);

    function GridGaugeComponent2() {
        _classCallCheck(this, GridGaugeComponent2);

        return _possibleConstructorReturn(this, (GridGaugeComponent2.__proto__ || Object.getPrototypeOf(GridGaugeComponent2)).call(this));
    }

    _createClass(GridGaugeComponent2, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.canvas_controller = new CanvasController(this._element);
            this.canvas_controller.options.refresh = false;
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_dataRender",
        value: function _dataRender() {
            var self = this;
            this.canvas_controller.resize(this.width, this.height);
            var unit_width = this.canvas_controller.data_ctx.canvas.width / this.column - self.margin * 2;
            var unit_height = this.canvas_controller.data_ctx.canvas.height / this.row - self.margin * 2;
            var rate = this.column * this.row / 100;
            var calculated_percent = _get(GridGaugeComponent2.prototype.__proto__ || Object.getPrototypeOf(GridGaugeComponent2.prototype), "getPercent", this).call(this);
            var i = this.direction === "top" ? self.row - 1 : 0;
            var j = this.direction === "left" ? self.column - 1 : 0;
            var flag = true;
            self.canvas_controller.frame = [];

            var grd = this.canvas_controller.createGradient({
                type: 'linear',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: self.height,
                colors: ['rgba(65,197,213,1)', 'rgba(111,239,255,1)', 'rgba(65,197,213,1)']
            });

            var grd1 = this.canvas_controller.createGradient({
                type: 'linear',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: self.height,
                colors: ['rgba(97,109,116,1)', 'rgba(126,137,144,1)', 'rgba(97,109,116,1)']
            });

            while (flag) {
                var position = 0;
                switch (self.direction) {
                    case 'top':
                        position = Math.abs(i - self.row + 1) * self.column + j;
                        break;
                    case 'bottom':
                        position = i * self.column + j;
                        break;
                    case 'left':
                        position = Math.abs(j - self.column + 1) * self.row + i;
                        break;
                    case 'right':
                        position = j * self.row + i;
                        break;
                }
                position = position / rate;

                this.canvas_controller.frame.push({
                    shape: 'rectangle',
                    x: unit_width * j + self.margin * j * 2 + self.margin,
                    y: unit_height * i + self.margin * i * 2 + self.margin,
                    width: unit_width,
                    height: unit_height,
                    fill: grd1,
                    radius: { lt: self.radius, lb: self.radius, rt: self.radius, rb: self.radius },
                    frame: false
                });

                if (position < calculated_percent) {
                    var percent = 1;
                    if (Math.abs(position - calculated_percent) < 1 / rate && Math.abs(position - calculated_percent) > 0) {
                        percent = Math.abs(position - calculated_percent) / (1 / rate);
                    }
                    self.canvas_controller.frame.push({
                        shape: 'rectangle',
                        x: (self.direction === "left" ? unit_width * j + unit_width * (1 - percent) : unit_width * j) + self.margin * j * 2 + self.margin,
                        y: (self.direction === "top" ? unit_height * i + unit_height * (1 - percent) : unit_height * i) + self.margin * i * 2 + self.margin,
                        width: self.direction === "left" || self.direction === "right" ? unit_width * percent : unit_width,
                        height: self.direction === "top" || self.direction === "bottom" ? unit_height * percent : unit_height,
                        fill: grd,
                        radius: { lt: self.radius, lb: self.radius, rt: self.radius, rb: self.radius },
                        frame: true
                    });
                }

                switch (self.direction) {
                    case 'top':

                        j++;
                        if (j >= self.column) {
                            j = 0;
                            i--;
                        }
                        break;
                    case 'bottom':

                        j++;
                        if (j >= self.column) {
                            j = 0;
                            i++;
                        }
                        break;
                    case 'left':

                        i++;
                        if (i >= self.row) {
                            i = 0;
                            j--;
                        }
                        break;
                    case 'right':

                        i++;
                        if (i >= self.row) {
                            i = 0;
                            j++;
                        }
                        break;
                }

                if (!(i >= 0 && i < self.row && j >= 0 && j < self.column)) {
                    flag = false;
                }
            }

            self.canvas_controller.draw();
            self.canvas_controller.animate(function(d) {
                // animation 완료 후 발생할 함수 처리.
            });
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            _get(GridGaugeComponent2.prototype.__proto__ || Object.getPrototypeOf(GridGaugeComponent2.prototype), "_onCommitProperties", this).call(this);
            if (this._updatePropertiesMap.has('setter.width') || this._updatePropertiesMap.has('setter.height')) {
                this._dataRender();
            }
            if (this._updatePropertiesMap.has('extension')) {
                this._dataRender();
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.canvas_controller.dispose();
        }
    }, {
        key: "row",
        get: function get() {
            return this.getGroupPropertyValue("extension", "row");
        }
    }, {
        key: "column",
        get: function get() {
            return this.getGroupPropertyValue("extension", "column");
        }
    }, {
        key: "gaugeColor",
        get: function get() {
            return this.getGroupPropertyValue("extension", "gaugeColor");
        }
    }, {
        key: "gridColor",
        get: function get() {
            return this.getGroupPropertyValue("extension", "gridColor");
        }
    }, {
        key: "margin",
        get: function get() {
            return this.getGroupPropertyValue("extension", "margin");
        }
    }, {
        key: "radius",
        get: function get() {
            return this.getGroupPropertyValue("extension", "radius");
        }
    }, {
        key: "direction",
        get: function get() {
            return this.getGroupPropertyValue("extension", "direction");
        }
    }]);

    return GridGaugeComponent2;
}(NumericCoreComponent);

WVPropertyManager.attach_default_component_infos(GridGaugeComponent2, {
    "info": {
        "componentName": "GridGaugeComponent2",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "value": 10,
        "min": 0,
        "max": 100
    },
    "extension": {
        "row": 1,
        "column": 10,
        "gaugeColor": "rgba(221,77,136,1)",
        "gridColor": "rgba(221,77,136,0.5)",
        "direction": "right",
        "margin": 1,
        "radius": 0
    },
    "label": {
        "label_using": "N",
        "label_text": "Grid Gauge Component"
    },
    "style": {
        "border": "0px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 0
    }
});

GridGaugeComponent2.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "background"
}, {
    template: "label"
}, {
    label: "Numeric",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "value",
        type: "number",
        label: "Value",
        show: true,
        writable: true,
        description: "guage value"
    }, {
        owner: "setter",
        name: "min",
        type: "number",
        label: "Min",
        show: true,
        writable: true,
        description: "guage min"
    }, {
        owner: "setter",
        name: "max",
        type: "number",
        label: "Max",
        show: true,
        writable: true,
        description: "guage max"
    }]
}, {
    label: "Grid Guage",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "margin",
        type: "number",
        label: "Margin",
        show: true,
        writable: true,
        description: "grid margin"
    }, {
        owner: "extension",
        name: "radius",
        type: "number",
        label: "Radius",
        show: true,
        writable: true,
        description: "grid radius"
    }]
}];

WVPropertyManager.add_property_group_info(GridGaugeComponent2, {
    label: "GridGaugeComponent2 고유 속성",
    children: [{
        name: "value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min, max값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }]
});

WVPropertyManager.add_method_info(GridGaugeComponent2, {
    name: "getPercent",
    description: "min, max값을 기준으로 value를 백분율로 계산한 값입니다."
});
