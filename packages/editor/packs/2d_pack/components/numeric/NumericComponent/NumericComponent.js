class NumericComponent extends WVDOMComponent {

    constructor() {
        super();
    }

    _onCreateProperties() {
        this.containier = null;
        this.$bar = null;
        this.$barValue = null;
        this.$barValueArea = null;
        this.$barDirection = null;
        this._invalidateTextvalue = false;
        this._invalidateBarValue = false;
        this._invalidateArrow = false;
        this.__invalidateArrowValue = false;
        this.__invalidateMinvalue = false;
        this.__invalidateMaxvalue = false;
    }

    //element 생성
    _onCreateElement() {
        this.containier = document.createElement("div");
        this.containier.classList.add('numbericArea');
        this._element.appendChild(this.containier);

    }

    ///페이지 로드가 다 끝났을때
    onLoadPage() {
        this.initProperty();
    }

    initProperty() {
        this.createNumberBarArea();
        this.setBarValue();
        this.setBarStyle();
        this.setTitleDirection();
        this.setTitleValue();
        this.setTitleStyle();
    }

    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {
        this.initProperty();
    }

    createNumberBarArea() {

        $(this.containier).empty();

        var barDirection = document.createElement("div");
        barDirection.classList.add('barDeriection');
        barDirection.classList.add(this.direction);
        this.containier.appendChild(barDirection);

        let barValueArea = document.createElement("div");
        barValueArea.classList.add('barValueArea');
        barDirection.appendChild(barValueArea);

        let barValue = document.createElement("div");
        barValue.classList.add('barValue');
        barValueArea.appendChild(barValue);

        let bar = document.createElement("div");
        bar.classList.add('bar');
        barDirection.appendChild(bar);

        this.$bar = $(bar);
        this.$barValue = $(barValue);
        this.$barValueArea = $(barValueArea);
        this.$barDirection = $(barDirection);

    }

    setBarValue() {

        if (this.min_value != null && this.max_value != null) {

            let margin = this.bt_margin / $(this._element).height();

            this.$barDirection.get(0).style.height = (margin + ((this.bar_value - this.min_value) / (this.max_value - this.min_value))) * 100 + "%";
        }

        this.dispatcherWscript(this.bar_value);

    }

    setBarStyle() {

        this.$bar.css("background-color", this.bar_color)
            .css("border-radius", this.bar_radius + "px");

    }

    setTitleDirection() {

        let flag = this.direction == "barLeft" ? "barRight" : "barLeft";

        this.$barDirection.removeClass(flag).addClass(this.direction);

        if (this.direction == "barLeft") {
            this.$barValueArea
                .css("left", "")
                .css("right", this.$bar.width() + 10 + "px");
        } else {
            this.$barValueArea
                .css("right", "")
                .css("left", this.$bar.width() + 10 + "px");
        }

    }

    setTitleValue() {

        this.$barValue.html(this.title_value);

        if (this.direction == "barLeft") {
            this.$barValueArea.css("right", this.$bar.width() + 10 + "px");
        } else {
            this.$barValueArea.css("left", this.$bar.width() + 10 + "px");
        }

        this.dispatcherWscript(this.title_value);

    }

    setTitleStyle() {
        this.$barValueArea.css("background-color", this.title_BackColor);
        this.$barValue.css("color", this.title_FontColor);
    }

    dispatcherWscript(value) {
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                newValue: value
            });
        }
    }

    //properties 변경시
    _onCommitProperties() {

        if (this._updatePropertiesMap.has("extensionBarStyle")) {
            this.validateCallLater(this.setBarStyle);
        }

        if (this._updatePropertiesMap.has("extensionBarValue")) {
            this.validateCallLater(this.setBarValue);
        }

        if (this._updatePropertiesMap.has("extensionTitleValue.direction")) {
            this.validateCallLater(this.setTitleDirection);
        } else {
            this.validateCallLater(this.setTitleValue);
        }

        if (this._updatePropertiesMap.has("extensionTitleStyle")) {
            this.validateCallLater(this.setTitleStyle);
        }

        if (this._updatePropertiesMap.has("extensionArrowStyle")) {
            this.validateCallLater(this.setArrowValue);
        }

    }

    _onDestroy() {
        //삭제 처리
        this.containier = null;
        this.$bar = null;
        this.$barValue = null;
        this.$barValueArea = null;
        this.$barDirection = null;
        this._invalidateTextvalue = false;
        this._invalidateBarValue = false;
        this._invalidateArrow = false;
        this.__invalidateArrowValue = false;
        this.__invalidateMinvalue = false;
        this.__invalidateMaxvalue = false;

        super._onDestroy();
    }

    get bar_radius() {
        return this.getGroupPropertyValue("extensionBarStyle", "bar_radius");
    }

    get bar_color() {
        return this.getGroupPropertyValue("extensionBarStyle", "bar_color");
    }

    set bar_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionBarValue", "bar_value", value)) {
            this._invalidateBarValue = true;
        }
        this.setBarValue();
    }

    get bar_value() {
        return this.getGroupPropertyValue("extensionBarValue", "bar_value");
    }

    set min_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionBarValue", "min_value", value)) {
            this.__invalidateMinvalue = true;
        }
        this.setBarValue();
    }

    get min_value() {
        return this.getGroupPropertyValue("extensionBarValue", "min_value");
    }

    set max_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionBarValue", "max_value", value)) {
            this.__invalidateMaxvalue = true;
        }
        this.setBarValue();
    }

    get max_value() {
        return this.getGroupPropertyValue("extensionBarValue", "max_value");
    }

    get bt_margin() {
        return this.getGroupPropertyValue("extensionBarValue", "bt_margin");
    }

    set title_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionTitleValue", "title_value", value)) {
            this._invalidateTextvalue = true;
        }
        this.setTitleValue();
    }

    get title_value() {
        return this.getGroupPropertyValue("extensionTitleValue", "title_value");
    }

    get direction() {
        return this.getGroupPropertyValue("extensionTitleValue", "direction");
    }

    get title_BackColor() {
        return this.getGroupPropertyValue("extensionTitleStyle", "title_BackColor");
    }

    get title_FontColor() {
        return this.getGroupPropertyValue("extensionTitleStyle", "title_FontColor");
    }

}

WVPropertyManager.attach_default_component_infos(NumericComponent, {
    "info": {
        "componentName": "NumericComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 100,
        "height": 150,
        "value": ''
    },

    "label": {
        "label_using": "N",
        "label_text": "Input Component"
    },

    "style": {
        "border": "1px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 10
    },
    "extensionBarValue": {
        "bar_value": 60,
        "min_value": 0,
        "max_value": 100,
        "bt_margin": 0,
    },
    "extensionBarStyle": {
        "bar_color": "#5a3c6c",
        "bar_radius": 10
    },
    "extensionTitleValue": {
        "direction": "barRight",
        "title_value": "111,111"
    },
    "extensionTitleStyle": {
        "title_BackColor": "#4e5357",
        "title_FontColor": "#ffffff"
    }
});

NumericComponent.property_panel_info = [{
        template: "primary"
    }, {
        template: "pos-size-2d"
    }, { template: "background" }, {
        template: "label"
    }, {
        template: "border"
    },

    {
        label: "Bar Option",
        template: "vertical",
        children: [{
            owner: "extensionBarValue",
            name: "bar_value",
            type: "string",
            label: "Value",
            show: true,
            writable: true,
            description: "bar_value"
        }, {
            owner: "extensionBarValue",
            name: "min_value",
            type: "string",
            label: "Min",
            show: true,
            writable: true,
            description: "min_value"
        }, {
            owner: "extensionBarValue",
            name: "max_value",
            type: "string",
            label: "Max",
            show: true,
            writable: true,
            description: "max_value"
        }, {
            owner: "extensionBarStyle",
            name: "bar_color",
            type: "color",
            label: "Color",
            show: true,
            writable: true,
            description: "bar_color"
        }, {
            owner: "extensionBarStyle",
            name: "bar_radius",
            type: "string",
            label: "Radius",
            show: true,
            writable: true,
            description: "bar_radius"
        }, {
            owner: "extensionBarValue",
            name: "bt_margin",
            type: "string",
            label: "BT Margin",
            show: true,
            writable: true,
            description: "bt_margin"
        }],
    },
    {
        label: "Title Option",
        template: "vertical",
        children: [{
            owner: "extensionTitleValue",
            name: "title_value",
            type: "string",
            label: "Value",
            show: true,
            writable: true,
            description: "title_value"
        }, {
            owner: "extensionTitleStyle",
            name: "title_BackColor",
            type: "color",
            label: "Bg Color",
            show: true,
            writable: true,
            description: "title_BackColor"
        }, {
            owner: "extensionTitleStyle",
            name: "title_FontColor",
            type: "color",
            label: "Label Color",
            show: true,
            writable: true,
            description: "title_FontColor"
        }, {
            owner: "extensionTitleValue",
            name: "direction",
            label: "Direction",
            type: "select",
            options: {
                items: [
                    { label: "left", value: "barLeft" },
                    { label: "right", value: "barRight" }
                ]
            }
        }],
    }
]


WVPropertyManager.add_event(NumericComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.add_property_group_info(NumericComponent, {
    label: "NumericComponent 고유 속성",
    children: [{
        name: "bar_value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: 50,
        description: "value 입니다. min_value, max_value값을 기준으로 백분율로 표현됩니다."
    }, {
        name: "min_value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "0",
        description: "minimum값 입니다."
    }, {
        name: "max_value",
        type: "number",
        show: true,
        writable: true,
        defaultValue: "100",
        description: "maximum값 입니다."
    }, {
        name: "title_value",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'label'",
        description: "bar 옆에 위치하는 라벨 영역에 노출될 값입니다."
    }]
});
