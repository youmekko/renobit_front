'use strict';

var _createClass = function() {
      function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                  var descriptor = props[i];
                  descriptor.enumerable = descriptor.enumerable || false;
                  descriptor.configurable = true;
                  if ("value" in descriptor) descriptor.writable = true;
                  Object.defineProperty(target, descriptor.key, descriptor);
            }
      }
      return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
      if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
      subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
      if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var BasicGridComponent = function(_WVDOMComponent) {
      _inherits(BasicGridComponent, _WVDOMComponent);

      function BasicGridComponent() {
            _classCallCheck(this, BasicGridComponent);

            return _possibleConstructorReturn(this, (BasicGridComponent.__proto__ || Object.getPrototypeOf(BasicGridComponent)).call(this));
      }

      _createClass(BasicGridComponent, [{
            key: 'getExtensionProperties',
            value: function getExtensionProperties() {
                  return true;
            }
      }, {
            key: '_onCreateProperties',
            value: function _onCreateProperties() {
                  this.$el;
                  this.$tableWrap = null;
                  this._sortData = [];
                  this._dataProvider = [];
                  this._sortInfo = { key: '', state: '' };
                  this._datasetWorker = null;
                  this._invalidateSize = false;
                  this._invaldatePreivewProperty = false;

                  //extenstion setting..
                  this.settings = this.getGroupPropertyValue("extension", "settings");
                  this.previewData = this.getGroupPropertyValue("extension", "previewData");
                  this.gridStyle = this.getGroupPropertyValue("extension", "gridStyle");

                  this._styleStr = this._getStyleStr();
            }
      }, {
            key: '_onDestroy',
            value: function _onDestroy() {
                  if (this._datasetWorker) {
                        this._datasetWorker.clear();
                        this._datasetWorker = null;
                  }

                  if (this.$tableWrap) {
                        //viewer일때만..
                        this.$tableWrap.off("click.grid");
                        this.$tableWrap.remove();
                  }

                  this.$el.off("click.datagrid");
                  this.$el.off("dblclick.datagrid");

                  this.$tableWrap = null;
                  this._sortData = null;
                  this._dataProvider = null;
                  this._sortInfo = null;
                  this.settings = null;
                  this.previewData = null;
                  this.gridStyle = null;
                  this.$el = null;

                  _get(BasicGridComponent.prototype.__proto__ || Object.getPrototypeOf(BasicGridComponent.prototype), '_onDestroy', this).call(this);
            }
      }, {
            key: '_onCreateElement',
            value: function _onCreateElement() {
                  this.$el = $(this._element);
                  this.$el.attr("data-id", this.id);
                  if (!this.isEditorMode) {
                        this._setDataTableStyle();
                        this.bindItemEvent();
                  }
            }
      }, {
            key: 'bindItemEvent',
            value: function bindItemEvent() {
                  var settings = this.getGroupPropertyValue("extension", "settings");
                  if (this.isEditorMode || !settings) {
                        return;
                  }

                  var targetStr = void 0;
                  if (settings.layout == 'horizontal') {
                        targetStr = "tbody>tr";
                  } else {
                        targetStr = "tbody>tr>td";
                  }

                  var self = this;
                  this.$el.off("click.datagrid").on("click.datagrid", targetStr, function(e) {
                        var index = parseInt($(this).attr("data-index"));
                        var rowData = null;
                        if (!isNaN(index)) {
                              rowData = self.dataProvider[index];
                        }

                        self.dispatchWScriptEvent("itemClick", {
                              value: rowData
                        });
                  });

                  this.$el.off("dblclick.datagrid").on("dblclick.datagrid", targetStr, function(e) {
                        var index = parseInt($(this).attr("data-index"));
                        var rowData = null;
                        if (!isNaN(index)) {
                              rowData = self.dataProvider[index];
                        }

                        self.dispatchWScriptEvent("itemDoubleClick", {
                              value: rowData
                        });
                  });
            }
      }, {
            key: '_onImmediateUpdateDisplay',
            value: function _onImmediateUpdateDisplay() {
                  if (!this.isEditorMode) {
                        this._registerDefaultMouseEvent();
                  }

                  if (this.isEditorMode) {
                        //복사하여 붙여넣기시
                        this._updateEditTablePreview();
                  }
            }
      }, {
            key: 'onLoadPage',
            value: function onLoadPage() {
                  if (!this.isEditorMode && this.getGroupPropertyValue("setter", "autoExecute")) {
                        this.execute({});
                  }

                  if (this.isEditorMode) {
                        //저장된것을 불러드릴때
                        this._updateEditTablePreview();
                  }
            }
      }, {
            key: '_updateEditTablePreview',
            value: function _updateEditTablePreview() {
                  if (!this.isEditorMode || !this.getGroupPropertyValue("setter", "visible")) {
                        return;
                  }

                  var showPreview = this.getGroupPropertyValue("editor", "showPreview");

                  if (!showPreview) {
                        this._setEditModeIconStyle();
                  } else {
                        this._setDataTableStyle();

                        if (this.previewData && this.previewData.data) {
                              if (showPreview) {
                                    if (this.previewData && this.previewData.data) {
                                          this.dataProvider = this.previewData.data;
                                    } else {
                                          this._setEditModeNoDataStyle();
                                    }
                              }
                        } else {
                              this._setEditModeNoDataStyle();
                        }
                  }
            }

            ///edit모드에서 데이터 미리보기 실행시

      }, {
            key: '_setDataTableStyle',
            value: function _setDataTableStyle() {
                  if (!this.$el.hasClass("grid-basic-comp")) {
                        this.$el.addClass("grid-basic-comp");

                        this._removeIconPreviewStyle();
                        this._removeNodataStyle();

                        this.$el.prepend('<div class="grid-basic-comp-wrap"></div>');
                        this.$tableWrap = this.$el.find(".grid-basic-comp-wrap");
                  }
            }

            //edit모드에서 데이터 미리보기 하지 않고 icon으로 표기

      }, {
            key: '_setEditModeIconStyle',
            value: function _setEditModeIconStyle() {
                  if (!this.isEditorMode) {
                        return;
                  }

                  if (!this.$el.hasClass("grid-basic-comp-edit")) {
                        this.$el.addClass("grid-basic-comp-edit");

                        this._removeNodataStyle();
                        this._removeDataPreviewStyle();

                        this.$el.prepend('<span class="icon"></span>');
                        this.$tableWrap = null;
                  }
            }

            ///edit모드에서 데이터 미리보기 실행시 데이터가 없을경우

      }, {
            key: '_setEditModeNoDataStyle',
            value: function _setEditModeNoDataStyle() {
                  if (!this.isEditorMode) {
                        return;
                  }

                  if (!this.$el.hasClass("grid-basic-comp-edit-nodata")) {
                        this.$el.addClass("grid-basic-comp-edit-nodata");

                        this._removeIconPreviewStyle();
                        this._removeDataPreviewStyle();

                        this.$el.prepend('<span class="no-data">데이터가 없습니다.</span>');
                        this.$tableWrap = null;
                  }
            }
      }, {
            key: '_removeNodataStyle',
            value: function _removeNodataStyle() {
                  this.$el.removeClass("grid-basic-comp-edit-nodata");
                  this.$el.find(".no-data").remove();
            }
      }, {
            key: '_removeIconPreviewStyle',
            value: function _removeIconPreviewStyle() {
                  this.$el.removeClass("grid-basic-comp-edit");
                  this.$el.find(".icon").remove();
            }
      }, {
            key: '_removeDataPreviewStyle',
            value: function _removeDataPreviewStyle() {
                  this.$el.removeClass("grid-basic-comp");
                  this.$el.find(".grid-basic-comp-wrap").remove();
            }
      }, {
            key: '_registerDefaultMouseEvent',
            value: function _registerDefaultMouseEvent() {
                  // 뷰어 모드 일때만
                  if (this.isEditorMode == false) {
                        var self = this;
                        this.$el.off("click.grid").on("click.grid", ".sortable-head", function() {
                              var $target = $(this);
                              var key = $target.attr("data-key");

                              var state = "";
                              if ($target.hasClass("ascending")) {
                                    state = "descending";
                              } else if ($target.hasClass("descending")) {
                                    state = "";
                              } else {
                                    state = "ascending";
                              }

                              self._sortInfo = { key: key, state: state };
                              self.render(self.dataProvider);
                        });
                  }
            }
      }, {
            key: 'execute',
            value: function execute(param) {
                  if (!this.isEditorMode && this.datasetId) {

                        //데이터셋 실행
                        if (this._datasetWorker) {
                              this._datasetWorker.clear();
                        }
                        param = param || {};
                        var self = this;
                        var datasetWorker = this.page.dataService.callById(this.datasetId, { "param": param });
                        this._datasetWorker = datasetWorker;
                        if (datasetWorker && datasetWorker.item) {
                              datasetWorker.on("error", function(event) {
                                    console.warn("dataset call error", event);
                              });

                              datasetWorker.on("success", function(event) {
                                    self.dataProvider = event.rowData;
                              });
                        } else {
                              console.warn("dataset is not defined. component id : ", this.id);
                        }
                  }
            }
      }, {
            key: 'sort',
            value: function sort(data, sortData) {
                  if (!data || !sortData || !sortData.key || !sortData.state) {
                        return data;
                  }

                  data = data.concat();
                  var key = sortData.key;
                  var state = sortData.state;

                  var sortOrder = 1;
                  if (state == "descending") {
                        sortOrder = -1;
                  }

                  data.sort(function(a, b) {
                        var result = a[key] < b[key] ? -1 : a[key] > b[key] ? 1 : 0;
                        return result * sortOrder;
                  });

                  this._sortInfo = { key: key, state: state };
                  return data;
            }
      }, {
            key: 'getTranslateText',
            value: function getTranslateText(str) {
                  return wemb.localeManager.translatePrefixStr(str);
            }
      }, {
            key: '_drawVerticalLayout',
            value: function _drawVerticalLayout(settings, data) {
                  var $table = $('<table class="vtable"><tbody></tbody></table>');
                  var bodyStr = '';
                  var thWidth = settings.thFixedWidth || 'auto';

                  if (settings.useCount) {
                        bodyStr += '<tr ><th style="width:' + thWidth + '"><div class="column-wrap"><span class="column-txt">No.</span></div></th>';
                        for (var i = 0, len = data.length; i < len; i++) {
                              bodyStr += '<td style="text-align:center;">' + (i + 1) + '</td>';
                        }

                        bodyStr += "</tr>";
                  }

                  for (var _i = 0, _len = settings.keys.length; _i < _len; _i++) {
                        var key = settings.keys[_i];
                        bodyStr += '<tr>';

                        var columnHeight = settings.settings[key].width || 'auto';

                        if (settings.settings[key].sortable) {

                              bodyStr += '<th style="width:' + thWidth + ';height:' + columnHeight + ';" class="sortable-head" data-key="' + key + '"><div class="column-wrap">' + '<span class="column-txt">' + this.srtToHtmlStr(this.getTranslateText(settings.settings[key].label)) + '</span>' + '<span class="caret-wrapper">' + '<i class="sort-caret ascending el-icon-caret-top"></i>' + '<i class="sort-caret descending el-icon-caret-bottom"></i>' + '</span>'; + '</div></th>';
                        } else {
                              bodyStr += '<th style="width:' + thWidth + ';height:' + columnHeight + ';" data-key="' + key + '><div class="column-wrap"><span class="column-txt">' + this.srtToHtmlStr(this.getTranslateText(settings.settings[key].label)) + '</span></div></th>';
                        }

                        for (var j = 0, _len2 = data.length; j < _len2; j++) {
                              var rowData = data[j];
                              bodyStr += '<td data-index="' + j + '" style="height:' + columnHeight + ';"><div style="text-align:' + settings.settings[key].align + ';">' + rowData[key] + '</div></td>';
                        }

                        bodyStr += '</tr>';
                  }

                  $table.find("tbody").html(bodyStr);

                  if (!data || !data.length) {
                        var $th = $table.find("th");
                        var emptyEl = '<td style="border-bottom:none;" rowspan="' + $th.length + '"><div style="text-align:center;">데이터가 없습니다.</div></td>';
                        $th.eq(0).after(emptyEl);
                  }

                  if (this._sortInfo.state) {
                        $table.find("[data-key=" + this._sortInfo.key + "]").addClass(this._sortInfo.state);
                  }

                  this.$tableWrap.empty();
                  this.$tableWrap.append(this._styleStr);
                  this.$tableWrap.append($table);
                  this.$tableWrap.css("overflow", "auto");
            }
      }, {
            key: 'srtToHtmlStr',
            value: function srtToHtmlStr(html) {
                  /*var template = document.createElement('template');
				  html = html.trim(); // Never return a text node of whitespace as the result
				  template.innerHTML = html;
				  return template.content.firstChild;*/
                  var $div = $("<div></div>");
                  $div.html(html);
                  return $div.html();
            }
      }, {
            key: '_drawHorizontalLayout',
            value: function _drawHorizontalLayout(settings, data) {
                  var str = '<table class="htable">' + '<thead><tr>' + '</tr></thead>' + '<tbody>' + '</tbody>' + '</table>';

                  var $table = $(str);
                  var thStr = "";
                  if (settings.useCount) {
                        thStr += '<th style="width: 50px;"><div class="column-wrap"><span class="column-txt">No.</span></div></th>';
                  }

                  for (var i = 0, len = settings.keys.length; i < len; i++) {

                        var key = settings.keys[i];
                        var thWidth = settings.settings[key].width || "auto";
                        if (settings.settings[key].sortable) {
                              thStr += '<th style="width:' + thWidth + ';" class="sortable-head" data-key="' + key + '"><div class="column-wrap">' + '<span class="column-txt">' + this.srtToHtmlStr(this.getTranslateText(settings.settings[key].label)) + '</span>' + '<span class="caret-wrapper">' + '<i class="sort-caret ascending el-icon-caret-top"></i>' + '<i class="sort-caret descending el-icon-caret-bottom"></i>' + '</span>'; + '</div></th>';
                        } else {
                              thStr += '<th style="width:' + thWidth + '" data-key="' + key + '"><div class="column-wrap">' + '<span class="column-txt">' + this.srtToHtmlStr(this.getTranslateText(settings.settings[key].label)) + '</span>' + '</div></th>';
                        }
                  }

                  var bodyStr = '';
                  for (var _i2 = 0, _len3 = data.length; _i2 < _len3; _i2++) {
                        var rowData = data[_i2];

                        bodyStr += '<tr data-index="' + _i2 + '">';
                        if (settings.useCount) {
                              bodyStr += '<td><div style="text-align: center;">' + (_i2 + 1) + '</div></td>';
                        }

                        for (var j = 0, _len4 = settings.keys.length; j < _len4; j++) {
                              var _key = settings.keys[j];
                              bodyStr += '<td ><div style="text-align:' + settings.settings[_key].align + ';">' + rowData[_key] + '</div></td>';
                        }

                        bodyStr += '</tr>';
                  }

                  $table.find("thead>tr").html(thStr);
                  $table.find("tbody").html(bodyStr);

                  if (!data || !data.length) {
                        var $th = $table.find("th");
                        var emptyEl = '<tr><td colspan="' + $th.length + '"><div style="text-align: center;">데이터가 없습니다.</div></td></tr>';
                        $table.find("tbody").html(emptyEl);
                  }

                  if (this._sortInfo.state) {
                        $table.find("[data-key=" + this._sortInfo.key + "]").addClass(this._sortInfo.state);
                  }

                  this.$tableWrap.empty();
                  this.$tableWrap.append(this._styleStr);
                  this.$tableWrap.append($table);

                  $table.CongelarFilaColumna({ soloThead: true });
                  $(this._element).find('.fht-tbody').addClass("scrollbar-inner").scrollbar();

                  this.$tableWrap.css("overflow", "");
            }
      }, {
            key: 'render',
            value: function render(data) {
                  if (!data) {
                        return;
                  }

                  this._sortData = this.sort(data.concat(), { key: this._sortInfo.key, state: this._sortInfo.state });

                  if (!this.getGroupPropertyValue("setter", "visible")) {
                        return;
                  }
                  var settings = this.getGroupPropertyValue("extension", "settings");
                  if (!settings) {
                        return;
                  }
                  if (this.$tableWrap == null) {
                        this._setDataTableStyle();
                  }
                  if (settings.layout == 'horizontal') {
                        this._drawHorizontalLayout(settings, this._sortData);
                  } else {
                        this._drawVerticalLayout(settings, this._sortData);
                  }
            }
      }, {
            key: '_getStyleStr',
            value: function _getStyleStr() {
                  if (!this.gridStyle || !this.gridStyle.border) {
                        return '';
                  }

                  var gridStyle = this.gridStyle;
                  var header = gridStyle.header;
                  var body = gridStyle.body;
                  var sortIcon = gridStyle.sortIcon;
                  var border = gridStyle.border;
                  var shadow = gridStyle.shadow;

                  var insideBorderStr = border.inside.size + 'px ' + border.inside.style + ' ' + border.inside.color + ';';
                  var outsideBorderStr = border.outside.size + 'px ' + border.outside.style + ' ' + border.outside.color + ';';

                  var __style = '<style>' + '.grid-basic-comp[id="' + this.id + '"] .sort-caret{color:' + sortIcon.color + ';}' + '.grid-basic-comp[id="' + this.id + '"] .ascending .sort-caret.ascending,' + '.grid-basic-comp[id="' + this.id + '"] .descending .sort-caret.descending{color:' + sortIcon.activeColor + ';}' + '.grid-basic-comp[id="' + this.id + '"] table th,' + '.grid-basic-comp[id="' + this.id + '"] table td{border-left:' + insideBorderStr + 'border-bottom:' + insideBorderStr + '}';

                  if (this.settings.layout === 'horizontal') {
                        __style +=
                              '.grid-basic-comp[id="' + this.id + '"] table.htable tbody tr{background:' + this.getLabelBackgroundStyle(body.bgColor1) + '; color:' + body.fontColor1 + ';}' +
                              '.grid-basic-comp[id="' + this.id + '"] table.htable tbody td {background-color: initial}' +
                              '.grid-basic-comp[id="' + this.id + '"] table.htable thead tr{ background:' + this.getLabelBackgroundStyle(header.bgColor) + ';color:' + header.fontColor + ';}' +
                              '.grid-basic-comp[id="' + this.id + '"] table.htable th { background-color: initial }';
                  } else {
                        __style +=
                              '.grid-basic-comp[id="' + this.id + '"] table td{background:' + this.getLabelBackgroundStyle(body.bgColor1) + '; color:' + body.fontColor1 + ';}' +
                              '.grid-basic-comp[id="' + this.id + '"] table th{ background:' + this.getLabelBackgroundStyle(header.bgColor) + ';color:' + header.fontColor + ';}';
                  }

                  if (body.striped) {
                        __style += '.grid-basic-comp[id="' + this.id + '"] table.htable tr:nth-child(even) {background:' + this.getLabelBackgroundStyle(body.bgColor2) + '; color:' + body.fontColor2 + ';}' + '.grid-basic-comp[id="' + this.id + '"] table.vtable tr>td:nth-child(odd){background:' + this.getLabelBackgroundStyle(body.bgColor2) + '; color:' + body.fontColor2 + ';}';
                  }

                  if (shadow) {
                        __style += ".grid-basic-comp[id=\"".concat(this.id, "\"] {box-shadow: ").concat(shadow.offsetX, "px ").concat(shadow.offsetY, "px ").concat(shadow.blur, "px ").concat(shadow.spread, "px ").concat(shadow.color, ";}");
                  }

                  __style += '.grid-basic-comp[id="' + this.id + '"] table th .column-txt { display:block; justify-content: ' + header.align + '; padding: ' + header.paddingTB + 'px ' + header.paddingRL + 'px; font-size: ' + header.fontSize + 'px; font-weight: ' + (header.bold ? "bold" : "normal") + ';}' + '.grid-basic-comp[id="' + this.id + '"] table td>div {padding: ' + body.paddingTB + 'px ' + body.paddingRL + 'px;' + 'font-size: ' + body.fontSize + 'px; font-weight: ' + (body.bold ? "bold" : "normal") + ';}' + '.grid-basic-comp[id="' + this.id + '"] .grid-basic-comp-wrap{border: ' + outsideBorderStr + ' border-radius:' + border.outside.radius + 'px;}' + '</style>';

                  return __style;
            }
      }, {
            key: 'getLabelBackgroundStyle',
            value: function getLabelBackgroundStyle(bgData) {
                  if (typeof bgData === 'string' && bgData) return bgData;

                  var TOOLTIP_DIRECTION_STYLE = {
                        left : 'linear-gradient( 90deg',
                        top : 'linear-gradient( 180deg',
                        diagonal1 : 'linear-gradient( -45deg',
                        diagonal2 : 'linear-gradient( 45deg',
                        radial : 'radial-gradient(ellipse at center'
                  }

                  bgData.bgType = bgData.bgType || 'solid';
                  bgData.bgColor = bgData.bgColor || 'rgba(0,0,0,0)';
                  bgData.bgGradientColor1 = bgData.bgGradientColor1 || bgData.bgColor;

                  if (bgData.bgType == 'solid') {
                        return "".concat(bgData.bgColor);
                  } else if (bgData.bgType == 'gradient') {
                        return "".concat(TOOLTIP_DIRECTION_STYLE[bgData.bgGradientDir], ", ").concat(bgData.bgGradientColor1, " 0%, ").concat(bgData.bgGradientColor2, " 100%)");
                  }
            }
      }, {
            key: '_onCommitProperties',
            value: function _onCommitProperties() {
                  if (this._updatePropertiesMap.has("extension")) {
                        this.validateCallLater(this._validateExtentionProperty);
                  }

                  if (this._updatePropertiesMap.has("editor.showPreview")) {
                        this.validateCallLater(this._validateSizeProperty);
                  }

                  if (this.invalidateSize || this.invalidateVisible) {
                        if (this.isEditorMode) {
                              this.validateCallLater(this._validateSizeProperty);
                        } else {
                              this.validateCallLater(this._validateVisibleProperty);
                        }
                  }
            }
      }, {
            key: '_validateExtentionProperty',
            value: function _validateExtentionProperty() {
                  this.settings = this.getGroupPropertyValue("extension", "settings");
                  this.previewData = this.getGroupPropertyValue("extension", "previewData");
                  this.gridStyle = this.getGroupPropertyValue("extension", "gridStyle");

                  this._styleStr = this._getStyleStr();
                  if (this.isEditorMode) {
                        this._updateEditTablePreview();
                  }
            }
      }, {
            key: '_validateVisibleProperty',
            value: function _validateVisibleProperty() {
                  //감춰졌다가 보이면
                  if (this.getGroupPropertyValue("setter", "visible")) {
                        this.render(this.dataProvider);
                  }
            }
      }, {
            key: '_validateSizeProperty',
            value: function _validateSizeProperty() {
                  var showPreview = this.getGroupPropertyValue("editor", "showPreview");
                  if (this.isEditorMode) {
                        this._updateEditTablePreview();
                  }
            }
      }, {
            key: 'datasetId',
            get: function get() {
                  return this.getGroupPropertyValue("extension", "datasetId");
            }
      }, {
            key: 'autoExecute',
            set: function set(bool) {
                  this._checkUpdateGroupPropertyValue("setter", "autoExecute", bool);
            }
      }, {
            key: 'dataProvider',
            set: function set(data) {
                  var oldValue = this.dataProvider;
                  this._dataProvider = data;

                  this.render(this.dataProvider);

                  if (!this.isEditorMode && data != oldValue) {
                        this.dispatchWScriptEvent("change", {
                              value: data
                        });
                  }
            },
            get: function get() {
                  return this._dataProvider;
            }
      }, {
            key: 'sortData',
            get: function get() {
                  return this._sortData;
            }
      }]);

      return BasicGridComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(BasicGridComponent, {
      "info": {
            "componentName": "BasicGridComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 700,
            "height": 200,
            "autoExecute": true
      },

      "editor": {
            "showPreview": false
      },

      "label": {
            "label_using": "N",
            "label_text": "Basic Grid Component"
      },

      "extension": {
            "settings": null,
            "datasetId": "",
            "gridStyle": {},
            "previewData": {}
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
BasicGridComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}, {
      label: "Preview",
      template: "vertical",
      children: [{
            owner: "editor",
            name: "showPreview",
            type: "checkbox",
            label: "Use",
            show: true,
            writable: true,
            description: "미리보기 설정",
            options: {
                  label: "사용"
            }
      }]
}, {
      label: "Auto Execute",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "autoExecute",
            type: "checkbox",
            label: "Use",
            show: true,
            writable: true,
            description: "자동실행 설정",
            options: {
                  label: "사용"
            }
      }]
}];

// 이벤트 정보
WVPropertyManager.add_event(BasicGridComponent, {
      name: "change",
      label: "값 체인지 이벤트",
      description: "값 체인지 이벤트 입니다.",
      properties: [{
            name: "value",
            type: "string",
            default: "",
            description: "새로운 값입니다."
      }]
});

WVPropertyManager.add_event(BasicGridComponent, {
      name: "itemClick",
      label: "item click 이벤트",
      description: "item click 이벤트 입니다.",
      properties: [{
            name: "value",
            type: "string",
            default: "",
            description: "click한 row data 값입니다."
      }]
});

WVPropertyManager.add_event(BasicGridComponent, {
      name: "itemDoubleClick",
      label: "item double click 이벤트",
      description: "item double click 이벤트 입니다.",
      properties: [{
            name: "value",
            type: "string",
            default: "",
            description: "double click한 row data 값입니다."
      }]
});

WVPropertyManager.add_property_group_info(BasicGridComponent, {
      label: "BasicGridComponent 고유 속성",
      children: [{
            name: "datasetId",
            type: "string",
            show: true,
            writable: false,
            description: "설정한 데이터셋의 고유 id입니다."
      }, {
            name: "dataProvider",
            type: "Array<any>",
            show: true,
            writable: true,
            description: "그리드를 구성할 데이터(row data)입니다."
      }, {
            name: "sortData",
            type: "Array<any>",
            show: true,
            writable: false,
            description: "사용자가 정렬한 그리드 데이터(row data)입니다."
      }]
});

WVPropertyManager.add_method_info(BasicGridComponent, {
      name: "render",
      description: "parameter로 전달된 row data를 그리드로 출력합니다.",
      params: [{
            name: "data",
            type: "Array<any>",
            description: "그리드를 구성할 데이터(row data) 입니다."
      }]
});

WVPropertyManager.add_method_info(BasicGridComponent, {
      name: "execute",
      description: "설정한 데이터셋을 호출하여 데이터 그리드를 업데이트합니다.",
      params: [{
            name: "params",
            type: "object",
            description: "설정한 데이터셋의 parameters입니다."
      }]
});
