"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var BgLayerLabelComponent = function(_WVDOMComponent) {
    _inherits(BgLayerLabelComponent, _WVDOMComponent);

    function BgLayerLabelComponent() {
        _classCallCheck(this, BgLayerLabelComponent);

        var _this = _possibleConstructorReturn(this, (BgLayerLabelComponent.__proto__ || Object.getPrototypeOf(BgLayerLabelComponent)).call(this));

        _this.$label;
        _this._invalidatePropertyTxt = false;
        return _this;
    }

    _createClass(BgLayerLabelComponent, [{
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            var str = '<div class="front-layer"><span class="txt-label"></span></div>';
            $(this._element).append(str);
            this.$label = $(this._element).find(".txt-label");
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.$label.remove();
            this.$label = null;
            _get(BgLayerLabelComponent.prototype.__proto__ || Object.getPrototypeOf(BgLayerLabelComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.updateStyle();
            this._validateTxtProperty();
            this._validateFontStyleProperty();
        }
    }, {
        key: "_onCreateProperties",
        value: function _onCreateProperties() {
            /*이전버전 호환처리*/
            if (this.getGroupPropertyValue("background", "type") == "") {
                this.setGroupPropertyValue("background", "type", "solid");
                var color = this.getGroupPropertyValue("style", "backgroundColor");
                if (color) {
                    this.setGroupPropertyValue("background", "color1", color);
                }
            }
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("background") || this._updatePropertiesMap.has("front_layer_background") || this._updatePropertiesMap.has("front_layer_style") || this._updatePropertiesMap.has("label_align")) {
                this.validateCallLater(this.updateStyle);
            }

            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this._validateFontStyleProperty);
            }

            if (this._invalidatePropertyTxt) {
                this.validateCallLater(this._validateTxtProperty);
                this._invalidatePropertyTxt = false;
            }
        }
    }, {
        key: "getBgStyleStr",
        value: function getBgStyleStr(groupName, selector) {
            var bg = this.getGroupProperties(groupName);
            var styleStr = this._styleManager.getBackgroundStyle(bg, selector);
            styleStr = styleStr.replace("<style>", "");
            styleStr = styleStr.replace("</style>", "");
            return styleStr;
        }
    }, {
        key: "_validateFontStyleProperty",
        value: function _validateFontStyleProperty() {
            this.$label.css({
                "fontFamily": this.font.font_type,
                "fontSize": this.font.font_size,
                "fontWeight": this.font.font_weight,
                "color": this.font.font_color,
                "textAlign": this.font.text_align
            });
        }
    }, {
        key: "updateStyle",
        value: function updateStyle() {
            var compBgStr = this.getBgStyleStr("background", "[id='" + this.id + "']");
            var frontBgStr = this.getBgStyleStr("front_layer_background", "[id='" + this.id + "']>.front-layer");
            var frontMargin = this.getGroupPropertyValue("front_layer_style", "margin");
            var frontRadius = this.getGroupPropertyValue("front_layer_style", "radius");
            var labelPadding = this.getGroupPropertyValue("label_align", "padding");
            var styleStr = "\n            <style>\n                " + compBgStr + "\n                " + frontBgStr + "\n\n                [id='" + this.id + "']>.front-layer{\n                    margin: " + frontMargin + ";\n                    border-radius: " + frontRadius + ";\n                }\n\n                [id='" + this.id + "']>.front-layer>.txt-label{\n                    padding: " + labelPadding + ";\n                }\n                \n            </style>\n        ";

            var $el = $(this._element);
            $el.find("style").remove();
            $el.append(styleStr);
        }
    }, {
        key: "_validateTxtProperty",
        value: function _validateTxtProperty() {
            var str = wemb.localeManager.translatePrefixStr(this.text);
            this.$label.html(str);
        }
    }, {
        key: "text",
        set: function set(txt) {
            if (this._checkUpdateGroupPropertyValue("setter", "text", txt)) {
                this._invalidatePropertyTxt = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "text");
        }
    }, {
        key: "font",
        get: function get() {
            return this.getGroupProperties("font");
        }
    }]);

    return BgLayerLabelComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(BgLayerLabelComponent, {
    "info": {
        "componentName": "BgLayerLabelComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 94,
        "height": 28,
        "text": "Label"
    },

    "label": {
        "label_using": "N",
        "label_text": "Bg Layer Label Component"
    },

    "style": {
        "border": "1px none #000000",
        "borderRadius": "12px",
        "cursor": "default"
    },

    "background": {
        "type": "gradient",
        "direction": "top", //top, left, diagonal1, diagonal2, radial
        "color1": "#BC547E",
        "color2": "#681369",
        "text": 'linear-gradient(to bottom, #C0547E 7%,#af4c7a 27%,#9e3d76 54%,#681369 100%)'
    },

    "front_layer_background": {
        "type": "solid",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#212222",
        "color2": "#000",
        "text": ''
    },

    "front_layer_style": {
        "radius": "11px",
        "margin": "2px"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#fff",
        "font_size": 13,
        "font_weight": "normal",
        "text_align": "center"
    },

    "label_align": {
        "padding": "0px"
    }
});

BgLayerLabelComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    template: "background-gradient",
    owner: "background",
    label: "Background"
}, {
    template: "border"
}, {
    template: "background-gradient",
    owner: "front_layer_background",
    label: "Front Layer Background"
}, {
    template: "border-radius",
    owner: "front_layer_style",
    name: "radius",
    label: "Front Layer Radius"
}, {
    label: "Front Layer margin",
    template: "padding",
    owner: "front_layer_style",
    name: "margin"
}, {
    label: "Text Label",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "text",
        type: "string",
        label: "Text",
        show: true,
        writable: true,
        description: "vertical align"
    }]
}, {
    template: "font",
    label: "Font"
}, {
    label: "Text Label Margin",
    template: "padding",
    owner: "label_align",
    name: "padding"
}];

WVPropertyManager.add_property_group_info(BgLayerLabelComponent, {
    label: "BgLayerLabelComponent 고유 속성",
    children: [{
        name: "text",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'text...'",
        description: "화면에 출력할 라벨 문자열입니다."
    }]
});
