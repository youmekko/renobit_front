class BgLayerLabelComponent extends WVDOMComponent {

    constructor() {
        super();
        this.$label;
        this._invalidatePropertyTxt = false;
    }

    onLoadPage() {}

    _onCreateElement() {
        let str = '<div class="front-layer"><span class="txt-label"></span></div>';
        $(this._element).append(str);
        this.$label = $(this._element).find(".txt-label");
    }

    _onDestroy() {
        this.$label.remove();
        this.$label = null;
        super._onDestroy();
    }


    _onImmediateUpdateDisplay() {
        this.updateStyle();
        this._validateTxtProperty();
        this._validateFontStyleProperty();
    }

    _onCreateProperties() {
        /*이전버전 호환처리*/
        if (this.getGroupPropertyValue("background", "type") == "") {
            this.setGroupPropertyValue("background", "type", "solid");
            let color = this.getGroupPropertyValue("style", "backgroundColor");
            if (color) {
                this.setGroupPropertyValue("background", "color1", color);
            }
        }
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("background") ||
            this._updatePropertiesMap.has("front_layer_background") ||
            this._updatePropertiesMap.has("front_layer_style") ||
            this._updatePropertiesMap.has("label_align")
        ) {
            this.validateCallLater(this.updateStyle);
        }

        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty)
        }

        if (this._invalidatePropertyTxt) {
            this.validateCallLater(this._validateTxtProperty);
            this._invalidatePropertyTxt = false;
        }
    }

    getBgStyleStr(groupName, selector) {
        let bg = this.getGroupProperties(groupName);
        let styleStr = this._styleManager.getBackgroundStyle(bg, selector);
        styleStr = styleStr.replace("<style>", "");
        styleStr = styleStr.replace("</style>", "");
        return styleStr;
    }

    _validateFontStyleProperty() {
        this.$label.css({
            "fontFamily": this.font.font_type,
            "fontSize": this.font.font_size,
            "fontWeight": this.font.font_weight,
            "color": this.font.font_color,
            "textAlign": this.font.text_align
        });
    }

    updateStyle() {
        let compBgStr = this.getBgStyleStr("background", `[id='${this.id}']`);
        let frontBgStr = this.getBgStyleStr("front_layer_background", `[id='${this.id}']>.front-layer`);
        let frontMargin = this.getGroupPropertyValue("front_layer_style", "margin");
        let frontRadius = this.getGroupPropertyValue("front_layer_style", "radius");
        let labelPadding = this.getGroupPropertyValue("label_align", "padding");
        let styleStr = `
            <style>
                ${compBgStr}
                ${frontBgStr}

                [id='${this.id}']>.front-layer{
                    margin: ${frontMargin};
                    border-radius: ${frontRadius};
                }

                [id='${this.id}']>.front-layer>.txt-label{
                    padding: ${labelPadding};
                }
                
            </style>
        `;

        let $el = $(this._element);
        $el.find("style").remove();
        $el.append(styleStr);
    }

    _validateTxtProperty() {
        let str = wemb.localeManager.translatePrefixStr(this.text);
        this.$label.html(str);
    }

    set text(txt) {
        if (this._checkUpdateGroupPropertyValue("setter", "text", txt)) {
            this._invalidatePropertyTxt = true;
        }
    }

    get text() {
        return this.getGroupPropertyValue("setter", "text");
    }

    get font() {
        return this.getGroupProperties("font");
    }

}

WVPropertyManager.attach_default_component_infos(BgLayerLabelComponent, {
    "info": {
        "componentName": "BgLayerLabelComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 94,
        "height": 28,
        "text": "Label"
    },

    "label": {
        "label_using": "N",
        "label_text": "Bg Layer Label Component"
    },

    "style": {
        "border": "1px none #000000",
        "borderRadius": "12px",
        "cursor": "default"
    },

    "background": {
        "type": "gradient",
        "direction": "top", //top, left, diagonal1, diagonal2, radial
        "color1": "#BC547E",
        "color2": "#681369",
        "text": 'linear-gradient(to bottom, #C0547E 7%,#af4c7a 27%,#9e3d76 54%,#681369 100%)'
    },

    "front_layer_background": {
        "type": "solid",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#212222",
        "color2": "#000",
        "text": ''
    },

    "front_layer_style": {
        "radius": "11px",
        "margin": "2px"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#fff",
        "font_size": 13,
        "font_weight": "normal",
        "text_align": "center"
    },


    "label_align": {
        "padding": "0px"
    }
});


BgLayerLabelComponent.property_panel_info = [{
        template: "primary"
    }, {
        template: "pos-size-2d"
    }, {
        template: "cursor"
    }, {
        template: "label"
    }, {
        template: "background-gradient",
        owner: "background",
        label: "Background"
    }, {
        template: "border"
    }, {
        template: "background-gradient",
        owner: "front_layer_background",
        label: "Front Layer Background"
    },
    {
        template: "border-radius",
        owner: "front_layer_style",
        name: "radius",
        label: "Front Layer Radius"
    }, {
        label: "Front Layer margin",
        template: "padding",
        owner: "front_layer_style",
        name: "margin"
    }, {
        label: "Text Label",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "text",
            type: "string",
            label: "Text",
            show: true,
            writable: true,
            description: "vertical align"
        }]
    }, {
        template: "font",
        label: "Font"
    }, {
        label: "Text Label Margin",
        template: "padding",
        owner: "label_align",
        name: "padding"
    }
];



WVPropertyManager.add_property_group_info(BgLayerLabelComponent, {
    label: "BgLayerLabelComponent 고유 속성",
    children: [{
        name: "text",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'text...'",
        description: "화면에 출력할 라벨 문자열입니다."
    }]
});
