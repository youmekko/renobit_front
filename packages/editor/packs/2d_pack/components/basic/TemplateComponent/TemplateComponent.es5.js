"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var TemplateComponent = function(_WVDOMComponent) {
    _inherits(TemplateComponent, _WVDOMComponent);

    function TemplateComponent() {
        _classCallCheck(this, TemplateComponent);

        var _this = _possibleConstructorReturn(this, (TemplateComponent.__proto__ || Object.getPrototypeOf(TemplateComponent)).call(this));

        _this.$el;
        _this.$wrap;
        _this.targetSelector;
        _this._dataProvider = null;
        _this.isCreated = false;
        _this.timer;
        return _this;
    }

    _createClass(TemplateComponent, [{
        key: "getExtensionProperties",
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.$el = $(this._element);
            this.$el.attr("data-id", this.id).addClass("template-comp");
            this.$el.append('<div class="tmpl-wrap"></div>');
            this.$wrap = this.$el.find(".tmpl-wrap");
            this.targetSelector = "[data-id='" + this.id + "'] .tmpl-wrap";

            this.createTransform();

            ///////////////////////////////////////////////////////////////////////

            if (this.isEditorMode) {
                this.setNodateStyle();
            }
            this.isCreated = true;
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.render();
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            clearTimeout(this.timer);
            this.$el = null;
            this.$wrap.remove();
            this.$wrap = null;
            this._dataProvider = null;
            _get(TemplateComponent.prototype.__proto__ || Object.getPrototypeOf(TemplateComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "dispatchRegisterEvent",
        value: function dispatchRegisterEvent() {
            _get(TemplateComponent.prototype.__proto__ || Object.getPrototypeOf(TemplateComponent.prototype), "dispatchRegisterEvent", this).call(this);
            if (this.isEditorMode) {
                this.render();
            }
        }
    }, {
        key: "setNodateStyle",
        value: function setNodateStyle() {
            if (this.isEditorMode) {
                this.$el.addClass("no-data");
                this.$wrap.html("<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAOCAYAAABpcp9aAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MEZGNTdDREUyNTVEMTFFODg5QjlDQTk0Q0E0RTE2RjIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MEZGNTdDREYyNTVEMTFFODg5QjlDQTk0Q0E0RTE2RjIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowRkY1N0NEQzI1NUQxMUU4ODlCOUNBOTRDQTRFMTZGMiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowRkY1N0NERDI1NUQxMUU4ODlCOUNBOTRDQTRFMTZGMiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpTnVn0AAAH2SURBVHjaxFbLUcMwELU9uRMqQBw4x1RAaACcCnAHmBNHnCMn3AGmAw8NYCqIOXOIU0FMBWHfzHNmR2MlYJLwZnYUSav1vv1I8VerlfdTPL5+ZjI091dnKefeHhFzzF0K4ocX9DBae4dBrEg4ETgibUQia20sw5FIyTlSlx6ITCIy3EpAnBqyTOYioaULQh+StppkACMypnGjzhhKiyH19Fqo9EPuuxAx84mTgDjVlgfGu7bOFcZt9AVvHG/4O+S5QqRiAObUx9qSenM6A2Tcxzdn3G86AtcSgP4T7a/JBoimCAw9s2GMOJ/ZJSXDiM6geXxuTUV8ReyEZXWKbIlc8IOYH4ssFAGPDse0cc61rrJslN2GZAv4NeAGPnQpjpWOFILx14Z9jUKdWaooexx1GdWKfMUA3m6wXdMufH7AmYB19Q5WwihntLtSWP6y8ZoezbrtTEg/4PwLCAcS1UoErCZkVwmJFA3tqP//gGF2Zpyj3GJcKIF6FAoRw/QkbccLkZDXZ+GIyC4RsXds5AziOsjOd4ANbJTDMLoAW0sVH7pmZuKeDl+wrjM6NXI0cWr5tMbA8UQ3imXkiH6sbpTK8UJPrfW8Iwi5KtFElarWdZavv+m/EPsAN8kEJdax/5dyKVV/9QL+Cw22KDS8o/eBahdGvgUYAH0jt3P7TWSdAAAAAElFTkSuQmCC'/>");
            }
        }
    }, {
        key: "render",
        value: function render() {
            var data = this.getTemplateData();
            clearTimeout(this.timer);

            var self = this;
            this.timer = setTimeout(function() {
                self.$wrap.empty();
                if (!self.extension.tmpl) {
                    if (self.isEditorMode) {
                        self.setNodateStyle();
                    }
                    return;
                }

                self.$el.removeClass("no-data");
                try {
                    $.tmpl(self.extension.tmpl, data).appendTo(self.targetSelector);
                } catch (error) {
                    console.warn("template error", error);
                }

                var style = self.getInstanceStyle();
                self.$wrap.append('<style>' + style + '</style>');
            }, 5);
        }
    }, {
        key: "getTemplateData",
        value: function getTemplateData() {
            var data = { data: this.dataProvider };
            /*if (!this.dataProvider) {
                data = { data: {} };
            }*/
            return data;
        }
    }, {
        key: "getInstanceStyle",
        value: function getInstanceStyle() {
            //let trimStr = this.styleStr.replace(/\s/gi, "");
            var trimStr = this.extension.tmpl_style.trim();
            if (!trimStr) {
                return '';
            }

            var prefix = this.targetSelector + " ";
            trimStr = trimStr.replace(/}/gi, "} " + prefix);
            trimStr = prefix + trimStr;

            var lastIndex = trimStr.lastIndexOf(prefix);
            trimStr = trimStr.substr(0, lastIndex);

            return trimStr;
        }
    }, {
        key: "getTestData",
        value: function getTestData() {
            var trimDataStr = this.extension.tmpl_test_data.trim();

            if (!trimDataStr) {
                trimDataStr = "{}";
            }

            var str = '{"data" :' + trimDataStr + "}";
            var json = JSON.parse(str);
            return json.data;
        }
    }, {
        key: "show",
        value: function show() {
            this.$el.show();
        }
    }, {
        key: "hide",
        value: function hide() {
            this.$el.hide();
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension") && this.isCreated) {
                this.validateCallLater(this._validateSetterProperty);
            }
        }
    }, {
        key: "_validateSetterProperty",
        value: function _validateSetterProperty() {
            this.render();
        }
    }, {
        key: "dataProvider",
        set: function set(data) {
            //if (this._sortData.length) { return; }
            var oldValue = this.dataProvider;
            this._dataProvider = data;

            var self = this;
            if (!this.isEditorMode && data != oldValue) {
                this.dispatchWScriptEvent("change", {
                    value: data
                });
                self.render();
            }
        },
        get: function get() {
            if (this.isEditorMode) {
                return this.getTestData();
            }

            return this._dataProvider;
        }
    }, {
        key: "extension",
        get: function get() {
            return this.getGroupProperties("extension");
        }
    }]);

    return TemplateComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(TemplateComponent, {
    "info": {
        "componentName": "TemplateComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 100,
        "height": 100
    },

    "label": {
        "label_using": "N",
        "label_text": "Template Component"
    },
    "extension": {
        "tmpl": "",
        "tmpl_style": "",
        "tmpl_test_data": ""
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
TemplateComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}];

// 이벤트 정보
WVPropertyManager.add_event(TemplateComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(TemplateComponent, {
    label: "TemplateComponent 고유 속성",
    children: [{
        name: "dataProvider",
        type: "any",
        show: true,
        writable: true,
        description: "template data입니다."
    }]
});

//  추후 추가 예정
WVPropertyManager.add_method_info(TemplateComponent, {
    name: "render",
    description: "저장된 dataProvider를 작성된 template포맷에 맞추어 화면에 출력합니다."
});
