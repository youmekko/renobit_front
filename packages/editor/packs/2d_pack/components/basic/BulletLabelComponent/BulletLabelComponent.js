class BulletLabelComponent extends WVDOMComponent {
    constructor() {
        super();
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateElement() {
        var str = '<ul class="bullet-label-component">' +
            '<li class="bullet-label-list" v-for="(item,index) in compSetter.items">' +
            '<div class="bullet" :style="\'margin-right:\'+bulletStyle.gapBullet + \'px;width:\'+bulletStyle.bulletSize+\'px;height:\'+bulletStyle.bulletSize+\'px;\'">' +
            '<div class="bullet-shape-wrap">' +
            '<div v-if="bulletStyle.bullet == \'arrow-up\'" class="bullet-shape" :class="bulletStyle.bullet" :style="\'border-bottom-color:\'+item.color"></div>' +
            '<div v-else class="bullet-shape" :class="bulletStyle.bullet" :style="\'background-color:\'+item.color"></div>' +
            '</div>' +
            '</div>' +
            '<div class="label" :style="\'margin-right:\'+bulletStyle.gapItem + \'px;\'">' +
            '<div class="txt">{{getTranslateItemName(item.label)}}' +
            '</div>' +
            '</div>' +
            '</ul>';

        let $el = $(this._element);
        let temp = $(str);
        $el.append(temp);

        var self = this;
        var app = new Vue({
            el: temp.get(0),
            data: function() {
                return {
                    bulletStyle: self.getGroupProperties("bullet_style"),
                    compSetter: self.getGroupProperties("setter")
                }
            },

            methods: {
                getTranslateItemName(str) {
                    return wemb.localeManager.translatePrefixStr(str);
                }
            }

        })
    }

    _onDestroy() {
        super._onDestroy();
    }

    _onImmediateUpdateDisplay() {
        this._validateFontStyleProperty();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty);
        }
    }

    _validateFontStyleProperty() {
        var fontProps = this.getGroupProperties("font");
        $(this._element).find(".label .txt").css({
            "fontFamily": fontProps.font_type,
            "fontSize": fontProps.font_size,
            "fontWeight": fontProps.font_weight,
            "color": fontProps.font_color
        });
    }


    get items() {
        return this.getGroupPropertyValue("setter", "items");
    }

    set items(items) {
        this._checkUpdateGroupPropertyValue("setter", "items", items);
    }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(BulletLabelComponent, {
    "info": {
        "componentName": "BulletLabelComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 17,
        "items": [{ "label": "item01", "color": "#000000" }]
    },

    "bullet_style": {
        "bullet": "circle",
        "gapBullet": 10,
        "gapItem": 10,
        "bulletSize": 10
    },


    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },

    "style": {
        "backgroundColor": "rgba(255, 255, 255, 0)",
        "border": "1px none #000000",
        "borderRadius": "0",
        "cursor": "default"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
BulletLabelComponent.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    }, {
        template: "font",
        label: "font"
    },
    {
        label: "Style",
        template: "vertical",
        children: [{
            owner: "bullet_style",
            name: "gapItem",
            type: "number",
            label: "Gap Item",
            show: true,
            writable: true,
            description: "gapItem"
        }, {
            owner: "bullet_style",
            name: "gapBullet",
            type: "number",
            label: "Gap Bullet",
            show: true,
            writable: true,
            description: "gapBullet"
        }, {
            owner: "bullet_style",
            name: "bulletSize",
            type: "number",
            label: "Bullet Size",
            show: true,
            writable: true,
            description: "Bullet Size"
        }]
    }
];


WVPropertyManager.add_property_group_info(BulletLabelComponent, {
    label: "BulletLabelComponent 고유 속성",
    children: [{
        name: "items",
        type: "object",
        show: true,
        writable: true,
        defaultValue: "[{label: '요소1', color: '#ff6600'}, {label: '요소2', color: 'rgba(255, 101, 0, 0.5)'}]",
        description: "bullet구성 요소입니다. \nex) this.items = [{label: '요소1', color: '#ff6600'}, {label: '요소2', color: 'rgba(255, 101, 0, 0.5)'}]"
    }]
});


WVPropertyManager.remove_property_group_info(BulletLabelComponent, "label");
WVPropertyManager.remove_property_group_info(BulletLabelComponent, "background");

// 이벤트 정보
WVPropertyManager.remove_event(BulletLabelComponent, "dblclick");
WVPropertyManager.remove_event(BulletLabelComponent, "click");