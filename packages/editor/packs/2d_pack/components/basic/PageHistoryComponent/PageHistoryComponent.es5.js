"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PageHistoryComponent = function (_WVDOMComponent) {
      _inherits(PageHistoryComponent, _WVDOMComponent);

      function PageHistoryComponent() {
            _classCallCheck(this, PageHistoryComponent);

            var _this = _possibleConstructorReturn(this, (PageHistoryComponent.__proto__ || Object.getPrototypeOf(PageHistoryComponent)).call(this));

            _this.pageInfoList = [{
                  name: "test1"
            }, {
                  name: "test2"
            }, {
                  name: "test3"
            }];

            _this._app = null;
            return _this;
      }

      _createClass(PageHistoryComponent, [{
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  /* 추후 추가 예정
                  <select @change="on_gotoPageAt($event)" v-model="currentIndex">
                  <option v-for="pageInfo in pageInfoList" :value="pageInfo.idx" >{{currentIndex}}{{pageInfo.idx}}{{pageInfo.name}}</option>
                  </select>
                  */
                  var str = '';
                  if (this.isEditorMode) {
                        str = "\n            <div class=\"page-history-btn-wrap\">\n                  <a :style=\"{'color':btnStyle.normalColor, 'border-color':btnStyle.normalColor}\"  class=\"prev\" nohref><i class=\"fa fa-arrow-left\"></i></a>\n                  <a :style=\"{'color':btnStyle.normalColor, 'border-color':btnStyle.normalColor}\" class=\"next\"  nohref><i class=\"fa fa-arrow-right\"></i></a>\n            </div>\n            ";
                  } else {
                        str = "\n            <div class=\"page-history-btn-wrap\">\n                  <a :style=\"{'color':btnStyle.normalColor, 'border-color':btnStyle.normalColor}\" :class=\"{'disabled':(isFirstHistory==true)}\" class=\"prev\" @click=\"on_gotoPrevPage\" nohref><i class=\"fa fa-arrow-left\"></i></a>\n                  <a :style=\"{'color':btnStyle.normalColor, 'border-color':btnStyle.normalColor}\"  :class=\"{'disabled':(isLastHistory==true)}\" class=\"next\" @click=\"on_gotoNextPage\" nohref><i class=\"fa fa-arrow-right\"></i></a>\n            </div>\n            ";
                  }

                  var self = this;
                  var $temp = $(str);
                  var app;
                  $(this.element).append($temp);
                  if (this.isEditorMode) {
                        app = new Vue({
                              el: $temp[0],
                              data: function data() {
                                    return {
                                          btnStyle: self.getGroupProperties("btnStyle")
                                    };
                              }
                        });
                  } else {
                        this._app = new Vue({
                              el: $temp[0],
                              data: function data() {
                                    return {
                                          pageInfoList: window.wemb.pageHistoryManager.pageInfoList,
                                          currentIndex: 0,
                                          btnStyle: self.getGroupProperties("btnStyle"),
                                          isFirstHistory: false,
                                          isLastHistory: false

                                    };
                              },


                              methods: {
                                    on_gotoPrevPage: function on_gotoPrevPage() {
                                          window.wemb.pageHistoryManager.gotoPrevPage();
                                    },
                                    on_gotoNextPage: function on_gotoNextPage() {
                                          window.wemb.pageHistoryManager.gotoNextPage();
                                    },
                                    on_gotoPageAt: function on_gotoPageAt(event) {
                                          window.wemb.pageHistoryManager.gotoPageAt(this.currentIndex);
                                    },
                                    updateHistoryInfo: function updateHistoryInfo() {
                                          if (window.wemb.pageHistoryManager) {
                                                this.currentIndex = window.wemb.pageHistoryManager.currentIndex;
                                                this.isFirstHistory = window.wemb.pageHistoryManager.isFirstHistory;
                                                this.isLastHistory = window.wemb.pageHistoryManager.isLastHistory;
                                          }
                                    }
                              }
                        });
                  }
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  if (this.isViewerMode == true) {
                        window.wemb.pageHistoryManager.removeCallback(this.name);
                        this._app.$destroy();
                        this._app = null;
                  }
                  _get(PageHistoryComponent.prototype.__proto__ || Object.getPrototypeOf(PageHistoryComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "onLoadPage",
            value: function onLoadPage() {
                  // editor모드에서는 실행되지 않음.
                  if (this.isEditorMode == true) return;
            }

            /*
            컴포넌트가 container에 추가되면 자동으로 호출
            */

      }, {
            key: "_onImmediateUpdateDisplay",
            value: function _onImmediateUpdateDisplay() {
                  var _this2 = this;

                  if (this.isViewerMode == true) {
                        window.wemb.pageHistoryManager.addUpdateCallback(this.name, function () {
                              _this2._app.updateHistoryInfo();
                        });
                        if (this._app) this._app.updateHistoryInfo();
                  }
            }
      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {}
      }]);

      return PageHistoryComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(PageHistoryComponent, {
      "info": {
            "version": "1.0.0",
            "componentName": "PageHistoryComponent"
      },

      "setter": {
            "width": 58,
            "height": 28
      },

      "style": {
            "overflow": "hidden"
      },

      "btnStyle": {
            "normalColor": "#c5c4be"
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
PageHistoryComponent["property_panel_info"] = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      label: "Button Color",
      template: "vertical",
      children: [{
            owner: "btnStyle",
            name: "normalColor",
            label: "Normal",
            type: "color",
            writable: true,
            show: true,
            description: "normal color"
      }]
}];
