"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var EventBrowserComponent = /*#__PURE__*/function (_WVDOMComponent) {
  _inherits(EventBrowserComponent, _WVDOMComponent);

  var _super = _createSuper(EventBrowserComponent);

  function EventBrowserComponent() {
    _classCallCheck(this, EventBrowserComponent);

    return _super.call(this);
  } // 확장 팝업 사용 여부


  _createClass(EventBrowserComponent, [{
    key: "getExtensionProperties",
    value: function getExtensionProperties() {
      return true;
    }
  }, {
    key: "_onCreateProperties",
    value: function _onCreateProperties() {
      // header
      this.header = [{
        key: "severity",
        label: "등급"
      }, {
        key: "host",
        label: "호스트명"
      }, {
        key: "type",
        label: "이벤트타입"
      }, {
        key: "stime",
        label: "발생시간"
      }, {
        key: "dupl",
        label: "중복"
      }, {
        key: "msg",
        label: "메세지"
      }];
      this.foldedHeight = 35;
      this.ackList = [];
      this.activeSeverity = [];
      this._sortInfo = {
        key: "",
        state: ""
      }; // slide String 값

      this.upStr = "▲";
      this.downStr = "▼"; // 에디터에서 보여질 preview DataProvider

      this.previewData = [{
        "type": "이벤트",
        "msg": "샘플 데이터 입니다.",
        "host": "test1",
        "severity": "major",
        "dupl": "N",
        "id": 1,
        "stime": "09:05:02"
      }, {
        "type": "이벤트",
        "msg": "샘플 데이터 입니다.",
        "host": "test2",
        "severity": "critical",
        "dupl": "N",
        "id": 2,
        "stime": "09:05:03"
      }, {
        "type": "이벤트",
        "msg": "샘플 데이터 입니다.",
        "host": "test3",
        "severity": "minor",
        "dupl": "N",
        "id": 3,
        "stime": "09:05:04"
      }, {
        "type": "이벤트",
        "msg": "샘플 데이터 입니다.",
        "host": "test4",
        "severity": "critical",
        "dupl": "N",
        "id": 4,
        "stime": "09:05:05"
      }];
    }
  }, {
    key: "_onDestroy",
    value: function _onDestroy() {
      if (this._dataProviderWorker) {
        this._dataProviderWorker.clear();

        this._dataProviderWorker = null;
      }

      if (this._countWorker) {
        this._countWorker.clear();

        this._countWorker = null;
      }

      _get(_getPrototypeOf(EventBrowserComponent.prototype), "_onDestroy", this).call(this);
    }
  }, {
    key: "_onCreateElement",
    value: function _onCreateElement() {
      $(this._element).addClass("event-browser");
      var $container = $("<div class='container'></div>");
      this.$toolArea = $("<div class='tool-area'></div>");
      this.$contentsArea = $("<div class='contents-area'></div>");
      $container.append(this.$toolArea, this.$contentsArea);
      $(this._element).append($container);

      this._validateProperty();

      this._setToolArea();

      this._setHeaderArea();

      this._setStyle();
    }
  }, {
    key: "_onImmediateUpdateDisplay",
    value: function _onImmediateUpdateDisplay() {
      if (this.isEditorMode) {
        this._setDataProvider();

        this._setSeverityCount();

        this._setStyle();
      } else {
        // 데이터셋 호출 (dataProvider, Count)
        this._callDataset();
      }
    }
  }, {
    key: "_setDatasetParam",
    value: function _setDatasetParam(paramInfo) {
      var _this = this;

      var obj = {};

      if (paramInfo) {
        paramInfo.map(function (value, index) {
          if (value.isDirect == 0) {
            obj[value.param_name] = value.default_value;
          } else if (value.isDirect == 1) {
            obj[value.param_name] = _this.activeSeverity.indexOf(value.isSeverityValue) > -1 ? 'Y' : 'N';
          } else if (value.isDirect == -1) {
            obj[value.param_name] = value.value;
          }
        });
      }

      return obj;
    }
  }, {
    key: "_callDataset",
    value: function _callDataset() {
      var config = this.getGroupPropertyValue("extension", "configInfo");

      if (!config) {
        this._setNoData();

        return;
      } else {
        this.executeDataProvider(config.dpInfo);
        this.executeCount(config.countInfo);
      }
    }
    /**
     * Toolbar 영역 엘리먼트 생성 및 이벤트 바인딩
     *
     *  ( title, auto버튼, slide 버튼, ack버튼, severity 버튼)
     * @private
     */

  }, {
    key: "_setToolArea",
    value: function _setToolArea() {
      var $titleArea = $("<div style='min-width: 400px;'></div>");
      var $title = $("<span class='title'>" + this.title + "</span>");
      var autoTitle = $("<span style='padding-left: 4px; padding-right: 3px;'>AUTO</span>");
      var $btnAuto = $("<input class='auto-skewed' id=" + this.name + " type='checkbox'><label class='btn-auto' data-off='OFF' data-on='ON' for=" + this.name + "></label>");
      var $btnSlide = $("<div class='btn btn-slide' mode='up'>" + this.downStr + "</div>");
      var $btnAck = $("<div class='btn btn-ack'>ACK</div>");
      var $btnAll = $("<div class='btn btn-all'>ALL</div>");
      $titleArea.append($title, autoTitle, $btnAuto, $btnSlide);
      var $severityArea = $("<div></div>");
      var severityEle = '';

      for (var i = 0; i < this.severity.length; i++) {
        severityEle += '<div class="severity" data-severity="' + this.severity[i] + '">' + '   <div class="bullet ' + this.severity[i] + '"></div>' + '   <span>-</span>' + '</div>';
      }

      $severityArea.append($btnAck, $btnAll, severityEle);
      this.$toolArea.append($titleArea, $severityArea);

      if (!this.isEditorMode) {
        this._btnBindEvent();

        if (this.getGroupPropertyValue("extension", "isFolded")) {
          this._btnSlideClick($btnSlide);
        }

        if (this.getGroupPropertyValue("extension", "isAuto")) {
          $btnAuto.trigger("click");
        }
      }
    }
    /**
     * 이벤트 브라우저 헤더 영역 엘리먼트 생성
     * @private
     */

  }, {
    key: "_setHeaderArea",
    value: function _setHeaderArea() {
      var $header = $("<div class='header'></div>");
      var self = this;

      for (var i = 0; i < this.header.length; i++) {
        var $headerItem = $("<div>" + this.header[i].label + "<i></i></div>");
        $headerItem.attr("data-key", this.header[i].key);
        $header.append($headerItem);

        if (!this.isEditorMode) {
          $headerItem.click(function (event) {
            self._headerClick(event);
          });
        }
      }

      this.$events = $("<div class='event-area'></div>");
      this.$events.addClass("scrollbar-inner");
      this.$contentsArea.append($header, this.$events);
    }
    /**
     * dataProvider 적용
     * @private
     */

  }, {
    key: "_setDataProvider",
    value: function _setDataProvider() {
      this.$events.find("div").remove();
      /*[ 데이터가 없을 경우 no data 표시 ]*/

      if (this.dataProvider.length < 1 && !this.isEditorMode) {
        this._setNoData();

        return;
      }
      /*[ auto 모드일 경우 이벤트 브라우저 패널 up ]*/


      if (this.isAutoMode) {
        var $btnSlide = this.$toolArea.find(".btn-slide");
        $btnSlide.attr("mode", "down");

        this._btnSlideClick($btnSlide);
      }
      /*[ severity 별로 데이터 filter ]*/


      var self = this;
      var provider = this.isEditorMode ? this.previewData : this.sort(this._sortInfo.key, this._sortInfo.state);
      provider.map(function (value, index) {
        if (index >= self.getGroupPropertyValue("extension", "maxRowCount")) {
          return;
        }

        var $contents = $("<div class='contents'></div>");
        $contents.attr({
          "data-seq": index,
          "data-id": value.id
        });

        if (self.ackList && self.ackList.indexOf(value.id + "") >= 0) {
          $contents.addClass("active");
        }

        for (var i = 0; i < self.header.length; i++) {
          var event = $('<div>' + value[self.header[i].key] + '</div>');
          event.attr(self.header[i].key, value[self.header[i].key]);
          $contents.append(event);
        }

        if (!self.isEditorMode) {
          $contents.click(function (event) {
            self._contentsClick(event);
          });
          $contents.dblclick(function (event) {
            self._contentsDoubleClick(event);
          });
        }

        self.$events.append($contents);
      });
      this.$events.scrollbar();
    }
    /**
     * severity 카운트 적용 메소드
     * @private
     */

  }, {
    key: "_setSeverityCount",
    value: function _setSeverityCount() {
      var self = this;
      this.severity.map(function (value) {
        var severityValue = self.count[value];
        self.$toolArea.find(".severity[data-severity=" + value + "] > span").text(severityValue);
      });
    }
    /**
     * 데이터 없을경우 메세지 표시 메소드
     * @private
     */

  }, {
    key: "_setNoData",
    value: function _setNoData() {
      this.$events.find("div").remove();
      this.$events.append($("<div class='no-data'>No data.</div>"));
    }
  }, {
    key: "_setStyle",
    value: function _setStyle() {
      this._validateHeaderStyle();

      this._validateHeaderBorder();

      this._validateContentBorder();

      this._validateHeaderFontStyle();

      this._validateContentStyle();

      this._validateContentBackgroundStyle();

      this._validateTitleColor();

      this._validateShadowProperty();
    }
    /**
     * 버튼 클릭 이벤트 바인드 메소드
     * (auto버튼, slide 버튼, ack버튼, severity 버튼)
     * @private
     */

  }, {
    key: "_btnBindEvent",
    value: function _btnBindEvent() {
      var $btnSlide = this.$toolArea.find(".btn-slide");
      var $btnAck = this.$toolArea.find(".btn-ack");
      var $btnAuto = this.$toolArea.find(".btn-auto");
      var $btnAll = this.$toolArea.find(".btn-all");
      var $severity = this.$toolArea.find(".severity");
      var self = this;
      $btnSlide.click(function (event) {
        self._btnSlideClick($btnSlide);
      });
      $btnAck.click(function (event) {
        self._btnAckClick();
      });
      $btnAuto.click(function (event) {
        self._btnAutoClick(event);
      });
      $severity.click(function (event) {
        self._btnSeverityClick(event);
      });
      $btnAll.click(function (event) {
        self._btnAllClick();
      });
    }
  }, {
    key: "changeUpDown",
    value: function changeUpDown(param) {
      var $btnSlide = this.$toolArea.find(".btn-slide");

      if (param === "up") {
        $btnSlide.attr('mode', 'up');
      } else if (param === "down") {
        $btnSlide.attr('mode', 'down');
      }

      this._btnSlideClick($btnSlide);
    }
  }, {
    key: "_btnSlideClick",
    value: function _btnSlideClick(target) {
      if (target.attr("mode") == 'up') {
        $(this._element).animate({
          "height": this.foldedHeight,
          "top": this.y + this.height - this.foldedHeight
        });
        target.text(this.upStr);
        target.attr("mode", "down");
      } else {
        $(this._element).animate({
          "height": this.height,
          "top": this.y
        });
        target.text(this.downStr);
        target.attr("mode", "up");
      }
    }
  }, {
    key: "_btnAckClick",
    value: function _btnAckClick() {
      this.procAckList(this.ackList);
    }
  }, {
    key: "_btnAllClick",
    value: function _btnAllClick() {
      if (this.isAll) {
        $(this._element).find(".contents").removeClass("active").addClass("active");
        this.ackList = this.dataProvider.concat();
      } else {
        $(this._element).find(".contents").removeClass("active");
        this.ackList = [];
      }

      this.isAll = !this.isAll;
      this.procAckList(this.ackList);
    }
  }, {
    key: "procAckList",
    value: function procAckList(list) {
      this.dispatchWScriptEvent("eventAck", {
        ackList: list.concat()
      });

      if (this._dataProviderWorker) {
        this._dataProviderWorker.pause();
      }

      if (this._countWorker) {
        this._countWorker.pause();
      }
    }
  }, {
    key: "_btnSeverityClick",
    value: function _btnSeverityClick(event) {
      var $severity = $(event.currentTarget);
      var value = $(event.currentTarget).attr("data-severity");

      if ($severity.hasClass("active")) {
        $severity.removeClass("active");
        this.activeSeverity.splice(this.activeSeverity.indexOf(value), 1);
      } else {
        $severity.addClass("active");
        this.activeSeverity.push(value);
      } //==========================================
      //  dataset Call
      //==========================================


      if (this._dataProviderWorker && this._dataProviderWorker.isActive) {
        var config = this.getGroupPropertyValue("extension", "configInfo");

        if (!config) {
          this._setNoData();

          return;
        } else {
          var info = this.activeSeverity.length == 0 ? config.dpInfo : config.severityInfo;
          this.executeDataProvider(info);
        }
      } //=========================================

      /*[ severity active 상태 이벤트 ]*/
      //=========================================


      this.dispatchWScriptEvent("severityClick", {
        item: this.activeSeverity
      });
    }
  }, {
    key: "_btnAutoClick",
    value: function _btnAutoClick(event) {
      this.isAutoMode = !this.isAutoMode;
    }
  }, {
    key: "sort",
    value: function sort(key, state) {
      var data = this.dataProvider.concat();

      if (state != "") {
        data.sort(function (a, b) {
          var aValue = a[key].toUpperCase();
          var bValue = b[key].toUpperCase();
          return aValue < bValue ? -1 : aValue > bValue ? 1 : 0;
        });

        if (state == "descending") {
          data.reverse();
        }
      }

      this._sortInfo = {
        key: key,
        state: state
      };
      return data;
    }
  }, {
    key: "_headerClick",
    value: function _headerClick(event) {
      var $target = $(event.currentTarget);
      var key = $target.attr("data-key");
      var state = "";

      if ($target.find("i").hasClass("ascending")) {
        state = "descending";
      } else if ($target.find("i").hasClass("descending")) {
        state = "";
      } else {
        state = "ascending";
      }

      $target.parent().find("i").removeClass("ascending descending");
      $target.find("i").addClass(state);
      this.dataProvider = this.sort(key, state);
    }
    /**
     * 컨텐츠 클릭 메소드
     *
     * ack 처리를 위해 선택 리스트 저장
     * @param event
     * @private
     */

  }, {
    key: "_contentsClick",
    value: function _contentsClick(event) {
      event.preventDefault();
      event.stopPropagation();
      var $target = $(event.currentTarget);
      var event_id = $target.attr("data-id");

      if ($target.hasClass("active")) {
        $target.removeClass("active");
        this.ackList.splice(this.ackList.indexOf(event_id), 1);
      } else {
        $target.addClass("active");
        this.ackList.push(event_id);
      }
    }
    /**
     * 컨텐츠 더블 클릭 메소드
     *
     * itemDoubleClick 으로 dispatch
     *
     * @param event
     * @private
     */

  }, {
    key: "_contentsDoubleClick",
    value: function _contentsDoubleClick(event) {
      var $target = $(event.currentTarget);
      var sequence = $target.attr("data-seq");
      var item = this.dataProvider[sequence]; //=========================================

      /*[ item 더블 클릭 이벤트 ]*/
      //=========================================

      this.dispatchWScriptEvent("itemDoubleClick", {
        item: item
      });
    }
  }, {
    key: "_onCommitProperties",
    value: function _onCommitProperties() {
      if (this._invalidateDataProvider) {
        this.validateCallLater(this._validateDataProvider);
        this._invalidateDataProvider = false;
      }

      if (this._invalidateCount) {
        this.validateCallLater(this._validateCount);
        this._invalidateCount = false;
      }

      if (this._invalidateTitle) {
        this.validateCallLater(this._validateTitle);
        this._invalidateTitle = false;
      }

      if (this._updatePropertiesMap.has("headerStyle")) {
        this.validateCallLater(this._validateHeaderStyle);
      }

      if (this._updatePropertiesMap.has("contentStyle")) {
        this.validateCallLater(this._validateContentStyle);
      }

      if (this._updatePropertiesMap.has("shadow")) {
        this.validateCallLater(this._validateShadowProperty);
      }

      if (this._updatePropertiesMap.has("headerFont")) {
        this.validateCallLater(this._validateHeaderFontStyle);
      }

      if (this._updatePropertiesMap.has("contentBackground")) {
        this.validateCallLater(this._validateContentBackgroundStyle);
      }

      if (this._updatePropertiesMap.has("extension")) {
        this.validateCallLater(this._validateTitleColor);
      }
    }
  }, {
    key: "_validateDataProvider",
    value: function _validateDataProvider() {
      this._setDataProvider();
    }
  }, {
    key: "_validateCount",
    value: function _validateCount() {
      this._setSeverityCount();
    }
    /**
     * 타이틀 적용 메소드
     * @private
     */

  }, {
    key: "_validateTitle",
    value: function _validateTitle() {
      var titleEle = this.$toolArea.find(".title");
      $(titleEle).text(this.title);
    }
    /**
     * 이벤트 브라우저 헤더 스타일 적용 메소드
     * @private
     */

  }, {
    key: "_validateHeaderStyle",
    value: function _validateHeaderStyle() {
      var headerStyle = this.getGroupProperties("headerStyle");

      if (headerStyle.type === 'solid') {
        this.$contentsArea.find(".header").css({
          "background": "none",
          "background-color": headerStyle.color1
        });
      } else if (headerStyle.type === 'gradient') {
        this.$contentsArea.find(".header").css({
          "background": this.getGradientDirection(headerStyle)
        });
      }
    }
    /**
     * 이벤트 브라우저 컨텐츠 스타일 적용 메소드
     * @private
     */

  }, {
    key: "_validateContentStyle",
    value: function _validateContentStyle() {
      var contentStyle = this.getGroupProperties("contentStyle");
      var style = $('<style>' + '[id="' + this.id + '"] .event-area > .contents {' + '       color: ' + contentStyle.color + '; ' + '       font-size: ' + contentStyle.fontSize + 'px;} ' + '[id="' + this.id + '"] .event-area > .contents:hover { ' + '       color: ' + contentStyle.hoverFont + '} ' + '[id="' + this.id + '"] .event-area > .contents.active { ' + '       color: ' + contentStyle.activeFont + '} ' + '</style>');
      this.$contentsArea.append(style);
    }
  }, {
    key: "_validateHeaderBorder",
    value: function _validateHeaderBorder() {
      var headerBorderStyle = this.getGroupProperties("style");
      this.$contentsArea.find(".header").css({
        "border": "".concat(headerBorderStyle.border)
      });
    }
  }, {
    key: "_validateContentBorder",
    value: function _validateContentBorder() {
      var contentBorderStyle = this.getGroupProperties("contentBorder");
      var style = $('<style>' + '[id="' + this.id + '"] .event-area > .contents { ' + '       border: ' + contentBorderStyle.border + '; } ' + '</style>');
      this.$contentsArea.append(style);
    }
  }, {
    key: "_validateShadowProperty",
    value: function _validateShadowProperty() {
      var shadow = this.getGroupProperties("shadow");
      this._element.style.boxShadow = "".concat(shadow.xPosition, "px ").concat(shadow.yPosition, "px ").concat(shadow.blur, "px ").concat(shadow.color);
    }
  }, {
    key: "_validateStyle",
    value: function _validateStyle() {
      this._styleManager.validateProperty();

      this._validateHeaderBorder();
    }
    /**
     * dataProvider 적용을 위한 데이터셋 콜 메소드
     * @param info
     */

  }, {
    key: "executeDataProvider",
    value: function executeDataProvider(info) {
      if (!info || !info.datasetId) {
        return;
      } // 현재 실행중인 dataSet 끄기


      var self = this;

      if (this._dataProviderWorker) {
        this._dataProviderWorker.clear();
      }

      this._dataProviderWorker = this.page.dataService.callById(info.datasetId, {
        param: this._setDatasetParam(info.param)
      });

      if (this._dataProviderWorker && this._dataProviderWorker.item) {
        this._dataProviderWorker.on("error", function (event) {
          console.warn("dataset call error", event);
        });

        this._dataProviderWorker.on("success", function (event) {
          self.dataProvider = event.rowData;
        });
      } else {
        console.warn("dataset is not defined component id : ", this.id);
      }
    }
    /**
     * count 적용을 위한 데이터셋 콜 메소드
     * @param info
     */

  }, {
    key: "executeCount",
    value: function executeCount(info) {
      if (!info || !info.datasetId) {
        return;
      }

      var self = this;

      if (this._countWorker) {
        this._countWorker.clear();
      }

      this._countWorker = this.page.dataService.callById(info.datasetId, {
        param: this._setDatasetParam(info.param)
      });

      if (this._countWorker && this._countWorker.item) {
        this._countWorker.on("error", function (event) {
          console.warn("dataset call error", event);
        });

        this._countWorker.on("success", function (event) {
          self.count = event.rowData[0];
        });
      } else {
        console.warn("dataset is not defined component id : ", this.id);
      }
    }
    /**
     * ack 처리후 반드시 실행 시켜주어야 하는 메소드
     *
     *  1. dataProvider, count 데이터셋 start
     *  2. 초기화 처리
     */

  }, {
    key: "ackComplete",
    value: function ackComplete() {
      if (this._dataProviderWorker) {
        this._dataProviderWorker.start();
      }

      if (this._countWorker) {
        this._countWorker.start();
      }

      this.$events.find(".active").removeClass("active");
      this.ackList = [];
    }
  }, {
    key: "getGradientDirection",
    value: function getGradientDirection(gradient) {
      var GRADIENT_DIRECTION_STYLE = {
        left: 'linear-gradient( 90deg',
        top: 'linear-gradient( 180deg',
        diagonal1: 'linear-gradient( -45deg',
        diagonal2: 'linear-gradient( 45deg',
        radial: 'radial-gradient(ellipse at center'
      };
      return "".concat(GRADIENT_DIRECTION_STYLE[gradient.direction], ", ").concat(gradient.color1, ", ").concat(gradient.color2, " )");
    }
  }, {
    key: "_validateHeaderFontStyle",
    value: function _validateHeaderFontStyle() {
      var headerFontStyle = this.getGroupProperties("headerFont");
      this.$contentsArea.find(".header").css({
        "color": headerFontStyle.color,
        "fontSize": headerFontStyle.fontSize
      });
    }
  }, {
    key: "_validateContentBackgroundStyle",
    value: function _validateContentBackgroundStyle() {
      var contentBackgroundStyle = this.getGroupProperties("contentBackground");
      var bgStyle = "[id=\"".concat(this.id, "\"] .event-area > .contents");

      if (contentBackgroundStyle.type === 'solid') {
        bgStyle += "{background : none;background-color: ".concat(contentBackgroundStyle.color1, ";");
      } else if (contentBackgroundStyle.type === 'gradient') {
        bgStyle += "{background : ".concat(this.getGradientDirection(contentBackgroundStyle), ";");
      }

      var style = $('<style>' + '[id="' + this.id + '"] .event-area > .contents:hover { ' + ' background: none; background-color: ' + contentBackgroundStyle.hoverBg + '; } ' + '[id="' + this.id + '"] .event-area > .contents.active { ' + ' background: none; background-color: ' + contentBackgroundStyle.activeBg + '; } ' + bgStyle + ' </style>');
      this.$contentsArea.append(style);
    }
  }, {
    key: "_validateProperty",
    value: function _validateProperty() {
      var headerStyle = this.getGroupProperties('headerStyle');
      var contentStyle = this.getGroupProperties('contentStyle');

      if (headerStyle.bgColor) {
        this._call_setGroupPropertyValue('headerStyle', 'color1', headerStyle.bgColor);

        this._call_setGroupPropertyValue('headerStyle', 'bgColor', null);
      }

      if (contentStyle.bgColor) {
        this._call_setGroupPropertyValue('contentBackground', 'color1', contentStyle.bgColor);

        this._call_setGroupPropertyValue('contentBackground', 'activeBG', contentStyle.activeBg);

        this._call_setGroupPropertyValue('contentBackground', 'hoverBg', contentStyle.hoverBg);

        this._call_setGroupPropertyValue('contentStyle', 'bgColor', null);

        this._call_setGroupPropertyValue('contentStyle', 'activeBg', null);

        this._call_setGroupPropertyValue('contentStyle', 'hoverBg', null);
      }
    }
  }, {
    key: "_validateTitleColor",
    value: function _validateTitleColor() {
      var extension = this.getGroupProperties("extension");
      this.$toolArea.find('.title').css('color', extension.titleColor);
    }
  }, {
    key: "dataProvider",
    set: function set(value) {
      var change = JSON.stringify(this.dataProvider) !== JSON.stringify(value);

      if (this._checkUpdateGroupPropertyValue("setter", "dataProvider", value) && change) {
        this._invalidateDataProvider = true;
      }
    },
    get: function get() {
      return this.getGroupPropertyValue("setter", "dataProvider");
    }
  }, {
    key: "count",
    set: function set(value) {
      if (this._checkUpdateGroupPropertyValue("setter", "count", value)) {
        this._invalidateCount = true;
      }
    },
    get: function get() {
      return this.getGroupPropertyValue("setter", "count");
    }
  }, {
    key: "title",
    set: function set(value) {
      if (this._checkUpdateGroupPropertyValue("setter", "title", value)) {
        this._invalidateTitle = true;
      }
    },
    get: function get() {
      return this.getGroupPropertyValue("setter", "title");
    }
  }, {
    key: "severity",
    get: function get() {
      return this.getGroupPropertyValue("extension", "severity");
    }
  }]);

  return EventBrowserComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(EventBrowserComponent, {
  "info": {
    "componentName": "EventBrowserComponent",
    "version": "1.0.0"
  },
  "setter": {
    "width": 800,
    "height": 200,
    "dataProvider": "",
    "title": "이벤트 브라우저",
    "count": {}
  },
  "extension": {
    "maxRowCount": 100,
    "isAuto": false,
    "isFolded": false,
    "configInfo": null,
    "severity": ["critical", "major", "warning", "minor", "normal"]
  },
  "label": {
    "label_using": "N",
    "label_text": "EventBrowser Component"
  },
  "style": {
    "backgroundColor": "rgba(34, 38, 41, 0.88)",
    "cursor": "default",
    "border": "1px solid #000000"
  },
  "headerStyle": {
    "fontSize": 13,
    "type": "solid",
    "direction": "left",
    //top, left, diagonal1, diagonal2, radial
    "color1": "#fff",
    "color2": "#000",
    "text": ''
  },
  "headerFont": {
    "color": "#dad8db",
    "fontSize": 13
  },
  "contentStyle": {
    "fontSize": 12,
    "color": "#dad8db",
    "activeFont": "#ffffff",
    "hoverFont": "#dad8db"
  },
  "shadow": {
    "xPosition": '0',
    "yPosition": '0',
    "blur": '0',
    "color": 'rgb(255,255,255)'
  },
  "contentBorder": {
    "border": "1px none #000000",
    "backgroundColor": "rgba(34, 38, 41, 0.88)",
    "cursor": "default",
    "borderRadius": 0,
    "topLeftRadius": 0,
    "topRightRadius": 0,
    "bottomRightRadius": 0,
    "bottomLeftRadius": 0
  },
  "contentBackground": {
    "activeBg": "#645d67",
    "hoverBg": "#424243",
    "type": "solid",
    "direction": "left",
    //top, left, diagonal1, diagonal2, radial
    "color1": "#fff",
    "color2": "#000",
    "text": ''
  }
});
WVPropertyManager.add_event(EventBrowserComponent, {
  name: "eventAck",
  label: "eventAck 이벤트",
  description: "eventAck 이벤트 입니다.",
  properties: []
});
WVPropertyManager.add_event(EventBrowserComponent, {
  name: "itemDoubleClick",
  label: "itemDoubleClick 이벤트",
  description: "itemDoubleClick 이벤트 입니다.",
  properties: []
});
WVPropertyManager.add_event(EventBrowserComponent, {
  name: "severityClick",
  label: "severityClick 이벤트",
  description: "severityClick 이벤트 입니다.",
  properties: []
});
EventBrowserComponent.property_panel_info = [{
  template: "primary"
}, {
  template: "pos-size-2d"
}, {
  template: "label"
}, {
  template: "background"
}, {
  label: "config",
  owner: "vertical",
  children: [{
    owner: "setter",
    name: "title",
    type: "string",
    label: "Title",
    show: true,
    writable: true,
    description: "title"
  }, {
    owner: "extension",
    name: "titleColor",
    type: "color",
    label: "Title Color",
    show: true,
    writable: true,
    description: "titleColor"
  }, {
    owner: "extension",
    name: "maxRowCount",
    type: "number",
    label: "MaxRowCnt",
    show: true,
    writable: true,
    description: "maxRowCount"
  }, {
    owner: "extension",
    name: "isAuto",
    type: "checkbox",
    label: "Auto",
    show: true,
    writable: true,
    description: "isAuto"
  }, {
    owner: "extension",
    name: "isFolded",
    type: "checkbox",
    label: "Folded",
    show: true,
    writable: true,
    description: "isFolded"
  }]
}, {
  label: "Header Background Style",
  owner: "headerStyle",
  template: "background-gradient"
}, {
  label: "Header Font Style",
  owner: "vertical",
  children: [{
    owner: "headerFont",
    name: "fontSize",
    type: "number",
    label: "FontSize",
    show: true,
    writable: true,
    description: "fontSize"
  }, {
    owner: "headerFont",
    name: "color",
    type: "color",
    label: "Color",
    show: true,
    writable: true,
    description: "color"
  }]
}, {
  label: "Content Background Style",
  owner: "contentBackground",
  template: "table-background"
}, {
  label: "Content Style",
  owner: "vertical",
  children: [{
    owner: "contentStyle",
    name: "fontSize",
    type: "number",
    label: "FontSize",
    show: true,
    writable: true,
    description: "fontSize"
  }, {
    owner: "contentStyle",
    name: "color",
    type: "color",
    label: "Color",
    show: true,
    writable: true,
    description: "color"
  }, {
    owner: "contentStyle",
    name: "activeFont",
    type: "color",
    label: "ActiveFont",
    show: true,
    writable: true,
    description: "activeFont"
  }, {
    owner: "contentStyle",
    name: "hoverFont",
    type: "color",
    label: "HoverFont",
    show: true,
    writable: true,
    description: "hoverFont"
  }]
}, {
  label: "Shadow",
  template: "shadow"
}, {
  label: "Header Border",
  template: "border",
  owner: "style"
}, {
  label: "Content Border",
  template: "event-border",
  owner: "contentBorder"
}];
WVPropertyManager.add_property_group_info(EventBrowserComponent, {
  label: "EventBrowserComponent 고유 속성",
  children: [{
    name: "title",
    type: "string",
    show: true,
    writable: true,
    defaultValue: "이벤트 브라우저",
    description: "이벤트 브라우저 Title값 입니다."
  }, {
    name: "dataProvider",
    type: "array",
    show: true,
    writable: true,
    defaultValue: "[]",
    description: "이벤트 브라우저 데이터값 입니다."
  }, {
    name: "count",
    type: "object",
    show: true,
    writable: true,
    defaultValue: "{}",
    description: "severity 별로 count값을 나타내는 값입니다. (ex : {critical : 10, normal: 20}"
  }, {
    name: "activeSeverity",
    type: "object",
    show: true,
    writable: true,
    defaultValue: "{}",
    description: "현재 선택된 severity 값입니다."
  }]
});
WVPropertyManager.add_method_info(EventBrowserComponent, {
  name: "ackComplete",
  description: "이벤트 브라우저 ack 처리후, 반드시 실행 시켜주어야 하는 메소드입니다."
});