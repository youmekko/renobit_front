class TickerComponent extends WVDOMComponent {

    constructor() {
        super();
        this.translateText;
        this._invalidatePropertyTxt = true; //다국어 처리를 위해..
        this.textElement;
        this.$txt;
    }

    _onDestroy() {
        this.stop();
        this.$txt.remove();
        this.$txt = null;
        this.textElement = null;
        super._onDestroy();
    }

    _onCreateElement() {
        if (!this.textElement) {
            this.textElement = document.createElement("marquee");
            this.$txt = $(this.textElement);
            this.$txt.attr("truespeed", true);

            this._element.appendChild(this.textElement);
        }
    }

    _onImmediateUpdateDisplay() {
        this._updateFontProperty();
        this._updateTickerOption();
        this._updateTextValue();
    }

    _onCommitProperties() {
        if (this._invalidatePropertyTxt) {
            this.validateCallLater(this._updateTextValue);
            this._invalidatePropertyTxt = false;
        }

        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._updateFontProperty);
        }

        if (this._updatePropertiesMap.has("option")) {
            this.validateCallLater(this._updateTickerOption);
        }
    }

    /**
     * 티커 컴포넌트 폰트 프로퍼티 업데이트 메소드
     * @private
     */
    _updateFontProperty() {
        var fontProps = this.getGroupProperties("font");

        this.$txt.css({
            "fontFamily": fontProps.font_type,
            "fontSize": fontProps.font_size,
            "fontWeight": fontProps.font_weight,
            "color": fontProps.font_color
        });
    }

    /**
     * 티커 컴포넌트 옵션 적용 메소드 ( delay, direction, loop )
     * 옵션 적용을 위해 애니메이션 stop 후 start
     * @private
     */
    _updateTickerOption() {
        var option = this.getGroupProperties("option");

        this.$txt.attr("scrollDelay", option.delay)
            .attr("behavior", option.behavior)
            .attr("direction", option.direction);

        this.stop();
        this.start();
    }

    /**
     * 티커 컴포넌트 text 값 setting
     * @private
     */
    _updateTextValue() {
        if (this.textElement) {
            this.translateText = wemb.localeManager.translatePrefixStr(this.getGroupPropertyValue("setter", "text"));
            this.textElement.innerHTML = this.translateText;
        }
    }

    /**
     * Ticker Component 애니메이션 start
     */
    start() {
        if (this.textElement) {
            this.textElement.start();
        }
    }

    /**
     * Ticker Component 애니메이션 stop
     */
    stop() {
        if (this.textElement) {
            this.textElement.stop();
        }
    }

    set text(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "text", value)) {
            this._invalidatePropertyTxt = true;
        }
    }

    get text() {
        return this.getGroupPropertyValue("setter", "text");
    }
}

WVPropertyManager.attach_default_component_infos(TickerComponent, {
    "info": {
        "componentName": "TickerComponent",
        "category": "2D",
        "version": "1.0.0"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#000000",
        "font_size": 12,
        "font_weight": "normal",
    },
    "option": {
        "direction": "right",
        "delay": 85,
        "behavior": "scroll"
    },

    "setter": {
        "width": 500,
        "height": 100,
        "text": "Ticker Component"
    },

    "style": {
        "border": "1px none #000000",
        "backgroundColor": "rgba(0, 0, 0, 0.2)",
        "borderRadius": 1,
        "cursor": "default"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
TickerComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "background"
}, {
    template: "border"
}, {
    template: "font"
}, {
    label: "Ticker Option",
    children: [{
        owner: "option",
        name: "direction",
        label: "Direction",
        type: "select",
        options: {
            items: [
                { label: "right to left", value: "left" },
                { label: "left to right", value: "right" },
                { label: "bottom to top", value: "up" },
                { label: "top to bottom", value: "down" }
            ]
        },
        writable: true,
        show: true,
        description: "direction"
    }, {
        owner: "option",
        name: "behavior",
        label: "Behavior",
        type: "select",
        options: {
            items: [
                { label: "scroll", value: "scroll" },
                { label: "alternate", value: "alternate" }
            ]
        },
        writable: true,
        show: true,
        description: "behavior"
    }, {
        owner: "option",
        name: "delay",
        label: "Delay",
        type: "number",
        writable: true,
        show: true,
        description: "delay"
    }, {
        owner: "setter",
        name: "text",
        label: "Text",
        type: "string",
        writable: true,
        show: true,
        description: "text"
    }]
}];

// 이벤트 정보
WVPropertyManager.add_event(TickerComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(TickerComponent, {
    label: "TickerComponent 고유 속성",
    children: [{
        name: "text",
        type: "string",
        show: true,
        writable: true,
        description: "화면에 출력할 문자열 데이터입니다."
    }]
});

WVPropertyManager.add_method_info(TickerComponent, {
    name: "start",
    description: "ticker Animation을 실행합니다."
});

WVPropertyManager.add_method_info(TickerComponent, {
    name: "stop",
    description: "ticker Animation을 실행을 멈춥니다."
});
