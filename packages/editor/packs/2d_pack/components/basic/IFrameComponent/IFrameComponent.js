class IFrameComponent extends WVDOMComponent {
    constructor() {
        super();
        this.invalidateLinkUrl = false;
        this.invalidateFrameScroll = false;

    }

    _onCreateElement() {
        this._element.classList.add("iframe-component");

        if (this.isViewerMode == true) {
            this._iframe = document.createElement("iframe");
            this._iframe.style.width = "100%";
            this._iframe.style.height = "100%";
            this._element.appendChild(this._iframe);
        } else {
            $(this._element).addClass("isEditor");
        }
    }

    _onDestroy() {
        if (this.isEditorMode == false && this._iframe) {
            $(this._iframe).off();
            this._element.removeChild(this._iframe);
            this._iframe = null;
        }

        super._onDestroy();
    }

    _onImmediateUpdateDisplay() {
        if (this.isViewerMode == true) {
            this._validateLinkUrl();
            this._validateFrameScroll();
        }
    }

    _onCommitProperties() {

        if (this.invalidateLinkUrl) {
            this.validateCallLater(this._validateLinkUrl);
            this.invalidateLinkUrl = false;
        }

        if (this.invalidateFrameScroll) {
            this.validateCallLater(this._validateFrameScroll);
            this.invalidateFrameScroll = false;
        }

    }



    _validateLinkUrl() {
        if (this.isViewerMode == true) {

              /*
              2018.11.05(ckkim)
              - IE11에서 link_url이 없는 경우 에러 발생.
               */

              if(this.link_url==null)
                    return;

              try {
                    let url = this.link_url.trim();
                    if (!url)
                          return;

                    if (url.length <= 0) {
                          return;
                    }

                    this._iframe.src = url;
              }catch(error){
                    console.log("error = ", error);
              }
        }
    }

    _validateFrameScroll() {
        if (this.isViewerMode == true) {
              /*
              2018.11.05 (ckkim)
              IE11에서 scrolling 대입시 에러 발생
               */

              try {
                    this._iframe.scrolling = this.link_scrolling;
              }catch(error) {
                    if (CPUtil.getInstance().browserDetection() != "Internet Explorer")
                          this._iframe.scrolling = this.link_scrolling;
              }
        }
    }




    set link_url(url) {
        if (this._checkUpdateGroupPropertyValue("link", "link_url", url)) {
            this.invalidateLinkUrl = true;
        }

    }

    get link_url() {
        return this.getGroupPropertyValue("link", "link_url");
    }


    set link_scrolling(value) {
        if (this._checkUpdateGroupPropertyValue("link", "invalidateFrameScroll", value)) {
            this.invalidateFrameScroll = true;
        }
    }

    get link_scrolling() {
        return this.getGroupPropertyValue("link", "link_scrolling");
    }
}




// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(IFrameComponent, {
    "info": {
        "componentName": "IFrameComponent",
        "version": "1.0.1"
    },

    "setter": {
        "width": 100,
        "height": 100
    },

    "label": {
        "label_using": "N",
        "label_text": "IFrame Component"
    },

    "style": {
        "border": "1px dashed #808080",
        "backgroundColor": "rgba(255, 255, 255, 0.6)",
        "borderRadius": 0
    },

    "link": {
        "link_url": "",
        "link_scrolling": "yes"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
IFrameComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        template: "label"
    }, {
        template: "background"
    }, {
        template: "border"
    }, {
        label: "Link",
        template: "vertical",
        children: [{
            owner: "link",
            name: "link_url",
            type: "string",
            label: "Url",
            show: true,
            writable: true,
            description: "링크입력"
        }, {
            owner: "link",
            name: "link_scrolling",
            type: "select",
            label: "Scroll",
            show: true,
            writable: true,
            description: "스크롤 옵션",
            options: {
                items: [
                    { label: "hidden", value: "no" },
                    { label: "scroll", value: "yes" },
                ]
            }
        }]
    }
];


WVPropertyManager.add_property_group_info(IFrameComponent, {
    label: "IFrameComponent 고유 속성",
    children: [{
        name: "link_url",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'url..'",
        description: "IFrame에 적용할 src 정보입니다."
    }]
});
