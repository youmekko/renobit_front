"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var IFrameComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(IFrameComponent, _WVDOMComponent);

            function IFrameComponent() {
                  var _this;

                  _classCallCheck(this, IFrameComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(IFrameComponent).call(this));
                  _this.invalidateLinkUrl = false;
                  _this.invalidateFrameScroll = false;
                  return _this;
            }

            _createClass(IFrameComponent, [{
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this._element.classList.add("iframe-component");

                        if (this.isViewerMode == true) {
                              this._iframe = document.createElement("iframe");
                              this._iframe.style.width = "100%";
                              this._iframe.style.height = "100%";

                              this._element.appendChild(this._iframe);
                        } else {
                              $(this._element).addClass("isEditor");
                        }
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        if (this.isEditorMode == false && this._iframe) {
                              $(this._iframe).off();

                              this._element.removeChild(this._iframe);

                              this._iframe = null;
                        }

                        _get(_getPrototypeOf(IFrameComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        if (this.isViewerMode == true) {
                              this._validateLinkUrl();

                              this._validateFrameScroll();
                        }
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this.invalidateLinkUrl) {
                              this.validateCallLater(this._validateLinkUrl);
                              this.invalidateLinkUrl = false;
                        }

                        if (this.invalidateFrameScroll) {
                              this.validateCallLater(this._validateFrameScroll);
                              this.invalidateFrameScroll = false;
                        }
                  }
            }, {
                  key: "_validateLinkUrl",
                  value: function _validateLinkUrl() {
                        if (this.isViewerMode == true) {
                              /*
                              2018.11.05(ckkim)
                              - IE11에서 link_url이 없는 경우 에러 발생.
                               */
                              if (this.link_url == null) return;

                              try {
                                    var url = this.link_url.trim();
                                    if (!url) return;

                                    if (url.length <= 0) {
                                          return;
                                    }

                                    this._iframe.src = url;
                              } catch (error) {
                                    console.log("error = ", error);
                              }
                        }
                  }
            }, {
                  key: "_validateFrameScroll",
                  value: function _validateFrameScroll() {
                        if (this.isViewerMode == true) {
                              /*
                              2018.11.05 (ckkim)
                              IE11에서 scrolling 대입시 에러 발생
                               */
                              try {
                                    this._iframe.scrolling = this.link_scrolling;
                              } catch (error) {
                                    if (CPUtil.getInstance().browserDetection() != "Internet Explorer") this._iframe.scrolling = this.link_scrolling;
                              }
                        }
                  }
            }, {
                  key: "link_url",
                  set: function set(url) {
                        if (this._checkUpdateGroupPropertyValue("link", "link_url", url)) {
                              this.invalidateLinkUrl = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("link", "link_url");
                  }
            }, {
                  key: "link_scrolling",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("link", "invalidateFrameScroll", value)) {
                              this.invalidateFrameScroll = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("link", "link_scrolling");
                  }
            }]);

            return IFrameComponent;
      }(WVDOMComponent); // 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(IFrameComponent, {
      "info": {
            "componentName": "IFrameComponent",
            "version": "1.0.1"
      },
      "setter": {
            "width": 100,
            "height": 100
      },
      "label": {
            "label_using": "N",
            "label_text": "IFrame Component"
      },
      "style": {
            "border": "1px dashed #808080",
            "backgroundColor": "rgba(255, 255, 255, 0.6)",
            "borderRadius": 0
      },
      "link": {
            "link_url": "",
            "link_scrolling": "yes"
      }
}); // 프로퍼티 패널에서 사용할 정보 입니다.

IFrameComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}, {
      template: "background"
}, {
      template: "border"
}, {
      label: "Link",
      template: "vertical",
      children: [{
            owner: "link",
            name: "link_url",
            type: "string",
            label: "Url",
            show: true,
            writable: true,
            description: "링크입력"
      }, {
            owner: "link",
            name: "link_scrolling",
            type: "select",
            label: "Scroll",
            show: true,
            writable: true,
            description: "스크롤 옵션",
            options: {
                  items: [{
                        label: "hidden",
                        value: "no"
                  }, {
                        label: "scroll",
                        value: "yes"
                  }]
            }
      }]
}];
WVPropertyManager.add_property_group_info(IFrameComponent, {
      label: "IFrameComponent 고유 속성",
      children: [{
            name: "link_url",
            type: "string",
            show: true,
            writable: true,
            defaultValue: "'url..'",
            description: "IFrame에 적용할 src 정보입니다."
      }]
});
