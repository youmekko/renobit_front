'use strict';

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GroupButtonComponent = function(_WVDOMComponent) {
    _inherits(GroupButtonComponent, _WVDOMComponent);

    function GroupButtonComponent() {
        _classCallCheck(this, GroupButtonComponent);

        var _this = _possibleConstructorReturn(this, (GroupButtonComponent.__proto__ || Object.getPrototypeOf(GroupButtonComponent)).call(this));

        _this._invalidatePropertyTabs = false;
        _this._invalidatePropertyActive = false;
        return _this;
    }

    _createClass(GroupButtonComponent, [{
        key: 'getExtensionProperties',
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: '_onCreateElement',
        value: function _onCreateElement() {
            var self = this;
            var str = '<div class="renobit-btn-group">' + '<div class="item" v-for="(item, index) in compSetter.items"><span>{{getTranslateItemName(item)}}</span></div>' + '</div>';

            var temp = $(str);
            $(this._element).append(temp);

            var self = this;
            var app = new Vue({
                el: temp.get(0),
                data: function data() {
                    return {
                        compSetter: self.getGroupProperties("setter")
                    };
                },

                methods: {
                    getTranslateItemName: function getTranslateItemName(str) {
                        return wemb.localeManager.translatePrefixStr(str);
                    }
                }

            });
        }
    }, {
        key: 'bindEvent',
        value: function bindEvent() {
            if (this.isEditorMode) {
                return;
            }
            var self = this;
            $(this._element).on("click", ".renobit-btn-group .item", function(e) {
                e.preventDefault();
                var index = $(self._element).find(".item").index($(this));
                self.dispatchWScriptEvent("select", {
                    index: index
                });
            });
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            $(this._element).off("click");
            _get(GroupButtonComponent.prototype.__proto__ || Object.getPrototypeOf(GroupButtonComponent.prototype), '_onDestroy', this).call(this);
        }
    }, {
        key: 'getExtensionProperties',
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: '_onImmediateUpdateDisplay',
        value: function _onImmediateUpdateDisplay() {
            this.updateComponentStyle();
            this.bindEvent();
            this._validateFontStyleProperty();
        }
    }, {
        key: '_onCommitProperties',
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension")) {
                this.validateCallLater(this._updateStyle);
            }

            if (this._invalidatePropertyActive) {
                this.validateCallLater(this.changeActiveIndex);
                this._invalidatePropertyActive = false;
            }

            if (this._updatePropertiesMap.has("font")) {
                this.validateCallLater(this._validateFontStyleProperty);
            }
        }
    }, {
        key: '_validateFontStyleProperty',
        value: function _validateFontStyleProperty() {
            var fontProps = this.font;
            $(this._element).css("fontFamily", fontProps.font_type);
        }
    }, {
        key: '_updateStyle',
        value: function _updateStyle() {
            this.updateComponentStyle();
        }
    }, {
        key: 'updateComponentStyle',
        value: function updateComponentStyle() {
            var $el = $(this._element);
            $el.find("[style-id='" + this.id + "']").remove();

            var style = this.getGroupPropertyValue("extension", "style");
            var trimStr = style.trim();
            if (!trimStr) {
                return '';
            }

            var selectorStr = "[id='" + this.id + "'] ";
            trimStr = trimStr.replace(/}/gi, "} " + selectorStr);
            trimStr = selectorStr + trimStr;

            var lastIndex = trimStr.lastIndexOf(selectorStr);
            trimStr = trimStr.substr(0, lastIndex);

            var tagStr = "<style style-id='" + this.id + "'>" + trimStr + '</style>';
            $el.append(tagStr);
        }
    }, {
        key: 'items',
        set: function set(items) {
            if (this._checkUpdateGroupPropertyValue("setter", "items", items)) {
                this._invalidatePropertyTabs = true;
            }
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "items");
        }
    }, {
        key: 'font',
        get: function get() {
            return this.getGroupProperties("font");
        }
    }]);

    return GroupButtonComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(GroupButtonComponent, {
    "info": {
        "componentName": "GroupButtonComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 400,
        "height": 40,
        "items": ["item01"]
    },

    "label": {
        "label_using": "N",
        "label_text": "Group Button"
    },

    "font": {
        "font_type": "inherit"
    },

    "extension": {
        "style": "div.renobit-btn-group{\n    display: flex; \n    width:100%; \n    height: 100%;\n    font-family: inherit !important;\n}\n\ndiv.renobit-btn-group *{\n    font-family: inherit !important;\n}\n\ndiv.renobit-btn-group .item{\n    flex:1;\n    color: #fff;\n    background-color: #7ebeff;\n    border-color: #409eff;\n    border-radius: 5px;\n    cursor:pointer;\n    overflow: hidden;\n    padding:5px 10px;\n}\n\ndiv.renobit-btn-group .item:not(:last-child){\n    margin-right: 4px;\n}\n\ndiv.renobit-btn-group .item:hover{\n    background-color: #409eff;\n}\n\ndiv.renobit-btn-group .item>span{\n    display:flex; \n    align-items: center; \n    justify-content: center;\n    height: 100%;    \n}\n\ndiv.renobit-btn-group .item>span{\n    overflow: hidden;\n    text-overflow:ellipsis;\n    white-space: nowrap;\n    text-align:center;\n    display:table-cell;\n    vertical-align:middle;\n    width:100%;\n}\n\ndiv.renobit-btn-group .item{\n    display:table;\n    height:100%;\n    table-layout:fixed;\n    width:100%;\n}"
    },

    "style": {
        "backgroundColor": "rgba(255, 255, 255, 0)",
        "border": "1px none #000000",
        "borderRadius": "0",
        "cursor": "default"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
GroupButtonComponent.property_panel_info = [{
    template: "primary",
    label: "primary"
}, {
    template: "pos-size-2d",
    label: "pos-size-2d"
}, {
    template: "label",
    label: "label"
}, {
    template: "background",
    label: "background"
}, {
    template: "border",
    label: "border"
}, {
    template: "font-type",
    label: "font-type"
}];

// 이벤트 정보
WVPropertyManager.remove_event(GroupButtonComponent, "dblclick");
WVPropertyManager.remove_event(GroupButtonComponent, "click");

// 이벤트 정보
WVPropertyManager.add_event(GroupButtonComponent, {
    name: "select",
    label: "버튼 클릭 이벤트",
    description: "버튼 클릭 이벤트 입니다.",
    properties: [{
        name: "index",
        type: "Number",
        default: "",
        description: "클릭한 버튼의 index값 입니다."
    }]
});

WVPropertyManager.add_property_group_info(GroupButtonComponent, {
    label: "GroupButtonComponent 고유 속성",
    children: [{
        name: "items",
        type: "Array<string>",
        show: true,
        writable: true,
        defaultValue: "['button name1', 'button name2', 'button name3']",
        description: "버튼 이름 리스트\nex) this.item = ['button name1', 'button name2', 'button name3']"
    }]
});