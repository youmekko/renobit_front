"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var TemplateManagerComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(TemplateManagerComponent, _WVDOMComponent);

            function TemplateManagerComponent() {
                  var _this;

                  _classCallCheck(this, TemplateManagerComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(TemplateManagerComponent).call(this));
                  _this.$el;
                  _this.$wrap;
                  _this.targetSelector;
                  _this._dataProvider = null;
                  _this.isCreated = false;
                  _this.timer;
                  return _this;
            }

            _createClass(TemplateManagerComponent, [{
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        var _this2 = this;

                        this.$el = $(this._element);
                        this.$el.attr("data-id", this.id).addClass("template-manager-comp");
                        this.$el.append('<div class="tmpl-manager-wrap"></div>');
                        this.$wrap = this.$el.find(".tmpl-manager-wrap");
                        this.targetSelector = "[data-id='" + this.id + "'] .tmpl-manager-wrap";
                        this.createTransform(); ///////////////////////////////////////////////////////////////////////

                        if (!this.templateId) {
                              this.setNodateStyle();
                        }

                        if (this.isEditorMode) {
                              window.wemb.templateDataManager.$bus.$on("template_manager_update", function (templateList) {
                                    _this2.initTemplate();

                                    _this2.render();
                              });
                        }

                        this.isCreated = true;
                  }
            }, {
                  key: "openTemplate",
                  value: function openTemplate() {
                        var self = this;

                        if (this.isEditorMode) {
                              var templateId = self.templateId === null ? "" : self.templateId;
                              window.wemb.$templateManagerModal.show(templateId, false).then(function (result) {
                                    self.setGroupPropertyValue("extension", "templateId", result.template.id);
                              }), function (error) {
                                    console.log("template manager component template modal open error", error);
                              };
                        }
                  }
            }, {
                  key: "initTemplate",
                  value: function initTemplate() {
                        var _this3 = this;

                        this.templateList = window.wemb.templateDataManager.concatData;

                        if (!this.templateList.length || !this.templateId || this.templateList.find(function (x) {
                              return x.id === _this3.templateId;
                        })) {
                              this.setNodateStyle();
                              return;
                        }
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        this.render();
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.$el = null;
                        this.$wrap.remove();
                        this.$wrap = null;
                        this._dataProvider = null;

                        _get(_getPrototypeOf(TemplateManagerComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "dispatchRegisterEvent",
                  value: function dispatchRegisterEvent() {
                        _get(_getPrototypeOf(TemplateManagerComponent.prototype), "dispatchRegisterEvent", this).call(this);

                        if (this.isEditorMode) {
                              this.render();
                        }
                  }
            }, {
                  key: "setNodateStyle",
                  value: function setNodateStyle() {
                        if (this.$el && this.isEditorMode) {
                              this.$el.addClass("no-data");
                              this.$wrap.html("<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAOCAYAAABpcp9aAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MEZGNTdDREUyNTVEMTFFODg5QjlDQTk0Q0E0RTE2RjIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MEZGNTdDREYyNTVEMTFFODg5QjlDQTk0Q0E0RTE2RjIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowRkY1N0NEQzI1NUQxMUU4ODlCOUNBOTRDQTRFMTZGMiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowRkY1N0NERDI1NUQxMUU4ODlCOUNBOTRDQTRFMTZGMiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpTnVn0AAAH2SURBVHjaxFbLUcMwELU9uRMqQBw4x1RAaACcCnAHmBNHnCMn3AGmAw8NYCqIOXOIU0FMBWHfzHNmR2MlYJLwZnYUSav1vv1I8VerlfdTPL5+ZjI091dnKefeHhFzzF0K4ocX9DBae4dBrEg4ETgibUQia20sw5FIyTlSlx6ITCIy3EpAnBqyTOYioaULQh+StppkACMypnGjzhhKiyH19Fqo9EPuuxAx84mTgDjVlgfGu7bOFcZt9AVvHG/4O+S5QqRiAObUx9qSenM6A2Tcxzdn3G86AtcSgP4T7a/JBoimCAw9s2GMOJ/ZJSXDiM6geXxuTUV8ReyEZXWKbIlc8IOYH4ssFAGPDse0cc61rrJslN2GZAv4NeAGPnQpjpWOFILx14Z9jUKdWaooexx1GdWKfMUA3m6wXdMufH7AmYB19Q5WwihntLtSWP6y8ZoezbrtTEg/4PwLCAcS1UoErCZkVwmJFA3tqP//gGF2Zpyj3GJcKIF6FAoRw/QkbccLkZDXZ+GIyC4RsXds5AziOsjOd4ANbJTDMLoAW0sVH7pmZuKeDl+wrjM6NXI0cWr5tMbA8UQ3imXkiH6sbpTK8UJPrfW8Iwi5KtFElarWdZavv+m/EPsAN8kEJdax/5dyKVV/9QL+Cw22KDS8o/eBahdGvgUYAH0jt3P7TWSdAAAAAElFTkSuQmCC'/>");
                        }
                  }
            }, {
                  key: "render",
                  value: function render() {
                        var data = this.getTemplateData();
                        clearTimeout(this.timer);
                        var self = this;
                        this.timer = setTimeout(function () {
                              if (self.$wrap) {
                                    self.$wrap.empty();

                                    if (!self.templateId || !window.wemb.templateDataManager.concatData.find(function (x) {
                                          return x.id === self.templateId;
                                    })) {
                                          if (self.isEditorMode) {
                                                self.setNodateStyle();
                                          }

                                          return;
                                    }

                                    self.$el.removeClass("no-data");
                                    var trimStr = self.getHtmlDomStr();

                                    try {
                                          $.tmpl(trimStr, data).appendTo(self.targetSelector);
                                    } catch (error) {
                                          console.warn("template error", error);
                                    }

                                    var style = self.getInstanceStyle();
                                    self.$wrap.append('<style>' + style + '</style>');
                              }
                        }, 5);
                  }
            }, {
                  key: "getTemplateData",
                  value: function getTemplateData() {
                        var data = {
                              data: this.dataProvider
                        };
                        return data;
                  }
            }, {
                  key: "getHtmlDomStr",
                  value: function getHtmlDomStr() {
                        var _this4 = this;

                        if (this.templateId) {
                              var template = window.wemb.templateDataManager.concatData.find(function (x) {
                                    return x.id === _this4.templateId;
                              });

                              if (template) {
                                    var trimStr = template.htmlStr.trim();
                                    return trimStr;
                              }
                        }

                        return "";
                  }
            }, {
                  key: "getInstanceStyle",
                  value: function getInstanceStyle() {
                        var _this5 = this;

                        if (this.templateId) {
                              var template = window.wemb.templateDataManager.concatData.find(function (x) {
                                    return x.id === _this5.templateId;
                              });
                              var trimStr = null;

                              if (template) {
                                    trimStr = template.styleStr.trim();
                                    var selectorStr = this.targetSelector;
                                    trimStr = trimStr.replace(/@app/gi, selectorStr);
                                    return trimStr;
                              }
                        }

                        return '';
                  }
            }, {
                  key: "getTestData",
                  value: function getTestData() {
                        var _this6 = this;

                        if (this.templateId) {
                              var template = window.wemb.templateDataManager.concatData.find(function (x) {
                                    return x.id === _this6.templateId;
                              });
                              var trimDataStr = null;

                              if (template) {
                                    trimDataStr = template.dataStr.trim();
                              }

                              if (!trimDataStr) {
                                    trimDataStr = "{}";
                              }

                              var str = '{"data" :' + trimDataStr + "}";
                              var json = JSON.parse(str);
                              return json.data;
                        }

                        return {
                              "data": {}
                        };
                  }
            }, {
                  key: "show",
                  value: function show() {
                        this.$el.show();
                  }
            }, {
                  key: "hide",
                  value: function hide() {
                        this.$el.hide();
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._updatePropertiesMap.has("extension.templateId") && this.isCreated) {
                              this.validateCallLater(this._validateSetterProperty);
                        }
                  }
            }, {
                  key: "_validateSetterProperty",
                  value: function _validateSetterProperty() {
                        this.render();
                  }
            }, {
                  key: "dataProvider",
                  set: function set(data) {
                        //if (this._sortData.length) { return; }
                        var oldValue = this.dataProvider;
                        this._dataProvider = data;
                        var self = this;

                        if (!this.isEditorMode && data != oldValue) {
                              this.dispatchWScriptEvent("change", {
                                    value: data
                              });
                              self.render();
                        }
                  },
                  get: function get() {
                        if (this.isEditorMode) {
                              return this.getTestData();
                        }

                        return this._dataProvider;
                  }
            }, {
                  key: "templateId",
                  get: function get() {
                        return this.getGroupPropertyValue("extension", "templateId");
                  }
            }]);

            return TemplateManagerComponent;
      }(WVDOMComponent);
      
WVPropertyManager.attach_default_component_infos(TemplateManagerComponent, {
      "info": {
            "componentName": "TemplateManagerComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 100,
            "height": 100
      },
      "label": {
            "label_using": "N",
            "label_text": "Template Manager Component"
      },
      "extension": {
            "templateId": null
      }
}); // 프로퍼티 패널에서 사용할 정보 입니다.

TemplateManagerComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}, {
      template: "template-manager",
      children: [{
            owner: "extension",
            name: "templateId",
            show: true,
            label: "Template",
            writable: false,
            description: "Select Template"
      }]
}];


// 이벤트 정보
WVPropertyManager.add_event(TemplateManagerComponent, {
      name: "change",
      label: "값 체인지 이벤트",
      description: "값 체인지 이벤트 입니다.",
      properties: [{
            name: "value",
            type: "string",
            default: "",
            description: "새로운 값입니다."
      }]
})

//사용하지 않는 이벤트
// WVPropertyManager.add_event(TemplateManagerComponent, {
//       name: "selectTemplateItem",
//       label: "selectTemplateItem  이벤트",
//       description: "selectTemplateItem 이벤트 입니다.",
//       properties: []
// });


WVPropertyManager.add_property_group_info(TemplateManagerComponent, {
      label: "TemplateComponent Property",
      children: [{
            name: "dataProvider",
            type: "any",
            show: true,
            writable: true,
            description: "template data"
      }]
});
WVPropertyManager.add_property_group_info(TemplateManagerComponent, {
      label: "TemplateManagerComponent Property",
      children: [{
            name: "templateId",
            type: "string",
            show: "true",
            writable: false,
            description: "Set template"
      }]
}); //  추후 추가 예정

WVPropertyManager.add_method_info(TemplateManagerComponent, {
      name: "render",
      description: "Outputs the saved dataProvider to the screen according to the created template format."
});
