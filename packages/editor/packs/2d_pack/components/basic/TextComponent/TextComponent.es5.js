'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextComponent = function (_WVDOMComponent) {
      _inherits(TextComponent, _WVDOMComponent);

      function TextComponent() {
            _classCallCheck(this, TextComponent);

            var _this = _possibleConstructorReturn(this, (TextComponent.__proto__ || Object.getPrototypeOf(TextComponent)).call(this));

            _this.translateText = '';
            _this.$txt;
            _this.$wrap;
            _this._invalidatePropertyTxt = false;
            _this._tempTimerID = 0;
            _this.tempList = [];
            return _this;
      }

      _createClass(TextComponent, [{
            key: '_onCreateElement',
            value: function _onCreateElement() {
                  var $el = $(this._element);
                  $el.append('<div class="txt-wrap"><pre></pre></div>');
                  this.$wrap = $el.find(".txt-wrap").addClass("align-v");
                  this.$txt = this.$wrap.find("pre");

                  this.$txt.css({
                        "width": "100%",
                        "userSelect": "none",
                        "background": "none",
                        "border": "none",
                        "overflow": "hidden",
                        "resize": "none",
                        "cursor": "default",
                        "word-wrap": "break-word",
                        "white-space": "pre-wrap",
                        /*"pointer-events": "none"*/
                  });
            }
      }, {
            key: '_validateTextShadowProperty',
            value: function _validateTextShadowProperty() {
                  var _this$textShadow = this.textShadow,
                        h_shadow = _this$textShadow.h_shadow,
                        v_shadow = _this$textShadow.v_shadow,
                        blur_radius = _this$textShadow.blur_radius,
                        color = _this$textShadow.color;

                  function hasValue() {
                        return !(h_shadow == 0 && v_shadow == 0 && blur_radius == 0);
                  }

                  var cssValue = hasValue() ? "".concat(h_shadow, "px ").concat(v_shadow, "px ").concat(blur_radius, "px ").concat(color) : '';
                  this.$txt.css("text-shadow", cssValue);
            }
      }, {
            key: '_onDestroy',
            value: function _onDestroy() {
                  if (this._tempTimerID) {
                        clearInterval(this._tempTimerID);
                  }
                  this._tempTimerID = 0;
                  this.$txt.remove();
                  this.$wrap.remove();
                  this.$txt = null;
                  this.$wrap = null;
                  _get(TextComponent.prototype.__proto__ || Object.getPrototypeOf(TextComponent.prototype), '_onDestroy', this).call(this);
            }
      }, {
            key: 'getExtensionProperties',
            value: function getExtensionProperties() {
                  return true;
            }
      }, {
            key: '_onImmediateUpdateDisplay',
            value: function _onImmediateUpdateDisplay() {
                  this.changeTxtElement("textarea");
                  this._validateTxtProperty();
                  this._validateFontStyleProperty();
                  this._validateAlignStyleProperty();
                  this._validateTextShadowProperty();
                  this._validateTextGradientProperty();
                  this._validateTextStrokeProperty();
                  this.updateBackground();
            }
      }, {
            key: '_onCreateProperties',
            value: function _onCreateProperties() {
                  /*이전버전 호환처리*/
                  if (this.getGroupPropertyValue("background", "type") == "") {
                        this.setGroupPropertyValue("background", "type", "solid");
                        var color = this.getGroupPropertyValue("style", "backgroundColor");
                        this.setGroupPropertyValue("background", "color1", color);
                  }
            }
      }, {
            key: 'onLoadPage',
            value: function onLoadPage() {
                  this.changeTxtElement("textarea");
                  this._validateFontStyleProperty();
                  this._validateAlignStyleProperty();
                  this._validateTextShadowProperty();
                  this._validateTextGradientProperty();
                  this._validateTextStrokeProperty();
            }
      }, {
            key: '_onCommitProperties',
            value: function _onCommitProperties() {
                  if (this._updatePropertiesMap.has("font")) {
                        this.validateCallLater(this._validateFontStyleProperty);
                  }

                  if (this._updatePropertiesMap.has("align")) {
                        this.validateCallLater(this._validateAlignStyleProperty);
                  }

                  if (this._updatePropertiesMap.has("textShadow")) {
                        this.validateCallLater(this._validateTextShadowProperty);
                  }

                  if (this._updatePropertiesMap.has("gradient")) {
                        this.validateCallLater(this._validateTextGradientProperty);
                  }

                  if (this._updatePropertiesMap.has("stroke")) {
                        this.validateCallLater(this._validateTextStrokeProperty);
                  }

                  if (this._invalidatePropertyTxt) {
                        this.validateCallLater(this._validateTxtProperty);
                        this._invalidatePropertyTxt = false;
                  }

                  if (this._updatePropertiesMap.has("background")) {
                        this.validateCallLater(this.updateBackground);
                  }

                  this.updateTxtElHeight();
            }
      }, {
            key: 'updateBackground',
            value: function updateBackground() {
                  var bgData = this.getGroupProperties("background");
                  var style = this._styleManager.getBackgroundStyle(bgData, '[id=\'' + this.id + '\']');
                  $(this._element).find("style").remove();
                  $(this._element).append(style);
            }
      }, {
            key: '_validateTxtProperty',
            value: function _validateTxtProperty() {
                  this.translateText = wemb.localeManager.translatePrefixStr(this.text);
                  this.updateTextValue();
            }
      }, {
            key: '_validateFontStyleProperty',
            value: function _validateFontStyleProperty() {

                  var fontProps = this.font;

                  var textFillColorValue = fontProps.useGradient === true ? 'transparent' : '';

                  this.$txt.css({
                        "fontFamily": fontProps.font_type,
                        "fontSize": fontProps.font_size,
                        "fontWeight": fontProps.font_weight,
                        "color": fontProps.font_color,
                        "textAlign": fontProps.text_align,
                        "lineHeight": fontProps.line_height + "px",
                        "-webkit-text-fill-color" : textFillColorValue
                  });

                  this.updateTxtElHeight();

            }
      }, {
            key: '_validateAlignStyleProperty',
            value: function _validateAlignStyleProperty() {
                  var alignProps = this.align;
                  this.$txt.css("padding", alignProps.padding_tb + "px " + alignProps.padding_lr + "px");

                  //"vertical_align": "top",
                  this.$wrap.removeClass("align-top align-center align-bottom");
                  switch (alignProps.vertical_align) {
                        case "center":
                              this.$wrap.addClass("align-center");
                              break;
                        case "bottom":
                              this.$wrap.addClass("align-bottom");
                              break;
                        case "top":
                        default:
                              this.$wrap.addClass("align-top");
                  }

                  this.updateWritingMode();
            }
      }, {
            key: '_validateTextGradientProperty',
            value: function _validateTextGradientProperty() {

                  var fontProps = this.font;
                  var css;

                  if (fontProps.useGradient) {
                        css = {
                              "background": this.getTextGradientDirection(this.gradient),
                              "-webkit-background-clip": "text",
                        };
                  } else {
                        css = {
                              "background": 'none',
                              "-webkit-background-clip": '',
                        };
                  }

                  this.$txt.css(css);
            }
      }, {
            key : 'getTextGradientDirection',
            value : function getTextGradientDirection(gradient) {

                  var GRADIENT_DIRECTION_STYLE = {
                        left: 'linear-gradient( 90deg',
                        top: 'linear-gradient( 180deg',
                        diagonal1: 'linear-gradient( -45deg',
                        diagonal2: 'linear-gradient( 45deg',
                        radial: 'radial-gradient(ellipse at center'
                  };
                  return "".concat(GRADIENT_DIRECTION_STYLE[gradient.direction], ", ").concat(gradient.firstGradientColor, ", ").concat(gradient.secondGradientColor, " )")
            }
      }, {
            key : '_validateTextStrokeProperty',
            value : function _validateTextStrokeProperty() {

                  var strokeProps = this.stroke;

                  this.$txt.css({
                        "-webkit-text-stroke-width" : strokeProps.strokeWidth + "px",
                        "-webkit-text-stroke-color" : strokeProps.strokeColor
                  })
            }
      },{
            key: 'updateWritingMode',
            value: function updateWritingMode() {
                  var writing_mode = this.getGroupPropertyValue("align", "writing_mode");
                  switch (writing_mode) {
                        case "vertical-lr":
                              this.$txt.css("writing-mode", writing_mode);
                              this.$txt.css("writing-mode", 'tb-lr'); //for ie...
                              break;
                        case "vertical-rl":
                              this.$txt.css("writing-mode", writing_mode);
                              this.$txt.css("writing-mode", 'tb-rl'); //for ie...
                              break;
                        default:
                              this.$txt.css("writing-mode", '');
                  }

                  if (writing_mode == "vertical-lr" || writing_mode == "vertical-rl") {
                        if (this.$txt.get(0).tagName != "PRE") {
                              this.changeTxtElement("pre");
                        }
                  } else if (this.$txt.get(0).tagName == "PRE") {
                        this.changeTxtElement("textarea");
                  } else {
                        this.changeTxtElement("textarea");
                  }
            }
      }, {
            key: 'changeTxtElement',
            value: function changeTxtElement(tagName) {
                  var text = this.translateText;

                  var tempStyle = this.$txt.attr("style");
                  this.$txt.remove();
                  if (tagName == "pre") {
                        this.$txt = $("<pre></pre>");
                  } else {
                        this.$txt = $("<textarea disabled readonly></textarea>");
                  }

                  this.$txt.attr("style", tempStyle);
                  this.$wrap.prepend(this.$txt);

                  if (tagName == "pre") {
                        this.$txt.html(text);
                  } else {
                        this.$txt.css({ "outline": "none", "user-select": "none" });
                        this.$txt.val(text);
                  }

                  this.updateTxtElHeight();
            }
      }, {
            key: 'updateTxtElHeight',
            value: function updateTxtElHeight() {
                  var _this2 = this;

                  if (this.$txt.get(0).tagName.toLowerCase() == "pre") {
                        this.$txt.height('');
                  } else {
                        var $tempPre = $("<pre class='textcomponent-pre'>" + this.translateText + "</pre>");
                        $(this._element).append($tempPre);
                        var tempStyle = this.$txt.attr("style");
                        $tempPre.attr("style", tempStyle);
                        $tempPre.css({ "height": "auto", "position": "absolute", "visibility": "hidden", "left": "-99999px" });
                        $tempPre.css("width", this.appendElement.style.width);
                        /*if (this.$wrap.find("pre").length > 1) {
				    this.$wrap.find("pre").eq(0).remove();
				    //$tempPre.remove();
				    //$tempPre = null;
				}*/

                        this.tempList.push($tempPre);

                        //알수없는 오류... settimeout필요
                        if (this._tempTimerID) {
                              clearInterval(this._tempTimerID);
                              this._tempTimerID = 0;
                        }

                        this._tempTimerID = setTimeout(function () {
                              _this2.$txt.height($tempPre.height());
                              _this2.tempList.forEach(function(temp) {
                                    temp.remove();
                              });
                              _this2.tempList = [];
                              $tempPre = null;
                        }, 0);
                  }
            }
      }, {
            key: 'updateTextValue',
            value: function updateTextValue() {
                  var text = this.translateText;
                  if (this.$txt.get(0).tagName.toLowerCase() == "pre") {
                        this.$txt.html(text);
                  } else {
                        this.$txt.val(text);
                  }

                  this.updateTxtElHeight();

                  if (!this.isEditorMode) {
                        this.dispatchWScriptEvent("change", {
                              value: text
                        });
                  }
            }
      }, {
            key: 'text',
            set: function set(txt) {
                  if (this._checkUpdateGroupPropertyValue("setter", "text", txt)) {
                        this._invalidatePropertyTxt = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "text");
            }
      }, {
            key: 'align',
            get: function get() {
                  return this.getGroupProperties("align");
            }
      }, {
            key: 'font',
            get: function get() {
                  return this.getGroupProperties("font");
            }

            /*sample() {
		    this.dispatchWScriptEvent("change", {
			  newValue: "test1",
			  oldValue: "test3"
		    })
		 }*/

      },{
            key: 'textShadow',
            get: function get() {
                  return this.getGroupProperties("textShadow");
            }
      },{
            key: 'gradient',
            get: function get() {
                  return this.getGroupProperties("gradient");
            }
      },{
            key: 'stroke',
            get: function get() {
                  return this.getGroupProperties("stroke");
            }
      }]);

      return TextComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(TextComponent, {
      "info": {
            "componentName": "TextComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 100,
            "height": 100,
            "text": "Text Component"
      },

      "label": {
            "label_using": "N",
            "label_text": "Text Component"
      },

      "font": {
            "font_type": "inherit",
            "font_color": "#333333",
            "font_size": 14,
            "font_weight": "normal",
            "line_height": 14,
            "text_align": "left",
            "useGradient" : false
      },

      "align": {
            "vertical_align": "top",
            "writing_mode": "horizontal-tb",
            "padding_tb": 0,
            "padding_lr": 0,
      },

      "style": {
            "border": "1px none #000000",
            "borderRadius": "0",
            "cursor": "default",
      },

      "background": {
            "type": "",
            "color1": "#fff",
            "color2": "#000",
            "text": ''
      },
      "textShadow" : {
            "h_shadow" : 0,
            "v_shadow" : 0,
            "blur_radius" : 0,
            "color" : "#000"
      },
      "gradient" : {
            "firstGradientColor": "#000",
            "secondGradientColor": "#000",
            "direction": "left",
            "useGradient": false
      },
      "stroke" : {
            "strokeColor" : "#000",
            "strokeWidth" : 0,
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
TextComponent.property_panel_info = [{
      template: "primary",
      label: "primary"
}, {
      template: "pos-size-2d",
      label: "pos-size-2d"
}, {
      template: "cursor",
      label: "pos-size-2d"
}, {
      template: "label",
      label: "label"
}, {
      template: "background-gradient",
      owner: "background",
      label: "Background"
}, {
      template: "border",
      label: "border"
}, {
      template: "font",
      label: "font"
}, {
      label : "Gradient",
      template : "gradient"
}, {
      label: "Align & Margin",
      template: "vertical",
      children: [{
            owner: "align",
            name: "vertical_align",
            type: "select",
            options: {
                  items: [{ label: "top", value: "top" }, { label: "center", value: "center" }, { label: "bottom", value: "bottom" }]
            },
            label: "vertical",
            show: true,
            writable: true,
            description: "vertical align"
      }, {
            owner: "align",
            name: "writing_mode",
            type: "select",
            options: {
                  items: [{ label: "horizontal-tb", value: "horizontal-tb" }, { label: "vertical-lr", value: "vertical-lr" }, { label: "vertical-rl", value: "vertical-rl" }]
            },
            label: "writing",
            show: true,
            writable: true,
            description: "writing mode"
      }, {
            owner: "align",
            name: "padding_tb",
            type: "number",
            label: "margin TB",
            show: true,
            writable: true,
            description: "padding top, bottom 설정"
      }, {
            owner: "align",
            name: "padding_lr",
            type: "number",
            label: "margin LR",
            show: true,
            writable: true,
            description: "padding left, right 설정"
      }]
}, {
      label : "Text Shadow",
      template : "text-shadow"
}, {
      label : "Stroke",
      template : "stroke"
}];

// 이벤트 정보
WVPropertyManager.add_event(TextComponent, {
      name: "change",
      label: "값 체인지 이벤트",
      description: "값 체인지 이벤트 입니다.",
      properties: [{
            name: "value",
            type: "string",
            default: "",
            description: "새로운 값입니다."
      }]
});

WVPropertyManager.add_property_group_info(TextComponent, {
      label: "TextComponent 고유 속성",
      children: [{
            name: "text",
            type: "string",
            show: true,
            writable: true,
            defaultValue: "'text...'",
            description: "화면에 출력할 문자열 데이터입니다."
      }]
});
