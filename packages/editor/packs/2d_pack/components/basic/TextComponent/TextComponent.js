class TextComponent extends WVDOMComponent {
    constructor() {
        super();
        this.translateText = '';
        this.$txt;
        this.$wrap;
        this._invalidatePropertyTxt = false;
        this._tempTimerID =0;
        this.tempList = [];
    }

    _onCreateElement() {
        let $el = $(this._element);
        $el.append('<div class="txt-wrap"><pre></pre></div>');
        this.$wrap = $el.find(".txt-wrap").addClass("align-v");
        this.$txt = this.$wrap.find("pre");

        this.$txt.css({
            "width": "100%",
            "userSelect": "none",
            "background": "none",
            "border": "none",
            "overflow": "hidden",
            "resize": "none",
            "cursor": "default",
            "word-wrap": "break-word",
            "white-space": "pre-wrap",
            /*"pointer-events": "none"*/
        });
    }


      _validateTextShadowProperty() {

            const { h_shadow, v_shadow, blur_radius, color } = this.textShadow;

            function hasValue() {
                  return !(h_shadow == 0 && v_shadow == 0 && blur_radius == 0)
            }

            const cssValue = hasValue() ? `${h_shadow}px ${v_shadow}px ${blur_radius}px ${color}` : '';
            this.$txt.css(
                  "text-shadow", cssValue
            );
      }

      _validateTextGradientProperty() {

            let fontProps = this.font;
            let css;

            if (fontProps.useGradient) {
                  css = {
                        'background': this.getTextGradientDirection(this.gradient),
                        "-webkit-background-clip": "text",
                  }
            }else{
                  css = {
                        'background': 'none',
                        "-webkit-background-clip": "",
                  }
            }

            this.$txt.css(css);
      }

      getTextGradientDirection(gradient) {

          const GRADIENT_DIRECTION_STYLE = {
                  left : 'linear-gradient( 90deg',
                  top : 'linear-gradient( 180deg',
                  diagonal1 : 'linear-gradient( -45deg',
                  diagonal2 : 'linear-gradient( 45deg',
                  radial : 'radial-gradient(ellipse at center'
          };

          return  `${GRADIENT_DIRECTION_STYLE[gradient.direction]}, ${gradient.firstGradientColor}, ${gradient.secondGradientColor} )`

      }

    _onDestroy() {
      if(this._tempTimerID) {
          clearInterval(this._tempTimerID);
      }
        this._tempTimerID = 0;
        this.$txt.remove();
        this.$wrap.remove();
        this.$txt = null;
        this.$wrap = null;
        super._onDestroy();
    }

    getExtensionProperties() {
        return true;
    }

    _onImmediateUpdateDisplay() {
        this.changeTxtElement("textarea");
        this._validateTxtProperty();
        this._validateFontStyleProperty();
        this._validateAlignStyleProperty();
        this._validateTextShadowProperty();
        this._validateTextGradientProperty();
        this._validateTextStrokeProperty();
        this.updateBackground();
    }

    _onCreateProperties() {
        /*이전버전 호환처리*/
        if (this.getGroupPropertyValue("background", "type") == "") {
            this.setGroupPropertyValue("background", "type", "solid");
            let color = this.getGroupPropertyValue("style", "backgroundColor");
            this.setGroupPropertyValue("background", "color1", color);
        }
    }

    onLoadPage() {
        this.changeTxtElement("textarea");
        this._validateFontStyleProperty();
        this._validateAlignStyleProperty();
        this._validateTextShadowProperty();
        this._validateTextGradientProperty();
        this._validateTextStrokeProperty();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._validateFontStyleProperty)
        }

        if (this._updatePropertiesMap.has("align")) {
            this.validateCallLater(this._validateAlignStyleProperty)
        }

        if (this._updatePropertiesMap.has("textShadow")) {
            this.validateCallLater(this._validateTextShadowProperty)
        }

        if (this._updatePropertiesMap.has("gradient")) {
              this.validateCallLater(this._validateTextGradientProperty);
        }

        if (this._invalidatePropertyTxt) {
            this.validateCallLater(this._validateTxtProperty);
            this._invalidatePropertyTxt = false;
        }

        if (this._updatePropertiesMap.has("background")) {
            this.validateCallLater(this.updateBackground);
        }

        if(this._updatePropertiesMap.has("stroke")) {
              this.validateCallLater(this._validateTextStrokeProperty);
        }

        this.updateTxtElHeight();

    }

    updateBackground() {
        let bgData = this.getGroupProperties("background");
        let style = this._styleManager.getBackgroundStyle(bgData, `[id='${this.id}']`);
        $(this._element).find("style").remove();
        $(this._element).append(style);
    }


    _validateTxtProperty() {
        this.translateText = wemb.localeManager.translatePrefixStr(this.text);
        this.updateTextValue();
    }

    _validateFontStyleProperty() {

        let fontProps = this.font;

        let textFillColorValue = fontProps.useGradient === true ? 'transparent' : '';

        this.$txt.css({
              "fontFamily": fontProps.font_type,
              "fontSize": fontProps.font_size,
              "fontWeight": fontProps.font_weight,
              "color": fontProps.font_color,
              "textAlign": fontProps.text_align,
              "lineHeight": fontProps.line_height + "px",
              "-webkit-text-fill-color": textFillColorValue
        });

        this.updateTxtElHeight();
    }

    _validateTextStrokeProperty() {

          let strokeProps = this.stroke;

          this.$txt.css({
                "-webkit-text-stroke-width" : strokeProps.strokeWidth + "px",
                "-webkit-text-stroke-color" : strokeProps.strokeColor
          });
    }

    _validateAlignStyleProperty() {
        var alignProps = this.align;
        this.$txt.css("padding", alignProps.padding_tb + "px " + alignProps.padding_lr + "px");

        //"vertical_align": "top",
        this.$wrap.removeClass("align-top align-center align-bottom");
        switch (alignProps.vertical_align) {
            case "center":
                this.$wrap.addClass("align-center");
                break;
            case "bottom":
                this.$wrap.addClass("align-bottom");
                break;
            case "top":
            default:
                this.$wrap.addClass("align-top");
        }

        this.updateWritingMode();
    }

    updateWritingMode() {
        var writing_mode = this.getGroupPropertyValue("align", "writing_mode");
        switch (writing_mode) {
            case "vertical-lr":
                this.$txt.css("writing-mode", writing_mode);
                this.$txt.css("writing-mode", 'tb-lr'); //for ie...
                break;
            case "vertical-rl":
                this.$txt.css("writing-mode", writing_mode);
                this.$txt.css("writing-mode", 'tb-rl'); //for ie...
                break;
            default:
                this.$txt.css("writing-mode", '');
        }

        if (writing_mode == "vertical-lr" || writing_mode == "vertical-rl") {
            if (this.$txt.get(0).tagName != "PRE") {
                this.changeTxtElement("pre");
            }
        } else if (this.$txt.get(0).tagName == "PRE") {
            this.changeTxtElement("textarea");
        } else {
            this.changeTxtElement("textarea");
        }
    }

    changeTxtElement(tagName) {
        let text = this.translateText;

        var tempStyle = this.$txt.attr("style");
        this.$txt.remove();
        if (tagName == "pre") {
            this.$txt = $("<pre></pre>");
        } else {
            this.$txt = $("<textarea disabled readonly></textarea>");
        }

        this.$txt.attr("style", tempStyle);
        this.$wrap.prepend(this.$txt);

        if (tagName == "pre") {
            this.$txt.html(text);
        } else {
            this.$txt.css({ "outline": "none", "user-select": "none" });
            this.$txt.val(text);
        }

        this.updateTxtElHeight();
    }


    updateTxtElHeight() {
        if (this.$txt.get(0).tagName.toLowerCase() == "pre") {
            this.$txt.height('');
        } else {
            let $tempPre = $("<pre class='textcomponent-pre'>" + this.translateText + "</pre>");
            $(this._element).append($tempPre);
            let tempStyle = this.$txt.attr("style");
            $tempPre.attr("style", tempStyle);
            $tempPre.css({ "height": "auto", "position": "absolute", "visibility": "hidden", "left": "-99999px" });
            $tempPre.css("width", this.appendElement.style.width);
            /*if (this.$wrap.find("pre").length > 1) {
                this.$wrap.find("pre").eq(0).remove();
                //$tempPre.remove();
                //$tempPre = null;
            }*/

            this.tempList.push($tempPre);

            //알수없는 오류... settimeout필요
            if(this._tempTimerID){
                  clearInterval(this._tempTimerID);
                  this._tempTimerID = 0;
            }
            this._tempTimerID=setTimeout(() => {
                this.$txt.height($tempPre.height());
                this.tempList.forEach((temp) => {
                      temp.remove();
                });
                $tempPre = null;
                this.tempList = [];
            }, 0);

        }
    }


    updateTextValue() {
        let text = this.translateText;
        if (this.$txt.get(0).tagName.toLowerCase() == "pre") {
            this.$txt.html(text);
        } else {
            this.$txt.val(text);
        }

        this.updateTxtElHeight();

        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: text
            })
        }
    }

    set text(txt) {
        if (this._checkUpdateGroupPropertyValue("setter", "text", txt)) {
            this._invalidatePropertyTxt = true;
        }
    }

    get text() {
        return this.getGroupPropertyValue("setter", "text");
    }


    get align() {
        return this.getGroupProperties("align");
    }

    get font() {
        return this.getGroupProperties("font");
    }

    get gradient() {
          return this.getGroupProperties("gradient");
    }

    get stroke() {
          return this.getGroupProperties("stroke");
    }

    /*sample() {
        this.dispatchWScriptEvent("change", {
            newValue: "test1",
            oldValue: "test3"
        })

    }*/
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(TextComponent, {
    "info": {
        "componentName": "TextComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "text": "Text Component"
    },

    "label": {
        "label_using": "N",
        "label_text": "Text Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal",
        "line_height": 14,
        "text_align": "left"
    },

    "align": {
        "vertical_align": "top",
        "writing_mode": "horizontal-tb",
        "padding_tb": 0,
        "padding_lr": 0
    },

    "style": {
        "border": "1px none #000000",
        "borderRadius": "0",
        "cursor": "default"
    },

    "background": {
        "type": "",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#fff",
        "color2": "#000",
        "text": ''
    },

      "textShadow" : {
            "h_shadow" : 0,
            "v_shadow" : 0,
            "blur_radius" : 0,
            "color" : "#000"
      },
      "gradient" : {
            "firstGradientColor": "#000",
            "secondGradientColor": "#000",
            "direction": "left",
            "useGradient": false
      },
      "stroke" : {
          "strokeColor" : "#000",
          "strokeWidth" : 0,
      }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
TextComponent.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    }, {
        template: "cursor",
        label: "pos-size-2d"
    },
    {
        template: "label",
        label: "label"
    }, {
        template: "background-gradient",
        owner: "background",
        label: "Background"
    }, {
        template: "border",
        label: "border"
    }, {
        template: "font",
        label: "font"
    }, {
            label : "Gradient",
            template : "gradient"
    }, {
        label: "Align & Margin",
        template: "vertical",
        children: [{
            owner: "align",
            name: "vertical_align",
            type: "select",
            options: {
                items: [
                    { label: "top", value: "top" },
                    { label: "center", value: "center" },
                    { label: "bottom", value: "bottom" }
                ]
            },
            label: "Vertical",
            show: true,
            writable: true,
            description: "vertical align"
        }, {
            owner: "align",
            name: "writing_mode",
            type: "select",
            options: {
                items: [
                    { label: "horizontal-tb", value: "horizontal-tb" },
                    { label: "vertical-lr", value: "vertical-lr" },
                    { label: "vertical-rl", value: "vertical-rl" }
                ]
            },
            label: "Writing",
            show: true,
            writable: true,
            description: "writing mode"
        }, {
            owner: "align",
            name: "padding_tb",
            type: "number",
            label: "Margin TB",
            show: true,
            writable: true,
            description: "padding top, bottom 설정"
        }, {
            owner: "align",
            name: "padding_lr",
            type: "number",
            label: "Margin LR",
            show: true,
            writable: true,
            description: "padding left, right 설정"
        }]
      }, {
            label : "Text Shadow",
            template : "text-shadow"
      }, {
            label : "Stroke",
            template : "stroke"
      }
];

// 이벤트 정보
WVPropertyManager.add_event(TextComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(TextComponent, {
    label: "TextComponent 고유 속성",
    children: [{
        name: "text",
        type: "string",
        show: true,
        writable: true,
        defaultValue: "'text...'",
        description: "화면에 출력할 문자열 데이터입니다."
    }]
});
