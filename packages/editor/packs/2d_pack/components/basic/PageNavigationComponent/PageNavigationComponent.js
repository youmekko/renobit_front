/*
실행 조건

1. 시작시
      autoPlay가 true인 경우

      - 롤링에 포함된 페이지 목록에서 현재 열린 페이지 index와 pageInfo를 찾는다.

      - 만약 존재한다면
            index와 pageInfo를 설정 한 후 autoPlay가 true인 경우 자동 실행 시작.

      - 존재하지 않는다면
            index= -1, pageInfo = null, autoPlay = false로 설정


2. 시작 후
      - 존재하는 페이지에서 존재하지 않는 페이지로 이동하면
            index= -1, pageInfo = null, autoPlay = false로 설정

      - 존재하지 않는 페이지에서 존재하는 페이지로 이동하면
            index = 자동 구하기, pageInfo = 자동구학, autoPlay = false


 */
class PageNavigationComponent extends WVDOMComponent {


    get currentIndex() {
        return this.currentIndex;
    }

    get currentPageInfo() {
        return this._currentPageInfo;
    }

    constructor() {
        super();

        this._currentIndex = -1;
        this._isAutoPlaying = false;
        this._timerID = 0;
        this._currentPageInfo = null;
        this._realDataProvider = [];
        this._$info = null;
        this._invalidatePropertyBtnColor = false;
    }

    _onCreateElement() {
        var str = `
                  <div class="page-nav-comp-btn-wrap">
                        <a class="play" nohref><i class="fa fa-play"></i></a>
                        <a class="pause" nohref><i class="fa fa-pause"></i></a>
                        <a class="prev" nohref><i class="fa fa-backward"></i></a>
                        <a class="next" nohref><i class="fa fa-forward"></i></a>
                  </div>
          `;

        $(this._element).append(str);

        this._$info = $(this._element).find(".info");

        this._initEvent();
    }




    _initEvent() {
        if (this.layerName != "masterLayer") {
            $(this._element).html("<p>" + Vue.$i18n.messages.wv.etc.etc08 + "</p>");
            return;
        }


        // editor모드에서는 실행되지 않음.
        if (this.isEditorMode == true)
            return;



        $(this._element).find(".play").click(() => {
            if (this._isAutoPlaying == false) {
                this.play();
            }
        })


        $(this._element).find(".pause").click(() => {
            if (this._isAutoPlaying == true) {
                this.stop();
            }
        })


        $(this._element).find(".prev").click(() => {
            this.prevPage();
        })

        $(this._element).find(".next").click(() => {
            this.nextPage();
        })


    }

    updateStyle() {
        let $el = $(this._element);
        $el.find('style').remove();

        let styleStr = `
                  <style>
                        [id='${this.id}'] .page-nav-comp-btn-wrap a{
                              border-color: ${this.normalColor};
                              color: ${this.normalColor};
                        }

                        [id='${this.id}'] .page-nav-comp-btn-wrap a.active{
                              border-color: ${this.normalColor};
                              background: ${this.normalColor};
                              color: ${this.activeColor};
                        }
                  </style>
            `;
        $el.append(styleStr);
    }

    _onDestroy() {
        this._clearAutoPlay();
        $(this._element).find(".play").off();
        $(this._element).find(".pause").off();

        this._$info = null;
        super._onDestroy();
    }


    getExtensionProperties() {
        return true;
    }


    onLoadPage() {
        // editor모드에서는 실행되지 않음.
        if (this.isEditorMode == true)
            return;

        if (window.wemb.mainPageComponent) {

            //window.wemb.mainPageComponent.eventWatcherBus.$on("loaded", () => {
              window.wemb.mainPageComponent.onWScriptEvent("loaded", () => {

                // 롤링에 하는 페이지 인 경우
                if (this._updateCurrentPageInfo() == true) {
                    this._displayPageInfo();
                    this._replay();
                } else {
                    // 롤링에 속하지 않는 페이지 인 경우
                    this._displayPageInfo();

                }
            })

            //window.wemb.mainPageComponent.eventWatcherBus.$on("beforeUnLoad", () => {
              window.wemb.mainPageComponent.onWScriptEvent("beforeUnLoad", () => {

                this._pause();
            })
        }
    }


    /*
     컴포넌트가 container에 추가되면 자동으로 호출
	*/
    _onImmediateUpdateDisplay() {
        this.updateStyle();
        this._validatePage();

    }


    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this._validatePage);
        }

        if (this._updatePropertiesMap.has("btnStyle")) {
            this.validateCallLater(this.updateStyle);
        }

        if (this._invalidatePropertyBtnColor) {
            this._invalidatePropertyBtnColor = false;
            this.validateCallLater(this.updateStyle);
        }
    }


    _validatePage() {
        // editor모드에서는 실행되지 않음.
        if (this.isEditorMode == true)
            return;


        if (this.layerName != "masterLayer") {
            return;
        }


        let datas = this.getGroupPropertyValue("extension", "dataProvider");

        this._realDataProvider = [];
        datas.forEach((data) => {
            // 페이지 관리자에서 페이지 정보가 구하기.
            let pageInfo = wemb.pageManager.getPageInfoBy(data.id);


            // 페이지 정보가 있을때만 추가.
            if (pageInfo) {
                let rollingPageInfo = {
                    pageName: "",
                    pageID: "",
                    duration: 0
                }
                rollingPageInfo.pageName = pageInfo.name;
                rollingPageInfo.pageID = data.id;
                rollingPageInfo.duration = parseInt(data.duration) * 1000;
                this._realDataProvider.push(rollingPageInfo);
            }
        })



        // 환경설정에 저장한 auto playing 정보
        this._isAutoPlaying = this.getGroupPropertyValue("extension", "autoPlay");
    }






    /**
     □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
     기본 처리 끝.
     □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
     * */


    /**
     □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
     메서드 추가
     □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
     **/



    _updateCurrentPageInfo() {
        let startOpenPageInfo = wemb.pageManager.currentPageInfo;
        startOpenPageInfo.id

        let index = -1;

        for (var i = 0; i < this._realDataProvider.length; i++) {
            let pageInfo = this._realDataProvider[i];

            if (pageInfo.pageID == startOpenPageInfo.id) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            this._currentIndex = -1;
            this._currentPageInfo = null;
            this._isAutoPlaying = false;
            return false;
        } else {
            this._currentIndex = index;
            this._currentPageInfo = this._realDataProvider[index];

            return true;
        }
    }

    _displayPageInfo() {
        if (this._currentPageInfo != null) {
            this._$info.html(this._currentPageInfo.pageName + ", " + (this._currentIndex + 1) + "/" + this._realDataProvider.length);
        } else {
            this._$info.html("");
        }
    }

    _replay() {
        // _isAutoPlaying 값이 true일때만 autoPlay 실행
        if (this._isAutoPlaying == true) {
            this._startAutoPlay();
        }
    }

    _pause() {
        // _isAutoPlaying 값을 건드리지 않고 실행.
        this._clearAutoPlay();
    }

    play() {

        this._isAutoPlaying = true;
        this._startAutoPlay();

    }



    stop() {
        this._isAutoPlaying = false;
        this._clearAutoPlay();
    }

    nextPage() {


        this._clearAutoPlay();

        let nIndex = this._currentIndex + 1;
        if (nIndex >= this._realDataProvider.length)
            nIndex = 0;

        this.gotoPageAt(nIndex);
    }

    prevPage() {


        this._clearAutoPlay();
        let nIndex = this._currentIndex - 1;
        if (nIndex < 0)
            nIndex = this._realDataProvider.length - 1;

        this.gotoPageAt(nIndex);
    }


    gotoPageAt(index) {
        console.log("gotoPageAt ", index, this._realDataProvider.length);
        if (this._realDataProvider == null || this._realDataProvider.length == 0) {
            console.log("데이터가 존재하지 않습니다.");
            return;
        }

        console.log("gotoPageAt 2");
        // 0에서 length-1만큼 index 처리
        if (index < 0) {
            index = 0;
        }


        console.log("gotoPageAt 3");
        if (index >= this._realDataProvider.length) {
            index = this._realDataProvider.length - 1;
        }



        // index에 해당하는 롤링 페이지 정보 구하기.
        this._currentIndex = index;

        this._currentPageInfo = this._realDataProvider[index];

        console.log("gotoPageAt 4", this._currentPageInfo.pageName);
        wemb.pageManager.openPageByName(this._currentPageInfo.pageName);


        if (this._isAutoPlaying) {
            this.play();
        }

    }



    _clearAutoPlay() {
        clearTimeout(this._timerID);
        this._timerID = 0;

        $(this._element).find(".play").removeClass("active");
        $(this._element).find(".pause").addClass("active");

    }


    _startAutoPlay() {
        this._clearAutoPlay();
        $(this._element).find(".play").addClass("active");
        $(this._element).find(".pause").removeClass("active");


        // 만약 설정된게 없다면 3000으로 기본 값 설정하기.
        let duration = this._currentPageInfo == null ? 3000 : this._currentPageInfo.duration;


        this._timerID = setTimeout(() => {
            this.nextPage();
        }, duration);
    }

    set activeColor(color) {
        if (this._checkUpdateGroupPropertyValue("btnStyle", "activeColor", color)) {
            this._invalidatePropertyBtnColor = true;
        }
    }

    get activeColor() {
        return this.getGroupPropertyValue("btnStyle", "activeColor");
    }

    set normalColor(color) {
        if (this._checkUpdateGroupPropertyValue("btnStyle", "normalColor", color)) {
            this._invalidatePropertyBtnColor = true;
        }
    }

    get normalColor() {
        return this.getGroupPropertyValue("btnStyle", "normalColor");
    }

}

WVPropertyManager.attach_default_component_infos(PageNavigationComponent, {
    "info": {
        "version": "1.0.0",
        "componentName": "PageNavigationComponent"
    },

    "setter": {
        "width": 118,
        "height": 28
    },

    "extension": {
        "dataProvider": [],
        "autoPlay": true
    },

    "style": {
        "overflow": "hidden"
    },

    "btnStyle": {
        "activeColor": "#333",
        "normalColor": "#c5c4be"
    }
});


// 이벤트 정보
WVPropertyManager.add_event(PageNavigationComponent, {
    name: "change",
    label: "change",
    description: "페이지가 변경될때 발생하는 이벤트",
    properties: []
});


// 프로퍼티 패널에서 사용할 정보 입니다.
PageNavigationComponent["property_panel_info"] = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    }, {
        label: "Auto Play",
        template: "vertical",
        children: [{
            owner: "extension",
            name: "autoPlay",
            label: "Auto Play",
            type: "checkbox",
            writable: true,
            show: true,
            description: "autoplay"
        }]
    }, {
        label: "Button Color",
        template: "vertical",
        children: [{
            owner: "btnStyle",
            name: "normalColor",
            label: "Normal",
            type: "color",
            writable: true,
            show: true,
            description: "normal color"
        }, {
            owner: "btnStyle",
            name: "activeColor",
            label: "Active",
            type: "color",
            writable: true,
            show: true,
            description: "active color"
        }]
    }
];
