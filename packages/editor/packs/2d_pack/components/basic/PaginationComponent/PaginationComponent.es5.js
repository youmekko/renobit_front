"use strict";

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var PaginationComponent = function(_WVDOMComponent) {
    _inherits(PaginationComponent, _WVDOMComponent);

    function PaginationComponent() {
        _classCallCheck(this, PaginationComponent);

        var _this = _possibleConstructorReturn(this, (PaginationComponent.__proto__ || Object.getPrototypeOf(PaginationComponent)).call(this));

        _this._invalidatePropertyPage = false;
        _this._invalidatePropertyVisiblePage = false;
        _this._invalidatePropertyTotalPage = false;

        _this._currentStartPage = 1;
        _this.app;
        return _this;
    }

    _createClass(PaginationComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            var _this2 = this;

            var self = this;
            var str = "<ul>\n                    <li v-if=\"uiOptions.useFirstLastButton\"\n                        :class=\"{disabled: currentStartPage == 1}\"\n                    >\n                        <a @click=\"onClickHandler('first')\" data-page=\"first-page\" nohref>\n                            <i class=\"el-icon el-icon-d-arrow-left\"></i>\n                        </a>\n                    </li>\n                    <li v-if=\"uiOptions.usePrevNextListButton\"\n                        :class=\"{disabled: currentStartPage == 1}\"\n                    >\n                        <a @click=\"onClickHandler('prev')\" data-page=\"prev-page\" nohref>\n                            <i class=\"el-icon el-icon-arrow-left\"></i>\n                        </a>\n                    </li>\n                    <li v-for=\"(item, index ) in compSetter.visiblePage\" :key=\"index\"                         \n                        v-if=\"(currentStartPage+index) <= compSetter.totalPage\"\n                        :class=\"{active: compSetter.active == (currentStartPage+index)}\"\n                    >\n                        <a @click=\"onClickHandler(currentStartPage+index)\" nohref>{{currentStartPage+index}}</a>\n                    </li>\n                    <li v-if=\"uiOptions.usePrevNextListButton\"\n                        :class=\"{disabled: currentStartPage == lastStartPage}\"\n                    >\n                        <a @click=\"onClickHandler('next')\" data-page=\"next-page\" nohref>\n                                <i class=\"el-icon el-icon-arrow-right\"></i>\n                            </a>\n                    </li>\n                    <li v-if=\"uiOptions.useFirstLastButton\"\n                        :class=\"{disabled: currentStartPage == lastStartPage}\"\n                    >\n                        <a @click=\"onClickHandler('last')\" data-page=\"last-page\" nohref>\n                            <i class=\"el-icon el-icon-d-arrow-right\"></i>\n                        </a>\n                    </li>\n                </ul>\n            ";

            var temp = $(str);
            $(this._element).append(temp);

            this.app = new Vue({
                el: temp.get(0),
                data: function data() {
                    return {
                        compSetter: self.getGroupProperties("setter"),
                        uiOptions: self.getGroupProperties("ui_options")
                    };
                },

                computed: {
                    currentStartPage: function currentStartPage() {
                        var count = Math.ceil(this.compSetter.totalPage / this.compSetter.visiblePage);
                        var checkNum = void 0;
                        for (var i = 1, len = count; i <= len; i++) {
                            checkNum = i * this.compSetter.visiblePage;
                            if (this.compSetter.active <= checkNum) {
                                break;
                            }
                        }
                        return Math.min(checkNum - this.compSetter.visiblePage + 1, this.compSetter.totalPage);
                    },

                    lastStartPage: function lastStartPage() {
                        var p = (Math.ceil(this.compSetter.totalPage / this.compSetter.visiblePage) - 1) * this.compSetter.visiblePage;
                        p += 1;
                        return p;
                    }
                },

                mounted: function mounted() {},

                methods: {
                    onClickHandler: function onClickHandler(pageStr) {
                        var page = void 0;
                        switch (pageStr) {
                            case "next":
                                var nextStart = this.currentStartPage + this.compSetter.visiblePage;
                                if (nextStart > this.lastStartPage) {
                                    page = this.compSetter.active;
                                } else {
                                    page = nextStart;
                                }
                                break;
                            case "prev":
                                if (this.currentStartPage == 1) {
                                    page = this.compSetter.active;
                                } else {
                                    page = Math.max(this.currentStartPage - this.compSetter.visiblePage, 1);
                                }
                                break;
                            case "first":
                                page = 1;
                                break;
                            case "last":
                                page = this.compSetter.totalPage;
                                break;
                            default:
                                page = pageStr;
                        }

                        this.$emit("change", page);
                    }
                }
            });

            this.app.$on("change", function(page) {
                _this2.active = page;
            });
        }
    }, {
        key: "_onCreateProperties",
        value: function _onCreateProperties() {}
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            this.app = null;
            _get(PaginationComponent.prototype.__proto__ || Object.getPrototypeOf(PaginationComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this._validateStyleProperty();
        }
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._invalidatePropertyPage) {
                this._invalidatePropertyPage = false;
                this.validateCallLater(this._validatePageProperty);
            }

            if (this._updatePropertiesMap.has("font") || this._updatePropertiesMap.has("style_options")) {
                this.validateCallLater(this._validateStyleProperty);
            }
        }
    }, {
        key: "_validatePageProperty",
        value: function _validatePageProperty() {
            //dispatch
            this.dispatchWScriptEvent("change", {
                index: this.active
            });
        }
    }, {
        key: "_validateStyleProperty",
        value: function _validateStyleProperty() {
            var font = this.getGroupProperties("font");
            var options = this.getGroupProperties("style_options");

            var styleStr = "\n            <style>\n                [id='" + this.id + "'].PaginationComponent>ul{\n                    justify-content: " + options.align + ";\n                }\n                \n                [id='" + this.id + "'].PaginationComponent>ul li a {\n                    padding-left:" + options.margin + "px;\n                    padding-right:" + options.margin + "px;\n                    font-family: " + font.font_type + ";\n                    font-size: " + font.font_size + ";\n                    color: " + font.font_color + ";\n                    font-weight: " + font.font_weight + ";\n                }\n\n                [id='" + this.id + "'].PaginationComponent>ul li.active>a {\n                    color:" + options.active_color + ";\n                }                \n            </style>\n        ";

            var $el = $(this._element);
            $el.find("style").remove();
            $el.append(styleStr);
        }
    }, {
        key: "visiblePage",
        set: function set(visiblePage) {
            visiblePage = Math.max(visiblePage, 1);
            this._invalidatePropertyVisiblePage = this._checkUpdateGroupPropertyValue("setter", "visiblePage", visiblePage);
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "visiblePage");
        }
    }, {
        key: "totalPage",
        set: function set(totalPage) {
            totalPage = Math.max(totalPage, 1);
            this._invalidatePropertyTotalPage = this._checkUpdateGroupPropertyValue("setter", "totalPage", totalPage);
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "totalPage");
        }
    }, {
        key: "active",
        set: function set(page) {
            page = Math.min(Math.max(page, 1), this.totalPage);
            this._invalidatePropertyPage = this._checkUpdateGroupPropertyValue("setter", "active", page);
        },
        get: function get() {
            return this.getGroupPropertyValue("setter", "active");
        }
    }]);

    return PaginationComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(PaginationComponent, {
    "info": {
        "componentName": "PaginationComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 220,
        "height": 26,
        "totalPage": 22,
        "visiblePage": 5,
        "active": 1
    },

    "ui_options": {
        "usePrevNextListButton": true,
        "useFirstLastButton": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Pagination Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },

    "style_options": {
        "active_color": "#337ab7",
        "align": "center",
        "margin": 6
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
PaginationComponent.property_panel_info = [{
    template: "primary",
    label: "primary"
}, {
    template: "pos-size-2d",
    label: "pos-size-2d"
}, {
    label: "UI",
    template: "vertical",
    children: [{
        owner: "ui_options",
        name: "usePrevNextListButton",
        type: "checkbox",
        label: "prev/next",
        show: true,
        writable: true,
        description: "use prev/next button"
    }, {
        owner: "ui_options",
        name: "useFirstLastButton",
        type: "checkbox",
        label: "first/last",
        show: true,
        writable: true,
        description: "use first/last button"
    }]
}, {
    label: "Page Options",
    template: "vertical",
    children: [{
        owner: "setter",
        name: "active",
        type: "number",
        label: "active",
        show: true,
        writable: true,
        description: "active page length"
    }, {
        owner: "setter",
        name: "visiblePage",
        type: "number",
        label: "visible",
        show: true,
        writable: true,
        description: "visible page length"
    }, {
        owner: "setter",
        name: "totalPage",
        type: "number",
        label: "total",
        show: true,
        writable: true,
        description: "total page length"
    }]
}, {
    template: "font",
    label: "font"
}, {
    label: "Style",
    template: "vertical",
    children: [{
        owner: "style_options",
        name: "active_color",
        type: "color",
        label: "Active",
        show: true,
        writable: true,
        description: "active color"
    }, {
        owner: "style_options",
        name: "align",
        type: "select",
        options: {
            items: [{ label: "left", value: "flex-start" }, { label: "center", value: "center" }, { label: "right", value: "flex-end" }]
        },
        label: "Align",
        show: true,
        writable: true,
        description: "align"
    }, {
        owner: "style_options",
        name: "margin",
        type: "number",
        label: "Margin",
        show: true,
        writable: true,
        description: "margin"
    }]
}];

// 이벤트 정보
WVPropertyManager.add_event(PaginationComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.remove_property_group_info(PaginationComponent, "label");
WVPropertyManager.remove_property_group_info(PaginationComponent, "background");


WVPropertyManager.add_property_group_info(PaginationComponent, {
    label: "PaginationComponent 고유 속성",
    children: [{
        name: "visiblePage",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: 5,
        description: "화면에 구성할 페이지번호 개수 입니다."
    }, {
        name: "totalPage",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: 10,
        description: "전체 페이지 수 입니다."
    }, {
        name: "active",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: 1,
        description: "active할 페이지 index입니다."
    }]
});
