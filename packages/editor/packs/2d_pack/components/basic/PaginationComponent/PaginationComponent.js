class PaginationComponent extends WVDOMComponent {
    constructor() {
        super();
        this._invalidatePropertyPage = false;
        this._invalidatePropertyVisiblePage = false;
        this._invalidatePropertyTotalPage = false;

        this._currentStartPage = 1;
        this.app;
    }


    _onCreateElement() {
        let self = this;
        let str =
            `<ul>
                    <li v-if="uiOptions.useFirstLastButton"
                        :class="{disabled: currentStartPage == 1}"
                    >
                        <a @click="onClickHandler('first')" data-page="first-page" nohref>
                            <i class="el-icon el-icon-d-arrow-left"></i>
                        </a>
                    </li>
                    <li v-if="uiOptions.usePrevNextListButton"
                        :class="{disabled: currentStartPage == 1}"
                    >
                        <a @click="onClickHandler('prev')" data-page="prev-page" nohref>
                            <i class="el-icon el-icon-arrow-left"></i>
                        </a>
                    </li>
                    <li v-for="(item, index ) in compSetter.visiblePage" :key="index"                         
                        v-if="(currentStartPage+index) <= compSetter.totalPage"
                        :class="{active: compSetter.active == (currentStartPage+index)}"
                    >
                        <a @click="onClickHandler(currentStartPage+index)" nohref>{{currentStartPage+index}}</a>
                    </li>
                    <li v-if="uiOptions.usePrevNextListButton"
                        :class="{disabled: currentStartPage == lastStartPage}"
                    >
                        <a @click="onClickHandler('next')" data-page="next-page" nohref>
                                <i class="el-icon el-icon-arrow-right"></i>
                            </a>
                    </li>
                    <li v-if="uiOptions.useFirstLastButton"
                        :class="{disabled: currentStartPage == lastStartPage}"
                    >
                        <a @click="onClickHandler('last')" data-page="last-page" nohref>
                            <i class="el-icon el-icon-d-arrow-right"></i>
                        </a>
                    </li>
                </ul>
            `

        let temp = $(str);
        $(this._element).append(temp);

        this.app = new Vue({
            el: temp.get(0),
            data: function() {
                return {
                    compSetter: self.getGroupProperties("setter"),
                    uiOptions: self.getGroupProperties("ui_options")
                }
            },

            computed: {
                currentStartPage: function() {
                    let count = Math.ceil(this.compSetter.totalPage / this.compSetter.visiblePage);
                    let checkNum;
                    for (let i = 1, len = count; i <= len; i++) {
                        checkNum = (i * this.compSetter.visiblePage); 
                        if (this.compSetter.active <= checkNum) {
                            break;
                        }
                    }
                    return Math.min(checkNum - this.compSetter.visiblePage + 1, this.compSetter.totalPage);
                },

                lastStartPage: function() {
                    let p = (Math.ceil(this.compSetter.totalPage / this.compSetter.visiblePage) - 1) * this.compSetter.visiblePage;
                    p += 1;
                    return p;
                }
            },

            mounted() {

            },
            methods: {
                onClickHandler(pageStr) {
                    let page;
                    switch (pageStr) {
                        case "next":
                            var nextStart = this.currentStartPage + this.compSetter.visiblePage;
                            if (nextStart > this.lastStartPage) {
                                page = this.compSetter.active;
                            } else {
                                page = nextStart;
                            }
                            break;
                        case "prev":
                            if (this.currentStartPage == 1) {
                                page = this.compSetter.active;
                            } else {
                                page = Math.max(this.currentStartPage - this.compSetter.visiblePage, 1); 
                            }
                            break;
                        case "first":
                            page = 1;
                            break;
                        case "last":
                            page = this.compSetter.totalPage;
                            break;
                        default:
                            page = pageStr;
                    }

                    this.$emit("change", page);
                }

            }
        })

        this.app.$on("change", (page) => {
            this.active = page;
        })
    }



    _onCreateProperties() {}

    _onDestroy() {
        this.app = null;
        super._onDestroy();
    }

    _onImmediateUpdateDisplay() {
        this._validateStyleProperty();
    }

    onLoadPage() {

    }

    _onCommitProperties() {
        if (this._invalidatePropertyPage) {
            this._invalidatePropertyPage = false;
            this.validateCallLater(this._validatePageProperty)
        }

        if (this._updatePropertiesMap.has("font") || this._updatePropertiesMap.has("style_options")) {
            this.validateCallLater(this._validateStyleProperty)
        }
    }


    _validatePageProperty() {
        //dispatch
        this.dispatchWScriptEvent("change", {
            index: this.active
        })
    }

    _validateStyleProperty() {
        let font = this.getGroupProperties("font");
        let options = this.getGroupProperties("style_options");


        let styleStr = `
            <style>
                [id='${this.id}'].PaginationComponent>ul{
                    justify-content: ${options.align};
                }
                
                [id='${this.id}'].PaginationComponent>ul li a {
                    padding-left:${options.margin}px;
                    padding-right:${options.margin}px;
                    font-family: ${font.font_type};
                    font-size: ${font.font_size};
                    color: ${font.font_color};
                    font-weight: ${font.font_weight};
                }

                [id='${this.id}'].PaginationComponent>ul li.active>a {
                    color:${options.active_color};
                }                
            </style>
        `;

        let $el = $(this._element);
        $el.find("style").remove();
        $el.append(styleStr);
    }

    set visiblePage(visiblePage) {
        visiblePage = Math.max(visiblePage, 1);
        this._invalidatePropertyVisiblePage = this._checkUpdateGroupPropertyValue("setter", "visiblePage", visiblePage);
    }

    get visiblePage() {
        return this.getGroupPropertyValue("setter", "visiblePage");
    }

    set totalPage(totalPage) {
        totalPage = Math.max(totalPage, 1);
        this._invalidatePropertyTotalPage = this._checkUpdateGroupPropertyValue("setter", "totalPage", totalPage);
    }

    get totalPage() {
        return this.getGroupPropertyValue("setter", "totalPage");
    }

    set active(page) {
        page = Math.min(Math.max(page, 1), this.totalPage);
        this._invalidatePropertyPage = this._checkUpdateGroupPropertyValue("setter", "active", page);
    }

    get active() {
        return this.getGroupPropertyValue("setter", "active");
    }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(PaginationComponent, {
    "info": {
        "componentName": "PaginationComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 220,
        "height": 26,
        "totalPage": 22,
        "visiblePage": 5,
        "active": 1
    },

    "ui_options": {
        "usePrevNextListButton": true,
        "useFirstLastButton": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Pagination Component"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#333333",
        "font_size": 14,
        "font_weight": "normal"
    },

    "style_options": {
        "active_color": "#337ab7",
        "align": "center",
        "margin": 6
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
PaginationComponent.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    }, {
        label: "UI",
        template: "vertical",
        children: [{
            owner: "ui_options",
            name: "usePrevNextListButton",
            type: "checkbox",
            label: "prev/next",
            show: true,
            writable: true,
            description: "use prev/next button"
        }, {
            owner: "ui_options",
            name: "useFirstLastButton",
            type: "checkbox",
            label: "first/last",
            show: true,
            writable: true,
            description: "use first/last button"
        }]
    },
    {
        label: "Page Options",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "active",
            type: "number",
            label: "active",
            show: true,
            writable: true,
            description: "active page length"
        }, {
            owner: "setter",
            name: "visiblePage",
            type: "number",
            label: "visible",
            show: true,
            writable: true,
            description: "visible page length"
        }, {
            owner: "setter",
            name: "totalPage",
            type: "number",
            label: "total",
            show: true,
            writable: true,
            description: "total page length"
        }]
    },
    {
        template: "font",
        label: "font"
    },
    {
        label: "Style",
        template: "vertical",
        children: [{
            owner: "style_options",
            name: "active_color",
            type: "color",
            label: "Active",
            show: true,
            writable: true,
            description: "active color"
        }, {
            owner: "style_options",
            name: "align",
            type: "select",
            options: {
                items: [{ label: "left", value: "flex-start" },
                    { label: "center", value: "center" },
                    { label: "right", value: "flex-end" }
                ]
            },
            label: "Align",
            show: true,
            writable: true,
            description: "align"
        }, {
            owner: "style_options",
            name: "margin",
            type: "number",
            label: "Margin",
            show: true,
            writable: true,
            description: "margin"
        }]
    }
];


WVPropertyManager.remove_property_group_info(PaginationComponent, "label");
WVPropertyManager.remove_property_group_info(PaginationComponent, "background");

// 이벤트 정보
WVPropertyManager.add_event(PaginationComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});


WVPropertyManager.add_property_group_info(PaginationComponent, {
    label: "PaginationComponent 고유 속성",
    children: [{
        name: "visiblePage",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: 5,
        description: "화면에 구성할 페이지번호 개수 입니다."
    }, {
        name: "totalPage",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: 10,
        description: "전체 페이지 수 입니다."
    }, {
        name: "active",
        type: "Number",
        show: true,
        writable: true,
        defaultValue: 1,
        description: "active할 페이지 index입니다."
    }]
});
