"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FrameComponent = function(_WVDOMComponent) {
    _inherits(FrameComponent, _WVDOMComponent);

    function FrameComponent() {
        _classCallCheck(this, FrameComponent);

        return _possibleConstructorReturn(this, (FrameComponent.__proto__ || Object.getPrototypeOf(FrameComponent)).call(this));
    }

    _createClass(FrameComponent, [{
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.updateBackground();
        }
    }, {
        key: "_onCreateProperties",
        value: function _onCreateProperties() {
            /*이전버전 호환처리*/
            if (this.getGroupPropertyValue("background", "type") == "") {
                this.setGroupPropertyValue("background", "type", "solid");
                var color = this.getGroupPropertyValue("style", "backgroundColor");
                if (color) {
                    this.setGroupPropertyValue("background", "color1", color);
                }
            }
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("background")) {
                this.validateCallLater(this.updateBackground);
            }
        }
    }, {
        key: "updateBackground",
        value: function updateBackground() {
            var bgData = this.getGroupProperties("background");
            var style = this._styleManager.getBackgroundStyle(bgData, "[id='" + this.id + "']");
            $(this._element).find("style").remove();
            $(this._element).append(style);
        }
    }]);

    return FrameComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(FrameComponent, {
    "info": {
        "componentName": "FrameComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100
    },

    "label": {
        "label_using": "N",
        "label_text": "Frame Component"
    },

    "background": {
        "type": "",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#eee",
        "color2": "#000",
        "text": ''
    },

    "style": {
        "border": "1px solid #000000",
        "backgroundColor": "#eee",
        "borderRadius": 1,
        "cursor": "default"
    }
});

FrameComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}, {
    template: "background-gradient",
    owner: "background",
    label: "Background"
}, {
    template: "border"
}];
