class ImageLoaderComponent extends WVDOMComponent {
      constructor() {
            super();
            this.$image = null;
            this.isSettingsReady = false;
            this._isPageLoaded = false;
            this._isResourceComponent = true;
      }

      /*
      _onCreateElement() {
          this.loadImage();
      }
      */

      _onDestroy() {
            if (this.$image != null) {
                  this.$image.off();
                  this.$image.remove();
                  this.$image = null;
            }

            super._onDestroy();
      }

      loadImage() {
            this._loadImage().then(() => {

            }, (error) => {
                  this._setNoImageStyle();
                  this.isSettingsReady = true;
            })
      }

      _loadImage() {
            return new Promise((resolve, reject) => {
                  /*if (!this.getGroupPropertyValue("setter", "visible")) {
                      this.isSettingsReady = true;
                      resolve(false);
                      return;
                  }*/

                  if (this.$image == null) {
                        $(this._element).append('<img>');
                        this.$image = $(this._element).find("img");
                        this.$image.attr("draggable", false);

                        /*
                        isCompleted = true인 경우 페이지 로딩 후 활성화 된 상태
                         */
                        let startVisible = (this._isPageLoaded == true || this.isEditorMode) ? "visible" : "hidden";


                        this.$image.css({
                              "width": "100%",
                              "height": "100%",
                              "visibility": startVisible
                        });
                  }

                  if (!this.extension.selectImage) {
                        reject("error");
                  } else {

                        /*
                                기존 버전 호환성 처리
                                 */
                        var srcName = this.extension.selectImage;
                        if (typeof srcName == "object") {
                              srcName = this.extension.selectImage.path;
                        }

                        var self = this;
                        this.$image.off("error").on("error", function () {
                              reject("error");

                        }).attr("src", wemb.configManager.serverUrl + srcName);

                        this.$image.off("load").on("load", function () {
                              if (self.$noImageContainer) {
                                    self.$image.detach();
                                    self.$noImageContainer.remove();
                                    self.$noImageContainer = null;
                                    $(self._element).append(self.$image);
                              }
                              self.$image.css({"max-width": "none", "max-height": "none"});
                              $(self._element).removeClass("no-data-comp-bg");

                              if (self.isSettingsReady) {
                                    //생성시 로드를 제외하고 새로 로드시 이미지 원본 사이즈로 셋팅
                                    self.$image.width("auto");
                                    self.$image.height("auto");


                                    //setTimeout(() => {
                                    self.width = parseInt(self.$image.width());
                                    self.height = parseInt(self.$image.height());
                                    if (self.isEditorMode) {
                                          self.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
                                    }
                              }

                              /*
                                  isCompleted = true인 경우 페이지 로딩 후 활성화 된 상태
                                  */
                              let startVisible = (self._isPageLoaded == true || self.isEditorMode || self.isViewerMode) ? "visible" : "hidden";
                              self.$image.css({
                                    'width': '100%',
                                    'height': '100%',
                                    "visibility": startVisible
                              });

                              self.isSettingsReady = true;

                              resolve(true);
                        })
                  }
            });
      }

      _setNoImageStyle() {
            this.$image.off();
            $(this._element).addClass("no-data-comp-bg");
            if (this.$noImageContainer) {
                  this.$noImageContainer.remove();
            }

            var image = this.$image.detach();
            this.$noImageContainer = $('<div style="overflow: hidden; height: 100%; display: flex; align-items: center; justify-content: center; "></div>');
            this.$noImageContainer.append(image);
            $(this._element).append(this.$noImageContainer);

            this.$image.removeAttr("style").attr("src", ImageLoaderComponent.NO_IMAGE);
            this.$image.css({'maxWidth': '50px', 'width': '100%', 'maxHeight': '40px', 'height': '100%'});
      }

      _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension")) {
                  this.validateCallLater(this.loadImage)
            }

            /*if (this.invalidateVisible) {
                this.validateCallLater(this.loadImage);
            }*/

      }


      get extension() {
            return this.getGroupProperties("extension");
      }


      async startLoadResource() {
            try {
                  /**
                   마스터 영역에서 리소스 재로드하는 이슈 때문에 플래그값 재설정.
                   **/
                  this.isSettingsReady = false;
                  let success = await this.loadImage();
            } catch (error) {
                  console.log("error ", error);
            } finally {
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }
      }


      onLoadPage() {
            this.$image.css("visibility", "visible");
            this._isPageLoaded = true;
      }
}

ImageLoaderComponent.NO_IMAGE = 'data:image/jpg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMxaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzEzOCA3OS4xNTk4MjQsIDIwMTYvMDkvMTQtMDE6MDk6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE3IChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMwNzNBMzY5MjRDRjExRTg4OUI5Q0E5NENBNEUxNkYyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMwNzNBMzZBMjRDRjExRTg4OUI5Q0E5NENBNEUxNkYyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzA3M0EzNjcyNENGMTFFODg5QjlDQTk0Q0E0RTE2RjIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzA3M0EzNjgyNENGMTFFODg5QjlDQTk0Q0E0RTE2RjIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCAAoADIDAREAAhEBAxEB/8QAjAAAAwADAQAAAAAAAAAAAAAAAAUHAQMGBAEAAwEBAQAAAAAAAAAAAAAAAAMFBAECEAABAwICCAIGCwAAAAAAAAACAQMEAAYRBSExEtITo1QWQXHwUWGBIhWx0eEyYiODs6RVNhEAAgEDAgUDBQEAAAAAAAAAAAECUQMUIbERMUESBIGhIvAyUhMjM//aAAwDAQACEQMRAD8Au9rWtkM7IYsqVF4j7nE2z4jg47LhCmgSRNSVu8jyJxm0noY7FiEoJtDbsi1+i5ru/Scq5XYbjQoHZFr9FzXd+jKuV2DGhQOyLX6Lmu79GVcrsGNCgdkWv0XNd36Mq5XYMaFA7Itfoua7v0ZVyuwY0KEpquSyrWR/l4X6v7p1I8r/AEf10KnjfYhnHzODJlPxWHhN+NhxgTwx+n20qUGkm+o1TTfBdD014PQUAcVIHKJM/NSz2STMqO4SQwUyDYZRMWyaFFTaJa3x7lGPYtHzMT7W33vU6O23pj2Rw3ZmKyCDElLWqYrsqvmOC1lvpKb4cjTZbcVxI/VokHc29PnS8kiZNlaq26KGs2YqaGQJ0lRB9Zkmr0wwXoKM3KXoqm21JuKjH1Y3k24WXjElZG2izInwuNkSJx2yX40MlwTHxx+ykxvd3FT5P2HSs9vBx5r3MuZ3nsE25GbQ2m8uNdlw2CJw2lXUR+Gz5VxWoS0i/kDuSjrJaDVvOsmcMQbnxzMlwERdBVVV8ETGku1JdGNVyNUbn4MKQYuPx23TD7hmAkqeSqmiuKbXJnXFPmjdXk9ENq+RCqWM2A2zFIRRCNXCNUTSqo4SYr7kRKkeU/6MqeMvgh9WceYMAcAgMUMCRUISTFFRdaKi0Jg0L37cyF5omigMChJgpA2IEnkQoipTVemnzYt2YPojwNt3LlK8CO183hYfk7bgtPN/hIi0ElMbtz1fxYtKcNF8kbPm90f0H8tr6q5+u3+Xszv7J/j7kpquSygWtdOQwchixZUrhvt8TbDhuFhtOESaRFU1LU7yPHnKbaWhvsX4Rgk2Nu97X63lO7lJxblNhuTCod72v1vKd3KMW5TYMmFQ73tfreU7uUYtymwZMKh3va/W8p3coxblNgyYVDve1+t5Tu5Ri3KbBkwqSmq5LP/Z';


////////////////////////////////////////////////
// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(ImageLoaderComponent, {
      "info": {
            "componentName": "ImageLoaderComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 100,
            "height": 100,
      },
      "label": {
            "label_using": "N",
            "label_text": "Image Loader"
      },

      "style": {
            "border": "1px none #808080",
            "borderRadius": "0",
            "cursor": "default"
      },

      "extension": {
            "selectImage": null
      }
});


WVPropertyManager.add_property_panel_group_info(ImageLoaderComponent, {
      label: "리소스 설정",
      template: "resource",
      children: [{
            owner: "extension",
            name: "selectImage",
            type: "resource",
            label: "Resource",
            resource_options: {
                  minLength: 1,
                  maxLenght: 1,
                  type: "image"
            },
            show: true,
            writable: true,
            description: "이미지 리소스 선택"
      }]
});
