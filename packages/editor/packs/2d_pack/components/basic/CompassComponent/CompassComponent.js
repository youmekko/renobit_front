class CompassComponent extends WVDOMComponent {

      constructor() {
            super();
            this._iRenderer;
            this._iScene;
            this._iCamera;
            this._enabled;
            this._objHolder;
            this._objOrientation;
      }

      _onCreateProperties() {
            super._onCreateProperties();
            this._isResourceComponent = true;
      }


      _onCreateElement() {
            if (wemb.configManager.using3D) {
                  this._iRenderer = new THREE.WebGLRenderer({alpha: true});
                  this._iRenderer.setClearColor(0x000000, 0);
                  this._iRenderer.setSize(this.width, this.height);

                  this._element.appendChild(this._iRenderer.domElement);

                  this._iScene = new THREE.Scene();
                  this._iCamera = new THREE.PerspectiveCamera(60, this.width / this.height, 0.1, 1000);
                  this._iCamera.up = wemb.threeElements.camera.up; // 메인 카메라의 업벡터 동기화
                  var ambientLight = new THREE.AmbientLight(0xffffff);
                  this._iScene.add(ambientLight);

                  this._objHolder = new THREE.Object3D();
                  this._objHolder.scale.set(5,5,5);
                  this._iScene.add(this._objHolder);
            }
            $(this.element).css({
                  "background-image": "url('client/common/assets/orientation/bg_orientation.png')",
                  "background-repeat":"no-repeat",
                  "background-size":"contain",
                  "pointer-events": "none"
            });
      }

      /*transformMinimumScale() {
            let ow = this.getDefaultProperties().setter.width;
            return (ow/this._transform.width);
      }*/

      _validateResource() {

            var newTextureUrl = 'client/common/assets/orientation/orientation.png';
            var newObjUrl = 'client/common/assets/orientation/orientation_ET.obj';
            var image = document.createElement("img");
            image.src = newTextureUrl;
            return new Promise((resolve) => {
                  image.onload = (event) => {
                        let loader = new THREE.OBJLoader();
                        let texture = new THREE.Texture(image);
                        texture.needsUpdate = true;
                        loader.load(
                              newObjUrl,
                              (objObject) => {
                                    objObject.traverse(function (child) {
                                          if (child instanceof THREE.Mesh) {
                                                child.material.map = texture;
                                          }
                                    });
                                    objObject.name = "obj_orientation";
                                    resolve([null, objObject]);
                              }, undefined, (error) => {

                                    resolve([error, null]);
                              }
                        );
                  }
            });

      }

      _validateSize() {
            super._validateSize();
            if(wemb.configManager.using3D){
                  this._iRenderer.setSize(this.width, this.height);
                  this._iCamera.aspect = this.width/this.height;
                  this._iCamera.updateProjectionMatrix();
            }
      }


      async startLoadResource() {
            try{
                  if(wemb.configManager.using3D){
                        let [error, loadedObj] = await this._validateResource();
                        if(loadedObj){
                              this._objOrientation = loadedObj;
                              this._objHolder.add(this._objOrientation);
                              this.startRender();
                        }
                  }
            }catch(e){
                  CPLogger.log("compassComponent", e.toString());
            } finally {
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }
      }

      startRender(){
            this.animationID = window.requestAnimationFrame(()=>{
                  this.render();
                  this.startRender();
            });
      }

      stopRender(){
            window.cancelAnimationFrame(this.animationID);
      }

      render(){
            if( this._objOrientation != null ){

                  this._iCamera.rotation.copy(wemb.threeElements.camera.rotation);
                  this._iCamera.position.copy(wemb.threeElements.camera.position);

                  if(!wemb.threeElements.personControls.enabled){

                        this._iCamera.position.sub(wemb.threeElements.mainControls.target);
                        this._iCamera.position.x*=-1;
                  } else {
                        this._objHolder.rotation.set(0, wemb.threeElements.personControls.rotateY, 0);
                        this._objOrientation.rotation.set(wemb.threeElements.personControls.rotateX, 0, 0 );
                  }
                  this._iCamera.position.setLength(10);
                  this._iCamera.lookAt( this._iScene.position );
            }
            this._iRenderer.render( this._iScene, this._iCamera );
      }

      _onDestroy() {
            if(wemb.configManager.using3D){
                  this.stopRender();
                  this._objHolder.remove(this._objOrientation);
                  MeshManager.disposeMesh(this._objOrientation);
                  this._iRenderer.dispose();
                  this._iCamera   = null;
                  this._iRenderer = null;
                  this._iScene    = null;
                  this._objHolder = null;
                  this._objOrientation = null;
            }
            super._onDestroy();
      }

}


WVPropertyManager.attach_default_component_infos(CompassComponent, {
      "info": {
            "componentName": "CompassComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 106,
            "height": 106
      },
      "label": {
            "label_using": "N",
            "label_text": "CompassComponent"
      }
});

CompassComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}];
