"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompassComponent = function (_WVDOMComponent) {
      _inherits(CompassComponent, _WVDOMComponent);

      function CompassComponent() {
            _classCallCheck(this, CompassComponent);

            var _this = _possibleConstructorReturn(this, (CompassComponent.__proto__ || Object.getPrototypeOf(CompassComponent)).call(this));

            _this._iRenderer;
            _this._iScene;
            _this._iCamera;
            _this._enabled;
            _this._objHolder;
            _this._objOrientation;
            return _this;
      }

      _createClass(CompassComponent, [{
            key: "_onCreateProperties",
            value: function _onCreateProperties() {
                  _get(CompassComponent.prototype.__proto__ || Object.getPrototypeOf(CompassComponent.prototype), "_onCreateProperties", this).call(this);
                  this._isResourceComponent = true;
            }
      }, {
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  if (wemb.configManager.using3D) {
                        this._iRenderer = new THREE.WebGLRenderer({ alpha: true });
                        this._iRenderer.setClearColor(0x000000, 0);
                        this._iRenderer.setSize(this.width, this.height);

                        this._element.appendChild(this._iRenderer.domElement);

                        this._iScene = new THREE.Scene();
                        this._iCamera = new THREE.PerspectiveCamera(60, this.width / this.height, 0.1, 1000);
                        this._iCamera.up = wemb.threeElements.camera.up; // 메인 카메라의 업벡터 동기화
                        var ambientLight = new THREE.AmbientLight(0xffffff);
                        this._iScene.add(ambientLight);

                        this._objHolder = new THREE.Object3D();
                        this._objHolder.scale.set(5, 5, 5);
                        this._iScene.add(this._objHolder);
                  }
                  $(this.element).css({
                        "background-image": "url('client/common/assets/orientation/bg_orientation.png')",
                        "background-repeat": "no-repeat",
                        "background-size": "contain",
                        "pointer-events": "none"
                  });
            }

            /*transformMinimumScale() {
                  let ow = this.getDefaultProperties().setter.width;
                  return (ow/this._transform.width);
            }*/

      }, {
            key: "_validateResource",
            value: function _validateResource() {

                  var newTextureUrl = 'client/common/assets/orientation/orientation.png';
                  var newObjUrl = 'client/common/assets/orientation/orientation_ET.obj';
                  var image = document.createElement("img");
                  image.src = newTextureUrl;
                  return new Promise(function (resolve) {
                        image.onload = function (event) {
                              var loader = new THREE.OBJLoader();
                              var texture = new THREE.Texture(image);
                              texture.needsUpdate = true;
                              loader.load(newObjUrl, function (objObject) {
                                    objObject.traverse(function (child) {
                                          if (child instanceof THREE.Mesh) {
                                                child.material.map = texture;
                                          }
                                    });
                                    objObject.name = "obj_orientation";
                                    resolve([null, objObject]);
                              }, undefined, function (error) {

                                    resolve([error, null]);
                              });
                        };
                  });
            }
      }, {
            key: "_validateSize",
            value: function _validateSize() {
                  _get(CompassComponent.prototype.__proto__ || Object.getPrototypeOf(CompassComponent.prototype), "_validateSize", this).call(this);
                  if (wemb.configManager.using3D) {
                        this._iRenderer.setSize(this.width, this.height);
                        this._iCamera.aspect = this.width / this.height;
                        this._iCamera.updateProjectionMatrix();
                  }
            }
      }, {
            key: "startLoadResource",
            value: function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        var _ref2, _ref3, error, loadedObj;

                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                _context.prev = 0;

                                                if (!wemb.configManager.using3D) {
                                                      _context.next = 9;
                                                      break;
                                                }

                                                _context.next = 4;
                                                return this._validateResource();

                                          case 4:
                                                _ref2 = _context.sent;
                                                _ref3 = _slicedToArray(_ref2, 2);
                                                error = _ref3[0];
                                                loadedObj = _ref3[1];

                                                if (loadedObj) {
                                                      this._objOrientation = loadedObj;
                                                      this._objHolder.add(this._objOrientation);
                                                      this.startRender();
                                                }

                                          case 9:
                                                _context.next = 14;
                                                break;

                                          case 11:
                                                _context.prev = 11;
                                                _context.t0 = _context["catch"](0);

                                                CPLogger.log("compassComponent", _context.t0.toString());

                                          case 14:
                                                _context.prev = 14;

                                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                return _context.finish(14);

                                          case 17:
                                          case "end":
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this, [[0, 11, 14, 17]]);
                  }));

                  function startLoadResource() {
                        return _ref.apply(this, arguments);
                  }

                  return startLoadResource;
            }()
      }, {
            key: "startRender",
            value: function startRender() {
                  var _this2 = this;

                  this.animationID = window.requestAnimationFrame(function () {
                        _this2.render();
                        _this2.startRender();
                  });
            }
      }, {
            key: "stopRender",
            value: function stopRender() {
                  window.cancelAnimationFrame(this.animationID);
            }
      }, {
            key: "render",
            value: function render() {
                  if (this._objOrientation != null) {

                        this._iCamera.rotation.copy(wemb.threeElements.camera.rotation);
                        this._iCamera.position.copy(wemb.threeElements.camera.position);

                        if (!wemb.threeElements.personControls.enabled) {

                              this._iCamera.position.sub(wemb.threeElements.mainControls.target);
                              this._iCamera.position.x *= -1;
                        } else {
                              this._objHolder.rotation.set(0, wemb.threeElements.personControls.rotateY, 0);
                              this._objOrientation.rotation.set(wemb.threeElements.personControls.rotateX, 0, 0);
                        }
                        this._iCamera.position.setLength(10);
                        this._iCamera.lookAt(this._iScene.position);
                  }
                  this._iRenderer.render(this._iScene, this._iCamera);
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  if (wemb.configManager.using3D) {
                        this.stopRender();
                        this._objHolder.remove(this._objOrientation);
                        MeshManager.disposeMesh(this._objOrientation);
                        this._iRenderer.dispose();
                        this._iCamera = null;
                        this._iRenderer = null;
                        this._iScene = null;
                        this._objHolder = null;
                        this._objOrientation = null;
                  }
                  _get(CompassComponent.prototype.__proto__ || Object.getPrototypeOf(CompassComponent.prototype), "_onDestroy", this).call(this);
            }
      }]);

      return CompassComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(CompassComponent, {
      "info": {
            "componentName": "CompassComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 106,
            "height": 106
      },
      "label": {
            "label_using": "N",
            "label_text": "CompassComponent"
      }
});

CompassComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}];
