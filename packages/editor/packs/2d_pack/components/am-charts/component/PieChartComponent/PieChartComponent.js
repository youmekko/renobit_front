class PieChartComponent extends ChartCoreComponent {
    constructor() {
        super();
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateElement() {
        super._onCreateElement();
        this._element.classList.add('pie-chart-component');
    }

    setChartConfig() {
        var config = this.chartConfig;
        this.chart.valueField = config.valueField;
        this.chart.titleField = config.titleField;
        this.chart.balloonText = config.balloonText;

        if (this.showPreview) {
            this.chart.dataProvider = this.previewData;
        }

        this.chart.validateData();
    }
}

// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(PieChartComponent, {
    "info": {
        "componentName": "PieChartComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "autoExecute": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Pie Chart"
    },
    "extension": {
        "chartConfig": {
            "valueField": '',
            "titleField": ''
        },
        "datasetId": null,
        "styleConfig": null,
        "initConfig": {
            "fontFamily": "NanumSquareR",
            "type": "pie",
            "theme": "light",
            "titles": [{
                "text": "Chart Title",
                "size": 15,
                "color": "#000000",
                "bold": true
            }],
            "legend": {},
            "labelText": "[[title]]",
            "startDuration": 0
        },
        "showPreview": false
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
PieChartComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Preview",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "showPreview",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "미리보기 설정",
        options: {
            label: "사용"
        }
    }],
}, {
    label: "Auto Execute",
    children: [{
        owner: "setter",
        name: "autoExecute",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "자동실행 설정"
    }]
}];


//  추후 추가 예정
PieChartComponent.method_info = [];

// 이벤트 정보
WVPropertyManager.add_event(PieChartComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});



WVPropertyManager.add_property_group_info(PieChartComponent, {
    label: "PieChartComponent 고유 속성",
    children: [{
        name: "datasetId",
        type: "string",
        show: true,
        writable: false,
        description: "설정한 데이터셋의 고유 id입니다."
    }, {
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        description: "Chart를 구성할 데이터(row data)입니다."
    }]
});


WVPropertyManager.add_method_info(PieChartComponent, {
    name: "execute",
    description: "설정한 데이터셋을 호출하여 Chart를 업데이트합니다.",
    params: [{
        name: "params",
        type: "object",
        description: "설정한 데이터셋의 parameters입니다."
    }]
});
