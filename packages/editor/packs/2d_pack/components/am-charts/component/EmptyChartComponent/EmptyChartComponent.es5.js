"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var EmptyChartComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(EmptyChartComponent, _WVDOMComponent);

            function EmptyChartComponent() {
                  var _this;

                  _classCallCheck(this, EmptyChartComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(EmptyChartComponent).call(this));
                  _this._isAdded = false;
                  _this._chart = null;
                  _this._config = null;
                  _this.chartId = "";
                  _this._dataProvider = [];
                  _this._invaldatePreivewProperty = false;
                  return _this;
            }

            _createClass(EmptyChartComponent, [{
                  key: "getExtensionProperties",
                  value: function getExtensionProperties() {
                        return true;
                  }
            }, {
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        this.chartId = "id-" + this.id;
                        var config = this.getParseConfigData();

                        if (config) {
                              this.config = config;
                        }
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        $(this._element).append('<div style="width:100%;height:100%;" class="chart-area" id="' + this.chartId + '"></div><span class="txt">NO DATA</span>');
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        this._isAdded = true;
                        this.setPreviewMode();
                  }
            }, {
                  key: "onLoadPage",
                  value: function onLoadPage() {}
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._updatePropertiesMap.has("extension")) {
                              this.validateCallLater(this._updateChartConfig);
                        }

                        if (this._updatePropertiesMap.has("preview")) {
                              this.validateCallLater(this.setPreviewMode);
                        }
                  }
            }, {
                  key: "setPreviewMode",
                  value: function setPreviewMode() {
                        if (this.isEditorMode && !this.getGroupPropertyValue("preview", "use")) {
                              $(this._element).removeClass("no-data").addClass("editor");

                              if (this._chart) {
                                    this.clearChart();
                              }
                        } else {
                              $(this._element).removeClass("editor");
                              this.render();
                        }
                  }
            }, {
                  key: "_updateChartConfig",
                  value: function _updateChartConfig() {
                        this.clearChart();
                        var config = this.getParseConfigData();

                        if (config) {
                              this.config = config;
                              this.render();
                        } else {
                              this.dataProvider = null;
                        }
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.clearChart();

                        _get(_getPrototypeOf(EmptyChartComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "clearChart",
                  value: function clearChart() {
                        if (this._chart) {
                              this._chart.destroy();

                              this._chart.clear();

                              this._chart = null;
                        }
                  }
            }, {
                  key: "makeChart",
                  value: function makeChart() {
                        if (!this._isAdded || $(this._element).hasClass("editor")) {
                              return;
                        }

                        if (this.config && this.dataProvider && this.dataProvider.length) {
                              $(this._element).removeClass("no-data");
                              this.clearChart();
                              this._chart = AmCharts.makeChart(this.chartId, this.config);
                        } else {
                              $(this._element).addClass("no-data");
                        }
                  }
            }, {
                  key: "render",
                  value: function render() {
                        if (!this._chart) {
                              this.makeChart();
                        } else {
                              if (this.dataProvider && this.dataProvider.length) {
                                    this._chart.dataProvider = this.dataProvider;
                                    this.validateData();
                                    $(this._element).removeClass("no-data");
                              } else {
                                    $(this._element).addClass("no-data");
                              }
                        }
                  }
            }, {
                  key: "getParseConfigData",
                  value: function getParseConfigData() {
                        var data = null;

                        try {
                              var configStr = this.getGroupPropertyValue("extension", "configStr");
                              data = JSON.parse(configStr);
                              return data;
                        } catch (error) {
                              return null;
                        }
                  }
            }, {
                  key: "validateData",
                  value: function validateData() {
                        this._chart.validateData();
                  }
            }, {
                  key: "config",
                  set: function set(config) {
                        this._config = config;

                        if (this._config && this._config.dataProvider) {
                              this.dataProvider = this._config.dataProvider;
                        }

                        this.makeChart();
                  },
                  get: function get() {
                        if (this._config) {
                              this._config.dataProvider = this.dataProvider;
                        }

                        return this._config;
                  }
            }, {
                  key: "dataProvider",
                  set: function set(data) {
                        this._dataProvider = data;

                        if (this.isViewerMode) {
                              this.dispatchWScriptEvent("change", {
                                    dataProvider: this._dataProvider
                              });
                        }

                        this.render();
                  },
                  get: function get() {
                        return this._dataProvider;
                  }
            }, {
                  key: "chart",
                  get: function get() {
                        return this._chart;
                  }
            }]);

            return EmptyChartComponent;
      }(WVDOMComponent); // 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(EmptyChartComponent, {
      "info": {
            "componentName": "EmptyChartComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 100,
            "height": 100,
            "autoExecute": true
      },
      "label": {
            "label_using": "N",
            "label_text": "Empty Chart"
      },
      "extension": {
            "configStr": ''
      },
      "preview": {
            "use": false
      }
}); // 프로퍼티 패널에서 사용할 정보 입니다.

EmptyChartComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}, {
      label: "Preview",
      template: "vertical",
      children: [{
            owner: "preview",
            name: "use",
            type: "checkbox",
            label: "Use",
            show: true,
            writable: true,
            description: "미리보기 설정",
            options: {
                  label: "사용"
            }
      }]
}]; // 이벤트 정보

WVPropertyManager.add_event(EmptyChartComponent, {
      name: "change",
      label: "값 체인지 이벤트",
      description: "값 체인지 이벤트 입니다.",
      properties: [{
            name: "dataProvider",
            type: "string",
            default: "",
            description: "새로운 값입니다."
      }]
});
WVPropertyManager.add_property_group_info(EmptyChartComponent, {
      label: "EmptyChartComponent 고유 속성",
      children: [{
            name: "config",
            type: "Object",
            show: true,
            writable: true,
            description: "Chart 설정 정보 입니다."
      }, {
            name: "dataProvider",
            type: "Array<any>",
            show: true,
            writable: true,
            description: "Chart를 구성할 데이터(row data)입니다."
      }, {
            name: "chart",
            type: "AmCharts Instance",
            show: true,
            writable: false,
            description: "생성된 chart(관련 설정은 amchat 참조)객체 입니다. 데이터가 없을경우 null입니다."
      }]
});
WVPropertyManager.remove_property_group_info(EmptyChartComponent, "label");
WVPropertyManager.add_method_info(EmptyChartComponent, {
      name: "validateData",
      description: "chart 설정 정보를 변경한 후 호출하면 변경된 정보로 차트를 다시 그립니다.",
      params: []
});
