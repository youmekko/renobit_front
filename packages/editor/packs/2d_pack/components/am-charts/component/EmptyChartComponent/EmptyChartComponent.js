class EmptyChartComponent extends WVDOMComponent {
    constructor() {
        super();
        this._isAdded = false;
        this._chart = null;
        this._config = null;
        this.chartId = "";
        this._dataProvider = [];
        this._invaldatePreivewProperty = false;
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateProperties() {
        this.chartId = "id-" + this.id;
        let config = this.getParseConfigData();
        if (config) {
            this.config = config;
        }
    }

    _onCreateElement() {
        $(this._element).append('<div style="width:100%;height:100%;" class="chart-area" id="' + this.chartId + '"></div><span class="txt">NO DATA</span>');
    }

    _onImmediateUpdateDisplay() {
        this._isAdded = true;
        this.setPreviewMode();
    }

    onLoadPage() {}

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this._updateChartConfig);
        }

        if (this._updatePropertiesMap.has("preview")) {
            this.validateCallLater(this.setPreviewMode);
        }
    }


    setPreviewMode() {
        if (this.isEditorMode && !this.getGroupPropertyValue("preview", "use")) {
            $(this._element).removeClass("no-data").addClass("editor");
            if (this._chart) {
                this.clearChart();
            }
        } else {
            $(this._element).removeClass("editor");
            this.render();
        }
    }


    _updateChartConfig() {
        this.clearChart();
        let config = this.getParseConfigData();
        if (config) {
            this.config = config;
            this.render();
        }else{
              this.dataProvider = null;
        }


    }


    _onDestroy() {
        this.clearChart();
        super._onDestroy();
    }


    clearChart() {
        if (this._chart) {
            this._chart.destroy();
            this._chart.clear();
            this._chart = null;
        }
    }

    makeChart() {
        if (!this._isAdded || $(this._element).hasClass("editor")) { return; }
        if (this.config && this.dataProvider && this.dataProvider.length) {
            $(this._element).removeClass("no-data");
            this.clearChart();
            this._chart = AmCharts.makeChart(this.chartId, this.config);
        } else {
            $(this._element).addClass("no-data");
        }
    }

    render() {
        if (!this._chart) {
            this.makeChart();
        } else {
            if (this.dataProvider && this.dataProvider.length) {
                this._chart.dataProvider = this.dataProvider;
                this.validateData();
                $(this._element).removeClass("no-data");
            } else {
                $(this._element).addClass("no-data");
            }
        }
    }



    getParseConfigData() {
        let data = null;
        try {
            let configStr = this.getGroupPropertyValue("extension", "configStr");
            data = JSON.parse(configStr);
            return data;
        } catch (error) {
            return null;
        }
    }

    validateData() {
        this._chart.validateData();
    }

    set config(config) {
        this._config = config;
        if (this._config && this._config.dataProvider) {
            this.dataProvider = this._config.dataProvider;
        }

        this.makeChart();
    }

    get config() {
        if (this._config) {
            this._config.dataProvider = this.dataProvider;
        }

        return this._config;
    }

    set dataProvider(data) {
        this._dataProvider = data;
        if (this.isViewerMode) {
            this.dispatchWScriptEvent("change", {
                dataProvider: this._dataProvider
            })
        }

        this.render();
    }

    get dataProvider() {
        return this._dataProvider;
    }

    get chart() {
        return this._chart;
    }
}

// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(EmptyChartComponent, {
    "info": {
        "componentName": "EmptyChartComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 100,
        "height": 100,
        "autoExecute": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Empty Chart"
    },

    "extension": {
        "configStr": '',
    },

    "preview": {
        "use": false
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
EmptyChartComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
      template: "label"
}, {
    label: "Preview",
    template: "vertical",
    children: [{
        owner: "preview",
        name: "use",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "미리보기 설정",
        options: {
            label: "사용"
        }
    }],
}];

// 이벤트 정보
WVPropertyManager.add_event(EmptyChartComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "dataProvider",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(EmptyChartComponent, {
    label: "EmptyChartComponent 고유 속성",
    children: [{
        name: "config",
        type: "Object",
        show: true,
        writable: true,
        description: "Chart 설정 정보 입니다."
    }, {
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        description: "Chart를 구성할 데이터(row data)입니다."
    }, {
        name: "chart",
        type: "AmCharts Instance",
        show: true,
        writable: false,
        description: "생성된 chart(관련 설정은 amchat 참조)객체 입니다. 데이터가 없을경우 null입니다."
    }]
});


WVPropertyManager.remove_property_group_info(EmptyChartComponent, "label");

WVPropertyManager.add_method_info(EmptyChartComponent, {
    name: "validateData",
    description: "chart 설정 정보를 변경한 후 호출하면 변경된 정보로 차트를 다시 그립니다.",
    params: []
});
