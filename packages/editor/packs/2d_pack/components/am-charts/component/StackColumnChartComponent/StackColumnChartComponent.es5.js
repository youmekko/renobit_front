"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var StackColumnChartComponent = function(_ChartCoreComponent) {
    _inherits(StackColumnChartComponent, _ChartCoreComponent);

    function StackColumnChartComponent() {
        _classCallCheck(this, StackColumnChartComponent);

        return _possibleConstructorReturn(this, (StackColumnChartComponent.__proto__ || Object.getPrototypeOf(StackColumnChartComponent)).call(this));
    }

    _createClass(StackColumnChartComponent, [{
        key: "getExtensionProperties",
        value: function getExtensionProperties() {
            return true;
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            _get(StackColumnChartComponent.prototype.__proto__ || Object.getPrototypeOf(StackColumnChartComponent.prototype), "_onCreateElement", this).call(this);
            this._element.classList.add('stack-column-chart-component');
        }

        // 차트 시리즈 생성

    }, {
        key: "makeSeries",
        value: function makeSeries(series) {
            _get(StackColumnChartComponent.prototype.__proto__ || Object.getPrototypeOf(StackColumnChartComponent.prototype), "makeSeries", this).call(this, series);

            var graphs = [];

            for (var i = 0; i < series.length; i++) {
                var target = series[i];

                target.type = "column";
                target.lineAlpha = 0;
                target.fillAlphas = 1;

                if (this.chartConfig.colors[i] != null) {
                    target.fillColors = this.chartConfig.colors[i];
                }

                graphs.push(JSON.parse(JSON.stringify(target)));
            }

            return graphs;
        }
    }]);

    return StackColumnChartComponent;
}(ChartCoreComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(StackColumnChartComponent, {
    "info": {
        "componentName": "StackColumnChartComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 100,
        "height": 100,
        "autoExecute": true
    },
    "label": {
        "label_using": "N",
        "label_text": "Stack Column Chart"
    },

    "extension": {
        "datasetId": null,
        "chartConfig": {
            "categoryField": '',
            "graphs": [],
            "colors": [],
            "radius": []
        },
        "styleConfig": {
            "guides": [{}],
            "valueAxes": [{
                "stackType": "100%"
            }]
        },
        "initConfig": {
            "fontFamily": "NanumSquareR",
            "type": "serial",
            "theme": "light",
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "startDuration": 0,
            "legend": {},
            "titles": [{
                "text": "Chart Title",
                "size": 15,
                "color": "#000000",
                "bold": true
            }],
            "chartScrollbar": {
                "enabled": false
            },
            "chartCursor": {
                "cursorAlpha": 0,
                "categoryBalloonEnabled": false,
                "valueLineEnabled": false
            },
            "dataProvider": []
        },
        "showPreview": false
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
StackColumnChartComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Preview",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "showPreview",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "미리보기 설정",
        options: {
            label: "사용"
        }
    }]
}, {
    label: "Auto Execute",
    children: [{
        owner: "setter",
        name: "autoExecute",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "자동실행 설정"
    }]
}];

//  추후 추가 예정
StackColumnChartComponent.method_info = [];

// 이벤트 정보
WVPropertyManager.add_event(StackColumnChartComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_property_group_info(StackColumnChartComponent, {
    label: "StackColumnChartComponent 고유 속성",
    children: [{
        name: "datasetId",
        type: "string",
        show: true,
        writable: false,
        description: "설정한 데이터셋의 고유 id입니다."
    }, {
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        description: "Chart를 구성할 데이터(row data)입니다."
    }]
});

WVPropertyManager.add_method_info(StackColumnChartComponent, {
    name: "execute",
    description: "설정한 데이터셋을 호출하여 Chart를 업데이트합니다.",
    params: [{
        name: "params",
        type: "object",
        description: "설정한 데이터셋의 parameters입니다."
    }]
});
