class ComboChartComponent extends ChartCoreComponent {
    constructor() {
        super();
    }

    getExtensionProperties() {
        return true;
    }

    _onCreateElement() {
        super._onCreateElement();
        this._element.classList.add('combo-chart-component');
    }

    // 차트 시리즈 생성
    makeSeries(series) {
        super.makeSeries(series);
        var graphs = [];

        for (var j = 0; j < series.length; j++) {
            var target = series[j];

            if (this.chartConfig.colors[j] != null) {
                target.fillColors = this.chartConfig.colors[j];
            }

            graphs.push(JSON.parse(JSON.stringify(target)));
        }

        return graphs;
    }
}

// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(ComboChartComponent, {
    "info": {
        "componentName": "ComboChartComponent",
        "version": "1.0.0"
    },

    "setter": {
        "autoExecute": true
    },

    "label": {
        "label_using": "N",
        "label_text": "Column Line Chart"
    },

    "style": {
        "backgroundColor": "rgba(255,255,255,0)"
    },
    "extension": {
        "chartConfig": {
            "categoryField": '',
            "graphs": []
        },
        "datasetId": null,
        "styleConfig": {},
        "initConfig": {
            "type": "serial",
            "theme": "light",
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "startDuration": 0,
            "legend": {},
            "titles": [{
                "text": "Chart Title",
                "size": 15,
                "color": "#000000",
                "bold": true
            }],
            "chartScrollbar": {
                "enabled": false
            },
            "chartCursor": {
                "cursorAlpha": 0,
                "categoryBalloonEnabled": false,
                "valueLineEnabled": false
            }
        },
        "showPreview": false
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
ComboChartComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Preview",
    template: "vertical",
    children: [{
        owner: "extension",
        name: "showPreview",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "미리보기 설정",
        options: {
            label: "사용"
        }
    }],
}, {
    label: "Auto Execute",
    children: [{
        owner: "setter",
        name: "autoExecute",
        type: "checkbox",
        label: "Use",
        show: true,
        writable: true,
        description: "자동실행 설정"
    }]
}];


// 이벤트 정보
WVPropertyManager.add_event(ComboChartComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});



WVPropertyManager.add_property_group_info(ComboChartComponent, {
    label: "ComboChartComponent 고유 속성",
    children: [{
        name: "datasetId",
        type: "string",
        show: true,
        writable: false,
        description: "설정한 데이터셋의 고유 id입니다."
    }, {
        name: "dataProvider",
        type: "Array<any>",
        show: true,
        writable: true,
        description: "Chart를 구성할 데이터(row data)입니다."
    }]
});


WVPropertyManager.add_method_info(ComboChartComponent, {
    name: "execute",
    description: "설정한 데이터셋을 호출하여 Chart를 업데이트합니다.",
    params: [{
        name: "params",
        type: "object",
        description: "설정한 데이터셋의 parameters입니다."
    }]
});
