class ChartCoreComponent extends WVDOMComponent {
      constructor() {
            super();
            this.chart;
            this.dataset;
      }

      _onCreateElement() {
            if (!this.isEditorMode) {
                  var chartContainer = this.initChart();
                  this._element.appendChild(chartContainer);
            }
      }

      _onImmediateUpdateDisplay() {
            if (this.isEditorMode) {
                  this._updatePreviewData();
            } else {
                  this.setChartConfig();
            }
      }

      onLoadPage() {
            ///페이지가 모두 로드되고 난 뒤 호출되는 메소드
            if (!this.isEditorMode && this.getGroupPropertyValue("setter", "autoExecute")) {
                  this.execute({});
            }
      }

      _onDestroy() {
            //  차트 초기화
            this.clearChart();

            if (this.datasetWorker) {
                  this.datasetWorker.clear({});
                  this.datasetWorker = null;
            }

            super._onDestroy();
      }

      _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension")) {
                  if (this._updatePropertiesMap.has("extension.dataProvider")) {
                        this.validateCallLater(this._updateDataProvider);
                  } else {
                        this.validateCallLater(this._updatePreviewData);
                  }
            }
      }


      clearChart() {
            if (this.chart) {
                  this.chart.cleanChart();
                  this.chart.clear();
                  this.chart.destroy();
                  this.chart = null;
            }
      }

      initChart() {
            // 차트 초기화
            this.clearChart();

            var chartContainer = document.createElement("div");
            chartContainer.style.height = "100%";

            var initConfig = JSON.parse(JSON.stringify(this.initConfig));
            var styleConfig = JSON.parse(JSON.stringify(this.styleConfig));

            // 차트별 기본 설정, 스타일 설정 적용
            var chartConfig = $.extend(true, {}, initConfig, styleConfig);
            chartConfig.startDuration = 0;


            for (let index in chartConfig.titles) {
                  let item = chartConfig.titles[index];
                  if (item && item.text) {
                        item.text = this.getTranslateText(item.text);
                  }
            }

            // 차트 생성
            this.chart = AmCharts.makeChart(chartContainer, JSON.parse(JSON.stringify(chartConfig)));

            return chartContainer
      }

      setChartConfig() {
            var config = JSON.parse(JSON.stringify(this.chartConfig));

            this.chart.categoryField = config.categoryField;
            let graphs = this.makeSeries(config.graphs);

            for (let index in graphs) {
                  let item = graphs[index];
                  if (item && item.title) {
                        item.title = this.getTranslateText(item.title);
                  }
            }

            this.chart.graphs = graphs;

            if (this.showPreview && this.isEditorMode) {
                  this.chart.dataProvider = this.previewData;
                  this.chart.validateData();
            }
      }

      makeSeries(series) {

      }

      getTranslateText(str) {
            return wemb.localeManager.translatePrefixStr(str);
      }


      makeChartData(data) {
            let oldValue = this.chart.dataProvider;

            this.chart.cleanChart();
            this.chart.clearLabels();
            this.chart.chartDiv.style.opacity = 1;

            if (data != null && data.length > 0) {
                  this.chart.dataProvider = data.concat();

            } else {
                  var dataPoint = {
                        dummyValue: 0
                  };

                  dataPoint[this.chart.categoryField] = '';
                  this.chart.dataProvider = [dataPoint];

                  try {
                        this.chart.createLabelsSet()
                        this.chart.addLabel(0, '50%', '차트 데이터가 없습니다.', 'center');
                        this.chart.chartDiv.style.opacity = 0.5;
                  } catch(e) {
                        console.log(e);
                  }
            }
            
            this.chart.validateData();

            if (!this.isEditorMode && this.chart.dataProvider != oldValue) {
                  this.dispatchWScriptEvent("change", {
                        value: this.chart.dataProvider
                  })
            }

      }

      execute(param) {
            if (this.datasetId) {
                  // 현재 실행중인 dataSet 끄기
                  var self = this;
                  this.datasetWorker = this.page.dataService.callById(this.datasetId, {"param": param});

                  if (this.datasetWorker && this.datasetWorker.item) {
                        this.datasetWorker.on("error", function (event) {
                              console.warn("dataset call error", event);
                        })
                        this.datasetWorker.on("success", function (event) {
                              self.makeChartData(event.rowData);
                        });
                  } else {
                        console.warn("dataset is not defined component id : ", this.id);
                  }

            } else {
                  this.makeChartData(null);
            }
      }

      _updatePreviewData() {
            let value = this.showPreview;
            let previewData = this.previewData;

            let $el = $(this._element);
            $el.removeClass("isEditor d-flex").children().remove();

            if (value) {
                  if (!previewData) {
                        $el.addClass("isEditor d-flex");
                        $el.prepend('<span class="no-data">데이터가 없습니다.</span>');
                        return;
                  }

                  let chartContainer = this.initChart();
                  this._element.appendChild(chartContainer);

                  $el.css("backgroundColor", this.getGroupPropertyValue("style", "backgroundColor"));
                  this.setChartConfig(true);
            } else {
                  this.clearChart();
                  $el.addClass("isEditor");
            }
      }

      _updateDataProvider() {
            if (this.chart) {
                  this.makeChartData(this.dataProvider);
            }
      }


      /**
       *
       * 차트 가이드라인 설정을 위한 메소드
       *
       ary(Array) 구성 :

       ex )
       [
       {
    		   value : 90,                         // 가이드 value 값
    		   toValue: 100,                       // 가이드값이 영역일 경우 toValue 사용, 라인일 경우 value와 같은 값으로 설정
    		   fillAlpha: 0,                       // 영역 alpha 값
    		   fillColor : '#ff0000',              // 영역 color 값
    		   lineColor: '#ff0000',               // 라인 color 값
    		   inside: true,                       // 라벨 내부 위치 여부
    		   label: 'test'                       // 가이드라인 라벨값
    		   color : "#ffffff",                  // 라벨 컬러
    		   labelRotation: 90,                  // 라벨 회전값
    		   position: "top",                    // 라벨 위치값
    	 },
       { ... }
       ]

       * @param ary
       *
       */
      makeHorizontalGuideLine(ary) {
            if (this.chart && this.chart.valueAxes.length > 0) {
                  this.chart.valueAxes[0].guides = ary;
                  this.chart.validateData();
            }

      }

      makeVerticalGuideLine(ary) {
            if (this.chart && this.chart.categoryAxis) {
                  this.chart.categoryAxis.guides = ary;
                  this.chart.validateData();
            }
      }

      /**
       차트 추가 옵션 설정을 위한 메소드

       [ json 구성 ]
       ex ) {key: "maximum", value: 80}
       ex ) {key: "minimum", value: 0}

       @param json

       **/
      setValueAxes(json) {
            if (this.chart && this.chart.valueAxes.length > 0) {
                  this.chart.valueAxes[0][json.key] = json.value;
                  this.chart.validateData();
            }
      }

      get datasetId() {
            return this.getGroupPropertyValue("extension", "datasetId");
      }

      get showPreview() {
            return this.getGroupPropertyValue("extension", "showPreview");
      }

      get previewData() {
            return this.getGroupPropertyValue("extension", "previewData");
      }

      get initConfig() {
            return this.getGroupPropertyValue("extension", "initConfig");
      }

      get styleConfig() {
            return this.getGroupPropertyValue("extension", "styleConfig");
      }

      get chartConfig() {
            return this.getGroupPropertyValue("extension", "chartConfig");
      }

      set autoExecute(bool) {
            this._checkUpdateGroupPropertyValue("setter", "autoExecute", bool);
      }

      set dataProvider(value) {
            this._checkUpdateGroupPropertyValue("extension", "dataProvider", value);
      }

      get dataProvider() {
            return this.getGroupPropertyValue("extension", "dataProvider");
      }
}
