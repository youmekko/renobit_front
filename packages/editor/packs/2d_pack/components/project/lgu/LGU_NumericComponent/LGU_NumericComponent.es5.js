"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LGU_NumericComponent = function (_WVDOMComponent) {
    _inherits(LGU_NumericComponent, _WVDOMComponent);

    function LGU_NumericComponent() {
        _classCallCheck(this, LGU_NumericComponent);

        return _possibleConstructorReturn(this, (LGU_NumericComponent.__proto__ || Object.getPrototypeOf(LGU_NumericComponent)).call(this));
    }

    _createClass(LGU_NumericComponent, [{
        key: "_onCreateProperties",
        value: function _onCreateProperties() {
            this.containier = null;
            this.$bar = null;
            this.$barValue = null;
            this.$barValueArea = null;
            this.$barDirection = null;
            this._invalidateTextvalue = false;
            this._invalidateBarValue = false;
            this._invalidateArrow = false;
            this.__invalidateArrowValue = false;
            this.__invalidateMinvalue = false;
            this.__invalidateMaxvalue = false;
        }

        //element 생성

    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this.containier = document.createElement("div");
            this.containier.classList.add('lgu_numbericBar');
            this._element.appendChild(this.containier);
        }

        ///페이지 로드가 다 끝났을때

    }, {
        key: "onLoadPage",
        value: function onLoadPage() {
            this.initProperty();
        }
    }, {
        key: "initProperty",
        value: function initProperty() {
            this.createNumberBarArea();
            this.setBarValue();
            this.setBarStyle();
            this.setTitleDirection();
            this.setTitleValue();
            this.setTitleStyle();
            this.setArrowValue();
        }

        ///화면에 붙였을때

    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.initProperty();
        }
    }, {
        key: "createNumberBarArea",
        value: function createNumberBarArea() {

            $(this.containier).empty();

            var barDirection = document.createElement("div");
            barDirection.classList.add('barDeriection');
            barDirection.classList.add(this.direction);
            this.containier.appendChild(barDirection);

            this.$barDirection = $(barDirection);

            this.createArrowArea();
            this.setArrowValue();

            var barValueArea = document.createElement("div");
            barValueArea.classList.add('barValueArea');
            barDirection.appendChild(barValueArea);

            var barValue = document.createElement("div");
            barValue.classList.add('barValue');
            barValueArea.appendChild(barValue);

            var bar = document.createElement("div");
            bar.classList.add('bar');
            barDirection.appendChild(bar);

            this.$bar = $(bar);
            this.$barValue = $(barValue);
            this.$barValueArea = $(barValueArea);
        }
    }, {
        key: "createArrowArea",
        value: function createArrowArea() {

            var arrow = document.createElement("div");
            arrow.classList.add('arrowArea');
            this.$barDirection.append(arrow);

            this.$arrow = $(arrow);

            var icon = document.createElement("div");
            icon.classList.add('icon');
            this.$arrow.append(icon);

            var value = document.createElement("div");
            value.classList.add('value');
            this.$arrow.append(value);
        }
    }, {
        key: "setArrowValue",
        value: function setArrowValue() {

            if (this.arrow_value == "") {
                if (this.$arrow) {
                    this.$arrow.remove();
                    this.$arrow = null;
                }
            } else {

                if (!this.$arrow) {
                    this.createArrowArea();
                }

                var flag = this.arrow == "up" ? "down" : "up";
                this.$arrow.removeClass(flag).addClass(this.arrow);
                $(this.$arrow).find(".value").html(this.arrow_value);
                this.dispatcherWscript(this.arrow_value);
            }
        }
    }, {
        key: "setBarValue",
        value: function setBarValue() {

            if (this.min_value != null && this.max_value != null) {

                var margin = this.bt_margin / $(this._element).height();

                this.$barDirection.get(0).style.height = (margin + (this.bar_value - this.min_value) / (this.max_value - this.min_value)) * 100 + "%";
            }

            this.dispatcherWscript(this.bar_value);
        }
    }, {
        key: "setBarStyle",
        value: function setBarStyle() {

            this.$bar.css("background-color", this.bar_color).css("border-radius", this.bar_radius + "px");
        }
    }, {
        key: "setTitleDirection",
        value: function setTitleDirection() {

            var flag = this.direction == "barLeft" ? "barRight" : "barLeft";

            this.$barDirection.removeClass(flag).addClass(this.direction);

            if (this.direction == "barLeft") {
                this.$barValueArea.css("left", "").css("right", this.$bar.width() + 10 + "px");
            } else {
                this.$barValueArea.css("right", "").css("left", this.$bar.width() + 10 + "px");
            }
        }
    }, {
        key: "setTitleValue",
        value: function setTitleValue() {

            this.$barValue.html(this.title_value);

            if (this.direction == "barLeft") {
                this.$barValueArea.css("right", this.$bar.width() + 10 + "px");
            } else {
                this.$barValueArea.css("left", this.$bar.width() + 10 + "px");
            }

            this.dispatcherWscript(this.title_value);
        }
    }, {
        key: "setTitleStyle",
        value: function setTitleStyle() {
            this.$barValueArea.css("background-color", this.title_BackColor);
            this.$barValue.css("color", this.title_FontColor);
        }
    }, {
        key: "dispatcherWscript",
        value: function dispatcherWscript(value) {
            if (!this.isEditorMode) {
                this.dispatchWScriptEvent("change", {
                    newValue: value
                });
            }
        }

        //properties 변경시

    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {

            if (this._updatePropertiesMap.has("extensionBarStyle")) {
                this.validateCallLater(this.setBarStyle);
            }

            if (this._updatePropertiesMap.has("extensionBarValue")) {
                this.validateCallLater(this.setBarValue);
            }

            if (this._updatePropertiesMap.has("extensionTitleValue.direction")) {
                this.validateCallLater(this.setTitleDirection);
            } else {
                this.validateCallLater(this.setTitleValue);
            }

            if (this._updatePropertiesMap.has("extensionTitleStyle")) {
                this.validateCallLater(this.setTitleStyle);
            }

            if (this._updatePropertiesMap.has("extensionArrowStyle")) {
                this.validateCallLater(this.setArrowValue);
            }
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            //삭제 처리
            this.containier = null;
            this.$bar = null;
            this.$barValue = null;
            this.$barValueArea = null;
            this.$barDirection = null;
            this._invalidateTextvalue = false;
            this._invalidateBarValue = false;
            this._invalidateArrow = false;
            this.__invalidateArrowValue = false;
            this.__invalidateMinvalue = false;
            this.__invalidateMaxvalue = false;

            _get(LGU_NumericComponent.prototype.__proto__ || Object.getPrototypeOf(LGU_NumericComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "bar_radius",
        get: function get() {
            return this.getGroupPropertyValue("extensionBarStyle", "bar_radius");
        }
    }, {
        key: "bar_color",
        get: function get() {
            return this.getGroupPropertyValue("extensionBarStyle", "bar_color");
        }
    }, {
        key: "bar_value",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extensionBarValue", "bar_value", value)) {
                this._invalidateBarValue = true;
            }
            this.setBarValue();
        },
        get: function get() {
            return this.getGroupPropertyValue("extensionBarValue", "bar_value");
        }
    }, {
        key: "min_value",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extensionBarValue", "min_value", value)) {
                this.__invalidateMinvalue = true;
            }
            this.setBarValue();
        },
        get: function get() {
            return this.getGroupPropertyValue("extensionBarValue", "min_value");
        }
    }, {
        key: "max_value",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extensionBarValue", "max_value", value)) {
                this.__invalidateMaxvalue = true;
            }
            this.setBarValue();
        },
        get: function get() {
            return this.getGroupPropertyValue("extensionBarValue", "max_value");
        }
    }, {
        key: "bt_margin",
        get: function get() {
            return this.getGroupPropertyValue("extensionBarValue", "bt_margin");
        }
    }, {
        key: "title_value",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extensionTitleValue", "title_value", value)) {
                this._invalidateTextvalue = true;
            }
            this.setTitleValue();
        },
        get: function get() {
            return this.getGroupPropertyValue("extensionTitleValue", "title_value");
        }
    }, {
        key: "direction",
        get: function get() {
            return this.getGroupPropertyValue("extensionTitleValue", "direction");
        }
    }, {
        key: "title_BackColor",
        get: function get() {
            return this.getGroupPropertyValue("extensionTitleStyle", "title_BackColor");
        }
    }, {
        key: "title_FontColor",
        get: function get() {
            return this.getGroupPropertyValue("extensionTitleStyle", "title_FontColor");
        }
    }, {
        key: "arrow_value",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extensionArrowStyle", "arrow_value", value)) {
                this._invalidateArrowValue = true;
            }
            this.setArrowValue();
        },
        get: function get() {
            return this.getGroupPropertyValue("extensionArrowStyle", "arrow_value");
        }
    }, {
        key: "arrow",
        set: function set(value) {
            if (this._checkUpdateGroupPropertyValue("extensionArrowStyle", "arrow", value)) {
                this._invalidateArrow = true;
            }
            this.setArrowValue();
        },
        get: function get() {
            return this.getGroupPropertyValue("extensionArrowStyle", "arrow");
        }
    }]);

    return LGU_NumericComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(LGU_NumericComponent, {
    "info": {
        "componentName": "LGU_NumericComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 100,
        "height": 150,
        "value": ''
    },

    "label": {
        "label_using": "N",
        "label_text": "Input Component"
    },

    "style": {
        "border": "1px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 10
    },
    "extensionBarValue": {
        "bar_value": 60,
        "min_value": 0,
        "max_value": 100,
        "bt_margin": 0
    },
    "extensionBarStyle": {
        "bar_color": "#5a3c6c",
        "bar_radius": 10
    },
    "extensionTitleValue": {
        "direction": "barRight",
        "title_value": "111,111"
    },
    "extensionTitleStyle": {
        "title_BackColor": "#4e5357",
        "title_FontColor": "#ffffff"
    },
    "extensionArrowStyle": {
        "arrow": "up",
        "arrow_value": "2.8%"
    }
});

LGU_NumericComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, { template: "background" }, {
    template: "label"
}, {
    template: "border"
}, {
    label: "Bar Option",
    template: "vertical",
    children: [{
        owner: "extensionBarValue",
        name: "bar_value",
        type: "string",
        label: "value",
        show: true,
        writable: true,
        description: "bar_value"
    }, {
        owner: "extensionBarValue",
        name: "min_value",
        type: "string",
        label: "min",
        show: true,
        writable: true,
        description: "min_value"
    }, {
        owner: "extensionBarValue",
        name: "max_value",
        type: "string",
        label: "max",
        show: true,
        writable: true,
        description: "max_value"
    }, {
        owner: "extensionBarStyle",
        name: "bar_color",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "bar_color"
    }, {
        owner: "extensionBarStyle",
        name: "bar_radius",
        type: "string",
        label: "radius",
        show: true,
        writable: true,
        description: "bar_radius"
    }, {
        owner: "extensionBarValue",
        name: "bt_margin",
        type: "string",
        label: "bt_margin",
        show: true,
        writable: true,
        description: "bt_margin"
    }]
}, {
    label: "Title Option",
    template: "vertical",
    children: [{
        owner: "extensionTitleValue",
        name: "title_value",
        type: "string",
        label: "value",
        show: true,
        writable: true,
        description: "title_value"
    }, {
        owner: "extensionTitleStyle",
        name: "title_BackColor",
        type: "color",
        label: "BackColor",
        show: true,
        writable: true,
        description: "title_BackColor"
    }, {
        owner: "extensionTitleStyle",
        name: "title_FontColor",
        type: "color",
        label: "FontColor",
        show: true,
        writable: true,
        description: "title_FontColor"
    }, {
        owner: "extensionTitleValue",
        name: "direction",
        label: "direction",
        type: "select",
        options: {
            items: [{ label: "left", value: "barLeft" }, { label: "right", value: "barRight" }]
        }
    }]
}, {
    label: "Arrow Option",
    template: "vertical",
    children: [{
        owner: "extensionArrowStyle",
        name: "arrow_value",
        type: "string",
        label: "value",
        show: true,
        writable: true,
        description: "arrow_value"
    }, {
        owner: "extensionArrowStyle",
        name: "arrow",
        label: "arrow",
        type: "select",
        options: {
            items: [{ label: "up", value: "up" }, { label: "down", value: "down" }]
        }
    }]
}];

WVPropertyManager.add_event(LGU_NumericComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});
