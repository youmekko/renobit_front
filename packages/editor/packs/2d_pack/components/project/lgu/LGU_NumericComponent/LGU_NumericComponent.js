class LGU_NumericComponent extends WVDOMComponent {

    constructor() {
        super();
    }

    _onCreateProperties(){
        this.containier = null;
        this.$bar = null;
        this.$barValue = null;
        this.$barValueArea = null;
        this.$barDirection = null;
        this._invalidateTextvalue = false;
        this._invalidateBarValue = false;
        this._invalidateArrow = false;
        this.__invalidateArrowValue = false;
        this.__invalidateMinvalue = false;
        this.__invalidateMaxvalue = false;
    }

    //element 생성
    _onCreateElement() {
        this.containier = document.createElement("div");
        this.containier.classList.add('lgu_numbericBar');
        this._element.appendChild(this.containier);

    }

    ///페이지 로드가 다 끝났을때
    onLoadPage() {
        this.initProperty();
    }

    initProperty(){
        this.createNumberBarArea();
        this.setBarValue();
        this.setBarStyle();
        this.setTitleDirection();
        this.setTitleValue();
        this.setTitleStyle();
        this.setArrowValue();
    }

    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {
        this.initProperty();
    }

    createNumberBarArea(){

        $(this.containier).empty();

        var barDirection = document.createElement("div");
        barDirection.classList.add('barDeriection');
        barDirection.classList.add(this.direction);
        this.containier.appendChild(barDirection);

        this.$barDirection = $(barDirection);

        this.createArrowArea();
        this.setArrowValue();

        let barValueArea = document.createElement("div");
        barValueArea.classList.add('barValueArea');
        barDirection.appendChild(barValueArea);

        let barValue = document.createElement("div");
        barValue.classList.add('barValue');
        barValueArea.appendChild(barValue);

        let bar = document.createElement("div");
        bar.classList.add('bar');
        barDirection.appendChild(bar);

        this.$bar = $(bar);
        this.$barValue = $(barValue);
        this.$barValueArea = $(barValueArea);

    }

    createArrowArea(){

        var arrow = document.createElement("div");
        arrow.classList.add('arrowArea');
        this.$barDirection.append(arrow);

        this.$arrow = $(arrow);

        let icon = document.createElement("div");
        icon.classList.add('icon');
        this.$arrow.append(icon);

        let value = document.createElement("div");
        value.classList.add('value');
        this.$arrow.append(value);

    }

    setArrowValue(){

        if(this.arrow_value == ""){
            if(this.$arrow) {
                this.$arrow.remove();
                this.$arrow = null;
            }
        }else{

            if(!this.$arrow){
                this.createArrowArea();
            }

            let flag = this.arrow == "up" ? "down" : "up";
            this.$arrow.removeClass(flag).addClass(this.arrow);
            $(this.$arrow).find(".value").html(this.arrow_value);
            this.dispatcherWscript(this.arrow_value);
        }

    }

    setBarValue(){

        if(this.min_value != null && this.max_value != null){

            let margin = this.bt_margin / $(this._element).height();

            this.$barDirection.get(0).style.height = (margin + ((this.bar_value - this.min_value) / (this.max_value - this.min_value))) * 100 + "%";
        }

        this.dispatcherWscript(this.bar_value);

    }

    setBarStyle(){

        this.$bar.css("background-color", this.bar_color)
            .css("border-radius", this.bar_radius + "px");

    }

    setTitleDirection(){

        let flag = this.direction == "barLeft" ? "barRight" : "barLeft";

        this.$barDirection.removeClass(flag).addClass(this.direction);

        if(this.direction == "barLeft"){
            this.$barValueArea
                    .css("left", "")
                    .css("right", this.$bar.width() + 10 + "px" );
        }else{
            this.$barValueArea
                    .css("right", "")
                    .css("left", this.$bar.width() + 10 + "px" );
        }

    }

    setTitleValue(){

        this.$barValue.html(this.title_value);

        if(this.direction == "barLeft"){
            this.$barValueArea.css("right", this.$bar.width() + 10 + "px" );
        }else{
            this.$barValueArea.css("left", this.$bar.width() + 10 + "px" );
        }

        this.dispatcherWscript(this.title_value);

    }

    setTitleStyle(){
        this.$barValueArea.css("background-color", this.title_BackColor);
        this.$barValue.css("color", this.title_FontColor);
    }

    dispatcherWscript(value){
        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                newValue: value
            });
        }
    }

    //properties 변경시
    _onCommitProperties() {

        if(this._updatePropertiesMap.has("extensionBarStyle")){
            this.validateCallLater(this.setBarStyle);
        }

        if(this._updatePropertiesMap.has("extensionBarValue")){
            this.validateCallLater(this.setBarValue);
        }

        if(this._updatePropertiesMap.has("extensionTitleValue.direction")){
            this.validateCallLater(this.setTitleDirection);
        }else{
            this.validateCallLater(this.setTitleValue);
        }

        if(this._updatePropertiesMap.has("extensionTitleStyle")){
            this.validateCallLater(this.setTitleStyle);
        }

        if(this._updatePropertiesMap.has("extensionArrowStyle")){
            this.validateCallLater(this.setArrowValue);
        }

    }

    _onDestroy() {
        //삭제 처리
        this.containier = null;
        this.$bar = null;
        this.$barValue = null;
        this.$barValueArea = null;
        this.$barDirection = null;
        this._invalidateTextvalue = false;
        this._invalidateBarValue = false;
        this._invalidateArrow = false;
        this.__invalidateArrowValue = false;
        this.__invalidateMinvalue = false;
        this.__invalidateMaxvalue = false;

        super._onDestroy();
    }

    get bar_radius() {
        return this.getGroupPropertyValue("extensionBarStyle", "bar_radius");
    }

    get bar_color() {
        return this.getGroupPropertyValue("extensionBarStyle", "bar_color");
    }

    set bar_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionBarValue", "bar_value", value)) {
            this._invalidateBarValue = true;
        }
        this.setBarValue();
    }

    get bar_value() {
        return this.getGroupPropertyValue("extensionBarValue", "bar_value");
    }

    set min_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionBarValue", "min_value", value)) {
            this.__invalidateMinvalue = true;
        }
        this.setBarValue();
    }

    get min_value() {
        return this.getGroupPropertyValue("extensionBarValue", "min_value");
    }

    set max_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionBarValue", "max_value", value)) {
            this.__invalidateMaxvalue = true;
        }
        this.setBarValue();
    }

    get max_value() {
        return this.getGroupPropertyValue("extensionBarValue", "max_value");
    }

    get bt_margin() {
        return this.getGroupPropertyValue("extensionBarValue", "bt_margin");
    }

    set title_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionTitleValue", "title_value", value)) {
            this._invalidateTextvalue = true;
        }
        this.setTitleValue();
    }

    get title_value() {
        return this.getGroupPropertyValue("extensionTitleValue", "title_value");
    }

    get direction() {
        return this.getGroupPropertyValue("extensionTitleValue", "direction");
    }

    get title_BackColor() {
        return this.getGroupPropertyValue("extensionTitleStyle", "title_BackColor");
    }

    get title_FontColor() {
        return this.getGroupPropertyValue("extensionTitleStyle", "title_FontColor");
    }

    set arrow_value(value) {
        if (this._checkUpdateGroupPropertyValue("extensionArrowStyle", "arrow_value", value)) {
            this._invalidateArrowValue = true;
        }
        this.setArrowValue();
    }

    get arrow_value() {
        return this.getGroupPropertyValue("extensionArrowStyle", "arrow_value");
    }

    set arrow(value){
        if (this._checkUpdateGroupPropertyValue("extensionArrowStyle", "arrow", value)) {
            this._invalidateArrow = true;
        }
        this.setArrowValue();
    }

    get arrow() {
        return this.getGroupPropertyValue("extensionArrowStyle", "arrow");
    }

}

WVPropertyManager.attach_default_component_infos(LGU_NumericComponent, {
    "info": {
        "componentName": "LGU_NumericComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 100,
        "height": 150,
        "value": ''
    },

    "label": {
        "label_using": "N",
        "label_text": "Input Component"
    },

    "style": {
        "border": "1px none #000000",
        "backgroundColor": "rgba(255,255,255,0)",
        "borderRadius": 10
    },
    "extensionBarValue": {
        "bar_value" : 60,
        "min_value": 0,
        "max_value": 100,
        "bt_margin" : 0,
    },
    "extensionBarStyle" : {
        "bar_color": "#5a3c6c",
        "bar_radius" : 10
    },
    "extensionTitleValue" : {
        "direction" : "barRight",
        "title_value" : "111,111"
    },
    "extensionTitleStyle" : {
        "title_BackColor" : "#4e5357",
        "title_FontColor" : "#ffffff"
    },
    "extensionArrowStyle" : {
        "arrow" : "up",
        "arrow_value" : "2.8%"
    }
});

LGU_NumericComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, { template: "background" }, {
    template: "label"
}, {
    template: "border"
},

{
    label: "Bar Option",
    template: "vertical",
    children: [{
        owner: "extensionBarValue",
        name: "bar_value",
        type: "string",
        label: "value",
        show: true,
        writable: true,
        description: "bar_value"
    }, {
        owner: "extensionBarValue",
        name: "min_value",
        type: "string",
        label: "min",
        show: true,
        writable: true,
        description: "min_value"
    },{
        owner: "extensionBarValue",
        name: "max_value",
        type: "string",
        label: "max",
        show: true,
        writable: true,
        description: "max_value"
    },{
        owner: "extensionBarStyle",
        name: "bar_color",
        type: "color",
        label: "color",
        show: true,
        writable: true,
        description: "bar_color"
    },{
        owner: "extensionBarStyle",
        name: "bar_radius",
        type: "string",
        label: "radius",
        show: true,
        writable: true,
        description: "bar_radius"
    },{
        owner: "extensionBarValue",
        name: "bt_margin",
        type: "string",
        label: "bt_margin",
        show: true,
        writable: true,
        description: "bt_margin"
    }],
},
{
    label: "Title Option",
    template: "vertical",
    children: [{
        owner: "extensionTitleValue",
        name: "title_value",
        type: "string",
        label: "value",
        show: true,
        writable: true,
        description: "title_value"
    },{
        owner: "extensionTitleStyle",
        name: "title_BackColor",
        type: "color",
        label: "BackColor",
        show: true,
        writable: true,
        description: "title_BackColor"
    },{
        owner: "extensionTitleStyle",
        name: "title_FontColor",
        type: "color",
        label: "FontColor",
        show: true,
        writable: true,
        description: "title_FontColor"
    },{
        owner: "extensionTitleValue",
        name: "direction",
        label: "direction",
        type: "select",
        options: {
            items: [
                { label: "left", value: "barLeft" },
                { label: "right", value: "barRight" }
            ]
        }
    }],
},
{
    label: "Arrow Option",
    template: "vertical",
    children: [{
        owner: "extensionArrowStyle",
        name: "arrow_value",
        type: "string",
        label: "value",
        show: true,
        writable: true,
        description: "arrow_value"
    },{
        owner: "extensionArrowStyle",
        name: "arrow",
        label: "arrow",
        type: "select",
        options: {
            items: [
                { label: "up", value: "up" },
                { label: "down", value: "down" }
            ]
        }
    }],
}

]


WVPropertyManager.add_event(LGU_NumericComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});
