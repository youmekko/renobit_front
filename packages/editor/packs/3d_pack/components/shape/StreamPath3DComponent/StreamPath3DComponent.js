class StreamPath3DComponent extends Line{

      constructor(){
            super();
      }
      get pointCount() {
            return this.getGroupPropertyValue("setter", "pointCount");
      }
      set pointCount(cnt){
            if(cnt < 2)
                  console.warn("경고 : 라인은 최소 2개 이상의 점을 필요로 합니다.");
            else if(cnt > 50){
                  console.warn("경고 : 최대 50개의 점 까지 입력 가능합니다.");
            }
            else
                  this.setGroupPropertyValue("helper_count", "count", cnt);
      }
      set width(wd){
            if(wd < 1)
                  this.setGroupPropertyValue("line_width", "width", 1);
            else
                  this.setGroupPropertyValue("line_width", "width", wd);
      }
      get width(){
            return this.getGroupPropertyValue("line_width", "width");
      }
      get points(){
            return this.getGroupPropertyValue("setter", "points");
      }
      set points(pts){
            if(pts.length >= 2)
            {
                  this._checkUpdateGroupPropertyValue("setter", "points", pts)
            }else{
                  console.warn("Err : 경고 : PATH LINE은 점 두개 이상을 입력하셔야 합니다.");
            }
      }
      set particle_size(size){
            this.setGroupPropertyValue("extension", "particle_size", size);
      }
      get particle_size(){
            return this.getGroupPropertyValue("extension","particle_size")
      }
      set particle_speed(speed){
            this.setGroupPropertyValue("extension", "particle_speed", speed);
      }
      get particle_speed(){
            return this.getGroupPropertyValue("extension", "particle_speed");
      }
      set particle_segments(segments){
            this.setGroupPropertyValue("extension","particle_segments", segments);
      }
      get particle_segments(){
            return this.getGroupPropertyValue("extension", "particle_segments");
      }
      set particle_direction(direction){
            this.setGroupPropertyValue("extension", "particle_direction", direction)
      }
      get particle_direction(){
            return this.getGroupPropertyValue("extension", "particle_direction");
      }
      changePointByIndex(idx, point){
            if(idx < this.points.length && (point instanceof THREE.Vector3))
            {
                  var pts =[];
                  for(let i = 0 ;i < this.points.length; i++)
                  {
                        if(i !== idx)
                              pts.push(this.points[i])
                  }
                  pts.splice(idx,0, point);
                  this.points = pts;
            }else{
                  console.warn("올바른 입력 형식이 아닙니다. (number, THREE.Vector3)")
            }
      }
      _onCreateProperties(){
            super._onCreateProperties();
            // 사이즈 정보 writable을 false로 설정
            let sizeInfo = WVPropertyManager.getPropertyGroupChildrenByName(StreamPath3DComponent.property_panel_info, "display", "size");
            sizeInfo.writable = false;
            WVPropertyManager.removePropertyGroupChildrenByName(StreamPath3DComponent.property_info, "display", "size");

            this._elementSize = this.getDefaultProperties().setter.size;
            this._onHelperChangeCalle = this._onHelperChange.bind(this);
            this._onHelperClickCalle = this._onHelperClick.bind(this);
            this._onTransformMouseUpCalle = this._onTransformMouseUp.bind(this);

            //월드 포인트
            this._points = [];

            this._pointsOrder =  this.getGroupPropertyValue("helper_order", "order");

            //controls
            this._controls = window.wemb.mainPageComponent.threeLayer._transformControls;
            if(this.isEditorMode)
            {
                  this._controls.addEventListener("objectChange", this._onHelperChangeCalle, false);
                  this._controls.addEventListener("mouseUp", this._onTransformMouseUpCalle, false);
            }

            // 커브
            this._curve = null;
            this._curveSegments = 1000;

            //라인 피킹 세그먼트
            this._linePickingSegments = 300;

            // 두꺼운 선
            this._fatLine = null;

            //파티클 이미지
            this._particle = null;
            this._particleImg = this._setParticleImg();
            this._particleSegments = this.getGroupPropertyValue("extension","particle_segments");
            this._particleSize = this.getGroupPropertyValue("extension","particle_size");
            this._particleSpeed = this.getGroupPropertyValue("extension","particle_speed");
            this._particleDirection = this.getGroupPropertyValue("extension","particle_direction");

            //clock
            this.clock = new THREE.Clock();
            this._time = 1.0;

            this._element = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshPhongMaterial').clone());
            this._element.castShadow = true;
            this._element.receiveShadow = true;


      }
      _onCreateElement() {
            super._onCreateElement();
            var boxSize = 2;
            this._pointHelperGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
      }
      _onImmediateUpdateDisplay(){

            //helper 생성
            this._initPoints();

            //여기서 월드 행렬이 바뀌어 있으니 월드 로컬 점을 만들어준다.
            //월드는 그대로 쓰고 로컬 포인트를 만들어준다
            this._makeLocalAndWorldPoints();

            if(this.isEditorMode)
            {
                  //에디터라면 만들어진 점들을 가지고 helper를 만들어준다.
                  this._initPointHelper();
            }

            //커브를 그린다.
            //뚱라인을 그린다.
            //엘리먼트를 추가한다.
            //그리는건 로컬 좌표를 이용해서 그린다.
            this._makeCurve();
      }
      _initPoints(){
            //this.points는 월드좌표
            var v_up = new THREE.Vector3(30, 30, 30);
            var v_down = new THREE.Vector3(-30, -30, -30);

            var v_pos = new THREE.Vector3(this.position.x, this.position.y, this.position.z);
            var default_points = [new THREE.Vector3(0, 0, 0).addVectors(v_pos, v_up), new THREE.Vector3(0, 0, 0).addVectors(v_pos, v_down)];
            var pts = [];
            if(this._pointsOrder.length === 0)
            {
                  //새로 생성되는 거니까 월드좌표들을 만들어서 넣는다.
                  for(let i = 0 ; i < default_points.length; i++)
                  {
                        pts.push(default_points[i].clone());
                        this._pointsOrder.push(i);
                  }
                  this.points = pts;
            }else{

                  for(let i = 0 ; i < this._pointsOrder.length; i++)
                  {
                        pts[this._pointsOrder[i]] = (new THREE.Vector3(this.points[this._pointsOrder[i]].x, this.points[this._pointsOrder[i]].y, this.points[this._pointsOrder[i]].z));
                  }
                  this.points = pts;
            }
      }
      _makeLocalAndWorldPoints(){

            this._points = this.points;

            var pts = [];
            for(let i = 0 ; i < this.points.length; i++)
            {
                  var v = this.points[i].clone();
                  this.appendElement.worldToLocal(v);
                  pts.push(v);
            }
            this._localPoints = pts;
      }
      _initPointHelper(){
            var localHelper, worldHelper;
            //먼저 직접 에디터에서 조작하는 로컬 포인트 헬퍼
            for(let i = 0 ; i < this._pointsOrder.length; i++)
            {
                  localHelper = this._addPointHelperObject(this._localPoints[this._pointsOrder[i]]);
                  localHelper.name = "local"+this._pointsOrder[i];
                  this._domEvents.addEventListener(localHelper, 'click', this._onHelperClickCalle, false);
                  this.appendElement.add(localHelper);
            }
            //패널에 표현되는 월드좌표를 위한 월드 포인트 헬퍼
            for(let i = 0 ; i < this._pointsOrder.length; i++)
            {
                  worldHelper = this._addPointHelperObject(this._points[this._pointsOrder[i]]);
                  worldHelper.name = "world"+this._pointsOrder[i];
                  worldHelper.visible = false;
                  this.appendElement.add(worldHelper);
            }
      }

      _makeFatline(){
            super._makeFatline();
            this._makeParticle();
      }
      _makeParticle(){
            var particleMaterial = new THREE.PointsMaterial(
                  {
                        size : this._particleSize,
                        map : this._particleImg,
                        depthTest : true,
                        depthWrite : false,
                        transparent : true,
                        color : new THREE.Color(this._colors[0], this._colors[1], this._colors[2]),
                        blending:THREE.AdditiveBlending,
                  });

            var particle_path = this._curve.getPoints(this._particleSegments);
            var particleGeometry = new THREE.BufferGeometry();
            particleGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array((particle_path.length - 1) * 3), 3));

            for(var i = 0 ; i < particle_path.length ; i++) {
                  particleGeometry.attributes.position.setXYZ(i, particle_path[i].x, particle_path[i].y, particle_path[i].z);
            }
            this._particle = new THREE.Points(particleGeometry, particleMaterial);
            this._particle.dynamic = true;
            this.appendElement.add(this._particle);
      }
      _rgbToArray(color){
            var color_arr = color.substring(4, color.length-1)
                  .replace(/ /g, '')
                  .split(',');
            return [color_arr[0]/255,color_arr[1]/255,color_arr[2]/255]
      }
      _hexToRgb(hex) {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                  return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                  r: parseInt(result[1], 16) / 255,
                  g: parseInt(result[2], 16) / 255,
                  b: parseInt(result[3], 16) / 255
            } : null;
      }
      //라인 업데이트 합수
      _Update(){
            super._Update();

            //파티클 업데이트
            var particle_path = this._curve.getPoints(this._particleSegments);

            this._particle.geometry.removeAttribute('position');
            this._particle.geometry.addAttribute('position', new THREE.BufferAttribute( new Float32Array( (particle_path.length - 1) * 3 ), 3 ));

            for(var i = 0 ; i < particle_path.length; i++)
            {
                  this._particle.geometry.attributes.position.setXYZ(i, particle_path[i].x, particle_path[i].y, particle_path[i].z);
            }
            this._particle.geometry.attributes.position.needsUpdate = true;
            this._particle.material.size = this._particleSize;
            this._particle.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);

      }

      _onCommitProperties(){
            super._onCommitProperties();
            if(this._updatePropertiesMap.has("setter.color")){
                  var cl =this.getGroupPropertyValue("setter", "color");
                  if(cl.indexOf('#') > -1)
                  {
                        var c = this._hexToRgb(cl);
                        this._colors = [c.r, c.g, c.b]
                  }else{
                        this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                  }
                  this._Update();

            }
            if(this._updatePropertiesMap.has("line_width")){
                  if(this.width < 1)
                        this.width = 1;
                  this._Update();
            }
            if(this._updatePropertiesMap.has("setter.position") || this._updatePropertiesMap.has("setter.rotation") || this._updatePropertiesMap.has("setter.size"))
            {
                  this.validateCallLater(this._syncPropertyToHelper);
            }
            if(this._updatePropertiesMap.has("setter","points"))
            {
                  this._syncInputedPoints();
                  this._Update();
            }
            if(this._updatePropertiesMap.has("syncPoint.points"))
            {
                  if(this.isEditorMode)
                  {
                        this._syncWorldLocal();
                  }
                  this._Update();
            }
            if(this._updatePropertiesMap.has("helper_count.count"))
            {
                  if (this.isEditorMode) {
                        if(this.getGroupPropertyValue("helper_count", "count") < 2){
                              console.warn("경고 : 라인은 최소 2개 이상의 점을 필요로 합니다.");
                        }else if(this.getGroupPropertyValue("helper_count", "count") > 50){
                              console.warn("경고 : 최대 50개의 점 까지 입력 가능합니다.");
                        }else{
                              this._addPointByCount(this.getGroupPropertyValue("helper_count", "count"));

                              this._lineAdjustment();

                              this._Update();
                        }
                  }
            }
            if (this._updatePropertiesMap.has("extension")) {
                  this.validateCallLater(this._syncParticleProperty)
            }
      }
      _syncParticleProperty(){
            this._particleImg = this._setParticleImg();
            this._particle.material.map = this._particleImg;
            if(this.getGroupPropertyValue("extension","particle_segments") < 0)
                  this._particleSegments = 1;
            else
                  this._particleSegments = this.getGroupPropertyValue("extension","particle_segments");

            if(this.getGroupPropertyValue("extension","particle_size") < 0)
                  this._particleSize = 1;
            else
                  this._particleSize = this.getGroupPropertyValue("extension","particle_size");

            this._particleSpeed = this.getGroupPropertyValue("extension","particle_speed");
            this._particleDirection = this.getGroupPropertyValue("extension","particle_direction");
            this._element.visible = this.getGroupPropertyValue("extension","line_visible");
            this._fatLine.visible = this.getGroupPropertyValue("extension","line_visible");

            this._Update();
      }
      _syncInputedPoints(){
            //입력된 점이 에티터에 있는 헬퍼보다 많으면 점을 추가하고 그린다
            this._addPointByCount(this.points.length); //입력된 점의 수에 맞춰서 헬퍼만 조정된 상태.

            //입력된 점을 기준으로 점을 업데이트 한다.
            for(let i = 0 ; i < this._pointsOrder.length; i++)
            {
                  this._points[i].x = this.points[i].x;
                  this._points[i].y = this.points[i].y;
                  this._points[i].z = this.points[i].z;
                  var v = this.points[i].clone();
                  this.appendElement.worldToLocal(v);
                  this._localPoints[i] = v;
            }

            //에디터에서는
            if(this.isEditorMode)
            {
                  //업데이트 된 점을 기준으로 헬퍼들의 배치를 다시 한다.
                  for(let i = 0; i < this._pointsOrder.length ; i++)
                  {
                        //로컬
                        var localHelper = this.appendElement.getObjectByName("local"+this._pointsOrder[i]);
                        var v = this._points[i].clone();
                        this.appendElement.worldToLocal(v);
                        if(localHelper !== undefined)
                        {
                              localHelper.position.copy(v);
                        }

                        //월드
                        var worldHelper = this.appendElement.getObjectByName("world"+this._pointsOrder[i]);
                        if(worldHelper !== undefined)
                        {
                              worldHelper.position.copy(this._points[i]);
                        }
                  }
            }

            this._lineAdjustment();
            this._Update();
      }

      _syncPropertyToHelper(){
            //행렬을 업데이트 시켜주고.
            this.appendElement.updateMatrixWorld(true);

            //로컬좌표를 다시 변환하여 월드에 셋팅한다.
            for(let i = 0; i < this._pointsOrder.length; i++)
            {
                  //에디터 모드라면 헬퍼들의 속성을 조절해줘야 한다.(월드 로컬 모두)
                  if(this.isEditorMode)
                  {
                        //점도 계산되어 바뀌어야 한다.
                        var v = this._localPoints[i].clone();
                        this.appendElement.localToWorld(v);

                        var worldHelper = this.appendElement.getObjectByName("world"+this._pointsOrder[i]);
                        var localHelper = this.appendElement.getObjectByName("local"+this._pointsOrder[i]);

                        if(worldHelper !== undefined)
                        {
                              worldHelper.position.copy(v);
                              this._points[i] = v;
                        }
                        //월드 헬퍼는 안보이니까 굳이 필요 없다.
                        //로컬 헬퍼 박스는 스케일 회전을 다시 원상복구 시켜야 한다.
                        if(localHelper  !== undefined){
                              var scale = this.appendElement.getWorldScale();
                              localHelper.rotation.set(0, 0, 0);
                              localHelper.scale.set(1/(scale.x), 1/(scale.y), 1/(scale.z))
                        }
                  }else{
                        //에디터모드가 아니라면 점 싱크 처리만 하면 된다.
                        this.appendElement.localToWorld(this._localPoints[i]);
                  }
            }

      }
      _syncWorldLocal(){
            //월드좌표를 보고 로컬을 다시 바꿔준다.
            this.appendElement.updateMatrixWorld(true);
            for(let i = 0 ; i < this._pointsOrder.length; i++)
            {
                  var v = this._points[i].clone();
                  this.appendElement.worldToLocal(v);
                  var worldHelper = this.appendElement.getObjectByName("world"+this._pointsOrder[i]);
                  var localHelper = this.appendElement.getObjectByName("local"+this._pointsOrder[i]);

                  if(this.isEditorMode)
                  {
                        if(worldHelper !== undefined)
                        {
                              worldHelper.position.copy(this.points[i]);
                        }
                        //월드 헬퍼는 안보이니까 굳이 필요 없다.
                        //로컬 헬퍼 박스는 스케일 회전을 다시 원상복구 시켜야 한다.
                        if(localHelper  !== undefined)
                        {
                              localHelper.position.copy(v);
                        }
                  }
                  //에디터, 뷰어 상관없이 점은 바뀌어야 한다
                  this._localPoints[i] = v;
            }

            //중점을 조정한다
            this._lineAdjustment();

            //라인을 다시 그려준다
            this._Update();
      }
      _syncLocalWorld(){
            //로컬좌표를 보고 월드를 바꿔준다
            this.appendElement.updateMatrixWorld(true);
            for(let i = 0 ;i  < this._pointsOrder.length; i++)
            {
                  var v = this._localPoints[i].clone();
                  this.appendElement.localToWorld(v);
                  var worldHelper = this.appendElement.getObjectByName("world"+this._pointsOrder[i]);

                  if(this.isEditorMode)
                  {
                        if(worldHelper !== undefined)
                        {
                              worldHelper.position.copy(v);
                        }
                  }
                  //에디터, 뷰어 상관없이 점은 바뀌어야 한다
                  this._points[i] = v;
            }
      }
      _lineAdjustment(){
            //월드 점이 움직이던, 로컬 점이 움직이던
            //라인의 중점과, 로컬의 원점과 싱크를 맞춰줍니다.(평행이동을 해서~)
            this.appendElement.updateMatrixWorld(true);
            if(this._curve !== null)
            {
                  this._curve.points = this._localPoints;
                  var center = this._curve.getPoint(0.5).clone();
                  var move = center.clone();
                  this.appendElement.localToWorld(move);

                  if(this.isEditorMode)
                  {
                        //월드 포지션 싱크를 맞춰줍니다.
                        for(let i = 0 ; i < this._pointsOrder.length; i++)
                        {
                              var worldHelper = this.appendElement.getObjectByName("world"+this._pointsOrder[i]);
                              if(worldHelper !== undefined)
                              {
                                    worldHelper.position.copy(this._points[i])
                              }
                        }
                  }
                  //로컬 포지션 싱크만 맞춰줍니다.
                  //월드 포지션은 어차피 원래 설정하고자 하는 좌표가 되어 있기 때문입니다.
                  for(let i = 0 ; i < this._pointsOrder.length; i++)
                  {
                        var v = this._points[i].clone();
                        this.appendElement.worldToLocal(v);
                        v.sub(center);
                        if(this.isEditorMode)
                        {
                              var localHelper = this.appendElement.getObjectByName("local"+this._pointsOrder[i]);
                              if(localHelper !== undefined)
                              {

                                    localHelper.position.x = v.x;
                                    localHelper.position.y = v.y;
                                    localHelper.position.z = v.z;
                              }
                        }
                        this._localPoints[i] = v;
                  }
                  this.appendElement.position.x += center.x;
                  this.appendElement.position.y += center.y;
                  this.appendElement.position.z += center.z;
                  this._properties.setter.position.x =  parseInt(this.appendElement.position.x);
                  this._properties.setter.position.y =  parseInt(this.appendElement.position.y);
                  this._properties.setter.position.z =  parseInt(this.appendElement.position.z);
                  // var pos = {x : this.appendElement.position.x, y : this.appendElement.position.y, z : this.appendElement.position.z}
                  // this.setGroupPropertyValue("setter","position", pos )
            }
      }

      _addPoint(){
            //로컬!
            //점을 선 위의 점 하나로 잡아서 삽입
            var dist = 0;
            var idx = 0;
            for(var i = 0 ; i < this._localPoints.length - 1; i++)
            {
                  var distance = this._localPoints[i].distanceTo(this._localPoints[i+ 1]);
                  if(dist < distance)
                  {
                        dist = distance;
                        idx = i;
                  }
            }
            if(this.isEditorMode)
            {
                  this._addPointHelper(idx)
            }
      }
      _addPointHelper(idx){
            // 최종 idx, idx+1 사이에 점을 삽입한다.
            var newPoint = new THREE.Vector3();
            newPoint.addVectors(this._localPoints[idx], this._localPoints[idx + 1]);
            newPoint.x *= 0.5;
            newPoint.y *= 0.5;
            newPoint.z *= 0.5;

            //로컬 점을 셋팅한다.
            this._pointsOrder.splice(idx + 1, 0, this._localPoints.length);
            this._localPoints.splice(idx + 1, 0, newPoint);

            var max_idx = this._pointsOrder.indexOf(Math.max(...this._pointsOrder));

            //새로운 헬퍼를 만든다.
            var localHelper = this._addPointHelperObject(newPoint);
            localHelper.name = 'local'+ (this._pointsOrder[max_idx]);
            this._domEvents.addEventListener(localHelper, 'click', this._onHelperClick, false);
            this.appendElement.add(localHelper);

            //월드 헬퍼
            var v = localHelper.position.clone();
            this.appendElement.localToWorld(v);
            var worldHelper = this._addPointHelperObject(v);
            worldHelper.name = 'world'+ (this._pointsOrder[max_idx]);
            worldHelper.visible = false;
            this.appendElement.add(worldHelper);
            this._points.splice(idx + 1, 0, v);
      }
      _removePoint(){
            //로컬 점을 뺀다.
            var poped_idx = this._pointsOrder.indexOf(Math.max(...this._pointsOrder));
            if(this.isEditorMode)
            {
                  var poped = this.appendElement.getObjectByName('local'+ this._pointsOrder[poped_idx]);
                  this.appendElement.remove(poped);
                  poped.geometry.dispose();
                  poped.material.dispose();

                  poped = this.appendElement.getObjectByName('world'+ this._pointsOrder[poped_idx]);
                  this.appendElement.remove(poped);
                  poped.geometry.dispose();
                  poped.material.dispose();

            }

            this._pointsOrder.splice(poped_idx, 1);
            this._localPoints.splice(poped_idx, 1);
            this._points.splice(poped_idx, 1);
      }
      _addPointByCount(cnt){
            if(cnt > 0)
            {
                  var len = this.pointCount;

                  if(cnt < len)
                  {
                        for(i = 0; i < (len - cnt); i++)
                        {
                              this._removePoint();
                        }
                  }
                  else{
                        var need_to_add = cnt - len;

                        for(var i = 0 ; i < need_to_add; i++)
                        {
                              this._addPoint();
                        }

                  }
                  this.setter.pointCount = this.points.length;
                  this.setGroupPropertyValue("helper_count","count", this.points.length)
            }
      }


      _onHelperChange(e){
            //움직이때마다 로컬 헬퍼 좌표와 월드 헬퍼 좌표를 바꾼다.
            for(let i = 0 ; i < this._pointsOrder.length; i++)
            {
                  var localHelper = this.appendElement.getObjectByName("local"+this._pointsOrder[i]);
                  if(localHelper !== undefined)
                  {

                        this._localPoints[i] = localHelper.position.clone();
                        var v = localHelper.position.clone();
                        this.appendElement.localToWorld(v);
                        this._points[i] = v;
                        this._syncLocalWorld();
                  }
            }
            this._Update();
      }
      _onTransformMouseUp(e){
            var ctrl = window.wemb.mainPageComponent.threeLayer._transformControls;
            ctrl.detach(e.target);
            this._lineAdjustment()
            this._Update();
            ctrl.space = "local";
      }
      _onHelperClick(e){
            e.stopPropagation();
            e.origDomEvent.stopPropagation();
            let ctrl= window.wemb.mainPageComponent.threeLayer._transformControls;
            ctrl.attach(e.target);
            ctrl.space = "local";
      }
      //rendering loop
      get usingRenderer() {
            if(this.isEditorMode)
                  return false;
            else
                  return true;
      }
      render() {

            var t0 = this.clock.getElapsedTime();
            this._time = 0.125 * t0 * this._particleSpeed;
            var p;

            if(!this._particle)
                  return;

            var len = this._particle.geometry.attributes.position.array.length;
            for( var v = 0; v <= len - 3; v+=3 )
            {
                  var timeOffset = this._time;

                  if((timeOffset + (v / (len))) > 1)
                  {
                        this._time = 0;
                        delete this.clock;
                        this.clock = new THREE.Clock();
                        break;
                  }
                  if(this._particleDirection)
                        p = this._curve.getPoint(timeOffset + (v / len));
                  else
                        p = this._curve.getPoint(1 - (timeOffset + (v / len)));

                  this._particle.geometry.attributes.position.setXYZ(v / 3, p.x, p.y, p.z);
            }
            this._particle.geometry.attributes.position.needsUpdate = true;

      }

      //파티클 이미지
      getImageUrl(str) {
            return wemb.configManager.serverUrl + str;
      }
      get extension() {
            return this.getGroupProperties("extension");
      }
      //particle image load
      _setParticleImg(){
            if (this.extension.particle_image && this.extension.particle_image.path) {
                  let upStateImgUrl = this.getImageUrl(this.extension.particle_image.path);
                  return new THREE.TextureLoader().load(upStateImgUrl);
            }else{
                  return new THREE.TextureLoader().load( "custom/packs/3d_pack/components/images/particle.png" );
            }
      }
      _makeWorldMatrix()
      {
            var pos = this.getGroupPropertyValue("setter","position");
            var rot = this.getGroupPropertyValue("setter", "rotation");
            var scale = this.getGroupPropertyValue("setter", "size");
            var m = new THREE.Matrix4();
            m.compose(
                  new THREE.Vector3(pos.x, pos.y, pos.z),
                  new THREE.Quaternion().setFromEuler(new THREE.Euler(rot.x *  Math.PI/180, rot.y*  Math.PI/180, rot.z * Math.PI/180)),
                  new THREE.Vector3(scale.x * 0.1, scale.y * 0.1, scale.z * 0.1));
            return m;
      }
      _onDestroy(){
            this.appendElement.remove(this._particle);
            this._particle.geometry.dispose();
            this._particle.material.dispose();
            this._particle = null;
            super._onDestroy();
      }


}
WV3DPropertyManager.attach_default_component_infos(StreamPath3DComponent, {
      "setter": {
            "size": {x: 10, y: 10, z: 10},
            "pointCount" : 2,
            "points" : [],
            "pointCountRange" : {
                  min : 0,
                  max : 50
            },
            //"helper_size" : 1
      },
      "connection" : {
            "targets" : {}
      },
      "label": {
            "label_text": "StreamPath3DComponent",
            "label_line_size": 15,
            "label_background_color": "#3351ED",
            "label_using": "N",
      },

      "info": {
            "componentName": "StreamPath3DComponent",
            "version": "1.0.0",
      },
      "extension" : {
            "particle_image" : null,
            "particle_segments" : 10,
            "particle_speed" : 1,
            "particle_size" : 20,
            "particle_direction" : true,
            "line_visible" : true
      },
      "line_width" : {
            "width" : 2
      },
      "line_tension" : {
            "tension" : 0
      },
      "syncPoint" : {
            "point" : "",
            "scale" : 1
      },
      "helper_count" :{
            "count" : 2
      },
      "helper_order" : {
            "order" : []
      },
      "dash" : {
            "dash" : false,
            "dashSize" : 1,
            "dashScale" : 1,
            "gapSize" : 1
      }
});


WV3DPropertyManager.add_property_panel_group_info(StreamPath3DComponent, {
      label : "Particle Properties",
      template : "vertical",
      children : [
            {
                  owner : "extension",
                  name : "particle_segments",
                  type : "number",
                  label : "Segments",
                  show : true,
                  writable : true,
                  description : "파티클 개수"
            },
            {
                  owner : "extension",
                  name : "particle_speed",
                  type : "number",
                  label : "Speed",
                  show : true,
                  writable : true,
                  description : "파티클 속도"
            },
            {
                  owner : "extension",
                  name : "particle_size",
                  type : "number",
                  label : "Size",
                  show : true,
                  writable : true,
                  description : "파티클 크기"
            },
            {
                  owner : "extension",
                  name : "particle_direction",
                  type : "checkbox",
                  label : "Direction",
                  show : true,
                  writable : true,
                  description : "파티클 진행 방향"
            }
      ]
});
WVPropertyManager.add_property_panel_group_info(StreamPath3DComponent, {
      label: "파티클 리소스 설정",
      template: "resource",
      children: [{
            owner: "extension",
            name: "particle_image",
            type: "resource",
            label: "Image",
            resource_options: {
                  minLength: 1,
                  maxLenght: 1,
                  type: "image"
            },
            show: true,
            writable: true,
            description: "이미지 리소스 선택"
      }
      ]
});
WV3DPropertyManager.add_property_panel_group_info(StreamPath3DComponent, {
      label : "Line Properties",
      template : "vertical",
      children : [
            {
                  owner : "line_width",
                  name : "width",
                  type : "number",
                  label : "Width",
                  show : true,
                  writable : true,
                  description : "선 두께"
            },
            {
                  owner : "extension",
                  name : "line_visible",
                  type : "checkbox",
                  label : "Line Visible",
                  show : true,
                  writable : true,
                  description : "선 보이게/안보이게"
            }
      ]
});

WVPropertyManager.add_property_panel_group_info(StreamPath3DComponent, {
      label: "controlPoint",
      template: "points",
      children: [{
            owner: "syncPoint",
            name: "point",
            type: "object",
            label: "Length",
            show: true,
            writable: true,
            description: "point"
      }]
});

WV3DPropertyManager.add_property_panel_group_info(StreamPath3DComponent, {
      label : "Dash Properties",
      template : "vertical",
      children : [
            {
                  owner : "dash",
                  name :"dash",
                  type : "checkbox",
                  label : "Dash",
                  show : true,
                  writable : true,
                  description : "대쉬"
            },
            {
                  owner : "dash",
                  name : "dashSize",
                  type : "number",
                  label : "DashSize",
                  show : true,
                  writable : true,
                  description : "대쉬 사이즈"
            },
            {
                  owner : "dash",
                  name : "dashScale",
                  type : "number",
                  label : "DashScale",
                  show : true,
                  writable : true,
                  description : "대쉬 스케일"
            },
            {
                  owner : "dash",
                  name : "gapSize",
                  type : "number",
                  label : "GapSize",
                  show : true,
                  writable : true,
                  description : "갭 사이즈"
            }
      ]
})
// WV3DPropertyManager.add_property_panel_group_info(StreamPath3DComponent, {
//       label : "Helper Size",
//       template : "vertical",
//       children : [
//             {
//                   owner : "setter",
//                   name : "helper_size",
//                   type : "number",
//                   label : "HelperSize",
//                   show : true,
//                   writable : true,
//                   description : "헬퍼 사이즈"
//             }
//       ]
// })
