class Circle3DComponent extends NWV3DComponent {
      constructor() {
            super();
      }

      _onCreateProperties(){
            super._onCreateProperties();

            // 사이즈 정보 writable을 false로 설정
            // let sizeInfo = WVPropertyManager.getPropertyGroupChildrenByName(Circle3DComponent.property_panel_info, "display", "size");
            // sizeInfo.writable = false;
            // WVPropertyManager.removePropertyGroupChildrenByName(Circle3DComponent.property_info, "display", "size");

            this._elementSize = this.getDefaultProperties().setter.size;

            //color
            var cl =this.getGroupPropertyValue("setter", "color");
            if(cl.indexOf('#') > -1)
            {
                  var c = this._hexToRgb(cl);
                  this._colors = [c.r, c.g, c.b]
            }else{
                  this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
            }

            //width
            this._width = this.getGroupPropertyValue("line_width", "width");

            // curve spline
            //curve
            this._curve = null;
            this._curveSegments = 100;
            this._xRadius = this.getGroupPropertyValue("radius","xRadius");
            this._yRadius = this.getGroupPropertyValue("radius","yRadius");

            this._curveFillSegments = 128;
            this._curveFill = null;

            // line picking segments
            this._linePickingSegments = 300;

            // fatline mesh;
            this._fatLine = null;
      }

      _onCreateElement() {
            super._onCreateElement();
            var boxSize = 2;
            this._pointHelperGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
            this._makeCurve();
      }
      _onCommitProperties(){
            super._onCommitProperties();
            if(this._updatePropertiesMap.has("setter.color"))
            {
                  var cl = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                  this._colors[0] = cl[0];
                  this._colors[1] = cl[1];
                  this._colors[2] = cl[2];
                  this._Update();
            }
            if(this._updatePropertiesMap.has("line_width"))
            {
                  this._width =  this.getGroupPropertyValue("line_width","width");
                  this._Update();

            }
            if(this._updatePropertiesMap.has("fill.fill"))
            {
                  if(this.getGroupPropertyValue("fill","fill"))
                  {
                        this._curveFill.visible = true;
                  }else{
                        this._curveFill.visible = false;
                  }
                  this._Update();
            }
            if(this._updatePropertiesMap.has("radius"))
            {
                  this._xRadius = this.getGroupPropertyValue("radius", "xRadius");
                  this._yRadius = this.getGroupPropertyValue("radius", "yRadius");
                  this._Update();
            }
      }
      _makeCurve(){
            this._curve = new THREE.EllipseCurve(0, 0, this._xRadius, this._yRadius, 0, 2 * Math.PI, false, 0);

            var curveGeometry = new THREE.BufferGeometry();
            curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curveSegments * 3), 3));
            curveGeometry.setFromPoints(this._curve.getPoints(this._curveSegments));

            var curveMaterial = new THREE.LineBasicMaterial({color : new THREE.Color(this._colors[0], this._colors[1], this._colors[2])});
            var curveMesh = new THREE.LineSegments(curveGeometry, curveMaterial);//new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);
            this._curve.mesh = curveMesh;
            this._curve.mesh.castShadow = true;
            this._element = this._curve.mesh;


            if(this.getGroupPropertyValue("fill", "fill"))
            {
                  var curveFillGeometry = new THREE.CircleGeometry(1, this._curveFillSegments);
                  var pt = this._curve.getPoints(this._curveFillSegments - 1);
                  for(var i = 0; i < pt.length; i++){
                        curveFillGeometry.vertices[i] = new THREE.Vector3(pt[i].x, pt[i].y, 0);
                  }
                  this._curveFill = new THREE.Mesh(curveFillGeometry,
                        new THREE.MeshBasicMaterial( { color: new THREE.Color(this._colors[0], this._colors[1], this._colors[2]), side: THREE.DoubleSide} ))
                  this.appendElement.add(this._curveFill);
            }
            this.appendElement.add(this._element);
            this._makeFatline();
      }
      _makeFatline(){
            var fatline_points = this._curve.getPoints(this._curveSegments-1);
            var fatline_positions = [];
            var fatline_colors = [];

            for(var i = 0 ; i < fatline_points.length ;i++)
            {
                  fatline_positions.push(fatline_points[i].x, fatline_points[i].y, 0);
                  fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
            }

            var fatlineGeometry = new THREE.LineGeometry();
            fatlineGeometry.setPositions(fatline_positions);
            fatlineGeometry.setColors(fatline_colors);

            var fatlineMaterial = new THREE.LineMaterial({
                  color :  0xffffff,
                  linewidth : this._width,
                  vertexColors : THREE.VertexColors,
                  dashed: false
            });
            fatlineMaterial.resolution.set(window.innerWidth, window.innerHeight);

            this._fatLine = new THREE.Line2(fatlineGeometry, fatlineMaterial);
            this._fatLine.computeLineDistances();
            this._fatLine.scale.set(1,1,1);
            this.appendElement.add(this._fatLine);
      }

      /** 모든것을 한번에 업데이트하는 함수 */
      _Update()
      {
            this._curve.xRadius = this._xRadius;
            this._curve.yRadius = this._yRadius;
            this._curve.mesh.geometry.setFromPoints(this._curve.getPoints(this._curveSegments));
            this._curve.mesh.geometry.attributes.position.needsUpdate = true;
            this._curve.mesh.geometry.computeBoundingBox();
            this._curve.mesh.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);

            var pts = this._curve.getPoints(this._curveSegments - 1);
            var fatline_positions = [];
            var fatline_colors = [];
            for(var i = 0; i < pts.length; i++)
            {
                  fatline_positions.push(pts[i].x, pts[i].y, 0);
                  fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
            }
            this._fatLine.geometry.setPositions(fatline_positions);
            this._fatLine.geometry.setColors(fatline_colors);

            this._fatLine.material.linewidth = this._width;

            if(this._curveFill !== null)
            {
                  var pt = this._curve.getPoints(this._curveFillSegments - 1);
                  for(var i = 0; i < pt.length; i++){
                        this._curveFill.geometry.vertices[i] = new THREE.Vector3(pt[i].x, pt[i].y, 0);
                  }
                  this._curveFill.geometry.verticesNeedUpdate = true;
                  this._curveFill.geometry.elementsNeedUpdate = true;

                  this._curveFill.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2])
            }
      }

      _hexToRgb(hex) {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                  return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                  r: parseInt(result[1], 16) / 255,
                  g: parseInt(result[2], 16) / 255,
                  b: parseInt(result[3], 16) / 255
            } : null;
      }
      _rgbToArray(color){
            var color_arr = color.substring(4, color.length-1)
                  .replace(/ /g, '')
                  .split(',');
            return [color_arr[0]/255,color_arr[1]/255,color_arr[2]/255]
      }
      _onDestroy(){

            this.appendElement.remove(this._fatLine);
            this._fatLine.geometry.dispose();
            this._fatLine.material.dispose();
            this._fatLine = null;

            this.appendElement.remove(this._curve);
            this._curve.mesh.geometry.dispose();
            this._curve.mesh.material.dispose();
            this._curve = null;

            if(this._curveFill !== null)
            {
                  this.appendElement.remove(this._curveFill);
                  this._curveFill.geometry.dispose();
                  this._curveFill.material.dispose();
                  this._curveFill = null;
            }
            super._onDestroy();
      }

      get width(){
            return this.getGroupPropertyValue("line_width", "width");
      }
      set width(wd){
            if(wd < 1)
                  this.setGroupPropertyValue("line_width", "width", 1);
            else
                  this.setGroupPropertyValue("line_width", "width", wd)
      }
}
WV3DPropertyManager.attach_default_component_infos(Circle3DComponent, {
      "setter": {
            "size": {x: 10, y: 10, z: 10},
      },
      "label": {
            "label_text": "Circle3DComponent",
            "label_line_size": 15,
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "Circle3DComponent",
            "version": "1.0.0",
      },
      "line_width" : {
            "width" : 2
      },
      "radius" :
            {
                  "xRadius" : 10,
                  "yRadius" : 10
            },
      "fill" :
            {
                  "fill" : true
            }
});
WV3DPropertyManager.add_property_panel_group_info(Circle3DComponent, {
      label : "Shape Properties",
      template : "vertical",
      children : [
            {
                  owner : "radius",
                  name : "xRadius",
                  type : "number",
                  label : "Y Radius",
                  show : true,
                  writable : true,
                  description : "X축 길이"
            },
            {
                  owner : "radius",
                  name : "yRadius",
                  type : "number",
                  label : "Y Radius",
                  show : true,
                  writable : true,
                  description : "Y축 길이"
            },
            {
                  owner : "fill",
                  name :"fill",
                  type : "checkbox",
                  label : "Fill",
                  show : true,
                  writable : true,
                  description : "채우기"
            }

      ]
});
WV3DPropertyManager.add_property_panel_group_info(Circle3DComponent, {
      label : "Line Properties",
      template : "vertical",
      children : [
            {
                  owner : "line_width",
                  name : "width",
                  type : "number",
                  label : "Width",
                  show : true,
                  writable : true,
                  description : "선 두께"
            }
      ]
});
