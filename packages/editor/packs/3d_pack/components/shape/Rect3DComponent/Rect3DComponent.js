class Rect3DComponent extends NWV3DComponent {
      constructor(){
            super();
      }

      _onCreateProperties(){
            super._onCreateProperties();
            this._elementSize = this.getDefaultProperties().setter.size;

            //color
            var cl =this.getGroupPropertyValue("setter", "color");
            if(cl.indexOf('#') > -1)
            {
                  var c = this._hexToRgb(cl);
                  this._colors = [c.r, c.g, c.b]
            }else{
                  this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
            }

            //width
            this._width = this.getGroupPropertyValue("line_width", "width");

            //curve
            this._curve = null;
            this._curveSegments = 100;
            this._rectWidth = this.getGroupPropertyValue("rect", "rect_width");
            this._rectHeight = this.getGroupPropertyValue("rect", "rect_height");

            this._fillRect = null;
            this._fatLine = null;

            //내부 모서리 점
            this._innerPoints = [];
      }
      _onCreateElement() {
            super._onCreateElement();
            var boxSize = 2;
            this._pointHelperGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
            this._makeInnerPoints();
            this._makeCurve();
      }
      _onCommitProperties(){
            super._onCommitProperties();

            if(this._updatePropertiesMap.has("setter.color"))
            {
                  var cl = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                  this._colors[0] = cl[0];
                  this._colors[1] = cl[1];
                  this._colors[2] = cl[2];
                  this._Update();
            }
            if(this._updatePropertiesMap.has("line_width"))
            {
                  this._width =  this.getGroupPropertyValue("line_width","width");
                  this._Update();
            }
            if(this._updatePropertiesMap.has("rect.rect_width") || this._updatePropertiesMap.has("rect.rect_height"))
            {
                  this._rectWidth = this.getGroupPropertyValue("rect", "rect_width");
                  this._rectHeight = this.getGroupPropertyValue("rect", "rect_height");
                  this._Update();
            }
            if(this._updatePropertiesMap.has("fill.fill"))
            {
                  if(this.getGroupPropertyValue("fill","fill"))
                  {
                        this._fillRect.visible = true;
                  }else{
                        this._fillRect.visible = false;
                  }
                  this._Update();
            }
      }

      _makeCurve(){
            this._curve = new THREE.CatmullRomCurve3(this._innerPoints);
            this._curve.tension = 0;
            this._curve.curveType = 'catmullrom';

            var curveGeometry = new THREE.BufferGeometry();
            curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curveSegments * 3),3));

            curveGeometry.setFromPoints(this._curve.getPoints(this._curveSegments - 1));
            var curveMaterial = new THREE.LineBasicMaterial({color : new THREE.Color(this._colors[0], this._colors[1], this._colors[2])});
            var curveMesh = new THREE.LineSegments(curveGeometry, curveMaterial);//new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);

            this._curve.mesh = curveMesh;
            this._element = this._curve.mesh;

            if(this.getGroupPropertyValue("fill", "fill"))
            {
                  var curveFillGeometry = new THREE.PlaneGeometry(this._rectWidth * 2, this._rectHeight * 2, 1, 1);
                  this._fillRect = new THREE.Mesh(curveFillGeometry,
                        new THREE.MeshBasicMaterial( { color: new THREE.Color(this._colors[0], this._colors[1], this._colors[2]), side: THREE.DoubleSide} ))
                  this.appendElement.add(this._fillRect);
            }

            this.appendElement.add(this._element);
            this._makeFatLine();
      }
      _makeFatLine(){
            var fatline_positions = [];
            var fatline_colors = [];

            for(var i = 0 ; i < this._innerPoints.length ;i++)
            {
                  fatline_positions.push(this._innerPoints[i].x, this._innerPoints[i].y, 0);
                  fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
            }

            var fatlineGeometry = new THREE.LineGeometry();
            fatlineGeometry.setPositions(fatline_positions);
            fatlineGeometry.setColors(fatline_colors);

            var fatlineMaterial = new THREE.LineMaterial({
                  color : 0xffffff,
                  linewidth : this._width,
                  vertexColors : THREE.VertexColors,
            });
            fatlineMaterial.resolution.set(window.innerWidth, window.innerHeight);
            this._fatLine = new THREE.Line2(fatlineGeometry, fatlineMaterial);

            this._fatLine.computeLineDistances();
            this._fatLine.scale.set(1,1,1);

            this.appendElement.add(this._fatLine);
      }
      _Update(){
            this._makeInnerPoints();
            this._curve.points = this._innerPoints;
            this._element.geometry.setFromPoints(this._curve.getPoints(this._curveSegments));
            this._element.geometry.attributes.position.needsUpdate = true;
            this._element.geometry.computeBoundingBox();
            this._element.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);

            this._fatLine.material.linewidth = this._width;

            var fatline_positions = [];
            var fatline_colors = [];
            for(var i = 0; i < this._innerPoints.length; i++)
            {
                  fatline_positions.push(this._innerPoints[i].x, this._innerPoints[i].y, 0);
                  fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
            }

            this._fatLine.geometry.dispose();
            var fatlineGeometry = new THREE.LineGeometry();
            fatlineGeometry.setPositions(fatline_positions);
            fatlineGeometry.setColors(fatline_colors);
            this._fatLine.geometry= fatlineGeometry;

            if(this._fillRect !== null)
            {
                  this._fillRect.geometry.dispose();
                  var rectGeometry = new THREE.PlaneGeometry(this._rectWidth * 2, this._rectHeight * 2, 1, 1);
                  this._fillRect.geometry = rectGeometry;
                  this._fillRect.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2])

            }
      }

      _makeInnerPoints(){
            this._innerPoints = [];
            this._innerPoints[0] = new THREE.Vector3(-1 * this._rectWidth, -1 * this._rectHeight, 0);
            this._innerPoints[1] = new THREE.Vector3(-1 * this._rectWidth, this._rectHeight, 0);
            this._innerPoints[2] = new THREE.Vector3(this._rectWidth, this._rectHeight, 0);
            this._innerPoints[3] = new THREE.Vector3(this._rectWidth, -1 * this._rectHeight, 0);
            this._innerPoints[4] = new THREE.Vector3(-1 * this._rectWidth, -1 * this._rectHeight, 0);
      }

      _rgbToArray(color){
            var color_arr = color.substring(4, color.length-1)
                  .replace(/ /g, '')
                  .split(',');
            return [color_arr[0]/255,color_arr[1]/255,color_arr[2]/255]
      }
      _hexToRgb(hex) {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                  return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                  r: parseInt(result[1], 16) / 255,
                  g: parseInt(result[2], 16) / 255,
                  b: parseInt(result[3], 16) / 255
            } : null;
      }
      _onDestroy(){
            this.appendElement.remove(this._fatLine);
            this._fatLine.geometry.dispose();
            this._fatLine.material.dispose();
            this._fatLine = null;

            this.appendElement.remove(this._curve);
            this._curve.mesh.geometry.dispose();
            this._curve.mesh.material.dispose();
            this._curve = null;

            if(this._fillRect !== null) {
                  this.appendElement.remove(this._fillRect);
                  this._fillRect.geometry.dispose();
                  this._fillRect.material.dispose();
                  this._fillRect = null;
            }

            super._onDestroy();
      }
      get width(){
            return this.getGroupPropertyValue("line_width", "width");
      }
      set width(wd){
            if(wd < 1)
                  this.setGroupPropertyValue("line_width", "width", 1);
            else
                  this.setGroupPropertyValue("line_width", "width", wd)
      }
}
WV3DPropertyManager.attach_default_component_infos(Rect3DComponent, {
      "setter": {
            "size": {x: 10, y: 10, z: 10},
      },
      "label": {
            "label_text": "Rect3DComponent",
            "label_line_size": 15,
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "Rect3DComponent",
            "version": "1.0.0",
      },
      "line_width" : {
            "width" : 2
      },
      "rect" : {
            "rect_width" : 30,
            "rect_height" : 30
      },
      "fill" :
            {
                  "fill" : true
            }
});

WV3DPropertyManager.add_property_panel_group_info(Rect3DComponent, {
      label : "Shape Properties",
      template : "vertical",
      children : [
            {
                  owner : "rect",
                  name : "rect_width",
                  type : "number",
                  label : "rect_width",
                  show : true,
                  writable : true,
                  description : "가로 길이"
            },
            {
                  owner : "rect",
                  name : "rect_height",
                  type : "number",
                  label : "rect_height",
                  show : true,
                  writable : true,
                  description : "세로 길이"
            },
            {
                  owner : "fill",
                  name :"fill",
                  type : "checkbox",
                  label : "Fill",
                  show : true,
                  writable : true,
                  description : "채우기"
            }
      ]
});
WV3DPropertyManager.add_property_panel_group_info(Rect3DComponent, {
      label : "Line Properties",
      template : "vertical",
      children : [
            {
                  owner : "line_width",
                  name : "width",
                  type : "number",
                  label : "width",
                  show : true,
                  writable : true,
                  description : "선 두께"
            }
      ]
});
