"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Path3DComponent =
      /*#__PURE__*/
      function (_Line) {
            _inherits(Path3DComponent, _Line);

            function Path3DComponent() {
                  _classCallCheck(this, Path3DComponent);

                  return _possibleConstructorReturn(this, _getPrototypeOf(Path3DComponent).call(this));
            }

            _createClass(Path3DComponent, [{
                  key: "changePointByIndex",
                  value: function changePointByIndex(idx, point) {
                        if (idx < this.points.length && _instanceof(point, THREE.Vector3)) {
                              var pts = [];

                              for (var i = 0; i < this.points.length; i++) {
                                    if (i !== idx) pts.push(this.points[i]);
                              }

                              pts.splice(idx, 0, point);
                              this.points = pts;
                        } else {
                              console.warn("올바른 입력 형식이 아닙니다. (number, THREE.Vector3)");
                        }
                  }
            }, {
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(Path3DComponent.prototype), "_onCreateProperties", this).call(this); // 사이즈 정보 writable을 false로 설정


                        var sizeInfo = WVPropertyManager.getPropertyGroupChildrenByName(Path3DComponent.property_panel_info, "display", "size");
                        sizeInfo.writable = false;
                        WVPropertyManager.removePropertyGroupChildrenByName(Path3DComponent.property_info, "display", "size");
                        this._elementSize = this.getDefaultProperties().setter.size;
                        this._onHelperChangeCalle = this._onHelperChange.bind(this);
                        this._onHelperClickCalle = this._onHelperClick.bind(this);
                        this._onTransformMouseUpCalle = this._onTransformMouseUp.bind(this); //월드 포인트

                        this._points = [];
                        this._pointsOrder = this.getGroupPropertyValue("helper_order", "order"); //controls

                        this._controls = window.wemb.mainPageComponent.threeLayer._transformControls;

                        if (this.isEditorMode) {
                              this._controls.addEventListener("objectChange", this._onHelperChangeCalle, false);

                              this._controls.addEventListener("mouseUp", this._onTransformMouseUpCalle, false);
                        } // 커브


                        this._curve = null;
                        this._curveSegments = 1000; //라인 피킹 세그먼트

                        this._linePickingSegments = 300; // 두꺼운 선

                        this._fatLine = null;
                        this._element = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshPhongMaterial').clone());
                        this._element.castShadow = true;
                        this._element.receiveShadow = true;
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        _get(_getPrototypeOf(Path3DComponent.prototype), "_onCreateElement", this).call(this);

                        var boxSize = 2;
                        this._pointHelperGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        //helper 생성
                        this._initPoints(); //여기서 월드 행렬이 바뀌어 있으니 월드 로컬 점을 만들어준다.
                        //월드는 그대로 쓰고 로컬 포인트를 만들어준다


                        this._makeLocalAndWorldPoints();

                        if (this.isEditorMode) {
                              //에디터라면 만들어진 점들을 가지고 helper를 만들어준다.
                              this._initPointHelper();
                        } //커브를 그린다.
                        //뚱라인을 그린다.
                        //엘리먼트를 추가한다.
                        //그리는건 로컬 좌표를 이용해서 그린다.


                        this._makeCurve();
                  }
            }, {
                  key: "_initPoints",
                  value: function _initPoints() {
                        //this.points는 월드좌표
                        var v_up = new THREE.Vector3(30, 30, 30);
                        var v_down = new THREE.Vector3(-30, -30, -30);
                        var v_pos = new THREE.Vector3(this.position.x, this.position.y, this.position.z);
                        var default_points = [new THREE.Vector3(0, 0, 0).addVectors(v_pos, v_up), new THREE.Vector3(0, 0, 0).addVectors(v_pos, v_down)];
                        var pts = [];

                        if (this._pointsOrder.length === 0) {
                              //새로 생성되는 거니까 월드좌표들을 만들어서 넣는다.
                              for (var i = 0; i < default_points.length; i++) {
                                    pts.push(default_points[i].clone());

                                    this._pointsOrder.push(i);
                              }

                              this.points = pts;
                        } else {
                              for (var _i = 0; _i < this._pointsOrder.length; _i++) {
                                    pts[this._pointsOrder[_i]] = new THREE.Vector3(this.points[this._pointsOrder[_i]].x, this.points[this._pointsOrder[_i]].y, this.points[this._pointsOrder[_i]].z);
                              }

                              this.points = pts;
                        }
                  }
            }, {
                  key: "_makeLocalAndWorldPoints",
                  value: function _makeLocalAndWorldPoints() {
                        this._points = this.points;
                        var pts = [];

                        for (var i = 0; i < this.points.length; i++) {
                              var v = this.points[i].clone();
                              this.appendElement.worldToLocal(v);
                              pts.push(v);
                        }

                        this._localPoints = pts;
                  }
            }, {
                  key: "_initPointHelper",
                  value: function _initPointHelper() {
                        var localHelper, worldHelper; //먼저 직접 에디터에서 조작하는 로컬 포인트 헬퍼

                        for (var i = 0; i < this._pointsOrder.length; i++) {
                              localHelper = this._addPointHelperObject(this._localPoints[this._pointsOrder[i]]);
                              localHelper.name = "local" + this._pointsOrder[i];

                              this._domEvents.addEventListener(localHelper, 'click', this._onHelperClickCalle, false);

                              this.appendElement.add(localHelper);
                        } //패널에 표현되는 월드좌표를 위한 월드 포인트 헬퍼


                        for (var _i2 = 0; _i2 < this._pointsOrder.length; _i2++) {
                              worldHelper = this._addPointHelperObject(this._points[this._pointsOrder[_i2]]);
                              worldHelper.name = "world" + this._pointsOrder[_i2];
                              worldHelper.visible = false;
                              this.appendElement.add(worldHelper);
                        }
                  }
            }, {
                  key: "_addPointHelperObject",
                  value: function _addPointHelperObject(position) {
                        var object = new THREE.Mesh(this._pointHelperGeometry.clone(), new THREE.MeshBasicMaterial({
                              color: 0xffffff
                        }));

                        if (position) {
                              object.position.copy(position);
                        } else {
                              object.position.x = 1 * 80 + 30;
                              object.position.y = 1 * 10 + 30;
                              object.position.z = 1 * 10 + 30;
                        }

                        object.castShadow = true;
                        object.receiveShadow = true;
                        return object;
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        _get(_getPrototypeOf(Path3DComponent.prototype), "_onCommitProperties", this).call(this);

                        if (this._updatePropertiesMap.has("setter.color")) {
                              var cl = this.getGroupPropertyValue("setter", "color");

                              if (cl.indexOf('#') > -1) {
                                    var c = this._hexToRgb(cl);

                                    this._colors = [c.r, c.g, c.b];
                              } else {
                                    this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                              }

                              this._Update();
                        }

                        if (this._updatePropertiesMap.has("line_width")) {
                              if (this.width < 1) this.width = 1;

                              this._Update();
                        }

                        if (this._updatePropertiesMap.has("setter.position") || this._updatePropertiesMap.has("setter.rotation") || this._updatePropertiesMap.has("setter.size")) {
                              this.validateCallLater(this._syncPropertyToHelper);
                        }

                        if (this._updatePropertiesMap.has("setter", "points")) {
                              this._syncInputedPoints();

                              this._Update();
                        }

                        if (this._updatePropertiesMap.has("syncPoint.points")) {
                              if (this.isEditorMode) {
                                    this._syncWorldLocal();
                              }

                              this._Update();
                        }

                        if (this._updatePropertiesMap.has("helper_count.count")) {
                              if (this.isEditorMode) {
                                    if (this.getGroupPropertyValue("helper_count", "count") < 2) {
                                          console.warn("경고 : 라인은 최소 2개 이상의 점을 필요로 합니다.");
                                    } else if (this.getGroupPropertyValue("helper_count", "count") > 50) {
                                          console.warn("경고 : 최대 50개의 점 까지 입력 가능합니다.");
                                    } else {
                                          this._addPointByCount(this.getGroupPropertyValue("helper_count", "count"));

                                          this._lineAdjustment();

                                          this._Update();
                                    }
                              }
                        }
                  }
            }, {
                  key: "_syncInputedPoints",
                  value: function _syncInputedPoints() {
                        //입력된 점이 에티터에 있는 헬퍼보다 많으면 점을 추가하고 그린다
                        this._addPointByCount(this.points.length); //입력된 점의 수에 맞춰서 헬퍼만 조정된 상태.
                        //입력된 점을 기준으로 점을 업데이트 한다.


                        for (var i = 0; i < this._pointsOrder.length; i++) {
                              this._points[i].x = this.points[i].x;
                              this._points[i].y = this.points[i].y;
                              this._points[i].z = this.points[i].z;
                              var v = this.points[i].clone();
                              this.appendElement.worldToLocal(v);
                              this._localPoints[i] = v;
                        } //에디터에서는


                        if (this.isEditorMode) {
                              //업데이트 된 점을 기준으로 헬퍼들의 배치를 다시 한다.
                              for (var _i3 = 0; _i3 < this._pointsOrder.length; _i3++) {
                                    //로컬
                                    var localHelper = this.appendElement.getObjectByName("local" + this._pointsOrder[_i3]);

                                    var v = this._points[_i3].clone();

                                    this.appendElement.worldToLocal(v);

                                    if (localHelper !== undefined) {
                                          localHelper.position.copy(v);
                                    } //월드


                                    var worldHelper = this.appendElement.getObjectByName("world" + this._pointsOrder[_i3]);

                                    if (worldHelper !== undefined) {
                                          worldHelper.position.copy(this._points[_i3]);
                                    }
                              }
                        }

                        this._lineAdjustment();

                        this._Update();
                  }
            }, {
                  key: "_syncPropertyToHelper",
                  value: function _syncPropertyToHelper() {
                        //행렬을 업데이트 시켜주고.
                        this.appendElement.updateMatrixWorld(true); //로컬좌표를 다시 변환하여 월드에 셋팅한다.

                        for (var i = 0; i < this._pointsOrder.length; i++) {
                              //에디터 모드라면 헬퍼들의 속성을 조절해줘야 한다.(월드 로컬 모두)
                              if (this.isEditorMode) {
                                    //점도 계산되어 바뀌어야 한다.
                                    var v = this._localPoints[i].clone();

                                    this.appendElement.localToWorld(v);
                                    var worldHelper = this.appendElement.getObjectByName("world" + this._pointsOrder[i]);
                                    var localHelper = this.appendElement.getObjectByName("local" + this._pointsOrder[i]);

                                    if (worldHelper !== undefined) {
                                          worldHelper.position.copy(v);
                                          this._points[i] = v;
                                    } //월드 헬퍼는 안보이니까 굳이 필요 없다.
                                    //로컬 헬퍼 박스는 스케일 회전을 다시 원상복구 시켜야 한다.


                                    if (localHelper !== undefined) {
                                          var scale = this.appendElement.getWorldScale();
                                          localHelper.rotation.set(0, 0, 0);
                                          localHelper.scale.set(1 / scale.x, 1 / scale.y, 1 / scale.z);
                                    }
                              } else {
                                    //에디터모드가 아니라면 점 싱크 처리만 하면 된다.
                                    this.appendElement.localToWorld(this._localPoints[i]);
                              }
                        }
                  }
            }, {
                  key: "_syncWorldLocal",
                  value: function _syncWorldLocal() {
                        //월드좌표를 보고 로컬을 다시 바꿔준다.
                        this.appendElement.updateMatrixWorld(true);

                        for (var i = 0; i < this._pointsOrder.length; i++) {
                              var v = this._points[i].clone();

                              this.appendElement.worldToLocal(v);
                              var worldHelper = this.appendElement.getObjectByName("world" + this._pointsOrder[i]);
                              var localHelper = this.appendElement.getObjectByName("local" + this._pointsOrder[i]);

                              if (this.isEditorMode) {
                                    if (worldHelper !== undefined) {
                                          worldHelper.position.copy(this.points[i]);
                                    } //월드 헬퍼는 안보이니까 굳이 필요 없다.
                                    //로컬 헬퍼 박스는 스케일 회전을 다시 원상복구 시켜야 한다.


                                    if (localHelper !== undefined) {
                                          localHelper.position.copy(v);
                                    }
                              } //에디터, 뷰어 상관없이 점은 바뀌어야 한다


                              this._localPoints[i] = v;
                        } //중점을 조정한다


                        this._lineAdjustment(); //라인을 다시 그려준다


                        this._Update();
                  }
            }, {
                  key: "_syncLocalWorld",
                  value: function _syncLocalWorld() {
                        //로컬좌표를 보고 월드를 바꿔준다
                        this.appendElement.updateMatrixWorld(true);

                        for (var i = 0; i < this._pointsOrder.length; i++) {
                              var v = this._localPoints[i].clone();

                              this.appendElement.localToWorld(v);
                              var worldHelper = this.appendElement.getObjectByName("world" + this._pointsOrder[i]);

                              if (this.isEditorMode) {
                                    if (worldHelper !== undefined) {
                                          worldHelper.position.copy(v);
                                    }
                              } //에디터, 뷰어 상관없이 점은 바뀌어야 한다


                              this._points[i] = v;
                        }
                  }
            }, {
                  key: "_lineAdjustment",
                  value: function _lineAdjustment() {
                        //월드 점이 움직이던, 로컬 점이 움직이던
                        //라인의 중점과, 로컬의 원점과 싱크를 맞춰줍니다.(평행이동을 해서~)
                        this.appendElement.updateMatrixWorld(true);

                        if (this._curve !== null) {
                              this._curve.points = this._localPoints;

                              var center = this._curve.getPoint(0.5).clone();

                              var move = center.clone();
                              this.appendElement.localToWorld(move);

                              if (this.isEditorMode) {
                                    //월드 포지션 싱크를 맞춰줍니다.
                                    for (var i = 0; i < this._pointsOrder.length; i++) {
                                          var worldHelper = this.appendElement.getObjectByName("world" + this._pointsOrder[i]);

                                          if (worldHelper !== undefined) {
                                                worldHelper.position.copy(this._points[i]);
                                          }
                                    }
                              } //로컬 포지션 싱크만 맞춰줍니다.
                              //월드 포지션은 어차피 원래 설정하고자 하는 좌표가 되어 있기 때문입니다.


                              for (var _i4 = 0; _i4 < this._pointsOrder.length; _i4++) {
                                    var v = this._points[_i4].clone();

                                    this.appendElement.worldToLocal(v);
                                    v.sub(center);

                                    if (this.isEditorMode) {
                                          var localHelper = this.appendElement.getObjectByName("local" + this._pointsOrder[_i4]);

                                          if (localHelper !== undefined) {
                                                localHelper.position.x = v.x;
                                                localHelper.position.y = v.y;
                                                localHelper.position.z = v.z;
                                          }
                                    }

                                    this._localPoints[_i4] = v;
                              }

                              this.appendElement.position.x += center.x;
                              this.appendElement.position.y += center.y;
                              this.appendElement.position.z += center.z; // var pos = {x : this.appendElement.position.x, y : this.appendElement.position.y, z : this.appendElement.position.z}
                              // this.setGroupPropertyValue("setter","position", pos )

                              this._properties.setter.position.x = parseInt(this.appendElement.position.x);
                              this._properties.setter.position.y = parseInt(this.appendElement.position.y);
                              this._properties.setter.position.z = parseInt(this.appendElement.position.z);
                        }
                  }
            }, {
                  key: "_addPoint",
                  value: function _addPoint() {
                        //로컬!
                        //점을 선 위의 점 하나로 잡아서 삽입
                        var dist = 0;
                        var idx = 0;

                        for (var i = 0; i < this._localPoints.length - 1; i++) {
                              var distance = this._localPoints[i].distanceTo(this._localPoints[i + 1]);

                              if (dist < distance) {
                                    dist = distance;
                                    idx = i;
                              }
                        }

                        if (this.isEditorMode) {
                              this._addPointHelper(idx);
                        }
                  }
            }, {
                  key: "_addPointHelper",
                  value: function _addPointHelper(idx) {
                        // 최종 idx, idx+1 사이에 점을 삽입한다.
                        var newPoint = new THREE.Vector3();
                        newPoint.addVectors(this._localPoints[idx], this._localPoints[idx + 1]);
                        newPoint.x *= 0.5;
                        newPoint.y *= 0.5;
                        newPoint.z *= 0.5; //로컬 점을 셋팅한다.

                        this._pointsOrder.splice(idx + 1, 0, this._localPoints.length);

                        this._localPoints.splice(idx + 1, 0, newPoint);

                        var max_idx = this._pointsOrder.indexOf(Math.max.apply(Math, _toConsumableArray(this._pointsOrder))); //새로운 헬퍼를 만든다.


                        var localHelper = this._addPointHelperObject(newPoint);

                        localHelper.name = 'local' + this._pointsOrder[max_idx];

                        this._domEvents.addEventListener(localHelper, 'click', this._onHelperClick, false);

                        this.appendElement.add(localHelper); //월드 헬퍼

                        var v = localHelper.position.clone();
                        this.appendElement.localToWorld(v);

                        var worldHelper = this._addPointHelperObject(v);

                        worldHelper.name = 'world' + this._pointsOrder[max_idx];
                        worldHelper.visible = false;
                        this.appendElement.add(worldHelper);

                        this._points.splice(idx + 1, 0, v);
                  }
            }, {
                  key: "_removePoint",
                  value: function _removePoint() {
                        //로컬 점을 뺀다.
                        var poped_idx = this._pointsOrder.indexOf(Math.max.apply(Math, _toConsumableArray(this._pointsOrder)));

                        if (this.isEditorMode) {
                              var poped = this.appendElement.getObjectByName('local' + this._pointsOrder[poped_idx]);
                              this.appendElement.remove(poped);
                              poped.geometry.dispose();
                              poped.material.dispose();
                              poped = this.appendElement.getObjectByName('world' + this._pointsOrder[poped_idx]);
                              this.appendElement.remove(poped);
                              poped.geometry.dispose();
                              poped.material.dispose();
                        }

                        this._pointsOrder.splice(poped_idx, 1);

                        this._localPoints.splice(poped_idx, 1);

                        this._points.splice(poped_idx, 1);
                  }
            }, {
                  key: "_addPointByCount",
                  value: function _addPointByCount(cnt) {
                        if (cnt > 0) {
                              var len = this.pointCount;

                              if (cnt < len) {
                                    for (i = 0; i < len - cnt; i++) {
                                          this._removePoint();
                                    }
                              } else {
                                    var need_to_add = cnt - len;

                                    for (var i = 0; i < need_to_add; i++) {
                                          this._addPoint();
                                    }
                              }

                              this.setter.pointCount = this.points.length;
                              this.setGroupPropertyValue("helper_count", "count", this.points.length);
                        }
                  }
            }, {
                  key: "_onHelperChange",
                  value: function _onHelperChange(e) {
                        //움직이때마다 로컬 헬퍼 좌표와 월드 헬퍼 좌표를 바꾼다.
                        for (var i = 0; i < this._pointsOrder.length; i++) {
                              var localHelper = this.appendElement.getObjectByName("local" + this._pointsOrder[i]);

                              if (localHelper !== undefined) {
                                    this._localPoints[i] = localHelper.position.clone();
                                    var v = localHelper.position.clone();
                                    this.appendElement.localToWorld(v);
                                    this._points[i] = v;

                                    this._syncLocalWorld();
                              }
                        }

                        this._Update();
                  }
            }, {
                  key: "_onTransformMouseUp",
                  value: function _onTransformMouseUp(e) {
                        var ctrl = window.wemb.mainPageComponent.threeLayer._transformControls;
                        ctrl.detach(e.target);

                        this._lineAdjustment();

                        this._Update();

                        ctrl.space = "local";
                  }
            }, {
                  key: "_onHelperClick",
                  value: function _onHelperClick(e) {
                        e.stopPropagation();
                        e.origDomEvent.stopPropagation();
                        var ctrl = window.wemb.mainPageComponent.threeLayer._transformControls;
                        ctrl.attach(e.target);
                        ctrl.space = "local";
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        _get(_getPrototypeOf(Path3DComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "pointCount",
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "pointCount");
                  },
                  set: function set(cnt) {
                        if (cnt < 2) console.warn("경고 : 라인은 최소 2개 이상의 점을 필요로 합니다.");else if (cnt > 50) {
                              console.warn("경고 : 최대 50개의 점 까지 입력 가능합니다.");
                        } else this.setGroupPropertyValue("helper_count", "count", cnt);
                  }
            }, {
                  key: "width",
                  set: function set(wd) {
                        if (wd < 1) this.setGroupPropertyValue("line_width", "width", 1);else this.setGroupPropertyValue("line_width", "width", wd);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("line_width", "width");
                  }
            }, {
                  key: "points",
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "points");
                  },
                  set: function set(pts) {
                        if (pts.length >= 2) {
                              this._checkUpdateGroupPropertyValue("setter", "points", pts);
                        } else {
                              console.warn("Err : 경고 : PATH LINE은 점 두개 이상을 입력하셔야 합니다.");
                        }
                  }
            }]);

            return Path3DComponent;
      }(Line);

WV3DPropertyManager.attach_default_component_infos(Path3DComponent, {
      "setter": {
            "size": {
                  x: 10,
                  y: 10,
                  z: 10
            },
            "pointCount": 2,
            "points": [],
            "pointCountRange": {
                  min: 0,
                  max: 50
            },
            //"helper_size" : 1
      },
      "connection": {
            "targets": {}
      },
      "label": {
            "label_text": "Path3DComponent",
            "label_line_size": 15,
            "label_background_color": "#3351ED",
            "label_using": "N"
      },
      "syncPoint": {
            "points": [],
            "scale": 1,
            "count": 2
      },
      "info": {
            "componentName": "Path3DComponent",
            "version": "1.0.0"
      },
      "line_width": {
            "width": 2
      },
      "helper_count": {
            "count": 2
      },
      "helper_order": {
            "order": []
      },
      "dash": {
            "dash": false,
            "dashSize": 1,
            "dashScale": 1,
            "gapSize": 1
      }
});
WV3DPropertyManager.add_property_panel_group_info(Path3DComponent, {
      label: "Line Properties",
      template: "vertical",
      children: [{
            owner: "line_width",
            name: "width",
            type: "number",
            label: "Width",
            show: true,
            writable: true,
            description: "선 두께"
      }]
});
WVPropertyManager.add_property_panel_group_info(Path3DComponent, {
      label: "controlPoint",
      template: "points",
      children: [{
            owner: "syncPoint",
            name: "point",
            type: "object",
            label: "Length",
            show: true,
            writable: true,
            description: "point"
      }]
});
WV3DPropertyManager.add_property_panel_group_info(Path3DComponent, {
      label: "Dash Properties",
      template: "vertical",
      children: [{
            owner: "dash",
            name: "dash",
            type: "checkbox",
            label: "Dash",
            show: true,
            writable: true,
            description: "대쉬"
      }, {
            owner: "dash",
            name: "dashSize",
            type: "number",
            label: "DashSize",
            show: true,
            writable: true,
            description: "대쉬 사이즈"
      }, {
            owner: "dash",
            name: "dashScale",
            type: "number",
            label: "DashScale",
            show: true,
            writable: true,
            description: "대쉬 스케일"
      }, {
            owner: "dash",
            name: "gapSize",
            type: "number",
            label: "GapSize",
            show: true,
            writable: true,
            description: "갭 사이즈"
      }]
});
// WV3DPropertyManager.add_property_panel_group_info(Path3DComponent, {
//       label : "Helper Size",
//       template : "vertical",
//       children : [
//             {
//                   owner : "setter",
//                   name : "helper_size",
//                   type : "number",
//                   label : "HelperSize",
//                   show : true,
//                   writable : true,
//                   description : "헬퍼 사이즈"
//             }
//       ]
// })
