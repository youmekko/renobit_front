"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var StreamLine3DComponent =
      /*#__PURE__*/
      function (_Line) {
            _inherits(StreamLine3DComponent, _Line);

            function StreamLine3DComponent() {
                  _classCallCheck(this, StreamLine3DComponent);

                  return _possibleConstructorReturn(this, _getPrototypeOf(StreamLine3DComponent).call(this));
            }

            _createClass(StreamLine3DComponent, [{
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(StreamLine3DComponent.prototype), "_onCreateProperties", this).call(this); // 사이즈 정보 writable을 false로 설정


                        var sizeInfo = WVPropertyManager.getPropertyGroupChildrenByName(StreamLine3DComponent.property_panel_info, "display", "size");
                        sizeInfo.writable = false;
                        WVPropertyManager.removePropertyGroupChildrenByName(StreamLine3DComponent.property_info, "display", "size");
                        this._elementSize = this.getDefaultProperties().setter.size;
                        this._onHelperChangeCalle = this._onHelperChange.bind(this);
                        this._onHelperClickCalle = this._onHelperClick.bind(this);
                        this._onTransformMouseUpCalle = this._onTransformMouseUp.bind(this); //월드 포인트

                        this._points = []; //컨트롤
                        //controls

                        this._controls = window.wemb.mainPageComponent.threeLayer._transformControls;

                        if (this.isEditorMode) {
                              this._controls.addEventListener("objectChange", this._onHelperChangeCalle, false);

                              this._controls.addEventListener("mouseUp", this._onTransformMouseUpCalle, false);
                        } //커브


                        this._curve = null;
                        this._curveSegments = 1000; //두꺼운 선

                        this._fatLine = null; //라인 피킹 세그먼트

                        this._linePickingSegments = 300; //파티클 이미지

                        this._particle = null;
                        this._particleImg = this._setParticleImg();
                        this._particleSegments = this.getGroupPropertyValue("extension", "particle_segments");
                        this._particleSize = this.getGroupPropertyValue("extension", "particle_size");
                        this._particleSpeed = this.getGroupPropertyValue("extension", "particle_speed");
                        this._particleDirection = this.getGroupPropertyValue("extension", "particle_direction"); //clock

                        this.clock = new THREE.Clock();
                        this._time = 1.0;
                        this._element = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshPhongMaterial').clone());
                        this._element.castShadow = true;
                        this._element.receiveShadow = true;
                        this._pointsOrder = [0, 1];
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        _get(_getPrototypeOf(StreamLine3DComponent.prototype), "_onCreateElement", this).call(this);

                        var boxSize = 2;
                        this._pointHelperGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        //helper 생성
                        this._initPoints(); //여기서 월드 행렬이 바뀌어 있으니 월드 로컬 점을 만들어준다.
                        //월드는 그대로 쓰고 로컬 포인트를 만들어준다


                        this._makeLocalAndWorldPoints();

                        if (this.isEditorMode) {
                              //에디터라면 만들어진 점들을 가지고 helper를 만들어준다.
                              this._initPointHelper();
                        } //커브를 그린다.
                        //뚱라인을 그린다.
                        //엘리먼트를 추가한다.
                        //그리는건 로컬 좌표를 이용해서 그린다.


                        this._makeCurve();
                  }
            }, {
                  key: "_initPoints",
                  value: function _initPoints() {
                        //this.points는 월드좌표
                        var v_up = new THREE.Vector3(30, 30, 30);
                        var v_down = new THREE.Vector3(-30, -30, -30);
                        var v_pos = new THREE.Vector3(this.position.x, this.position.y, this.position.z);
                        var default_points = [new THREE.Vector3(0, 0, 0).addVectors(v_pos, v_up), new THREE.Vector3(0, 0, 0).addVectors(v_pos, v_down)];
                        var pts = [];

                        if (this.points.length === 0) {
                              //새로 생성되는 거니까 월드좌표들을 만들어서 넣는다.
                              for (var i = 0; i < default_points.length; i++) {
                                    pts.push(default_points[i].clone());
                              }

                              this.points = pts;
                        } else {
                              for (var _i = 0; _i < this.points.length; _i++) {
                                    pts.push(new THREE.Vector3(this.points[_i].x, this.points[_i].y, this.points[_i].z));
                              }

                              this.points = pts;
                        }
                  }
            }, {
                  key: "_initPointHelper",
                  value: function _initPointHelper() {
                        var localHelper, worldHelper; //먼저 직접 에디터에서 조작하는 로컬 포인트 헬퍼

                        for (var i = 0; i < this._localPoints.length; i++) {
                              localHelper = this._addPointHelperObject(this._localPoints[i]);
                              localHelper.name = "local" + i;

                              this._domEvents.addEventListener(localHelper, 'click', this._onHelperClickCalle, false);

                              this.appendElement.add(localHelper);
                        } //패널에 표현되는 월드좌표를 위한 월드 포인트 헬퍼


                        for (var _i2 = 0; _i2 < this._points.length; _i2++) {
                              worldHelper = this._addPointHelperObject(this._points[_i2]);
                              worldHelper.name = "world" + _i2;
                              worldHelper.visible = false;
                              this.appendElement.add(worldHelper);
                        }
                  }
            }, {
                  key: "_makeLocalAndWorldPoints",
                  value: function _makeLocalAndWorldPoints() {
                        this._points = this.points;
                        var pts = [];

                        for (var i = 0; i < this.points.length; i++) {
                              var v = this.points[i].clone();
                              this.appendElement.worldToLocal(v);
                              pts.push(v);
                        }

                        this._localPoints = pts;
                  }
            }, {
                  key: "_makeFatline",
                  value: function _makeFatline() {
                        _get(_getPrototypeOf(StreamLine3DComponent.prototype), "_makeFatline", this).call(this);

                        this._fatLine.visible = this.getGroupPropertyValue("extension", "line_visible");

                        this._makeParticle();
                  }
            }, {
                  key: "_makeParticle",
                  value: function _makeParticle() {
                        var particleMaterial = new THREE.PointsMaterial({
                              size: this._particleSize,
                              map: this._particleImg,
                              depthTest: true,
                              depthWrite: false,
                              transparent: true,
                              color: new THREE.Color(this._colors[0], this._colors[1], this._colors[2]),
                              blending: THREE.AdditiveBlending
                        });

                        var particle_path = this._curve.getPoints(this._particleSegments);

                        var particleGeometry = new THREE.BufferGeometry();
                        particleGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array((particle_path.length - 1) * 3), 3));

                        for (var i = 0; i < particle_path.length; i++) {
                              particleGeometry.attributes.position.setXYZ(i, particle_path[i].x, particle_path[i].y, particle_path[i].z);
                        }

                        this._particle = new THREE.Points(particleGeometry, particleMaterial);
                        this._particle.dynamic = true;
                        this.appendElement.add(this._particle);
                  }
            }, {
                  key: "_rgbToArray",
                  value: function _rgbToArray(color) {
                        var color_arr = color.substring(4, color.length - 1).replace(/ /g, '').split(',');
                        return [color_arr[0] / 255, color_arr[1] / 255, color_arr[2] / 255];
                  }
            }, {
                  key: "_hexToRgb",
                  value: function _hexToRgb(hex) {
                        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
                        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                        hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                              return r + r + g + g + b + b;
                        });
                        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                        return result ? {
                              r: parseInt(result[1], 16) / 255,
                              g: parseInt(result[2], 16) / 255,
                              b: parseInt(result[3], 16) / 255
                        } : null;
                  }
            }, {
                  key: "_Update",
                  value: function _Update() {
                        _get(_getPrototypeOf(StreamLine3DComponent.prototype), "_Update", this).call(this); //파티클 업데이트


                        var particle_path = this._curve.getPoints(this._particleSegments);

                        this._particle.geometry.removeAttribute('position');

                        this._particle.geometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array((particle_path.length - 1) * 3), 3));

                        for (var i = 0; i < particle_path.length; i++) {
                              this._particle.geometry.attributes.position.setXYZ(i, particle_path[i].x, particle_path[i].y, particle_path[i].z);
                        }

                        this._particle.geometry.attributes.position.needsUpdate = true;
                        this._particle.material.size = this._particleSize;
                        this._particle.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        _get(_getPrototypeOf(StreamLine3DComponent.prototype), "_onCommitProperties", this).call(this);

                        if (this._updatePropertiesMap.has("setter.color")) {
                              var cl = this.getGroupPropertyValue("setter", "color");

                              if (cl.indexOf('#') > -1) {
                                    var c = this._hexToRgb(cl);

                                    this._colors = [c.r, c.g, c.b];
                              } else {
                                    this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                              }

                              this._Update();
                        }

                        if (this._updatePropertiesMap.has("line_width")) {
                              if (this.width < 1) this.width = 1;

                              this._Update();
                        } //이동, 회전, 스케일등을 줌


                        if (this._updatePropertiesMap.has("setter.position") || this._updatePropertiesMap.has("setter.rotation") || this._updatePropertiesMap.has("setter.size")) {
                              this.validateCallLater(this._syncPropertyToHelper);
                        } //패널에서 월드 좌표가 변경됨


                        if (this._updatePropertiesMap.has("syncPoint.points")) {
                              if (this.isEditorMode) this.validateCallLater(this._syncWorldLocal);
                        } //외부에서 코드로 점들을 삽입함


                        if (this._updatePropertiesMap.has("setter", "points")) {
                              this.validateCallLater(this._syncInputedPoints);
                        }

                        if (this._updatePropertiesMap.has("extension")) {
                              this.validateCallLater(this._syncParticleProperty);
                        }
                  }
            }, {
                  key: "_syncParticleProperty",
                  value: function _syncParticleProperty() {
                        this._particleImg = this._setParticleImg();
                        this._particle.material.map = this._particleImg;
                        if (this.getGroupPropertyValue("extension", "particle_segments") < 0) this._particleSegments = 1;else this._particleSegments = this.getGroupPropertyValue("extension", "particle_segments");
                        if (this.getGroupPropertyValue("extension", "particle_size") < 0) this._particleSize = 1;else this._particleSize = this.getGroupPropertyValue("extension", "particle_size");
                        this._particleSpeed = this.getGroupPropertyValue("extension", "particle_speed");
                        this._particleDirection = this.getGroupPropertyValue("extension", "particle_direction");
                        this._element.visible = this.getGroupPropertyValue("extension", "line_visible");
                        this._fatLine.visible = this.getGroupPropertyValue("extension", "line_visible");

                        this._Update();
                  }
            }, {
                  key: "_syncInputedPoints",
                  value: function _syncInputedPoints() {
                        for (var i = 0; i < this._points.length; i++) {
                              this._points[i].x = this.points[i].x;
                              this._points[i].y = this.points[i].y;
                              this._points[i].z = this.points[i].z;
                              var v = this.points[i].clone();
                              this.appendElement.worldToLocal(v);
                              this._localPoints[i] = v;
                        }

                        this._lineAdjustment();

                        this._Update();
                  }
            }, {
                  key: "_syncPropertyToHelper",
                  value: function _syncPropertyToHelper() {
                        //행렬을 업데이트 시켜주고.
                        this.appendElement.updateMatrixWorld(true); //로컬좌표를 다시 변환하여 월드에 셋팅한다.

                        for (var i = 0; i < this._localPoints.length; i++) {
                              //에디터 모드라면 헬퍼들의 속성을 조절해줘야 한다.(월드 로컬 모두)
                              if (this.isEditorMode) {
                                    //점도 계산되어 바뀌어야 한다.
                                    var v = this._localPoints[i].clone();

                                    this.appendElement.localToWorld(v);
                                    var worldHelper = this.appendElement.getObjectByName("world" + i);
                                    var localHelper = this.appendElement.getObjectByName("local" + i);

                                    if (worldHelper !== undefined) {
                                          worldHelper.position.copy(v);
                                          this._points[i] = v;
                                    } //월드 헬퍼는 안보이니까 굳이 필요 없다.
                                    //로컬 헬퍼 박스는 스케일 회전을 다시 원상복구 시켜야 한다.


                                    if (localHelper !== undefined) {
                                          var scale = this.appendElement.getWorldScale();
                                          localHelper.rotation.set(0, 0, 0);
                                          localHelper.scale.set(1 / scale.x, 1 / scale.y, 1 / scale.z);
                                    }
                              } else {
                                    //에디터모드가 아니라면 점 싱크 처리만 하면 된다.
                                    this.appendElement.localToWorld(this._localPoints[i]);
                              }
                        }
                  }
            }, {
                  key: "_syncWorldLocal",
                  value: function _syncWorldLocal() {
                        //월드좌표를 보고 로컬을 다시 바꿔준다.
                        this.appendElement.updateMatrixWorld(true);

                        for (var i = 0; i < this._points.length; i++) {
                              var v = this._points[i].clone();

                              this.appendElement.worldToLocal(v);
                              var worldHelper = this.appendElement.getObjectByName("world" + i);
                              var localHelper = this.appendElement.getObjectByName("local" + i);

                              if (this.isEditorMode) {
                                    if (worldHelper !== undefined) {
                                          worldHelper.position.copy(this.points[i]);
                                    } //월드 헬퍼는 안보이니까 굳이 필요 없다.
                                    //로컬 헬퍼 박스는 스케일 회전을 다시 원상복구 시켜야 한다.


                                    if (localHelper !== undefined) {
                                          localHelper.position.copy(v);
                                    }
                              } //에디터, 뷰어 상관없이 점은 바뀌어야 한다


                              this._localPoints[i] = v;
                        } //중점을 조정한다


                        this._lineAdjustment(); //라인을 다시 그려준다


                        this._Update();
                  }
            }, {
                  key: "_syncLocalWorld",
                  value: function _syncLocalWorld() {
                        //로컬좌표를 보고 월드를 바꿔준다
                        this.appendElement.updateMatrixWorld(true);

                        for (var i = 0; i < this._localPoints.length; i++) {
                              var v = this._localPoints[i].clone();

                              this.appendElement.localToWorld(v);
                              var worldHelper = this.appendElement.getObjectByName("world" + i);

                              if (this.isEditorMode) {
                                    if (worldHelper !== undefined) {
                                          worldHelper.position.copy(v);
                                    }
                              } //에디터, 뷰어 상관없이 점은 바뀌어야 한다


                              this._points[i] = v;
                        }
                  }
            }, {
                  key: "_lineAdjustment",
                  value: function _lineAdjustment() {
                        //월드 점이 움직이던, 로컬 점이 움직이던
                        //라인의 중점과, 로컬의 원점과 싱크를 맞춰줍니다.(평행이동을 해서~)
                        this.appendElement.updateMatrixWorld(true);

                        if (this._curve !== null) {
                              this._curve.points = this._localPoints;

                              var center = this._curve.getPoint(0.5).clone();

                              var move = center.clone();
                              this.appendElement.localToWorld(move);

                              if (this.isEditorMode) {
                                    //월드 포지션 싱크를 맞춰줍니다.
                                    for (var i = 0; i < this._points.length; i++) {
                                          var worldHelper = this.appendElement.getObjectByName("world" + i);

                                          if (worldHelper !== undefined) {
                                                worldHelper.position.copy(this._points[i]);
                                          }
                                    }
                              } //로컬 포지션 싱크만 맞춰줍니다.
                              //월드 포지션은 어차피 원래 설정하고자 하는 좌표가 되어 있기 때문입니다.


                              for (var _i3 = 0; _i3 < this._points.length; _i3++) {
                                    var v = this._points[_i3].clone();

                                    this.appendElement.worldToLocal(v);
                                    v.sub(center);

                                    if (this.isEditorMode) {
                                          var localHelper = this.appendElement.getObjectByName("local" + _i3);

                                          if (localHelper !== undefined) {
                                                localHelper.position.x = v.x;
                                                localHelper.position.y = v.y;
                                                localHelper.position.z = v.z;
                                          }
                                    }

                                    this._localPoints[_i3] = v;
                              }

                              this.appendElement.position.x += center.x;
                              this.appendElement.position.y += center.y;
                              this.appendElement.position.z += center.z;
                              this._properties.setter.position.x = parseInt(this.appendElement.position.x);
                              this._properties.setter.position.y = parseInt(this.appendElement.position.y);
                              this._properties.setter.position.z = parseInt(this.appendElement.position.z); // var pos = {x : this.appendElement.position.x, y : this.appendElement.position.y, z : this.appendElement.position.z}
                              // this.setGroupPropertyValue("setter","position", pos )
                        }
                  }
            }, {
                  key: "_onHelperChange",
                  value: function _onHelperChange(e) {
                        //움직이때마다 로컬 헬퍼 좌표와 월드 헬퍼 좌표를 바꾼다.
                        for (var i = 0; i < this._localPoints.length; i++) {
                              var localHelper = this.appendElement.getObjectByName("local" + i);

                              if (localHelper !== undefined) {
                                    this._localPoints[i] = localHelper.position.clone();
                                    var v = localHelper.position.clone();
                                    this.appendElement.localToWorld(v);
                                    this._points[i] = v;

                                    this._syncLocalWorld();
                              }
                        }

                        this._Update();
                  }
            }, {
                  key: "_onTransformMouseUp",
                  value: function _onTransformMouseUp(e) {
                        var ctrl = window.wemb.mainPageComponent.threeLayer._transformControls;
                        ctrl.detach(e.target);

                        this._lineAdjustment();

                        this._Update();

                        ctrl.space = "local";
                  }
            }, {
                  key: "_onHelperClick",
                  value: function _onHelperClick(e) {
                        e.stopPropagation();
                        e.origDomEvent.stopPropagation();
                        var ctrl = window.wemb.mainPageComponent.threeLayer._transformControls;
                        ctrl.attach(e.target);
                        ctrl.space = "local";
                  } //rendering loop

            }, {
                  key: "render",
                  value: function render() {
                        var t0 = this.clock.getElapsedTime();
                        this._time = 0.125 * t0 * this._particleSpeed;
                        var p;
                        if (!this._particle) return;
                        var len = this._particle.geometry.attributes.position.array.length;

                        for (var v = 0; v <= len - 3; v += 3) {
                              var timeOffset = this._time;

                              if (timeOffset + v / len > 1) {
                                    this._time = 0;
                                    delete this.clock;
                                    this.clock = new THREE.Clock();
                                    break;
                              }

                              if (this._particleDirection) p = this._curve.getPoint(timeOffset + v / len);else p = this._curve.getPoint(1 - (timeOffset + v / len));

                              this._particle.geometry.attributes.position.setXYZ(v / 3, p.x, p.y, p.z);
                        }

                        this._particle.geometry.attributes.position.needsUpdate = true;
                  } //파티클 이미지

            }, {
                  key: "getImageUrl",
                  value: function getImageUrl(str) {
                        return wemb.configManager.serverUrl + str;
                  }
            }, {
                  key: "_setParticleImg",
                  //particle image load
                  value: function _setParticleImg() {
                        if (this.extension.particle_image && this.extension.particle_image.path) {
                              var upStateImgUrl = this.getImageUrl(this.extension.particle_image.path);
                              return new THREE.TextureLoader().load(upStateImgUrl);
                        } else {
                              return new THREE.TextureLoader().load("custom/packs/3d_pack/components/images/particle.png");
                        }
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.appendElement.remove(this._particle);

                        this._particle.geometry.dispose();

                        this._particle.material.dispose();

                        this._particle = null;

                        _get(_getPrototypeOf(StreamLine3DComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "changePointByIndex",
                  value: function changePointByIndex(idx, point) {
                        if (idx < this.points.length && idx > 0) {
                              /*this._points = [];
                              for(let i = 0 ;i < this.points.length; i++)
                              {
                                    if(i !== idx)
                                          this._points.push(this.points[i])
                              }
                              this._points.splice(idx,0, point);*/
                              try {
                                    var targetPoint = this.points[idx];
                                    targetPoint.set(point.x, point.y, point.z);
                              } catch (error) {
                                    console.log(error);
                              } //this.points = this._points;

                        } else {
                              console.warn("올바른 입력 형식이 아닙니다. (number, THREE.Vector3)");
                        }
                  }
            }, {
                  key: "usingRenderer",
                  get: function get() {
                        if (this.isEditorMode) return false;else return true;
                  }
            }, {
                  key: "extension",
                  get: function get() {
                        return this.getGroupProperties("extension");
                  }
            }, {
                  key: "pointCount",
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "pointCount");
                  }
            }, {
                  key: "width",
                  set: function set(wd) {
                        if (wd < 1) this.setGroupPropertyValue("line_width", "width", 1);else this.setGroupPropertyValue("line_width", "width", wd);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("line_width", "width");
                  }
            }, {
                  key: "points",
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "points");
                  },
                  set: function set(pts) {
                        if (pts.length === 2) {
                              this._checkUpdateGroupPropertyValue("setter", "points", pts);
                        } else {
                              console.warn("Err : 경고 : 일반 라인은 점 두개를 입력하셔야 합니다.");
                        }
                  }
            }, {
                  key: "particle_size",
                  set: function set(size) {
                        this.setGroupPropertyValue("extension", "particle_size", size);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("extension", "particle_size");
                  }
            }, {
                  key: "particle_speed",
                  set: function set(speed) {
                        this.setGroupPropertyValue("extension", "particle_speed", speed);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("extension", "particle_speed");
                  }
            }, {
                  key: "particle_segments",
                  set: function set(segments) {
                        this.setGroupPropertyValue("extension", "particle_segments", segments);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("extension", "particle_segments");
                  }
            }, {
                  key: "particle_direction",
                  set: function set(direction) {
                        this.setGroupPropertyValue("extension", "particle_direction", direction);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("extension", "particle_direction");
                  }
            }]);

            return StreamLine3DComponent;
      }(Line);

WV3DPropertyManager.attach_default_component_infos(StreamLine3DComponent, {
      "setter": {
            "size": {
                  x: 10,
                  y: 10,
                  z: 10
            },
            "pointCount": 2,
            "points": [],
            //"helper_size": 1
      },
      "connection": {
            "targets": {}
      },
      "label": {
            "label_text": "StreamLine3DComponent",
            "label_using": "N"
      },
      "info": {
            "componentName": "StreamLine3DComponent",
            "version": "1.0.0"
      },
      "extension": {
            "particle_image": null,
            "particle_segments": 10,
            "particle_speed": 1,
            "particle_size": 20,
            "particle_direction": true,
            "line_visible": true
      },
      "line_width": {
            "width": 2
      },
      "syncPoint": {
            "point": "",
            "scale": 1
      },
      "dash": {
            "dash": false,
            "dashSize": 1,
            "dashScale": 1,
            "gapSize": 1
      }
});
WV3DPropertyManager.add_property_panel_group_info(StreamLine3DComponent, {
      label: "Particle Properties",
      template: "vertical",
      children: [{
            owner: "extension",
            name: "particle_segments",
            type: "number",
            label: "Segments",
            show: true,
            writable: true,
            description: "파티클 개수"
      }, {
            owner: "extension",
            name: "particle_speed",
            type: "number",
            label: "Speed",
            show: true,
            writable: true,
            description: "파티클 속도"
      }, {
            owner: "extension",
            name: "particle_size",
            type: "number",
            label: "Size",
            show: true,
            writable: true,
            description: "파티클 크기"
      }, {
            owner: "extension",
            name: "particle_direction",
            type: "checkbox",
            label: "Direction",
            show: true,
            writable: true,
            description: "파티클 진행 방향"
      }]
});
WVPropertyManager.add_property_panel_group_info(StreamLine3DComponent, {
      label: "Particle Resource",
      template: "resource",
      children: [{
            owner: "extension",
            name: "particle_image",
            type: "resource",
            label: "Image",
            resource_options: {
                  minLength: 1,
                  maxLenght: 1,
                  type: "image"
            },
            show: true,
            writable: true,
            description: "이미지 리소스 선택"
      }]
});
WV3DPropertyManager.add_property_panel_group_info(StreamLine3DComponent, {
      label: "Line Properties",
      template: "vertical",
      children: [{
            owner: "line_width",
            name: "width",
            type: "number",
            label: "Width",
            show: true,
            writable: true,
            description: "선 두께"
      }, {
            owner: "extension",
            name: "line_visible",
            type: "checkbox",
            label: "Line Visible",
            show: true,
            writable: true,
            description: "선 보이게/안보이게"
      }]
});
WVPropertyManager.add_property_panel_group_info(StreamLine3DComponent, {
      label: "controlPoint",
      template: "points",
      children: [{
            owner: "syncPoint",
            name: "point",
            type: "object",
            label: "Length",
            show: true,
            writable: true,
            description: "point"
      }]
});
WV3DPropertyManager.add_property_panel_group_info(StreamLine3DComponent, {
      label: "Dash Properties",
      template: "vertical",
      children: [{
            owner: "dash",
            name: "dash",
            type: "checkbox",
            label: "Dash",
            show: true,
            writable: true,
            description: "대쉬"
      }, {
            owner: "dash",
            name: "dashSize",
            type: "number",
            label: "DashSize",
            show: true,
            writable: true,
            description: "대쉬 사이즈"
      }, {
            owner: "dash",
            name: "dashScale",
            type: "number",
            label: "DashScale",
            show: true,
            writable: true,
            description: "대쉬 스케일"
      }, {
            owner: "dash",
            name: "gapSize",
            type: "number",
            label: "GapSize",
            show: true,
            writable: true,
            description: "갭 사이즈"
      }]
});
// WV3DPropertyManager.add_property_panel_group_info(StreamLine3DComponent, {
//       label: "Helper Size",
//       template: "vertical",
//       children: [{
//             owner: "setter",
//             name: "helper_size",
//             type: "number",
//             label: "HelperSize",
//             show: true,
//             writable: true,
//             description: "헬퍼 사이즈"
//       }]
// });
