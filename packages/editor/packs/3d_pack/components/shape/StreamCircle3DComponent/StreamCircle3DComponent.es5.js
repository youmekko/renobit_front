"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var StreamCircle3DComponent = function (_NWV3DComponent) {
      _inherits(StreamCircle3DComponent, _NWV3DComponent);

      function StreamCircle3DComponent() {
            _classCallCheck(this, StreamCircle3DComponent);

            return _possibleConstructorReturn(this, (StreamCircle3DComponent.__proto__ || Object.getPrototypeOf(StreamCircle3DComponent)).call(this));
      }

      _createClass(StreamCircle3DComponent, [{
            key: "_onCreateProperties",
            value: function _onCreateProperties() {
                  _get(StreamCircle3DComponent.prototype.__proto__ || Object.getPrototypeOf(StreamCircle3DComponent.prototype), "_onCreateProperties", this).call(this);
                  this._elementSize = this.getDefaultProperties().setter.size;

                  // 사이즈 정보 writable을 false로 설정
                  // let sizeInfo = WVPropertyManager.getPropertyGroupChildrenByName(Circle3DComponent.property_panel_info, "display", "size");
                  // sizeInfo.writable = false;
                  // WVPropertyManager.removePropertyGroupChildrenByName(Circle3DComponent.property_info, "display", "size");

                  //color
                  var cl = this.getGroupPropertyValue("setter", "color");
                  if (cl.indexOf('#') > -1) {
                        var c = this._hexToRgb(cl);
                        this._colors = [c.r, c.g, c.b];
                  } else {
                        this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                  }

                  //width
                  this._width = this.getGroupPropertyValue("line_width", "width");

                  // curve spline
                  //curve
                  this._curve = null;
                  this._curveSegments = 100;
                  this._xRadius = this.getGroupPropertyValue("radius", "xRadius");
                  this._yRadius = this.getGroupPropertyValue("radius", "yRadius");

                  this._curveFillSegments = 128;
                  this._curveFill = null;

                  // line picking segments
                  this._linePickingSegments = 300;

                  // fatline mesh;
                  this._fatLine = null;

                  //particles
                  this._particle = null;
                  this._particleImg = this._setParticleImg();
                  this._particleSegments = this.getGroupPropertyValue("extension", "particle_segments");
                  this._particleSize = this.getGroupPropertyValue("extension", "particle_size");
                  this._particleSpeed = this.getGroupPropertyValue("extension", "particle_speed");
                  this._particleDirection = this.getGroupPropertyValue("extension", "particle_direction");

                  //clock
                  this.clock = new THREE.Clock();
                  this._time = 1.0;
            }
      }, {
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  //point helper position;
                  var boxSize = 2;
                  this._pointHelperGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
                  this._makeCurve();
            }
      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {
                  _get(StreamCircle3DComponent.prototype.__proto__ || Object.getPrototypeOf(StreamCircle3DComponent.prototype), "_onCommitProperties", this).call(this);

                  if (this._updatePropertiesMap.has("setter.color")) {
                        var cl = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                        this._colors[0] = cl[0];
                        this._colors[1] = cl[1];
                        this._colors[2] = cl[2];
                        this._Update();
                  }
                  if (this._updatePropertiesMap.has("line_width")) {
                        this._width = this.getGroupPropertyValue("line_width", "width");
                        this._Update();
                  }
                  if (this._updatePropertiesMap.has("extension")) {
                        this._particleImg = this._setParticleImg();
                        this._particle.material.map = this._particleImg;
                        if (this.getGroupPropertyValue("extension", "particle_segments") < 0) this._particleSegments = 1;else this._particleSegments = this.getGroupPropertyValue("extension", "particle_segments");

                        if (this.getGroupPropertyValue("extension", "particle_size") < 0) this._particleSize = 1;else this._particleSize = this.getGroupPropertyValue("extension", "particle_size");

                        this._particleSpeed = this.getGroupPropertyValue("extension", "particle_speed");
                        this._particleDirection = this.getGroupPropertyValue("extension", "particle_direction");

                        this._element.visible = this.getGroupPropertyValue("extension", "line_visible");
                        this._fatLine.visible = this.getGroupPropertyValue("extension", "line_visible");

                        this._Update();
                  }
                  if (this._updatePropertiesMap.has("radius")) {
                        this._xRadius = this.getGroupPropertyValue("radius", "xRadius");
                        this._yRadius = this.getGroupPropertyValue("radius", "yRadius");
                        this._Update();
                  }
                  if (this._updatePropertiesMap.has("line_visible")) {
                        this._element.visible = this.getGroupPropertyValue("line_visible", "visible");
                        this._fatLine.visible = this.getGroupPropertyValue("line_visible", "visible");
                        this._Update();
                  }
                  if (this._updatePropertiesMap.has("fill.fill")) {
                        if (this.getGroupPropertyValue("fill", "fill")) {
                              this._curveFill.visible = true;
                        } else {
                              this._curveFill.visible = false;
                        }
                        this._Update();
                  }
            }
      }, {
            key: "_makeCurve",
            value: function _makeCurve() {
                  this._curve = new THREE.EllipseCurve(0, 0, this._xRadius, this._yRadius, 0, 2 * Math.PI, false, 0);

                  var curveGeometry = new THREE.BufferGeometry();
                  curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curveSegments * 3), 3));
                  var pts = [];
                  var curve_pts = this._curve.getPoints(this._curveSegments);
                  for (var i = 0; i < curve_pts.length; i++) {
                        pts.push(new THREE.Vector3(curve_pts[i].x, curve_pts[i].y, 0));
                  }
                  curveGeometry.setFromPoints(pts);
                  var curveMaterial = new THREE.LineBasicMaterial({ color: new THREE.Color(this._colors[0], this._colors[1], this._colors[2]), side: THREE.DoubleSide });
                  var curveMesh = new THREE.LineSegments(curveGeometry, curveMaterial); //new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);
                  this._curve.mesh = curveMesh;
                  this._curve.mesh.castShadow = true;
                  this._element = this._curve.mesh;

                  if (this.getGroupPropertyValue("fill", "fill")) {
                        var curveFillGeometry = new THREE.CircleGeometry(1, this._curveFillSegments);
                        var pt = this._curve.getPoints(this._curveFillSegments - 1);
                        for (var i = 0; i < pt.length; i++) {
                              curveFillGeometry.vertices[i] = new THREE.Vector3(pt[i].x, pt[i].y, 0);
                        }
                        this._curveFill = new THREE.Mesh(curveFillGeometry, new THREE.MeshBasicMaterial({ color: new THREE.Color(this._colors[0], this._colors[1], this._colors[2]), side: THREE.DoubleSide }));
                        this.appendElement.add(this._curveFill);
                  }
                  this.appendElement.add(this._element);
                  this._makeFatline();
            }
      }, {
            key: "_makeFatline",
            value: function _makeFatline() {
                  var fatline_points = this._curve.getPoints(this._curveSegments - 1);
                  var fatline_positions = [];
                  var fatline_colors = [];

                  for (var i = 0; i < fatline_points.length; i++) {
                        fatline_positions.push(fatline_points[i].x, fatline_points[i].y, 0);
                        fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
                  }

                  var fatlineGeometry = new THREE.LineGeometry();
                  fatlineGeometry.setPositions(fatline_positions);
                  fatlineGeometry.setColors(fatline_colors);

                  var fatlineMaterial = new THREE.LineMaterial({
                        color: 0xffffff,
                        linewidth: this._width,
                        vertexColors: THREE.VertexColors,
                        dashed: false
                  });
                  fatlineMaterial.resolution.set(window.innerWidth, window.innerHeight);

                  this._fatLine = new THREE.Line2(fatlineGeometry, fatlineMaterial);
                  this._fatLine.computeLineDistances();
                  this._fatLine.scale.set(1, 1, 1);
                  this.appendElement.add(this._fatLine);
                  this._makeParticle();
            }
      }, {
            key: "_makeParticle",
            value: function _makeParticle() {
                  var particleMaterial = new THREE.PointsMaterial({
                        size: this._particleSize,
                        map: this._particleImg,
                        depthTest: true,
                        depthWrite: false,
                        transparent: true,
                        color: new THREE.Color(this._colors[0], this._colors[1], this._colors[2]),
                        blending: THREE.AdditiveBlending
                  });

                  var particle_path = this._curve.getPoints(this._particleSegments);
                  var particleGeometry = new THREE.BufferGeometry();
                  particleGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array((particle_path.length - 1) * 3), 3));

                  for (var i = 0; i < particle_path.length; i++) {
                        particleGeometry.attributes.position.setXYZ(i, particle_path[i].x, particle_path[i].y, 0);
                  }

                  this._particle = new THREE.Points(particleGeometry, particleMaterial);
                  this._particle.dynamic = true;
                  this.appendElement.add(this._particle);
            }
      }, {
            key: "_Update",
            value: function _Update() {
                  if (this._curve !== null) {
                        this._curve.xRadius = this._xRadius;
                        this._curve.yRadius = this._yRadius;
                        this._curve.mesh.geometry.setFromPoints(this._curve.getPoints(this._curveSegments));
                        this._curve.mesh.geometry.attributes.position.needsUpdate = true;
                        this._curve.mesh.geometry.computeBoundingBox();
                        this._curve.mesh.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);

                        var pts = this._curve.getPoints(this._curveSegments - 1);
                        var fatline_positions = [];
                        var fatline_colors = [];
                        for (var i = 0; i < pts.length; i++) {
                              fatline_positions.push(pts[i].x, pts[i].y, 0);
                              fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
                        }
                        this._fatLine.geometry.setPositions(fatline_positions);
                        this._fatLine.geometry.setColors(fatline_colors);

                        this._fatLine.material.linewidth = this._width;

                        //파티클 업데이트
                        var particle_path = this._curve.getPoints(this._particleSegments);

                        this._particle.geometry.removeAttribute('position');
                        this._particle.geometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array((particle_path.length - 1) * 3), 3));

                        for (var i = 0; i < particle_path.length; i++) {
                              this._particle.geometry.attributes.position.setXYZ(i, particle_path[i].x, particle_path[i].y, 0);
                        }
                        this._particle.geometry.attributes.position.needsUpdate = true;

                        this._particle.material.size = this._particleSize;
                        this._particle.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);

                        if (this._curveFill !== null) {
                              var pt = this._curve.getPoints(this._curveFillSegments - 1);

                              for (var i = 0; i < pt.length; i++) {
                                    this._curveFill.geometry.vertices[i] = new THREE.Vector3(pt[i].x, pt[i].y, 0);
                              }
                              this._curveFill.geometry.verticesNeedUpdate = true;
                              this._curveFill.geometry.elementsNeedUpdate = true;

                              this._curveFill.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);
                        }
                  }
            }
      }, {
            key: "_rgbToArray",
            value: function _rgbToArray(color) {
                  var color_arr = color.substring(4, color.length - 1).replace(/ /g, '').split(',');
                  return [color_arr[0] / 255, color_arr[1] / 255, color_arr[2] / 255];
            }
      }, {
            key: "_hexToRgb",
            value: function _hexToRgb(hex) {
                  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
                  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                        return r + r + g + g + b + b;
                  });

                  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                  return result ? {
                        r: parseInt(result[1], 16) / 255,
                        g: parseInt(result[2], 16) / 255,
                        b: parseInt(result[3], 16) / 255
                  } : null;
            }

            //particle image load

      }, {
            key: "getImageUrl",
            value: function getImageUrl(str) {
                  return wemb.configManager.serverUrl + str;
            }
      }, {
            key: "_setParticleImg",
            value: function _setParticleImg() {
                  if (this.extension.particle_image && this.extension.particle_image.path) {
                        var upStateImgUrl = this.getImageUrl(this.extension.particle_image.path);
                        return new THREE.TextureLoader().load(upStateImgUrl);
                  } else {
                        return new THREE.TextureLoader().load("custom/packs/3d_pack/components/images/particle.png");
                  }
            }
      }, {
            key: "render",
            value: function render() {
                  var t0 = this.clock.getElapsedTime();
                  this._time = 0.125 * t0 * this._particleSpeed;
                  var p;

                  var len = this._particle.geometry.attributes.position.array.length;
                  for (var v = 0; v <= len - 3; v += 3) {
                        var timeOffset = this._time;

                        if (timeOffset + v / len > 1) {
                              this._time = 0;
                              delete this.clock;
                              this.clock = new THREE.Clock();
                              break;
                        }
                        if (this._particleDirection) p = this._curve.getPoint(timeOffset + v / len);else p = this._curve.getPoint(1 - (timeOffset + v / len));

                        this._particle.geometry.attributes.position.setXYZ(v / 3, p.x, p.y, 0);
                  }
                  this._particle.geometry.attributes.position.needsUpdate = true;
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  this.appendElement.remove(this._fatLine);
                  this._fatLine.geometry.dispose();
                  this._fatLine.material.dispose();
                  this._fatLine = null;

                  this.appendElement.remove(this._curve);
                  this._curve.mesh.geometry.dispose();
                  this._curve.mesh.material.dispose();
                  this._curve = null;

                  if (this._curveFill !== null) {
                        this.appendElement.remove(this._curveFill);
                        this._curveFill.geometry.dispose();
                        this._curveFill.material.dispose();
                        this._curveFill = null;
                  }

                  this.appendElement.remove(this._particle);
                  this._particle.geometry.dispose();
                  this._particle.material.dispose();
                  this._particle = null;
                  _get(StreamCircle3DComponent.prototype.__proto__ || Object.getPrototypeOf(StreamCircle3DComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "extension",
            get: function get() {
                  return this.getGroupProperties("extension");
            }
      }, {
            key: "usingRenderer",
            get: function get() {
                  if (this.isEditorMode) return false;else return true;
            }
      }, {
            key: "width",
            get: function get() {
                  return this.getGroupPropertyValue("line_width", "width");
            },
            set: function set(wd) {
                  if (wd < 1) this.setGroupPropertyValue("line_width", "width", 1);else this.setGroupPropertyValue("line_width", "width", wd);
            }
      }, {
            key: "particle_size",
            set: function set(size) {
                  this.setGroupPropertyValue("extension", "particle_size", size);
            },
            get: function get() {
                  return this.getGroupPropertyValue("extension", "particle_size");
            }
      }, {
            key: "particle_speed",
            set: function set(speed) {
                  this.setGroupPropertyValue("extension", "particle_speed", speed);
            },
            get: function get() {
                  return this.getGroupPropertyValue("extension", "particle_speed");
            }
      }, {
            key: "particle_segments",
            set: function set(segments) {
                  this.setGroupPropertyValue("extension", "particle_segments", segments);
            },
            get: function get() {
                  return this.getGroupPropertyValue("extension", "particle_segments");
            }
      }, {
            key: "particle_direction",
            set: function set(direction) {
                  this.setGroupPropertyValue("extension", "particle_direction", direction);
            },
            get: function get() {
                  return this.getGroupPropertyValue("extension", "particle_direction");
            }
      }]);

      return StreamCircle3DComponent;
}(NWV3DComponent);

WV3DPropertyManager.attach_default_component_infos(StreamCircle3DComponent, {
      "setter": {
            "size": { x: 10, y: 10, z: 10 }
      },
      "label": {
            "label_text": "StreamCircle3DComponent",
            "label_line_size": 15,
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "StreamCircle3DComponent",
            "version": "1.0.0"
      },
      "line_width": {
            "width": 2
      },
      "radius": {
            "xRadius": 10,
            "yRadius": 10
      },
      "extension": {
            "particle_image": null,
            "particle_segments": 10,
            "particle_speed": 1,
            "particle_size": 20,
            "particle_direction": true,
            "line_visible": true
      },
      "line_visible": {
            "visible": true
      },
      "fill": {
            "fill": true
      }
});
WV3DPropertyManager.add_property_panel_group_info(StreamCircle3DComponent, {
      label: "Shape Properties",
      template: "vertical",
      children: [{
            owner: "radius",
            name: "xRadius",
            type: "number",
            label: "X Radius",
            show: true,
            writable: true,
            description: "X축 길이"
      }, {
            owner: "radius",
            name: "yRadius",
            type: "number",
            label: "Y Radius",
            show: true,
            writable: true,
            description: "Y축 길이"
      }, {
            owner: "fill",
            name: "fill",
            type: "checkbox",
            label: "Fill",
            show: true,
            writable: true,
            description: "채우기"
      }]
});
WV3DPropertyManager.add_property_panel_group_info(StreamCircle3DComponent, {
      label: "Line Properties",
      template: "vertical",
      children: [{
            owner: "line_width",
            name: "width",
            type: "number",
            label: "Width",
            show: true,
            writable: true,
            description: "선 두께"
      }]
});
WV3DPropertyManager.add_property_panel_group_info(StreamCircle3DComponent, {
      label: "Particle Properties",
      template: "vertical",
      children: [{
            owner: "extension",
            name: "particle_segments",
            type: "number",
            label: "Segments",
            show: true,
            writable: true,
            description: "파티클 개수"
      }, {
            owner: "extension",
            name: "particle_speed",
            type: "number",
            label: "Speed",
            show: true,
            writable: true,
            description: "파티클 속도"
      }, {
            owner: "extension",
            name: "particle_size",
            type: "number",
            label: "Size",
            show: true,
            writable: true,
            description: "파티클 크기"
      }, {
            owner: "extension",
            name: "particle_direction",
            type: "checkbox",
            label: "Direction",
            show: true,
            writable: true,
            description: "파티클 진행 방향"
      }]
});
WVPropertyManager.add_property_panel_group_info(StreamCircle3DComponent, {
      label: "파티클 리소스 설정",
      template: "resource",
      children: [{
            owner: "extension",
            name: "particle_image",
            type: "resource",
            label: "Image",
            resource_options: {
                  minLength: 1,
                  maxLenght: 1,
                  type: "image"
            },
            show: true,
            writable: true,
            description: "이미지 리소스 선택"
      }]
});
