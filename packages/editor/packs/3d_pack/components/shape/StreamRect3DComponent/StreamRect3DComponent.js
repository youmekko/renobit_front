class StreamRect3DComponent extends NWV3DComponent {
    constructor(){
        super();
    }

    _onCreateProperties(){
        super._onCreateProperties();
        this._elementSize = this.getDefaultProperties().setter.size;

        //color
        var cl =this.getGroupPropertyValue("setter", "color");
        if(cl.indexOf('#') > -1)
        {
            var c = this._hexToRgb(cl);
            this._colors = [c.r, c.g, c.b]
        }else{
            this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
        }

        //width
        this._width = this.getGroupPropertyValue("line_width", "width");

        //curve
        this._curve = null;
        this._curveSegments = 100;
        this._rectWidth = this.getGroupPropertyValue("rect", "rect_width");
        this._rectHeight = this.getGroupPropertyValue("rect", "rect_height");

        this._fillRect = null;
        this._fatLine = null;

        //particles
        this._particle = null;
        this._particleImg = this._setParticleImg();
        this._particleSegments = this.getGroupPropertyValue("extension","particle_segments");
        this._particleSize = this.getGroupPropertyValue("extension","particle_size");
        this._particleSpeed = this.getGroupPropertyValue("extension","particle_speed");
        this._particleDirection = this.getGroupPropertyValue("extension","particle_direction");

        //clock
        this.clock = new THREE.Clock();
        this._time = 1.0;

        //inner points
        this._innerPoints = [];

    }
    _onCreateElement() {

        var boxSize = 2;

        this._pointHelperGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
        this._makeInnerPoints();
        this._makeCurve();
    }
    _onCommitProperties(){
        super._onCommitProperties();

        if(this._updatePropertiesMap.has("setter.color"))
        {
            var cl = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
            this._colors[0] = cl[0];
            this._colors[1] = cl[1];
            this._colors[2] = cl[2];
            this._Update();

        }
        if(this._updatePropertiesMap.has("line_width"))
        {
            this._width =  this.getGroupPropertyValue("line_width","width");
            this._Update();

        }
        if(this._updatePropertiesMap.has("rect.rect_width") || this._updatePropertiesMap.has("rect.rect_height"))
        {
            this._rectWidth = this.getGroupPropertyValue("rect", "rect_width");
            this._rectHeight = this.getGroupPropertyValue("rect", "rect_height");
            this._Update();

        }
        if(this._updatePropertiesMap.has("setter.position") || this._updatePropertiesMap.has("setter.rotation") || this._updatePropertiesMap.has("setter.size"))
        {
            this._Update();
        }
        if(this._updatePropertiesMap.has("extension")){
                this._particleImg = this._setParticleImg();
                this._particle.material.map = this._particleImg;
                if(this.getGroupPropertyValue("extension","particle_segments") < 0)
                      this._particleSegments = 1;
                else
                      this._particleSegments = this.getGroupPropertyValue("extension","particle_segments");

                if(this.getGroupPropertyValue("extension","particle_size") < 0)
                      this._particleSize = 1;
                else
                      this._particleSize = this.getGroupPropertyValue("extension","particle_size");

                this._particleSpeed = this.getGroupPropertyValue("extension","particle_speed");
                this._particleDirection = this.getGroupPropertyValue("extension","particle_direction");

                this._element.visible = this.getGroupPropertyValue("extension","line_visible");
                this._fatLine.visible = this.getGroupPropertyValue("extension","line_visible");

                this._Update();
          }
        if(this._updatePropertiesMap.has("line_visible"))
        {
            this._element.visible = this.getGroupPropertyValue("line_visible","visible");
            this._fatLine.visible = this.getGroupPropertyValue("line_visible","visible");
            this._Update();

        }
        if(this._updatePropertiesMap.has("fill.fill"))
        {
            if(this.getGroupPropertyValue("fill","fill"))
            {
                this._fillRect.visible = true;
            }else{
                this._fillRect.visible = false;
            }
            this._Update();

        }
    }

    _makeCurve(){
        this._curve = new THREE.CatmullRomCurve3(this._innerPoints);
        this._curve.tension = 0;
        this._curve.curveType = 'catmullrom';

        var curveGeometry = new THREE.BufferGeometry();
        curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curveSegments * 3),3));

        curveGeometry.setFromPoints(this._curve.getPoints(this._curveSegments - 1));
        var curveMaterial = new THREE.LineBasicMaterial({color : new THREE.Color(this._colors[0], this._colors[1], this._colors[2])});
        var curveMesh = new THREE.LineSegments(curveGeometry, curveMaterial);//new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);

        this._curve.mesh = curveMesh;
        this._element = this._curve.mesh;

        if(this.getGroupPropertyValue("fill", "fill"))
        {
            var curveFillGeometry = new THREE.PlaneGeometry(this._rectWidth * 2, this._rectHeight * 2, 1, 1);
            this._fillRect = new THREE.Mesh(curveFillGeometry,
                new THREE.MeshBasicMaterial( { color: new THREE.Color(this._colors[0], this._colors[1], this._colors[2]), side: THREE.DoubleSide} ))
            this.appendElement.add(this._fillRect);
        }

        this.appendElement.add(this._element);
        this._makeFatLine();
    }
    _makeFatLine(){
        var fatline_positions = [];
        var fatline_colors = [];

        for(var i = 0 ; i < this._innerPoints.length ;i++)
        {
            fatline_positions.push(this._innerPoints[i].x, this._innerPoints[i].y, 0);
            fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
        }

        var fatlineGeometry = new THREE.LineGeometry();
        fatlineGeometry.setPositions(fatline_positions);
        fatlineGeometry.setColors(fatline_colors);

        var fatlineMaterial = new THREE.LineMaterial({
            color : 0xffffff,
            linewidth : this._width,
            vertexColors : THREE.VertexColors,
        });
        fatlineMaterial.resolution.set(window.innerWidth, window.innerHeight);
        this._fatLine = new THREE.Line2(fatlineGeometry, fatlineMaterial);

        this._fatLine.computeLineDistances();
        this._fatLine.scale.set(1,1,1);

        this.appendElement.add(this._fatLine);
        this._makeParticle();
    }
    _makeParticle(){
        var particleMaterial = new THREE.PointsMaterial(
            {
                size : this._particleSize,
                map : this._particleImg,
                depthTest : true,
                depthWrite : false,
                transparent : true,
                color : new THREE.Color(this._colors[0], this._colors[1], this._colors[2]),
                blending:THREE.AdditiveBlending
            });


        var particle_path = this._curve.getPoints(this._particleSegments);
        var particleGeometry = new THREE.BufferGeometry();
        particleGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array((particle_path.length - 1) * 3), 3));

        for(var i = 0 ; i < particle_path.length ; i++) {
            particleGeometry.attributes.position.setXYZ(i, particle_path[i].x, particle_path[i].y, 0);        }

        this._particle = new THREE.Points(particleGeometry, particleMaterial);
        this._particle.dynamic = true;
        this.appendElement.add(this._particle);
    }
    _Update(){
          this._makeInnerPoints();
          this._curve.points = this._innerPoints;
          this._element.geometry.setFromPoints(this._curve.getPoints(this._curveSegments));
          this._element.geometry.attributes.position.needsUpdate = true;
          this._element.geometry.computeBoundingBox();
          this._element.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);

          this._fatLine.material.linewidth = this._width;

          var fatline_positions = [];
          var fatline_colors = [];
          for(var i = 0; i < this._innerPoints.length; i++)
          {
                fatline_positions.push(this._innerPoints[i].x, this._innerPoints[i].y, 0);
                fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
          }

          this._fatLine.geometry.dispose();
          var fatlineGeometry = new THREE.LineGeometry();
          fatlineGeometry.setPositions(fatline_positions);
          fatlineGeometry.setColors(fatline_colors);
          this._fatLine.geometry= fatlineGeometry;

          if(this._fillRect !== null)
          {
                this._fillRect.geometry.dispose();
                var rectGeometry = new THREE.PlaneGeometry(this._rectWidth * 2, this._rectHeight * 2, 1, 1);
                this._fillRect.geometry = rectGeometry;
                this._fillRect.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2])

          }
          //파티클 업데이트
          var particle_path = this._curve.getPoints(this._particleSegments);
          this._particle.geometry.removeAttribute('position');
          this._particle.geometry.addAttribute('position', new THREE.BufferAttribute( new Float32Array( (particle_path.length - 1) * 3 ), 3 ));


          for(var i = 0 ; i < particle_path.length; i++)
          {
                this._particle.geometry.attributes.position.setXYZ(i, particle_path[i].x, particle_path[i].y, 0);
          }
          this._particle.geometry.attributes.position.needsUpdate = true;

          this._particle.material.size = this._particleSize;
          this._particle.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);
    }
    _makeInnerPoints(){
        this._innerPoints = [];
        this._innerPoints[0] = new THREE.Vector3(-1 * this._rectWidth, -1 * this._rectHeight, 0);
        this._innerPoints[1] = new THREE.Vector3(-1 * this._rectWidth, this._rectHeight, 0);
        this._innerPoints[2] = new THREE.Vector3(this._rectWidth, this._rectHeight, 0);
        this._innerPoints[3] = new THREE.Vector3(this._rectWidth, -1 * this._rectHeight, 0);
        this._innerPoints[4] = new THREE.Vector3(-1 * this._rectWidth, -1 * this._rectHeight, 0);
    }

    _rgbToArray(color){
        var color_arr = color.substring(4, color.length-1)
            .replace(/ /g, '')
            .split(',');
        return [color_arr[0]/255,color_arr[1]/255,color_arr[2]/255]
    }
    _hexToRgb(hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16) / 255,
            g: parseInt(result[2], 16) / 255,
            b: parseInt(result[3], 16) / 255
        } : null;
    }
    //particle image load
    getImageUrl(str) {
        return wemb.configManager.serverUrl + str;
    }
    get extension() {
        return this.getGroupProperties("extension");
    }
    _setParticleImg(){
        if (this.extension.particle_image && this.extension.particle_image.path) {
            let upStateImgUrl = this.getImageUrl(this.extension.particle_image.path);
            return new THREE.TextureLoader().load(upStateImgUrl);
        }else{
            return new THREE.TextureLoader().load( "custom/packs/3d_pack/components/images/particle.png" );
        }
    }
    get usingRenderer() {
        if(this.isEditorMode)
              return false;
        else
              return true;
    }
    render() {
        var t0 = this.clock.getElapsedTime();
        this._time = 0.125 * t0 * this._particleSpeed;
        var p;

        var len = this._particle.geometry.attributes.position.array.length;
        for( var v = 0; v <= len - 3; v+=3 )
        {
            var timeOffset = this._time;

            if((timeOffset + (v / (len))) > 1)
            {
                this._time = 0;
                delete this.clock;
                this.clock = new THREE.Clock();
                break;
            }
            if(this._particleDirection)
                p = this._curve.getPoint(timeOffset + (v / len));
            else
                p = this._curve.getPoint(1 - (timeOffset + (v / len)));

            this._particle.geometry.attributes.position.setXYZ(v / 3, p.x, p.y, 0);
        }
        this._particle.geometry.attributes.position.needsUpdate = true;
    }
    _onDestroy(){
        this.appendElement.remove(this._fatLine);
        this._fatLine.geometry.dispose();
        this._fatLine.material.dispose();
        this._fatLine = null;

        this.appendElement.remove(this._curve);
        this._curve.mesh.geometry.dispose();
        this._curve.mesh.material.dispose();
        this._curve = null;

        if(this._fillRect !== null)
        {
              this.appendElement.remove(this._curveFill);
              this._fillRect.geometry.dispose();
              this._fillRect.material.dispose();
              this._fillRect = null;
        }

        this.appendElement.remove(this._particle);
        this._particle.geometry.dispose();
        this._particle.material.dispose();
        this._particle = null;

        super._onDestroy();

    }
    get width(){
            return this.getGroupPropertyValue("line_width", "width");
      }
    set width(wd){
            if(wd < 1)
                  this.setGroupPropertyValue("line_width", "width", 1);
            else
                  this.setGroupPropertyValue("line_width", "width", wd)
      }
    set particle_size(size){
            this.setGroupPropertyValue("extension", "particle_size", size);
      }
    get particle_size(){
            return this.getGroupPropertyValue("extension","particle_size")
      }
    set particle_speed(speed){
            this.setGroupPropertyValue("extension", "particle_speed", speed);
      }
    get particle_speed(){
            return this.getGroupPropertyValue("extension", "particle_speed");
      }
    set particle_segments(segments){
            this.setGroupPropertyValue("extension","particle_segments", segments);
      }
    get particle_segments(){
            return this.getGroupPropertyValue("extension", "particle_segments");
      }
    set particle_direction(direction){
            this.setGroupPropertyValue("extension", "particle_direction", direction)
      }
    get particle_direction(){
            return this.getGroupPropertyValue("extension", "particle_direction");
      }
}
WV3DPropertyManager.attach_default_component_infos(StreamRect3DComponent, {
    "setter": {
        "size": {x: 10, y: 10, z: 10},
    },
    "label": {
        "label_text": "StreamRect3DComponent",
        "label_line_size": 15,
        "label_background_color": "#3351ED"
    },

    "info": {
        "componentName": "StreamRect3DComponent",
        "version": "1.0.0",
    },
    "line_width" : {
        "width" : 2
    },
    "rect" : {
        "rect_width" : 30,
        "rect_height" : 30
    },
    "extension" : {
            "particle_image" : null,
            "particle_segments" : 10,
            "particle_speed" : 1,
            "particle_size" : 20,
            "particle_direction" : true,
            "line_visible" : true
      },
    "line_visible" : {
        "visible" : true
    },
    "fill" :
        {
            "fill" : true
        }
});

WV3DPropertyManager.add_property_panel_group_info(StreamRect3DComponent, {
    label : "Shape Properties",
    template : "vertical",
    children : [
        {
            owner : "rect",
            name : "rect_width",
            type : "number",
            label : "Rect Width",
            show : true,
            writable : true,
            description : "가로 길이"
        },
        {
            owner : "rect",
            name : "rect_height",
            type : "number",
            label : "Rect Height",
            show : true,
            writable : true,
            description : "세로 길이"
        },
        {
            owner : "fill",
            name :"fill",
            type : "checkbox",
            label : "Fill",
            show : true,
            writable : true,
            description : "채우기"
        }
    ]
});
WV3DPropertyManager.add_property_panel_group_info(StreamRect3DComponent, {
    label : "Line Properties",
    template : "vertical",
    children : [
        {
            owner : "line_width",
            name : "width",
            type : "number",
            label : "Width",
            show : true,
            writable : true,
            description : "선 두께"
        }
    ]
});
WV3DPropertyManager.add_property_panel_group_info(StreamRect3DComponent, {
    label : "Particle Properties",
    template : "vertical",
    children : [
          {
                owner: "extension",
                name: "particle_segments",
                type: "number",
                label: "Segments",
                show: true,
                writable: true,
                description: "파티클 개수"
          },
          {
                owner : "extension",
                name : "particle_speed",
                type : "number",
                label : "Speed",
                show : true,
                writable : true,
                description : "파티클 속도"
          },
          {
                owner : "extension",
                name : "particle_size",
                type : "number",
                label : "Size",
                show : true,
                writable : true,
                description : "파티클 크기"
          },
          {
                owner : "extension",
                name : "particle_direction",
                type : "checkbox",
                label : "Direction",
                show : true,
                writable : true,
                description : "파티클 진행 방향"
          }
    ]
});
WVPropertyManager.add_property_panel_group_info(StreamRect3DComponent, {
    label: "파티클 리소스 설정",
    template: "resource",
    children: [{
        owner: "extension",
        name: "particle_image",
        type: "resource",
        label: "Image",
        resource_options: {
            minLength: 1,
            maxLenght: 1,
            type: "image"
        },
        show: true,
        writable: true,
        description: "이미지 리소스 선택"
    }
    ]
});
