function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var WV3DTextComponent =
      /*#__PURE__*/
      function (_NWV3DComponent) {
            "use strict";

            _inherits(WV3DTextComponent, _NWV3DComponent);

            function WV3DTextComponent() {
                  _classCallCheck(this, WV3DTextComponent);

                  return _possibleConstructorReturn(this, _getPrototypeOf(WV3DTextComponent).call(this));
            }

            _createClass(WV3DTextComponent, [{
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(WV3DTextComponent.prototype), "_onCreateProperties", this).call(this);

                        this._isResourceComponent = true;
                  }
            }, {
                  key: "_colorLuminance",
                  value: function _colorLuminance(hex, lum) {
                        // validate hex string
                        hex = String(hex).replace(/[^0-9a-f]/gi, '');

                        if (hex.length < 6) {
                              hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
                        }

                        lum = lum || 0; // convert to decimal and change luminosity

                        var rgb = "#",
                              c,
                              i;

                        for (i = 0; i < 3; i++) {
                              c = parseInt(hex.substr(i * 2, 2), 16);
                              c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16);
                              rgb += ("00" + c).substr(c.length);
                        }

                        return rgb;
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this.materials = [new THREE.MeshBasicMaterial({
                              color: 0xffffff,
                              flatShading: false
                        }), // front
                              new THREE.MeshBasicMaterial({
                                    color: this._colorLuminance("ffffff", -0.3)
                              }) // side
                        ];
                        var geometry = new THREE.BufferGeometry();
                        this._element = new THREE.Mesh(geometry, this.materials);
                        this.appendElement.add(this.element);
                  }
            }, {
                  key: "_commitProperties",
                  value: function _commitProperties() {
                        if (this.font != null) {
                              if (this._updatePropertiesMap.has("textInfo")) {
                                    this.validateCallLater(this._inValidateElementSize);
                                    this.validateCallLater(this._onValidateResource);
                              }
                        }

                        _get(_getPrototypeOf(WV3DTextComponent.prototype), "_commitProperties", this).call(this);
                  }
            }, {
                  key: "_validateTextInfo",
                  value: function _validateTextInfo() {
                        var textInfo = this.getGroupProperties("textInfo");
                        var height = 4;
                        var size = textInfo.fontSize;
                        var textGeo = new THREE.TextGeometry(textInfo.text, {
                              font: this.font,
                              size: size,
                              height: height,
                              curveSegments: 4,
                              bevelThickness: 0,
                              bevelSize: 0,
                              bevelEnabled: false
                        });
                        textGeo.computeBoundingBox();
                        textGeo.computeVertexNormals();
                        var triangleAreaHeuristics = 0.1 * (height * size);

                        for (var i = 0; i < textGeo.faces.length; i++) {
                              var face = textGeo.faces[i];

                              if (face.materialIndex == 1) {
                                    for (var j = 0; j < face.vertexNormals.length; j++) {
                                          face.vertexNormals[j].z = 0;
                                          face.vertexNormals[j].normalize();
                                    }

                                    var va = textGeo.vertices[face.a];
                                    var vb = textGeo.vertices[face.b];
                                    var vc = textGeo.vertices[face.c];
                                    var s = THREE.GeometryUtils.triangleArea(va, vb, vc);

                                    if (s > triangleAreaHeuristics) {
                                          for (var j = 0; j < face.vertexNormals.length; j++) {
                                                face.vertexNormals[j].copy(face.normal);
                                          }
                                    }
                              }
                        }

                        var centerOffset = -0.5 * (textGeo.boundingBox.max.x - textGeo.boundingBox.min.x);
                        textGeo = new THREE.BufferGeometry().fromGeometry(textGeo);
                        textGeo.computeBoundingBox();
                        textGeo.translate(centerOffset, 0, 0);
                        size = textGeo.boundingBox.getSize(new THREE.Vector3());

                        this._element.geometry.dispose();

                        this._element.geometry = textGeo;

                        this._validateElementSize(size);
                  }
            }, {
                  key: "_onValidateResource",
                  value: function _onValidateResource() {
                        this._validateTextInfo();

                        this._validateColor();

                        this._validateOutline();

                        this._validateOpacity();
                  }
            }, {
                  key: "startLoadResource",
                  value: function () {
                        var _startLoadResource = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee() {
                                    var path, font;
                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                          while (1) {
                                                switch (_context.prev = _context.next) {
                                                      case 0:
                                                            _context.prev = 0;
                                                            path = "./client/common/assets/WV3DText_font.json";
                                                            font = NLoaderManager.getFontPool(path);

                                                            if (!(font == null)) {
                                                                  _context.next = 8;
                                                                  break;
                                                            }

                                                            _context.next = 6;
                                                            return NLoaderManager.loadFont(path);

                                                      case 6:
                                                            font = _context.sent;
                                                            NLoaderManager.registFont(path, font);

                                                      case 8:
                                                            this.font = font;

                                                            this._onValidateResource();

                                                            _context.next = 15;
                                                            break;

                                                      case 12:
                                                            _context.prev = 12;
                                                            _context.t0 = _context["catch"](0);
                                                            console.log("WV3DTextComponent-error", _context.t0);
                                                            window.Vue.$message({ message:"/client/common/assets/WV3DText_font.json load fail",  type: 'error' });
                                                      case 15:
                                                            _context.prev = 15;
                                                            this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                            return _context.finish(15);

                                                      case 18:
                                                      case "end":
                                                            return _context.stop();
                                                }
                                          }
                                    }, _callee, this, [[0, 12, 15, 18]]);
                              }));

                        function startLoadResource() {
                              return _startLoadResource.apply(this, arguments);
                        }

                        return startLoadResource;
                  }()
            }, {
                  key: "_validateColor",
                  value: function _validateColor() {
                        var value;

                        if (this.color.indexOf("#") == -1) {
                              value = CPUtil.getInstance().parseColor(this.color).hex;
                        } else {
                              value = this.color;
                        }

                        this.materials[0].color.set(value);
                        this.materials[1].color.set(this._colorLuminance(value, -0.3));
                  }
            }]);

            return WV3DTextComponent;
      }(NWV3DComponent);

WV3DPropertyManager.attach_default_component_infos(WV3DTextComponent, {
      "setter": {
            "size": {
                  x: 10,
                  y: 5,
                  z: 5
            }
      },
      "textInfo": {
            "text": "Plane",
            "fontSize": 10
      },
      "label": {
            "label_using": "N"
      },
      "info": {
            "componentName": "WV3DTextComponent",
            "version": "1.0.0"
      }
});
WVPropertyManager.remove_property_panel_group_info_by_label(WV3DTextComponent, "label");
WVPropertyManager.add_property_panel_group_info(WV3DTextComponent, {
      label: "Text Info",
      template: "vertical",
      children: [{
            owner: "textInfo",
            name: "text",
            type: "string",
            label: "Text",
            show: true,
            writable: true,
            description: "text"
      }, {
            owner: "textInfo",
            name: "fontSize",
            type: "number",
            label: "FontSize",
            show: true,
            writable: true,
            description: "fontSize"
      }]
});
