class WV3DTextComponent extends NWV3DComponent {

      constructor() {
            super();
      }

      _onCreateProperties() {
            super._onCreateProperties();
            this._isResourceComponent = true;
      }

      _colorLuminance(hex, lum) {
            // validate hex string
            hex = String(hex).replace(/[^0-9a-f]/gi, '');
            if (hex.length < 6) {
                  hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
            }
            lum = lum || 0;
            // convert to decimal and change luminosity
            var rgb = "#", c, i;
            for (i = 0; i < 3; i++) {
                  c = parseInt(hex.substr(i * 2, 2), 16);
                  c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                  rgb += ("00" + c).substr(c.length);
            }
            return rgb;
      }


      _onCreateElement() {
            this.materials = [
                  new THREE.MeshBasicMaterial({color: 0xffffff, flatShading: false}),        // front
                  new THREE.MeshBasicMaterial({color: this._colorLuminance("ffffff", -0.3)}) // side
            ];
            let geometry = new THREE.BufferGeometry();
            this._element = new THREE.Mesh(geometry, this.materials);
            this.appendElement.add(this.element);
      }


      _commitProperties() {
            if (this.font != null) {
                  if (this._updatePropertiesMap.has("textInfo")) {
                        this.validateCallLater(this._inValidateElementSize);
                        this.validateCallLater(this._onValidateResource);
                  }
            }
            super._commitProperties();
      }


      _validateTextInfo() {

            let textInfo = this.getGroupProperties("textInfo");
            let height = 4;
            let size = textInfo.fontSize;
            let textGeo = new THREE.TextGeometry(textInfo.text, {
                  font: this.font,
                  size: size,
                  height: height,
                  curveSegments: 4,
                  bevelThickness: 0,
                  bevelSize: 0,
                  bevelEnabled: false
            });

            textGeo.computeBoundingBox();
            textGeo.computeVertexNormals();

            var triangleAreaHeuristics = 0.1 * (height * size);
            for (var i = 0; i < textGeo.faces.length; i++) {

                  var face = textGeo.faces[i];
                  if (face.materialIndex == 1) {

                        for (var j = 0; j < face.vertexNormals.length; j++) {
                              face.vertexNormals[j].z = 0;
                              face.vertexNormals[j].normalize();
                        }

                        var va = textGeo.vertices[face.a];
                        var vb = textGeo.vertices[face.b];
                        var vc = textGeo.vertices[face.c];

                        var s = THREE.GeometryUtils.triangleArea(va, vb, vc);

                        if (s > triangleAreaHeuristics) {
                              for (var j = 0; j < face.vertexNormals.length; j++) {
                                    face.vertexNormals[j].copy(face.normal);
                              }
                        }

                  }
            }


            let centerOffset = -0.5 * (textGeo.boundingBox.max.x - textGeo.boundingBox.min.x);
            textGeo = new THREE.BufferGeometry().fromGeometry(textGeo);
            textGeo.computeBoundingBox();
            textGeo.translate(centerOffset, 0, 0);
            size = textGeo.boundingBox.getSize(new THREE.Vector3);
            this._element.geometry.dispose();
            this._element.geometry = textGeo;
            this._validateElementSize(size);

      }


      _onValidateResource() {
            this._validateTextInfo();
            this._validateColor();
            this._validateOutline();
            this._validateOpacity();
      }

      async startLoadResource() {
            try{
                  let path = "./client/common/assets/WV3DText_font.json";
                  let font = NLoaderManager.getFontPool(path);
                  if (font == null) {
                        font = await NLoaderManager.loadFont(path);
                        NLoaderManager.registFont(path, font);
                  }
                  this.font = font;
                  this._onValidateResource();
            }catch(error){
                  console.log("WV3DTextComponent-error", error );
            } finally {
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }



      }

      _validateColor() {
            let value;
            if (this.color.indexOf("#") == -1) {
                  value = CPUtil.getInstance().parseColor(this.color).hex;
            } else {
                  value = this.color;
            }
            this.materials[0].color.set(value);
            this.materials[1].color.set(this._colorLuminance(value, -0.3));
      }

}


WV3DPropertyManager.attach_default_component_infos(WV3DTextComponent, {
      "setter": {
            "size": {x: 10, y: 5, z: 5}
      },
      "textInfo": {
            "text": "Plane",
            "fontSize": 10
      },
      "label": {
            "label_using": "N"
      },
      "info": {
            "componentName": "WV3DTextComponent",
            "version": "1.0.0",
      }
});


WVPropertyManager.remove_property_panel_group_info_by_label(WV3DTextComponent, "label");

WVPropertyManager.add_property_panel_group_info(WV3DTextComponent, {
      label: "Text Info",
      template: "vertical",
      children: [{
            owner: "textInfo",
            name: "text",
            type: "string",
            label: "Text",
            show: true,
            writable: true,
            description: "text"
      }, {
            owner: "textInfo",
            name: "fontSize",
            type: "number",
            label: "FontSize",
            show: true,
            writable: true,
            description: "fontSize"
      }]
});


