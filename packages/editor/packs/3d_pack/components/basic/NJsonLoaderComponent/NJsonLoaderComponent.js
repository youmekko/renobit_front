class NJsonLoaderComponent extends WV3DResourceComponent {

      constructor(){ super(); }

      _onCreateProperties(){
            super._onCreateProperties();
            this.animation = null;
            this._invalidateSelectItem = false;
            if(this.isViewerMode)this.outlineEnable =false;
      }


      _onCommitProperties() {
            super._onCommitProperties();
            if (this._invalidateSelectItem) {
                  this._invalidateSelectItem = false;
                  this.validateCallLater(this._validateResource);
            }
      }


      composeResource(loadedObj) {
            loadedObj.rotation.x = -Math.PI/2;
            super.composeResource(loadedObj);
      }

      async _validateResource() {
            var info = this.selectItem;
            this.removePrevResource();
            // 초기 리로드 로드 후 아이템이 변경된 거면 기본 사이즈 정보 제거
            if (info == null || info == "" ) {
                  this._onCreateElement();
                  this._elementSize = null;
                  this.opacity = 100;
                  this.color = "#ffffff";
                  this.size = this.getDefaultProperties().setter.size;
            }

            if (info) {
                  try{
                        let loadedObj = await this.loadSelectItem(info);
                        this.composeResource(loadedObj);
                        this._onValidateResource();
                  }catch(error){
                        throw error;
                  }
            }
            this.validateCallLater(this._validateSize);
      }

      async startLoadResource() {

            try {
                  if(this.selectItem) {
                        await this._validateResource();
                  }
            } catch (error) {
                  //this._resourceLoaded = false;
                  console.log("startLoadResource-error ", error, this.selectItem.path );
            } finally {
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }

      }

      async setMaterialTexture( material, prefix ) {
            let serverPath = window.wemb.configManager.serverUrl;
            let numChildren = material.length;
            let materialName = material[0].name;
            let child;
            for( let i=0; i<numChildren; i++) {
                  child = material[i];
                  child.skinning = true;
                  child.castShadow =true;
                  child.receiveShadow= true;
                  /* 투명 처리  */
                  if (materialName.includes("_A")) {
                        child.transparent = true;
                  }
                  try{
                        let path  =  prefix + materialName + ".png";
                        let texture;
                        if(NLoaderManager.hasTexture(path)){
                              texture = NLoaderManager.getTexturePool(path);
                        } else {
                              texture = await NLoaderManager.loadTexture( serverPath + path );
                              if(texture)NLoaderManager.registTexturePool(path, texture);
                        }
                        child.map = texture;
                        child.map.needsUpdate=true;
                  }catch(error){
                        console.log("error", error);
                  }

            }

      }


      render() {
            if (this.mixer != undefined) {
                  this.mixer.update(this.clock.getDelta());
            }
      }

      async loadSelectItem(info) {

            try{
                  let loadedObj = NLoaderManager.getCloneLoaderPool(  this.convertServerPath(info.path) );
                  if(loadedObj == null ){
                        loadedObj = await NLoaderManager.loadJson( this.convertServerPath(info.path) );
                        if(loadedObj){
                              if(info.mapPath.substr(-1) != "/"){
                                    info.mapPath += "/";
                              }
                              await this.setMaterialTexture( loadedObj.material, info.mapPath );
                              NLoaderManager.setLoaderPool( this.convertServerPath(info.path), loadedObj );
                        }
                  }
                  return loadedObj;
            }catch( error ){
                  throw error;
            }

      }

      play(){
            this.animation.play();
      }

      stop(){
            this.animation.stop();
      }

      onLoadPage() {
            super.onLoadPage();
            if(this.isViewerMode && this._element.geometry.animations){
                  this.clock = new THREE.Clock();
                  this.mixer = new THREE.AnimationMixer(this._element);
                  this.animation = this.mixer.clipAction(this._element.geometry.animations[0]);
                  this.animation.play();
            }
      }


      createGroupEdge( object ){
            // 변경 정보를 반영하기 위해 updatematrixworld를 실행해 줌.
            object.updateMatrixWorld(true);
            let box = ThreeUtil.getObjectBox(object);
            // 원점을 기준으로 위치를 계산하기 위해 translate를 통해 position값을 offset처리
            let inverse = new THREE.Matrix4().getInverse(object.matrixWorld);
            box = box.applyMatrix4(inverse);
            // 회전은 유지
            let rMatrix =new THREE.Matrix4();
            rMatrix.makeRotationX(-Math.PI/2);
            box = box.applyMatrix4(rMatrix);
            let boxSize= box.getSize(new THREE.Vector3());
            let geometry = new THREE.BoxGeometry(boxSize.x, boxSize.y, boxSize.z);
            let offset = box.getCenter(new THREE.Vector3());
            geometry.translate(offset.x, offset.y, offset.z);
            return this.createEdge(geometry);
      }


      _onDestroy() {
            this._invalidateSelectItem = null;
            if(this.animation!=null){
                  this.stop();
                  this.animation = null;
                  this.mixer = null;
            }
            super._onDestroy();
      }

      set selectItem(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                  this._invalidateSelectItem = true;
            }

      }

      get usingRenderer() {
            return true;
      }

      get selectItem() {
            return this.getGroupPropertyValue("setter", "selectItem");
      }

}


WV3DPropertyManager.attach_default_component_infos(NJsonLoaderComponent, {
      "setter": {
            "size": { x: 10, y: 10, z: 10 },
            "selectItem": "",
            "originSize": false
      },
      "label": {
            "label_text": "NJsonLoaderComponent",
      },

      "info": {
            "componentName": "NJsonLoaderComponent",
            "version": "1.0.0",
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
WV3DPropertyManager.add_property_panel_group_info(NJsonLoaderComponent, {
      label: "Resource",
      template: "resource",
      children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D json",
            options: {
                  type: "json3d"
            }
      }]
});



