"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NJsonLoaderComponent = function (_WV3DResourceComponen) {
      _inherits(NJsonLoaderComponent, _WV3DResourceComponen);

      function NJsonLoaderComponent() {
            _classCallCheck(this, NJsonLoaderComponent);

            return _possibleConstructorReturn(this, (NJsonLoaderComponent.__proto__ || Object.getPrototypeOf(NJsonLoaderComponent)).call(this));
      }

      _createClass(NJsonLoaderComponent, [{
            key: "_onCreateProperties",
            value: function _onCreateProperties() {
                  _get(NJsonLoaderComponent.prototype.__proto__ || Object.getPrototypeOf(NJsonLoaderComponent.prototype), "_onCreateProperties", this).call(this);
                  this.animation = null;
                  this._invalidateSelectItem = false;
                  if(this.isViewerMode)this.outlineEnable =false;
            }
      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {
                  _get(NJsonLoaderComponent.prototype.__proto__ || Object.getPrototypeOf(NJsonLoaderComponent.prototype), "_onCommitProperties", this).call(this);
                  if (this._invalidateSelectItem) {
                        this._invalidateSelectItem = false;
                        this.validateCallLater(this._validateResource);
                  }
            }
      }, {
            key: "composeResource",
            value: function composeResource(loadedObj) {
                  loadedObj.rotation.x = -Math.PI / 2;
                  _get(NJsonLoaderComponent.prototype.__proto__ || Object.getPrototypeOf(NJsonLoaderComponent.prototype), "composeResource", this).call(this, loadedObj);
            }
      }, {
            key: "_validateResource",
            value: function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        var info, loadedObj;
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                info = this.selectItem;

                                                this.removePrevResource();
                                                // 초기 리로드 로드 후 아이템이 변경된 거면 기본 사이즈 정보 제거
                                                if (info == null || info == "") {
                                                      this._onCreateElement();
                                                      this._elementSize = null;
                                                      this.opacity = 100;
                                                      this.color = "#ffffff";
                                                      this.size = this.getDefaultProperties().setter.size;
                                                }

                                                if (!info) {
                                                      _context.next = 15;
                                                      break;
                                                }

                                                _context.prev = 4;
                                                _context.next = 7;
                                                return this.loadSelectItem(info);

                                          case 7:
                                                loadedObj = _context.sent;

                                                this.composeResource(loadedObj);
                                                this._onValidateResource();
                                                _context.next = 15;
                                                break;

                                          case 12:
                                                _context.prev = 12;
                                                _context.t0 = _context["catch"](4);
                                                throw _context.t0;

                                          case 15:
                                                this.validateCallLater(this._validateSize);

                                          case 16:
                                          case "end":
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this, [[4, 12]]);
                  }));

                  function _validateResource() {
                        return _ref.apply(this, arguments);
                  }

                  return _validateResource;
            }()
      }, {
            key: "startLoadResource",
            value: function () {
                  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                              while (1) {
                                    switch (_context2.prev = _context2.next) {
                                          case 0:
                                                _context2.prev = 0;

                                                if (!this.selectItem) {
                                                      _context2.next = 4;
                                                      break;
                                                }

                                                _context2.next = 4;
                                                return this._validateResource();

                                          case 4:
                                                _context2.next = 9;
                                                break;

                                          case 6:
                                                _context2.prev = 6;
                                                _context2.t0 = _context2["catch"](0);

                                                //this._resourceLoaded = false;
                                                console.log("startLoadResource-error ", _context2.t0, this.selectItem.path);

                                          case 9:
                                                _context2.prev = 9;

                                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                return _context2.finish(9);

                                          case 12:
                                          case "end":
                                                return _context2.stop();
                                    }
                              }
                        }, _callee2, this, [[0, 6, 9, 12]]);
                  }));

                  function startLoadResource() {
                        return _ref2.apply(this, arguments);
                  }

                  return startLoadResource;
            }()
      }, {
            key: "setMaterialTexture",
            value: function () {
                  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(material, prefix) {
                        var serverPath, numChildren, materialName, child, i, path, texture;
                        return regeneratorRuntime.wrap(function _callee3$(_context3) {
                              while (1) {
                                    switch (_context3.prev = _context3.next) {
                                          case 0:
                                                serverPath = window.wemb.configManager.serverUrl;
                                                numChildren = material.length;
                                                materialName = material[0].name;
                                                child = void 0;
                                                i = 0;

                                          case 5:
                                                if (!(i < numChildren)) {
                                                      _context3.next = 32;
                                                      break;
                                                }

                                                child = material[i];
                                                child.skinning = true;
                                                child.castShadow = true;
                                                child.receiveShadow = true;
                                                /* 투명 처리  */
                                                if (materialName.includes("_A")) {
                                                      child.transparent = true;
                                                }
                                                _context3.prev = 11;
                                                path = prefix + materialName + ".png";
                                                texture = void 0;

                                                if (!NLoaderManager.hasTexture(path)) {
                                                      _context3.next = 18;
                                                      break;
                                                }

                                                texture = NLoaderManager.getTexturePool(path);
                                                _context3.next = 22;
                                                break;

                                          case 18:
                                                _context3.next = 20;
                                                return NLoaderManager.loadTexture(serverPath + path);

                                          case 20:
                                                texture = _context3.sent;

                                                if (texture) NLoaderManager.registTexturePool(path, texture);

                                          case 22:
                                                child.map = texture;
                                                child.map.needsUpdate = true;
                                                _context3.next = 29;
                                                break;

                                          case 26:
                                                _context3.prev = 26;
                                                _context3.t0 = _context3["catch"](11);

                                                console.log("error", _context3.t0);

                                          case 29:
                                                i++;
                                                _context3.next = 5;
                                                break;

                                          case 32:
                                          case "end":
                                                return _context3.stop();
                                    }
                              }
                        }, _callee3, this, [[11, 26]]);
                  }));

                  function setMaterialTexture(_x, _x2) {
                        return _ref3.apply(this, arguments);
                  }

                  return setMaterialTexture;
            }()
      }, {
            key: "render",
            value: function render() {
                  if (this.mixer != undefined) {
                        this.mixer.update(this.clock.getDelta());
                  }
            }
      }, {
            key: "loadSelectItem",
            value: function () {
                  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(info) {
                        var loadedObj;
                        return regeneratorRuntime.wrap(function _callee4$(_context4) {
                              while (1) {
                                    switch (_context4.prev = _context4.next) {
                                          case 0:
                                                _context4.prev = 0;
                                                loadedObj = NLoaderManager.getCloneLoaderPool( this.convertServerPath(info.path));

                                                if (!(loadedObj == null)) {
                                                      _context4.next = 11;
                                                      break;
                                                }

                                                _context4.next = 5;
                                                return NLoaderManager.loadJson(this.convertServerPath(info.path));

                                          case 5:
                                                loadedObj = _context4.sent;

                                                if (!loadedObj) {
                                                      _context4.next = 11;
                                                      break;
                                                }

                                                if (info.mapPath.substr(-1) != "/") {
                                                      info.mapPath += "/";
                                                }
                                                _context4.next = 10;
                                                return this.setMaterialTexture(loadedObj.material, info.mapPath);

                                          case 10:
                                                NLoaderManager.setLoaderPool( this.convertServerPath(info.path), loadedObj );

                                          case 11:
                                                return _context4.abrupt("return", loadedObj);

                                          case 14:
                                                _context4.prev = 14;
                                                _context4.t0 = _context4["catch"](0);
                                                throw _context4.t0;

                                          case 17:
                                          case "end":
                                                return _context4.stop();
                                    }
                              }
                        }, _callee4, this, [[0, 14]]);
                  }));

                  function loadSelectItem(_x3) {
                        return _ref4.apply(this, arguments);
                  }

                  return loadSelectItem;
            }()
      }, {
            key: "play",
            value: function play() {
                  this.animation.play();
            }
      }, {
            key: "stop",
            value: function stop() {
                  this.animation.stop();
            }
      }, {
            key: "onLoadPage",
            value: function onLoadPage() {
                  _get(NJsonLoaderComponent.prototype.__proto__ || Object.getPrototypeOf(NJsonLoaderComponent.prototype), "onLoadPage", this).call(this);
                  if (this.isViewerMode && this._element.geometry.animations) {
                        this.clock = new THREE.Clock();
                        this.mixer = new THREE.AnimationMixer(this._element);
                        this.animation = this.mixer.clipAction(this._element.geometry.animations[0]);
                        this.animation.play();
                  }
            }
      }, {
            key: "createGroupEdge",
            value: function createGroupEdge(object) {
                  // 변경 정보를 반영하기 위해 updatematrixworld를 실행해 줌.
                  object.updateMatrixWorld(true);
                  var box = ThreeUtil.getObjectBox(object);
                  // 원점을 기준으로 위치를 계산하기 위해 translate를 통해 position값을 offset처리
                  var inverse = new THREE.Matrix4().getInverse(object.matrixWorld);
                  box = box.applyMatrix4(inverse);
                  // 회전은 유지
                  var rMatrix = new THREE.Matrix4();
                  rMatrix.makeRotationX(-Math.PI / 2);
                  box = box.applyMatrix4(rMatrix);
                  var boxSize = box.getSize(new THREE.Vector3());
                  var geometry = new THREE.BoxGeometry(boxSize.x, boxSize.y, boxSize.z);
                  var offset = box.getCenter(new THREE.Vector3());
                  geometry.translate(offset.x, offset.y, offset.z);
                  return this.createEdge(geometry);
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  this._invalidateSelectItem = null;
                  if (this.animation != null) {
                        this.stop();
                        this.animation = null;
                        this.mixer = null;
                  }
                  _get(NJsonLoaderComponent.prototype.__proto__ || Object.getPrototypeOf(NJsonLoaderComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "selectItem",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                        this._invalidateSelectItem = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "selectItem");
            }
      }, {
            key: "usingRenderer",
            get: function get() {
                  return true;
            }
      }]);

      return NJsonLoaderComponent;
}(WV3DResourceComponent);

WV3DPropertyManager.attach_default_component_infos(NJsonLoaderComponent, {
      "setter": {
            "size": { x: 10, y: 10, z: 10 },
            "selectItem": "",
            "originSize": false
      },
      "label": {
            "label_text": "NJsonLoaderComponent"
      },

      "info": {
            "componentName": "NJsonLoaderComponent",
            "version": "1.0.0"
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
WV3DPropertyManager.add_property_panel_group_info(NJsonLoaderComponent, {
      label: "Resource",
      template: "resource",
      children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D json",
            options: {
                  type: "json3d"
            }
      }]
});
