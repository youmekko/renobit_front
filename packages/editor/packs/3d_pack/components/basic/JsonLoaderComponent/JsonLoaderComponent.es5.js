'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var JsonLoaderComponent = function (_WV3DComponent) {
      _inherits(JsonLoaderComponent, _WV3DComponent);

      function JsonLoaderComponent() {
            _classCallCheck(this, JsonLoaderComponent);

            var _this = _possibleConstructorReturn(this, (JsonLoaderComponent.__proto__ || Object.getPrototypeOf(JsonLoaderComponent)).call(this));

            _this._isResourceComponent = true;
            return _this;
      }

      _createClass(JsonLoaderComponent, [{
            key: '_onCreateProperties',
            value: function _onCreateProperties() {
                  this.clock = null;
                  this.mixer = null;
                  this.oMeshObj = "";
                  this.using = null;
                  this._invalidateSelectItem = false;
                  this._invalidOriginSize = false;
                  this.isResourceLoaded = false;
            }
      }, {
            key: '_onCreateElement',
            value: function _onCreateElement() {
                  var template = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

                  this._elementSize = new THREE.Box3().setFromObject(this._element).getSize();
                  //MeshManager.procCreateBoxHelper(this._element);
            }
      }, {
            key: 'unbindDomEvents',
            value: function unbindDomEvents() {
                  wemb.threeElements.domEvents.removeEventListener(this._element, 'mouseover', this._onMouseOver, false);
                  wemb.threeElements.domEvents.removeEventListener(this._element, 'mouseout', this._onMouseOut, false);
            }
      }, {
            key: 'bindDomEvents',
            value: function bindDomEvents() {
                  this.unbindDomEvents();
                  wemb.threeElements.domEvents.addEventListener(this._element, 'mouseover', this._onMouseOver, false);
                  wemb.threeElements.domEvents.addEventListener(this._element, 'mouseout', this._onMouseOut, false);
            }
      }, {
            key: '_onCommitProperties',
            value: function _onCommitProperties() {

                  if (this._invalidateSelectItem) {
                        this._invalidateSelectItem = false;
                        this.validateCallLater(this._validateSelectProperty);
                  }

                  if (this._invalidOriginSize) {
                        this._invalidOriginSize = false;
                        this.validateCallLater(this._validateOriginSizeProperty);
                  }
            }
      }, {
            key: '_validateOriginSizeProperty',
            value: function _validateOriginSizeProperty() {
                  this.size = this._elementSize;
            }
      }, {
            key: '_validateSelectProperty',
            value: function _validateSelectProperty() {
                  var info = this.selectItem;

                  if (info == null) {
                        this.removeChild();
                        return;
                  }

                  if (info) {
                        this.procSelectItem(info);
                  }
            }
      }, {
            key: 'startLoadResource',
            value: function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        var success;
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                _context.prev = 0;

                                                if (!this.selectItem) {
                                                      _context.next = 5;
                                                      break;
                                                }

                                                _context.next = 4;
                                                return this.procSelectItem(this.selectItem);

                                          case 4:
                                                success = _context.sent;

                                          case 5:
                                                _context.next = 10;
                                                break;

                                          case 7:
                                                _context.prev = 7;
                                                _context.t0 = _context['catch'](0);

                                                console.log("error ", _context.t0);

                                          case 10:
                                                _context.prev = 10;

                                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                return _context.finish(10);

                                          case 13:
                                          case 'end':
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this, [[0, 7, 10, 13]]);
                  }));

                  function startLoadResource() {
                        return _ref.apply(this, arguments);
                  }

                  return startLoadResource;
            }()
      }, {
            key: 'procSelectItem',
            value: function procSelectItem(info) {
                  var _this2 = this;

                  this.clock = new THREE.Clock();

                  /* Json 로딩 */
                  var obj = {
                        compName: info.name.split(".zip")[0],
                        jsonPath: info.path,
                        texturePath: info.mapPath,
                        recycleYn: true
                  };

                  return LoaderManager.setLoaderJson(obj).then(function (result) {
                        var oldObject = _this2._element.getObjectByName("3DJsonobject");

                        if (oldObject != undefined) {
                              MeshManager.disposeMesh(oldObject);
                              _this2._element.removeChild(oldObject);
                        }

                        _this2._elementSize = result.SIZE;
                        _this2.oMeshObj = result.MESH;
                        _this2.oMeshObj.name = "3DJsonobject";
                        _this2._element.material.visible = false;
                        _this2._element.add(_this2.oMeshObj);
                        _this2.size = _this2._elementSize;
                        _this2.mixer = new THREE.AnimationMixer(_this2.oMeshObj);
                        _this2.mixer.clipAction(_this2.oMeshObj.geometry.animations[0]).play();

                        if (_this2.isEditorMode) {

                              _this2.mixer.clipAction(_this2.oMeshObj.geometry.animations[0]).stop();
                        } else {

                              _this2.mixer.clipAction(_this2.oMeshObj.geometry.animations[0]).play();
                        }
                  });
            }
      }, {
            key: 'removeChild',
            value: function removeChild() {

                  this._element.material.visible = true;
                  var oldObject = this._element.getObjectByName("3DJsonobject");

                  if (oldObject != undefined) {
                        MeshManager.disposeMesh(oldObject);
                        this._element.remove(oldObject);
                  }

                  this.size = { x: 10, y: 10, z: 10 };
            }
      }, {
            key: '_onMouseOver',
            value: function _onMouseOver(event) {
                  MeshManager.mouseEvent(event, "over");
            }
      }, {
            key: '_onMouseOut',
            value: function _onMouseOut(event) {
                  MeshManager.mouseEvent(event, "out");
            }
      }, {
            key: 'render',
            value: function render() {
                  if (this.mixer != undefined) {
                        this.mixer.update(this.clock.getDelta());
                  }
            }
      }, {
            key: '_onDestroy',
            value: function _onDestroy() {
                  this.clock = null;
                  this.mixer = null;
                  this.using = null;
                  this.oMeshObj = null;
                  this._invalidateSelectItem = null;
                  this._invalidOriginSize = null;
                  //this.unbindDomEvents();
                  //MeshManager.disposeMesh(this._container);
                  _get(JsonLoaderComponent.prototype.__proto__ || Object.getPrototypeOf(JsonLoaderComponent.prototype), '_onDestroy', this).call(this);
            }
      }, {
            key: 'usingRenderer',
            get: function get() {
                  return true;
            }
      }, {
            key: 'originSize',
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "originSize", value)) {
                        this._invalidOriginSize = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "originSize");
            }
      }, {
            key: 'selectItem',
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                        this._invalidateSelectItem = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "selectItem");
            }
      }]);

      return JsonLoaderComponent;
}(WV3DComponent);

WV3DPropertyManager.attach_default_component_infos(JsonLoaderComponent, {
      "setter": {
            "position": { x: 0, y: 0, z: 0 },
            "size": { x: 10, y: 10, z: 10 },
            "selectItem": "",
            "originSize": false
      },
      "label": {
            "label_text": "JsonLoaderComponent"
      },

      "info": {
            "componentName": "JsonLoaderComponent",
            "version": "1.0.0"
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
JsonLoaderComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-3d"
}, {
      template: "label-3d"
}, {
      label: "Original Size",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "originSize",
            type: "checkbox",
            label: "Use",
            show: true,
            writable: true,
            description: "원본 크기 전환"
      }]
}, {
      label: "Resource",
      template: "resource",
      children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D json",
            options: {
                  type: "json3d"
            }
      }]
}];
