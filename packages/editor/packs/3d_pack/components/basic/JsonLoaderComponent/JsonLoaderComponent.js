class JsonLoaderComponent extends WV3DComponent {

    constructor() {
        super();
        this._isResourceComponent =  true;
    }

    _onCreateProperties() {
        this.clock = null;
        this.mixer = null;
        this.oMeshObj = "";
        this.using = null;
        this._invalidateSelectItem = false;
        this._invalidOriginSize = false;
        this.isResourceLoaded = false;
    }

    _onCreateElement(template = null) {
        this._elementSize = new THREE.Box3().setFromObject(this._element).getSize();
        //MeshManager.procCreateBoxHelper(this._element);
    }

    unbindDomEvents() {
        wemb.threeElements.domEvents.removeEventListener(this._element, 'mouseover', this._onMouseOver, false);
        wemb.threeElements.domEvents.removeEventListener(this._element, 'mouseout', this._onMouseOut, false);
    }

    bindDomEvents() {
        this.unbindDomEvents();
        wemb.threeElements.domEvents.addEventListener(this._element, 'mouseover', this._onMouseOver, false);
        wemb.threeElements.domEvents.addEventListener(this._element, 'mouseout', this._onMouseOut, false);
    }

    _onCommitProperties() {

        if (this._invalidateSelectItem) {
            this._invalidateSelectItem = false;
            this.validateCallLater(this._validateSelectProperty);
        }

        if (this._invalidOriginSize) {
            this._invalidOriginSize = false;
            this.validateCallLater(this._validateOriginSizeProperty);
        }
    }

    _validateOriginSizeProperty() {
        this.size = this._elementSize;
    }

    _validateSelectProperty() {
        var info = this.selectItem;

        if (info == null) {
            this.removeChild();
            return;
        }

        if (info) {
            this.procSelectItem(info);
        }
    }

    async startLoadResource(){

        //if(!this.selectItem) return;
        try {
              if(this.selectItem){
                    let success = await this.procSelectItem(this.selectItem);
              }
        }catch(error){
              console.log("error ", error);
        } finally {
              this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
        }

    }

    procSelectItem(info) {

        this.clock = new THREE.Clock();

        /* Json 로딩 */
        var obj = {
            compName: info.name.split(".zip")[0],
            jsonPath: info.path,
            texturePath: info.mapPath,
            recycleYn: true
        };

        return LoaderManager.setLoaderJson(obj).then(result => {
            var oldObject = this._element.getObjectByName("3DJsonobject");

            if (oldObject != undefined) {
                MeshManager.disposeMesh(oldObject);
                this._element.removeChild(oldObject);
            }

            this._elementSize = result.SIZE;
            this.oMeshObj = result.MESH;
            this.oMeshObj.name = "3DJsonobject";
            this._element.material.visible = false;
            this._element.add(this.oMeshObj);
            this.size = this._elementSize;
            this.mixer = new THREE.AnimationMixer(this.oMeshObj);
            this.mixer.clipAction(this.oMeshObj.geometry.animations[0]).play();

            if (this.isEditorMode) {

                this.mixer.clipAction(this.oMeshObj.geometry.animations[0]).stop();

            } else {

                this.mixer.clipAction(this.oMeshObj.geometry.animations[0]).play();

            }
        });

    }


    removeChild() {

        this._element.material.visible = true;
        var oldObject = this._element.getObjectByName("3DJsonobject");

        if (oldObject != undefined) {
            MeshManager.disposeMesh(oldObject);
            this._element.remove(oldObject);
        }

        this.size = { x: 10, y: 10, z: 10 };

    }

    _onMouseOver(event) {
        MeshManager.mouseEvent(event, "over");
    }

    _onMouseOut(event) {
        MeshManager.mouseEvent(event, "out");
    }

    render() {
        if (this.mixer != undefined) {
            this.mixer.update(this.clock.getDelta());
        }
    }

    _onDestroy() {
        this.clock = null;
        this.mixer = null;
        this.using = null;
        this.oMeshObj = null;
        this._invalidateSelectItem = null;
        this._invalidOriginSize = null;
        //this.unbindDomEvents();
        //MeshManager.disposeMesh(this._container);
        super._onDestroy();
    }

    get usingRenderer() {
        return true;
    }

    set originSize(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "originSize", value)) {
            this._invalidOriginSize = true;
        }
    }

    get originSize() {
        return this.getGroupPropertyValue("setter", "originSize");
    }

    set selectItem(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
            this._invalidateSelectItem = true;
        }
    }

    get selectItem() {
        return this.getGroupPropertyValue("setter", "selectItem");
    }

}


WV3DPropertyManager.attach_default_component_infos(JsonLoaderComponent, {
    "setter": {
        "position": { x: 0, y: 0, z: 0 },
        "size": { x: 10, y: 10, z: 10 },
        "selectItem": "",
        "originSize": false
    },
    "label": {
        "label_text": "JsonLoaderComponent",
    },

    "info": {
        "componentName": "JsonLoaderComponent",
        "version": "1.0.0",
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
JsonLoaderComponent.property_panel_info = [{
        template: "primary"
    }, {
        template: "pos-size-3d"
    }, {
        template: "label-3d"
    },
    {
        label: "Original Size",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "originSize",
            type: "checkbox",
            label: "Use",
            show: true,
            writable: true,
            description: "원본 크기 전환"
        }]
    },
    {
        label: "Resource",
        template: "resource",
        children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D json",
            options: {
                type: "json3d"
            }
        }]
    }
];
