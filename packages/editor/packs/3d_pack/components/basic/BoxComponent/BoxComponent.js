
class BoxComponent extends NWV3DComponent {

      constructor() {
            super();
      }

      _onCreateElement() {
            this._element = new THREE.Mesh(new THREE.BoxGeometry(10, 10, 10), MeshManager.getMaterial('MeshPhongMaterial').clone());
            this._element.castShadow = true;
            this._element.receiveShadow = true;
            
            this._element.geometry.computeBoundingBox();
            const v = new THREE.Vector3();
            this._element.geometry.boundingBox.getSize(v);
            this._validateElementSize(v);
            this.appendElement.add(this._element);
      }
      
      _onCreateProperties(){
            super._onCreateProperties();
            this._elementSize = null;
      }



}


WV3DPropertyManager.attach_default_component_infos(BoxComponent, {
      "setter": {
            "size": {x: 1, y: 1, z: 1}
      },
      "label": {
            "label_text": "BoxComponent",
      },

      "info": {
            "componentName": "BoxComponent",
            "version": "1.0.0",
      }
});

