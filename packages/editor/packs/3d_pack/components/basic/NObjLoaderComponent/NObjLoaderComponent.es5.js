"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function set(target, property, value, receiver) { if (typeof Reflect !== "undefined" && Reflect.set) { set = Reflect.set; } else { set = function set(target, property, value, receiver) { var base = _superPropBase(target, property); var desc; if (base) { desc = Object.getOwnPropertyDescriptor(base, property); if (desc.set) { desc.set.call(receiver, value); return true; } else if (!desc.writable) { return false; } } desc = Object.getOwnPropertyDescriptor(receiver, property); if (desc) { if (!desc.writable) { return false; } desc.value = value; Object.defineProperty(receiver, property, desc); } else { _defineProperty(receiver, property, value); } return true; }; } return set(target, property, value, receiver); }

function _set(target, property, value, receiver, isStrict) { var s = set(target, property, value, receiver || target); if (!s && isStrict) { throw new Error('failed to set property'); } return value; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var NObjLoaderComponent = /*#__PURE__*/function (_WV3DResourceComponen) {
      _inherits(NObjLoaderComponent, _WV3DResourceComponen);

      var _super = _createSuper(NObjLoaderComponent);

      function NObjLoaderComponent() {
            _classCallCheck(this, NObjLoaderComponent);

            return _super.call(this);
      }

      _createClass(NObjLoaderComponent, [{
            key: "_onCreateProperties",
            value: function _onCreateProperties() {
                  _get(_getPrototypeOf(NObjLoaderComponent.prototype), "_onCreateProperties", this).call(this);

                  this._invalidateSelectItem = false;
            }
      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {
                  _get(_getPrototypeOf(NObjLoaderComponent.prototype), "_onCommitProperties", this).call(this);

                  if (this._invalidateSelectItem) {
                        this._invalidateSelectItem = false;
                        this.validateCallLater(this._validateResource);
                  }
            }
      }, {
            key: "_validateResource",
            value: function () {
                  var _validateResource2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        var convertServerPath, info, loadedObj;
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                convertServerPath = function _convertServerPath(str) {
                                                      if (str.indexOf("http") <= -1) {
                                                            return wemb.configManager.serverUrl + str;
                                                      }
                                                };

                                                info = this.selectItem;
                                                this.removePrevResource(); // 초기 리로드 로드 후 아이템이 변경된 거면 기본 사이즈 정보 제거

                                                if (info == null || info == "") {
                                                      this._onCreateElement();

                                                      this._elementSize = null;
                                                      this.opacity = 100;
                                                      this.color = "#ffffff";
                                                      this.size = this.getDefaultProperties().setter.size;
                                                }

                                                if (!info) {
                                                      _context.next = 25;
                                                      break;
                                                }

                                                _context.prev = 5;

                                                if (info.mapPath.substr(-1) != "/") {
                                                      info.mapPath += "/";
                                                }

                                                loadedObj = null;

                                                if (!info.path.includes('.obj')) {
                                                      _context.next = 14;
                                                      break;
                                                }

                                                _context.next = 11;
                                                return NLoaderManager.composeResource(info, true);

                                          case 11:
                                                loadedObj = _context.sent;
                                                _context.next = 18;
                                                break;

                                          case 14:
                                                if (!info.path.includes('.gltf')) {
                                                      _context.next = 18;
                                                      break;
                                                }

                                                _context.next = 17;
                                                return NLoaderManager.loadGLTF(info, true);

                                          case 17:
                                                loadedObj = _context.sent;

                                          case 18:
                                                this.composeResource(loadedObj);

                                                this._onValidateResource();

                                                _context.next = 25;
                                                break;

                                          case 22:
                                                _context.prev = 22;
                                                _context.t0 = _context["catch"](5);
                                                throw _context.t0;

                                          case 25:
                                          case "end":
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this, [[5, 22]]);
                  }));

                  function _validateResource() {
                        return _validateResource2.apply(this, arguments);
                  }

                  return _validateResource;
            }()
      }, {
            key: "startLoadResource",
            value: function () {
                  var _startLoadResource = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                              while (1) {
                                    switch (_context2.prev = _context2.next) {
                                          case 0:
                                                _context2.prev = 0;

                                                if (!this.selectItem) {
                                                      _context2.next = 4;
                                                      break;
                                                }

                                                _context2.next = 4;
                                                return this._validateResource();

                                          case 4:
                                                _context2.next = 9;
                                                break;

                                          case 6:
                                                _context2.prev = 6;
                                                _context2.t0 = _context2["catch"](0);
                                                console.log("startLoadResource-error ", _context2.t0);

                                          case 9:
                                                _context2.prev = 9;
                                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                return _context2.finish(9);

                                          case 12:
                                          case "end":
                                                return _context2.stop();
                                    }
                              }
                        }, _callee2, this, [[0, 6, 9, 12]]);
                  }));

                  function startLoadResource() {
                        return _startLoadResource.apply(this, arguments);
                  }

                  return startLoadResource;
            }()
            /*
            async loadSelectItem(info) {
                 try{
                       let loadedObj = NLoaderManager.getCloneLoaderPool( info.name );
                       if(loadedObj == null ){
                             loadedObj = await NLoaderManager.loadObj( this.convertServerPath(info.path) );
                             if(loadedObj){
                                   await this.setChildMeshTexture( loadedObj, info.mapPath + "/" );
                                   NLoaderManager.setLoaderPool( info.name, loadedObj );
                             } else {
                                   throw new Error("올바른 파일 형식이 아닙니다.");
                             }
                       }
                       return loadedObj;
                 }catch( error ){
                       throw error;
                 }
            }*/

      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  this._invalidateSelectItem = null;

                  _get(_getPrototypeOf(NObjLoaderComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "selected",
            set: function set(bValue) {
                  _set(_getPrototypeOf(NObjLoaderComponent.prototype), "selected", bValue, this, true);

                  this.toggleSelectedOutline(bValue);
            },
            get: function get() {
                  return _get(_getPrototypeOf(NObjLoaderComponent.prototype), "selected", this);
            }
      }, {
            key: "selectItem",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                        this._invalidateSelectItem = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "selectItem");
            }
      }]);

      return NObjLoaderComponent;
}(WV3DResourceComponent);

WV3DPropertyManager.attach_default_component_infos(NObjLoaderComponent, {
      "setter": {
            "size": {
                  x: 10,
                  y: 10,
                  z: 10
            },
            "selectItem": ""
      },
      "label": {
            "label_text": "NObjLoaderComponent"
      },
      "info": {
            "componentName": "NObjLoaderComponent",
            "version": "1.0.0"
      }
}); // 프로퍼티 패널에서 사용할 정보 입니다.

WV3DPropertyManager.add_property_panel_group_info(NObjLoaderComponent, {
      label: "Resource",
      template: "resource",
      children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D Object",
            options: {
                  type: "obj3d"
            }
      }]
});
