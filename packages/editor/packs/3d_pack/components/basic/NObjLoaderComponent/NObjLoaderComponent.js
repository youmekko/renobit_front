class NObjLoaderComponent extends WV3DResourceComponent {

      constructor() {super();}

      _onCreateProperties() {
            super._onCreateProperties();
            this._invalidateSelectItem = false;
      }


      _onCommitProperties() {
            super._onCommitProperties();
            if (this._invalidateSelectItem) {
                  this._invalidateSelectItem = false;
                  this.validateCallLater(this._validateResource);
            }
      }
      set selected(bValue) {
            super.selected = bValue;
            this.toggleSelectedOutline(bValue);
      }

      get selected() {
            return super.selected;
      }

      async _validateResource() {
            function convertServerPath( str ){
                  if(str.indexOf("http")<= -1){
                        return wemb.configManager.serverUrl  + str;
                  }
            }

            let info = this.selectItem;
            this.removePrevResource();
            // 초기 리로드 로드 후 아이템이 변경된 거면 기본 사이즈 정보 제거
            if (info == null || info == "" ) {
                  this._onCreateElement();
                  this._elementSize = null;
                  this.opacity = 100;
                  this.color = "#ffffff";
                  this.size = this.getDefaultProperties().setter.size;
            }

            if (info) {
                  try{
                        if(info.mapPath.substr(-1) != "/"){
                              info.mapPath += "/";
                        }
                        let loadedObj = null;
                        if(info.path.includes('.obj'))
                              loadedObj = await NLoaderManager.composeResource(info, true);
                        else if(info.path.includes('.gltf'))
                              loadedObj = await NLoaderManager.loadGLTF(info, true);
                        this.composeResource(loadedObj);
                        this._onValidateResource();
                  }catch(error){
                        throw error;
                  }
            }
      }



      async startLoadResource() {
            try {
                  if(this.selectItem) {
                        await this._validateResource();
                  }
            } catch (error) {
                  console.log("startLoadResource-error ", error );
            } finally {
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }

      }


       /*
      async loadSelectItem(info) {
            try{
                  let loadedObj = NLoaderManager.getCloneLoaderPool( info.name );
                  if(loadedObj == null ){
                        loadedObj = await NLoaderManager.loadObj( this.convertServerPath(info.path) );
                        if(loadedObj){
                              await this.setChildMeshTexture( loadedObj, info.mapPath + "/" );
                              NLoaderManager.setLoaderPool( info.name, loadedObj );
                        } else {
                              throw new Error("올바른 파일 형식이 아닙니다.");
                        }
                  }
                  return loadedObj;
            }catch( error ){
                  throw error;
            }

      }*/

      _onDestroy() {
            this._invalidateSelectItem = null;
            super._onDestroy();
      }


      set selectItem(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                  this._invalidateSelectItem = true;
            }

      }

      get selectItem() {
            return this.getGroupPropertyValue("setter", "selectItem");
      }
}



WV3DPropertyManager.attach_default_component_infos(NObjLoaderComponent, {
    "setter": {
        "size": { x: 10, y: 10, z: 10 },
        "selectItem": ""
    },
    "label": {
        "label_text": "NObjLoaderComponent",
    },

    "info": {
        "componentName": "NObjLoaderComponent",
        "version": "1.0.0",
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
WV3DPropertyManager.add_property_panel_group_info(NObjLoaderComponent, {
      label: "Resource",
      template: "resource",
      children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D Object",
            options: {
                  type: "obj3d"
            }
      }]
});
