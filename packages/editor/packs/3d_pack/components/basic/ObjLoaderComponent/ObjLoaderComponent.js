class ObjLoaderComponent extends WV3DComponent {

      constructor() {
            super();
            this._isResourceComponent =  true;
            this._elementdblclick = this._onClick.bind(this);
      }

      _onCreateProperties() {
            this.oMeshObj = "";
            this._invalidateSelectItem = false;
            this._invalidOriginSize = false;
            this.isResourceLoaded = false;
      }

      _onCreateElement() {
            this._element.material.wireframe = true;
            if(this.isViewerMode){
                  this._element.material.visible = false;
            }
            this._elementSize = new THREE.Box3().setFromObject(this._element).getSize();
      }

      unbindDomEvents() {
            this._domEvents.removeEventListener(this._element, 'mouseover', this._onMouseOver, false);
            this._domEvents.removeEventListener(this._element, 'mouseout', this._onMouseOut, false);
      }

      bindDomEvents() {
            this.unbindDomEvents();
            this._domEvents.addEventListener(this._element, 'mouseover', this._onMouseOver, false);
            this._domEvents.addEventListener(this._element, 'mouseout', this._onMouseOut, false);
      }

      _onCommitProperties() {

            if (this._invalidateSelectItem) {
                  this._invalidateSelectItem = false;
                  this.validateCallLater(this._validateSelectProperty);
            }

            if (this._invalidOriginSize) {
                  this._invalidOriginSize = false;
                  this.validateCallLater(this._validateOriginSizeProperty);
            }

      }

      _validateOriginSizeProperty() {
            this.size = this._elementSize;
      }

      _validateSelectProperty() {
            var info = this.selectItem;
            if (info == null) {
                  this.removeChild();
                  return;
            }

            if (info) {
                  this.procSelectItem(info).then(result => {
                        this.oMeshObj.visible = true;
                  });

            }
      }

      async startLoadResource(){

            try {
                  if(this.selectItem) await this.procSelectItem(this.selectItem);
            }catch(error){
                  console.log("error ", error);
            } finally {
                  //this.isResourceLoaded = true;
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }

      }

      procSelectItem(info) {

            this._element.material.visible = false;

            /* OBJ 로딩 */
            var obj = {
                  compName: info.name,
                  objPath: info.path,
                  mapPath: info.mapPath,
                  recycleYn: true
            };

            return new Promise((resolve, reject)=> {
                  LoaderManager.setLoaderObj(obj).then(result => {
                        var oldObject = this._element.getObjectByName("3Dobject");

                        if (oldObject != undefined) {
                              MeshManager.disposeMesh(oldObject);
                              this._element.remove(oldObject);
                        }

                        this._elementSize = result.SIZE;
                        this.oMeshObj = result.MESH;
                        this.oMeshObj.name = "3Dobject";
                        this._element.material.visible = false;
                        this.oMeshObj.visible = false;
                        this.isResourceLoaded = true;
                        this._element.add(this.oMeshObj);
                        this.size = this._elementSize;

                        resolve(true);
                  });
            });

      }

      onLoadPage(){
            console.log("$onLoadPage ", this);
            if( this.isResourceLoaded){
                  this.oMeshObj.visible = true;
            }

      }


      removeChild() {

            this._element.material.visible = true;
            var oldObject = this._element.getObjectByName("3Dobject");

            if (oldObject != undefined) {
                  MeshManager.disposeMesh(oldObject);
                  this._element.remove(oldObject);
            }

            this.size = { x: 15, y: 15, z: 15 };

      }

      _onClick(event){

            this.dispatchWScriptEvent("click", {
                  value: event.target
            });

      }

      _onMouseOver(event) {
            MeshManager.mouseEvent(event, "over");
      }

      _onMouseOut(event) {
            MeshManager.mouseEvent(event, "out");
      }

      _onDestroy() {
            this._useResource = null;
            this.oMeshObj = null;
            this._invalidateSelectItem = null;
            this._invalidOriginSize = null;
            //this.unbindDomEvents();

            this._clickHandler = null;

            super._onDestroy();
      }

      set originSize(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "originSize", value)) {
                  this._invalidOriginSize = true;
            }
      }

      get originSize() {
            return this.getGroupPropertyValue("setter", "originSize");
      }

      set selectItem(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                  this._invalidateSelectItem = true;
            }

      }

      get selectItem() {
            return this.getGroupPropertyValue("setter", "selectItem");
      }

}


WV3DPropertyManager.attach_default_component_infos(ObjLoaderComponent, {
    "setter": {
        "position": { x: 0, y: 0, z: 0 },
        "size": { x: 10, y: 10, z: 10 },
        "selectItem": "",
        "originSize": false
    },
    "label": {
        "label_text": "ObjLoaderComponent",
    },

    "info": {
        "componentName": "ObjLoaderComponent",
        "version": "1.0.0",
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.

ObjLoaderComponent.property_panel_info = [{
        template: "primary"
    }, {
        template: "pos-size-3d"
    }, {
        template: "label-3d"
    },
    {
        label: "Original Size",
        template: "vertical",
        children: [{
            owner: "setter",
            name: "originSize",
            type: "checkbox",
            label: "Use",
            show: true,
            writable: true,
            description: "원본 크기 전환"
        }]
    },
    {
        label: "Resource",
        template: "resource",
        children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D Object",
            options: {
                type: "obj3d"
            }
        }]
    }
];
