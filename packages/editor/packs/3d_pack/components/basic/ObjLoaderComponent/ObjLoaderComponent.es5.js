'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ObjLoaderComponent = function (_WV3DComponent) {
      _inherits(ObjLoaderComponent, _WV3DComponent);

      function ObjLoaderComponent() {
            _classCallCheck(this, ObjLoaderComponent);

            var _this = _possibleConstructorReturn(this, (ObjLoaderComponent.__proto__ || Object.getPrototypeOf(ObjLoaderComponent)).call(this));

            _this._isResourceComponent = true;
            _this._elementdblclick = _this._onClick.bind(_this);
            return _this;
      }

      _createClass(ObjLoaderComponent, [{
            key: '_onCreateProperties',
            value: function _onCreateProperties() {
                  this.oMeshObj = "";
                  this._invalidateSelectItem = false;
                  this._invalidOriginSize = false;
                  this.isResourceLoaded = false;
            }
      }, {
            key: '_onCreateElement',
            value: function _onCreateElement() {
                  this._element.material.wireframe = true;
                  if (this.isViewerMode) {
                        this._element.material.visible = false;
                  }
                  this._elementSize = new THREE.Box3().setFromObject(this._element).getSize();
            }
      }, {
            key: 'unbindDomEvents',
            value: function unbindDomEvents() {
                  this._domEvents.removeEventListener(this._element, 'mouseover', this._onMouseOver, false);
                  this._domEvents.removeEventListener(this._element, 'mouseout', this._onMouseOut, false);
            }
      }, {
            key: 'bindDomEvents',
            value: function bindDomEvents() {
                  this.unbindDomEvents();
                  this._domEvents.addEventListener(this._element, 'mouseover', this._onMouseOver, false);
                  this._domEvents.addEventListener(this._element, 'mouseout', this._onMouseOut, false);
            }
      }, {
            key: '_onCommitProperties',
            value: function _onCommitProperties() {

                  if (this._invalidateSelectItem) {
                        this._invalidateSelectItem = false;
                        this.validateCallLater(this._validateSelectProperty);
                  }

                  if (this._invalidOriginSize) {
                        this._invalidOriginSize = false;
                        this.validateCallLater(this._validateOriginSizeProperty);
                  }
            }
      }, {
            key: '_validateOriginSizeProperty',
            value: function _validateOriginSizeProperty() {
                  this.size = this._elementSize;
            }
      }, {
            key: '_validateSelectProperty',
            value: function _validateSelectProperty() {
                  var _this2 = this;

                  var info = this.selectItem;
                  if (info == null) {
                        this.removeChild();
                        return;
                  }

                  if (info) {
                        this.procSelectItem(info).then(function (result) {
                              _this2.oMeshObj.visible = true;
                        });
                  }
            }
      }, {
            key: 'startLoadResource',
            value: function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                _context.prev = 0;

                                                if (!this.selectItem) {
                                                      _context.next = 4;
                                                      break;
                                                }

                                                _context.next = 4;
                                                return this.procSelectItem(this.selectItem);

                                          case 4:
                                                _context.next = 9;
                                                break;

                                          case 6:
                                                _context.prev = 6;
                                                _context.t0 = _context['catch'](0);

                                                console.log("error ", _context.t0);

                                          case 9:
                                                _context.prev = 9;

                                                //this.isResourceLoaded = true;
                                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                return _context.finish(9);

                                          case 12:
                                          case 'end':
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this, [[0, 6, 9, 12]]);
                  }));

                  function startLoadResource() {
                        return _ref.apply(this, arguments);
                  }

                  return startLoadResource;
            }()
      }, {
            key: 'procSelectItem',
            value: function procSelectItem(info) {
                  var _this3 = this;

                  this._element.material.visible = false;

                  /* OBJ 로딩 */
                  var obj = {
                        compName: info.name,
                        objPath: info.path,
                        mapPath: info.mapPath,
                        recycleYn: true
                  };

                  return new Promise(function (resolve, reject) {
                        LoaderManager.setLoaderObj(obj).then(function (result) {
                              var oldObject = _this3._element.getObjectByName("3Dobject");

                              if (oldObject != undefined) {
                                    MeshManager.disposeMesh(oldObject);
                                    _this3._element.remove(oldObject);
                              }

                              _this3._elementSize = result.SIZE;
                              _this3.oMeshObj = result.MESH;
                              _this3.oMeshObj.name = "3Dobject";
                              _this3._element.material.visible = false;
                              _this3.oMeshObj.visible = false;
                              _this3.isResourceLoaded = true;
                              _this3._element.add(_this3.oMeshObj);
                              _this3.size = _this3._elementSize;

                              resolve(true);
                        });
                  });
            }
      }, {
            key: 'onLoadPage',
            value: function onLoadPage() {
                  console.log("$onLoadPage ", this);
                  if (this.isResourceLoaded) {
                        this.oMeshObj.visible = true;
                  }
            }
      }, {
            key: 'removeChild',
            value: function removeChild() {

                  this._element.material.visible = true;
                  var oldObject = this._element.getObjectByName("3Dobject");

                  if (oldObject != undefined) {
                        MeshManager.disposeMesh(oldObject);
                        this._element.remove(oldObject);
                  }

                  this.size = { x: 15, y: 15, z: 15 };
            }
      }, {
            key: '_onClick',
            value: function _onClick(event) {

                  this.dispatchWScriptEvent("click", {
                        value: event.target
                  });
            }
      }, {
            key: '_onMouseOver',
            value: function _onMouseOver(event) {
                  MeshManager.mouseEvent(event, "over");
            }
      }, {
            key: '_onMouseOut',
            value: function _onMouseOut(event) {
                  MeshManager.mouseEvent(event, "out");
            }
      }, {
            key: '_onDestroy',
            value: function _onDestroy() {
                  this._useResource = null;
                  this.oMeshObj = null;
                  this._invalidateSelectItem = null;
                  this._invalidOriginSize = null;
                  //this.unbindDomEvents();

                  this._clickHandler = null;

                  _get(ObjLoaderComponent.prototype.__proto__ || Object.getPrototypeOf(ObjLoaderComponent.prototype), '_onDestroy', this).call(this);
            }
      }, {
            key: 'originSize',
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "originSize", value)) {
                        this._invalidOriginSize = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "originSize");
            }
      }, {
            key: 'selectItem',
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                        this._invalidateSelectItem = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "selectItem");
            }
      }]);

      return ObjLoaderComponent;
}(WV3DComponent);

WV3DPropertyManager.attach_default_component_infos(ObjLoaderComponent, {
      "setter": {
            "position": { x: 0, y: 0, z: 0 },
            "size": { x: 10, y: 10, z: 10 },
            "selectItem": "",
            "originSize": false
      },
      "label": {
            "label_text": "ObjLoaderComponent"
      },

      "info": {
            "componentName": "ObjLoaderComponent",
            "version": "1.0.0"
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.

ObjLoaderComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-3d"
}, {
      template: "label-3d"
}, {
      label: "Original Size",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "originSize",
            type: "checkbox",
            label: "Use",
            show: true,
            writable: true,
            description: "원본 크기 전환"
      }]
}, {
      label: "Resource",
      template: "resource",
      children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D Object",
            options: {
                  type: "obj3d"
            }
      }]
}];
