class PlaneTextComponent extends NWV3DComponent {

      constructor(){ super();}

      _createDomComponentLabel(){}



      immediateUpdateDisplay(){
            this._validateTextInfo();
            super.immediateUpdateDisplay();
      }


      _onCreateElement(){
            this._layer = document.createElement("canvas");
            this._layerCtx = this._layer.getContext("2d");
            this._textureLayer = document.createElement("canvas");
            this._textureCtx = this._textureLayer.getContext("2d");
            let texture = new THREE.Texture(this._textureLayer);
            texture.needsUpdate = true;
            let material  = new THREE.MeshPhongMaterial({map:texture, transparent:true, side:THREE.DoubleSide});
            let geometry  = new THREE.PlaneGeometry( 1, 1, 1, 1 );
            this._element = new THREE.Mesh( geometry, material );
            this.appendElement.add(this._element);
      }

      _commitProperties()  {
            if(this._updatePropertiesMap.has("textInfo")){
                  this.validateCallLater(this._validateTextInfo);
                  // validateTextInfo후 size 갱신처리
                  this.validateCallLater(this._validateSize);
            }
            super._commitProperties();
      }



      _validateTextInfo(){

            let textInfo = this.getGroupProperties("textInfo");
            let textProps = Object.assign({color:this.color}, textInfo);
            let textureInfo = ThreeUtil.getLabelTexture(textProps);
            let w = textureInfo.w;
            let h = textureInfo.h;
            this.element.material.map.dispose();
            this.element.geometry.dispose();
            this.element.geometry   = new THREE.PlaneGeometry(w, h, 4, 4 );
            //this.element.geometry   = new THREE.PlaneGeometry(this.size.x, this.size.y, 4, 4 );
            this.element.material.map     = textureInfo.texture;
            this._inValidateElementSize();
            this._validateElementSize({x:w, y:h, z:1});
            this._validateOutline();
      }

      _validateColor(){
            // 컬러만 변경 시 texture를 갱신하고
            // 이전 크기 정보를 다시 설정해 줌
            let prevSize = this.size;
            this._validateTextInfo();
            this.size = prevSize;
      }

      _onDestroy(){
            this._textureCtx        = null;
            this._layerCtx          = null;
            this._layer             = null;
            this._textureLayer      = null;
            super._onDestroy();
      }

}




WV3DPropertyManager.attach_default_component_infos(PlaneTextComponent, {
      "setter": {
            "size": { x: 80, y: 50, z: 1 },
            "color":"#000000"
      },
      "textInfo": {
            "text": "Text",
            "fontName":"Arial",
            "fontSize":80,
            "fontWeight":700,
            "marginW":0,
            "marginH":10,
            "useBackGround":false,
            "cornerRadius" :0,
            "bgColor":"#ffffff",
            "borderColor":"#000000",
            "borderWidth":4
      },
      "label": {
            "label_using": "N"
      },
      "info": {
            "componentName": "PlaneTextComponent",
            "version": "1.0.0",
      }
});


WV3DPropertyManager.add_property_panel_group_info(PlaneTextComponent, {
      label: "textInfo",
      template: "text-info",
      children:[{
            owner: "textInfo",
            name: "textInfo",
            type: "object",
            label: "Text Info",
            show: true,
            writable: true,
            description: "textInfo"
      }]
});

WV3DPropertyManager.add_property_group_info(PlaneTextComponent, {
      name : "textInfo",
      label: "텍스트 정보",
      children: [{
            owner:"textInfo",
            name: "text",
            type: "string",
            show: true,
            writable: true,
            defaultValue: "Text",
            description: "컴포넌트 text을 설정합니다. <br>을 이용한 줄바꿈 제공"
      }, {
            owner:"textInfo",
            name: "fontSize",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "80",
            description: "컴포넌트 fontSize를 설정합니다."
      }, {
            owner:"textInfo",
            name: "fontWeight",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "700",
            description: "컴포넌트 fontWeight를 설정합니다."
      },{
            owner:"textInfo",
            name: "marginW",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "0",
            description: "text 가로 margin을 설정합니다."
      },{
            owner:"textInfo",
            name: "marginH",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "0",
            description: "text 세로 margin을 설정합니다."
      },{
            owner:"textInfo",
            name: "useBackGround",
            type: "boolean",
            show: true,
            writable: true,
            defaultValue: false,
            description: "text에 배경 사용여부를 설정합니다."
      },{
            owner:"textInfo",
            name: "bgColor",
            type: "color",
            show: true,
            writable: true,
            defaultValue: "#ffffff",
            description: "배경 컬러를 설정합니다."
      },{
            owner:"textInfo",
            name: "cornerRadius",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "0",
            description: "배경에 cornerRadius를 설정합니다."
      }]
});

WV3DPropertyManager.removePropertyGroupByName(PlaneTextComponent.property_panel_info, "label", "label");
WV3DPropertyManager.removePropertyGroupByName(PlaneTextComponent.property_info, "label", "레이블 속성");
WV3DPropertyManager.removePropertyGroupChildrenByName( PlaneTextComponent.property_info, "normal", "boundingEnable");


