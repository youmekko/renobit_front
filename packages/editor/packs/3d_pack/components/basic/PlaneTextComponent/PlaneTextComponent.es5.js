function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var PlaneTextComponent =
      /*#__PURE__*/
      function (_NWV3DComponent) {
            "use strict";

            _inherits(PlaneTextComponent, _NWV3DComponent);

            function PlaneTextComponent() {
                  _classCallCheck(this, PlaneTextComponent);

                  return _possibleConstructorReturn(this, _getPrototypeOf(PlaneTextComponent).call(this));
            }

            _createClass(PlaneTextComponent, [{
                  key: "_createDomComponentLabel",
                  value: function _createDomComponentLabel() {}
            },  {
                  key: "immediateUpdateDisplay",
                  value: function immediateUpdateDisplay() {
                        this._validateTextInfo();

                        _get(_getPrototypeOf(PlaneTextComponent.prototype), "immediateUpdateDisplay", this).call(this);
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this._layer = document.createElement("canvas");
                        this._layerCtx = this._layer.getContext("2d");
                        this._textureLayer = document.createElement("canvas");
                        this._textureCtx = this._textureLayer.getContext("2d");
                        var texture = new THREE.Texture(this._textureLayer);
                        texture.needsUpdate = true;
                        var material = new THREE.MeshPhongMaterial({
                              map: texture,
                              transparent: true,
                              side: THREE.DoubleSide
                        });
                        var geometry = new THREE.PlaneGeometry(1, 1, 1, 1);
                        this._element = new THREE.Mesh(geometry, material);
                        this.appendElement.add(this._element);
                  }
            }, {
                  key: "_commitProperties",
                  value: function _commitProperties() {
                        if (this._updatePropertiesMap.has("textInfo")) {
                              this.validateCallLater(this._validateTextInfo); // validateTextInfo후 size 갱신처리
                              this.validateCallLater(this._validateSize);
                        }

                        _get(_getPrototypeOf(PlaneTextComponent.prototype), "_commitProperties", this).call(this);
                  }
            }, {
                  key: "_validateTextInfo",
                  value: function _validateTextInfo() {
                        var textInfo = this.getGroupProperties("textInfo");
                        var textProps = Object.assign({
                              color: this.color
                        }, textInfo);
                        var textureInfo = ThreeUtil.getLabelTexture(textProps);
                        var w = textureInfo.w;
                        var h = textureInfo.h;
                        this.element.material.map.dispose();
                        this.element.geometry.dispose(); //this.element.geometry   = new THREE.PlaneGeometry(w, h, 4, 4 );

                        this.element.geometry = new THREE.PlaneGeometry(w, h, 4, 4);
                        this.element.material.map = textureInfo.texture;
                        this._inValidateElementSize();
                        this._validateElementSize({x:w, y:h, z:1});
                        this._validateOutline();
                  }
            }, {
                  key: "_validateColor",
                  value: function _validateColor() {
                        var prevSize = this.size;
                        this._validateTextInfo();
                        this.size = prevSize;
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this._textureCtx = null;
                        this._layerCtx = null;
                        this._layer = null;
                        this._textureLayer = null;

                        _get(_getPrototypeOf(PlaneTextComponent.prototype), "_onDestroy", this).call(this);
                  }
            }]);

            return PlaneTextComponent;
      }(NWV3DComponent);

WV3DPropertyManager.attach_default_component_infos(PlaneTextComponent, {
      "setter": {
            "size": {
                  x: 80,
                  y: 50,
                  z: 1
            },
            "color": "#000000"
      },
      "textInfo": {
            "text": "Text",
            "fontName": "Arial",
            "fontSize": 80,
            "fontWeight": 700,
            "marginW": 0,
            "marginH": 10,
            "useBackGround": false,
            "cornerRadius": 0,
            "bgColor": "#ffffff",
            "borderColor": "#000000",
            "borderWidth": 4
      },
      "label": {
            "label_using": "N"
      },
      "info": {
            "componentName": "PlaneTextComponent",
            "version": "1.0.0"
      }
});
WV3DPropertyManager.add_property_panel_group_info(PlaneTextComponent, {
      label: "textInfo",
      template: "text-info",
      children: [{
            owner: "textInfo",
            name: "textInfo",
            type: "object",
            label: "Text Info",
            show: true,
            writable: true,
            description: "textInfo"
      }]
});
WV3DPropertyManager.add_property_group_info(PlaneTextComponent, {
      name: "textInfo",
      label: "텍스트 정보",
      children: [{
            owner: "textInfo",
            name: "text",
            type: "string",
            show: true,
            writable: true,
            defaultValue: "Text",
            description: "컴포넌트 text을 설정합니다. <br>을 이용한 줄바꿈 제공"
      }, {
            owner: "textInfo",
            name: "fontSize",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "80",
            description: "컴포넌트 fontSize를 설정합니다."
      }, {
            owner: "textInfo",
            name: "fontWeight",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "700",
            description: "컴포넌트 fontWeight를 설정합니다."
      }, {
            owner: "textInfo",
            name: "marginW",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "0",
            description: "text 가로 margin을 설정합니다."
      }, {
            owner: "textInfo",
            name: "marginH",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "0",
            description: "text 세로 margin을 설정합니다."
      }, {
            owner: "textInfo",
            name: "useBackGround",
            type: "boolean",
            show: true,
            writable: true,
            defaultValue: false,
            description: "text에 배경 사용여부를 설정합니다."
      }, {
            owner: "textInfo",
            name: "bgColor",
            type: "color",
            show: true,
            writable: true,
            defaultValue: "#ffffff",
            description: "배경 컬러를 설정합니다."
      }, {
            owner: "textInfo",
            name: "cornerRadius",
            type: "number",
            show: true,
            writable: true,
            defaultValue: "0",
            description: "배경에 cornerRadius를 설정합니다."
      }]
});
WV3DPropertyManager.removePropertyGroupByName(PlaneTextComponent.property_panel_info, "label", "label");
WV3DPropertyManager.removePropertyGroupByName(PlaneTextComponent.property_info, "label", "레이블 속성");
WV3DPropertyManager.removePropertyGroupChildrenByName(PlaneTextComponent.property_info, "normal", "boundingEnable");
