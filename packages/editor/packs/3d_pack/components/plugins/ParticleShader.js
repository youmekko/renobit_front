var particle_vertexshader = 'uniform float amplitude;\n' +
    '\t\t\tattribute float size;\n' +
    '\t\t\tattribute vec3 customColor;\n' +
    '\n' +
    '\t\t\tvarying vec3 vColor;\n' +
    '\n' +
    '\t\t\tvoid main() {\n' +
    '\n' +
    '\t\t\t\tvColor = customColor;\n' +
    '\n' +
    '\t\t\t\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n' +
    '\n' +
    '\t\t\t\tgl_PointSize = size;\n' +
    '\n' +
    '\t\t\t\tgl_Position = projectionMatrix * mvPosition;\n' +
    '\n' +
    '\t\t\t}'

var particle_fragmentshader = 'uniform vec3 color;\n' +
    '\t\t\tuniform sampler2D texture;\n' +
    '\t\t\tuniform sampler2D mask_texture;\n' +
    '\n' +
    '\t\t\tvarying vec3 vColor;\n' +
    '\n' +
    '\t\t\tvoid main() {\n' +
    '\n' +
    '\t\t\t\tgl_FragColor = vec4( color * vColor, 1 );\n' +
    '\t\t\t\tgl_FragColor = gl_FragColor * texture2D( texture, gl_PointCoord );\n' +
    '\n' +
    '\t\t\t}'

var particle_dot_vertexshader = 'attribute float scale; \n' +
    '\t\t\tvoid main() { \n' +
    ' \n' +
    '\t\t\t\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 ); \n' +
    ' \n' +
    '\t\t\t\tgl_PointSize = scale * ( 300.0 / - mvPosition.z ); \n' +
    ' \n' +
    '\t\t\t\tgl_Position = projectionMatrix * mvPosition; \n' +
    ' \n' +
    '\t\t\t} \n';

var particle_dot_fragmentshader = 'uniform vec3 color; \n' +
    ' \n' +
    '\t\t\tvoid main() { \n' +
    ' \n' +
    '\t\t\t\tif ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard; \n' +
    '\n' +
    '\t\t\t\tgl_FragColor = vec4( color, 1.0 ); \n' +
    ' \n' +
    '\t\t\t} \n'