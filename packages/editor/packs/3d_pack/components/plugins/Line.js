class Line extends NWV3DComponent {
      constructor(){
            super();
            this._fatLine = null;
            this._localPoints = [];
            this._colors = [];
            this._dashProperties = null;
            this._curve = null;
      }
      setConnectionInfo(){
            let info = this.getGroupPropertyValue("connection", "targets");
            let entries = Object.entries(info);
            for( let [key, value] of entries){
                  window.wemb.mainPageComponent.getComInstanceByName(value).setConnection(key, value);
            }
      }
      _rgbToArray(color){
            var color_arr = color.substring(4, color.length-1)
                  .replace(/ /g, '')
                  .split(',');
            return [color_arr[0]/255,color_arr[1]/255,color_arr[2]/255]
      }
      _hexToRgb(hex) {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                  return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                  r: parseInt(result[1], 16) / 255,
                  g: parseInt(result[2], 16) / 255,
                  b: parseInt(result[3], 16) / 255
            } : null;
      }
      _onCreateProperties(){
            super._onCreateProperties();

            if(this.isEditorMode){
                  window.wemb.hookManager.addAction('hook/before_open_page', function(){
                        this.setConnectionInfo();
                  }.bind(this))
            }

            let cl =this.getGroupPropertyValue("setter", "color");
            if(cl.indexOf('#') > -1)
            {
                  var c = this._hexToRgb(cl);
                  this._colors = [c.r, c.g, c.b]
            }else{
                  this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
            }
            this._dashProperties = this.getGroupProperties("dash");
      }
      _onCommitProperties(){
            super._onCommitProperties();
            if(this._updatePropertiesMap.has("dash.dash")){
                  if(this.getGroupPropertyValue("dash", "dash"))
                        this._fatLine.material.defines.USE_DASH = "";
                  else
                        delete this._fatLine.material.defines.USE_DASH;
                  this._fatLine.computeLineDistances();
                  this._fatLine.material.needsUpdate = true;
            }
            if(this._updatePropertiesMap.has("dash.dashSize") || this._updatePropertiesMap.has("dash.gapSize") || this._updatePropertiesMap.has("dash.dashScale")){
                  this._fatLine.material.dashSize = this._dashProperties.dashSize;
                  this._fatLine.material.gapSize = this._dashProperties.gapSize;
                  this._fatLine.material.dashScale = this._dashProperties.dashScale;
                  this._fatLine.computeLineDistances();
                  this._fatLine.material.needsUpdate = true;
            }
            if(this._updatePropertiesMap.has("setter.helper_size")){
                  let children = this.appendElement.children;
                  let size = this.getGroupPropertyValue("setter","helper_size");
                  children.filter((child)=>  child.name.includes('local')).forEach((child)=>{
                        child.scale.set(size, size, size);
                  });
            }
      }
      _addPointHelperObject(position){
            var object = new THREE.Mesh(this._pointHelperGeometry.clone(), new THREE.MeshBasicMaterial({color :0xFF00CC}));
            object.position.copy(position);
            object.castShadow = true;
            object.receiveShadow = true;

            var size = this.getGroupPropertyValue("setter","helper_size");
            object.scale.x = size;
            object.scale.y = size;
            object.scale.z = size;

            return object
      }
      _makeCurve(){
            this._curve = new THREE.CatmullRomCurve3(this._localPoints);
            this._curve.curveType = 'catmullrom';
            this._curve.tension = 0;

            let curveGeometry = new THREE.BufferGeometry();
            curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curve.points.length * 3), 3));
            curveGeometry.setFromPoints(this._curve.points);
            var curveMaterial = new THREE.LineBasicMaterial({color : new THREE.Color(this._colors[0], this._colors[1], this._colors[2]) });
            var curveMesh = new THREE.LineSegments(curveGeometry, curveMaterial);//new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);
            this._curve.mesh = curveMesh;
            this._curve.mesh.castShadow = true;

            this._element.geometry.dispose();
            this._element.material.dispose();
            this._element = this._curve.mesh;
            if(!this.isEditorMode)
                  this._element.visible = false;
            this.appendElement.add(this._element);
            this._makeFatline();
      }
      _makeFatline(){
            let fatline_positions = [];
            let fatline_colors = [];

            for(let i = 0 ; i < this._localPoints.length ;i++)
            {
                  fatline_positions.push(this._localPoints[i].x, this._localPoints[i].y, this._localPoints[i].z);
                  fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
            }

            let fatlineGeometry = new THREE.LineGeometry();
            fatlineGeometry.setPositions(fatline_positions);
            fatlineGeometry.setColors(fatline_colors);

            let fatlineMaterial = new THREE.LineMaterial({
                  color : 0xffffff,
                  linewidth : this.width,
                  vertexColors : THREE.VertexColors,
                  dashSize : this._dashProperties.dashSize,
                  dashScale : this._dashProperties.dashScale,
                  gapSize : this._dashProperties.gapSize
            });
            fatlineMaterial.resolution.set(window.innerWidth, window.innerHeight);

            this._fatLine = new THREE.Line2(fatlineGeometry, fatlineMaterial);
            if(this._dashProperties.dash)
                  this._fatLine.material.defines.USE_DASH = "";
            this._fatLine.computeLineDistances();
            this._fatLine.geometry.needsUpdate = true;
            this._fatLine.material.needsUpdate = true;

            this._fatLine.scale.set(1,1,1);
            this.appendElement.add(this._fatLine);
      }
      _Update(){
            this._curve.points = this._localPoints;
            this._fatLine.material.linewidth = this.width;

            let fatline_positions = [];
            let fatline_colors = [];

            for(let i = 0; i < this._localPoints.length; i++)
            {
                  fatline_positions.push(this._localPoints[i].x, this._localPoints[i].y, this._localPoints[i].z);
                  fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
            }

            this._fatLine.geometry.setPositions(fatline_positions);
            this._fatLine.geometry.setColors(fatline_colors);

            //피킹 라인 업데이트
            let pickingLinePoint = this._curve.getPoints(this._linePickingSegments);
            this._element.geometry.removeAttribute('position');
            this._element.geometry.addAttribute('position', new THREE.Float32BufferAttribute( pickingLinePoint.map((data)=> [data.x, data.y, data.z]).flat(), 3 ) );
            this._element.geometry.needsUpdate = true;
            this._element.geometry.attributes.position.needsUpdate = true;
            this._element.geometry.computeBoundingBox();
            this._element.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2])
      }
      set width(wd){
            if(wd < 1)
                  this.setGroupPropertyValue("line_width", "width", 1);
            else
                  this.setGroupPropertyValue("line_width", "width", wd);
      }
      get width(){
            return this.getGroupPropertyValue("line_width", "width");
      }
      get helper_size(){
            return this.getGroupPropertyValue("setter", "helper_size");
      }
      set helper_size(v){
            return this.setGroupPropertyValue("setter", "helper_size", v);
      }
      _onDestroy(){
            for(let [key, value] of Object.entries(this.getGroupPropertyValue('connection', 'targets'))){
                  wemb.mainPageComponent.getComInstanceByName(value).deleteConnection(key);
            }

            this._pointsOrder.forEach((i)=>{
                  var obj = this.appendElement.getObjectByName('local'+i);
                  this.appendElement.remove(obj);
                  obj.geometry.dispose();
                  obj.material.dispose();
                  obj = this.appendElement.getObjectByName('world'+i);
                  this.appendElement.remove(obj);
                  obj.geometry.dispose();
                  obj.material.dispose();
                  this._domEvents.removeEventListener(obj, 'click', this._onHelperClickCalle, false);
            });

            if(this.isEditorMode) {
                  this._controls.removeEventListener('objectChange', this._onHelperChangeCalle, false);
                  this._controls.removeEventListener('mouseUp', this._onTransformMouseUpCalle, false);
            }

            this.appendElement.remove(this._fatLine);
            this._fatLine.geometry.dispose();
            this._fatLine.material.dispose();
            this._fatLine = null;

            this.appendElement.remove(this._curve);
            this._curve.mesh.geometry.dispose();
            this._curve.mesh.material.dispose();
            this._curve = null;

            this._localPoints = [];
            this._points = [];
            this._pointsOrder = [];
            this._outlineElement = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshPhongMaterial').clone());

            super._onDestroy();
      }
}
