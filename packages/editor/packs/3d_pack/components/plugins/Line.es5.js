"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Line =
      /*#__PURE__*/
      function (_NWV3DComponent) {
            _inherits(Line, _NWV3DComponent);

            function Line() {
                  var _this;

                  _classCallCheck(this, Line);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(Line).call(this));
                  _this._fatLine = null;
                  _this._localPoints = [];
                  _this._colors = [];
                  _this._dashProperties = null;
                  _this._curve = null;
                  return _this;
            }

            _createClass(Line, [{
                  key: "setConnectionInfo",
                  value: function setConnectionInfo() {
                        var info = this.getGroupPropertyValue("connection", "targets");
                        var entries = Object.entries(info);

                        for (var _i = 0, _entries = entries; _i < _entries.length; _i++) {
                              var _entries$_i = _slicedToArray(_entries[_i], 2),
                                    key = _entries$_i[0],
                                    value = _entries$_i[1];

                              window.wemb.mainPageComponent.getComInstanceByName(value).setConnection(key, value);
                        }
                  }
            }, {
                  key: "_rgbToArray",
                  value: function _rgbToArray(color) {
                        var color_arr = color.substring(4, color.length - 1).replace(/ /g, '').split(',');
                        return [color_arr[0] / 255, color_arr[1] / 255, color_arr[2] / 255];
                  }
            }, {
                  key: "_hexToRgb",
                  value: function _hexToRgb(hex) {
                        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
                        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                        hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                              return r + r + g + g + b + b;
                        });
                        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                        return result ? {
                              r: parseInt(result[1], 16) / 255,
                              g: parseInt(result[2], 16) / 255,
                              b: parseInt(result[3], 16) / 255
                        } : null;
                  }
            }, {
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(Line.prototype), "_onCreateProperties", this).call(this);

                        if (this.isEditorMode) {
                              window.wemb.hookManager.addAction('hook/before_open_page', function () {
                                    this.setConnectionInfo();
                              }.bind(this));
                        }

                        var cl = this.getGroupPropertyValue("setter", "color");

                        if (cl.indexOf('#') > -1) {
                              var c = this._hexToRgb(cl);

                              this._colors = [c.r, c.g, c.b];
                        } else {
                              this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                        }

                        this._dashProperties = this.getGroupProperties("dash");
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        _get(_getPrototypeOf(Line.prototype), "_onCommitProperties", this).call(this);

                        if (this._updatePropertiesMap.has("dash.dash")) {
                              if (this.getGroupPropertyValue("dash", "dash")) this._fatLine.material.defines.USE_DASH = "";else delete this._fatLine.material.defines.USE_DASH;

                              this._fatLine.computeLineDistances();

                              this._fatLine.material.needsUpdate = true;
                        }

                        if (this._updatePropertiesMap.has("dash.dashSize") || this._updatePropertiesMap.has("dash.gapSize") || this._updatePropertiesMap.has("dash.dashScale")) {
                              this._fatLine.material.dashSize = this._dashProperties.dashSize;
                              this._fatLine.material.gapSize = this._dashProperties.gapSize;
                              this._fatLine.material.dashScale = this._dashProperties.dashScale;

                              this._fatLine.computeLineDistances();

                              this._fatLine.material.needsUpdate = true;
                        }

                        if (this._updatePropertiesMap.has("setter.helper_size")) {
                              var children = this.appendElement.children;
                              var size = this.getGroupPropertyValue("setter", "helper_size");
                              children.filter(function (child) {
                                    return child.name.includes('local');
                              }).forEach(function (child) {
                                    child.scale.set(size, size, size);
                              });
                        }
                  }
            }, {
                  key: "_addPointHelperObject",
                  value: function _addPointHelperObject(position) {
                        var object = new THREE.Mesh(this._pointHelperGeometry.clone(), new THREE.MeshBasicMaterial({
                              color: 0xFF00CC
                        }));
                        object.position.copy(position);
                        object.castShadow = true;
                        object.receiveShadow = true;
                        var size = this.getGroupPropertyValue("setter", "helper_size");
                        object.scale.x = size;
                        object.scale.y = size;
                        object.scale.z = size;
                        return object;
                  }
            }, {
                  key: "_makeCurve",
                  value: function _makeCurve() {
                        this._curve = new THREE.CatmullRomCurve3(this._localPoints);
                        this._curve.curveType = 'catmullrom';
                        this._curve.tension = 0;
                        var curveGeometry = new THREE.BufferGeometry();
                        curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curve.points.length * 3), 3));
                        curveGeometry.setFromPoints(this._curve.points);
                        var curveMaterial = new THREE.LineBasicMaterial({
                              color: new THREE.Color(this._colors[0], this._colors[1], this._colors[2])
                        });
                        var curveMesh = new THREE.LineSegments(curveGeometry, curveMaterial); //new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);

                        this._curve.mesh = curveMesh;
                        this._curve.mesh.castShadow = true;

                        this._element.geometry.dispose();

                        this._element.material.dispose();

                        this._element = this._curve.mesh;
                        if (!this.isEditorMode) this._element.visible = false;
                        this.appendElement.add(this._element);

                        this._makeFatline();
                  }
            }, {
                  key: "_makeFatline",
                  value: function _makeFatline() {
                        var fatline_positions = [];
                        var fatline_colors = [];

                        for (var i = 0; i < this._localPoints.length; i++) {
                              fatline_positions.push(this._localPoints[i].x, this._localPoints[i].y, this._localPoints[i].z);
                              fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
                        }

                        var fatlineGeometry = new THREE.LineGeometry();
                        fatlineGeometry.setPositions(fatline_positions);
                        fatlineGeometry.setColors(fatline_colors);
                        var fatlineMaterial = new THREE.LineMaterial({
                              color: 0xffffff,
                              linewidth: this.width,
                              vertexColors: THREE.VertexColors,
                              dashSize: this._dashProperties.dashSize,
                              dashScale: this._dashProperties.dashScale,
                              gapSize: this._dashProperties.gapSize
                        });
                        fatlineMaterial.resolution.set(window.innerWidth, window.innerHeight);
                        this._fatLine = new THREE.Line2(fatlineGeometry, fatlineMaterial);
                        if (this._dashProperties.dash) this._fatLine.material.defines.USE_DASH = "";

                        this._fatLine.computeLineDistances();

                        this._fatLine.geometry.needsUpdate = true;
                        this._fatLine.material.needsUpdate = true;

                        this._fatLine.scale.set(1, 1, 1);

                        this.appendElement.add(this._fatLine);
                  }
            }, {
                  key: "_Update",
                  value: function _Update() {
                        this._curve.points = this._localPoints;
                        this._fatLine.material.linewidth = this.width;
                        var fatline_positions = [];
                        var fatline_colors = [];

                        for (var i = 0; i < this._localPoints.length; i++) {
                              fatline_positions.push(this._localPoints[i].x, this._localPoints[i].y, this._localPoints[i].z);
                              fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
                        }

                        this._fatLine.geometry.setPositions(fatline_positions);

                        this._fatLine.geometry.setColors(fatline_colors); //피킹 라인 업데이트


                        var pickingLinePoint = this._curve.getPoints(this._linePickingSegments);

                        this._element.geometry.removeAttribute('position');

                        this._element.geometry.addAttribute('position', new THREE.Float32BufferAttribute(pickingLinePoint.map(function (data) {
                              return [data.x, data.y, data.z];
                        }).reduce(function(a, b) {
                              return a.concat(b)
                        }), 3));

                        this._element.geometry.needsUpdate = true;
                        this._element.geometry.attributes.position.needsUpdate = true;

                        this._element.geometry.computeBoundingBox();

                        this._element.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2]);
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        var _this2 = this;

                        for (var _i2 = 0, _Object$entries = Object.entries(this.getGroupPropertyValue('connection', 'targets')); _i2 < _Object$entries.length; _i2++) {
                              var _Object$entries$_i = _slicedToArray(_Object$entries[_i2], 2),
                                    key = _Object$entries$_i[0],
                                    value = _Object$entries$_i[1];

                              wemb.mainPageComponent.getComInstanceByName(value).deleteConnection(key);
                        }

                        this._pointsOrder.forEach(function (i) {
                              var obj = _this2.appendElement.getObjectByName('local' + i);

                              _this2.appendElement.remove(obj);

                              obj.geometry.dispose();
                              obj.material.dispose();
                              obj = _this2.appendElement.getObjectByName('world' + i);

                              _this2.appendElement.remove(obj);

                              obj.geometry.dispose();
                              obj.material.dispose();

                              _this2._domEvents.removeEventListener(obj, 'click', _this2._onHelperClickCalle, false);
                        });

                        if (this.isEditorMode) {
                              this._controls.removeEventListener('objectChange', this._onHelperChangeCalle, false);

                              this._controls.removeEventListener('mouseUp', this._onTransformMouseUpCalle, false);
                        }

                        this.appendElement.remove(this._fatLine);

                        this._fatLine.geometry.dispose();

                        this._fatLine.material.dispose();

                        this._fatLine = null;
                        this.appendElement.remove(this._curve);

                        this._curve.mesh.geometry.dispose();

                        this._curve.mesh.material.dispose();

                        this._curve = null;
                        this._localPoints = [];
                        this._points = [];
                        this._pointsOrder = [];
                        this._outlineElement = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshPhongMaterial').clone());

                        _get(_getPrototypeOf(Line.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "width",
                  set: function set(wd) {
                        if (wd < 1) this.setGroupPropertyValue("line_width", "width", 1);else this.setGroupPropertyValue("line_width", "width", wd);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("line_width", "width");
                  }
            }, {
                  key: "helper_size",
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "helper_size");
                  },
                  set: function set(v) {
                        return this.setGroupPropertyValue("setter", "helper_size", v);
                  }
            }]);

            return Line;
      }(NWV3DComponent);
