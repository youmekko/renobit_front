"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UnitBase = function () {
      function UnitBase(objSkin) {
            _classCallCheck(this, UnitBase);

            this._obj = objSkin;
            this._unitSize = ThreeUtil.getObjectSize(this._obj);
            this._data = null;
            this.selected = false;
            this._focusTween = null;
      }

      _createClass(UnitBase, [{
            key: "compose",
            value: function compose() {
                  //회전 정보가 있으면 geometry를 회전
                  if (this.data.rotateY) {
                        this._obj.children[0].geometry.rotateZ(-90 * Math.PI / 180);
                  }

                  //회전값에 따라 x, y 값을 서로 변경
                  var sx = this.data.scale.x;
                  var sy = this.data.scale.y;
                  var tx = this.data.rotateY ? 0 : this._unitSize.x / 2;
                  var ty = this.data.rotateY ? -this._unitSize.x / 2 : -this._unitSize.y;
                  this._obj.children[0].geometry.translate(tx, ty, 0);
                  this._obj.scale.set(sx, sy, this.data.scale.z);
                  this._obj.position.set(this.data.position.x, this.data.position.y, this.data.position.z);
            }
      }, {
            key: "clearTween",
            value: function clearTween() {
                  if (this._focusTween != null && this._focusTween.isActive()) {
                        this._focusTween.pause();
                        this._focusTween.kill();
                  }
            }
      }, {
            key: "setFocus",
            value: function setFocus() {
                  this.selected = true;
                  this.clearTween();
                  console.log("data", this._data );
                  this._focusTween = TweenMax.to(this._obj.position, .5, {
                        z: this._unitSize.z / 2,
                        onComplete: function onComplete() {
                              //this._obj.children[0].material.color.setHex(0xffeeff);
                        }
                  });
            }
      }, {
            key: "clearFocus",
            value: function clearFocus() {
                  this.selected = false;
                  this.clearTween();
                  this._focusTween = TweenMax.to(this._obj.position, .5, {
                        z: 0,
                        onComplete: function onComplete() {
                              //this._obj.children[0].material.color.setHex(0xffffff);
                        }
                  });
            }
      }, {
            key: "toggle",
            value: function toggle() {
                  if (this._obj.position.z == 0) {
                        this.setFocus();
                  } else {
                        this.clearFocus();
                  }
            }
      }, {
            key: "destroy",
            value: function destroy() {
                  this.clearTween();
                  this._focusTween = null;
                  this._obj.userData = null;
                  MeshManager.disposeMesh(this._obj);
                  this._obj = null;
                  this._data = null;
                  this._unitSize = null;
            }
      }, {
            key: "name",
            set: function set(strName) {
                  this._obj.name = strName;
            },
            get: function get() {
                  return this._obj.name;
            }
      }, {
            key: "data",
            set: function set(data) {
                  this._data = data;
                  this.compose();
            },
            get: function get() {
                  return this._data;
            }
      }, {
            key: "skin",
            get: function get() {
                  return this._obj;
            }
      }, {
            key: "size",
            get: function get() {
                  return this._unitSize;
            }
      }]);

      return UnitBase;
}();
