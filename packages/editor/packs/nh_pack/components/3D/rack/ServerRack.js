class ServerRack extends AssetBaseComponent {
      constructor() {
            super();
      }

      _onCreateProperties() {
            super._onCreateProperties();
            this.units = [];
            this.rackDoor = null;
            this.unitArea = null;
            this.rackCover = null;
            this.hilight = false;
            this._useToolTip = true;
            this.unitAreaSize = null;
            this.focusTween = null;
            this._unitSelectHandler = this.onDblClick_ChildItem.bind(this);
      }

      /*
      convertRackDataInfo(data) {
            let objProvider = data.items;
            let i;
            let info;
            let limit = objProvider.length;
            let sorted = [];
            for (i = 0; i < limit; i++) {
                  info = objProvider[i];
                  sorted.push(new UnitVO(info));
            }

            // 들어온 데이터를 row순으로 정렬해 이중 배열로 만듬.
            let startIndex = sorted[0].rowIndex;
            let endIndex = sorted[limit - 1].rowIndex;
            let provider = [];
            for (i = startIndex; i <= endIndex; i++) {
                  let rowIdxArr = sorted.filter((vo) => {
                        return vo.rowIndex == i;
                  });
                  provider.push(rowIdxArr);
            }

            return provider;
      }*/


      set data(data) {
            if (!data) {
                  return;
            }
            data.areaHeight = this.unitAreaSize.y;
            this._data = new RackVO(data);
      }

      get data() {
            return this._data;
      }

      get toolTipTarget() {
            this.rackCover.userData = {owner: this};
            return this.rackCover;
      }


      composeResource(serverRack) {

            let rackSize = ThreeUtil.getObjectSize(serverRack);
            this.serverRack = serverRack;
            this.rackDoor = serverRack.getObjectByName("doorLeft");
            this.unitArea = serverRack.getObjectByName("area");
            this.unitArea.material.visible = false;
            this.rackCover = serverRack.getObjectByName("cover");
            serverRack.getObjectByName("base").material.visible = false;
            this.unitAreaSize = ThreeUtil.getObjectSize(this.unitArea);
            this.unitArea.geometry.computeBoundingBox();

            // element 변경 처리
            this.appendElement.remove(this.element);
            this._element = serverRack;
            this._element.name = this.name + "_rack";
            this.appendElement.add(this._element);

            // 문 위치 조정
            this.rackDoor.geometry.center();
            this.rackDoor.geometry.computeBoundingBox();
            this.rackDoor.geometry.translate(rackSize.x / 2, 0, 0);
            this.rackDoor.position.x = -rackSize.x / 2;
            this.rackDoor.position.y = rackSize.y / 2;
            this.rackDoor.position.z = rackSize.z / 2;

            this._validateElementSize(rackSize);
      }


      // server unit 로드 처리
      async validateUnitDataByType(unitInfo) {
            try {
                  let unitName = this.getResourceFileName(unitInfo.objPath);
                  let objUnit = NLoaderManager.getCloneLoaderPool(unitName);
                  if (objUnit == null) {
                        objUnit = await NLoaderManager.loadObj(this.convertServerPath(unitInfo.objPath));
                        await this.setChildMeshTexture(objUnit, unitInfo.mapPath );
                        NLoaderManager.setLoaderPool(unitName, objUnit);
                  }
            } catch (error) {
                  CPLogger.log(CPLogger.getCallerInfo(this, "serverUnit-Resource" + unitInfo), [error.toString()]);
            }
      }


      getResourceUnits() {
            return this.getGroupPropertyValue("setter", "resource").units;
      }

      async _validateResource() {
            let loadedObj = super._validateResource();
            //사용 유닛 로드 처리
            let unitResource = this.getResourceUnits();
            let typeResources;
            for (let prop in unitResource) {
                  typeResources = unitResource[prop];
                  for (let unitType in typeResources) {
                        await this.validateUnitDataByType(typeResources[unitType]);
                  }

            }
            return loadedObj;
      }

      initDummyData() {
            this.data = {
                  am_asset_id: "AM1803101436",
                  areaHeight: 18.899999916553497,
                  asset_no: "HGD001436",
                  max_unit_qty: 40,
                  "items": [{
                        am_asset_id: "AM1803100473",
                        asset_no: "HTDL30473",
                        columnWeight: 50,
                        lv2_class_id: "HTD00",
                        lv2_class_nm: "통신장비",
                        lv3_class_id: "HTDL3",
                        lv3_class_nm: "L3스위치",
                        pId: "AM1803101441",
                        padding: 30,
                        rack_asset_id: "AM1803101441",
                        use_vertical_yn: "N",
                        unit_end_no: "3",
                        unit_start_no: "2",
                        unit_use_qty: 2,
                        vendor_id: "CISCO",
                        vendor_name: "시스코"
                  },{
                        am_asset_id: "AM1803100474",
                        asset_no: "HTDL30474",
                        columnWeight: 70,
                        lv2_class_id: "HTD00",
                        lv2_class_nm: "통신장비",
                        lv3_class_id: "HTDL3",
                        lv3_class_nm: "L3스위치",
                        pId: "AM1803101441",
                        padding: 10,
                        rack_asset_id: "AM1803101441",
                        use_vertical_yn: "Y",
                        unit_end_no: "9",
                        unit_start_no: "5",
                        unit_use_qty: 4,
                        vendor_id: "CISCO",
                        vendor_name: "시스코"
                  }]
            }
      }


      clearTween() {
            if (this.focusTween != null && this.focusTween.isActive()) {
                  this.focusTween.pause();
                  this.focusTween.kill();
            }
      }

      // provider을 이용해 내부 유닛 구성
      composeUnitData() {
            if (!this.data) {
                  return;
            }
            this.hasUnit = true;
            let cols;
            let units = this.data.units;
            let rows = units.length;
            let colUnit = this.unitAreaSize.x/100;    //가로 스케일 단위
            let rowUnit = this.data.rowHeight;        //세로 스케일 단위
            let i, j, vo, unit;
            let colProvider;
            let rowIndex, rowStart;

            for (i = 0; i < rows; i++) {
                  colProvider = units[i];
                  cols = colProvider.length;
                  for (j = 0; j < cols; j++) {
                        vo = colProvider[j];
                        unit = new UnitBase(NLoaderManager.getCloneLoaderPool(this.getResourceFileName(vo.resourceName)));
                        unit.name = vo.type + "_" + i + "_" + j;
                        unit.skin.name = vo.type + "_" + i + "_" + j;
                        rowIndex = vo.rowIndex - 1;
                        rowStart = this.unitAreaSize.y;
                        if (this.data.reverseRow) {
                              rowIndex = (rowIndex + vo.scaleY);
                              rowStart = this.unitArea.geometry.boundingBox.min.y;
                        }
                        let scaleRateX = (vo.rotateY) ? colUnit/unit.size.y : colUnit/unit.size.x;
                        let scaleRateY = (vo.rotateY) ? rowUnit/unit.size.x : rowUnit/unit.size.y;
                        vo.scale = new THREE.Vector3( scaleRateX*vo.scaleX, scaleRateY*vo.scaleY, 1);
                        vo.position = new THREE.Vector3(
                              -this.unitAreaSize.x/2 + (colUnit * vo.spaceX),
                              rowStart + (rowIndex * rowUnit), 0);
                        unit.data = vo;
                        unit.skin.userData = unit;
                        this.unitArea.add(unit.skin);
                  }
            }
      }

      registUnitEvent() {
            let i;
            let unit;
            for (i = 0; i < this.unitArea.children.length; i++) {
                  unit = this.unitArea.children[i];
                  this.registEventByType(unit, "dblclick", this._unitSelectHandler, false);
            }
      }

      clearUnitEvent() {
            let i;
            let unit;
            for (i = 0; i < this.unitArea.children.length; i++) {
                  unit = this.unitArea.children[i];
                  this.removeEventByType(unit, "dblclick", this._unitSelectHandler, false);
            }
      }

      onDblClick_ChildItem(event) {
            event.stopPropagation();
            let selectUnit = event.target.userData;
            this.dispatchEvent({
                  type: "WVComponentEvent.SELECT_ITEM",
                  target: this,
                  data: {selectItem: selectUnit}
            });

            this.dispatchWScriptEvent("itemDblClick", {target: this, selectItem: selectUnit, type: "itemDblClick"});
      }

      focusSelectUnit(selectUnit) {
            this.clearUnitFocus(selectUnit);
            if (selectUnit != null && !selectUnit.selected) {
                  selectUnit.setFocus();
            } else {
                  this.clearUnitFocus();
            }
      }

      setFocusByColor(value) {
            this.rackCover.material.color.setHex(value);
            this.rackDoor.material.color.setHex(value);
      }

      transitionIn(data) {
            this.clearTween();
            this._useToolTip = false;
            if (!this.hasUnit) {
                  this.composeUnitData();
            }
            this.selected = true;
            this.focusTween = TweenMax.to(this.rackDoor.rotation, ServerRack.FOCUS_DURATION / 2, {
                  y: ServerRack.DOOR_OPEN_ANGLE * Math.PI / 180,
                  onComplete: () => {
                        this.registUnitEvent();
                  }
            });
      }

      transitionOut(data) {
            this._useToolTip = true;
            this.clearUnitEvent();
            this.clearTween();
            this.clearUnitFocus();
            this.selected = false;
            this.focusTween = TweenMax.to(this.rackDoor.rotation, ServerRack.FOCUS_DURATION / 2, {
                  y: 0
            });
      }


      clearUnitFocus(selectUnit) {
            let unitObj;
            for (let i = 0; i < this.unitArea.children.length; i++) {
                  unitObj = this.unitArea.children[i];
                  if (unitObj.userData == selectUnit) continue;
                  if (unitObj.userData.selected) unitObj.userData.clearFocus();
            }
      }

      getUnitById(id) {
            for (let i = 0; i < this.unitArea.children.length; i++) {
                  var unitObj = this.unitArea.children[i];
                  if (unitObj.userData.data.id == id) return unitObj.userData;
            }
            return null;
      }


      removeUnit() {
            let max = this.unitArea.children.length;
            let idx;
            let unitMesh;
            for (idx = 0; idx < max; idx++) {
                  unitMesh = this.unitArea.children[idx];
                  unitMesh.userData.destroy();
            }
      }

      _onDestroy() {
            this.clearTween();
            this.clearUnitEvent();
            this.removeUnit();
            this.rackDoor = null;
            this.unitArea = null;
            this.unitAreaSize = null;
            this.focusTween = null;
            super._onDestroy();
      }

      onLoadPage(){
            if(this.isViewerMode){
                 this.initDummyData();
            }
      }

}


WV3DPropertyManager.attach_default_component_infos(ServerRack, {
      "setter": {
            "size": {x: 1, y: 1, z: 1},
            "assetId": "",
            "popupId": ""
      },

      "label": {
            "label_text": "ServerRack",
            "label_line_size": 10,
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "ServerRack",
            "version": "1.0.0",
      }
});

ServerRack.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-3d"
}, {
      template: "label-3d"
},
      {
            label: "popupId",
            template: "vertical",
            children: [{
                  owner: "setter",
                  name: "popupId",
                  type: "string",
                  label: "popupId",
                  show: true,
                  writable: true,
                  description: "popup id"
            }]
      }
];

ServerRack.DOOR_OPEN_ANGLE = -135;
ServerRack.FOCUS_DURATION = 1;

ServerRack.UNIT_TYPE = {
      SERVER: "server",
      SECURITY: "security",
      STORAGE: "storage",
      NETWORK: "network",
      BACKUP: "backup",
      ETC: "etc"
}
