"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RackVO = function (_AssetVO) {
      _inherits(RackVO, _AssetVO);

      function RackVO(info) {
            _classCallCheck(this, RackVO);

            var _this = _possibleConstructorReturn(this, (RackVO.__proto__ || Object.getPrototypeOf(RackVO)).call(this, info));

            _this.id = info.am_asset_id; // 자산 id
            //this.type         = info.type;                        // 랙 타입
            //this.brand        = info.brand;                       // 제조사
            //this.name         = info.name;                        // 제품명
            _this.rowCount = info.max_unit_qty; // row수
            var direction = info.unitDir || "bt";
            _this.reverseRow = direction == "bt" ? true : false; // row증가 방향
            _this.rowHeight = info.areaHeight / _this.rowCount; // row 높이값 기준
            if (_this.hasUnit) {
                  _this.units = _this.composeData(info.items); // 실장 유닛 정보
            }
            return _this;
      }

      _createClass(RackVO, [{
            key: "composeData",
            value: function composeData(objProvider) {
                  objProvider = objProvider.sort(function (a, b) {
                        return parseInt(a.unit_start_no) - parseInt(b.unit_start_no);
                  });
                  var i = void 0;
                  var info = void 0;
                  var limit = objProvider.length;
                  var sorted = [];
                  for (i = 0; i < limit; i++) {
                        info = objProvider[i];
                        info.pId = this.id;
                        sorted.push(new UnitVO(info));
                  }
                  if (sorted.length == 0) return sorted;
                  // 들어온 데이터를 row순으로 정렬해 이중 배열로 만듬.
                  var startIndex = parseInt(sorted[0].rowIndex);
                  var endIndex = parseInt(sorted[limit - 1].rowIndex);
                  var provider = [];
                  for (i = startIndex; i <= endIndex; i++) {
                        var rowIdxArr = sorted.filter(function (vo) {
                              return vo.rowIndex == i;
                        });
                        if (rowIdxArr.length > 0) {
                              provider.push(rowIdxArr);
                        }
                  }

                  return provider;
            }
      }]);

      return RackVO;
}(AssetVO);
