class RackVO extends AssetVO {

      constructor( info ){
            super(info);
            //this.id           = info.am_asset_id;                   // 자산 id
            //this.type         = info.type;                        // 랙 타입
            //this.brand        = info.brand;                       // 제조사
            //this.name         = info.name;                        // 제품명
            this.rowCount     = info.max_unit_qty;                  // row수
            let direction     = info.unitDir || "bt";
            this.reverseRow   = direction == "bt" ? true : false;   // row증가 방향
            this.rowHeight    = info.areaHeight/this.rowCount;      // row 높이값 기준
            if(this.hasUnit){
                  this.units        = this.composeData( info.items );   // 실장 유닛 정보
            }
      }

      composeData( objProvider ){
            objProvider = objProvider.sort(( a, b )=>{
                  return parseInt(a.unit_start_no) - parseInt(b.unit_start_no);
            });
            let i;
            let info;
            let limit = objProvider.length;
            let sorted =[];
            for( i=0; i<limit; i++ ) {
                  info = objProvider[i];
                  info.pId = this.id;
                  sorted.push(new UnitVO(info));
            }
            if(sorted.length == 0) return sorted;
            // 들어온 데이터를 row순으로 정렬해 이중 배열로 만듬.
            let startIndex    = parseInt(sorted[0].rowIndex);
            let endIndex      = parseInt(sorted[limit-1].rowIndex);
            let provider      = [];
            for( i = startIndex; i<=endIndex; i++ ) {
                  let rowIdxArr = sorted.filter((vo)=>{
                        return vo.rowIndex == i;
                  });
                  if(rowIdxArr.length > 0){
                        provider.push(rowIdxArr);
                  }
            }

            return provider;
      }
}
