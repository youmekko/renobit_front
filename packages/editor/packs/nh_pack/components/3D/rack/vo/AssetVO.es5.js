"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AssetVO = function AssetVO(info) {
      _classCallCheck(this, AssetVO);

      this.id = info.am_asset_id; // 자산 id
      this.type = info.type; // 타입
      this.brand = info.brand; // 제조사
      this.hasUnit = info.items ? true : false; // 실장 유닛
      this.name = info.name; // 제품명
      this.loc_surf_nm = info.loc_surf_nm;
};
