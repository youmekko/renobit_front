"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RackSearchUIComponent = function(_WVDOMComponent) {
    _inherits(RackSearchUIComponent, _WVDOMComponent);

    function RackSearchUIComponent() {
        _classCallCheck(this, RackSearchUIComponent);

        var _this = _possibleConstructorReturn(this, (RackSearchUIComponent.__proto__ || Object.getPrototypeOf(RackSearchUIComponent)).call(this));

        _this.$treeEl;
        _this._currentRackId;
        _this.dummyTreeData = [{ "id": "wv_root", "text": "자산 목록", "parent": "#", "type": "root" }, { "id": "areat-001", "text": "목동", "parent": "wv_root", "type": "group" }, { "id": "ID_METI_KCAR_TIBONER_0001", "text": "Rack01", "parent": "areat-001", "type": "rack" }, { "id": "ID_METI_KCAR_TIBONER_0002", "text": "Rack02", "parent": "areat-001", "type": "rack" }, { "id": "ID_METI_KCAR_TIBONER_0003", "text": "Rack03", "parent": "areat-001", "type": "rack" }, { "id": "ID_METI_KCAR_TIBONER_0004", "text": "Rack04", "parent": "areat-001", "type": "rack" }, { "id": "ID_METI_KCAR_TIBONER_0005", "text": "Rack05", "parent": "areat-001", "type": "rack" }, { "id": "SERVER_0001-01", "text": "Server01-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" }, { "id": "SERVER_0002-01", "text": "Server02-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" }, { "id": "NETWORK_0001-01", "text": "Network01-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" }, { "id": "NETWORK_0002-01", "text": "Network02-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" }, { "id": "Backup_0001-01", "text": "Backup01-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" }, { "id": "SERVER_0001-02", "text": "Server01-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" }, { "id": "SERVER_0002-02", "text": "Server02-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" }, { "id": "NETWORK_0001-02", "text": "Network01-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" }, { "id": "NETWORK_0002-02", "text": "Network02-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" }, { "id": "NETWORK_0003-02", "text": "Network03-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" }, { "id": "SERVER_0001-04", "text": "Server01-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" }, { "id": "STORAGE_0001-04", "text": "Strorage01-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" }, { "id": "STORAGE_0002-04", "text": "Strorage02-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" }, { "id": "NETWORK_0002-04", "text": "Network02-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" }, { "id": "NETWORK_0003-04", "text": "Network03-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" }, { "id": "areat-002", "text": "안산", "parent": "wv_root", "type": "group" }, { "id": "ID_METI_KCAR_TIBONER_0001-01", "text": "Rack01", "parent": "areat-002", "type": "rack" }, { "id": "ID_METI_KCAR_TIBONER_0002-01", "text": "Rack02", "parent": "areat-002", "type": "rack" }, { "id": "ID_METI_KCAR_TIBONER_0003-01", "text": "Rack03", "parent": "areat-002", "type": "rack" }, { "id": "ID_METI_KCAR_TIBONER_0004-01", "text": "Rack04", "parent": "areat-002", "type": "rack" }, { "id": "ID_METI_KCAR_TIBONER_0005-01", "text": "Rack05", "parent": "areat-002", "type": "rack" }, { "id": "SERVER_0001-01-01", "text": "Server01-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" }, { "id": "SERVER_0002-01-01", "text": "Server02-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" }, { "id": "NETWORK_0001-01-01", "text": "Network01-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" }, { "id": "NETWORK_0002-01-01", "text": "Network02-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" }, { "id": "Backup_0001-01-01", "text": "Backup01-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" }, { "id": "SERVER_0001-02-01", "text": "Server01-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" }, { "id": "SERVER_0002-02-01", "text": "Server02-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" }, { "id": "NETWORK_0001-02-01", "text": "Network01-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" }, { "id": "NETWORK_0002-02-01", "text": "Network02-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" }, { "id": "NETWORK_0003-02-01", "text": "Network03-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" }, { "id": "SERVER_0001-04-01", "text": "Server01-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" }, { "id": "STORAGE_0001-04-01", "text": "Strorage01-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" }, { "id": "STORAGE_0002-04-01", "text": "Strorage02-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" }, { "id": "NETWORK_0002-04-01", "text": "Network02-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" }, { "id": "NETWORK_0003-04-01", "text": "Network03-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" }];
        return _this;
    }

    //element 생성


    _createClass(RackSearchUIComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            var $el = $(this._element);
            var str = '<div class="wrap rack-search-ui">' + '<div class="field-wrap">' + '<select>' + '<option>타입선택</option>' + '<option>랙</option>' + '<option>서버</option>' + '<option>네트워크</option>' + '<option>스토리지</option>' + '<option>백업</option>' + '</select>' + '<input type="search">' + '</div>' + '<div class="tree-wrap">' + '<div class="nh-demo-tree">' + '</div>' + '</div>' + '</div>';

            $el.append(str);
            this.$treeEl = $el.find('.nh-demo-tree');
        }
    }, {
        key: "createTree",
        value: function createTree(treeData) {
            //this.getTreeObject().select_node(self.selectedPageID);
            var self = this;
            this.$treeEl.empty().jstree({
                "core": {
                    "animation": 0,
                    "check_callback": true,
                    "themes": {
                        "stripes": true
                    },
                    'data': treeData
                },

                "types": {
                    "#": {
                        "max_children": 1,
                        "max_depth": 15,
                        "valid_children": ["root"]
                    },

                    "root": {
                        "valid_children": ["group", "rack", "item"]
                    },

                    "group": {
                        //"icon": "fa fa-folder-o",
                        "valid_children": ["group", "rack"]
                    },

                    "rack": {
                        "icon": "fa fa-file-o",
                        "valid_children": ["item"]
                    },

                    "item": {
                        "icon": "fa fa-file-o",
                        "valid_children": []
                    }
                },

                "plugins": ["state", "types", "wholerow"]

            }).on("create_node.jstree", function(e, data) {
                alert("create");
            }).on("select_node.jstree", function(e, data) {

                var id = "";
                if (data.node.type == "item") {
                    id = data.node.parent;
                } else if (data.node.type == "rack") {
                    id = data.node.id;
                }

                var oldRackInstance = self.page["_" + self._currentRackId];

                if (self._currentRackId && self._currentRackId != id && oldRackInstance && oldRackInstance.closeRackPopup) {
                    oldRackInstance.rotateAboutPoint();
                }

                if (id.length >= 25) {
                    id = id.slice(0, 25);
                    self._currentRackId = id;
                    var instance = self.page["_" + id];
                    if (instance && instance.rotateAboutPoint) {
                        self._currentInstance = instance;
                        instance.rotateAboutPoint();
                    }
                }
            });

            setTimeout(function() {
                self.$treeEl.jstree(true).open_all();
            }, 500);
        }

        ///화면에 붙였을때

    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.createTree(this.dummyTreeData);
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            //삭제 처리
            _get(RackSearchUIComponent.prototype.__proto__ || Object.getPrototypeOf(RackSearchUIComponent.prototype), "_onDestroy", this).call(this);
        }
    }]);

    return RackSearchUIComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(RackSearchUIComponent, {
    "info": {
        "componentName": "RackSearchUIComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 500,
        "dataProvider": {}
    },

    "style": {
        "backgroundColor": "#ffffff"
    },

    "label": {
        "label_using": "N",
        "label_text": "RackSearchUI Component"
    }
});

RackSearchUIComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "background"
}, {
    template: "label"
}];

WVPropertyManager.add_event(RackSearchUIComponent, {
    name: "select",
    label: "값 선택 이벤트",
    description: "값 선택 이벤트 입니다.",
    properties: [{
        name: "id",
        type: "string",
        default: "",
        description: "선택 ID 값입니다."
    }]
});
