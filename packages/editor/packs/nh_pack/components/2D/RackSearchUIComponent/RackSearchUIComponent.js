class RackSearchUIComponent extends WVDOMComponent {

    constructor() {
        super();
        this.$treeEl;
        this._currentRackId;
        this.dummyTreeData = [
            { "id": "wv_root", "text": "자산 목록", "parent": "#", "type": "root" },
            { "id": "areat-001", "text": "목동", "parent": "wv_root", "type": "group" },
            { "id": "ID_METI_KCAR_TIBONER_0001", "text": "Rack01", "parent": "areat-001", "type": "rack" },
            { "id": "ID_METI_KCAR_TIBONER_0002", "text": "Rack02", "parent": "areat-001", "type": "rack" },
            { "id": "ID_METI_KCAR_TIBONER_0003", "text": "Rack03", "parent": "areat-001", "type": "rack" },
            { "id": "ID_METI_KCAR_TIBONER_0004", "text": "Rack04", "parent": "areat-001", "type": "rack" },
            { "id": "ID_METI_KCAR_TIBONER_0005", "text": "Rack05", "parent": "areat-001", "type": "rack" },

            { "id": "SERVER_0001-01", "text": "Server01-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" },
            { "id": "SERVER_0002-01", "text": "Server02-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" },
            { "id": "NETWORK_0001-01", "text": "Network01-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" },
            { "id": "NETWORK_0002-01", "text": "Network02-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" },
            { "id": "Backup_0001-01", "text": "Backup01-01", "parent": "ID_METI_KCAR_TIBONER_0001", "type": "item" },

            { "id": "SERVER_0001-02", "text": "Server01-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" },
            { "id": "SERVER_0002-02", "text": "Server02-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" },
            { "id": "NETWORK_0001-02", "text": "Network01-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" },
            { "id": "NETWORK_0002-02", "text": "Network02-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" },
            { "id": "NETWORK_0003-02", "text": "Network03-02", "parent": "ID_METI_KCAR_TIBONER_0002", "type": "item" },


            { "id": "SERVER_0001-04", "text": "Server01-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" },
            { "id": "STORAGE_0001-04", "text": "Strorage01-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" },
            { "id": "STORAGE_0002-04", "text": "Strorage02-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" },
            { "id": "NETWORK_0002-04", "text": "Network02-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" },
            { "id": "NETWORK_0003-04", "text": "Network03-04", "parent": "ID_METI_KCAR_TIBONER_0004", "type": "item" },


            { "id": "areat-002", "text": "안산", "parent": "wv_root", "type": "group" },
            { "id": "ID_METI_KCAR_TIBONER_0001-01", "text": "Rack01", "parent": "areat-002", "type": "rack" },
            { "id": "ID_METI_KCAR_TIBONER_0002-01", "text": "Rack02", "parent": "areat-002", "type": "rack" },
            { "id": "ID_METI_KCAR_TIBONER_0003-01", "text": "Rack03", "parent": "areat-002", "type": "rack" },
            { "id": "ID_METI_KCAR_TIBONER_0004-01", "text": "Rack04", "parent": "areat-002", "type": "rack" },
            { "id": "ID_METI_KCAR_TIBONER_0005-01", "text": "Rack05", "parent": "areat-002", "type": "rack" },

            { "id": "SERVER_0001-01-01", "text": "Server01-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" },
            { "id": "SERVER_0002-01-01", "text": "Server02-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" },
            { "id": "NETWORK_0001-01-01", "text": "Network01-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" },
            { "id": "NETWORK_0002-01-01", "text": "Network02-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" },
            { "id": "Backup_0001-01-01", "text": "Backup01-01", "parent": "ID_METI_KCAR_TIBONER_0001-01", "type": "item" },

            { "id": "SERVER_0001-02-01", "text": "Server01-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" },
            { "id": "SERVER_0002-02-01", "text": "Server02-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" },
            { "id": "NETWORK_0001-02-01", "text": "Network01-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" },
            { "id": "NETWORK_0002-02-01", "text": "Network02-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" },
            { "id": "NETWORK_0003-02-01", "text": "Network03-02", "parent": "ID_METI_KCAR_TIBONER_0002-01", "type": "item" },


            { "id": "SERVER_0001-04-01", "text": "Server01-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" },
            { "id": "STORAGE_0001-04-01", "text": "Strorage01-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" },
            { "id": "STORAGE_0002-04-01", "text": "Strorage02-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" },
            { "id": "NETWORK_0002-04-01", "text": "Network02-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" },
            { "id": "NETWORK_0003-04-01", "text": "Network03-04", "parent": "ID_METI_KCAR_TIBONER_0004-01", "type": "item" }

        ];
    }

    //element 생성
    _onCreateElement() {
        let $el = $(this._element);
        let str =
            '<div class="wrap rack-search-ui">' +
            '<div class="field-wrap">' +
            '<select>' +
            '<option>타입선택</option>' +
            '<option>랙</option>' +
            '<option>서버</option>' +
            '<option>네트워크</option>' +
            '<option>스토리지</option>' +
            '<option>백업</option>' +
            '</select>' +
            '<input type="search">' +
            '</div>' +
            '<div class="tree-wrap">' +
            '<div class="nh-demo-tree">' +
            '</div>' +
            '</div>' +
            '</div>';

        $el.append(str);
        this.$treeEl = $el.find('.nh-demo-tree');

    }

    createTree(treeData) {
        //this.getTreeObject().select_node(self.selectedPageID);
        let self = this;
        this.$treeEl.empty().jstree({
            "core": {
                "animation": 0,
                "check_callback": true,
                "themes": {
                    "stripes": true
                },
                'data': treeData,
            },

            "types": {
                "#": {
                    "max_children": 1,
                    "max_depth": 15,
                    "valid_children": ["root"]
                },

                "root": {
                    "valid_children": ["group", "rack", "item"]
                },

                "group": {
                    //"icon": "fa fa-folder-o",
                    "valid_children": ["group", "rack"]
                },

                "rack": {
                    "icon": "fa fa-file-o",
                    "valid_children": ["item"]
                },

                "item": {
                    "icon": "fa fa-file-o",
                    "valid_children": []
                }
            },

            "plugins": [
                "state", "types", "wholerow"
            ]

        }).on("create_node.jstree", function(e, data) {
            alert("create");
        }).on("select_node.jstree", function(e, data) {

            var id = "";
            if (data.node.type == "item") {
                id = data.node.parent;
            } else if (data.node.type == "rack") {
                id = data.node.id;
            }

            var oldRackInstance = self.page["_" + self._currentRackId];
            if (self._currentRackId && self._currentRackId != id && oldRackInstance && oldRackInstance.closeRackPopup) {
                oldRackInstance.closeRackPopup();
            }

            if (id.length >= 25) {
                id = id.slice(0, 25);
                self._currentRackId = id;
                var instance = self.page["_" + id];
                if (instance && instance.rotateAboutPoint) {
                    self._currentInstance = instance;
                    instance.rotateAboutPoint();
                }
            }


        })

        setTimeout(() => {
            self.$treeEl.jstree(true).open_all();
        }, 500);

    }


    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {
        this.createTree(this.dummyTreeData);


    }

    _onDestroy() {
        //삭제 처리
        super._onDestroy();
    }
}

WVPropertyManager.attach_default_component_infos(RackSearchUIComponent, {
    "info": {
        "componentName": "RackSearchUIComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 500,
        "dataProvider": {}
    },

    "style": {
        "backgroundColor": "#ffffff"
    },

    "label": {
        "label_using": "N",
        "label_text": "RackSearchUI Component"
    }
});

RackSearchUIComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "background"
}, {
    template: "label"
}]


WVPropertyManager.add_event(RackSearchUIComponent, {
    name: "select",
    label: "값 선택 이벤트",
    description: "값 선택 이벤트 입니다.",
    properties: [{
        name: "id",
        type: "string",
        default: "",
        description: "선택 ID 값입니다."
    }]
});
