class RackPreviewComponent extends WVDOMComponent {

    constructor() {
        super();
        let rootPath = "static/components/2D/project/nh/RackPreviewComponent/img/";
        this.venderImgPath = [{
            name: "cisco",
            path: rootPath + "logo-cisco.png"
        }, {
            name: "dell",
            path: rootPath + "logo-dell.png"
        }, {
            name: "hp",
            path: rootPath + "logo-hp.png"
        }, {
            name: "ibm",
            path: rootPath + "logo-ibm.png"
        }, {
            name: "sun",
            path: rootPath + "logo-sun.png"
        }];

        this._dataProvider = {};

        this.unitWidth;
        this.unitHeight;
        this.unitLen;
        this.unitDir;
        this.items;
        this.$itemWrap;
        this.$rackWarp;
        this.$itemsEl;
        this.activeId;
    }

    //element 생성
    _onCreateElement() {
        let $el = $(this._element);
        $el.attr("data-comp-name", "rack-preview");
        $el.append('<div class="rack-warp"><ul></ul></div>');
        this.$rackWarp = $el.find(".rack-warp");
    }

    init() {
        this.activeId = "";
        this.$rackWarp.find("ul").empty();
        this.$rackWarp.off();
        //unbindEvent
        this.unitLen = this.dataProvider.rowCount;
        this.items = this.dataProvider.units;
        this.occupyIndex = new Map();
        this.setUnitRow(this.unitLen);
        this.$itemWrap = this.$rackWarp.find(".item-wrap");
        this.setUnitSize();

        // 실장 장비의 padding, columnIndex 계산
        //    this.calcOccupiedIndex();
        //    this.setMountData();
        this.drawItems();
        this.$itemsEl = this.$rackWarp.find(".item .cont");
        this.bindEvent();
    }


    bindEvent() {
        let self = this;
        this.$rackWarp.on("click", ".item-wrap .item", function(e) {
            let $cont = $(this).find(".cont");
            let $target = self.$itemsEl.filter($cont);
            if ($target.length) {
                self.focus($target.attr("data-id"));
            } else {
                self.blur();
            }
        })
    }

    focusMountedDevice(id) {
        if (this.activeId == id || id == undefined) {
            this.activeId = "";
            this.$itemsEl.removeClass("active");
            return;
        }
        this.activeId = id;
        this.$itemsEl.removeClass("active");
        this.$itemsEl.filter("[data-id='" + id + "']").addClass("active");
    }

    focus(id) {
        //      this.focusMountedDevice(id);

        var hasActive = this.activeId == "" ? false : true;
        this.dispatchWScriptEvent("focus", {
            id: id,
            hasActive: hasActive
        })
    }

    blur() {
        if (this.activeId == "") {
            return;
        }
        this.activeId = "";
        this.$itemsEl.removeClass("active");

        this.dispatchWScriptEvent("blur");
    }


    setUnitRow(len) {
        let ary = [];
        for (let i = 0; i < len; i++) {
            let unitNo = i + 1;
            let str = '<li><span>' + unitNo + '</span><div class="item-wrap" data-unit-no="' + unitNo + '"></div></li>';
            ary.unshift(str);
            this.occupyIndex.set(unitNo, 0);
        }

        let domStr = ary.join('');
        this.$rackWarp.find("ul").append(domStr);
    }

    setUnitSize() {
        let unitEl = this.$itemWrap.eq(0);
        this.unitWidth = unitEl.width();
        this.unitHeight = unitEl.outerHeight();
    }

    drawItems() {
        for (let i = 0, len = this.items.length; i < len; i++) {
            let indexItems = this.items[i];

            for (let j = 0; j < indexItems.length; j++) {
                this.appendItem(indexItems[j]);
            }
        }
    }

    appendItem(itemData) {
        var index = parseInt(itemData.rowIndex) + itemData.scaleY - 1;
        var $wrap = this.$itemWrap.filter("[data-unit-no=" + index + "]");
        var wid = this.unitWidth / 100 * itemData.scaleX;
        var hei = this.unitHeight * itemData.scaleY;
        var left = this.unitWidth / 100 * itemData.spaceX;
        var lineHeight = hei;

        var str = '<div class="item tooltip" style="height:' + hei + 'px;width:' + wid + 'px;left:' + left + 'px;">';

        if (this.unitWidth / 2 > wid && wid < hei && wid > this.unitHeight) {
            //3분할 이상, height가 더 길다면..
            lineHeight = wid;
            var originSize = wid / 2;
            str += '<div style="height:' + wid + 'px;width:' + hei + 'px; line-height:' + lineHeight + 'px;  transform-origin: ' + originSize + 'px; transform: rotate(90deg);" class="cont" data-id="' + itemData.id + '" ><em>';
        } else {
            str += '<div style="line-height:' + lineHeight + 'px;" class="cont ' + itemData.type + '" data-id="' + itemData.id + '" ><em>';
        }

        /*let img = this._getVenderImgEl(itemData.vender);*/
        var label = itemData.vender != null && itemData.vender.length > 0 ? '( ' + itemData.vender + ' ) ' + itemData.name : itemData.name;
        str += label +
            /*img +*/
            '</em></div>' + '<span class="tooltiptext"><span>' + label + '</span></span>' + '<div>';

        $wrap.append(str);
        $wrap.find(".tooltiptext").each(function() {
            let $target = $(this);
            let $cont = $target.find("span");
            $target.css("display", "block");
            let wid = $cont.outerWidth();
            let left = Math.round(wid / 2) * -1;
            $cont.css("left", left);
            $target.css("display", '');
        });


        itemData.typeColor = this.getGroupPropertyValue("bgColors", itemData.type);
    }

    _getVenderImgEl(name) {

        let data = this.venderImgPath.find(x => {
            if (!name) { return null; }
            return x.name == name.toLowerCase();
        });
        if (data) {
            return '<img class="vender-logo" src="' + data.path + '">';
        } else {
            return '';
        }
    }

    setBackgroundStyle() {
        var bgColors = this.getGroupProperties("bgColors");

        var style = $('<style>' +
            '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.server { background: ' + bgColors.server + ' } ' +
            '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.network { background: ' + bgColors.network + ' } ' +
            '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.storage { background: ' + bgColors.storage + ' } ' +
            '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.backup { background: ' + bgColors.backup + ' } ' +
            '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.security { background: ' + bgColors.security + ' } ' +
            '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.etc { background: ' + bgColors.etc + ' } ' +
            '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.active { border: 3px solid ' + bgColors.active + ' } ' +
            '</style>');

        $(this._element).append(style);
    }


    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {
        this.setBackgroundStyle();
    }

    _onDestroy() {
        //삭제 처리
        this.$rackWarp.find("ul").empty();
        this.$rackWarp.off();

        this.occupyIndex = null;
        super._onDestroy();
    }


    set dataProvider(data) {
        let oldValue = this.dataProvider;
        this._dataProvider = data;

        if (!this.isEditorMode && data != oldValue) {
            this.init();
            this.dispatchWScriptEvent("change", {
                value: data
            })
        }
    }

    get dataProvider() {
        if (this.isEditorMode) {
            return {};
        }

        return this._dataProvider;
    }

}

WVPropertyManager.attach_default_component_infos(RackPreviewComponent, {
    "info": {
        "componentName": "RackPreviewComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 210,
        "height": 715,
        "dataProvider": {}
    },

    "label": {
        "label_using": "N",
        "label_text": "RackPreview Component"
    },

    "bgColors": {
        "server": "#4e70b5",
        "network": "#414fa2",
        "storage": "#695b80",
        "backup": "#128997",
        "security": "#306b6e",
        "etc": "#6e6e6e",
        "active": "#3cd3ff"
    }
});

RackPreviewComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Background Color",
    owner: "vertical",
    children: [{
        owner: "bgColors",
        name: "server",
        type: "color",
        label: "server",
        show: true,
        writable: true,
        description: "server"
    }, {
        owner: "bgColors",
        name: "network",
        type: "color",
        label: "network",
        show: true,
        writable: true,
        description: "network"
    }, {
        owner: "bgColors",
        name: "storage",
        type: "color",
        label: "storage",
        show: true,
        writable: true,
        description: "storage"
    }, {
        owner: "bgColors",
        name: "backup",
        type: "color",
        label: "backup",
        show: true,
        writable: true,
        description: "backup"
    }, {
        owner: "bgColors",
        name: "security",
        type: "color",
        label: "security",
        show: true,
        writable: true,
        description: "security"
    }, {
        owner: "bgColors",
        name: "etc",
        type: "color",
        label: "etc",
        show: true,
        writable: true,
        description: "etc"
    }, {
        owner: "bgColors",
        name: "active",
        type: "color",
        label: "active",
        show: true,
        writable: true,
        description: "active"
    }]
}]


WVPropertyManager.add_event(RackPreviewComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_event(RackPreviewComponent, {
    name: "focus",
    label: "focus 이벤트",
    description: "focus 이벤트 입니다.",
    properties: [{
        name: "id",
        type: "string",
        default: "",
        description: "focus된 item id 정보입니다."
    }]
});

WVPropertyManager.add_event(RackPreviewComponent, {
    name: "blur",
    label: "focus 이벤트",
    description: "focus 이벤트 입니다."
});
