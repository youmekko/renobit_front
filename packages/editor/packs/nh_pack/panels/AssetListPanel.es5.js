"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AssetListPanel = function (_ExtensionPanelCore) {
      _inherits(AssetListPanel, _ExtensionPanelCore);

      function AssetListPanel() {
            _classCallCheck(this, AssetListPanel);

            var _this = _possibleConstructorReturn(this, (AssetListPanel.__proto__ || Object.getPrototypeOf(AssetListPanel)).call(this));

            _this._assetList = [];
            _this._app = null;
            _this._openPageState = false;
            _this._loadingAssetState = false;
            _this._loadingPageIdList = false;

            _this._assetLoader = AssetLoadManager.getInstance();

            console.log("AssetListPanel 생성");
            return _this;
      }

      /*
      call :
      인스턴스 생성 후 실행
      단, 아직 화면에 붙지 않은 상태임.
      */


      _createClass(AssetListPanel, [{
            key: "create",
            value: function create() {

                  var template = "\n            <div style=\"overflow: auto;\">\n                  <div style=\"display: flex;padding: 6px;\">\n                        <el-select :disabled=\"assetPageId.length!=0\" v-model=\"tempAssetPageId\" placeholder=\"Select\" size=\"small\" style=\"flex: 1;margin-right: 4px;\">\n                              <el-option\n                                    v-for=\"item in assetPageIdList\"\n                                    :key=\"item.LOCATION_ID\"\n                                    :label=\"item.LOCATION_NM\"\n                                    :value=\"item.LOCATION_ID\">\n                              </el-option>\n                        </el-select>\n                        <el-button size=\"small\" type=\"primary\" v-if=\"assetPageId.length==0\" @click=\"on_updatePageId\">\uC801\uC6A9</el-button>\n                  </div>\n                  <div>\n                        <button @click=\"on_cancelPageId\">\uCDE8\uC18C</button>\n                  </div>\n                  \n                  <template v-if=\"active==true && activeLayerName=='threeLayer'\">                  \n                        <el-tree\n                        ref=\"assetTree\"\n                        :data=\"groupList\"\n                        highlight-current\n                              :props=\"defaultProps\"\n                              default-expand-all\n                              node-key=\"id\"\n                              @node-click=\"onSelectedGroup($event)\"\n                        >\n                        </el-tree>\n                        \n                        <hr>\n                        <div v-if=\"selectedExtensionListInGroup.length>0\" class=\"component-txt-list\">     \n                              <div class=\"search-field\">\n                                    <el-select v-model=\"assetsListFilter\" placeholder=\"Select\" size=\"small\"> \n                                          <el-option\n                                                v-for=\"item in assetsListFilterOptions\"\n                                                :key=\"item.value\"\n                                                :label=\"item.label\" \n                                                :value=\"item.value\">\n                                          </el-option>\n                                    </el-select>\n                                    <el-input placeholder=\"search\" v-model=\"assetsSearchStr\" size=\"small\" @keyup.enter.native=\"on_searchAssetItems\">\n                                          <i slot=\"prefix\" class=\"el-input__icon el-icon-search\"></i>\n                                    </el-input>\n                              </div>                               \n                              <ul class=\"list\">\n                                    <!--\uC0AC\uC6A9\uD55C \uD56D\uBAA9\uC740 li.disabled-->\n                                    <li class=\"component\" :class=\"{disabled:item.arrangement}\" v-for=\"item in selectedExtensionListInGroup\" @dblclick=\"\" v-if=\"filterAssetItem(item)\">\n                                          <div class=\"txt-wrap\"  @dragstart=\"onDragStartThreeComponent($event, item)\" :draggable=\"item.arrangement==false\" >\n                                                <span>{{item.label}}</span>\n                                          </div>\n                                    </li>\n                              </ul> \n                        </div>\n                  </template>\n                   \n                  <div v-if=\"active==false\" style=\"text-align: center; padding: 20px;\">\n                        \uB370\uC774\uD130\uAC00 \uC124\uC815\uB418\uC9C0 \uC54A\uC558\uC2B5\uB2C8\uB2E4.\n                  </div>\n                  <div v-if=\"active==true && activeLayerName!='threeLayer' \" style=\"text-align: center; padding: 20px;\">\n                        \uC790\uC0B0 \uC815\uBCF4\uB294 3D\uC5D0\uC11C\uB9CC \uC0AC\uC6A9\uD560 \uC218 \uC788\uC2B5\uB2C8\uB2E4.\n                  </div>\n            </div>\n      ";
                  this._$element = $(template);

                  if (this._$parentView == null) {
                        console.log("Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.");
                        return;
                  }

                  /*
                  중요:
                  Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.
                   */
                  this._$parentView.append(this._$element);

                  // 트리 목록에서 그룹 목록만 구해오기
                  this._app = new Vue({
                        el: this._$element[0],
                        data: {
                              assetPageId: "",
                              tempAssetPageId: "",
                              assetPageIdList: [],
                              ownerPanel: this,
                              groupList: [],
                              defaultProps: {
                                    children: 'children',
                                    label: 'label'
                              },
                              selectedExtensionListInGroup: {},
                              active: false,
                              activeLayerName: "",

                              assetsListFilterOptions: [{
                                    value: 'all',
                                    label: '전체'
                              }, {
                                    value: 'used',
                                    label: '배치'
                              }, {
                                    value: 'unused',
                                    label: '미배치'
                              }],

                              assetsListFilter: 'all',
                              assetsSearchStr: '',
                              message: ""

                        },

                        methods: {
                              selectedFirstGroup: function selectedFirstGroup() {
                                    try {
                                          if (this.groupList[0].children && this.groupList[0].children.length > 0) {
                                                this.selectedExtensionListInGroup = this.groupList[0].children[0].model.children;
                                                //this.$refs.assetTree.setCheckedNodes([this.groupList[0].children[0].model]);
                                          } else this.selectedExtensionListInGroup = [];
                                    } catch (error) {
                                          console.log("selectedFirstGroup Error ", error);

                                          this.selectedExtensionListInGroup = null;
                                    }
                              },

                              onSelectedGroup: function onSelectedGroup(tree) {
                                    if (tree && tree.model) {
                                          this.assetsSearchStr = "";
                                          this.selectedGroupModel = tree.model;
                                          this.selectedExtensionListInGroup = tree.model.children;

                                          // 그룹 아이템에 남아 있는 필터 정보를 제거하기(show=true)
                                          this.clearSearchAssetItems();
                                    }
                              },

                              onDragStartThreeComponent: function onDragStartThreeComponent(event, item) {
                                    var newItem = Object.assign(item, { category: "3D" });
                                    event.dataTransfer.effectAllowed = 'move';

                                    if (CPUtil.getInstance().browserDetection() != "Internet Explorer") {
                                          event.dataTransfer.setData('text/plane', JSON.stringify(item));
                                    }
                                    window["dragData"] = JSON.stringify(item);
                              },
                              setActive: function setActive(active) {
                                    this.active = active;
                              },
                              setActiveLayerName: function setActiveLayerName(layerName) {
                                    this.activeLayerName = layerName;
                              },
                              on_updatePageId: function on_updatePageId(value) {
                                    if (this.tempAssetPageId.length > 0) {

                                          this.assetPageId = this.tempAssetPageId;
                                          window.wemb.mainPageComponent.setDynamicGroupPropertyValue("extension", "asset_page_id", this.assetPageId);
                                          // 페이지 정보가 변경되면
                                          this.ownerPanel._loadAssetListInPageId(this.assetPageId);
                                    }
                              },
                              on_cancelPageId: function on_cancelPageId() {
                                    this.assetPageId = "";
                                    window.wemb.mainPageComponent.setDynamicGroupPropertyValue("extension", "asset_page_id", this.assetPageId);
                                    delete window.wemb.mainPageComponent.properties.extension.asset_page_id;
                              },


                              // 검색 처리 (enter 키 입력시)
                              on_searchAssetItems: function on_searchAssetItems(obj) {

                                    if (this.selectedExtensionListInGroup) {
                                          var _iteratorNormalCompletion = true;
                                          var _didIteratorError = false;
                                          var _iteratorError = undefined;

                                          try {
                                                for (var _iterator = this.selectedExtensionListInGroup[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                                                      var assetItem = _step.value;


                                                      if (assetItem.label.indexOf(this.assetsSearchStr) == -1) {
                                                            assetItem.show = false;
                                                      } else {
                                                            assetItem.show = true;
                                                      }
                                                }
                                          } catch (err) {
                                                _didIteratorError = true;
                                                _iteratorError = err;
                                          } finally {
                                                try {
                                                      if (!_iteratorNormalCompletion && _iterator.return) {
                                                            _iterator.return();
                                                      }
                                                } finally {
                                                      if (_didIteratorError) {
                                                            throw _iteratorError;
                                                      }
                                                }
                                          }
                                    }
                              },


                              // 검색 처리 취소
                              clearSearchAssetItems: function clearSearchAssetItems() {
                                    if (this.selectedExtensionListInGroup) {
                                          var _iteratorNormalCompletion2 = true;
                                          var _didIteratorError2 = false;
                                          var _iteratorError2 = undefined;

                                          try {
                                                for (var _iterator2 = this.selectedExtensionListInGroup[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                                      var assetItem = _step2.value;

                                                      assetItem.show = true;
                                                }
                                          } catch (err) {
                                                _didIteratorError2 = true;
                                                _iteratorError2 = err;
                                          } finally {
                                                try {
                                                      if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                                            _iterator2.return();
                                                      }
                                                } finally {
                                                      if (_didIteratorError2) {
                                                            throw _iteratorError2;
                                                      }
                                                }
                                          }
                                    }
                              },
                              filterAssetItem: function filterAssetItem(item) {

                                    if (this.assetsListFilter == "all" && item.show == true) return true;

                                    if (this.assetsListFilter == "used" && item.arrangement == true && item.show == true) {
                                          return true;
                                    }

                                    if (this.assetsListFilter == "unused" && item.arrangement == false && item.show == true) {
                                          return true;
                                    }

                                    return false;
                              }
                        }
                  });

                  return this._app;
            }
      }, {
            key: "handleNotification",


            /*
            noti가 온 경우 실행
            call : mediator에서 실행
             */
            value: function handleNotification(note) {
                  switch (note.name) {

                        /*
                        case EditorProxy.NOTI_UPDATE_EDITOR_STATE :
                              this.noti_updateEditorState(note.get());
                              break;
                        */
                        case EditorProxy.NOTI_OPEN_PAGE:
                              this.noti_openPage();
                              break;

                        case EditorProxy.NOTI_CLOSED_PAGE:
                              this.noti_closedPage();
                              break;

                        case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER:
                              this.noti_changeActiveLayer(note.getBody());
                              break;

                        case EditorProxy.NOTI_ADD_COM_INSTANCE:
                              this.noti_addComInstance(note.getBody());
                              break;

                        case EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE:
                              this.noti_beforeRemoveComInstance(note.getBody());
                              break;
                  }
            }

            /*
            페이지가 열릴 때 자산 정보가 로드되어 있지 않으면 자산 정보 로드하기.
              1. 페이지 ID 목록  읽기
             2. 현재 페이지 정보에서 assset_page_id 구하기
                만약 설정되어 있지 않다면 설정 화면 만들기
                만약 설정되어 있다면 해당 id에 대한 어셋 정보만 출력하기
                 */

      }, {
            key: "noti_openPage",
            value: function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        var success;
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                this._openPageState = true;
                                                this._app.setActive(false);

                                                if (!(this._loadingPageIdList == false)) {
                                                      _context.next = 17;
                                                      break;
                                                }

                                                _context.next = 5;
                                                return this._assetLoader.loadCustomComponentInfoList();

                                          case 5:
                                                success = _context.sent;

                                                if (!(success == false)) {
                                                      _context.next = 9;
                                                      break;
                                                }

                                                this._app.message = "서버에서 자산 컴포넌트 정보를 읽을 수 없습니다.";
                                                return _context.abrupt("return", false);

                                          case 9:
                                                _context.next = 11;
                                                return this._assetLoader.loadAssetPageIdList();

                                          case 11:
                                                success = _context.sent;

                                                if (!(success == false)) {
                                                      _context.next = 15;
                                                      break;
                                                }

                                                this._app.message = "서버에서 자산 페이지를 읽을 수 없습니다.";
                                                return _context.abrupt("return", false);

                                          case 15:

                                                this._loadingPageIdList = true;
                                                this._app.assetPageIdList = this._assetLoader.assetPageIdList;

                                          case 17:
                                                if (!(this._checkAndUpdateAssetPageIdProperty() == false)) {
                                                      _context.next = 20;
                                                      break;
                                                }

                                                this._app.message = "배치할 페이지 정보가 설정되지 않았습니다. 설정 후 실행해주세요.";
                                                return _context.abrupt("return", false);

                                          case 20:

                                                // 어셋 정보 구하기
                                                this._app.tempAssetPageId = this._app.assetPageId;
                                                this._loadAssetListInPageId(this._app.assetPageId);

                                          case 22:
                                          case "end":
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this);
                  }));

                  function noti_openPage() {
                        return _ref.apply(this, arguments);
                  }

                  return noti_openPage;
            }()
      }, {
            key: "noti_closedPage",
            value: function noti_closedPage() {
                  this._openPageState = false;
            }
      }, {
            key: "noti_changeActiveLayer",
            value: function noti_changeActiveLayer(layerName) {
                  this._app.setActiveLayerName(layerName);
                  //this._app.setActive(layerName=="threeLayer");
            }
      }, {
            key: "noti_addComInstance",
            value: function noti_addComInstance(instance) {
                  if (this._openPageState == false) return;

                  if (instance) {
                        this._removeTreeGroupItem(instance.assetId);
                  }
            }
      }, {
            key: "noti_beforeRemoveComInstance",
            value: function noti_beforeRemoveComInstance(instance) {
                  this._addTreeGroupItem(instance.assetId);
            }

            ////////////////////////////


            /*
            페이지 id에 해당하는 어셋 구하기
             */

      }, {
            key: "_loadAssetListInPageId",
            value: function () {
                  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(assetPageId) {
                        var _this2 = this;

                        var result;
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                              while (1) {
                                    switch (_context2.prev = _context2.next) {
                                          case 0:
                                                _context2.next = 2;
                                                return this._assetLoader.loadAssetList(assetPageId);

                                          case 2:
                                                result = _context2.sent;

                                                this._loadingAssetState = result;

                                                if (!(result == false)) {
                                                      _context2.next = 7;
                                                      break;
                                                }

                                                this._app.message = "자산 정보를 구하는 도중에 에러 발생";
                                                return _context2.abrupt("return", false);

                                          case 7:

                                                // 그룹 리스트 정보를 vue.app에 전달.
                                                this._app.groupList = this._assetLoader.treeGroupList;

                                                //2. 첫 번재 그룹이 선택되게 만든다.
                                                this._app.selectedFirstGroup();

                                                /*
                                                   3. 페이지 열고 난 후 이미 배치되어 있는 인스턴스 삭제
                                                 */
                                                try {
                                                      window.wemb.editorProxy.comInstanceList.forEach(function (instance) {
                                                            _this2._removeTreeGroupItem(instance.assetId);
                                                      });
                                                } catch (error) {}

                                                this._app.setActive(true);

                                          case 11:
                                          case "end":
                                                return _context2.stop();
                                    }
                              }
                        }, _callee2, this);
                  }));

                  function _loadAssetListInPageId(_x) {
                        return _ref2.apply(this, arguments);
                  }

                  return _loadAssetListInPageId;
            }()

            /*
                  페이지 정보에서 asset_page_id가 있는지 검사 후
                  있다면
                        asset_page_id 정보를 _app.assetPageId에 설정하기
             */

      }, {
            key: "_checkAndUpdateAssetPageIdProperty",
            value: function _checkAndUpdateAssetPageIdProperty() {
                  var pageInfo = window.wemb.mainPageComponent.properties;

                  if (pageInfo.hasOwnProperty("extension")) {
                        if (pageInfo.extension.hasOwnProperty("asset_page_id") == true && pageInfo.extension.asset_page_id.length > 0) {
                              this._app.assetPageId = pageInfo.extension.asset_page_id;
                              return true;
                        }
                  }

                  return false;
            }

            /**
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             자산 추가 -> 삭제 처리
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             * */

      }, {
            key: "_removeTreeGroupItem",
            value: function _removeTreeGroupItem(assetId) {
                  var _this3 = this;

                  if (this._app.groupList.length <= 0) return;

                  if (this._app.groupList[0].children.length <= 0) {
                        return;
                  }

                  // 트리정보에서 asetId 항목 삭제
                  this._app.groupList[0].children.forEach(function (groupItem) {
                        if (_this3._findAndRemoveTreeGroupItem(groupItem, assetId)) {
                              return;
                        }
                  });
            }
      }, {
            key: "_findAndRemoveTreeGroupItem",
            value: function _findAndRemoveTreeGroupItem(group, assetId) {
                  if (group.hasOwnProperty("model") == true) {
                        if (group.model.children.length > 0) {

                              var index = 0;
                              var _iteratorNormalCompletion3 = true;
                              var _didIteratorError3 = false;
                              var _iteratorError3 = undefined;

                              try {
                                    for (var _iterator3 = group.model.children[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                                          var assetItem = _step3.value;

                                          if (assetItem.asset_id == assetId) {
                                                assetItem.arrangement = true;
                                                return true;
                                          }

                                          index++;
                                    }
                              } catch (err) {
                                    _didIteratorError3 = true;
                                    _iteratorError3 = err;
                              } finally {
                                    try {
                                          if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                                _iterator3.return();
                                          }
                                    } finally {
                                          if (_didIteratorError3) {
                                                throw _iteratorError3;
                                          }
                                    }
                              }
                        }
                  }

                  return false;
            }

            // assetType에 해당하는 자산 정보 구하기(즉, 자산 그룹 구하기)

      }, {
            key: "_findGroupItem",
            value: function _findGroupItem(assetType) {
                  if (this._app.groupList.length <= 0) return null;

                  if (this._app.groupList[0].children.length <= 0) {
                        return null;
                  }

                  try {
                        var _iteratorNormalCompletion4 = true;
                        var _didIteratorError4 = false;
                        var _iteratorError4 = undefined;

                        try {
                              for (var _iterator4 = this._app.groupList[0].children[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                                    var groupItem = _step4.value;

                                    if (groupItem.model.asset_type == assetType) {
                                          return groupItem;
                                    }
                              }
                        } catch (err) {
                              _didIteratorError4 = true;
                              _iteratorError4 = err;
                        } finally {
                              try {
                                    if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                          _iterator4.return();
                                    }
                              } finally {
                                    if (_didIteratorError4) {
                                          throw _iteratorError4;
                                    }
                              }
                        }
                  } catch (error) {
                        console.log("aset _findGroupItem error ", error);
                  }
                  return null;
            }
            /**
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             자산 추가 후 삭제가 되는 경우, 다시 배치할 ㅅ수 잇게 arrangement를 false로 변경
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             * */

      }, {
            key: "_addTreeGroupItem",
            value: function _addTreeGroupItem(assetId) {

                  // 그룹 목록 유무 판단
                  if (this._app.groupList.length <= 0) return;

                  if (this._app.groupList[0].children.length <= 0) {
                        return;
                  }

                  //////////////////////////////////
                  // 2. asset list에서 asetid로 assetItem 찾기
                  if (this._assetLoader.assetList == null) return;

                  // 리스트에서 assetItem 찾기
                  var assetItem = null;
                  var _iteratorNormalCompletion5 = true;
                  var _didIteratorError5 = false;
                  var _iteratorError5 = undefined;

                  try {
                        for (var _iterator5 = this._assetLoader.assetList[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                              var tempAssetItem = _step5.value;

                              if (tempAssetItem.AM_ASSET_ID == assetId) {
                                    assetItem = tempAssetItem;
                              }
                        }
                  } catch (err) {
                        _didIteratorError5 = true;
                        _iteratorError5 = err;
                  } finally {
                        try {
                              if (!_iteratorNormalCompletion5 && _iterator5.return) {
                                    _iterator5.return();
                              }
                        } finally {
                              if (_didIteratorError5) {
                                    throw _iteratorError5;
                              }
                        }
                  }

                  if (assetItem == null) return;

                  // 3. 그룹 찾기
                  var groupItem = this._findGroupItem(assetItem.LV3_CLASS_ID);
                  if (groupItem == null) return;

                  if (groupItem.model.children.length <= 0) {
                        return;
                  }
                  //////////////////////////////////


                  //////////////////////////////////
                  // 4. 그룹에서 assetItem 찾아 arrangement를 false로 설정.
                  var _iteratorNormalCompletion6 = true;
                  var _didIteratorError6 = false;
                  var _iteratorError6 = undefined;

                  try {
                        for (var _iterator6 = groupItem.model.children[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                              var childItem = _step6.value;

                              if (childItem.asset_id == assetId) {
                                    childItem.arrangement = false;
                              }
                        }
                  } catch (err) {
                        _didIteratorError6 = true;
                        _iteratorError6 = err;
                  } finally {
                        try {
                              if (!_iteratorNormalCompletion6 && _iterator6.return) {
                                    _iterator6.return();
                              }
                        } finally {
                              if (_didIteratorError6) {
                                    throw _iteratorError6;
                              }
                        }
                  }
            }

            /**
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             자산 삭제 -> 추가 처리
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             * */

      }, {
            key: "notificationList",
            get: function get() {
                  return [EditorProxy.NOTI_OPEN_PAGE, EditorProxy.NOTI_CLOSED_PAGE, EditorProxy.NOTI_CHANGE_ACTIVE_LAYER, EditorProxy.NOTI_ADD_COM_INSTANCE, EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE];
            }
      }]);

      return AssetListPanel;
}(ExtensionPanelCore);

var AssetLoadManager = function () {
      _createClass(AssetLoadManager, [{
            key: "assetPageIdList",
            get: function get() {
                  return this._assetPageIdList;
            }
      }, {
            key: "assetList",
            get: function get() {
                  return this._assetList;
            }
      }, {
            key: "treeAsssetList",
            get: function get() {
                  return this._assetList;
            }
      }, {
            key: "assetComponentInfoList",
            get: function get() {
                  return this._assetComponentInfoList;
            }
      }, {
            key: "treeGroupList",
            get: function get() {
                  return this._treeGroupList;
            }
      }], [{
            key: "getInstance",
            value: function getInstance() {
                  if (AssetLoadManager.instance == null) {
                        AssetLoadManager.instance = new AssetLoadManager();
                  }

                  return AssetLoadManager.instance;
            }
      }]);

      function AssetLoadManager() {
            _classCallCheck(this, AssetLoadManager);

            this._assetPageIdList = [];
            this._assetList = [];

            this._treeAssetList = {
                  label: "ASSET LIST",
                  children: []
            };

            this._assetComponentInfoList = [];

            this._treeGroupList = [];
      }

      _createClass(AssetLoadManager, [{
            key: "loadAssetPageIdList",
            value: function () {
                  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                        var url, data, result;
                        return regeneratorRuntime.wrap(function _callee3$(_context3) {
                              while (1) {
                                    switch (_context3.prev = _context3.next) {
                                          case 0:
                                                if (!(this._assetPageIdList.length > 0)) {
                                                      _context3.next = 2;
                                                      break;
                                                }

                                                return _context3.abrupt("return", true);

                                          case 2:
                                                url = window.wemb.configManager.serverUrl + "/nhAsset.do";
                                                //url = "http://115.21.12.44:8080/renobit" + "/nhAsset.do";
                                                data = {
                                                      id: "nhAssetService.getPageList",
                                                      params: {}
                                                };
                                                _context3.prev = 4;
                                                _context3.next = 7;
                                                return wemb.$http.post(url, data);

                                          case 7:
                                                result = _context3.sent;

                                                this._assetPageIdList = result.data.data;

                                                return _context3.abrupt("return", true);

                                          case 12:
                                                _context3.prev = 12;
                                                _context3.t0 = _context3["catch"](4);

                                                console.log("loadAssetPageIdList Error ", _context3.t0);
                                                return _context3.abrupt("return", false);

                                          case 16:
                                          case "end":
                                                return _context3.stop();
                                    }
                              }
                        }, _callee3, this, [[4, 12]]);
                  }));

                  function loadAssetPageIdList() {
                        return _ref3.apply(this, arguments);
                  }

                  return loadAssetPageIdList;
            }()
      }, {
            key: "loadAssetList",
            value: function () {
                  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(location_id) {
                        var url, data, result;
                        return regeneratorRuntime.wrap(function _callee4$(_context4) {
                              while (1) {
                                    switch (_context4.prev = _context4.next) {
                                          case 0:
                                                //let
                                                url = window.wemb.configManager.serverUrl + "/nhAsset.do";
                                                //url = "http://115.21.12.44:8080/renobit" + "/nhAsset.do";
                                                data = {
                                                      id: "nhAssetService.getAssetList",
                                                      params: {
                                                            location_id: location_id
                                                      }
                                                };
                                                _context4.prev = 2;
                                                _context4.next = 5;
                                                return wemb.$http.post(url, data);

                                          case 5:
                                                result = _context4.sent;

                                                this._assetList = result.data.data;

                                                // 기본 정보를 바탕으로 트리 정봅 생성.
                                                this._convertAssetInfoTreeInfo();



                                                // rmfnq
                                                this._treeGroupList = this._createTreeGroupList();

                                                return _context4.abrupt("return", true);

                                          case 13:
                                                _context4.prev = 13;
                                                _context4.t0 = _context4["catch"](2);

                                                console.log("loadAssetList Error ", _context4.t0);
                                                return _context4.abrupt("return", false);

                                          case 17:
                                          case "end":
                                                return _context4.stop();
                                    }
                              }
                        }, _callee4, this, [[2, 13]]);
                  }));

                  function loadAssetList(_x2) {
                        return _ref4.apply(this, arguments);
                  }

                  return loadAssetList;
            }()

            /*
            컴포넌트 정보 읽기
             */

      }, {
            key: "loadCustomComponentInfoList",
            value: function () {
                  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                        var url, result;
                        return regeneratorRuntime.wrap(function _callee5$(_context5) {
                              while (1) {
                                    switch (_context5.prev = _context5.next) {
                                          case 0:
                                                _context5.prev = 0;
                                                url = window.wemb.componentLibraryManager.getExtentionPath("panel", "AssetListPanel") + "/asset_component_info_list.json";
                                                _context5.next = 4;
                                                return wemb.$http.get(url);

                                          case 4:
                                                result = _context5.sent;


                                                if (result.data.hasOwnProperty("list") == false) {
                                                      this._assetComponentInfoList = [];
                                                } else {
                                                      this._assetComponentInfoList = result.data.list;
                                                }

                                                return _context5.abrupt("return", true);

                                          case 9:
                                                _context5.prev = 9;
                                                _context5.t0 = _context5["catch"](0);

                                                console.log("loadCustomComponentInfoList Error ", _context5.t0);
                                                return _context5.abrupt("return", false);

                                          case 13:
                                          case "end":
                                                return _context5.stop();
                                    }
                              }
                        }, _callee5, this, [[0, 9]]);
                  }));

                  function loadCustomComponentInfoList() {
                        return _ref5.apply(this, arguments);
                  }

                  return loadCustomComponentInfoList;
            }()

            // assets 정보(리스트)를 트리 구조로 변경
            // 실행시 오직한번만 실행

      }, {
            key: "_convertAssetInfoTreeInfo",
            value: function _convertAssetInfoTreeInfo() {
                  var _this4 = this;

                  this._treeAssetList = {
                        label: "ASSET LIST",
                        children: []
                  };


                  if (this._assetList == null) return false;

                  this._assetList.forEach(function (assetItem) {
                        //1. 그룹 정보 만들기
                        var groupInfo = null;
                        // 그룹 유무 판단

                        var _iteratorNormalCompletion7 = true;
                        var _didIteratorError7 = false;
                        var _iteratorError7 = undefined;

                        try {
                              for (var _iterator7 = _this4._treeAssetList.children[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                                    var groupItem = _step7.value;


                                    if (groupItem.asset_type == assetItem.LV3_CLASS_ID) {
                                          groupInfo = groupItem;

                                          break;
                                    }
                              }

                              // 신규 그룹 정보 생성 + 추가
                        } catch (err) {
                              _didIteratorError7 = true;
                              _iteratorError7 = err;
                        } finally {
                              try {
                                    if (!_iteratorNormalCompletion7 && _iterator7.return) {
                                          _iterator7.return();
                                    }
                              } finally {
                                    if (_didIteratorError7) {
                                          throw _iteratorError7;
                                    }
                              }
                        }

                        if (groupInfo == null) {
                              groupInfo = {
                                    label: assetItem.LV3_CLASS_NM,
                                    asset_type: assetItem.LV3_CLASS_ID,
                                    children: []
                              };
                              _this4._treeAssetList.children.push(groupInfo);
                        }

                        ////////////////////


                        //2. 어셋 + 컴포넌트 아이템 만들기
                        var assetComponentItem = _this4._createAssetItemToAssetComponentItem(assetItem);
                        if (assetComponentItem != null) {
                              groupInfo.children.push(assetComponentItem);
                        }

                        ////////////////////
                  });
                  return this._treeAssetList;
            }

            ////////////////////////////
            // 그룹 정보 구하기
            // 실행시 오직한번만 실행

      }, {
            key: "_createTreeGroupList",
            value: function _createTreeGroupList() {

                  var data = [];
                  var children = [];
                  data.push({ label: this._treeAssetList.label });

                  if (this._treeAssetList.children && this._treeAssetList.children.length > 0) {

                        for (var i = 0, len = this._treeAssetList.children.length; i < len; i++) {
                              var child = this._treeAssetList.children[i];

                              children.push({ label: child.label + "(" + child.asset_type + ")", model: child });
                        }

                        data[0].children = children;
                  }

                  return data;
            }

            // 타입에 따른 컴포넌트 기본 정보 구하기

      }, {
            key: "_getAssetInitProperties",
            value: function _getAssetInitProperties(LV3_CLASS_ID) {
                  var initProperties = null;

                  var _iteratorNormalCompletion8 = true;
                  var _didIteratorError8 = false;
                  var _iteratorError8 = undefined;

                  try {
                        for (var _iterator8 = this._assetComponentInfoList[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
                              var comInitProperties = _step8.value;

                              if (comInitProperties.asset_type == LV3_CLASS_ID) {
                                    initProperties = $.extend(true, {}, comInitProperties);
                                    return initProperties;
                              }
                        }
                  } catch (err) {
                        _didIteratorError8 = true;
                        _iteratorError8 = err;
                  } finally {
                        try {
                              if (!_iteratorNormalCompletion8 && _iterator8.return) {
                                    _iterator8.return();
                              }
                        } finally {
                              if (_didIteratorError8) {
                                    throw _iteratorError8;
                              }
                        }
                  }

                  return null;
            }

            /*
            assetList에서 assetId에 해당하는 assetItem을 찾아 어셋 컴포넌트 아이템으로 생성
             */

      }, {
            key: "createAssetComponentItemById",
            value: function createAssetComponentItemById(assetId) {

                  if (this._assetList.length <= 0) return null;

                  var assetItem = null;
                  var _iteratorNormalCompletion9 = true;
                  var _didIteratorError9 = false;
                  var _iteratorError9 = undefined;

                  try {
                        for (var _iterator9 = this._assetList[Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                              var tempAssetItem = _step9.value;

                              if (tempAssetItem.AM_ASSET_ID == assetId) {
                                    assetItem = tempAssetItem;
                                    break;
                              }
                        }
                  } catch (err) {
                        _didIteratorError9 = true;
                        _iteratorError9 = err;
                  } finally {
                        try {
                              if (!_iteratorNormalCompletion9 && _iterator9.return) {
                                    _iterator9.return();
                              }
                        } finally {
                              if (_didIteratorError9) {
                                    throw _iteratorError9;
                              }
                        }
                  }

                  if (assetItem == null) return null;

                  return this._createAssetItemToAssetComponentItem(assetItem);
            }

            /*
            asssetItem을 어셋 컴포넌트 아이템으로 ㅂ녀경
             */

      }, {
            key: "_createAssetItemToAssetComponentItem",
            value: function _createAssetItemToAssetComponentItem(assetItem) {

                  var assetComponentItem = null;
                  //2. 어셋 만들기
                  var assetInitProperties = this._getAssetInitProperties(assetItem.LV3_CLASS_ID);

                  if (assetInitProperties) {

                        assetInitProperties.initProperties.props.setter.assetId = assetItem.AM_ASSET_ID;
                        ////////////////////
                        // assetInfo에 값 적용하기
                        assetComponentItem = {
                              label: assetItem.ASSET_NO,
                              asset_type: assetItem.LV3_CLASS_ID,
                              asset_id: assetItem.AM_ASSET_ID,
                              name: assetInitProperties.name,
                              show: true,
                              arrangement: false,
                              "initProperties": assetInitProperties.initProperties
                        };
                  }

                  return assetComponentItem;
            }
      }]);

      return AssetLoadManager;
}();

window.AssetListPanel = AssetListPanel;
