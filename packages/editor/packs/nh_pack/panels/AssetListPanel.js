class AssetListPanel extends ExtensionPanelCore {



      constructor(){
            super();
            this._assetList = [];
            this._app = null;
            this._openPageState=false;
            this._loadingAssetState=false;
            this._loadingPageIdList=false;


            this._assetLoader = AssetLoadManager.getInstance();
      }


      /*
	call :
		인스턴스 생성 후 실행
		단, 아직 화면에 붙지 않은 상태임.

	 */
      create() {

            let template =
                  `
            <div style="overflow: auto;">
                  <div style="display: flex;padding: 6px;">
                        <el-select :disabled="assetPageId.length!=0" v-model="tempAssetPageId" placeholder="Select" size="small" style="flex: 1;margin-right: 4px;">
                              <el-option
                                    v-for="item in assetPageIdList"
                                    :key="item.LOCATION_ID"
                                    :label="item.LOCATION_NM"
                                    :value="item.LOCATION_ID">
                              </el-option>
                        </el-select>
                        <el-button size="small" type="primary" v-if="assetPageId.length==0" @click="on_updatePageId">적용</el-button>
                  </div>
                  <!--
                  <div>
                        <button @click="on_cancelPageId">취소</button>
                  </div>
                  -->
                  
                  <template v-if="active==true && activeLayerName=='threeLayer'">                  
                        <el-tree
                        ref="assetTree"
                        :data="groupList"
                        highlight-current
                              :props="defaultProps"
                              default-expand-all
                              node-key="id"
                              @node-click="onSelectedGroup($event)"
                        >
                        </el-tree>
                        
                        <hr>
                        <div v-if="selectedExtensionListInGroup.length>0" class="component-txt-list">     
                              <div class="search-field">
                                    <el-select v-model="assetsListFilter" placeholder="Select" size="small"> 
                                          <el-option
                                                v-for="item in assetsListFilterOptions"
                                                :key="item.value"
                                                :label="item.label" 
                                                :value="item.value">
                                          </el-option>
                                    </el-select>
                                    <el-input placeholder="search" v-model="assetsSearchStr" size="small" @keyup.enter.native="on_searchAssetItems">
                                          <i slot="prefix" class="el-input__icon el-icon-search"></i>
                                    </el-input>
                              </div>                               
                              <ul class="list">
                                    <!--사용한 항목은 li.disabled-->
                                    <li class="component" :class="{disabled:item.arrangement}" v-for="item in selectedExtensionListInGroup" @dblclick="" v-if="filterAssetItem(item)">
                                          <div class="txt-wrap"  @dragstart="onDragStartThreeComponent($event, item)" :draggable="item.arrangement==false" >
                                                <span>{{item.label}}</span>
                                          </div>
                                    </li>
                              </ul> 
                        </div>
                  </template>
                   
                  <div v-if="active==false" style="text-align: center; padding: 20px;">
                        데이터가 설정되지 않았습니다.
                  </div>
                  <div v-if="active==true && activeLayerName!='threeLayer' " style="text-align: center; padding: 20px;">
                        자산 정보는 3D에서만 사용할 수 있습니다.
                  </div>
            </div>
      `
            this._$element = $(template);

            if(this._$parentView==null) {
                  console.log("Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.")
                  return;
            }


            /*
            중요:
            Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.
             */
            this._$parentView.append(this._$element);



            // 트리 목록에서 그룹 목록만 구해오기
            this._app = new Vue({
                  el: this._$element[0],
                  data: {
                        assetPageId:"",
                        tempAssetPageId:"",
                        assetPageIdList:[],
                        ownerPanel:this,
                        groupList: [],
                        defaultProps: {
                              children: 'children',
                              label: 'label'
                        },
                        selectedExtensionListInGroup: {},
                        active: false,
                        activeLayerName:"",

                        assetsListFilterOptions: [{
                              value: 'all',
                              label: '전체'
                        }, {
                              value: 'used',
                              label: '배치'
                        }, {
                              value: 'unused',
                              label: '미배치'
                        }],

                        assetsListFilter: 'all',
                        assetsSearchStr: '',
                        message:""

                  },

                  methods: {

                        selectedFirstGroup(){
                              try {
                                    if(this.groupList[0].children && this.groupList[0].children.length>0) {
                                          this.selectedExtensionListInGroup = this.groupList[0].children[0].model.children;
                                          //this.$refs.assetTree.setCheckedNodes([this.groupList[0].children[0].model]);

                                    }
                                    else
                                          this.selectedExtensionListInGroup = [];
                              }catch(error){
                                    console.log("selectedFirstGroup Error ", error);

                                    this.selectedExtensionListInGroup = null;
                              }
                        },
                        onSelectedGroup:function(tree){
                              if( tree && tree.model ){
                                    this.assetsSearchStr = "";
                                    this.selectedGroupModel= tree.model;
                                    this.selectedExtensionListInGroup = tree.model.children;

                                    // 그룹 아이템에 남아 있는 필터 정보를 제거하기(show=true)
                                    this.clearSearchAssetItems();
                              }
                        },


                        onDragStartThreeComponent(event, item){
                              let newItem = Object.assign(item,{category:"3D"});
                              event.dataTransfer.effectAllowed = 'move';

                              if( CPUtil.getInstance().browserDetection() != "Internet Explorer" ){
                                    event.dataTransfer.setData('text/plane', JSON.stringify(item));
                              }
                              window["dragData"] = JSON.stringify(item);
                        },

                        setActive(active){
                              this.active = active;
                        },

                        setActiveLayerName(layerName){
                              this.activeLayerName = layerName;
                        },


                        on_updatePageId(value){
                              if(this.tempAssetPageId.length>0) {

                                    this.assetPageId = this.tempAssetPageId;
                                    window.wemb.mainPageComponent.setDynamicGroupPropertyValue("extension",  "asset_page_id", this.assetPageId);
                                    // 페이지 정보가 변경되면
                                    this.ownerPanel._loadAssetListInPageId(this.assetPageId);
                              }

                        },


                        on_cancelPageId(){
                              this.assetPageId = "";
                              window.wemb.mainPageComponent.setDynamicGroupPropertyValue("extension",  "asset_page_id", this.assetPageId);
                              delete window.wemb.mainPageComponent.properties.extension.asset_page_id;

                        },

                        // 검색 처리 (enter 키 입력시)
                        on_searchAssetItems(obj){

                              if(this.selectedExtensionListInGroup){
                                    for(var assetItem of this.selectedExtensionListInGroup){

                                          if(assetItem.label.indexOf(this.assetsSearchStr)==-1){
                                                assetItem.show=false;
                                          }else {
                                                assetItem.show=true;
                                          }
                                    }
                              }
                        },


                        // 검색 처리 취소
                        clearSearchAssetItems(){
                              if(this.selectedExtensionListInGroup){
                                    for(var assetItem of this.selectedExtensionListInGroup){
                                          assetItem.show=true;
                                    }
                              }
                        },



                        filterAssetItem(item){

                              if(this.assetsListFilter=="all" && item.show==true)
                                    return true;


                              if(this.assetsListFilter=="used" && item.arrangement==true &&  item.show==true) {
                                    return true;
                              }

                              if(this.assetsListFilter=="unused" && item.arrangement==false &&  item.show==true) {
                                    return true;
                              }


                              return false;
                        }
                  }
            })


            return this._app;

      }













      get notificationList(){
            return [
                  EditorProxy.NOTI_OPEN_PAGE,
                  EditorProxy.NOTI_CLOSED_PAGE,
                  EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
                  EditorProxy.NOTI_ADD_COM_INSTANCE,
                  EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE
            ]
      }




      /*
      noti가 온 경우 실행
      call : mediator에서 실행
       */
      handleNotification(note){
            switch(note.name){

                  /*
                  case EditorProxy.NOTI_UPDATE_EDITOR_STATE :
                        this.noti_updateEditorState(note.get());
                        break;
                  */
                  case EditorProxy.NOTI_OPEN_PAGE :
                        this.noti_openPage();
                        break;

                  case EditorProxy.NOTI_CLOSED_PAGE :
                        this.noti_closedPage();
                        break;

                  case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :
                        this.noti_changeActiveLayer(note.getBody());
                        break;

                  case EditorProxy.NOTI_ADD_COM_INSTANCE  :
                        this.noti_addComInstance(note.getBody());
                        break;

                  case EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE:
                        this.noti_beforeRemoveComInstance(note.getBody());
                        break;
            }
      }

      /*
      페이지가 열릴 때 자산 정보가 로드되어 있지 않으면 자산 정보 로드하기.


      1. 페이지 ID 목록  읽기

      2. 현재 페이지 정보에서 assset_page_id 구하기

         만약 설정되어 있지 않다면 설정 화면 만들기

         만약 설정되어 있다면 해당 id에 대한 어셋 정보만 출력하기




       */
      async noti_openPage(){
            this._openPageState=true;
            this._app.setActive(false);
            if(this._loadingPageIdList==false){

                  // 커스텀 컴포넌트 정보 구하기
                  let success = await this._assetLoader.loadCustomComponentInfoList();
                  if(success==false){
                        this._app.message="서버에서 자산 컴포넌트 정보를 읽을 수 없습니다.";
                        return false;
                  }



                  success = await this._assetLoader.loadAssetPageIdList();
                  if(success==false){
                        this._app.message="서버에서 자산 페이지를 읽을 수 없습니다.";
                        return false;
                  }



                  this._loadingPageIdList = true;
                  this._app.assetPageIdList = this._assetLoader.assetPageIdList;
            }



            // 페이지에 설정되어 있는 pageID 구하기
            if(this._checkAndUpdateAssetPageIdProperty()==false){

                  this._app.message="배치할 페이지 정보가 설정되지 않았습니다. 설정 후 실행해주세요.";
                  return false;
            }


            // 어셋 정보 구하기
            this._app.tempAssetPageId = this._app.assetPageId;
            this._loadAssetListInPageId(this._app.assetPageId);



      }

      noti_closedPage(){
            this._openPageState = false;

      }

      noti_changeActiveLayer(layerName){
            this._app.setActiveLayerName(layerName);
            //this._app.setActive(layerName=="threeLayer");
      }


      noti_addComInstance(instance){
            if(this._openPageState==false)
                  return;


            if(instance) {
                  this._removeTreeGroupItem(instance.assetId);
            }
      }


      noti_beforeRemoveComInstance(instance){
            this._addTreeGroupItem(instance.assetId);
      }

      ////////////////////////////












      /*
      페이지 id에 해당하는 어셋 구하기
       */
      async _loadAssetListInPageId(assetPageId){
            // asset정보 구하기
            let result = await this._assetLoader.loadAssetList(assetPageId);
            this._loadingAssetState= result;

            if(result==false){
                  this._app.message="자산 정보를 구하는 도중에 에러 발생";
                  return false;
            }

            // 그룹 리스트 정보를 vue.app에 전달.
            this._app.groupList = this._assetLoader.treeGroupList;


            //2. 첫 번재 그룹이 선택되게 만든다.
            this._app.selectedFirstGroup();


            /*
               3. 페이지 열고 난 후 이미 배치되어 있는 인스턴스 삭제
             */
            try {
                  window.wemb.editorProxy.comInstanceList.forEach((instance)=>{
                        this._removeTreeGroupItem(instance.assetId);
                  })
            }catch(error){

            }


            this._app.setActive(true);

      }



      /*
            페이지 정보에서 asset_page_id가 있는지 검사 후
            있다면
                  asset_page_id 정보를 _app.assetPageId에 설정하기
       */
      _checkAndUpdateAssetPageIdProperty(){
            let pageInfo = window.wemb.mainPageComponent.properties;

            if(pageInfo.hasOwnProperty("extension")){
                  if(pageInfo.extension.hasOwnProperty("asset_page_id")==true && pageInfo.extension.asset_page_id.length>0){
                        this._app.assetPageId = pageInfo.extension.asset_page_id;
                        return true;
                  }
            }

            return false;
      }

      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       자산 추가 -> 삭제 처리
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      _removeTreeGroupItem(assetId){
            if(this._app.groupList.length<=0)
                  return;

            if(this._app.groupList[0].children.length<=0){
                  return;
            }


            // 트리정보에서 asetId 항목 삭제
            this._app.groupList[0].children.forEach((groupItem)=>{
                  if(this._findAndRemoveTreeGroupItem(groupItem, assetId)){
                        return;
                  }
            })
      }

      _findAndRemoveTreeGroupItem(group, assetId){
            if(group.hasOwnProperty("model")==true){
                  if(group.model.children.length>0){


                        var index = 0;
                        for(var assetItem of group.model.children){
                              if(assetItem.asset_id == assetId){
                                    assetItem.arrangement = true;
                                    return true;
                              }

                              index++;
                        }

                  }
            }


            return false;
      }


      // assetType에 해당하는 자산 정보 구하기(즉, 자산 그룹 구하기)
      _findGroupItem(assetType){
            if(this._app.groupList.length<=0)
                  return null;

            if(this._app.groupList[0].children.length<=0){
                  return null;
            }


            try {
                  for (let groupItem of this._app.groupList[0].children) {
                        if (groupItem.model.asset_type == assetType) {
                              return groupItem;
                        }
                  }
            }catch(error){
                  console.log("aset _findGroupItem error ", error);
            }
            return null;
      }
      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       자산 추가 후 삭제가 되는 경우, 다시 배치할 ㅅ수 잇게 arrangement를 false로 변경
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      _addTreeGroupItem(assetId){

            // 그룹 목록 유무 판단
            if(this._app.groupList.length<=0)
                  return;

            if(this._app.groupList[0].children.length<=0){
                  return;
            }



            //////////////////////////////////
            // 2. asset list에서 asetid로 assetItem 찾기
            if(this._assetLoader.assetList ==null)
                  return;

            // 리스트에서 assetItem 찾기
            let assetItem = null;
            for (let tempAssetItem of this._assetLoader.assetList) {
                  if (tempAssetItem.AM_ASSET_ID == assetId) {
                        assetItem = tempAssetItem;

                  }
            }

            if (assetItem == null)
                  return;



            // 3. 그룹 찾기
            let groupItem = this._findGroupItem(assetItem.LV3_CLASS_ID);
            if(groupItem==null)
                  return;


            if(groupItem.model.children.length<=0){
                  return;
            }
            //////////////////////////////////


            //////////////////////////////////
            // 4. 그룹에서 assetItem 찾아 arrangement를 false로 설정.
            for(var childItem of groupItem.model.children){
                  if(childItem.asset_id == assetId){
                        childItem.arrangement = false;
                  }

            }
      }





      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       자산 삭제 -> 추가 처리
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */

}





class AssetLoadManager {


      static getInstance(){
            if(AssetLoadManager.instance==null){
                  AssetLoadManager.instance = new AssetLoadManager();
            }

            return AssetLoadManager.instance;
      }


      get assetPageIdList(){
            return this._assetPageIdList;
      }

      get assetList(){
            return this._assetList;

      }

      get treeAsssetList(){
            return this._assetList;
      }


      get assetComponentInfoList(){
            return this._assetComponentInfoList;
      }



      get treeGroupList(){
            return this._treeGroupList;
      }


      constructor() {
            this._assetPageIdList = [];
            this._assetList = [];

            this._treeAssetList = {
                  label: "ASSET LIST",
                  children: []
            };


            this._assetComponentInfoList = [];

            this._treeGroupList = [];
      }


      async loadAssetPageIdList(){

            // 페이지를 이미 로드한 경우
            if(this._assetPageIdList.length>0){
                  return true;
            }

            //let url = "http://115.21.12.44:8080" + "/nhAsset.do";
            let url = window.wemb.configManager.serverUrl + "/nhAsset.do";
            let data = {
                  id: "nhAssetService.getPageList",
                  params: {}
            };

            try {

                  let result =  await wemb.$http.post(url,data);
                  this._assetPageIdList = result.data.data;

                  return true;


            }catch(error){
                  console.log("loadAssetPageIdList Error ", error);
                  return false;
            }
      }
      async loadAssetList(location_id){
            let url = window.wemb.configManager.serverUrl + "/nhAsset.do";
            //let url = "http://115.21.12.44:8080/renobit" + "/nhAsset.do";
            let data = {
                  id: "nhAssetService.getAssetList",
                  params: {
                        location_id:location_id
                  }
            };


            try {
                  let result =  await wemb.$http.post(url,data);
                  this._assetList = result.data.data;

                  // 기본 정보를 바탕으로 트리 정봅 생성.
                  this._convertAssetInfoTreeInfo();


                  // rmfnq
                  this._treeGroupList = this._createTreeGroupList();

                  return true;


            }catch(error){
                  console.log("loadAssetList Error ", error);
                  return false;
            }
      }

      /*
      컴포넌트 정보 읽기
       */
      async loadCustomComponentInfoList(){
            try {

                  let url = window.wemb.componentLibraryManager.getExtentionPath("panel", "AssetListPanel")+"/asset_component_info_list.json";
                  let result =  await wemb.$http.get(url);


                  if(result.data.hasOwnProperty("list")==false) {
                        this._assetComponentInfoList = [];
                  }else {
                        this._assetComponentInfoList = result.data.list;
                  }

                  return true;
            }catch(error){
                  console.log("loadCustomComponentInfoList Error ", error);
                  return false;
            }
      }





      // assets 정보(리스트)를 트리 구조로 변경
      // 실행시 오직한번만 실행
      _convertAssetInfoTreeInfo(){

            this._treeAssetList = {
                  label: "ASSET LIST",
                  children: []
            };


            if(this._assetList==null)
                  return false;

            this._assetList.forEach((assetItem)=>{
                  //1. 그룹 정보 만들기
                  let groupInfo = null;
                  // 그룹 유무 판단

                  for(let groupItem of this._treeAssetList.children){

                        if(groupItem.asset_type == assetItem.LV3_CLASS_ID){
                              groupInfo = groupItem;

                              break;
                        }
                  }

                  // 신규 그룹 정보 생성 + 추가
                  if(groupInfo==null){
                        groupInfo = {
                              label:assetItem.LV3_CLASS_NM,
                              asset_type:assetItem.LV3_CLASS_ID,
                              children:[]
                        }
                        this._treeAssetList.children.push(groupInfo);
                  }


                  ////////////////////


                  //2. 어셋 + 컴포넌트 아이템 만들기
                  let assetComponentItem = this._createAssetItemToAssetComponentItem(assetItem);
                  if(assetComponentItem!=null){
                        groupInfo.children.push(assetComponentItem);
                  }



                  ////////////////////

            });
            return this._treeAssetList;
      }


      ////////////////////////////
      // 그룹 정보 구하기
      // 실행시 오직한번만 실행
      _createTreeGroupList(){

            let data = [];
            let children = [];
            data.push({label: this._treeAssetList.label});

            if( this._treeAssetList.children && this._treeAssetList.children.length > 0 ){

                  for( var i=0, len=this._treeAssetList.children.length; i<len; i++ ){
                        let child = this._treeAssetList.children[i];

                        children.push({label: child.label+"("+child.asset_type+")", model: child});
                  }

                  data[0].children = children;
            }

            return data;
      }



      // 타입에 따른 컴포넌트 기본 정보 구하기
      _getAssetInitProperties(LV3_CLASS_ID){
            let initProperties = null;

            for(var comInitProperties of this._assetComponentInfoList){
                  if(comInitProperties.asset_type==LV3_CLASS_ID){
                        initProperties= $.extend(true, {}, comInitProperties);
                        return initProperties;
                  }


            }

            return null;
      }



      /*
      assetList에서 assetId에 해당하는 assetItem을 찾아 어셋 컴포넌트 아이템으로 생성
       */
      createAssetComponentItemById(assetId) {

            if (this._assetList.length <= 0)
                  return null;


            let assetItem = null;
            for (let tempAssetItem of this._assetList) {
                  if (tempAssetItem.AM_ASSET_ID == assetId) {
                        assetItem = tempAssetItem;
                        break;
                  }
            }


            if (assetItem == null)
                  return null;


            return this._createAssetItemToAssetComponentItem(assetItem);

      }


      /*
      asssetItem을 어셋 컴포넌트 아이템으로 ㅂ녀경
       */
      _createAssetItemToAssetComponentItem(assetItem){

            let assetComponentItem = null;
            //2. 어셋 만들기
            let assetInitProperties = this._getAssetInitProperties(assetItem.LV3_CLASS_ID);

            if (assetInitProperties) {

                  assetInitProperties.initProperties.props.setter.assetId = assetItem.AM_ASSET_ID;
                  ////////////////////
                  // assetInfo에 값 적용하기
                  assetComponentItem = {
                        label: assetItem.ASSET_NO,
                        asset_type: assetItem.LV3_CLASS_ID,
                        asset_id: assetItem.AM_ASSET_ID,
                        name: assetInitProperties.name,
                        show: true,
                        arrangement:false,
                        "initProperties": assetInitProperties.initProperties
                  }

            }


            return assetComponentItem;
      }





      static mockup(){
            let info = [{
                  "ASSET_NO": "ASSET_NO1_01",
                  "LV3_CLASS_ID": "ID_ASSSET_01",
                  "LV3_CLASS_NM": "ASSSET_01",
                  "VENDOR_NAME": "장비1-1",
                  "ITEM_NAME": "장비NAME_1",
                  "LV2_CLASS_ID": "HGD00",
                  "AM_ASSET_ID": "AM1803100177",
                  "LV2_CLASS_NM": "기반장비"
            }, {
                  "ASSET_NO": "ASSET_NO1_02",
                  "LV3_CLASS_ID": "ID_ASSSET_01",
                  "LV3_CLASS_NM": "ASSSET_01",
                  "VENDOR_NAME": "장비1-2",
                  "ITEM_NAME": "장비NAME_2",
                  "LV2_CLASS_ID": "HGD00",
                  "AM_ASSET_ID": "AM1803100177",
                  "LV2_CLASS_NM": "기반장비"
            },{
                  "ASSET_NO": "ASSET_NO1_03",
                  "LV3_CLASS_ID": "ID_ASSSET_01",
                  "LV3_CLASS_NM": "ASSSET_01",
                  "VENDOR_NAME": "장비1-3",
                  "ITEM_NAME": "장비NAME_3",
                  "LV2_CLASS_ID": "HGD00",
                  "AM_ASSET_ID": "AM1803100177",
                  "LV2_CLASS_NM": "기반장비"
            },{
                  "ASSET_NO": "ASSET_NO2_01",
                  "LV3_CLASS_ID": "ID_ASSSET_02",
                  "LV3_CLASS_NM": "ASSSET_02",
                  "VENDOR_NAME": "장비2-1",
                  "ITEM_NAME": "장비NAME_1",
                  "LV2_CLASS_ID": "HGD00",
                  "AM_ASSET_ID": "AM1803100177",
                  "LV2_CLASS_NM": "기반장비"
            }, {
                  "ASSET_NO": "ASSET_NO2_02",
                  "LV3_CLASS_ID": "ID_ASSSET_02",
                  "LV3_CLASS_NM": "ASSSET_02",
                  "VENDOR_NAME": "장비2-2",
                  "ITEM_NAME": "장비NAME_2",
                  "LV2_CLASS_ID": "HGD00",
                  "AM_ASSET_ID": "AM1803100177",
                  "LV2_CLASS_NM": "기반장비"
            },{
                  "ASSET_NO": "ASSET_NO2_03",
                  "LV3_CLASS_ID": "ID_ASSSET_02",
                  "LV3_CLASS_NM": "ASSSET_02",
                  "VENDOR_NAME": "장비3",
                  "ITEM_NAME": "장비NAME2-3",
                  "LV2_CLASS_ID": "HGD00",
                  "AM_ASSET_ID": "AM1803100177",
                  "LV2_CLASS_NM": "기반장비"
            }
            ]

            return info;
      }



}





window.AssetListPanel = AssetListPanel;



/*
      자산 출력시 트리 구조
	    this._assetList = {
		    label:"전체",
		    children:[{
			    label:"그룹1",
			    children:[{
				    label:"child1-1",
				    name:"componentName1",
				    show:true
			    },{
				    label:"child1-2",
				    name:"componentName2",
				    show:true
			    },{
				    label:"child1-3",
				    name:"componentName3",
				    show:true
			    }]
		    },{
			    label:"그룹2",
			    children:[{
				    label:"child2-1",
				    name:"componentName2-1",
				    show:true
			    },{
				    label:"child2-2",
				    name:"componentName2-2",
				    show:true
			    },{
				    label:"child2-3",
				    name:"componentName2-3",
				    show:true
			    }]
		    }]
	    }
*/
