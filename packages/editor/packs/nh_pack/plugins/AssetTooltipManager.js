class AssetTooltipManager extends ExtensionPluginCore {

      constructor() {
            super();

      }



      get notificationList() {
            return [
                  ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE,
                  ViewerProxy.NOTI_ADD_COM_INSTANCE,
                  ViewerProxy.NOTI_CLOSED_PAGE
            ]
      }


      /*
      call :
            인스턴스 생성 후 실행
            단, 아직 화면에 붙지 않은 상태임.

       */
      create() {


      }


      /*
      noti가 온 경우 실행
      call : mediator에서 실행
       */
      handleNotification(note) {
            // 뷰어 모드에서만 실행
            if(window.wemb.configManager.isEditorMode==true)
                  return;

            switch (note.name) {
                  //case ViewerProxy.NOTI_ADD_COM_INSTANCE :
                        //this.noti_addComInstance(note.getBody());
                  //      break;
                  case ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE :
                        this.noti_changePageLoadingState(note.getBody());
                        break;
                  // case ViewerProxy.NOTI_CLOSED_PAGE:
                  //       this.noti_closePage();
                  //       break;
            }

      }



      noti_changePageLoadingState(pageLoadingState) {
            // 모든 정보가 로드된 상태.
            if (pageLoadingState == "loaded") {
                  console.log("")
                  let comList = window.wemb.mainPageComponent.$threeLayer;
                  comList.forEach((comInstance)=>{
                        if(comInstance instanceof AssetBaseComponent){
                              comInstance.toolTipPropertyName = AssetTooltipManager.TOOTIP_PROPERTY_NAME;
                        }
                  })


            }
      }


}

window.AssetTooltipManager = AssetTooltipManager;
AssetTooltipManager.TOOTIP_PROPERTY_NAME = "loc_surf_nm";
