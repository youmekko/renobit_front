class RackAnimation extends ExtensionPluginCore {


      constructor() {
            super();
            /*
            2018.06.01 ddandongne
             - 임시적으로 적용 상태.
                  - 추후 instanceManagerOnPageViewer 클래스에서 처리 해야함.
             - 전역에서 접근할 rackAnimation 클래스 인스턴스
             - RackAnimation 인스턴스는 CreateExtensionCommand에서 config정보에 들어 있는 값을 읽어 생성함.
             */
            window.wemb["extension"] = {};
      }

      onDoubleClick_Target(event) {
            event.stopPropagation();
            let instance = this.instances.get(event.target);
            if (instance) {
                  var selected = !instance.selected;
                  if (selected) {
                        this.transitionInFromAssetID(instance.assetId);
                  } else {
                        this.transitionOutFromInstance(instance);
                  }
                  this.toggleBoxHelperByItem();
            }
      }

      onSelectClientItem(event) {
            let selectUnit = event.data.selectItem;
            this.transitionInFromAssetID(event.target.data.id, selectUnit.data.id);
      }

      onPopupClose() {
            this.transitionOut();
            this._popupInstance = null;
      }

      /**
       *  팝업 요청 함수
       *  배치 타입에 따라 요청하는 팝업 페이지 이름이 다름
       *  refresh를 이용하면 이전 타입을 비교해 요청을 다르게 해야 한다.
       *  결론 close - open 을 이용하는게 답이다.
       *
       *   parameter 구성
       * */
      requestPopup(popupInfo) {

            if (this._popupInstance != null) {
                  if (this._popupInstance.name == popupInfo.name) {
                        let popup = this._popupInstance;
                        popup.innerPage.updateDisplay(popupInfo.params);
                        return;
                  } else {
                        this._popupInstance.closed();
                  }
            }

            // 팝업 호출
            this._popupInstance = window.wemb.popupManager.open(popupInfo.name, popupInfo);
            // 팝업 타입 설정
            this._popupInstance.name = popupInfo.name;

            if (this._popupInstance && !this._popupInstance.hasEventListener("event/closedPopup", this._popCloseHandler)) {
                  this._popupInstance.addEventListener("event/closedPopup", this._popCloseHandler);
            }

      }


      transitionInFromAssetID(assetId, unitId) {

            let selectInstances = this.getInstanceById(assetId);

            if (selectInstances.length > 0) {
                  let selectAsset = selectInstances.shift();
                  let popupObj = this.composePopupInfo(selectAsset.popupId, selectAsset.assetId);
                  popupObj.params.mountData = selectAsset.data;
                  let selectUnit;
                  if (!selectAsset.selected) {
                        this.transitionInFromInstance(selectAsset);
                  }
                  selectUnit = selectAsset.getUnitById(unitId);
                  selectAsset.focusSelectUnit(selectUnit);
                  this.toggleBoxHelperByItem(selectUnit);
                  // 실장 정보가 있다면 실장을 선택처리하고 팝업 요청
                  if (selectUnit != null && selectUnit.selected) {
                        popupObj.params.assetId = selectUnit.data.id;
                        popupObj.params.mode = "mountInfo";
                  }
                  this.requestPopup(popupObj);
            } else {

                  this.transitionOut();

            }

      }

      composePopupInfo(popName, assetId) {
            var info = {
                  name: popName,
                  title: "Asset Info",
                  className: "popup",
                  params: {
                        popupOwner: this,
                        assetId: assetId,
                        mode: ''
                  }
            };
            if (popName.indexOf("Rack") > -1) {
                  info.x = 1140;
                  info.y = 200;
                  info.width = 750;
                  info.height = 625;
                  info.params.mode = "rackInfo";
            } else {
                  info.x = 1300;
                  info.y = 250;
                  info.width = 550;
                  info.height = 480;
            }
            return info;
      }

      requestFocusAssetById(assetId) {
            this.requestFocusClear();
            let instances = this.getInstanceById(assetId);
            if (instances.length >= 0) {
                  let instance = instances.shift();
                  instance.hilight = true;
                  this.threes.boxHelper.setFromObject(instance.transitionTarget);
                  this.threes.boxHelper.material.visible = true;
            }
      }

      requestFocusClear() {
            this.threes.boxHelper.target = null;
            this.threes.boxHelper.material.visible = false;
      }

      getInstanceById(assetId) {
            let instanceArr = Array.from(this.instances.values());
            let selectInstances = instanceArr.filter((instance) => {
                  return (instance.assetId == assetId);
            });
            return selectInstances;
      }

      transitionOutFromInstance(instance) {
            instance.transitionOut();
            this.toggleVisibleOtherInstance(instance, true);
            this.cameraTransitionOut();
            // 팝업 있으면 닫기
            if (this._popupInstance) {
                  this._popupInstance.closed();
            }
      }


      toggleVisibleOtherInstance(currentSelect, bValue) {
            this.instances.forEach((instance) => {
                  if (instance != currentSelect) {
                        instance.visible = bValue;
                        if (instance.selected) {
                              instance.transitionOut();
                        }
                  }
            });
      }

      transitionInFromInstance(instance) {
            instance.transitionIn();
            if(!instance.visible){ instance.visible = true;}
            this.toggleVisibleOtherInstance(instance, false);
            // 카메라 포커스 처리
            this.cameraTransitionInInstance(instance);
            // 검색 하이라이트 제거
            this.requestFocusClear();
      }

      cameraTransitionInInstance(instance) {
            // instance의 스케일 정보를 초기 벡터에 반영
            let offset = new THREE.Vector3(0, 0, 30 * instance.elementSize.z/instance.size.z );
            offset = offset.applyMatrix4(instance.appendElement.matrixWorld);

            this.threes.mainControls.transitionFocusInTarget(
                  offset,
                  instance.appendElement.position.clone().add({x: 0, y: 10, z: 0}),
                  1
            );
            WVToolTipManager.setEnabled(false);
      }

      cameraTransitionOut() {
            this.threes.mainControls.transitionFocusInTarget(
                  this.threes.mainControls.position0.clone(),
                  this.threes.mainControls.target0.clone(),
                  1
            );
            this.toggleBoxHelperByItem();
            WVToolTipManager.setEnabled(true);
      }

      // 모든 객체 초기화 배경 클릭 시 호출 됨
      transitionOut() {
            this.toggleVisibleOtherInstance(null, true);
            this.cameraTransitionOut();
      }

      // 선택 된 장비가 있거나 장비가 selected상태이면 boxHelper 적용
      toggleBoxHelperByItem(selectItem) {
            if (selectItem && selectItem.selected) {
                  this.threes.boxHelper.setFromObject(selectItem.skin);
                  this.threes.boxHelper.material.visible = true;
            } else {
                  this.threes.boxHelper.target = null;
                  this.threes.boxHelper.material.visible = false;
            }
      }


      get notificationList() {
            return [
                  ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE,
                  ViewerProxy.NOTI_CLOSED_PAGE
            ]
      }


      /*
      call :
            인스턴스 생성 후 실행
            단, 아직 화면에 붙지 않은 상태임.

       */
      create() {


      }


      /*
      noti가 온 경우 실행
      call : mediator에서 실행
       */
      handleNotification(note) {
            switch (note.name) {
                  case ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE :
                        this.noti_changePageLoadingState(note.getBody());
                        break;
                  case ViewerProxy.NOTI_CLOSED_PAGE:
                        this.noti_closePage();
                        break;
            }

      }


      initProperties() {
            this._popupInstance = null;
            this.instances = new Map();
            this.threes = window.wemb.threeElements;
            this._clientItemSelectHandler = this.onSelectClientItem.bind(this);
            this._dblClickHandler = this.onDoubleClick_Target.bind(this);
            this._popCloseHandler = this.onPopupClose.bind(this);
            window.wemb.extension.rackAnimation = this;
      }


      destroy() {
            if (this.instances) {
                  WVToolTipManager.destroy();
                  this.removeEventListener();
                  this.instances.clear();
            }
            this.instances = null;
            this.threes = null;
            this._clientItemSelectHandler = null;
            this._dblClickHandler = null;
            this._popCloseHandler = null;
            if(this._popupInstance){
                  this._popupInstance.closed();
            }
            this._popupInstance = null;
            window.wemb.extension.rackAnimation = null;

      }

      async noti_changePageLoadingState(pageLoadingState) {
            // 기존  open page

            if (pageLoadingState == "ready") {
                  let spinner = Spinner.getInstance();
                  spinner.show(document.body);
                  spinner.setStyle({"border-color":"#ffffff",
                        "border-left-color": "transparent",
                        "border-right-color":"transparent"});
                  this.initProperties();
                  return;
            }

            // 모든 정보가 로드된 상태.
            if (pageLoadingState == "loaded") {
                  let assetsInfos = await this.loadData(window.wemb.pageManager.currentPageInfo.id);
                  assetsInfos = this.parseProvider(assetsInfos.rackInfo, assetsInfos.mountList);
                  //console.log("assetsInfos",assetsInfos, JSON.stringify(assetsInfos) );
                  var proxy = this._facade.retrieveProxy(ViewerProxy.NAME);
                  proxy.comInstanceList.forEach((instance) => {
                        if (instance instanceof AssetBaseComponent) {
                              this.instances.set(instance.transitionTarget, instance);
                        }
                  });
                  this.composeInstanceData(assetsInfos);
                  this.registEventListener();
                  this.initTooltip();
                  this.checkqueryParams();
                  Spinner.getInstance().hide();
            }

      }

      initTooltip() {
            // 3d 라벨이 그려지는 컨테이너에 추가
            WVToolTipManager.create(this.threes, document.querySelector(".label-container"));
            this.instances.forEach((instance) => {
                  WVToolTipManager.add(instance);
            });
            WVToolTipManager.setEnabled(true);
      }


      checkqueryParams() {
            // 쿼리 파람 처리
            let qParams = window.wemb.configManager.queryParams;
            let assetId = qParams.rack_id || "";
            let unitId = qParams.am_asset_id || "";
            if (assetId) {
                  this.transitionInFromAssetID(assetId, unitId);
            }
      }


      // 배치된 자산에 데이터 맵핑
      composeInstanceData(dataProvider) {
            this.instances.forEach((client) => {
                  let data = dataProvider.filter((vo) => {
                        return (vo.am_asset_id == client.assetId);
                  });

                  client.data = data[0];
            });
      }


      registEventListener() {
            this.instances.forEach((instance) => {
                  this.threes.domEvents.addEventListener(instance.transitionTarget, "dblclick", this._dblClickHandler, false);
                  instance.addEventListener("WVComponentEvent.SELECT_ITEM", this._clientItemSelectHandler);
            });

      }


      removeEventListener() {

            this.instances.forEach((instance) => {
                  this.threes.domEvents.removeEventListener(instance.transitionTarget, "dblclick", this._dblClickHandler, false);
                  instance.addEventListener("WVComponentEvent.SELECT_ITEM", this._clientItemSelectHandler);
            });

      }

      ////////////////////////////
      parseProvider(rackInfoList, mountList) {
            let results = [];
            let errorList = [];
            rackInfoList.map((value) => {
                  value.items = mountList.filter(mount => mount.rack_asset_id == value.am_asset_id);

                  var newData = this.parseData(value);
                  if(newData.hasOwnProperty("__error__") == true) {
                        errorList.push(newData.__error_unit__);
                  }

                  results.push(newData);
            });
            return results;
      }

      parseData(data) {
            let i, j;
            let max = data.max_unit_qty;
            let items = data.items.filter(function(item){
                  return parseInt(item.unit_use_qty) > 0
            });
            let itemLen = items.length;
            let occupyIndex = new Array(max + 1).fill(0);

            for (i = 0; i < itemLen; i++) {
                  let item = items[i];
                  let endIndex = parseInt(item.unit_end_no);
                  let startIndex = parseInt(item.unit_start_no);
                  for (j = startIndex; j <= endIndex; j++) {
                        let volume = occupyIndex[j] + 1;
                        occupyIndex[j] = volume;
                  }
            }

            // order by startIndex
            items.sort(function (a, b) {
                  var p1 = parseInt(a.unit_start_no);
                  var p2 = parseInt(b.unit_start_no);

                  if (p1 < p2) return -1;
                  if (p1 > p2) return 1;
                  return 0;
            });

            let indexArray = [];
            for (i = 1; i <= max; i++) {
                  var indexItem = occupyIndex[i];
                  var pushItem = {};

                  for (j = 1; j <= indexItem; j++) {
                        pushItem[j] = 0;
                  }
                  indexArray[i] = pushItem;
            }

            for (i = 0; i < itemLen; i++) {
                  let item = items[i];
                  let index = 1;
                  let s_index = parseInt(item.unit_start_no);
                  let e_index = parseInt(item.unit_end_no);

                  if(isNaN(index) == true){
                        data.items = [];
                        data.__error__ = true;
                        data.__error_unit__ = _item.asset_no;
                        return data;
                  }

                  for (var tempIndex = s_index; tempIndex <= e_index; tempIndex++) {
                        var occupied = occupyIndex[tempIndex];
                        index = index >= occupied ? index : occupied;
                  }

                  item.columnWeight = 100 / index;

                  let indexObj = indexArray[s_index];
                  for (let key in indexObj) {
                        // 세로 영역의 occupied 계산
                        for (j = item.unit_use_qty; j > 0; j--) {
                              indexArray[s_index + j -1 ][key] = item.columnWeight * (key-1);
                        }

                  }
            }

            for (i = 0; i < itemLen; i++) {
                  let item = items[i];
                  let s_index = parseInt(item.unit_start_no);

                  let indexObj = indexArray[s_index];
                  for (let key in indexObj) {
                        if(indexObj[key] != -1) {
                              item.padding = indexObj[key];

                              for (j = item.unit_use_qty; j > 0; j--) {
                                    indexArray[s_index + j -1 ][key] = -1;
                              }
                              break;
                        }
                  }
            }

            data.items = items;

            return data;
      }


      // 데이터 가져오기
      async loadData(pageId) {
            let promise = new Promise((resolve, reject) => {
                  let rackInfoList = null;
                  let mountList = null;
                  let datasetWorker = window.wemb.dataService.call(pageId, "rack_info_list", {});
                  if (datasetWorker && datasetWorker.item) {
                        datasetWorker.on("error", (event) => {
                              reject(event);
                        });
                        datasetWorker.on("success", (event) => {
                              rackInfoList = event.rowData;
                              if (mountList != null) {
                                    resolve({rackInfo: rackInfoList, mountList: mountList});
                              }
                        });
                  }


                  let mountWorker = window.wemb.dataService.call(pageId, "mount_list", {});
                  if (mountWorker && mountWorker.item) {
                        mountWorker.on("error", (event) => {
                              reject(event);
                        });
                        mountWorker.on("success", (event) => {
                              mountList = event.rowData;
                              if (rackInfoList != null) {
                                    resolve({rackInfo: rackInfoList, mountList: mountList});
                              }
                        });
                  }
            });
            return promise;
      }


      noti_closePage() {
            console.log("RackAnimation close-");
            this.destroy();
      }


}

window.RackAnimation = RackAnimation;
