class FireEffect {
      constructor(){
            this._parent      = null;
            this._element     = null;
            this._clock       = new THREE.Clock();
            this._animateID   = null;
            this._fire        = null;
      }

      create( info ){
            //let texture   = NLoaderManager.textureLoader.load(info.path);
            VolumetricFire.texturePath = info.path;
            this._fire = new VolumetricFire(2, 4, 2, 0.2, window.wemb.camera );
            /*this._element.material.uniforms.magnitude.value = 1.5;
            this._element.material.uniforms.gain.value      = 0;
            this._element.material.uniforms.lacunarity.value      = 0;
            this._element.material.uniforms.noiseScale.value = new THREE.Vector4(
                  5, 2, 5, 0.3
            );*/
            this._element = this._fire.mesh;
            this._element.material.depthTest = false;
            this._element.renderOrder = 5000;
            this._element.scale.set( info.size, info.size, info.size );
            this._element.position.set(info.pos.x, info.pos.y, info.pos.z );
      }

      draw( parent ) {
            this._parent = parent;
            this._parent.add(this._element);
      }

      animate(){
            this._clock.getDelta();
            this._fire.update(this._clock.elapsedTime);
            this._animateID = requestAnimationFrame(()=>{
                  this.animate();
            });
      }

      destroy(){
            cancelAnimationFrame(this._animateID);
            this._parent.remove(this._element);
            MeshManager.disposeMesh(this._element);
            this._parent = null;
            this._element = null;
            this._fire    = null;
      }

}
