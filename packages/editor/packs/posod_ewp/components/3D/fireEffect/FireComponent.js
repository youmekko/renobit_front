class FireComponent extends WV3DResourceComponent {
      constructor(){ super(); }
      _onCreateProperties(){
            super._onCreateProperties();
            this._elementSize = this.properties.elementSize || {x:1, y:1, z:1};
      }

      _onCommitProperties(){
            super._onCommitProperties();
            if(this._resourceLoaded && this.isEditorMode ) {
                  this.validateCallLater(this.refreshRender);
            }
      }

      _validateSize(){
            let value = this.size;
            if(this._resourceLoaded){
                  this.element.scale.set(value.x, value.y, value.z);
            }
      }

      /*
      일반적으로 controller에 의해서 움직인 값을 setter에 적용할 때 사용.
       */
      syncLocationSizeElementPropsComponentProps(){
            this._properties.setter.position.x =  parseInt(this.appendElement.position.x);
            this._properties.setter.position.y =  parseInt(this.appendElement.position.y);
            this._properties.setter.position.z =  parseInt(this.appendElement.position.z);

            this._properties.setter.rotation.x =  (this.appendElement.rotation.x*Matrix.TO_DEGREE).toFixed(0);
            this._properties.setter.rotation.y =  (this.appendElement.rotation.y*Matrix.TO_DEGREE).toFixed(0);
            this._properties.setter.rotation.z =  (this.appendElement.rotation.z*Matrix.TO_DEGREE).toFixed(0);

            // elementSize정보가 없을 경우 size값을 이용
            let sizeInfo =  this.size;
            this._properties.setter.size.x =  (+this.appendElement.scale.x)*+sizeInfo.x;
            this._properties.setter.size.y =  (+this.appendElement.scale.y)*+sizeInfo.y;
            this._properties.setter.size.z =  (+this.appendElement.scale.z)*+sizeInfo.z;
            this._validateLabel();
            this.refreshRender();
      }


      async validateResource(){
            // rack obj 로드 처리
            try{
                  this.removePrevResource();
                  let resourceInfo     = this.getGroupPropertyValue("setter", "resource");
                  let path             = resourceInfo.texturePath;
                  let texture          = NLoaderManager.textureLoader.load(path);
                  this._element = new THREE.Fire(texture);
                  this._element.material.uniforms.magnitude.value = 1.5;
                  this._element.material.uniforms.gain.value             = 0;
                  this._element.material.uniforms.lacunarity.value      = 0;
                  this._element.material.uniforms.noiseScale.value = new THREE.Vector4(
                        5, 2, 5, 0.3
                  );
                  this._element.material.depthTest = false;
                  this._element.renderOrder = 5000;
                  this.appendElement.add(this._element);
            } catch( error ){
                  throw error;
            }
      }

      _onValidateResource(){
            this._resourceLoaded = true;
            this._validateSize();
      }


      get usingRenderer() {
            return true;
      }

      refreshRender(){
            if(this.clock){
                  this.clock.getDelta();
                  this._element.update(this.clock.elapsedTime);
            }
      }

      render() {
            if(this.clock && this.isViewerMode && this.visible ) {
                  this.clock.getDelta();
                  this._element.update(this.clock.elapsedTime*1.4);
            }
      }


      onLoadPage() {
            super.onLoadPage();
            this.clock = new THREE.Clock();
            if(this.isEditorMode){
                  this.clock.getDelta();
                  this._element.update(this.clock.elapsedTime);
            }
      }

}

WV3DPropertyManager.attach_default_component_infos(FireComponent, {
      "setter": {
            "size": {x: 50, y:50, z: 50}
      },
      "label": {
            "label_text": "FireComponent",
      },

      "info": {
            "componentName": "FireComponent",
            "version": "1.0.0",
      }
});


