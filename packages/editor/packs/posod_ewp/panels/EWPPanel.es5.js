
class EWPPanel extends ExtensionPanelCore {

      constructor(){
            super();
            this._app = null;
            this.excelFileLoader = new ExcelFileLoader();
             console.log("EWPPanel")

      }


      /*
	call :
		인스턴스 생성 후 실행
		단, 아직 화면에 붙지 않은 상태임.

	 */
      create() {

            let template =
                  `
            <div style="overflow: auto;padding:8px;"> 
                  <template v-if="activeLayerName=='threeLayer'">
                        <button class="el-button el-button--primary" style="width:100%;" @click="on_selectExcelFile()"> Excel 파일 선택하기</button>
                        <input type="file" id="excel-file-input" @change="on_change_file($event)" style="display:none"/></p>
                  </template>
                  <el-alert v-else 
                      title="3D Layer가 선택된 상태에서만 실행할 수 있습니다. "
                      type="info"
                      :closable="false"
                      show-icon>
                    </el-alert>  
            </div>
      `
            this._$element = $(template);

            if(this._$parentView==null) {
                  console.log("Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.")
                  return;
            }


            /*
            중요:
            Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.
             */
            this._$parentView.append(this._$element);



            // 트리 목록에서 그룹 목록만 구해오기
            this._app = new Vue({
                  el: this._$element[0],
                  data: {
                        parent:this,
                        activeLayerName:""
                  },

                  methods: {
                        on_selectExcelFile(){

                              var $importFile = $("#excel-file-input");

                              if ($importFile.length == 0) {
                                    alert("excel-file-input 아이디를 가진 input 태그가 존재하지 않습니다. ");
                                    return;
                              }


                              $importFile.attr("accept", ".xlsx, xlsx/*,.xls, xls/*");
                              // 기존 저장된 이벤트 제거
                              $importFile.off();
                              $importFile.val("");

                              // 생성되는 경우
                              $importFile.on("change", async (event) => {
                                    var result = await this.parent.excelFileLoader.parseExcelFile(event);
                                    this.parent.excelFileLoader.showInfo();

                                    this.parent.createPage();
                              });

                              $importFile.trigger("click");
                        },

                        on_change_file(event){
                              console.log("event ", event);
                        },

                        setActiveLayerName(layerName){
                              this.activeLayerName = layerName;
                        }
                  }
            })


            return this._app;

      }

      get notificationList(){
            return [
                  EditorProxy.NOTI_CHANGE_ACTIVE_LAYER
            ]
      }


      /*
      noti가 온 경우 실행
      call : mediator에서 실행
       */
      handleNotification(note){
            switch(note.name){

                  case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER :
                        this.noti_changeActiveLayer(note.getBody());
                        break;
            }
      }

      noti_changeActiveLayer(layerName){
            this._app.setActiveLayerName(layerName);

      }


      async createPage(){


            console.log("this.excelFileLoader.sourceCompositionData ", this.excelFileLoader.sourceCompositionData[0]);
            //let positionData = this.excelFileLoader.sourceCompositionData[0];
            this.excelFileLoader.sourceCompositionData.forEach((positionData, index)=>{
                  this.create3DComponent(positionData, index);
            });

      }



      create3DComponent(positionData,rowIndex){

            // componentInstance 생성
            var componentInstance = wemb.componentLibraryManager.createComponentInstance("FrameComponent");;
            // 컴포넌트 메타 프로퍼티 생성
            var comName = "com_d_"+rowIndex;


            let componentInstanceInfoVO = {
                  componentName:"NObjLoaderComponent",
                  initProperties:{
                        props:{
                              "setter": {
                                    "lock": false,
                                    "visible": true,
                                    "position": {
                                          "x": positionData.x,
                                          "y": positionData.y,
                                          "z": positionData.z
                                    },
                                    "size": {
                                          "x": 1.7999999523162842,
                                          "y": 1.7999999523162842,
                                          "z": 0.5
                                    },
                                    "rotation": {
                                          "x": "0",
                                          "y": "0",
                                          "z": "0"
                                    },
                                    "opacity": 1,
                                    "shadow": false,
                                    "wireframe": false,
                                    "componentName": "NObjLoaderComponent",
                                    "selectItem": {
                                          "path": "/output/resource/obj3d/43b26d1a-8b37-48a8-b682-356ed3582526/leakDetector_LD.obj",
                                          "mapPath": "/output/resource/obj3d/43b26d1a-8b37-48a8-b682-356ed3582526/maps",
                                          "name": "leakDetector_LD.zip"
                                    },
                                    "depth": 1
                              },
                              "label": {
                                    "label_using": "N"
                              }
                        }
                  }
            }

            this.sendNotification(EditorStatic.CMD_ADD_COM_INSTANCE_BY_NAME, componentInstanceInfoVO);

      }
}

const ExcelHeader={
      ID:"ID",
      x:"x",
      y:"y",
      z:"z"
}


class ExcelFileLoader {

      static get SHEET_NAME() {
            return "구성정보";
      }
      static get HEADER() {
            return Object.values(ExcelHeader);
      }

      static get START_DATA_ROW(){
            return 1;
      }

      constructor(){

            this.sourceCompositionData = [];

      }

      async parseExcelFile(e) {
            var file = e.target.files[0];
            var reader = new FileReader();
            if(reader==null) {
                  alert("FileReader를 지원하지 않는 브라우저입니다.");
                  return;
            }

            var fileName = file.name;

            return new Promise((resolve, reject)=>{
                  reader.onload =(e)=>{
                        var data = null;
                        /*
					e.target.result가 있는 경우는 기본
					content가 있는 경우 IE11 override한 readAsBinaryString()이 실행되는 경우임.
				*/
                        if(e.target.result)
                              data = e.target.result;
                        else
                              data = e.target.content;

                        // 이진 파일 유무 및 sheets 전조 유무 확인
                        var workbook = XLSX.read(data, {type: 'binary'});
                        if(workbook==null || workbook.hasOwnProperty("Sheets")==false){
                              reject(new Error("엑셀 파일이 정사적이지 않습니다. 확인 후 다시 실행해주세요."));
                              return;
                        }

                        // 구성정보 sheet 존재 유무 확인.
                        var compositionSheet = workbook.Sheets[ExcelFileLoader.SHEET_NAME];
                        if(compositionSheet==null){
                              reject(new Error(ExcelFileLoader.SHEET_NAME+" sheet가 존재하지 않습니다. 확인 후 다시 실행해주세요."));
                              return;
                        }
                        /////////////////////////////////



                        // row정보 출력
                        this.sourceCompositionData = XLSX.utils.sheet_to_json(compositionSheet, {range:ExcelFileLoader.START_DATA_ROW, raw:true,  header:ExcelFileLoader.HEADER});

                        resolve(true);

                  }; //end onload

                  reader.readAsBinaryString(file);
            });
      }

      getCelData(formulae, selName){
            var selData = formulae.find(function(data){
                  return data.indexOf(selName+"='")!=-1;
            })


            if(selData!=undefined){
                  return selData.replace(selName+"='","");
            }

            return selData;
      }

      /*
       배열 데이터를 row로 분리
       */
      parseSourceCompositionDataToRowCompositionData(){
            if(this.rowCompositionData){
                  this.rowCompositionData.clear();
            }
            /////////////



            this.sourceCompositionData.forEach((rowData)=>{

                  if(this.rowCompositionData.has(rowData.ROW)==false){
                        this.rowCompositionData.set(rowData.ROW, [])
                  }
                  let dataList = this.rowCompositionData.get(rowData.ROW);
                  dataList.push(rowData)
            });
      }



      // 확장자를 제외시킨 파일 이름만 구하기.
      getFileName(name){
            var index = name.lastIndexOf(".");
            if(index!=-1){
                  return name.slice(0, index);
            }

            return name;
      }


      showInfo(){

            console.log(" sourceCompositionData = ", this.sourceCompositionData)
      }

}

if (!FileReader.prototype.readAsBinaryString) {
      console.log('readAsBinaryString definition not found');

      FileReader.prototype.readAsBinaryString = function (fileData) {
            var binary = '';
            var pk = this;
            var reader = new FileReader();

            reader.onload = function (e) {
                  var bytes = new Uint8Array(reader.result);
                  var length = bytes.byteLength;
                  for (var i = 0; i < length; i++) {
                        var a = bytes[i];

                        var b = String.fromCharCode(a)
                        binary += b;
                  }

                  /*
			pk.content를 pk.result로 하지 않는 이유는?
			FileReader 내부에서 result를 초기화 시킴
			 */
                  pk.content = binary;

                  var event = document.createEvent("Event");
                  event.initEvent("load", false, true);
                  pk.dispatchEvent(event)


            }

            reader.readAsArrayBuffer(fileData);
      }
}


window.EWPPanel = EWPPanel;
