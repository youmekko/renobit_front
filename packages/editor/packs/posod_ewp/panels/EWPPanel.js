
class EWPPanel extends ExtensionPanelCore {

      constructor(){
            super();

            this._app = null;
            this.excelFileLoader = new ExcelFileLoader();

      }


      /*
	call :
		인스턴스 생성 후 실행
		단, 아직 화면에 붙지 않은 상태임.

	 */
      create() {

            let template =
                  `
            <div style="overflow: auto;padding:8px;">
                  <button class="el-button el-button--primary" style="width:100%;" @click="on_selectExcelFile()"> Excel 파일 선택하기</button>
                  <input type="file" id="excel-file-input" @change="on_change_file($event)" style="display:none"/></p>
            </div>
      `
            this._$element = $(template);

            if(this._$parentView==null) {
                  console.log("Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.")
                  return;
            }


            /*
            중요:
            Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.
             */
            this._$parentView.append(this._$element);



            // 트리 목록에서 그룹 목록만 구해오기
            this._app = new Vue({
                  el: this._$element[0],
                  data: {
                        parent:this
                  },

                  methods: {
                        on_selectExcelFile(){
                              var $importFile = $("#excel-file-input");

                              if ($importFile.length == 0) {
                                    alert("excel-file-input 아이디를 가진 input 태그가 존재하지 않습니다. ");
                                    return;
                              }


                              $importFile.attr("accept", ".xlsx, xlsx/*,.xls, xls/*");
                              // 기존 저장된 이벤트 제거
                              $importFile.off();
                              $importFile.val("");

                              // 생성되는 경우
                              $importFile.on("change", async (event) => {
                                    var result = await this.parent.excelFileLoader.parseExcelFile(event);
                                    this.parent.excelFileLoader.showInfo();

                                    //this.parent.createPage();
                              });

                              $importFile.trigger("click");
                        },

                        on_change_file(event){
                              console.log("event ", event);
                        }
                  }
            })


            return this._app;

      }

      get notificationList(){
            return [

            ]
      }


      /*
      noti가 온 경우 실행
      call : mediator에서 실행
       */
      handleNotification(note){

      }


      async createPage(){


            // 1. 템플릿 페이지의 pageid 구하기
            let pageId=window.wemb.pageManager.getPageIdByName(this.excelFileLoader.templatePageName);
            if(pageId==null){
                  alert(`읽어들일 ${this.excelFileLoader.templatePageName} 템플릿 페이지가 존재하지 않습니다. 확인 후 다시 실행해 주세요.`);
                  return;
            }


            // 2. 페이지 편집 정보 구하기
            let serverUrl = window.wemb.configManager.workUrl;
            let data = {
                  "id": "editorService.getPageInfo",
                  "params": {
                        "page_id": pageId,
                        "user_id" : window.wemb.configManager.userId,
                        "isViewer" : false
                  }
            };
            let result = await window.wemb.$http.post(serverUrl, data);
            if(result==null || result==false){
                  alert("파일 정보를 읽어들이는 도중 에러 발생 ");
                  return;
            }
            ///////////////






            ///////////////
            /*
            // 환경설정 정보 구하기
             */

            let pageData = result.data.data;
            let tempComInstanceMap = new Map();
            let realComInstanceList = [];

            let configIndex = -1;
            let configInstance = pageData.content_info.two_layer.find((comInstance, index)=>{
                  configIndex = index;
                  return comInstance.name=="config";
            })

            if(configInstance==null){
                  alert("페이지에 환경설정 정보가 존재하지 않습니다. 확인 후 다시 실행해주세요.");
                  return;
            }

            // 페이지 정보에서 config component 삭제
            pageData.content_info.two_layer.splice(configIndex, 1);
            ////////////////////







            ////////////////////
            // 페이지 이름 설정하기
            // 기본으로 페이지 이름은 파일이름으로
            pageData.page_info.name=this.excelFileLoader.fileName;

            let configInfo = JSON.parse(configInstance.props.setter.text);
            this.excelFileLoader.rowCompositionData.forEach((rowInfoList, rowIndex)=>{
                  if(rowInfoList.length<=0)
                        return;



                  // rowIndex에 해당하는 config 정보 구하기.
                  let rowConfig = configInfo["row_"+rowIndex];
                  if(rowConfig==null)
                        return;




                  /////////////////////////
                  // header 부분 처리
                  if(rowIndex==0){
                        this.createHeaderComponent(rowConfig, rowInfoList, pageData );
                        return;
                  }

                  /////////////////////////







                  /////////////////////////
                  // main 부분 처리
                  var startPoint = {
                        x:rowConfig.x,
                        y:rowConfig.y
                  }

                  // rowInfo 개수만큼 for문 돌며 컴포넌트 인스턴스 생성하기
                  rowInfoList.forEach((rowInfo, index)=>{
                        let lineName = `row_${rowIndex}_svg_col_${index+1}`;
                        var LINE_GAB = 8;
                        this.drawSVGVLine(lineName+"_1", rowConfig.x-LINE_GAB,rowConfig.y-rowConfig.line_height, rowConfig.line_height, pageData);
                        this.drawSVGVLine(lineName+"_2", rowConfig.x+LINE_GAB,rowConfig.y-rowConfig.line_height, rowConfig.line_height+15, pageData, true);
                        let symbolName = `row_${rowIndex}_com_${index+1}`;
                        let comMetaInfo = this.createSymbolComponent(symbolName,  rowInfo, rowConfig, pageData);

                        // 50은 textcomponent height값.

                        var textInfo = {
                              x:rowConfig.x - Math.floor(TextComponentInfo.WIDTH/2),
                              y:rowConfig.y-(rowConfig.line_height+comMetaInfo.props.setter.height+TextComponentInfo.HEIGHT),
                              width:TextComponentInfo.WIDTH,
                              height:TextComponentInfo.HEIGHT
                        }

                        this.createTextComponent(symbolName+"_text", rowInfo, textInfo, pageData)


                        // line x 위치 설정
                        rowConfig.x+=rowConfig.step;


                  })

            })

            console.log("@@ pageData 55", pageData);


            this.sendNotification(EditorStatic.CMD_SAVE_DATA_TO_PAGE, pageData);

      }

      createHeaderComponent(headerConfigs, rowInfoList, pageData){
            if(headerConfigs==null)
                  return;


            // header 부분 처리
            rowInfoList.forEach((rowInfo, index)=>{
                  var config = headerConfigs[index];

                  // rowInfo에서 장비종류 값 구하기.
                  let equipmentType = rowInfo[ExcelHeader.EQUIPMENT_TYPE];

                  // equipmentType에 해당하는 컴포넌트 정보 구하기
                  var comVO= EquipmentTypeToComponent[equipmentType];

                  // component Instance 생성
                  var componentInstance = null;
                  if(comVO==null)
                        componentInstance = wemb.componentLibraryManager.createComponentInstance("FrameComponent");
                  else
                        componentInstance = wemb.componentLibraryManager.createComponentInstance(comVO.name);

                  var symbolName =`row_0_com_${index+1}`
                  // 컴포넌트 메타 프로퍼티 생성
                  let tempInitMetaProperties = $.extend(true, {
                        name:symbolName,
                        id:"__", //아이디 값은  command에서 다시 설정됨.
                        layerName:"twoLayer",
                        props:{
                              setter:{
                                    //x:(config.x-(comVO.initProperties.props.setter.width/2)),
                                    //y:config.y-(comVO.initProperties.props.setter.height/2)
                                    x:config.x,
                                    y:config.y
                              }
                        }
                  },comVO.initProperties);

                  // 컴포넌트에 프로퍼티 적용하기
                  componentInstance.create(tempInitMetaProperties);
                  componentInstance._onImmediateUpdateDisplay();

                  pageData.content_info.two_layer.push(componentInstance.serializeMetaProperties());

                  var y = tempInitMetaProperties.props.setter.y-TextComponentInfo.HEIGHT;
                  if(index==1){
                        y = tempInitMetaProperties.props.setter.y + tempInitMetaProperties.props.setter.height
                  }
                  var textInfo = {
                        x:config.x -Math.floor(TextComponentInfo.WIDTH/2)+(tempInitMetaProperties.props.setter.width/2),
                        y:y,
                        width:TextComponentInfo.WIDTH,
                        height:TextComponentInfo.HEIGHT
                  }
                  this.createTextComponent(symbolName+"_text", rowInfo, textInfo, pageData)

            });
      }




      drawSVGVLine(comName, startX, startY, strokeHeight,pageData, dotType=false){

            var startDotSvgComInstance = wemb.componentLibraryManager.createComponentInstance("SVGCircleComponent");
            var circleSize=8;
            // 컴포넌트 메타 프로퍼티 생성
            let startDotMetaProperty = {
                  name:comName+"_start",
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              width:circleSize,
                              height:circleSize,
                              x:startX-(circleSize/2),
                              y:startY-(circleSize/2)
                        },
                        style:{
                              "fill":"rgb(0,0,0)",
                              "stroke-width":0
                        }
                  }
            };

            // 컴포넌트 생성
            startDotSvgComInstance.create(startDotMetaProperty);
            pageData.content_info.two_layer.push(startDotSvgComInstance.serializeMetaProperties())



            var svgLineComponentInstance = wemb.componentLibraryManager.createComponentInstance("SVGLineComponent");
            // 컴포넌트 메타 프로퍼티 생성
            let tempInitMetaProperties2 = {
                  name:comName,
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              width:0,
                              height:strokeHeight,
                              x:startX,
                              y:startY,
                              points:[{
                                    x:startX,
                                    y:startY
                              },{
                                    x:startX,
                                    y:startY+strokeHeight
                              }]

                        },
                        style:{
                              "stroke-width":3,
                              "stroke":"rgb(0, 0, 0)",

                        }
                  }
            };
            if(dotType){
                  tempInitMetaProperties2.props.style["stroke-dasharray"]="5 5"
            }
            // 컴포넌트 생성
            svgLineComponentInstance.create(tempInitMetaProperties2);
            svgLineComponentInstance._onImmediateUpdateDisplay();
            var serializeProperties = svgLineComponentInstance.serializeMetaProperties();
            //serializeProperties.props.setter.width=1;
            pageData.content_info.two_layer.push(serializeProperties)



            var endDotSvgComInstance = wemb.componentLibraryManager.createComponentInstance("SVGCircleComponent");
            // 컴포넌트 메타 프로퍼티 생성
            let endDotMetaProperty = {
                  name:comName+"_end",
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              width:circleSize,
                              height:circleSize,
                              x:startX-(circleSize/2),
                              y:startY+strokeHeight-(circleSize/2)
                        },
                        style:{
                              "fill":"rgb(0,0,0)",
                              "stroke-width":0
                        }
                  }
            };

            // 컴포넌트 생성
            endDotSvgComInstance.create(endDotMetaProperty);
            pageData.content_info.two_layer.push(endDotSvgComInstance.serializeMetaProperties())
      }

      createSymbolComponent(comName, rowInfo, rowConfig, pageData){

            // rowInfo에서 장비종류 값 구하기.
            let equipmentType = rowInfo[ExcelHeader.EQUIPMENT_TYPE];

            // componentVO 생성
            var comVO= EquipmentTypeToComponent[equipmentType];

            // componentInstance 생성
            var componentInstance = null;
            if(comVO==null)
                  componentInstance = wemb.componentLibraryManager.createComponentInstance("FrameComponent");
            else
                  componentInstance = wemb.componentLibraryManager.createComponentInstance(comVO.name);

            var severityList = ["normal", "critical", "major", "minor", "warning"];
            var selectedSeverity = severityList[Math.floor(Math.random()*5)];
            // 컴포넌트 메타 프로퍼티 생성
            let tempInitMetaProperties = $.extend(true, {
                  name:comName,
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              x:(rowConfig.x-(comVO.initProperties.props.setter.width/2)),
                              y:rowConfig.y-(rowConfig.line_height+comVO.initProperties.props.setter.height),
                              severity:selectedSeverity
                        }

                  }
            },comVO.initProperties);

            // 컴포넌트 생성
            componentInstance.create(tempInitMetaProperties);
            pageData.content_info.two_layer.push(componentInstance.serializeMetaProperties())

            return tempInitMetaProperties;
      }

      createTextComponent(comName, rowInfo, textInfo, pageData){
            var componentInstance = wemb.componentLibraryManager.createComponentInstance("TextComponent");
            // 컴포넌트 메타 프로퍼티 생성


            let tempInitMetaProperties ={
                  name:comName,
                  id:"__", //아이디 값은  command에서 다시 설정됨.
                  layerName:"twoLayer",
                  props:{
                        setter:{
                              x:textInfo.x,
                              y:textInfo.y,
                              width:textInfo.width,
                              height:textInfo.height,
                              text:rowInfo[ExcelHeader.EQUIPMENT_ALIAS]+"\n"+rowInfo[ExcelHeader.IP]
                        },
                        font: {
                              "text_align": "center",
                              font_size:10,
                              font_height:10,
                              font_weight:"bold"
                        }
                  }
            };
            console.log("@@ tempInitMetaProperties6222 ", tempInitMetaProperties)

            // 컴포넌트 생성
            componentInstance.create(tempInitMetaProperties);
            componentInstance._onImmediateUpdateDisplay();
            pageData.content_info.two_layer.push(componentInstance.serializeMetaProperties())
      }

}

const ExcelHeader={
      ID:"ID",
      EQUIPMENT_NAME:"장비명",
      EQUIPMENT_ALIAS:"장비Alias",
      IP:"IP",
      EQUIPMENT_TYPE:"장비종류",
      MANUFACTURER:"제조사",
      MODEL_NAME:"모델명",
      ROW:"ROW"

}


const EquipmentTypeToComponent = {
      "백본 스위치":{
            "name": "BasicStateClipComponent",
            "initProperties": {
                  "props": {
                        "setter": {
                              "width":66,
                              "height":90,
                              "info": {
                                    "name": "network007",
                                    "path": "/client/packs/2d_pack/components/symbol/network_flat_theme/NetworkFlat007/res/"
                              },
                              "isSaved":true
                        }
                  }
            }
      },
      "스위치":{
            "name": "BasicStateClipComponent",
            "initProperties": {
                  "props": {
                        "setter": {
                              "width":100,
                              "height":50,
                              "info": {
                                    "name": "network005",
                                    "path": "/client/packs/2d_pack/components/symbol/network_flat_theme/NetworkFlat005/res/"
                              },
                              "isSaved":true
                        }
                  }
            }
      }
}

const TextComponentInfo = {
      WIDTH:140,
      HEIGHT:50
}

class ExcelFileLoader {

      static get SHEET_NAME() {
            return "구성정보";
      }
      static get HEADER() {
            return Object.values(ExcelHeader);
      }

      static get START_DATA_ROW(){
            return 1;
      }

      constructor(){

            this.creatingPageDescription="";
            this.fileName ="";
            this.templatePageName = "";
            this.sourceCompositionData = [];
            this.rowCompositionData = new Map();

      }

      async parseExcelFile(e) {
            var file = e.target.files[0];
            var reader = new FileReader();
            if(reader==null) {
                  alert("FileReader를 지원하지 않는 브라우저입니다.");
                  return;
            }

            var fileName = file.name;

            return new Promise((resolve, reject)=>{
                  reader.onload =(e)=>{
                        var data = null;
                        /*
					e.target.result가 있는 경우는 기본
					content가 있는 경우 IE11 override한 readAsBinaryString()이 실행되는 경우임.
				*/
                        if(e.target.result)
                              data = e.target.result;
                        else
                              data = e.target.content;

                        // 이진 파일 유무 및 sheets 전조 유무 확인
                        var workbook = XLSX.read(data, {type: 'binary'});
                        if(workbook==null || workbook.hasOwnProperty("Sheets")==false){
                              reject(new Error("엑셀 파일이 정사적이지 않습니다. 확인 후 다시 실행해주세요."));
                              return;
                        }

                        // 구성정보 sheet 존재 유무 확인.
                        var compositionSheet = workbook.Sheets[ExcelFileLoader.SHEET_NAME];
                        if(compositionSheet==null){
                              reject(new Error(ExcelFileLoader.SHEET_NAME+" sheet가 존재하지 않습니다. 확인 후 다시 실행해주세요."));
                              return;
                        }
                        /////////////////////////////////



                        // row정보 출력
                        this.sourceCompositionData = XLSX.utils.sheet_to_json(compositionSheet, {range:ExcelFileLoader.START_DATA_ROW, raw:true,  header:ExcelFileLoader.HEADER});

                        console.log("sourceCompositionData = ", this.sourceCompositionData);
                        //this.parseSourceCompositionDataToRowCompositionData();
                        resolve(true);

                  }; //end onload

                  reader.readAsBinaryString(file);
            });
      }

      getCelData(formulae, selName){
            var selData = formulae.find(function(data){
                  return data.indexOf(selName+"='")!=-1;
            })


            if(selData!=undefined){
                  return selData.replace(selName+"='","");
            }

            return selData;
      }

      /*
       배열 데이터를 row로 분리
       */
      parseSourceCompositionDataToRowCompositionData(){
            if(this.rowCompositionData){
                  this.rowCompositionData.clear();
            }
            /////////////



            this.sourceCompositionData.forEach((rowData)=>{

                  if(this.rowCompositionData.has(rowData.ROW)==false){
                        this.rowCompositionData.set(rowData.ROW, [])
                  }
                  let dataList = this.rowCompositionData.get(rowData.ROW);
                  dataList.push(rowData)
            });
      }



      // 확장자를 제외시킨 파일 이름만 구하기.
      getFileName(name){
            var index = name.lastIndexOf(".");
            if(index!=-1){
                  return name.slice(0, index);
            }

            return name;
      }


      showInfo(){
            console.log(" fileName = ", this.fileName);

            console.log(" creatingPageDescription = ", this.creatingPageDescription);
            console.log(" templatePageName2222 = ", this.templatePageName);
            console.log(" sourceCompositionData = ", this.sourceCompositionData)
            console.log(" rowCompositionData = ", this.rowCompositionData)
      }

}

if (!FileReader.prototype.readAsBinaryString) {
      console.log('readAsBinaryString definition not found');

      FileReader.prototype.readAsBinaryString = function (fileData) {
            var binary = '';
            var pk = this;
            var reader = new FileReader();

            reader.onload = function (e) {
                  var bytes = new Uint8Array(reader.result);
                  var length = bytes.byteLength;
                  for (var i = 0; i < length; i++) {
                        var a = bytes[i];

                        var b = String.fromCharCode(a)
                        binary += b;
                  }

                  /*
			pk.content를 pk.result로 하지 않는 이유는?
			FileReader 내부에서 result를 초기화 시킴
			 */
                  pk.content = binary;

                  var event = document.createEvent("Event");
                  event.initEvent("load", false, true);
                  pk.dispatchEvent(event)


            }

            reader.readAsArrayBuffer(fileData);
      }
}


window.EWPPanel = EWPPanel;
