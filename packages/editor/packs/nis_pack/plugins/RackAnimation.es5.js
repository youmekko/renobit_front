"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RackAnimation = function (_ExtensionPluginCore) {
      _inherits(RackAnimation, _ExtensionPluginCore);

      function RackAnimation() {
            _classCallCheck(this, RackAnimation);

            /*
            2018.06.01 ddandongne
             - 임시적으로 적용 상태.
                  - 추후 instanceManagerOnPageViewer 클래스에서 처리 해야함.
             - 전역에서 접근할 rackAnimation 클래스 인스턴스
             - RackAnimation 인스턴스는 CreateExtensionCommand에서 config정보에 들어 있는 값을 읽어 생성함.
             */
            var _this = _possibleConstructorReturn(this, (RackAnimation.__proto__ || Object.getPrototypeOf(RackAnimation)).call(this));

            window.wemb["extension"] = {};
            return _this;
      }

      _createClass(RackAnimation, [{
            key: "onDoubleClick_Target",
            value: function onDoubleClick_Target(event) {
                  event.stopPropagation();
                  var instance = this.instances.get(event.target);
                  if (instance) {
                        var selected = !instance.selected;
                        if (selected) {
                              this.transitionInFromAssetID(instance.assetId);
                        } else {
                              this.transitionOutFromInstance(instance);
                        }
                        this.toggleBoxHelperByItem();
                  }
            }
      }, {
            key: "onSelectClientItem",
            value: function onSelectClientItem(event) {
                  var selectUnit = event.data.selectItem;
                  if(!selectUnit.data.id){
                        this.transitionInFromAssetID(event.target.data.id, selectUnit.data.id);
                  }
            }
      }, {
            key: "onPopupClose",
            value: function onPopupClose() {
                  this.transitionOut();
                  this._popupInstance = null;
            }

            /**
             *  팝업 요청 함수
             *  배치 타입에 따라 요청하는 팝업 페이지 이름이 다름
             *  refresh를 이용하면 이전 타입을 비교해 요청을 다르게 해야 한다.
             *  결론 close - open 을 이용하는게 답이다.
             *
             *   parameter 구성
             * */

      }, {
            key: "requestPopup",
            value: function requestPopup(popupInfo) {

                  if (this._popupInstance != null) {
                        if (this._popupInstance.name == popupInfo.name) {
                              var popup = this._popupInstance;
                              popup.innerPage.updateDisplay(popupInfo.params);
                              return;
                        } else {
                              this._popupInstance.closed();
                        }
                  }

                  // 팝업 호출
                  this._popupInstance = window.wemb.popupManager.open(popupInfo.name, popupInfo);
                  // 팝업 타입 설정
                  this._popupInstance.name = popupInfo.name;

                  if (this._popupInstance && !this._popupInstance.hasEventListener("event/closedPopup", this._popCloseHandler)) {
                        this._popupInstance.addEventListener("event/closedPopup", this._popCloseHandler);
                  }
            }

            /**
             *  자산id 정보로 해당 컴포넌트 포커스 처리 및 팝업 요청 처리 수행
             *  unitId값이 없으면 실장 요소는 포커스 처리 하지 않음
             *  @params assetId : 자산 id
             *  @params unitID  : 실장 id
             * */

      }, {
            key: "transitionInFromAssetID",
            value: function transitionInFromAssetID(assetId, unitId) {

                  var selectInstances = this.getInstanceById(assetId);

                  if (selectInstances.length > 0) {
                        var selectAsset = selectInstances.shift();
                        var popupObj = this.composePopupInfo(selectAsset.popupId, selectAsset.assetId);
                        popupObj.params.mountData = selectAsset.data;
                        var selectUnit = void 0;
                        if (!selectAsset.selected) {
                              this.transitionInFromInstance(selectAsset);
                        }
                        selectUnit = selectAsset.getUnitById(unitId);
                        selectAsset.focusSelectUnit(selectUnit);
                        this.toggleBoxHelperByItem(selectUnit);
                        // 실장 정보가 있다면 실장을 선택처리하고 팝업 요청
                        if (selectUnit != null && selectUnit.selected) {
                              popupObj.params.assetId = selectUnit.data.id;
                              popupObj.params.mode = "mountInfo";
                        }
                        this.requestPopup(popupObj);
                  } else {

                        this.transitionOut();
                  }
            }

            /**
             * 팝업 페이지 호출 시 전달 파라미터 구성
             * */

      }, {
            key: "composePopupInfo",
            value: function composePopupInfo(popName, assetId) {
                  var info = {
                        name: popName,
                        title: "Asset Info",
                        className: "popup",
                        params: {
                              popupOwner: this,
                              assetId: assetId,
                              mode: ''
                        }
                  };
                  if (popName.indexOf("Rack") > -1) {
                        info.x = 1140;
                        info.y = 200;
                        info.width = 750;
                        info.height = 625;
                        info.params.mode = "rackInfo";
                  } else {
                        info.x = 1300;
                        info.y = 250;
                        info.width = 550;
                        info.height = 480;
                  }
                  return info;
            }

            // 자산 하이라이트 처리

      }, {
            key: "requestFocusAssetById",
            value: function requestFocusAssetById(assetId) {
                  this.requestFocusClear();
                  var instances = this.getInstanceById(assetId);
                  if (instances.length >= 0) {
                        var instance = instances.shift();
                        instance.hilight = true;
                        this.threes.boxHelper.setFromObject(instance.transitionTarget);
                        this.threes.boxHelper.material.visible = true;
                  }
            }

            // 하이라이트 제거

      }, {
            key: "requestFocusClear",
            value: function requestFocusClear() {
                  this.threes.boxHelper.target = null;
                  this.threes.boxHelper.material.visible = false;
            }

            // assetId에 해당하는 컴포넌트 반환

      }, {
            key: "getInstanceById",
            value: function getInstanceById(assetId) {
                  var instanceArr = Array.from(this.instances.values());
                  var selectInstances = instanceArr.filter(function (instance) {
                        return instance.assetId == assetId;
                  });
                  return selectInstances;
            }
      }, {
            key: "transitionOutFromInstance",
            value: function transitionOutFromInstance(instance) {
                  instance.transitionOut();
                  this.toggleVisibleOtherInstance(instance, true);
                  this.cameraTransitionOut();
                  // 팝업 있으면 닫기
                  if (this._popupInstance) {
                        this._popupInstance.closed();
                  }
            }
      }, {
            key: "toggleVisibleOtherInstance",
            value: function toggleVisibleOtherInstance(currentSelect, bValue) {
                  this.instances.forEach(function (instance) {
                        if (instance != currentSelect) {
                              instance.visible = bValue;
                              if (instance.selected) {
                                    instance.transitionOut();
                              }
                        }
                  });
            }

            // 특정 컴포넌트 포커스 처리

      }, {
            key: "transitionInFromInstance",
            value: function transitionInFromInstance(instance) {
                  instance.transitionIn();
                  if (!instance.visible) {
                        instance.visible = true;
                  }
                  this.toggleVisibleOtherInstance(instance, false);
                  // 카메라 포커스 처리
                  this.cameraTransitionInInstance(instance);
                  // 검색 하이라이트 제거
                  this.requestFocusClear();
            }

            // 카메라 포커스 처리 이때 툴팁기능 off

      }, {
            key: "cameraTransitionInInstance",
            value: function cameraTransitionInInstance(instance) {
                  // instance의 스케일 정보를 초기 벡터에 반영
                  var offset = new THREE.Vector3(0, 0, 30 * instance.elementSize.z / instance.size.z);
                  offset = offset.applyMatrix4(instance.appendElement.matrixWorld);

                  this.threes.mainControls.transitionFocusInTarget(offset, instance.appendElement.position.clone().add({ x: 0, y: 10, z: 0 }), 1);
                  WVToolTipManager.setEnabled(false);
            }

            // 카메라 포커스 아웃 처리 이때 툴팁기능 on

      }, {
            key: "cameraTransitionOut",
            value: function cameraTransitionOut() {
                  this.threes.mainControls.transitionFocusInTarget(this.threes.mainControls.position0.clone(), this.threes.mainControls.target0.clone(), 1);
                  this.toggleBoxHelperByItem();
                  WVToolTipManager.setEnabled(true);
            }

            // 모든 객체 초기화 배경 클릭 시 호출 됨

      }, {
            key: "transitionOut",
            value: function transitionOut() {
                  this.toggleVisibleOtherInstance(null, true);
                  this.cameraTransitionOut();
            }

            // 선택 된 장비가 있거나 장비가 selected상태이면 boxHelper 적용

      }, {
            key: "toggleBoxHelperByItem",
            value: function toggleBoxHelperByItem(selectItem) {
                  if (selectItem && selectItem.selected) {
                        this.threes.boxHelper.setFromObject(selectItem.skin);
                        this.threes.boxHelper.material.visible = true;
                  } else {
                        this.threes.boxHelper.target = null;
                        this.threes.boxHelper.material.visible = false;
                  }
            }
      }, {
            key: "create",


            /*
            call :
                  인스턴스 생성 후 실행
                  단, 아직 화면에 붙지 않은 상태임.
              */
            value: function create() {}

            /*
            noti가 온 경우 실행
            call : mediator에서 실행
             */

      }, {
            key: "handleNotification",
            value: function handleNotification(note) {
                  switch (note.name) {
                        case ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE:
                              this.noti_changePageLoadingState(note.getBody());
                              break;
                        case ViewerProxy.NOTI_CLOSED_PAGE:
                              this.noti_closePage();
                              break;
                  }
            }
      }, {
            key: "initProperties",
            value: function initProperties() {
                  this._popupInstance = null;
                  this.instances = new Map();
                  this.threes = window.wemb.threeElements;
                  this._clientItemSelectHandler = this.onSelectClientItem.bind(this);
                  this._dblClickHandler = this.onDoubleClick_Target.bind(this);
                  this._popCloseHandler = this.onPopupClose.bind(this);
                  window.wemb.extension.rackAnimation = this;
            }
      }, {
            key: "destroy",
            value: function destroy() {
                  if (this.instances) {
                        WVToolTipManager.destroy();
                        this.removeEventListener();
                        this.instances.clear();
                  }
                  this.instances = null;
                  this.threes = null;
                  this._clientItemSelectHandler = null;
                  this._dblClickHandler = null;
                  this._popCloseHandler = null;
                  if (this._popupInstance) {
                        this._popupInstance.closed();
                  }
                  this._popupInstance = null;
                  window.wemb.extension.rackAnimation = null;
            }
      }, {
            key: "noti_changePageLoadingState",
            value: function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(pageLoadingState) {
                        var _this2 = this;

                        var spinner, assetsInfos, proxy;
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                if (!(pageLoadingState == "ready")) {
                                                      _context.next = 6;
                                                      break;
                                                }

                                                spinner = Spinner.getInstance();

                                                spinner.show(document.body);
                                                spinner.setStyle({ "border-color": "#ffffff",
                                                      "border-left-color": "transparent",
                                                      "border-right-color": "transparent" });
                                                this.initProperties();
                                                return _context.abrupt("return");

                                          case 6:
                                                if (!(pageLoadingState == "loaded")) {
                                                      _context.next = 18;
                                                      break;
                                                }

                                                _context.next = 9;
                                                return this.loadJSONData();

                                          case 9:
                                                assetsInfos = _context.sent;

                                                assetsInfos = this.parseProvider(assetsInfos.rackInfo, assetsInfos.mountList); // 실장 정보 구성
                                                proxy = this._facade.retrieveProxy(ViewerProxy.NAME);
                                                // 자산 컴포넌트 목록 구성

                                                proxy.comInstanceList.forEach(function (instance) {
                                                      if (instance instanceof AssetBaseComponent) {
                                                            _this2.instances.set(instance.transitionTarget, instance);
                                                            instance.toolTipPropertyName = "loc_surf_nm";
                                                      }
                                                });
                                                // 컴포넌트에 자산 데이터 설정
                                                this.composeInstanceData(assetsInfos);
                                                this.registEventListener();
                                                this.initTooltip();
                                                this.checkqueryParams();
                                                Spinner.getInstance().hide();

                                          case 18:
                                          case "end":
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this);
                  }));

                  function noti_changePageLoadingState(_x) {
                        return _ref.apply(this, arguments);
                  }

                  return noti_changePageLoadingState;
            }()
      }, {
            key: "initTooltip",
            value: function initTooltip() {
                  // 3d 라벨이 그려지는 컨테이너에 추가
                  WVToolTipManager.create(this.threes, document.querySelector(".label-container"));
                  this.instances.forEach(function (instance) {
                        WVToolTipManager.add(instance);
                        instance.toolTipPropertyName = "loc_surf_nm";
                  });
                  WVToolTipManager.setEnabled(true);
            }

            /**
             파라미터로 넘어 온 값으로 포커스 처리
             url에 파라미터가 넘어오면 그 값을 이용한 포커스 처리
             */

      }, {
            key: "checkqueryParams",
            value: function checkqueryParams() {
                  // 쿼리 파람 처리
                  var qParams = window.wemb.configManager.queryParams;
                  var assetId = qParams.rack_id || ""; // 랙 자산 id
                  var unitId = qParams.am_asset_id || ""; // 실장 자산 id
                  if (assetId) {
                        this.transitionInFromAssetID(assetId, unitId);
                  }
            }

            // 배치된 자산에 데이터 맵핑

      }, {
            key: "composeInstanceData",
            value: function composeInstanceData(dataProvider) {
                  this.instances.forEach(function (client) {
                        var data = dataProvider.filter(function (vo) {
                              return vo.am_asset_id == client.assetId;
                        });

                        client.data = data[0];
                  });
            }
      }, {
            key: "registEventListener",
            value: function registEventListener() {
                  var _this3 = this;

                  this.instances.forEach(function (instance) {
                        _this3.threes.domEvents.addEventListener(instance.transitionTarget, "dblclick", _this3._dblClickHandler, false);
                        instance.addEventListener("WVComponentEvent.SELECT_ITEM", _this3._clientItemSelectHandler);
                  });
            }
      }, {
            key: "removeEventListener",
            value: function removeEventListener() {
                  var _this4 = this;

                  this.instances.forEach(function (instance) {
                        _this4.threes.domEvents.removeEventListener(instance.transitionTarget, "dblclick", _this4._dblClickHandler, false);
                        instance.addEventListener("WVComponentEvent.SELECT_ITEM", _this4._clientItemSelectHandler);
                  });
            }

            ////////////////////////////

      }, {
            key: "parseProvider",
            value: function parseProvider(rackInfoList, mountList) {
                  var _this5 = this;

                  var results = [];
                  var errorList = [];
                  rackInfoList.map(function (value) {
                        value.items = mountList.filter(function (mount) {
                              return mount.rack_asset_id == value.am_asset_id;
                        });

                        var newData = _this5.parseData(value);
                        if (newData.hasOwnProperty("__error__") == true) {
                              errorList.push(newData.__error_unit__);
                        }

                        results.push(newData);
                  });
                  return results;
            }
      }, {
            key: "parseData",
            value: function parseData(data) {
                  var i = void 0,
                        j = void 0;
                  var max = data.max_unit_qty;
                  var items = data.items.filter(function (item) {
                        return parseInt(item.unit_use_qty) > 0;
                  });
                  var itemLen = items.length;
                  var occupyIndex = new Array(max + 1).fill(0);

                  for (i = 0; i < itemLen; i++) {
                        var item = items[i];
                        var endIndex = parseInt(item.unit_end_no);
                        var startIndex = parseInt(item.unit_start_no);
                        for (j = startIndex; j <= endIndex; j++) {
                              var volume = occupyIndex[j] + 1;
                              occupyIndex[j] = volume;
                        }
                  }

                  // order by startIndex
                  items.sort(function (a, b) {
                        var p1 = parseInt(a.unit_start_no);
                        var p2 = parseInt(b.unit_start_no);

                        if (p1 < p2) return -1;
                        if (p1 > p2) return 1;
                        return 0;
                  });

                  var indexArray = [];
                  for (i = 1; i <= max; i++) {
                        var indexItem = occupyIndex[i];
                        var pushItem = {};

                        for (j = 1; j <= indexItem; j++) {
                              pushItem[j] = 0;
                        }
                        indexArray[i] = pushItem;
                  }

                  for (i = 0; i < itemLen; i++) {
                        var _item2 = items[i];
                        var index = 1;
                        var s_index = parseInt(_item2.unit_start_no);
                        var e_index = parseInt(_item2.unit_end_no);

                        if (isNaN(index) == true) {
                              data.items = [];
                              data.__error__ = true;
                              data.__error_unit__ = _item.asset_no;
                              return data;
                        }

                        for (var tempIndex = s_index; tempIndex <= e_index; tempIndex++) {
                              var occupied = occupyIndex[tempIndex];
                              index = index >= occupied ? index : occupied;
                        }

                        _item2.columnWeight = 100 / index;

                        var indexObj = indexArray[s_index];
                        for (var key in indexObj) {
                              // 세로 영역의 occupied 계산
                              for (j = _item2.unit_use_qty; j > 0; j--) {
                                    indexArray[s_index + j - 1][key] = _item2.columnWeight * (key - 1);
                              }
                        }
                  }

                  for (i = 0; i < itemLen; i++) {
                        var _item3 = items[i];
                        var _s_index = parseInt(_item3.unit_start_no);

                        var _indexObj = indexArray[_s_index];
                        for (var _key in _indexObj) {
                              if (_indexObj[_key] != -1) {
                                    _item3.padding = _indexObj[_key];

                                    for (j = _item3.unit_use_qty; j > 0; j--) {
                                          indexArray[_s_index + j - 1][_key] = -1;
                                    }
                                    break;
                              }
                        }
                  }

                  data.items = items;

                  return data;
            }

            // 데이터 가져오기

      }, {
            key: "loadJSONData",
            value: function () {
                  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                              while (1) {
                                    switch (_context2.prev = _context2.next) {
                                          case 0:
                                                return _context2.abrupt("return", new Promise(function (resolve, reject) {
                                                      $.getJSON('./custom/assets/data.json', function (data) {
                                                            resolve(data);
                                                      });
                                                }));

                                          case 1:
                                          case "end":
                                                return _context2.stop();
                                    }
                              }
                        }, _callee2, this);
                  }));

                  function loadJSONData() {
                        return _ref2.apply(this, arguments);
                  }

                  return loadJSONData;
            }()
      }, {
            key: "loadData",
            value: function () {
                  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(pageId) {
                        var promise;
                        return regeneratorRuntime.wrap(function _callee3$(_context3) {
                              while (1) {
                                    switch (_context3.prev = _context3.next) {
                                          case 0:
                                                promise = new Promise(function (resolve, reject) {
                                                      var rackInfoList = null;
                                                      var mountList = null;
                                                      var datasetWorker = window.wemb.dataService.call(pageId, "rack_info_list", {});
                                                      if (datasetWorker && datasetWorker.item) {
                                                            datasetWorker.on("error", function (event) {
                                                                  reject(event);
                                                            });
                                                            datasetWorker.on("success", function (event) {
                                                                  rackInfoList = event.rowData;
                                                                  if (mountList != null) {
                                                                        resolve({ rackInfo: rackInfoList, mountList: mountList });
                                                                  }
                                                            });
                                                      }

                                                      var mountWorker = window.wemb.dataService.call(pageId, "mount_list", {});
                                                      if (mountWorker && mountWorker.item) {
                                                            mountWorker.on("error", function (event) {
                                                                  reject(event);
                                                            });
                                                            mountWorker.on("success", function (event) {
                                                                  mountList = event.rowData;
                                                                  if (rackInfoList != null) {
                                                                        resolve({ rackInfo: rackInfoList, mountList: mountList });
                                                                  }
                                                            });
                                                      }
                                                });
                                                return _context3.abrupt("return", promise);

                                          case 2:
                                          case "end":
                                                return _context3.stop();
                                    }
                              }
                        }, _callee3, this);
                  }));

                  function loadData(_x2) {
                        return _ref3.apply(this, arguments);
                  }

                  return loadData;
            }()
      }, {
            key: "noti_closePage",
            value: function noti_closePage() {
                  console.log("RackAnimation close-");
                  this.destroy();
            }
      }, {
            key: "notificationList",
            get: function get() {
                  return [ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE, ViewerProxy.NOTI_CLOSED_PAGE];
            }
      }]);

      return RackAnimation;
}(ExtensionPluginCore);

window.RackAnimation = RackAnimation;
