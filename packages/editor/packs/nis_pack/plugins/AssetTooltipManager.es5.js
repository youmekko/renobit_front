"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AssetTooltipManager = function (_ExtensionPluginCore) {
      _inherits(AssetTooltipManager, _ExtensionPluginCore);

      function AssetTooltipManager() {
            _classCallCheck(this, AssetTooltipManager);

            return _possibleConstructorReturn(this, (AssetTooltipManager.__proto__ || Object.getPrototypeOf(AssetTooltipManager)).call(this));
      }

      _createClass(AssetTooltipManager, [{
            key: "create",


            /*
            call :
                  인스턴스 생성 후 실행
                  단, 아직 화면에 붙지 않은 상태임.
              */
            value: function create() {}

            /*
            noti가 온 경우 실행
            call : mediator에서 실행
             */

      }, {
            key: "handleNotification",
            value: function handleNotification(note) {
                  // 뷰어 모드에서만 실행
                  if (window.wemb.configManager.isEditorMode == true) return;

                  switch (note.name) {
                        //case ViewerProxy.NOTI_ADD_COM_INSTANCE :
                        //this.noti_addComInstance(note.getBody());
                        //      break;
                        case ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE:
                              this.noti_changePageLoadingState(note.getBody());
                              break;
                        // case ViewerProxy.NOTI_CLOSED_PAGE:
                        //       this.noti_closePage();
                        //       break;
                  }
            }
      }, {
            key: "noti_changePageLoadingState",
            value: function noti_changePageLoadingState(pageLoadingState) {
                  // 모든 정보가 로드된 상태.
                  if (pageLoadingState == "loaded") {
                        console.log("");
                        var comList = window.wemb.mainPageComponent.$threeLayer;
                        comList.forEach(function (comInstance) {
                              if (comInstance instanceof AssetBaseComponent) {
                                    comInstance.toolTipPropertyName = AssetTooltipManager.TOOTIP_PROPERTY_NAME;
                              }
                        });
                  }
            }
      }, {
            key: "notificationList",
            get: function get() {
                  return [ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE, ViewerProxy.NOTI_ADD_COM_INSTANCE, ViewerProxy.NOTI_CLOSED_PAGE];
            }
      }]);

      return AssetTooltipManager;
}(ExtensionPluginCore);

window.AssetTooltipManager = AssetTooltipManager;
AssetTooltipManager.TOOTIP_PROPERTY_NAME = "loc_surf_nm";