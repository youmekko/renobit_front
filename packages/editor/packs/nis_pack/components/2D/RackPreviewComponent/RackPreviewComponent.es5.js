"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RackPreviewComponent = function(_WVDOMComponent) {
    _inherits(RackPreviewComponent, _WVDOMComponent);

    function RackPreviewComponent() {
        _classCallCheck(this, RackPreviewComponent);

        var _this = _possibleConstructorReturn(this, (RackPreviewComponent.__proto__ || Object.getPrototypeOf(RackPreviewComponent)).call(this));

        var rootPath = "static/components/2D/project/nh/RackPreviewComponent/img/";
        _this.venderImgPath = [{
            name: "cisco",
            path: rootPath + "logo-cisco.png"
        }, {
            name: "dell",
            path: rootPath + "logo-dell.png"
        }, {
            name: "hp",
            path: rootPath + "logo-hp.png"
        }, {
            name: "ibm",
            path: rootPath + "logo-ibm.png"
        }, {
            name: "sun",
            path: rootPath + "logo-sun.png"
        }];

        _this._dataProvider = {};

        _this.unitWidth;
        _this.unitHeight;
        _this.unitLen;
        _this.unitDir;
        _this.items;
        _this.$itemWrap;
        _this.$rackWarp;
        _this.$itemsEl;
        _this.activeId;
        return _this;
    }

    //element 생성


    _createClass(RackPreviewComponent, [{
        key: "_onCreateElement",
        value: function _onCreateElement() {
            var $el = $(this._element);
            $el.attr("data-comp-name", "rack-preview");
            $el.append('<div class="rack-warp"><ul></ul></div>');
            this.$rackWarp = $el.find(".rack-warp");
        }
    }, {
        key: "init",
        value: function init() {
            this.activeId = "";
            this.$rackWarp.find("ul").empty();
            this.$rackWarp.off();
            //unbindEvent
            this.unitLen = this.dataProvider.rowCount;
            this.items = this.dataProvider.units;
            this.occupyIndex = new Map();
            this.setUnitRow(this.unitLen);
            this.$itemWrap = this.$rackWarp.find(".item-wrap");
            this.setUnitSize();

            // 실장 장비의 padding, columnIndex 계산
            //    this.calcOccupiedIndex();
            //    this.setMountData();
            this.drawItems();
            this.$itemsEl = this.$rackWarp.find(".item .cont");
            this.bindEvent();
        }
    }, {
        key: "bindEvent",
        value: function bindEvent() {
            var self = this;
            this.$rackWarp.on("click", ".item-wrap .item", function(e) {
                var $cont = $(this).find(".cont");
                var $target = self.$itemsEl.filter($cont);
                if ($target.length) {
                    self.focus($target.attr("data-id"));
                } else {
                    self.blur();
                }
            });
        }
    }, {
        key: "focusMountedDevice",
        value: function focusMountedDevice(id) {
            if (this.activeId == id || id == undefined) {
                this.activeId = "";
                this.$itemsEl.removeClass("active");
                return;
            }
            this.activeId = id;
            this.$itemsEl.removeClass("active");
            this.$itemsEl.filter("[data-id='" + id + "']").addClass("active");
        }
    }, {
        key: "focus",
        value: function focus(id) {
            //      this.focusMountedDevice(id);

            var hasActive = this.activeId == "" ? false : true;
            this.dispatchWScriptEvent("focus", {
                id: id,
                hasActive: hasActive
            });
        }
    }, {
        key: "blur",
        value: function blur() {
            if (this.activeId == "") {
                return;
            }
            this.activeId = "";
            this.$itemsEl.removeClass("active");

            this.dispatchWScriptEvent("blur");
        }
    }, {
        key: "setUnitRow",
        value: function setUnitRow(len) {
            var ary = [];
            for (var i = 0; i < len; i++) {
                var unitNo = i + 1;
                var str = '<li><span>' + unitNo + '</span><div class="item-wrap" data-unit-no="' + unitNo + '"></div></li>';
                ary.unshift(str);
                this.occupyIndex.set(unitNo, 0);
            }

            var domStr = ary.join('');
            this.$rackWarp.find("ul").append(domStr);
        }
    }, {
        key: "setUnitSize",
        value: function setUnitSize() {
            var unitEl = this.$itemWrap.eq(0);
            this.unitWidth = unitEl.width();
            this.unitHeight = unitEl.outerHeight();
        }
    }, {
        key: "drawItems",
        value: function drawItems() {
            for (var i = 0, len = this.items.length; i < len; i++) {
                var indexItems = this.items[i];

                for (var j = 0; j < indexItems.length; j++) {
                    this.appendItem(indexItems[j]);
                }
            }
        }
    }, {
        key: "appendItem",
        value: function appendItem(itemData) {
            var index = parseInt(itemData.rowIndex) + itemData.scaleY - 1;
            var $wrap = this.$itemWrap.filter("[data-unit-no=" + index + "]");
            var wid = this.unitWidth / 100 * itemData.scaleX;
            var hei = this.unitHeight * itemData.scaleY;
            var left = this.unitWidth / 100 * itemData.spaceX;
            var lineHeight = hei;

            var str = '<div class="item tooltip" style="height:' + hei + 'px;width:' + wid + 'px;left:' + left + 'px;">';

            if (this.unitWidth / 2 > wid && wid < hei && wid > this.unitHeight) {
                //3분할 이상, height가 더 길다면..
                lineHeight = wid;
                var originSize = wid / 2;
                str += '<div style="height:' + wid + 'px;width:' + hei + 'px; line-height:' + lineHeight + 'px;  transform-origin: ' + originSize + 'px; transform: rotate(90deg);" class="cont" data-id="' + itemData.id + '" ><em>';
            } else {
                str += '<div style="line-height:' + lineHeight + 'px;" class="cont ' + itemData.type + '" data-id="' + itemData.id + '" ><em>';
            }

            /*let img = this._getVenderImgEl(itemData.vender);*/
            var label = itemData.vender != null && itemData.vender.length > 0 ? '( ' + itemData.vender + ' ) ' + itemData.name : itemData.name;
            str += label +
                /*img +*/
                '</em></div>' + '<span class="tooltiptext"><span>' + label + '</span></span>' + '<div>';

            $wrap.append(str);
            $wrap.find(".tooltiptext").each(function() {
                var $target = $(this);
                var $cont = $target.find("span");
                $target.css("display", "block");
                var wid = $cont.outerWidth();
                var left = Math.round(wid / 2) * -1;
                $cont.css("left", left);
                $target.css("display", '');
            });

            itemData.typeColor = this.getGroupPropertyValue("bgColors", itemData.type);
        }
    }, {
        key: "_getVenderImgEl",
        value: function _getVenderImgEl(name) {

            var data = this.venderImgPath.find(function(x) {
                if (!name) {
                    return null;
                }
                return x.name == name.toLowerCase();
            });
            if (data) {
                return '<img class="vender-logo" src="' + data.path + '">';
            } else {
                return '';
            }
        }
    }, {
        key: "setBackgroundStyle",
        value: function setBackgroundStyle() {
            var bgColors = this.getGroupProperties("bgColors");

            var style = $('<style>' + '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.server { background: ' + bgColors.server + ' } ' + '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.network { background: ' + bgColors.network + ' } ' + '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.storage { background: ' + bgColors.storage + ' } ' + '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.backup { background: ' + bgColors.backup + ' } ' + '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.security { background: ' + bgColors.security + ' } ' + '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.etc { background: ' + bgColors.etc + ' } ' + '[id="' + this.id + '"] .rack-warp>ul>li>.item-wrap>.item .cont.active { border: 3px solid ' + bgColors.active + ' } ' + '</style>');

            $(this._element).append(style);
        }

        ///화면에 붙였을때

    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.setBackgroundStyle();
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            //삭제 처리
            this.$rackWarp.find("ul").empty();
            this.$rackWarp.off();

            this.occupyIndex = null;
            _get(RackPreviewComponent.prototype.__proto__ || Object.getPrototypeOf(RackPreviewComponent.prototype), "_onDestroy", this).call(this);
        }
    }, {
        key: "dataProvider",
        set: function set(data) {
            var oldValue = this.dataProvider;
            this._dataProvider = data;

            if (!this.isEditorMode && data != oldValue) {
                this.init();
                this.dispatchWScriptEvent("change", {
                    value: data
                });
            }
        },
        get: function get() {
            if (this.isEditorMode) {
                return {};
            }

            return this._dataProvider;
        }
    }]);

    return RackPreviewComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(RackPreviewComponent, {
    "info": {
        "componentName": "RackPreviewComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 210,
        "height": 715,
        "dataProvider": {}
    },

    "label": {
        "label_using": "N",
        "label_text": "RackPreview Component"
    },

    "bgColors": {
        "server": "#4e70b5",
        "network": "#414fa2",
        "storage": "#695b80",
        "backup": "#128997",
        "security": "#306b6e",
        "etc": "#6e6e6e",
        "active": "#3cd3ff"
    }
});

RackPreviewComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "label"
}, {
    label: "Background Color",
    owner: "vertical",
    children: [{
        owner: "bgColors",
        name: "server",
        type: "color",
        label: "server",
        show: true,
        writable: true,
        description: "server"
    }, {
        owner: "bgColors",
        name: "network",
        type: "color",
        label: "network",
        show: true,
        writable: true,
        description: "network"
    }, {
        owner: "bgColors",
        name: "storage",
        type: "color",
        label: "storage",
        show: true,
        writable: true,
        description: "storage"
    }, {
        owner: "bgColors",
        name: "backup",
        type: "color",
        label: "backup",
        show: true,
        writable: true,
        description: "backup"
    }, {
        owner: "bgColors",
        name: "security",
        type: "color",
        label: "security",
        show: true,
        writable: true,
        description: "security"
    }, {
        owner: "bgColors",
        name: "etc",
        type: "color",
        label: "etc",
        show: true,
        writable: true,
        description: "etc"
    }, {
        owner: "bgColors",
        name: "active",
        type: "color",
        label: "active",
        show: true,
        writable: true,
        description: "active"
    }]
}];

WVPropertyManager.add_event(RackPreviewComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WVPropertyManager.add_event(RackPreviewComponent, {
    name: "focus",
    label: "focus 이벤트",
    description: "focus 이벤트 입니다.",
    properties: [{
        name: "id",
        type: "string",
        default: "",
        description: "focus된 item id 정보입니다."
    }]
});

WVPropertyManager.add_event(RackPreviewComponent, {
    name: "blur",
    label: "focus 이벤트",
    description: "focus 이벤트 입니다."
});
