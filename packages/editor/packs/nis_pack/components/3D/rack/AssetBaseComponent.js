class AssetBaseComponent extends WV3DResourceComponent {
      constructor(){ super(); }
      _onCreateProperties() {
            super._onCreateProperties();
            this.selected      = false;
            this._data         = null;
            this.hasUnit       = false;
            this._useToolTip    = true;
            this.toolTipPropertyName = "";
      }

      get transitionTarget(){
            return this.element;
      }

      set data(data) {
            if(!data){
                  return;
            }
            this._data = new AssetVO(data);
      }

      get data() {
            return this._data;
      }

      set assetId(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "assetId", value)) {
                  this._invalidRackId = true;
            }
      }

      get assetId() {
            return this.getGroupPropertyValue("setter", "assetId");
      }

      set popupId(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "popupId", value)) {
                  this._invalidatePopupId = true;
            }
      }

      get popupId() {
            return this.getGroupPropertyValue("setter", "popupId");
      }


      get toolTipMessage(){

            return this._data[this.toolTipPropertyName];
      }

      get toolTipTarget(){
            if(this.element.type == "Mesh"){
                  this.element.userData ={owner:this};
                  return this.element;
            }
            this.element.children[0].userData = {owner:this};
            return this.element.children[0];
      }

      get useToolTip(){
            return this._useToolTip;
      }



      focusSelectUnit(selectUnit){

      }

      getUnitById( id ){
            return null;
      }

      transitionIn(data) {
            this.selected = true;
            this._useToolTip = false;
      }

      transitionOut(data) {
            this.selected = false;
            this._useToolTip = true;
      }


      _onDestroy() {
            this.selected     = false;
            this.data         = null;
            super._onDestroy();
      }



}


WV3DPropertyManager.attach_default_component_infos(AssetBaseComponent, {
      "setter": {
            "size": {x: 1, y: 1, z: 1},
            "assetId": "",
            "popupId": ""
      },

      "label": {
            "label_text": "AssetBaseComponent",
            "label_line_size": 15,
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "AssetBaseComponent",
            "version": "1.0.0",
      }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
WV3DPropertyManager.add_property_panel_group_info(AssetBaseComponent, {
      label: "popupId",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "popupId",
            type: "string",
            label: "popupId",
            show: true,
            writable: true,
            description: "popup id"
      }]
});

