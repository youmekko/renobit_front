"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ServerRack = function (_AssetBaseComponent) {
      _inherits(ServerRack, _AssetBaseComponent);

      function ServerRack() {
            _classCallCheck(this, ServerRack);

            return _possibleConstructorReturn(this, (ServerRack.__proto__ || Object.getPrototypeOf(ServerRack)).call(this));
      }

      _createClass(ServerRack, [{
            key: "_onCreateProperties",
            value: function _onCreateProperties() {
                  _get(ServerRack.prototype.__proto__ || Object.getPrototypeOf(ServerRack.prototype), "_onCreateProperties", this).call(this);
                  this.units = [];
                  this.rackDoor = null;
                  this.unitArea = null;
                  this.rackCover = null;
                  this.hilight = false;
                  this._useToolTip = true;
                  this.unitAreaSize = null;
                  this.focusTween = null;
                  this._unitSelectHandler = this.onDblClick_ChildItem.bind(this);
            }
      }, {
            key: "composeResource",
            value: function composeResource(serverRack) {

                  var rackSize = ThreeUtil.getObjectSize(serverRack);
                  this.serverRack = serverRack;
                  this.rackDoor = serverRack.getObjectByName("doorLeft");
                  this.unitArea = serverRack.getObjectByName("area");
                  this.unitArea.material.visible = false;
                  this.rackCover = serverRack.getObjectByName("cover");
                  serverRack.getObjectByName("base").material.visible = false;
                  this.unitAreaSize = ThreeUtil.getObjectSize(this.unitArea);
                  this.unitArea.geometry.computeBoundingBox();

                  // element 변경 처리
                  this.appendElement.remove(this.element);
                  this._element = serverRack;
                  this._element.name = this.name + "_rack";
                  this.appendElement.add(this._element);

                  // 문 위치 조정
                  this.rackDoor.geometry.center();
                  this.rackDoor.geometry.computeBoundingBox();
                  this.rackDoor.geometry.translate(rackSize.x / 2, 0, 0);
                  this.rackDoor.position.x = -rackSize.x / 2;
                  this.rackDoor.position.y = rackSize.y / 2;
                  this.rackDoor.position.z = rackSize.z / 2;

                  this._validateElementSize(rackSize);
            }

            // server unit 로드 처리

      }, {
            key: "validateUnitDataByType",
            value: function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(unitInfo) {
                        var unitName, objUnit;
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                _context.prev = 0;
                                                unitName = this.getResourceFileName(unitInfo.objPath);
                                                objUnit = NLoaderManager.getCloneLoaderPool(unitName);

                                                if (!(objUnit == null)) {
                                                      _context.next = 10;
                                                      break;
                                                }

                                                _context.next = 6;
                                                return NLoaderManager.loadObj(this.convertServerPath(unitInfo.objPath));

                                          case 6:
                                                objUnit = _context.sent;
                                                _context.next = 9;
                                                return this.setChildMeshTexture(objUnit, unitInfo.mapPath);

                                          case 9:
                                                NLoaderManager.setLoaderPool(unitName, objUnit);

                                          case 10:
                                                _context.next = 15;
                                                break;

                                          case 12:
                                                _context.prev = 12;
                                                _context.t0 = _context["catch"](0);

                                                CPLogger.log(CPLogger.getCallerInfo(this, "serverUnit-Resource" + unitInfo), [_context.t0.toString()]);

                                          case 15:
                                          case "end":
                                                return _context.stop();
                                    }
                              }
                        }, _callee, this, [[0, 12]]);
                  }));

                  function validateUnitDataByType(_x) {
                        return _ref.apply(this, arguments);
                  }

                  return validateUnitDataByType;
            }()
      }, {
            key: "getResourceUnits",
            value: function getResourceUnits() {
                  return this.getGroupPropertyValue("setter", "resource").units;
            }
      }, {
            key: "_validateResource",
            value: function () {
                  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                        var loadedObj, unitResource, typeResources, prop, unitType;
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                              while (1) {
                                    switch (_context2.prev = _context2.next) {
                                          case 0:
                                                loadedObj = _get(ServerRack.prototype.__proto__ || Object.getPrototypeOf(ServerRack.prototype), "_validateResource", this).call(this);
                                                //사용 유닛 로드 처리

                                                unitResource = this.getResourceUnits();
                                                typeResources = void 0;
                                                _context2.t0 = regeneratorRuntime.keys(unitResource);

                                          case 4:
                                                if ((_context2.t1 = _context2.t0()).done) {
                                                      _context2.next = 16;
                                                      break;
                                                }

                                                prop = _context2.t1.value;

                                                typeResources = unitResource[prop];
                                                _context2.t2 = regeneratorRuntime.keys(typeResources);

                                          case 8:
                                                if ((_context2.t3 = _context2.t2()).done) {
                                                      _context2.next = 14;
                                                      break;
                                                }

                                                unitType = _context2.t3.value;
                                                _context2.next = 12;
                                                return this.validateUnitDataByType(typeResources[unitType]);

                                          case 12:
                                                _context2.next = 8;
                                                break;

                                          case 14:
                                                _context2.next = 4;
                                                break;

                                          case 16:
                                                return _context2.abrupt("return", loadedObj);

                                          case 17:
                                          case "end":
                                                return _context2.stop();
                                    }
                              }
                        }, _callee2, this);
                  }));

                  function _validateResource() {
                        return _ref2.apply(this, arguments);
                  }

                  return _validateResource;
            }()
      }, {
            key: "initDummyData",
            value: function initDummyData() {
                  this.data = {
                        am_asset_id: "AM1803101436",
                        areaHeight: 18.899999916553497,
                        asset_no: "HGD001436",
                        max_unit_qty: 40,
                        "items": [{
                              am_asset_id: "AM1803100473",
                              asset_no: "HTDL30473",
                              columnWeight: 50,
                              lv2_class_id: "HTD00",
                              lv2_class_nm: "통신장비",
                              lv3_class_id: "HTDL3",
                              lv3_class_nm: "L3스위치",
                              pId: "AM1803101441",
                              padding: 30,
                              rack_asset_id: "AM1803101441",
                              use_vertical_yn: "N",
                              unit_end_no: "3",
                              unit_start_no: "2",
                              unit_use_qty: 2,
                              vendor_id: "CISCO",
                              vendor_name: "시스코"
                        }, {
                              am_asset_id: "AM1803100474",
                              asset_no: "HTDL30474",
                              columnWeight: 70,
                              lv2_class_id: "HTD00",
                              lv2_class_nm: "통신장비",
                              lv3_class_id: "HTDL3",
                              lv3_class_nm: "L3스위치",
                              pId: "AM1803101441",
                              padding: 10,
                              rack_asset_id: "AM1803101441",
                              use_vertical_yn: "Y",
                              unit_end_no: "9",
                              unit_start_no: "5",
                              unit_use_qty: 4,
                              vendor_id: "CISCO",
                              vendor_name: "시스코"
                        }]
                  };
            }
      }, {
            key: "clearTween",
            value: function clearTween() {
                  if (this.focusTween != null && this.focusTween.isActive()) {
                        this.focusTween.pause();
                        this.focusTween.kill();
                  }
            }

            // provider을 이용해 내부 유닛 구성

      }, {
            key: "composeUnitData",
            value: function composeUnitData() {
                  if (!this.data) {
                        return;
                  }
                  this.hasUnit = true;
                  var cols = void 0;
                  var units = this.data.units;
                  var rows = units.length;
                  var colUnit = this.unitAreaSize.x / 100; //가로 스케일 단위
                  var rowUnit = this.data.rowHeight; //세로 스케일 단위
                  var i = void 0,
                        j = void 0,
                        vo = void 0,
                        unit = void 0;
                  var colProvider = void 0;
                  var rowIndex = void 0,
                        rowStart = void 0;

                  for (i = 0; i < rows; i++) {
                        colProvider = units[i];
                        cols = colProvider.length;
                        for (j = 0; j < cols; j++) {
                              vo = colProvider[j];
                              unit = new UnitBase(NLoaderManager.getCloneLoaderPool(this.getResourceFileName(vo.resourceName)));
                              unit.name = vo.type + "_" + i + "_" + j;
                              unit.skin.name = vo.type + "_" + i + "_" + j;
                              rowIndex = vo.rowIndex - 1;
                              rowStart = this.unitAreaSize.y;
                              if (this.data.reverseRow) {
                                    rowIndex = rowIndex + vo.scaleY;
                                    rowStart = this.unitArea.geometry.boundingBox.min.y;
                              }
                              var scaleRateX = vo.rotateY ? colUnit / unit.size.y : colUnit / unit.size.x;
                              var scaleRateY = vo.rotateY ? rowUnit / unit.size.x : rowUnit / unit.size.y;
                              vo.scale = new THREE.Vector3(scaleRateX * vo.scaleX, scaleRateY * vo.scaleY, 1);
                              vo.position = new THREE.Vector3(-this.unitAreaSize.x / 2 + colUnit * vo.spaceX, rowStart + rowIndex * rowUnit, 0);
                              unit.data = vo;
                              unit.skin.userData = unit;
                              this.unitArea.add(unit.skin);
                        }
                  }
            }
      }, {
            key: "registUnitEvent",
            value: function registUnitEvent() {
                  var i = void 0;
                  var unit = void 0;
                  for (i = 0; i < this.unitArea.children.length; i++) {
                        unit = this.unitArea.children[i];
                        this.registEventByType(unit, "dblclick", this._unitSelectHandler, false);
                  }
            }
      }, {
            key: "clearUnitEvent",
            value: function clearUnitEvent() {
                  var i = void 0;
                  var unit = void 0;
                  for (i = 0; i < this.unitArea.children.length; i++) {
                        unit = this.unitArea.children[i];
                        this.removeEventByType(unit, "dblclick", this._unitSelectHandler, false);
                  }
            }
      }, {
            key: "onDblClick_ChildItem",
            value: function onDblClick_ChildItem(event) {
                  event.stopPropagation();
                  var selectUnit = event.target.userData;
                  this.dispatchEvent({
                        type: "WVComponentEvent.SELECT_ITEM",
                        target: this,
                        data: { selectItem: selectUnit }
                  });

                  this.dispatchWScriptEvent("itemDblClick", { target: this, selectItem: selectUnit, type: "itemDblClick" });
            }
      }, {
            key: "focusSelectUnit",
            value: function focusSelectUnit(selectUnit) {
                  this.clearUnitFocus(selectUnit);
                  if (selectUnit != null && !selectUnit.selected) {
                        selectUnit.setFocus();
                  } else {
                        this.clearUnitFocus();
                  }
            }
      }, {
            key: "setFocusByColor",
            value: function setFocusByColor(value) {
                  this.rackCover.material.color.setHex(value);
                  this.rackDoor.material.color.setHex(value);
            }
      }, {
            key: "transitionIn",
            value: function transitionIn(data) {
                  var _this2 = this;

                  this.clearTween();
                  this._useToolTip = false;
                  if (!this.hasUnit) {
                        this.composeUnitData();
                  }
                  this.selected = true;
                  this.focusTween = TweenMax.to(this.rackDoor.rotation, ServerRack.FOCUS_DURATION / 2, {
                        y: ServerRack.DOOR_OPEN_ANGLE * Math.PI / 180,
                        onComplete: function onComplete() {
                              _this2.registUnitEvent();
                        }
                  });
            }
      }, {
            key: "transitionOut",
            value: function transitionOut(data) {
                  this._useToolTip = true;
                  this.clearUnitEvent();
                  this.clearTween();
                  this.clearUnitFocus();
                  this.selected = false;
                  this.focusTween = TweenMax.to(this.rackDoor.rotation, ServerRack.FOCUS_DURATION / 2, {
                        y: 0
                  });
            }
      }, {
            key: "clearUnitFocus",
            value: function clearUnitFocus(selectUnit) {
                  var unitObj = void 0;
                  for (var i = 0; i < this.unitArea.children.length; i++) {
                        unitObj = this.unitArea.children[i];
                        if (unitObj.userData == selectUnit) continue;
                        if (unitObj.userData.selected) unitObj.userData.clearFocus();
                  }
            }
      }, {
            key: "getUnitById",
            value: function getUnitById(id) {
                  for (var i = 0; i < this.unitArea.children.length; i++) {
                        var unitObj = this.unitArea.children[i];
                        if (unitObj.userData.data.id == id) return unitObj.userData;
                  }
                  return null;
            }
      }, {
            key: "removeUnit",
            value: function removeUnit() {
                  var max = this.unitArea.children.length;
                  var idx = void 0;
                  var unitMesh = void 0;
                  for (idx = 0; idx < max; idx++) {
                        unitMesh = this.unitArea.children[idx];
                        unitMesh.userData.destroy();
                  }
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  this.clearTween();
                  this.clearUnitEvent();
                  this.removeUnit();
                  this.rackDoor = null;
                  this.unitArea = null;
                  this.unitAreaSize = null;
                  this.focusTween = null;
                  _get(ServerRack.prototype.__proto__ || Object.getPrototypeOf(ServerRack.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "data",
            set: function set(data) {
                  if (!data) {
                        return;
                  }
                  data.areaHeight = this.unitAreaSize.y;
                  this._data = new RackVO(data);
            },
            get: function get() {
                  return this._data;
            }
      }, {
            key: "toolTipTarget",
            get: function get() {
                  this.rackCover.userData = { owner: this };
                  return this.rackCover;
            }
      }]);

      return ServerRack;
}(AssetBaseComponent);

WV3DPropertyManager.attach_default_component_infos(ServerRack, {
      "setter": {
            "size": { x: 1, y: 1, z: 1 },
            "assetId": "",
            "popupId": ""
      },

      "label": {
            "label_text": "ServerRack",
            "label_line_size": 10,
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "ServerRack",
            "version": "1.0.0"
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
WV3DPropertyManager.add_property_panel_group_info(ServerRack, {
      label: "popupId",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "popupId",
            type: "string",
            label: "popupId",
            show: true,
            writable: true,
            description: "popup id"
      }]
});

ServerRack.DOOR_OPEN_ANGLE = -135;
ServerRack.FOCUS_DURATION = 1;

ServerRack.UNIT_TYPE = {
      SERVER: "server",
      SECURITY: "security",
      STORAGE: "storage",
      NETWORK: "network",
      BACKUP: "backup",
      ETC: "etc"
};
