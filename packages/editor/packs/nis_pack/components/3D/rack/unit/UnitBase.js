class UnitBase {

      constructor( objSkin ) {
            this._obj =  objSkin;
            this._unitSize = ThreeUtil.getObjectSize(this._obj);
            this._data = null;
            this.selected = false;
            this._focusTween = null;
      }

      set name( strName ){
            this._obj.name = strName;
      }

      get name(){
            return this._obj.name;
      }

      set data( data ){
            this._data = data;
            this.compose();
      }

      get data(){
            return this._data;
      }

      get skin(){
            return this._obj;
      }

      get size(){
            return this._unitSize;
      }

      compose() {
            //회전 정보가 있으면 geometry를 회전
            if( this.data.rotateY ){
                  this._obj.children[0].geometry.rotateZ(-90*Math.PI/180);
            }

            //회전값에 따라 x, y 값을 서로 변경
            let sx =  this.data.scale.x;
            let sy =  this.data.scale.y;
            let tx = ( this.data.rotateY ) ? 0 : this._unitSize.x/2;
            let ty = ( this.data.rotateY ) ? -this._unitSize.x/2 : -this._unitSize.y;
            this._obj.children[0].geometry.translate(tx, ty, 0);
            this._obj.scale.set(sx, sy, this.data.scale.z);
            this._obj.position.set(this.data.position.x, this.data.position.y, this.data.position.z );

      }

      clearTween(){
            if(this._focusTween != null && this._focusTween.isActive() ){
                  this._focusTween.pause();
                  this._focusTween.kill();
            }
      }

      setFocus(){
            this.selected = true;
            this.clearTween();
            this._focusTween = TweenMax.to(
                  this._obj.position, .5, {
                        z:this._unitSize.z/2,
                        onComplete:()=>{
                              //this._obj.children[0].material.color.setHex(0xffeeff);
                        }
                  }
            );
      }

      clearFocus(){
            this.selected = false;
            this.clearTween();
            this._focusTween = TweenMax.to(
                  this._obj.position, .5, {
                        z:0,
                        onComplete:()=>{
                              //this._obj.children[0].material.color.setHex(0xffffff);
                        }
                  }
            );
      }

      toggle() {
            if(this._obj.position.z == 0){
                  this.setFocus();
            } else {
                  this.clearFocus();
            }
      }

      destroy() {
            this.clearTween();
            this._focusTween = null;
            this._obj.userData = null;
            MeshManager.disposeMesh(this._obj);
            this._obj =  null;
            this._data = null;
            this._unitSize = null;
      }

}
