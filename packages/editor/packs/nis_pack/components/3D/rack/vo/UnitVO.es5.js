"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UnitVO = function () {
      function UnitVO(info) {
            _classCallCheck(this, UnitVO);

            this.rotateY = info.use_vertical_yn == "Y"; //유닛 회전 여부
            this.id = info.am_asset_id; //자산 id
            this.name = info.asset_no; //자산 이름
            this.type = this.composeType(info.lv2_class_id); //유닛 타입
            this.vender = info.vendor_id; //유닛 제조사
            this.rowIndex = info.unit_start_no; //유닛 로우 정보
            this.scaleX = info.columnWeight; //유닛 가로 크기
            var unitScaleY = parseInt(info.unit_use_qty); //유닛 세로 크기
            this.scaleY = unitScaleY;
            this.hasFixUnit = unitScaleY == 1 || unitScaleY == 2 || unitScaleY == 4 || unitScaleY == 10 || unitScaleY == 20;
            if (!this.hasFixUnit) {
                  if (this.scaleY > 20) {
                        unitScaleY = 20;
                  } else if (this.scaleY > 10 && this.scaleY < 20) {
                        unitScaleY = 10;
                  } else if (this.scaleY > 4 && this.scaleY < 10) {
                        unitScaleY = 4;
                  } else {
                        unitScaleY = 1;
                  }
            }
            this.resourceName = this.getResourceNameByScale(unitScaleY);
            this.typeColor = info.color; //유닛 분류에 따른 컬러 값 hex 형식
            this.spaceX = parseInt(info.padding); //유닛 가로 공백
      }

      _createClass(UnitVO, [{
            key: "getResourceNameByScale",
            value: function getResourceNameByScale(scale) {
                  switch (this.type) {
                        case "server":
                              return "hpU" + scale + "_SV.obj";
                              break;

                        case "network":
                              return "ciscoU" + scale + "_NT.obj";
                              break;

                        case "security":
                              return "juniperU" + scale + "_NT.obj";
                              break;

                        case "storage":
                              return "emcU" + scale + "_ST.obj";
                              break;

                        case "backup":
                              return "oraclesunU" + scale + "_ST.obj";
                              break;

                        case "etc":
                              return "etcU" + scale + "_SV.obj";
                              break;
                  }
            }
      }, {
            key: "composeType",
            value: function composeType(type) {
                  var typeString = "";

                  switch (type) {
                        case "HSV00":
                              typeString = "server";
                              break;
                        case "HTD00":
                              typeString = "network";
                              break;
                        case "HSD00":
                              typeString = "security";
                              break;
                        case "HDS00":
                              typeString = "storage";
                              break;
                        case "HBD00":
                              typeString = "backup";
                              break;
                        default:
                              typeString = "etc";
                  }

                  return typeString;
            }
      }]);

      return UnitVO;
}();
