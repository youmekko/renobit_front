"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AssetBaseComponent = function (_WV3DResourceComponen) {
      _inherits(AssetBaseComponent, _WV3DResourceComponen);

      function AssetBaseComponent() {
            _classCallCheck(this, AssetBaseComponent);

            return _possibleConstructorReturn(this, (AssetBaseComponent.__proto__ || Object.getPrototypeOf(AssetBaseComponent)).call(this));
      }

      _createClass(AssetBaseComponent, [{
            key: "_onCreateProperties",
            value: function _onCreateProperties() {
                  _get(AssetBaseComponent.prototype.__proto__ || Object.getPrototypeOf(AssetBaseComponent.prototype), "_onCreateProperties", this).call(this);
                  this.selected = false;
                  this._data = null;
                  this.hasUnit = false;
                  this._useToolTip = true;
                  this.toolTipPropertyName = "";
            }
      }, {
            key: "focusSelectUnit",
            value: function focusSelectUnit(selectUnit) {}
      }, {
            key: "getUnitById",
            value: function getUnitById(id) {
                  return null;
            }
      }, {
            key: "transitionIn",
            value: function transitionIn(data) {
                  this.selected = true;
                  this._useToolTip = false;
            }
      }, {
            key: "transitionOut",
            value: function transitionOut(data) {
                  this.selected = false;
                  this._useToolTip = true;
            }
      }, {
            key: "onLoadPage",
            value: function onLoadPage() {
                  if (this.isViewerMode) {
                        this._onInitWScriptEvent();
                  }
            }
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  this.selected = false;
                  this.data = null;
                  _get(AssetBaseComponent.prototype.__proto__ || Object.getPrototypeOf(AssetBaseComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "transitionTarget",
            get: function get() {
                  return this.element;
            }
      }, {
            key: "data",
            set: function set(data) {
                  if (!data) {
                        return;
                  }
                  this._data = new AssetVO(data);
            },
            get: function get() {
                  return this._data;
            }
      }, {
            key: "assetId",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "assetId", value)) {
                        this._invalidRackId = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "assetId");
            }
      }, {
            key: "popupId",
            set: function set(value) {
                  if (this._checkUpdateGroupPropertyValue("setter", "popupId", value)) {
                        this._invalidatePopupId = true;
                  }
            },
            get: function get() {
                  return this.getGroupPropertyValue("setter", "popupId");
            }
      }, {
            key: "toolTipMessage",
            get: function get() {

                  return this._data[this.toolTipPropertyName];
            }
      }, {
            key: "toolTipTarget",
            get: function get() {
                  if (this.element.type == "Mesh") {
                        this.element.userData = { owner: this };
                        return this.element;
                  }
                  this.element.children[0].userData = { owner: this };
                  return this.element.children[0];
            }
      }, {
            key: "useToolTip",
            get: function get() {
                  return this._useToolTip;
            }
      }]);

      return AssetBaseComponent;
}(WV3DResourceComponent);

WV3DPropertyManager.attach_default_component_infos(AssetBaseComponent, {
      "setter": {
            "size": { x: 1, y: 1, z: 1 },
            "assetId": "",
            "popupId": ""
      },

      "label": {
            "label_text": "AssetBaseComponent",
            "label_line_size": 15,
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "AssetBaseComponent",
            "version": "1.0.0"
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
WV3DPropertyManager.add_property_panel_group_info(AssetBaseComponent, {
      label: "popupId",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "popupId",
            type: "string",
            label: "popupId",
            show: true,
            writable: true,
            description: "popup id"
      }]
});
