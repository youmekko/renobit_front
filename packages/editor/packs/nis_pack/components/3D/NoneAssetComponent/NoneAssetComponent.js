class NoneAssetComponent extends WV3DResourceComponent{
      // 비자산을 표현을 위한 컴포넌트
      constructor(){
            super();
            this._useToolTip        = true;
            this._toolTipMessage    = "";
      }

      // 툴팁 기능 구현
      get useToolTip(){
            return this._useToolTip;
      }

      get toolTipTarget() {
            if(this.element.type == "Mesh"){
                  this.element.userData ={owner:this};
                  return this.element;
            }
            this.element.children[0].userData = {owner:this};
            return this.element.children[0];
      }

      set toolTipMessage( value ){
            this._toolTipMessage = value;
      }

      get toolTipMessage() {
            return  this._toolTipMessage;
      }


}



WV3DPropertyManager.attach_default_component_infos(NoneAssetComponent, {
      "setter": {
            "size": {x: 1, y: 1, z: 1}
      },
      "label": {
            "label_text": "NoneAssetComponent",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "NoneAssetComponent",
            "version": "1.0.0",
      }
});
