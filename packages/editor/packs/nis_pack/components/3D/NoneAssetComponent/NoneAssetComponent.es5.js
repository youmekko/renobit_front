"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NoneAssetComponent = function (_WV3DResourceComponen) {
      _inherits(NoneAssetComponent, _WV3DResourceComponen);

      // 비자산을 표현을 위한 컴포넌트
      function NoneAssetComponent() {
            _classCallCheck(this, NoneAssetComponent);

            var _this = _possibleConstructorReturn(this, (NoneAssetComponent.__proto__ || Object.getPrototypeOf(NoneAssetComponent)).call(this));

            _this._useToolTip = true;
            _this._toolTipMessage = "";
            return _this;
      }

      // 툴팁 기능 구현


      _createClass(NoneAssetComponent, [{
            key: "useToolTip",
            get: function get() {
                  return this._useToolTip;
            }
      }, {
            key: "toolTipTarget",
            get: function get() {
                  if (this.element.type == "Mesh") {
                        this.element.userData = { owner: this };
                        return this.element;
                  }
                  this.element.children[0].userData = { owner: this };
                  return this.element.children[0];
            }
      }, {
            key: "toolTipMessage",
            set: function set(value) {
                  this._toolTipMessage = value;
            },
            get: function get() {
                  return this._toolTipMessage;
            }
      }]);

      return NoneAssetComponent;
}(WV3DResourceComponent);

WV3DPropertyManager.attach_default_component_infos(NoneAssetComponent, {
      "setter": {
            "size": { x: 1, y: 1, z: 1 }
      },
      "label": {
            "label_text": "NoneAssetComponent",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "NoneAssetComponent",
            "version": "1.0.0"
      }
});
