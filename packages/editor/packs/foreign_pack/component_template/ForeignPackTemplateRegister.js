class ForeignPackTemplateRegister {
      static regist() {
            if (ForeignPackTemplateRegister.init == true)
                  return;

            ForeignPackTemplateRegister.init = true;
            ForeignPackTemplateRegister._createAssetTypeGroupListTemplate();

            console.log("ForeignPackTemplateRegister 23")
      }

      static _createAssetTypeGroupListTemplate() {
            wemb.componentLibraryManager.addComponentPropertyTemplate("magnetic-link");
            Vue.component('magnetic-link-template', {
                  extends: CoreTemplate,
                  template: ` 
                  <div class="prop-group">
                        
                        <h5 class="header">link info</h5>
                       
                        
                        <div class="body">
                              <div class="d-flex row">
                                    <span class="label">from</span>
                              </div>
                              
                              <div class="d-flex row">
                                    <span class="label">instance</span>
                                    <div class="flex d-flex" v-if="instanceList && instanceList.length">
                                          <el-select class="select flex" 
                                          v-bind:value="model.link.fromTarget" 
                                          @change="onChangeFromTargetValue($event)">
                                              <el-option v-for="instanceName of instanceList" 
                                                    :value="instanceName" :label="instanceName"
                                                    :key="instanceName">                                         
                                              </el-option>
                                          </el-select> 
                                    </div>
                              </div>
                        </div>
                        <hr class="body">
                        <div class="body">
                              <div class="d-flex row">
                                    <span class="label">to</span>
                              </div>
                              
                              <div class="d-flex row">
                                    <span class="label">instance</span>
                                    <div class="flex d-flex" v-if="instanceList && instanceList.length">
                                          <el-select class="select flex" 
                                          v-model="model.link.toTarget" 
                                         @change="onChangeToTargetValue($event)">
                                              <el-option v-for="instanceName of instanceList" 
                                                    :value="instanceName" :label="instanceName"
                                                    :key="instanceName">                                         
                                              </el-option>
                                          </el-select>
                                    </div>
                              </div>
                        </div>
                    </div>
            `,

                  mounted: function() {},
                  data: function() {
                        return {
                              typeId: "",
                              comInstanceList:[]
                        };
                  },

                  computed: {
                        instanceList:function(){
                              let list = [];

                              list.push("");
                              wemb.editorProxy.threeLayerInstanceList.forEach((comInstance)=>{

                                    list.push(comInstance.name);

                              });

                              return list;
                        }
                  },

                  methods: {

                        onChangeFromTargetValue:function(fromTarget){
                              let toTarget = this.model.link.toTarget.trim();

                              if(fromTarget){
                                    if(fromTarget==toTarget){
                                          alert("from, to에 동일한 요소가 선택 될 수 없습니다.");
                                          return;
                                    }
                              }
                              this.dispatchChangeEvent("link", "fromTarget",fromTarget)


                        },

                        onChangeToTargetValue:function(toTarget){
                              let fromTarget = this.model.link.fromTarget.trim();

                              if(toTarget){
                                    if(toTarget==fromTarget){
                                          alert("from, to에 동일한 요소가 선택 될 수 없습니다.");
                                          return;
                                    }
                              }
                              this.dispatchChangeEvent("link", "toTarget",toTarget)

                        }


                  }
            })
      }


}

ForeignPackTemplateRegister.init = false;
