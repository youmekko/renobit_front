class MLine3DComponent extends NWV3DComponent {

      constructor() {
            super();

            this._updateLinkTimerID=0;
      }

      _onCreateProperties(){
            super._onCreateProperties();

            // 사이즈 정보 writable을 false로 설정
            let sizeInfo = WVPropertyManager.getPropertyGroupChildrenByName(MLine3DComponent.property_panel_info, "display", "size");
            sizeInfo.writable = false;
            WVPropertyManager.removePropertyGroupChildrenByName(MLine3DComponent.property_info, "display", "size");

            this._elementSize = this.getDefaultProperties().setter.size;
            this._onHelperChangeCalle = this._onHelperChange.bind(this);
            this._onHelperClickCalle = this._onHelperClick.bind(this);
            this._onTransformMouseUpCalle = this._onTransformMouseUp.bind(this);

            //월드 포인트
            this._points = [];

            //로컬 포인트
            this._localPoints = [];

            //색깔
            this._colors = [];
            var cl =this.getGroupPropertyValue("setter", "color");
            if(cl.indexOf('#') > -1)
            {
                  var c = this._hexToRgb(cl);
                  this._colors = [c.r, c.g, c.b]
            }else{
                  this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
            }

            //컨트롤
            //controls
            this._controls = window.wemb.mainPageComponent.threeLayer._transformControls;
            if(this.isEditorMode)
            {
                  this._controls.addEventListener("objectChange", this._onHelperChangeCalle, false);
                  this._controls.addEventListener("mouseUp", this._onTransformMouseUpCalle, false);
            }

            //커브
            this._curve = null;
            this._curveSegments = 1000;

            //두꺼운 선
            this._fatLine = null;

            //라인 피킹 세그먼트
            this._linePickingSegments = 300;

            this._element = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshPhongMaterial').clone());
            this._element.castShadow = true;
            this._element.receiveShadow = true;
      }
      _onCreateElement() {
            super._onCreateElement();
            var boxSize = 2;
            this._pointHelperGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
      }

      _onImmediateUpdateDisplay(){

            //helper 생성
            this._initPoints();

            //여기서 월드 행렬이 바뀌어 있으니 월드 로컬 점을 만들어준다.
            //월드는 그대로 쓰고 로컬 포인트를 만들어준다
            this._makeLocalAndWorldPoints();

            if(this.isEditorMode)
            {
                  //에디터라면 만들어진 점들을 가지고 helper를 만들어준다.
                  this._initPointHelper();
            }

            //커브를 그린다.
            //뚱라인을 그린다.
            //엘리먼트를 추가한다.
            //그리는건 로컬 좌표를 이용해서 그린다.
            this._makeCurve();

      }
      _initPoints(){
            //this.points는 월드좌표
            var v_up = new THREE.Vector3(30, 30, 30);
            var v_down = new THREE.Vector3(-30, -30, -30);

            var v_pos = new THREE.Vector3(this.position.x, this.position.y, this.position.z);
            var default_points = [new THREE.Vector3(0, 0, 0).addVectors(v_pos, v_up), new THREE.Vector3(0, 0, 0).addVectors(v_pos, v_down)];
            var pts = [];
            if(this.points.length === 0)
            {
                  //새로 생성되는 거니까 월드좌표들을 만들어서 넣는다.
                  for(let i = 0 ; i < default_points.length; i++)
                  {
                        pts.push(default_points[i].clone());
                  }
                  this.points = pts;
            }else{
                  for(let i = 0 ; i < this.points.length; i++)
                  {
                        pts.push(new THREE.Vector3(this.points[i].x, this.points[i].y, this.points[i].z));
                  }
                  this.points = pts;
            }
      }
      _initPointHelper(){
            var localHelper, worldHelper;
            //먼저 직접 에디터에서 조작하는 로컬 포인트 헬퍼
            for(let i = 0 ; i < this._localPoints.length; i++)
            {
                  localHelper = this._addPointHelperObject(this._localPoints[i]);
                  localHelper.name = "local"+i
                  this._domEvents.addEventListener(localHelper, 'click', this._onHelperClickCalle, false);
                  this.appendElement.add(localHelper);
            }
            //패널에 표현되는 월드좌표를 위한 월드 포인트 헬퍼
            for(let i = 0 ; i < this._points.length; i++)
            {
                  worldHelper = this._addPointHelperObject(this._points[i]);
                  worldHelper.name = "world"+i;
                  worldHelper.visible = false;
                  this.appendElement.add(worldHelper);
            }
      }
      _addPointHelperObject(position){
            var object = new THREE.Mesh(this._pointHelperGeometry.clone(), new THREE.MeshBasicMaterial({color : Math.random() * 0xffffff}));
            if(position)
            {
                  object.position.copy(position);
            }else{
                  object.position.x = Math.random()* 80 + 30;
                  object.position.y = Math.random()* 10 + 30;
                  object.position.z = Math.random()* 10 + 30;
            }
            object.castShadow = true;
            object.receiveShadow = true;
            return object
      }
      _makeLocalAndWorldPoints(){
            this._points = this.points;
            var pts = [];
            for(let i = 0 ; i < this.points.length; i++)
            {
                  var v = this.points[i].clone();
                  this.appendElement.worldToLocal(v);
                  pts.push(v);
            }
            this._localPoints = pts;
      }
      _makeCurve(){
            this._curve = new THREE.CatmullRomCurve3(this._localPoints);
            this._curve.curveType = 'catmullrom';
            this._curve.tension = 0;

            var curveGeometry = new THREE.BufferGeometry();
            curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curve.points.length * 3), 3));
            curveGeometry.setFromPoints(this._curve.points);
            var curveMaterial = new THREE.LineBasicMaterial({color : new THREE.Color(this._colors[0], this._colors[1], this._colors[2]) });
            var curveMesh = new THREE.LineSegments(curveGeometry, curveMaterial);//new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);
            this._curve.mesh = curveMesh;
            this._curve.mesh.castShadow = true;

            this._element.geometry.dispose()
            this._element.material.dispose()
            this._element = this._curve.mesh;
            this.appendElement.add(this._element);
            this._makeFatline();
      }
      _makeFatline(){
            var fatline_positions = [];
            var fatline_colors = [];

            for(let i = 0 ; i < this._localPoints.length ;i++)
            {
                  fatline_positions.push(this._localPoints[i].x, this._localPoints[i].y, this._localPoints[i].z);
                  fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
            }

            var fatlineGeometry = new THREE.LineGeometry();
            fatlineGeometry.setPositions(fatline_positions);
            fatlineGeometry.setColors(fatline_colors);

            var fatlineMaterial = new THREE.LineMaterial({
                  color : 0xffffff,
                  linewidth : this.width,
                  vertexColors : THREE.VertexColors,
            });
            fatlineMaterial.resolution.set(window.innerWidth, window.innerHeight);
            this._fatLine = new THREE.Line2(fatlineGeometry, fatlineMaterial);
            this._fatLine.computeLineDistances();
            this._fatLine.scale.set(1,1,1);
            this.appendElement.add(this._fatLine);
      }
      _rgbToArray(color){
            var color_arr = color.substring(4, color.length-1)
                  .replace(/ /g, '')
                  .split(',');
            return [color_arr[0]/255,color_arr[1]/255,color_arr[2]/255]
      }
      _hexToRgb(hex) {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                  return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                  r: parseInt(result[1], 16) / 255,
                  g: parseInt(result[2], 16) / 255,
                  b: parseInt(result[3], 16) / 255
            } : null;
      }

      //라인 업데이트 합수
      _Update(){
            this._curve.points = this._localPoints;
            this._fatLine.material.linewidth = this.width;

            var fatline_positions = [];
            var fatline_colors = [];

            for(var i = 0; i < this._localPoints.length; i++)
            {
                  fatline_positions.push(this._localPoints[i].x, this._localPoints[i].y, this._localPoints[i].z);
                  fatline_colors.push(this._colors[0], this._colors[1], this._colors[2]);
            }

            this._fatLine.geometry.dispose();
            var fatlineGeometry = new THREE.LineGeometry();
            fatlineGeometry.setPositions(fatline_positions);
            fatlineGeometry.setColors(fatline_colors);
            this._fatLine.geometry=(fatlineGeometry);

            //피킹 라인 업데이트
            var pickingLinePoint = this._curve.getPoints(this._linePickingSegments);
            this._element.geometry.removeAttribute('position');
            var pts = [];
            for(var i = 0; i < pickingLinePoint.length; i++)
            {
                  pts.push(pickingLinePoint[i].x, pickingLinePoint[i].y, pickingLinePoint[i].z);
            }
            this._element.geometry.addAttribute('position', new THREE.Float32BufferAttribute( pts, 3 ) );
            this._element.geometry.needsUpdate = true;
            this._element.geometry.attributes.position.needsUpdate = true;
            this._element.geometry.computeBoundingBox();
            this._element.material.color = new THREE.Color(this._colors[0], this._colors[1], this._colors[2])


      }
      _onCommitProperties(){
            super._onCommitProperties();
            if(this._updatePropertiesMap.has("setter.color"))
            {
                  var cl =this.getGroupPropertyValue("setter", "color");
                  if(cl.indexOf('#') > -1)
                  {
                        var c = this._hexToRgb(cl);
                        this._colors = [c.r, c.g, c.b]
                  }else{
                        this._colors = this._rgbToArray(this.getGroupPropertyValue("setter", "color"));
                  }
                  this._Update();
            }
            if(this._updatePropertiesMap.has("line_width"))
            {
                  if(this.width < 1)
                        this.width = 1;
                  this._Update();
            }
            //이동, 회전, 스케일등을 줌
            if(this._updatePropertiesMap.has("setter.position") || this._updatePropertiesMap.has("setter.rotation") || this._updatePropertiesMap.has("setter.size"))
            {
                  this.validateCallLater(this._syncPropertyToHelper)
            }
            //패널에서 월드 좌표가 변경됨
            if(this._updatePropertiesMap.has("syncPoint.point"))
            {
                  if(this.isEditorMode)
                        this.validateCallLater(this._syncWorldLocal)
            }




            if (this._updatePropertiesMap.has("link")) {
                  this.validateCallLater(this._validateLink);
            }


            //외부에서 코드로 점들을 삽입함
            if(this._updatePropertiesMap.has("setter.points"))
            {
                  this.validateCallLater(this._syncInputedPoints);
            }


      }
      _syncInputedPoints(){
            for(let i = 0 ; i < this.points.length; i++)
            {
                  this._points[i].x = this.points[i].x;
                  this._points[i].y = this.points[i].y;
                  this._points[i].z = this.points[i].z;
                  var v = this.points[i].clone();
                  this.appendElement.worldToLocal(v);
                  this._localPoints[i] = v;
            }
            this._lineAdjustment();
            this._Update();
      }
      _syncPropertyToHelper(){
            //행렬을 업데이트 시켜주고.
            this.appendElement.updateMatrixWorld(true);

            //로컬좌표를 다시 변환하여 월드에 셋팅한다.
            for(let i = 0; i < this._localPoints.length; i++)
            {
                  //에디터 모드라면 헬퍼들의 속성을 조절해줘야 한다.(월드 로컬 모두)
                  if(this.isEditorMode)
                  {
                        //점도 계산되어 바뀌어야 한다.
                        var v = this._localPoints[i].clone();
                        this.appendElement.localToWorld(v);

                        var worldHelper = this.appendElement.getObjectByName("world"+i);
                        var localHelper = this.appendElement.getObjectByName("local"+i);

                        if(worldHelper !== undefined)
                        {
                              worldHelper.position.copy(v);
                              this._points[i] = v;
                        }
                        //월드 헬퍼는 안보이니까 굳이 필요 없다.
                        //로컬 헬퍼 박스는 스케일 회전을 다시 원상복구 시켜야 한다.
                        if(localHelper  !== undefined){
                              var scale = this.appendElement.getWorldScale();
                              localHelper.rotation.set(0, 0, 0);
                              localHelper.scale.set(1/(scale.x), 1/(scale.y), 1/(scale.z))
                        }
                  }else{
                        //에디터모드가 아니라면 점 싱크 처리만 하면 된다.
                        this.appendElement.localToWorld(this._localPoints[i]);
                  }
            }

      }
      _syncWorldLocal(){
            //월드좌표를 보고 로컬을 다시 바꿔준다.
            this.appendElement.updateMatrixWorld(true);
            for(let i = 0 ; i < this._points.length; i++)
            {
                  var v = this._points[i].clone();
                  this.appendElement.worldToLocal(v);
                  var worldHelper = this.appendElement.getObjectByName("world"+i);
                  var localHelper = this.appendElement.getObjectByName("local"+i);

                  if(this.isEditorMode)
                  {
                        if(worldHelper !== undefined)
                        {
                              worldHelper.position.copy(this.points[i]);
                        }
                        //월드 헬퍼는 안보이니까 굳이 필요 없다.
                        //로컬 헬퍼 박스는 스케일 회전을 다시 원상복구 시켜야 한다.
                        if(localHelper  !== undefined)
                        {
                              localHelper.position.copy(v);
                        }
                  }
                  //에디터, 뷰어 상관없이 점은 바뀌어야 한다
                  this._localPoints[i] = v;
            }

            //중점을 조정한다
            this._lineAdjustment();

            //라인을 다시 그려준다
            this._Update();
      }
      _syncLocalWorld(){
            //로컬좌표를 보고 월드를 바꿔준다
            this.appendElement.updateMatrixWorld(true);
            for(let i = 0 ;i  < this._localPoints.length; i++)
            {
                  var v = this._localPoints[i].clone();
                  this.appendElement.localToWorld(v);
                  var worldHelper = this.appendElement.getObjectByName("world"+i);

                  if(this.isEditorMode)
                  {
                        if(worldHelper !== undefined)
                        {
                              worldHelper.position.copy(v);
                        }
                  }
                  //에디터, 뷰어 상관없이 점은 바뀌어야 한다
                  this._points[i] = v;
            }
      }
      _lineAdjustment(){
            //월드 점이 움직이던, 로컬 점이 움직이던
            //라인의 중점과, 로컬의 원점과 싱크를 맞춰줍니다.(평행이동을 해서~)
            this.appendElement.updateMatrixWorld(true);
            if(this._curve !== null)
            {
                  this._curve.points = this._localPoints;
                  var center = this._curve.getPoint(0.5).clone();
                  var move = center.clone();
                  this.appendElement.localToWorld(move);

                  if(this.isEditorMode)
                  {
                        //월드 포지션 싱크를 맞춰줍니다.
                        for(let i = 0 ; i < this._points.length; i++)
                        {
                              var worldHelper = this.appendElement.getObjectByName("world"+i);
                              if(worldHelper !== undefined)
                              {
                                    worldHelper.position.copy(this._points[i])
                              }
                        }
                  }
                  //로컬 포지션 싱크만 맞춰줍니다.
                  //월드 포지션은 어차피 원래 설정하고자 하는 좌표가 되어 있기 때문입니다.
                  for(let i = 0 ; i < this._points.length; i++)
                  {
                        var v = this._points[i].clone();
                        this.appendElement.worldToLocal(v);
                        v.sub(center);
                        if(this.isEditorMode)
                        {
                              var localHelper = this.appendElement.getObjectByName("local"+i);
                              if(localHelper !== undefined)
                              {

                                    localHelper.position.x = v.x;
                                    localHelper.position.y = v.y;
                                    localHelper.position.z = v.z;
                              }
                        }
                        this._localPoints[i] = v;
                  }

                  this.appendElement.position.x += center.x;
                  this.appendElement.position.y += center.y;
                  this.appendElement.position.z += center.z;
                  // var pos = {x : this.appendElement.position.x, y : this.appendElement.position.y, z : this.appendElement.position.z}
                  // this.setGroupPropertyValue("setter","position", pos )
                  this._properties.setter.position.x =  parseInt(this.appendElement.position.x);
                  this._properties.setter.position.y =  parseInt(this.appendElement.position.y);
                  this._properties.setter.position.z =  parseInt(this.appendElement.position.z);
            }
      }
      _onHelperChange(e){
            //움직이때마다 로컬 헬퍼 좌표와 월드 헬퍼 좌표를 바꾼다.
            for(let i = 0 ; i < this._localPoints.length; i++)
            {
                  var localHelper = this.appendElement.getObjectByName("local"+i);
                  if(localHelper !== undefined)
                  {

                        this._localPoints[i] = localHelper.position.clone();
                        var v = localHelper.position.clone();
                        this.appendElement.localToWorld(v);
                        this._points[i] = v;
                        this._syncLocalWorld();
                  }
            }
            this._Update();
      }
      _onTransformMouseUp(e){
            var ctrl = window.wemb.mainPageComponent.threeLayer._transformControls;
            ctrl.detach(e.target);
            this._lineAdjustment()
            this._Update();
            ctrl.space = "local";
      }
      _onHelperClick(e){
            e.stopPropagation();
            e.origDomEvent.stopPropagation();
            let ctrl= window.wemb.mainPageComponent.threeLayer._transformControls;
            ctrl.attach(e.target);
            ctrl.space = "local";
      }
      _onDestroy(){
            for(var i = 0; i < this.appendElement.children.length; i++)
            {
                  if(this.appendElement.getObjectByName('local'+i) !== undefined){
                        var obj = this.appendElement.getObjectByName('local'+i);
                        this.appendElement.remove(obj);
                        obj.geometry.dispose();
                        obj.material.dispose();
                        obj = this.appendElement.getObjectByName('world'+i);
                        this.appendElement.remove(obj);
                        obj.geometry.dispose();
                        obj.material.dispose();
                        this._domEvents.removeEventListener(obj, 'click', this._onHelperClickCalle, false);
                  }
            }
            this._controls.removeEventListener('objectChange', this._onHelperChangeCalle);
            this._controls.removeEventListener('mouseUp', this._onTransformMouseUpCalle);

            this.appendElement.remove(this._fatLine);
            this._fatLine.geometry.dispose();
            this._fatLine.material.dispose();
            this._fatLine = null;

            this.appendElement.remove(this._curve);
            this._curve.mesh.geometry.dispose();
            this._curve.mesh.material.dispose();
            this._curve = null;

            this._localPoints = [];
            this._points = [];
            super._onDestroy();
      }

      get pointCount() {
            return this.getGroupPropertyValue("setter", "pointCount");
      }
      set width(wd){
            if(wd < 1)
                  this.setGroupPropertyValue("line_width", "width", 1);
            else
                  this.setGroupPropertyValue("line_width", "width", wd);

      }
      get width(){
            return this.getGroupPropertyValue("line_width", "width");
      }
      get points(){
            return this.getGroupPropertyValue("setter", "points");
      }
      set points(pts)
      {
            if(pts.length === 2)
            {
                  this._checkUpdateGroupPropertyValue("setter", "points", pts)
            }else{
                  console.warn("Err : 경고 : 일반 라인은 점 두개를 입력하셔야 합니다.");
            }

      }

      changePointByIndex(idx, point)
      {
            if(idx>=0 && idx < this.points.length)
            {
                  try {
                        let targetPoint= this.points[idx];
                        targetPoint.set(point.x, point.y, point.z);
                        this._syncInputedPoints();
                  }catch(error){
                        console.log("ERROR ", error);
                  }
            }else{
                  console.warn("올바른 입력 형식이 아닙니다. (number, THREE.Vector3)")
            }
      }





      _validateLink() {
            let linkInfo = this.getGroupProperties("link");
            let fromComInstance = null;
            let toComInstance = null;

            let proxy = null;
            if (wemb.configManager.isEditorMode) {
                  proxy = wemb.editorProxy;
            } else {
                  proxy = wemb.viewerProxy;
            }

            if (linkInfo.fromTarget) {
                  fromComInstance = proxy.threeLayerInstanceList.find((comInstance) => {
                        if (linkInfo.fromTarget == comInstance.name) {
                              return true;
                        } else {
                              return false;
                        }
                  })
            }

            if (linkInfo.toTarget) {
                  toComInstance = proxy.threeLayerInstanceList.find((comInstance) => {
                        if (linkInfo.toTarget == comInstance.name) {
                              return true;
                        } else {
                              return false;
                        }
                  })
            }

            this._drawUpdateLinkLine(fromComInstance, toComInstance);
      }





      _drawUpdateLinkLine(fromComInstance, toComInstance) {
            try {
                  if (fromComInstance) {
                        this.changePointByIndex(0, fromComInstance.position);
                  }

                  if (toComInstance) {
                        this.changePointByIndex(1, toComInstance.position);
                  }
            }catch(error){
                  console.warn("ERROR ", error);
            }

      }


      /*
    외부에서 호출
     */
      updateLinkLine() {
            this._validateLink();

      }


      onLoadPage() {
            this._validateLink();
      }


      get toTarget() {
            let link = this.getGroupProperties("link");
            return link.toTarget;
      }

      get fromTarget() {
            let link = this.getGroupProperties("link");
            return link.fromTarget;
      }




}
WV3DPropertyManager.attach_default_component_infos(MLine3DComponent, {
      "setter": {
            "size": {x: 10, y: 10, z: 10},
            "pointCount" : 2,
            "points" : [],
      },
      "label": {
            "label_text": "MLine3DComponent",
            "label_line_size": 15,
            "label_background_color": "#3351ED",
            "label_using": "N",
      },
      "syncPoint" : {
            "point" : "",
            "scale" : 1
      },
      "info": {
            "componentName": "MLine3DComponent",
            "version": "1.0.0",
      },
      "line_width" : {
            "width" : 5
      },
      "link":{
            "fromTarget":"",
            "toTarget":""
      }
});
WVPropertyManager.add_property_group_info(MLine3DComponent, {
      label: "설정 정보",
      name: "pointCount",
      children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "pointCount",
            show: true,
            writable: true,
            defaultValue: 2,
            description: "헬퍼의 갯수"
      }]
});
WVPropertyManager.add_property_panel_group_info(MLine3DComponent, {
      label: "controlPoint",
      template: "vector",
      children: [{
            owner: "syncPoint",
            name: "point",
            type: "object",
            label: "length",
            show: true,
            writable: true,
            description: "point"
      }]
});
WV3DPropertyManager.add_property_panel_group_info(MLine3DComponent, {
      label : "Line Properties",
      template : "vertical",
      children : [
            {
                  owner : "line_width",
                  name : "width",
                  type : "number",
                  label : "width",
                  show : true,
                  writable : true,
                  description : "선 두께"
            }
      ]
});


WV3DPropertyManager.add_property_panel_group_info(MLine3DComponent, {
      label : "링크 설정",
      template : "magnetic-link",
      children : [
            {
                  owner : "link",
                  name : "fromTarget",
                  type : "string",
                  label : "from",
                  show : true,
                  writable : true,
                  defaultValue: "",
                  description : "from정보입니다."
            },{
                  owner : "link",
                  name : "toTarget",
                  type : "string",
                  label : "to",
                  show : true,
                  writable : true,
                  defaultValue: "",
                  description : "to정보입니다."
            }
      ]
});


