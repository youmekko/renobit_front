/*
plugin은 모두 mediator의 view로 등록됨.
 */
class ForeignManager extends ExtensionPluginCore {

      get lineComponentName(){
            return this._lineComponentName;
      }



      constructor() {
            super();
            this._$bus = new Vue();
            this._selectProxy = null;
            this._lineComponentName = "MLine3DComponent";

            console.log("ForeignManager start 2");
      }



      start() {
            window.wemb.forginManager = ForeignManager._instance;

            /*
		에디터 모드에서만 템플릿 등록하기
		 */
            if (wemb.configManager.isEditorMode) {
                  ForeignPackTemplateRegister.regist();
            }
      }


      setFacade(facade) {
            super.setFacade(facade);
            this._selectProxy = this._facade.retrieveProxy(SelectProxy.NAME);
      }

      get $bus() {
            return this._$bus;
      }

      $on(eventName, data) {
            return this._$bus.$on(eventName, data);
      }


      destroy() {
      }

      get notificationList() {
            return [
                  SelectProxy.NOTI_UPDATE_PROPERTIES
            ]
      }


      /*
	  noti가 온 경우 실행
	  call : mediator에서 실행
	   */
      handleNotification(note) {
            switch (note.name) {
                  // 편집 페이지가 열릴때마다 실행
                  case SelectProxy.NOTI_UPDATE_PROPERTIES:
                        this.noti_updateProperties();
            }

      }


      initProperties() {
      }

      /*
	컴포넌트 프로퍼티가 업데이트되면 실행.

	컴포넌트 중 링크 컴포넌트에 연결된 컴포넌트 위치가 변경되는 경우에만
	링크 컴포넌트를 찾아 업데이트 처리한다.
	방법:
		단계01: 현재 선택되어 있는 컴포넌트 중에서 링크 컴포넌트에 연결할 수 있는 컴포넌트만 찾는다.
		단계02: 단계01에서 찾은 타겟요소가 속한  LinkComponent를 찾는다.
		단계03: 단계02에서 찾은 LinkComponent를 업데이트 처리한다.


	 */
      noti_updateProperties() {


            // 단계01: 현재 선택되어 있는 컴포넌트 중에서 링크 컴포넌트에 연결할 수 있는 컴포넌트만 찾는다.
          /*  let tempTargetComInstanceList = this._selectProxy.currentPropertyManager.getInstanceList().filter((comInstance)=>{
                  if(comInstance.componentName==this._targetComponentName){
                        return true;
                  }

                  return false;
            })


            // 선택한 컴포넌트 중 타겟 컴포넌트 인스턴스가 0이면 취소 처리
            if(tempTargetComInstanceList.length<=0){
                  return;
            }*/




            // 단계02: 링크 컴포넌트만들 골라 낸다.
            let lineComInstanceList = wemb.editorProxy.threeLayerInstanceList.filter((comInstance)=>{

                  if(comInstance.componentName==this._lineComponentName){
                        return true;
                  }else {
                        return false;
                  }
            })

            if(lineComInstanceList.length<=0){
                  return;
            }




            // 단계03: 단계02중 단계01을 가진 컴포넌트너 골라내기
            let tempLineComInstanceList = lineComInstanceList.filter((lineComInstance)=>{

                  let lineCom=this._selectProxy.currentPropertyManager.getInstanceList().find((targetComInstance)=>{
                        if(lineComInstance.toTarget == targetComInstance.name)
                              return true;

                        if(lineComInstance.fromTarget == targetComInstance.name)
                              return true;

                        return false;

                  })

                  if(lineCom)
                        return true;

                  return false;

            })


            // 연결된 링크 컴포넌트만 업데이트 처리
            if(tempLineComInstanceList){
                  tempLineComInstanceList.forEach((lineComInstance)=>{
                        console.log("OK!!!!");
                        lineComInstance.updateLinkLine();
                  });
                  wemb.editorProxy.sendNotification(SelectProxy.NOTI_SYNC_TRANSFORM_LOCATION_SIZE)
            }
      }
      /////////////////////////////////////////////////////////////

}
