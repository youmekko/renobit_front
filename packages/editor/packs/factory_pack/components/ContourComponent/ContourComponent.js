class ContourComponent  extends NWV3DComponent {

      constructor(){
            super();
            this.grayTexture;
            this.colorTexture;
            this.element;
            this.dataProvider = [];
            this.mapGenerator;
            this._useBackGround = true;
            this.divisor = 1;
            this.space;
            this.type = "";
            this.contourInfo = {
                  temperature:{
                        colorGradient:{
                              0.2: '#00FFFF',
                              0.4: '#0099FF',
                              0.6: '#00FF00',
                              0.8: '#ffff00',
                              1.0: '#ff0000'
                        },
                        grayGradient:{
                              0.2: '#333333',
                              0.4: '#666666',
                              0.6: '#999999',
                              0.8: '#CCCCCC',
                              1.0: '#ffffff'
                        }
                  },
                  background:"black",
                  humidity:{
                        colorGradient:{
                              0.2: '#58b1cb',
                              0.4: '#02D5F9',
                              0.6: '#067acb',
                              0.8: '#0566cb',
                              1.0: '#004AFA'
                        },
                        grayGradient:{
                              0.2: '#333333',
                              0.4: '#666666',
                              0.6: '#999999',
                              0.8: '#CCCCCC',
                              1.0: '#ffffff'
                        }
                  }
            }
            this.tempSections = [0, 100];
            this.humiSections = [0, 100];
            this.displacementScale = 50;
            this.ContourMapGenerator = null;
            this.radius;
      }

      _onCreateProperties() {
            super._onCreateProperties();
            //프로퍼티 삭제 -> 지금 propertypanel 에 null을 넣어서 프레임을 키웠으므로 패널에 티가 나지 않는다
            WVPropertyManager.removePropertyGroupChildrenByName(ContourComponent.property_info, "display", "size");
            WVPropertyManager.removePropertyGroupChildrenByName(ContourComponent.property_info, "normal", "color");

            this._rectWidth = this.getBestPowerOf2(this.getGroupPropertyValue("rect", "rect_width"));
            this._rectHeight = this.getBestPowerOf2(this.getGroupPropertyValue("rect", "rect_height"));

      }
      _onCreateElement() {
            super._onCreateElement();
            this.createContourMap();
            this._makeInnerPoints();
            this._makeCurve();
      }
      _onCommitProperties(){
            super._onCommitProperties();
            if(this._updatePropertiesMap.has("rect.rect_width") || this._updatePropertiesMap.has("rect.rect_height"))
            {
                  this._rectWidth = this.getBestPowerOf2(this.getGroupPropertyValue("rect", "rect_width"));
                  this._rectHeight = this.getBestPowerOf2(this.getGroupPropertyValue("rect", "rect_height"));
                  this._makeInnerPoints();
                  this._curve.points = this._innerPoints;
                  this._curve.mesh.geometry.setFromPoints(this._curve.points);
                  this._curve.mesh.geometry.attributes.position.needsUpdate = true;

                  let minSide = Math.min(this._rectWidth, this._rectHeight);
                  let radius = (minSide / this.divisor) / 2;
                  let blur = radius * 1.4;
                  this._element.geometry.dispose();
                  this._outlineElement = null;
                  this._element.geometry = new THREE.PlaneBufferGeometry(this._rectWidth * 2, this._rectHeight * 2, 128, 128);
                  this._validateOutline();
                  this.ContourMapGenerator.colorLayer.width = this._rectWidth;
                  this.ContourMapGenerator.colorLayer.height = this._rectHeight;
                  this.ContourMapGenerator.grayLayer = this.cloneCanvas(this.colorLayer);
                  this.ContourMapGenerator.clear();
            }
      }
      createContourMap(){
            let minSide = Math.min(this._rectWidth * 2, this._rectHeight * 2);
            let radius = (minSide / this.divisor) / 2;
            let blur = radius * 1.4;

            this.ContourMapGenerator = new MapGenerator({width : this._rectWidth * 2, height : this._rectHeight * 2,radius : radius, blur : blur});
            this.changeType('temperature');

            var geometry                = new THREE.PlaneGeometry( this._rectWidth * 2, this._rectHeight * 2, 128, 128);
            this.colorTexture             = new THREE.Texture( this.ContourMapGenerator.colorLayer );
            this.colorTexture.needsUpdate = true;

            this.grayTexture              = new THREE.Texture( this.ContourMapGenerator.grayLayer );
            this.grayTexture.needsUpdate  = true;

            var material                = new THREE.MeshPhongMaterial();
            material.side                 = THREE.DoubleSide;
            material.map                  = this.colorTexture;
            material.normapMap            = this.colorTexture;
            material.displacementMap      = this.grayTexture;
            material.normalMapScale       = new THREE.Vector2(1, -1);
            material.displacementScale    = this.displacementScale;
            material.displacementBias     =-0.5;
            material.transparent          = true;
            // material.wireframe            = true;

            this._element                 = new THREE.Mesh( geometry, material );
            this._element.rotation.x = -Math.PI / 2;
            this.parentBoundingBox = new THREE.Box3().setFromObject(this._element);

            var pos = this.getGroupPropertyValue("setter","position");
            var v = new THREE.Vector3(parseInt(pos.x), parseInt(pos.y), parseInt(pos.z));
            this.parentBoundingBox.min.add(v);
            this.parentBoundingBox.max.add(v);
            this._element.rotation.x = -0;
            this.appendElement.add(this._element);
      }
      setDataProvider( objProvider ){
            if(this.dataProvider != objProvider ){
                  this.dataProvider = objProvider;
                  this.update();
            }
      }
      getBestPowerOf2( value ){
            let MAX_SIZE = 4096;
            var p = 1;
            while (p < value)
                  p <<= 1;

            if (p > MAX_SIZE)
                  p = MAX_SIZE;
            return p;
      }
      _makeInnerPoints(){
            this._innerPoints = [];
            this._innerPoints[0] = new THREE.Vector3(-1 * this._rectWidth, -1 * this._rectHeight, 0);
            this._innerPoints[1] = new THREE.Vector3(-1 * this._rectWidth, this._rectHeight, 0);
            this._innerPoints[2] = new THREE.Vector3(this._rectWidth, this._rectHeight, 0);
            this._innerPoints[3] = new THREE.Vector3(this._rectWidth, -1 * this._rectHeight, 0);
            this._innerPoints[4] = new THREE.Vector3(-1 * this._rectWidth, -1 * this._rectHeight, 0);
      }
      _makeCurve(){
            this._curve = new THREE.CatmullRomCurve3(this._innerPoints);
            this._curve.tension = 0;
            this._curve.curveType = 'catmullrom';

            var curveGeometry = new THREE.BufferGeometry();
            curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curveSegments * 3),3));
            curveGeometry.setFromPoints(this._curve.getPoints(100));
            var curveMaterial = new THREE.LineBasicMaterial({color : 0xff0000});
            var curveMesh = new THREE.Line(curveGeometry, curveMaterial);//new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);

            this._curve.mesh = curveMesh;
            this.appendElement.add(this._curve.mesh);
      }
      changeType(type){
            if(this.type != type){
                  this.type = type;
                  let colors = this.contourInfo[type].colorGradient;
                  let grays = this.contourInfo[type].grayGradient;
                  this.ContourMapGenerator.setGradient(colors, grays, this.useBackGround)
            }
      }
      clear(){
            this.ContourMapGenerator.clear();
            this.ContourMapGenerator.draw();
            this.colorTexture.needsUpdate=true;
            this.grayTexture.needsUpdate=true;
      }
      calculate( min, max, value ){
            return (value-min)/(max-min);
      }
      paint( info ){
            let isTemp  = (info.type == "temperature" );
            let min     = isTemp ? this.tempSections[0] : this.humiSections[0];
            let max     = isTemp ? this.tempSections[1] : this.humiSections[1];
            let value   = isTemp ? info.temp : info.humi;
            let force   = this.calculate( min, max, value );
            let spacePosition = this.parentBoundingBox.getParameter(info.object.appendElement.position );
            let px = spacePosition.x * this.colorTexture.image.width - this._element.position.x;
            let py = spacePosition.z * this.colorTexture.image.height - this._element.position.z;
            this.ContourMapGenerator.add( px, py, force);
      }
      update(){
            this.clear();
            let max = this.dataProvider.length;
            let sensorVO, idx;
            for(idx=0; idx<max; idx++){
                  sensorVO = this.dataProvider[idx];
                  sensorVO.type = this.type;
                  this.paint( sensorVO );
            }

            this.ContourMapGenerator.draw();
            this.colorTexture.needsUpdate=true;
            this.grayTexture.needsUpdate=true;
      }
      set useBackGround( value ){
            if(this._useBackGround != value ){
                  this._useBackGround = value;
            }
      }

      get useBackGround(){
            return this._useBackGround;
      }
      _onDestroy(){
            super._onDestroy();
      }
}

class MapGenerator {

      constructor(opt) {
            this.blur = opt.blur;
            this.radius = opt.radius;
            this.colorLayer = document.createElement("canvas");
            this.colorLayer.width = opt.width;
            this.colorLayer.height = opt.height;
            this.grayLayer = this.cloneCanvas(this.colorLayer);
            this.heatmap = simpleheat(this.colorLayer);
            this.graymap = simpleheat(this.grayLayer);
            this.heatmap.radius(opt.radius, opt.blur).max(1);
            this.graymap.radius(opt.radius, opt.blur).max(1);
      }

      setGradient(colorGradient, grayGradient, useBackGround) {
            this.heatmap.gradient(colorGradient);
            this.graymap.gradient(grayGradient);
            // 배경 사용 시 가장 낮은 컬러값을 적용
            if (useBackGround) {
                  let prop = Object.keys(colorGradient).sort();

                  this.heatmap.bgColor(colorGradient[prop[0]]);
                  this.graymap.bgColor(grayGradient[prop[0]]);
            }
      }

      cloneCanvas(origin) {
            var clone = document.createElement("canvas");
            clone.width = origin.width;
            clone.height = origin.height;
            var context = clone.getContext("2d");
            context.drawImage(origin, 0, 0);
            return clone;
      }

      add(x, y, value) {
            this.heatmap.add([x, y, value]);
            this.graymap.add([x, y, value]);
      }

      clear() {
            this.heatmap.clear();
            this.graymap.clear();
      }

      draw() {
            this.heatmap.draw();
            this.graymap.draw();
      }

      destroy() {
            this.heatmap = null;
            this.graymap = null;
            this.colorLayer = null;
            this.grayLayer = null;
      }

      changeColorLayer(opt){
            this.clear();
            this.colorLayer.width = opt.width;
            this.colorLayer.height = opt.height;
            this.grayLayer = this.cloneCanvas(this.colorLayer);
            this.heatmap = simpleheat(this.colorLayer);
            this.graymap = simpleheat(this.grayLayer);
            this.heatmap.radius(opt.radius, opt.blur).max(1);
            this.graymap.radius(opt.radius, opt.blur).max(1);
      }
}
WV3DPropertyManager.attach_default_component_infos(ContourComponent, {
      "setter": {
            "size": {x: 10, y: 10, z: 10},
            "rotation" : {x : -90, y : 0, z : 0}
      },
      "label": {
            "label_text": "ContourComponent",
            "label_line_size": 15,
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "ContourComponent",
            "version": "1.0.0",
      },
      "rect" : {
            "rect_width" : 30,
            "rect_height" : 30
      },
      "plot" : {
            "blur" : 1.4,
            "displacement" : 50,
            "type" : "temperature",
            "temperature_range" : { "min" : 0, "max" : 100},
            "humidity_range" : { "min" : 0, "max" : 100}
      }
});

WV3DPropertyManager.add_property_panel_group_info(ContourComponent, {
      label : "Shape Properties",
      template : "vertical",
      children : [
            {
                  owner : "rect",
                  name : "rect_width",
                  type : "number",
                  label : "rect_width",
                  show : true,
                  writable : true,
                  description : "가로 길이"
            },
            {
                  owner : "rect",
                  name : "rect_height",
                  type : "number",
                  label : "rect_height",
                  show : true,
                  writable : true,
                  description : "세로 길이"
            }
      ]
});

WV3DPropertyManager.add_property_panel_group_info(ContourComponent, {
      label : "Plot Properties",
      template : "vertical",
      children : [
            {
                  owner : "plot",
                  name : "blur",
                  type : "number",
                  label : "blur",
                  show : true,
                  writable : true,
                  description : "블러 범위 파라미터"
            },
            {
                  owner : "plot",
                  name : "displacement",
                  type : "number",
                  label : "displacement",
                  show : true,
                  writable : true,
                  description : "그래프 봉우리 높이 파라미터"
            },
            {
                  owner : "plot",
                  name : "type",
                  type : "string",
                  label : "type",
                  show : true,
                  writable : true,
                  description : "그래프 타입"
            },
            {
                  owner : "plot",
                  name : "temperature_range",
                  type : "object",
                  label : "temperature_range",
                  show : true,
                  writable : true,
                  description : "온도 표현값 - 최소"
            },
            {
                  owner : "plot",
                  name : "humidity_range",
                  type : "object",
                  label : "humidity_range",
                  show : true,
                  writable : true,
                  description : "온도 표현값 - 최소"
            }
      ]
});
