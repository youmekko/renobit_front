class VNVManager extends ExtensionPluginCore {
	constructor() {
		super();

		// this._sensorList = [];
		this.gridValues = new Array();
		this.tempList = new Array();
		// this.gridObj = {};
	}

	start() {
		window.wemb.vnvManager = VNVManager._instance;
	}

	listClear() {
		// this.gridValues = [];
	}

	listUp(upperText, lowerText) {
		this.gridValues = [];
		if(upperText.alarm) {
			this.gridValues.push(upperText);
		}

		if(lowerText.alarm) {
			this.gridValues.push(lowerText);
		}
	}

	dataPush(sensorList) { // 내접해 있는 센서 리스트 건내 받음
		let contiText = " (";


		sensorList.forEach((item, index) => {
			item.setGroupPropertyValue("setter", "severity", "critical");
			if(index == 0) {
				contiText += item.name;
			}else{
				contiText += ", "+item.name;
			}
		})
		if(sensorList.length > 0) {
			contiText += " 유저에게 SMS 발송)";
		}else {
			contiText += "인접 태그가 존재하지 않습니다.)";
		}
		this.gridValues.forEach((item, index) => {
			let value = $.extend(true, {}, item);
			value.alarm += contiText;
			this.tempList.push(value);
		});

		let dataGird = wemb.mainPageComponent.comInstanceList.find((instance) => {
			return instance.componentName.indexOf("BasicGridComponent") != -1;
		})

		dataGird.dataProvider = null;
		dataGird.dataProvider = this.tempList;
	}

}

VNVManager.SensorVO = {
	"sensor": "",
	"tagname": "",
	"alarm": "",
	"time": ""
}