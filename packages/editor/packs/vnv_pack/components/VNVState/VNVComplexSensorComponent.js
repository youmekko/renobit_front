class VNVComplexSensorComponent extends WVDOMComponent {
	constructor() {
		super();
		this.isResourceLoaded = false;
		this.isSettingsReady = false;
		this.$container = null;
		this.animateClip = null;
		this.spriteList = {};
		this.settingObj = {
			width: "",
			height: "",
			fps: 24,
			loop: true,
			columns: "",
			autoplay: false,
			totalFrames: ""
		};

		this.publicMotionOptions = null;
		this._invalidateServerity = false;
		this._isDestroyed = false;
		this._radius = null; // 반지름
		this._centerPoint = {}; // 크리티컬 중심점
		this._inscribed = []; // 내접 센서 배열
		this._sensorName = null;

		this._left = null;
		this._right = null;
		this.leftValue = null;
		this.topValue = null;

	}

	/**
	 * 1. Critical Area Value를 입력받는다.
	 * 2. Value로 critical area를 설정한다.
	 *
	 * 3. 설정한 area 안에 rtls 위치 센서가 존재하는지 확인한다.
	 *   3-1. area에 중심점을 구한다.
	 *   3-2. rtls 위치 센서의 중심점을 구한다.
	 *   3-3. 구 중심점의 거리가 Area의 반지름 보다 작은지 큰지 확인한다.
	 *   3-4. 반지름보다 크면 내접 하지 않는다.
	 *   3-5. 반지름보다 작거나 같으면 내접한다.
	 *   3-6. 내접하면 내접 배열에 넣는다.
	 * 4. 배열 인스턴스들(내접하는 센서)의 ID를 알아낸다.
	 * 5. 내접하는 센서가 하나라도 있을 경우에 메시지를 넣어준다.
	 * @private
	 */

	_onCreateElement() {
		let $element = $(this._element);
		$element.append('<div class="critical-area"></div><div class="state-clip-wrap"></div>');

		// let $ciriticalArea = $element.find(".critical-area");
		// let $stateClip = $element.find(".state-clip-wrap");

		// console.log("asdsadsad", $stateClip.offset(), $stateClip.width(), $stateClip.height());
		// console.log("asdsadsad", $stateClip.offset(), $stateClip.width(), $stateClip.height());

		if (!this.$container) {
			this.createContainer();
		}

		this._createTooltip();
	}

	_createTooltip() {
		let that = this;2
		$(this._element).find(".critical-area").tooltip({
			items: ".state-clip-wrap",
			position: {
				my: "center bottom",
				at: "center bottom-500",
			},
			content: function () {
				// return that.name;
				return "name";
			}
		})
	}

	_onCreateProperties() {
		let info = this.getGroupPropertyValue("setter", "info");
		if (info && info.path) {
			let newInfo = $.extend(true, {}, info);
			newInfo.path = newInfo.path.replace("static/components/2D/symbol/", "client/packs/vnv_pack/components/VNVState/");
			this.setGroupPropertyValue("setter", "info", newInfo);
		}
	}

	_onDestroy() {
		this._isDestroyed = true;
		//this.$container.animateSprite('destroy');
		if (this.animateClip) {
			this.animateClip.destroy();
			this.animateClip = null;
		}
		this.$container.remove();
		this.$container = null;
		this.spriteList = null;
		this.settingObj = null;
		super._onDestroy();
	}

	createContainer() {
		if (this.$container) {
			//this.$container.animateSprite('destroy');
			this.$container.remove();
		}

		if (this.animateClip) {
			this.animateClip.destroy();
		}

		$(this._element).find(".state-clip-wrap").empty();

		this.$container = $("<div><div class='sprite-image'></div></div>");
		this.$container.css({
			"position": "relative",
			"width": "100%",
			"height": "100%",
			"background-image": "url(" + VNVComplexSensorComponent.DEFAULT_IMAGE + ")",
			"background-position": "0 0",
			"background-color": "rgba(255, 255, 255, .6)",
			"background-position": "center center",
			"border": "1px dashed #808080",
			"transform-origin": "left top",
			"overflow": "hidden"
		});

		this.$container.find(".sprite-image").css({
			"position": "absolute",
			"background-position": "0 0",
			"background-repeat": "no-repeat",
			"pointer-events": "none",
			"width": 5000,
			"height": 5000
		})
		if (this.settingObj.width != "") {
			this.$container.css({
				"width": this.settingObj.width,
				"height": this.settingObj.height
			});

			this.calcScale();
		}


		$(this._element).find(".state-clip-wrap").append(this.$container);
	}


	_onCommitProperties() {
		if (this.invalidateSize || this.invalidateVisible) {
			this.validateCallLater(this.calcScale);
		}

		if (this._invalidateServerity) {
			this._invalidateServerity = false;
			this.validateCallLater(this._validateSeverityProperty);
		}

		if (this.invalidateResource) {
			this.validateCallLater(this.dispatchSizeEvent);
			this.invalidateResource = false;
		}

	}

	_onImmediateUpdateDisplay() {
		this.selectProperty();
	}

	dispatchSizeEvent() {
		if (this.isEditorMode) {
			this.dispatchComponentEvent("WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE");
		}

		this.setGroupPropertyValue("setter", "isSaved", true);
	}

	_validateSeverityProperty() {
		this.selectProperty();

		if (!this.isEditorMode) {
			this.dispatchWScriptEvent("change", {
				value: this.severity
			})
		}
	}

	excuteAnimate() {
		if (this.severity) {
			if (!this.$container) {
				this.createContainer();
			}
			;

			this.$container.css({
				"backgroundImage": "none",
				"backgroundColor": "transparent",
				"border": "none"
			});

			this.$container.find(".sprite-image").css({
				"backgroundImage": "url('" + wemb.configManager.serverUrl + this.selectItem.path + this.severity + ".png')"
			});

			this.calcScale();

		} else {
			throw this.exceptionSymbolInfo("this.severity");
		}
	}

	selectProperty() {
		var info = this.getGroupPropertyValue("setter", "info");
		var severity = this.getGroupPropertyValue("setter", "severity");

		this.selectItem = info;
		this.selectItem.path = this.selectItem.path.replace("static/components/2D/symbol/", "client/packs/vnv_pack/components/VNVState/");

		if (severity === "critical") {
			this.drawCriticalArea();
			this.distanceCheck();

			$(this._element).find(".critical-area").show();

		} else {
			$(this._element).find(".critical-area").hide();
		}
		// console.log($(this._element).find(".critical-area"));

		this.createContainer();

		if (!info) {
			this.settingObj.width = "";
			this.settingObj.height = "";

			//    this.savedResourceLoaded();
			return;
		}

		this.setAnimateInfo(info).then(() => {
			//    this.savedResourceLoaded();

			var currentState = this.spriteList[this.severity];

			if (currentState.type != undefined && currentState.type == "motion") {
				var motionOps;

				if (currentState.motionOptions) {
					motionOps = (currentState.motionOptions != undefined) ? currentState.motionOptions : this.publicMotionOptions;
				} else {
					motionOps = this.publicMotionOptions;
				}

				this.settingObj.totalFrames = motionOps.totalFrames;
				this.settingObj.columns = motionOps.columns;

				if (!this.settingObj.totalFrames || !this.settingObj.columns) {
					throw this.exceptionSymbolInfo("totalFrames || columns");
				}

				if (!this.isEditorMode) {
					if (this.animateClip) {
						this.animateClip.destroy();
					}

					this.animateClip = new AnimateSpriteClip(this.$container.find(".sprite-image").get(0), this.settingObj);
					this.animateClip.play();
				}
			}

			this.invalidateResource = true;
			this.invalidateProperties();
		});
	}

	drawCriticalArea() {
		let $element = $(this._element);
		let areaValue = this.getGroupPropertyValue("critical", "area");
		// $element.append('<div class="critical-area"></div><div class="state-clip-wrap"></div>');

		let $criticalArea = $element.find(".critical-area");
		let $stateClip = $element.find(".state-clip-wrap");

		let $width = $stateClip.width();
		let $height = $stateClip.height();

		let criticalAreaSize = 150 * areaValue;
		$criticalArea.css({"width": criticalAreaSize});
		$criticalArea.css({"height": criticalAreaSize});

		if (criticalAreaSize > $element.width()) {
			let tempValue = $criticalArea.width() / 2;

			if(this.leftValue == null) {
				this.leftValue = $criticalArea.offset().left - tempValue + ($width / 2) - 3;
				this.topValue = $criticalArea.offset().top - tempValue + ($width / 2) - 3;
			}
		} else {
			console.log("Critical Area Error");
		}

		let that = this;
		$criticalArea.css({"left": that.leftValue, "top": that.topValue});

		console.log("중심점", this.setter.x - (this.setter.width / 2)); // 크리티컬 중심점
		console.log("반지름", criticalAreaSize / 2);
		this._radius = criticalAreaSize / 2;

		this._centerPoint = {
			x: this.setter.x + (this.setter.width / 2),
			y: this.setter.y + (this.setter.height / 2)
		}

		$(wemb.mainPageComponent.element).append("<div class='test' style='z-index:99999; position:absolute; width:1px; height:1px;'></div>");
		$(wemb.mainPageComponent.element).find(".test").css({
			"left": that._centerPoint.x - 2,
			"top": that._centerPoint.y - 2
		});
	}

	distanceCheck() {
		this._inscribed = []; // 배열 초기화

		let $sensorObj = $(this.element).find(".critical-area");

		// (wemb.mainPageComponent.comInstanceList);
		let list = wemb.mainPageComponent.comInstanceList.filter((instance) => {
			return instance.componentName.indexOf("BasicStateClipComponent") != -1;
		})

		list.forEach((ins, index) => {
			//  3-3. 중심점의 거리가 Area의 반지름 보다 작은지 큰지 확인한다.
			// disY = ins.top - this._centerPoint.y

			var _value = 17;
			let disX = Math.abs(ins.x + _value - this._centerPoint.x);
			let disY = Math.abs(ins.y + _value - this._centerPoint.y);
			let dis = Math.sqrt(Math.abs(disX * disX) + Math.abs(disY * disY)); // 거리 구하는 공식

			if (this._radius >= dis) {
				this._inscribed.push(ins);
			}

			if(this._left == null) {
				this._left = $(ins.element).offset().left;
				this._top = $(ins.element).offset().top;
			}

			let that = this;
			$(wemb.mainPageComponent.element).append("<div class='test" + index + "' style='z-index:99999; position:absolute; width:3px; height:3px;'></div>");
			$(wemb.mainPageComponent.element).find(".test" + index).css({
				"left": that.left + 18,
				"top": that.top + 20
			});
		})

		window.wemb.vnvManager.dataPush(this._inscribed);

		//dis = Math.sqrt(Math.abs(disX*disX) + Math.abs(disY*disY)); // 거리 구하는 공식

		// this._templateInstanceList = this._pageDataList.content_info.two_layer.filter((instance) => {
		// 	return instance.name.indexOf("tmp_") != -1;
		// })
		// console.log(wemb.mainPageComponent.comInstanceList);

	}

	procAnimateInfo(filePath) {
		var locale_msg = Vue.$i18n.messages.wv;
		return wemb.$http.get(wemb.configManager.serverUrl + filePath + "info.json").then((result) => {
			return result.data;
		}, (error) => {
			Vue.$alert(filePath + 'Info.json ' + locale_msg.common.errorFormat, 'Error', {
				confirmButtonText: 'OK',
				type: "error",
				dangerouslyUseHTMLString: true
			});
		})
	}

	setSettingObj(result) {
		if (this._isDestroyed) {
			return;
		}

		this.settingObj.width = result.width;
		this.settingObj.height = result.height;


		if (this.isEditorMode && !this.isSaved) {
			this.width = this.settingObj.width;
			this.height = this.settingObj.height;
		}

		if (result.motionOptions != undefined) {
			if (!result.motionOptions.totalFrames || !result.motionOptions.columns) throw this.exceptionSymbolInfo("공용 motionOptions 구성 값을 확인해주세요.");
			this.publicMotionOptions = result.motionOptions;
		}

		if (!this.severity) this.severity = result.default;

		if (!this.settingObj.width || !this.settingObj.height) {
			throw this.exceptionSymbolInfo("width || height");
		}

		for (var state in result.data) {
			this.spriteList[state] = result.data[state];
		}

		this.excuteAnimate();
	}

	setAnimateInfo(info) {
		return new Promise((resolve, reject) => {

			let path = info.path.replace("static/components/2D/symbol/", "client/packs/2d_pack/components/symbol/");


			if (VNVComplexSensorComponent.stateClipPool.has(path)) {
				this.setSettingObj(VNVComplexSensorComponent.stateClipPool.get(path));
				resolve();
			} else {

				this.procAnimateInfo(path).then((result) => {

					VNVComplexSensorComponent.stateClipPool.set(path, result);
					this.setSettingObj(result);

					resolve();
				});
			}
		});
	}

	/*컴포넌트 로드 완료시점 체크 */
	savedResourceLoaded() {
		if (!this.isResourceLoaded) {
			this.validateResource();
			this.isResourceLoaded = true;
		}
	}

	exceptionSymbolInfo(type) {
		console.log("VNVComplexSensorComponent _ 에러가 발생하였습니다. [" + type + " ]");
	}

	calcScale() {
		if (this.$container && this.settingObj.width != "") {
			var oriWidth = parseInt(this.settingObj.width);
			var scale = 1 / oriWidth * this.width;
			this.$container.css({
				"transform": "scale(" + scale + "," + scale + ")",
				"width": this.settingObj.width,
				"height": this.settingObj.height,
				"background-position": "0 0"
			});
		}
	}

	set severity(value) {
		if (this._checkUpdateGroupPropertyValue("setter", "severity", value)) {
			this._invalidateServerity = true;
		}
	}

	get severity() {
		return this.getGroupPropertyValue("setter", "severity");
	}

	set isSaved(value) {
		if (this._checkUpdateGroupPropertyValue("setter", "isSaved", value)) {
		}
	}

	get isSaved() {
		return this.getGroupPropertyValue("setter", "isSaved");
	}

}

VNVComplexSensorComponent.DEFAULT_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAK0AAACtAB0IQVDAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA0qSURBVGiB7ZprkFzFdcd/3ffeec++l9U+JO1KSOiBJIQsZBsCGDuxIS5TJganKgkggoOxkDDPqsQfsuWKHYJBYITiMjgSckiqDElhQyrGIZjEPIUeXgmh56LHSqt9aXZ33nPn3tudD7Oa2dcMKxD+kPL/y8w5t/v0+Xf39Dl97sD/E4jzbfCJ9VsaPVihlaxTghoAIchpdEpqjiQaThzo7Ox0z/e4H5vI43c/3aQ84zqF+CNpGF/QntdQeUThaSG6hNIvKE+89MCPbtn7cX2Aj0HkkW9tXS0k30aIm9DaFFKilQJAV1fjLlqAHBjEON5D8jOr8fX24TvZi9AaLSVohdCgDXlQeuqHXsD45wceuTn9OyPy2Ld+ulgZ6iG0/gqmiW/BXHwdbWTf2Ys3GsdZuRxv6SIQAnPHbsz9hxi+4csAGKMJIjt2I1NpUosXIDxNoPc0ZiqNNowYrnfPfZtvfVYg9CdG5LkbnzNONqX/BkSnkEL6l19EcM1yRMBP4vmXcfvP4HzuCry21mKfyUQAhOtS9Zu3MeIJhteswqmvw9/XT+RQN2Y6g5byHUuIr9/9w5t7zoWInEmjx+9+uqlnVuYN4LvW3GZZvfYGQp9bgwgFyW7fg3t6EGfNqgkkykGbJsnL16ACfmq69oHrkmuZxZmrP0vy4kUIwacdrQ9uXLftq+eVyGN3bpnnYe0QiE8HL7+U6Ff/EFkVAcAdGia7/T289rl4Cy+c8aDK7yO9eiUyZ1P1/oGCUkjS7XM4c/kavFAwiND//ui6LXeeFyI/uOunHcoytiPk7KobvkDwsuUgSrsx+/pOsEzcNZfOmMRZOA312O1tBHr7sBLJot6tihK7Yg12Q61AiH985K4t93wsIk+u21YvJa8KKRqiX/k85pyWiY6c7MM5cRr34sXoQOCciQBkli4GaRA5eGSCXpsmI6tXkq+vRSA2bly39U8/zJb593f+S+1kZVS60pb6BZTqsBZdiDl71pSOud37we/DW7TgI5EA0H4f+Y45+LuPYSbTuNFw6ZlhkFy8kPq3d6AVzz66YUvPfU/c9lY5W9OeWhvXb31Aax6W0TAqmUZGQgQuXYp/yXxEMIBKZxl9+nncJQtwP1V+W013ak2GTGeo/q9fk+loJ7lkIaDxxUYJHz2Ob3AIpIE2JNJV/VJ6i+95fO3otGNNVjy8/pn5CL5ntTYR/doXyXf3kNveReY3O8i8sQtrbgvCskAr1LyOsg7OFCocwquvI9jbB2iCpweQuRzaMMh0tJOe345vZJSanV2zlObvgLumszNlRR7dsPXnAnl91V9cj1FXXVBqcHr7yR8+jnPkOCqTK6jDYXRtDbq2GlVbjY5G0T4fWD6038Lc1VVcEeE4SMdF5B1E3saIJzASKcx4AplMIjyFFgKnvpZcyyxyzU0oyyr6VfvubvyDMS00y+7dfOv7FYk8tm7LpZ4Qu0KrlxG8YtW0M6gzWUZ+/DNUUyMEgojhEUQqBfqcgzHaZ+FWVaGiYfzHekhf2EGyzG/OyGRpfO0NNDx3/6Zbvz75+YStpYR4UEqJf+WSsoO7/WcA8JYswpvTNqZ0ESNxZDqFsPOQsxF2HtE/gBwZJdcxF+230JYP7bNQgQCqKoIXDBbtWgNDWKPxsuN6oSC55lkET/ff+Mi6nzx4/+bbT0xL5Ml12+ptwZ9YC9qR4eBUS2Nw+oYKpC8Yl+SaJrqxHq+xfkJbc8du5MgomZXLyto7C7euDqtvoJBUiukzp3THbAKn+4TEvAX47vhnxThiS3U9WpmBFRdVHNAbjKEj4Y8cO8rBratBeC5GJlO2jVNbg1sVRRn8+eRnpYCouU74fZjNF1Qc0BtNoquiH8PlMnYjhRhipLMV29kXNCCUXvDYt7e2j9eXiEjxebO1CWSFhFgpVDz1iRIxU5WvJPm6QvxWrr5ivF4CbPzmtlaUrjFbKq+GVhphGqiGypfAIsJhVDg0o6Y6FET5/ehKEwnka2vQQqA1l4zXmwDK0PMFYNRUnmlhGvhvu5FsbmZHrbvkIuw5LTCD5lpKRq69Bsf1KrezTLTPh7Tt+eP1EkAK3QYgggHcviFwSrUBt3cAxq6waI03ECvFDKWQA0PjPHeRQ7GiKPJ5ZLyU2UrbxkgkSnImgzFuK5npDDKXK8mJJDKfL8rWaBzpuiifDy3E7ClEFCICIKQk8bP/JPd+IRv1RhIknvsl+e7CZc3pHcT5+a+Qg4VYYvScwvfyfyOSBWfNI0fx/fIVxNjg5r6DRP7njeJgwX0Hiby9syiHu/YR3tlVknd0Ub3vUFGue3c3kUMfFCZFaere2kHoWA/KZyKEqJlCRGhlwNgO0Bp9dkXGPrU7Jk/+9ArbQIxvpzXaGytCeC5i/FZRHtIrycLzQJWX8byCDtBaI5RCeB7aMECIUv7C2YAoRRYNAo1v0TystkLabtRG8S1ox5zVWJAbapHz5qDPnhwN9ai5c9DRwo1Rz2rC65gLAX/heUsztm0XB3Oam1ChUqqeb20uOgqQn92CMy6/ys5uJV9XmHghJdnZrdiNDfjODINWpT16loiAmAZ0Lk/k2itLTy2LyJevLooyEsL3pavIpMdWoLqK/NWXlya8sR7V+NmS3NpMrrYKxlYo39YKbSXzuXnt430hc9ECHMcpysklpeCsBcRXLC34kc+D1gPj+xbiiOY4gPqQYIQG961dyNFE5XZjEGdi+A91z6gtQOj9Q1jxyraF1kg7D4oJuZYEMEXyMEK4bt/Q9L3HoF0Ht2s/8ujxGTlmHDtB4L39M2orbZvg4W78Q2cqtjOTqcLvROh9E/oDbNi0wUawx+0dmL73GIRlIQL+4ik1E8y01GakCjnW+Ix4Olix4YJdyfbx+lKKotTL3mgClSqftEEhaIpkambenQNkqmDTDVXOBHyxEYSUmUjO2TGh/9kvSvMSgL2/8p42aquR8XgpSJ4nGPEEWpRyrukg83n8g2dQSr14x1N3OBOenf3ywObbtmspjtjvHQZVfj+YLReA6yFjI2XbCMdBpDOIfGEsI50ZO2nKkzdjI3jhCMqaUkYoItjTi1AK0M9M6T9BUnqTSqSecI6fwpo3e3LbEhFAnjoNjoMcGUGOjCKG44hMGvL5KblV9a9+XfyuDQPt9+FVRXCrq1FVUdxoBHM0TnZ2+ZKr0JrQyV60EMdS9SdeqUgkms//Uyrg70y/sauuur0VISfW77zhOPahY4WOe8cdGqaFqq9BNbQVig8+C3w+ZM8pZN8A6ZXLC6vkuEjHQdg5zHgSc+AMYtxd30wkCfQPkmtqnFDRBAidOImRziAQ3+vs7JyytBOI3PHUHZmNd239joqN/sje+R6By1aAUuQPHiXbdRBvYOxotCxwHJzPXIZqvgAdiU5bITOTKWTfAHbHnOln2VPIZJJQ1z7MkVHMRJKanV0oyyLb2kxm/ly8YBCZyxE50A1SHkjUHds2na0pGzLRcOKpSKz9tuz2vasBcu8dQSVSGHU1hK5YhW/BXLzRJMkXCqurox/9kqUNiYpGMBNJ7KZG4pdcTGAwhr+vn9CJk4R6TpJrbcbI5ZGepwV8o9xruym1387OTqWE/jPt6Wzmzd8iLIvIdVdTffP1BFYvQ9ZUYc1pQUbDmIdnHrXLwXeqD+G6ZOe0oU2TbEsTo6tWELv6cuyWZoKn+vENnUEL/fA9T976Zjk70xaxH3xi7RGh1e0aEOEgvnltE/esFPiXLUTEhpH9gx+ZhNAQOPIBXjBIflIFxg2HsOtrC9k0crtF8m8r2Spbjb/3ybX/Cvy123Oa5C9eLaX2YwhcshgR8GN27ZvRDXA6WKf7MBJJUgvnTSkBhU72Ur1nP0h5NKD54w2bNthlzFQmAnD/k2sfEprvOyf7SP7by6hEKaILv4/gpy5GDgxgHDt+ziSE6xLaux8vHCLb1lzSa03kcDdVe/ajBR9oL3/NXZtviVUw9eFEAO7dvPY7CP1Ntz+m4s++SP5IKen0r1qK0ViH9e5uRLpyajMZoT37kNkc8eVLQRTckDmb2rd3ETl8FKR4R5ruZZMrih+ZCMB9m277sYQrtev2pv7jNVIvvopXyHmIXHsVKA/rtdeLV9wPQ6D7GP4Tp0hf2F54meN5hLuP0vC/b+IfHtECHorkclfe+9jtwzMyyDm+nn5i/bNVjnIeRvINENK3aB6BZQvRdp7kS6+ha2uwr/kDGEv8Jr8fERoCh7oJHDiIPauJxNKLCPb2Ez56AmnbaCkPSq3+6t5Na18/F7/OmchZ/ODObcsNw3sI5Jc0WsiqCMKy8GIj6KAfb8UyvPkdGL/dWyRiDI8QOnAYa2AIDEm+phpreBShNULKU9rzvj97MPLUTc/fVLkedD6JnMXD65+Zb2j+Uku+JpSe+D7AMNCmgbDz6EAAMa7MA4CUGZT+hUZtSzX0vDJd2nEuOG9/qvmH9U+3GVhXofQKIbgEqEOKehBSa5UVmjiaI0KyB/S7ifqeNz+JP9f8Hr/Hecb/AQvdsKb7Z4awAAAAAElFTkSuQmCC';

VNVComplexSensorComponent.stateClipPool = new Map();

WVPropertyManager.attach_default_component_infos(VNVComplexSensorComponent, {
	"info": {
		"componentName": "VNVComplexSensorComponent",
		"version": "1.0.0"
	},

	"setter": {
		"width": 40,
		"height": 40,
		"selectItem": "",
		"severity": "normal",
		"isSaved": false
	},

	"critical": {
		"area": 1
	},

	"label": {
		"label_using": "N",
		"label_text": "State Clip"
	}
});

// 기본 프로퍼티 정보입니다.

// 프로퍼티 패널에서 사용할 정보 입니다.
VNVComplexSensorComponent.property_panel_info = [{
	template: "primary"
}, {
	template: "pos-size-2d"
}, {
	template: "label",
}, {
	label: "Status",
	template: "vertical",
	children: [{
		name: "severity",
		type: "select",
		label: "severity",
		owner: "setter",
		show: true,
		writable: true,
		description: "상태 값",
		options: {
			items: [
				{label: "critical", value: "critical"},
				{label: "major", value: "major"},
				{label: "minor", value: "minor"},
				{label: "warning", value: "warning"},
				{label: "normal", value: "normal"}
			]
		}
	}]
}, {
	label: "Critical Area",
	template: "vertical",
	children: [{
		name: "area",
		type: "number",
		label: "Area Value",
		owner: "critical",
		show: true,
		writable: true,
		description: "area value",
	}]
}];

// 이벤트 정보
WVPropertyManager.add_event(VNVComplexSensorComponent, {
	name: "change",
	label: "값 체인지 이벤트",
	description: "값 체인지 이벤트 입니다.",
	properties: [{
		name: "value",
		type: "string",
		default: "",
		description: "새로운 값입니다."
	}]
});

WVPropertyManager.add_event(VNVComplexSensorComponent, {
	name: "enter",
	label: "엔터키 이벤트",
	description: "엔터키 이벤트 입니다.",
	properties: [{
		name: "value",
		type: "string",
		default: "",
		description: "새로운 값입니다."
	}]
});

WVPropertyManager.add_property_group_info(VNVComplexSensorComponent, {
	label: "VNVComplexSensorComponent 고유 속성",
	children: [{
		name: "severity",
		type: "string",
		show: true,
		writable: true,
		defaultValue: "'normal'",
		description: "설정한 severity 정보 입니다. \nex) this.severity = 'critical' | 'major' | 'minor' | 'warning' | 'normal'; "
	}]
});
