"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ContourComponent =
      /*#__PURE__*/
      function (_NWV3DComponent) {
            _inherits(ContourComponent, _NWV3DComponent);

            function ContourComponent() {
                  var _this;

                  _classCallCheck(this, ContourComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(ContourComponent).call(this));
                  _this.grayTexture;
                  _this.colorTexture;
                  _this.element;
                  _this.dataProvider = [];
                  _this.mapGenerator;
                  _this._useBackGround = true;
                  _this.divisor = 1;
                  _this.space;
                  _this.type = "";
                  _this.contourInfo = {
                        temperature: {
                              colorGradient: {
                                    0.2: '#00FFFF',
                                    0.4: '#0099FF',
                                    0.6: '#00FF00',
                                    0.8: '#ffff00',
                                    1.0: '#ff0000'
                              },
                              grayGradient: {
                                    0.2: '#333333',
                                    0.4: '#666666',
                                    0.6: '#999999',
                                    0.8: '#CCCCCC',
                                    1.0: '#ffffff'
                              }
                        },
                        background: "black",
                        humidity: {
                              colorGradient: {
                                    0.2: '#58b1cb',
                                    0.4: '#02D5F9',
                                    0.6: '#067acb',
                                    0.8: '#0566cb',
                                    1.0: '#004AFA'
                              },
                              grayGradient: {
                                    0.2: '#333333',
                                    0.4: '#666666',
                                    0.6: '#999999',
                                    0.8: '#CCCCCC',
                                    1.0: '#ffffff'
                              }
                        }
                  };
                  _this.tempSections = [0, 100];
                  _this.humiSections = [0, 100];
                  _this.displacementScale = 50;
                  _this.ContourMapGenerator = null;
                  _this.radius;
                  return _this;
            }

            _createClass(ContourComponent, [{
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(ContourComponent.prototype), "_onCreateProperties", this).call(this); //프로퍼티 삭제 -> 지금 propertypanel 에 null을 넣어서 프레임을 키웠으므로 패널에 티가 나지 않는다


                        WVPropertyManager.removePropertyGroupChildrenByName(ContourComponent.property_info, "display", "size");
                        WVPropertyManager.removePropertyGroupChildrenByName(ContourComponent.property_info, "normal", "color");
                        this._rectWidth = this.getBestPowerOf2(this.getGroupPropertyValue("rect", "rect_width"));
                        this._rectHeight = this.getBestPowerOf2(this.getGroupPropertyValue("rect", "rect_height"));
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        _get(_getPrototypeOf(ContourComponent.prototype), "_onCreateElement", this).call(this);

                        this.createContourMap();

                        this._makeInnerPoints();

                        this._makeCurve();
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        _get(_getPrototypeOf(ContourComponent.prototype), "_onCommitProperties", this).call(this);

                        if (this._updatePropertiesMap.has("rect.rect_width") || this._updatePropertiesMap.has("rect.rect_height")) {
                              this._rectWidth = this.getBestPowerOf2(this.getGroupPropertyValue("rect", "rect_width"));
                              this._rectHeight = this.getBestPowerOf2(this.getGroupPropertyValue("rect", "rect_height"));

                              this._makeInnerPoints();

                              this._curve.points = this._innerPoints;

                              this._curve.mesh.geometry.setFromPoints(this._curve.points);

                              this._curve.mesh.geometry.attributes.position.needsUpdate = true;
                              var minSide = Math.min(this._rectWidth, this._rectHeight);
                              var radius = minSide / this.divisor / 2;
                              var blur = radius * 1.4;

                              this._element.geometry.dispose();

                              this._outlineElement = null;
                              this._element.geometry = new THREE.PlaneBufferGeometry(this._rectWidth * 2, this._rectHeight * 2, 128, 128);

                              this._validateOutline();

                              this.ContourMapGenerator.colorLayer.width = this._rectWidth;
                              this.ContourMapGenerator.colorLayer.height = this._rectHeight;
                              this.ContourMapGenerator.grayLayer = this.cloneCanvas(this.colorLayer);
                              this.ContourMapGenerator.clear();
                        }
                  }
            }, {
                  key: "createContourMap",
                  value: function createContourMap() {
                        var minSide = Math.min(this._rectWidth * 2, this._rectHeight * 2);
                        var radius = minSide / this.divisor / 2;
                        var blur = radius * 1.4;
                        if (this.ContourMapGenerator !== null) this.ContourMapGenerator.destroy();
                        this.ContourMapGenerator = new MapGenerator({
                              width: this._rectWidth * 2,
                              height: this._rectHeight * 2,
                              radius: radius,
                              blur: blur
                        });
                        this.changeType('temperature');
                        var geometry = new THREE.PlaneGeometry(this._rectWidth * 2, this._rectHeight * 2, 128, 128);
                        this.colorTexture = new THREE.Texture(this.ContourMapGenerator.colorLayer);
                        this.colorTexture.needsUpdate = true;
                        this.grayTexture = new THREE.Texture(this.ContourMapGenerator.grayLayer);
                        this.grayTexture.needsUpdate = true;
                        var material = new THREE.MeshPhongMaterial();
                        material.side = THREE.DoubleSide;
                        material.map = this.colorTexture;
                        material.normapMap = this.colorTexture;
                        material.displacementMap = this.grayTexture;
                        material.normalMapScale = new THREE.Vector2(1, -1);
                        material.displacementScale = this.displacementScale;
                        material.displacementBias = -0.5;
                        material.transparent = true; // material.wireframe            = true;

                        this._element = new THREE.Mesh(geometry, material);
                        this._element.rotation.x = -Math.PI / 2;
                        this.parentBoundingBox = new THREE.Box3().setFromObject(this._element.clone());
                        var pos = this.getGroupPropertyValue("setter", "position");
                        var v = new THREE.Vector3(parseInt(pos.x), parseInt(pos.y), parseInt(pos.z));
                        this.parentBoundingBox.min.add(v);
                        this.parentBoundingBox.max.add(v);
                        this._element.rotation.x = -0;
                        this.appendElement.add(this._element);
                  }
            }, {
                  key: "setDataProvider",
                  value: function setDataProvider(objProvider) {
                        if (this.dataProvider != objProvider) {
                              this.dataProvider = objProvider;
                              this.update();
                        }
                  }
            }, {
                  key: "getBestPowerOf2",
                  value: function getBestPowerOf2(value) {
                        var MAX_SIZE = 4096;
                        var p = 1;

                        while (p < value) {
                              p <<= 1;
                        }

                        if (p > MAX_SIZE) p = MAX_SIZE;
                        return p;
                  }
            }, {
                  key: "_makeInnerPoints",
                  value: function _makeInnerPoints() {
                        this._innerPoints = [];
                        this._innerPoints[0] = new THREE.Vector3(-1 * this._rectWidth, -1 * this._rectHeight, 0);
                        this._innerPoints[1] = new THREE.Vector3(-1 * this._rectWidth, this._rectHeight, 0);
                        this._innerPoints[2] = new THREE.Vector3(this._rectWidth, this._rectHeight, 0);
                        this._innerPoints[3] = new THREE.Vector3(this._rectWidth, -1 * this._rectHeight, 0);
                        this._innerPoints[4] = new THREE.Vector3(-1 * this._rectWidth, -1 * this._rectHeight, 0);
                  }
            }, {
                  key: "_makeCurve",
                  value: function _makeCurve() {
                        this._curve = new THREE.CatmullRomCurve3(this._innerPoints);
                        this._curve.tension = 0;
                        this._curve.curveType = 'catmullrom';
                        var curveGeometry = new THREE.BufferGeometry();
                        curveGeometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this._curveSegments * 3), 3));
                        curveGeometry.setFromPoints(this._curve.getPoints(100));
                        var curveMaterial = new THREE.LineBasicMaterial({
                              color: 0xff0000
                        });
                        var curveMesh = new THREE.Line(curveGeometry, curveMaterial); //new THREE.Line(curveGeometry, curveMaterial, THREE.LinePieces);

                        this._curve.mesh = curveMesh;
                        this.appendElement.add(this._curve.mesh);
                  }
            }, {
                  key: "changeType",
                  value: function changeType(type) {
                        if (this.type != type) {
                              this.type = type;
                              var colors = this.contourInfo[type].colorGradient;
                              var grays = this.contourInfo[type].grayGradient;
                              this.ContourMapGenerator.setGradient(colors, grays, this.useBackGround);
                        }
                  }
            }, {
                  key: "clear",
                  value: function clear() {
                        this.ContourMapGenerator.clear();
                        this.ContourMapGenerator.draw();
                        this.colorTexture.needsUpdate = true;
                        this.grayTexture.needsUpdate = true;
                  }
            }, {
                  key: "calculate",
                  value: function calculate(min, max, value) {
                        return (value - min) / (max - min);
                  }
            }, {
                  key: "paint",
                  value: function paint(info) {
                        var isTemp = info.type == "temperature";
                        var min = isTemp ? this.tempSections[0] : this.humiSections[0];
                        var max = isTemp ? this.tempSections[1] : this.humiSections[1];
                        var value = isTemp ? info.temp : info.humi;
                        var force = this.calculate(min, max, value);
                        var spacePosition = this.parentBoundingBox.getParameter(info.object.appendElement.position);
                        var px = spacePosition.x * this.colorTexture.image.width - this._element.position.x;
                        var py = spacePosition.z * this.colorTexture.image.height - this._element.position.z;
                        this.ContourMapGenerator.add(px, py, force);
                  }
            }, {
                  key: "update",
                  value: function update() {
                        this.clear();
                        var max = this.dataProvider.length;
                        var sensorVO, idx;

                        for (idx = 0; idx < max; idx++) {
                              sensorVO = this.dataProvider[idx];
                              sensorVO.type = this.type;
                              this.paint(sensorVO);
                        }

                        this.ContourMapGenerator.draw();
                        this.colorTexture.needsUpdate = true;
                        this.grayTexture.needsUpdate = true;
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        _get(_getPrototypeOf(ContourComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "useBackGround",
                  set: function set(value) {
                        if (this._useBackGround != value) {
                              this._useBackGround = value;
                        }
                  },
                  get: function get() {
                        return this._useBackGround;
                  }
            }]);

            return ContourComponent;
      }(NWV3DComponent);

var MapGenerator =
      /*#__PURE__*/
      function () {
            function MapGenerator(opt) {
                  _classCallCheck(this, MapGenerator);

                  this.blur = opt.blur;
                  this.radius = opt.radius;
                  this.colorLayer = document.createElement("canvas");
                  this.colorLayer.width = opt.width;
                  this.colorLayer.height = opt.height;
                  this.grayLayer = this.cloneCanvas(this.colorLayer);
                  this.heatmap = simpleheat(this.colorLayer);
                  this.graymap = simpleheat(this.grayLayer);
                  this.heatmap.radius(opt.radius, opt.blur).max(1);
                  this.graymap.radius(opt.radius, opt.blur).max(1);
            }

            _createClass(MapGenerator, [{
                  key: "setGradient",
                  value: function setGradient(colorGradient, grayGradient, useBackGround) {
                        this.heatmap.gradient(colorGradient);
                        this.graymap.gradient(grayGradient); // 배경 사용 시 가장 낮은 컬러값을 적용

                        if (useBackGround) {
                              var prop = Object.keys(colorGradient).sort();
                              this.heatmap.bgColor(colorGradient[prop[0]]);
                              this.graymap.bgColor(grayGradient[prop[0]]);
                        }
                  }
            }, {
                  key: "cloneCanvas",
                  value: function cloneCanvas(origin) {
                        var clone = document.createElement("canvas");
                        clone.width = origin.width;
                        clone.height = origin.height;
                        var context = clone.getContext("2d");
                        context.drawImage(origin, 0, 0);
                        return clone;
                  }
            }, {
                  key: "add",
                  value: function add(x, y, value) {
                        this.heatmap.add([x, y, value]);
                        this.graymap.add([x, y, value]);
                  }
            }, {
                  key: "clear",
                  value: function clear() {
                        this.heatmap.clear();
                        this.graymap.clear();
                  }
            }, {
                  key: "draw",
                  value: function draw() {
                        this.heatmap.draw();
                        this.graymap.draw();
                  }
            }, {
                  key: "destroy",
                  value: function destroy() {
                        this.heatmap = null;
                        this.graymap = null;
                        this.colorLayer = null;
                        this.grayLayer = null;
                  }
            }, {
                  key: "changeColorLayer",
                  value: function changeColorLayer(opt) {
                        this.clear();
                        this.colorLayer.width = opt.width;
                        this.colorLayer.height = opt.height;
                        this.grayLayer = this.cloneCanvas(this.colorLayer);
                        this.heatmap = simpleheat(this.colorLayer);
                        this.graymap = simpleheat(this.grayLayer);
                        this.heatmap.radius(opt.radius, opt.blur).max(1);
                        this.graymap.radius(opt.radius, opt.blur).max(1);
                  }
            }]);

            return MapGenerator;
      }();

WV3DPropertyManager.attach_default_component_infos(ContourComponent, {
      "setter": {
            "size": {
                  x: 10,
                  y: 10,
                  z: 10
            },
            "rotation": {
                  x: -90,
                  y: 0,
                  z: 0
            }
      },
      "label": {
            "label_text": "ContourComponent",
            "label_line_size": 15,
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "ContourComponent",
            "version": "1.0.0"
      },
      "rect": {
            "rect_width": 30,
            "rect_height": 30
      }
});
WV3DPropertyManager.add_property_panel_group_info(ContourComponent, {
      label: "Shape Properties",
      template: "vertical",
      children: [{
            owner: "rect",
            name: "rect_width",
            type: "number",
            label: "rect_width",
            show: true,
            writable: true,
            description: "가로 길이"
      }, {
            owner: "rect",
            name: "rect_height",
            type: "number",
            label: "rect_height",
            show: true,
            writable: true,
            description: "세로 길이"
      }]
});
