class CJDockStatusComponent extends WVDOMComponent {
    constructor() {
        super();

        this.$containier = null;
        this._invalidateProperty = false;
    }

    _onCreateElement() {
        let containier = document.createElement("div");
        containier.classList.add('dock-status-component');
        this.$containier = $(containier);
        this._element.appendChild(containier);
    }

    _onDestroy() {
        this.$containier.remove();
        this.$containier = null;
        super._onDestroy();
    }

    _onImmediateUpdateDisplay() {
        this.drawDockContainer();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("extension")) {
            this.validateCallLater(this.drawDockContainer);
        }

        if (this._invalidateProperty) {
            this.validateCallLater(this.changeSize);
            this._invalidateProperty = false;
        }
    }

    drawDockContainer() {
        this.$containier.empty();

        let prefix = this.prefix;
        let start_num = this.start_num;
        let end_num = this.end_num;
        let except_num = this.except_num;
        let isDescending = start_num > end_num;

        let exceptList = except_num.split(",");

        isDescending ? start_num++ : start_num--;

        while (start_num != end_num) {
            isDescending ? start_num-- : start_num++;

            let status = true;
            exceptList.map(function(value) {
                if (value == start_num) status = false;
            });

            let dock_id = start_num + "";

            if (start_num < 10) dock_id = "0" + start_num;

            if (status) this.initDrawDock({ id: prefix + dock_id });
        }

        this.changeSize();
    }

    initDrawDock(data) {
        let dockContainer = document.createElement("div");

        let htmlStr = "<div class='clip' id=" + data.id + ">" +
            "     <div class='gray-box normal-dock'></div>" +
            "     <div class='mask'>" +
            "           <div class='dock'></div>" +
            "     </div>" +
            "     <div class='dock-status'></div>" +
            "     <span class='dock-label'>" + data.id + "</span>" +
            "</div>" +
            "<div class='standby-dotline' style='opacity: 0'></div>" +
            "<div class='dock-standby' style='opacity: 0'></div>";

        dockContainer.classList.add('dock-container');
        dockContainer.innerHTML = htmlStr;

        this.$containier.append(dockContainer);
    }

    setEventStatus(ary) {
        /* 전체 초기화 */
        this.$containier.find(".dock").css("top", "93px").parent().css("top", "-93px");

        this.$containier.find(".dock-status").attr("class", "dock-status")
            .parent().find(".dock-label").attr("class", "dock-label")
            .parent().find(".gray-box").attr("class","gray-box normal-dock");

        this.$containier.find(".dock-standby, .standby-dotline").css("opacity", 0);

        for (var i = 0; i < ary.length; i++) {
            var data = ary[i];
            var clip = this.$containier.find("#" + data.id);
            var dock = $(clip).find(".dock")[0];

            /* 도크 사용여부 */
            if (data.use_yn == 'N') {
                $(clip).find(".dock-label").addClass("use-yn-label");
            }

            /* 대기차량 설정 */
            var standby = $(clip).parent().find(".standby-dotline");

            if (data.wait_cnt && data.wait_cnt > 0) {
                $(standby).css("opacity", 1).next().text(data.wait_cnt).css("opacity", 1);
            } else {
                $(standby).css("opacity", 0).next().css("opacity", 0)
            }

            /* status 가 'info' 인 경우 진척률 반영 X */
            if (data.status == 'info') continue;

            /* 도크 이벤트&진척률 반영 */
            var value = 100 - data.dock_ratio;

            $(clip).find(".gray-box").attr("class", "gray-box " +data.status)
                .parent().find(".dock-status").attr("class", "dock-status " + data.status)
                .next().addClass("event-label");

            $(dock).attr("class", "dock " + data.status).css("top", value + "%").parent().css("top", -value + "%");
        }

        if (!this.isEditorMode) {
            this.dispatchWScriptEvent("change", {
                value: ary
            })
        }

        this.changeSize();
    }

    changeSize() {
        this.$containier.find("div").removeClass("small");
        this.$containier.find("span").removeClass("small");

        if (this.size == "small") {
            this.$containier.find("div").addClass(this.size);
            this.$containier.find("span").addClass(this.size);
        }
    }

    get start_num() {
        return this.getGroupPropertyValue("extension", "start_num");
    }

    get end_num() {
        return this.getGroupPropertyValue("extension", "end_num");
    }

    get except_num() {
        return this.getGroupPropertyValue("extension", "except_num");
    }

    get prefix() {
        return this.getGroupPropertyValue("extension", "prefix");
    }

    set size(value) {
        if (this._checkUpdateGroupPropertyValue("extension", "size", value)) {
            this._invalidateProperty = true;
        }
        this.changeSize(value);
    }

    get size() {
        return this.getGroupPropertyValue("extension", "size");
    }

    get dataProvider() {
        return this.getGroupPropertyValue("setter", "dataProvider");
    }

    set dataProvider(value) {
        this.setEventStatus(value);
    }

}

// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(CJDockStatusComponent, {
    "info": {
        "componentName": "CJDockStatusComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 1150,
        "height": 120,
        "dataProvider": []
    },

    "label": {
        "label_using": "N",
        "label_text": "CJ Dock Status"
    },

    "extension": {
        "size": "small",
        "start_num": 22,
        "end_num": 60,
        "except_num": "51,39,27",
        "prefix": "B"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
CJDockStatusComponent.property_panel_info = [{
        template: "primary"
    },
    {
        template: "pos-size-2d"
    },
    {
        template: "label"
    }, {
        label: "Option",
        template: "vertical",
        children: [{
            owner: "extension",
            name: "start_num",
            type: "string",
            label: "start_num",
            show: true,
            writable: true,
            description: "start_num"
        }, {
            owner: "extension",
            name: "end_num",
            type: "string",
            label: "end_num",
            show: true,
            writable: true,
            description: "end_num"
        }, {
            owner: "extension",
            name: "except_num",
            type: "string",
            label: "except_num",
            show: true,
            writable: true,
            description: "except_num"
        }, {
            owner: "extension",
            name: "prefix",
            type: "string",
            label: "prefix",
            show: true,
            writable: true,
            description: "prefix"
        }, {
            owner: "extension",
            name: "size",
            label: "size",
            type: "select",
            options: {
                items: [
                    { label: "small", value: "small" },
                    { label: "medium", value: "medium" }
                ]
            }
        }]
    }
];


//  추후 추가 예정
CJDockStatusComponent.method_info = [];


// 이벤트 정보
WVPropertyManager.add_event(CJDockStatusComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});
