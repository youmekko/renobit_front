class CJDMSSingleCalendarComponent extends WVDOMComponent {

    constructor() {
        super();
        this._oldDate = "";
        this.$datepicker = null;
    }

    _onCreateProperties() {}

    //element 생성
    _onCreateElement() {

        $(this._element).append('<div class="cj-dms-datepicker-field"><input type="text" class="cj-dms-single-picker" readonly size="30"></div>');
        this.createCalender();

    }

    createCalender() {
        $(this._element).addClass("cj-dms-datepicker");
        this.$datepicker = $(this._element).find(".cj-dms-single-picker").datepicker({
            beforeShow: function() { $("#ui-datepicker-div").addClass("ll-skin-lugo") },
            showOn: "button",
            buttonImage: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OUUxMDUwRTBCMzVEMTFFODgxMjZCN0I3NzVFNTcwM0IiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OUUxMDUwREZCMzVEMTFFODgxMjZCN0I3NzVFNTcwM0IiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBN0MyNDJBMEE5RDcxMUU4ODExRUNDRDY2NDUwQkE5RiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBN0MyNDJBMUE5RDcxMUU4ODExRUNDRDY2NDUwQkE5RiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjpAcw8AAAFvSURBVHjarJPPK0RRGIbvTOO3himMhYgNilKztVK2k1j4G2xGSmJBwk6KDZZs2EzKkixJSkZJsRIjNMlGaSwYz9F7dLvNnYy89TTnfvN+53znnO8EupbTjtQKl5CDTrhz8qsDzuDT+C5Gm74nCLoM1VABlVDj+KtOviqotcEQTEAUNjwJqzDiia3BljvQvXK/xM+jqWhOK/xVJnfBVFQGDZCBQ+39yV22SxG4ke/DVKLcspAMcUjCgL77YSjPRIOw6/HF7RkZlcD2L7ZR6ucLOv+kkGs8A8dF5vfBlLeiTTiAMMTgWYfaI44Ui8ljvOv5KrIag17dmumbRcX3YBgmtcCO39asjCkLKXiFfcVNS5yrklNvUoC3ltO4pcD78lMz3PrdmumnF0i4GjWjpIT+Sxa6NauoOrgRyqFe8bBiEXl8J7Ljca1+Ag/qZqMr3VLKbkcL/ZxRVu/tGtJFnlGbeDdVzMI0tIti9QbzXwIMADIaTId0Txb/AAAAAElFTkSuQmCC",
            buttonImageOnly: true,
            showMonthAfterYear: true,
            yearSuffix: "년",
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
            dayNamesShort: ["일", "월", "화", "수", "목", "금", "토"],
            dateFormat: "yy/mm/dd (D)"
        }).on("change", function() {
            this.changedDate();
        }.bind(this));
    }

    _onDestroy() {
        ///this.$datepicker.datepicker("destroy"); core destroy처리 문제로 오류 발생하여 삭제 
        this.$datepicker = null;
        super._onDestroy();
    }

    changedDate() {
        if (this._oldDate == this.date) { return; }
        this._oldDate = this.date;
        this.dispatchWScriptEvent("changeDate", {
            date: this.date
        });
    }

    get datepicker() {
        return this.$datepicker;
    }

    set date(date) {
        this.$datepicker.datepicker("setDate", date);
        this.changedDate();
    }

    get date() {
        return this.$datepicker.datepicker("getDate");
    }
}

WVPropertyManager.attach_default_component_infos(CJDMSSingleCalendarComponent, {
    "info": {
        "componentName": "CJDMSSingleCalendarComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 181,
        "height": 25,
        "date": ""
    },
    "label": {
        "label_using": "N",
        "label_text": "CJDMSSingleCalendarComponent Component"
    }
});

WVPropertyManager.add_event(CJDMSSingleCalendarComponent, {
    name: "changeDate",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "date",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});