/**
 * promise 부분을 await으로 변경
 *
 *
 *
 * @description 하차 정보를 나타내는 컴포넌트
 * @author Kangeunji
 * @version 2018.3.26
 */
class CjTerminalB2f_ROOMComponent extends WV3DComponent {

      constructor() {
            super();
            this._isResourceComponent =  true;
      }

      _onCreateProperties(){
            this.syncAuto = true;

            this.dbclickCallback = this._onDblclick.bind(this);

            /* [ 상태 컬러 정보를 담아두기 위한 풀 ] */
            this.truck11ton_textureList = new Map();

            this.truckTrailer_textureList = new Map();

            /* [ 트럭 - 1 ] */
            this.truck11ton = null;

            /* [ 트럭 - 2 ] */
            this.truckTrailer = null;

            /* [ WorkView에서 필요한 dockList ] */
            this.workViewDockList = new Map();

            /* [ 아래값을 가지고 구성 하기때문에 데이터 포맷 변경시 필히 키값 변경해야함 ] */

            this.dock_id = "dock_id"; // dock_id 키

            /**
             *  @description 상태 키 정보
             *  || status  || -- 상태값 가진 컬럼 키
             *
             *  ||    3    ||  -- 상태값이 minor 일때
             *  ||    2    ||  -- 상태값이 major 일때
             */
            this.state_info = {
                  "key": "status",
                  5: "critical",
                  4: "warning",
                  3: "minor",
                  2: "major",
                  1: "normal",
                  6: "info"
            };

            /**
             *  @description 트럭 타입 정보
             *  || tr_yn || -- 트럭값을 가진 컬럼 키
             *
             *  ||   Y   ||  -- 11ton 트럭일때
             *  ||   N   ||  -- trailer 트럭일때
             */
            this.truck_info = {
                  "key": "tr_yn",
                  "11ton": "Y",
                  "trailer": "N"
            };

            /**
             *  @description 트럭 노출 여부 정보
             *  || model_yn || -- 트럭 노출 여부 값을 가진 컬럼 키
             *
             *  ||     Y    ||  -- 트럭이 노출될때
             *  ||     N    ||  -- 트럭이 노출되지 않을때
             */
            this.model_info = {
                  "key": "model_yn",
                  "visible": "Y",
                  "invisible": "N"
            };
      }

      /**
       * 컴포넌트 생성
       */
      _onCreateElement() {
            this._element.material.visible = false;
            this._elementSize = new THREE.Box3().setFromObject(this._element).getSize();
      }

      /**
       * @description 데이터셋 데이터를 주입 받아 처리하는 함수
       * @param {object} data 데이터셋으로 주입받는 데이터
       */
      syncDockProperties(data) {

            if (this._element) {
                  let truck_key = this.truck_info["key"];
                  let model_key = this.model_info["key"];
                  let state_key = this.state_info["key"];
                  let dockSize = data.length;

                  for (let i = 0; i < dockSize; i++) {

                        let dock = this._element.children[0].getObjectByName(data[i].dock_id);

                        if (dock == undefined) continue;

                        if (this.workViewDockList.get(dock.name)) this.workViewDockList.get(dock.name).mesh.dockInfo = JSON.parse(JSON.stringify(data[i]));

                        /*[ 초기 로드 될 경우 ]*/
                        if (!dock.dockInfo) {

                              dock.dockInfo = JSON.parse(JSON.stringify(data[i]));
                              if (data[i][model_key] == this.model_info["visible"]) {

                                    this.setInit(dock, data[i]);

                              };

                              continue;

                        }

                        /*[ 노출 여부가 바뀔 경우 ]*/
                        if (dock.dockInfo[model_key] != data[i][model_key]) {

                              this.setVisible(dock, data[i]);

                              continue;

                        }

                        /*[ 트럭 종류가 바뀔 경우 ]*/
                        if (dock.dockInfo[truck_key] != data[i][truck_key]) {

                              if (data[i][model_key] == this.model_info["visible"]) this.setTruck(dock, data[i]);

                              continue;

                        }

                        /*[ 상태값이 바뀔 경우 ]*/
                        if (dock.dockInfo[state_key] != data[i][state_key]) {

                              if (data[i][model_key] == this.model_info["visible"]) {

                                    this.setState(dock, data[i])

                              }
                        }

                  }

            }
      }

      setInit(dock, info) {

            let truck_key = this.truck_info["key"];

            this._domEvents.addEventListener(dock, 'dblclick', this.dbclickCallback, false);

            dock.getObjectByName(info[truck_key]).material.visible = true;

            this.setState(dock, info);
      }

      setVisible(dock, info) {

            let model_key = this.model_info["key"];

            let state_key = this.state_info["key"];

            let truck_key = this.truck_info["key"];

            let targetTruck = dock.getObjectByName(info[truck_key]);

            if (dock._3xDomEvent && dock._3xDomEvent.dblclickHandlers.length > 0) {

                  this._domEvents.removeEventListener(dock, 'dblclick', this.dbclickCallback, false);

            }

            /*[ invisible 일 경우 ]*/
            if (info[model_key] == this.model_info["invisible"]) {

                  if (dock.children.length > 0) {

                        for (var i = 0; i < dock.children.length; i++) {

                              dock.children[i].material.visible = false;

                        }

                  }

                  dock.dockInfo[model_key] = info[model_key];

                  dock.dockInfo[truck_key] = info[truck_key];


                  /*[ visible 일 경우 ]*/
            } else if (info[model_key] == this.model_info["visible"]) {

                  for (var i = 0; i < dock.children.length; i++) {

                        dock.children[i].material.visible = false;

                  }

                  targetTruck.material.visible = true;

                  dock.dockInfo[model_key] = info[model_key];

                  if (dock.dockInfo[truck_key] != info[truck_key]) {

                        dock.dockInfo[truck_key] = info[truck_key];

                        this.setTruck(dock, info);

                  } else {

                        this.setState(dock, info);

                  }

            }

      }

      setTruck(dock, info) {

            let truck_key = this.truck_info["key"];

            let targetTruck = dock.getObjectByName(info[truck_key]);

            for (var i = 0; i < dock.children.length; i++) {

                  dock.children[i].material.visible = false;

            }

            targetTruck.material.visible = true;

            dock.dockInfo[truck_key] = info[truck_key];

            this.setState(dock, info);

      }

      setState(dock, info) {

            let truck_key = this.truck_info["key"];

            let state_key = this.state_info["key"];

            let targetTruck = dock.getObjectByName(info[truck_key]);

            dock.dockInfo[state_key] = info[state_key];

            if (targetTruck.material.map) {

                  targetTruck.material.map.dispose();
                  targetTruck.material.map = null;
                  targetTruck.material.dispose();

                  wemb.threeElements.renderer.dispose();
            }

            let texture = null;

            let stateFlag = info[state_key];

            if (dock.dockInfo[truck_key] == this.truck_info["trailer"]) {

                  texture = this.truckTrailer_textureList.get(this.state_info[stateFlag]);

            } else if (dock.dockInfo[truck_key] == this.truck_info["11ton"]) {

                  texture = this.truck11ton_textureList.get(this.state_info[stateFlag]);

            }

            targetTruck.material.map = texture;
            targetTruck.material.needsUpdate = true;

      }
      _onDblclick(e) {

            if (!this.isEditorMode) {
                  this.dispatchWScriptEvent("change", {
                        value: JSON.stringify(e.target.dockInfo)
                  });
            }
      }

      async startLoadResource(){

            try {
                  let success = await this.validateResource();
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);

            }catch(error){
                  console.log("error ", error);
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }

      }

      async validateResource() {


            var that = this;

            /* [ 룸 로드 정보 주입 ] */
            var room_obj = {

                  compName: "CjTerminalB2f_customROOM",
                  objPath: "/output/resource/obj3d/cjTerminalB2f_customROOM/cjTerminalB2f_customROOM.obj",
                  mapPath: "/output/resource/obj3d/cjTerminalB2f_customROOM/maps",
                  recycleYn: false,
                  loaderFunc: function(info) {
                        return new Promise((resolve, reject) => {

                              LoaderManager.objLoader.load(wemb.configManager.serverUrl + info.objPath, object => {

                                    let oSize = object.children.length;

                                    for (let i = 0; i < oSize; i++) {

                                          let meshChild = object.children[i];

                                          let name = meshChild["name"];

                                          if (name == "b2fWall" || name == "b2fFloorDown" || name == "b2fFloorUp" || name == "logo_A" || name == "window_A"  || name.indexOf("pillar") != -1) {

                                                if (meshChild instanceof THREE.Mesh) {

                                                      meshChild["material"] = new THREE.MeshBasicMaterial();
                                                      meshChild["material"]["side"] = THREE.DoubleSide;
                                                      meshChild["material"]["map"] = LoaderManager.textureLoader.load(wemb.configManager.serverUrl + info.mapPath + "/" + name + ".png");

                                                      /* 투명 처리  */
                                                      if (name.indexOf("_A") != -1) {

                                                            meshChild["material"].transparent = true;

                                                      }

                                                }

                                          } else {

                                                /* [ 트럭 BOX 투명처리 ] */
                                                meshChild["material"].transparent = true;
                                                meshChild["material"].opacity = 0;
                                                meshChild["material"].visible = false;

                                                meshChild.geometry.computeBoundingBox();

                                                meshChild.geometry.computeBoundingSphere();

                                                meshChild.position.set(meshChild.geometry.boundingSphere.center.x, meshChild.geometry.boundingSphere.center.y, meshChild.geometry.boundingSphere.center.z);

                                                meshChild.geometry.center();


                                                if (name.indexOf("F") != -1 || name.indexOf("N") != -1) {

                                                      meshChild.rotation.y = Math.PI;
                                                }

                                                if (name.indexOf("N") != -1 || name.indexOf("S") != -1) {

                                                      that.workViewDockList.set(name, {
                                                            name: name,
                                                            pos: meshChild.position,
                                                            mesh: meshChild
                                                      });
                                                }

                                                let truck_1 = LoaderManager.clone(new THREE.Mesh(), that.truck11ton);

                                                let truck_2 = LoaderManager.clone(new THREE.Mesh(), that.truckTrailer);

                                                truck_2.position.z += 15;

                                                truck_2.position.y += 2;

                                                meshChild.add(truck_1);

                                                meshChild.add(truck_2)

                                          }


                                    }

                                    resolve(object);

                              });

                        });

                  }

            };


            /**
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             [ 트럭 2종 로드 정보 주입 ]
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             * */
            let truck11ton_Obj = {
                  compName: "cjTruck11ton_customET",
                  objPath: "/output/resource/obj3d/cjTruck11ton_customET/cjTruck11ton_customET.obj",
                  recycleYn: false,
            };


            /* [ 트럭 2종 로드 ] */
            let  result = await LoaderManager.setLoaderObj(truck11ton_Obj);
            this.truck11ton = result.MESH.children[0];
            this.truck11ton.name = "Y";
            this.truck11ton["material"].transparent = true;
            this.truck11ton["material"].visible = false;





            let truckTrailer_Obj = {
                  compName: "cjTrailer_customET",
                  objPath: "/output/resource/obj3d/cjTrailer_customET/cjTrailer_customET.obj",
                  recycleYn: false,
            };

            result = await LoaderManager.setLoaderObj(truckTrailer_Obj);
            this.truckTrailer = result.MESH.children[0];
            this.truckTrailer.name = "N";
            this.truckTrailer["material"].transparent = true;
            this.truckTrailer["material"].visible = false;





            /**
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             [ 트럭 텍스쳐 로딩 ]
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             * */
            var truck11tonLoader =  new LoaderCjTruck11ton();
            let success = await truck11tonLoader.loadStart();

            if(success==false)
                  return false;

            this.truck11ton_textureList = truck11tonLoader.truck11ton_textureList;

            var loaderCjTrailer = new LoaderCjTrailer();
            success= await loaderCjTrailer.loadStart();
            if(success==false)
                  return false;

            this.truckTrailer_textureList = loaderCjTrailer.textureList;




            /**
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             [ 룸 로딩 ]
             □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
             * */
            result = await LoaderManager.setLoaderObj(room_obj);

            this._elementSize = result.SIZE;

            this._element.add(result.MESH);

            this.size = this._elementSize;

            return true;

      }



      _onDestroy() {

            this.dbclickCallback = null;

            this.workViewDockList = null;

            MeshManager.disposeMesh(this.truck11ton);

            MeshManager.disposeMesh(this.truckTrailer);

            this.truck11ton = null;

            this.truckTrailer = null;

            this.dock_id = null;

            this.state_info = null;

            this.truck_info = null;

            this.model_info = null;

            this.truck11ton_textureList.forEach(function(item, key, mapObj) {
                  item.dispose();
            });

            this.truck11ton_textureList = null;

            this.truckTrailer_textureList.forEach(function(item, key, mapObj) {

                  item.dispose();

            });

            this.truckTrailer_textureList = null;

            MeshManager.disposeMesh(this._container);

            super._onDestroy();

      }

}

class LoaderCjTruck11ton {

      constructor(){
            this.truck11ton_texture = {
                  compName: "cjTruck11ton_customET",
                  mapPath: "/output/resource/obj3d/cjTruck11ton_customET/maps",
                  mapNameList: [
                        { "key": "major", "name": "cjTruck11ton_orange" },
                        { "key": "critical", "name": "cjTruck11ton_red" },
                        { "key": "minor", "name": "cjTruck11ton_yellow" },
                        { "key": "warning", "name": "cjTruck11ton_blue" },
                        { "key": "info", "name": "cjTruck11ton_gray" },
                        { "key": "normal", "name": "cjTruck11ton_green" }
                  ],
                  recycleYn: false,
            };

            this.currentIndex = 0;
            this.truck11ton_textureList = new Map();
            this._resolve =null;

      }


      loadStart(){
            return new Promise((resolve,reject)=>{
                  this._resolve = resolve;

                  this.currentIndex = 0;
                  this.loadMapAt(this.currentIndex);
            })


      }


      nextLoad(){
            this.currentIndex++;
            if(this.truck11ton_texture.mapNameList.length<=this.currentIndex){
                  //this.callback(true);
                  this._resolve(true);
            }else {
                  this.loadMapAt(this.currentIndex);
            }
      }


      async loadMapAt(index){
            let temp = Object.assign({}, this.truck11ton_texture);
            let mapItem = this.truck11ton_texture.mapNameList[index];
            temp.mapNameList = mapItem["name"];


            try {
                  let result = await LoaderManager.setLoaderTexture(temp);
                  this.truck11ton_textureList.set(mapItem.key, result);
                  this.nextLoad();

            }catch(error){

            }


      }
}


class LoaderCjTrailer {

      constructor(){


            this.textureInfo = {
                  compName: "cjTrailer_customET",
                  mapPath: "/output/resource/obj3d/cjTrailer_customET/maps",
                  mapNameList: [
                        { "key": "major", "name": "cjTrailer_orange" },
                        { "key": "critical", "name": "cjTrailer_red" },
                        { "key": "minor", "name": "cjTrailer_yellow" },
                        { "key": "warning", "name": "cjTrailer_blue" },
                        { "key": "info", "name": "cjTrailer_gray" },
                        { "key": "normal", "name": "cjTrailer_green" }
                  ],
                  recycleYn: false,
            };


            this.currentIndex = 0;
            this.textureList = new Map();

            this._resolve =null;

      }


      loadStart(){
            return new Promise((resolve,reject)=>{
                  this._resolve = resolve;

                  this.currentIndex = 0;
                  this.loadMapAt(this.currentIndex);
            })
      }


      nextLoad(){
            this.currentIndex++;
            if(this.textureInfo.mapNameList.length<=this.currentIndex){
                  this._resolve(true);
            }else {
                  this.loadMapAt(this.currentIndex);
            }
      }


      async loadMapAt(index){
            let temp = Object.assign({}, this.textureInfo);
            let mapItem = this.textureInfo.mapNameList[index];
            temp.mapNameList = mapItem["name"];

            try {
                  let result = await LoaderManager.setLoaderTexture(temp);
                  this.textureList.set(mapItem.key, result);
                  this.nextLoad();

            }catch(error){

            }


      }
}



WV3DPropertyManager.attach_default_component_infos(CjTerminalB2f_ROOMComponent, {
      "setter": {
            "size": { x: 0, y: 0, z: 0 }
      },

      "label": {
            "label_text": "CjTerminalB2f_ROOMComponent",
      },

      "info": {
            "componentName": "CjTerminalB2f_ROOMComponent",
            "version": "1.0.0",
      }
});


CjTerminalB2f_ROOMComponent.property_panel_info = [{
      label: "일반 속성",
      template: "primary",
      children: [{
            owner: "primary",
            name: "name",
            type: "string",
            label: "이름",
            writable: true,
            show: true,
            description: "컴포넌트 아이디"
      }]
}, {
      label: "위치 크기 정보",
      template: "pos-size-3d",
      children: [{
            owner: "setter",
            name: "lock",
            type: "checkbox",
            label: "잠금",
            show: true,
            writable: true,
            description: "잠금 유무"
      }, {
            owner: "setter",
            name: "position",
            type: "object",
            label: "위치",
            show: true,
            writable: true,
            description: "위치 값"
      }, {
            owner: "setter",
            name: "size",
            type: "object",
            label: "크기",
            show: true,
            writable: true,
            description: "크기 값"
      }, {
            owner: "setter",
            name: "rotation",
            type: "object",
            label: "회전",
            show: true,
            writable: true,
            description: "회전 값"
      }]
}, {
      label: "레이블 속성",
      template: "label-3d",
      children: [{
            owner: "label",
            name: "label",
            type: "string",
            label: "레이블",
            writable: true,
            show: true,
            description: "레이블 속성입니다."
      }]
}];

WV3DPropertyManager.add_event(CjTerminalB2f_ROOMComponent, {
      name: "change",
      label: "값 체인지 이벤트",
      description: "값 체인지 이벤트 입니다.",
      properties: [{
            name: "value",
            type: "string",
            default: "",
            description: "새로운 값입니다."
      }]
});

WV3DPropertyManager.remove_event(CjTerminalB2f_ROOMComponent,"dblclick");
WV3DPropertyManager.remove_event(CjTerminalB2f_ROOMComponent,"click");
