"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @description 하차 정보를 나타내는 컴포넌트
 * @author Kangeunji
 * @version 2018.3.26
 */
var CjTerminal1f_ROOMComponent = function (_WV3DComponent) {
    _inherits(CjTerminal1f_ROOMComponent, _WV3DComponent);

    function CjTerminal1f_ROOMComponent() {
        _classCallCheck(this, CjTerminal1f_ROOMComponent);

        var _this = _possibleConstructorReturn(this, (CjTerminal1f_ROOMComponent.__proto__ || Object.getPrototypeOf(CjTerminal1f_ROOMComponent)).call(this));

        _this._isResourceComponent = true;
        return _this;
    }

    _createClass(CjTerminal1f_ROOMComponent, [{
        key: "_onCreateProperties",
        value: function _onCreateProperties() {

            this.syncAuto = true;

            this.dbclickCallback = this._onDblclick.bind(this);

            /* [ 상태 컬러 정보를 담아두기 위한 풀 ] */
            this.truck11ton_textureList = new Map();

            this.truckTrailer_textureList = new Map();

            this.truck11ton = null;

            /* [ 트럭 - 2 ] */
            this.truckTrailer = null;

            /* [ WorkView에서 필요한 dockList ] */
            this.workViewDockList = new Map();

            /* [ 아래값을 가지고 구성 하기때문에 데이터 포맷 변경시 필히 키값 변경해야함 ] */

            this.dock_id = "dock_id"; // dock_id 키

            /**
             *  @description 상태 키 정보
             *  || status  || -- 상태값 가진 컬럼 키
             *
             *  ||    3    ||  -- 상태값이 minor 일때
             *  ||    2    ||  -- 상태값이 major 일때
             */
            this.state_info = {
                "key": "status",
                5: "critical",
                4: "warning",
                3: "minor",
                2: "major",
                1: "normal",
                6: "info"
            };

            /**
             *  @description 트럭 타입 정보
             *  || tr_yn || -- 트럭값을 가진 컬럼 키
             *
             *  ||   Y   ||  -- 11ton 트럭일때
             *  ||   N   ||  -- trailer 트럭일때
             */
            this.truck_info = {
                "key": "tr_yn",
                "11ton": "Y",
                "trailer": "N"
            };

            /**
             *  @description 트럭 노출 여부 정보
             *  || model_yn || -- 트럭 노출 여부 값을 가진 컬럼 키
             *
             *  ||     Y    ||  -- 트럭이 노출될때
             *  ||     N    ||  -- 트럭이 노출되지 않을때
             */
            this.model_info = {
                "key": "model_yn",
                "visible": "Y",
                "invisible": "N"
            };
        }

        /**
         * 컴포넌트 생성
         */

    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            this._element.material.visible = false;
            this._elementSize = new THREE.Box3().setFromObject(this._element).getSize();
        }

        /**
         * @description 데이터셋 데이터를 주입 받아 처리하는 함수
         * @param {object} data 데이터셋으로 주입받는 데이터
         */

    }, {
        key: "syncDockProperties",
        value: function syncDockProperties(data) {

            if (this._element) {
                var truck_key = this.truck_info["key"];
                var model_key = this.model_info["key"];
                var state_key = this.state_info["key"];
                var dockSize = data.length;

                for (var i = 0; i < dockSize; i++) {

                    var dock = this._element.children[0].getObjectByName(data[i].dock_id);

                    if (dock == undefined) continue;

                    if (this.workViewDockList.get(dock.name)) this.workViewDockList.get(dock.name).mesh.dockInfo = JSON.parse(JSON.stringify(data[i]));

                    /*[ 초기 로드 될 경우 ]*/
                    if (!dock.dockInfo) {

                        dock.dockInfo = JSON.parse(JSON.stringify(data[i]));
                        if (data[i][model_key] == this.model_info["visible"]) {

                            this.setInit(dock, data[i]);
                        };

                        continue;
                    }

                    /*[ 노출 여부가 바뀔 경우 ]*/
                    if (dock.dockInfo[model_key] != data[i][model_key]) {

                        this.setVisible(dock, data[i]);

                        continue;
                    }

                    /*[ 트럭 종류가 바뀔 경우 ]*/
                    if (dock.dockInfo[truck_key] != data[i][truck_key]) {

                        if (data[i][model_key] == this.model_info["visible"]) this.setTruck(dock, data[i]);

                        continue;
                    }

                    /*[ 상태값이 바뀔 경우 ]*/
                    if (dock.dockInfo[state_key] != data[i][state_key]) {

                        if (data[i][model_key] == this.model_info["visible"]) {

                            this.setState(dock, data[i]);
                        }
                    }
                }
            }
        }
    }, {
        key: "setInit",
        value: function setInit(dock, info) {

            var truck_key = this.truck_info["key"];

            this._domEvents.addEventListener(dock, 'dblclick', this.dbclickCallback, false);

            dock.getObjectByName(info[truck_key]).material.visible = true;

            this.setState(dock, info);
        }
    }, {
        key: "setVisible",
        value: function setVisible(dock, info) {

            var model_key = this.model_info["key"];

            var state_key = this.state_info["key"];

            var truck_key = this.truck_info["key"];

            var targetTruck = dock.getObjectByName(info[truck_key]);

            if (dock._3xDomEvent && dock._3xDomEvent.dblclickHandlers.length > 0) {

                this._domEvents.removeEventListener(dock, 'dblclick', this.dbclickCallback, false);
            }

            /*[ invisible 일 경우 ]*/
            if (info[model_key] == this.model_info["invisible"]) {

                if (dock.children.length > 0) {

                    for (var i = 0; i < dock.children.length; i++) {

                        dock.children[i].material.visible = false;
                    }
                }

                dock.dockInfo[model_key] = info[model_key];

                dock.dockInfo[truck_key] = info[truck_key];

                /*[ visible 일 경우 ]*/
            } else if (info[model_key] == this.model_info["visible"]) {

                for (var i = 0; i < dock.children.length; i++) {

                    dock.children[i].material.visible = false;
                }

                targetTruck.material.visible = true;

                dock.dockInfo[model_key] = info[model_key];

                if (dock.dockInfo[truck_key] != info[truck_key]) {

                    dock.dockInfo[truck_key] = info[truck_key];

                    this.setTruck(dock, info);
                } else {

                    this.setState(dock, info);
                }
            }
        }
    }, {
        key: "setTruck",
        value: function setTruck(dock, info) {

            var truck_key = this.truck_info["key"];

            var targetTruck = dock.getObjectByName(info[truck_key]);

            for (var i = 0; i < dock.children.length; i++) {

                dock.children[i].material.visible = false;
            }

            targetTruck.material.visible = true;

            dock.dockInfo[truck_key] = info[truck_key];

            this.setState(dock, info);
        }
    }, {
        key: "setState",
        value: function setState(dock, info) {

            var truck_key = this.truck_info["key"];

            var state_key = this.state_info["key"];

            var targetTruck = dock.getObjectByName(info[truck_key]);

            dock.dockInfo[state_key] = info[state_key];

            if (targetTruck.material.map) {

                targetTruck.material.map.dispose();
                targetTruck.material.map = null;
                targetTruck.material.dispose();

                wemb.threeElements.renderer.dispose();
            }

            var texture = null;

            var stateFlag = info[state_key];

            if (dock.dockInfo[truck_key] == this.truck_info["trailer"]) {

                texture = this.truckTrailer_textureList.get(this.state_info[stateFlag]);
            } else if (dock.dockInfo[truck_key] == this.truck_info["11ton"]) {

                texture = this.truck11ton_textureList.get(this.state_info[stateFlag]);
            }

            targetTruck.material.map = texture;
            targetTruck.material.needsUpdate = true;
        }
    }, {
        key: "_onDblclick",
        value: function _onDblclick(e) {

            if (!this.isEditorMode) {
                this.dispatchWScriptEvent("change", {
                    value: JSON.stringify(e.target.dockInfo)
                });
            }
        }
    }, {
        key: "startLoadResource",
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var success;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _context.next = 3;
                                return this.validateResource();

                            case 3:
                                success = _context.sent;

                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);

                                _context.next = 11;
                                break;

                            case 7:
                                _context.prev = 7;
                                _context.t0 = _context["catch"](0);

                                console.log("error ", _context.t0);
                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);

                            case 11:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 7]]);
            }));

            function startLoadResource() {
                return _ref.apply(this, arguments);
            }

            return startLoadResource;
        }()
    }, {
        key: "validateResource",
        value: function validateResource() {
            var _this2 = this;

            var that = this;

            return new Promise(function (resolve, reject) {
                /* [ 룸 로드 정보 주입 ] */
                var room_obj = {

                    compName: "CjTerminal1f_customROOM",
                    objPath: "/output/resource/obj3d/cjTerminal1f_customROOM/cjTerminal1f_customROOM.obj",
                    mapPath: "/output/resource/obj3d/cjTerminal1f_customROOM/maps",
                    recycleYn: false,
                    loaderFunc: function loaderFunc(info) {

                        return new Promise(function (resolve, reject) {

                            LoaderManager.objLoader.load(wemb.configManager.serverUrl + info.objPath, function (object) {

                                var oSize = object.children.length;

                                for (var i = 0; i < oSize; i++) {

                                    var meshChild = object.children[i];

                                    var name = meshChild["name"];

                                    if (name == "1fWall" || name == "1fFloorDown" || name == "1fFloorUp" || name == "logo_A" || name == "window_A" || name.indexOf("pillar") != -1) {

                                        if (meshChild instanceof THREE.Mesh) {

                                            meshChild["material"] = new THREE.MeshBasicMaterial();
                                            meshChild["material"]["side"] = THREE.DoubleSide;
                                            meshChild["material"]["map"] = LoaderManager.textureLoader.load(wemb.configManager.serverUrl + info.mapPath + "/" + name + ".png");

                                            /* 투명 처리  */
                                            if (name.indexOf("_A") != -1) {

                                                meshChild["material"].transparent = true;
                                            }
                                        }
                                    } else {

                                        /* [ 트럭 BOX 투명처리 ] */
                                        meshChild["material"].transparent = true;
                                        meshChild["material"].opacity = 0;
                                        meshChild["material"].visible = false;

                                        meshChild.geometry.computeBoundingBox();
                                        meshChild.geometry.computeBoundingSphere();
                                        meshChild.position.set(meshChild.geometry.boundingSphere.center.x, meshChild.geometry.boundingSphere.center.y, meshChild.geometry.boundingSphere.center.z);
                                        meshChild.geometry.center();
                                        if (name.indexOf("A") != -1 || name.indexOf("B") != -1) {

                                            meshChild.rotation.y = Math.PI;
                                        }

                                        if (name.indexOf("B") != -1 || name.indexOf("C") != -1) {

                                            that.workViewDockList.set(name, {
                                                name: name,
                                                pos: meshChild.position,
                                                mesh: meshChild
                                            });
                                        }

                                        var truck_1 = LoaderManager.clone(new THREE.Mesh(), that.truck11ton);

                                        var truck_2 = LoaderManager.clone(new THREE.Mesh(), that.truckTrailer);

                                        truck_2.position.z += 15;

                                        truck_2.position.y += 2;

                                        meshChild.add(truck_1);

                                        meshChild.add(truck_2);
                                    }
                                }

                                resolve(object);
                            });
                        });
                    }

                };

                /* [ 트럭 2종 로드 정보 주입 ] */
                var truck11ton_Obj = {
                    compName: "cjTruck11ton_customET",
                    objPath: "/output/resource/obj3d/cjTruck11ton_customET/cjTruck11ton_customET.obj",
                    recycleYn: false
                };

                var truckTrailer_Obj = {
                    compName: "cjTrailer_customET",
                    objPath: "/output/resource/obj3d/cjTrailer_customET/cjTrailer_customET.obj",
                    recycleYn: false
                };

                /* [ 트럭 2종 로드 ] */
                LoaderManager.setLoaderObj(truck11ton_Obj).then(function (result) {

                    _this2.truck11ton = result.MESH.children[0];

                    _this2.truck11ton.name = "Y";

                    _this2.truck11ton["material"].transparent = true;
                    _this2.truck11ton["material"].visible = false;

                    LoaderManager.setLoaderObj(truckTrailer_Obj).then(function (result) {

                        _this2.truckTrailer = result.MESH.children[0];

                        _this2.truckTrailer.name = "N";

                        _this2.truckTrailer["material"].transparent = true;
                        _this2.truckTrailer["material"].visible = false;
                    });
                });

                var truck11ton_texture = {
                    compName: "cjTruck11ton_customET",
                    mapPath: "/output/resource/obj3d/cjTruck11ton_customET/maps",
                    mapNameList: [{ "key": "major", "name": "cjTruck11ton_orange" }, { "key": "critical", "name": "cjTruck11ton_red" }, { "key": "minor", "name": "cjTruck11ton_yellow" }, { "key": "warning", "name": "cjTruck11ton_blue" }, { "key": "info", "name": "cjTruck11ton_gray" }, { "key": "normal", "name": "cjTruck11ton_green" }],
                    recycleYn: false
                };

                var truckTrailer_texture = {
                    compName: "cjTrailer_customET",
                    mapPath: "/output/resource/obj3d/cjTrailer_customET/maps",
                    mapNameList: [{ "key": "major", "name": "cjTrailer_orange" }, { "key": "critical", "name": "cjTrailer_red" }, { "key": "minor", "name": "cjTrailer_yellow" }, { "key": "warning", "name": "cjTrailer_blue" }, { "key": "info", "name": "cjTrailer_gray" }, { "key": "normal", "name": "cjTrailer_green" }],
                    recycleYn: false
                };

                var _loop = function _loop(i) {

                    var temp = Object.assign({}, truck11ton_texture);

                    temp.mapNameList = truck11ton_texture.mapNameList[i].name;

                    /* [ Truck 상태별 텍스쳐 로드 ] */
                    LoaderManager.setLoaderTexture(temp).then(function (result) {

                        _this2.truck11ton_textureList.set(truck11ton_texture.mapNameList[i].key, result);
                        temp = null;
                    });
                };

                for (var i = 0; i < truck11ton_texture.mapNameList.length; i++) {
                    _loop(i);
                }

                var _loop2 = function _loop2(i) {

                    var temp = Object.assign({}, truckTrailer_texture);

                    temp.mapNameList = truckTrailer_texture.mapNameList[i].name;

                    /* [ Trailer 상태별 텍스쳐 로드 ] */
                    LoaderManager.setLoaderTexture(temp).then(function (result) {

                        _this2.truckTrailer_textureList.set(truckTrailer_texture.mapNameList[i].key, result);

                        temp = null;
                    });
                };

                for (var i = 0; i < truckTrailer_texture.mapNameList.length; i++) {
                    _loop2(i);
                }

                /* [ 룸 로드 ] */
                LoaderManager.setLoaderObj(room_obj).then(function (result) {

                    _this2._elementSize = result.SIZE;

                    _this2._element.add(result.MESH);

                    _this2.size = _this2._elementSize;

                    resolve();
                });
            });
        }
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {

            this.dbclickCallback = null;

            this.workViewDockList = null;

            MeshManager.disposeMesh(this.truck11ton);

            MeshManager.disposeMesh(this.truckTrailer);

            this.truck11ton = null;

            this.truckTrailer = null;

            this.dock_id = null;

            this.state_info = null;

            this.truck_info = null;

            this.model_info = null;

            this.truck11ton_textureList.forEach(function (item, key, mapObj) {

                MeshManager.disposeMesh(item);
            });

            this.truck11ton_textureList = null;

            this.truckTrailer_textureList.forEach(function (item, key, mapObj) {

                MeshManager.disposeMesh(item);
            });

            this.truckTrailer_textureList = null;

            MeshManager.disposeMesh(this._container);

            _get(CjTerminal1f_ROOMComponent.prototype.__proto__ || Object.getPrototypeOf(CjTerminal1f_ROOMComponent.prototype), "_onDestroy", this).call(this);
        }
    }]);

    return CjTerminal1f_ROOMComponent;
}(WV3DComponent);

WV3DPropertyManager.attach_default_component_infos(CjTerminal1f_ROOMComponent, {
    "setter": {
        "size": { x: 0, y: 0, z: 0 }
    },

    "label": {
        "label_text": "CjTerminal1f_ROOMComponent"
    },

    "info": {
        "componentName": "CjTerminal1f_ROOMComponent",
        "version": "1.0.0"
    }
});

CjTerminal1f_ROOMComponent.property_panel_info = [{
    label: "일반 속성",
    template: "primary",
    children: [{
        owner: "primary",
        name: "name",
        type: "string",
        label: "이름",
        writable: true,
        show: true,
        description: "컴포넌트 아이디"
    }]
}, {
    label: "위치 크기 정보",
    template: "pos-size-3d",
    children: [{
        owner: "setter",
        name: "lock",
        type: "checkbox",
        label: "잠금",
        show: true,
        writable: true,
        description: "잠금 유무"
    }, {
        owner: "setter",
        name: "position",
        type: "object",
        label: "위치",
        show: true,
        writable: true,
        description: "위치 값"
    }, {
        owner: "setter",
        name: "size",
        type: "object",
        label: "크기",
        show: true,
        writable: true,
        description: "크기 값"
    }, {
        owner: "setter",
        name: "rotation",
        type: "object",
        label: "회전",
        show: true,
        writable: true,
        description: "회전 값"
    }]
}, {
    label: "레이블 속성",
    template: "label-3d",
    children: [{
        owner: "label",
        name: "label",
        type: "string",
        label: "레이블",
        writable: true,
        show: true,
        description: "레이블 속성입니다."
    }]
}];

WV3DPropertyManager.add_event(CjTerminal1f_ROOMComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
        name: "value",
        type: "string",
        default: "",
        description: "새로운 값입니다."
    }]
});

WV3DPropertyManager.remove_event(CjTerminal1f_ROOMComponent, "dblclick");
WV3DPropertyManager.remove_event(CjTerminal1f_ROOMComponent, "click");
