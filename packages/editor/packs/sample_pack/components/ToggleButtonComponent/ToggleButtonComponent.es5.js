class ToggleButtonComponent extends WVDOMComponent {

    constructor() {
        super();
        console.log("AAA , ToggleButtonComponent 시작!");

        // step #07-01
        this._$label = null;
    }


      onLoadPage(){
          console.log("AAA, ToggleButtonComponent", this._properties);
      }


      // step #07-01
    _onCreateElement() {
        this._$label = $("<div>label</div>");
        this._$label.css({
            position: "absolute",
            left: 0,
            top: 0
        })
        $(this._element).append(this._$label);


        // step #07-04
        if(this.isViewerMode==true) {
            $(this._element).on("click", () => {
               let selected = this.getGroupPropertyValue("toggle", "selected");
               this.setGroupPropertyValue("toggle", "selected", !selected);

             })
          }

    }

    
    _onImmediateUpdateDisplay(){ 
        this._validateToggle();
    }


    _onCommitProperties(){
            if(this._updatePropertiesMap.has("toggle")){
                this._validateToggle();
            }

            if(this._updatePropertiesMap.has("toggle.selected")){
                var toggleValues = this.getGroupProperties("toggle");
                this.dispatchWScriptEvent("change",{value:toggleValues.selected})
            }
      
    }
    

  
    _validateToggle(){
        // 토글 그룹 속성 구하기
        var toggleValues = this.getGroupProperties("toggle");
 
        // 선택 처리 
        if(toggleValues.selected){ 
              this._$label.text(toggleValues.selectedLabel);
             this._$label.css("color", toggleValues.selectedColor);
        }else {// 일반 처리 
             this._$label.text(toggleValues.normalLabel);
             this._$label.css("color", toggleValues.normalColor);
        }

        // label text의 width, height 값 구하기 ○2-○4
        var labelSizeInfo = this.getLabelSize(this._$label.text(), 11);
        // 위치 값 계산하기 ○2-○5
        var left = (this.width - labelSizeInfo.width)/2;
        var top = (this.height - labelSizeInfo.height)/2;

        // 위치 설정 하기 ○2-○6
        this._$label.css({
             left:left,
             top:top
        })

    }


}

WVPropertyManager.attach_default_component_infos(ToggleButtonComponent, {
    "info": {
        "componentName": "ToggleButtonComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 300,
        "height": 300
    },

    "toggle":{
        "selected":true,
        "selectedColor":"#ff0000",
        "normalColor":"#000000",
        "selectedLabel":"",
        "normalLabel":""
    },

    "style": {
        "border": "1px solid #000000",
        "backgroundColor": "#eeeeee",
        "borderRadius": 1,
        "cursor": "default"
    }
});


// 컴포넌트 속성 그룹 삭제
WVPropertyManager.remove_default_props(ToggleButtonComponent, "label");

// 컴포넌트 속성 그룹 정보 삭제
WVPropertyManager.remove_property_panel_group_info(ToggleButtonComponent, "label");


// 신규 추가 내용
WVPropertyManager.add_property_panel_group_info(ToggleButtonComponent, {
      label: "토글 정보 ",
      template: "vertical",
      children: [{
            owner: "toggle",
            name: "selected",
            type: "checkbox",
            label: "selected",
            show: true,
            writable: true,
            description: "선택 유무"
      },{
            owner: "toggle",
            name: "selectedColor",
            type: "color",
            label: "S.Color",
            show: true,
            writable: true,
            description: "선택 색상"
      },{
            owner: "toggle",
            name: "selectedLabel",
            type: "normal",
            label: "S.Label",
            show: true,
            writable: true,
            description: "선택시 출력 라벨"
      },{
            owner: "toggle",
            name: "normalColor",
            type: "color",
            label: "N.Color",
            show: true,
            writable: true,
            description: "일반 색상"
      },{
            owner: "toggle",
            name: "normalLabel",
            type: "normal",
            label: "N.Label",
            show: true,
            writable: true,
            description: "일반 상태시 출력 라벨"
      }]
})


WVPropertyManager.add_event(ToggleButtonComponent, {
    name: "change",
    label: "값 체인지 이벤트",
    description: "값 체인지 이벤트 입니다.",
    properties: [{
          name: "value",
          type: "boolean",
          default: "",
          description: "새로운 값입니다."
    }]
});
