class Sample3DComponent extends NWV3DComponent {

      constructor() {
            super();
      }

      _onCreateElement() {
            this._element = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshPhongMaterial').clone());
            this._element.castShadow = true;
            this._element.receiveShadow = true;
            this.appendElement.add(this._element);

      }

      _onCreateProperties(){
            super._onCreateProperties();
            this._elementSize = {x:1, y:1, z:1};
      }



}


WV3DPropertyManager.attach_default_component_infos(Sample3DComponent, {
      "setter": {
            "size": {x: 10, y: 10, z: 10}
      },
      "label": {
            "label_text": "Sample3DComponent",
      },

      "info": {
            "componentName": "Sample3DComponent",
            "version": "1.0.0",
      }
});

