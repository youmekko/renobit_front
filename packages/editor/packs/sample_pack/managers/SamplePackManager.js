/*
plugin은 모두 mediator의 view로 등록됨.
 */
class SamplePackManager extends ExtensionPluginCore {


      constructor() {
            super();


            console.log("SamplePackManager start 2");
      }



      start() {
            window.wemb.samplePackManager = SamplePackManager._instance;

            console.log("HookManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE ",  HookManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE);
            wemb.hookManager.addAction(HookManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE, function(instance){
                  console.log(instance);
                  return true;
            })
      }


      setFacade(facade) {
            super.setFacade(facade);
            this._selectProxy = this._facade.retrieveProxy(SelectProxy.NAME);
      }



      destroy() {
      }

      get notificationList() {
            return [
                  SelectProxy.NOTI_UPDATE_PROPERTIES
            ]
      }


      /*
	  noti가 온 경우 실행
	  call : mediator에서 실행
	   */
      handleNotification(note) {
            switch (note.name) {
                  // 편집 페이지가 열릴때마다 실행
                  case SelectProxy.NOTI_UPDATE_PROPERTIES:
                        this.noti_updateProperties();
            }

      }


      /*
	컴포넌트 프로퍼티가 업데이트되면 실행.



	 */
      noti_updateProperties() {
            console.log("updateProperty ")

      }
      /////////////////////////////////////////////////////////////

}
