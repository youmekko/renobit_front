class SamplePanel extends ExtensionPanelCore {

      constructor() {
            super();
            this.$_element = null;
            console.log("SamplePanel");

      }

      /*
	      call :
	            인스턴스 생성 후 실행2
	            단, 아직 화면에 붙지 않은 상태임.


      */
      create() {
            this._$element = $("<div class='sample-panel'>panel</div>");
            this._$parentView.append(this._$element);
      }

      /*
	-모든 확장요소 생성이 완료되면 호출
	*/
      start() {
            console.log("element");
      }


      get notificationList() {
            return [
                  EditorProxy.NOTI_OPENED_PAGE,
                  EditorProxy.NOTI_CLOSED_PAGE,
            ]
      }


      /*
	  noti가 온 경우 실행
	  call : mediator에서 실행
	  */
      handleNotification(note) {
            switch (note.name) {
                  case EditorProxy.NOTI_OPENED_PAGE:
                        console.log("PANEL, OPENED PAGE ");
                        break;
                  case EditorProxy.NOTI_CLOSED_PAGE:
                        console.log("PANEL, CLOSED PAGE ");
                        break;
            }
      }
}
