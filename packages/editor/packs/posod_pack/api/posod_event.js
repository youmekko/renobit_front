module.exports = function (renobit) {
      'use strict';
      var _ = require('lodash');
      const moment = require('moment');
      const TB_PREFIX = 'A_';
      const TB_ASSET_EVENT_CODE = 'TB_ASSET_EVENT_CODE';
      const TB_ASSET_EVENT = 'TB_ASSET_EVENT';
      const TB_INSTANCE = 'TB_INSTANCE';
      const TB_PAGE = 'TB_PAGE';
      const TB_ASSET_COMMON = 'TB_ASSET_COMMON';
      const TB_ASSET_RELATION = 'TB_ASSET_RELATION';
      const SCHEMA = {
            "code": {"type": "string", "defaultValue": null, "maxLength": 30, "nullable": true},
            "asset_type": {"type": "string", "defaultValue": null, "maxLength": 50, "nullable": true},
            "severity": {"type": "string", "defaultValue": null, "maxLength": 20, "nullable": true},
            "event_status": {"type": "string", "defaultValue": null, "maxLength": 255, "nullable": true},
            "event_msg": {"type": "string", "defaultValue": null, "maxLength": 255, "nullable": true},
            "ack": {"type": "string", "defaultValue": null, "maxLength": 1, "nullable": true},
            "employee_type": {"type": "integer", "defaultValue": null, "maxLength": null, "nullable": true}
      };

      const SEVERITY = {
            NORMAL: "normal",
            MINOR: "minor",
            MAJOR: "major",
            WARNING: "warning",
            CRITICAL: "critical"
      };

      const TYPE = {
            CCTV: "CCTV",
            ACCESS: "Access",
            LEAK: "WaterLeak",
            FIRE: "Fire",
            TEMPHUMI: "THSensor",
            MDM: "MDM",
            GAS: "GasLeak",
            EARTH: "EarthQuake",
            PDU: "PDU",
            RACK: "Rack",
            SERVER: "Server",
            NETWORK: "Network",
            STORAGE: "Storage",
            BACKUP: "Backup",
            THERMOHYGROSTAT: "Thermohygrostat"
      };

      const code = {
            uninit: function () {
                  return renobit.database.schema.dropTableIfExists(TB_ASSET_EVENT_CODE);
            },
            init: function (data) {
                  var promise = new Promise(function (resolve, reject) {
                        renobit.database.schema.hasTable(TB_ASSET_EVENT_CODE).then(function (exists) {
                              if (!exists) {
                                    renobit.database.schema.createTable(TB_ASSET_EVENT_CODE, function (table) {
                                          var pk_arr = [];
                                          _.each(SCHEMA, (detail, column_name) => {
                                                var column = detail.type === 'string' ? table[detail.type](column_name, detail.maxLength) : table[detail.type](column_name);
                                                if(detail.type === "timestamp") column.defaultTo(RENOBIT.database.fn.now())
                                                if(detail.defaultValue !== null) column.defaultTo(detail.defaultValue);
                                                else column.notNullable();
                                                if(detail.nullable) column.nullable();
                                                if(detail.pk) pk_arr.push(column_name);
                                          })
                                          if(pk_arr.length > 0) table.primary(pk_arr);
                                    }).then(function (res) {
                                          if(data) {
                                                renobit.database.batchInsert(TB_ASSET_EVENT_CODE, data).then(() => {
                                                      resolve();
                                                }).catch(err => {
                                                      reject(err);
                                                });
                                          } else {
                                                resolve();
                                          }
                                    }).catch(function (err) {
                                          reject(err);
                                    })
                              } else {
                                    resolve();
                              }
                        })
                  });
                  return promise;
            },
            select: function (param = null) {
                  return new Promise(function (resolve, reject) {
                        let queryBuilder = renobit.database(TB_ASSET_EVENT_CODE).select("*");
                        if (param) {
                              queryBuilder.where(param);
                        }

                        queryBuilder.limit(1).then((result) => {
                              resolve(result);
                        }).catch((err) => {
                              reject(err);
                        })
                  })
            },
            insert: function (query) {
                  return new Promise(function (resolve, reject) {
                        let resolveList = [];

                        if(Array.isArray(query)) {
                              query.map((item)=>{
                                    resolveList.push(procInsertSocketData(item));
                              })

                              Promise.all(resolveList).then(() => resolve()).catch((err) => reject(err));
                        } else {
                              return procInsertSocketData(query);
                        }

                        function procInsertSocketData(query) {
                              return new Promise((res2, reject) => {
                                    code.select({code: query.code}).then((list) => {
                                          let promises = [];
                                          let item = list[0];
                                          if(item) {
                                                query.msg = item.event_msg;
                                                query.severity = query.severity || item.severity;
                                          }
                                          query.extra = JSON.stringify(query.extra);

                                          let repMsg = query.msg.replace("%LaCode%", query.asset_id);
                                          repMsg = repMsg.replace("%CardNo%", query.object);

                                          renobit.database(TB_INSTANCE).select("NAME AS name").where({ASSET_ID: query.asset_id})
                                                .then(result => {
                                                      if (result.length > 0) {
                                                            repMsg = repMsg.replace("%Name%", result[0].name);
                                                      }
                                                      query.msg = repMsg;

                                                      let action = query.action;
                                                      if (action == 'add') {
                                                            delete query.code;
                                                            delete query.action;
                                                            query.dupl_cnt = 0;
                                                            renobit.database(TB_ASSET_EVENT).insert(query).then(()=>res2()).catch((err) => reject(err));
                                                      } else if (action == 'release') {
                                                            renobit.database(TB_ASSET_EVENT)
                                                                  .andWhere({evt_id: query.evt_id, ack: 'N'})
                                                                  .andWhereNot({severity: 'normal'})
                                                                  .update({ack: 'Y', stime: query.stime}).then(()=>res2()).catch((err) => reject(err));
                                                      } else if (action == 'increment') {
                                                            query.application = query.application || null;
                                                            query.object = query.object || null;

                                                            renobit.database(TB_ASSET_EVENT)
                                                                  .andWhere({
                                                                        asset_id: query.asset_id,
                                                                        object: query.object,
                                                                        application: query.application
                                                                  })
                                                                  .update({
                                                                        stime: query.stime,
                                                                        dupl_cnt: renobit.database.raw('"dupl_cnt" + ' + 1)
                                                                  }).then(()=>res2()).catch((err) => reject(err));
                                                      }
                                                });
                                    }).catch((err) => {
                                          reject(err);
                                    });
                              })
                        }
                  })
            },
            accessEventList: function(query){
                  return new Promise((resolve, reject)=> {
                        let promises = [];
                        let queryBuilder = renobit.database(TB_ASSET_EVENT);
                        let userFilter = query.userFilter || false;
                        delete query.userFilter;

                        queryBuilder.where(TB_ASSET_EVENT+ '.asset_type', 'Access');
                        queryBuilder.whereBetween(TB_ASSET_EVENT + '.stime', [query.s_time, query.e_time]);

                        if(query.isCritical) {
                              // 비인가
                              queryBuilder.andWhere({severity: 'critical'})
                        } else {
                              // 일반
                              queryBuilder.where(TB_ASSET_EVENT + '.extra', 'like', '%'+query.text+'%');
                        }

                        let subQuery = renobit.database(TB_ASSET_RELATION).count("child_id").as('count').whereRaw('"parent_id" = "TB_ASSET_EVENT"."asset_id"');
                        queryBuilder.select(TB_ASSET_EVENT + '.*', TB_INSTANCE + ".PAGE_ID AS page_id",
                              TB_INSTANCE + ".INST_ID AS inst_id", TB_ASSET_COMMON + ".name AS asset_name",
                              TB_PAGE + ".NAME AS page_name", subQuery)
                              .innerJoin(TB_INSTANCE, TB_ASSET_EVENT + '.asset_id', TB_INSTANCE + '.ASSET_ID')
                              .innerJoin(TB_PAGE, TB_PAGE + '.PAGE_ID', TB_INSTANCE + '.PAGE_ID')
                              .innerJoin(TB_ASSET_COMMON, TB_ASSET_COMMON + '.id', TB_INSTANCE + '.ASSET_ID')
                              .orderBy(TB_ASSET_EVENT + ".stime", 'desc')
                              .map(row => {
                                    let event_time = moment(row.stime, "YYYYMMDDHHmmss");
                                    row.stime = event_time.format("YYYY-MM-DD HH:mm:ss");
                                    row.dt_stime = event_time.valueOf();
                                    row.extra = JSON.parse(row.extra);
                                    if(row.extra && Object.keys(row.extra).length > 0) {
                                          promises.push(this.accessDetailEventList(row).then(result=>{
                                                row.detail = result;
                                          }))
                                    }
                                    return row;
                              }).then(result => {
                              if(userFilter){
                                    let reducer = function( source, value ){
                                          if(value.extra && value.extra.userName) {
                                                let userValue = value.extra.userName;
                                                let uniqueUser = source.every(( info, idx )=>{
                                                      return info.extra.userName != userValue;
                                                });

                                                if(uniqueUser){
                                                      source.push(value);
                                                }
                                          }

                                          return source;
                                    }
                                    result = result.reduce(reducer, []);
                              }

                              Promise.all(promises).then(()=>{
                                    resolve(result);
                              });
                        }).catch(error => {
                              console.log(error);
                              reject(error);
                        })
                  })
            },
            accessDetailEventList: function(query){
                  return new Promise(function (resolve, reject) {
                        let queryBuilder = renobit.database(TB_ASSET_EVENT);

                        queryBuilder.where(TB_ASSET_EVENT+ '.asset_type', 'Access');

                        if(Object.keys(query).length > 0 && query.extra) {
                              queryBuilder.where(TB_ASSET_EVENT + '.extra', 'like', '%'+query.extra.cardNo+'%');
                              queryBuilder.where(TB_ASSET_EVENT + '.extra', 'like', '%'+query.extra.userName+'%');
                        }

                        let subQuery = renobit.database(TB_ASSET_RELATION).count("child_id").as('count').whereRaw('"parent_id" = "TB_ASSET_EVENT"."asset_id"');
                        queryBuilder.select(TB_ASSET_EVENT + '.*', TB_INSTANCE + ".PAGE_ID AS page_id",
                              TB_INSTANCE + ".INST_ID AS inst_id", TB_ASSET_COMMON + ".name AS asset_name",
                              TB_PAGE + ".NAME AS page_name", subQuery)
                              .innerJoin(TB_INSTANCE, TB_ASSET_EVENT + '.asset_id', TB_INSTANCE + '.ASSET_ID')
                              .innerJoin(TB_PAGE, TB_PAGE + '.PAGE_ID', TB_INSTANCE + '.PAGE_ID')
                              .innerJoin(TB_ASSET_COMMON, TB_ASSET_COMMON + '.id', TB_INSTANCE + '.ASSET_ID')
                              .orderBy(TB_ASSET_EVENT + ".stime", 'desc')
                              .map(row => {
                                    let event_time = moment(row.stime, "YYYYMMDDHHmmss");
                                    row.stime = event_time.format("YYYY-MM-DD HH:mm:ss");
                                    row.dt_stime = event_time.valueOf();
                                    row.extra = JSON.parse(row.extra);
                                    return row;
                              }).then(result => {
                              resolve(result);
                        }).catch(error => {
                              reject(error);
                        })
                  })
            },

            convertCodeToType(type) {
                  let code = type;
                  switch (type) {
                        case "CD10000000" :
                              code = TYPE.SERVER;
                              break;
                        case "CD10000001" :
                              code =  TYPE.NETWORK;
                              break;
                        case "CD10000001" :
                              code =  TYPE.NETWORK;
                              break;
                        case "CD10000002" :
                              code =  TYPE.RACK;
                              break;
                        case "CD10000003" :
                              code =  TYPE.PDU;
                              break;
                        case "CD10000004" :
                              code =  TYPE.THERMOHYGROSTAT;
                              break;
                        case "CD10000005" :
                              code =  TYPE.CCTV;
                              break;
                        case "CD10000006" :
                              code =  TYPE.TEMPHUMI;
                              break;
                        case "CD10000007" :
                              code =  TYPE.ACCESS;
                              break;
                        case "CD10000008" :
                              code =  TYPE.FIRE;
                              break;
                        case "CD10000009" :
                              code =  TYPE.MDM;
                              break;
                        case "CD10000010" :
                              code =  TYPE.STORAGE;
                              break;
                        case "CD10000011" :
                              code =  TYPE.BACKUP;
                              break;
                        default:
                              break;
                  }

                  return code;
            }
      }

      function ensureAuth(req, res, next) {
            if (req.isAuthenticated()) {
                  next();
            } else {
                  res.status(401).send({code:'AUTH_001'});
            }
      }
      var mask_code = ['0x1','0x2','0x4','0x8','0x10','0x20','0x40','0x80','0x100','0x200','0x400','0x800','0x1000','0x2000','0x4000','0x8000','0x10000','0x20000','0x40000','0x80000','0x100000','0x200000','0x400000','0x800000','0x1000000','0x2000000','0x4000000','0x8000000','0x10000000','0x20000000','0x30000000','0x80000000','0x10000000','0x20000000','0x40000000','0x80000000','0x100000000','0x200000000','0x400000000','0x800000000','0x1000000000','0x2000000000','0x4000000000','0x8000000000','0x10000000000','0x20000000000','0x40000000000','0x50000000000','0x100000000000','0x200000000000','0x400000000000','0x800000000000','0x1000000000000','0x2000000000000','0x4000000000000','0x8000000000000','0x10000000000000','0x20000000000000','0x40000000000000','0x80000000000000','0x100000000000000','0x200000000000000','0x400000000000000','0x500000000000000'];

      return {
            init: function (data) {
                  if(!data) {return true;}
                  else {return code.init(data[TB_ASSET_EVENT_CODE])};
            },
            uninit: function () {
                  code.uninit().then(() => {
                        console.log("favorite table drop success");
                  });
            },
            regist: function () {
                  renobit.app.post("/posod/playlist", function(req,res,next) {
                        var count = 0;
                        var result = [];
                        var parents = req.body.parents;
                        new Promise((resolve, reject) => {
                              try {
                                    _.each(parents, (parent_id,k) => {
                                          renobit.database(TB_ASSET_RELATION).select('*').where({parent_id:parent_id}).map((row) => {
                                                return row.child_id;
                                          }).then((childs) => {
                                                renobit.database(TB_ASSET_EVENT).where({asset_id:parent_id}).orderBy('stime', 'desc')
                                                      .then((event_info) => {
                                                            if(event_info.length > 0) {
                                                                  var event_time = moment(event_info[0].stime, "YYYYMMDDHHmmss");
                                                                  var from_time = event_time.clone().add(-10, 'second');
                                                                  var to_time = event_time.clone().add(1, 'minute');
                                                                  renobit.database(TB_PREFIX + 'CCTV').whereIn('id', childs).map((row) => {
                                                                        var mask_val = (mask_code.length < parseInt(row.record_mask) || !row.record_mask) ? "" : mask_code[parseInt(row.record_mask)-1];
                                                                        var record_pass = RENOBIT.utils.base64.decode(row.record_pass);
                                                                        return {
                                                                              "cameraName" : row.id,
                                                                              "recordServerUrl" : "qenx://" +
                                                                                    (row.record_id ? row.record_id : "") + ":" + record_pass + "@" +
                                                                                    (row.record_ip ? row.record_ip : "") + ":" + (row.record_port ? row.record_port : ""),
                                                                              "cameraMask" : mask_val,
                                                                              "fromTime":from_time.format("YYYY-MM-DD HH:mm:ss"),
                                                                              "toTime":to_time.format("YYYY-MM-DD HH:mm:ss")
                                                                        }
                                                                  }).then((rows) => {
                                                                        result = result.concat(rows);
                                                                        count++;
                                                                        if(count === parents.length) {
                                                                              resolve()
                                                                        }
                                                                  })
                                                            } else {
                                                                  count++;
                                                                  if(count === parents.length) {
                                                                        resolve()
                                                                  }
                                                            }
                                                      })
                                          });
                                    })
                              } catch (error) {
                                    reject()
                              }
                        }).then(() => {
                              res.status(200).send({playList:result});
                        }).catch(() => {
                              res.status(500).send({code:'ETC_001'});
                        })
                  })
                  
                  renobit.app.get("/posod/playlist", function(req,res,next) {
                        renobit.database(TB_ASSET_RELATION).select('*').where({parent_id:req.query.parent_id}).map((row) => {
                              return row.child_id;
                        }).then((childs) => {
                              renobit.database(TB_ASSET_EVENT).where({asset_id:req.query.parent_id}).orderBy('stime', 'desc')
                                    .then((event_info) => {
                                          if(event_info.length > 0) {
                                                var event_time = moment(event_info[0].stime, "YYYYMMDDHHmmss");
                                                var from_time = event_time.clone().add(-10, 'second');
                                                var to_time = event_time.clone().add(1, 'minute');
                                                renobit.database(TB_PREFIX + 'CCTV').whereIn('id', childs).map((row) => {
                                                      var mask_val = (mask_code.length < parseInt(row.record_mask) || !row.record_mask) ? "" : mask_code[parseInt(row.record_mask)-1];
                                                      var record_pass = RENOBIT.utils.base64.decode(row.record_pass);
                                                      return {
                                                            "cameraName" : row.id,
                                                            "recordServerUrl" : "qenx://" +
                                                                  (row.record_id ? row.record_id : "") + ":" + record_pass + "@" +
                                                                  (row.record_ip ? row.record_ip : "") + ":" + (row.record_port ? row.record_port : ""),
                                                            "cameraMask" : mask_val,
                                                            "fromTime":from_time.format("YYYY-MM-DD HH:mm:ss"),
                                                            "toTime":to_time.format("YYYY-MM-DD HH:mm:ss")
                                                      }
                                                }).then((rows) => {
                                                      res.status(200).send({
                                                            playList:rows
                                                      });
                                                })
                                          } else {
                                                res.status(200).send({
                                                      playList:[]
                                                });
                                          }
                                    })
                        });
                  })

                  renobit.app.post("/posod/event", ensureAuth, function (req, res, next) {
                        code.insert(req.body).then(() => {
                              res.status(200).send();
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });

                  renobit.app.post("/posod/access/event", ensureAuth, function(req, res, next){
                        code.accessEventList(req.body).then((result)=>{
                              res.status(200).send(result);
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  })

                  renobit.app.post("/posod/access/event/detail", ensureAuth, function(req, res, next){
                        code.accessDetailEventList(req.body).then((result)=>{
                              res.status(200).send(result);
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  })

                  renobit.app.post("/event/socket/posod", function (req, res, next) {
                        renobit.logger.info(req.body);
                        var eventList = [];
                        if (Array.isArray(req.body.event)) {
                              eventList = eventList.concat(req.body.event);
                        } else {
                              eventList.push(req.body.event);
                        }

                        eventList.map(event=>{
                              event.ack = "N";
                              let ack_msg = "Invalid";

                              // 1. severity 체크 ( severity 체크, 스펠링 체크 )
                              if(!event.severity || !SEVERITY[event.severity.toUpperCase()]) {
                                    event.ack = 'Y';
                                    ack_msg += ' severity';
                                    event.ack_info = ack_msg;
                              }

                              // 2. stime 체크 ( 24시간 이내의 데이터, stime 형식 체크 )
                              if(event.stime) {
                                    var d_stime = moment(event.stime, 'YYYYMMDDHHmmss', true);
                                    if(!d_stime.isValid() || !d_stime.isBetween(moment().add(-1, 'days'), moment())) {
                                          event.ack = 'Y';
                                          ack_msg += ' stime';
                                          event.ack_info = ack_msg;
                                    }
                              }

                              // 3. convert code
                              event.asset_type = code.convertCodeToType(event.asset_type);
                        });

                        code.insert(eventList).then(() => {
                              process.send({event: "/event/socket/posod", data: eventList.filter(x=> x.ack == 'N')});
                              res.status(200).send({process: "success"});
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'})
                        })
                  })
            },

            unregist: function () {
                  try {
                        var remove_route = [];
                        var stack = renobit.app._router.stack;
                        stack.forEach(removeMiddlewares);
                        function removeMiddlewares(layer, i) {
                              if (layer.handle.name === "bound dispatch") {
                                    if (layer.route.path.includes("/posod")) {
                                          remove_route.push(layer);
                                    }
                              }
                        }
                        remove_route.forEach((v, k) => {
                              var idx = stack.indexOf(v);
                              stack.splice(idx, 1);
                        })
                  } catch (error) {
                        RENOBIT.logger.error(error);
                  }
            },
            backup : function() {
                  var promise = new Promise((resolve, reject) => {
                        var result = {};
                        renobit.database(TB_ASSET_EVENT_CODE).select('*').then((rows) => {
                              result[TB_ASSET_EVENT_CODE] = {
                                    schema:SCHEMA,
                                    rows:rows
                              }
                              resolve(result);
                        })
                  })
                  return promise;
            }
      }
}
