const _ = require('lodash');

module.exports = function (renobit) {
    'use strict';
    const TB_ASSET_FAVORITE = 'TB_ASSET_FAVORITE';
    const TB_INSTANCE = 'TB_INSTANCE';
    const SCHEMA = {"asset_id":{"type":"string","defaultValue":null,"maxLength":50,"nullable":false},"asset_name":{"type":"string","defaultValue":null,"maxLength":50,"nullable":false},"interval":{"type":"integer","defaultValue":null,"maxLength":null,"nullable":false},"seq":{"type":"integer","defaultValue":null,"maxLength":null,"nullable":false}};

    const favorite = {
        uninit: function () {
            return renobit.database.schema.dropTableIfExists(TB_ASSET_FAVORITE);
        },
        init: function () {
            var promise = new Promise(function (resolve, reject) {
                renobit.database.schema.hasTable(TB_ASSET_FAVORITE).then(function (exists) {
                    if (!exists) {
                        renobit.database.schema.createTable(TB_ASSET_FAVORITE, function (table) {
                            table.string('asset_id', 50);
                            table.string('asset_name', 50);
                            table.integer('interval');
                            table.integer('seq')
                        }).then(function (res) {
                            resolve();
                        }).catch(function (err) {
                            reject(err);
                        })
                    } else {
                        resolve();
                    }
                })
            });
            return promise;
        },
        selectQuery: function () {
            return renobit.database(TB_ASSET_FAVORITE).join(TB_INSTANCE, TB_INSTANCE + '.ASSET_ID', TB_ASSET_FAVORITE+'.asset_id').select('asset_id as id', 'asset_name as name', 'interval','INST_ID as instance_id', 'PAGE_ID as page_id').orderBy('seq', 'asc');
        },
        insertQuery: function (rows,trx) {
            return renobit.database.batchInsert(TB_ASSET_FAVORITE, rows).transacting(trx)
        },
        deleteQuery: function(trx) {
            return renobit.database(TB_ASSET_FAVORITE).transacting(trx).del();
        }
    }

    function ensureAuth(req, res, next) {
        if (req.isAuthenticated()) {
            next();
        } else {
            res.status(401).send({code:'AUTH_001'});
        }
    }

    return {
        init: function () {
            return favorite.init();
        },
        uninit: function () {
            favorite.uninit().then(() => {
                console.log("favorite table drop success");
            });
        },
        regist: function () {
            renobit.app.get("/favorite", ensureAuth, function (req, res, next) {
                favorite.selectQuery().then((rows) => {
                    res.status(200).send(rows);
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });

            renobit.app.post("/favorite", ensureAuth, function (req, res, next) {
                renobit.database.transaction(function(trx){
                    favorite.deleteQuery(trx).then(() => {
                        var insert_rows = [];
                        _.each(req.body, (v,k) => {
                            insert_rows.push({
                                asset_id:v.id,
                                asset_name:v.name,
                                interval:v.interval,
                                seq:v.seq
                            })
                        })
                        favorite.insertQuery(insert_rows, trx).then(trx.commit).catch(trx.rollback);
                    }).catch(trx.rollback);
                }).then(() => {
                    res.status(200).send()
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            })
        },

        unregist: function () {
            try {
                var remove_route = [];
                var stack = renobit.app._router.stack;
                stack.forEach(removeMiddlewares);
                function removeMiddlewares(layer, i) {
                    if (layer.handle.name === "bound dispatch") {
                        if (layer.route.path.includes("/favorite")) {
                            remove_route.push(layer);
                        }
                    }
                }
                remove_route.forEach((v, k) => {
                    var idx = stack.indexOf(v);
                    stack.splice(idx, 1);
                })
            } catch (error) {
                RENOBIT.logger.error(error);
            }
        },
        backup : function() {
            var promise = new Promise((resolve, reject) => {
                var result = {};
                renobit.database(TB_ASSET_FAVORITE).select('*').then((rows) => {
                    result[TB_ASSET_FAVORITE] = {
                        schema:SCHEMA,
                        rows:rows
                    }
                    resolve(result);
                })
            })
            return promise;
        }
    }
}
