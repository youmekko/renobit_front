/*
plugin은 모두 mediator의 view로 등록됨.
 */
class POSODManager extends ExtensionPluginCore {
      constructor() {
            super();
            this._path = wemb.configManager.serverUrl + "/posod.do";
            this._menuData = null;
            this._socketEventList = [];
            this._assetWatcher = null; //순차감시, 자동이동
            this._$bus = new Vue();
            this.lang;
            this._isActiveCCTVPlayer=false;


      }

      $on(event, func) {
            this._$bus.$on(event, func);
      }

      $off(event, func) {
            this._$bus.$off(event, func);
      }

      destroy() {
      }

      start() {
            this.lang = Vue.$i18n.messages.wv.posod_pack;
            /*_instance는 EXtensionPluginCore에서 생성함.*/
            window.wemb.posodManager = POSODManager._instance;


            if (wemb.configManager.exeMode == "editor") {
                  /*에디터 모드에서만 템플릿 등록하기*/
                  POSODComponentPropertyTemplateManager.regist();
            } else {
                  /*viewer에서만 순차감시/자동이등 실행*/
                  this._assetWatcher = new POSODAssetWatcher();
                  this._attachFavoriteData();
                  this._bindSocketData();
            }
      }

      _bindSocketData() {
            if (window.socket) {
                  window.socket.on(POSODManager.POSOD_SOCKET_EVENT, (data) => {
                        if(!window.wemb.pageManager._currentPageInfo.props.page_info.event) {
                              this.posodSocketEventData(data);
                        }
                  })
            }
      }

      get assetWatcher() {
            return this._assetWatcher;
      }

      get notificationList() {
            return [
                  EditorProxy.NOTI_OPEN_PAGE,
                  EditorProxy.NOTI_OPENED_PAGE,
                  AssetComponentControllerPlugin.NOTI_SELECTED_COMPONENT
            ]
      }

      get path() {
            return this._path;
      }

      handleNotification(note) {
            switch (note.name) {
                  case EditorProxy.NOTI_OPENED_PAGE:
                        if (wemb.configManager.exeMode == "viewer") {
                              this._assetWatcher.onLoadedPage();
                        }
                        break;
                  case EditorProxy.NOTI_OPEN_PAGE: //loaded

                        break;
                  case AssetComponentControllerPlugin.NOTI_SELECTED_COMPONENT:
                        //console.log("@@ noti ", AssetComponentControllerPlugin.NOTI_SELECTED_COMPONENT);
            }
      }

      async _attachFavoriteData() {
            try {
                  let response = await this._assetWatcher.startLoading();
            } catch (error) {
                  console.log("error = Favorite data load error", error);
            }
      }


      posodSocketEventData(data) {
            let item = data.filter(x => x.severity == 'critical');
            let fireEventList = data.filter(x => (x.evt_type == 'Fire' || x.asset_type == 'Fire'));
            item = fireEventList.length > 0 ? fireEventList : item; // 이벤트 우선순위 : 화재 > critical 이벤트
            if(item && item.length > 0){
                  this._listenSocketEvent(item[item.length - 1]);
            }

            this._$bus.$emit(POSODManager.REQUEST_EVENT_LIST, data);
      }

      /*
      [ 포소드 소켓 이벤트 발생 시 ]

      1. 순차감시 중지
      2. 자산 타입에 따라 페이지 이동
          - 화재 이벤트   | 자동이동 flag 값과 상관없이 '부지'로 이동 + 화재 애니메이션 + 티커노출
          - 일반 타입     | 자동이동 flag 값 체크하여 해당 페이지로 이동
      3. 자동 이동 기능 해제

      ( 순차감시 기능과 자동이동 기능이 충돌하는 경우, 순차감시 기능 off )
       */
      // TODO : 페이지 트리 패널 이동 또는 3D 더블클릭으로 페이지 이동 시 autoMoveFlag = true 설정
      _listenSocketEvent(data) {
            this._assetWatcher.occursEvent(data);
      }

      requestCCTVPlayList( data ) {
            let requestParams = data.constructor  === Array ? data : [data.asset_id];
            let params = {
                  "id": "assetPosodService.getPlayListFromParents",
                  "params" : {"parents":requestParams, "evt_id" : data.evt_id }
            }

            return CPUtil.getInstance().destructAssignmentPromise(wemb.$http.post(this.path, params ) );
            //return CPUtil.getInstance().destructAssignmentPromise(wemb.$http.get("/posod/playlist?parent_id="+ data.asset_id ));
      }

      requestHistoryData( accessData ){
            let params = {
                  "id": "assetPosodEventService.accessDetailEventList",
                  "params" : accessData
            }

            return CPUtil.getInstance().destructAssignmentPromise(wemb.$http.post(this.path, params));
      }

      requestAccessGridData( params ){
            let paramsData = {
                  "id": "assetPosodEventService.accessEventList",
                  "params" : params
            }

            return CPUtil.getInstance().destructAssignmentPromise(wemb.$http.post(this.path, paramsData));
      }


      /** 브라우저 IE체크 * */
      checkBrowserIE(){
            if(CPUtil.getInstance().browserDetection() != "Internet Explorer"){
                  alert(this.lang.manager.ActiveXMsg01);
                  return false;
            }
            return true;

      }


      async requestRecordQENXPlayer(data) {
            const [err, res] = await this.requestCCTVPlayList(data);
            if(err) {
                  console.log(err);
                  return false;
            }
            this.openRecordQENXViewer({title: "QENXRecordViewer", playList: res.data.playList });
            return true;
      }

      /**
       * 녹화영상을 playlist가 아니라 직접 조합하여 요청해 보여줄 경우 사용
       * channelMask연산은 연관cctv들이 동일한 서버를 가지고 있다는 가정에 적용.
       * */
      async requestNVRQENXPlayer(data){
            const [err, res] = await this.requestCCTVPlayList(data);
            if(err) {
                  console.log(err);
                  return false;
            }
            let nvrList = res.data.playList;
            if(nvrList.length>0){
                  let cctvInfo = nvrList[0];
                  let params = {
                        cameraName        :cctvInfo.cameraName,
                        recordServerUrl   :cctvInfo.recordServerUrl,
                        fromTime          :cctvInfo.fromTime,
                        toTime            :cctvInfo.toTime
                  };
                  let sumMak    = 0;
                  nvrList.forEach( cctvInfo => {
                        sumMak += parseInt(cctvInfo.cameraMask); // 문자열을 숫자로 변경 후 마스크 합을 구함.
                  });
                  sumMak = "0x"+sumMak.toString(16);       // 마스크값을 합하여 요청
                  params.cameraMask = sumMak;
                  params.title      = "QENXNVRViewer";
                  this.openNVRQENXViewer(params);
            }
      }

      /**
       *    params:Object
       *    params.title:string =  타이틀
       *    playList :any       =  플레이 목록 jsonstring
       // 샘플
       {
            title:"record",
            playList:[{
                  cameraName: "cctv-16",
                  recordServerUrl: "qenx://admin:@192.168.1.100:12001",
                  cameraMask: "0x1",
                  fromTime: "2018-11-23 13:11:38",
                  toTime: "2018-11-23 13:12:48"
            },{
                        cameraName: "cctv-01",
                        recordServerUrl: "qenx://admin:@192.168.1.100:12001",
                        cameraMask: "0x2",
                        fromTime: "2018-11-23 13:11:38",
                        toTime: "2018-11-23 13:12:48"
            }]
       }
       * */
      openRecordQENXViewer(params)
      {

            try {
                  let player = POSODManager.QENX_PLAYER_PATH;
                  let fullPath = "C:\\Program Files (x86)/QENXPopViewer/CCTV/";
                  let playFileName = "playList.json";
                  let pathList = fullPath.split('/');
                  let paramPath = "";
                  for(let i=0; i<pathList.length; i++) {
                        paramPath += pathList[i];
                        if(i < pathList.length -1) {
                              paramPath += "\\";
                        }
                  }

                  //options 조합
                  let options = ' --title="' + params.title +'"' + ' --top_most';
                  options += ' --json_url="'+ paramPath + playFileName + '"';

                  let param = new Object();
                  param.playList = params.playList;
                  let jsonText = JSON.stringify(param);
                  let fso = new ActiveXObject("Scripting.FileSystemObject");
                  let path = "";
                  pathList = fullPath.split('/');
                  for(let i=0; i<pathList.length; i++) {
                        path += pathList[i] + "\\";

                        if(!fso.FolderExists(path)) {
                              fso.CreateFolder(path);
                        }
                  }
                  let filename= path + playFileName;

                  let fileObj = fso.CreateTextFile(filename,true);
                  jsonText.charset = "UTF-8";
                  fileObj.WriteLine(jsonText);
                  fileObj.close();

                  let shell = new ActiveXObject("WScript.Shell");
                  shell.run(player + options, 1, false); //player 실행

                  this._isActiveCCTVPlayer=true;
            }  catch(e) {
                  alert(e.message + this.lang.manager.ActiveXMsg02);
            }
      }


      openNVRQENXViewer(params)
      {
            try {
                  let player = POSODManager.QENX_PLAYER_PATH;
                  //options 조합
                  let options = ' --recorder_url=' + params.recordServerUrl + ' --cam_mask='+ params.cameraMask;
                  options += ' --play_from="'+ params.fromTime +'"'+ ' --play_to="'+ params.toTime +'" --title='+params.title+' --size=480,320 --datef=yyy-MM-dd --timef=hhh:mm:ss';
                  let shell = new ActiveXObject("WScript.Shell");
                  shell.run(player + options, 1, false); //player 실행

                  this._isActiveCCTVPlayer=true;
            }  catch(e) {
                  alert(e.message + this.lang.manager.ActiveXMsg02);
            }
      }

      /**
       * //실시간 영상
       *{
            url:"rtsp://admin:!1q2w3e4r@192.168.1.101:554/onvif/profile1/media.smp",
            options:" --top_most --size=480,320 --pos=100,85 --title=test"
       }**/
      openQENXLivePopViewer(params)  {
            if(this.checkBrowserIE()){
                  try {
                        let player = POSODManager.QENX_PLAYER_PATH;
                        let options =" --camera_url="+ params.url +" "+params.options;
                        let shell = new ActiveXObject("WScript.Shell");
                        shell.run(player+options, 1, false); //player 실행
                        this._isActiveCCTVPlayer=true;
                  }  catch(e) {
                        alert(e.message + this.lang.manager.ActiveXMsg02);
                  }
            }
      }






      /**
       *열린 qenx player 닫기
       * */
      closeQENXViewer(){
            if(this._isActiveCCTVPlayer) {
                  if (this.checkBrowserIE()) {
                        let player = POSODManager.QENX_PLAYER_PATH;
                        let options = ' --quit';
                        let shell = new ActiveXObject("WScript.Shell");
                        shell.run(player + options, 1, false); //player 실행

                  }
                  this._isActiveCCTVPlayer = false;
            }
      }

}

POSODManager.POSOD_SOCKET_EVENT = '/event/socket/posod'; // 소켓 이벤트용
POSODManager.REQUEST_EVENT_LIST = '/request/event/list';
POSODManager.PACK_PATH = "custom/packs/posod_pack/components/2D/";

// qenxplayer 설치 경로
POSODManager.QENX_PLAYER_PATH = '"C:\\Program Files (x86)\\QENXPopViewer\\QENXPopViewer.exe"';


class POSODComponentPropertyTemplateManager {
      static regist() {
      }
}

POSODComponentPropertyTemplateManager.init = false;

