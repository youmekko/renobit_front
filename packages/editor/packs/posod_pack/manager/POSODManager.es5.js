"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/*
plugin은 모두 mediator의 view로 등록됨.
 */
var POSODManager =
      /*#__PURE__*/
      function (_ExtensionPluginCore) {
            _inherits(POSODManager, _ExtensionPluginCore);

            function POSODManager() {
                  var _this;

                  _classCallCheck(this, POSODManager);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(POSODManager).call(this));
                  _this._path = wemb.configManager.serverUrl + "/posod.do";
                  _this._menuData = null;
                  _this._socketEventList = [];
                  _this._assetWatcher = null; //순차감시, 자동이동

                  _this._$bus = new Vue();
                  _this.lang;
                  _this._isActiveCCTVPlayer = false;
                  return _this;
            }

            _createClass(POSODManager, [{
                  key: "$on",
                  value: function $on(event, func) {
                        this._$bus.$on(event, func);
                  }
            }, {
                  key: "$off",
                  value: function $off(event, func) {
                        this._$bus.$off(event, func);
                  }
            }, {
                  key: "destroy",
                  value: function destroy() {}
            }, {
                  key: "start",
                  value: function start() {
                        this.lang = Vue.$i18n.messages.wv.posod_pack;
                        /*_instance는 EXtensionPluginCore에서 생성함.*/

                        window.wemb.posodManager = POSODManager._instance;

                        if (wemb.configManager.exeMode == "editor") {
                              /*에디터 모드에서만 템플릿 등록하기*/
                              POSODComponentPropertyTemplateManager.regist();
                        } else {
                              /*viewer에서만 순차감시/자동이등 실행*/
                              this._assetWatcher = new POSODAssetWatcher();

                              this._attachFavoriteData();

                              this._bindSocketData();
                        }
                  }
            }, {
                  key: "_bindSocketData",
                  value: function _bindSocketData() {
                        var _this2 = this;

                        if (window.socket) {
                              window.socket.on(POSODManager.POSOD_SOCKET_EVENT, function (data) {
                                    if (!window.wemb.pageManager._currentPageInfo.props.page_info.event) {
                                          _this2.posodSocketEventData(data);
                                    }
                              });
                        }
                  }
            }, {
                  key: "handleNotification",
                  value: function handleNotification(note) {
                        switch (note.name) {
                              case EditorProxy.NOTI_OPENED_PAGE:
                                    if (wemb.configManager.exeMode == "viewer") {
                                          this._assetWatcher.onLoadedPage();
                                    }

                                    break;

                              case EditorProxy.NOTI_OPEN_PAGE:
                                    //loaded
                                    break;

                              case AssetComponentControllerPlugin.NOTI_SELECTED_COMPONENT: //console.log("@@ noti ", AssetComponentControllerPlugin.NOTI_SELECTED_COMPONENT);

                        }
                  }
            }, {
                  key: "_attachFavoriteData",
                  value: function () {
                        var _attachFavoriteData2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee() {
                                    var response;
                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                          while (1) {
                                                switch (_context.prev = _context.next) {
                                                      case 0:
                                                            _context.prev = 0;
                                                            _context.next = 3;
                                                            return this._assetWatcher.startLoading();

                                                      case 3:
                                                            response = _context.sent;
                                                            _context.next = 9;
                                                            break;

                                                      case 6:
                                                            _context.prev = 6;
                                                            _context.t0 = _context["catch"](0);
                                                            console.log("error = Favorite data load error", _context.t0);

                                                      case 9:
                                                      case "end":
                                                            return _context.stop();
                                                }
                                          }
                                    }, _callee, this, [[0, 6]]);
                              }));

                        function _attachFavoriteData() {
                              return _attachFavoriteData2.apply(this, arguments);
                        }

                        return _attachFavoriteData;
                  }()
            }, {
                  key: "posodSocketEventData",
                  value: function posodSocketEventData(data) {
                        var item = data.filter(function (x) {
                              return x.severity == 'critical';
                        });
                        var fireEventList = data.filter(function (x) {
                              return x.evt_type == 'Fire' || x.asset_type == 'Fire';
                        });
                        item = fireEventList.length > 0 ? fireEventList : item; // 이벤트 우선순위 : 화재 > critical 이벤트

                        if (item && item.length > 0) {
                              this._listenSocketEvent(item[item.length - 1]);
                        }

                        this._$bus.$emit(POSODManager.REQUEST_EVENT_LIST, data);
                  }
                  /*
				  [ 포소드 소켓 이벤트 발생 시 ]
				   1. 순차감시 중지
				  2. 자산 타입에 따라 페이지 이동
					  - 화재 이벤트   | 자동이동 flag 값과 상관없이 '부지'로 이동 + 화재 애니메이션 + 티커노출
					  - 일반 타입     | 자동이동 flag 값 체크하여 해당 페이지로 이동
				  3. 자동 이동 기능 해제
				   ( 순차감시 기능과 자동이동 기능이 충돌하는 경우, 순차감시 기능 off )
				   */
                  // TODO : 페이지 트리 패널 이동 또는 3D 더블클릭으로 페이지 이동 시 autoMoveFlag = true 설정

            }, {
                  key: "_listenSocketEvent",
                  value: function _listenSocketEvent(data) {
                        this._assetWatcher.occursEvent(data);
                  }
            }, {
                  key: "requestCCTVPlayList",
                  value: function requestCCTVPlayList(data) {
                        var requestParams = data.constructor === Array ? data : [data.asset_id];
                        var params = {
                              "id": "assetPosodService.getPlayListFromParents",
                              "params": {
                                    "parents": requestParams
                                    , "evt_id" : data.evt_id 
                              }
                        };
                        return CPUtil.getInstance().destructAssignmentPromise(wemb.$http.post(this.path, params)); //return CPUtil.getInstance().destructAssignmentPromise(wemb.$http.get("/posod/playlist?parent_id="+ data.asset_id ));
                  }
            }, {
                  key: "requestHistoryData",
                  value: function requestHistoryData(accessData) {
                        var params = {
                              "id": "assetPosodEventService.accessDetailEventList",
                              "params": accessData
                        };
                        return CPUtil.getInstance().destructAssignmentPromise(wemb.$http.post(this.path, params));
                  }
            }, {
                  key: "requestAccessGridData",
                  value: function requestAccessGridData(params) {
                        var paramsData = {
                              "id": "assetPosodEventService.accessEventList",
                              "params": params
                        };
                        return CPUtil.getInstance().destructAssignmentPromise(wemb.$http.post(this.path, paramsData));
                  }
                  /** 브라우저 IE체크 * */

            }, {
                  key: "checkBrowserIE",
                  value: function checkBrowserIE() {
                        if (CPUtil.getInstance().browserDetection() != "Internet Explorer") {
                              alert(this.lang.manager.ActiveXMsg01);
                              return false;
                        }

                        return true;
                  }
            }, {
                  key: "requestRecordQENXPlayer",
                  value: function () {
                        var _requestRecordQENXPlayer = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2(data) {
                                    var _ref, _ref2, err, res;

                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            _context2.next = 2;
                                                            return this.requestCCTVPlayList(data);

                                                      case 2:
                                                            _ref = _context2.sent;
                                                            _ref2 = _slicedToArray(_ref, 2);
                                                            err = _ref2[0];
                                                            res = _ref2[1];

                                                            if (!err) {
                                                                  _context2.next = 9;
                                                                  break;
                                                            }

                                                            console.log(err);
                                                            return _context2.abrupt("return", false);

                                                      case 9:
                                                            this.openRecordQENXViewer({
                                                                  title: "QENXRecordViewer",
                                                                  playList: res.data.playList
                                                            });
                                                            return _context2.abrupt("return", true);

                                                      case 11:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2, this);
                              }));

                        function requestRecordQENXPlayer(_x) {
                              return _requestRecordQENXPlayer.apply(this, arguments);
                        }

                        return requestRecordQENXPlayer;
                  }()
                  /**
                   * 녹화영상을 playlist가 아니라 직접 조합하여 요청해 보여줄 경우 사용
                   * channelMask연산은 연관cctv들이 동일한 서버를 가지고 있다는 가정에 적용.
                   * */

            }, {
                  key: "requestNVRQENXPlayer",
                  value: function () {
                        var _requestNVRQENXPlayer = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee3(data) {
                                    var _ref3, _ref4, err, res, nvrList, cctvInfo, params, sumMak;

                                    return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                          while (1) {
                                                switch (_context3.prev = _context3.next) {
                                                      case 0:
                                                            _context3.next = 2;
                                                            return this.requestCCTVPlayList(data);

                                                      case 2:
                                                            _ref3 = _context3.sent;
                                                            _ref4 = _slicedToArray(_ref3, 2);
                                                            err = _ref4[0];
                                                            res = _ref4[1];

                                                            if (!err) {
                                                                  _context3.next = 9;
                                                                  break;
                                                            }

                                                            console.log(err);
                                                            return _context3.abrupt("return", false);

                                                      case 9:
                                                            nvrList = res.data.playList;

                                                            if (nvrList.length > 0) {
                                                                  cctvInfo = nvrList[0];
                                                                  params = {
                                                                        cameraName: cctvInfo.cameraName,
                                                                        recordServerUrl: cctvInfo.recordServerUrl,
                                                                        fromTime: cctvInfo.fromTime,
                                                                        toTime: cctvInfo.toTime
                                                                  };
                                                                  sumMak = 0;
                                                                  nvrList.forEach(function (cctvInfo) {
                                                                        sumMak += parseInt(cctvInfo.cameraMask); // 문자열을 숫자로 변경 후 마스크 합을 구함.
                                                                  });
                                                                  sumMak = "0x" + sumMak.toString(16); // 마스크값을 합하여 요청

                                                                  params.cameraMask = sumMak;
                                                                  params.title = "QENXNVRViewer";
                                                                  this.openNVRQENXViewer(params);
                                                            }

                                                      case 11:
                                                      case "end":
                                                            return _context3.stop();
                                                }
                                          }
                                    }, _callee3, this);
                              }));

                        function requestNVRQENXPlayer(_x2) {
                              return _requestNVRQENXPlayer.apply(this, arguments);
                        }

                        return requestNVRQENXPlayer;
                  }()
                  /**
                   *    params:Object
                   *    params.title:string =  타이틀
                   *    playList :any       =  플레이 목록 jsonstring
                   // 샘플
                   {
          title:"record",
          playList:[{
                cameraName: "cctv-16",
                recordServerUrl: "qenx://admin:@192.168.1.100:12001",
                cameraMask: "0x1",
                fromTime: "2018-11-23 13:11:38",
                toTime: "2018-11-23 13:12:48"
          },{
                      cameraName: "cctv-01",
                      recordServerUrl: "qenx://admin:@192.168.1.100:12001",
                      cameraMask: "0x2",
                      fromTime: "2018-11-23 13:11:38",
                      toTime: "2018-11-23 13:12:48"
          }]
     }
                   * */

            }, {
                  key: "openRecordQENXViewer",
                  value: function openRecordQENXViewer(params) {
                        try {
                              var player = POSODManager.QENX_PLAYER_PATH;
                              var fullPath = "C:\\Program Files (x86)/QENXPopViewer/CCTV/";
                              var playFileName = "playList.json";
                              var pathList = fullPath.split('/');
                              var paramPath = "";

                              for (var i = 0; i < pathList.length; i++) {
                                    paramPath += pathList[i];

                                    if (i < pathList.length - 1) {
                                          paramPath += "\\";
                                    }
                              } //options 조합


                              var options = ' --title="' + params.title + '"' + ' --top_most';
                              options += ' --json_url="' + paramPath + playFileName + '"';
                              var param = new Object();
                              param.playList = params.playList;
                              var jsonText = JSON.stringify(param);
                              var fso = new ActiveXObject("Scripting.FileSystemObject");
                              var path = "";
                              pathList = fullPath.split('/');

                              for (var _i2 = 0; _i2 < pathList.length; _i2++) {
                                    path += pathList[_i2] + "\\";

                                    if (!fso.FolderExists(path)) {
                                          fso.CreateFolder(path);
                                    }
                              }

                              var filename = path + playFileName;
                              var fileObj = fso.CreateTextFile(filename, true);
                              jsonText.charset = "UTF-8";
                              fileObj.WriteLine(jsonText);
                              fileObj.close();
                              var shell = new ActiveXObject("WScript.Shell");
                              shell.run(player + options, 1, false); //player 실행

                              this._isActiveCCTVPlayer = true;
                        } catch (e) {
                              alert(e.message + this.lang.manager.ActiveXMsg02);
                        }
                  }
            }, {
                  key: "openNVRQENXViewer",
                  value: function openNVRQENXViewer(params) {
                        try {
                              var player = POSODManager.QENX_PLAYER_PATH; //options 조합

                              var options = ' --recorder_url=' + params.recordServerUrl + ' --cam_mask=' + params.cameraMask;
                              options += ' --play_from="' + params.fromTime + '"' + ' --play_to="' + params.toTime + '" --title=' + params.title + ' --size=480,320 --datef=yyy-MM-dd --timef=hhh:mm:ss';
                              var shell = new ActiveXObject("WScript.Shell");
                              shell.run(player + options, 1, false); //player 실행

                              this._isActiveCCTVPlayer = true;
                        } catch (e) {
                              alert(e.message + this.lang.manager.ActiveXMsg02);
                        }
                  }
                  /**
                   * //실시간 영상
                   *{
          url:"rtsp://admin:!1q2w3e4r@192.168.1.101:554/onvif/profile1/media.smp",
          options:" --top_most --size=480,320 --pos=100,85 --title=test"
     }**/

            }, {
                  key: "openQENXLivePopViewer",
                  value: function openQENXLivePopViewer(params) {
                        if (this.checkBrowserIE()) {
                              try {
                                    var player = POSODManager.QENX_PLAYER_PATH;
                                    var options = " --camera_url=" + params.url + " " + params.options;
                                    var shell = new ActiveXObject("WScript.Shell");
                                    shell.run(player + options, 1, false); //player 실행

                                    this._isActiveCCTVPlayer = true;
                              } catch (e) {
                                    alert(e.message + this.lang.manager.ActiveXMsg02);
                              }
                        }
                  }
                  /**
                   *열린 qenx player 닫기
                   * */

            }, {
                  key: "closeQENXViewer",
                  value: function closeQENXViewer() {
                        if (this._isActiveCCTVPlayer) {
                              if (this.checkBrowserIE()) {
                                    var player = POSODManager.QENX_PLAYER_PATH;
                                    var options = ' --quit';
                                    var shell = new ActiveXObject("WScript.Shell");
                                    shell.run(player + options, 1, false); //player 실행
                              }

                              this._isActiveCCTVPlayer = false;
                        }
                  }
            }, {
                  key: "assetWatcher",
                  get: function get() {
                        return this._assetWatcher;
                  }
            }, {
                  key: "notificationList",
                  get: function get() {
                        return [EditorProxy.NOTI_OPEN_PAGE, EditorProxy.NOTI_OPENED_PAGE, AssetComponentControllerPlugin.NOTI_SELECTED_COMPONENT];
                  }
            }, {
                  key: "path",
                  get: function get() {
                        return this._path;
                  }
            }]);

            return POSODManager;
      }(ExtensionPluginCore);

POSODManager.POSOD_SOCKET_EVENT = '/event/socket/posod'; // 소켓 이벤트용

POSODManager.REQUEST_EVENT_LIST = '/request/event/list';
POSODManager.PACK_PATH = "custom/packs/posod_pack/components/2D/"; // qenxplayer 설치 경로

POSODManager.QENX_PLAYER_PATH = '"C:\\Program Files (x86)\\QENXPopViewer\\QENXPopViewer.exe"';

var POSODComponentPropertyTemplateManager =
      /*#__PURE__*/
      function () {
            function POSODComponentPropertyTemplateManager() {
                  _classCallCheck(this, POSODComponentPropertyTemplateManager);
            }

            _createClass(POSODComponentPropertyTemplateManager, null, [{
                  key: "regist",
                  value: function regist() {}
            }]);

            return POSODComponentPropertyTemplateManager;
      }();

POSODComponentPropertyTemplateManager.init = false;
