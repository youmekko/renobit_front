"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * 순차감시와 자동이동을 실행하는 객체
 * - 즐겨찾기에 등록된 자산으로 순차적으로 처리한다(즐겨찾기는 별도의 RENOBIT 페이지로 제작되어 Viewer에서 이를 실행하여 설정한다.)
 * 자세한 내용은 POSOD 매뉴얼 문서 참조

 * - 자동이동(자동이동을 on)
 *      on/off 설정 : TopNavigation 컴포넌트에서 자동이동 토글 버튼을 on/off한다.
 *      * 자동이동을 실행하면 자산에서 Critical 이벤트 발생시 해당 자산으로 자동으로 이동한다. 이때 순차감시가 실행 상태이면 이를 멈춘다.
 *      * on을 사용자가 실행하는 시점에 Critical이벤트 중 가장 최신 이벤트가 아직 확인하지 않은 이벤트인 경우(“New” 표시가 있는 경우) 해당 자산으로 바로 이동한다.
 *      * 자동이동이 on상태이나 자동이동을 하지 않는케이스
 *          이미 자동 이동한 상태에서 해당 이벤트를 Ack하지 않은 경우 추가적으로 Critical 이벤트가 발생해도 이동하지 않음.
 *          단, 다른 페이지나 다른 자산으로 이동시 다시 이동함.
 *
 * - 수동이동(자동이동을 off)
 *      자산에 Critical 이벤트가 발생하더라도 이동하지 않음
 *      단, 화재센서에 Critical 이벤트가 발생한 경우 부지페이지로 이동하여 화재가 발생된 건물에 불꽃이 애니메이션을 실행한다.
 *
 * - 순차감시 로직
 *   play/stop 설정 : MainNavigation 컴포넌트에서 토글 버튼을 통해 play/stop 한다.
 *   * 순차감시가 시작되면 즐겨찾기에 등록된 순번대로 자산을 포커스한다. 설정된 시간이 되면 다음 자산으로 다시 이동한다.
 *   * 자동이동의 경우 UI를 이용해서만 on/off되지만 순차감시는 자동이동이나 화재 상태에 따라 강제 멈춤이 실행되므로 MainNavigation 컴포넌트와 연동된다.
 *     이는 POSODAssetWatcher에서 이벤트를 발생시키고 컴포넌트에서 이 이벤트를 듣고 UI상태를 처리한다.
 *   * 이동은 1,2,3실행후 멈춘후 재실행시 이어서 4,5,6번을 실행한다. 다시 1,2,3을 실행하지 않는다. 이를 변경하고 싶으면 _lastWatchId 속성을 초기화 하면 된다.
 *
 *   * 자동으로 멈춤상태가 되는 케이스
 *      - 자동이동이 켜진 상태에서 배치자산의 Critical 이벤트 발생으로 자동이동이 실행된 경우
 *      - 자동이동이 꺼진 상태에서 화재센서 Critical 이벤트 발생으로 부지페이지로 이동한 경우
 */
var POSODAssetWatcher =
      /*#__PURE__*/
      function () {
            function POSODAssetWatcher() {
                  _classCallCheck(this, POSODAssetWatcher);

                  //자동이동, 순차감시
                  this._isEventWatch = false;
                  this._isAutoPlay = false;
                  this._lastWatchId = null; //마지막 순차감시한 자산 ID

                  this._favoriteList = [];
                  this._$bus = new Vue();
                  this._autoMoveEventId = null;
                  this._autoMoveAssetId = null;
                  this._isBindAckEvent = false; //this._autoPageId = null; //자동이동시 이동한 페이지 저장
                  //TODO
                  //사용자 인터랙션시 자동 off
                  //이동중에 이벤트 발생시

                  this._timer;
                  this._tempWatch = {
                        pageId: '',
                        time: 10
                  };
            }

            _createClass(POSODAssetWatcher, [{
                  key: "$emit",
                  value: function $emit(event, param) {
                        this._$bus.$emit(event, param);
                  }
            }, {
                  key: "$on",
                  value: function $on(event, func) {
                        this._$bus.$on(event, func);
                  }
                  /**
                   * 자동이동 on/off
                   * 상태가 변경된 경우 이벤트를 발생하여 컴포넌트와 연동한다.
                   * on실행 시점에 가장 최신의 이벤트가 New 상태이면 곧바로 그 자산으로 이동한다. goToLatestCriticalEventTarget()
                   * */

            }, {
                  key: "_bindAckEvent",

                  /**
                   * 자산이동 pause기능을 해지하는 조건을 체크하기 위한 이벤트 바인딩. 자세한 설명은 상단 주석 확인
                   */
                  value: function _bindAckEvent() {
                        var _this = this;

                        if (this._isBindAckEvent) {
                              return;
                        }

                        this._isBindAckEvent = true; ///이벤트 바인딩 체크
                        //자동이동한 자산 에크 됐는지 체크

                        window.wemb.dcimManager.eventManager.$on(DCIMManager.EVENT_COMPLETE_ACK, function (data) {
                              if (data.ackList.find(function (x) {
                                    return x.evt_id == _this._autoMoveEventId;
                              }) && _this.eventWatch) {
                                    _this._autoMoveEventId = null;
                                    _this._autoMoveAssetId = null;
                              }
                        }); //자산 이동이벤트 발생시 pause 취소

                        window.wemb.$globalBus.$on(AssetComponentProxy.NOTI_SELECTED_ASSET, function (id) {
                              if (id != _this._autoMoveAssetId) {
                                    _this._autoMoveEventId = null;
                                    _this._autoMoveAssetId = null;
                              }
                        });
                  }
                  /**
                   * 사용자 행동이 있을 경우 순차감시를 자동 종료하게 초기에 작성하였으나 요건이 변경되어 이 코드는 사용하지 않음
                   */

            }, {
                  key: "_bindUserActEvent",
                  value: function _bindUserActEvent() {
                        var _this2 = this;

                        this._unbindUserActEvent();

                        $("#app-main").on("click.posodWatcher", function () {
                              _this2.autoPlay = false;
                        });
                  }
                  /**
                   * 사용자 행동이 있을 경우 순차감시를 자동 종료하게 초기에 작성하였으나 요건이 변경되어 이 코드는 사용하지 않음
                   */

            }, {
                  key: "_unbindUserActEvent",
                  value: function _unbindUserActEvent() {
                        $("#app-main").off("click.posodWatcher");
                  }
            }, {
                  key: "_autoPlayStop",
                  value: function _autoPlayStop() {
                        this._clearTempWatch();

                        this._clearTimer();
                  }
            }, {
                  key: "_setTimer",
                  value: function _setTimer(interval) {
                        var _this3 = this;

                        this._clearTimer();

                        this._timer = setTimeout(function () {
                              if (!_this3.autoPlay) {
                                    return;
                              }

                              _this3.next();
                        }, interval * 1000);
                  }
            }, {
                  key: "_clearTempWatch",
                  value: function _clearTempWatch() {
                        this._tempWatch.pageId = '';
                        this._tempWatch.time = '';
                  }
            }, {
                  key: "_clearTimer",
                  value: function _clearTimer() {
                        clearTimeout(this._timer);
                        this._timer = null;
                  }
                  /**
                   * 가장 최신의 critical 이벤트가 new상태인 경우 이동하게하는 메소드
                   */

            }, {
                  key: "goToLatestCriticalEventTarget",
                  value: function goToLatestCriticalEventTarget() {
                        var event;

                        try {
                              event = window.wemb.dcimManager.eventManager.eventBrowserList.critical[0];

                              if (event.confirm != 'N') {
                                    event = null;
                              }
                        } catch (error) {
                              console.log(error);
                        }

                        if (event && event.asset_id) {
                              this.occursCriticalEvent(event.asset_id, event);
                        }
                  }
                  /**
                   * 순차감시를 실행하는 메소드, 저장된 마지막 순차감시 순번을 체크해 그 다음 감시를 실행한다.
                   */

            }, {
                  key: "next",
                  value: function next() {
                        var _this4 = this;

                        if (!this._favoriteList.length) {
                              //즐겨 찾기 목록이 없는경우 autoplay 종료
                              this.autoPlay = false;
                              return;
                        }

                        var target;

                        var lastWatchTarget = this._favoriteList.find(function (x) {
                              return x.id == _this4._lastWatchId;
                        });

                        var nextIndex = this._favoriteList.indexOf(lastWatchTarget) + 1; //없을경우 -1+1 = 0번째로 시작

                        if (nextIndex <= this._favoriteList.length - 1) {
                              target = this._favoriteList[nextIndex];
                        } else {
                              target = this._favoriteList[0];
                        }

                        this._tempWatch.pageId = target.page_id;
                        this._tempWatch.time = target.interval;
                        this._lastWatchId = target.id;
                        this.moveToAsset(target.page_id, target.id);
                  }
            }, {
                  key: "startLoading",
                  value: function startLoading() {
                        return this.loadFavoriteData();
                  }
                  /**
                   * 수동이동시 호출됨
                   * 화재이벤트가 발생하면 부지로 이동하고(이동했을때 화재 상태는 해당 페이지에서 처리)
                   * 현재페이지가 부지페이지면 화재처리해주는 메소드를 호출함
                   */

            }, {
                  key: "occursFireEvent",
                  value: function occursFireEvent(assetId, event) {
                        //critical 발생
                        var pageId = this.getAssetInPageId(assetId); //배치항목인지 체크

                        if (!pageId) {
                              return;
                        }

                        var site = window.wemb.dcimManager.pagesetManager.getSite(); //페이지셋에서 부지가 설정되었는지 확인

                        if (site) {
                              this.autoPlay = false;
                              var page = window.wemb.pageManager.getPageInfoBy(site.id); //설정된 부지 페이지 정보가 유효한지 확인

                              if (page) {
                                    if (this.getIsSamePage(page.id)) {
                                          window.wemb.dcimManager.actionController.gotoFireAsset3DComponent(assetId); //현재 부지페이지 일때
                                    } else {
                                          window.wemb.pageManager.openPageById(page.id); //부지페이지가 아니면 부지 페이지로 이동
                                    } //this.updateNewMark(event);

                              }
                        }
                  } //화재 이외의 critical event 발생

            }, {
                  key: "occursCriticalEvent",
                  value: function occursCriticalEvent(assetId, event) {
                        //자동이동이 꺼져있거나, 자동이동한 자산 아이디가 존재하는 경우 return
                        if (!this.eventWatch || this.eventWatch && this._autoMoveEventId) {
                              return;
                        } //critical 발생


                        var pageId = this.getAssetInPageId(assetId); //배치된 자산인지 확인

                        if (pageId) {
                              this.moveToEventTarget(pageId, assetId);
                              this.updateNewMark(event); //해당 자산으로 이동하므로 확인처리(이벤트 브라우저에서 new표기 삭제됨)
                              //이벤트 이동 후 자동이동 잠시 멈춤(사용자 Ack/페이지 이동 후 다시 자동이동 진행 )
                              //_autoMoveEventId 값이 비었을 경우에만 자동이동함

                              this._autoMoveEventId = event.evt_id;
                              this._autoMoveAssetId = assetId;

                              this._bindAckEvent();
                        }
                  }
                  /*이벤트 확인 처리*/

            }, {
                  key: "updateNewMark",
                  value: function updateNewMark(event) {
                        var data = {
                              ackList: [event],
                              confirm: "Y"
                        };
                        window.wemb.dcimManager.eventManager.ackEventData(data);
                  }
                  /**소켓 이벤트 발생시 이 메소드가 호출됨, critical 이벤트일 때 자동/수동이동에 따라 로직처리 상세내용은 상위 주석 확인*/

            }, {
                  key: "occursEvent",
                  value: function occursEvent(eventData) {
                        var event = eventData;

                        if (Array.isArray(eventData)) {
                              //제일 앞에 있는 critical event
                              var criticalEvent = eventData.find(function (x) {
                                    return x.severity == "critical";
                              });

                              if (criticalEvent) {
                                    event = criticalEvent;
                              } else {
                                    event = null;
                              }
                        } else if (event.severity != "critical") {
                              event = null;
                        }

                        if (!event) {
                              return;
                        }

                        var asset = window.wemb.dcimManager.assetManager.assetFlatMap.get(event.asset_id);

                        if (!asset) {
                              return;
                        }

                        if (asset.asset_type === AssetComponentProxy.TYPE.FIRE) {
                              //화재발생
                              if (this.eventWatch) {
                                    //자동이동일 경우 해당 타겟으로 보냄
                                    this.occursCriticalEvent(event.asset_id, event);
                              } else {
                                    //수동이동일 경우 부지로 이동함
                                    this.occursFireEvent(event.asset_id, event);
                              }
                        } else {
                              this.occursCriticalEvent(event.asset_id, event);
                        }
                  }
            }, {
                  key: "moveToEventTarget",
                  value: function moveToEventTarget(pageId, assetId) {
                        //이벤트 이동일경우 autoPlay정지
                        this.autoPlay = false;
                        this.moveToAsset(pageId, assetId);
                  }
                  /*외부에서 호출됨 - pasodManger
                  * 페이지 이동시마다 호출되는 메소드
                  * 자동이동 pause상태를 해지함, 단, 자동이동에 의한 페이지 이동인지 체크하여 처리
                  */

            }, {
                  key: "onLoadedPage",
                  value: function onLoadedPage() {
                        /*if (this._autoPageId && this.getIsSamePage(this._autoPageId)) {
                        } else {
                        //this._autoMoveEventId = null;
                        }*/
                        //페이지 이동이 자동이동인지 아닌지를 체크하여, 자동이동이 아닐경우에만 자동이동 타겟정보 삭제
                        if (this._autoMoveAssetId && this.getIsSamePage(this.getAssetInPageId(this._autoMoveAssetId))) {//
                        } else {
                              this._autoMoveEventId = null;
                              this._autoMoveAssetId = null;
                        }

                        this._checkAutoPlay();
                  }
            }, {
                  key: "_checkAutoPlay",
                  value: function _checkAutoPlay() {
                        if (this.autoPlay) {
                              var currentPageId = wemb.pageManager.currentPageInfo.id;

                              this._setTimer(this._tempWatch.time); //타이머 시작, 타이머는 순차감시 실행시 저장한 주기 정보를 사용


                              this._clearTempWatch();
                        }
                        /* 시나리오 변경 autoPlay는 정지버튼을 누르거나 이벤트 자동이동시에 정지
                        if (this.autoPlay) {
                        let currentPageId = wemb.pageManager.currentPageInfo.id;
                        if (currentPageId == this._tempWatch.pageId) {
                        this._setTimer(this._tempWatch.time); //타이머 시작
                        this._clearTempWatch();
                        } else {
                        //watcher가 이동시킨게 아니라 사용자에 의해 이동했을경우.. autoPlay 정지
                        this.autoPlay = false;
                        }
                        }*/

                  }
            }, {
                  key: "focusAsset",
                  value: function focusAsset(assetId) {
                        window.wemb.dcimManager.actionController.gotoAsset3DComponent(assetId);

                        this._checkAutoPlay();
                  }
            }, {
                  key: "getIsSamePage",
                  value: function getIsSamePage(pageId) {
                        var currentPageId = wemb.pageManager.currentPageInfo.id;
                        return pageId == currentPageId;
                  }
            }, {
                  key: "moveToAsset",
                  value: function moveToAsset(pageId, assetId) {
                        if (!this.getIsSamePage(pageId)) {
                              window.wemb.pageManager.openPageById(pageId, {
                                    assetId: assetId
                              });
                        } else {
                              this.focusAsset(assetId);
                        }
                  }
                  /**
                   * getAssetDataByAssetId 인자로 전달받은 자산 아이디가 배치된 페이지 id를 반환함
                   */

            }, {
                  key: "getAssetInPageId",
                  value: function getAssetInPageId(assetId) {
                        var assetData = window.wemb.dcimManager.assetManager.getAssetDataByAssetId(assetId);
                        var pageId = ''; //assetProxy에서 가져오기

                        if (assetData) {
                              pageId = assetData.page_id;
                        }

                        return pageId;
                  }
                  /**
                   * 즐겨찾기 정보 업데이트
                   * 즐겨찾기를 등록하는 컴포넌트에서(POSODAssetFavoriteComponent) 즐겨찾기 정보 변경시 이 메소드를 호출해줌
                   * 순차감시를 멈추고 마지막 감시 자산 정보를 삭제함
                   */

            }, {
                  key: "updateFavoriteData",
                  value: function updateFavoriteData(list) {
                        this._lastWatchId = null;
                        this._favoriteList = list || [];
                        this.autoPlay = false;
                  }
                  /**
                   * 생성시 즐겨찾기 정보를 로드함
                   */

            }, {
                  key: "loadFavoriteData",
                  value: function loadFavoriteData() {
                        var _this5 = this;

                        var url = window.wemb.configManager.serverUrl + '/posod.do';
                        var paramsData = {
                              "id": "assetFavoriteService.getFavorite",
                              "params": {}
                        };
                        return wemb.$http.post(url, paramsData).then(function (res) {
                              _this5.updateFavoriteData(res.data.data);
                        }); // return http.get(url).then((result) => {

                        /*result = [{
                        "id": "sensor004",
                        "name": "sensor004",
                        "page_id": pageIds[1],
                        "instance_id": "4e47dc09-53d7-4e69-99e5-870bcf6039d1",
                        "interval": 5
                        },
                        {
                        "id": "sensor002",
                        "name": "sensor001",
                        "page_id": pageIds[3],
                        "instance_id": "b5a47b47-be47-4a9a-8c24-f47630726085",
                        "interval": 5
                        },
                        {
                        "id": "sensor003",
                        "name": "sensor003",
                        "page_id": pageIds[3],
                        "instance_id": "bcf96f02-4f7d-48e4-9b63-ffea98894c93",
                        "interval": 5
                        },
                        {
                        "id": "sensor001",
                        "name": "sensor001",
                        "page_id": pageIds[4],
                        "instance_id": "1cbfc97e-7b93-40d2-93ce-92b4c3ef2bcd",
                        "interval": 5
                        },
                        {
                        "id": "sensor005",
                        "name": "sensor005",
                        "page_id": pageIds[5],
                        "instance_id": "1e371909-eb14-4500-bef7-0545449601bc",
                        "interval": 5
                        }
                        ];*/
                        // this.updateFavoriteData(result);
                        // });
                  }
            }, {
                  key: "eventWatch",
                  set: function set(bool) {
                        var isChange = this._isEventWatch != bool;
                        this._isEventWatch = bool; //자동이동한 자산 아이디 체크하던 부분 해지

                        if (!this._isEventWatch) {
                              this._autoMoveEventId = null;
                              this._autoMoveAssetId = null;
                        }

                        if (isChange) {
                              this.$emit(POSODAssetWatcher.CHANGE_AUTO_MOVE_STATE, this._isEventWatch);

                              if (this._isEventWatch) {
                                    this.goToLatestCriticalEventTarget();
                              }
                        }
                  },
                  get: function get() {
                        return this._isEventWatch;
                  }
                  /**
                   * 순차감시 on/off
                   * 순차감시 상태가 변경된 경우 이벤트를 발생하여 컴포넌트와 연동한다.
                   * 실행시 곧바로 즐겨찾기 등록된 자산으로 이동한다.
                   **/

            }, {
                  key: "autoPlay",
                  set: function set(bool) {
                        var isChange = this._isAutoPlay != bool;
                        this._isAutoPlay = bool;

                        if (isChange) {
                              this.$emit(POSODAssetWatcher.CHANGE_ASSET_WATCH_STATE, this._isAutoPlay);
                        }

                        if (bool) {
                              //this._bindUserActEvent();
                              this.next();
                        } else {
                              //this._unbindUserActEvent();
                              this._autoPlayStop();
                        }
                  },
                  get: function get() {
                        return this._isAutoPlay;
                  }
            }]);

            return POSODAssetWatcher;
      }();

POSODAssetWatcher.CHANGE_ASSET_WATCH_STATE = "event/posod/changeAssetWatchState";
POSODAssetWatcher.CHANGE_AUTO_MOVE_STATE = "event/posod/changeAutoMoveState";
