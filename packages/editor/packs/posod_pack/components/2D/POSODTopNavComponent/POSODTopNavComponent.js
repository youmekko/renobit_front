class POSODTopNavComponent extends WVDOMComponent {
      constructor() {
            super();
            this.$favoriteBtn;
            this.$editorBtn;
            this.$helpBtn;
            this.$logoutBtn;
            this.$switch;

            this.favoriteType;
            this.favoritePage;
            this.favoritePopupOption;

            this.helpType;
            this.helpPage;
            this.helpPopupOption;
            this.lang;
      }

      onLoadPage() {

      }


      _onCreateElement() {

            this.lang = Vue.$i18n.messages.wv.posod_pack;

            let $el = $(this._element);
            let temp = `<div class='comp-wrap'>                             
                              <label class="switch">
                                    <input type="checkbox" checked>
                                    <span class="slider round"></span>
                              </label>
                              <span class="switch-text">` + this.lang.common.autoMove + `</span>
                              
                              <div class="img-btn favorite-btn" title=` + this.lang.topNav.favorite + `>
                                    <img class="up" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/btn_favorite.png">
                                    <img class="hover" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/btn_favorite_select.png">
                              </div>
                              <div class="img-btn editor-btn" title=` + this.lang.topNav.editor + `>
                                    <img class="up" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/btn_editor.png">
                                    <img class="hover" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/btn_editor_select.png">
                              </div>
                              <div class="img-btn help-btn" title=` + this.lang.topNav.help + `>
                                    <img class="up" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/btn_help.png">
                                    <img class="hover" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/btn_help_select.png">      
                              </div>
                              <div class="img-btn logout-btn" title=` + this.lang.topNav.logout + `>
                                    <img class="up" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/btn_logout.png">
                                    <img class="hover" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/btn_logout_select.png">
                              </div>
                        </div>`;

            $el.append(temp);

            this.$favoriteBtn = $el.find(".favorite-btn");
            this.$editorBtn = $el.find(".editor-btn");
            this.$helpBtn = $el.find(".help-btn");
            this.$logoutBtn = $el.find(".logout-btn");

            this.$switch = $el.find(".switch>input");


            if (!window.wemb.userInfo.hasAdminRole) {
                  this.$editorBtn.hide();
            }

            if (this.isViewerMode === true) {
                  if (window.wemb.posodManager.assetWatcher.eventWatch) {
                        this.$switch.prop("checked", true);
                  } else {
                        this.$switch.prop("checked", false);
                  }

                  this._bindEvent();
                  this._bindFavoriteBtnEvent();
                  this._bindHelpBtnEvent();
            }
      }


      _bindEvent() {
            this.$switch.on('change', () => {
                  window.wemb.posodManager.assetWatcher.eventWatch = this.$switch.prop("checked");
            });

            this.$editorBtn.on("click", () => {
                  window.wemb.gotoEditor()
            });

            this.$logoutBtn.on("click", () => {
                  window.wemb.logout()
            });

            let watcher = window.wemb.posodManager.assetWatcher;
            watcher.$on(POSODAssetWatcher.CHANGE_AUTO_MOVE_STATE, (state) => {
                  this.$switch.prop("checked", state);
            })
      }

      _bindFavoriteBtnEvent() {
            this._validateFavorite();

            this.$favoriteBtn.on("click", () => {
                  if (this.favoritePage !== "") {
                        if (this.favoriteType === 'link') {
                              wemb.pageManager.openPageById(this.favoritePage);
                        } else {
                              let pageName = wemb.pageManager.getPageNameById(this.favoritePage);
                              if (this.favoritePopupOption !== "") {
                                    let popupOption = JSON.parse(this.favoritePopupOption);
                                    wemb.popupManager.open(pageName, popupOption);
                              }
                        }
                  }
            });

      }

      _validateFavorite() {
            this.favoriteType = this.getGroupPropertyValue("favorite", "selectType");
            this.favoritePage = this.getGroupPropertyValue("favorite", "selectPage");
            this.favoritePopupOption = this.getGroupPropertyValue("favorite", "popupOption");
      }


      _bindHelpBtnEvent() {
            this._validateHelp();

            this.$helpBtn.on("click", () => {
                  if (this.helpPage !== "") {
                        if (this.helpType === 'link') {
                              wemb.pageManager.openPageById(this.helpPage);
                        } else {
                              let pageName = wemb.pageManager.getPageNameById(this.helpPage);
                              if (this.helpPopupOption !== "") {
                                    let popupOption = JSON.parse(this.helpPopupOption);
                                    wemb.popupManager.open(pageName, popupOption);
                              }
                        }
                  }
            });

      }

      _validateHelp() {
            this.helpType = this.getGroupPropertyValue("help", "selectType");
            this.helpPage = this.getGroupPropertyValue("help", "selectPage");
            this.helpPopupOption = this.getGroupPropertyValue("help", "popupOption");

      }


      _onDestroy() {
            this.$favoriteBtn.remove();
            this.$editorBtn.remove();
            this.$helpBtn.remove();
            this.$logoutBtn.remove();
            this.$switch.remove();

            this.$favoriteBtn = null;
            this.$editorBtn = null;
            this.$helpBtn = null;
            this.$logoutBtn = null;
            this.$switch = null;

            this.favoriteType = null;
            this.favoritePage = null;
            this.favoritePopupOption = null;

            this.helpType = null;
            this.helpPage = null;
            this.helpPopupOption = null;

            super._onDestroy();
      }

}


//기본 프로퍼티 정보
WVPropertyManager.attach_default_component_infos(POSODTopNavComponent, {
      "info": {
            "componentName": "POSODTopNavComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 242,
            "height": 26
      },
      "favorite": {
            "selectType": "link",
            "selectPage": "",
            "popupOption": ""
      },
      "help": {
            "selectType": "link",
            "selectPage": "",
            "popupOption": ""
      }

});


POSODTopNavComponent.property_panel_info = [{
      template: "primary",
}, {
      template: "pos-size-2d",
}, {
      template: "page-select-input",
      owner: "favorite",
      label: "Favorite"
}, {
      template: "page-select-input",
      owner: "help",
      label: "Help"
}];


WVPropertyManager.add_event(POSODTopNavComponent, {
      name: "select",
      label: "Button",
      description: "Button select event",
      properties: [{
            name: "name",
            type: "string",
            default: "",
            description: "Select favorite button"
      }]
});
