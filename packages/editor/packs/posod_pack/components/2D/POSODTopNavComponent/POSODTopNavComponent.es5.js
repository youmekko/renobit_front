"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var POSODTopNavComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(POSODTopNavComponent, _WVDOMComponent);

            function POSODTopNavComponent() {
                  var _this;

                  _classCallCheck(this, POSODTopNavComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(POSODTopNavComponent).call(this));
                  _this.$favoriteBtn;
                  _this.$editorBtn;
                  _this.$helpBtn;
                  _this.$logoutBtn;
                  _this.$switch;
                  _this.favoriteType;
                  _this.favoritePage;
                  _this.favoritePopupOption;
                  _this.helpType;
                  _this.helpPage;
                  _this.helpPopupOption;
                  _this.lang;
                  return _this;
            }

            _createClass(POSODTopNavComponent, [{
                  key: "onLoadPage",
                  value: function onLoadPage() {}
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this.lang = Vue.$i18n.messages.wv.posod_pack;
                        var $el = $(this._element);
                        var temp = "<div class='comp-wrap'>                             \n                              <label class=\"switch\">\n                                    <input type=\"checkbox\" checked>\n                                    <span class=\"slider round\"></span>\n                              </label>\n                              <span class=\"switch-text\">" + this.lang.common.autoMove + "</span>\n                              \n                              <div class=\"img-btn favorite-btn\" title=" + this.lang.topNav.favorite + ">\n                                    <img class=\"up\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/btn_favorite.png\">\n                                    <img class=\"hover\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/btn_favorite_select.png\">\n                              </div>\n                              <div class=\"img-btn editor-btn\" title=" + this.lang.topNav.editor + ">\n                                    <img class=\"up\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/btn_editor.png\">\n                                    <img class=\"hover\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/btn_editor_select.png\">\n                              </div>\n                              <div class=\"img-btn help-btn\" title=" + this.lang.topNav.help + ">\n                                    <img class=\"up\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/btn_help.png\">\n                                    <img class=\"hover\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/btn_help_select.png\">      \n                              </div>\n                              <div class=\"img-btn logout-btn\" title=" + this.lang.topNav.logout + ">\n                                    <img class=\"up\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/btn_logout.png\">\n                                    <img class=\"hover\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/btn_logout_select.png\">\n                              </div>\n                        </div>";
                        $el.append(temp);
                        this.$favoriteBtn = $el.find(".favorite-btn");
                        this.$editorBtn = $el.find(".editor-btn");
                        this.$helpBtn = $el.find(".help-btn");
                        this.$logoutBtn = $el.find(".logout-btn");
                        this.$switch = $el.find(".switch>input");

                        if (!window.wemb.userInfo.hasAdminRole) {
                              this.$editorBtn.hide();
                        }

                        if (this.isViewerMode === true) {
                              if (window.wemb.posodManager.assetWatcher.eventWatch) {
                                    this.$switch.prop("checked", true);
                              } else {
                                    this.$switch.prop("checked", false);
                              }

                              this._bindEvent();

                              this._bindFavoriteBtnEvent();

                              this._bindHelpBtnEvent();
                        }
                  }
            }, {
                  key: "_bindEvent",
                  value: function _bindEvent() {
                        var _this2 = this;

                        this.$switch.on('change', function () {
                              window.wemb.posodManager.assetWatcher.eventWatch = _this2.$switch.prop("checked");
                        });
                        this.$editorBtn.on("click", function () {
                              window.wemb.gotoEditor();
                        });
                        this.$logoutBtn.on("click", function () {
                              window.wemb.logout();
                        });
                        var watcher = window.wemb.posodManager.assetWatcher;
                        watcher.$on(POSODAssetWatcher.CHANGE_AUTO_MOVE_STATE, function (state) {
                              _this2.$switch.prop("checked", state);
                        });
                  }
            }, {
                  key: "_bindFavoriteBtnEvent",
                  value: function _bindFavoriteBtnEvent() {
                        var _this3 = this;

                        this._validateFavorite();

                        this.$favoriteBtn.on("click", function () {
                              if (_this3.favoritePage !== "") {
                                    if (_this3.favoriteType === 'link') {
                                          wemb.pageManager.openPageById(_this3.favoritePage);
                                    } else {
                                          var pageName = wemb.pageManager.getPageNameById(_this3.favoritePage);

                                          if (_this3.favoritePopupOption !== "") {
                                                var popupOption = JSON.parse(_this3.favoritePopupOption);
                                                wemb.popupManager.open(pageName, popupOption);
                                          }
                                    }
                              }
                        });
                  }
            }, {
                  key: "_validateFavorite",
                  value: function _validateFavorite() {
                        this.favoriteType = this.getGroupPropertyValue("favorite", "selectType");
                        this.favoritePage = this.getGroupPropertyValue("favorite", "selectPage");
                        this.favoritePopupOption = this.getGroupPropertyValue("favorite", "popupOption");
                  }
            }, {
                  key: "_bindHelpBtnEvent",
                  value: function _bindHelpBtnEvent() {
                        var _this4 = this;

                        this._validateHelp();

                        this.$helpBtn.on("click", function () {
                              if (_this4.helpPage !== "") {
                                    if (_this4.helpType === 'link') {
                                          wemb.pageManager.openPageById(_this4.helpPage);
                                    } else {
                                          var pageName = wemb.pageManager.getPageNameById(_this4.helpPage);

                                          if (_this4.helpPopupOption !== "") {
                                                var popupOption = JSON.parse(_this4.helpPopupOption);
                                                wemb.popupManager.open(pageName, popupOption);
                                          }
                                    }
                              }
                        });
                  }
            }, {
                  key: "_validateHelp",
                  value: function _validateHelp() {
                        this.helpType = this.getGroupPropertyValue("help", "selectType");
                        this.helpPage = this.getGroupPropertyValue("help", "selectPage");
                        this.helpPopupOption = this.getGroupPropertyValue("help", "popupOption");
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.$favoriteBtn.remove();
                        this.$editorBtn.remove();
                        this.$helpBtn.remove();
                        this.$logoutBtn.remove();
                        this.$switch.remove();
                        this.$favoriteBtn = null;
                        this.$editorBtn = null;
                        this.$helpBtn = null;
                        this.$logoutBtn = null;
                        this.$switch = null;
                        this.favoriteType = null;
                        this.favoritePage = null;
                        this.favoritePopupOption = null;
                        this.helpType = null;
                        this.helpPage = null;
                        this.helpPopupOption = null;

                        _get(_getPrototypeOf(POSODTopNavComponent.prototype), "_onDestroy", this).call(this);
                  }
            }]);

            return POSODTopNavComponent;
      }(WVDOMComponent); //기본 프로퍼티 정보


WVPropertyManager.attach_default_component_infos(POSODTopNavComponent, {
      "info": {
            "componentName": "POSODTopNavComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 242,
            "height": 26
      },
      "favorite": {
            "selectType": "link",
            "selectPage": "",
            "popupOption": ""
      },
      "help": {
            "selectType": "link",
            "selectPage": "",
            "popupOption": ""
      }
});
POSODTopNavComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "page-select-input",
      owner: "favorite",
      label: "Favorite"
}, {
      template: "page-select-input",
      owner: "help",
      label: "Help"
}];
WVPropertyManager.add_event(POSODTopNavComponent, {
      name: "select",
      label: "Button",
      description: "Button select event",
      properties: [{
            name: "name",
            type: "string",
            default: "",
            description: "Select favorite button"
      }]
});
