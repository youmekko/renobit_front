class POSODMessageBoxComponent extends WVDOMComponent {

    constructor() {
        super();
    }

    _onDestroy() {
        super._onDestroy();
    }

    _onCreateElement() {
        $(this._element).addClass("posod-msg-box");
        let ele = ''
        if (this.isEditorMode) {
            ele = `
            <div class="cont-wrap">
                  <div class="bg"><div class="msg blink"></div></div>
                  <div class="symbol">
                        <img class="icon" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/symbol.png">
                  </div>
            </div>`;
        } else {
            ele = `
            <div class="cont-wrap">
                  <div class="bg"><div class="msg blink"></div></div>
                  <div class="symbol">
                        <div class="effect pinkBg" >
                              <span class="ripple pinkBg"></span>
                              <span class="ripple pinkBg"></span>
                              <span class="ripple pinkBg"></span>
                        </div>
                        <img class="icon" src="` + POSODManager.PACK_PATH + this.componentName + `/resource/symbol.png">
                  </div>
            </div>`;
        }
        this._element.innerHTML = ele;

        if (!this.isEditorMode) {
            this._setEvent();
        }
    }

    _setEvent() {
        window.wemb.dcimManager.eventManager.$on(DCIMManager.GET_EVENT_LIST, this._listenEvent.bind(this));
        this._listenEvent(window.wemb.dcimManager.eventManager.eventBrowserList);

        // 메세지 박스 클릭시 해당 자산이 배치된 페이지로 이동
        $(this._element).find(".cont-wrap").click(() => {
            let currentPage = wemb.pageManager.currentPageInfo.id;

            if (this.event) {
                if (this.event.page_id == currentPage) {
                    window.wemb.dcimManager.actionController.gotoAsset3DComponent(this.event.asset_id);
                } else {
                    window.wemb.pageManager.openPageById(this.event.page_id, { assetId: this.event.asset_id });
                }

                  // NEW mark 제거
                  let data = {ackList: [this.event], confirm: "Y"};
                  window.wemb.dcimManager.eventManager.procAckEventData(data);
            }
        });
    }

    _listenEvent(data) {
        if (data && data.critical){
            if (data.critical.length > 0) {
                this.event = data.critical[0];
                let $msgbox = $(this._element).find(".msg.blink");
                $msgbox.empty().append(this.event.msg);
                this.visible = true;
            } else {
                this.visible = false;
            }
        }
    }
}

WVPropertyManager.attach_default_component_infos(POSODMessageBoxComponent, {
    "info": {
        "componentName": "POSODMessageBoxComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 600,
        "height": 65
    },
    "label": {
        "label_using": "N",
        "label_text": "POSODMessageBoxComponent"
    }
});

POSODMessageBoxComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "cursor"
}, {
    template: "label"
}]
