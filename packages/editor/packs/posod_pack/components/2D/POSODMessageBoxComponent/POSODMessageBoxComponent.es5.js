"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var POSODMessageBoxComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(POSODMessageBoxComponent, _WVDOMComponent);

            function POSODMessageBoxComponent() {
                  _classCallCheck(this, POSODMessageBoxComponent);

                  return _possibleConstructorReturn(this, _getPrototypeOf(POSODMessageBoxComponent).call(this));
            }

            _createClass(POSODMessageBoxComponent, [{
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        _get(_getPrototypeOf(POSODMessageBoxComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        $(this._element).addClass("posod-msg-box");
                        var ele = '';

                        if (this.isEditorMode) {
                              ele = "\n            <div class=\"cont-wrap\">\n                  <div class=\"bg\"><div class=\"msg blink\"></div></div>\n                  <div class=\"symbol\">\n                        <img class=\"icon\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/symbol.png\">\n                  </div>\n            </div>";
                        } else {
                              ele = "\n            <div class=\"cont-wrap\">\n                  <div class=\"bg\"><div class=\"msg blink\"></div></div>\n                  <div class=\"symbol\">\n                        <div class=\"effect pinkBg\" >\n                              <span class=\"ripple pinkBg\"></span>\n                              <span class=\"ripple pinkBg\"></span>\n                              <span class=\"ripple pinkBg\"></span>\n                        </div>\n                        <img class=\"icon\" src=\"" + POSODManager.PACK_PATH + this.componentName + "/resource/symbol.png\">\n                  </div>\n            </div>";
                        }

                        this._element.innerHTML = ele;

                        if (!this.isEditorMode) {
                              this._setEvent();
                        }
                  }
            }, {
                  key: "_setEvent",
                  value: function _setEvent() {
                        var _this = this;

                        window.wemb.dcimManager.eventManager.$on(DCIMManager.GET_EVENT_LIST, this._listenEvent.bind(this));

                        this._listenEvent(window.wemb.dcimManager.eventManager.eventBrowserList); // 메세지 박스 클릭시 해당 자산이 배치된 페이지로 이동


                        $(this._element).find(".cont-wrap").click(function () {
                              var currentPage = wemb.pageManager.currentPageInfo.id;

                              if (_this.event) {
                                    if (_this.event.page_id == currentPage) {
                                          window.wemb.dcimManager.actionController.gotoAsset3DComponent(_this.event.asset_id);
                                    } else {
                                          window.wemb.pageManager.openPageById(_this.event.page_id, {
                                                assetId: _this.event.asset_id
                                          });
                                    } // NEW mark 제거


                                    var data = {
                                          ackList: [_this.event],
                                          confirm: "Y"
                                    };
                                    window.wemb.dcimManager.eventManager.procAckEventData(data);
                              }
                        });
                  }
            }, {
                  key: "_listenEvent",
                  value: function _listenEvent(data) {
                        if (data && data.critical){
                              if (data.critical.length > 0) {
                                    this.event = data.critical[0];
                                    var $msgbox = $(this._element).find(".msg.blink");
                                    $msgbox.empty().append(this.event.msg);
                                    this.visible = true;
                              } else {
                                    this.visible = false;
                              }
                        }
                  }
            }]);

            return POSODMessageBoxComponent;
      }(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(POSODMessageBoxComponent, {
      "info": {
            "componentName": "POSODMessageBoxComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 600,
            "height": 65
      },
      "label": {
            "label_using": "N",
            "label_text": "POSODMessageBoxComponent"
      }
});
POSODMessageBoxComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}];
