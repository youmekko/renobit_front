class POSODAssetFavoriteComponent extends WVDOMComponent {

      constructor() {
            super();
            this._path = wemb.configManager.serverUrl + "/posod.do";
            this.origin_asset = [];
            this.asset_list = [];
            this.favorite_list = [];
            this.interval_list = [10, 20, 30];
            this.lang;
      }

      onLoadPage() {
            console.log('load page!!')
      }

      _onDestroy() {
            super._onDestroy();
      }

      _onCreateElement() {
            var self = this;

            this.lang = Vue.$i18n.messages.wv.posod_pack;

            console.log("@@@lang", this.lang.common);

            this.$container = $('<div style="width: 100%;height: calc(100%);"></div>')

            this.$asset_table = $('<table class="POSODAssetFavoriteComponent" style="width:100%;"><thead><tr><th>' + this.lang.common.name + '</th></tr></thead></table>');
            this.$asset_tbody = $('<tbody></tbody>');
            this.$asset_table.append(this.$asset_tbody);
            this.$asset_container = $('<div style="display:inline-block;width:45%;height:100%;box-shadow: 3px 3px 6px rgba(0, 0, 0, 0.3);vertical-align:middle;text-align:center;overflow-y:auto;"></div>').append(this.$asset_table);

            this.$unregist_btn = $('<button type="button" class="el-button el-button--primary el-transfer__button"><span><i class="el-icon-arrow-left"></i><!----></span></button>');
            this.$regist_btn = $('<button type="button" class="el-button el-button--primary el-transfer__button"><span><i class="el-icon-arrow-right"></i><!----></span></button>');
            this.$transfer_container = $('<div style="display:inline-block;width:10%;"></div>').append($('<div></div>').append(this.$unregist_btn)).append($('<div></div>').append(this.$regist_btn));

            this.$favorite_table = $('<table class="POSODAssetFavoriteComponent" style="width:100%;"><thead><tr><th>' + this.lang.common.name + '</th><th>' + this.lang.common.cycle + '</th></tr></thead></table>');
            this.$favorite_tbody = $('<tbody></tbody>');
            this.$favorite_table.append(this.$favorite_tbody);
            this.$favorite_container = $('<div style="display:inline-block;width:45%;height:100%;box-shadow: 3px 3px 6px rgba(0, 0, 0, 0.3);vertical-align:middle;text-align:center;overflow-y:auto;"></div>').append(this.$favorite_table);

            this.$container.append(this.$asset_container).append(this.$transfer_container).append(this.$favorite_container);
            $(this._element).append(this.$container);

            if (this.isViewerMode == true) {
                  this.$regist_btn.click(this.registFavorite.bind(this));
                  this.$unregist_btn.click(this.unregistFavorite.bind(this));
            }
            var url = location.origin + "/favorite";
            let paramsData = {
                  "id": "assetFavoriteService.getFavorite",
                  "params" : {}
            }
            wemb.$http.post(this.path, paramsData).then((res) => {
                  console.log(res.data.data);
                  self.favorite_list = res.data.data;
                  self._setFavoriteList();
                  self.asset_init();
                  self.$favorite_tbody.sortable();
            });
      }

      _onImmediateUpdateDisplay() {

      }

      _onCreateProperties() {

      }

      _onCommitProperties() {
            if (this._updatePropertiesMap.has("setter.selected_page")) {
                  this.validateCallLater(this.asset_init);
            }
      }

      _setFavoriteList() {
            this.$favorite_tbody.empty();
            for (var i = 0; i < this.favorite_list.length; i++) {
                  var item = this.favorite_list[i];
                  var row = $('<tr data-id=' + item.id + '></tr>');
                  row.append('<td class="label"><span>' + this.getParentsStr(item.id) + '</span>' + item.name + '</td>');
                  var option_text = '';
                  this.interval_list.forEach(function (value, index) {
                        option_text += ('<option background-color: rgba(0, 0, 0,0) !important;value="' + value + '"' + (value === item.interval ? 'selected' : '') + '>' + value + '</option>');
                  });
                  var interval_selector = $('<select style="color:grey;background-color: rgba(0, 0, 0,0) !important; outline:none;">' + option_text + '</select>');
                  row.append($('<td></td>').append(interval_selector));
                  row.data('favorite', item);
                  row.click(this._handleSelected);
                  interval_selector.change(function (arg, e) {
                        arg.interval = parseInt(e.target.value);
                  }.bind(this, item));
                  this.$favorite_tbody.append(row);
            }
      }

      _setAssetList() {
            for (var i = 0; i < this.asset_list.length; i++) {
                  var item = this.asset_list[i];
                  var row = $('<tr></tr>');
                  row.append('<td>' + item.name + '</td>');
                  row.data('asset', item);
                  this.$asset_tbody.append(row)
                  row.click(this._handleSelected);
            }
      }

      asset_init() {
            console.log('_setAssetList');
            this.asset_list = [];
            this.origin_asset = [];
            this.$asset_tbody.empty();
            let params = {
                  "id": "assetDataService.getAllData",
                  "params" : {
                        "page_id" : (this.selected_page !== "" ? this.selected_page : wemb.pageManager.currentPageInfo.id)
                  }
            }
            wemb.$http.post(this.path, params).then((res) => {
                  var asset_list = res.data.data.result;

                  for (var i = 0; i < Object.keys(asset_list).length; i++) {
                        var asset_type = Object.keys(asset_list)[i];
                        var list = asset_list[asset_type];

                        for (var j = 0; j < list.length; j++) {
                              if (!this.favorite_list.find(function (d) {
                                    return d.id === list[j].id
                              })) {
                                    this.asset_list.push(list[j]);
                              }
                              this.origin_asset.push(list[j]);
                        }
                  }
                  this._setAssetList();
            }).catch((error)=>{
                  console.log("error", error);
            })
      }

      registFavorite() {
            console.log('regist');
            var selected_items = this.$asset_table.find('tr.selected');
            for (var i = 0; i < selected_items.length; i++) {
                  var item = $(selected_items[i]).data('asset');
                  item['interval'] = this.interval;
                  var row = $('<tr data-id=' + item.id + '></tr>');
                  row.append('<td class="label"><span>' + this.getParentsStr(item.id) + "</span>" + item.name + '</td>');
                  var option_text = '';
                  this.interval_list.forEach(function (value, index) {
                        option_text += ('<option background-color: rgba(0, 0, 0,0) !important;value="' + value + '"' + (value === item.interval ? 'selected' : '') + '>' + value + '</option>');
                  });
                  var interval_selector = $('<select style="color:grey;background-color: rgba(0, 0, 0,0) !important; outline:none;">' + option_text + '</select>');
                  row.append($('<td></td>').append(interval_selector));
                  row.data('favorite', item);
                  row.click(this._handleSelected);
                  interval_selector.change(function (arg, e) {
                        arg.interval = parseInt(e.target.value);
                  }.bind(this, item));
                  this.$favorite_tbody.append(row);
                  $(selected_items[i]).remove();
                  this.favorite_list.push(item);
            }
      }

      getParentsStr(assetId) {
            let str = '';
            try {
                  let pageId = wemb.dcimManager.assetManager.getAssetDataByAssetId(assetId).page_id;
                  let parents = wemb.dcimManager.pagesetManager.getParents(pageId);

                  parents.forEach((parent) => {
                        if (parent.type != DCIMPagesetType.SITE) {
                              str += parent.name + ">";
                        }
                  });
            } catch (error) {
                  console.log(error);
            }
            return str;
      }

      saveFavorite() {
            var me = this;
            var url = location.origin + "/favorite";
            var sorted_items = me.$favorite_tbody.sortable("toArray", {attribute: "data-id"});
            var result = [];
            sorted_items.forEach(function (d, i) {
                  var item = me.favorite_list.find(function (favorite) {
                        return favorite.id === d
                  });
                  item["seq"] = i;
                  result.push(item);
            });
            let paramsData = {
                  "id": "assetFavoriteService.setFavorite",
                  "params" : {
                        "list": result
                  }
            }
            wemb.$http.post(this.path, paramsData).then((res) => {
                  Vue.$message(this.lang.common.saveSuccess);
                  window.wemb.posodManager.assetWatcher.updateFavoriteData(me.favorite_list);
            }).catch((err)=>{
                  console.log("favorite asset save failed", err);
            });
      }

      unregistFavorite() {
            console.log('unregist');
            var selected_items = this.$favorite_table.find('tr.selected');
            for (var i = 0; i < selected_items.length; i++) {
                  var item = $(selected_items[i]).data('favorite');
                  if (this.origin_asset.find(function (d) {
                        return item.id === d.id
                  })) {
                        var row = $('<tr></tr>');
                        row.append('<td>' + item.name + '</td>');
                        row.data('asset', item);
                        row.click(this._handleSelected);
                        this.$asset_tbody.append(row);
                  }
                  $(selected_items[i]).remove();

                  var remove_index = this.favorite_list.findIndex(function (params) {
                        return params.id === item.id;
                  });
                  this.favorite_list.splice(remove_index, 1);
                  console.log(this.favorite_list);
            }
            //this.saveFavorite();
      }

      _handleSelected(e) {
            var target = $(e.currentTarget);
            if (target.hasClass('selected')) {
                  target.removeClass('selected');
            } else {
                  target.addClass('selected');
            }
      }

      set interval(value) {
            this._checkUpdateGroupPropertyValue("setter", "interval", value);
      }

      get interval() {
            return this.getGroupPropertyValue("setter", "interval");
      }

      set selected_page(value) {
            this._checkUpdateGroupPropertyValue("setter", "selected_page", value);
      }

      get selected_page() {
            return this.getGroupPropertyValue("setter", "selected_page");
      }

      get path() {
            return this._path;
      }
}

WVPropertyManager.attach_default_component_infos(POSODAssetFavoriteComponent, {
      "info": {
            "componentName": "POSODAssetFavoriteComponent",
            "version": "1.0.0"
      },

      "setter": {
            "selected_page": "",
            "interval": 10
      },

      "label": {
            "label_using": "N",
            "label_text": "Asset Favorite"
      },

      "typeInfo": {
            "typeGroupName": ""
      }
});

POSODAssetFavoriteComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}];
