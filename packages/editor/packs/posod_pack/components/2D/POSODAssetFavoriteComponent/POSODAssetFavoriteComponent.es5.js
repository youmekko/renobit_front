"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var POSODAssetFavoriteComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(POSODAssetFavoriteComponent, _WVDOMComponent);

            function POSODAssetFavoriteComponent() {
                  var _this;

                  _classCallCheck(this, POSODAssetFavoriteComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(POSODAssetFavoriteComponent).call(this));
                  _this._path = wemb.configManager.serverUrl + "/posod.do";
                  _this.origin_asset = [];
                  _this.asset_list = [];
                  _this.favorite_list = [];
                  _this.interval_list = [10, 20, 30];
                  _this.lang;
                  return _this;
            }

            _createClass(POSODAssetFavoriteComponent, [{
                  key: "onLoadPage",
                  value: function onLoadPage() {
                        console.log('load page!!');
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        _get(_getPrototypeOf(POSODAssetFavoriteComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        var self = this;
                        this.lang = Vue.$i18n.messages.wv.posod_pack;
                        console.log("@@@lang", this.lang.common);
                        this.$container = $('<div style="width: 100%;height: calc(100%);"></div>');
                        this.$asset_table = $('<table class="POSODAssetFavoriteComponent" style="width:100%;"><thead><tr><th>' + this.lang.common.name + '</th></tr></thead></table>');
                        this.$asset_tbody = $('<tbody></tbody>');
                        this.$asset_table.append(this.$asset_tbody);
                        this.$asset_container = $('<div style="display:inline-block;width:45%;height:100%;box-shadow: 3px 3px 6px rgba(0, 0, 0, 0.3);vertical-align:middle;text-align:center;overflow-y:auto;"></div>').append(this.$asset_table);
                        this.$unregist_btn = $('<button type="button" class="el-button el-button--primary el-transfer__button"><span><i class="el-icon-arrow-left"></i><!----></span></button>');
                        this.$regist_btn = $('<button type="button" class="el-button el-button--primary el-transfer__button"><span><i class="el-icon-arrow-right"></i><!----></span></button>');
                        this.$transfer_container = $('<div style="display:inline-block;width:10%;"></div>').append($('<div></div>').append(this.$unregist_btn)).append($('<div></div>').append(this.$regist_btn));
                        this.$favorite_table = $('<table class="POSODAssetFavoriteComponent" style="width:100%;"><thead><tr><th>' + this.lang.common.name + '</th><th>' + this.lang.common.cycle + '</th></tr></thead></table>');
                        this.$favorite_tbody = $('<tbody></tbody>');
                        this.$favorite_table.append(this.$favorite_tbody);
                        this.$favorite_container = $('<div style="display:inline-block;width:45%;height:100%;box-shadow: 3px 3px 6px rgba(0, 0, 0, 0.3);vertical-align:middle;text-align:center;overflow-y:auto;"></div>').append(this.$favorite_table);
                        this.$container.append(this.$asset_container).append(this.$transfer_container).append(this.$favorite_container);
                        $(this._element).append(this.$container);

                        if (this.isViewerMode == true) {
                              this.$regist_btn.click(this.registFavorite.bind(this));
                              this.$unregist_btn.click(this.unregistFavorite.bind(this));
                        }

                        var url = location.origin + "/favorite";
                        var paramsData = {
                              "id": "assetFavoriteService.getFavorite",
                              "params": {}
                        };
                        wemb.$http.post(this.path, paramsData).then(function (res) {
                              console.log(res.data.data);
                              self.favorite_list = res.data.data;

                              self._setFavoriteList();

                              self.asset_init();
                              self.$favorite_tbody.sortable();
                        });
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {}
            }, {
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {}
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._updatePropertiesMap.has("setter.selected_page")) {
                              this.validateCallLater(this.asset_init);
                        }
                  }
            }, {
                  key: "_setFavoriteList",
                  value: function _setFavoriteList() {
                        this.$favorite_tbody.empty();

                        for (var i = 0; i < this.favorite_list.length; i++) {
                              var item = this.favorite_list[i];
                              var row = $('<tr data-id=' + item.id + '></tr>');
                              row.append('<td class="label"><span>' + this.getParentsStr(item.id) + '</span>' + item.name + '</td>');
                              var option_text = '';
                              this.interval_list.forEach(function (value, index) {
                                    option_text += '<option background-color: rgba(0, 0, 0,0) !important;value="' + value + '"' + (value === item.interval ? 'selected' : '') + '>' + value + '</option>';
                              });
                              var interval_selector = $('<select style="color:grey;background-color: rgba(0, 0, 0,0) !important; outline:none;">' + option_text + '</select>');
                              row.append($('<td></td>').append(interval_selector));
                              row.data('favorite', item);
                              row.click(this._handleSelected);
                              interval_selector.change(function (arg, e) {
                                    arg.interval = parseInt(e.target.value);
                              }.bind(this, item));
                              this.$favorite_tbody.append(row);
                        }
                  }
            }, {
                  key: "_setAssetList",
                  value: function _setAssetList() {
                        for (var i = 0; i < this.asset_list.length; i++) {
                              var item = this.asset_list[i];
                              var row = $('<tr></tr>');
                              row.append('<td>' + item.name + '</td>');
                              row.data('asset', item);
                              this.$asset_tbody.append(row);
                              row.click(this._handleSelected);
                        }
                  }
            }, {
                  key: "asset_init",
                  value: function asset_init() {
                        var _this2 = this;

                        console.log('_setAssetList');
                        this.asset_list = [];
                        this.origin_asset = [];
                        this.$asset_tbody.empty();
                        var params = {
                              "id": "assetDataService.getAllData",
                              "params": {
                                    "page_id": this.selected_page !== "" ? this.selected_page : wemb.pageManager.currentPageInfo.id
                              }
                        };
                        wemb.$http.post(this.path, params).then(function (res) {
                              var asset_list = res.data.data.result;

                              for (var i = 0; i < Object.keys(asset_list).length; i++) {
                                    var asset_type = Object.keys(asset_list)[i];
                                    var list = asset_list[asset_type];

                                    for (var j = 0; j < list.length; j++) {
                                          if (!_this2.favorite_list.find(function (d) {
                                                return d.id === list[j].id;
                                          })) {
                                                _this2.asset_list.push(list[j]);
                                          }

                                          _this2.origin_asset.push(list[j]);
                                    }
                              }

                              _this2._setAssetList();
                        }).catch(function (error) {
                              console.log("error", error);
                        });
                  }
            }, {
                  key: "registFavorite",
                  value: function registFavorite() {
                        console.log('regist');
                        var selected_items = this.$asset_table.find('tr.selected');

                        for (var i = 0; i < selected_items.length; i++) {
                              var item = $(selected_items[i]).data('asset');
                              item['interval'] = this.interval;
                              var row = $('<tr data-id=' + item.id + '></tr>');
                              row.append('<td class="label"><span>' + this.getParentsStr(item.id) + "</span>" + item.name + '</td>');
                              var option_text = '';
                              this.interval_list.forEach(function (value, index) {
                                    option_text += '<option background-color: rgba(0, 0, 0,0) !important;value="' + value + '"' + (value === item.interval ? 'selected' : '') + '>' + value + '</option>';
                              });
                              var interval_selector = $('<select style="color:grey;background-color: rgba(0, 0, 0,0) !important; outline:none;">' + option_text + '</select>');
                              row.append($('<td></td>').append(interval_selector));
                              row.data('favorite', item);
                              row.click(this._handleSelected);
                              interval_selector.change(function (arg, e) {
                                    arg.interval = parseInt(e.target.value);
                              }.bind(this, item));
                              this.$favorite_tbody.append(row);
                              $(selected_items[i]).remove();
                              this.favorite_list.push(item);
                        }
                  }
            }, {
                  key: "getParentsStr",
                  value: function getParentsStr(assetId) {
                        var str = '';

                        try {
                              var pageId = wemb.dcimManager.assetManager.getAssetDataByAssetId(assetId).page_id;
                              var parents = wemb.dcimManager.pagesetManager.getParents(pageId);
                              parents.forEach(function (parent) {
                                    if (parent.type != DCIMPagesetType.SITE) {
                                          str += parent.name + ">";
                                    }
                              });
                        } catch (error) {
                              console.log(error)
                        }

                        return str;
                  }
            }, {
                  key: "saveFavorite",
                  value: function saveFavorite() {
                        var _this3 = this;

                        var me = this;
                        var url = location.origin + "/favorite";
                        var sorted_items = me.$favorite_tbody.sortable("toArray", {
                              attribute: "data-id"
                        });
                        var result = [];
                        sorted_items.forEach(function (d, i) {
                              var item = me.favorite_list.find(function (favorite) {
                                    return favorite.id === d;
                              });
                              item["seq"] = i;
                              result.push(item);
                        });
                        var paramsData = {
                              "id": "assetFavoriteService.setFavorite",
                              "params": {
                                    list: result
                              }
                        };
                        wemb.$http.post(this.path, paramsData).then(function (res) {
                              Vue.$message(_this3.lang.common.saveSuccess);
                              window.wemb.posodManager.assetWatcher.updateFavoriteData(me.favorite_list);
                        });
                  }
            }, {
                  key: "unregistFavorite",
                  value: function unregistFavorite() {
                        console.log('unregist');
                        var selected_items = this.$favorite_table.find('tr.selected');

                        for (var i = 0; i < selected_items.length; i++) {
                              var item = $(selected_items[i]).data('favorite');

                              if (this.origin_asset.find(function (d) {
                                    return item.id === d.id;
                              })) {
                                    var row = $('<tr></tr>');
                                    row.append('<td>' + item.name + '</td>');
                                    row.data('asset', item);
                                    row.click(this._handleSelected);
                                    this.$asset_tbody.append(row);
                              }

                              $(selected_items[i]).remove();
                              var remove_index = this.favorite_list.findIndex(function (params) {
                                    return params.id === item.id;
                              });
                              this.favorite_list.splice(remove_index, 1);
                              console.log(this.favorite_list);
                        } //this.saveFavorite();

                  }
            }, {
                  key: "_handleSelected",
                  value: function _handleSelected(e) {
                        var target = $(e.currentTarget);

                        if (target.hasClass('selected')) {
                              target.removeClass('selected');
                        } else {
                              target.addClass('selected');
                        }
                  }
            }, {
                  key: "interval",
                  set: function set(value) {
                        this._checkUpdateGroupPropertyValue("setter", "interval", value);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "interval");
                  }
            }, {
                  key: "selected_page",
                  set: function set(value) {
                        this._checkUpdateGroupPropertyValue("setter", "selected_page", value);
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "selected_page");
                  }
            }, {
                  key: "path",
                  get: function get() {
                        return this._path;
                  }
            }]);

            return POSODAssetFavoriteComponent;
      }(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(POSODAssetFavoriteComponent, {
      "info": {
            "componentName": "POSODAssetFavoriteComponent",
            "version": "1.0.0"
      },
      "setter": {
            "selected_page": "",
            "interval": 10
      },
      "label": {
            "label_using": "N",
            "label_text": "Asset Favorite"
      },
      "typeInfo": {
            "typeGroupName": ""
      }
});
POSODAssetFavoriteComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}];
