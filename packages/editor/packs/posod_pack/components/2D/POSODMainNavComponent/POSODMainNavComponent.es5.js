"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var POSODMainNavComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(POSODMainNavComponent, _WVDOMComponent);

            function POSODMainNavComponent() {
                  var _this;

                  _classCallCheck(this, POSODMainNavComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(POSODMainNavComponent).call(this));
                  _this.app = null;
                  return _this;
            }

            _createClass(POSODMainNavComponent, [{
                  key: "onLoadPage",
                  value: function onLoadPage() {}
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this.createVueElement();
                  }
            }, {
                  key: "onOpenPage",
                  value: function onOpenPage() {
                        if (!this.isEditorMode && this.app) {
                              this.app.opendPage();
                        }
                  }
            }, {
                  key: "createVueElement",
                  value: function createVueElement() {
                        var str = "\n                <ul class=\"posod-main-nav-component\">\n                    <template v-if=\"!isEditorMode\">\n                        <li class=\"icon-site\" v-if=\"!isSiteType\"><a href=\"#\" @click.prevent=\"onClickSiteBtn\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_site.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_site_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-dashboard\"><a href=\"#\" @click.prevent=\"onClickDasbboardBtn()\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_dashboard.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_dashboard_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-access\"><a href=\"#\" @click.prevent=\"onClickAccessBtn()\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_access.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_access_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-position\"><a href=\"#\" @click.prevent=\"onClickInitPositionBtn()\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_position.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_position_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-watch\" v-show=\"!assetWatchOn\"><a href=\"#\" @click.prevent.stop=\"onClickAssetWatchToggleBtn\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_asset_watch.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_asset_watch_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-watch\" v-show=\"assetWatchOn\"><a href=\"#\" @click.prevent.stop=\"onClickAssetWatchToggleBtn\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_asset_watch_pause.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_asset_watch_pause.png'\">\n                        </a></li>\n                    </template>\n                    <template v-else>\n                        <li class=\"icon-site\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_site.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_site_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-dashboard\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_dashboard.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_dashboard_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-access\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_access.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_access_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-position\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_position.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_position_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-watch\" v-show=\"!assetWatchOn\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_asset_watch.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_asset_watch_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-watch\" v-show=\"assetWatchOn\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_asset_watch_pause.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_asset_watch_pause.png'\">\n                        </a></li>\n                    </template>\n                </ul>\n            ";
                        var $temp = $(str);
                        var self = this;
                        var isEditorMode = this.isEditorMode;
                        $(this._element).append($temp);
                        var imagePath = POSODManager.PACK_PATH + this.componentName + "/resource/";
                        this.app = new Vue({
                              el: $temp.get(0),
                              data: function data() {
                                    return {
                                          imagePath: imagePath,
                                          comp: self,
                                          isEditorMode: isEditorMode,
                                          assetWatchOn: false,
                                          watcher: null,
                                          pagesetManager: window.wemb.dcimManager.pagesetManager,
                                          isRoomType: true,
                                          isSiteType: false,
                                          isAnimate: false
                                    };
                              },
                              created: function created() {
                                    if (!this.isEditorMode) {
                                          this.watcher = window.wemb.posodManager.assetWatcher;
                                          this.assetWatchOn = this.watcher.autoPlay;
                                          this.bindEvent();
                                          this.opendPage();
                                    }
                              },
                              mounted: function mounted() {},
                              methods: {
                                    bindEvent: function bindEvent() {
                                          var _this2 = this;

                                          this.watcher.$on(POSODAssetWatcher.CHANGE_ASSET_WATCH_STATE, function (state) {
                                                _this2.assetWatchOn = state;
                                          });
                                    },
                                    opendPage: function opendPage() {
                                          if (this.isEditorMode) {
                                                return;
                                          }

                                          var pageId = window.wemb.pageManager.currentPageInfo.id;
                                          var parents = this.pagesetManager.getParents(pageId);
                                          this.isRoomType = parents.length == 4;
                                          var siteData = this.pagesetManager.getSite();
                                          this.isSiteType = siteData && siteData.id == pageId;
                                    },
                                    onClickInitPositionBtn: function onClickInitPositionBtn() {
                                          var _this3 = this;

                                          if (this.isAnimate) {
                                                return;
                                          }

                                          this.isAnimate = true;

                                          try {
                                                window.wemb.dcimManager.actionController.resetController();
                                                this.isAnimate = false;
                                          } catch (error) {
                                                window.wemb.mainControls.transitionFocusInTarget(wemb.mainControls.position0.clone(), wemb.mainControls.target0.clone(), 1, function () {
                                                      _this3.isAnimate = false;
                                                });
                                          }

                                          wemb.$globalBus.$emit(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION); //wemb.viewerFacade.sendNotification(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION);
                                    },
                                    onClickAssetWatchToggleBtn: function onClickAssetWatchToggleBtn() {
                                          this.watcher.autoPlay = !this.assetWatchOn;
                                    },
                                    onClickDasbboardBtn: function onClickDasbboardBtn() {
                                          var dashboardType = self.getGroupPropertyValue("dashboard", "selectType");
                                          var dashboardPage = self.getGroupPropertyValue("dashboard", "selectPage");
                                          var dashboardPopupOption = self.getGroupPropertyValue("dashboard", "popupOption");

                                          if (dashboardPage !== "") {
                                                if (dashboardType === 'link') {
                                                      wemb.pageManager.openPageById(dashboardPage);
                                                } else {
                                                      var pageName = wemb.pageManager.getPageNameById(dashboardPage);
                                                      var options = {};

                                                      if (dashboardPopupOption !== "") {
                                                            try {
                                                                  options = JSON.parse(dashboardPopupOption);
                                                            } catch (error) {}
                                                      }

                                                      wemb.popupManager.open(pageName, options);
                                                }
                                          }
                                    },
                                    onClickAccessBtn: function onClickAccessBtn() {
                                          var accessType = self.getGroupPropertyValue("accessflow", "selectType");
                                          var accessPage = self.getGroupPropertyValue("accessflow", "selectPage");
                                          var accessPopupOption = self.getGroupPropertyValue("accessflow", "popupOption");

                                          if (accessPage !== "") {
                                                if (accessType === 'link') {
                                                      wemb.pageManager.openPageById(accessPage);
                                                } else {
                                                      var pageName = wemb.pageManager.getPageNameById(accessPage);
                                                      var options = {};

                                                      if (accessPopupOption !== "") {
                                                            try {
                                                                  options = JSON.parse(accessPopupOption);
                                                            } catch (error) {}
                                                      }

                                                      wemb.popupManager.open(pageName, options);
                                                }
                                          }
                                    },
                                    onClickSiteBtn: function onClickSiteBtn() {
                                          try {
                                                var siteData = this.pagesetManager.getSite();
                                                var validPageId = window.wemb.pageManager.getPageInfoBy(siteData.id).id;
                                                window.wemb.pageManager.openPageById(validPageId);
                                          } catch (error) {
                                                Vue.$message.error("Check Site information");
                                          }
                                    }
                              }
                        });
                  }
            }]);

            return POSODMainNavComponent;
      }(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(POSODMainNavComponent, {
      "info": {
            "componentName": "POSODMainNavComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 296,
            "height": 64
      },
      "label": {
            "label_using": "N",
            "label_text": "POSODMainNavComponent"
      },
      "dashboard": {
            "selectType": "popup",
            "selectPage": "",
            "popupOption": ""
      },
      "accessflow": {
            "selectType": "link",
            "selectPage": "",
            "popupOption": ""
      }
});
POSODMainNavComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "page-select-input",
      owner: "dashboard",
      label: "Dashboard"
}, {
      template: "page-select-input",
      owner: "accessflow",
      label: "Access Flow"
}]; //  추후 추가 예정

POSODMainNavComponent.method_info = [];
WVPropertyManager.remove_event(POSODMainNavComponent, "dblclick");
WVPropertyManager.remove_event(POSODMainNavComponent, "click"); // 이벤트 정보

WVPropertyManager.add_event(POSODMainNavComponent, {
      name: "select",
      label: "Button click event",
      description: "Button click event",
      properties: [{
            name: "name",
            type: "Number",
            default: "",
            description: "The name of the button you clicked."
      }]
});
