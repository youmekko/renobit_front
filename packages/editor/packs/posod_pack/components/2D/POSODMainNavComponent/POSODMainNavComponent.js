class POSODMainNavComponent extends WVDOMComponent {
    constructor() {
        super();
        this.app = null;
    }

    onLoadPage() {}

    _onCreateElement() {
        this.createVueElement();
    }

    onOpenPage() {
        if (!this.isEditorMode && this.app) {
            this.app.opendPage();
        }
    }

    createVueElement() {
        var str = `
                <ul class="posod-main-nav-component">
                    <template v-if="!isEditorMode">
                        <li class="icon-site" v-if="!isSiteType"><a href="#" @click.prevent="onClickSiteBtn">
                            <img class="up" :src="imagePath + 'icon_site.png'">
                            <img class="hover" :src="imagePath + 'icon_site_hover.png'">
                        </a></li>
                        <li class="icon-dashboard"><a href="#" @click.prevent="onClickDasbboardBtn()">
                            <img class="up" :src="imagePath + 'icon_dashboard.png'">
                            <img class="hover" :src="imagePath + 'icon_dashboard_hover.png'">
                        </a></li>
                        <li class="icon-access"><a href="#" @click.prevent="onClickAccessBtn()">
                            <img class="up" :src="imagePath + 'icon_access.png'">
                            <img class="hover" :src="imagePath + 'icon_access_hover.png'">
                        </a></li>
                        <li class="icon-position"><a href="#" @click.prevent="onClickInitPositionBtn()">
                            <img class="up" :src="imagePath + 'icon_position.png'">
                            <img class="hover" :src="imagePath + 'icon_position_hover.png'">
                        </a></li>
                        <li class="icon-watch" v-show="!assetWatchOn"><a href="#" @click.prevent.stop="onClickAssetWatchToggleBtn">
                            <img class="up" :src="imagePath + 'icon_asset_watch.png'">
                            <img class="hover" :src="imagePath + 'icon_asset_watch_hover.png'">
                        </a></li>
                        <li class="icon-watch" v-show="assetWatchOn"><a href="#" @click.prevent.stop="onClickAssetWatchToggleBtn">
                            <img class="up" :src="imagePath + 'icon_asset_watch_pause.png'">
                            <img class="hover" :src="imagePath + 'icon_asset_watch_pause.png'">
                        </a></li>
                    </template>
                    <template v-else>
                        <li class="icon-site"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_site.png'">
                            <img class="hover" :src="imagePath + 'icon_site_hover.png'">
                        </a></li>
                        <li class="icon-dashboard"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_dashboard.png'">
                            <img class="hover" :src="imagePath + 'icon_dashboard_hover.png'">
                        </a></li>
                        <li class="icon-access"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_access.png'">
                            <img class="hover" :src="imagePath + 'icon_access_hover.png'">
                        </a></li>
                        <li class="icon-position"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_position.png'">
                            <img class="hover" :src="imagePath + 'icon_position_hover.png'">
                        </a></li>
                        <li class="icon-watch" v-show="!assetWatchOn"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_asset_watch.png'">
                            <img class="hover" :src="imagePath + 'icon_asset_watch_hover.png'">
                        </a></li>
                        <li class="icon-watch" v-show="assetWatchOn"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_asset_watch_pause.png'">
                            <img class="hover" :src="imagePath + 'icon_asset_watch_pause.png'">
                        </a></li>
                    </template>
                </ul>
            `;

        let $temp = $(str);
        let self = this;
        let isEditorMode = this.isEditorMode;
        $(this._element).append($temp);

        let imagePath = POSODManager.PACK_PATH + this.componentName + "/resource/";
        this.app = new Vue({
            el: $temp.get(0),
            data: function() {
                return {
                    imagePath: imagePath,
                    comp: self,
                    isEditorMode: isEditorMode,
                    assetWatchOn: false,
                    watcher: null,
                    pagesetManager: window.wemb.dcimManager.pagesetManager,
                    isRoomType: true,
                    isSiteType: false,
                    isAnimate: false,

                }
            },

            created() {
                if (!this.isEditorMode) {
                    this.watcher = window.wemb.posodManager.assetWatcher;
                    this.assetWatchOn = this.watcher.autoPlay;
                    this.bindEvent();
                    this.opendPage();
                }

            },


            mounted: function() {},

            methods: {
                bindEvent() {
                    this.watcher.$on(POSODAssetWatcher.CHANGE_ASSET_WATCH_STATE, (state) => {
                        this.assetWatchOn = state;
                    })
                },

                opendPage() {
                    if (this.isEditorMode) { return; }
                    let pageId = window.wemb.pageManager.currentPageInfo.id;
                    let parents = this.pagesetManager.getParents(pageId);

                    this.isRoomType = parents.length == 4;
                    let siteData = this.pagesetManager.getSite();
                    this.isSiteType = siteData && siteData.id == pageId;
                },

                onClickInitPositionBtn() {
                    if (this.isAnimate) { return; }
                    this.isAnimate = true;

                    try {
                        window.wemb.dcimManager.actionController.resetController();
                        this.isAnimate = false;
                    } catch (error) {
                        window.wemb.mainControls.transitionFocusInTarget(wemb.mainControls.position0.clone(), wemb.mainControls.target0.clone(), 1, () => {
                            this.isAnimate = false;
                        })
                    }
                    wemb.$globalBus.$emit(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION);
                    //wemb.viewerFacade.sendNotification(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION);
                },


                onClickAssetWatchToggleBtn() {
                    this.watcher.autoPlay = !this.assetWatchOn;
                },

                onClickDasbboardBtn() {
                    let dashboardType = self.getGroupPropertyValue("dashboard", "selectType");
                    let dashboardPage = self.getGroupPropertyValue("dashboard", "selectPage");
                    let dashboardPopupOption = self.getGroupPropertyValue("dashboard", "popupOption");

                    if (dashboardPage !== "") {
                        if (dashboardType === 'link') {
                            wemb.pageManager.openPageById(dashboardPage);
                        } else {
                            let pageName = wemb.pageManager.getPageNameById(dashboardPage);
                            let options = {};
                            if (dashboardPopupOption !== "") {
                                try {
                                    options = JSON.parse(dashboardPopupOption);
                                } catch (error) {}
                            }

                            wemb.popupManager.open(pageName, options);
                        }
                    }
                },

                onClickAccessBtn() {
                    let accessType = self.getGroupPropertyValue("accessflow", "selectType");
                    let accessPage = self.getGroupPropertyValue("accessflow", "selectPage");
                    let accessPopupOption = self.getGroupPropertyValue("accessflow", "popupOption");

                    if (accessPage !== "") {
                        if (accessType === 'link') {
                            wemb.pageManager.openPageById(accessPage);
                        } else {
                            let pageName = wemb.pageManager.getPageNameById(accessPage);
                            let options = {};
                            if (accessPopupOption !== "") {
                                try {
                                    options = JSON.parse(accessPopupOption);
                                } catch (error) {}
                            }

                            wemb.popupManager.open(pageName, options);
                        }
                    }
                },

                onClickSiteBtn() {
                    try {
                        let siteData = this.pagesetManager.getSite();
                        let validPageId = window.wemb.pageManager.getPageInfoBy(siteData.id).id;
                        window.wemb.pageManager.openPageById(validPageId);
                    } catch (error) {
                        Vue.$message.error("Check Site information")
                    }
                }
            }
        });
    }

}

WVPropertyManager.attach_default_component_infos(POSODMainNavComponent, {
    "info": {
        "componentName": "POSODMainNavComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 296,
        "height": 64
    },

    "label": {
        "label_using": "N",
        "label_text": "POSODMainNavComponent"
    },
    "dashboard": {
        "selectType": "popup",
        "selectPage": "",
        "popupOption": ""
    },
    "accessflow": {
        "selectType": "link",
        "selectPage": "",
        "popupOption": ""
    }
});

POSODMainNavComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "page-select-input",
    owner: "dashboard",
    label: "Dashboard"
}, {
    template: "page-select-input",
    owner: "accessflow",
    label: "Access Flow"
}];

//  추후 추가 예정
POSODMainNavComponent.method_info = [];
WVPropertyManager.remove_event(POSODMainNavComponent, "dblclick");
WVPropertyManager.remove_event(POSODMainNavComponent, "click");

// 이벤트 정보
WVPropertyManager.add_event(POSODMainNavComponent, {
    name: "select",
    label: "Button click event",
    description: "Button click event",
    properties: [{
        name: "name",
        type: "Number",
        default: "",
        description: "The name of the button you clicked."
    }]
});
