"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var POSODEventBrowserComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(POSODEventBrowserComponent, _WVDOMComponent);

            function POSODEventBrowserComponent() {
                  var _this;

                  _classCallCheck(this, POSODEventBrowserComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(POSODEventBrowserComponent).call(this));
                  _this.isAll = false;
                  _this.lang;
                  return _this;
            }

            _createClass(POSODEventBrowserComponent, [{
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        if (this.app) {
                              this.app.$destroy();
                              this.app = null;
                        }

                        if (this.popup) {
                              this.popup.$destroy();
                              this.popup = null;
                        }

                        if (this.$audio) {
                              this.$audio.get(0).pause();
                        }

                        if (this.$temp) {
                              this.$temp.remove();
                        }

                        _get(_getPrototypeOf(POSODEventBrowserComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this.lang = Vue.$i18n.messages.wv.posod_pack;
                        $(this._element).addClass("posod-event-browser");
                        this.$audio = $('<audio id="evt_browser_audio" loop></audio>');
                        $(this._element).append(this.$audio);
                        var str = "<div class=\"container\">\n                              <div class=\"tool-area\">\n                                    <div class=\"border-label\"><label>{{lang.common.event}}</label>\n                                          <div class=\"btn-sound\" :class=\"{off: !hasSoundEffect}\" @click=\"_btnSoundClick\"></div>\n                                          <div class=\"auto-btn checks\"><input type=\"checkbox\" :id=\"_uid\" @change=\"isAuto = !isAuto;\"> <label :for=\"_uid\">Auto</label>\n                                          </div>\n                                          <div class=\"fold-btn\" @click=\"_btnFoldClick\">\n                                                <div :class=\"{up: isFolded, down: !isFolded}\"></div>\n                                          </div>\n                                    </div>\n                                    <div class=\"ack-btn-area\">\n                                          <div v-for=\"(v, sItem) in severityInfo\" class=\"btn\" :class=\"{active: severityInfo[sItem].active}\" :key=\"sItem\"\n                                                @click=\"_btnSeverityClick(sItem)\">\n                                                <div class=\"bullet\" :class=\"sItem\"></div>\n                                                <div>{{severityInfo[sItem].count}}</div>\n                                          </div>\n                                          \n                                          <div class=\"btn btn-ack\" @click=\"_btnAckClick\">Ack</div>\n                                          <div class=\"btn btn-ack\" @click=\"_btnAllClick\">ALL</div>\n                                    </div>\n                              </div>\n                              <div class=\"grid-area\">\n                                    <div class=\"header\">\n                                          <div class=\"severity\">{{lang.common.class}}</div>\n                                          <div class=\"asset_name\">{{lang.common.equipmentName}}</div>\n                                          <div class=\"stime\">{{lang.common.time}}</div>\n                                          <div class=\"asset_type\">{{lang.common.eventType}}</div>\n                                          <div class=\"dupl_cnt\">{{lang.common.overlap}}</div>\n                                          <div class=\"msg\">{{lang.common.message}}</div>\n                                          <div class=\"is_new\">{{lang.common.new}}</div>\n                                          <div class=\"play\">{{lang.common.video}}</div>\n                                    </div>\n                                    <div style=\"flex:1; overflow: auto;\">\n                                          <div class=\"contents\">\n                                                <div v-for=\"item in dataProvider\" class=\"content\" :data-id=\"item.evt_id\" \n                                                            :data-asset-id=\"item.asset_id\" :key=\"item.evt_id\"\n                                                            @click=\"_contentClick\" @dblclick=\"_contentDbClick\">\n                                                    <div class=\"severity\" :class=\"item.severity\">{{item.severity.substring(0,2).toUpperCase()}}</div>\n                                                    <div class=\"asset_name\">{{item.asset_name}}</div>\n                                                    <div class=\"stime\">{{item.stime}}</div>\n                                                    <div class=\"asset_type\">{{item.asset_type}}</div>\n                                                    <div class=\"dupl_cnt\">{{item.dupl_cnt}}</div>\n                                                    <div class=\"msg\" style=\"text-align: left;\" :data-tooltip=\"item.msg\">{{item.msg}}</div>\n                                                    <div class=\"is_new\" v-if=\"item.confirm == 'N' && (item.dt_stime >= beforeTime)\">NEW</div>\n                                                    <div style=\"width: 40px;\" v-else></div>\n                                                    <div class=\"play\" v-if=\"item.count > 0\" @click=\"_btnPlayClick(item)\"></div>\n                                                    <div style=\"width: 40px;\" v-else></div>\n                                                </div>\n                                                <div v-if=\"!dataProvider || dataProvider.length == 0\" class='no-data'>No data.</div>\n                                          </div>\n                                    </div>\n                              </div>\n                        </div>";
                        var temp = $(str);
                        $(this._element).append(temp);
                        var self = this;
                        this.app = new Vue({
                              el: temp.get(0),
                              data: function data() {
                                    return {
                                          lang: Vue.$i18n.messages.wv.posod_pack,
                                          dataProvider: [],
                                          beforeTime: new Date(),
                                          isAuto: false,
                                          isFolded: false,
                                          hasSoundEffect: true,
                                          foldedHeight: 45,
                                          severityInfo: {
                                                critical: {
                                                      count: 0,
                                                      active: false
                                                },
                                                major: {
                                                      count: 0,
                                                      active: false
                                                },
                                                minor: {
                                                      count: 0,
                                                      active: false
                                                },
                                                warning: {
                                                      count: 0,
                                                      active: false
                                                },
                                                normal: {
                                                      count: 0,
                                                      active: false
                                                }
                                          },
                                          criticalList: []
                                    };
                              },
                              mounted: function mounted() {
                                    if (!self.isEditorMode) {
                                          window.wemb.dcimManager.eventManager.$on(DCIMManager.GET_EVENT_LIST, this.dataProviderHanlder); // 소켓 이벤트 발생시, 이벤트 리스트 요청을 위해.

                                          window.wemb.posodManager.$on(POSODManager.REQUEST_EVENT_LIST, this.requestHandler);

                                          this._setDataProvider(window.wemb.dcimManager.eventManager.eventBrowserList);
                                    }

                                    $(self._element).find(".container").tooltip({
                                          items: ".msg",
                                          show: {
                                                delay: 1000
                                          },
                                          content: function content() {
                                                var msg = $(this).attr("data-tooltip");
                                                return msg;
                                          }
                                    });
                              },
                              created: function created() {
                                    this.dataProviderHanlder = this._setDataProvider.bind(this);
                                    this.requestHandler = this.requestEventData.bind(this);
                              },
                              destroyed: function destroyed() {
                                    window.wemb.dcimManager.eventManager.$off(DCIMManager.GET_EVENT_LIST, this.dataProviderHanlder);
                                    window.wemb.posodManager.$off(POSODManager.REQUEST_EVENT_LIST, this.requestHandler);
                                    this.dataProviderHanlder = null;
                                    this.requestHandler = null;
                                    $(self._element).find(".container").tooltip("destroy");
                              },
                              methods: {
                                    _setDataProvider: function _setDataProvider() {
                                          var _this2 = this;

                                          var event = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
                                          this.beforeTime = new Date().getTime() - 1000 * 60 * 60 * 24;
                                          this.dataProvider = event.list;
                                          this.criticalList = event.critical;
                                          Object.keys(this.severityInfo).map(function (item) {
                                                _this2.severityInfo[item].count = event.count[item] || 0;
                                          }); // auto 여부 판별

                                          if (this.isAuto && this.isFolded) {
                                                this._btnFoldClick();
                                          }

                                          this.soundEffect();
                                    },
                                    requestEventData: function requestEventData(data) {
                                          this.call();
                                    },
                                    call: function call() {
                                          var _this3 = this;

                                          try {
                                                var activeList = Object.keys(this.severityInfo).filter(function (key) {
                                                      return _this3.severityInfo[key].active;
                                                });
                                                window.wemb.dcimManager.eventManager.procEventBrowserData(activeList);
                                          } catch (err) {
                                                CPLogger.log(this.name + " :: " + err);
                                          }
                                    },
                                    _contentClick: function _contentClick(event) {
                                          var $target = $(event.currentTarget);
                                          var event_id = $target.attr("data-id");
                                          var isMulti = event.ctrlKey;

                                          if (isMulti) {
                                                $target.toggleClass('active');
                                          } else {
                                                $(self._element).find(".contents > .active").not("[data-id=" + event_id + "]").removeClass("active");
                                                $target.toggleClass('active');
                                          }
                                    },
                                    _contentDbClick: function _contentDbClick(event) {
                                          try {
                                                var $target = $(event.currentTarget);
                                                var event_id = $target.attr("data-id");
                                                var item = this.dataProvider.find(function (item) {
                                                      return item.evt_id == event_id;
                                                });
                                                var currentPage = wemb.pageManager.currentPageInfo.id;

                                                if (item) {
                                                      if (item.page_id == currentPage) {
                                                            window.wemb.dcimManager.actionController.gotoAsset3DComponent(item.asset_id);
                                                      } else {
                                                            window.wemb.pageManager.openPageById(item.page_id, {
                                                                  assetId: item.asset_id
                                                            });
                                                      } // NEW mark 제거


                                                      var data = {
                                                            ackList: [item],
                                                            confirm: "Y"
                                                      };
                                                      self.ackEvent(data);
                                                }
                                          } catch (err) {
                                                CPLogger.log(this.name + " :: " + err);
                                          }
                                    },
                                    _btnSeverityClick: function _btnSeverityClick(severity) {
                                          this.severityInfo[severity].active = !this.severityInfo[severity].active;
                                          this.call();
                                    },
                                    _btnFoldClick: function _btnFoldClick() {
                                          this.isFolded = !this.isFolded;

                                          if (this.isFolded) {
                                                $(self._element).animate({
                                                      "height": this.foldedHeight,
                                                      "top": self.y + self.height - this.foldedHeight
                                                }).find(".grid-area").animate({
                                                      "padding": 0
                                                });
                                          } else {
                                                $(self._element).animate({
                                                      "height": self.height,
                                                      "top": self.y
                                                }).find(".grid-area").animate({
                                                      "padding": 10
                                                });
                                          }
                                    },
                                    _btnSoundClick: function _btnSoundClick() {
                                          this.hasSoundEffect = !this.hasSoundEffect;
                                          this.soundEffect();
                                    },
                                    _btnAckClick: function _btnAckClick() {
                                          var ackList = $(self._element).find(".content.active");

                                          if (ackList.length > 0 || self.isAll && this.dataProvider.length > 0) {
                                                this.$modal.show('ack-popup');
                                          } else {
                                                Vue.$message({
                                                      message: this.lang.eventBrowser.selectEventToAck,
                                                      type: 'info'
                                                });
                                          }
                                    },
                                    _btnAllClick: function _btnAllClick() {
                                          self.isAll = true;

                                          this._btnAckClick();
                                    },
                                    _btnPlayClick: function _btnPlayClick(item) {
                                          console.log("play click", item);
                                          window.wemb.posodManager.requestNVRQENXPlayer(item);
                                    },
                                    soundEffect: function soundEffect() {
                                          if (this.hasSoundEffect) {
                                                if (this.criticalList && this.criticalList.length > 0) {
                                                      try {
                                                            self.$audio.get(0).src = POSODManager.PACK_PATH + self.componentName + '/resource/' + this.criticalList[0].asset_type + ".mp3";
                                                            self.$audio.get(0).play();
                                                      } catch (err) {
                                                            console.log(err);
                                                      }
                                                } else {
                                                      self.$audio.get(0).pause();
                                                }
                                          } else {
                                                self.$audio.get(0).pause();
                                          }
                                    }
                              }
                        });

                        this._createAckPopup();
                  }
            }, {
                  key: "_createAckPopup",
                  value: function _createAckPopup() {
                        var str = "<modal name=\"ack-popup\"\n                         id=\"posod-ack-popup\"\n                         class=\"ack-popup\"\n                         :height=\"150\"\n                         :width=\"300\"\n                         :click-to-close=\"false\"\n                         @before-open=\"beforeOpen\">\n                    <div class=\"modal-header\">\n                          <h4>Ack Message</h4>\n                          <a class=\"close-modal-btn\" role=\"button\" @click=\"closedPopup\">\n                              <i class=\"el-icon-close\"></i>\n                          </a>\n                    </div>\n                    <div class=\"modal-body\">\n                        <el-input size=\"small\" v-model=\"value\"></el-input>\n                        <div class=\"modal-footer\">\n                            <el-button class=\"bottom-btn\" size=\"small\" @click=\"procAck\">Save</el-button>\n                            <el-button class=\"bottom-btn\" size=\"small\" @click=\"closedPopup\">Cancel</el-button>\n                        </div>\n                    </div>\n                 </modal>";
                        this.$temp = $(str);
                        var self = this;
                        $("body").append(this.$temp);
                        this.popup = new Vue({
                              el: self.$temp.get(0),
                              data: function data() {
                                    return {
                                          value: null
                                    };
                              },
                              methods: {
                                    beforeOpen: function beforeOpen(event) {
                                          this.value = null;
                                    },
                                    closedPopup: function closedPopup() {
                                          this.$modal.hide('ack-popup');
                                          self.isAll = false;
                                    },
                                    procAck: function procAck() {
                                          if (this.value) {
                                                self._procAck(this.value);

                                                this.closedPopup();
                                          } else {
                                                var lang = Vue.$i18n.messages.wv.posod_pack;
                                                Vue.$message({
                                                      message: lang.eventBrowser.AckMessage,
                                                      type: 'info'
                                                });
                                          }
                                    }
                              }
                        });
                  }
            }, {
                  key: "_procAck",
                  value: function _procAck(value) {
                        var _this4 = this;

                        var user_id = window.wemb.configManager.userId;
                        var activeList = $(this._element).find(".content.active");
                        var ackList = [];

                        if (this.isAll) {
                              ackList = this.app.dataProvider;
                        } else {
                              activeList.map(function (i, item) {
                                    var evt_id = $(item).attr("data-id");
                                    ackList.push(_this4.app.dataProvider.find(function (x) {
                                          return x.evt_id == evt_id;
                                    }));
                              });
                        }

                        var item = {
                              ackList: ackList,
                              ack_info: value,
                              ack: 'Y',
                              ack_user: user_id,
                              confirm: "Y"
                        };
                        this.ackEvent(item);
                        this.isAll = false;
                        $(self._element).find(".content").removeClass("active");
                  }
            }, {
                  key: "ackEvent",
                  value: function ackEvent(item) {
                        try {
                              window.wemb.dcimManager.eventManager.procAckEventData(item);
                        } catch (err) {
                              CPLogger.log(this.name + " :: " + err);
                        }
                  }
            }]);

            return POSODEventBrowserComponent;
      }(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(POSODEventBrowserComponent, {
      "info": {
            "componentName": "POSODEventBrowserComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 900,
            "height": 230
      },
      "label": {
            "label_using": "N",
            "label_text": "POSODEventBrowserComponent"
      }
});
POSODEventBrowserComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}];
