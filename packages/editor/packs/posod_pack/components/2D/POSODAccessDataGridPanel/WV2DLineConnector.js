class WV2DLineConnector {

      constructor(){
            this._parent;
            this.assetCompProxies   = [];
            this.connectItems       = [];
            this._dataProvider      = [];
            this._focus = false;
            this.domComponent       = null;
            this.connectAreaElement = null;
            this.connectItemMap     = null;
      }

      setConnectAreaElement( area ){
            this.connectAreaElement = area;
      }

      setDomCompontent( component ){ this.domComponent = component; }

      set focus( value ){ this._focus = value; }
      get focus(){ return this._focus; }

      draw( parent ){
            this._element = document.createElement("canvas");
            this._ctx     = this._element.getContext("2d");
            this._element.width = wemb.mainPageComponent.width;
            this._element.height = wemb.mainPageComponent.height;
            $(parent).append(this._element);
            this._parent = parent;
      }

      getHitItemByPoint(point){
            if(this.connectItems.length > 0 ){
                  let item;
                  for(let i=0; i<this.connectItems.length; i++){
                        item  = this.connectItems[i];
                        if(!item.iconVO) continue;
                        if(this._focus && !item.selected ){
                              // 포커스 상태인데 아이템이 선택 상태가 아니면 예외
                              continue;
                        }
                        if( point.x > item.iconVO.x && point.x < item.iconVO.x + item.iconVO.w &&
                                    point.y > item.iconVO.y && point.y < item.iconVO.y + item.iconVO.h){
                              return item;
                        }
                  }
            }
            return null;
      }


      /**
       objProvider를 통해 2D Grid 엘리먼트 목록이 받음.
       2D 엘리먼트 목록의 속성을 3D컴포넌트 목록에서 조회하여 매칭 목록을 구함.
       */
      set dataProvider( objProvider ){
            if(this._dataProvider != objProvider ){

                  // 현재 연결된 자산 초기화
                  this.connectItems.forEach((item)=>{
                        if(item.selected){ item.selected=false; }
                  });

                  this._dataProvider = objProvider;
                  let calculator = ( source, vo ) => {
                        let result = this.assetCompProxies.filter((item)=>{
                           return item.assetId == vo.assetId;
                        });
                        if(result.length>0){
                              source.set(vo.assetId, result[0]);
                        }
                        return source;
                  }
                  this.connectItemMap = this._dataProvider.reduce(calculator, new Map());
                  this.connectItems   = [...this.connectItemMap.values()];
            }
      }



      get dataProvider(){
            return this._dataProvider;
      }

      render(){

            this._ctx.clearRect(0, 0, this._element.width, this._element.height);
            if(this.connectItems.length > 0 ){

                  let from, to, p1, p2, vo, iconInfo, pos, assetCompProxy, ix, iy, domElement, idx;
                  let connectLine;
                  let rowList = this.domComponent.getGridRowItems();

                  let area    = this.connectAreaElement.getBoundingClientRect(); //{x:this.connectArea.x, y:this.connectArea.y, h:this.connectArea.bottom};
                  if(area.x==undefined){
                        area.x=area.left;
                        area.y=area.top;
                  }
                  let offset = $(this._parent).offset();
                  offset.left*=-1;
                  offset.top*=-1;
                  area.x += offset.left;
                  area.y += offset.top;

                  for( let j=0; j<rowList.length; j++ ) {
                        domElement = rowList[j];
                        idx = $(domElement).data("index");
                        vo  = this._dataProvider[idx];
                        if(this.connectItemMap.has(vo.assetId)){
                              assetCompProxy = this.connectItemMap.get(vo.assetId);
                              if(this._focus){
                                    if(!assetCompProxy.selected) continue;
                              }
                              connectLine = true;
                              iconInfo    = vo.iconInfo;
                              pos         = assetCompProxy.getWorldPosition();
                              from        = ThreeUtil.getThreePositionTo2D(pos, window.wemb.camera, window.wemb.renderer );
                              to   = domElement.getBoundingClientRect();
                              if(to.x == undefined){
                                    to.x = to.left;
                                    to.y = to.top;
                              }

                              to.y += to.height/2; // y값을 중앙으로 이동
                              // parent의 offset반영
                              to.x += (offset.left);
                              to.y += (offset.top);


                              // 영역을 벗어난 값을 제외
                              if( area.y > to.y || (area.y + area.height) < to.y+to.height/2 ){
                                    connectLine = false;
                                    //프록시에 있는 iconVO속성 초기화
                                    //assetCompProxy.iconVO = null;
                              }

                              if(connectLine){
                                    p1   = CPUtil.getInstance().lerp(from,to, 0.1);
                                    p2   = CPUtil.getInstance().lerp(from,to, 0.9);
                                    this._ctx.save();
                                    this._ctx.beginPath();
                                    this._ctx.moveTo( from.x, from.y );
                                    this._ctx.lineTo( p1.x, from.y );
                                    this._ctx.lineTo( p1.x, p1.y);
                                    this._ctx.lineTo( p2.x, p1.y );
                                    this._ctx.lineTo( p2.x, to.y );
                                    this._ctx.lineTo( to.x, to.y );
                                    this._ctx.strokeStyle = vo.color;
                                    this._ctx.lineWidth = 2;
                                    this._ctx.stroke();
                                    this._ctx.closePath();
                                    ix = from.x - iconInfo.width/2;
                                    iy = from.y - iconInfo.height/2;
                                    this._ctx.drawImage( iconInfo.icon, ix, iy, iconInfo.width, iconInfo.height+1);
                                    this._ctx.restore();
                                    assetCompProxy.iconVO = {x:ix, y:iy, w:iconInfo.width, h:iconInfo.height, idx:idx};
                              }

                        }


                  }


            }
      }

      destroy(){
            this._ctx.clearRect(0, 0, this._element.width, this._element.height);
            this._ctx = null;
            this.assetCompProxies.forEach((vo)=>{
                  vo.iconVO = null;
                  vo.destroy();
            });
            this.assetCompProxies = [];
            this.connectItemMap.clear();
            this.connectItems = null;
            this._dataProvider.forEach((vo)=>{ vo = null; });
            $(this._element).off().remove();
            this._element = null;
      }

}
