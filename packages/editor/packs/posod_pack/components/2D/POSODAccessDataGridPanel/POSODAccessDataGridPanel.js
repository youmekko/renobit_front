
class POSODAccessDataGridPanel extends WVDOMComponent {

      constructor(){
            super();
            this.accessDataGrid;
            this.historyDataGrid;
            this.searchForm;
            this.searchInput;
            this.searchRadios;
            this.userFilterButton;
            this.searchButton;
            this.lang;
      }

      _onCreateElement(){

            this.lang = Vue.$i18n.messages.wv.posod_pack;

            let template = '<div class="access-grid-panel">'+
                                    '<div class="label"><span>' + this.lang.common.accessRoadPanel + '</span></div>'+
                                    '<div class="search-layer">'+
                                          '<form accept-charset="utf-8" id= "access-search-form" method = "post">'+
                                                '<fieldset>'+
                                                      '<div class="date-area search-cell"><input type = "text" readonly class="date-picker" name = "date"/></div>'+
                                                      '<div class="user-filter-area search-cell">'+
                                                            '<input type = "radio" name = "user_filter" checked id="none_access_user" value="none_access" /><label for="user_filter">' + this.lang.common.unauthorized + '</label>'+
                                                            '<input type = "radio" name = "user_filter" id="access_user" value="access" /><label for="user_filter">' + this.lang.common.normal + '</label>'+
                                                      '</div>'+
                                                      '<div class="search-area search-cell">'+
                                                            '<input type="text" autocomplete="off" placeholder="search..." disabled name="ipt_search" />'+
                                                            '<button class="btn-search" ><img src="' + POSODManager.PACK_PATH + this.componentName + '/resource/btn_search_off.png"/></button>'+
                                                            '<button class="btn-access-user" disabled ><img src="' + POSODManager.PACK_PATH + this.componentName + '/resource/btn_man_off.png"/></button>'+
                                                      '</div>'+
                                                '</fieldset>'+
                                          '</form>'+
                                    '</div>'+
                                    '<div class="contents-layer">'+
                                          '<div class="access-grid-area"></div>'+
                                          '<h6>' + this.lang.common.accessHistory + '</h6>'+
                                          '<div class="history-grid-area"></div>'+
                                    '</div>'+
                              '</div>';

            $(this._element).append($(template));
            this.accessDataGrid   = new WVDataGrid();
            this.historyDataGrid = new WVDataGrid();
            this.searchForm      = $(this.element).find("#access-search-form");
            this.searchInput     = this.searchForm.find("input[name='ipt_search']");
            this.searchRadios    = this.searchForm.find("input[type='radio']");
            this.userFilterButton = this.searchForm.find(".btn-access-user");
            this.searchButton = this.searchForm.find(".btn-search");

            this.initializeGrid();
      }
            /*
            *  stime: "이벤트 발생시간",
               // 사용자 정보
               extra: {"cardNo":"출입ID | 1901019500022","userName":"출입자 | 홍길동","card":"출입카드 | 1901019500022","name":"이름 | 홍길동","employeNo":"직원번호 | 1901019500022","employeeType":"직원타입 | 내부직원","company":"소속회사 | 위엠비","department":"부서 | R&D센터"},
               seveirty : "등급",
               inst_name: "인스턴스명",
               inst_id: "인스턴스id",
               page_id: "페이지id",
               page_name: "페이지명"
            *
            * */
      initializeGrid(){
            let columnDataProvider = [
                  {label:this.lang.common.class,  field:"severity", itemRenderer:AccessDataGridItemRenderer.SeverityRenderer},
                  {label:this.lang.common.access, field:"asset_name" },
                  {label:this.lang.common.time,  field:"stime", css:{width:100} },
                  {label:this.lang.common.name,  field:"extra", css:{width:100}, itemRenderer:AccessDataGridItemRenderer.UserNameRenderer },
                  {label:this.lang.common.location,  field:"page_name",  css:{width:130}},
                  {label:this.lang.common.cctv, field:"count", itemRenderer:AccessDataGridItemRenderer.CCTVRenderer}
            ];


            this.accessDataGrid.columnProvider = columnDataProvider;

            this.historyDataGrid.columnProvider = [
                  {label:this.lang.common.class,  field:"severity", itemRenderer:AccessDataGridItemRenderer.SeverityRenderer},
                  {label:this.lang.common.access,field:"asset_name"},
                  {label:this.lang.common.time,  field:"stime",  css:{width:100} },
                  {label:this.lang.common.name,  field:"extra", css:{width:80}, itemRenderer:AccessDataGridItemRenderer.UserNameRenderer},
                  {label:this.lang.common.location,  field:"page_name" }
            ];


            this.historyDataGrid.enabled=false;

            this.accessDataGrid.dataProvider = [];
            this.historyDataGrid.dataProvider = [];
            this.accessDataGrid.draw($(this._element).find(".access-grid-area"));
            this.historyDataGrid.draw($(this._element).find(".history-grid-area"));

      }

      getSearchFormValues(){
            return this.searchForm.serializeArray();
      }

      getSearchDateValue(){
            return this.getSearchFormValues()[0];
      }

      getUserFilterValue(){
            return this.getSearchFormValues()[1];
      }

      getSearchInputValue(){
            if(this.searchInput.prop("disabled")){
                  return {value:""};
            }
            return this.getSearchFormValues()[2];
      }



      set width( value ){
            let w = Math.max( value, this.getDefaultProperties().setter.width );
            super.width = w;
      }

      get width(){
            return super.width;
      }

      // transform최소 사이즈
      transformMinimumScale() {
            let ow = this.getDefaultProperties().setter.width;
            return (ow/this._transform.width);
      }


      _validateSize(){
            super._validateSize();
            if(this._parent){
                  this.accessDataGrid._validateSize();
                  this.historyDataGrid._validateSize();
            }
      }


      onLoadPage(){
            //this.initializeGrid();
      }

      _onDestroy() {
            $(this.element).find(".date-picker").off();
            this.searchButton.off();
            this.userFilterButton.off();
            this.searchRadios.off();
            this.searchForm.off();
            this.accessDataGrid.destroy();
            this.historyDataGrid.destroy();
            super._onDestroy();
      }

}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(POSODAccessDataGridPanel, {
      "info": {
            "componentName": "POSODAccessDataGridPanel",
            "version": "1.0.0"
      },
      "setter": {
            "width": 490,
            "height": 600,
      },
      "label": {
            "label_using": "N",
            "label_text": "POSODAccessDataGridPanel"
      },
      "font": {
            "font_type": "inherit"
      }
});

POSODAccessDataGridPanel.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}];
