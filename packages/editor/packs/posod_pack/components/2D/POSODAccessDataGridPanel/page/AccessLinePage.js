class AccessLinePage {

      constructor( accessGridPanel ){
            this.connector  = new WV2DLineConnector();
            this.floors    = [];
            this.iconMap   = new Map();
            this.animateID = null;
            this.animateHandler = this.animate.bind(this);
            this.mouseDownHandler = this.onClickPage.bind(this);
            this.accessGridPanel =  accessGridPanel;
            this.resetAccessGridHandler = this.resetAccessGrid.bind(this);
            this.accessGridItemClickHandler = this.onClickAccessGridItem.bind(this);
            $(this.accessGridPanel.accessDataGrid).on("wvGridItemClick", this.accessGridItemClickHandler );
            wemb.$globalBus.$on( DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION, this.resetAccessGridHandler );
            wemb.mainControls.saveState();
            $(this.accessGridPanel.accessDataGrid._bodyTable).tooltip({
                  items       :"td",
                  content     :function(){
                        let colIndex = $(this).data("columnIndex");
                        let data     = $(this).parent().data("vo");
                        let msg="";
                        switch(colIndex){
                              case 0: msg = data.severity;
                                    break;

                              case 1: msg = data.asset_name;
                                    break;

                              case 2:msg = data.stime;
                                    break;

                              case 3:
                                    for( let prop in data.extra ){
                                          msg += data.extra[prop]+'<br/>';
                                    }
                                    break;

                              case 4: msg = data.page_name
                                    break;
                        }
                        return msg;
                  }
            });

            $(this.accessGridPanel.historyDataGrid).on("wvGridItemClick", (event)=>{
                  console.log("historyDataGrid-Grid Click", event );
            });



            $(this.accessGridPanel.searchForm).find(".date-picker").datepicker({
                  showOn: "button",
                  buttonImage: POSODManager.PACK_PATH + '/POSODAccessDataGridPanel/resource/date-picker.png',
                  buttonImageOnly: true,
                  showOtherMonths: true,
                  /*minDate:"-1M",*/
                  maxDate: "+0",
                  dateFormat:"yy.mm.dd",
                  dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
            }).datepicker("setDate", new Date()).on("change", this.initSearchInput.bind(this) );



            this.accessGridPanel.searchForm.on("submit", (event)=>{
                  event.preventDefault();
                  let searchParams= this.getSearchParams();
                  this.requestAccessGridData(searchParams);
            });

            this.accessGridPanel.searchRadios.on("change",(event)=>{
                  let selectedValue = this.accessGridPanel.getUserFilterValue().value;

                  if(selectedValue == "access"){
                        this.accessGridPanel.searchInput.prop("disabled", false);
                        //this.accessGridPanel.userFilterButton.prop("disabled", false);
                  } else {
                        this.accessGridPanel.searchInput.prop("disabled", true);
                        //this.accessGridPanel.userFilterButton.prop("disabled", true);
                  }
                  //this.accessGridPanel.searchInput.prop("disabled")
                  let params = this.getSearchParams();
                  this.requestAccessGridData(params);
            });

            /* 사용자 그룹버튼 사용안함.
            this.accessGridPanel.userFilterButton.on("click", (event)=>{
                  event.preventDefault();
                  let searchParams= this.getSearchParams();
                  this.requestAccessGridData(searchParams);
            });
            */
            this.accessGridPanel.userFilterButton.prop("disabled", true);
      }

      initSearchInput(){
            //$(this.accessGridPanel.searchForm).find(".date-picker").change();
            this.accessGridPanel.searchInput.val('');
      }

      getSearchParams( useFilter ){
            let strDate = this.accessGridPanel.getSearchDateValue().value;
            strDate = strDate.replace(/\./g,"");
            let isCritical = this.accessGridPanel.searchInput.prop("disabled");
            let searchWord =  this.accessGridPanel.getSearchInputValue().value || "";
            let searchParams={
                  text: isCritical ? "" : searchWord,
                  isCritical:isCritical,
                  s_time:strDate != "" ? strDate+"000000" : "",
                  e_time:strDate != "" ? strDate+"235959" : "",
                  userFilter : useFilter || true
            };
            this.accessGridPanel.searchInput.val('');
            return searchParams;
      }



      errorLog( error ){
            console.warn("##RENOBIT-response-error",  error );
      }

      updateHistoryData( objProvider ){
            this.accessGridPanel.historyDataGrid.dataProvider = objProvider;
            this.changeData();
      }

      validatePlayListFromData( data ){
            let reqList = new Map();
            data.detail.forEach((historyData)=>{
                  if(historyData.count > 0){
                        if(!reqList.has(historyData.asset_id)){
                              reqList.set(historyData.asset_id, true);
                        }
                  }
            });
            return Array.from( reqList.keys() );
      }

       onClickAccessGridItem( event ){
            let data     = event.selectData;
             //선택 처리
            if(event.columnIndex==5){
                  let reqList = this.validatePlayListFromData(data);
                  wemb.posodManager.requestRecordQENXPlayer(reqList);
            } else {
                  //this.requestHistoryData(data);
                  this.updateHistoryData(data.detail);
            }
      }


      async requestHistoryData( accessData ){
            const [error, response] = await wemb.posodManager.requestHistoryData(accessData);

            if(error){
                  this.errorLog(error);
                  return;
            }
            try{
                  this.updateHistoryData(response.data);
            }catch(error){
                  this.errorLog(error);
            }

      }


      // 층 목록 구성
      initFloorList(){
            wemb.mainPageComponent.$threeLayer.forEach((instance)=>{
                  if( instance instanceof FloorComponent){
                        this.floors.push( instance );
                  }
            });
      }

      // 연결 커넥터 구성
      initConnector(){
            this.connector.draw(wemb.mainPageComponent.drawLayer.element);
            $(this.connector._parent.parentNode).on("click", this.mouseDownHandler );
            this.connector.setDomCompontent(this.accessGridPanel.historyDataGrid);
            this.connector.setConnectAreaElement(this.accessGridPanel.historyDataGrid._bodyContainer[0]);
      }

      // 데이터를 로드하는 구간
      composeFloorAssetItem(){

            let requestPageNames=[];
            this.floors.forEach((floor)=>{
                  requestPageNames.push(floor.name);
            });


            function parseInstanceData( instanceList, parent ){
                  let instanceVO;
                  let source;
                  let results =[];
                  for( let idx = 0; idx < instanceList.length; idx++){
                        source = instanceList[idx];
                        if(source.props.setter.data){
                              instanceVO = new MeshInstanceProxy(source);
                              instanceVO.parent = parent;
                              results.push(instanceVO);
                        }
                  }
                  return results;
            }

            return new Promise(async(resolve) => {
                  wemb.dcimManager.getAccessSensor3DModelDataInFloorPage(requestPageNames).then((pageInfos)=>{
                        let instances = [];
                        for( let i=0; i<this.floors.length;i++){
                              let floor =  this.floors[i];
                              let matchPage = pageInfos.filter((page)=>{
                                    return page.pageName == floor.name;
                              });
                              if(matchPage){
                                    instances = instances.concat(parseInstanceData(matchPage[0].data, floor));
                              }
                        }
                        resolve(instances);
                  });
            });

      }

      // 선택 아이템이 있으면 풀고 그리드 초기 상태로 변경
      resetAccessGrid(){
            this.connector.focus = false;
            for(let idx = 0; idx<this.connector.connectItems.length; idx++){
                  let proxy = this.connector.connectItems[idx];
                  if(proxy.selected){
                        this.removeInstanceFromProxy(proxy);
                        this.connector.focus = proxy.selected = false;
                  }
                  proxy.parent.opacity = 100;
            }
            //this.cameraTransitionOut();
      }

      changeEventData( eventInfo ){
            this.floors.forEach( (floor) => { floor.opacity = 50; } );
            // 대상 자산 인스턴스 설정
            this.connector.focus = false;
            this.connector.dataProvider = eventInfo;
            for(let idx = 0; idx<this.connector.connectItems.length; idx++){
                  let item = this.connector.connectItems[idx];
                  item.parent.opacity = 100;
            }
      }


      // 출입 동선에 사용하는 어셋 로드
      async prepareAccessIconAsset(){
            return new Promise(
                  async (resolve)=>{
                        let iconPromises = await Promise.all([
                              this.getIconFromStatus(WV3DAssetComponent.STATE.NORMAL),
                              this.getIconFromStatus(WV3DAssetComponent.STATE.MINOR),
                              this.getIconFromStatus(WV3DAssetComponent.STATE.MAJOR),
                              this.getIconFromStatus(WV3DAssetComponent.STATE.WARNING),
                              this.getIconFromStatus(WV3DAssetComponent.STATE.CRITICAL)
                        ]);
                        iconPromises.forEach(info =>{
                              this.iconMap.set(info.state, info );
                        });
                        resolve();
                  }
            );
      }


      getLineColorFromState( state ){
            switch(state){
                  case WV3DAssetComponent.STATE.NORMAL: return "#3ca0c5";
                        break;
                  case WV3DAssetComponent.STATE.MINOR: return "#e7c33b";
                        break;
                  case WV3DAssetComponent.STATE.MAJOR: return "#cd821d";
                        break;
                  case WV3DAssetComponent.STATE.WARNING: return "#40bf56";
                        break;
                  case WV3DAssetComponent.STATE.CRITICAL: return "#c73f45";
                        break;
                  default: return "#ffffff";
            }
      }

      getIconFromStatus(state){
            let path = "";
            switch(state){
                  case WV3DAssetComponent.STATE.NORMAL:
                  case WV3DAssetComponent.STATE.MINOR:
                  case WV3DAssetComponent.STATE.MAJOR:
                  case WV3DAssetComponent.STATE.WARNING:
                  case WV3DAssetComponent.STATE.CRITICAL:
                        path = `${WV3DAssetComponent.ICON_PATH}Access_${state}.png`;
                        break;
                  default:
                        path = `${WV3DAssetComponent.ICON_PATH}Access_normal.png`;
            }

            return new Promise((resolve)=>{
                  let img = new Image();
                  img.onload = () =>{
                        document.body.appendChild(img);
                        let w = img.offsetWidth;
                        let h = img.offsetHeight;
                        let icon = document.createElement("canvas");
                        icon.width = w;
                        icon.height =h;
                        let iconCtx = icon.getContext("2d");
                        iconCtx.drawImage( img, 0, 0, w, h);
                        document.body.removeChild(img);
                        resolve({state:state, icon:icon, width:w, height:h});
                  }
                  img.onerror = (error) => resolve({event:error, messgae:"img load error!"});
                  img.src = path;
            });
      }





      cameraTransitionInInstance(instance) {
            // instance의 스케일 정보를 초기 벡터에 반영
            let instanceSize = instance.size.clone();
            let posZ = instance.size.z + 50;
            let posY = Math.max( 40, instance.size.y + 40 );
            let offset = new THREE.Vector3(0, posY * instanceSize.y/instanceSize.y , posZ * instanceSize.z/instanceSize.z );
            offset = offset.applyMatrix4(instance.appendElement.matrixWorld);
            wemb.mainControls.transitionFocusInTarget(
                  offset,
                  instance.getWorldPosition().clone(),
                  1
            );
      }

      cameraTransitionOut() {
            wemb.mainControls.transitionFocusInTarget(
                  wemb.mainControls.position0.clone(),
                  wemb.mainControls.target0.clone(),
                  1
            );
      }


      transitionInFromAssetID( selectAssetProxy ){
            if(!selectAssetProxy.selected){
                  this.connector.focus = selectAssetProxy.selected = true;
                  // 클릭 시 그리드 스크롤 이동
                  this.accessGridPanel.historyDataGrid.setScrollTopByIndex(selectAssetProxy.iconVO.idx);
                  this.cameraTransitionInInstance(selectAssetProxy.instance);
            } else {
                  this.removeInstanceFromProxy(selectAssetProxy);
                  this.connector.focus = selectAssetProxy.selected = false;
                  this.cameraTransitionOut();
            }
      }






      async requestAccessGridData( params ){
            const [error, response] = await wemb.posodManager.requestAccessGridData(params);
            if(error){
                  this.errorLog(error);
                  return;
            };
            let dataProvider = response.data;
            this.accessGridPanel.accessDataGrid.dataProvider = dataProvider;
            if(dataProvider.length>0){
                  this.requestHistoryData(dataProvider[0]);
            } else {
                  this.updateHistoryData([]);
            }
            this.connector.dataProvider = [];
      }

      async start(){
            this.initFloorList();
            this.initConnector();
            await this.prepareAccessIconAsset();
            try{
                  this.connector.assetCompProxies = await this.composeFloorAssetItem();
                  let params  = this.getSearchParams();
                  this.requestAccessGridData(params);
                  this.animate();
            }catch(error){
                  this.errorLog(error);
            }

      }


      changeData() {

            let provider = this.accessGridPanel.historyDataGrid.dataProvider;
            let connectProvider =[];
            for(let i=0; i<provider.length;i++){
                  let vo = provider[i];
                  let icon = this.iconMap.has(vo.severity) ? this.iconMap.get(vo.severity) : this.iconMap.get(WV3DAssetComponent.STATE.NORMAL);
                  connectProvider.push({
                        index: i,
                        color:this.getLineColorFromState(vo.severity),
                        iconInfo: icon,
                        assetId:vo.asset_id
                  });
            };
            this.changeEventData(connectProvider);
      }


      createInstanceFromProxy( assetItemProxy ){
            return new Promise(async(resolve, reject )=>{
                  let floor = assetItemProxy.parent;
                  assetItemProxy.instance  = await floor.createAssetItemFromProxy(assetItemProxy).catch(reject);
                  resolve();
            });
      }

      removeInstanceFromProxy( assetItemProxy ){
            let floor = assetItemProxy.parent;
            floor.removeAssetItemFromProxy(assetItemProxy);
      }

      async onClickPage( event ){
            let itemProxy = this.connector.getHitItemByPoint({x:event.offsetX, y:event.offsetY});
            if(itemProxy){
                  if(!itemProxy.instance){
                        await this.createInstanceFromProxy(itemProxy).catch(this.errorLog);
                  }
                  if(itemProxy.instance != null){
                        //포커스 처리
                        this.transitionInFromAssetID(itemProxy);
                  }
            }

      }

      animate(){
            try{
                  this.connector.render();
            }catch(error){
                  this.errorLog(error);
            }
            this.animateID = window.requestAnimationFrame(this.animateHandler);
      }

      stop(){
            window.cancelAnimationFrame(this.animateID);
      }

      destroy(){
            this.stop();
            wemb.$globalBus.$off( DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION, this.resetAccessGridHandler );
            $(this.accessGridPanel.accessDataGrid._bodyTable).tooltip("destroy");
            $(this.accessGridPanel.searchForm).find(".date-picker").datepicker("destroy");
            $(this.connector._parent.parentNode).off();
            this.animateHandler = null;
            this.connector.destroy();
            this.connector = null;
            this.floors    = null;
            this.iconMap.clear();
            this.iconMap  = null;
      }

}


