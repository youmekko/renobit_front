function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AccessLinePage =
      /*#__PURE__*/
      function () {
            "use strict";

            function AccessLinePage(accessGridPanel) {
                  var _this = this;

                  _classCallCheck(this, AccessLinePage);

                  this.connector = new WV2DLineConnector();
                  this.floors = [];
                  this.iconMap = new Map();
                  this.animateID = null;
                  this.animateHandler = this.animate.bind(this);
                  this.mouseDownHandler = this.onClickPage.bind(this);
                  this.accessGridPanel = accessGridPanel;
                  this.resetAccessGridHandler = this.resetAccessGrid.bind(this);
                  this.accessGridItemClickHandler = this.onClickAccessGridItem.bind(this);
                  $(this.accessGridPanel.accessDataGrid).on("wvGridItemClick", this.accessGridItemClickHandler);
                  wemb.$globalBus.$on(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION, this.resetAccessGridHandler);
                  wemb.mainControls.saveState();
                  $(this.accessGridPanel.accessDataGrid._bodyTable).tooltip({
                        items: "td",
                        content: function content() {
                              var colIndex = $(this).data("columnIndex");
                              var data = $(this).parent().data("vo");
                              var msg = "";

                              switch (colIndex) {
                                    case 0:
                                          msg = data.severity;
                                          break;

                                    case 1:
                                          msg = data.asset_name;
                                          break;

                                    case 2:
                                          msg = data.stime;
                                          break;

                                    case 3:
                                          for (var prop in data.extra) {
                                                msg += data.extra[prop] + '<br/>';
                                          }

                                          break;

                                    case 4:
                                          msg = data.page_name;
                                          break;
                              }

                              return msg;
                        }
                  });
                  $(this.accessGridPanel.historyDataGrid).on("wvGridItemClick", function (event) {
                        console.log("historyDataGrid-Grid Click", event);
                  });
                  $(this.accessGridPanel.searchForm).find(".date-picker").datepicker({
                        showOn: "button",
                        buttonImage: POSODManager.PACK_PATH + '/POSODAccessDataGridPanel/resource/date-picker.png',
                        buttonImageOnly: true,
                        showOtherMonths: true,

                        /*minDate:"-1M",*/
                        maxDate: "+0",
                        dateFormat: "yy.mm.dd",
                        dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
                  }).datepicker("setDate", new Date()).on("change", this.initSearchInput.bind(this));
                  this.accessGridPanel.searchForm.on("submit", function (event) {
                        event.preventDefault();

                        var searchParams = _this.getSearchParams();

                        _this.requestAccessGridData(searchParams);
                  });
                  this.accessGridPanel.searchRadios.on("change", function (event) {
                        var selectedValue = _this.accessGridPanel.getUserFilterValue().value;

                        if (selectedValue == "access") {
                              _this.accessGridPanel.searchInput.prop("disabled", false); //this.accessGridPanel.userFilterButton.prop("disabled", false);

                        } else {
                              _this.accessGridPanel.searchInput.prop("disabled", true); //this.accessGridPanel.userFilterButton.prop("disabled", true);

                        } //this.accessGridPanel.searchInput.prop("disabled")


                        var params = _this.getSearchParams();

                        _this.requestAccessGridData(params);
                  });
                  /* 사용자 그룹버튼 사용안함.
                  this.accessGridPanel.userFilterButton.on("click", (event)=>{
                        event.preventDefault();
                        let searchParams= this.getSearchParams();
                        this.requestAccessGridData(searchParams);
                  });
                  */

                  this.accessGridPanel.userFilterButton.prop("disabled", true);
            }

            _createClass(AccessLinePage, [{
                  key: "initSearchInput",
                  value: function initSearchInput() {
                        //$(this.accessGridPanel.searchForm).find(".date-picker").change();
                        this.accessGridPanel.searchInput.val('');
                  }
            }, {
                  key: "getSearchParams",
                  value: function getSearchParams(useFilter) {
                        var strDate = this.accessGridPanel.getSearchDateValue().value;
                        strDate = strDate.replace(/\./g, "");
                        var isCritical = this.accessGridPanel.searchInput.prop("disabled");
                        var searchWord = this.accessGridPanel.getSearchInputValue().value || "";
                        var searchParams = {
                              text: isCritical ? "" : searchWord,
                              isCritical: isCritical,
                              s_time: strDate != "" ? strDate + "000000" : "",
                              e_time: strDate != "" ? strDate + "235959" : "",
                              userFilter: useFilter || true
                        };
                        this.accessGridPanel.searchInput.val('');
                        return searchParams;
                  }
            }, {
                  key: "errorLog",
                  value: function errorLog(error) {
                        console.warn("##RENOBIT-response-error", error);
                  }
            }, {
                  key: "updateHistoryData",
                  value: function updateHistoryData(objProvider) {
                        this.accessGridPanel.historyDataGrid.dataProvider = objProvider;
                        this.changeData();
                  }
            }, {
                  key: "validatePlayListFromData",
                  value: function validatePlayListFromData(data) {
                        var reqList = new Map();
                        data.detail.forEach(function (historyData) {
                              if (historyData.count > 0) {
                                    if (!reqList.has(historyData.asset_id)) {
                                          reqList.set(historyData.asset_id, true);
                                    }
                              }
                        });
                        return Array.from(reqList.keys());
                  }
            }, {
                  key: "onClickAccessGridItem",
                  value: function onClickAccessGridItem(event) {
                        var data = event.selectData; //선택 처리

                        if (event.columnIndex == 5) {
                              var reqList = this.validatePlayListFromData(data);
                              wemb.posodManager.requestRecordQENXPlayer(reqList);
                        } else {
                              //this.requestHistoryData(data);
                              this.updateHistoryData(data.detail);
                        }
                  }
            }, {
                  key: "requestHistoryData",
                  value: function () {
                        var _requestHistoryData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee(accessData) {
                                    var _ref, _ref2, error, response;

                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                          while (1) {
                                                switch (_context.prev = _context.next) {
                                                      case 0:
                                                            _context.next = 2;
                                                            return wemb.posodManager.requestHistoryData(accessData);

                                                      case 2:
                                                            _ref = _context.sent;
                                                            _ref2 = _slicedToArray(_ref, 2);
                                                            error = _ref2[0];
                                                            response = _ref2[1];

                                                            if (!error) {
                                                                  _context.next = 9;
                                                                  break;
                                                            }

                                                            this.errorLog(error);
                                                            return _context.abrupt("return");

                                                      case 9:
                                                            try {
                                                                  this.updateHistoryData(response.data);
                                                            } catch (error) {
                                                                  this.errorLog(error);
                                                            }

                                                      case 10:
                                                      case "end":
                                                            return _context.stop();
                                                }
                                          }
                                    }, _callee, this);
                              }));

                        function requestHistoryData(_x) {
                              return _requestHistoryData.apply(this, arguments);
                        }

                        return requestHistoryData;
                  }() // 층 목록 구성

            }, {
                  key: "initFloorList",
                  value: function initFloorList() {
                        var _this2 = this;

                        wemb.mainPageComponent.$threeLayer.forEach(function (instance) {
                              if (_instanceof(instance, FloorComponent)) {
                                    _this2.floors.push(instance);
                              }
                        });
                  } // 연결 커넥터 구성

            }, {
                  key: "initConnector",
                  value: function initConnector() {
                        this.connector.draw(wemb.mainPageComponent.drawLayer.element);
                        $(this.connector._parent.parentNode).on("click", this.mouseDownHandler);
                        this.connector.setDomCompontent(this.accessGridPanel.historyDataGrid);
                        this.connector.setConnectAreaElement(this.accessGridPanel.historyDataGrid._bodyContainer[0]);
                  } // 데이터를 로드하는 구간

            }, {
                  key: "composeFloorAssetItem",
                  value: function composeFloorAssetItem() {
                        var _this3 = this;

                        var requestPageNames = [];
                        this.floors.forEach(function (floor) {
                              requestPageNames.push(floor.name);
                        });

                        function parseInstanceData(instanceList, parent) {
                              var instanceVO;
                              var source;
                              var results = [];

                              for (var idx = 0; idx < instanceList.length; idx++) {
                                    source = instanceList[idx];

                                    if (source.props.setter.data) {
                                          instanceVO = new MeshInstanceProxy(source);
                                          instanceVO.parent = parent;
                                          results.push(instanceVO);
                                    }
                              }

                              return results;
                        }

                        return new Promise(
                              /*#__PURE__*/
                              function () {
                                    var _ref3 = _asyncToGenerator(
                                          /*#__PURE__*/
                                          regeneratorRuntime.mark(function _callee2(resolve) {
                                                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                                      while (1) {
                                                            switch (_context2.prev = _context2.next) {
                                                                  case 0:
                                                                        wemb.dcimManager.getAccessSensor3DModelDataInFloorPage(requestPageNames).then(function (pageInfos) {
                                                                              var instances = [];

                                                                              var _loop = function _loop(i) {
                                                                                    var floor = _this3.floors[i];
                                                                                    var matchPage = pageInfos.filter(function (page) {
                                                                                          return page.pageName == floor.name;
                                                                                    });

                                                                                    if (matchPage) {
                                                                                          instances = instances.concat(parseInstanceData(matchPage[0].data, floor));
                                                                                    }
                                                                              };

                                                                              for (var i = 0; i < _this3.floors.length; i++) {
                                                                                    _loop(i);
                                                                              }

                                                                              resolve(instances);
                                                                        });

                                                                  case 1:
                                                                  case "end":
                                                                        return _context2.stop();
                                                            }
                                                      }
                                                }, _callee2);
                                          }));

                                    return function (_x2) {
                                          return _ref3.apply(this, arguments);
                                    };
                              }());
                  } // 선택 아이템이 있으면 풀고 그리드 초기 상태로 변경

            }, {
                  key: "resetAccessGrid",
                  value: function resetAccessGrid() {
                        this.connector.focus = false;

                        for (var idx = 0; idx < this.connector.connectItems.length; idx++) {
                              var proxy = this.connector.connectItems[idx];

                              if(proxy.selected){
                                    this.removeInstanceFromProxy(proxy);
                                    this.connector.focus = proxy.selected = false;
                              }

                              proxy.parent.opacity = 100;
                        }

                  }
            }, {
                  key: "changeEventData",
                  value: function changeEventData(eventInfo) {
                        this.floors.forEach(function (floor) {
                              floor.opacity = 50;
                        }); // 대상 자산 인스턴스 설정

                        this.connector.focus = false;
                        this.connector.dataProvider = eventInfo;

                        for (var idx = 0; idx < this.connector.connectItems.length; idx++) {
                              var item = this.connector.connectItems[idx];
                              item.parent.opacity = 100;
                        }
                  } // 출입 동선에 사용하는 어셋 로드

            }, {
                  key: "prepareAccessIconAsset",
                  value: function () {
                        var _prepareAccessIconAsset = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee4() {
                                    var _this4 = this;

                                    return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                          while (1) {
                                                switch (_context4.prev = _context4.next) {
                                                      case 0:
                                                            return _context4.abrupt("return", new Promise(
                                                                  /*#__PURE__*/
                                                                  function () {
                                                                        var _ref4 = _asyncToGenerator(
                                                                              /*#__PURE__*/
                                                                              regeneratorRuntime.mark(function _callee3(resolve) {
                                                                                    var iconPromises;
                                                                                    return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                                                                          while (1) {
                                                                                                switch (_context3.prev = _context3.next) {
                                                                                                      case 0:
                                                                                                            _context3.next = 2;
                                                                                                            return Promise.all([_this4.getIconFromStatus(WV3DAssetComponent.STATE.NORMAL), _this4.getIconFromStatus(WV3DAssetComponent.STATE.MINOR), _this4.getIconFromStatus(WV3DAssetComponent.STATE.MAJOR), _this4.getIconFromStatus(WV3DAssetComponent.STATE.WARNING), _this4.getIconFromStatus(WV3DAssetComponent.STATE.CRITICAL)]);

                                                                                                      case 2:
                                                                                                            iconPromises = _context3.sent;
                                                                                                            iconPromises.forEach(function (info) {
                                                                                                                  _this4.iconMap.set(info.state, info);
                                                                                                            });
                                                                                                            resolve();

                                                                                                      case 5:
                                                                                                      case "end":
                                                                                                            return _context3.stop();
                                                                                                }
                                                                                          }
                                                                                    }, _callee3);
                                                                              }));

                                                                        return function (_x3) {
                                                                              return _ref4.apply(this, arguments);
                                                                        };
                                                                  }()));

                                                      case 1:
                                                      case "end":
                                                            return _context4.stop();
                                                }
                                          }
                                    }, _callee4);
                              }));

                        function prepareAccessIconAsset() {
                              return _prepareAccessIconAsset.apply(this, arguments);
                        }

                        return prepareAccessIconAsset;
                  }()
            }, {
                  key: "getLineColorFromState",
                  value: function getLineColorFromState(state) {
                        switch (state) {
                              case WV3DAssetComponent.STATE.NORMAL:
                                    return "#3ca0c5";
                                    break;

                              case WV3DAssetComponent.STATE.MINOR:
                                    return "#e7c33b";
                                    break;

                              case WV3DAssetComponent.STATE.MAJOR:
                                    return "#cd821d";
                                    break;

                              case WV3DAssetComponent.STATE.WARNING:
                                    return "#40bf56";
                                    break;

                              case WV3DAssetComponent.STATE.CRITICAL:
                                    return "#c73f45";
                                    break;

                              default:
                                    return "#ffffff";
                        }
                  }
            }, {
                  key: "getIconFromStatus",
                  value: function getIconFromStatus(state) {
                        var path = "";

                        switch (state) {
                              case WV3DAssetComponent.STATE.NORMAL:
                              case WV3DAssetComponent.STATE.MINOR:
                              case WV3DAssetComponent.STATE.MAJOR:
                              case WV3DAssetComponent.STATE.WARNING:
                              case WV3DAssetComponent.STATE.CRITICAL:
                                    path = "".concat(WV3DAssetComponent.ICON_PATH, "Access_").concat(state, ".png");
                                    break;

                              default:
                                    path = "".concat(WV3DAssetComponent.ICON_PATH, "Access_normal.png");
                        }

                        return new Promise(function (resolve) {
                              var img = new Image();

                              img.onload = function () {
                                    document.body.appendChild(img);
                                    var w = img.offsetWidth;
                                    var h = img.offsetHeight;
                                    var icon = document.createElement("canvas");
                                    icon.width = w;
                                    icon.height = h;
                                    var iconCtx = icon.getContext("2d");
                                    iconCtx.drawImage(img, 0, 0, w, h);
                                    document.body.removeChild(img);
                                    resolve({
                                          state: state,
                                          icon: icon,
                                          width: w,
                                          height: h
                                    });
                              };

                              img.onerror = function (error) {
                                    return resolve({
                                          event: error,
                                          messgae: "img load error!"
                                    });
                              };

                              img.src = path;
                        });
                  }
            }, {
                  key: "cameraTransitionInInstance",
                  value: function cameraTransitionInInstance(instance) {
                        // instance의 스케일 정보를 초기 벡터에 반영
                        var instanceSize = instance.size.clone();
                        var posZ = instance.size.z + 50;
                        var posY = Math.max(40, instance.size.y + 40);
                        var offset = new THREE.Vector3(0, posY * instanceSize.y / instanceSize.y, posZ * instanceSize.z / instanceSize.z);
                        offset = offset.applyMatrix4(instance.appendElement.matrixWorld);
                        wemb.mainControls.transitionFocusInTarget(offset, instance.getWorldPosition().clone(), 1);
                  }
            }, {
                  key: "cameraTransitionOut",
                  value: function cameraTransitionOut() {
                        wemb.mainControls.transitionFocusInTarget(wemb.mainControls.position0.clone(), wemb.mainControls.target0.clone(), 1);
                  }
            }, {
                  key: "transitionInFromAssetID",
                  value: function transitionInFromAssetID(selectAssetProxy) {
                        if (!selectAssetProxy.selected) {
                              this.connector.focus = selectAssetProxy.selected = true; // 클릭 시 그리드 스크롤 이동

                              this.accessGridPanel.historyDataGrid.setScrollTopByIndex(selectAssetProxy.iconVO.idx);
                              this.cameraTransitionInInstance(selectAssetProxy.instance);
                        } else {
                              this.removeInstanceFromProxy(selectAssetProxy);
                              this.connector.focus = selectAssetProxy.selected = false;
                              this.cameraTransitionOut();
                        }
                  }
            }, {
                  key: "requestAccessGridData",
                  value: function () {
                        var _requestAccessGridData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee5(params) {
                                    var _ref5, _ref6, error, response, dataProvider;

                                    return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                          while (1) {
                                                switch (_context5.prev = _context5.next) {
                                                      case 0:
                                                            _context5.next = 2;
                                                            return wemb.posodManager.requestAccessGridData(params);

                                                      case 2:
                                                            _ref5 = _context5.sent;
                                                            _ref6 = _slicedToArray(_ref5, 2);
                                                            error = _ref6[0];
                                                            response = _ref6[1];

                                                            if (!error) {
                                                                  _context5.next = 9;
                                                                  break;
                                                            }

                                                            this.errorLog(error);
                                                            return _context5.abrupt("return");

                                                      case 9:
                                                            ;
                                                            dataProvider = response.data;
                                                            this.accessGridPanel.accessDataGrid.dataProvider = dataProvider;

                                                            if (dataProvider.length > 0) {
                                                                  this.requestHistoryData(dataProvider[0]);
                                                            } else {
                                                                  this.updateHistoryData([]);
                                                            }

                                                            this.connector.dataProvider = [];

                                                      case 14:
                                                      case "end":
                                                            return _context5.stop();
                                                }
                                          }
                                    }, _callee5, this);
                              }));

                        function requestAccessGridData(_x4) {
                              return _requestAccessGridData.apply(this, arguments);
                        }

                        return requestAccessGridData;
                  }()
            }, {
                  key: "start",
                  value: function () {
                        var _start = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee6() {
                                    var params;
                                    return regeneratorRuntime.wrap(function _callee6$(_context6) {
                                          while (1) {
                                                switch (_context6.prev = _context6.next) {
                                                      case 0:
                                                            this.initFloorList();
                                                            this.initConnector();
                                                            _context6.next = 4;
                                                            return this.prepareAccessIconAsset();

                                                      case 4:
                                                            _context6.prev = 4;
                                                            _context6.next = 7;
                                                            return this.composeFloorAssetItem();

                                                      case 7:
                                                            this.connector.assetCompProxies = _context6.sent;
                                                            params = this.getSearchParams();
                                                            this.requestAccessGridData(params);
                                                            this.animate();
                                                            _context6.next = 16;
                                                            break;

                                                      case 13:
                                                            _context6.prev = 13;
                                                            _context6.t0 = _context6["catch"](4);
                                                            this.errorLog(_context6.t0);

                                                      case 16:
                                                      case "end":
                                                            return _context6.stop();
                                                }
                                          }
                                    }, _callee6, this, [[4, 13]]);
                              }));

                        function start() {
                              return _start.apply(this, arguments);
                        }

                        return start;
                  }()
            }, {
                  key: "changeData",
                  value: function changeData() {
                        var provider = this.accessGridPanel.historyDataGrid.dataProvider;
                        var connectProvider = [];

                        for (var i = 0; i < provider.length; i++) {
                              var vo = provider[i];
                              var icon = this.iconMap.has(vo.severity) ? this.iconMap.get(vo.severity) : this.iconMap.get(WV3DAssetComponent.STATE.NORMAL);
                              connectProvider.push({
                                    index: i,
                                    color: this.getLineColorFromState(vo.severity),
                                    iconInfo: icon,
                                    assetId: vo.asset_id
                              });
                        }

                        ;
                        this.changeEventData(connectProvider);
                  }
            }, {
                  key: "createInstanceFromProxy",
                  value: function createInstanceFromProxy(assetItemProxy) {
                        return new Promise(
                              /*#__PURE__*/
                              function () {
                                    var _ref7 = _asyncToGenerator(
                                          /*#__PURE__*/
                                          regeneratorRuntime.mark(function _callee7(resolve, reject) {
                                                var floor;
                                                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                                                      while (1) {
                                                            switch (_context7.prev = _context7.next) {
                                                                  case 0:
                                                                        floor = assetItemProxy.parent;
                                                                        _context7.next = 3;
                                                                        return floor.createAssetItemFromProxy(assetItemProxy).catch(reject);

                                                                  case 3:
                                                                        assetItemProxy.instance = _context7.sent;
                                                                        resolve();

                                                                  case 5:
                                                                  case "end":
                                                                        return _context7.stop();
                                                            }
                                                      }
                                                }, _callee7);
                                          }));

                                    return function (_x5, _x6) {
                                          return _ref7.apply(this, arguments);
                                    };
                              }());
                  }
            }, {
                  key: "removeInstanceFromProxy",
                  value: function removeInstanceFromProxy(assetItemProxy) {
                        var floor = assetItemProxy.parent;
                        floor.removeAssetItemFromProxy(assetItemProxy);
                  }
            }, {
                  key: "onClickPage",
                  value: function () {
                        var _onClickPage = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee8(event) {
                                    var itemProxy;
                                    return regeneratorRuntime.wrap(function _callee8$(_context8) {
                                          while (1) {
                                                switch (_context8.prev = _context8.next) {
                                                      case 0:
                                                            itemProxy = this.connector.getHitItemByPoint({
                                                                  x: event.offsetX,
                                                                  y: event.offsetY
                                                            });

                                                            if (!itemProxy) {
                                                                  _context8.next = 6;
                                                                  break;
                                                            }

                                                            if (itemProxy.instance) {
                                                                  _context8.next = 5;
                                                                  break;
                                                            }

                                                            _context8.next = 5;
                                                            return this.createInstanceFromProxy(itemProxy).catch(this.errorLog);

                                                      case 5:
                                                            if (itemProxy.instance != null) {
                                                                  //포커스 처리
                                                                  this.transitionInFromAssetID(itemProxy);
                                                            }

                                                      case 6:
                                                      case "end":
                                                            return _context8.stop();
                                                }
                                          }
                                    }, _callee8, this);
                              }));

                        function onClickPage(_x7) {
                              return _onClickPage.apply(this, arguments);
                        }

                        return onClickPage;
                  }()
            }, {
                  key: "animate",
                  value: function animate() {
                        try {
                              this.connector.render();
                        } catch (error) {
                              this.errorLog(error);
                        }

                        this.animateID = window.requestAnimationFrame(this.animateHandler);
                  }
            }, {
                  key: "stop",
                  value: function stop() {
                        window.cancelAnimationFrame(this.animateID);
                  }
            }, {
                  key: "destroy",
                  value: function destroy() {
                        this.stop();
                        wemb.$globalBus.$off(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION, this.resetAccessGridHandler);
                        $(this.accessGridPanel.accessDataGrid._bodyTable).tooltip("destroy");
                        $(this.accessGridPanel.searchForm).find(".date-picker").datepicker("destroy");
                        $(this.connector._parent.parentNode).off();
                        this.animateHandler = null;
                        this.connector.destroy();
                        this.connector = null;
                        this.floors = null;
                        this.iconMap.clear();
                        this.iconMap = null;
                  }
            }]);

            return AccessLinePage;
      }();
