window.AccessDataGridItemRenderer = {

      SeverityRenderer:function( columnLabel, parent, vo, context ) {
            var state = vo[columnLabel];
            return '<div class="severity '+state+'">'+state.substring(0,2)+'</div>'
      },

      CCTVRenderer:function(columnLabel, parent, vo, context){
            var cctvInfo = vo[columnLabel];
            if(cctvInfo && cctvInfo > 0 ){
                  return '<div class="btn-cctv"></div>';
            }
            return "";
      },

      UserNameRenderer:function( columnLabel, parent, vo, context ){
            var extraInfo = vo[columnLabel];
            if(extraInfo){
                  let userNameMap = extraInfo.userName.split("|");
                  return '<div class="text-content">'+userNameMap[1]+'</div>';
            }
            return '<div class="text-content"></div>';
      }

}
