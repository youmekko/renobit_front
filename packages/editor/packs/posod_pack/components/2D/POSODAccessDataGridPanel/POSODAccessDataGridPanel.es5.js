"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function set(target, property, value, receiver) { if (typeof Reflect !== "undefined" && Reflect.set) { set = Reflect.set; } else { set = function set(target, property, value, receiver) { var base = _superPropBase(target, property); var desc; if (base) { desc = Object.getOwnPropertyDescriptor(base, property); if (desc.set) { desc.set.call(receiver, value); return true; } else if (!desc.writable) { return false; } } desc = Object.getOwnPropertyDescriptor(receiver, property); if (desc) { if (!desc.writable) { return false; } desc.value = value; Object.defineProperty(receiver, property, desc); } else { _defineProperty(receiver, property, value); } return true; }; } return set(target, property, value, receiver); }

function _set(target, property, value, receiver, isStrict) { var s = set(target, property, value, receiver || target); if (!s && isStrict) { throw new Error('failed to set property'); } return value; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var POSODAccessDataGridPanel =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(POSODAccessDataGridPanel, _WVDOMComponent);

            function POSODAccessDataGridPanel() {
                  var _this;

                  _classCallCheck(this, POSODAccessDataGridPanel);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(POSODAccessDataGridPanel).call(this));
                  _this.accessDataGrid;
                  _this.historyDataGrid;
                  _this.searchForm;
                  _this.searchInput;
                  _this.searchRadios;
                  _this.userFilterButton;
                  _this.searchButton;
                  _this.lang;
                  return _this;
            }

            _createClass(POSODAccessDataGridPanel, [{
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this.lang = Vue.$i18n.messages.wv.posod_pack;
                        var template = '<div class="access-grid-panel">' + '<div class="label"><span>' + this.lang.common.accessRoadPanel + '</span></div>' + '<div class="search-layer">' + '<form accept-charset="utf-8" id= "access-search-form" method = "post">' + '<fieldset>' + '<div class="date-area search-cell"><input type = "text" readonly class="date-picker" name = "date"/></div>' + '<div class="user-filter-area search-cell">' + '<input type = "radio" name = "user_filter" checked id="none_access_user" value="none_access" /><label for="user_filter">' + this.lang.common.unauthorized + '</label>' + '<input type = "radio" name = "user_filter" id="access_user" value="access" /><label for="user_filter">' + this.lang.common.normal + '</label>' + '</div>' + '<div class="search-area search-cell">' + '<input type="text" autocomplete="off" placeholder="search..." disabled name="ipt_search" />' + '<button class="btn-search" ><img src="' + POSODManager.PACK_PATH + this.componentName + '/resource/btn_search_off.png"/></button>' + '<button class="btn-access-user" disabled ><img src="' + POSODManager.PACK_PATH + this.componentName + '/resource/btn_man_off.png"/></button>' + '</div>' + '</fieldset>' + '</form>' + '</div>' + '<div class="contents-layer">' + '<div class="access-grid-area"></div>' + '<h6>' + this.lang.common.accessHistory + '</h6>' + '<div class="history-grid-area"></div>' + '</div>' + '</div>';
                        $(this._element).append($(template));
                        this.accessDataGrid = new WVDataGrid();
                        this.historyDataGrid = new WVDataGrid();
                        this.searchForm = $(this.element).find("#access-search-form");
                        this.searchInput = this.searchForm.find("input[name='ipt_search']");
                        this.searchRadios = this.searchForm.find("input[type='radio']");
                        this.userFilterButton = this.searchForm.find(".btn-access-user");
                        this.searchButton = this.searchForm.find(".btn-search");
                        this.initializeGrid();
                  }
                  /*
                  *  stime: "이벤트 발생시간",
                     // 사용자 정보
                     extra: {"cardNo":"출입ID | 1901019500022","userName":"출입자 | 홍길동","card":"출입카드 | 1901019500022","name":"이름 | 홍길동","employeNo":"직원번호 | 1901019500022","employeeType":"직원타입 | 내부직원","company":"소속회사 | 위엠비","department":"부서 | R&D센터"},
                     seveirty : "등급",
                     inst_name: "인스턴스명",
                     inst_id: "인스턴스id",
                     page_id: "페이지id",
                     page_name: "페이지명"
                  *
                  * */

            }, {
                  key: "initializeGrid",
                  value: function initializeGrid() {
                        var columnDataProvider = [{
                              label: this.lang.common.class,
                              field: "severity",
                              itemRenderer: AccessDataGridItemRenderer.SeverityRenderer
                        }, {
                              label: this.lang.common.access,
                              field: "asset_name"
                        }, {
                              label: this.lang.common.time,
                              field: "stime",
                              css: {
                                    width: 100
                              }
                        }, {
                              label: this.lang.common.name,
                              field: "extra",
                              css: {
                                    width: 100
                              },
                              itemRenderer: AccessDataGridItemRenderer.UserNameRenderer
                        }, {
                              label: this.lang.common.location,
                              field: "page_name",
                              css: {
                                    width: 130
                              }
                        }, {
                              label: this.lang.common.cctv,
                              field: "count",
                              itemRenderer: AccessDataGridItemRenderer.CCTVRenderer
                        }];
                        this.accessDataGrid.columnProvider = columnDataProvider;
                        this.historyDataGrid.columnProvider = [{
                              label: this.lang.common.class,
                              field: "severity",
                              itemRenderer: AccessDataGridItemRenderer.SeverityRenderer
                        }, {
                              label: this.lang.common.access,
                              field: "asset_name"
                        }, {
                              label: this.lang.common.time,
                              field: "stime",
                              css: {
                                    width: 100
                              }
                        }, {
                              label: this.lang.common.name,
                              field: "extra",
                              css: {
                                    width: 80
                              },
                              itemRenderer: AccessDataGridItemRenderer.UserNameRenderer
                        }, {
                              label: this.lang.common.location,
                              field: "page_name"
                        }];
                        this.historyDataGrid.enabled = false;
                        this.accessDataGrid.dataProvider = [];
                        this.historyDataGrid.dataProvider = [];
                        this.accessDataGrid.draw($(this._element).find(".access-grid-area"));
                        this.historyDataGrid.draw($(this._element).find(".history-grid-area"));
                  }
            }, {
                  key: "getSearchFormValues",
                  value: function getSearchFormValues() {
                        return this.searchForm.serializeArray();
                  }
            }, {
                  key: "getSearchDateValue",
                  value: function getSearchDateValue() {
                        return this.getSearchFormValues()[0];
                  }
            }, {
                  key: "getUserFilterValue",
                  value: function getUserFilterValue() {
                        return this.getSearchFormValues()[1];
                  }
            }, {
                  key: "getSearchInputValue",
                  value: function getSearchInputValue() {
                        if (this.searchInput.prop("disabled")) {
                              return {
                                    value: ""
                              };
                        }

                        return this.getSearchFormValues()[2];
                  }
            }, {
                  key: "transformMinimumScale",
                  // transform최소 사이즈
                  value: function transformMinimumScale() {
                        var ow = this.getDefaultProperties().setter.width;
                        return ow / this._transform.width;
                  }
            }, {
                  key: "_validateSize",
                  value: function _validateSize() {
                        _get(_getPrototypeOf(POSODAccessDataGridPanel.prototype), "_validateSize", this).call(this);

                        if (this._parent) {
                              this.accessDataGrid._validateSize();

                              this.historyDataGrid._validateSize();
                        }
                  }
            }, {
                  key: "onLoadPage",
                  value: function onLoadPage() {//this.initializeGrid();
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        $(this.element).find(".date-picker").off();
                        this.searchButton.off();
                        this.userFilterButton.off();
                        this.searchRadios.off();
                        this.searchForm.off();
                        this.accessDataGrid.destroy();
                        this.historyDataGrid.destroy();

                        _get(_getPrototypeOf(POSODAccessDataGridPanel.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "width",
                  set: function set(value) {
                        var w = Math.max(value, this.getDefaultProperties().setter.width);

                        _set(_getPrototypeOf(POSODAccessDataGridPanel.prototype), "width", w, this, true);
                  },
                  get: function get() {
                        return _get(_getPrototypeOf(POSODAccessDataGridPanel.prototype), "width", this);
                  }
            }]);

            return POSODAccessDataGridPanel;
      }(WVDOMComponent); // 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(POSODAccessDataGridPanel, {
      "info": {
            "componentName": "POSODAccessDataGridPanel",
            "version": "1.0.0"
      },
      "setter": {
            "width": 490,
            "height": 600
      },
      "label": {
            "label_using": "N",
            "label_text": "POSODAccessDataGridPanel"
      },
      "font": {
            "font_type": "inherit"
      }
});
POSODAccessDataGridPanel.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "label"
}];
