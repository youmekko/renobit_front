"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var WV2DLineConnector = function () {
      function WV2DLineConnector() {
            _classCallCheck(this, WV2DLineConnector);

            this._parent;
            this.assetCompProxies = [];
            this.connectItems = [];
            this._dataProvider = [];
            this._focus = false;
            this.domComponent = null;
            this.connectAreaElement = null;
            this.connectItemMap = null;
      }

      _createClass(WV2DLineConnector, [{
            key: "setConnectAreaElement",
            value: function setConnectAreaElement(area) {
                  this.connectAreaElement = area;
            }
      }, {
            key: "setDomCompontent",
            value: function setDomCompontent(component) {
                  this.domComponent = component;
            }
      }, {
            key: "draw",
            value: function draw(parent) {
                  this._element = document.createElement("canvas");
                  this._ctx = this._element.getContext("2d");
                  this._element.width = wemb.mainPageComponent.width;
                  this._element.height = wemb.mainPageComponent.height;
                  $(parent).append(this._element);
                  this._parent = parent;
            }
      }, {
            key: "getHitItemByPoint",
            value: function getHitItemByPoint(point) {
                  if (this.connectItems.length > 0) {
                        var item = void 0;
                        for (var i = 0; i < this.connectItems.length; i++) {
                              item = this.connectItems[i];
                              if (!item.iconVO) continue;
                              if(this._focus && !item.selected ){
                                    // 포커스 상태인데 아이템이 선택 상태가 아니면 예외
                                    continue;
                              }
                              if (point.x > item.iconVO.x && point.x < item.iconVO.x + item.iconVO.w && point.y > item.iconVO.y && point.y < item.iconVO.y + item.iconVO.h) {
                                    return item;
                              }
                        }
                  }
                  return null;
            }

            /**
             objProvider를 통해 2D Grid 엘리먼트 목록이 받음.
             2D 엘리먼트 목록의 속성을 3D컴포넌트 목록에서 조회하여 매칭 목록을 구함.
             */

      }, {
            key: "render",
            value: function render() {

                  this._ctx.clearRect(0, 0, this._element.width, this._element.height);
                  if (this.connectItems.length > 0) {

                        var from = void 0,
                              to = void 0,
                              p1 = void 0,
                              p2 = void 0,
                              vo = void 0,
                              iconInfo = void 0,
                              pos = void 0,
                              assetCompProxy = void 0,
                              ix = void 0,
                              iy = void 0,
                              domElement = void 0,
                              idx = void 0;
                        var connectLine = void 0;
                        var rowList = this.domComponent.getGridRowItems();

                        var area = this.connectAreaElement.getBoundingClientRect(); //{x:this.connectArea.x, y:this.connectArea.y, h:this.connectArea.bottom};
                        if (area.x == undefined) {
                              area.x = area.left;
                              area.y = area.top;
                        }
                        var offset = $(this._parent).offset();
                        offset.left *= -1;
                        offset.top *= -1;
                        area.x += offset.left;
                        area.y += offset.top;

                        for (var j = 0; j < rowList.length; j++) {
                              domElement = rowList[j];
                              idx = $(domElement).data("index");
                              vo = this._dataProvider[idx];
                              if (this.connectItemMap.has(vo.assetId)) {
                                    assetCompProxy = this.connectItemMap.get(vo.assetId);
                                    if (this._focus) {
                                          if (!assetCompProxy.selected) continue;
                                    }
                                    connectLine = true;
                                    iconInfo = vo.iconInfo;
                                    pos = assetCompProxy.getWorldPosition();
                                    from = ThreeUtil.getThreePositionTo2D(pos, window.wemb.camera, window.wemb.renderer);
                                    to = domElement.getBoundingClientRect();
                                    if (to.x == undefined) {
                                          to.x = to.left;
                                          to.y = to.top;
                                    }

                                    to.y += to.height / 2; // y값을 중앙으로 이동
                                    // parent의 offset반영
                                    to.x += offset.left;
                                    to.y += offset.top;

                                    // 영역을 벗어난 값을 제외
                                    if (area.y > to.y || area.y + area.height < to.y + to.height / 2) {
                                          connectLine = false;
                                          //프록시에 있는 iconVO속성 초기화
                                          //assetCompProxy.iconVO = null;
                                    }

                                    if (connectLine) {
                                          p1 = CPUtil.getInstance().lerp(from, to, 0.1);
                                          p2 = CPUtil.getInstance().lerp(from, to, 0.9);
                                          this._ctx.save();
                                          this._ctx.beginPath();
                                          this._ctx.moveTo(from.x, from.y);
                                          this._ctx.lineTo(p1.x, from.y);
                                          this._ctx.lineTo(p1.x, p1.y);
                                          this._ctx.lineTo(p2.x, p1.y);
                                          this._ctx.lineTo(p2.x, to.y);
                                          this._ctx.lineTo(to.x, to.y);
                                          this._ctx.strokeStyle = vo.color;
                                          this._ctx.lineWidth = 2;
                                          this._ctx.stroke();
                                          this._ctx.closePath();

                                          ix = from.x - iconInfo.width / 2;
                                          iy = from.y - iconInfo.height / 2;
                                          this._ctx.drawImage(iconInfo.icon, ix, iy, iconInfo.width, iconInfo.height + 1);
                                          this._ctx.restore();
                                          assetCompProxy.iconVO = { x: ix, y: iy, w: iconInfo.width, h: iconInfo.height, idx: idx };
                                    }


                              }
                        }
                  }
            }
      }, {
            key: "destroy",
            value: function destroy() {
                  this._ctx.clearRect(0, 0, this._element.width, this._element.height);
                  this._ctx = null;
                  this.assetCompProxies.forEach(function (vo) {
                        vo.iconVO = null;
                        vo.destroy();
                  });
                  this.assetCompProxies = [];
                  this.connectItemMap.clear();
                  this.connectItems = null;
                  this._dataProvider.forEach(function (vo) {
                        vo = null;
                  });
                  $(this._element).off().remove();
                  this._element = null;
            }
      }, {
            key: "focus",
            set: function set(value) {
                  this._focus = value;
            },
            get: function get() {
                  return this._focus;
            }
      }, {
            key: "dataProvider",
            set: function set(objProvider) {
                  var _this = this;

                  if (this._dataProvider != objProvider) {

                        // 현재 연결된 자산 초기화
                        this.connectItems.forEach(function (item) {
                              if (item.selected) {
                                    item.selected = false;
                              }
                        });

                        this._dataProvider = objProvider;
                        var calculator = function calculator(source, vo) {
                              var result = _this.assetCompProxies.filter(function (item) {
                                    return item.assetId == vo.assetId;
                              });
                              if (result.length > 0) {
                                    source.set(vo.assetId, result[0]);
                              }
                              return source;
                        };
                        this.connectItemMap = this._dataProvider.reduce(calculator, new Map());
                        this.connectItems = [].concat(_toConsumableArray(this.connectItemMap.values()));
                  }
            },
            get: function get() {
                  return this._dataProvider;
            }
      }]);

      return WV2DLineConnector;
}();
