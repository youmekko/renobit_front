function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var FloorComponent =
      /*#__PURE__*/
      function (_WV3DResourceComponen) {
            "use strict";

            _inherits(FloorComponent, _WV3DResourceComponen);

            function FloorComponent() {
                  var _this;

                  _classCallCheck(this, FloorComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(FloorComponent).call(this));
                  _this._childProvider = [];
                  _this._childs = [];
                  return _this;
            }

            _createClass(FloorComponent, [{
                  key: "createAssetItemFromProxy",
                  // 지정된 정보에 기반한 자식요소 구성
                  value: function createAssetItemFromProxy(proxy) {
                        var _this2 = this;

                        return new Promise(
                              /*#__PURE__*/
                              function () {
                                    var _ref = _asyncToGenerator(
                                          /*#__PURE__*/
                                          regeneratorRuntime.mark(function _callee(resolve, reject) {
                                                var resource, loadedObj, comp;
                                                return regeneratorRuntime.wrap(function _callee$(_context) {
                                                      while (1) {
                                                            switch (_context.prev = _context.next) {
                                                                  case 0:
                                                                        resource = proxy.getResourceInfo();
                                                                        _context.prev = 1;
                                                                        _context.next = 4;
                                                                        return NLoaderManager.composeResource(resource, true);

                                                                  case 4:
                                                                        loadedObj = _context.sent;
                                                                        comp = new WVMeshDecorator(loadedObj); // assetId를 이름으로 설정

                                                                        comp.name = "asset_" + proxy.assetId;
                                                                        comp.assetId = proxy.assetId;
                                                                        comp.data = proxy.data;
                                                                        comp.draw(_this2);
                                                                        comp.position = proxy.position;
                                                                        comp.rotation = proxy.rotation;
                                                                        comp.scale = proxy.scale;

                                                                        _this2._childs.push(comp);

                                                                        resolve(comp);
                                                                        _context.next = 20;
                                                                        break;

                                                                  case 17:
                                                                        _context.prev = 17;
                                                                        _context.t0 = _context["catch"](1);
                                                                        reject(_context.t0);

                                                                  case 20:
                                                                  case "end":
                                                                        return _context.stop();
                                                            }
                                                      }
                                                }, _callee, null, [[1, 17]]);
                                          }));

                                    return function (_x, _x2) {
                                          return _ref.apply(this, arguments);
                                    };
                              }());
                  }
            }, {
                  key: "removeAssetItemFromProxy",
                  value: function removeAssetItemFromProxy(proxy) {
                        var idx = this._childs.indexOf(proxy.instance);

                        if (idx > -1) {
                              this._childs.splice(idx, 1);
                        }

                        proxy.instance.destroy();
                  } //현재 어셋을 제거

            }, {
                  key: "clearAssetItem",
                  value: function clearAssetItem() {
                        this._childs.forEach(function (child) {
                              child.destroy();
                        });

                        this._childs = [];
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        _get(_getPrototypeOf(FloorComponent.prototype), "_onCommitProperties", this).call(this);

                        if (this._invalidateSelectItem) {
                              this._invalidateSelectItem = false;
                              this.validateCallLater(this._validateResource);
                        }
                  }
            }, {
                  key: "_validateResource",
                  value: function () {
                        var _validateResource2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2() {
                                    var info, loadedObj;
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            info = this.selectItem;
                                                            this.removePrevResource(); // 초기 리로드 로드 후 아이템이 변경된 거면 기본 사이즈 정보 제거

                                                            if (info == null || info == "") {
                                                                  this._onCreateElement();

                                                                  this._elementSize = null;
                                                                  this.opacity = 100;
                                                                  this.color = "#ffffff";
                                                                  this.size = this.getDefaultProperties().setter.size;
                                                            }

                                                            if (!info) {
                                                                  _context2.next = 16;
                                                                  break;
                                                            }

                                                            _context2.prev = 4;

                                                            if (info.mapPath.substr(-1) != "/") {
                                                                  info.mapPath += "/";
                                                            }

                                                            _context2.next = 8;
                                                            return NLoaderManager.composeResource(info, true);

                                                      case 8:
                                                            loadedObj = _context2.sent;
                                                            this.composeResource(loadedObj);

                                                            this._onValidateResource();

                                                            _context2.next = 16;
                                                            break;

                                                      case 13:
                                                            _context2.prev = 13;
                                                            _context2.t0 = _context2["catch"](4);
                                                            throw _context2.t0;

                                                      case 16:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2, this, [[4, 13]]);
                              }));

                        function _validateResource() {
                              return _validateResource2.apply(this, arguments);
                        }

                        return _validateResource;
                  }()
            }, {
                  key: "startLoadResource",
                  value: function () {
                        var _startLoadResource = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee3() {
                                    return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                          while (1) {
                                                switch (_context3.prev = _context3.next) {
                                                      case 0:
                                                            _context3.prev = 0;

                                                            if (!this.selectItem) {
                                                                  _context3.next = 4;
                                                                  break;
                                                            }

                                                            _context3.next = 4;
                                                            return this._validateResource();

                                                      case 4:
                                                            _context3.next = 9;
                                                            break;

                                                      case 6:
                                                            _context3.prev = 6;
                                                            _context3.t0 = _context3["catch"](0);
                                                            console.log("startLoadResource-error ", _context3.t0);

                                                      case 9:
                                                            _context3.prev = 9;
                                                            this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                                            return _context3.finish(9);

                                                      case 12:
                                                      case "end":
                                                            return _context3.stop();
                                                }
                                          }
                                    }, _callee3, this, [[0, 6, 9, 12]]);
                              }));

                        function startLoadResource() {
                              return _startLoadResource.apply(this, arguments);
                        }

                        return startLoadResource;
                  }()
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.clearAssetItem();
                        this._childs = null;
                        this._childProvider = null;
                        this._invalidateSelectItem = null;

                        _get(_getPrototypeOf(FloorComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "selectItem",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) {
                              this._invalidateSelectItem = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "selectItem");
                  }
            }]);

            return FloorComponent;
      }(WV3DResourceComponent);

WV3DPropertyManager.attach_default_component_infos(FloorComponent, {
      "setter": {
            "size": {
                  x: 1,
                  y: 1,
                  z: 1
            },
            "state": "normal",
            "selectItem": ""
      },
      "label": {
            "label_text": "FloorComponent",
            "label_background_color": "#3351ED",
            "label_using": "N"
      },
      "info": {
            "componentName": "FloorComponent",
            "version": "1.0.0"
      }
});
WVPropertyManager.add_property_panel_group_info(FloorComponent, {
      label: "Resource",
      template: "resource",
      children: [{
            owner: "setter",
            name: "selectItem",
            type: "resource",
            label: "Resource",
            show: true,
            writable: true,
            description: "3D Object",
            options: {
                  type: "obj3d"
            }
      }]
}); //WV3DPropertyManager.removePropertyGroupByName(FloorComponent.property_panel_info, "label", "label");
