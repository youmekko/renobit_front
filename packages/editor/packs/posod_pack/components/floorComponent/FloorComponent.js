class FloorComponent extends WV3DResourceComponent {

    constructor() {
        super();
        this._childProvider = [];
        this._childs = [];
    }

    set selectItem(value) {
        if (this._checkUpdateGroupPropertyValue("setter", "selectItem", value)) { this._invalidateSelectItem = true; }
    }

    get selectItem() { return this.getGroupPropertyValue("setter", "selectItem"); }




    // 지정된 정보에 기반한 자식요소 구성
    createAssetItemFromProxy(proxy) {
        return new Promise(async(resolve, reject) => {
            let resource;
            let loadedObj;
            resource = proxy.getResourceInfo();
            try {
                loadedObj = await NLoaderManager.composeResource(resource, true);
                let comp = new WVMeshDecorator(loadedObj);
                // assetId를 이름으로 설정
                comp.name = "asset_" + proxy.assetId;
                comp.assetId = proxy.assetId;
                comp.data = proxy.data;
                comp.draw(this);
                comp.position = proxy.position;
                comp.rotation = proxy.rotation;
                comp.scale    = proxy.scale;
                this._childs.push(comp);
                resolve(comp);
            } catch (error) {
                reject(error);
            }
        });
    }

    removeAssetItemFromProxy(proxy){
          let idx = this._childs.indexOf(proxy.instance);
          if(idx>-1){
                this._childs.splice( idx, 1 );
          }
          proxy.instance.destroy();
    }

    //현재 어셋을 제거
    clearAssetItem() {
        this._childs.forEach((child) => {
            child.destroy();
        });
        this._childs = [];
    }

    _onCommitProperties() {
        super._onCommitProperties();
        if (this._invalidateSelectItem) {
            this._invalidateSelectItem = false;
            this.validateCallLater(this._validateResource);
        }
    }


    async _validateResource() {
        let info = this.selectItem;

        this.removePrevResource();
        // 초기 리로드 로드 후 아이템이 변경된 거면 기본 사이즈 정보 제거
        if (info == null || info == "") {
            this._onCreateElement();
            this._elementSize = null;
            this.opacity = 100;
            this.color = "#ffffff";
            this.size = this.getDefaultProperties().setter.size;
        }

        if (info) {
            try {
                if (info.mapPath.substr(-1) != "/") {
                    info.mapPath += "/";
                }
                let loadedObj = await NLoaderManager.composeResource(info, true);
                this.composeResource(loadedObj);
                this._onValidateResource();
            } catch (error) {
                throw error;
            }
        }
    }

    async startLoadResource() {
        try {
            if (this.selectItem) {
                await this._validateResource();
            }
        } catch (error) {
            console.log("startLoadResource-error ", error );
        } finally {
            this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
        }

    }


    _onDestroy() {
        this.clearAssetItem();
        this._childs = null;
        this._childProvider = null;
        this._invalidateSelectItem = null;
        super._onDestroy();
    }

}



WV3DPropertyManager.attach_default_component_infos(FloorComponent, {
    "setter": {
        "size": { x: 1, y: 1, z: 1 },
        "state": "normal",
        "selectItem": ""
    },
    "label": {
        "label_text": "FloorComponent",
        "label_background_color": "#3351ED",
        "label_using": "N"
    },
    "info": {
        "componentName": "FloorComponent",
        "version": "1.0.0",
    }
});


WVPropertyManager.add_property_panel_group_info(FloorComponent, {
    label: "Resource",
    template: "resource",
    children: [{
        owner: "setter",
        name: "selectItem",
        type: "resource",
        label: "Resource",
        show: true,
        writable: true,
        description: "3D Object",
        options: {
            type: "obj3d"
        }
    }]
});


//WV3DPropertyManager.removePropertyGroupByName(FloorComponent.property_panel_info, "label", "label");
