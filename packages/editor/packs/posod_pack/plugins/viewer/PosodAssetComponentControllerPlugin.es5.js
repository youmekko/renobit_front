"use strict";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var PosodAssetComponentControllerPlugin =
      /*#__PURE__*/
      function (_AssetComponentContro) {
            _inherits(PosodAssetComponentControllerPlugin, _AssetComponentContro);

            function PosodAssetComponentControllerPlugin() {
                  _classCallCheck(this, PosodAssetComponentControllerPlugin);

                  return _possibleConstructorReturn(this, _getPrototypeOf(PosodAssetComponentControllerPlugin).call(this));
            }

            _createClass(PosodAssetComponentControllerPlugin, [{
                  key: "start",
                  value: function start() {
                        console.log("START_POSOD");

                        _get(_getPrototypeOf(PosodAssetComponentControllerPlugin.prototype), "start", this).call(this);
                  }
            }]);

            return PosodAssetComponentControllerPlugin;
      }(AssetComponentControllerPlugin); //*****
// 컴포넌트 액션 설정
//*****


(function () {
      var ActionManager =
            /*#__PURE__*/
            function () {
                  _createClass(ActionManager, [{
                        key: "currentAction",
                        set: function set(action) {
                              this._currentAction = action;
                        },
                        get: function get() {
                              return this._currentAction;
                        }
                  }]);

                  function ActionManager() {
                        _classCallCheck(this, ActionManager);

                        this._actionMap = null;
                        this._currentAction = null;

                        this._createActionMap();
                  }

                  _createClass(ActionManager, [{
                        key: "_createActionMap",
                        value: function _createActionMap() {
                              this._actionMap = new Map();

                              this._actionMap.set(AssetComponentProxy.TYPE.CCTV, new CCTVAction(this));

                              this._actionMap.set(AssetComponentProxy.TYPE.PDU, new PDUAction(this));

                              this._actionMap.set(AssetComponentProxy.TYPE.RACK, new RackAction(this));

                              this._actionMap.set(AssetComponentProxy.TYPE.ACCESS, new AccessAction(this));

                              this._actionMap.set(AssetComponentProxy.TYPE.FIRE, new FireAction(this));

                              this._actionMap.set(AssetComponentProxy.EFFECT.FIRE_EFFECT, new FireEffectAction(this));
                        }
                  }, {
                        key: "clearCurrentAction",
                        value: function clearCurrentAction() {
                              if (this._currentAction) {
                                    this._currentAction.clear();

                                    this._currentAction = null;
                              }
                        }
                  }, {
                        key: "clearFireEffectAction",
                        value: function clearFireEffectAction() {
                              var fireEffectAction = this._actionMap.get(AssetComponentProxy.EFFECT.FIRE_EFFECT);

                              if (fireEffectAction) {
                                    fireEffectAction.clear();
                              }
                        }
                  }, {
                        key: "getActionByType",
                        value: function getActionByType(typeName) {
                              // assetAction 정보 구하기
                              var assetAction = this._actionMap.get(typeName);

                              return assetAction;
                        }
                        /*
						불효과 활성화
						*/

                  }, {
                        key: "addFireEffect",
                        value: function addFireEffect(name, comInstance) {
                              var fireEffect = this._actionMap.get(AssetComponentProxy.EFFECT.FIRE_EFFECT);

                              fireEffect.addFireEffect(name, comInstance);
                        } // 타입에 따른 액션 실행.

                  }, {
                        key: "executeAction",
                        value: function executeAction(comInstance) {
                              try {
                                    var assetInfo = comInstance.data;

                                    var assetAction = this._actionMap.get(assetInfo.asset_type);

                                    if (assetAction == null) {
                                          return null;
                                    }

                                    assetAction.action(comInstance);
                              } catch (error) {
                                    console.log("ERROR ActionManager executeAction", error);
                              }
                        }
                  }]);

                  return ActionManager;
            }();

      var AssetAction =
            /*#__PURE__*/
            function () {
                  _createClass(AssetAction, [{
                        key: "eventBus",
                        get: function get() {
                              return this._$eventBus;
                        }
                  }]);

                  function AssetAction(controller) {
                        _classCallCheck(this, AssetAction);

                        this._controller = controller;
                        this._$eventBus = new Vue();
                  } // action이 실행될때 실행.


                  _createClass(AssetAction, [{
                        key: "action",
                        value: function action(comInstance) {} // 페이지가 닫힐때 실행

                  }, {
                        key: "clear",
                        value: function clear() {
                              if (this._$eventBus) {
                                    this.eventBus.$off();
                              }
                        }
                  }, {
                        key: "$on",
                        value: function $on(eventName, listener) {
                              this._$eventBus.$on(eventName, listener);
                        }
                  }, {
                        key: "$off",
                        value: function $off(eventName, listener) {
                              this._$eventBus.$off(eventName, listener);
                        } //////////////////////////////////
                        // 특정 자산과 연관된 CCTV 자산 목록을 play 리스트로 만들어 실행

                  }, {
                        key: "playRelationCCTVList",
                        value: function playRelationCCTVList(cctvAssetInfoList) {
                              // cctvAssetInfoList에는 cctv assetid만 들어 있기때문에
                              // 이 값을 가지고 자산정보를 구해야함.
                              cctvAssetInfoList = cctvAssetInfoList.map(function (info) {
                                    try {
                                          var assetInfo = wemb.dcimManager.assetManager.assetFlatMap.get(info.child_id);
                                          return assetInfo;
                                    } catch (error) {
                                          console.warn("## ", error);
                                    }
                              });

                              if (cctvAssetInfoList.length <= 0) {
                                    return;
                              }
                              /*
									QENXLivePopViewer 파라메터 생성
							   */


                              try {
                                    var playList = cctvAssetInfoList.map(function (assetInfo) {
                                          return assetInfo.rtsp_url.replace("rtsp://", "rtsp://".concat(assetInfo.rtsp_id, ":").concat(assetInfo.rtsp_pass, "@"));
                                    });
                                    var playListUrlString = playList.join(",");
                                    var params = {
                                          url: playListUrlString,
                                          options: cctvAssetInfoList[0].rtsp_options
                                    };
                                    wemb.posodManager.openQENXLivePopViewer(params);
                              } catch (error) {
                                    Vue.$message("The player can not run because there is no RTSP information in the asset information.");
                                    console.log("cctvAssetInfoList", cctvAssetInfoList);
                              }
                        } // 특정 자산과 연관된 CCTV 자산을 선택 상태로

                  }, {
                        key: "activeRelationCCTVList",
                        value: function activeRelationCCTVList(cctvAssetInfoList, relationAssets) {
                              var relationAsset, relationData;

                              for (var i = 0; i < cctvAssetInfoList.length; i++) {
                                    relationData = cctvAssetInfoList[i];

                                    if (wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.has(relationData.child_id)) {
                                          relationAsset = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(relationData.child_id); // 화각을 critical로 변경
                                          // 선택상태로 ㅂ녀경

                                          if (relationAsset.comInstance) {
                                                relationAssets.push(relationAsset.comInstance);
                                                relationAsset.comInstance.changeCameraRangeSeverity(WV3DAssetComponent.STATE.CRITICAL);
                                                relationAsset.comInstance.selected = true; // 선택 아웃라인 비활성화 처리

                                                relationAsset.comInstance.toggleSelectedOutline(false);
                                          }
                                    }
                              }
                        }
                  }]);

                  return AssetAction;
            }();

      AssetAction.Event = {};
      AssetAction.Event.FOCUS = "focus";
      AssetAction.Event.BLUR = "blur";
      AssetAction.Event.SELECTED = "selected";
      /*
	  A. 출입센서 선택시  출입센서 상태가 critical인 경우에만
			1. 연관된 카메라를 critical로 변경
			2.  연관된 카메라.selected = true
				- 마우스 이벤트 영향을 받지 않기 위해
		B. 비선택 시
			1.
		*/

      var AccessAction =
            /*#__PURE__*/
            function (_AssetAction) {
                  _inherits(AccessAction, _AssetAction);

                  function AccessAction(controller) {
                        var _this;

                        _classCallCheck(this, AccessAction);

                        _this = _possibleConstructorReturn(this, _getPrototypeOf(AccessAction).call(this, controller));
                        _this.relationAssets = [];
                        return _this;
                  }

                  _createClass(AccessAction, [{
                        key: "action",
                        value: function () {
                              var _action = _asyncToGenerator(
                                    /*#__PURE__*/
                                    regeneratorRuntime.mark(function _callee(comInstance) {
                                          var assetId, _ref, _ref2, error, response, cctvAssetInfoList;

                                          return regeneratorRuntime.wrap(function _callee$(_context) {
                                                while (1) {
                                                      switch (_context.prev = _context.next) {
                                                            case 0:
                                                                  assetId = "";

                                                                  if (!_instanceof(comInstance, WV3DAssetComponent)) {
                                                                        _context.next = 6;
                                                                        break;
                                                                  }

                                                                  if (comInstance.assetId) {
                                                                        _context.next = 5;
                                                                        break;
                                                                  }

                                                                  console.log("컴포넌트에 자산 정보가 설정되어 있지 않습니다. ");
                                                                  return _context.abrupt("return");

                                                            case 5:
                                                                  assetId = comInstance.assetId;

                                                            case 6:
                                                                  _context.next = 8;
                                                                  return window.wemb.dcimManager.getRelationData(assetId);

                                                            case 8:
                                                                  _ref = _context.sent;
                                                                  _ref2 = _slicedToArray(_ref, 2);
                                                                  error = _ref2[0];
                                                                  response = _ref2[1];

                                                                  if (!error) {
                                                                        _context.next = 15;
                                                                        break;
                                                                  }

                                                                  CPLogger.log("출입센서 연관자산 데이터 로드 에러", error);
                                                                  return _context.abrupt("return");

                                                            case 15:
                                                                  // 연관 CCTV PLAy
                                                                  cctvAssetInfoList = response.data.data;

                                                                  if (!(cctvAssetInfoList.length <= 0)) {
                                                                        _context.next = 18;
                                                                        break;
                                                                  }

                                                                  return _context.abrupt("return");

                                                            case 18:
                                                                  this.playRelationCCTVList(cctvAssetInfoList); // critical인 경우만 처리

                                                                  if (comInstance.isCriticalState() == true) {
                                                                        this.activeRelationCCTVList(cctvAssetInfoList, this.relationAssets);
                                                                  }

                                                            case 20:
                                                            case "end":
                                                                  return _context.stop();
                                                      }
                                                }
                                          }, _callee, this);
                                    }));

                              function action(_x) {
                                    return _action.apply(this, arguments);
                              }

                              return action;
                        }()
                  }, {
                        key: "clear",
                        value: function clear() {
                              this.relationAssets.forEach(function (comInstance) {
                                    comInstance.selected = false;
                              });
                              this.relationAssets = [];
                              wemb.posodManager.closeQENXViewer();

                              _get(_getPrototypeOf(AccessAction.prototype), "clear", this).call(this);
                        }
                  }]);

                  return AccessAction;
            }(AssetAction);
      /*
	  A. 출입센서 선택시  출입센서 상태가 critical인 경우에만
			1. 연관된 카메라를 critical로 변경
			2.  연관된 카메라.selected = true
				- 마우스 이벤트 영향을 받지 않기 위해
	  B. 비선택 시
			1.
	   */


      var FireAction =
            /*#__PURE__*/
            function (_AssetAction2) {
                  _inherits(FireAction, _AssetAction2);

                  function FireAction(controller) {
                        var _this2;

                        _classCallCheck(this, FireAction);

                        _this2 = _possibleConstructorReturn(this, _getPrototypeOf(FireAction).call(this, controller));
                        _this2.relationAssets = [];
                        return _this2;
                  }

                  _createClass(FireAction, [{
                        key: "action",
                        value: function () {
                              var _action2 = _asyncToGenerator(
                                    /*#__PURE__*/
                                    regeneratorRuntime.mark(function _callee2(comInstance) {
                                          var assetId, _ref3, _ref4, error, response, cctvAssetInfoList;

                                          return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                                while (1) {
                                                      switch (_context2.prev = _context2.next) {
                                                            case 0:
                                                                  assetId = "";

                                                                  if (!_instanceof(comInstance, WV3DAssetComponent)) {
                                                                        _context2.next = 6;
                                                                        break;
                                                                  }

                                                                  if (comInstance.assetId) {
                                                                        _context2.next = 5;
                                                                        break;
                                                                  }

                                                                  console.log("컴포넌트에 자산 정보가 설정되어 있지 않습니다. ");
                                                                  return _context2.abrupt("return");

                                                            case 5:
                                                                  assetId = comInstance.assetId;

                                                            case 6:
                                                                  _context2.next = 8;
                                                                  return window.wemb.dcimManager.getRelationData(assetId);

                                                            case 8:
                                                                  _ref3 = _context2.sent;
                                                                  _ref4 = _slicedToArray(_ref3, 2);
                                                                  error = _ref4[0];
                                                                  response = _ref4[1];

                                                                  if (!error) {
                                                                        _context2.next = 15;
                                                                        break;
                                                                  }

                                                                  CPLogger.log("출입센서 연관자산 데이터 로드 에러", error);
                                                                  return _context2.abrupt("return");

                                                            case 15:
                                                                  // 연관 CCTV PLAy
                                                                  cctvAssetInfoList = response.data.data;

                                                                  if (!(cctvAssetInfoList.length <= 0)) {
                                                                        _context2.next = 18;
                                                                        break;
                                                                  }

                                                                  return _context2.abrupt("return");

                                                            case 18:
                                                                  this.playRelationCCTVList(cctvAssetInfoList); // critical인 경우만 처리

                                                                  if (comInstance.isCriticalState() == true) {
                                                                        this.activeRelationCCTVList(cctvAssetInfoList, this.relationAssets);
                                                                  }

                                                            case 20:
                                                            case "end":
                                                                  return _context2.stop();
                                                      }
                                                }
                                          }, _callee2, this);
                                    }));

                              function action(_x2) {
                                    return _action2.apply(this, arguments);
                              }

                              return action;
                        }()
                  }, {
                        key: "clear",
                        value: function clear() {
                              this.relationAssets.forEach(function (comInstance) {
                                    comInstance.selected = false;
                              });
                              this.relationAssets = [];
                              wemb.posodManager.closeQENXViewer();

                              _get(_getPrototypeOf(FireAction.prototype), "clear", this).call(this);
                        }
                  }]);

                  return FireAction;
            }(AssetAction);

      var CCTVAction =
            /*#__PURE__*/
            function (_AssetAction3) {
                  _inherits(CCTVAction, _AssetAction3);

                  function CCTVAction(controller) {
                        _classCallCheck(this, CCTVAction);

                        return _possibleConstructorReturn(this, _getPrototypeOf(CCTVAction).call(this, controller));
                  }

                  _createClass(CCTVAction, [{
                        key: "action",
                        value: function action(comInstance) {
                              try {
                                    var assetInfo = comInstance.data;
                                    /**
                                     * //실시간 영상
                                     *{
          url:"rtsp://admin:!1q2w3e4r@192.168.1.101:554/onvif/profile1/media.smp",
          options:" --top_most --size=480,320 --pos=100,85 --title=test"
          }**/

                                    var tempUrl = assetInfo.rtsp_url.replace("rtsp://", "rtsp://".concat(assetInfo.rtsp_id, ":").concat(assetInfo.rtsp_pass, "@"));
                                    var params = {
                                          url: tempUrl,
                                          options: assetInfo.rtsp_options
                                    };
                                    wemb.posodManager.openQENXLivePopViewer(params);
                              } catch (error) {
                                    //Vue.$message("자산 정보에 RTSP 정보가 존재하지 않아 플레이어를 실행할 수 없습니다.")
                                    Vue.$message("The player can not run because there is no RTSP information in the asset information.");
                              }
                        }
                  }, {
                        key: "clear",
                        value: function clear() {
                              wemb.posodManager.closeQENXViewer();

                              _get(_getPrototypeOf(CCTVAction.prototype), "clear", this).call(this);
                        }
                  }]);

                  return CCTVAction;
            }(AssetAction);

      var PDUAction =
            /*#__PURE__*/
            function (_AssetAction4) {
                  _inherits(PDUAction, _AssetAction4);

                  function PDUAction(controller) {
                        var _this3;

                        _classCallCheck(this, PDUAction);

                        _this3 = _possibleConstructorReturn(this, _getPrototypeOf(PDUAction).call(this, controller));
                        _this3._onClickPduChildHandler = _this3.onClickPduChlidItem.bind(_assertThisInitialized(_this3));
                        _this3._activeComInstance = null;
                        _this3._activeItem = null;
                        return _this3;
                  }

                  _createClass(PDUAction, [{
                        key: "clear",
                        value: function clear() {
                              // 선 제거
                              this.clearConnectLine(); // 선택한 포트 비활성화

                              this.blurChildAsset(); // 닫기

                              if (this._activeComInstance) {
                                    this._activeComInstance.transitionOut();

                                    this._activeComInstance.offWScriptEvent("itemClick", this._onClickPduChildHandler);

                                    this._activeComInstance = null;
                              }

                              _get(_getPrototypeOf(PDUAction.prototype), "clear", this).call(this);
                        }
                  }, {
                        key: "action",
                        value: function action(comInstance) {
                              var assetInfo = comInstance.data;
                              this._activeComInstance = comInstance;

                              if (comInstance.selected) {
                                    this.clearConnectLine();
                                    comInstance.transitionOut();
                                    comInstance.offWScriptEvent("itemClick", this._onClickPduChildHandler);
                              } else {
                                    comInstance.transitionIn();
                                    comInstance.onWScriptEvent("itemClick", this._onClickPduChildHandler);
                              }
                        }
                        /*
						분전반에서 포트가 선택되는 경우 실행.
						  단계01. 선택 포트 활성화
						  단계02. 선택 포트 = 선택포트와 연결된 자산 선 그리기
						*/

                  }, {
                        key: "onClickPduChlidItem",
                        value: function onClickPduChlidItem(event) {
                              var selectedItem = event.data.selectItem; // 단계01. 선택 포트 활성화 + 팝업창의 포트 활성화 처리

                              this.focusChildAsset(selectedItem); // 단계02. 선택 포트 = 선택포트와 연결된 자산 선 그리기

                              var parentComp = window.wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(selectedItem.data.child_id);

                              if (parentComp) {
                                    // 내부에서 기존에 그려진 선 지우고 다시 그리는 구문이 존재.
                                    this.connectAssetWithLine(this._activeItem, parentComp.comInstance);
                              }
                        }
                        /**
                         * 분전반 라인연결 메서드
                         * @param fromComponent 연결되는 하는 포트
                         * @param toComponent   연결되는 자산컴포넌트
                         * */

                  }, {
                        key: "connectAssetWithLine",
                        value: function connectAssetWithLine(fromComponent, toComponent) {
                              this.clearConnectLine();
                              var hasLeft = fromComponent.name.indexOf("LEFT") > -1 ? true : false;
                              var appendPortSize = fromComponent.size.x / 2;
                              if (hasLeft) appendPortSize *= -1;
                              var from = fromComponent.getWorldPosition();
                              var to = toComponent.appendElement.getWorldPosition(new THREE.Vector3()).clone();
                              from.x += appendPortSize;
                              from.z += fromComponent.size.z / 2; // 포트의 parent참조 pdu패널

                              var pduPanel = fromComponent.parent;
                              var parentHeight = pduPanel.position.y + (pduPanel.size.y + 10);
                              var material = new THREE.LineBasicMaterial({
                                    color: 0x00ff00
                              });
                              var geo = new THREE.Geometry();
                              geo.vertices.push(from.clone(), new THREE.Vector3(from.x, parentHeight, from.z), new THREE.Vector3(to.x, parentHeight, to.z), to.clone());
                              var connectLine = new THREE.Line(geo, material);
                              connectLine.name = AssetComponentControllerPlugin.CONNECT_LINE;
                              window.wemb.scene.add(connectLine);
                        }
                  }, {
                        key: "clearConnectLine",
                        value: function clearConnectLine() {
                              var connectLine = window.wemb.scene.getObjectByName(AssetComponentControllerPlugin.CONNECT_LINE);

                              if (connectLine) {
                                    connectLine.geometry.dispose();
                                    connectLine.material.dispose();
                                    window.wemb.scene.remove(connectLine);
                              }
                        }
                        /*
						2019.02.21(ckkim)
						call : PDU Popup 페이지에서 호출, PDU 유닛에 선택될때, data = 유닛 정보
						 */

                  }, {
                        key: "drawConnectLine",
                        value: function drawConnectLine(data) {
                              // 선택 처리
                              var itemMesh = this._activeComInstance.getChildByAssetName(data.direction + "_" + data.index);

                              var compositionAssetInfo = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(data.child_id);

                              if (compositionAssetInfo && itemMesh) {
                                    this.connectAssetWithLine(itemMesh, compositionAssetInfo.comInstance);
                                    this.focusChildAsset(itemMesh);
                              }
                        }
                        /*
						분전반에서 포트가 클릭되는 경우
						단계01. 기존 포트 비활성화
						단계02. 신규 포트 활성화
						단계03. 팝업 포트 활성화
						*/

                  }, {
                        key: "focusChildAsset",
                        value: function focusChildAsset(selectItem) {
                              if (this._activeItem) {
                                    this._activeItem.selected = false;
                              }

                              selectItem.selected = true;
                              this._activeItem = selectItem;

                              try {
                                    //window.focusPDUMountItem(selectItem.name);
                                    this.eventBus.$emit(AssetAction.Event.FOCUS, selectItem.name);
                              } catch (error) {
                                    console.warn("The mounted port can not be activated because the focusPDUMountItem () method does not exist in the PDU detail popup.", error);
                              }
                        }
                  }, {
                        key: "blurChildAsset",
                        value: function blurChildAsset() {
                              if (this._activeItem) {
                                    this._activeItem.selected = false;
                                    this._activeItem = null;

                                    try {
                                          //window.blurPDUMountItem();
                                          this.eventBus.$emit(AssetAction.Event.BLUR);
                                    } catch (error) {
                                          console.warn("The mounted port can not be disabled because the blurPDUMountItem () method does not exist in the PDU detail popup.", error);
                                    }
                              }
                        }
                  }]);

                  return PDUAction;
            }(AssetAction);

      var RackAction =
            /*#__PURE__*/
            function (_AssetAction5) {
                  _inherits(RackAction, _AssetAction5);

                  function RackAction(controller) {
                        var _this4;

                        _classCallCheck(this, RackAction);

                        _this4 = _possibleConstructorReturn(this, _getPrototypeOf(RackAction).call(this, controller));
                        _this4._onClickRackChildHandler = _this4.onClickRackChildItem.bind(_assertThisInitialized(_this4));
                        _this4._activeComInstance = null;
                        _this4._activeItem = null;
                        return _this4;
                  }

                  _createClass(RackAction, [{
                        key: "clear",
                        value: function clear() {
                              // 활성화 된 아이템을 비활성화
                              this.blurChildAsset();

                              if (this._activeComInstance) {
                                    this._activeComInstance.transitionOut();

                                    this._activeComInstance.offWScriptEvent("itemClick", this._onClickRackChildHandler);

                                    this._activeComInstance = null;
                              }

                              _get(_getPrototypeOf(RackAction.prototype), "clear", this).call(this);
                        }
                  }, {
                        key: "action",
                        value: function action(comInstance) {
                              this._activeComInstance = comInstance;

                              if (comInstance.selected) {
                                    comInstance.transitionOut();
                                    comInstance.offWScriptEvent("itemClick", this._onClickRackChildHandler);
                              } else {
                                    comInstance.transitionIn();
                                    comInstance.onWScriptEvent("itemClick", this._onClickRackChildHandler);
                              }
                        }
                  }, {
                        key: "onClickRackChildItem",
                        value: function onClickRackChildItem(event) {
                              this.toggleChildAsset(event.data.selectItem);
                        }
                  }, {
                        key: "toggleChildAsset",
                        value: function toggleChildAsset(selectItem) {
                              var selected = !selectItem.selected;

                              if (selected) {
                                    this.focusChildAsset(selectItem);
                              } else {
                                    this.blurChildAsset();
                              }
                        }
                        /*
						2019.02.21(ckkim)
						-  call : Rack Popup > loaded 이벤트 리스너에서 랙 유닛이 선택되는 경우 호출
						 */

                  }, {
                        key: "focusChildAssetByAssetId",
                        value: function focusChildAssetByAssetId(assetId) {
                              var itemMesh = this._activeComInstance.getChildByAssetID(assetId);

                              if (itemMesh) {
                                    this.focusChildAsset(itemMesh);
                              } else {
                                    console.warn(assetId + "에 해당하는 자산Mesh가 존재하지 않습니다.");
                              }
                        }
                  }, {
                        key: "focusChildAsset",
                        value: function focusChildAsset(selectItem) {
                              if (this._activeItem) {
                                    this._activeItem.selected = false;

                                    this._activeItem.transitionOut();
                              }

                              selectItem.transitionIn();
                              this._activeItem = selectItem;
                              this._activeItem.selected = true;

                              try {
                                    this.eventBus.$emit(AssetAction.Event.FOCUS, selectItem.data.id); //window.focusRackMountItem(selectItem.data.id);
                              } catch (error) {
                                    console.warn("mounted racks can not be activated because the focusRackMountItem () method does not exist in the Rack Detail popup.", error);
                              }
                        }
                  }, {
                        key: "blurChildAsset",
                        value: function blurChildAsset() {
                              if (this._activeItem) {
                                    this._activeItem.transitionOut();

                                    this._activeItem.selected = false;
                                    this._activeItem = null;

                                    try {
                                          //window.blurRackMountItem();
                                          this.eventBus.$emit(AssetAction.Event.BLUR);
                                    } catch (error) {
                                          console.warn("mounted racks can not be deactivated because the focusRackMountItem () method does not exist in the Rack Detail popup.", error);
                                    }
                              }
                        }
                  }]);

                  return RackAction;
            }(AssetAction);

      var FireEffectAction =
            /*#__PURE__*/
            function (_AssetAction6) {
                  _inherits(FireEffectAction, _AssetAction6);

                  function FireEffectAction(controller) {
                        var _this5;

                        _classCallCheck(this, FireEffectAction);

                        _this5 = _possibleConstructorReturn(this, _getPrototypeOf(FireEffectAction).call(this, controller));
                        _this5._fireEffectMap = new Map();
                        return _this5;
                  }

                  _createClass(FireEffectAction, [{
                        key: "clear",
                        value: function clear() {
                              this._fireEffectMap.forEach(function (fireEffect) {
                                    // 페이지가 닫히는 경우
                                    if (fireEffect) {
                                          fireEffect.destroy();
                                          fireEffect = null;
                                    }
                              });

                              this._fireEffectMap.clear();

                              _get(_getPrototypeOf(FireEffectAction.prototype), "clear", this).call(this);
                        }
                  }, {
                        key: "removeFireEffectByBuildingName",
                        value: function removeFireEffectByBuildingName(buildingNames) {
                              for (var i = 0; i < buildingNames.length; i++) {
                                    var buildingName = buildingNames[i];

                                    if (this._fireEffectMap.has(buildingName) == false) {
                                          return;
                                    }

                                    var effect = this._fireEffectMap.get(buildingName);

                                    effect.destroy();
                                    effect = null;

                                    this._fireEffectMap["delete"](buildingName);
                              }
                        }
                        /*
						불효과 활성화
						@buildingName : 대상 자산이 위치하고 있는 빌딩이름
						*/

                  }, {
                        key: "addFireEffect",
                        value: function addFireEffect(buildingName, comInstance) {
                              // 활성화 된 불 유무 확인
                              if (this._fireEffectMap.has(buildingName) == true) {
                                    return;
                              }

                              var fireEffect = new FireEffect();
                              fireEffect.draw(comInstance);
                              fireEffect.animate();

                              this._fireEffectMap.set(buildingName, fireEffect);
                        } // 싱크 처리하기
                        // 신규 이름과  기존 이름을 비교해 신규 이름에 없는 효과를 제거한다.

                  }, {
                        key: "syncFireEffect",
                        value: function syncFireEffect(buildingNameMap) {
                              var removeNames = [];

                              this._fireEffectMap.forEach(function (value, key) {
                                    // 신규 이름에 기존 목록에 존재하지 않는 경우 제외 대상
                                    if (buildingNameMap.has(key) == false) {
                                          removeNames.push(key);
                                    }
                              });

                              this.removeFireEffectByBuildingName(removeNames);
                        }
                  }]);

                  return FireEffectAction;
            }(AssetAction);

      window.ActionManager = ActionManager;
})();
