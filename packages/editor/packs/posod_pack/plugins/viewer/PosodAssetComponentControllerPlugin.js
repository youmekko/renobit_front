class PosodAssetComponentControllerPlugin extends AssetComponentControllerPlugin {

      constructor() { super(); }

      start() {
            console.log("START_POSOD");
            super.start();
      }

}




//*****
// 컴포넌트 액션 설정
//*****
(function(){


      class ActionManager {

            set currentAction(action) {
                  this._currentAction = action;
            }

            get currentAction() {
                  return this._currentAction;
            }


            constructor() {
                  this._actionMap = null;
                  this._currentAction = null;
                  this._createActionMap();
            }

            _createActionMap() {
                  this._actionMap = new Map();
                  this._actionMap.set(AssetComponentProxy.TYPE.CCTV, new CCTVAction(this));
                  this._actionMap.set(AssetComponentProxy.TYPE.PDU, new PDUAction(this));
                  this._actionMap.set(AssetComponentProxy.TYPE.RACK, new RackAction(this));
                  this._actionMap.set(AssetComponentProxy.TYPE.ACCESS, new AccessAction(this));
                  this._actionMap.set(AssetComponentProxy.TYPE.FIRE, new FireAction(this));
                  this._actionMap.set(AssetComponentProxy.EFFECT.FIRE_EFFECT, new FireEffectAction(this));
            }

            clearCurrentAction() {
                  if (this._currentAction) {
                        this._currentAction.clear();
                        this._currentAction = null;
                  }
            }

            clearFireEffectAction() {
                  let fireEffectAction = this._actionMap.get(AssetComponentProxy.EFFECT.FIRE_EFFECT);
                  if (fireEffectAction) {
                        fireEffectAction.clear();
                  }
            }




            getActionByType(typeName) {
                  // assetAction 정보 구하기
                  let assetAction = this._actionMap.get(typeName);

                  return assetAction;
            }




            /*
          불효과 활성화
           */
            addFireEffect(name, comInstance ) {
                  let fireEffect = this._actionMap.get(AssetComponentProxy.EFFECT.FIRE_EFFECT);
                  fireEffect.addFireEffect(name, comInstance);
            }


            // 타입에 따른 액션 실행.
            executeAction(comInstance) {

                  try {

                        let assetInfo = comInstance.data;


                        let assetAction = this._actionMap.get(assetInfo.asset_type);
                        if (assetAction == null) {
                              return null;
                        }
                        assetAction.action(comInstance);

                  } catch (error) {
                        console.log("ERROR ActionManager executeAction", error);
                  }
            }

      }


      class AssetAction {

            get eventBus(){
                  return this._$eventBus;
            }
            constructor(controller) {
                  this._controller = controller;
                  this._$eventBus = new Vue();
            }


            // action이 실행될때 실행.
            action(comInstance) {

            }

            // 페이지가 닫힐때 실행
            clear() {
                  if(this._$eventBus){
                        this.eventBus.$off();
                  }
            }


            $on(eventName, listener){
                  this._$eventBus.$on(eventName, listener);
            }

            $off(eventName, listener){
                  this._$eventBus.$off(eventName, listener);
            }




            //////////////////////////////////
            // 특정 자산과 연관된 CCTV 자산 목록을 play 리스트로 만들어 실행
            playRelationCCTVList(cctvAssetInfoList){

                  // cctvAssetInfoList에는 cctv assetid만 들어 있기때문에
                  // 이 값을 가지고 자산정보를 구해야함.
                  cctvAssetInfoList = cctvAssetInfoList.map((info)=>{
                        try {
                              let assetInfo = wemb.dcimManager.assetManager.assetFlatMap.get(info.child_id)
                              return assetInfo;
                        }catch(error){
                              console.warn("## ", error);
                        }
                  })


                  if(cctvAssetInfoList.length<=0){
                        return;
                  }


                  /*
                        QENXLivePopViewer 파라메터 생성
                   */
                  try{
                        let playList = cctvAssetInfoList.map((assetInfo)=>{
                              return assetInfo.rtsp_url.replace("rtsp://", `rtsp://${assetInfo.rtsp_id}:${assetInfo.rtsp_pass}@`);
                        })
                        let playListUrlString = playList.join(",");
                        let params = {
                              url: playListUrlString,
                              options: cctvAssetInfoList[0].rtsp_options
                        }
                        wemb.posodManager.openQENXLivePopViewer(params);
                  }catch(error){
                        Vue.$message("The player can not run because there is no RTSP information in the asset information.");
                        console.log("cctvAssetInfoList", cctvAssetInfoList);
                  }


            }

            // 특정 자산과 연관된 CCTV 자산을 선택 상태로
            activeRelationCCTVList(cctvAssetInfoList, relationAssets){

                  let relationAsset, relationData;
                  for( let i=0; i<cctvAssetInfoList.length; i++ ) {
                        relationData = cctvAssetInfoList[i];
                        if(wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.has(relationData.child_id)){
                              relationAsset = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(relationData.child_id);

                              // 화각을 critical로 변경
                              // 선택상태로 ㅂ녀경
                              if(relationAsset.comInstance){
                                    relationAssets.push(relationAsset.comInstance);
                                    relationAsset.comInstance.changeCameraRangeSeverity(WV3DAssetComponent.STATE.CRITICAL);
                                    relationAsset.comInstance.selected = true;
                                    // 선택 아웃라인 비활성화 처리
                                    relationAsset.comInstance.toggleSelectedOutline(false);

                              }
                        }
                  }
            }
      }


      AssetAction.Event = {};
      AssetAction.Event.FOCUS="focus";
      AssetAction.Event.BLUR="blur";
      AssetAction.Event.SELECTED="selected";

      /*
      A. 출입센서 선택시  출입센서 상태가 critical인 경우에만
            1. 연관된 카메라를 critical로 변경
            2.  연관된 카메라.selected = true
                - 마우스 이벤트 영향을 받지 않기 위해


      B. 비선택 시
            1.

       */

      class AccessAction extends AssetAction {
            constructor(controller) {
                  super(controller);

                  this.relationAssets = [];

            }

            async action(comInstance) {

                  let assetId = "";
                  if (comInstance instanceof WV3DAssetComponent) {
                        if (!comInstance.assetId) {
                              console.log("컴포넌트에 자산 정보가 설정되어 있지 않습니다. ")
                              return;
                        }

                        assetId = comInstance.assetId;
                  }


                  // 연관 CCTV 목록 구하기
                  let [error, response] = await window.wemb.dcimManager.getRelationData(assetId);
                  if (error) {
                        CPLogger.log("출입센서 연관자산 데이터 로드 에러", error);
                        return;
                  }


                  // 연관 CCTV PLAy
                  let cctvAssetInfoList = response.data.data;
                  if (cctvAssetInfoList.length <= 0) {
                        return;
                  }

                  this.playRelationCCTVList(cctvAssetInfoList);

                  // critical인 경우만 처리
                  if (comInstance.isCriticalState() == true) {
                        this.activeRelationCCTVList(cctvAssetInfoList, this.relationAssets);
                  }

            }


            clear() {
                  this.relationAssets.forEach((comInstance)=>{
                        comInstance.selected = false;
                  })

                  this.relationAssets= [];


                  wemb.posodManager.closeQENXViewer();

                  super.clear();
            }
      }




      /*
      A. 출입센서 선택시  출입센서 상태가 critical인 경우에만
            1. 연관된 카메라를 critical로 변경
            2.  연관된 카메라.selected = true
                - 마우스 이벤트 영향을 받지 않기 위해
      B. 비선택 시
            1.
       */
      class FireAction extends AssetAction {
            constructor(controller) {
                  super(controller);

                  this.relationAssets = [];

            }

            async action(comInstance) {

                  let assetId ="";
                  if (comInstance instanceof WV3DAssetComponent) {
                        if (!comInstance.assetId) {
                              console.log("컴포넌트에 자산 정보가 설정되어 있지 않습니다. ")
                              return;
                        }

                        assetId = comInstance.assetId;
                  }


                  // 연관 CCTV 목록 구하기
                  let [error, response] = await window.wemb.dcimManager.getRelationData(assetId);
                  if (error) {
                        CPLogger.log("출입센서 연관자산 데이터 로드 에러", error);
                        return;
                  }

                  // 연관 CCTV PLAy
                  let cctvAssetInfoList = response.data.data;
                  if (cctvAssetInfoList.length <= 0) {
                        return;
                  }

                  this.playRelationCCTVList(cctvAssetInfoList);

                  // critical인 경우만 처리
                  if (comInstance.isCriticalState() == true) {
                        this.activeRelationCCTVList(cctvAssetInfoList, this.relationAssets);
                  }
            }


            clear() {
                  this.relationAssets.forEach((comInstance)=>{
                        comInstance.selected = false;
                  })

                  this.relationAssets= [];

                  wemb.posodManager.closeQENXViewer();

                  super.clear();
            }
      }






      class CCTVAction extends AssetAction {

            constructor(controller) {
                  super(controller);

            }

            action(comInstance) {
                  try {
                        let assetInfo = comInstance.data;
                        /**
                         * //실시간 영상
                         *{
                      url:"rtsp://admin:!1q2w3e4r@192.168.1.101:554/onvif/profile1/media.smp",
                      options:" --top_most --size=480,320 --pos=100,85 --title=test"
                 }**/
                        let tempUrl = assetInfo.rtsp_url.replace("rtsp://", `rtsp://${assetInfo.rtsp_id}:${assetInfo.rtsp_pass}@`);
                        let params = {

                              url: tempUrl,
                              options: assetInfo.rtsp_options
                        }

                        wemb.posodManager.openQENXLivePopViewer(params);


                  } catch (error) {

                        //Vue.$message("자산 정보에 RTSP 정보가 존재하지 않아 플레이어를 실행할 수 없습니다.")
                        Vue.$message("The player can not run because there is no RTSP information in the asset information.");

                  }

            }


            clear() {
                  wemb.posodManager.closeQENXViewer();
                  super.clear();
            }
      }


      class PDUAction extends AssetAction {


            constructor(controller) {
                  super(controller);
                  this._onClickPduChildHandler = this.onClickPduChlidItem.bind(this);
                  this._activeComInstance = null;
                  this._activeItem = null;
            }

            clear() {
                  // 선 제거
                  this.clearConnectLine();
                  // 선택한 포트 비활성화
                  this.blurChildAsset();
                  // 닫기
                  if (this._activeComInstance) {
                        this._activeComInstance.transitionOut();
                        this._activeComInstance.offWScriptEvent("itemClick", this._onClickPduChildHandler);
                        this._activeComInstance = null;
                  }

                  super.clear();
            }

            action(comInstance) {

                  let assetInfo = comInstance.data;
                  this._activeComInstance = comInstance;
                  if (comInstance.selected) {
                        this.clearConnectLine();
                        comInstance.transitionOut();
                        comInstance.offWScriptEvent("itemClick", this._onClickPduChildHandler);
                  } else {
                        comInstance.transitionIn();
                        comInstance.onWScriptEvent("itemClick", this._onClickPduChildHandler);
                  }


            }

            /*
          분전반에서 포트가 선택되는 경우 실행.
              단계01. 선택 포트 활성화
              단계02. 선택 포트 = 선택포트와 연결된 자산 선 그리기
           */
            onClickPduChlidItem(event) {
                  let selectedItem = event.data.selectItem;
                  // 단계01. 선택 포트 활성화 + 팝업창의 포트 활성화 처리
                  this.focusChildAsset(selectedItem);

                  // 단계02. 선택 포트 = 선택포트와 연결된 자산 선 그리기
                  let parentComp = window.wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(selectedItem.data.child_id)
                  if (parentComp) {
                        // 내부에서 기존에 그려진 선 지우고 다시 그리는 구문이 존재.
                        this.connectAssetWithLine(this._activeItem, parentComp.comInstance);
                  }
            }

            /**
             * 분전반 라인연결 메서드
             * @param fromComponent 연결되는 하는 포트
             * @param toComponent   연결되는 자산컴포넌트
             * */
            connectAssetWithLine(fromComponent, toComponent) {
                  this.clearConnectLine();
                  let hasLeft = fromComponent.name.indexOf("LEFT") > -1 ? true : false;
                  let appendPortSize = fromComponent.size.x / 2;
                  if (hasLeft) appendPortSize *= -1;
                  let from = fromComponent.getWorldPosition();
                  let to = toComponent.appendElement.getWorldPosition(new THREE.Vector3).clone();
                  from.x += appendPortSize;
                  from.z += fromComponent.size.z / 2;

                  // 포트의 parent참조 pdu패널
                  let pduPanel = fromComponent.parent;
                  let parentHeight = pduPanel.position.y + (pduPanel.size.y + 10);

                  let material = new THREE.LineBasicMaterial({ color: 0x00ff00 });
                  let geo = new THREE.Geometry();
                  geo.vertices.push(
                        from.clone(),
                        new THREE.Vector3(from.x, parentHeight, from.z),
                        new THREE.Vector3(to.x, parentHeight, to.z),
                        to.clone()
                  );
                  let connectLine = new THREE.Line(geo, material);
                  connectLine.name = AssetComponentControllerPlugin.CONNECT_LINE;
                  window.wemb.scene.add(connectLine);
            }

            clearConnectLine() {
                  let connectLine = window.wemb.scene.getObjectByName(AssetComponentControllerPlugin.CONNECT_LINE);
                  if (connectLine) {
                        connectLine.geometry.dispose();
                        connectLine.material.dispose();
                        window.wemb.scene.remove(connectLine);
                  }
            }



            /*
            2019.02.21(ckkim)
            call : PDU Popup 페이지에서 호출, PDU 유닛에 선택될때, data = 유닛 정보
             */
            drawConnectLine(data){
                  // 선택 처리
                  var itemMesh=this._activeComInstance.getChildByAssetName(data.direction+"_"+data.index);
                  var compositionAssetInfo = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(data.child_id);

                  if(compositionAssetInfo && itemMesh){
                        this.connectAssetWithLine(itemMesh, compositionAssetInfo.comInstance);
                        this.focusChildAsset(itemMesh);
                  }
            }
            /*
          분전반에서 포트가 클릭되는 경우
          단계01. 기존 포트 비활성화
          단계02. 신규 포트 활성화
          단계03. 팝업 포트 활성화

           */
            focusChildAsset(selectItem) {

                  if (this._activeItem) {
                        this._activeItem.selected=false;
                  }


                  selectItem.selected = true;
                  this._activeItem = selectItem;

                  try {
                        //window.focusPDUMountItem(selectItem.name);
                        this.eventBus.$emit(AssetAction.Event.FOCUS, selectItem.name);


                  } catch (error) {
                        console.warn("The mounted port can not be activated because the focusPDUMountItem () method does not exist in the PDU detail popup.", error);
                  }
            }

            blurChildAsset() {
                  if (this._activeItem) {
                        this._activeItem.selected=false;

                        this._activeItem = null;

                        try {
                              //window.blurPDUMountItem();
                              this.eventBus.$emit(AssetAction.Event.BLUR);
                        } catch (error) {
                              console.warn("The mounted port can not be disabled because the blurPDUMountItem () method does not exist in the PDU detail popup.", error);
                        }
                  }
            }


      }


      class RackAction extends AssetAction {


            constructor(controller) {
                  super(controller);
                  this._onClickRackChildHandler = this.onClickRackChildItem.bind(this);
                  this._activeComInstance = null;
                  this._activeItem = null;
            }

            clear() {

                  // 활성화 된 아이템을 비활성화
                  this.blurChildAsset();
                  if (this._activeComInstance) {
                        this._activeComInstance.transitionOut();
                        this._activeComInstance.offWScriptEvent("itemClick", this._onClickRackChildHandler);
                        this._activeComInstance = null;

                  }

                  super.clear();
            }


            action(comInstance) {
                  this._activeComInstance = comInstance;
                  if (comInstance.selected) {
                        comInstance.transitionOut();
                        comInstance.offWScriptEvent("itemClick", this._onClickRackChildHandler);
                  } else {
                        comInstance.transitionIn();
                        comInstance.onWScriptEvent("itemClick", this._onClickRackChildHandler);
                  }
            }

            onClickRackChildItem(event) {
                  this.toggleChildAsset(event.data.selectItem);
            }

            toggleChildAsset(selectItem) {
                  let selected = !selectItem.selected;
                  if (selected) {
                        this.focusChildAsset(selectItem);
                  } else {
                        this.blurChildAsset();

                  }
            }

            /*
            2019.02.21(ckkim)
            -  call : Rack Popup > loaded 이벤트 리스너에서 랙 유닛이 선택되는 경우 호출
             */
            focusChildAssetByAssetId(assetId){
                  let itemMesh = this._activeComInstance.getChildByAssetID(assetId);
                  if(itemMesh){
                        this.focusChildAsset(itemMesh);
                  }else{
                        console.warn(assetId+"에 해당하는 자산Mesh가 존재하지 않습니다.");
                  }
            }

            focusChildAsset(selectItem) {
                  if (this._activeItem) {
                        this._activeItem.selected = false;
                        this._activeItem.transitionOut();
                  }
                  selectItem.transitionIn();
                  this._activeItem = selectItem;
                  this._activeItem.selected = true;
                  try {
                        this.eventBus.$emit(AssetAction.Event.FOCUS, selectItem.data.id);
                        //window.focusRackMountItem(selectItem.data.id);
                  } catch (error) {
                        console.warn("mounted racks can not be activated because the focusRackMountItem () method does not exist in the Rack Detail popup.", error);
                  }
            }

            blurChildAsset() {
                  if (this._activeItem) {
                        this._activeItem.transitionOut();
                        this._activeItem.selected=false;
                        this._activeItem = null;

                        try {
                              //window.blurRackMountItem();
                              this.eventBus.$emit(AssetAction.Event.BLUR);
                        } catch (error) {
                              console.warn("mounted racks can not be deactivated because the focusRackMountItem () method does not exist in the Rack Detail popup.", error);
                        }
                  }
            }
      }


      class FireEffectAction extends AssetAction {
            constructor(controller) {
                  super(controller);
                  this._fireEffectMap = new Map();
            }

            clear() {
                  this._fireEffectMap.forEach((fireEffect) => {
                        // 페이지가 닫히는 경우
                        if (fireEffect) {
                              fireEffect.destroy();
                              fireEffect = null;
                        }
                  })

                  this._fireEffectMap.clear();


                  super.clear();
            }



            removeFireEffectByBuildingName(buildingNames) {
                  for (let i = 0; i < buildingNames.length; i++) {
                        let buildingName = buildingNames[i];
                        if (this._fireEffectMap.has(buildingName) == false) {
                              return;
                        }

                        let effect = this._fireEffectMap.get(buildingName);
                        effect.destroy();
                        effect = null;
                        this._fireEffectMap.delete(buildingName);
                  }
            }


            /*
          불효과 활성화
          @buildingName : 대상 자산이 위치하고 있는 빌딩이름
           */
            addFireEffect(buildingName, comInstance ) {
                  // 활성화 된 불 유무 확인
                  if (this._fireEffectMap.has(buildingName) == true) {
                        return;
                  }


                  let fireEffect = new FireEffect();
                  fireEffect.draw(comInstance);
                  fireEffect.animate();
                  this._fireEffectMap.set(buildingName, fireEffect);
            }


            // 싱크 처리하기
            // 신규 이름과  기존 이름을 비교해 신규 이름에 없는 효과를 제거한다.
            syncFireEffect(buildingNameMap){

                  let removeNames = [];
                  this._fireEffectMap.forEach((value, key)=>{
                        // 신규 이름에 기존 목록에 존재하지 않는 경우 제외 대상
                        if(buildingNameMap.has(key)==false){
                              removeNames.push(key);
                        }
                  })

                  this.removeFireEffectByBuildingName(removeNames);
            }
      }

      window.ActionManager = ActionManager;
})();
