"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/**
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□

 주의:
 extension_files.json에 dependency로 AssetComponentProxy를 추가하는 경우
 등록되지 않는 문제 발생.
 우선 파일을 나누지 않음.


 2018.10.21(ckkim)
 - AssetComponentProxy 이름을 변경해야함.


 2018.10.01 (ckkim)
 RENOBIT 에디터에 동적으로 추가되는 Proxy
 생성:
 - AssetEditorPlugin.js에서 RENOBIT Eidtdor의 Proxy으로 동적추가

 주 용도:
 - RENOBIT Editor와 자산 관련 플러그인, 패널간의 데이터 연동 처리
 예)
 - 페이지 저장 후 자산 배치 정보  저장하기 등
 */
var AssetComponentProxy =
      /*#__PURE__*/
      function (_window$puremvc$Proxy) {
            _inherits(AssetComponentProxy, _window$puremvc$Proxy);

            function AssetComponentProxy() {
                  var _this;

                  _classCallCheck(this, AssetComponentProxy);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(AssetComponentProxy).call(this, AssetComponentProxy.NAME)); // 자산 타입 정보

                  _this._assetTypes = []; // 타입별 자산 정보

                  /*
                  AccessSensor: Array(27)
                  CCTV: Array(0)
                  FireSensor: Array(0)
                  LeakSensor: Array(0)
                  Pdu: Array(0)
                  Rack: Array(0)
                  length: 0
                   */

                  _this._assets = [];
                  _this._assetFlat = [];
                  _this._assetComponentsFlat = [];
                  _this.assets2dComponentsInfo = {};
                  _this.assets3dComponentsInfo = {};
                  _this._assetsDataManager = new AssetsDataManager();
                  _this._fields = {
                        "common": [],
                        "type": {}
                  };
                  _this._visibleFields = {
                        "common": [],
                        "type": {}
                  };
                  /*
                  컴포넌트 id를 키로 자산 정보가 맵핑.
                   */

                  _this._componentIdAssetFlatMap = new Map();
                  /*
                  {
                      key:componentName,
                      data:asset_Type
                   */

                  _this.asset2DTypeInfoMap = new Map();
                  _this.asset3DTypeInfoMap = new Map();
                  /* assetFlat array => map
                  {
                      key:asset_id,
                      data:asset_item
                   */

                  _this._assetFlatMap = new Map();
                  /*
                      2018.11.06(ckkim)
                      페이지별 배치되어 있는 자산 정보 리스트를 가지고 있는 맵
                      {
                          key:page_id,
                          data:{
                              key:asset_id,
                              data:asset_item
                          }
                       */

                  _this._assetInfosInPageMap = new Map();
                  /* 페이지 별 배치되어 있는 자산 타입 리스트를 가지고 있는 맵 */

                  /*
                      {
                          key:page_id,
                          data:[asset_type
                      },{
                          ....
                      }
                   */

                  _this._assetTypesGroupListMap = new Map();
                  return _this;
            }
            /*
            데이터 관리자
             */


            _createClass(AssetComponentProxy, [{
                  key: "getTypeFields",
                  value: function getTypeFields(type, isOnlyVisibleField) {
                        var targetData = isOnlyVisibleField === true ? this._visibleFields : this._fields;
                        var commonFields = targetData.common;
                        var typeFields = this.getOnlyTypeFields(type);
                        return commonFields.concat(typeFields);
                  }
            }, {
                  key: "getOnlyTypeFields",
                  value: function getOnlyTypeFields(type, isOnlyVisibleField) {
                        var targetData = isOnlyVisibleField === true ? this._visibleFields : this._fields;
                        var typeFields = targetData.type[type] || [];
                        return typeFields;
                  }
            }, {
                  key: "getDeployedAssets",
                  value: function getDeployedAssets() {
                        var assets = {};

                        for (var key in this._assets) {
                              var typeAsset = this._assets[key];
                              assets[key] = typeAsset.filter(function (x) {
                                    return x.instance_id;
                              });
                        }

                        return assets;
                  }
            }, {
                  key: "getAssetListByType",
                  value: function getAssetListByType(type) {
                        return this._assets[type] || [];
                  }
            }, {
                  key: "loadAssetComponentsInfo",

                  /*
                  컴포넌트 정보 및 환경설정 정보 읽기
                   */
                  value: function loadAssetComponentsInfo() {
                        var _this2 = this;

                        var param, comp_res, compsData, index, typeData;
                        return regeneratorRuntime.async(function loadAssetComponentsInfo$(_context) {
                              while (1) {
                                    switch (_context.prev = _context.next) {
                                          case 0:
                                                param = {
                                                      "id": "assetComponentService.getComponentList",
                                                      "params": {}
                                                };
                                                _context.next = 3;
                                                return regeneratorRuntime.awrap(wemb.$http.post(window.wemb.dcimManager.path, param));

                                          case 3:
                                                comp_res = _context.sent;
                                                compsData = comp_res.data.data;
                                                this._assetComponentsFlat = [];
                                                this.asset2DTypeInfoMap.clear();
                                                this.asset3DTypeInfoMap.clear();

                                                try {
                                                      for (index in compsData) {
                                                            typeData = compsData[index];
                                                            this.assets3dComponentsInfo[typeData.id] = typeData.children.filter(function (x) {
                                                                  return x.data.layer == "3D";
                                                            });
                                                            this.assets2dComponentsInfo[typeData.id] = typeData.children.filter(function (x) {
                                                                  return x.data.layer == "2D";
                                                            });
                                                            /*
                                                            2018.11.06(ckkim)
                                                            컴포넌트 별 => 타입 값 할당하기
                                                            컴포넌트 이름을 키 값으로 자산 타입정보 저장하기
                                                               */

                                                            typeData.children.forEach(function (item) {
                                                                  if (item.data.layer == "2D") {
                                                                        _this2.asset2DTypeInfoMap.set(item.data.file_id, item.data);

                                                                        _this2._assetComponentsFlat.push(item);
                                                                  }

                                                                  if (item.data.layer == "3D") {
                                                                        _this2.asset3DTypeInfoMap.set(item.data.file_id, item.data);

                                                                        _this2._assetComponentsFlat.push(item);
                                                                  }
                                                            });
                                                      }
                                                } catch (error) {
                                                      console.log("The asset component information is not loaded properly.");
                                                }

                                          case 9:
                                          case "end":
                                                return _context.stop();
                                    }
                              }
                        }, null, this);
                  }
            }, {
                  key: "getAssetTypeByAssetCompName",
                  value: function getAssetTypeByAssetCompName(compName) {
                        var name = '';

                        try {
                              name = this._assetComponentsFlat.find(function (x) {
                                    return x.data.name == compName;
                              }).data.asset_type;
                        } catch (error) {
                              console.log(error);
                        }

                        return name;
                  }
            }, {
                  key: "_updateAssetTypes",
                  value: function _updateAssetTypes(data) {
                        this._assetTypes.splice(0, this._assetTypes.length);

                        try {
                              var _this$_assetTypes;

                              (_this$_assetTypes = this._assetTypes).push.apply(_this$_assetTypes, _toConsumableArray(data.types));

                              this._updateFieldData(data.common_fields, data.types);
                        } catch (error) {
                              console.log("Asset type information is not loaded properly.");
                        }
                  }
            }, {
                  key: "_updateFieldData",
                  value: function _updateFieldData(common, types) {
                        common = common || [];
                        types = types || {};
                        this._fields.common = common;
                        this._visibleFields.common = common.filter(function (x) {
                              return x.visible == true;
                        });

                        for (var index in types) {
                              var typeData = types[index];
                              this._fields.type[typeData.id] = typeData.fields.type_fields || [];
                              this._visibleFields.type[typeData.id] = this._fields.type[typeData.id].filter(function (x) {
                                    return x.visible == true;
                              });
                        }
                  }
                  /*
                  전체 자산 정보 읽기
                        1. 전체 자산 정보 읽기
                        2. 자산 타입 정보 및 필드 정보 생성
                        3. 데이터 메니저에 자산 정보 업데이트(주입)
                        4. 자산 id를 키로 자산 그룹 맵 만들기
                        5. 컴포넌트 id키로  맵과 페이지 id를 키로 맵 만들기
                        6. 페이지 id를 키로 자산 타입 그룹 맵 만들기
                   */

            }, {
                  key: "loadAssetsData",
                  value: function loadAssetsData() {
                        var param, result, assets, ary, key, items;
                        return regeneratorRuntime.async(function loadAssetsData$(_context2) {
                              while (1) {
                                    switch (_context2.prev = _context2.next) {
                                          case 0:
                                                param = {
                                                      "id": "assetDataService.getAllData",
                                                      "params": {}
                                                }; // 장비에 대한 자산테이블(a_..)하고 join
                                                // 자산 테이블에 대한 컬럼명은 tb_asset_type에 있는 필드명 참조

                                                _context2.next = 3;
                                                return regeneratorRuntime.awrap(wemb.$http.post(window.wemb.dcimManager.path, param));

                                          case 3:
                                                result = _context2.sent;
                                                assets = result.data;
                                                this._assets = {};
                                                this._assetFlat = [];

                                                try {
                                                      // 자산 타입 정보 및 필드 정보 생성
                                                      this._updateAssetTypes(assets.data.schema);

                                                      this._assets = assets.data.result;
                                                      ary = [];

                                                      for (key in this._assets) {
                                                            items = this._assets[key];
                                                            ary = ary.concat(items);
                                                      }

                                                      this._assetFlat = ary;

                                                      this._assetsDataManager.updateAssetsData(this.assets, this._assetFlat); // 2018.11.06(ckkim)
                                                      // 자산 id를 키로 그룹 맵 만들기


                                                      this._createAssetMaps(); // 컴포넌트 id키로  맵과 페이지 id를 키로 맵 만들기


                                                      this._createComponentIdMapAndAssetInfoInPageMap(); // 페이지 id를 키로 자산 타입 그룹 맵 만들기


                                                      this._createAssetTypesInPageMap();
                                                } catch (error) {
                                                      console.log("Asset data does not load properly.");
                                                }

                                          case 8:
                                          case "end":
                                                return _context2.stop();
                                    }
                              }
                        }, null, this);
                  }
                  /*
                  assetId에 해당하는  자산 정보 구하기
                  1. 서버에서 자산 정보 구하기
                  2. 자산 정보 업데이트
                    */

            }, {
                  key: "updateAssetDataBy",
                  value: function updateAssetDataBy(assetId, assetType) {
                        var param, newAssetData, result, info, targetAssetData, encTypes, encNames, paramKey, i;
                        return regeneratorRuntime.async(function updateAssetDataBy$(_context3) {
                              while (1) {
                                    switch (_context3.prev = _context3.next) {
                                          case 0:
                                                param = {
                                                      "id": "assetDataService.getData",
                                                      "params": {
                                                            "id": assetId,
                                                            "asset_type": assetType
                                                      }
                                                };
                                                newAssetData = null;
                                                _context3.prev = 2;
                                                _context3.next = 5;
                                                return regeneratorRuntime.awrap(wemb.$http.post(window.wemb.dcimManager.path, param));

                                          case 5:
                                                result = _context3.sent;
                                                info = result.data;

                                                if (!(info.data.count == 0)) {
                                                      _context3.next = 10;
                                                      break;
                                                }

                                                console.log("데이터가 존재하지 않습니다.");
                                                return _context3.abrupt("return", false);

                                          case 10:
                                                if (!info.data.result) {
                                                      _context3.next = 14;
                                                      break;
                                                }

                                                newAssetData = info.data.result[0];
                                                _context3.next = 19;
                                                break;

                                          case 14:
                                                if (!info.data) {
                                                      _context3.next = 18;
                                                      break;
                                                }

                                                newAssetData = info.data[0];
                                                _context3.next = 19;
                                                break;

                                          case 18:
                                                return _context3.abrupt("return", false);

                                          case 19:
                                                _context3.next = 25;
                                                break;

                                          case 21:
                                                _context3.prev = 21;
                                                _context3.t0 = _context3["catch"](2);
                                                console.log("Asset data does not load properly.");
                                                return _context3.abrupt("return", false);

                                          case 25:
                                                targetAssetData = this.getAssetDataByAssetId(assetId);

                                                if (!targetAssetData) {
                                                      _context3.next = 34;
                                                      break;
                                                }

                                                encTypes = window.wemb.dcimManager._assetComponentProxy.fields.type[assetType].filter(function (asset) {
                                                      return asset.encryption;
                                                });
                                                encNames = encTypes.map(function (type) {
                                                      return type.name;
                                                });
                                                paramKey = Object.getOwnPropertyNames(newAssetData);

                                                for (i = 0; encNames.length > 0 && i < paramKey.length; i++) {
                                                      if (encNames.indexOf(paramKey[i]) > -1) {
                                                            newAssetData[paramKey[i]] = Base64.decode(newAssetData[paramKey[i]]);
                                                      }
                                                } //2. 자산 정보 업데이트


                                                $.extend(true, targetAssetData, newAssetData);
                                                console.log("업데이트  성공", newAssetData);
                                                return _context3.abrupt("return", targetAssetData);

                                          case 34:
                                                return _context3.abrupt("return", false);

                                          case 35:
                                          case "end":
                                                return _context3.stop();
                                    }
                              }
                        }, null, this, [[2, 21]]);
                  }
                  /*
                  asset id에 해당하는 asset data 찾기
                   */

            }, {
                  key: "getAssetDataByAssetId",
                  value: function getAssetDataByAssetId(assetId) {
                        return this.assetFlat.find(function (item) {
                              return item.id == assetId;
                        });
                  }
                  /*
                  자식 자산 정보 가져오기
                   */

            }, {
                  key: "getAssetChildren",
                  value: function getAssetChildren(parentId) {
                        return this.assetFlat.filter(function (item) {
                              return item.parent_id == parentId;
                        });
                  }
                  /*
                    sensor:3
                      */

            }, {
                  key: "getAssetCountListInPage",
                  value: function getAssetCountListInPage(typeList, pageId) {
                        var _this3 = this;

                        var result = {};
                        typeList.forEach(function (typeName) {
                              result[typeName] = 0;
                        });
                        typeList.forEach(function (typeName) {
                              result[typeName] = _this3.getAssetListByType(typeName).filter(function (assetInfo) {
                                    return assetInfo.page_id == pageId;
                              }).length;
                        });
                        return result;
                  }
                  /*
                   2018.11.06(ckkim)
                   페이지별 자산 정보 Map
                      key: pageId:{
                        key: assetid: {
                          자산 정보
                        },
                      }
                      key: pageId:{
                        key: assetid: {
                          자산 정보
                        },
                      }
                   조건:
                      페이지에 배치되어 있는 자산만 해당함.
                    */

            }, {
                  key: "_createAssetMaps",
                  value: function _createAssetMaps() {
                        this._assetFlatMap.clear(); // page_id를 키로


                        for (var i = 0; i < this.assetFlat.length; i++) {
                              var assetInfo = this.assetFlat[i];
                              assetInfo.valid_state = assetInfo.valid_state || "ok"; //ok, none, double

                              this._assetFlatMap.set(assetInfo.id, assetInfo);
                        }
                  }
                  /*
                  자산 정보를 읽은 후
                  - 컴포넌트 id를 키로 자산 정보 맵 만들기
                  - 페이지id를 키로 자산 정보 맵 만들기
                   */

            }, {
                  key: "_createComponentIdMapAndAssetInfoInPageMap",
                  value: function _createComponentIdMapAndAssetInfoInPageMap() {
                        this._assetInfosInPageMap.clear();

                        this._componentIdAssetFlatMap.clear(); // page_id를 키로


                        for (var i = 0; i < this.assetFlat.length; i++) {
                              var assetInfo = this.assetFlat[i]; // page_id, instance_id 가 없는 경우 자산 배치가 되지 않은 것으로 판단

                              if (assetInfo.page_id == null || assetInfo.instance_id == null) {
                                    continue;
                              } // 컴포넌트id별 자산 정보 맵과 페이지별 자산 정보 맵에 정보 추가


                              this.addComponentIdMapAndAssetInfoInPageMap(assetInfo);
                        } //console.log("saveAssetInfo loadAssetsData Group creation complete", this._assetInfosInPageMap);

                  }
                  /*
                  페이지별 자산 타입 리스트
                   */

            }, {
                  key: "_createAssetTypesInPageMap",
                  value: function _createAssetTypesInPageMap() {
                        this._assetTypesGroupListMap.clear();

                        var pageInfoList = wemb.pageManager.getPageInfoList();

                        for (var i = 0; i < pageInfoList.length; i++) {
                              // 페이지 정보 구하기
                              var pageInfo = pageInfoList[i]; // 페이지별 자산 타입 목록 만들기

                              this.updateAssetTypesInPageMap(pageInfo.id);
                        }
                  }
                  /*
                  2018.11.12(ckkim)
                  assetId에 해당하는 자산 정보 구하기
                   */

            }, {
                  key: "getAssetData",
                  value: function getAssetData(assetId) {
                        if (this._assetFlatMap.has(assetId) == false) {
                              return null;
                        }

                        return this._assetFlatMap.get(assetId);
                  }
                  /*
                  assetId에 바인딩되어 있는 컴포넌트 ID
                   */

            }, {
                  key: "getComponentInstanceIdBy",
                  value: function getComponentInstanceIdBy(assetId) {
                        var assetInfo = this.getAssetData(assetId);
                        if (assetInfo == null) return null;

                        if (assetInfo.hasOwnProperty("instance_id") && assetInfo.instance_id.length > 0) {
                              return assetInfo.instance_id;
                        }

                        return null;
                  }
                  /*
                          페이지별 자산 정보 만들기
                          call : 페이지
                   */

            }, {
                  key: "updateAssetInfosGroupListMap",
                  value: function updateAssetInfosGroupListMap() {
                        this._createAssetMaps();
                  }
                  /*
                  일반 컴포넌트 정보에 환경설정 정보 적용하기
                    */

            }, {
                  key: "attachComponentConfigInfoToComInstanceInfo",
                  value: function attachComponentConfigInfoToComInstanceInfo(comInstanceInfo) {
                        // 3D 컴포넌트 체크
                        if (comInstanceInfo.layerName != "threeLayer") {
                              return;
                        } // 라이브러리에서 자산 file_id 구하기


                        var file_id = wemb.componentLibraryManager.getComponentLabelByName("3D", comInstanceInfo.componentName);

                        if (this.asset3DTypeInfoMap.has(file_id) == false) {
                              return;
                        } // 컴포넌트 정보 구하기


                        var componentConfigInfo = this.asset3DTypeInfoMap.get(file_id); // 팝업 아이디 설정하기

                        if (comInstanceInfo.props.hasOwnProperty("settings") == false) {
                              comInstanceInfo.props.settings = {
                                    popupId: ""
                              };
                        } // 환경설정 팝업 아이디 추가


                        comInstanceInfo.props.settings.popupId = componentConfigInfo.props.initProperties.props.settings.popupId; // 환경설정 3d 맵핑 정보 추가

                        comInstanceInfo.props.resource = componentConfigInfo.props.initProperties.props.resource;
                  }
                  /*
                  자산 정보에서 target컴포넌트 id에 할당되어 있는 자산 정보를 컴포넌트에 설정하기
                  주로 컴포넌트 인스턴스 정보에 자산 정보를 사용할때 사용.
                    */

            }, {
                  key: "attachBindingAssetDataToComInstanceInfo",
                  value: function attachBindingAssetDataToComInstanceInfo(targetComInstanceInfo) {
                        // 자산 id 및 자산 정보 설정하기
                        if (this._componentIdAssetFlatMap.has(targetComInstanceInfo.id) == true) {
                              targetComInstanceInfo.props.setter.data = this._componentIdAssetFlatMap.get(targetComInstanceInfo.id);
                        } else {
                              targetComInstanceInfo.props.setter.data = {};
                        }
                  }
            }, {
                  key: "attachAssetDataToComInstanceInfo",
                  value: function attachAssetDataToComInstanceInfo(data, targetComInstanceInfo) {
                        // 자산 id 및 자산 정보 설정하기
                        if (data) {
                              targetComInstanceInfo.props.setter.data = data;
                        } else {
                              targetComInstanceInfo.props.setter.data = {};
                        }
                  } //////////////////////////////////////////////////
                  //////////////////////////////////////////////////

                  /*
                    2018.11.06(ckkim)
                    openPageId 정보를 기준으로 페이지 별로 중복 자산 찾기
                     @openPageId : 열린 페이지
                    @targetAssetList : 자산 타입 리스트
                      call :
                        외부에서 호출
                     result:
                        결과의 크기가 0개가 아닌 경우 중복된 상태임.
                        return [{page_id:"",page_name:"",data:[]},...]
                     */

            }, {
                  key: "checkDuplicatedAsset",
                  value: function checkDuplicatedAsset(openPageId, targetCompositionInfoList) {
                        var result = {
                              pageList: [],
                              assetList: []
                        };
                        var pageIdList = Array.from(this._assetInfosInPageMap.keys()); // 편집한 페이지 리스트를 기준으로 중복 배치 자산 찾기 시작

                        for (var i = 0; i < pageIdList.length; i++) {
                              var _pageId = pageIdList[i]; // 검색중인 페이지와 현재 저장하려고 하는 페이지가 동일한 경우 패스

                              if (openPageId == _pageId) {
                                    continue;
                              } // pageId로 그룹 알아내기 .


                              var allocationAssetGroupMap = this._assetInfosInPageMap.get(_pageId); // 중복 배치 유무 판단하기.


                              var selectedAssetList = []; // 중복 배치 자산 정보를 담을 배열 변수.

                              /*
                                  배치한 자산 정보와 페이지에 배치된 자산 정보 목록과 비교해 중복 배치 유무 판단하기.
                                  @targetAssetList = 배치한 자산 정보
                                  @allocationAssetGroup = 페이지에 배치된 자산 정보 목록이 들어 있음.
                                   */

                              var _loop = function _loop(m) {
                                    // let compositionInfo = targetCompositionInfoList[m];
                                    var assetId = targetCompositionInfoList[m].assetId; // 해당 자산이 현재 검사하고 있는 페이지에 배치되었는지 유무  판다.

                                    if (allocationAssetGroupMap.has(assetId) == false) {
                                          return "continue";
                                    } // 중복 배치 자산인 경우 자산 정보 저장하기


                                    selectedAssetList.push(allocationAssetGroupMap.get(assetId)); ///////////////

                                    var tempItem = result.assetList.find(function (item) {
                                          return item.id == assetId;
                                    }); // 중복된 자산 배치 목록만을 구하기 위해 맵에 저장

                                    if (tempItem != null) {
                                          result.assetList.push(allocationAssetGroupMap.get(assetId));
                                    }
                              };

                              for (var m = 0; m < targetCompositionInfoList.length; m++) {
                                    var _ret = _loop(m);

                                    if (_ret === "continue") continue;
                              } // 중복 배치가 있는 경우 result에 추가하기


                              if (selectedAssetList.length > 0) {
                                    var pageName = wemb.pageManager.getPageNameById(_pageId) || "";
                                    result.pageList.push({
                                          page_id: _pageId,
                                          page_name: pageName,
                                          list: selectedAssetList
                                    });
                              }
                        }

                        return result;
                  }
                  /*
                    pageId에 배치되어 있는  자산 타입 목록
                     */

            }, {
                  key: "getAssetTypesInPage",
                  value: function getAssetTypesInPage(pageId) {
                        return this._assetTypesGroupListMap.get(pageId);
                  }
            }, {
                  key: "getAssetsInPage",
                  value: function getAssetsInPage(pageId) {
                        return this._assetFlat.filter(function (x) {
                              return x.page_id == pageId;
                        });
                  }
            }, {
                  key: "getTypeAssetsInPage",
                  value: function getTypeAssetsInPage(type, pageId) {
                        return this.getAssetsInPage(pageId).filter(function (x) {
                              return x.asset_type == type;
                        });
                  } //자산이 배치되어 있는 페이지 ID

            }, {
                  key: "getPageIdPlaced",
                  value: function getPageIdPlaced(assetId) {
                        if (this._assetFlatMap.has(assetId) == true) {
                              var assetInfo = this._assetFlatMap.get(assetId);

                              try {
                                    if (assetInfo) return assetInfo.page_id;
                              } catch (error) {
                                    console.warn("@@ warnning AssetComponentProxy.js, getAssetPageId ", pageId);
                                    return null;
                              }
                        }

                        return null;
                  }
                  /*
                    2018.11.06(ckkim)
                    PageId에 assetId가 배치되었는지 유무 판단하기
                    단, 배치되어 있는 자산 정보만을 판단함.
                     call :
                        주로 페이지 저장 전 중복 배치 유무 판단시에 사용
                        AssetPagePlugin.js
                     */

            }, {
                  key: "hasAssetInPage",
                  value: function hasAssetInPage(assetId, pageId) {
                        if (this._assetInfosInPageMap.has(pageId) == false) return false;

                        var assetInfoGroupMap = this._assetInfosInPageMap.get(pageId);

                        if (assetInfoGroupMap.has(assetId) == false) {
                              return false;
                        }

                        return true;
                  }
                  /*
                  자산이 추가, 삭제 될때 마다 업데이트
                   */

            }, {
                  key: "updateAssetTypesInPageMap",
                  value: function updateAssetTypesInPageMap(pageId) {
                        // 타입 리스트 만들기
                        var typeList = []; // 페이지에 배치되어 있는 자산 정보를 기반으로 타입 구하기

                        var assetInfoListMap = this._assetInfosInPageMap.get(pageId);

                        if (assetInfoListMap && assetInfoListMap.size > 0) {
                              assetInfoListMap.forEach(function (item, key) {
                                    if (typeList.indexOf(item.asset_type) == -1) {
                                          typeList.push(item.asset_type);
                                    }
                              });
                        }

                        this._assetTypesGroupListMap.set(pageId, typeList);
                  }
                  /*
                  자산이 동적으로 배치되는 경우
                  - 자산 정보 업데이트
                        자산에 instance_id, page_id 정보 추가
                  - 페이지별 자산 정보 업데이트
                   - 페이지별 타입 정보 업데이트
                    */

            }, {
                  key: "addComponentIdMapAndAssetInfoInPageMap",
                  value: function addComponentIdMapAndAssetInfoInPageMap(assetInfo) {
                        // - 자산 정보 업데이트
                        // 컴포넌트 instance_id로 자산 정보 맵핑
                        this._componentIdAssetFlatMap.set(assetInfo.instance_id, assetInfo); // - 페이지별 자산 정보 업데이트


                        var groupMap = null; // - 페이지 그룹이 존재하지 않는 경우 생성하기

                        if (this._assetInfosInPageMap.has(assetInfo.page_id) == false) {
                              this._assetInfosInPageMap.set(assetInfo.page_id, new Map());
                        } // 그룹에 자산 정보 추가하기


                        groupMap = this._assetInfosInPageMap.get(assetInfo.page_id);
                        groupMap.set(assetInfo.id, assetInfo);
                  }
                  /*
                  배치된 자산이 삭제 될때 실행
                  1. 컴포넌트 id로 자산 정보 맵핑 삭제
                  2. 페이지별 자산 정보 업데이트
                    */

            }, {
                  key: "_removeComponentIdMapAndAssetInfoInPageMap",
                  value: function _removeComponentIdMapAndAssetInfoInPageMap(assetInfo) {
                        // - 자산 정보 업데이트
                        // 컴포넌트 instance_id로 자산 정보 맵핑
                        this._componentIdAssetFlatMap["delete"](assetInfo.instance_id); // - 페이지별 자산 정보 업데이트


                        if (this._assetInfosInPageMap.has(assetInfo.page_id) == true) {
                              var groupMap = this._assetInfosInPageMap.get(assetInfo.page_id);

                              groupMap["delete"](assetInfo.id);
                        }
                  }
                  /*
                  currentOpenPageId에 해당하는 맵핑 정보 삭제하기
                   */

            }, {
                  key: "resetAssetMappingDataInPage",
                  value: function resetAssetMappingDataInPage(currentOpenPageId) {
                        var assetInfoList = this.assetInfosInPageMap.get(currentOpenPageId);

                        if (assetInfoList) {
                              assetInfoList.forEach(function (assetInfo) {
                                    assetInfo.instance_id = "";
                                    assetInfo.page_id = "";
                              });
                        }
                  }
            }, {
                  key: "assetsDataManager",
                  get: function get() {
                        return this._assetsDataManager;
                  }
            }, {
                  key: "fields",
                  get: function get() {
                        return this._fields;
                  }
            }, {
                  key: "visibleFields",
                  get: function get() {
                        return this._visibleFields;
                  }
            }, {
                  key: "assets",
                  get: function get() {
                        //{sensor: [], cctv: []}
                        return this._assets;
                  }
            }, {
                  key: "assetTypes",
                  get: function get() {
                        //[{},{},{}]
                        return this._assetTypes;
                  }
            }, {
                  key: "assetFlat",
                  get: function get() {
                        return this._assetFlat;
                  }
            }, {
                  key: "assetFlatMap",
                  get: function get() {
                        return this._assetFlatMap;
                  }
                  /*
                  {
                    key:component_id
                    vlaue:assetInfo
                  }
                   */

            }, {
                  key: "componentIdAssetFlatMap",
                  get: function get() {
                        return this._componentIdAssetFlatMap;
                  }
            }, {
                  key: "assetInfosInPageMap",
                  get: function get() {
                        return this._assetInfosInPageMap || new Map();
                  }
            }, {
                  key: "assetCompsInfo",
                  get: function get() {
                        return {
                              comp2d: this.assets2dComponentsInfo,
                              comp3d: this.assets3dComponentsInfo
                        };
                  }
            }, {
                  key: "assetCompsInfoFlat",
                  get: function get() {
                        return this._assetComponentsFlat;
                  }
            }]);

            return AssetComponentProxy;
      }(window.puremvc.Proxy);

AssetComponentProxy.NAME = "AssetComponentProxy";
/**
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
 * */

/**
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□

 검색 기능, 소트 기능 담당
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
 * */

var AssetsDataManager =
      /*#__PURE__*/
      function () {
            function AssetsDataManager(itemLen) {
                  _classCallCheck(this, AssetsDataManager);

                  this.itemLen = itemLen || 15;
                  this.totalPage = 0;
                  this.assets = {};
                  this.flatAssets = [];
            }

            _createClass(AssetsDataManager, [{
                  key: "updateAssetsData",
                  value: function updateAssetsData(assetTypeData, assetFlatData) {
                        this.assets = assetTypeData;
                        this.flatAssets = assetFlatData;
                  }
            }, {
                  key: "getDataByOptions",
                  value: function getDataByOptions(type, options) {
                        var data = this.assets[type] || this.flatAssets;

                        if (options && options.search) {
                              data = this.getSearchData(data, options.search);
                        }

                        if (options && options.sort) {
                              data = this.getSortData(data, options.sort);
                        }

                        return data;
                  }
            }, {
                  key: "getSearchData",
                  value: function getSearchData(data, options) {
                        //TODO 배치 미배치 항목 추가
                        var list = data.concat();
                        var field = options.field;
                        var keyword = options.keyword;

                        if (field && keyword) {
                              list = list.filter(function (x) {
                                    var value = x[field];

                                    if (value && value.indexOf(keyword) != -1) {
                                          return x;
                                    }
                              });
                        }

                        window.server10 = data.find(function (x) {
                              return x.id == "server10";
                        });

                        if (options.onlyUnpositioned === true) {
                              list = list.filter(function (x) {
                                    return !x.instance_id && !x.parent_id;
                              });
                        }

                        if (options.onlyPositioned === true) {
                              list = list.filter(function (x) {
                                    return x.instance_id || x.parent_id;
                              });
                        }

                        return list;
                  }
            }, {
                  key: "getSortData",
                  value: function getSortData(data, options) {
                        var list = data.concat();
                        var key = options.field;
                        var state = options.state;
                        var sortOrder = 1;

                        if (key && state) {
                              if (state == "descending") {
                                    sortOrder = -1;
                              }

                              list.sort(function (a, b) {
                                    var result = a[key] < b[key] ? -1 : a[key] > b[key] ? 1 : 0;
                                    return result * sortOrder;
                              });
                        }

                        return list;
                  }
            }, {
                  key: "getPaginationData",
                  value: function getPaginationData(data, page) {
                        var maxPage = Math.ceil(data.length / this.itemLen);
                        page = Math.min(maxPage, Math.max(1, page));
                        return {
                              page: page,
                              totalPage: maxPage,
                              data: data.slice((page - 1) * this.itemLen, (page - 1) * this.itemLen + this.itemLen)
                        };
                  }
            }]);

            return AssetsDataManager;
      }();


AssetComponentProxy.TYPE = {
      CCTV: "CCTV",
      ACCESS: "Access",
      LEAK: "WaterLeak",
      FIRE: "Fire",
      TEMPHUMI: "THSensor",
      MDM: "MDM",
      GAS: "GasLeak",
      EARTH: "EarthQuake",
      PDU: "PDU",
      RACK: "Rack",
      SERVER: "Server",
      NETWORK: "Network",
      STORAGE: "Storage",
      BACKUP: "Backup",
      THERMOHYGROSTAT: "Thermohygrostat"
};
AssetComponentProxy.EFFECT = {
      FIRE: "FIRE"
};
AssetComponentProxy.NOTI_SELECTED_ASSET = "notification/selectedAsset";
