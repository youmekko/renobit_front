/*
페이지에 배치된 자산 정보 관리자
- 특정 편집 페이지에서 옵션에 따른 자산 정보 가져오기
- 출입센서 자산 정보만 가져오기


 */

class AssetComponentInfoPlacedPageProxy extends window.puremvc.Proxy {

	constructor(assetProxy) {
		super(AssetComponentInfoPlacedPageProxy.NAME);
		this._assetProxy = assetProxy;
	}

	onRegister() {

	}



	/*
	    pageName에서  자산 정보 구해오기

	    1. 읽어들일 페이지 이름으로 페이지 id 구하기
	    2. 서버에서 페이지 정보 구하기
	    3. 레이어 필터링 처리하기
	    4. 자산 타입 필터링 처리하기
		    - 자산 컴포넌트가 아닌 경우는 모두 추가
		    - 자산 컴포넌트 중 options.includeTypes에 해당하는 자산 컴포넌트 만 추가
	     */
	async getAssetDataInPage(pageName, options) {
		// 1. 읽어들일 페이지 이름으로 페이지 id 구하기
		let pageId = window.wemb.pageManager.getPageIdByName(pageName);
		if (pageId == null) {
			console.warn(`The ${pageName} to read does not exist. Please check and try again.`);
			return null;
		}

		let defaultOptions = {
			includeLayers: [AssetComponentInfoPlacedPageProxy.WORK_LAYERS.THREE_LAYER],
			includeTypes: [],
			includeNoneAssetType: false
		}

		options = Object.assign(defaultOptions, options);

		// 2. 서버에서 페이지 정보 구하기
		let serverUrl = window.wemb.configManager.workUrl;
		let data = {
			"id": "editorService.getPageInfo",
			"params": {
				"page_id": pageId,
				"user_id": window.wemb.configManager.userId,
				"isViewer": false
			}
		};
		let result = await window.wemb.$http.post(serverUrl, data);
		if (result == null || result == false) {
			console.warn("An error occurred while reading file information");
			return null;
		}
		///////////////


		// 3. 레이어 필터링 처리하기
		let pageData = result.data.data;
		let resultComponentList = [];
		// 2D 레이어 필터링 처리
		if (options.includeLayers.indexOf(AssetComponentInfoPlacedPageProxy.WORK_LAYERS.TWO_LAYER) != -1) {
			// 4. 자산 타입 필터링 처리하기
			let componentInstanceList = pageData.content_info.two_layer;
			resultComponentList = resultComponentList.concat(this.filterAssetComponentInstanceInfoList(componentInstanceList, options.includeTypes, options.includeNoneAssetType))
		}


		// 3D 레이어 필터링 처리
		if (options.includeLayers.indexOf(AssetComponentInfoPlacedPageProxy.WORK_LAYERS.THREE_LAYER) != -1) {
			// 4. 자산 타입 필터링 처리하기
			let componentInstanceList = pageData.content_info.three_layer;
			resultComponentList = resultComponentList.concat(this.filterAssetComponentInstanceInfoList(componentInstanceList, options.includeTypes, options.includeNoneAssetType))
		}


		return resultComponentList;
	}

	async getAccessSensor3DModelDataInFloorPage(pageName) {

	      // 페이지에 배치되어 있는 자산 컴포넌트중 access에 해당하는 자산 컴포넌트 인스턴스 정보만 가져오기
		let comInstanceInfoList = await this.getAssetDataInPage(pageName, {
			includeLayers: [AssetComponentInfoPlacedPageProxy.WORK_LAYERS.THREE_LAYER],
			includeTypes: ["Access"],
			accessory: false
		})

		if (comInstanceInfoList == null) {
			return [];
		}
		if (comInstanceInfoList.length <= 0)
			return [];




		let result = [];

		comInstanceInfoList.forEach((comInstanceInfo) => {

                  // 자산 정보가 있는 경우만.
                  if(this._assetProxy.componentIdAssetFlatMap.has(comInstanceInfo.id==false))
                        return;

                  let assetInfo = this._assetProxy.componentIdAssetFlatMap.get(comInstanceInfo.id);
                  // 팝업 아이디 설정하기
                  if(comInstanceInfo.props.hasOwnProperty("settings")==false){
                        comInstanceInfo.props.settings = {
                              popupId:""
                        }
                  }
                  let componentPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName(comInstanceInfo.category, comInstanceInfo.componentName);

                  // 환경설정 팝업 아이디 추가
                  comInstanceInfo.props.settings.popupId = componentPanelInfo.initProperties.props.settings.popupId;
                  // 환경설정 3d 맵핑 정보 추가
                  comInstanceInfo.props.resource = componentPanelInfo.initProperties.props.resource;

                 /// 자산 id 및 자산 정보 설정하기
                  comInstanceInfo.props.setter.data = assetInfo;


                  result.push(comInstanceInfo);

                /*
                삭제 예정(2018.11.28)
		      /!*
		      자산 목록에서 컴포넌트 id에 해당하는 자산 정보를 구한다.
		       *!/
			let assetId = item.props.setter.assetId || "";
			let popupId = item.props.setter.popupId || "";
			let assetInfo = this._assetProxy.assetFlatMap.get()
			let info = {
				setter: {
					assetId: assetId,
					popupId: popupId,
					position: {
						x: item.props.setter.position.x,
						y: item.props.setter.position.y,
						z: item.props.setter.position.z
					}
				},
				resource: {
					modeling: {
						default: {}
					}
				}
			}

			let modelingInfo = null;
			if (item.componentName == "NObjLoaderComponent") {
				modelingInfo = item.props.setter.selectItem;
			} else {
				modelingInfo = item.props.resource.modeling.default;
			}
			info.resource.modeling.default = modelingInfo;

                  {

                  }*/

		})

            return result;
	}

	/*
	자산 목록에서
	    - comInstanceList: 배치되어 있는 컴포넌트 인스턴스 리스트
	    - typeList: 타입 리스트이
	 */
	filterAssetComponentInstanceInfoList(comInstanceList, typeList, bIncludeNoneAssetComponent = false) {
		// 배치된 컴포넌트 목록에서 해당하는 자산 타입의 컴포넌트인스턴스 인지 찾기
		let resultComponentList = [];
		// 타입이 존재하지 않는다면 모든  내용을 그대로 전송.
		if (typeList == null || typeList.length <= 0) {
			return comInstanceList;
		}


		for (let i = 0; i < comInstanceList.length; i++) {
			let instanceInfo = comInstanceList[i];
			let infoMap = null;
			if (instanceInfo.layerName == AssetComponentInfoPlacedPageProxy.WORK_LAYERS.THREE_LAYER) {
				infoMap = this._assetProxy.asset3DTypeInfoMap;
			}
			if (instanceInfo.layerName == AssetComponentInfoPlacedPageProxy.WORK_LAYERS.TWO_LAYER) {
				infoMap = this._assetProxy.asset2DTypeInfoMap;
			}
                  let fileID;
                  try{
                        fileID = instanceInfo.props.resource.modeling.default.name.split("_")[0];
                  }catch(error){ fileID = instanceInfo.componentName }

			// 자산 컴포넌트가 아닌 경우는 즉시 추가
			if (infoMap.has(fileID) == false) {
				if (bIncludeNoneAssetComponent == true)
					resultComponentList.push(instanceInfo);

				continue;
			}

			// 자산 컴포넌트 중 타입에 해당하는  내용만 추가
			let assetType = infoMap.get(fileID).asset_type;
			if (typeList.indexOf(assetType) != -1) {
				resultComponentList.push(instanceInfo);
			}

		}

		return resultComponentList;
	}
}

AssetComponentInfoPlacedPageProxy.WORK_LAYERS = {
	TWO_LAYER: "twoLayer",
	THREE_LAYER: "threeLayer"
}



AssetComponentInfoPlacedPageProxy.NAME="AssetPlacedPageProxy";

