"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/*
페이지에 배치된 자산 정보 관리자
- 특정 편집 페이지에서 옵션에 따른 자산 정보 가져오기
- 출입센서 자산 정보만 가져오기


 */
var AssetComponentInfoPlacedPageProxy =
      /*#__PURE__*/
      function (_window$puremvc$Proxy) {
            _inherits(AssetComponentInfoPlacedPageProxy, _window$puremvc$Proxy);

            function AssetComponentInfoPlacedPageProxy(assetProxy) {
                  var _this;

                  _classCallCheck(this, AssetComponentInfoPlacedPageProxy);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(AssetComponentInfoPlacedPageProxy).call(this, AssetComponentInfoPlacedPageProxy.NAME));
                  _this._assetProxy = assetProxy;
                  return _this;
            }

            _createClass(AssetComponentInfoPlacedPageProxy, [{
                  key: "onRegister",
                  value: function onRegister() {}
                  /*
					  pageName에서  자산 정보 구해오기
						  1. 읽어들일 페이지 이름으로 페이지 id 구하기
					  2. 서버에서 페이지 정보 구하기
					  3. 레이어 필터링 처리하기
					  4. 자산 타입 필터링 처리하기
						  - 자산 컴포넌트가 아닌 경우는 모두 추가
						  - 자산 컴포넌트 중 options.includeTypes에 해당하는 자산 컴포넌트 만 추가
					   */

            }, {
                  key: "getAssetDataInPage",
                  value: function () {
                        var _getAssetDataInPage = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee(pageName, options) {
                                    var pageId, defaultOptions, serverUrl, data, result, pageData, resultComponentList, componentInstanceList, _componentInstanceList;

                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                          while (1) {
                                                switch (_context.prev = _context.next) {
                                                      case 0:
                                                            // 1. 읽어들일 페이지 이름으로 페이지 id 구하기
                                                            pageId = window.wemb.pageManager.getPageIdByName(pageName);

                                                            if (!(pageId == null)) {
                                                                  _context.next = 4;
                                                                  break;
                                                            }

                                                            console.warn("The ".concat(pageName, " to read does not exist. Please check and try again."));
                                                            return _context.abrupt("return", null);

                                                      case 4:
                                                            defaultOptions = {
                                                                  includeLayers: [AssetComponentInfoPlacedPageProxy.WORK_LAYERS.THREE_LAYER],
                                                                  includeTypes: [],
                                                                  includeNoneAssetType: false
                                                            };
                                                            options = Object.assign(defaultOptions, options); // 2. 서버에서 페이지 정보 구하기

                                                            serverUrl = window.wemb.configManager.workUrl;
                                                            data = {
                                                                  "id": "editorService.getPageInfo",
                                                                  "params": {
                                                                        "page_id": pageId,
                                                                        "user_id": window.wemb.configManager.userId,
                                                                        "isViewer": false
                                                                  }
                                                            };
                                                            _context.next = 10;
                                                            return window.wemb.$http.post(serverUrl, data);

                                                      case 10:
                                                            result = _context.sent;

                                                            if (!(result == null || result == false)) {
                                                                  _context.next = 14;
                                                                  break;
                                                            }

                                                            console.warn("An error occurred while reading file information");
                                                            return _context.abrupt("return", null);

                                                      case 14:
                                                            ///////////////
                                                            // 3. 레이어 필터링 처리하기
                                                            pageData = result.data.data;
                                                            resultComponentList = []; // 2D 레이어 필터링 처리

                                                            if (options.includeLayers.indexOf(AssetComponentInfoPlacedPageProxy.WORK_LAYERS.TWO_LAYER) != -1) {
                                                                  // 4. 자산 타입 필터링 처리하기
                                                                  componentInstanceList = pageData.content_info.two_layer;
                                                                  resultComponentList = resultComponentList.concat(this.filterAssetComponentInstanceInfoList(componentInstanceList, options.includeTypes, options.includeNoneAssetType));
                                                            } // 3D 레이어 필터링 처리


                                                            if (options.includeLayers.indexOf(AssetComponentInfoPlacedPageProxy.WORK_LAYERS.THREE_LAYER) != -1) {
                                                                  // 4. 자산 타입 필터링 처리하기
                                                                  _componentInstanceList = pageData.content_info.three_layer;
                                                                  resultComponentList = resultComponentList.concat(this.filterAssetComponentInstanceInfoList(_componentInstanceList, options.includeTypes, options.includeNoneAssetType));
                                                            }

                                                            return _context.abrupt("return", resultComponentList);

                                                      case 19:
                                                      case "end":
                                                            return _context.stop();
                                                }
                                          }
                                    }, _callee, this);
                              }));

                        function getAssetDataInPage(_x, _x2) {
                              return _getAssetDataInPage.apply(this, arguments);
                        }

                        return getAssetDataInPage;
                  }()
            }, {
                  key: "getAccessSensor3DModelDataInFloorPage",
                  value: function () {
                        var _getAccessSensor3DModelDataInFloorPage = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2(pageName) {
                                    var _this2 = this;

                                    var comInstanceInfoList, result;
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            _context2.next = 2;
                                                            return this.getAssetDataInPage(pageName, {
                                                                  includeLayers: [AssetComponentInfoPlacedPageProxy.WORK_LAYERS.THREE_LAYER],
                                                                  includeTypes: ["Access"],
                                                                  accessory: false
                                                            });

                                                      case 2:
                                                            comInstanceInfoList = _context2.sent;

                                                            if (!(comInstanceInfoList == null)) {
                                                                  _context2.next = 5;
                                                                  break;
                                                            }

                                                            return _context2.abrupt("return", []);

                                                      case 5:
                                                            if (!(comInstanceInfoList.length <= 0)) {
                                                                  _context2.next = 7;
                                                                  break;
                                                            }

                                                            return _context2.abrupt("return", []);

                                                      case 7:
                                                            result = [];
                                                            comInstanceInfoList.forEach(function (comInstanceInfo) {
                                                                  // 자산 정보가 있는 경우만.
                                                                  if (_this2._assetProxy.componentIdAssetFlatMap.has(comInstanceInfo.id == false)) return;

                                                                  var assetInfo = _this2._assetProxy.componentIdAssetFlatMap.get(comInstanceInfo.id); // 팝업 아이디 설정하기


                                                                  if (comInstanceInfo.props.hasOwnProperty("settings") == false) {
                                                                        comInstanceInfo.props.settings = {
                                                                              popupId: ""
                                                                        };
                                                                  }

                                                                  var componentPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName(comInstanceInfo.category, comInstanceInfo.componentName); // 환경설정 팝업 아이디 추가

                                                                  comInstanceInfo.props.settings.popupId = componentPanelInfo.initProperties.props.settings.popupId; // 환경설정 3d 맵핑 정보 추가

                                                                  comInstanceInfo.props.resource = componentPanelInfo.initProperties.props.resource; /// 자산 id 및 자산 정보 설정하기

                                                                  comInstanceInfo.props.setter.data = assetInfo;
                                                                  result.push(comInstanceInfo);
                                                                  /*
																  삭제 예정(2018.11.28)
																  /!*
																  자산 목록에서 컴포넌트 id에 해당하는 자산 정보를 구한다.
																  *!/
																  let assetId = item.props.setter.assetId || "";
																  let popupId = item.props.setter.popupId || "";
																  let assetInfo = this._assetProxy.assetFlatMap.get()
																  let info = {
																  setter: {
																  assetId: assetId,
																  popupId: popupId,
																  position: {
																  x: item.props.setter.position.x,
																  y: item.props.setter.position.y,
																  z: item.props.setter.position.z
																  }
																  },
																  resource: {
																  modeling: {
																  default: {}
																  }
																  }
																  }
																  let modelingInfo = null;
																  if (item.componentName == "NObjLoaderComponent") {
																  modelingInfo = item.props.setter.selectItem;
																  } else {
																  modelingInfo = item.props.resource.modeling.default;
																  }
																  info.resource.modeling.default = modelingInfo;
																	 {
																	 }*/
                                                            });
                                                            return _context2.abrupt("return", result);

                                                      case 10:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2, this);
                              }));

                        function getAccessSensor3DModelDataInFloorPage(_x3) {
                              return _getAccessSensor3DModelDataInFloorPage.apply(this, arguments);
                        }

                        return getAccessSensor3DModelDataInFloorPage;
                  }()
                  /*
				  자산 목록에서
					  - comInstanceList: 배치되어 있는 컴포넌트 인스턴스 리스트
					  - typeList: 타입 리스트이
				   */

            }, {
                  key: "filterAssetComponentInstanceInfoList",
                  value: function filterAssetComponentInstanceInfoList(comInstanceList, typeList) {
                        var bIncludeNoneAssetComponent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
                        // 배치된 컴포넌트 목록에서 해당하는 자산 타입의 컴포넌트인스턴스 인지 찾기
                        var resultComponentList = []; // 타입이 존재하지 않는다면 모든  내용을 그대로 전송.

                        if (typeList == null || typeList.length <= 0) {
                              return comInstanceList;
                        }

                        for (var i = 0; i < comInstanceList.length; i++) {
                              var instanceInfo = comInstanceList[i];
                              var infoMap = null;

                              if (instanceInfo.layerName == AssetComponentInfoPlacedPageProxy.WORK_LAYERS.THREE_LAYER) {
                                    infoMap = this._assetProxy.asset3DTypeInfoMap;
                              }

                              if (instanceInfo.layerName == AssetComponentInfoPlacedPageProxy.WORK_LAYERS.TWO_LAYER) {
                                    infoMap = this._assetProxy.asset2DTypeInfoMap;
                              }

                              var fileID = void 0;

                              try {
                                    fileID = instanceInfo.props.resource.modeling["default"].name.split("_")[0];
                              } catch (error) {
                                    fileID = instanceInfo.componentName;
                              } // 자산 컴포넌트가 아닌 경우는 즉시 추가


                              if (infoMap.has(fileID) == false) {
                                    if (bIncludeNoneAssetComponent == true) resultComponentList.push(instanceInfo);
                                    continue;
                              } // 자산 컴포넌트 중 타입에 해당하는  내용만 추가


                              var assetType = infoMap.get(fileID).asset_type;

                              if (typeList.indexOf(assetType) != -1) {
                                    resultComponentList.push(instanceInfo);
                              }
                        }

                        return resultComponentList;
                  }
            }]);

            return AssetComponentInfoPlacedPageProxy;
      }(window.puremvc.Proxy);

AssetComponentInfoPlacedPageProxy.WORK_LAYERS = {
      TWO_LAYER: "twoLayer",
      THREE_LAYER: "threeLayer"
};
AssetComponentInfoPlacedPageProxy.NAME = "AssetPlacedPageProxy";
