"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/*
배치되어 있는 컴포넌트 에서 자산 요소만을 관리자는 proxy
실행시기
- 페이지가 열릴때
- 페이지가 저장될때
- 컴포넌트가 추가될 때
- 컴ㅍ모넌트가 삭제될때
 */
var AssetAllocationProxy =
      /*#__PURE__*/
      function (_window$puremvc$Proxy) {
            _inherits(AssetAllocationProxy, _window$puremvc$Proxy);

            _createClass(AssetAllocationProxy, [{
                  key: "assetCompositionInfoList",

                  /*
				  페이지에 배치되어 있는 자산 정보 + 컴포넌트 정보
				  */
                  get: function get() {
                        return this._assetCompositionInfoList;
                  }
            }, {
                  key: "assetCompositionInfoMap",
                  get: function get() {
                        return this._assetCompositionInfoMap;
                  }
                  /*
				  자산 타입별 자산 정보 + 컴포넌트 정보
				  */

            }, {
                  key: "assetCompositionInfoGroupMap",
                  get: function get() {
                        return this._assetCompositionInfoGroupMap;
                  }
                  /*
				  2018.11.06(ckkim)
				  현재 편집중인 페이지 ID 구하기
				  */

            }, {
                  key: "currentOpenPageId",
                  get: function get() {
                        return wemb.pageManager.currentPageInfo.id;
                  }
            }]);

            function AssetAllocationProxy() {
                  var _this;

                  _classCallCheck(this, AssetAllocationProxy);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(AssetAllocationProxy).call(this, AssetAllocationProxy.NAME));
                  _this._mainPageProxy = null;
                  _this._assetComponentProxy = null;
                  _this._assetCompositionInfoList = [];
                  _this._assetCompositionInfoMap = new Map();
                  _this._assetCompositionInfoGroupMap = new Map();
                  return _this;
            }

            _createClass(AssetAllocationProxy, [{
                  key: "onRegister",
                  value: function onRegister() {}
                  /*
				  call :
						DCIMManager.setFacade()에서 호출
				  */

            }, {
                  key: "attachProxy",
                  value: function attachProxy() {
                        if (this._mainPageProxy == null) {
                              if (wemb.configManager.exeMode == "editor") this._mainPageProxy = this.facade.retrieveProxy(EditorProxy.NAME);else this._mainPageProxy = this.facade.retrieveProxy(ViewerProxy.NAME);
                        }

                        if (this._assetComponentProxy == null) {
                              this._assetComponentProxy = this.facade.retrieveProxy(AssetComponentProxy.NAME);
                        }
                  }
                  /*
				  자산 정보 + 컴포넌트 정보를 합한 자산 복합 정보를 생성 후 맵에 넣기
				  1. 복합 정보 생성.
				  2. 전체 복합정보 리스트에 추가
				  3. 전체 복합정보 맵에 추가
				  4. 타입 그룹별 전체 복합정보 리스트에 추가
					*/

            }, {
                  key: "_createAndAddCompositionInfo",
                  value: function _createAndAddCompositionInfo(assetItem, comInstance) {
                        try {
                              // 자산 + 컴포넌트 합성 정보 생성
                              var assetCompositionInfo = {
                                    comInstanceId: comInstance.id,
                                    comInstanceName: comInstance.name,
                                    comInstance: comInstance,
                                    assetId: assetItem.id,
                                    assetType: assetItem.asset_type,
                                    assetInfo: assetItem //console.log("saveAssetInfo 3 Asset info to be created = ",  assetCompositionInfo.comInstanceId);
                                    // 자산 + 컴포넌트 합성 정보 추가

                              };

                              this._assetCompositionInfoList.push(assetCompositionInfo);

                              this._assetCompositionInfoMap.set(assetItem.id, assetCompositionInfo); // 자산 타입별로 자산+ 컴포넌트 합성 정보 저장


                              var group = [];

                              if (this._assetCompositionInfoGroupMap.has(assetItem.asset_type) == true) {
                                    group = this._assetCompositionInfoGroupMap.get(assetItem.asset_type);
                              }

                              group.push(assetCompositionInfo);

                              this._assetCompositionInfoGroupMap.set(assetItem.asset_type, group);
                        } catch (error) {
                              console.warn("@@ error AssetAllocationProxy Error when executing _addCompositionInfo () method", error);
                        }
                  }
                  /*
				  1. 합성 맵에서 assetId에 해당하는 정보 삭제
				  2. 타입 그룹별 맵에서 asseetId에 해당하는 정보 삭제
					 단, 그룹이 0개가 되는 경우 그룹도 제거
					*/

            }, {
                  key: "_removeCompositionInfo",
                  value: function _removeCompositionInfo(assetItem) {
                        try {
                              var compositionInfoIndex = this._assetCompositionInfoList.findIndex(function (compositionInfo) {
                                    return compositionInfo.assetId == assetItem.id;
                              });

                              if (compositionInfoIndex != -1) {
                                    // 전체 복합정보 리스트에서 삭제
                                    this._assetCompositionInfoList.splice(compositionInfoIndex, 1); // 전체 복합정보 맵에서 삭제


                                    this._assetCompositionInfoMap["delete"](assetItem.id); // 타입 그룹별 복합정보 리스트에서 삭제


                                    var group = this._assetCompositionInfoGroupMap.get(assetItem.asset_type);

                                    if (group) {
                                          var index = group.findIndex(function (compositionInfo) {
                                                return compositionInfo.assetId == assetItem.id;
                                          });

                                          if (index != -1) {
                                                group.splice(index, 1);

                                                if (group.length == 0) {
                                                      this._assetCompositionInfoGroupMap.clear(assetItem.asset_type);
                                                }
                                          }
                                    }
                              }
                        } catch (error) {
                              console.warn("@@ error AssetAllocationProxy Error when executing _removeCompositionInfo () method", error);
                        }
                  }
                  /*
				  페이지가 열릴때
				  - 자산 정보 로드
				  - 페이지 정보 구하기
				  - 컴포넌트 정보 구하기
				  - layer에 컴포넌트 배치하기
				  - 이후 updateAssetAllocationList()가 호출되어 컴포넌트에 자산 정보 맵핑하기
					call : DCIMManager.noti_openPage();
				  */

            }, {
                  key: "updateAssetAllocationList",
                  value: function updateAssetAllocationList() {
                        var _this2 = this;

                        this._assetCompositionInfoList = [];

                        this._assetCompositionInfoMap.clear();

                        this._assetCompositionInfoGroupMap.clear(); // 전체 자산 정보에서 열리는 페이지의 자산 정보 구하기


                        var currentAssetList = this._assetComponentProxy.assetInfosInPageMap.get(this.currentOpenPageId); //console.log("@@assetInfo mapping 4, currentAssetList = ", currentAssetList);
                        // 컴포넌트 어셋 인스턴스 목록 만들기


                        var currentAssetComponentInstanceList = this._mainPageProxy.comInstanceList; // 현재 배치되어 있는 자산을 기반으로 자산 composition 정보 생성하기

                        if (currentAssetList) {
                              currentAssetList.forEach(function (assetInfo) {
                                    // 자산 정보에 설정되어 있는 instance_id와 배치되어 있는 컴포넌트 instnace_id와 동일한 경우
                                    // 컴포넌트의 assetId에 assetInfo 정보를 설정하기
                                    var instanceId = assetInfo.instance_id;
                                    var comInstance = currentAssetComponentInstanceList.find(function (item) {
                                          return instanceId == item.id;
                                    });

                                    if (comInstance) {
                                          /*
										  주의!! component.data = assetInfo를 하지 않는 이유는
										  컴포넌트 생성시 자산 정보가 맵핑 되어 있는 상태임.
										  페이지 저장전까지
										  */

                                          /*
										  자산 복합정보를 생성 후 자산 복합 배열과 맵에 추가
										  */
                                          _this2._createAndAddCompositionInfo(assetInfo, comInstance);
                                    }
                              });
                        }
                        /*
						자산 outline 정보를 업데이트 시키기위해 NOTI 날리기
						*/


                        this.sendUpdateAssetAllocationList();
                  }
                  /*
				  자산 컴포넌트의 자산 데이터를 업데이트
				   call : DCIMAssetMappingComponent.vue
				  onCLickAssetUpdate()
				   */

            }, {
                  key: "updateComponentAssetData",
                  value: function updateComponentAssetData(assetData, comInstance) {
                        console.log("TEMP2 1"); //업데이트 할 컴포넌트가 존재하는 경우만 업데이트 처리하기

                        var tempInstance = this._mainPageProxy.comInstanceList.find(function (item) {
                              return comInstance.id == item.id;
                        });

                        console.log("TEMP2 2");

                        if (tempInstance) {
                              console.log("TEMP2 3");
                              comInstance.changeNotification({
                                    type: DCIMManager.NOTI_CHANGE_ASSET_DATA,
                                    body: assetData
                              });
                        }
                  }
                  /*
				  호출되는 경우
				  경우01: 자산 컴포넌트가 배치될때 (drag) 자산 정보에 컴포넌트 정모 맵핑하기
						comInstance에는 이미 data가 적용된 상태임.
						- DCIMManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE() 처리 부분에서
						  컴포넌트에 자산 정보가 설정됨.
						 - 이후 AssetPagePlugin.NOTI_ADD_COM_INSTANCE()에서
						호출됨.
				   경우02:
						- 수동맵핑시
							  이때 컴포넌트에 자산 정보가 설정됨.
				   단계01: 맵핑할 자산 정보 구하기
				   단계02: 자산정보에 컴포넌트 정보 설정하기
				   단계03: 컴포넌트id에 자산 정보 맵핑, 페이지별 자산 정보 맵에 정보
				   단계04: 자산 정보를 배치 자산정보로 추가하기, 복합 자산 정보 생성하기
				   단계05: 업데이트  사실을 다른 패널에 알리기
				  */

            }, {
                  key: "addAssetAssetAllocation",
                  value: function addAssetAssetAllocation(comInstance) {
                        // 자산 정보가 존재하는지 확인하기.
                        if (this._assetComponentProxy.assetFlatMap.has(comInstance.assetId) == false) return false; // 단계01: 맵핑할 자산 정보 구하기

                        var assetInfo = this._assetComponentProxy.assetFlatMap.get(comInstance.assetId); // 단계02: 자산정보에 컴포넌트 정보 설정하기


                        assetInfo.instance_id = comInstance.id;
                        assetInfo.page_id = this.currentOpenPageId; // 단계03: 컴포넌트id에 자산 정보 맵핑, 페이지별 자산 정보 맵에 정보

                        this._assetComponentProxy.addComponentIdMapAndAssetInfoInPageMap(assetInfo); // 페이지별 타입 정보 업데이트 하기


                        this._assetComponentProxy.updateAssetTypesInPageMap(assetInfo.page_id); // 단계04: 자산 정보를 배치 자산정보로 추가하기, 복합 자산 정보 생성하기


                        this._createAndAddCompositionInfo(assetInfo, comInstance); // 단계05: 업데이트  사실을 다른 패널에 알리기


                        this.sendUpdateAssetAllocationList();
                        return true;
                  }
                  /*
				  자산 컴포넌트 인스턴스로 할당 정보 삭제
				  */

            }, {
                  key: "removeAssetAssetAllocation",
                  value: function removeAssetAssetAllocation(comInstance) {
                        var sendUpdateEvent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
                        var isRemoveComponent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
                        var assetId = comInstance.assetId;
                        if (this._assetComponentProxy.assetFlatMap.has(assetId) == false) return false;

                        var assetInfo = this._assetComponentProxy.assetFlatMap.get(assetId);

                        this._executeRemoveAssetAssetAllocation(comInstance, assetInfo, sendUpdateEvent, isRemoveComponent);

                        return true;
                  }
                  /*
				  자산 id로 할당 정보 삭제
				  call :
				   _removeAssetInfo()에서만 실행
				  */

            }, {
                  key: "removeAssetAllocationByAssetId",
                  value: function removeAssetAllocationByAssetId(assetId) {
                        var sendUpdateEvent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
                        var isRemoveComponent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
                        if (this.assetCompositionInfoMap.has(assetId) == false) return false;
                        var assetCompositionInfo = this.assetCompositionInfoMap.get(assetId);

                        this._assetComponentProxy.addComponentIdMapAndAssetInfoInPageMap(assetCompositionInfo.assetInfo);

                        this._assetComponentProxy.updateAssetTypesInPageMap(assetCompositionInfo.assetInfo.page_id);

                        this._executeRemoveAssetAssetAllocation(assetCompositionInfo.comInstance, assetCompositionInfo.assetInfo, sendUpdateEvent, isRemoveComponent);

                        return true;
                  }
            }, {
                  key: "_executeRemoveAssetAssetAllocation",
                  value: function _executeRemoveAssetAssetAllocation(comInstance, assetInfo) {
                        var sendUpdateEvent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
                        var isRemoveComponent = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
                        assetInfo.instance_id = "";
                        assetInfo.page_id = ""; // 삭제시 자산 상태를 기본값(ok)으로

                        assetInfo.valid_state = "ok"; // 컴포넌트가 삭제되지 않을경우만 실행

                        if (isRemoveComponent == false) this.updateComponentAssetData({}, comInstance);

                        this._removeCompositionInfo(assetInfo);

                        if (sendUpdateEvent) this.sendUpdateAssetAllocationList();
                  }
                  /*
				  자산 정보 저장하기
				  1. 자산 정보 저장하기
				  */

            }, {
                  key: "saveAssetInfo",
                  value: function () {
                        var _saveAssetInfo = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2() {
                                    var _this3 = this;

                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            return _context2.abrupt("return", new Promise(
                                                                  /*#__PURE__*/
                                                                  function () {
                                                                        var _ref = _asyncToGenerator(
                                                                              /*#__PURE__*/
                                                                              regeneratorRuntime.mark(function _callee(resolve, reject) {
                                                                                    var data, param, result;
                                                                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                                                                          while (1) {
                                                                                                switch (_context.prev = _context.next) {
                                                                                                      case 0:
                                                                                                            /*
																											자산 정보에서
																											 */
                                                                                                            data = _this3._assetCompositionInfoList.map(function (compositionInfo) {
                                                                                                                  var assetInfo = compositionInfo.assetInfo;
                                                                                                                  return {
                                                                                                                        asset_id: assetInfo.id,
                                                                                                                        instance_id: assetInfo.instance_id
                                                                                                                  };
                                                                                                            });
                                                                                                            param = {
                                                                                                                  "id": "assetDataService.setBatch",
                                                                                                                  "params": {
                                                                                                                        "list": data
                                                                                                                  }
                                                                                                            };
                                                                                                            param.params.page_id = wemb.pageManager.currentPageInfo.id;
                                                                                                            _context.prev = 3;
                                                                                                            _context.next = 6;
                                                                                                            return wemb.$http.post(window.wemb.dcimManager.path, param);

                                                                                                      case 6:
                                                                                                            result = _context.sent;
                                                                                                            console.log("saveAssetInfo 1 List of asset information to save = ", data);
                                                                                                            resolve(true);
                                                                                                            _context.next = 15;
                                                                                                            break;

                                                                                                      case 11:
                                                                                                            _context.prev = 11;
                                                                                                            _context.t0 = _context["catch"](3);
                                                                                                            reject(false);
                                                                                                            return _context.abrupt("return");

                                                                                                      case 15:
                                                                                                      case "end":
                                                                                                            return _context.stop();
                                                                                                }
                                                                                          }
                                                                                    }, _callee, null, [[3, 11]]);
                                                                              }));

                                                                        return function (_x, _x2) {
                                                                              return _ref.apply(this, arguments);
                                                                        };
                                                                  }()));

                                                      case 1:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2);
                              }));

                        function saveAssetInfo() {
                              return _saveAssetInfo.apply(this, arguments);
                        }

                        return saveAssetInfo;
                  }()
                  /*
				  자산 정보를 저장하는 경우
				  1. 모든 자산 정보를 가져온다.(asset/all)
				  2. 중복 확인
						- 신규 자산 정보와 현재 배치되어 있는  정보와 비교
						- 중복이  발생하는 경우 자선 정보에 중복 유무 적용
				  3. 중복이 없는 경우
						- 신규 맵핑 정보 저장(asset/batch)
				  4. 맵핑 정보  적용하기(현재 단계)
						- 1번이 실행되는 경우 신규 자산 정보이고
						  AssetAllocationProxy에 저장된 자산 정보는 이전 자산 정보가 됨.
						  즉, 잘못된 자산 정보를 가지고 있게됨.
						  또한, AssetComponentPanel에서 배치된 자산 정보임에도 불구하고
						  미맵핑 자산 정보로 판단하게 됨.
						   이를 위해
						  4-1. 맵핑 정보를 신규 자산 정보에 적용한다.
						  4-2. 자산 복합 정보(compositionInfo)를 신규로 만든다.
					*/

            }, {
                  key: "syncAssetCompositionInfoList",
                  value: function syncAssetCompositionInfoList() {
                        var _this4 = this;

                        try {
                              // 싱크 전 자산 정보에서 현재 페이지에 배치된 내용을 모두 지운다.
                              this._assetComponentProxy.resetAssetMappingDataInPage(this.currentOpenPageId); //  4-1. 맵핑 정보를 신규 자산 정보에 적용한다.


                              this._assetCompositionInfoList.forEach(function (compositionInfo) {
                                    var assetId = compositionInfo.assetId; // 신규 자산 정보 구하기

                                    var newAssetInfo = _this4._assetComponentProxy.assetFlatMap.get(assetId);

                                    newAssetInfo.instance_id = compositionInfo.comInstanceId;
                                    newAssetInfo.page_id = _this4.currentOpenPageId; // 단계03: 맵핑 정보를 여러 자산 Map에 적용하기

                                    _this4._assetComponentProxy.addComponentIdMapAndAssetInfoInPageMap(newAssetInfo);
                              }); // 페이지별 타입 정보 업데이트


                              this._assetComponentProxy.updateAssetTypesInPageMap(this.currentOpenPageId); // 4-2. 자산 복합 정보(compositionInfo)를 신규로 만든다.


                              this.updateAssetAllocationList();
                        } catch (error) {
                              console.log("syncAssetCompositionInfoList error ", error);
                        }
                  }
                  /*
				  call : 내부에서
						자산 중복 체크 시
				   */

            }, {
                  key: "sendUpdateAssetAllocationList",
                  value: function sendUpdateAssetAllocationList() {
                        this.sendNotification(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST);
                        wemb.$globalBus.$emit(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST);
                  }
                  /*
				  환경설정에 저장되어 있는 정보로 자산 정보 적용하기.
				  1. 기본 환경설정 팝업 아이디 설정
				  2. 기본 환경설정 맵핑 정보 설정
				  3. 자산 정보 설정
				   call : DCIMManage.refreshAssetAllData()
				  주의 :
						- _hookAfterLoadPage()은 자산 컴포넌트 생성 정보에 설정(즉, 생성 전에)
						- attachDefaultAssetComponentInfo()은 자산 컴포넌트에 설정
					*/

            }, {
                  key: "attachDefaultAssetComponentInfo",
                  value: function attachDefaultAssetComponentInfo() {
                        var _this5 = this;

                        try {
                              this._assetCompositionInfoList.forEach(function (compositionInfo) {
                                    var comInstance = compositionInfo.comInstance; //let componentPanelInfo = wemb.componentLibraryManager.getComponentPanelInfoByName(comInstance.category, comInstance.componentName);
                                    // 환경설정 팝업 아이디 추가
                                    //comInstance.popupId = componentPanelInfo.initProperties.props.settings.popupId;
                                    // 환경설정 3d 맵핑 정보 추가
                                    //comInstance.resource = componentPanelInfo.initProperties.props.resource;
                                    //comInstance.data = compositionInfo.assetInfo;
                                    // 라이브러리에서 자산 file_id 구하기

                                    var file_id = wemb.componentLibraryManager.getComponentLabelByName(comInstance.category, comInstance.componentName);

                                    if (_this5._assetComponentProxy.asset3DTypeInfoMap.has(file_id) == false) {
                                          return;
                                    } // 컴포넌트 정보 구하기


                                    var componentConfigInfo = _this5._assetComponentProxy.asset3DTypeInfoMap.get(file_id); // 환경설정 팝업 아이디 추가
                                    // 환경설정 3d 맵핑 정보 추가
                                    // 자산 정보 추가


                                    comInstance.changeNotification({
                                          type: DCIMManager.NOTI_CHANGE_PROPS_DATA,
                                          body: {
                                                popupId: componentConfigInfo.props.initProperties.props.settings.popupId,
                                                resource: componentConfigInfo.props.initProperties.props.resource,
                                                data: compositionInfo.assetInfo
                                          }
                                    });
                              });
                        } catch (error) {
                              console.error("ERROR", "AssetAllocationProxy.attachDefaultAssetComponentInfo()", error);
                        }
                  }
            }, {
                  key: "onRemove",
                  value: function onRemove() {
                        console.log("@@@@@리무브");
                  }
            }]);

            return AssetAllocationProxy;
      }(window.puremvc.Proxy);

AssetAllocationProxy.NAME = "AssetAllocationProxy";
AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST = "notification/updateAssetAllocationList";
