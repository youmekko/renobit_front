/**
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□

 주의:
 extension_files.json에 dependency로 AssetComponentProxy를 추가하는 경우
 등록되지 않는 문제 발생.
 우선 파일을 나누지 않음.


 2018.10.21(ckkim)
 - AssetComponentProxy 이름을 변경해야함.


 2018.10.01 (ckkim)
 RENOBIT 에디터에 동적으로 추가되는 Proxy
 생성:
 - AssetEditorPlugin.js에서 RENOBIT Eidtdor의 Proxy으로 동적추가

 주 용도:
 - RENOBIT Editor와 자산 관련 플러그인, 패널간의 데이터 연동 처리
 예)
 - 페이지 저장 후 자산 배치 정보  저장하기 등
 */
class AssetComponentProxy extends window.puremvc.Proxy {
      constructor() {
            super(AssetComponentProxy.NAME);
            // 자산 타입 정보
            this._assetTypes = [];
            // 타입별 자산 정보
            /*
            AccessSensor: Array(27)
            CCTV: Array(0)
            FireSensor: Array(0)
            LeakSensor: Array(0)
            Pdu: Array(0)
            Rack: Array(0)
            length: 0
             */
            this._assets = [];
            this._assetFlat = [];
            this._assetComponentsFlat = [];
            this.assets2dComponentsInfo = {};
            this.assets3dComponentsInfo = {};
            this._assetsDataManager = new AssetsDataManager();
            this._fields = {"common": [], "type": {}};
            this._visibleFields = {"common": [], "type": {}};


            /*
            컴포넌트 id를 키로 자산 정보가 맵핑.
             */
            this._componentIdAssetFlatMap = new Map();


            /*
            {
                key:componentName,
                data:asset_Type
             */
            this.asset2DTypeInfoMap = new Map();
            this.asset3DTypeInfoMap = new Map();


            /* assetFlat array => map
            {
                key:asset_id,
                data:asset_item

            */

            this._assetFlatMap = new Map();
            /*
                2018.11.06(ckkim)
                페이지별 배치되어 있는 자산 정보 리스트를 가지고 있는 맵
                {
                    key:page_id,
                    data:{
                        key:asset_id,
                        data:asset_item
                    }
                 */
            this._assetInfosInPageMap = new Map();

            /* 페이지 별 배치되어 있는 자산 타입 리스트를 가지고 있는 맵 */
            /*
                {
                    key:page_id,
                    data:[asset_type
                },{
                    ....
                }
             */
            this._assetTypesGroupListMap = new Map();
      }

      /*
      데이터 관리자
       */
      get assetsDataManager() {
            return this._assetsDataManager;
      }

      get fields() {
            return this._fields;
      }

      get visibleFields() {
            return this._visibleFields;
      }


      getTypeFields(type, isOnlyVisibleField) {
            let targetData = (isOnlyVisibleField === true) ? this._visibleFields : this._fields;
            let commonFields = targetData.common;
            let typeFields = this.getOnlyTypeFields(type);
            return commonFields.concat(typeFields);
      }

      getOnlyTypeFields(type, isOnlyVisibleField) {
            let targetData = (isOnlyVisibleField === true) ? this._visibleFields : this._fields;
            let typeFields = targetData.type[type] || [];
            return typeFields;
      }

      get assets() {
            //{sensor: [], cctv: []}
            return this._assets;
      }

      get assetTypes() {
            //[{},{},{}]
            return this._assetTypes;
      }

      get assetFlat() {
            return this._assetFlat;
      }

      get assetFlatMap() {
            return this._assetFlatMap;
      }

      /*
      {
        key:component_id
        vlaue:assetInfo
      }
       */
      get componentIdAssetFlatMap() {
            return this._componentIdAssetFlatMap;
      }


      get assetInfosInPageMap() {
            return this._assetInfosInPageMap || new Map();
      }

      getDeployedAssets() {
            let assets = {};
            for (let key in this._assets) {
                  let typeAsset = this._assets[key];
                  assets[key] = typeAsset.filter(x => x.instance_id);
            }

            return assets;
      }

      getAssetListByType(type) {
            return this._assets[type] || [];
      }

      get assetCompsInfo() {
            return {
                  comp2d: this.assets2dComponentsInfo,
                  comp3d: this.assets3dComponentsInfo
            }
      }


      /*
      컴포넌트 정보 및 환경설정 정보 읽기
       */
      async loadAssetComponentsInfo() {
            let param = {
                  "id": "assetComponentService.getComponentList",
                  "params" : {}
            };

            let comp_res = await wemb.$http.post(window.wemb.dcimManager.path, param);
            let compsData = comp_res.data.data;
            this._assetComponentsFlat = [];
            this.asset2DTypeInfoMap.clear();
            this.asset3DTypeInfoMap.clear();

            try {
                  for (let index in compsData) {
                        let typeData = compsData[index];


                        this.assets3dComponentsInfo[typeData.id] = typeData.children.filter(x => x.data.layer == "3D");
                        this.assets2dComponentsInfo[typeData.id] = typeData.children.filter(x => x.data.layer == "2D");

                        /*
                        2018.11.06(ckkim)
                        컴포넌트 별 => 타입 값 할당하기
                        컴포넌트 이름을 키 값으로 자산 타입정보 저장하기


                         */
                        typeData.children.forEach((item) => {
                              if (item.data.layer == "2D") {
                                    this.asset2DTypeInfoMap.set(item.data.file_id, item.data);
                                    this._assetComponentsFlat.push(item);
                              }

                              if (item.data.layer == "3D") {
                                    this.asset3DTypeInfoMap.set(item.data.file_id, item.data);
                                    this._assetComponentsFlat.push(item);
                              }
                        })

                  }
            } catch (error) {
                  console.log("The asset component information is not loaded properly.");
            }
      }

      get assetCompsInfoFlat() {
            return this._assetComponentsFlat;
      }

      getAssetTypeByAssetCompName(compName) {
            let name = '';
            try {
                  name = this._assetComponentsFlat.find(x => x.data.name == compName).data.asset_type;
            } catch (error) {
                  console.log(error);
            }
            return name;
      }


      _updateAssetTypes(data) {
            this._assetTypes.splice(0, this._assetTypes.length);
            try {
                  this._assetTypes.push(...data.types);
                  this._updateFieldData(data.common_fields, data.types);
            } catch (error) {
                  console.log("Asset type information is not loaded properly.");
            }
      }

      _updateFieldData(common, types) {
            common = common || [];
            types = types || {};
            this._fields.common = common;
            this._visibleFields.common = common.filter(x => x.visible == true);
            for (let index in types) {
                  let typeData = types[index];
                  this._fields.type[typeData.id] = typeData.fields.type_fields || [];
                  this._visibleFields.type[typeData.id] = this._fields.type[typeData.id].filter(x => x.visible == true);
            }
      }


      /*
      전체 자산 정보 읽기
            1. 전체 자산 정보 읽기
            2. 자산 타입 정보 및 필드 정보 생성
            3. 데이터 메니저에 자산 정보 업데이트(주입)
            4. 자산 id를 키로 자산 그룹 맵 만들기
            5. 컴포넌트 id키로  맵과 페이지 id를 키로 맵 만들기
            6. 페이지 id를 키로 자산 타입 그룹 맵 만들기
       */
      async loadAssetsData() {
            let param = {
                  "id": "assetDataService.getAllData",
                  "params" : {}
            };
            // 장비에 대한 자산테이블(a_..)하고 join
            // 자산 테이블에 대한 컬럼명은 tb_asset_type에 있는 필드명 참조
            let result = await wemb.$http.post(window.wemb.dcimManager.path, param);
            let assets = result.data;

            this._assets = {};
            this._assetFlat = [];
            try {

                  // 자산 타입 정보 및 필드 정보 생성
                  this._updateAssetTypes(assets.data.schema);

                  this._assets = assets.data.result;

                  let ary = [];
                  for (var key in this._assets) {
                        var items = this._assets[key];

                        ary = ary.concat(items);
                  }
                  this._assetFlat = ary;
                  this._assetsDataManager.updateAssetsData(this.assets, this._assetFlat);


                  // 2018.11.06(ckkim)
                  // 자산 id를 키로 그룹 맵 만들기
                  this._createAssetMaps();

                  // 컴포넌트 id키로  맵과 페이지 id를 키로 맵 만들기
                  this._createComponentIdMapAndAssetInfoInPageMap();

                  // 페이지 id를 키로 자산 타입 그룹 맵 만들기
                  this._createAssetTypesInPageMap();


            } catch (error) {
                  console.log("Asset data does not load properly.");
            }
      }


      /*
      assetId에 해당하는  자산 정보 구하기
      1. 서버에서 자산 정보 구하기
      2. 자산 정보 업데이트

       */
      async updateAssetDataBy(assetId, assetType){
            let param = {
                  "id": "assetDataService.getData",
                  "params" : {
                        "id": assetId,
                        "asset_type": assetType
                  }
            };
            let newAssetData = null;
            try {
                  let result = await wemb.$http.post(window.wemb.dcimManager.path, param);
                  let info = result.data;
                  if(info.data.count==0){
                        console.log("데이터가 존재하지 않습니다.")
                        return false;
                  }

                  if(info.data.result){
                        newAssetData = info.data.result[0];
                  }else if(info.data){
                        newAssetData = info.data[0];
                  }else {
                        return false;
                  }

            }catch(error){
                  console.log("Asset data does not load properly.");
                  return false;
            }

            let targetAssetData = this.getAssetDataByAssetId(assetId);
            if(targetAssetData){
                  var encTypes = window.wemb.dcimManager._assetComponentProxy.fields.type[assetType].filter(function(asset){
                        return asset.encryption;
                    });
                    var encNames = encTypes.map(function(type){
                                return type.name;	
                    });
                    var paramKey = Object.getOwnPropertyNames(newAssetData);
                    for(var i = 0; encNames.length > 0 && i < paramKey.length; i ++){
                        if(encNames.indexOf(paramKey[i]) > -1){
                              newAssetData[paramKey[i]] = Base64.decode(newAssetData[paramKey[i]]);
                        }
                    }

                  //2. 자산 정보 업데이트
                  $.extend(true, targetAssetData, newAssetData);

                  console.log("업데이트  성공", newAssetData)
                  return targetAssetData;

            }

            return false;
      }

      /*
      asset id에 해당하는 asset data 찾기
       */
      getAssetDataByAssetId(assetId) {
            return this.assetFlat.find((item) => {
                  return item.id == assetId
            })
      }

      /*
      자식 자산 정보 가져오기
       */
      getAssetChildren(parentId) {
            return this.assetFlat.filter((item) => {
                  return item.parent_id == parentId
            })
      }


      /*
        sensor:3

         */
      getAssetCountListInPage(typeList, pageId) {

            let result = {};

            typeList.forEach((typeName) => {
                  result[typeName] = 0;
            });


            typeList.forEach((typeName) => {
                  result[typeName] = this.getAssetListByType(typeName).filter((assetInfo) => {
                        return assetInfo.page_id == pageId;
                  }).length;

            });

            return result;

      }


      /*

      2018.11.06(ckkim)

      페이지별 자산 정보 Map
          key: pageId:{
            key: assetid: {
              자산 정보
            },
          }
          key: pageId:{
            key: assetid: {
              자산 정보
            },
          }

      조건:
          페이지에 배치되어 있는 자산만 해당함.

       */

      _createAssetMaps() {
            this._assetFlatMap.clear();

            // page_id를 키로
            for (var i = 0; i < this.assetFlat.length; i++) {
                  let assetInfo = this.assetFlat[i];
                  assetInfo.valid_state = assetInfo.valid_state || "ok"; //ok, none, double
                  this._assetFlatMap.set(assetInfo.id, assetInfo);

            }
      }

      /*
      자산 정보를 읽은 후
      - 컴포넌트 id를 키로 자산 정보 맵 만들기
      - 페이지id를 키로 자산 정보 맵 만들기
       */
      _createComponentIdMapAndAssetInfoInPageMap() {
            this._assetInfosInPageMap.clear();
            this._componentIdAssetFlatMap.clear();

            // page_id를 키로
            for (var i = 0; i < this.assetFlat.length; i++) {
                  let assetInfo = this.assetFlat[i];

                  // page_id, instance_id 가 없는 경우 자산 배치가 되지 않은 것으로 판단
                  if (assetInfo.page_id == null || assetInfo.instance_id == null) {
                        continue;
                  }

                  // 컴포넌트id별 자산 정보 맵과 페이지별 자산 정보 맵에 정보 추가
                  this.addComponentIdMapAndAssetInfoInPageMap(assetInfo);
            }

            //console.log("saveAssetInfo loadAssetsData Group creation complete", this._assetInfosInPageMap);
      }

      /*
      페이지별 자산 타입 리스트
       */
      _createAssetTypesInPageMap() {
            this._assetTypesGroupListMap.clear();

            let pageInfoList = wemb.pageManager.getPageInfoList();
            for (let i = 0; i < pageInfoList.length; i++) {

                  // 페이지 정보 구하기
                  let pageInfo = pageInfoList[i];

                  // 페이지별 자산 타입 목록 만들기
                  this.updateAssetTypesInPageMap(pageInfo.id);

            }
      }


      /*
      2018.11.12(ckkim)
      assetId에 해당하는 자산 정보 구하기
       */
      getAssetData(assetId) {
            if (this._assetFlatMap.has(assetId) == false) {
                  return null;
            }
            return this._assetFlatMap.get(assetId);
      }

      /*
      assetId에 바인딩되어 있는 컴포넌트 ID
       */
      getComponentInstanceIdBy(assetId) {
            let assetInfo = this.getAssetData(assetId);
            if (assetInfo == null)
                  return null;


            if (assetInfo.hasOwnProperty("instance_id") && assetInfo.instance_id.length > 0) {
                  return assetInfo.instance_id;
            }

            return null;

      }


      /*
              페이지별 자산 정보 만들기
              call : 페이지
       */
      updateAssetInfosGroupListMap() {
            this._createAssetMaps();
      }


      /*
      일반 컴포넌트 정보에 환경설정 정보 적용하기

       */
      attachComponentConfigInfoToComInstanceInfo(comInstanceInfo) {
            // 3D 컴포넌트 체크
            if (comInstanceInfo.layerName != "threeLayer") {
                  return;
            }

            // 라이브러리에서 자산 file_id 구하기
            let file_id = wemb.componentLibraryManager.getComponentLabelByName("3D", comInstanceInfo.componentName);
            if (this.asset3DTypeInfoMap.has(file_id) == false) {
                  return;
            }

            // 컴포넌트 정보 구하기
            let componentConfigInfo = this.asset3DTypeInfoMap.get(file_id);
            // 팝업 아이디 설정하기
            if (comInstanceInfo.props.hasOwnProperty("settings") == false) {
                  comInstanceInfo.props.settings = {
                        popupId: ""
                  }
            }


            // 환경설정 팝업 아이디 추가
            comInstanceInfo.props.settings.popupId = componentConfigInfo.props.initProperties.props.settings.popupId;
            // 환경설정 3d 맵핑 정보 추가
            comInstanceInfo.props.resource = componentConfigInfo.props.initProperties.props.resource;

      }


      /*
      자산 정보에서 target컴포넌트 id에 할당되어 있는 자산 정보를 컴포넌트에 설정하기
      주로 컴포넌트 인스턴스 정보에 자산 정보를 사용할때 사용.

       */
      attachBindingAssetDataToComInstanceInfo(targetComInstanceInfo) {
            // 자산 id 및 자산 정보 설정하기
            if (this._componentIdAssetFlatMap.has(targetComInstanceInfo.id) == true) {
                  targetComInstanceInfo.props.setter.data = this._componentIdAssetFlatMap.get(targetComInstanceInfo.id);

            } else {
                  targetComInstanceInfo.props.setter.data = {};
            }
      }


      attachAssetDataToComInstanceInfo(data, targetComInstanceInfo) {
            // 자산 id 및 자산 정보 설정하기
            if (data) {
                  targetComInstanceInfo.props.setter.data = data;

            } else {
                  targetComInstanceInfo.props.setter.data = {};
            }
      }

      //////////////////////////////////////////////////


      //////////////////////////////////////////////////
      /*
        2018.11.06(ckkim)
        openPageId 정보를 기준으로 페이지 별로 중복 자산 찾기

        @openPageId : 열린 페이지
        @targetAssetList : 자산 타입 리스트


        call :
            외부에서 호출

        result:
            결과의 크기가 0개가 아닌 경우 중복된 상태임.
            return [{page_id:"",page_name:"",data:[]},...]
         */
      checkDuplicatedAsset(openPageId, targetCompositionInfoList) {
            let result = {
                  pageList: [],
                  assetList: []
            };

            let pageIdList = Array.from(this._assetInfosInPageMap.keys());
            // 편집한 페이지 리스트를 기준으로 중복 배치 자산 찾기 시작
            for (let i = 0; i < pageIdList.length; i++) {
                  let pageId = pageIdList[i];

                  // 검색중인 페이지와 현재 저장하려고 하는 페이지가 동일한 경우 패스
                  if (openPageId == pageId) {
                        continue;
                  }


                  // pageId로 그룹 알아내기 .
                  let allocationAssetGroupMap = this._assetInfosInPageMap.get(pageId);


                  // 중복 배치 유무 판단하기.
                  let selectedAssetList = []; // 중복 배치 자산 정보를 담을 배열 변수.

                  /*
                      배치한 자산 정보와 페이지에 배치된 자산 정보 목록과 비교해 중복 배치 유무 판단하기.
                      @targetAssetList = 배치한 자산 정보
                      @allocationAssetGroup = 페이지에 배치된 자산 정보 목록이 들어 있음.
                       */
                  for (let m = 0; m < targetCompositionInfoList.length; m++) {
                        // let compositionInfo = targetCompositionInfoList[m];
                        let assetId = targetCompositionInfoList[m].assetId;


                        // 해당 자산이 현재 검사하고 있는 페이지에 배치되었는지 유무  판다.
                        if (allocationAssetGroupMap.has(assetId) == false) {
                              continue;
                        }

                        // 중복 배치 자산인 경우 자산 정보 저장하기
                        selectedAssetList.push(allocationAssetGroupMap.get(assetId));


                        ///////////////
                        let tempItem = result.assetList.find((item) => {
                              return item.id == assetId;
                        });
                        // 중복된 자산 배치 목록만을 구하기 위해 맵에 저장
                        if (tempItem != null) {
                              result.assetList.push(allocationAssetGroupMap.get(assetId))
                        }
                  }


                  // 중복 배치가 있는 경우 result에 추가하기
                  if (selectedAssetList.length > 0) {
                        let pageName = wemb.pageManager.getPageNameById(pageId) || "";
                        result.pageList.push({
                              page_id: pageId,
                              page_name: pageName,
                              list: selectedAssetList
                        })
                  }
            }

            return result;
      }


      /*
        pageId에 배치되어 있는  자산 타입 목록
         */
      getAssetTypesInPage(pageId) {
            return this._assetTypesGroupListMap.get(pageId);
      }

      getAssetsInPage(pageId) {
            return this._assetFlat.filter(x => x.page_id == pageId);
      }

      getTypeAssetsInPage(type, pageId) {
            return this.getAssetsInPage(pageId).filter(x => x.asset_type == type);
      }


      //자산이 배치되어 있는 페이지 ID
      getPageIdPlaced(assetId) {
            if (this._assetFlatMap.has(assetId) == true) {
                  let assetInfo = this._assetFlatMap.get(assetId);
                  try {
                        if (assetInfo)
                              return assetInfo.page_id;
                  } catch (error) {
                        console.warn("@@ warnning AssetComponentProxy.js, getAssetPageId ", pageId);
                        return null;
                  }
            }


            return null;
      }


      /*
        2018.11.06(ckkim)
        PageId에 assetId가 배치되었는지 유무 판단하기
        단, 배치되어 있는 자산 정보만을 판단함.

        call :
            주로 페이지 저장 전 중복 배치 유무 판단시에 사용
            AssetPagePlugin.js
         */
      hasAssetInPage(assetId, pageId) {
            if (this._assetInfosInPageMap.has(pageId) == false)
                  return false;

            let assetInfoGroupMap = this._assetInfosInPageMap.get(pageId);
            if (assetInfoGroupMap.has(assetId) == false) {
                  return false;
            }

            return true;

      }


      /*
      자산이 추가, 삭제 될때 마다 업데이트
       */
      updateAssetTypesInPageMap(pageId) {
            // 타입 리스트 만들기
            let typeList = [];

            // 페이지에 배치되어 있는 자산 정보를 기반으로 타입 구하기
            let assetInfoListMap = this._assetInfosInPageMap.get(pageId);
            if (assetInfoListMap && assetInfoListMap.size > 0) {
                  assetInfoListMap.forEach((item, key) => {

                        if (typeList.indexOf(item.asset_type) == -1) {
                              typeList.push(item.asset_type);
                        }
                  })
            }


            this._assetTypesGroupListMap.set(pageId, typeList);
      }


      /*
      자산이 동적으로 배치되는 경우
      - 자산 정보 업데이트
            자산에 instance_id, page_id 정보 추가
      - 페이지별 자산 정보 업데이트

      - 페이지별 타입 정보 업데이트

       */


      addComponentIdMapAndAssetInfoInPageMap(assetInfo) {

            // - 자산 정보 업데이트
            // 컴포넌트 instance_id로 자산 정보 맵핑
            this._componentIdAssetFlatMap.set(assetInfo.instance_id, assetInfo);

            // - 페이지별 자산 정보 업데이트
            let groupMap = null;
            // - 페이지 그룹이 존재하지 않는 경우 생성하기
            if (this._assetInfosInPageMap.has(assetInfo.page_id) == false) {
                  this._assetInfosInPageMap.set(assetInfo.page_id, new Map());
            }

            // 그룹에 자산 정보 추가하기
            groupMap = this._assetInfosInPageMap.get(assetInfo.page_id);
            groupMap.set(assetInfo.id, assetInfo);


      }


      /*
      배치된 자산이 삭제 될때 실행
      1. 컴포넌트 id로 자산 정보 맵핑 삭제
      2. 페이지별 자산 정보 업데이트

       */
      _removeComponentIdMapAndAssetInfoInPageMap(assetInfo) {
            // - 자산 정보 업데이트
            // 컴포넌트 instance_id로 자산 정보 맵핑
            this._componentIdAssetFlatMap.delete(assetInfo.instance_id);

            // - 페이지별 자산 정보 업데이트
            if (this._assetInfosInPageMap.has(assetInfo.page_id) == true) {
                  let groupMap = this._assetInfosInPageMap.get(assetInfo.page_id);
                  groupMap.delete(assetInfo.id);
            }
      }


      /*
      currentOpenPageId에 해당하는 맵핑 정보 삭제하기
       */
      resetAssetMappingDataInPage(currentOpenPageId) {
            let assetInfoList = this.assetInfosInPageMap.get(currentOpenPageId);
            if (assetInfoList) {
                  assetInfoList.forEach((assetInfo) => {
                        assetInfo.instance_id = "";
                        assetInfo.page_id = "";
                  })
            }
      }

}

AssetComponentProxy.NAME = "AssetComponentProxy";

/**
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
 * */


/**
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□

 검색 기능, 소트 기능 담당
 □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
 * */

class AssetsDataManager {
      constructor(itemLen) {
            this.itemLen = itemLen || 15;
            this.totalPage = 0;
            this.assets = {};
            this.flatAssets = [];
      }

      updateAssetsData(assetTypeData, assetFlatData) {
            this.assets = assetTypeData;
            this.flatAssets = assetFlatData;
      }


      getDataByOptions(type, options) {
            let data = this.assets[type] || this.flatAssets;
            if (options && options.search) {
                  data = this.getSearchData(data, options.search);
            }

            if (options && options.sort) {
                  data = this.getSortData(data, options.sort);
            }

            return data;
      }


      getSearchData(data, options) {
            //TODO 배치 미배치 항목 추가
            let list = data.concat();
            let field = options.field;
            let keyword = options.keyword;

            if (field && keyword) {
                  list = list.filter((x) => {
                        let value = x[field];
                        if (value && value.indexOf(keyword) != -1) {
                              return x;
                        }
                  })
            }

            window.server10 = data.find(x => x.id == "server10");

            if (options.onlyUnpositioned === true) {
                  list = list.filter(x => !x.instance_id && !x.parent_id);
            }

            if (options.onlyPositioned === true) {
                  list = list.filter(x => x.instance_id || x.parent_id);
            }
            return list;
      }


      getSortData(data, options) {

            let list = data.concat();
            let key = options.field;
            let state = options.state;
            let sortOrder = 1;

            if (key && state) {
                  if (state == "descending") {
                        sortOrder = -1;
                  }

                  list.sort(function (a, b) {
                        var result = (a[key] < b[key]) ? -1 : (a[key] > b[key]) ? 1 : 0;
                        return result * sortOrder;
                  });
            }

            return list;
      }

      getPaginationData(data, page) {
            let maxPage = Math.ceil(data.length / this.itemLen);
            page = Math.min(maxPage, Math.max(1, page));
            return {
                  page: page,
                  totalPage: maxPage,
                  data: data.slice((page - 1) * this.itemLen, (page - 1) * this.itemLen + this.itemLen)
            }
      }

}


AssetComponentProxy.TYPE = {
      CCTV: "CCTV",
      ACCESS: "Access",
      LEAK: "WaterLeak",
      FIRE: "Fire",
      TEMPHUMI: "THSensor",
      MDM: "MDM",
      GAS: "GasLeak",
      EARTH: "EarthQuake",
      PDU: "PDU",
      RACK: "Rack",
      SERVER: "Server",
      NETWORK: "Network",
      STORAGE: "Storage",
      BACKUP: "Backup",
      THERMOHYGROSTAT: "Thermohygrostat"
};

AssetComponentProxy.EFFECT = {
      FIRE: "FIRE"
}

AssetComponentProxy.NOTI_SELECTED_ASSET = "notification/selectedAsset";

