/*
2019.01.24- 19:12
ckkim

- 주요 3D 자산 컴포넌트 속성

- selected
      - 선택 아웃라인 활성화  + over,out 이벤트 무시

- toggleSelectedOutline(true/false)
      - 선택 아웃라인을 활성화/비활성화 처리

- toggleStateDisplay(true/false)
      =  아이콘 + 라벨 활성화 유무

 */
class AssetComponentControllerPlugin extends ExtensionPluginCore {

      constructor() {
            super();
            this.threes = null;
            this._popupInstance = null;
            this._popCloseHandler = null;

            this._actionManager = new ActionManager();
            this._selectedComInstance = null;

            this.lang = Vue.$i18n.messages.wv.dcim_pack;

            // wemb 변수에 컨트롤러 참조 기억
            window.wemb.assetComponentControllerPlugin = this;

            // wemb.threeElements.renderer.domElement.addEventListener('mousedown', function(event){
            //       if(CPUtil.getInstance().browserDetection() != "Internet Explorer"){
            //             if(event.ctrlKey === true && event.buttons === 2){
            //                   $('document').on("contextmenu",function(e){
            //                         return true;
            //                   });
            //                   console.log(window.wemb.threeElements.threeLayer.mouseOveredComponent);
            //                   if(window.wemb.threeElements.threeLayer.mouseOveredComponent === null)
            //                         $(wemb.threeElements.renderer.domElement).on('contextmenu', function(){return false;})
            //                   else if(
            //                         window.wemb.threeElements.threeLayer.mouseOveredComponent.data.asset_type === 'CCTV' ||
            //                         window.wemb.threeElements.threeLayer.mouseOveredComponent.data.asset_type === 'AccessSensor'
            //                         )
            //                         {
            //                         if($('#vlc-link')){
            //                               $('#vlc-link').remove()
            //                         }
            //                         //현재 mouseover된 컴포넌트의 자산 id를 가지고 dcim manager를 통해 자산 정보를 획득 후에
            //                         //적절한 url을 만들어 태그로 붙인다.
            //
            //                         let assetInfo = window.wemb.threeElements.threeLayer.mouseOveredComponent.data;
            //                         let tempUrl = assetInfo.rtsp_url.replace("rtsp://", `rtsp://${assetInfo.rtsp_id}:${assetInfo.rtsp_pass}@`);
            //
            //                         $("#viewerMainArea").append("<a href = 'javascript: void(0)' id='vlc-link' style='width : 30px; height : 30px; cursor : default'></a>")
            //                         $('#vlc-link').attr('href', tempUrl)
            //                         $('#vlc-link').css('position', 'absolute')
            //                         $('#vlc-link').css('top', event.offsetY)
            //                         $('#vlc-link').css('left', event.offsetX)
            //                         $(wemb.threeElements.renderer.domElement).on('contextmenu', function(){return true;})
            //                   }
            //                   setTimeout(function(){
            //                         $('#vlc-link').remove()
            //                         $('document').on("contextmenu",function(e){
            //                               return false;
            //                         });
            //                   }, 5000)
            //             }
            //       }else {
            //             if(event.ctrlKey === true && event.buttons === 2) {
            //                   $('document').on("contextmenu",function(e){
            //                         return false;
            //                   });
            //                   if(window.wemb.threeElements.threeLayer.mouseOveredComponent === null){
            //                         $(wemb.threeElements.renderer.domElement).on('contextmenu', function(){return false;})
            //                   }else if(
            //                         window.wemb.threeElements.threeLayer.mouseOveredComponent.data.asset_type === 'CCTV' ||
            //                         window.wemb.threeElements.threeLayer.mouseOveredComponent.data.asset_type === 'AccessSensor'
            //                   ){
            //                         $("#viewerMainArea").append("<a href = 'javascript: void(0)' id='vlc-link' style='width : 30px; height : 30px; cursor : default'></a>")
            //                         $('#vlc-link').css('position', 'absolute')
            //                         $('#vlc-link').css('top', event.offsetY - 15)
            //                         $('#vlc-link').css('left', event.offsetX + 15)
            //                         $(wemb.threeElements.renderer.domElement).on('contextmenu', function(){return true;})
            //                         $.contextMenu({
            //                               selector: '#vlc-link',
            //                               callback: function (key, options) {
            //                                     console.log(key)
            //                                     let assetInfo = window.wemb.threeElements.threeLayer.mouseOveredComponent.data;
            //                                     let shell = new ActiveXObject("WScript.Shell");
            //                                     //C:\Program Files (x86)\VideoLAN\VLC\vlc.exe
            //                                     shell.run('vlc ' + assetInfo.rtsp_url + ' --rtsp-user=' + assetInfo.rtsp_id + ' --rtsp-pwd=' + assetInfo.rtsp_pass, 1, false)
            //                               },
            //                               items: {
            //                                     "edit": {name: "Open in VLC..", icon: "vlc"},
            //                               }
            //                         })
            //
            //                         // console.log("IE CTRL + CONTEXT MENU")
            //                         // let assetInfo = window.wemb.threeElements.threeLayer.mouseOveredComponent.data;
            //                         //
            //                         // let shell = new ActiveXObject("WScript.Shell");
            //                         // //C:\Program Files (x86)\VideoLAN\VLC\vlc.exe
            //                         // shell.run('vlc ' + assetInfo.rtsp_url + ' --rtsp-user=' + assetInfo.rtsp_id + ' --rtsp-pwd=' + assetInfo.rtsp_pass, 1, false)
            //
            //                   }
            //             }
            //       }
            // });



      }



      async setFacade(facade) {
            super.setFacade(facade);
      }

      start() {
            this.lang = Vue.$i18n.messages.wv.dcim_pack;
            this.threes = window.wemb.threeElements;

            /*
		이벤트 정보가 오는 경우 이벤트 정보를 컴포넌트에 적용하기
		폴링 방식으로 주기적으로 실행됨.
		 */
            wemb.dcimManager.eventManager.$on(DCIMManager.GET_EVENT_LIST, (data) => {
                  // 이벤트 변경시마다 파이어 효과 유효성 유무 체크하기.
                  // 단, 이 내용은 site page에서만 실행.
                  this._syncBuildingFireEffect();

            })

            wemb.dcimManager.eventManager.$on(DCIMManager.GET_ASSET_EVENT_LIST, (data) => {
                  // 자산 컴포넌트에  이벤트 상태 반영하기.
                  this._updateAssetComponentStateByEventDataList(data);
            })


            /*
		에크 처리 이벤트가 발생하는 경우 불효과 제거하기
		 */
            wemb.dcimManager.eventManager.$on(DCIMManager.EVENT_COMPLETE_ACK, (data) => {
                  try {
                        // 효과 제거 후 신규 등록
                        this._actionManager.clearFireEffectAction();
                        this._updateFireEffect();
                  } catch (error) {
                        console.log("There is no assetList in the data after ack processing.");
                  }
            })

            // 팝업이 닫힐때 카메라 위치를 원래 위치로 처리하기 위한 핸들러기
            this._popCloseHandler = this._onPopupClose.bind(this);


      }

      get notificationList() {

            return [

                  ViewerProxy.NOTI_OPENED_PAGE,
                  ViewerProxy.NOTI_BEFORE_CLOSE_PAGE

            ]
      }

      /*
	noti가 온 경우 실행
	call : mediator에서 실행
	 */
      handleNotification(note) {
            let body = note.getBody();
            switch (note.name) {
                  // 편집 페이지가 열릴때마다 실행
                  case ViewerProxy.NOTI_OPENED_PAGE:
                        this.noti_openedPage();
                        break;
                  case ViewerProxy.NOTI_BEFORE_CLOSE_PAGE :
                        this.noti_beforeClosePage(body);
                        break;
            }
      }



      destroy() {}


      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       설정 영역 끝
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       **/


      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       noti 처리 영역 시작
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       **/


      async noti_openedPage() {
            // 페이지 시작 값 저장
            window.wemb.mainControls.saveState();

            // 자산별 Action 이벤트 등록
            this._registActionEventListener();

            /*
                 페이지가 열리자 마자 이벤트 정보(severity)를 컴포넌트에 반영하기

                 2019.02.14

             */

            let eventList = await wemb.dcimManager.eventManager.syncPageEventData();
            if(eventList){
                  this._updateAssetComponentStateByEventDataList(eventList);
            }


            // 페이지 이동시 쿼리 정보가 존재하는 경우, 자산 컴ㅍ모넌트로 이동후 팝업 호출
            this._checkQueryParams();

            // 부지 페이지일때 화재 이벤트가 있는 경우 이벤트 처리 하기
            if (this._checkSitePage()) {
                  this._updateFireEffect();
            }
      }

      /*
      주의!!!
      페이지가 닫히는 경우 팝업 윈도우 인스턴스 부터, 컴포넌트 인스턴스가 까지 모두 제거되기 때문에
      페이지가 닫히기 전에 처리해야함.
       */
      noti_beforeClosePage(pageId="") {
            // 페이지가 닫힐때 활성화된 팝업 처리 초기화
            // 이벤트 등록 삭제 처리
            this._removeActivePopup();

            this._removeSelectedAssetItem();

            if(wemb.posodManager !== undefined)
                  wemb.posodManager.closeQENXViewer();
            else
                  console.log("Posod Manager is undefined");

            // 활성화된 액션 비활성화
            this._actionManager.clearCurrentAction();


            if(this._checkSitePage()) {
                  this._actionManager.clearFireEffectAction();
            }
      }



      /** □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       noti 처리 영역 끝
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ **/


      /** □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       실행 영역 시작
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ **/


      /*
	팝업이 닫히고 난 후 다음 처리 작업
	 */
      _onPopupClose() {

            // 페이지가 닫힐때 활성화된 팝업 처리 초기화
            // 이벤트 등록 삭제 처리
            this._removeActivePopup();


            // 활성화된 액션 비활성화
            this._actionManager.clearCurrentAction();

            // 선택 제거
            this._removeSelectedAssetItem();

            // 카메라 원위치 처리
            this._transitionOut();


      }


      /*
	활성화된 팝업 제거하기
	 */
      _removeActivePopup() {
            try {
                  // 이벤트 등록 삭제 처리


                  if (this._popupInstance) {
                        // 팝업이 어떤 경우에의해 존재하지 않은 경우도 있음.
                        if(wemb.popupManager.hasPopupInstance(this._popupInstance))
                              this._popupInstance.removeEventListener("event/closedPopup", this._popCloseHandler);
                  }

                  this._popupInstance = null;
            } catch (error) {
                  this._popupInstance = null;
                  console.log("error removeActivePopup ", error);
            }
      }




      // 모든 객체 초기화 배경 클릭 시 호출 됨
      _transitionOut() {
            this._cameraTransitionOut();
      }


      // 카메라 원위치
      _cameraTransitionOut() {
            this.threes.mainControls.transitionFocusInTarget(
                  this.threes.mainControls.position0.clone(),
                  this.threes.mainControls.target0.clone(),
                  1
            );
      }


      /*
	2018.11.13(ckkim)
	이벤트 메니저를 통해, 자산 상태가 critical인 경우
	자산에 연결된 자산 컴포넌트 상태를 변경해야함.

	 */
      _updateAssetComponentStateByEventDataList(eventDataList) {
            try {

                  // 기존 자산 상태 값을 normal로 만들기
                  window.wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.forEach((compositionInfo)=>{
                        if (compositionInfo) {
                              compositionInfo.comInstance.changeNotification({
                                    type: DCIMManager.NOTI_CHANGE_SEVERITY,
                                    body: WV3DAssetComponent.STATE.DEFAULT
                              });

                        } else {
                              console.log("warning _updateAssetComponentStateByEventDataList.  There are no assets corresponding to " + assetId);
                        }
                  })

                  let eventDataMap = this._filterEventDataMapByLevel(eventDataList);
                  eventDataMap.forEach((eventData) => {
                        let assetId = eventData.asset_id;
                        // 자산 정보 및 컴포넌트 정보 구하기
                        let compositionInfo = window.wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(assetId);
                        if (compositionInfo) {
                              compositionInfo.comInstance.changeNotification({
                                    type: DCIMManager.NOTI_CHANGE_SEVERITY,
                                    body: eventData.severity
                              });

                        } else {
                              console.log("warning _updateAssetComponentStateByEventDataList.  There are no assets corresponding to " + assetId);
                        }
                  })

            } catch (error) {
                  console.log("error _updateAssetComponentStateByEventDataList() Errors during execution", error);
            }
      }




      /*
	2018.11.15(ckkim)
	eventDataList에서 asset_id를 기준으로
		event level이 높은 경우의 event만 골라내기
		event level이 같은 경우 최근 이벤트 만 처리

	 */
      _filterEventDataMapByLevel(eventDataList) {
            let eventMap = new Map();
            let EVENT_LEVEL = {
                  "normal": 1,
                  "minor": 2,
                  "warning": 3,
                  "major": 4,
                  "critical": 5
            }

            for (let i = 0; i < eventDataList.length; i++) {
                  let eventData = eventDataList[i];
                  let assetId = eventData.asset_id;
                  if (eventMap.has(assetId) == true) {
                        let saveEventData = eventMap.get(assetId);
                        let saveLevel = EVENT_LEVEL[saveEventData.severity];
                        let eventLevel = EVENT_LEVEL[eventData.severity];

                        if (saveLevel < eventLevel) {
                              eventMap.set(assetId, eventData);
                              continue;
                        }

                        // 동일한 경우 타임스템프가 큰 경우로 대처
                        if (saveLevel == eventLevel) {
                              if (parseInt(saveEventData.stime) < parseInt(eventData.stime)) {
                                    eventMap.set(assetId, eventData);
                              }
                        }


                  } else {
                        eventMap.set(assetId, eventData);
                  }

            }

            return eventMap;
      }

      setHighLevelSeverity(dataObj, severity) {
            let eventLevel = {
                  "normal": 1,
                  "minor": 2,
                  "warning": 3,
                  "major": 4,
                  "critical": 5
            }


            $.each(dataObj, (status, count) => {
                  if (eventLevel[status] > severity.level) {
                        severity.level = eventLevel[status];
                        severity.name = status;
                  }
            })
      }


      /*
	자산 컴포넌트에 기본 이벤트 처리 추가 이벤트 등록 시 3d 자산 컴포넌트만  this._3dAssetInstanceList에 추가함.
	 */
      _registActionEventListener() {
            if (wemb.configManager.exeMode == "viewer") {
                  //this._3dAssetInstanceList = [];
                  try {
                        wemb.mainPageComponent.$threeLayer.forEach((comInstance) => {
                              if (window.DirectionArrow && comInstance instanceof DirectionArrow) {
                                    comInstance.visible = false;
                                    return;
                              }


                              // 01. 자산 컴포넌트 이면서
                              if (window.WV3DAssetComponent && comInstance instanceof WV3DAssetComponent) {

                                    // 02. 자산이 맵핑되어 있는 컴포넌트만
                                    if (comInstance.assetId) {


                                          // dblclick 처리
                                          comInstance.onWScriptEvent("dblclick", () => {
                                                this.gotoAsset3DComponent(comInstance.assetId, true, true);
                                          })


                                          // click 처리
                                          comInstance.onWScriptEvent("click", () => {
                                                this._onClickAssetComponent();
                                          })
                                    }
                              }

                        });
                  } catch (error) {
                        console.log("error", error);
                  }

            }
      }


      /*
	2018.11.15(ckkim)

	페이지가 열릴때 쿼리 파라메터가 있는 경우,
	파라메터에 따른 초기 시작 처리

	만약 assetId가 존재하는 경우 이동 + 팝업 처리,
		일반적으로 event browser에서 event를 클릭할 때
		또는
		출입센서 소켓 이벤트 데이터가 발생할 때 실행.

	만약 fire가 존재하는 경우 화재 처리, 우선 순위는 fire
	call : 페이지가 열릴때 실행.

	 */
      _checkQueryParams() {
            // 쿼리 파람 처리
            // OpenPageByName, ById() 호출시 파라메터로 넘어오는 경우
            let qParams = window.wemb.mainPageComponent.params;

            if (qParams && qParams.hasOwnProperty("assetId")) {

                  this.gotoAsset3DComponent(qParams.assetId, true, true);
            }
      }


      // 부지 페이지 일때 화재 이벤트가 존재할때 활성화 처리
      /*
	2019.01.09(ckkim)
	 */
      _checkSitePage() {
            //1. 부지 페이지인지 확인하기
            let siteData = wemb.dcimManager.pagesetManager.getSite();

            if(!siteData)
                  return false;

            if(!wemb.pageManager.currentPageInfo)
                  return false;

            // 페이지 정보에 사이트 페이지가 없는 경우 X
            if (wemb.pageManager.hasPageInfoBy(siteData.id) == false)
                  return false;



            // 이동 페이지가
            if (siteData.id == wemb.pageManager.currentPageInfo.id)
                  return true;


            return false;

      }


      executeFireEffectByAssetId(assetId) {
            try {
                  // 대상 구하기
                  // fire 이벤트가 발생한 자산이 위치하는 pageid 구하기
                  let assetPageId = wemb.dcimManager.assetManager.getPageIdPlaced(assetId);

                  // fire  이벤트가 발생한 자산의 페이지가 포함된 동(층 리스트) 페이지 정보 구하기
                  /*
			현재는 pageName을 사용하는데
			추후 pageId로 변경해야함.
				*/
                  let dongPageName = window.wemb.dcimManager.pagesetManager.getParentBuilding(assetPageId).page_name;


                  // 동페이지에 해당하는 3D 컴포넌트 인스턴스 구하기
                  // 현재는 동페이지 이름이 = 부지의 건물이름(instanceName)과 동일
                  /*
			추후 수정해야함.
			2018.11.14(ckkim)
			*/

                  let dongBuildingInstance = wemb.mainPageComponent.$threeLayer.get(dongPageName);
                  if (dongBuildingInstance) {
                        // 파이어 효과
                        //this._actionManager.addFireEffect(dongPageName, dongBuildingInstance.position, dongBuildingInstance.size.z);
                        this._actionManager.addFireEffect(dongPageName, dongBuildingInstance);
                  }
            } catch (error) {
                  console.log("AssetComponentConttrollerPlugin.executeFireEffectByAssetId()  error ", error);
            }
      }


      /*
      메인 부지 화면에서 화재 효과 테스트를 두 대의 컴퓨터에서 테스트 하는 경우

      이슈:
            한곳에서 ack 처리를 하는 경우
            한곳만 불이 꺼지고 다른 한곳은 불이 꺼지지 않게 됨.

      처리 방법:
            이벤트는 특정 주기로 폴링됨.
            폴링 주기에 화재 발생 효과 체크하기
            단, 체크는 부지일때만 체크


      구현 방법:
            단계01: 파이어 이벤트 자산 id 목록 구하기
            단계02: 삭제 대상 파이어 이벤트 찾기
                  기존 파이어 효과 = 건물이름을 키로 fire효과가 매핑되어 있음.
                  01. 신규 파이어 이벤트 자산 id를  가지고 dong이름을 구한다.
                  02. 기존 파이어 건물 목록과 신규 파이어 건물 목록을 비교해
                        신규 파이어 건물 목록에서 삭제해야할 건물 명을

       */


      _syncBuildingFireEffect(){
            // 메인 사이트일때만 체크하기.
            if (this._checkSitePage()==false)
                  return;

            //2. 파이어 이벤트만 가져오기
            let asseDatatList = wemb.dcimManager.eventManager.eventBrowserList.critical.filter((eventData) => {
                  return eventData.asset_type == AssetComponentProxy.TYPE.FIRE
            })


            // critical 이벤트가 없는 경우 fire 효과를 모두 지우기
            if(asseDatatList.length==0){
                  // 효과 제거 후 신규 등록
                  this._actionManager.clearFireEffectAction();
                  return;

            }




            // 빌딩이름 목록 구하기.
            let buildingNameMap = new Map();
            //자산 정보를 가지고 building 목록을 구한다.
            asseDatatList.forEach((assetData)=>{
                  // fire 이벤트가 발생한 자산이 위치하는 pageid 구하기
                  let assetPageId = wemb.dcimManager.assetManager.getPageIdPlaced(assetData.asset_id);
                  let dongPageName = window.wemb.dcimManager.pagesetManager.getParentBuilding(assetPageId).page_name;

                  if(buildingNameMap.has(dongPageName)==false){
                        buildingNameMap.set(dongPageName, dongPageName);
                  }
            })


            // 싱크 처리하기
            // 신규 이름과  기존 이름을 비교해 신규 이름에 없는 효과를 제거한다.
            let fireEffect = this._actionManager.getActionByType(AssetComponentProxy.EFFECT.FIRE_EFFECT);
            fireEffect.syncFireEffect(buildingNameMap);

      }


      /*
	규칙:
		3D컴포넌트인스턴스(건물).name = 페이지 이름과 동일해야함.
	 */
      _updateFireEffect() {

            try {
                  //2. 파이어 이벤트만 가져오기
                  let assetList = wemb.dcimManager.eventManager.eventBrowserList.critical.filter((eventData) => {
                        return eventData.asset_type == AssetComponentProxy.TYPE.FIRE
                  })

                  // fire action 효과 추가하기
                  assetList.forEach((tempAssetData) => {
                        let assetId = tempAssetData.asset_id;
                        this.executeFireEffectByAssetId(assetId);
                  })

            } catch (error) {
                  console.log("AssetComponentConttrollerPlugin._updateFireEffect()  error ", error);
            }
      }


      /*
	2018.11.13(ckkim)
	자산 컴포넌트에서 클릭 시
	클릭
	 */
      _onClickAssetComponent() {
            this._facade.sendNotification(AssetComponentControllerPlugin.NOTI_SELECTED_COMPONENT);
      }


      /* 팝업 활성화
         - @comInstance : 팝업 연관 자산 컴포넌트 인스턴스
         - @assetAction : 자산 액션 인스턴스, 자산 팝업 페이지의 파라메터로 전달됨, 자산 팝업에서 action을 처리해야 하는 경우가 발생함.
      * */
      _executePopupShow(comInstance, assetAction) {

            var x = (window.wemb.mainPageComponent.width / 2) + 200;
            var pageId = comInstance.popupId;
            var pageInfo = window.wemb.pageManager.getPageInfoBy(pageId);


            // 팝업 페이지 정보가 없는 경우 기존 열린 팝업 페이지를 닫는다.
            if (pageInfo == null || window.wemb.pageManager.hasPageInfoByName(pageInfo.name) == false) {
                  console.warn("AssetComponentControllerPlugin throws an error popping up an asset, There is no asset pop-up page for " + pageId);
                  return;
            }

            /*
		2018.11.21(ckkim)
		자산 데이터 넘기기
		 */
            var options = {
                  x: x,
                  y: 100,
                  title: this.lang.assetComponentControllerPlugin.assetDetail,
                  params: {
                        assetId: comInstance.assetId,
                        assetData: comInstance.data,
                        popupOwner: comInstance,
                        controller: this,
                        action: assetAction
                  }
            }

            // 팝업 호출
            this._popupInstance = wemb.popupManager.open(pageInfo.name, options);
            // 팝업 타입 설정
            this._popupInstance.name = pageInfo.name;

            // 팝업이 닫히는 경우 이벤트 처리
            if (this._popupInstance && !this._popupInstance.hasEventListener("event/closedPopup", this._popCloseHandler)) {
                  this._popupInstance.addEventListener("event/closedPopup", this._popCloseHandler);
            }
      }

      /*
	카메라 이동 처리
	 */
      //cctv context click text do not erasse
      toScreenPosition(obj, camera)
      {
            var vector = new THREE.Vector3();

            var widthHalf = 0.5*wemb.threeElements.threeLayer._renderer.context.canvas.width;
            var heightHalf = 0.5*wemb.threeElements.threeLayer._renderer.context.canvas.height;

            obj.updateMatrixWorld();
            vector.setFromMatrixPosition(obj.matrixWorld);
            vector.project(camera);

            vector.x = ( vector.x * widthHalf ) + widthHalf;
            vector.y = - ( vector.y * heightHalf ) + heightHalf;

            return {
                  x: vector.x,
                  y: vector.y
            };

      };
      _cameraTransitionInInstance(instance, callback) {

            // instance의 스케일 정보를 초기 벡터에 반영
            let posZ = +instance.size.z + 50;
            let posY = Math.max(40, +instance.size.y + 40);
            let offset = new THREE.Vector3(0, posY * instance.scaleRate.y, posZ * instance.scaleRate.z);
            offset = offset.applyMatrix4(instance.appendElement.matrixWorld);

            // cctv context click text do not erasse
            // var client_coordinate = this.toScreenPosition(instance.appendElement, wemb.mainControls._camera);
            // this.threes.mainControls.transitionFocusInTarget(
            //       offset,
            //       instance.appendElement.position.clone().add({ x: 0, y: 0, z: 0 }),
            //       1, callback,
            //       [client_coordinate]
            // );
            this.threes.mainControls.transitionFocusInTarget(
                  offset,
                  instance.appendElement.position.clone().add({ x: 0, y: 0, z: 0 }),
                  1
            );
      }


      // private
      ////////////////////////////////////////////////////


      ////////////////////////////////////////////////////
      // public
      /*
	 assetId에 해당하는 컴포넌트로 이동 및 팝업 활성화 하기

	 주의: click, dblclick 시에는 사용하지 않음.
	*/
      gotoAsset3DComponent(assetId, showPopup = true, clearSelectedItem=false) {
            if (wemb.configManager.exeMode != "viewer") {
                  return false;
            }



            try {

                  //////////////////////////////
                  // 기존 선택된 내용 원위치
                  // 이벤트 등록 삭제 처리
                  this._removeActivePopup();

                  //1. 팝업이 열려 있으면 닫기
                  wemb.popupManager.closedAllPopup();

                  // 기존 선택 액션 clear처리 하기
                  this._actionManager.clearCurrentAction();


                  // 기존 선택 자산을 임시적으로 저장해놓기
                  let oldAssetId = "";
                  if(this._selectedComInstance){
                        oldAssetId = this._selectedComInstance.assetId;
                  }


                  // 선택 자산 비활성화
                  this._removeSelectedAssetItem();

                  // 기존과 동일하다면 카메라만 원위치
                  if(clearSelectedItem==true && assetId==oldAssetId){
                        //- 카메라 원위치
                        this._transitionOut();
                        return;
                  }
                  //////////////////////////////



                  //////////////////////////////
                  // 대상 구하기
                  // assetId를 이용해 컴포넌트 찾기
                  var assetCompositionInfo = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(assetId);


                  if (assetCompositionInfo == null) {
                        //console.log(`The component set in the ${assetId} does not exist.`);
                        Vue.$message(`The component set in the ${assetId} does not exist.`);
                        return false;

                  }else {

                        // componentID를  가지고 컴포넌트 인스턴스 구하기
                        let comInstance = assetCompositionInfo.comInstance;

                        // assetAction 정보 구하기
                        try {

                              let assetInfo = comInstance.data;
                              // 페이지 이동시 활성화된 액션을 취소 처리하기 위해 반드시 저장해야함.
                              this._actionManager.currentAction = this._actionManager.getActionByType(assetInfo.asset_type);
                              if (this._actionManager.currentAction) {
                                    this._actionManager.currentAction.action(comInstance);
                              }


                        } catch (error) {
                              console.log("ERROR AssetComponentControllerPlugin _executePopupShow", error);
                        }


                        // 팝업 활성화
                        if (showPopup == true) {
                              this._executePopupShow(comInstance, this._actionManager.currentAction);
                        }


                        // 선택처리(wireframe  활성화, 말풍선 활성화)
                        this.setSelectedAssetItem(comInstance);


                        // 카메라 이동
                        var that = this;
                        this._cameraTransitionInInstance(comInstance);
                        //cctv context click text do not erasse
                        // this._cameraTransitionInInstance(comInstance, function(clientCoord){
                        //       console.log("################### CLIENT COORDINATS #########################");
                        //       console.log(clientCoord);
                        //       if(assetCompositionInfo.assetType === 'CCTV'){
                        //             if (CPUtil.getInstance().browserDetection() != "Internet Explorer") {
                        //                   //assetInfo
                        //                   //       if($('#vlc-link')){
                        //                   //             $('#vlc-link').remove()
                        //                   //       }
                        //                   //       // 1 . a tag를 붙여서 context 메뉴를 띄우는 방법
                        //                   //       // - context menu는 브라우저 영역이 아니므로 프로그램으로 띄울 수 없음(보안 문제)
                        //                   //       // - 처음 클릭했던 지점에서 카메라가 이동하면서 좌표가 틀어지기 때문에 이동 된 후에 a 태그를 붙일 수 있음
                        //                   //       // - 줌 인 되고 자동으로 뭔가 띄우고 싶다면 레지스트리를 수정해서 실행하는 방법이 있음
                        //                   //       // - 레지스트리를 수정하는 때에는 태그의 href에 값을 넣고 강제 클릭 시키면 되므로 꼭 줌인이 된 후에 코드를 삽입할 필요는 없음
                        //                   //
                        //                   //       let assetInfo = assetCompositionInfo.assetInfo;
                        //                   //       let tempUrl = assetInfo.rtsp_url.replace("rtsp://", `rtsp://${assetInfo.rtsp_id}:${assetInfo.rtsp_pass}@`);
                        //                   //
                        //                   //       $("#viewerMainArea").append("<a href = '' id='vlc-link' style='width : 30px; height : 30px; cursor : default; background-color: white' >CCTV</a>")
                        //                   //       $('#vlc-link').attr('href', tempUrl)
                        //                   //       $('#vlc-link').css('position', 'absolute')
                        //                   //       $('#vlc-link').css('top', clientCoord.y - 15)
                        //                   //       $('#vlc-link').css('left', clientCoord.x +15)
                        //             }
                        //             else{
                        //                   //ie는 activeX로 바로 띄울 수 있다.
                        //                   // $("#viewerMainArea").append("<a href = '' id='vlc-link' style='width : 30px; height : 30px; cursor : default; background-color: white' ></a>")
                        //                   // $('#vlc-link').css('position', 'absolute')
                        //                   // $('#vlc-link').css('top', clientCoord.y - 15)
                        //                   // $('#vlc-link').css('left', clientCoord.x -15)
                        //             }
                        //       }
                        // });

                        // 자산 선택 이벤트 발생
                        //
                        wemb.$globalBus.$emit(AssetComponentProxy.NOTI_SELECTED_ASSET, assetId);
                        return true;
                  }


            } catch (error) {
                  console.log(`Error moving to ${assetId}`, error);
                  return false;
            }
      }


      /*
	진행 순서

	1. 팝업이 열려 있으면 닫기
	2. 자산이 위치한 동 페이지 구하기
	3. fire 효과 출력



	 */
      gotoFireAsset3DComponent(assetId) {
            if (wemb.configManager.exeMode != "viewer" && !this._checkSitePage()) {
                  return false;
            }

            //1. 팝업이 열려 있으면 닫기
            wemb.popupManager.closedAllPopup();


            // 이벤트 등록 삭제 처리
            this._removeActivePopup();


            // 파이어 효과 발생, assetId에 해당하는 건물을 찾게됨.
            this.executeFireEffectByAssetId(assetId);

      }


      /*
	컨트롤러 리셋 처리
	1. 활성화 팝업 닫기
	2. 카메라 원위치
	주의: Fire는 제외 시킴
	call : POSOD 기본 위치에서 호출
	 */
      resetController() {
            //1. 활성화 팝업 닫기


            //1. 팝업이 열려 있으면 닫기
            wemb.popupManager.closedAllPopup();

            // 이벤트 등록 삭제 처리
            this._removeActivePopup();

            // 현재 활성화 된 효과 제거
            this._actionManager.clearCurrentAction();


            //3. 카메라 원위치
            this._cameraTransitionOut();

            //선택 비활성화 처리
            this._removeSelectedAssetItem();
      }


      setSelectedAssetItem(comInstance){
            this._selectedComInstance = comInstance;
            if(this._selectedComInstance){

                  // 선택 아웃라인
                  this._selectedComInstance.selected=true;
                  /*
                  아이콘+ 라벨 보이기
                   */
                  this._selectedComInstance.toggleStateDisplay(true);

            }
      }

      _removeSelectedAssetItem(){

            try {
                  if (this._selectedComInstance) {
                        this._selectedComInstance.selected = false;
                  }

                  this._selectedComInstance = null;
            }catch(error){
                  console.warn("ERROR = ", error);
            }
      }

}

AssetComponentControllerPlugin.CONNECT_LINE = "assetConnect_line";
AssetComponentControllerPlugin.NOTI_SELECTED_COMPONENT = "notification/selectedAssetComponent";



//*****
// 컴포넌트 액션 설정
//*****
(function(){


      class ActionManager {

            set currentAction(action) {
                  this._currentAction = action;
            }

            get currentAction() {
                  return this._currentAction;
            }


            constructor() {
                  this._actionMap = null;
                  this._currentAction = null;
                  this._createActionMap();
            }

            _createActionMap() {
                  this._actionMap = new Map();
                  this._actionMap.set(AssetComponentProxy.TYPE.CCTV, new CCTVAction(this));
                  this._actionMap.set(AssetComponentProxy.TYPE.PDU, new PDUAction(this));
                  this._actionMap.set(AssetComponentProxy.TYPE.RACK, new RackAction(this));
                  this._actionMap.set(AssetComponentProxy.TYPE.ACCESS, new AccessAction(this));
                  this._actionMap.set(AssetComponentProxy.TYPE.FIRE, new FireAction(this));
                  this._actionMap.set(AssetComponentProxy.EFFECT.FIRE_EFFECT, new FireEffectAction(this));
            }

            clearCurrentAction() {
                  if (this._currentAction) {
                        this._currentAction.clear();
                        this._currentAction = null;
                  }
            }

            clearFireEffectAction() {
                  let fireEffectAction = this._actionMap.get(AssetComponentProxy.EFFECT.FIRE_EFFECT);
                  if (fireEffectAction) {
                        fireEffectAction.clear();
                  }
            }




            getActionByType(typeName) {
                  // assetAction 정보 구하기
                  let assetAction = this._actionMap.get(typeName);

                  return assetAction;
            }




            /*
          불효과 활성화
           */
            addFireEffect(name, comInstance ) {
                  let fireEffect = this._actionMap.get(AssetComponentProxy.EFFECT.FIRE_EFFECT);
                  fireEffect.addFireEffect(name, comInstance);
            }


            // 타입에 따른 액션 실행.
            executeAction(comInstance) {

                  try {

                        let assetInfo = comInstance.data;


                        let assetAction = this._actionMap.get(assetInfo.asset_type);
                        if (assetAction == null) {
                              return null;
                        }
                        assetAction.action(comInstance);

                  } catch (error) {
                        console.log("ERROR ActionManager executeAction", error);
                  }
            }

      }


      class AssetAction {

            get eventBus(){
                  return this._$eventBus;
            }
            constructor(controller) {
                  this._controller = controller;
                  this._$eventBus = new Vue();
            }


            // action이 실행될때 실행.
            action(comInstance) {

            }

            // 페이지가 닫힐때 실행
            clear() {
                  if(this._$eventBus){
                        this.eventBus.$off();
                  }
            }


            $on(eventName, listener){
                  this._$eventBus.$on(eventName, listener);
            }

            $off(eventName, listener){
                  this._$eventBus.$off(eventName, listener);
            }




            //////////////////////////////////
            // 특정 자산과 연관된 CCTV 자산 목록을 play 리스트로 만들어 실행
            playRelationCCTVList(cctvAssetInfoList){

                  // cctvAssetInfoList에는 cctv assetid만 들어 있기때문에
                  // 이 값을 가지고 자산정보를 구해야함.
                  cctvAssetInfoList = cctvAssetInfoList.map((info)=>{
                        try {
                              let assetInfo = wemb.dcimManager.assetManager.assetFlatMap.get(info.child_id)
                              return assetInfo;
                        }catch(error){
                              console.warn("## ", error);
                        }
                  })


                  if(cctvAssetInfoList.length<=0){
                        return;
                  }


                  /*
                        QENXLivePopViewer 파라메터 생성
                   */
                  try{
                        let playList = cctvAssetInfoList.map((assetInfo)=>{
                              return assetInfo.rtsp_url.replace("rtsp://", `rtsp://${assetInfo.rtsp_id}:${assetInfo.rtsp_pass}@`);
                        })
                        let playListUrlString = playList.join(",");
                        let params = {
                              url: playListUrlString,
                              options: cctvAssetInfoList[0].rtsp_options
                        }
                        if(wemb.posodManager !== undefined)
                              wemb.posodManager.openQENXLivePopViewer(params);
                  }catch(error){
                        Vue.$message("The player can not run because there is no RTSP information in the asset information.");
                        console.log("cctvAssetInfoList", cctvAssetInfoList);
                  }


            }

            // 특정 자산과 연관된 CCTV 자산을 선택 상태로
            activeRelationCCTVList(cctvAssetInfoList, relationAssets){

                  let relationAsset, relationData;
                  for( let i=0; i<cctvAssetInfoList.length; i++ ) {
                        relationData = cctvAssetInfoList[i];
                        if(wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.has(relationData.child_id)){
                              relationAsset = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(relationData.child_id);

                              // 화각을 critical로 변경
                              // 선택상태로 ㅂ녀경
                              if(relationAsset.comInstance){
                                    relationAssets.push(relationAsset.comInstance);
                                    relationAsset.comInstance.changeCameraRangeSeverity(WV3DAssetComponent.STATE.CRITICAL);
                                    relationAsset.comInstance.selected = true;
                                    // 선택 아웃라인 비활성화 처리
                                    relationAsset.comInstance.toggleSelectedOutline(false);

                              }
                        }
                  }
            }
      }


      AssetAction.Event = {};
      AssetAction.Event.FOCUS="focus";
      AssetAction.Event.BLUR="blur";
      AssetAction.Event.SELECTED="selected";

      /*
      A. 출입센서 선택시  출입센서 상태가 critical인 경우에만
            1. 연관된 카메라를 critical로 변경
            2.  연관된 카메라.selected = true
                - 마우스 이벤트 영향을 받지 않기 위해


      B. 비선택 시
            1.

       */

      class AccessAction extends AssetAction {
            constructor(controller) {
                  super(controller);

                  this.relationAssets = [];

            }

            async action(comInstance) {

                  let assetId = "";
                  if (comInstance instanceof WV3DAssetComponent) {
                        if (!comInstance.assetId) {
                              console.log("컴포넌트에 자산 정보가 설정되어 있지 않습니다. ")
                              return;
                        }

                        assetId = comInstance.assetId;
                  }


                  // 연관 CCTV 목록 구하기
                  let [error, response] = await window.wemb.dcimManager.getRelationData(assetId);
                  if (error) {
                        CPLogger.log("출입센서 연관자산 데이터 로드 에러", error);
                        return;
                  }


                  // 연관 CCTV PLAy
                  let cctvAssetInfoList = response.data.data;
                  if (cctvAssetInfoList.length <= 0) {
                        return;
                  }

                  this.playRelationCCTVList(cctvAssetInfoList);

                  // critical인 경우만 처리
                  if (comInstance.isCriticalState() == true) {
                        this.activeRelationCCTVList(cctvAssetInfoList, this.relationAssets);
                  }

            }


            clear() {
                  this.relationAssets.forEach((comInstance)=>{
                        comInstance.selected = false;
                  })

                  this.relationAssets= [];

                  if(wemb.posodManager !== undefined)
                        wemb.posodManager.closeQENXViewer();

                  super.clear();
            }
      }




      /*
      A. 출입센서 선택시  출입센서 상태가 critical인 경우에만
            1. 연관된 카메라를 critical로 변경
            2.  연관된 카메라.selected = true
                - 마우스 이벤트 영향을 받지 않기 위해
      B. 비선택 시
            1.
       */
      class FireAction extends AssetAction {
            constructor(controller) {
                  super(controller);

                  this.relationAssets = [];

            }

            async action(comInstance) {

                  let assetId ="";
                  if (comInstance instanceof WV3DAssetComponent) {
                        if (!comInstance.assetId) {
                              console.log("컴포넌트에 자산 정보가 설정되어 있지 않습니다. ")
                              return;
                        }

                        assetId = comInstance.assetId;
                  }


                  // 연관 CCTV 목록 구하기
                  let [error, response] = await window.wemb.dcimManager.getRelationData(assetId);
                  if (error) {
                        CPLogger.log("출입센서 연관자산 데이터 로드 에러", error);
                        return;
                  }

                  // 연관 CCTV PLAy
                  let cctvAssetInfoList = response.data.data;
                  if (cctvAssetInfoList.length <= 0) {
                        return;
                  }

                  this.playRelationCCTVList(cctvAssetInfoList);

                  // critical인 경우만 처리
                  if (comInstance.isCriticalState() == true) {
                        this.activeRelationCCTVList(cctvAssetInfoList, this.relationAssets);
                  }
            }


            clear() {
                  this.relationAssets.forEach((comInstance)=>{
                        comInstance.selected = false;
                  })

                  this.relationAssets= [];

                  if(wemb.posodManager !== undefined)
                        wemb.posodManager.closeQENXViewer();

                  super.clear();
            }
      }






      class CCTVAction extends AssetAction {

            constructor(controller) {
                  super(controller);

            }

            action(comInstance) {
                  try {
                        let assetInfo = comInstance.data;
                        /**
                         * //실시간 영상
                         *{
                      url:"rtsp://admin:!1q2w3e4r@192.168.1.101:554/onvif/profile1/media.smp",
                      options:" --top_most --size=480,320 --pos=100,85 --title=test"
                 }**/
                        let tempUrl = assetInfo.rtsp_url.replace("rtsp://", `rtsp://${assetInfo.rtsp_id}:${assetInfo.rtsp_pass}@`);
                        let params = {

                              url: tempUrl,
                              options: assetInfo.rtsp_options
                        }

                        if(wemb.posodManager !== undefined)
                              wemb.posodManager.openQENXLivePopViewer(params);


                  } catch (error) {

                        //Vue.$message("자산 정보에 RTSP 정보가 존재하지 않아 플레이어를 실행할 수 없습니다.")
                        Vue.$message("The player can not run because there is no RTSP information in the asset information.");

                  }

            }


            clear() {
                  if(wemb.posodManager !== undefined)
                        wemb.posodManager.closeQENXViewer();
                  super.clear();
            }
      }


      class PDUAction extends AssetAction {


            constructor(controller) {
                  super(controller);
                  this._onClickPduChildHandler = this.onClickPduChlidItem.bind(this);
                  this._activeComInstance = null;
                  this._activeItem = null;
            }

            clear() {
                  // 선 제거
                  this.clearConnectLine();
                  // 선택한 포트 비활성화
                  this.blurChildAsset();
                  // 닫기
                  if (this._activeComInstance) {
                        this._activeComInstance.transitionOut();
                        this._activeComInstance.offWScriptEvent("itemClick", this._onClickPduChildHandler);
                        this._activeComInstance = null;
                  }

                  super.clear();
            }

            action(comInstance) {

                  let assetInfo = comInstance.data;
                  this._activeComInstance = comInstance;
                  if (comInstance.selected) {
                        this.clearConnectLine();
                        comInstance.transitionOut();
                        comInstance.offWScriptEvent("itemClick", this._onClickPduChildHandler);
                  } else {
                        comInstance.transitionIn();
                        comInstance.onWScriptEvent("itemClick", this._onClickPduChildHandler);
                  }


            }

            /*
          분전반에서 포트가 선택되는 경우 실행.
              단계01. 선택 포트 활성화
              단계02. 선택 포트 = 선택포트와 연결된 자산 선 그리기
           */
            onClickPduChlidItem(event) {
                  let selectedItem = event.data.selectItem;
                  // 단계01. 선택 포트 활성화 + 팝업창의 포트 활성화 처리
                  this.focusChildAsset(selectedItem);

                  // 단계02. 선택 포트 = 선택포트와 연결된 자산 선 그리기
                  let parentComp = window.wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(selectedItem.data.child_id)
                  if (parentComp) {
                        // 내부에서 기존에 그려진 선 지우고 다시 그리는 구문이 존재.
                        this.connectAssetWithLine(this._activeItem, parentComp.comInstance);
                  }
            }

            /**
             * 분전반 라인연결 메서드
             * @param fromComponent 연결되는 하는 포트
             * @param toComponent   연결되는 자산컴포넌트
             * */
            connectAssetWithLine(fromComponent, toComponent) {
                  this.clearConnectLine();
                  let hasLeft = fromComponent.name.indexOf("LEFT") > -1 ? true : false;
                  let appendPortSize = fromComponent.size.x / 2;
                  if (hasLeft) appendPortSize *= -1;
                  let from = fromComponent.getWorldPosition();
                  let to = toComponent.appendElement.getWorldPosition(new THREE.Vector3).clone();
                  from.x += appendPortSize;
                  from.z += fromComponent.size.z / 2;

                  // 포트의 parent참조 pdu패널
                  let pduPanel = fromComponent.parent;
                  let parentHeight = pduPanel.position.y + (pduPanel.size.y + 10);

                  let material = new THREE.LineBasicMaterial({ color: 0x00ff00 });
                  let geo = new THREE.Geometry();
                  geo.vertices.push(
                        from.clone(),
                        new THREE.Vector3(from.x, parentHeight, from.z),
                        new THREE.Vector3(to.x, parentHeight, to.z),
                        to.clone()
                  );
                  let connectLine = new THREE.Line(geo, material);
                  connectLine.name = AssetComponentControllerPlugin.CONNECT_LINE;
                  window.wemb.scene.add(connectLine);
            }

            clearConnectLine() {
                  let connectLine = window.wemb.scene.getObjectByName(AssetComponentControllerPlugin.CONNECT_LINE);
                  if (connectLine) {
                        connectLine.geometry.dispose();
                        connectLine.material.dispose();
                        window.wemb.scene.remove(connectLine);
                  }
            }



            /*
            2019.02.21(ckkim)
            call : PDU Popup 페이지에서 호출, PDU 유닛에 선택될때, data = 유닛 정보
             */
            drawConnectLine(data){
                  // 선택 처리
                  var itemMesh=this._activeComInstance.getChildByAssetName(data.direction+"_"+data.index);
                  var compositionAssetInfo = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(data.child_id);

                  if(compositionAssetInfo && itemMesh){
                        this.connectAssetWithLine(itemMesh, compositionAssetInfo.comInstance);
                        this.focusChildAsset(itemMesh);
                  }
            }
            /*
          분전반에서 포트가 클릭되는 경우
          단계01. 기존 포트 비활성화
          단계02. 신규 포트 활성화
          단계03. 팝업 포트 활성화

           */
            focusChildAsset(selectItem) {

                  if (this._activeItem) {
                        this._activeItem.selected=false;
                  }


                  selectItem.selected = true;
                  this._activeItem = selectItem;

                  try {
                        //window.focusPDUMountItem(selectItem.name);
                        this.eventBus.$emit(AssetAction.Event.FOCUS, selectItem.name);


                  } catch (error) {
                        console.warn("The mounted port can not be activated because the focusPDUMountItem () method does not exist in the PDU detail popup.", error);
                  }
            }

            blurChildAsset() {
                  if (this._activeItem) {
                        this._activeItem.selected=false;

                        this._activeItem = null;

                        try {
                              //window.blurPDUMountItem();
                              this.eventBus.$emit(AssetAction.Event.BLUR);
                        } catch (error) {
                              console.warn("The mounted port can not be disabled because the blurPDUMountItem () method does not exist in the PDU detail popup.", error);
                        }
                  }
            }


      }


      class RackAction extends AssetAction {


            constructor(controller) {
                  super(controller);
                  this._onClickRackChildHandler = this.onClickRackChildItem.bind(this);
                  this._activeComInstance = null;
                  this._activeItem = null;
            }

            clear() {

                  // 활성화 된 아이템을 비활성화
                  this.blurChildAsset();
                  if (this._activeComInstance) {
                        this._activeComInstance.transitionOut();
                        this._activeComInstance.offWScriptEvent("itemClick", this._onClickRackChildHandler);
                        this._activeComInstance = null;

                  }

                  super.clear();
            }


            action(comInstance) {
                  this._activeComInstance = comInstance;
                  if (comInstance.selected) {
                        comInstance.transitionOut();
                        comInstance.offWScriptEvent("itemClick", this._onClickRackChildHandler);
                  } else {
                        comInstance.transitionIn();
                        comInstance.onWScriptEvent("itemClick", this._onClickRackChildHandler);
                  }
            }

            onClickRackChildItem(event) {
                  this.toggleChildAsset(event.data.selectItem);
            }

            toggleChildAsset(selectItem) {
                  let selected = !selectItem.selected;
                  if (selected) {
                        this.focusChildAsset(selectItem);
                  } else {
                        this.blurChildAsset();

                  }
            }

            /*
            2019.02.21(ckkim)
            -  call : Rack Popup > loaded 이벤트 리스너에서 랙 유닛이 선택되는 경우 호출
             */
            focusChildAssetByAssetId(assetId){
                  let itemMesh = this._activeComInstance.getChildByAssetID(assetId);
                  if(itemMesh){
                        this.focusChildAsset(itemMesh);
                  }else{
                        console.warn(assetId+"에 해당하는 자산Mesh가 존재하지 않습니다.");
                  }
            }

            focusChildAsset(selectItem) {
                  if (this._activeItem) {
                        this._activeItem.selected = false;
                        this._activeItem.transitionOut();
                  }
                  selectItem.transitionIn();
                  this._activeItem = selectItem;
                  this._activeItem.selected = true;
                  try {
                        this.eventBus.$emit(AssetAction.Event.FOCUS, selectItem.data.id);
                        //window.focusRackMountItem(selectItem.data.id);
                  } catch (error) {
                        console.warn("mounted racks can not be activated because the focusRackMountItem () method does not exist in the Rack Detail popup.", error);
                  }
            }

            blurChildAsset() {
                  if (this._activeItem) {
                        this._activeItem.transitionOut();
                        this._activeItem.selected=false;
                        this._activeItem = null;

                        try {
                              //window.blurRackMountItem();
                              this.eventBus.$emit(AssetAction.Event.BLUR);
                        } catch (error) {
                              console.warn("mounted racks can not be deactivated because the focusRackMountItem () method does not exist in the Rack Detail popup.", error);
                        }
                  }
            }
      }


      class FireEffectAction extends AssetAction {
            constructor(controller) {
                  super(controller);
                  this._fireEffectMap = new Map();
            }

            clear() {
                  this._fireEffectMap.forEach((fireEffect) => {
                        // 페이지가 닫히는 경우
                        if (fireEffect) {
                              fireEffect.destroy();
                              fireEffect = null;
                        }
                  })

                  this._fireEffectMap.clear();


                  super.clear();
            }



            removeFireEffectByBuildingName(buildingNames) {
                  for (let i = 0; i < buildingNames.length; i++) {
                        let buildingName = buildingNames[i];
                        if (this._fireEffectMap.has(buildingName) == false) {
                              return;
                        }

                        let effect = this._fireEffectMap.get(buildingName);
                        effect.destroy();
                        effect = null;
                        this._fireEffectMap.delete(buildingName);
                  }
            }


            /*
          불효과 활성화
          @buildingName : 대상 자산이 위치하고 있는 빌딩이름
           */
            addFireEffect(buildingName, comInstance ) {
                  // 활성화 된 불 유무 확인
                  if (this._fireEffectMap.has(buildingName) == true) {
                        return;
                  }


                  let fireEffect = new FireEffect();
                  fireEffect.draw(comInstance);
                  fireEffect.animate();
                  this._fireEffectMap.set(buildingName, fireEffect);
            }


            // 싱크 처리하기
            // 신규 이름과  기존 이름을 비교해 신규 이름에 없는 효과를 제거한다.
            syncFireEffect(buildingNameMap){

                  let removeNames = [];
                  this._fireEffectMap.forEach((value, key)=>{
                        // 신규 이름에 기존 목록에 존재하지 않는 경우 제외 대상
                        if(buildingNameMap.has(key)==false){
                              removeNames.push(key);
                        }
                  })

                  this.removeFireEffectByBuildingName(removeNames);
            }
      }

      window.ActionManager = ActionManager;
})();
