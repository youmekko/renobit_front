/*
주요 기능
	- 자산 컴포넌트가 추가, 삭제 될때
	  자산 컴포넌트와 자산 정보와 1:1 대응 처리 :
	      AssetAllicationProxy

      - 페이지 저장 시 중복 배치 처리 판단하기
사용:
	Editor에서만 사용.
 */
class AssetPagePlugin extends ExtensionPluginCore {
      constructor() {
            super();
            this._allocationProxy = null;
            this._assetComponentProxy = null;
            this._lang = Vue.$i18n.messages.wv.dcim_pack;
      }

      async setFacade(facade) {
            super.setFacade(facade);
      }


      start(){
            this._allocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);
            this._assetComponentProxy = this._facade.retrieveProxy(AssetComponentProxy.NAME);
      }

      get notificationList() {
            return [
                  EditorProxy.NOTI_UPDATE_EDITOR_STATE,
                  // EditorProxy.NOTI_SAVED_PAGE,
                  EditorProxy.NOTI_ADD_COM_INSTANCE,
                  EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE
            ]
      }

      /*
	noti가 온 경우 실행
	call : mediator에서 실행
	 */
      handleNotification(note) {
            switch (note.name) {
                  case EditorProxy.NOTI_UPDATE_EDITOR_STATE :
                        this.noti_changePageLoadingState(note.getBody());
                        break;

                  case EditorProxy.NOTI_SAVED_PAGE:
                        this.noti_savedPage();
                        break;


                  case EditorProxy.NOTI_ADD_COM_INSTANCE :
                        this.noti_addAssetAllocationComInstance(note.getBody());
                        break;
                  case EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE :
                        this.noti_removeAssetAllocationComInstance(note.getBody());
                        break;
            }

      }


      initProperties() {

      }


      destroy() {
      }



      noti_changePageLoadingState(pageLoadingState) {
            // 모든 정보가 로드된 상태.
            if (pageLoadingState == "readyCompleted") {
                  /*
			2018.11.26(ckkim)
			중요!!!
			- 저장 훅 이벤트 등록하기
				- 페이지 저장 전 처리 내용.

			 */
                  wemb.hookManager.addAction(HookManager.HOOK_BEFORE_SAVE_PAGE, this._hookBeforeSavePage.bind(this));

            }
      }



      /*
	중복 배치 유무 판단하기

	단계01: 저장 전 중복 배치 유무 판단하기
		단계01-01: 자산 정보를 최신 정보로 업데이트 하기
		단계01-02: 자산 중복 배치 검색
	 */
      async _hookBeforeSavePage(){

            //단계01: 저장 전 중복 배치 유무 판단하기
            try {
                  //단계01-01: 자산 정보를 최신 정보로 업데이트 하기
                  await this._assetComponentProxy.loadAssetsData();

            }catch(error){
                  console.warn("@@ action Error getting asset information. AssetPagePlugin.noti_savedPage", error);
                  return false;
            }



            //단계01-02: 자산 중복 배치 검색
            /*
            let result = {
                  pageList:[{
                         page_id: pageId,
                          page_name: pageName,
                          list: [{assetItem
                          },....]
                  }],
                  assetList:[{assetItem}....]
              };
             */
            let duplicationResult = this._assetComponentProxy.checkDuplicatedAsset(wemb.pageManager.currentPageInfo.id, this._allocationProxy.assetCompositionInfoList);
            // 중복이 존재하지 않는 경우 즉시 저장
            if(duplicationResult.assetList.length<=0)
                  return true;
            /////////////






            /////////////
            // 중복 자산이 존재하는 경우
            // 중복 정보 테이블 만들기.
            let isSave = await this._save_showDuplicateMessageBox(duplicationResult.pageList);

            if(isSave){
                  this._removeAssetInfo(duplicationResult.assetList);

            }else {
                  this._setDoubleAssetState(duplicationResult.assetList);
            }


            return isSave;
      }

      /*
      자산 중복 유무 체크 및 상태 적용
      call : DCIMManager.refreshAssetAllData();
       */
      executeCheckingDuplicatedAsset(){
            let duplicationResult = this._assetComponentProxy.checkDuplicatedAsset(wemb.pageManager.currentPageInfo.id, this._allocationProxy.assetCompositionInfoList);
            // 중복이 존재하지 않는 경우 즉시 저장
            if(duplicationResult.assetList.length<=0)
                  return false;
            /////////////

            this._sync_showDuplicateMessageBox(duplicationResult.pageList)

            // 상태를 중복으로 변경.
            this._setDoubleAssetState(duplicationResult.assetList);

            return true;

      }

      /*
      컴포넌트에서 자산 정보 제거하기(기본 사태로 변경
       */
      _removeAssetInfo(assetList){
            // 중복 자산 컴포넌트에서 자산 정보를 제거한다.
            // 취소를 선택하는 경우
            let updateSW=false;
            assetList.forEach((assetItem)=>{
                  updateSW=this._allocationProxy.removeAssetAllocationByAssetId(assetItem.id,false, false);

            })

            if(updateSW){
                  this._allocationProxy.sendUpdateAssetAllocationList();
            }
      }
      /*
     자산 상태를 중복 상태로 변경
       */
      _setDoubleAssetState(assetList){
            // 취소를 선택하는 경우
            assetList.forEach((assetItem)=>{
                  assetItem.valid_state = "double";
            })

            this._allocationProxy.sendUpdateAssetAllocationList();
      }



      /*
      중복 정보를 테이블로 만들기
       */
      _createDuplicateTable(pageList){
            let table = `<div style='overflow:auto; max-height:320px; border:1px solid #ebebeb;'>
                              <table style='border:1px solid #ebebeb; border-collapse:collapse; width:100%; text-align: left;'>
                                    <tr>
                                          <td style='width:30%; padding:10px; background-color:#F5F7FA; color:#94979D; font-weight:bold; border: 1px solid #ebebeb;'>` + this._lang.common.pageName + `</td>
                                          <td style='width:70%; padding:10px; background-color:#F5F7FA; color:#94979D; font-weight:bold; border: 1px solid #ebebeb;'>`+ this._lang.common.asset + `</td>
                                    </tr>`;


            //페이지 리스트
            for(var i=0;i<pageList.length;i++){

                  //페이지 정보
                  let pageInfo = pageList[i];

                  for(var q=0; q<pageInfo.list.length; q++){

                        //자산 정보
                        let assetInfo = pageInfo.list[q];

                        if(q===0){
                              table+=`<tr>
                                          <td rowspan='${pageInfo.list.length}' style='padding:10px; color:#808286; border: 1px solid #ebebeb;'>${pageInfo.page_name}</td>
                                          <td style='padding:10px; color:#808286; border: 1px solid #ebebeb;'>${assetInfo.name}</td>
                                      </tr>`;
                        }else{
                              table+=`<tr>
                                        <td style='padding:10px; color:#808286; border: 1px solid #ebebeb;'>${assetInfo.name}</td>
                                      </tr>`;
                        }
                  }
            }

            table+=`</table>`;

            return table;
      }

      /*
	pageList 정보를 바탕으로 중복 위치의 페이지 + 자산 정보 테이블 만들기.
	 */
      async _save_showDuplicateMessageBox(pageList){

            let table = this._createDuplicateTable(pageList);

            try {

                  await Vue.$confirm(`
                        <p style="margin-bottom:20px;">`
                        + this._lang.assetPagePlugin.duplicatedAssetMsg02 + `<br>`
                        + this._lang.assetPagePlugin.alertDeleteAssetInfo + `<br>`
                        + this._lang.assetPagePlugin.duplicatedAssetMsg03
                        + `</p>${table}`, this._lang.assetPagePlugin.duplicatedAssetMsg01, {
                        dangerouslyUseHTMLString: true,
                        showClose: false,
                        center: true,
                        confirmButtonText: 'Save',
                        cancelButtonText: 'Cancel',
                  });

                  return true;
            }catch(error){
                  return false;
            }

      }





      /*
	pageList 정보를 바탕으로 중복 위치의 페이지 + 자산 정보 테이블 만들기.
	 */
      _sync_showDuplicateMessageBox(pageList){
            let table = this._createDuplicateTable(pageList);

            try {
                  Vue.$alert(`
                        <p style="margin-bottom:20px;">`
                        + this._lang.assetPagePlugin.duplicatedAssetMsg02 +
                       `</p>${table}`, this._lang.assetPagePlugin.duplicatedAssetMsg01, {
                        dangerouslyUseHTMLString: true,
                        showClose: false,
                        center: true,
                  });


            }catch(error){

            }

      }

      /*

      noti_savedPage()가 실행되기 전
            - 자산 정보는 다시 불려진 상태임.


	단계01: 저장 전 중복 배치 유무 판단하기
		단계01-01: 자산 정보를 최신 정보로 업데이트 하기
		단계01-02: 자산 중복 배치 검색
			- 페이지 별로 중복 유무 리스트 출력하기
			   중복이 되는 경우
				경우1: 중복되는 컴포넌트의 자산 id 정보를 빈 상태로 만든 후 저장
					즉, mapping을 풀어서 저장하기
					단계02: 실행
				경우2: 중복되는 배치  정보 확인 하기
					assetOutline패널에 중복 배치 상태를 적용하기

	단계02: 자산 배치 정보 저장하기

	단계03: 자산 배치 정보 업데이트 정보 날리기 (NOTI_UPDATE_EXTENSION_DATA);

	 */
      async noti_savedPage() {


            // 1. 자산 배치 정보 저장하기
            try {

                  var success = await this._allocationProxy.saveAssetInfo();
                  if (success) {


                        // 싱크 맞추기 (싱크 맞추기 전 열린  페이지의  자산 정보가 삭제됨)
                        this._allocationProxy.syncAssetCompositionInfoList();

                        // 2. 자산 배치 정보 업데이트 정보 날리기 (NOTI_UPDATE_EXTENSION_DATA);
                        // DCIM 관리자가 활성화 되어 있는경우 실행됨.
                        this._facade.sendNotification(EditorProxy.NOTI_UPDATE_EXTENSION_DATA, "DCIM");
                  }

            } catch (error) {
                  console.log("error ", error);
            }
      }



      /*
      call : 자산 컴포넌트가 drop되는 순간
       - comInstance.data에는 이미 자산 정보가 맵핑된 상태임.
       - config

       */
      noti_addAssetAllocationComInstance(comInstance){
            var assetAllocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);
            /*
                  comInstance에는 자산 정보가 설정된 상태임.
                  자산 정보에 => 컴포넌트 정보를 설정해야하.ㅁ


                        단계01: 맵핑할 자산 정보 구하기

                        단계02: 자산정보에 컴포넌트 정보 설정하기

                        단계03: 맵핑 정보를 여러 자산 Map에 적용하기

                        단계04: 추가된 자산 저보를 동적으로 설정하기

                        단계05: 업데이트  사실을 다른 패널에 알리기

             */
            assetAllocationProxy.addAssetAssetAllocation(comInstance);
      }

      noti_removeAssetAllocationComInstance(comInstance){
            // 컴포넌트 인스턴스가 삭제되는 경우
            var assetAllocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);
            assetAllocationProxy.removeAssetAssetAllocation(comInstance, true, true);
      }


}
