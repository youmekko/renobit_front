"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/*
주요 기능
	- 자산 컴포넌트가 추가, 삭제 될때
	  자산 컴포넌트와 자산 정보와 1:1 대응 처리 :
	      AssetAllicationProxy

      - 페이지 저장 시 중복 배치 처리 판단하기
사용:
	Editor에서만 사용.
 */
var AssetPagePlugin =
      /*#__PURE__*/
      function (_ExtensionPluginCore) {
            _inherits(AssetPagePlugin, _ExtensionPluginCore);

            function AssetPagePlugin() {
                  var _this;

                  _classCallCheck(this, AssetPagePlugin);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(AssetPagePlugin).call(this));
                  _this._allocationProxy = null;
                  _this._assetComponentProxy = null;
                  _this._lang = Vue.$i18n.messages.wv.dcim_pack;
                  return _this;
            }

            _createClass(AssetPagePlugin, [{
                  key: "setFacade",
                  value: function () {
                        var _setFacade = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee(facade) {
                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                          while (1) {
                                                switch (_context.prev = _context.next) {
                                                      case 0:
                                                            _get(_getPrototypeOf(AssetPagePlugin.prototype), "setFacade", this).call(this, facade);

                                                      case 1:
                                                      case "end":
                                                            return _context.stop();
                                                }
                                          }
                                    }, _callee, this);
                              }));

                        function setFacade(_x) {
                              return _setFacade.apply(this, arguments);
                        }

                        return setFacade;
                  }()
            }, {
                  key: "start",
                  value: function start() {
                        this._allocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);
                        this._assetComponentProxy = this._facade.retrieveProxy(AssetComponentProxy.NAME);
                  }
            }, {
                  key: "handleNotification",

                  /*
				  noti가 온 경우 실행
				  call : mediator에서 실행
				  */
                  value: function handleNotification(note) {
                        switch (note.name) {
                              case EditorProxy.NOTI_UPDATE_EDITOR_STATE:
                                    this.noti_changePageLoadingState(note.getBody());
                                    break;

                              case EditorProxy.NOTI_SAVED_PAGE:
                                    this.noti_savedPage();
                                    break;

                              case EditorProxy.NOTI_ADD_COM_INSTANCE:
                                    this.noti_addAssetAllocationComInstance(note.getBody());
                                    break;

                              case EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE:
                                    this.noti_removeAssetAllocationComInstance(note.getBody());
                                    break;
                        }
                  }
            }, {
                  key: "initProperties",
                  value: function initProperties() {}
            }, {
                  key: "destroy",
                  value: function destroy() {}
            }, {
                  key: "noti_changePageLoadingState",
                  value: function noti_changePageLoadingState(pageLoadingState) {
                        // 모든 정보가 로드된 상태.
                        if (pageLoadingState == "readyCompleted") {
                              /*
							  2018.11.26(ckkim)
							  중요!!!
							  - 저장 훅 이벤트 등록하기
							  - 페이지 저장 전 처리 내용.
							  */
                              wemb.hookManager.addAction(HookManager.HOOK_BEFORE_SAVE_PAGE, this._hookBeforeSavePage.bind(this));
                        }
                  }
                  /*
				  중복 배치 유무 판단하기
				  단계01: 저장 전 중복 배치 유무 판단하기
				  단계01-01: 자산 정보를 최신 정보로 업데이트 하기
				  단계01-02: 자산 중복 배치 검색
				  */

            }, {
                  key: "_hookBeforeSavePage",
                  value: function () {
                        var _hookBeforeSavePage2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2() {
                                    var duplicationResult, isSave;
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            _context2.prev = 0;
                                                            _context2.next = 3;
                                                            return this._assetComponentProxy.loadAssetsData();

                                                      case 3:
                                                            _context2.next = 9;
                                                            break;

                                                      case 5:
                                                            _context2.prev = 5;
                                                            _context2.t0 = _context2["catch"](0);
                                                            console.warn("@@ action Error getting asset information. AssetPagePlugin.noti_savedPage", _context2.t0);
                                                            return _context2.abrupt("return", false);

                                                      case 9:
                                                            //단계01-02: 자산 중복 배치 검색

                                                            /*
															let result = {
																  pageList:[{
																		 page_id: pageId,
																		  page_name: pageName,
																		  list: [{assetItem
																		  },....]
																  }],
																  assetList:[{assetItem}....]
															  };
															 */
                                                            duplicationResult = this._assetComponentProxy.checkDuplicatedAsset(wemb.pageManager.currentPageInfo.id, this._allocationProxy.assetCompositionInfoList); // 중복이 존재하지 않는 경우 즉시 저장

                                                            if (!(duplicationResult.assetList.length <= 0)) {
                                                                  _context2.next = 12;
                                                                  break;
                                                            }

                                                            return _context2.abrupt("return", true);

                                                      case 12:
                                                            _context2.next = 14;
                                                            return this._save_showDuplicateMessageBox(duplicationResult.pageList);

                                                      case 14:
                                                            isSave = _context2.sent;

                                                            if (isSave) {
                                                                  this._removeAssetInfo(duplicationResult.assetList);
                                                            } else {
                                                                  this._setDoubleAssetState(duplicationResult.assetList);
                                                            }

                                                            return _context2.abrupt("return", isSave);

                                                      case 17:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2, this, [[0, 5]]);
                              }));

                        function _hookBeforeSavePage() {
                              return _hookBeforeSavePage2.apply(this, arguments);
                        }

                        return _hookBeforeSavePage;
                  }()
                  /*
				  자산 중복 유무 체크 및 상태 적용
				  call : DCIMManager.refreshAssetAllData();
				   */

            }, {
                  key: "executeCheckingDuplicatedAsset",
                  value: function executeCheckingDuplicatedAsset() {
                        var duplicationResult = this._assetComponentProxy.checkDuplicatedAsset(wemb.pageManager.currentPageInfo.id, this._allocationProxy.assetCompositionInfoList); // 중복이 존재하지 않는 경우 즉시 저장


                        if (duplicationResult.assetList.length <= 0) return false; /////////////

                        this._sync_showDuplicateMessageBox(duplicationResult.pageList); // 상태를 중복으로 변경.


                        this._setDoubleAssetState(duplicationResult.assetList);

                        return true;
                  }
                  /*
				  컴포넌트에서 자산 정보 제거하기(기본 사태로 변경
				   */

            }, {
                  key: "_removeAssetInfo",
                  value: function _removeAssetInfo(assetList) {
                        var _this2 = this;

                        // 중복 자산 컴포넌트에서 자산 정보를 제거한다.
                        // 취소를 선택하는 경우
                        var updateSW = false;
                        assetList.forEach(function (assetItem) {
                              updateSW = _this2._allocationProxy.removeAssetAllocationByAssetId(assetItem.id, false, false);
                        });

                        if (updateSW) {
                              this._allocationProxy.sendUpdateAssetAllocationList();
                        }
                  }
                  /*
				  자산 상태를 중복 상태로 변경
				   */

            }, {
                  key: "_setDoubleAssetState",
                  value: function _setDoubleAssetState(assetList) {
                        // 취소를 선택하는 경우
                        assetList.forEach(function (assetItem) {
                              assetItem.valid_state = "double";
                        });

                        this._allocationProxy.sendUpdateAssetAllocationList();
                  }
                  /*
				  중복 정보를 테이블로 만들기
				   */

            }, {
                  key: "_createDuplicateTable",
                  value: function _createDuplicateTable(pageList) {
                        var table = "<div style='overflow:auto; max-height:320px; border:1px solid #ebebeb;'>\n                              <table style='border:1px solid #ebebeb; border-collapse:collapse; width:100%; text-align: left;'>\n                                    <tr>\n                                          <td style='width:30%; padding:10px; background-color:#F5F7FA; color:#94979D; font-weight:bold; border: 1px solid #ebebeb;'>" + this._lang.common.pageName + "</td>\n                                          <td style='width:70%; padding:10px; background-color:#F5F7FA; color:#94979D; font-weight:bold; border: 1px solid #ebebeb;'>" + this._lang.common.asset + "</td>\n                                    </tr>"; //페이지 리스트

                        for (var i = 0; i < pageList.length; i++) {
                              //페이지 정보
                              var pageInfo = pageList[i];

                              for (var q = 0; q < pageInfo.list.length; q++) {
                                    //자산 정보
                                    var assetInfo = pageInfo.list[q];

                                    if (q === 0) {
                                          table += "<tr>\n                                          <td rowspan='".concat(pageInfo.list.length, "' style='padding:10px; color:#808286; border: 1px solid #ebebeb;'>").concat(pageInfo.page_name, "</td>\n                                          <td style='padding:10px; color:#808286; border: 1px solid #ebebeb;'>").concat(assetInfo.name, "</td>\n                                      </tr>");
                                    } else {
                                          table += "<tr>\n                                        <td style='padding:10px; color:#808286; border: 1px solid #ebebeb;'>".concat(assetInfo.name, "</td>\n                                      </tr>");
                                    }
                              }
                        }

                        table += "</table>";
                        return table;
                  }
                  /*
				  pageList 정보를 바탕으로 중복 위치의 페이지 + 자산 정보 테이블 만들기.
				  */

            }, {
                  key: "_save_showDuplicateMessageBox",
                  value: function () {
                        var _save_showDuplicateMessageBox2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee3(pageList) {
                                    var table;
                                    return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                          while (1) {
                                                switch (_context3.prev = _context3.next) {
                                                      case 0:
                                                            table = this._createDuplicateTable(pageList);
                                                            _context3.prev = 1;
                                                            _context3.next = 4;
                                                            return Vue.$confirm("\n                        <p style=\"margin-bottom:20px;\">" + this._lang.assetPagePlugin.duplicatedAssetMsg02 + "<br>" + this._lang.assetPagePlugin.alertDeleteAssetInfo + "<br>" + this._lang.assetPagePlugin.duplicatedAssetMsg03 + "</p>".concat(table), this._lang.assetPagePlugin.duplicatedAssetMsg01, {
                                                                  dangerouslyUseHTMLString: true,
                                                                  showClose: false,
                                                                  center: true,
                                                                  confirmButtonText: 'Save',
                                                                  cancelButtonText: 'Cancel'
                                                            });

                                                      case 4:
                                                            return _context3.abrupt("return", true);

                                                      case 7:
                                                            _context3.prev = 7;
                                                            _context3.t0 = _context3["catch"](1);
                                                            return _context3.abrupt("return", false);

                                                      case 10:
                                                      case "end":
                                                            return _context3.stop();
                                                }
                                          }
                                    }, _callee3, this, [[1, 7]]);
                              }));

                        function _save_showDuplicateMessageBox(_x2) {
                              return _save_showDuplicateMessageBox2.apply(this, arguments);
                        }

                        return _save_showDuplicateMessageBox;
                  }()
                  /*
				  pageList 정보를 바탕으로 중복 위치의 페이지 + 자산 정보 테이블 만들기.
				  */

            }, {
                  key: "_sync_showDuplicateMessageBox",
                  value: function _sync_showDuplicateMessageBox(pageList) {
                        var table = this._createDuplicateTable(pageList);

                        try {
                              Vue.$alert("\n                        <p style=\"margin-bottom:20px;\">" + this._lang.assetPagePlugin.duplicatedAssetMsg02 + "</p>".concat(table), this._lang.assetPagePlugin.duplicatedAssetMsg01, {
                                    dangerouslyUseHTMLString: true,
                                    showClose: false,
                                    center: true
                              });
                        } catch (error) {}
                  }
                  /*
				   noti_savedPage()가 실행되기 전
						- 자산 정보는 다시 불려진 상태임.
				  단계01: 저장 전 중복 배치 유무 판단하기
				  단계01-01: 자산 정보를 최신 정보로 업데이트 하기
				  단계01-02: 자산 중복 배치 검색
				  - 페이지 별로 중복 유무 리스트 출력하기
				  중복이 되는 경우
				  경우1: 중복되는 컴포넌트의 자산 id 정보를 빈 상태로 만든 후 저장
				  즉, mapping을 풀어서 저장하기
				  단계02: 실행
				  경우2: 중복되는 배치  정보 확인 하기
				  assetOutline패널에 중복 배치 상태를 적용하기
				  단계02: 자산 배치 정보 저장하기
				  단계03: 자산 배치 정보 업데이트 정보 날리기 (NOTI_UPDATE_EXTENSION_DATA);
				  */

            }, {
                  key: "noti_savedPage",
                  value: function () {
                        var _noti_savedPage = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee4() {
                                    var success;
                                    return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                          while (1) {
                                                switch (_context4.prev = _context4.next) {
                                                      case 0:
                                                            _context4.prev = 0;
                                                            _context4.next = 3;
                                                            return this._allocationProxy.saveAssetInfo();

                                                      case 3:
                                                            success = _context4.sent;

                                                            if (success) {
                                                                  // 싱크 맞추기 (싱크 맞추기 전 열린  페이지의  자산 정보가 삭제됨)
                                                                  this._allocationProxy.syncAssetCompositionInfoList(); // 2. 자산 배치 정보 업데이트 정보 날리기 (NOTI_UPDATE_EXTENSION_DATA);
                                                                  // DCIM 관리자가 활성화 되어 있는경우 실행됨.


                                                                  this._facade.sendNotification(EditorProxy.NOTI_UPDATE_EXTENSION_DATA, "DCIM");
                                                            }

                                                            _context4.next = 10;
                                                            break;

                                                      case 7:
                                                            _context4.prev = 7;
                                                            _context4.t0 = _context4["catch"](0);
                                                            console.log("error ", _context4.t0);

                                                      case 10:
                                                      case "end":
                                                            return _context4.stop();
                                                }
                                          }
                                    }, _callee4, this, [[0, 7]]);
                              }));

                        function noti_savedPage() {
                              return _noti_savedPage.apply(this, arguments);
                        }

                        return noti_savedPage;
                  }()
                  /*
				  call : 자산 컴포넌트가 drop되는 순간
				   - comInstance.data에는 이미 자산 정보가 맵핑된 상태임.
				   - config
					*/

            }, {
                  key: "noti_addAssetAllocationComInstance",
                  value: function noti_addAssetAllocationComInstance(comInstance) {
                        var assetAllocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);
                        /*
							  comInstance에는 자산 정보가 설정된 상태임.
							  자산 정보에 => 컴포넌트 정보를 설정해야하.ㅁ
									  단계01: 맵핑할 자산 정보 구하기
									 단계02: 자산정보에 컴포넌트 정보 설정하기
									 단계03: 맵핑 정보를 여러 자산 Map에 적용하기
									 단계04: 추가된 자산 저보를 동적으로 설정하기
									 단계05: 업데이트  사실을 다른 패널에 알리기
						  */


                        assetAllocationProxy.addAssetAssetAllocation(comInstance);
                  }
            }, {
                  key: "noti_removeAssetAllocationComInstance",
                  value: function noti_removeAssetAllocationComInstance(comInstance) {
                        // 컴포넌트 인스턴스가 삭제되는 경우
                        var assetAllocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);

                        assetAllocationProxy.removeAssetAssetAllocation(comInstance, true, true);
                  }
            }, {
                  key: "notificationList",
                  get: function get() {
                        return [EditorProxy.NOTI_UPDATE_EDITOR_STATE, EditorProxy.NOTI_ADD_COM_INSTANCE, EditorProxy.NOTI_BEOFRE_REMOVE_COM_INSTANCE];
                  }
            }]);

            return AssetPagePlugin;
      }(ExtensionPluginCore);
