"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DCIMEventManager =
      /*#__PURE__*/
      function () {
            function DCIMEventManager() {
                  _classCallCheck(this, DCIMEventManager);

                  this._duration = DCIMManager.EVENT_POLLING_INTERVAL_TIME;
                  this.timer = 0;
                  this.pageTimer = 0;
                  this.historyTimer = 0;
                  this._$bus = new Vue();
                  this._path = wemb.configManager.serverUrl + "/dcim.do";

                  if (!wemb.configManager.isEditorMode) {
                        this.startEventPolling();
                        this.procEventBrowserData(this.severity);
                  }
            }

            _createClass(DCIMEventManager, [{
                  key: "$on",
                  value: function $on(event, func) {
                        this._$bus.$on(event, func);
                  }
            }, {
                  key: "$off",
                  value: function $off(event, func) {
                        this._$bus.$off(event, func);
                  }
            }, {
                  key: "ackEventData",
                  value: function () {
                        var _ackEventData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee(item) {
                                    var param, res;
                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                          while (1) {
                                                switch (_context.prev = _context.next) {
                                                      case 0:
                                                            param = {
                                                                  "id": "assetEventService.updateEventData",
                                                                  "params": item
                                                            };
                                                            _context.next = 3;
                                                            return wemb.$http.post(this._path, param);

                                                      case 3:
                                                            res = _context.sent;

                                                            if (!res) {
                                                                  _context.next = 8;
                                                                  break;
                                                            }

                                                            return _context.abrupt("return", res.data);

                                                      case 8:
                                                            return _context.abrupt("return", null);

                                                      case 9:
                                                      case "end":
                                                            return _context.stop();
                                                }
                                          }
                                    }, _callee, this);
                              }));

                        function ackEventData(_x) {
                              return _ackEventData.apply(this, arguments);
                        }

                        return ackEventData;
                  }()
            }, {
                  key: "requestEventData",
                  value: function () {
                        var _requestEventData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2(pageId) {
                                    var param, res;
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            param = {
                                                                  "id": "assetEventService.getEventList",
                                                                  "params": {
                                                                        pageIds: [pageId]
                                                                  }
                                                            };
                                                            _context2.next = 3;
                                                            return wemb.$http.post(this._path, param);

                                                      case 3:
                                                            res = _context2.sent;

                                                            if (!res) {
                                                                  _context2.next = 8;
                                                                  break;
                                                            }

                                                            return _context2.abrupt("return", res.data);

                                                      case 8:
                                                            return _context2.abrupt("return", null);

                                                      case 9:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2, this);
                              }));

                        function requestEventData(_x2) {
                              return _requestEventData.apply(this, arguments);
                        }

                        return requestEventData;
                  }()
            }, {
                  key: "requestEventBrowserData",
                  value: function () {
                        var _requestEventBrowserData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee3() {
                                    var severityList,
                                          param,
                                          res,
                                          _args3 = arguments;
                                    return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                          while (1) {
                                                switch (_context3.prev = _context3.next) {
                                                      case 0:
                                                            severityList = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : null;
                                                            param = {
                                                                  "id": "assetEventService.getEventInitInfo",
                                                                  "params": {
                                                                        severity: severityList
                                                                  }
                                                            };
                                                            _context3.next = 4;
                                                            return wemb.$http.post(this._path, param);

                                                      case 4:
                                                            res = _context3.sent;

                                                            if (!res) {
                                                                  _context3.next = 9;
                                                                  break;
                                                            }

                                                            return _context3.abrupt("return", res.data);

                                                      case 9:
                                                            return _context3.abrupt("return", null);

                                                      case 10:
                                                      case "end":
                                                            return _context3.stop();
                                                }
                                          }
                                    }, _callee3, this);
                              }));

                        function requestEventBrowserData() {
                              return _requestEventBrowserData.apply(this, arguments);
                        }

                        return requestEventBrowserData;
                  }()
            }, {
                  key: "requestEventList",
                  value: function () {
                        var _requestEventList = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee4(pageIds, typeIds) {
                                    var param, res;
                                    return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                          while (1) {
                                                switch (_context4.prev = _context4.next) {
                                                      case 0:
                                                            param = {
                                                                  "id": "assetEventService.getEventInitInfo",
                                                                  "params": {
                                                                        pageIds: pageIds,
                                                                        typeIds: typeIds
                                                                  }
                                                            };
                                                            _context4.next = 3;
                                                            return wemb.$http.post(this._path, param);

                                                      case 3:
                                                            res = _context4.sent;

                                                            if (!res) {
                                                                  _context4.next = 8;
                                                                  break;
                                                            }

                                                            return _context4.abrupt("return", res.data);

                                                      case 8:
                                                            return _context4.abrupt("return", null);

                                                      case 9:
                                                      case "end":
                                                            return _context4.stop();
                                                }
                                          }
                                    }, _callee4, this);
                              }));

                        function requestEventList(_x3, _x4) {
                              return _requestEventList.apply(this, arguments);
                        }

                        return requestEventList;
                  }()
            }, {
                  key: "requestSeverityCount",
                  value: function () {
                        var _requestSeverityCount = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee5(pageIds, typeIds) {
                                    var param, res;
                                    return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                          while (1) {
                                                switch (_context5.prev = _context5.next) {
                                                      case 0:
                                                            param = {
                                                                  "id": "assetEventService.getEventCount",
                                                                  "params": {
                                                                        pageIds: pageIds,
                                                                        typeIds: typeIds
                                                                  }
                                                            };
                                                            _context5.next = 3;
                                                            return wemb.$http.post(this._path, param);

                                                      case 3:
                                                            res = _context5.sent;

                                                            if (!res) {
                                                                  _context5.next = 8;
                                                                  break;
                                                            }

                                                            return _context5.abrupt("return", res.data);

                                                      case 8:
                                                            return _context5.abrupt("return", null);

                                                      case 9:
                                                      case "end":
                                                            return _context5.stop();
                                                }
                                          }
                                    }, _callee5, this);
                              }));

                        function requestSeverityCount(_x5, _x6) {
                              return _requestSeverityCount.apply(this, arguments);
                        }

                        return requestSeverityCount;
                  }()
            }, {
                  key: "requestHistoryData",
                  value: function () {
                        var _requestHistoryData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee6(info) {
                                    var param, res;
                                    return regeneratorRuntime.wrap(function _callee6$(_context6) {
                                          while (1) {
                                                switch (_context6.prev = _context6.next) {
                                                      case 0:
                                                            param = {
                                                                  "id": "assetEventService.getHistoryData",
                                                                  "params": info
                                                            };
                                                            _context6.next = 3;
                                                            return wemb.$http.post(this._path, param);

                                                      case 3:
                                                            res = _context6.sent;

                                                            if (!res) {
                                                                  _context6.next = 8;
                                                                  break;
                                                            }

                                                            return _context6.abrupt("return", res.data);

                                                      case 8:
                                                            return _context6.abrupt("return", null);

                                                      case 9:
                                                      case "end":
                                                            return _context6.stop();
                                                }
                                          }
                                    }, _callee6, this);
                              }));

                        function requestHistoryData(_x7) {
                              return _requestHistoryData.apply(this, arguments);
                        }

                        return requestHistoryData;
                  }()
            }, {
                  key: "startEventPolling",
                  value: function startEventPolling() {
                        clearTimeout(this.timer);
                        var self = this;
                        this.timer = setTimeout(function () {
                              // api 호출
                              self.procEventBrowserData();
                        }, this._duration);
                  }
            }, {
                  key: "startPageEventPolling",
                  value: function startPageEventPolling() {
                        clearTimeout(this.pageTimer);
                        var self = this;
                        this.pageTimer = setTimeout(function () {
                              self.procPageEventData();
                        }, this._duration);
                  }
            }, {
                  key: "startPollingHistoryData",
                  value: function startPollingHistoryData() {
                        clearTimeout(this.historyTimer);
                        var self = this;
                        this.historyTimer = setTimeout(function () {
                              self.procHistoryData();
                        }, this._duration);
                  }
                  /*
                  2018.11.15(ckkim)
                  DCIMManager에서 페이지가 열릴때마다 호출.
                   */

            }, {
                  key: "clearEventList",
                  value: function clearEventList() {
                        clearTimeout(this.pageTimer);
                        this.eventList = [];
                        this.clearHistoryTimer();
                  }
            }, {
                  key: "clearHistoryTimer",
                  value: function clearHistoryTimer() {
                        clearTimeout(this.historyTimer);
                  }
            }, {
                  key: "clearAllTimer",
                  value: function clearAllTimer() {
                        clearTimeout(this.pageTimer);
                        clearTimeout(this.timer);
                        clearTimeout(this.historyTimer);
                  }
            }, {
                  key: "procAckEventData",
                  value: function procAckEventData(data) {
                        var _this = this;

                        try {
                              this.ackEventData(data).then(function () {
                                    _this.requestEventBrowserData(_this.severity).then(function (res) {
                                          if (res) {
                                                var before = JSON.stringify(_this.eventBrowserList);

                                                if (before != JSON.stringify(res.data)) {
                                                      _this.eventBrowserList = res.data;

                                                      _this._$bus.$emit(DCIMManager.GET_EVENT_LIST, _this.eventBrowserList);
                                                }
                                          }

                                          _this._$bus.$emit(DCIMManager.EVENT_COMPLETE_ACK, data);

                                          _this.startEventPolling();
                                    });

                                    _this.procPageEventData();
                              });
                        } catch (err) {
                              throw err;
                        }
                  }
            }, {
                  key: "procEventBrowserData",
                  value: function procEventBrowserData(activeList) {
                        var _this2 = this;

                        try {
                              this.severity = activeList || this.severity;
                              this.requestEventBrowserData(this.severity).then(function (res) {
                                    if (res) {
                                          var before = JSON.stringify(_this2.eventBrowserList);

                                          if (before != JSON.stringify(res.data)) {
                                                _this2.eventBrowserList = res.data;

                                                _this2._$bus.$emit(DCIMManager.GET_EVENT_LIST, _this2.eventBrowserList);
                                          }
                                    }

                                    _this2.startEventPolling();
                              });
                        } catch (err) {
                              throw err;
                        }
                  }
            }, {
                  key: "procPageEventData",
                  value: function procPageEventData() {
                        var _this3 = this;

                        try {
                              this.requestEventData(window.wemb.pageManager.currentPageInfo.id).then(function (res) {
                                    if (res) {
                                          var before = JSON.stringify(_this3.eventList);

                                          if (before != JSON.stringify(res.data)) {
                                                _this3.eventList = res.data;

                                                _this3._$bus.$emit(DCIMManager.GET_ASSET_EVENT_LIST, _this3.eventList);
                                          }
                                    }

                                    _this3.startPageEventPolling();
                              });
                        } catch (err) {
                              throw err;
                        }
                  }
            }, {
                  key: "procHistoryData",
                  value: function procHistoryData() {
                        var _this4 = this;

                        try {
                              this.activeHistoryInfo.current = true;
                              this.requestHistoryData(this._activeHistoryInfo).then(function (res) {
                                    if (res) {
                                          Object.keys(res.data).forEach(function (type) {
                                                var histList = res.data[type];
                                                wemb.dcimManager.assetAllocationProxy.assetCompositionInfoGroupMap.get(type).map(function (item) {
                                                      item.comInstance.changeNotification({
                                                            type: DCIMManager.NOTI_CHANGE_EVENT_DATA,
                                                            body: _this4.getAssetDataFromSource(histList, item.assetId)
                                                      });
                                                });
                                          });
                                    }

                                    _this4.startPollingHistoryData();
                              });
                        } catch (err) {
                              throw err;
                        }
                  }
            }, {
                  key: "getAssetDataFromSource",
                  value: function getAssetDataFromSource(source, assetId) {
                        var idx = source.findIndex(function (item) {
                              return item.id == assetId;
                        });

                        if (idx > -1) {
                              return source.splice(idx, 1)[0];
                        }

                        return assetId;
                  }
            }, {
                  key: "getDummyData",
                  value: function getDummyData() {
                        var pages = wemb.pageManager.getPageInfoList().map(function (x) {
                              return x.id;
                        });
                        var typeList = ["FireSensor", "cctv", "pdu", "LeakDetectorSensor", "TempHumiSensor", "AccessSensor"];
                        var severityList = ["normal", "critical", "warning", "major", "minor"];
                        var dataLen = 20;
                        var ary = [];

                        // for (var i = 0; i < dataLen; i++) {
                        //       ary.push({
                        //             "severity": severityList[Math.floor(Math.random() * severityList.length)],
                        //             "evt_id": "eventId" + i,
                        //             "msg": "outside Back Gate guard Top - Motion detected. 00" + i,
                        //             "page_id": pages[Math.floor(Math.random() * pages.length)],
                        //             "asset_id": "TEST" + i,
                        //             "asset_type": typeList[Math.floor(Math.random() * typeList.length)],
                        //             "asset_name": "TEST" + i,
                        //             "stime": this.formatStringToDate(new Date(+new Date() - Math.floor(Math.random() * 10000000000))),
                        //             "dupl_cnt": i
                        //       });
                        // }

                        return ary;
                  }
            }, {
                  key: "_filterEventListByTypes",
                  value: function _filterEventListByTypes(eventList, typeIds) {
                        if (typeof typeIds == "string") {
                              return eventList.filter(function (x) {
                                    return x.asset_type == typeIds;
                              }); //[]
                        } else {
                              var result = {};
                              typeIds.forEach(function (typeId) {
                                    result[typeId] = eventList.filter(function (x) {
                                          return x.asset_type == typeId;
                                    }); //{"sensor": [], ..}
                              });
                              return result;
                        }
                  }
            }, {
                  key: "filterEventList",
                  value: function filterEventList(list, pageIds, typeIds) {
                        var _this5 = this;

                        if (!list || !list.length) {
                              return null;
                        }

                        if (typeof pageIds == "string") {
                              /*  단일페이지
                                  단일 타입일 경우
                                  [{eventData}, {eventData}]
                                  복수 타입일 경우
                                  {
                                        "sensor" : [{eventData}, {eventData}],
                                        "cctv" : [{eventData}, {eventData}]
                                  }*/
                              var filterPageEvents = list.filter(function (x) {
                                    return x.page_id == pageIds;
                              });
                              return this._filterEventListByTypes(filterPageEvents, typeIds);
                        } else {
                              //list

                              /* 복수 페이지,
                                 단일 타입일경우
                                     { pageId001": [{eventData}, {eventData}] }
                                  복수 타입일경우
                                    { "pageId001": {
                                                "sensor" : [{eventData}, {eventData}],
                                                "cctv" : [{eventData}, {eventData}]
                                          }
                                    }*/
                              var result = {};
                              pageIds.forEach(function (pageId) {
                                    var filterPageEvents = list.filter(function (x) {
                                          return x.page_id == pageId;
                                    });
                                    result[pageId] = _this5._filterEventListByTypes(filterPageEvents, typeIds);
                              });
                              return result;
                        }
                  }
            }, {
                  key: "formatStringToDate",
                  value: function formatStringToDate(date) {}
            }, {
                  key: "syncPageEventData",

                  /*
                  2019. 02.14(ckkim)
                  이벤트 리스트 정보를 즉시 업데이트 하는 기능
                   */
                  value: function () {
                        var _syncPageEventData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee7() {
                                    var result, _ref, _ref2, error, res;

                                    return regeneratorRuntime.wrap(function _callee7$(_context7) {
                                          while (1) {
                                                switch (_context7.prev = _context7.next) {
                                                      case 0:
                                                            result = this.requestEventData(window.wemb.pageManager.currentPageInfo.id);
                                                            _context7.next = 3;
                                                            return CPUtil.getInstance().destructAssignmentPromise(result);

                                                      case 3:
                                                            _ref = _context7.sent;
                                                            _ref2 = _slicedToArray(_ref, 2);
                                                            error = _ref2[0];
                                                            res = _ref2[1];

                                                            if (!error) {
                                                                  _context7.next = 10;
                                                                  break;
                                                            }

                                                            console.log("error", error);
                                                            return _context7.abrupt("return", null);

                                                      case 10:
                                                            if (res) {
                                                                  this.eventList = res.data;
                                                            }

                                                            return _context7.abrupt("return", this.eventList);

                                                      case 12:
                                                      case "end":
                                                            return _context7.stop();
                                                }
                                          }
                                    }, _callee7, this);
                              }));

                        function syncPageEventData() {
                              return _syncPageEventData.apply(this, arguments);
                        }

                        return syncPageEventData;
                  }()
            }, {
                  key: "$bus",
                  get: function get() {
                        return this._$bus;
                  }
            }, {
                  key: "eventBrowserList",
                  get: function get() {
                        return this._eventBrowserList;
                  },
                  set: function set(list) {
                        this._eventBrowserList = list;
                  }
            }, {
                  key: "activeHistoryInfo",
                  set: function set(info) {
                        this._activeHistoryInfo = info;
                  },
                  get: function get() {
                        return this._activeHistoryInfo;
                  }
            }]);

            return DCIMEventManager;
      }();
