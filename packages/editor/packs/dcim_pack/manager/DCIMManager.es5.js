"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/*
plugin은 모두 mediator의 view로 등록됨.
 */
var DCIMManager =
      /*#__PURE__*/
      function (_ExtensionPluginCore) {
            _inherits(DCIMManager, _ExtensionPluginCore);

            function DCIMManager() {
                  var _this;

                  _classCallCheck(this, DCIMManager);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(DCIMManager).call(this));
                  _this._path = wemb.configManager.serverUrl + "/dcim.do";
                  _this._eventManager = new DCIMEventManager();
                  _this._pagesetManager = new DCIMPagesetManager();
                  _this._typesetManager = new DCIMTypesetManager();
                  _this._mountDataManager = new DCIMMountDataManager();
                  _this._assetComponentProxy = null;
                  _this._allocationProxy = null;
                  _this._lang = Vue.$i18n.messages.wv.dcim_pack;
                  _this._$bus = new Vue();
                  return _this;
            }
            /**
             * Viewer전용
             * 통합자산검색 팝업 호출
             */


            _createClass(DCIMManager, [{
                  key: "openIntergratedAssetSearch",
                  value: function openIntergratedAssetSearch() {
                        if (window.wemb.configManager.exeMode == "viewer") {
                              window.wemb.showExternalModal(window.ViewerStatic.CMD_SHOW_MODAL, window.wemb.configManager.context + "/custom/packs/dcim_pack/manager/view/search/DCIMUnifiedSearchMainComponent.vue");
                        }
                  }
            }, {
                  key: "start",
                  value: function start() {
                        window.wemb.dcimManager = DCIMManager._instance;
                        /*
                        에디터 모드에서만 템플릿 등록하기
                        */

                        if (wemb.configManager.exeMode == "editor") {
                              DCIMComponentPropertyTemplateRegister.regist();
                        }
                  }
            }, {
                  key: "setFacade",
                  value: function setFacade(facade) {
                        _get(_getPrototypeOf(DCIMManager.prototype), "setFacade", this).call(this, facade); // Renobit Puremvc proxy 등록하기


                        this._assetComponentProxy = new AssetComponentProxy(AssetComponentProxy.NAME);
                        facade.registerProxy(this._assetComponentProxy);
                        facade.registerProxy(new AssetComponentInfoPlacedPageProxy(this._assetComponentProxy));
                        this._allocationProxy = new AssetAllocationProxy(AssetAllocationProxy.NAME);
                        facade.registerProxy(this._allocationProxy);
                        /*
                        페이지가 최초 열릴때
                        */
                        // 내부에서 사용하는 proxy 사용하기

                        this._allocationProxy.attachProxy();
                  }
            }, {
                  key: "$on",
                  value: function $on(eventName, data) {
                        return this._$bus.$on(eventName, data);
                  }
            }, {
                  key: "destroy",
                  value: function destroy() {}
            }, {
                  key: "handleNotification",

                  /*
                  noti가 온 경우 실행
                  call : mediator에서 실행
                  */
                  value: function handleNotification(note) {
                        switch (note.name) {
                              case ViewerProxy.NOTI_UPDATE_VIEWER_STATE:
                              case EditorProxy.NOTI_UPDATE_EDITOR_STATE:
                                    this._register_HookAction(note.getBody());

                                    break;
                              // 편집 페이지가 열릴때마다 실행

                              case ViewerProxy.NOTI_OPENED_PAGE:
                                    this.noti_openedPage();
                                    break;
                        }
                  }
            }, {
                  key: "_register_HookAction",
                  value: function () {
                        var _register_HookAction2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee(pageLoadingState) {
                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                          while (1) {
                                                switch (_context.prev = _context.next) {
                                                      case 0:
                                                            if (!(pageLoadingState == "readyCompleted")) {
                                                                  _context.next = 16;
                                                                  break;
                                                            }

                                                            /*
                                                            2018.11.21(ckkim)
                                                            중요!!!
                                                            - 저장 훅 이벤트 등록하기
                                                            */

                                                            /*
                                                            페이지 정보를 로드하기 전
                                                            */
                                                            wemb.hookManager.addAction(HookManager.HOOK_BEFORE_LOAD_PAGE, this._hookBeforeLoadPage.bind(this)); // 페이지 정보를 로드 완료 한 후 실행, 컴포넌트 인스턴스가 만들어지기 전

                                                            wemb.hookManager.addAction(HookManager.HOOK_AFTER_LOAD_PAGE, this._hookAfterLoadPage.bind(this));
                                                            wemb.hookManager.addAction(HookManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE, this._hookBeforeAddComponentInstance.bind(this)); // 페이지 로딩될때

                                                            _context.prev = 4;
                                                            _context.next = 7;
                                                            return this._typesetManager.startLoading();

                                                      case 7:
                                                            _context.next = 9;
                                                            return this._pagesetManager.startLoading();

                                                      case 9:
                                                            return _context.abrupt("return", true);

                                                      case 12:
                                                            _context.prev = 12;
                                                            _context.t0 = _context["catch"](4);
                                                            console.log("@@error, DCIMManager._register_HookAction ", _context.t0);
                                                            return _context.abrupt("return", false);

                                                      case 16:
                                                      case "end":
                                                            return _context.stop();
                                                }
                                          }
                                    }, _callee, this, [[4, 12]]);
                              }));

                        function _register_HookAction(_x) {
                              return _register_HookAction2.apply(this, arguments);
                        }

                        return _register_HookAction;
                  }()
            }, {
                  key: "_hookBeforeLoadPage",
                  value: function () {
                        var _hookBeforeLoadPage2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2() {
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            _context2.prev = 0;
                                                            _context2.next = 3;
                                                            return this._assetComponentProxy.loadAssetsData();

                                                      case 3:
                                                            _context2.next = 5;
                                                            return this._assetComponentProxy.loadAssetComponentsInfo();

                                                      case 5:
                                                            return _context2.abrupt("return", true);

                                                      case 8:
                                                            _context2.prev = 8;
                                                            _context2.t0 = _context2["catch"](0);
                                                            console.log("@@ error AssetComponentPlugin loadAssetsData ", _context2.t0);
                                                            return _context2.abrupt("return", false);

                                                      case 12:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2, this, [[0, 8]]);
                              }));

                        function _hookBeforeLoadPage() {
                              return _hookBeforeLoadPage2.apply(this, arguments);
                        }

                        return _hookBeforeLoadPage;
                  }()
                  /*
                  컴포넌트가 만들어지기 컴포넌트 정보에 자산 정보 추가하기
                  */

            }, {
                  key: "_hookAfterLoadPage",
                  value: function () {
                        var _hookAfterLoadPage2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee3(comInstanceMetaInfoList) {
                                    var _this2 = this;

                                    return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                          while (1) {
                                                switch (_context3.prev = _context3.next) {
                                                      case 0:
                                                            comInstanceMetaInfoList.forEach(function (comInstanceMetaInfo) {
                                                                  _this2.injectAssetDataToComponentInfo(comInstanceMetaInfo);
                                                            });
                                                            return _context3.abrupt("return", true);

                                                      case 2:
                                                      case "end":
                                                            return _context3.stop();
                                                }
                                          }
                                    }, _callee3);
                              }));

                        function _hookAfterLoadPage(_x2) {
                              return _hookAfterLoadPage2.apply(this, arguments);
                        }

                        return _hookAfterLoadPage;
                  }()
                  /*
                  컴포넌트 중 자산 컴포넌트인 경우
                  - 자산 정보 + 자산 컴포넌트 인 경우 = 컴포넌트에 자산 정보에 환경설정 정보를 추가
                  - 자산 컴포넌트 인 경우 = 컴포넌트에 환경설정 정보만 추가
                  */

            }, {
                  key: "_hookBeforeAddComponentInstance",
                  value: function () {
                        var _hookBeforeAddComponentInstance2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee4(comInstanceMetaInfo) {
                                    var assetData;
                                    return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                          while (1) {
                                                switch (_context4.prev = _context4.next) {
                                                      case 0:
                                                            // 자산 패널에서 자산이 drag_drop되는 경우 실행
                                                            if (wemb.globalStore.has("__drag_asset_data__") == true) {
                                                                  assetData = wemb.globalStore.get("__drag_asset_data__");
                                                                  this.injectAssetDataToComponentInfo(comInstanceMetaInfo, assetData);
                                                                  wemb.globalStore.clear("__drag_asset_data__");
                                                            } else {
                                                                  // 일반 3D 자산 컴포넌트인 경우 환경설정 정보를 적용.
                                                                  this._assetComponentProxy.attachComponentConfigInfoToComInstanceInfo(comInstanceMetaInfo);
                                                            }

                                                            return _context4.abrupt("return", true);

                                                      case 2:
                                                      case "end":
                                                            return _context4.stop();
                                                }
                                          }
                                    }, _callee4, this);
                              }));

                        function _hookBeforeAddComponentInstance(_x3) {
                              return _hookBeforeAddComponentInstance2.apply(this, arguments);
                        }

                        return _hookBeforeAddComponentInstance;
                  }()
                  /*
                  컴포넌트 정보에 자산 정보 설정하기
                  1. 컴포넌트 정보가 3D 컴포넌트만
                  2. 컴포넌트 정보가 자산 컴포넌트인 경우만
                  3. 컴포넌트 id를 가지고 컴포넌트 환경설정 정보 구하기
                  4. 컴포넌트 정보에 기본 팝업 id  설정하기
                  5. 컴포넌트 정보에 리소스 정보 설정하기
                  6. 자산 정보가 존재하는 경우
                  */

            }, {
                  key: "injectAssetDataToComponentInfo",
                  value: function injectAssetDataToComponentInfo(comInstanceInfo) {
                        var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

                        // 컴포넌트가 만들지기 전, 인스턴스 정보에 환경설정 정보 추가
                        this._assetComponentProxy.attachComponentConfigInfoToComInstanceInfo(comInstanceInfo);
                        /*
                        경우01: 자산 정보를 인스턴스 정보에 추가하기
                        */


                        if (data) {
                              this._assetComponentProxy.attachAssetDataToComInstanceInfo(data, comInstanceInfo);
                        } else {
                              /*
                              경우02: 컴포넌트에 이미 바인딩되어 있는 자산 정보로 설정하기
                               */
                              this._assetComponentProxy.attachBindingAssetDataToComInstanceInfo(comInstanceInfo);
                        }
                  }
            }, {
                  key: "initProperties",
                  value: function initProperties() {}
                  /*
                   pageName에서  자산 정보 구해오기
                    1. 읽어들일 페이지 이름으로 페이지 id 구하기
                   2. 서버에서 페이지 정보 구하기
                   3. 레이어 필터링 처리하기
                   4. 자산 타입 필터링 처리하기
                  - 자산 컴포넌트가 아닌 경우는 모두 추가
                  - 자산 컴포넌트 중 options.includeTypes에 해당하는 자산 컴포넌트 만 추가
                  */

            }, {
                  key: "getAssetDataInPage",
                  value: function () {
                        var _getAssetDataInPage = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee5(pageName, options) {
                                    var assetComponentInfoPlacedPageProxy;
                                    return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                          while (1) {
                                                switch (_context5.prev = _context5.next) {
                                                      case 0:
                                                            assetComponentInfoPlacedPageProxy = this._facade.retrieveProxy(AssetComponentInfoPlacedPageProxy.NAME);
                                                            _context5.next = 3;
                                                            return assetComponentInfoPlacedPageProxy.getAssetDataInPage(pageName, options);

                                                      case 3:
                                                            return _context5.abrupt("return", _context5.sent);

                                                      case 4:
                                                      case "end":
                                                            return _context5.stop();
                                                }
                                          }
                                    }, _callee5, this);
                              }));

                        function getAssetDataInPage(_x4, _x5) {
                              return _getAssetDataInPage.apply(this, arguments);
                        }

                        return getAssetDataInPage;
                  }()
                  /*
                  pageNameList에 들어 있는 페이지 리스트별로
                  자산 정보 중  출입 센서 정보만을 가져오기
                  @pageNameList : 페이지이름 리스트, 페이지 ID가 아님.
                  */

            }, {
                  key: "getAccessSensor3DModelDataInFloorPage",
                  value: function () {
                        var _getAccessSensor3DModelDataInFloorPage = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee7(pageNameList) {
                                    var assetComponentInfoPlacedPageProxy;
                                    return regeneratorRuntime.wrap(function _callee7$(_context7) {
                                          while (1) {
                                                switch (_context7.prev = _context7.next) {
                                                      case 0:
                                                            assetComponentInfoPlacedPageProxy = this._facade.retrieveProxy(AssetComponentInfoPlacedPageProxy.NAME);

                                                            if (!(Array.isArray(pageNameList) == false)) {
                                                                  _context7.next = 4;
                                                                  break;
                                                            }

                                                            console.warn("RENOBIT The parameter value is not an array.");
                                                            return _context7.abrupt("return", Promise.resolve([]));

                                                      case 4:
                                                            return _context7.abrupt("return", new Promise(
                                                                  /*#__PURE__*/
                                                                  function () {
                                                                        var _ref = _asyncToGenerator(
                                                                              /*#__PURE__*/
                                                                              regeneratorRuntime.mark(function _callee6(resolve, reject) {
                                                                                    var result, i, pageName, info;
                                                                                    return regeneratorRuntime.wrap(function _callee6$(_context6) {
                                                                                          while (1) {
                                                                                                switch (_context6.prev = _context6.next) {
                                                                                                      case 0:
                                                                                                            result = [];
                                                                                                            i = 0;

                                                                                                      case 2:
                                                                                                            if (!(i < pageNameList.length)) {
                                                                                                                  _context6.next = 12;
                                                                                                                  break;
                                                                                                            }

                                                                                                            pageName = pageNameList[i];
                                                                                                            info = {
                                                                                                                  pageName: pageName,
                                                                                                                  data: []
                                                                                                            };
                                                                                                            _context6.next = 7;
                                                                                                            return assetComponentInfoPlacedPageProxy.getAccessSensor3DModelDataInFloorPage(pageName);

                                                                                                      case 7:
                                                                                                            info.data = _context6.sent;
                                                                                                            result.push(info);

                                                                                                      case 9:
                                                                                                            i++;
                                                                                                            _context6.next = 2;
                                                                                                            break;

                                                                                                      case 12:
                                                                                                            resolve(result);

                                                                                                      case 13:
                                                                                                      case "end":
                                                                                                            return _context6.stop();
                                                                                                }
                                                                                          }
                                                                                    }, _callee6);
                                                                              }));

                                                                        return function (_x7, _x8) {
                                                                              return _ref.apply(this, arguments);
                                                                        };
                                                                  }()));

                                                      case 5:
                                                      case "end":
                                                            return _context7.stop();
                                                }
                                          }
                                    }, _callee7, this);
                              }));

                        function getAccessSensor3DModelDataInFloorPage(_x6) {
                              return _getAccessSensor3DModelDataInFloorPage.apply(this, arguments);
                        }

                        return getAccessSensor3DModelDataInFloorPage;
                  }()
                  /*
                   2018.11.09(ckkim)
                   데이터 refresh
                    call : AssetComponentPanel에서  refresh() 버튼 시 호출
                     1. 컴포넌트 환경설정 정보 읽기
                    2. 자산 정보 읽기
                  2-1. 타입 정보 읽기
                  await dcimAssetProxy.loadAssetTypes();
                  2-2. 자산 정보 읽기
                  await dcimAssetProxy.loadAssetsData();
                  2-3. 컴포넌트 타입 정보 읽기
                  await dcimAssetProxy.loadAssetComponentsInfo();
                    3. 배치된 합성자산정보(자산 정보+컴포넌트 정보) 생성.
                  3-1. NOTI_UPDATE_ASSET_ALLOCATION_LIST 전송
                  (컴포넌트 업데이트)
                    4. 중복 체크
                    5. 배치된 자산 컴포넌트에 환경설정 기본 정보 적용하기
                  - popup Id (config 정보)
                  - assetData (자산 정보)
                  - resource (config 정보)
                   */

            }, {
                  key: "refreshAssetAllData",
                  value: function () {
                        var _refreshAssetAllData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee8() {
                                    var assetAllocationProxy, assetComponentProxy, checked;
                                    return regeneratorRuntime.wrap(function _callee8$(_context8) {
                                          while (1) {
                                                switch (_context8.prev = _context8.next) {
                                                      case 0:
                                                            _context8.prev = 0;
                                                            _context8.next = 3;
                                                            return this._assetComponentProxy.loadAssetComponentsInfo();

                                                      case 3:
                                                            //3. 배치된 합성자산정보(자산 정보+컴포넌트 정보) 생성.
                                                            assetAllocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME); // 2. 자산 정보 읽기

                                                            assetComponentProxy = this._facade.retrieveProxy(AssetComponentProxy.NAME);
                                                            _context8.next = 7;
                                                            return assetComponentProxy.loadAssetsData();

                                                      case 7:
                                                            //3. 중복 배치 체크
                                                            checked = AssetPagePlugin._instance.executeCheckingDuplicatedAsset();
                                                            /*
                                                            1.싱크 전 자산 정보에서 현재 페이지에 배치된 내용을 모두 지운다.
                                                            2. 맵핑 정보를 신규 자산 정보에 적용한다.
                                                            3. 자산 복합 정보(compositionInfo)를 신규로 만든다.
                                                            */

                                                            assetAllocationProxy.syncAssetCompositionInfoList(); //5. 배치된 자산 컴포넌트에 환경설정 기본 정보 적용하기

                                                            assetAllocationProxy.attachDefaultAssetComponentInfo();
                                                            if (checked == false) Vue.$message(this._lang.manager.sync);
                                                            _context8.next = 16;
                                                            break;

                                                      case 13:
                                                            _context8.prev = 13;
                                                            _context8.t0 = _context8["catch"](0);
                                                            console.error(new Date().getTime().toString(), "ERROR", "DCIMManager refreshAssetAllData ", _context8.t0);

                                                      case 16:
                                                      case "end":
                                                            return _context8.stop();
                                                }
                                          }
                                    }, _callee8, this, [[0, 13]]);
                              }));

                        function refreshAssetAllData() {
                              return _refreshAssetAllData.apply(this, arguments);
                        }

                        return refreshAssetAllData;
                  }()
            }, {
                  key: "noti_openedPage",
                  value: function noti_openedPage() {
                        this._eventManager.clearEventList();

                        if (!wemb.configManager.isEditorMode) {
                              this._eventManager.procPageEventData();
                        }
                        /*
                        자산 정보 업데이트
                        */
                        // 업데이트 처리(NOTI_UPDATE_ASSET_ALLOCATION_LIST 전송)


                        this._allocationProxy.updateAssetAllocationList();
                  }
                  /**
                   * 연관자산정보 가져오기
                   * */

            }, {
                  key: "getRelationData",
                  value: function getRelationData(parent_id) {
                        var _this3 = this;

                        var param = {
                              "id": "assetRelationService.getRelation",
                              "params": {
                                    parent_id: parent_id
                              }
                        };
                        return new Promise(function (resolve) {
                              window.wemb.$http.post(_this3.path, param).then(function (res) {
                                    resolve([null, res]);
                              })["catch"](function (error) {
                                    resolve([error, null]);
                              });
                        });
                  }
                  /**
                   * 연관자산 저장
                   * */

            }, {
                  key: "postRelationData",
                  value: function postRelationData(params) {
                        var _this4 = this;

                        var param = {
                              "id": "assetRelationService.setRelation",
                              "params": params
                        };
                        return new Promise(function (resolve) {
                              window.wemb.$http.post(_this4.path, param).then(function (response) {
                                    resolve([null, response]);
                              })["catch"](function (error) {
                                    resolve([error, null]);
                              });
                        });
                  }
                  /**
                   * 자산 정보 업데이트 요청
                   * */

            }, {
                  key: "updateAsset",
                  value: function updateAsset(params) {
                        var _this5 = this;

                        var encTypes = window.wemb.dcimManager._assetComponentProxy.fields.type[params.asset_type].filter(function(asset){
                              return asset.encryption;
                        });
                        var encNames = encTypes.map(function(type){
                                    return type.name;	
                        });
                        var paramKey = Object.getOwnPropertyNames(params);
                        for(var i = 0; encNames.length > 0 && i < paramKey.length; i ++){
                              if(encNames.indexOf(paramKey[i]) > -1){
                                    params[paramKey[i]] = Base64.encode(params[paramKey[i]]);
                              }
                        }

                        var param = {
                              "id": "assetDataService.updateData",
                              "params": params
                        };
                        return new Promise(function (resolve, reject) {
                              window.wemb.$http.post(_this5.path, param).then(function (response) {
                                    resolve([null, response]);
                              })["catch"](function (error) {
                                    resolve([error, null]);
                              });
                        });
                  }
                  /*
                  assetId에 해당하는 자산 정보 업데이트 처리
                  1. 자산 정보 업데이트
                  2. 컴포넌트에 신규 자산 정보 업데이트 처리
                  call : 외부에서, 특히 자산 팝업등에서 자산 정보가 DB에 쌓이는 경우 처리
                  */

            }, {
                  key: "updateAssetDataBy",
                  value: function () {
                        var _updateAssetDataBy = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee9(targetAssetData) {
                                    var assetId, assetType, assetData, assetCompositionInfo;
                                    return regeneratorRuntime.wrap(function _callee9$(_context9) {
                                          while (1) {
                                                switch (_context9.prev = _context9.next) {
                                                      case 0:
                                                            if (targetAssetData) {
                                                                  _context9.next = 2;
                                                                  break;
                                                            }

                                                            return _context9.abrupt("return", false);

                                                      case 2:
                                                            assetId = null;
                                                            assetType = null;
                                                            _context9.prev = 4;
                                                            assetId = targetAssetData.id;
                                                            assetType = targetAssetData.asset_type;
                                                            _context9.next = 12;
                                                            break;

                                                      case 9:
                                                            _context9.prev = 9;
                                                            _context9.t0 = _context9["catch"](4);
                                                            return _context9.abrupt("return", false);

                                                      case 12:
                                                            _context9.next = 14;
                                                            return this.assetManager.updateAssetDataBy(assetId, assetType);

                                                      case 14:
                                                            assetData = _context9.sent;

                                                            if (!(assetData == false)) {
                                                                  _context9.next = 17;
                                                                  break;
                                                            }

                                                            return _context9.abrupt("return", false);

                                                      case 17:
                                                            // 2. 컴포넌트에 신규 자산 정보 업데이트 처리
                                                            assetCompositionInfo = this.assetAllocationProxy.assetCompositionInfoMap.get(assetId);

                                                            if (assetCompositionInfo) {
                                                                  _context9.next = 20;
                                                                  break;
                                                            }

                                                            return _context9.abrupt("return", false);

                                                      case 20:
                                                            // 업데이트 메시지
                                                            assetCompositionInfo.comInstance.changeNotification({
                                                                  type: DCIMManager.NOTI_CHANGE_ASSET_DATA,
                                                                  body: assetData
                                                            });
                                                            return _context9.abrupt("return", true);

                                                      case 22:
                                                      case "end":
                                                            return _context9.stop();
                                                }
                                          }
                                    }, _callee9, this, [[4, 9]]);
                              }));

                        function updateAssetDataBy(_x9) {
                              return _updateAssetDataBy.apply(this, arguments);
                        }

                        return updateAssetDataBy;
                  }()
            }, {
                  key: "saveRackMountData",
                  value: function () {
                        var _saveRackMountData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee10() {
                                    var data, param, result;
                                    return regeneratorRuntime.wrap(function _callee10$(_context10) {
                                          while (1) {
                                                switch (_context10.prev = _context10.next) {
                                                      case 0:
                                                            data = this.getCurrentEditData();
                                                            param = {
                                                                  "id": "assetDataService.getMount",
                                                                  "params": data
                                                            };
                                                            _context10.next = 4;
                                                            return wemb.$http.post(window.wemb.dcimManager.path, param);

                                                      case 4:
                                                            result = _context10.sent;

                                                            if (result) {
                                                                  Vue.$message.success(this.lang_common.successSave);
                                                                  this.saveLocalData(data);
                                                                  this.refresh();
                                                            } else {
                                                                  Vue.$message.error(this.lang_common.errorSave);
                                                            }

                                                      case 6:
                                                      case "end":
                                                            return _context10.stop();
                                                }
                                          }
                                    }, _callee10, this);
                              }));

                        function saveRackMountData() {
                              return _saveRackMountData.apply(this, arguments);
                        }

                        return saveRackMountData;
                  }()
            }, {
                  key: "$bus",
                  get: function get() {
                        return this._$bus;
                  }
            }, {
                  key: "mountDataManager",
                  get: function get() {
                        return this._mountDataManager;
                  }
            }, {
                  key: "eventManager",
                  get: function get() {
                        return this._eventManager;
                  }
            }, {
                  key: "pagesetManager",
                  get: function get() {
                        return this._pagesetManager;
                  }
            }, {
                  key: "typesetManager",
                  get: function get() {
                        return this._typesetManager;
                  }
            }, {
                  key: "assetManager",
                  get: function get() {
                        return this._assetComponentProxy;
                  }
            }, {
                  key: "path",
                  get: function get() {
                        return this._path;
                  }
                  /*
                  팝업 호출, 카메라 이동 처리를 담당하는 플러그인.
                  */

            }, {
                  key: "actionController",
                  get: function get() {
                        return window.wemb.assetComponentControllerPlugin;
                  }
                  /*
                  2018.11.12(ckkim)
                  현재 열려있는 페이지의 자산+컴포넌트 정보
                  */

            }, {
                  key: "assetAllocationProxy",
                  get: function get() {
                        return this._allocationProxy;
                  }
            }, {
                  key: "notificationList",
                  get: function get() {
                        return [EditorProxy.NOTI_UPDATE_EDITOR_STATE, ViewerProxy.NOTI_UPDATE_VIEWER_STATE, ViewerProxy.NOTI_OPENED_PAGE];
                  }
            },{
                  key:"exportAssetSetting",
                  value:function() {
                        console.log('exportAssetSetting')
                        var _this3 = this;
                        var param = {
                              "id":"assetTypeService.getTypeList",
                              "params":{}
                        }
                        var result_json = {
                              "TB_ASSET_TYPE":[],
                              "TB_ASSET_RESOURCE":[],
                              "TB_ASSET_GROUP":[],
                              "TB_ASSET_COMPONENT":[]
                        };
                        window.wemb.$http.post(_this3.path, param).then(function (res) {
                              var asset_types = res.data;
                              var type_ids = res.data.types.map(function(d) { return d.id});
                              param.id = "assetResourceService.getTypeResource"
                              param.params = {
                                    asset_type : type_ids.toString()
                              }
                              window.wemb.$http.post(_this3.path, param).then(function (res) {
                                    var asset_resource = res.data;
                                    param.id = "assetGroupService.getGroupList";
                                    param.params = {}
                                    window.wemb.$http.post(_this3.path, param).then(function (res) {
                                          var asset_group = res.data;
                                          param.id = "assetComponentService.getComponentList";
                                          param.params = {}
                                          window.wemb.$http.post(_this3.path, param).then(function (res) {
                                                var query = "";
                                                var asset_comp = res.data;
                                                asset_types.types.push({"id":"common", "name":"name", "fields":asset_types.common_fields});
                                                result_json["TB_ASSET_TYPE"] = asset_types.types;

                                                asset_types.types.forEach(function(d) {
                                                      query += "INSERT INTO TB_ASSET_TYPE (\"id\", \"name\" , \"fields\") VALUES ('" + d.id + "', '" + d.name + "', '" + JSON.stringify(d.fields) + "');\r\n";
                                                });

                                                result_json["TB_ASSET_RESOURCE"] = asset_resource.data.map(function(d) {
                                                      query += "INSERT INTO TB_ASSET_RESOURCE (\"asset_type\", \"resource_id\") VALUES ('" + d.asset_type + "', '" + d.resource_id + "');\r\n";
                                                      return { asset_type:d.asset_type, resource_id:d.resource_id }
                                                })
                                                asset_group.data.forEach(function(d) {
                                                      d.types.forEach(function(b,i) {
                                                            query += "INSERT INTO TB_ASSET_GROUP (\"group_name\", \"asset_type\",\"seq\") VALUES ('" + d.name + "', '" + b.id +"','" + i + "');\r\n";
                                                            result_json["TB_ASSET_GROUP"].push( { group_name: d.name, asset_type: b.id, seq:i})
                                                      })
                                                })
                                                asset_comp.data.forEach(function(types) {
                                                      types.children.forEach(function(comp) {
                                                            query += "INSERT INTO TB_ASSET_COMPONENT (\"file_id\", \"name\",\"asset_type\",\"batch\",\"props\") VALUES ('" + comp.data.file_id + "', '" + comp.data.name +"','" + comp.data.asset_type +"','" + "1" +"','" + JSON.stringify(comp.data.props) + "');\r\n";
                                                            result_json["TB_ASSET_COMPONENT"].push({file_id:comp.data.file_id, name:comp.data.name, asset_type:comp.data.asset_type, batch:1, props:comp.data.props })
                                                    })
                                                })

                                                var fileName = "asset_setting.sql";
                                                var data = query + "commit;";
                                                if (window.navigator.msSaveBlob) {
                                                      let textFileAsBlob = new Blob([data], {type: 'text/plain'});
                                                      window.navigator.msSaveBlob(textFileAsBlob, fileName);
                                    
                                                } else {
                                                      let textFileAsBlob = new Blob([data], {type: 'text/plain'});
                                                      let downloadLink = document.createElement("a");
                                                      downloadLink.download = fileName;
                                                      downloadLink.innerHTML = "Download File";
                                    
                                                      downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
                                                      downloadLink.onclick = function(event) {
                                                            document.body.removeChild(event.target);
                                                      };
                                    
                                                      downloadLink.style.display = "none";
                                                      document.body.appendChild(downloadLink);
                                                      downloadLink.click();
                                                }
                                          });
                                    });
                              });
                        })["catch"](function (error) {
                              console.log(error);
                        });
                  }
            },{
                  key:"importAssetSetting",
                  value:function() {
                        console.log('importAssetSetting')
                  }
            }]);

            return DCIMManager;
      }(ExtensionPluginCore);

DCIMManager.API = {
      ASSET_RELATION: "/asset/relation",
      ASSET_UPDATE: "/asset/update"
};
DCIMManager.GET_EVENT_LIST = '/event/list'; // 이벤트 브라우저

DCIMManager.GET_ASSET_EVENT_LIST = '/event/data/type';
DCIMManager.CHANGE_EVENT_LIST = '/event/data';
DCIMManager.UPDATE_PAGESET_LIST = '/event/updatePagesetList';
DCIMManager.EVENT_COMPLETE_ACK = '/event/ack/complete';
DCIMManager.EVENT_POLLING_INTERVAL_TIME = 5000;
DCIMManager.NOTI_CHANGE_SEVERITY = "/notification/changeSeverity";
DCIMManager.NOTI_CHANGE_PROPS_DATA = "/notification/changePropsData";
DCIMManager.NOTI_CHANGE_ASSET_DATA = "/notification/changeAssetData";
DCIMManager.NOTI_CHANGE_EVENT_DATA = "/notification/changeEventData";
DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION = "/notification/selectInitCameraPosition";
DCIMManager.PACK_PATH = "custom/packs/dcim_pack/components/2D/";
