/**
 * 페이지셋에서 편집한 데이터를 API로 조회할 수 있는 Manager
 */
class DCIMPagesetManager {
    constructor() {
        this._apiPath = wemb.configManager.serverUrl + "/loader.do";
        this._filePath = "output/dcim/pageset.json";
        this._pagesetList = [];
        this._pagesetId = '';
        this._$bus = new Vue();

        this._buildings = null;
        this._pageset = null;
        this._pagesetFlatData = [];
    }

    //부지정보
    getSite() {
        let site = null;
        if (this._pageset) {
            site = this._pageset.site;
        }
        return site;
    }

    //설정된 페이지셋 정보
    getPageset() {
        return this._pageset;
    }

    //빌딩 리스트
    getPagesetBuildings() {
        return this._buildings || [];
    }

    $on(event, func) {
        this._$bus.$on(event, func);
    }

    $off(event, func) {
        this._$bus.$off(event, func);
    }

    get $bus() {
        return this._$bus;
    }

    startLoading() {
        return this.loadPagesetData();
    }

    get pagesetId() {
        return this._pagesetId;
    }

    ////페이지셋 전체 리스트
    get pagesetList() {
        return this._pagesetList;
    }


    getAllPageIds() {
        let ids = [];
        if (this._pagesetFlatData) {
            ids = this._pagesetFlatData.map(x => x.id);
        }
        return ids;
    }

    //전달 받은 인자(층/룸)의 상위 빌딩
    getParentBuilding(id) {
        let building = this.getParents(id).find(x => x.type == DCIMPagesetType.BUILDING);
        if (building) {
            return $.extend(true, {}, building);
        } else {
            return null;
        }
    }

    _getParents(tempList, id) {
        let findItem = this._pagesetFlatData.find(x => x.id == id);
        if (!findItem) { return; }
        let parentItem = this._pagesetFlatData.find(x => x.id == findItem.parent);
        if (parentItem) {
            tempList.unshift(parentItem);
            if (parentItem.parent) {
                this._getParents(tempList, parentItem.id);
            }
        }
    }

    //전달받은 인자 항목의 조상 정보 가져오기
    getParents(id) {
        let parents = [];
        let findItem = this._pagesetFlatData.find(x => id == x.id);
        if (findItem) {
            parents = [findItem];
            this._getParents(parents, findItem.id)
        }

        return parents;
    }

    //전달받은 pageId의 breadcrumbs정보 반환
    getPageBreadcrumbs(pageId) {
        let parents = this.getParents(pageId);
        return parents.map((x) => {
            return { name: x.name, id: x.id }
        });
    }


    set pagesetList(list) {
        this._pagesetList.splice(0, this._pagesetList.length);
        this._pagesetList.push(...list);
    }

    //전달받은 building의 층/룸 Id배열 반환
    getFloorRoomIds(building) {
        let ids = [];
        let floors = building.children || [];

        floors.forEach((floor) => {
            ids.push(floor.id);
            let rooms = floor.children;
            if (rooms) {
                rooms.forEach((room) => {
                    ids.push(room.id);
                });
            }
        })
        return ids;
    }

    //첫번째 빌딩의 층 리스트 반환
    getFirstBuildiingFloorList() {
        let buildings = this.getPagesetBuildings();
        if (buildings.length) {
            return buildings[0].children || [];
        }
        return [];
    }

    //전달받은 빌딩Id의 층 리스트 반환
    getFloorList(buildingId) {
        let buildings = this.getPagesetBuildings();
        let findBuilding = buildings.find(x => x.id == buildingId);
        if (findBuilding) {
            return findBuilding.children || [];
        }
        return [];
    }

    //전달받은 페이지셋 ID의 페이지셋 반환
    getPagesetById(id) {
        if (this.pagesetList && this.pagesetList.find(x => x.id == id)) {
            return this.pagesetList.find(x => x.id == id);
        } else {
            console.warn("No Pageset");
            return null;
        }
    }

    //전달받은 페이지셋 이름의 페이지셋 반환
    getPagesetByName(name) {
        if (this.pagesetList && this.pagesetList.find(x => x.name == name)) {
            return this.pagesetList.find(x => x.name == name);
        } else {
            console.warn("No Pageset");
            return null;
        }
    }

    loadPagesetData() {
        // return new Promise((resolve, reject) => {
        let data = {
            "id": "loaderService.registerClass",
            "params": {
                "class": "JSONFileParser",
                "method": "readJSON",
                "file_name": this._filePath
            }
        };

        return http.post(this._apiPath, data).then((result) => {
            if (!result || !result.data || !result.data.data) {
                //reject(result);
            } else {
                let pageData = JSON.parse(result.data.data);
                this._pagesetList = pageData.list || [];
                this.setPageset(pageData.pagesetId);
                this._$bus.$emit(DCIMManager.UPDATE_PAGESET_LIST, this._pagesetList.concat());
                //resolve(result);
            }
        }, (result) => {
            console.log("Pageset JSON data read error", result);
        });
    }

    //페이지셋 리스트에서 전달받은 ID를 페이지셋으로 설정
    setPageset(id) {
        if (id && this._pagesetList.length) {
            try {
                let findItem = $.extend(true, {}, this._pagesetList.find(x => x.id == id));
                this._pagesetId = id;
                let site = findItem.site;
                let structure = findItem.structure;
                if (site) {
                    findItem.structure = [{
                        id: site.id,
                        name: site.name,
                        children: structure
                    }]
                }

                this._pageset = findItem;
                this._buildings = structure;
                this._setPageFlatData(findItem);
            } catch (error) {
                this.emptyPagesetData();
            }
        } else {
            this.emptyPagesetData();
        }
    }

    //페이지셋 데이터를 트리구조에서 flat하게 변경
    _setPageFlatData(pagesetData) {
        this._pagesetFlatData = [];
        let site = pagesetData.site;
        if (site) {
            site.type = DCIMPagesetType.SITE;
        }

        this._pagesetFlatData.push(site);

        let buildings = this._buildings || [];
        buildings.forEach((building) => {
            building.type = DCIMPagesetType.BUILDING;
            building.parent = site ? site.id : '';

            this.syncName(building);
            this._pagesetFlatData.push(building);
            let floors = building.children || [];

            floors.forEach((floor) => {
                floor.type = DCIMPagesetType.FLOOR;
                floor.parent = building.id;
                this.syncName(floor);
                this._pagesetFlatData.push(floor);

                let rooms = floor.children || [];
                rooms.forEach((room) => {
                    room.type = DCIMPagesetType.ROOM;
                    room.parent = floor.id;
                    this.syncName(room);
                    this._pagesetFlatData.push(room);
                });
            });
        });
    }

    //페이지셋 항목 라벨 이름을 페이지 이름을 조회하여 sync함
    syncName(item) {
        let pageName = window.wemb.pageManager.getPageNameById(item.id);
        item.page_name = pageName;

        if (item.sync_page_name) {
            item.name = pageName || item.name;
        }
    }

    emptyPagesetData() {
        this._pageset = null;
        this._buildings = null;
        this._pagesetId = '';
    }

    updatePagesetData(pagesetData) {
        var jsonValue = JSON.stringify(pagesetData);
        let data = {
            "id": "loaderService.registerClass",
            "params": {
                "class": "JSONFileParser",
                "method": "writeJSON",
                "file_name": this._filePath,
                "value": jsonValue
            }
        };

        return new Promise((resolve, reject) => {
            http.post(this._apiPath, data).then((result) => {
                if (!result || !result.data || result.data.result != "ok") {
                    reject();
                } else {
                    this._pagesetList = pagesetData.list;
                    this.setPageset(this._pagesetId);
                    this._$bus.$emit(DCIMManager.UPDATE_PAGESET_LIST, this._pagesetList.concat());
                    resolve();
                }
            }, (result) => {
                console.log("Pageset JSON data read error", result);
                reject();
            });
        })
    }
}

var DCIMPagesetType = {
    SITE: "site",
    BUILDING: "building",
    FLOOR: "floor",
    ROOM: "room"
}