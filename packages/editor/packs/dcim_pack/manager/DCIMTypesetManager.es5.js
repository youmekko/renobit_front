"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DCIMTypesetManager =
      /*#__PURE__*/
      function () {
            function DCIMTypesetManager() {
                  _classCallCheck(this, DCIMTypesetManager);

                  // 자산 타입 그룹 리스트
                  this._assetTypeGroupList = []; // 자산 타입 그룹 이름 리스트

                  this._assetTypeGroupNameList = [];
            }

            _createClass(DCIMTypesetManager, [{
                  key: "startLoading",
                  value: function startLoading() {
                        return this.loadTypeList();
                  }
            }, {
                  key: "loadTypeList",
                  value: function loadTypeList() {
                        var _this = this;

                        var param = {
                              "id": "assetGroupService.getGroupList",
                              "params": {}
                        };
                        return wemb.$http.post(window.wemb.dcimManager.path, param).then(function (response) {
                              var data = response.data ? response.data.data : [];

                              _this.updateAssetTypeData(data);
                        }, function (error) {
                              _this.updateAssetTypeData([]);
                        });
                  }
            }, {
                  key: "updateAssetTypeData",
                  value: function updateAssetTypeData(list) {
                        var _this$_assetTypeGroup, _this$_assetTypeGroup2;

                        this._assetTypeGroupList.splice(0, this._assetTypeGroupList.length);

                        (_this$_assetTypeGroup = this._assetTypeGroupList).push.apply(_this$_assetTypeGroup, _toConsumableArray(list));

                        this._assetTypeGroupNameList.splice(0, this._assetTypeGroupNameList.length);

                        var groupNameList = this._assetTypeGroupList.map(function (item) {
                              return item.name;
                        });

                        (_this$_assetTypeGroup2 = this._assetTypeGroupNameList).push.apply(_this$_assetTypeGroup2, _toConsumableArray(groupNameList));
                  }
            }, {
                  key: "getAssetTypeGroupList",
                  value: function getAssetTypeGroupList() {
                        return this._assetTypeGroupList;
                  }
                  /*
				  자산 타입 그룹 리스트 구하기
				  */

            }, {
                  key: "getAssetTypeGroupData",
                  value: function getAssetTypeGroupData(groupName) {
                        if (this._assetTypeGroupList.length <= 0) {
                              return [];
                        }

                        var result = this._assetTypeGroupList.find(function (item) {
                              return item.name == groupName;
                        }); // 찾은 경우


                        if (result != null) {
                              return result.types;
                        } // 찾지 못한 경우


                        return [];
                  }
                  /*
				  타입그룹이름 목록 구하기
				  */

            }, {
                  key: "getAssetTypeGroupNameList",
                  value: function getAssetTypeGroupNameList() {
                        return this._assetTypeGroupNameList;
                  }
            }, {
                  key: "getAssetTypesByGroupName",
                  value: function getAssetTypesByGroupName(groupName) {
                        if (!this._assetTypeGroupList.length) {
                              return [];
                        }

                        var result = this._assetTypeGroupList.find(function (item) {
                              return item.name == groupName;
                        });

                        if (result != null) {
                              return result.types;
                        } // 찾지 못한 경우


                        return [];
                  }
                  /*
				  삭제
				  getAssetTypesByGroupId(groupId) {
				  if (!this._assetTypeGroupList.length) {
				  return [];
				  }
				  let result = this._assetTypeGroupList.find((item) => {
				  return item.id == groupId;
				  })
				  if (result != null) {
				  return result.types;
				  }
				  // 찾지 못한 경우
				  return [];
				  }*/

                  /*
				  첫 번째 그룹에 해당하는 데이터 구하기(타입 리스트)
				  call : AssetStateInLandComponent.updateTypeList()
				  */

            }, {
                  key: "getFirstAssetTypeGroupData",
                  value: function getFirstAssetTypeGroupData() {
                        if (this._assetTypeGroupList.length) {
                              return this._assetTypeGroupList[0].types;
                        }

                        return [];
                  }
            }]);

            return DCIMTypesetManager;
      }();
