class DCIMEventManager {
      constructor() {
            this._duration = DCIMManager.EVENT_POLLING_INTERVAL_TIME;
            this.timer = 0;
            this.pageTimer = 0;
            this.historyTimer = 0;
            this._$bus = new Vue();
            this._path = wemb.configManager.serverUrl + "/dcim.do";
            if(!wemb.configManager.isEditorMode) {
                  this.startEventPolling();
                  this.procEventBrowserData(this.severity);
            }
      }

      $on(event, func) {
            this._$bus.$on(event, func);
      }

      $off(event, func) {
            this._$bus.$off(event, func);
      }

      get $bus() {
            return this._$bus;
      }

      async ackEventData(item) {
            let param = {
                  "id": "assetEventService.updateEventData",
                  "params" : item
            };

            let res = await wemb.$http.post(this._path, param);
            if(res) {
                  return res.data;
            } else {
                  return null;
            }
      }

      async requestEventData(pageId) {
            let param = {
                  "id": "assetEventService.getEventList",
                  "params" : {pageIds: [pageId]}
            };
            let res = await wemb.$http.post(this._path, param);
            if(res) {
                  return res.data;
            } else {
                  return null;
            }
      }

      async requestEventBrowserData(severityList = null) {
            let param = {
                  "id": "assetEventService.getEventInitInfo",
                  "params" : {severity: severityList}
            };
            let res = await wemb.$http.post(this._path, param);
            if(res) {
                  return res.data;
            } else {
                  return null;
            }
      }

      async requestEventList(pageIds, typeIds) {
            let param = {
                  "id": "assetEventService.getEventInitInfo",
                  "params" : {pageIds: pageIds, typeIds: typeIds}
            };
            let res = await wemb.$http.post(this._path, param);
            if(res) {
                  return res.data;
            } else {
                  return null;
            }
      }

      async requestSeverityCount(pageIds, typeIds) {
            let param = {
                  "id": "assetEventService.getEventCount",
                  "params" : {pageIds: pageIds, typeIds: typeIds}
            };
            let res = await wemb.$http.post(this._path, param);
            if(res) {
                  return res.data;
            } else {
                  return null;
            }
      }

      async requestHistoryData(info) {
            let param = {
                  "id": "assetEventService.getHistoryData",
                  "params" : info
            };
            let res = await wemb.$http.post(this._path, param);
            if(res) {
                  return res.data;
            } else {
                  return null;
            }
      }

      startEventPolling() {
            clearTimeout(this.timer);
            var self = this;
            this.timer = setTimeout(function () {
                  // api 호출
                  self.procEventBrowserData();
            }, this._duration);
      }

      startPageEventPolling() {
            clearTimeout(this.pageTimer);
            var self = this;
            this.pageTimer = setTimeout(function () {
                  self.procPageEventData();
            }, this._duration);
      }

      startPollingHistoryData() {
            clearTimeout(this.historyTimer);
            var self = this;
            this.historyTimer = setTimeout(function () {
                  self.procHistoryData();
            }, this._duration);
      }

      /*
      2018.11.15(ckkim)
      DCIMManager에서 페이지가 열릴때마다 호출.
       */
      clearEventList() {
            clearTimeout(this.pageTimer);
            this.eventList = [];
            this.clearHistoryTimer();
      }

      clearHistoryTimer() {
            clearTimeout(this.historyTimer);
      }

      clearAllTimer() {
            clearTimeout(this.pageTimer);
            clearTimeout(this.timer);
            clearTimeout(this.historyTimer);
      }

      procAckEventData(data) {
            try {
                  this.ackEventData(data).then(()=>{
                        this.requestEventBrowserData(this.severity).then(res=>{
                              if(res) {
                                    var before = JSON.stringify(this.eventBrowserList);
                                    if (before != JSON.stringify(res.data)) {
                                          this.eventBrowserList = res.data;
                                          this._$bus.$emit(DCIMManager.GET_EVENT_LIST, this.eventBrowserList);
                                    }
                              }
                              this._$bus.$emit(DCIMManager.EVENT_COMPLETE_ACK, data);

                              this.startEventPolling();
                        });

                        this.procPageEventData();
                  })
            } catch(err) {
                  throw err
            }
      }

      procEventBrowserData(activeList) {
            try {
                  this.severity = activeList || this.severity;
                  this.requestEventBrowserData(this.severity).then(res=>{
                        if(res) {
                              var before = JSON.stringify(this.eventBrowserList);
                              if (before != JSON.stringify(res.data)) {
                                    this.eventBrowserList = res.data;
                                    this._$bus.$emit(DCIMManager.GET_EVENT_LIST, this.eventBrowserList);
                              }
                        }

                        this.startEventPolling();
                  });
            } catch (err) {
                  throw err;
            }
      }


      procPageEventData() {
            try {

                  this.requestEventData(window.wemb.pageManager.currentPageInfo.id).then(res=>{
                        if(res) {
                              var before = JSON.stringify(this.eventList);
                              if (before != JSON.stringify(res.data)) {
                                    this.eventList = res.data;
                                    this._$bus.$emit(DCIMManager.GET_ASSET_EVENT_LIST, this.eventList);
                              }
                        }

                        this.startPageEventPolling();
                  });
            } catch (err) {
                  throw err;
            }
      }




      procHistoryData() {
            try {
                  this.activeHistoryInfo.current = true;
                  this.requestHistoryData(this._activeHistoryInfo).then(res=>{
                        if(res) {
                              Object.keys(res.data).forEach(type => {
                                    let histList = res.data[type];
                                    wemb.dcimManager.assetAllocationProxy.assetCompositionInfoGroupMap.get(type).map(item=>{
                                          item.comInstance.changeNotification({type: DCIMManager.NOTI_CHANGE_EVENT_DATA , body:this.getAssetDataFromSource(histList, item.assetId)});
                                    });
                              })
                        }

                        this.startPollingHistoryData();
                  })
            } catch(err) {
                  throw err;
            }
      }


      getAssetDataFromSource(source, assetId ){
            let idx = source.findIndex((item)=>{ return item.id == assetId; });
            if(idx>-1){
                  return source.splice(idx,1)[0];
            }
            return assetId;
      }

      getDummyData() {
            let pages = wemb.pageManager.getPageInfoList().map(x => x.id);
            let typeList = ["FireSensor", "cctv", "pdu", "LeakDetectorSensor", "TempHumiSensor", "AccessSensor"];
            let severityList = ["normal", "critical", "warning", "major", "minor"];
            let dataLen = 20;
            let ary = [];
            for (let i = 0; i < dataLen; i++) {
                  // ary.push({
                  //       "severity": severityList[Math.floor(Math.random() * severityList.length)],
                  //       "evt_id": "eventId" + i,
                  //       "msg": "outside Back Gate guard Top - Motion detected. 00" + i,
                  //       "page_id": pages[Math.floor(Math.random() * pages.length)],
                  //       "asset_id": "TEST" + i,
                  //       "asset_type": typeList[Math.floor(Math.random() * typeList.length)],
                  //       "asset_name": "TEST" + i,
                  //       "stime": this.formatStringToDate(new Date(+(new Date()) - Math.floor(Math.random() * 10000000000))),
                  //       "dupl_cnt": i
                  // });
            }
            return ary;
      }


      _filterEventListByTypes(eventList, typeIds) {
            if (typeof typeIds == "string") {
                  return eventList.filter(x => x.asset_type == typeIds); //[]
            } else {
                  let result = {};
                  typeIds.forEach((typeId) => {
                        result[typeId] = eventList.filter(x => x.asset_type == typeId); //{"sensor": [], ..}
                  });

                  return result;
            }
      }

      filterEventList(list, pageIds, typeIds) {
            if (!list || !list.length) {
                  return null;
            }

            if (typeof pageIds == "string") {
                  /*  단일페이지
                      단일 타입일 경우
                      [{eventData}, {eventData}]
                      복수 타입일 경우
                      {
                            "sensor" : [{eventData}, {eventData}],
                            "cctv" : [{eventData}, {eventData}]
                      }*/
                  let filterPageEvents = list.filter(x => x.page_id == pageIds);
                  return this._filterEventListByTypes(filterPageEvents, typeIds);
            } else { //list
                  /* 복수 페이지,
                     단일 타입일경우
                         { pageId001": [{eventData}, {eventData}] }

                     복수 타입일경우
                        { "pageId001": {
                                    "sensor" : [{eventData}, {eventData}],
                                    "cctv" : [{eventData}, {eventData}]
                              }
                        }*/
                  let result = {};
                  pageIds.forEach((pageId) => {
                        let filterPageEvents = list.filter(x => x.page_id == pageId);
                        result[pageId] = this._filterEventListByTypes(filterPageEvents, typeIds);
                  })

                  return result;
            }
      }

      formatStringToDate(date) {

      }

      get eventBrowserList() {
            return this._eventBrowserList;
      }

      set eventBrowserList(list) {
            this._eventBrowserList = list;
      }

      set activeHistoryInfo(info) {
            this._activeHistoryInfo = info;
      }

      get activeHistoryInfo() {
            return this._activeHistoryInfo
      }


      /*
      2019. 02.14(ckkim)
      이벤트 리스트 정보를 즉시 업데이트 하는 기능
       */
      async syncPageEventData(){
            let result = this.requestEventData(window.wemb.pageManager.currentPageInfo.id);
            const [error, res] = await CPUtil.getInstance().destructAssignmentPromise(result);
            if(error){
                  console.log("error", error );
                  return null;
            }
            if(res){
                  this.eventList = res.data;
            }

            return this.eventList;

      }



}
