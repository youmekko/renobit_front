"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * 페이지셋에서 편집한 데이터를 API로 조회할 수 있는 Manager
 */
var DCIMPagesetManager =
      /*#__PURE__*/
      function () {
            function DCIMPagesetManager() {
                  _classCallCheck(this, DCIMPagesetManager);

                  this._apiPath = wemb.configManager.serverUrl + "/loader.do";
                  this._filePath = "output/dcim/pageset.json";
                  this._pagesetList = [];
                  this._pagesetId = '';
                  this._$bus = new Vue();
                  this._buildings = null;
                  this._pageset = null;
                  this._pagesetFlatData = [];
            } //부지정보


            _createClass(DCIMPagesetManager, [{
                  key: "getSite",
                  value: function getSite() {
                        var site = null;

                        if (this._pageset) {
                              site = this._pageset.site;
                        }

                        return site;
                  } //설정된 페이지셋 정보

            }, {
                  key: "getPageset",
                  value: function getPageset() {
                        return this._pageset;
                  } //빌딩 리스트

            }, {
                  key: "getPagesetBuildings",
                  value: function getPagesetBuildings() {
                        return this._buildings || [];
                  }
            }, {
                  key: "$on",
                  value: function $on(event, func) {
                        this._$bus.$on(event, func);
                  }
            }, {
                  key: "$off",
                  value: function $off(event, func) {
                        this._$bus.$off(event, func);
                  }
            }, {
                  key: "startLoading",
                  value: function startLoading() {
                        return this.loadPagesetData();
                  }
            }, {
                  key: "getAllPageIds",
                  value: function getAllPageIds() {
                        var ids = [];

                        if (this._pagesetFlatData) {
                              ids = this._pagesetFlatData.map(function (x) {
                                    return x.id;
                              });
                        }

                        return ids;
                  } //전달 받은 인자(층/룸)의 상위 빌딩

            }, {
                  key: "getParentBuilding",
                  value: function getParentBuilding(id) {
                        var building = this.getParents(id).find(function (x) {
                              return x.type == DCIMPagesetType.BUILDING;
                        });

                        if (building) {
                              return $.extend(true, {}, building);
                        } else {
                              return null;
                        }
                  }
            }, {
                  key: "_getParents",
                  value: function _getParents(tempList, id) {
                        var findItem = this._pagesetFlatData.find(function (x) {
                              return x.id == id;
                        });

                        if (!findItem) {
                              return;
                        }

                        var parentItem = this._pagesetFlatData.find(function (x) {
                              return x.id == findItem.parent;
                        });

                        if (parentItem) {
                              tempList.unshift(parentItem);

                              if (parentItem.parent) {
                                    this._getParents(tempList, parentItem.id);
                              }
                        }
                  } //전달받은 인자 항목의 조상 정보 가져오기

            }, {
                  key: "getParents",
                  value: function getParents(id) {
                        var parents = [];

                        var findItem = this._pagesetFlatData.find(function (x) {
                              return id == x.id;
                        });

                        if (findItem) {
                              parents = [findItem];

                              this._getParents(parents, findItem.id);
                        }

                        return parents;
                  } //전달받은 pageId의 breadcrumbs정보 반환

            }, {
                  key: "getPageBreadcrumbs",
                  value: function getPageBreadcrumbs(pageId) {
                        var parents = this.getParents(pageId);
                        return parents.map(function (x) {
                              return {
                                    name: x.name,
                                    id: x.id
                              };
                        });
                  }
            }, {
                  key: "getFloorRoomIds",
                  //전달받은 building의 층/룸 Id배열 반환
                  value: function getFloorRoomIds(building) {
                        var ids = [];
                        var floors = building.children || [];
                        floors.forEach(function (floor) {
                              ids.push(floor.id);
                              var rooms = floor.children;

                              if (rooms) {
                                    rooms.forEach(function (room) {
                                          ids.push(room.id);
                                    });
                              }
                        });
                        return ids;
                  } //첫번째 빌딩의 층 리스트 반환

            }, {
                  key: "getFirstBuildiingFloorList",
                  value: function getFirstBuildiingFloorList() {
                        var buildings = this.getPagesetBuildings();

                        if (buildings.length) {
                              return buildings[0].children || [];
                        }

                        return [];
                  } //전달받은 빌딩Id의 층 리스트 반환

            }, {
                  key: "getFloorList",
                  value: function getFloorList(buildingId) {
                        var buildings = this.getPagesetBuildings();
                        var findBuilding = buildings.find(function (x) {
                              return x.id == buildingId;
                        });

                        if (findBuilding) {
                              return findBuilding.children || [];
                        }

                        return [];
                  } //전달받은 페이지셋 ID의 페이지셋 반환

            }, {
                  key: "getPagesetById",
                  value: function getPagesetById(id) {
                        if (this.pagesetList && this.pagesetList.find(function (x) {
                              return x.id == id;
                        })) {
                              return this.pagesetList.find(function (x) {
                                    return x.id == id;
                              });
                        } else {
                              console.warn("No Pageset");
                              return null;
                        }
                  } //전달받은 페이지셋 이름의 페이지셋 반환

            }, {
                  key: "getPagesetByName",
                  value: function getPagesetByName(name) {
                        if (this.pagesetList && this.pagesetList.find(function (x) {
                              return x.name == name;
                        })) {
                              return this.pagesetList.find(function (x) {
                                    return x.name == name;
                              });
                        } else {
                              console.warn("No Pageset");
                              return null;
                        }
                  }
            }, {
                  key: "loadPagesetData",
                  value: function loadPagesetData() {
                        var _this = this;

                        // return new Promise((resolve, reject) => {
                        var data = {
                              "id": "loaderService.registerClass",
                              "params": {
                                    "class": "JSONFileParser",
                                    "method": "readJSON",
                                    "file_name": this._filePath
                              }
                        };
                        return http.post(this._apiPath, data).then(function (result) {
                              if (!result || !result.data || !result.data.data) {//reject(result);
                              } else {
                                    var pageData = JSON.parse(result.data.data);
                                    _this._pagesetList = pageData.list || [];

                                    _this.setPageset(pageData.pagesetId);

                                    _this._$bus.$emit(DCIMManager.UPDATE_PAGESET_LIST, _this._pagesetList.concat()); //resolve(result);

                              }
                        }, function (result) {
                              console.log("Pageset JSON data read error", result);
                        });
                  } //페이지셋 리스트에서 전달받은 ID를 페이지셋으로 설정

            }, {
                  key: "setPageset",
                  value: function setPageset(id) {
                        if (id && this._pagesetList.length) {
                              try {
                                    var findItem = $.extend(true, {}, this._pagesetList.find(function (x) {
                                          return x.id == id;
                                    }));
                                    this._pagesetId = id;
                                    var site = findItem.site;
                                    var structure = findItem.structure;

                                    if (site) {
                                          findItem.structure = [{
                                                id: site.id,
                                                name: site.name,
                                                children: structure
                                          }];
                                    }

                                    this._pageset = findItem;
                                    this._buildings = structure;

                                    this._setPageFlatData(findItem);
                              } catch (error) {
                                    this.emptyPagesetData();
                              }
                        } else {
                              this.emptyPagesetData();
                        }
                  } //페이지셋 데이터를 트리구조에서 flat하게 변경

            }, {
                  key: "_setPageFlatData",
                  value: function _setPageFlatData(pagesetData) {
                        var _this2 = this;

                        this._pagesetFlatData = [];
                        var site = pagesetData.site;

                        if (site) {
                              site.type = DCIMPagesetType.SITE;
                        }

                        this._pagesetFlatData.push(site);

                        var buildings = this._buildings || [];
                        buildings.forEach(function (building) {
                              building.type = DCIMPagesetType.BUILDING;
                              building.parent = site ? site.id : '';

                              _this2.syncName(building);

                              _this2._pagesetFlatData.push(building);

                              var floors = building.children || [];
                              floors.forEach(function (floor) {
                                    floor.type = DCIMPagesetType.FLOOR;
                                    floor.parent = building.id;

                                    _this2.syncName(floor);

                                    _this2._pagesetFlatData.push(floor);

                                    var rooms = floor.children || [];
                                    rooms.forEach(function (room) {
                                          room.type = DCIMPagesetType.ROOM;
                                          room.parent = floor.id;

                                          _this2.syncName(room);

                                          _this2._pagesetFlatData.push(room);
                                    });
                              });
                        });
                  } //페이지셋 항목 라벨 이름을 페이지 이름을 조회하여 sync함

            }, {
                  key: "syncName",
                  value: function syncName(item) {
                        var pageName = window.wemb.pageManager.getPageNameById(item.id);
                        item.page_name = pageName;

                        if (item.sync_page_name) {
                              item.name = pageName || item.name;
                        }
                  }
            }, {
                  key: "emptyPagesetData",
                  value: function emptyPagesetData() {
                        this._pageset = null;
                        this._buildings = null;
                        this._pagesetId = '';
                  }
            }, {
                  key: "updatePagesetData",
                  value: function updatePagesetData(pagesetData) {
                        var _this3 = this;

                        var jsonValue = JSON.stringify(pagesetData);
                        var data = {
                              "id": "loaderService.registerClass",
                              "params": {
                                    "class": "JSONFileParser",
                                    "method": "writeJSON",
                                    "file_name": this._filePath,
                                    "value": jsonValue
                              }
                        };
                        return new Promise(function (resolve, reject) {
                              http.post(_this3._apiPath, data).then(function (result) {
                                    if (!result || !result.data || result.data.result != "ok") {
                                          reject();
                                    } else {
                                          _this3._pagesetList = pagesetData.list;

                                          _this3.setPageset(_this3._pagesetId);

                                          _this3._$bus.$emit(DCIMManager.UPDATE_PAGESET_LIST, _this3._pagesetList.concat());

                                          resolve();
                                    }
                              }, function (result) {
                                    console.log("Pageset JSON data read error", result);
                                    reject();
                              });
                        });
                  }
            }, {
                  key: "$bus",
                  get: function get() {
                        return this._$bus;
                  }
            }, {
                  key: "pagesetId",
                  get: function get() {
                        return this._pagesetId;
                  } ////페이지셋 전체 리스트

            }, {
                  key: "pagesetList",
                  get: function get() {
                        return this._pagesetList;
                  },
                  set: function set(list) {
                        var _this$_pagesetList;

                        this._pagesetList.splice(0, this._pagesetList.length);

                        (_this$_pagesetList = this._pagesetList).push.apply(_this$_pagesetList, _toConsumableArray(list));
                  }
            }]);

            return DCIMPagesetManager;
      }();

var DCIMPagesetType = {
      SITE: "site",
      BUILDING: "building",
      FLOOR: "floor",
      ROOM: "room"
};
