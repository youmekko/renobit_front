/*
plugin은 모두 mediator의 view로 등록됨.
 */

class DCIMManager extends ExtensionPluginCore {
    constructor() {
        super();

        this._path = wemb.configManager.serverUrl + "/dcim.do";
        this._eventManager = new DCIMEventManager();
        this._pagesetManager = new DCIMPagesetManager();
        this._typesetManager = new DCIMTypesetManager();
        this._mountDataManager = new DCIMMountDataManager();

        this._assetComponentProxy = null;
        this._allocationProxy = null;

        this._lang = Vue.$i18n.messages.wv.dcim_pack;

        this._$bus = new Vue();
    }

    /**
     * Viewer전용
     * 통합자산검색 팝업 호출
     */
    openIntergratedAssetSearch() {
        if (window.wemb.configManager.exeMode == "viewer") {
            window.wemb.showExternalModal(
                window.ViewerStatic.CMD_SHOW_MODAL,
                  window.wemb.configManager.context + "/custom/packs/dcim_pack/manager/view/search/DCIMUnifiedSearchMainComponent.vue");
        }
    }

    start() {
        window.wemb.dcimManager = DCIMManager._instance;

        /*
		에디터 모드에서만 템플릿 등록하기
		 */
        if (wemb.configManager.exeMode == "editor") {
            DCIMComponentPropertyTemplateRegister.regist();
        }

    }


    setFacade(facade) {
        super.setFacade(facade);

        // Renobit Puremvc proxy 등록하기
        this._assetComponentProxy = new AssetComponentProxy(AssetComponentProxy.NAME);
        facade.registerProxy(this._assetComponentProxy);

        facade.registerProxy(new AssetComponentInfoPlacedPageProxy(this._assetComponentProxy));

        this._allocationProxy = new AssetAllocationProxy(AssetAllocationProxy.NAME);
        facade.registerProxy(this._allocationProxy);

        /*
		  페이지가 최초 열릴때
		   */
        // 내부에서 사용하는 proxy 사용하기
        this._allocationProxy.attachProxy();

    }

    get $bus() {
        return this._$bus;
    }

    $on(eventName, data) {
        return this._$bus.$on(eventName, data);
    }

    get mountDataManager() {
        return this._mountDataManager;
    }

    get eventManager() {
        return this._eventManager;
    }

    get pagesetManager() {
        return this._pagesetManager;
    }

    get typesetManager() {
        return this._typesetManager;
    }

    get assetManager() {
        return this._assetComponentProxy;
    }

    get path() {
          return this._path;
    }


    /*
	팝업 호출, 카메라 이동 처리를 담당하는 플러그인.
	 */
    get actionController() {
          return window.wemb.assetComponentControllerPlugin;
    }


    /*
	2018.11.12(ckkim)
	현재 열려있는 페이지의 자산+컴포넌트 정보
	 */
    get assetAllocationProxy() {
        return this._allocationProxy;
    }


    destroy() {}

    get notificationList() {
        return [

            EditorProxy.NOTI_UPDATE_EDITOR_STATE,
            ViewerProxy.NOTI_UPDATE_VIEWER_STATE,
            ViewerProxy.NOTI_OPENED_PAGE,
        ]
    }


    /*
	  noti가 온 경우 실행
	  call : mediator에서 실행
	   */
    handleNotification(note) {
        switch (note.name) {

            case ViewerProxy.NOTI_UPDATE_VIEWER_STATE:
            case EditorProxy.NOTI_UPDATE_EDITOR_STATE:
                this._register_HookAction(note.getBody());
                break;


                // 편집 페이지가 열릴때마다 실행
            case ViewerProxy.NOTI_OPENED_PAGE:
                this.noti_openedPage();
                break;
        }
    }




    async _register_HookAction(pageLoadingState) {
        // 모든 정보가 로드된 상태.
        if (pageLoadingState == "readyCompleted") {

            /*
			 2018.11.21(ckkim)
			 중요!!!
			 - 저장 훅 이벤트 등록하기

			*/

            /*
			페이지 정보를 로드하기 전
			 */
            wemb.hookManager.addAction(HookManager.HOOK_BEFORE_LOAD_PAGE, this._hookBeforeLoadPage.bind(this));

            // 페이지 정보를 로드 완료 한 후 실행, 컴포넌트 인스턴스가 만들어지기 전
            wemb.hookManager.addAction(HookManager.HOOK_AFTER_LOAD_PAGE, this._hookAfterLoadPage.bind(this));


            wemb.hookManager.addAction(HookManager.HOOK_BEFORE_ADD_COMPONENT_INSTANCE, this._hookBeforeAddComponentInstance.bind(this));

            // 페이지 로딩될때
            try {
                await this._typesetManager.startLoading();
                await this._pagesetManager.startLoading();
                return true;
            } catch (error) {
                console.log("@@error, DCIMManager._register_HookAction ", error);
                return false;
            }


        }
    }

    async _hookBeforeLoadPage() {
        try {
            await this._assetComponentProxy.loadAssetsData();
            await this._assetComponentProxy.loadAssetComponentsInfo();
            return true;
        } catch (error) {
            console.log("@@ error AssetComponentPlugin loadAssetsData ", error);
            return false;
        }
    }


    /*
	 컴포넌트가 만들어지기 컴포넌트 정보에 자산 정보 추가하기
	*/

    async _hookAfterLoadPage(comInstanceMetaInfoList) {
        comInstanceMetaInfoList.forEach((comInstanceMetaInfo) => {
            this.injectAssetDataToComponentInfo(comInstanceMetaInfo);
        })

        return true;
    }


    /*
	컴포넌트 중 자산 컴포넌트인 경우
		- 자산 정보 + 자산 컴포넌트 인 경우 = 컴포넌트에 자산 정보에 환경설정 정보를 추가
		- 자산 컴포넌트 인 경우 = 컴포넌트에 환경설정 정보만 추가
	 */
    async _hookBeforeAddComponentInstance(comInstanceMetaInfo) {

        // 자산 패널에서 자산이 drag_drop되는 경우 실행
        if (wemb.globalStore.has("__drag_asset_data__") == true) {
            let assetData = wemb.globalStore.get("__drag_asset_data__");
            this.injectAssetDataToComponentInfo(comInstanceMetaInfo, assetData);
            wemb.globalStore.clear("__drag_asset_data__");
        } else {
            // 일반 3D 자산 컴포넌트인 경우 환경설정 정보를 적용.
            this._assetComponentProxy.attachComponentConfigInfoToComInstanceInfo(comInstanceMetaInfo);
        }


        return true;
    }

    /*
	컴포넌트 정보에 자산 정보 설정하기

	1. 컴포넌트 정보가 3D 컴포넌트만
	2. 컴포넌트 정보가 자산 컴포넌트인 경우만
	3. 컴포넌트 id를 가지고 컴포넌트 환경설정 정보 구하기
	4. 컴포넌트 정보에 기본 팝업 id  설정하기
	5. 컴포넌트 정보에 리소스 정보 설정하기
	6. 자산 정보가 존재하는 경우

	 */
    injectAssetDataToComponentInfo(comInstanceInfo, data = null) {

        // 컴포넌트가 만들지기 전, 인스턴스 정보에 환경설정 정보 추가
        this._assetComponentProxy.attachComponentConfigInfoToComInstanceInfo(comInstanceInfo);


        /*
		경우01: 자산 정보를 인스턴스 정보에 추가하기
		*/
        if (data) {
            this._assetComponentProxy.attachAssetDataToComInstanceInfo(data, comInstanceInfo);
        } else {
            /*
            경우02: 컴포넌트에 이미 바인딩되어 있는 자산 정보로 설정하기
             */

            this._assetComponentProxy.attachBindingAssetDataToComInstanceInfo(comInstanceInfo);
        }


    }


    initProperties() {}



    /*
	    pageName에서  자산 정보 구해오기

	    1. 읽어들일 페이지 이름으로 페이지 id 구하기
	    2. 서버에서 페이지 정보 구하기
	    3. 레이어 필터링 처리하기
	    4. 자산 타입 필터링 처리하기
	- 자산 컴포넌트가 아닌 경우는 모두 추가
	- 자산 컴포넌트 중 options.includeTypes에 해당하는 자산 컴포넌트 만 추가
	*/
    async getAssetDataInPage(pageName, options) {
        let assetComponentInfoPlacedPageProxy = this._facade.retrieveProxy(AssetComponentInfoPlacedPageProxy.NAME);
        return await assetComponentInfoPlacedPageProxy.getAssetDataInPage(pageName, options);
    }

    /*

	 pageNameList에 들어 있는 페이지 리스트별로
	 자산 정보 중  출입 센서 정보만을 가져오기
	 @pageNameList : 페이지이름 리스트, 페이지 ID가 아님.
	 */
    async getAccessSensor3DModelDataInFloorPage(pageNameList) {
        let assetComponentInfoPlacedPageProxy = this._facade.retrieveProxy(AssetComponentInfoPlacedPageProxy.NAME);

        if (Array.isArray(pageNameList) == false) {
            console.warn("RENOBIT The parameter value is not an array.")
            return Promise.resolve([]);
        }


        return new Promise(async(resolve, reject) => {
            let result = [];
            for (let i = 0; i < pageNameList.length; i++) {
                let pageName = pageNameList[i];
                let info = {
                    pageName: pageName,
                    data: []
                }

                info.data = await assetComponentInfoPlacedPageProxy.getAccessSensor3DModelDataInFloorPage(pageName);
                result.push(info);
            }

            resolve(result);
        })

    }

    /*
	    2018.11.09(ckkim)
	    데이터 refresh

	    call : AssetComponentPanel에서  refresh() 버튼 시 호출


	    1. 컴포넌트 환경설정 정보 읽기

	    2. 자산 정보 읽기
		  2-1. 타입 정보 읽기
		  await dcimAssetProxy.loadAssetTypes();
		  2-2. 자산 정보 읽기
		  await dcimAssetProxy.loadAssetsData();
		  2-3. 컴포넌트 타입 정보 읽기
		  await dcimAssetProxy.loadAssetComponentsInfo();

	    3. 배치된 합성자산정보(자산 정보+컴포넌트 정보) 생성.
		  3-1. NOTI_UPDATE_ASSET_ALLOCATION_LIST 전송
			(컴포넌트 업데이트)

	    4. 중복 체크

	    5. 배치된 자산 컴포넌트에 환경설정 기본 정보 적용하기
		  - popup Id (config 정보)
		  - assetData (자산 정보)
		  - resource (config 정보)



	 */

    async refreshAssetAllData() {

        try {
            //  1. 컴포넌트 환경설정 다시 정보 읽기
            //await wemb.componentLibraryManager.getComponentPanelInfoList(true);
            await this._assetComponentProxy.loadAssetComponentsInfo();

            //3. 배치된 합성자산정보(자산 정보+컴포넌트 정보) 생성.
            var assetAllocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);




            // 2. 자산 정보 읽기
            let assetComponentProxy = this._facade.retrieveProxy(AssetComponentProxy.NAME);
            await assetComponentProxy.loadAssetsData();


            //3. 중복 배치 체크
            var checked = AssetPagePlugin._instance.executeCheckingDuplicatedAsset();

            /*
			1.싱크 전 자산 정보에서 현재 페이지에 배치된 내용을 모두 지운다.
			2. 맵핑 정보를 신규 자산 정보에 적용한다.
			3. 자산 복합 정보(compositionInfo)를 신규로 만든다.
			 */
            assetAllocationProxy.syncAssetCompositionInfoList();




            //5. 배치된 자산 컴포넌트에 환경설정 기본 정보 적용하기
            assetAllocationProxy.attachDefaultAssetComponentInfo();


            if (checked == false)
                Vue.$message(this._lang.manager.sync);


        } catch (error) {
            console.error((new Date()).getTime().toString(), "ERROR", "DCIMManager refreshAssetAllData ", error);
        }
    }


    noti_openedPage() {
        this._eventManager.clearEventList();
        if (!wemb.configManager.isEditorMode) {
            this._eventManager.procPageEventData();
        }

        /*
		자산 정보 업데이트
		 */

        // 업데이트 처리(NOTI_UPDATE_ASSET_ALLOCATION_LIST 전송)
        this._allocationProxy.updateAssetAllocationList();

    }


    /**
     * 연관자산정보 가져오기
     * */
    getRelationData(parent_id) {
          let param = {
                "id": "assetRelationService.getRelation",
                "params" : {parent_id: parent_id}
          };

          return new Promise((resolve)=> {
            window.wemb.$http.post(this.path, param).then(res => {
                resolve([null, res]);
            }).catch(error => {
                resolve([error, null]);
            });
        })
    }

    /**
     * 연관자산 저장
     * */
    postRelationData(params) {
          let param = {
                "id": "assetRelationService.setRelation",
                "params" : params
          };
        return new Promise((resolve) => {
            window.wemb.$http.post(this.path, param).then(function(response) {
                resolve([null, response]);
            }).catch(function(error) {
                resolve([error, null]);
            });
        });
    }

    /**
     * 자산 정보 업데이트 요청
     * */
    updateAsset(params) {
        var encTypes = window.wemb.dcimManager._assetComponentProxy.fields.type[params.asset_type].filter(function(asset){
            return asset.encryption;
        });
        var encNames = encTypes.map(function(type){
                    return type.name;	
        });
        var paramKey = Object.getOwnPropertyNames(params);
        for(var i = 0; encNames.length > 0 && i < paramKey.length; i ++){
            if(encNames.indexOf(paramKey[i]) > -1){
                    params[paramKey[i]] = Base64.encode(params[paramKey[i]]);
            }
        }


          let param = {
                "id": "assetDataService.updateData",
                "params" : params
          };

        return new Promise((resolve, reject) => {
            window.wemb.$http.post(this.path, param).then(function(response) {
                resolve([null, response]);
            }).catch((error) => {
                resolve([error, null]);
            });
        });
    }


    /*
	assetId에 해당하는 자산 정보 업데이트 처리

	1. 자산 정보 업데이트
	2. 컴포넌트에 신규 자산 정보 업데이트 처리

	call : 외부에서, 특히 자산 팝업등에서 자산 정보가 DB에 쌓이는 경우 처리
	*/
    async updateAssetDataBy(targetAssetData) {

        if (!targetAssetData)
            return false;

        let assetId = null;
        let assetType = null;

        try {
            assetId = targetAssetData.id;
            assetType = targetAssetData.asset_type;
        } catch (error) {
            return false;
        }



        // 1. 자산 정보 업데이트
        let assetData = await this.assetManager.updateAssetDataBy(assetId, assetType);

        if (assetData == false)
            return false;


        // 2. 컴포넌트에 신규 자산 정보 업데이트 처리
        let assetCompositionInfo = this.assetAllocationProxy.assetCompositionInfoMap.get(assetId);
        if (!assetCompositionInfo)
            return false;


        // 업데이트 메시지
        assetCompositionInfo.comInstance.changeNotification({
            type: DCIMManager.NOTI_CHANGE_ASSET_DATA,
            body: assetData
        });

        return true;

    }

      async saveRackMountData() {
            let data = this.getCurrentEditData();
            let param = {
                  "id": "assetDataService.getMount",
                  "params" : data
            };

            let result = await wemb.$http.post(window.wemb.dcimManager.path, param);
            if (result) {
                  Vue.$message.success(this.lang_common.successSave);
                  this.saveLocalData(data);
                  this.refresh();
            } else {
                  Vue.$message.error(this.lang_common.errorSave);
            }
      }
}

DCIMManager.API = {
    ASSET_RELATION: "/asset/relation",
    ASSET_UPDATE: "/asset/update"
};


DCIMManager.GET_EVENT_LIST = '/event/list'; // 이벤트 브라우저
DCIMManager.GET_ASSET_EVENT_LIST = '/event/data/type';
DCIMManager.CHANGE_EVENT_LIST = '/event/data';
DCIMManager.UPDATE_PAGESET_LIST = '/event/updatePagesetList';
DCIMManager.EVENT_COMPLETE_ACK = '/event/ack/complete';
DCIMManager.EVENT_POLLING_INTERVAL_TIME = 5000;


DCIMManager.NOTI_CHANGE_SEVERITY = "/notification/changeSeverity";
DCIMManager.NOTI_CHANGE_PROPS_DATA = "/notification/changePropsData";
DCIMManager.NOTI_CHANGE_ASSET_DATA = "/notification/changeAssetData";
DCIMManager.NOTI_CHANGE_EVENT_DATA = "/notification/changeEventData";
DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION = "/notification/selectInitCameraPosition";

DCIMManager.PACK_PATH = "custom/packs/dcim_pack/components/2D/";
