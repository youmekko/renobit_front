class DCIMTypesetManager {
      constructor() {
            // 자산 타입 그룹 리스트
            this._assetTypeGroupList = [];
            // 자산 타입 그룹 이름 리스트
            this._assetTypeGroupNameList = [];
      }

      startLoading() {
            return this.loadTypeList();
      }

      loadTypeList() {
            let param = {
                  "id": "assetGroupService.getGroupList",
                  "params": {}
            };

            return wemb.$http.post(window.wemb.dcimManager.path, param).then((response) => {
                  let data = response.data ? response.data.data : [];
                  this.updateAssetTypeData(data);
            }, (error) => {
                  this.updateAssetTypeData([]);
            })
      }

      updateAssetTypeData(list) {
            this._assetTypeGroupList.splice(0, this._assetTypeGroupList.length);
            this._assetTypeGroupList.push(...list);

            this._assetTypeGroupNameList.splice(0, this._assetTypeGroupNameList.length);
            let groupNameList = this._assetTypeGroupList.map((item) => {
                  return item.name;
            });
            this._assetTypeGroupNameList.push(...groupNameList);
      }

      getAssetTypeGroupList() {
            return this._assetTypeGroupList;
      }


      /*
		   자산 타입 그룹 리스트 구하기
		 */
      getAssetTypeGroupData(groupName) {
            if (this._assetTypeGroupList.length <= 0) {
                  return [];
            }

            let result = this._assetTypeGroupList.find((item) => {
                  return item.name == groupName;
            })

            // 찾은 경우
            if (result != null) {
                  return result.types;
            }


            // 찾지 못한 경우
            return [];
      }

      /*
	  타입그룹이름 목록 구하기
	   */
      getAssetTypeGroupNameList() {
            return this._assetTypeGroupNameList;
      }

      getAssetTypesByGroupName(groupName) {
            if (!this._assetTypeGroupList.length) {
                  return [];
            }

            let result = this._assetTypeGroupList.find((item) => {
                  return item.name == groupName;
            })

            if (result != null) {
                  return result.types;
            }

            // 찾지 못한 경우
            return [];
      }

      /*
	  삭제
	  getAssetTypesByGroupId(groupId) {
		  if (!this._assetTypeGroupList.length) {
			  return [];
		  }

		  let result = this._assetTypeGroupList.find((item) => {
			  return item.id == groupId;
		  })

		  if (result != null) {
			  return result.types;
		  }

		  // 찾지 못한 경우
		  return [];
	  }*/

      /*
	  첫 번째 그룹에 해당하는 데이터 구하기(타입 리스트)
	  call : AssetStateInLandComponent.updateTypeList()
	   */
      getFirstAssetTypeGroupData() {
            if (this._assetTypeGroupList.length) {
                  return this._assetTypeGroupList[0].types;
            }
            return [];
      }
}
