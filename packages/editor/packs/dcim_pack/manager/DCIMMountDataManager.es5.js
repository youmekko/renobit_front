"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * +DCIMMountDataManager 3D 랙 컴포넌트와 2D컴포넌트 랙 preview 컴포넌트에서 랙 실장 정보를 조회할때 사용한다.
 * 랙 자산 ID를 전달하면 실장 정보를 조회하여 반환해준다. 반환 정보 포맷은 아래와 같음
 * { items: 실장항목,
      rows: 쉘프 번호 기준 실장항목,(조회 편의성을 위해)
      error: 오류 항목
   };

 * +랙실장 관리자에서 유효성 체크 등을 해주는 기능을 한다.
 *
 */
var DCIMMountDataManager =
      /*#__PURE__*/
      function () {
            function DCIMMountDataManager() {
                  _classCallCheck(this, DCIMMountDataManager);

                  this._$bus = new Vue();
                  this._rackMountDataValidator = new RackMountDataValidator();
            }

            _createClass(DCIMMountDataManager, [{
                  key: "$on",
                  value: function $on(eventName, data) {
                        return this._$bus.$on(eventName, data);
                  }
            }, {
                  key: "getMountAssetTypes",

                  /**
                   * 랙실장 관리자에서 랙에 실장가능한 자산 타입을 조회하기 위한 메소드
                   * 설정/저장은 자산관리자>자산 컴포넌트 환경설정에서 함
                   */
                  value: function getMountAssetTypes(typeId) {
                        //typeid == component id 추후 type으로 변경해야함..
                        var ary = [];

                        try {
                              var types = window.wemb.dcimManager.assetManager.asset3DTypeInfoMap.get(typeId).props.initProperties.props.resource.units;
                              $.each(types, function (type) {
                                    var item = window.wemb.dcimManager.assetManager.assetTypes.find(function (item) {
                                          return item.id == type;
                                    });

                                    if (item) {
                                          ary.push({
                                                id: item.id,
                                                name: item.name
                                          });
                                    }
                              });
                        } catch (error) {
                              console.error(error);
                        }

                        return ary;
                  }
                  /**
                   * 랙실장 정보 요청시 이 메소드를 호출한다.
                   */

            }, {
                  key: "getMountDataByRackId",
                  value: function getMountDataByRackId(rackId) {
                        var maxUnitLen = 99;
                        var minUnitLen = 10;
                        this._rackMountData = window.dummyRackMountData;
                        var rack = window.wemb.dcimManager.assetManager.getAssetData(rackId); //let rack = { id: "rack01", name: "rack01", unit_size: 40, order_type: "asec" }

                        if (rack && rack.unit_size && rack.order_type) {
                              var currentRackMount = $.extend(true, [], window.wemb.dcimManager.assetManager.getAssetChildren(rackId));
                              var unitDirBT = rack.order_type.trim().toLowerCase() == 'desc' ? true : false;
                              var unitLen = parseInt(rack.unit_size) || minUnitLen;
                              unitLen = Math.min(maxUnitLen, Math.max(unitLen, minUnitLen));
                              var mounts = this.rackMountDataValidator.validate(rack.unit_size, unitDirBT, currentRackMount);
                              return {
                                    unitDirBT: unitDirBT,
                                    unitLen: unitLen,
                                    mounts: mounts
                              };
                        } else {
                              return null;
                        }
                  }
            }, {
                  key: "buildRackInnerResourceData",
                  value: function buildRackInnerResourceData(info, unitParams) {
                        var product = {};
                        product.rotateY = info.mountData.rotate; //유닛 회전 여부

                        product.name = info.name; //유닛 이름

                        product.type = info.asset_type; //유닛 타입

                        product.rowIndex = +info.mountData.row_index; //유닛 로우 정보

                        product.spaceX = +info.mountData.left; //유닛 가로 공백

                        product.scaleX = +info.mountData.width; //유닛 가로 크기

                        var unitScaleY = +info.mountData.row_weight; //유닛 세로 크기

                        product.scaleY = unitScaleY;
                        var hasFixUnit = unitScaleY == 1 || unitScaleY == 2 || unitScaleY == 4 || unitScaleY == 10 || unitScaleY == 20;

                        if (!hasFixUnit) {
                              if (product.scaleY >= 20) {
                                    unitScaleY = 20;
                              } else if (product.scaleY >= 10 && product.scaleY < 20) {
                                    unitScaleY = 10;
                              } else if (product.scaleY >= 4 && product.scaleY < 10) {
                                    unitScaleY = 4;
                              } else {
                                    unitScaleY = 1;
                              }
                        }
                        /*실장 데이터의 타입 정보가 있어야 세부 모델링을 data를 통해 결정할 수 있음*/


                        var unitTypeResource = unitParams[product.type];
                        var unitModeling = unitTypeResource.modeling;
                        var key = unitTypeResource.mappingKey;

                        if (key != "" && info.hasOwnProperty(key)) {
                              key = info[key];
                        } else {
                              key = "default";
                        } // 매칭되는 모델링 정보 추출 혹 키에 리소스가 없다면 기본 리소스 사용.


                        var resourceInfo = unitModeling[key] || unitModeling["default"];

                        function findMatchingResource(source, searchScale) {
                              var results = source.find(function (resource) {
                                    var path = resource.path;
                                    var name = path.substring(path.lastIndexOf("/") + 1);
                                    return name.indexOf(searchScale.toString()) > -1;
                              });
                              return results;
                        }

                        var selectItem = findMatchingResource(resourceInfo, unitScaleY);

                        if (selectItem == undefined) {
                              unitScaleY = 1;
                              selectItem = findMatchingResource(resourceInfo, unitScaleY);
                        }

                        return new Promise(
                              /*#__PURE__*/
                              function () {
                                    var _ref = _asyncToGenerator(
                                          /*#__PURE__*/
                                          regeneratorRuntime.mark(function _callee(resolve) {
                                                var hasUnit;
                                                return regeneratorRuntime.wrap(function _callee$(_context) {
                                                      while (1) {
                                                            switch (_context.prev = _context.next) {
                                                                  case 0:
                                                                        product.origin = info;
                                                                        product.id = info.id;

                                                                        if (!selectItem) {
                                                                              _context.next = 19;
                                                                              break;
                                                                        }

                                                                        _context.prev = 3;
                                                                        hasUnit = NLoaderManager.hasLoaderPool(selectItem.name);

                                                                        if (hasUnit) {
                                                                              _context.next = 8;
                                                                              break;
                                                                        }

                                                                        _context.next = 8;
                                                                        return NLoaderManager.composeResource(selectItem, true);

                                                                  case 8:
                                                                        product.resource = selectItem;
                                                                        resolve([null, product]);
                                                                        _context.next = 17;
                                                                        break;

                                                                  case 12:
                                                                        _context.prev = 12;
                                                                        _context.t0 = _context["catch"](3);
                                                                        product.origin.mountData.error = "resource-load-error";
                                                                        resolve([_context.t0, null]);
                                                                        CPLogger.log("Mounting modeling load error", selectItem, _context.t0);

                                                                  case 17:
                                                                        _context.next = 21;
                                                                        break;

                                                                  case 19:
                                                                        product.origin.mountData.error = "resource-type-undefined";
                                                                        resolve([null, product]);

                                                                  case 21:
                                                                  case "end":
                                                                        return _context.stop();
                                                            }
                                                      }
                                                }, _callee, null, [[3, 12]]);
                                          }));

                                    return function (_x) {
                                          return _ref.apply(this, arguments);
                                    };
                              }());
                  }
            }, {
                  key: "filterChildAssetData",
                  value: function () {
                        var _filterChildAssetData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2(objProvider, unitResource) {
                                    var assetData, errorData, i, info, limit, sorted, _ref2, _ref3, error, results, startIndex, endIndex, provider, rowIdxArr;

                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            assetData = [];
                                                            errorData = [];
                                                            objProvider.forEach(function (item) {
                                                                  if (item.mountData.error == "") {
                                                                        assetData.push(item);
                                                                  } else {
                                                                        errorData.push(item);
                                                                  }
                                                            });
                                                            assetData = assetData.sort(function (a, b) {
                                                                  return parseInt(a.mountData.row_index) - parseInt(b.mountData.row_index);
                                                            });
                                                            limit = assetData.length; //let unitResource = this.getResourceUnits();

                                                            sorted = [];
                                                            i = 0;

                                                      case 7:
                                                            if (!(i < limit)) {
                                                                  _context2.next = 21;
                                                                  break;
                                                            }

                                                            info = assetData[i];
                                                            _context2.next = 11;
                                                            return this.buildRackInnerResourceData(info, unitResource);

                                                      case 11:
                                                            _ref2 = _context2.sent;
                                                            _ref3 = _slicedToArray(_ref2, 2);
                                                            error = _ref3[0];
                                                            results = _ref3[1];

                                                            if (!error) {
                                                                  _context2.next = 17;
                                                                  break;
                                                            }

                                                            return _context2.abrupt("continue", 18);

                                                      case 17:
                                                            sorted.push(results);

                                                      case 18:
                                                            i++;
                                                            _context2.next = 7;
                                                            break;

                                                      case 21:
                                                            if (!(sorted.length == 0)) {
                                                                  _context2.next = 23;
                                                                  break;
                                                            }

                                                            return _context2.abrupt("return", sorted);

                                                      case 23:
                                                            // 들어온 데이터를 row순으로 정렬해 이중 배열로 만듬.
                                                            startIndex = parseInt(sorted[0].rowIndex);
                                                            endIndex = parseInt(sorted[limit - 1].rowIndex) || 0;
                                                            provider = [];

                                                            for (i = startIndex; i <= endIndex; i++) {
                                                                  rowIdxArr = sorted.filter(function (vo) {
                                                                        return vo.rowIndex == i;
                                                                  });

                                                                  if (rowIdxArr.length > 0) {
                                                                        provider.push(rowIdxArr);
                                                                  }
                                                            }

                                                            return _context2.abrupt("return", {
                                                                  assetData: provider,
                                                                  errorData: errorData
                                                            });

                                                      case 28:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2, this);
                              }));

                        function filterChildAssetData(_x2, _x3) {
                              return _filterChildAssetData.apply(this, arguments);
                        }

                        return filterChildAssetData;
                  }()
            }, {
                  key: "$bus",
                  get: function get() {
                        return this._$bus;
                  }
            }, {
                  key: "rackMountDataValidator",
                  get: function get() {
                        return this._rackMountDataValidator;
                  }
            }]);

            return DCIMMountDataManager;
      }();

window.RackMountError = {
      SHELF_INDEX: "shelf_index",
      DATA_TYPE: "data_type",
      OVERSIZE: "oversize",
      OVERLAP_ITEM: "overlap_item"
      /**
       * 랙 실장 데이터가 유효한지 체크해주는 클래스
       * 저장된 자산 데이터를 가공하여 실장 데이터로 반환해주고,
       * 랙실장 관리자에서도 실장 편집시 여기서 제공하는 메소드들을 사용한다.
       */

};

var RackMountDataValidator =
      /*#__PURE__*/
      function () {
            function RackMountDataValidator() {
                  _classCallCheck(this, RackMountDataValidator);

                  this.flexMinSize = 1; ///최소 사이즈 1%, 실장 항목의 width를 flex로 설정할 때, width를 계산하기 위함
            }

            _createClass(RackMountDataValidator, [{
                  key: "addMountData",
                  value: function addMountData(item) {
                        if (item.mountData) {
                              return;
                        }

                        var mountData = {
                              padding: null,
                              row_index: null,
                              row_weight: null,
                              column_weight: null,
                              column_index: null,
                              rotate: null
                        };
                        var itemData = {
                              padding: item.padding,
                              row_index: item.row_index,
                              row_weight: item.row_weight,
                              column_weight: item.column_weight,
                              column_index: item.column_index,
                              rotate: item.rotate
                        };

                        try {
                              item.mountData = $.extend(mountData, itemData); //$.extend(true, mountData, JSON.parse(item.props));
                        } catch (error) {
                              item.mountData = mountData;
                        }
                  }
                  /***
                   * 랙 쉘프 사이즈, 방향 정보와 실장 정보를 인자로 유효성체크하여 에러 결과와 함께 사용가능한 데이터를 반환해주는 메소드
                   * 결과값
                   * { items: 실장항목 기준,
                   *   rows: 쉘프 번호 기준,(조회 편의성을 위해)
                   *   error: 오류 항목
                   * };
                   */

            }, {
                  key: "validate",
                  value: function validate(unitSize, unitDirBT, items) {
                        var _this = this;

                        if (!items.length) {
                              return {
                                    items: [],
                                    rows: {}
                              };
                        }

                        var shelfs = {};
                        var error = [];
                        items.forEach(function (item) {
                              _this.addMountData(item);

                              var mountData = item.mountData;
                              mountData.error = '';
                              var i = mountData.row_index;

                              if (i && !shelfs[i]) {
                                    shelfs[i] = [];
                              } //데이터 타입 검사


                              _this.checkValidDataType(unitSize, item); //row별 아이템 관리


                              if (!mountData.error) {
                                    mountData.rotate = mountData.rotate === 'true' || mountData.rotate === true || mountData.rotate === '1' || mountData.rotate === 1 ? true : false;
                                    shelfs[i].push(item);
                                    /* column error는 무시하고 새로 부여한다.
									if (!shelfs[i][item.column_index]) {
										shelfs[i][item.column_index] = item;
									} else {
										//동일컬럼 row/column index에 중복된 아이템이 있을경우
										item.error = RackMountError.DOUBLE_COLUMN_INDEX;
									}*/
                              } else {
                                    error.push(item);
                              }
                        });
                        var validItem = items.filter(function (x) {
                              return x.mountData.error != RackMountError.DATA_TYPE;
                        }); //row별 사이즈 계산

                        $.each(shelfs, function (index, shelfItems) {
                              _this.updateValidColumnIndex(shelfItems);

                              _this.calcShelfItemsSize(unitSize, unitDirBT, shelfItems);
                        });
                        this.checkOverlapItem(unitDirBT, validItem, shelfs);
                        return {
                              items: items,
                              rows: shelfs,
                              error: error
                        };
                  }
                  /**
                   * 저장된 실장 자산 정보가 유요한 데이터 타입인지를 체크하는 메소드
                   */

            }, {
                  key: "checkValidDataType",
                  value: function checkValidDataType(unitSize, item) {
                        var mountData = item.mountData;
                        var padding = parseInt(mountData.padding);
                        var invalidColumnWidth = mountData.column_weight == 'flex' ? false : isNaN(parseInt(mountData.column_weight));
                        var rowWeight = parseInt(mountData.row_weight);
                        var rowIndex = parseInt(mountData.row_index);
                        var columnIndex = parseInt(mountData.column_index);
                        var invalidShelfIndex = rowIndex < 1 || rowIndex > unitSize;

                        if (isNaN(padding) || invalidColumnWidth || isNaN(rowWeight) || isNaN(rowIndex)) {
                              mountData.error = RackMountError.DATA_TYPE;
                        } else if (invalidShelfIndex) {
                              mountData.error = RackMountError.SHELF_INDEX;
                        } else {
                              mountData.padding = padding;
                              mountData.column_weight = mountData.column_weight == 'flex' ? 'flex' : parseInt(mountData.column_weight);
                              mountData.row_weight = rowWeight;
                              mountData.row_index = rowIndex;
                              mountData.column_index = columnIndex;
                        }
                  }
                  /**
                   * 실장 아이템을 Column Index순으로 정렬하고 이를 0부터 Index 다시 부여하는 메소드
                   * ex) 3,6,7,8 Index를 가진 row Items의 column index를 => 0,1,2,3로 변경
                   */

            }, {
                  key: "updateValidColumnIndex",
                  value: function updateValidColumnIndex(items) {
                        this.sortColumnIndex(items);
                        items.forEach(function (item, index) {
                              item.mountData.column_index = index;
                        });
                  }
                  /**
                   * flex값과 padding값을 실제 백분율로 계산시 소수 2째자리까지 계산하여 반환
                   */

            }, {
                  key: "getFixedNum",
                  value: function getFixedNum(n) {
                        return Number.isInteger(n) ? n : +(Math.floor(n * 100) / 100).toFixed(2);
                  }
                  /*
				   * shelf별 item x, width 구하기
				   */

            }, {
                  key: "calcShelfItemsSize",
                  value: function calcShelfItemsSize(unitSize, unitDirBT, shelfItems) {
                        var _this2 = this;

                        var flexItems = shelfItems.filter(function (x) {
                              return x.mountData.column_weight == "flex";
                        });
                        var flexItemLen = flexItems.length;
                        var totalWidth = 0;
                        /*
						 * flex 값을 구하기 위해 width가 flex 아닌 item의  width와 padding,
						 * flex인 item의 padding값을 모두 더하기
						 */

                        shelfItems.forEach(function (item) {
                              var mountData = item.mountData;
                              mountData.padding = _this2.getFixedNum(mountData.padding);

                              if (mountData.column_weight != "flex") {
                                    mountData.column_weight = _this2.getFixedNum(mountData.column_weight);
                                    var wid = mountData.column_weight;
                                    totalWidth += wid + mountData.padding;
                                    mountData.width = wid;
                              } else {
                                    totalWidth += mountData.padding;
                              }
                        });
                        /*
						 * flex인 item이 있을때 flex 실제값 구하기
						 */

                        if (flexItemLen) {
                              var flexWidth = this.flexMinSize;

                              if (totalWidth <= 100 - flexItemLen * this.flexMinSize) {
                                    flexWidth = (100 - totalWidth) / flexItemLen;
                                    flexWidth = this.getFixedNum(flexWidth);
                              }

                              flexItems.forEach(function (item) {
                                    item.mountData.width = flexWidth;
                              });
                        }
                        /*
						 * item x좌표 대입하고 oversize 정보 구하기
						 */


                        var left = 0;

                        var _loop = function _loop(i) {
                              var item = shelfItems.find(function (x) {
                                    return x.mountData.column_index == i;
                              });
                              var mountData = item.mountData;

                              if (mountData) {
                                    mountData.left = left + mountData.padding;
                                    left += mountData.width + mountData.padding;

                                    if (mountData.left + mountData.width > 100) {
                                          mountData.error = RackMountError.OVERSIZE;
                                    } else if (unitDirBT && mountData.row_index - (mountData.row_weight - 1) < 1 || !unitDirBT && mountData.row_index + (mountData.row_weight - 1) > unitSize) {
                                          mountData.error = RackMountError.OVERSIZE;
                                    }
                              }
                        };

                        for (var i = 0; i < shelfItems.length; i++) {
                              _loop(i);
                        }
                  }
                  /**
                   * 겹쳐진 실장 아이템이 있는지 체크
                   * unitDirBT : unit방향, 방향에 따라 계산이 달라지므로..
                   */

            }, {
                  key: "checkOverlapItem",
                  value: function checkOverlapItem(unitDirBT, items, mountRows) {
                        var _this3 = this;

                        /*row길이가 1이상인것 찾기*/
                        var checkTarget = items.filter(function (x) {
                              return x.mountData.row_weight > 1;
                        });
                        var rowCheckList = {};
                        /*unit 방향별로, 체크해야하는 row index 저장*/

                        checkTarget.forEach(function (item) {
                              for (var i = 1, len = item.mountData.row_weight; i < len; i++) {
                                    var rowIndex = item.mountData.row_index;

                                    if (unitDirBT) {
                                          rowIndex -= i;
                                    } else {
                                          rowIndex += i;
                                    }

                                    if (!rowCheckList[rowIndex]) {
                                          rowCheckList[rowIndex] = [];
                                    }

                                    rowCheckList[rowIndex].push(item);
                              }
                        });
                        /*row별 체크*/

                        $.each(rowCheckList, function (rowIndex, items) {
                              var rowItems = mountRows[rowIndex];

                              if (rowItems) {
                                    for (var i = 0, ilen = items.length; i < ilen; i++) {
                                          var targetItem = items[i];

                                          for (var j = 0, jlen = rowItems.length; j < jlen; j++) {
                                                var item = rowItems[j];

                                                var isOverlap = _this3.getIsOverlap(targetItem, item);

                                                if (isOverlap) {
                                                      targetItem.mountData.error = RackMountError.OVERLAP_ITEM;
                                                      item.mountData.error = RackMountError.OVERLAP_ITEM;
                                                }
                                          }
                                    }
                              }
                        });
                  }
                  /**
                   * item1, item2 두 항목이 겹치는지 여부를 반환
                   */

            }, {
                  key: "getIsOverlap",
                  value: function getIsOverlap(item1, item2) {
                        var startX1 = item1.mountData.left;
                        var startX2 = item2.mountData.left;
                        var endX1 = item1.mountData.left + item1.mountData.width;
                        var endX2 = item2.mountData.left + item2.mountData.width;

                        if (startX1 > startX2 && startX1 >= endX2 || endX1 <= startX2 && endX1 < endX2) {
                              return false;
                        } else {
                              return true;
                        }
                  }
                  /*쉘프의 total width 사이즈 "flex"의 경우 최소값으로 계산 : 남은 공간을 확인할 때 사용 */

            }, {
                  key: "getShelfAllColumnSize",
                  value: function getShelfAllColumnSize(items) {
                        var _this4 = this;

                        var size = 0;
                        items.forEach(function (item) {
                              if (item.mountData.column_weight == "flex") {
                                    size += _this4.flexMinSize;
                              } else {
                                    size += item.mountData.column_weight;
                              }

                              size += item.mountData.padding;
                        });
                        return size;
                  }
                  /**
                   * 변경된 사이즈가 유요한 사이즈인지 여부를 반환(실장관리자에서 사이즈 편집시 호출함)
                   * shelfItems: 실장항목이 실장돼있는 unit전체 항목 리스트
                   * targetItem: 변경을 적용하려는 실장항목
                   * data: 변경하려는 정보
                   */

            }, {
                  key: "validateChangeItemSize",
                  value: function validateChangeItemSize(shelfItems, targetItem, data) {
                        //사이즈가 넘치더라도 이전보다 줄거나 같으면 일단 유지해줌
                        //이전보다 커지면서 넘칠경우 다시 초기 데이터를 넣어줌
                        var itemColumnSize = data.mountData.column_weight == "flex" ? this.flexMinSize : data.mountData.column_weight;
                        var targetItemColumnSize = targetItem.mountData.column_weight == "flex" ? this.flexMinSize : targetItem.mountData.column_weight;
                        var dist = targetItemColumnSize + targetItem.mountData.padding - (itemColumnSize + data.mountData.padding);
                        var result;

                        if (dist >= 0 || this.getShelfAllColumnSize(shelfItems) + dist * -1 <= 100) {
                              //사이즈가 줄었으면 바로 적용
                              result = true;
                        } else {
                              result = false;
                        }

                        return result;
                  }
                  /**
                   *  실장이 가능한지 여부를 반환함
                   *  기존 실장 리스트와, 추가하고자하는 실장 아이템 정보를 받아, 추가 실장할 수 있는 공간이 존재하는지 확인하여 결과를 리턴한다.
                   */

            }, {
                  key: "getIsPossibleMount",
                  value: function getIsPossibleMount(shelfItems, item) {
                        if (!shelfItems) {
                              return true;
                        }

                        var itemWidth = item.mountData.column_weight == "flex" ? this.flexMinSize : item.mountData.column_weight;
                        var mountWidth = 100 - (this.getShelfAllColumnSize(shelfItems) + itemWidth);

                        if (mountWidth >= 0) {
                              return true;
                        } else {
                              return false;
                        }
                  }
                  /**컬럼 index를 새로 대입하는 메소드*/

            }, {
                  key: "overwriteColumnIndex",
                  value: function overwriteColumnIndex(shelfItems) {
                        shelfItems.forEach(function (item, index) {
                              item.mountData.column_index = index;
                        });
                  }
                  /**컬럼 index를 새로 대입하는 메소드*/

            }, {
                  key: "changeShelfItemOrderNo",
                  value: function changeShelfItemOrderNo(shelfItems, ids) {
                        ids.forEach(function (id, index) {
                              var item = shelfItems.find(function (x) {
                                    return x.id == id;
                              });
                              item.mountData.column_index = index;
                        });
                        this.sortColumnIndex(shelfItems);
                  }
                  /**
                   * 실장 리스트를 컬럼 순번대로 sort
                   */

            }, {
                  key: "sortColumnIndex",
                  value: function sortColumnIndex(ary) {
                        ary.sort(function (a, b) {
                              if (a.mountData.column_index > b.mountData.column_index) {
                                    return 1;
                              }

                              if (a.mountData.column_index < b.mountData.column_index) {
                                    return -1;
                              }

                              return 0;
                        });
                  }
            }]);

            return RackMountDataValidator;
      }();
