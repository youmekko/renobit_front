"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/***
 * 자산 배치 상태를 관리하는 allocationProxy와 연동하여 outline구성
 * outline특성상 페이지네이션이 어울리지 않아 전체 목록을 리스트로 구성하는데, 이 때문에 하나의 속성정보만으로 표기한다.
 *    기본 표기 속성은 ID인데 이것을 타입별로 변경할 수 있다.(타입별로 필드 구성이 다르기 때문에..)
 *
 * TODO : 컴포넌트 패널과 마찬가지로 표기하고자 하는 속성 정보를 사용자가 변경하면 이를 localstorage로 저장하여 관리해줄 필요가 있음.
 **/
var AssetOutlinePanel =
      /*#__PURE__*/
      function (_ExtensionPanelCore) {
            _inherits(AssetOutlinePanel, _ExtensionPanelCore);

            function AssetOutlinePanel() {
                  var _this;

                  _classCallCheck(this, AssetOutlinePanel);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(AssetOutlinePanel).call(this));
                  _this.assetComponentProxy = null;
                  _this._app = null;
                  _this.isLoaded = false;
                  return _this;
            }
            /*
                  call :
                        인스턴스 생성 후 실행
                        단, 아직 화면에 붙지 않은 상태임.
                    */


            _createClass(AssetOutlinePanel, [{
                  key: "create",
                  value: function create() {
                        var template = "\n      <div data-id=\"dcim-assets-outline-panel\" v-if=\"isActive\">     \n            <!--\uD0C0\uC785\uC158\uD0DD-->        \n            <div class=\"asset-types\">\n                  <el-select class=\"mini flex select-asset-type\" \n                              :disabled=\"assetTypes.length==0\" \n                              v-model=\"selectedType\" placeholder=\"Select Asset Type\" size=\"mini\"\n                              @change=\"changeSelectedType\">\n                        <el-option\n                              label=\"All\"\n                              value=\"\">\n                        </el-option>\n                        <el-option\n                              v-for=\"item in assetTypes\"\n                              :key=\"item.id\"\n                              :label=\"item.name\"\n                              :value=\"item.id\">\n                        </el-option>\n                  </el-select>\n                  <el-button size=\"mini\" @click=\"showModal\" type=\"primary\" class=\"icon-btn\"><i class=\"el-icon-edit-outline\"></i></el-button>\n            </div>   \n            <!--//\uD0C0\uC785\uC158\uD0DD-->   \n            <!--\uAC80\uC0C9 \uD544\uB4DC-->\n            <div class=\"asset-search\">\n                  <el-select class=\"mini select-search-type\" \n                        v-model=\"search.field\" placeholder=\"searh field\" size=\"mini\"\n                  >\n                        <el-option\n                              v-for=\"item in typeFields\"\n                              :key=\"item.name\"\n                              :label=\"item.label\"\n                              :value=\"item.name\">\n                        </el-option>\n                  </el-select>\n                  <el-input class=\"search-field\" v-model=\"search.keyword\" placeholder=\"search keyword\">\n                        <i v-if=\"search.keyword.length\" @click=\"clearSearchStr()\" class=\"clear-search-str el-icon-error el-input__icon\" slot=\"suffix\"></i>\n                  </el-input>\n            </div>\n            <!--//\uAC80\uC0C9 \uD544\uB4DC-->\n\n            <div class=\"asset-list flex-scroll\" v-if=\"!selectedType && assetSearchResult.length\">\n                  <el-collapse v-model=\"activeNames\" @change=\"handleChange\">\n                        <template v-for=\"type in assetTypes\">\n                              <el-collapse-item :name=\"type.id\" v-if=\"typeAssetObj[type.id] && typeAssetObj[type.id].length\">\n                                    <template slot=\"title\">{{type.name}}<em class=\"tag\">{{typeAssetObj[type.id].length}}</em></template>                                    \n                                    <dl v-for=\"assetCompositionInfo in typeAssetObj[type.id]\"                                           \n                                          :class=\"getStateClass(assetCompositionInfo.selected, assetCompositionInfo.assetInfo.valid_state)\"\n                                          @click=\"onClickAssetItem(assetCompositionInfo)\">\n                                          <dt>\n                                          <el-tooltip class=\"item\" effect=\"dark\" content=\"\uC911\uBCF5 \uBC30\uCE58\uB41C \uC790\uC0B0\uC785\uB2C8\uB2E4.\" placement=\"top-start\">\n                                                <i class=\"el-icon-warning icon double-asset\"></i>\n                                          </el-tooltip>\n                                          <el-tooltip class=\"item\" effect=\"dark\" content=\"\uC0AD\uC81C\uB41C \uC790\uC0B0\uC785\uB2C8\uB2E4.\" placement=\"top-start\">\n                                                <i class=\"el-icon-warning icon none-asset\"></i>\n                                          </el-tooltip>\n                                          {{showTypeFieldId[type.id].label}} : </dt>\n                                          <dd>{{getDataByFieldName(assetCompositionInfo.assetInfo, type.id )}}</dd>\n                                    </dl>                                    \n                              </el-collapse-item>\n                        </template>\n                  </el-collapse>\n            </div>\n            <div class=\"asset-list flex-scroll\" v-if=\"selectedType && selectedTypeAssets.length\">\n                  <dl   v-for=\"assetCompositionInfo in selectedTypeAssets\"\n                        :class=\"getStateClass(assetCompositionInfo.selected, assetCompositionInfo.assetInfo.valid_state)\"\n                        @click=\"onClickAssetItem(assetCompositionInfo)\"\n                        >\n                        <dt>\n                          <el-tooltip class=\"item\" effect=\"dark\" content=\"\uC911\uBCF5 \uBC30\uCE58\uB41C \uC790\uC0B0\uC785\uB2C8\uB2E4.\" placement=\"top-start\">\n                              <i class=\"el-icon-warning icon double-asset\"></i>\n                        </el-tooltip>\n                        <el-tooltip class=\"item\" effect=\"dark\" content=\"\uC0AD\uC81C\uB41C \uC790\uC0B0\uC785\uB2C8\uB2E4.\" placement=\"top-start\">\n                              <i class=\"el-icon-warning icon none-asset\"></i>\n                        </el-tooltip>\n                        {{showTypeFieldId[selectedType].label}} : </dt><dd>{{getDataByFieldName(assetCompositionInfo.assetInfo, selectedType )}}</dd></dl>\n            </div>\n            <div class=\"no-data\" v-if=\"(!selectedType && !assetSearchResult.length) || selectedType && !selectedTypeAssets.length\">\n                  NO DATA\n            </div>\n\n             <!---\uD544\uB4DC \uAD6C\uC131 \uC124\uC815--->\n            <modal\n                  class=\"w-modal\"\n                  id=\"asset-outline-field-settings\"\n                  name=\"asset-outline-field-settings\"\n                  :click-to-close=\"false\"\n                  @opened=\"openedModal\"\n                  @closed=\"cloasedModal\"\n                  :draggable=\"false\"\n                  :resizable=\"true\"\n                  :width=\"480\"\n                  :height=\"280\"\n                  :min-width=\"480\"\n                  :min-height=\"280\"\n\n            >\n                  <div class=\"modal-header\"><h4>Set up asset fields visible by type</h4><a class=\"close-modal-btn\" role=\"button\"\n                                                                              @click=\"hideModal\"><i\n                  class=\"el-icon-error\"></i></a></div>\n                  <div class=\"modal-body\">\n                        <div class=\"cont-wrap\">\n                              <div v-for=\"type in assetTypes\" class=\"radio-wrap\">\n                                    <h5>{{type.name}}</h5>\n                                    <el-radio-group v-model=\"showTypeFieldId[type.id]\">\n                                          <el-radio v-for=\"item in typesFields[type.id]\" :label=\"item\">{{item.label}}</el-radio>\n                                    </el-radio-group>\n                              </div>\n                        </div>\n                  </div>\n            </modal>\n            <!---//\uD544\uB4DC \uAD6C\uC131 \uC124\uC815--->\n       </div>\n      ";
                        /*`
                        <div style="overflow: auto;padding:8px;">
                        <div>자산 아웃라인 패널</div>
                        <div class="component-thumb-list">
                        <ul class="list">
                        <li class="component" v-for="item in assetCompositionInfoList2" >
                        <div class="img-wrap"draggable="true">
                        {{item.name}}
                        </div>
                        </li>
                        </ul>
                        </div>
                        </div>`*/

                        this._$element = $(template);

                        if (this._$parentView == null) {
                              console.log("The parentView must be set before calling the create () method in the Extension Panel.");
                              return;
                        }
                        /*
                        중요:
                        Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.
                        */


                        this._$parentView.append(this._$element);

                        this.assetComponentProxy = this._facade.retrieveProxy(AssetComponentProxy.NAME); // this.allocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);
                        // 트리 목록에서 그룹 목록만 구해오기

                        var that = this;
                        this._app = new Vue({
                              el: this._$element[0],
                              data: {
                                    parent: this,
                                    proxy: this.assetComponentProxy,
                                    allocationProxy: this.allocationProxy,
                                    search: {
                                          field: "",
                                          keyword: ""
                                    },
                                    selectedType: "",
                                    assetTypes: [],
                                    typeFields: [],
                                    ///선택한 타입의 필드 정보
                                    typesFields: {},
                                    //{sensor: []}타입별 전체 필드
                                    showTypeFieldId: {},
                                    // { cctv: "id", pdu: "id", sensor: "id" }, //{sensor : "id"}타입별 노출할 필드 정보
                                    commonFields: [],
                                    isActive: false,
                                    assetCompositionInfoList: [],
                                    assetSearchResult: [],
                                    typeAssetObj: {},
                                    //타입별 자산 데이터
                                    selectedTypeAssets: [],
                                    //선택한 타입의 자산 데이터(전체 선택시 데이터 없음)
                                    activeNames: [],
                                    searchTimer: null
                              },
                              watch: {
                                    search: {
                                          handler: function handler(newValue, oldValue) {
                                                this.onChangeSearchOptions();
                                          },
                                          deep: true
                                    }
                              },
                              mounted: function mounted() {},
                              methods: {
                                    getStateClass: function getStateClass(selected, validState) {
                                          //ok, none, double
                                          //validState = "double";
                                          var className = selected ? "active " : '';

                                          switch (validState) {
                                                case "none":
                                                      className += "none";
                                                      break;

                                                case "double":
                                                      className += "double";
                                                      break;
                                          }

                                          return className;
                                    },
                                    getDataByFieldName: function getDataByFieldName(asset, type) {
                                          var field = this.showTypeFieldId[type].name;
                                          return asset[field] || "";
                                    },
                                    handleChange: function handleChange(val) {
                                          console.log(val);
                                    },
                                    initAssetData: function initAssetData() {
                                          this._updateSelectedTypeFields();

                                          this.updateAssetList();
                                    },
                                    active: function active(bool) {
                                          if (bool) {
                                                this._updateAssetTypes();

                                                this._updateTypesFields();
                                          }

                                          this.isActive = bool;
                                    },
                                    showModal: function showModal() {
                                          this.$modal.show('asset-outline-field-settings');
                                    },
                                    openedModal: function openedModal() {},
                                    hideModal: function hideModal() {
                                          this.$modal.hide('asset-outline-field-settings');
                                    },
                                    cloasedModal: function cloasedModal() {},
                                    clearSearchStr: function clearSearchStr() {
                                          this.search.keyword = "";
                                    },
                                    getTypeFields: function getTypeFields(type) {
                                          return this.proxy.getTypeFields(type, true);
                                    },

                                    /*자산타입 업데이트 */
                                    _updateAssetTypes: function _updateAssetTypes() {
                                          var _this$assetTypes, _this$activeNames;

                                          this.assetTypes.splice(0, this.assetTypes.length);

                                          (_this$assetTypes = this.assetTypes).push.apply(_this$assetTypes, _toConsumableArray(this.proxy.assetTypes));

                                          this.activeNames.splice(0, this.activeNames.length);

                                          (_this$activeNames = this.activeNames).push.apply(_this$activeNames, _toConsumableArray(this.assetTypes.map(function (x) {
                                                return x.id;
                                          })));
                                    },
                                    _updateTypesFields: function _updateTypesFields() {
                                          this.typesFields = {};
                                          this.showTypeFieldId = {};

                                          for (var i in this.assetTypes) {
                                                var type = this.assetTypes[i].id;
                                                var fields = this.getTypeFields(type);
                                                var defaultField = fields.find(function (x) {
                                                      return x.name == "id";
                                                });

                                                if (!this.showTypeFieldId[type]) {
                                                      Vue.set(this.showTypeFieldId, type, defaultField);
                                                }

                                                this.typesFields[type] = fields;
                                          }
                                    },

                                    /*타입 변경 */
                                    changeSelectedType: function changeSelectedType() {
                                          this._updateSelectedTypeFields();

                                          this.updateSelectedTypeAsset();
                                    },

                                    /*update step2 */
                                    _updateSelectedTypeFields: function _updateSelectedTypeFields() {
                                          this.commonFields = this.proxy.visibleFields.common; //this.commonFields = this.commonFields.filter(x => x.visible == true);

                                          this.typeFields.splice(0, this.typeFields.length);

                                          if (this.selectedType == "") {
                                                var _this$typeFields;

                                                (_this$typeFields = this.typeFields).push.apply(_this$typeFields, _toConsumableArray(this.commonFields));
                                          } else {
                                                var _this$typeFields2;

                                                var typeFields = this.getTypeFields(this.selectedType);

                                                (_this$typeFields2 = this.typeFields).push.apply(_this$typeFields2, _toConsumableArray(typeFields));
                                          }

                                          this.search.field = this.typeFields[0].name;
                                          this.search.keyword = "";
                                    },
                                    updateAssetList: function updateAssetList() {
                                          var list = that._facade.retrieveProxy(AssetAllocationProxy.NAME).assetCompositionInfoList || [];
                                          var converted = list.map(function (data) {
                                                return {
                                                      assetId: data.assetId,
                                                      assetInfo: data.assetInfo,
                                                      assetType: data.assetType,
                                                      comInstanceId: data.comInstanceId,
                                                      comInstanceName: data.comInstanceName,
                                                      selected: data.comInstance.selected
                                                };
                                          });

                                          // 배열을 초기화 하는 기능
                                          this.assetCompositionInfoList = _toConsumableArray(converted);
                                          this.searchAssets();
                                    },
                                    sortTypeAsset: function sortTypeAsset() {
                                          var _this2 = this;

                                          this.typeAssetObj = {};

                                          var _loop = function _loop(i) {
                                                var type = _this2.assetTypes[i].id;

                                                var typeAsset = _this2.assetSearchResult.filter(function (x) {
                                                      return x.assetInfo.asset_type == type;
                                                });

                                                _this2.typeAssetObj[type] = typeAsset;
                                          };

                                          for (var i in this.assetTypes) {
                                                _loop(i);
                                          }
                                    },
                                    updateSelectedTypeAsset: function updateSelectedTypeAsset() {
                                          this.selectedTypeAssets.splice(0, this.selectedTypeAssets.length);

                                          if (this.selectedType) {
                                                var _this$selectedTypeAss;

                                                (_this$selectedTypeAss = this.selectedTypeAssets).push.apply(_this$selectedTypeAss, _toConsumableArray(this.typeAssetObj[this.selectedType]));
                                          }
                                    },
                                    onChangeSearchOptions: function onChangeSearchOptions() {
                                          var _this3 = this;

                                          clearTimeout(this.searchTimer);
                                          this.searchTimer = setTimeout(function () {
                                                _this3.searchAssets();
                                          }, 30);
                                    },
                                    searchAssets: function searchAssets() {
                                          var _this4 = this,
                                                _this$assetSearchResu;

                                          //this.search.keyword = "";
                                          var list = this.assetCompositionInfoList;
                                          var keword = this.search.keyword.trim();

                                          if (keword) {
                                                list = list.filter(function (assetCompositionInfo) {
                                                      var assetInfo = assetCompositionInfo.assetInfo;
                                                      var fieldValue = assetInfo[_this4.search.field];

                                                      if (fieldValue && fieldValue.indexOf(_this4.search.keyword) != -1) {
                                                            return assetCompositionInfo;
                                                      }
                                                });
                                          }

                                          this.assetSearchResult.splice(0, this.assetSearchResult.length);

                                          (_this$assetSearchResu = this.assetSearchResult).push.apply(_this$assetSearchResu, _toConsumableArray(list));

                                          this.sortTypeAsset();
                                          this.updateSelectedTypeAsset();
                                    },
                                    onClickAssetItem: function onClickAssetItem(assetCompositionInfo) {
                                          if (wemb.editorProxy.activeLayerInfo.name != "threeLayer") {
                                                this.parent._facade.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, "threeLayer");
                                          }

                                          var component_instance = wemb.mainPageComponent.getComInstanceByName(assetCompositionInfo.comInstanceName);

                                          try {
                                                this.parent._facade.sendNotification(EditorStatic.CMD_NEW_SELECTED_COMPONENT_INSTANCE, component_instance);
                                          } catch (error) {
                                                console.log("AssetOutlinePanel, Error when moving to asset component by asset click", component_instance);
                                          }
                                    }
                              }
                        });
                        return this._app;
                  }
            }, {
                  key: "setAssetData",
                  value: function setAssetData() {
                        this._app.initAssetData();
                  }
                  /*
                  noti가 온 경우 실행
                  call : mediator에서 실행
                  */

            }, {
                  key: "handleNotification",
                  value: function handleNotification(note) {
                        switch (note.name) {
                              case EditorProxy.NOTI_OPEN_PAGE:
                                    this._app.active(true);

                                    if (!this.isLoaded) {
                                          this.setAssetData();
                                          this.isLoaded = true;
                                    }

                                    break;

                              case EditorProxy.NOTI_CLOSED_PAGE:
                                    this._app.active(false);

                                    break;

                              case AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST:
                              case EditorProxy.NOTI_UPDATE_COM_INSTANCE:
                                    this.noti_UpdateAssetAllocationList();
                                    break;
                        }
                  }
            }, {
                  key: "noti_UpdateAssetAllocationList",
                  value: function noti_UpdateAssetAllocationList() {
                        if (this.isLoaded) {
                              this._app.updateAssetList();
                        }
                  }
            }, {
                  key: "notificationList",
                  get: function get() {
                        return [EditorProxy.NOTI_OPEN_PAGE, EditorProxy.NOTI_CLOSED_PAGE, EditorProxy.NOTI_CHANGE_ACTIVE_LAYER, AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, EditorProxy.NOTI_UPDATE_COM_INSTANCE];
                  }
            }]);

            return AssetOutlinePanel;
      }(ExtensionPanelCore);
