/***
 * 자산 컴포넌트 맵핑 패널
 * 자산관리자에서 설정된 자산타입별/컴포넌트 노출
 * assetComponentProxy == dcimManager.assetManager 에서 자산정보를 모두 가지고 시작
 * 배치여부를 실시간 반영, NOTI_UPDATE_ASSET_ALLOCATION_LIST Noti를 통해 실시간으로 배치여부에 따라 refresh 처리함
 *  - 자산 정보에 instance_id(컴포넌트와 연결) / parent_id(실장자산에 연결) 값이 있는지에 따라 배치여부를 구분함
 * 2D/3D 선택레이어를 변경할수 있는 버튼도 포함하는데, 기본 2D 레이어가 선택돼있는 상태에서 패널 접근시 컴포넌트가 없는것처럼 보이고 이 점이 금방 인지되지 않아 같은 기능의 버튼을 제공함
 *
 * 자산목록은 assetComponentProxy == dcimManager.assetManager 객체의 .assetsDataManager() 메소드를 통해 AssetsDataManager객체를 리턴받아 사용
 * 프로젝트에서 공유하는 자산 데이터를 참조. 페이지네이션/검색/정렬/필터 결과를 얻을 수 있음 *
 *
 * TODO :
 * 1. 자산 동기화시 자산 정보가 업데이트 되는데 컴포넌트 정보는 동기화되지 않음.
 * 2. 자산 패널에서 표출하는 필드를 설정하는 기능이 있는데, 자산 타입을 변경할 경우 이 정보가 없어짐. localstorage 저장처리가 있어야할듯..
 **/
class AssetComponentPanel extends ExtensionPanelCore {
    constructor() {
        super();
        this.activeLayer = AssetComponentPanel.LAYERS.TWO_LAYER;
        this.assetComponentProxy = null;
        this._app = null;
        this.isLoaded = false;
    }

    /* call :인스턴스 생성 후 실행  단, 아직 화면에 붙지 않은 상태임. */
    create() {
        let template = `
             <div style="overflow: auto;" data-id="dcim-assets-panel" v-if="activeLayer">
                <!--타입션택-->        
                <div style="display: flex;">
                        <el-select class="mini select-asset-type" 
                                    :disabled="assetTypes.length==0" 
                                    v-model="selectedType" placeholder="Select Asset Type" size="mini"
                                    @change="changeSelectedType">
                              <el-option
                                    v-for="item in assetTypes"
                                    :key="item.id"
                                    :label="item.name"
                                    :value="item.id">
                              </el-option>
                        </el-select>
                        <el-radio-group v-model="activeLayerModel" @change="onChangeActiveLayer" type="primary" size="mini">
                              <el-radio-button type="primary" size="mini" label="twoLayer">2D</el-radio-button>
                              <el-radio-button type="primary" size="mini" label="threeLayer">3D</el-radio-button>
                        </el-radio-group>
                </div>   
                <!--//타입션택-->    
                <split class="flex" direction="vertical">
                    <split-area  :size="40">
                        <!--컴포넌트 목록-->   
                        <div class="comps-wrap">
                        
                            <template v-if="selectedTypeComps && selectedTypeComps.length">
                          
                                <div class="preview-frame">
                                    <img :src="compPreviewPath" @error="onErrorPreviewImage" style="position:absolute;visibility: hidden;">
                                    <span class="preview" v-show="!isPreviewError" :style="{ 'background-image': 'url(' + compPreviewPath + ')' }"></span>
                                    <span class="preview-default" v-show="isPreviewError"></span>
                                </div>
                                
                                <div class="comp-list">
                                    <ul>
                                    <template v-for="(item, index) in selectedTypeComps">
                                        <li v-if="index%2==0">
                                            <label :for="selectedTypeComps[index].id">
                                                <input type="radio"
                                                v-model="selectedCompItem" 
                                                :id="selectedTypeComps[index].id"
                                                :value="selectedTypeComps[index]"
                                                name="renobtAssetComps">
                                                <span :style="{cursor: selectedTypeComps.length === 1 ? 'default' : 'pointer'}">{{ firstLetterToUpperCase(selectedTypeComps[index].label) }}</span>
                                            </label>
                                            <label v-if="selectedTypeComps[index+1]"
                                                :for="selectedTypeComps[index+1].id">
                                                <input type="radio"
                                                v-model="selectedCompItem" 
                                                :id="selectedTypeComps[index+1].id"
                                                :value="selectedTypeComps[index+1]"
                                                name="renobtAssetComps">
                                                <span>{{selectedTypeComps[index+1].label}}</span>
                                            </label>
                                        </li>
                                        </template>
                                    </ul>
                                </div>
                            </template>
                            <template v-else>
                            <span class="no-data">NO DATA</span>
                            </template>
                        </div>

                        <!--//컴포넌트 목록-->   
                    </split-area>
                    <split-area  :size="60">
                        <!--자산 목록-->                        
                        <div class="assets-wrap">
                            <template v-if="selectedCompItem">
                                <div class="assets-filter">
                                    
                                    <!--검색 필드-->
                                    <div class="assets-search">
                                         <el-select class="mini select-search-type" 
                                            v-model="search.field" placeholder="searh field" size="mini"
                                            @change="onChangeSearchField"
                                        >
                                            <el-option
                                                    v-for="item in typeFields"
                                                    :key="item.name"
                                                    :label="item.label"
                                                    :value="item.name">
                                            </el-option>
                                        </el-select>
                                        <el-input class="search-field" v-model="search.keyword" placeholder="search keyword">
                                            <i v-if="search.keyword.length" @click="clearSearchStr()" class="clear-search-str el-icon-error el-input__icon" slot="suffix"></i>
                                        </el-input>
                                    </div>
                                    <!--//검색 필드-->
                                    <!--정렬/필터 필드-->
                                    <div class="assets-sort">
                                         <el-select class="mini select-sort-type" 
                                                 v-model="sort.field" 
                                                 placeholder="sort field" size="mini"
                                                 @change="onChangeSortField"
                                        >
                                            <el-option
                                                    v-for="item in typeFields"
                                                    :key="item.name"
                                                    :label="item.label"
                                                    :value="item.name">
                                            </el-option>
                                        </el-select>

                                        <el-radio-group class="flex sort-radio" v-model="sort.state">
                                            <el-radio label="ascending"><i class="el-icon-sort-up"></i></el-radio>
                                            <el-radio label="descending"><i class="el-icon-sort-down"></i></el-radio>
                                        </el-radio-group>

                                        <div>
                                            <el-checkbox  class="unpositioned-checkbox" v-model="search.onlyUnpositioned">{{lang.common.unpositioned}}</el-checkbox>                                        
                                        </div>                                      
                                    </div>
                                   <!--//정렬/필터 필드-->
                                </div>
                                <div class="assets-list">
                                    <ul v-if="optionsResult.length">
                                        <template v-for="item in currentPageAssets">
                                        <li v-if="!item.instance_id && !item.parent_id" @dragstart="onDragStart($event, item)" draggable="true">
                                            <span v-for="field in checkedFields" :key="field.name">
                                                <em>{{field.label}}</em> : <em>{{item[field.name]}}</em>
                                            </span>                                            
                                        </li>  
                                        <li v-else class="disabled">
                                            <span v-for="field in checkedFields" :key="field.name">
                                                <em>{{field.label}}</em> : <em>{{item[field.name]}}</em>
                                            </span>                                            
                                        </li>  
                                        </template>                                                                       
                                    </ul>
                                    <span v-else class="no-data">NO DATA</span>
                                </div>
                                <div class="assets-pagination">
                                    <el-button size="mini" @click="showModal" type="primary" class="icon-btn"><i class="el-icon-edit-outline"></i></el-button>
                                    <el-button size="mini" @click="prevPage" :disabled="1 >= pagination.page">prev</el-button>
                                    <span>{{pagination.page}}/{{pagination.totalPage}}</span>
                                    <el-button size="mini" type="primary" @click="nextPage" :disabled="pagination.totalPage == pagination.page">next</el-button>
                                </div>
                            </template>
                            <template v-else>
                                <p class="no-data">NO DATA</p>
                            </template>    
                        </div>                    
                        <!--//자산 목록-->
                    </split-area>
                </split>



                <!---필드 구성 설정--->
                <modal
                    class="w-modal"
                    id="asset-panel-field-settings"
                    name="asset-panel-field-settings"
                    :click-to-close="false"
                    @closed="saveFieldSettings"
                    :draggable="false"
                    :resizable="true"
                    :width="480"
                    :height="280"
                    :min-width="480"
                    :min-height="280"

                >
                    <div class="modal-header"><h4>Asset Fields</h4><a class="close-modal-btn" role="button"
                                                                                    @click="hideModal"><i
                        class="el-icon-error"></i></a></div>
                    <div class="modal-body">
                        <div class="cont-wrap">
                            <el-checkbox :indeterminate="isIndeterminate" v-model="assetFieldsCheckAll" @change="handleCheckAllChange" style="margin-bottom: 16px;">Check all</el-checkbox>
                            <el-checkbox-group class="group-box" v-model="checkedFieldsName" @change="handleCheckedFieldsChange">
                                <el-checkbox v-for="field in typeFields" :label="field.name" :key="field.name">{{field.label}}</el-checkbox>
                            </el-checkbox-group>
                        </div>
                    </div>
                </modal>
                <!---//필드 구성 설정--->
                
            </div>
            `

        this._$element = $(template);
        if (this._$parentView == null) {
            console.log("The parentView must be set before calling the create() method in the Extension Panel.")
            return;
        }

        this._$parentView.append(this._$element);
        this.assetComponentProxy = this._facade.retrieveProxy(AssetComponentProxy.NAME); //==dcimManager.assetManager

        // 트리 목록에서 그룹 목록만 구해오기
        this._app = new Vue({
            el: this._$element[0],
            data: {
                lang: Vue.$i18n.messages.wv.dcim_pack,
                activeLayer: this.activeLayer,
                activeLayerModel: this.activeLayer,
                proxy: this.assetComponentProxy,
                assets2DCompObj: {},
                assets3DCompObj: {},
                currentComps: [],
                selectedType: "",
                selectedTypeComps: [],
                pagination: { page: 1, totalPage: 1 },
                sort: { field: "", state: "" },
                search: { field: "", keyword: "", onlyUnpositioned: true },
                assetTypes: [],
                selectedCompItem: null,
                parent: this,
                optionsResult: [], ///검색 결과 전체 자산
                currentPageAssets: [], //검색결과 전체에서 현재 페이지부분만
                isPreviewError: false,
                updateAssetListTimer: null,

                typeFields: [], ///선택한 타입의 필드 정보
                assetFieldsCheckAll: false,
                checkedFieldsName: [],
                checkedFields: [],
                isIndeterminate: true
            },

            computed: {
                //컴포넌트 미리보기 이미지 경로 가져오기
                compPreviewPath: function() {
                    var url = "";
                    switch (this.activeLayer) {
                        case AssetComponentPanel.LAYERS.TWO_LAYER:
                            try {
                                let previewPath;
                                if (this.selectedCompItem.data.props.preview_path) {
                                    previewPath = this.selectedCompItem.data.props.preview_path;
                                } else {
                                    previewPath = this.selectedCompItem.file_path;
                                    let tempAry = previewPath.split("/");
                                    tempAry.pop();
                                    url = tempAry.join("/") + "/preview.png";
                                }
                            } catch (error) {}
                            break;
                        case AssetComponentPanel.LAYERS.THREE_LAYER:
                            try {
                                let rootPath = this.selectedCompItem.data.props.initProperties.props.resource.modeling.default;
                                let objPath = rootPath.path || rootPath.objPath;
                                url = objPath.slice(0, objPath.lastIndexOf(".obj")) + "-P.png";
                                url = window.wemb.configManager.context + url;
                            } catch (error) {}

                            break;
                        default:

                    }

                    this.isPreviewError = false;
                    return url;
                }
            },

            watch: {
                search: {
                    handler: function(newValue, oldValue) {
                        this.changeAssetsByOptions();
                    },
                    deep: true
                },

                sort: {
                    handler: function() {
                        this.changeAssetsByOptions();
                    },
                    deep: true
                }
            },

            mounted() {},

            methods: {

                //자산 타입(파일명) 첫번째 글자 대문자로 변경하기
                firstLetterToUpperCase(assetTypeFileName) {
                    return assetTypeFileName.charAt(0).toUpperCase() + assetTypeFileName.slice(1);
                },


                /*필드설정 모달 관련*/
                handleCheckAllChange(val) {
                    this.checkedFieldsName.splice(0, this.checkedFieldsName.length);
                    let typeFieldsName = this.typeFields.map(x => x.name);
                    if (val) {
                        this.checkedFieldsName.push(...typeFieldsName);
                    }

                    this.isIndeterminate = false;
                },

                handleCheckedFieldsChange(value) {
                    let checkedCount = value.length;
                    this.assetFieldsCheckAll = checkedCount === this.typeFields.length;
                    this.isIndeterminate = checkedCount > 0 && checkedCount < this.typeFields.length;
                },

                /*
                2018.11.09(ckkim)
                데이터 다시 부르기
                현재 사용하지 않음.
                handleClickRefreshAssetData() {
                    wemb.dcimManager.refreshAssetAllData();
                },
                 */


                saveFieldSettings() {
                    this.checkedFields.splice(0, this.checkedFields.length);
                    let list = this.typeFields.filter(x => this.checkedFieldsName.indexOf(x.name) != -1);
                    this.checkedFields.push(...list);
                },

                showModal: function() {
                    this.$modal.show('asset-panel-field-settings');
                },

                hideModal() {
                    if (!this.checkedFieldsName.length) {
                        this.$message.error(this.lang.assetComponentPanel.selectField);
                    } else {
                        this.$modal.hide('asset-panel-field-settings');
                    }
                },
                /*//필드설정 모달 관련*/

                onErrorPreviewImage() {
                    this.isPreviewError = true;
                },

                clearSearchStr() {
                    this.search.keyword = "";
                },

                onChangeActiveLayer() {
                    this.$emit(AssetComponentPanel.EVENT_CHANGE_ACTIVE_LAYER, this.activeLayerModel);
                },

                onChangeSortField() {
                    this.sort.state = "";
                },

                onChangeSearchField() {
                    this.search.keyword = "";
                },

                changeActiveLayer(layer) {
                    if (layer != this.activeLayer) {
                        this.activeLayer = layer;
                        this.activeLayerModel = this.activeLayer;
                        this._updateSelectedTypeComps(this.selectedType);
                    }
                },

                async setAssetList() {
                    this._updateAssetTypes();
                    this._updateAssetCompsData();
                    this.initSelectedType();
                    this.changeSelectedType();
                },


                initSelectedType() {
                    if (this.assetTypes.length) {
                        this.selectedType = this.assetTypes[0].id;
                    }
                },


                /*자산타입 업데이트 */
                _updateAssetTypes() {
                    this.assetTypes.splice(0, this.assetTypes.length);
                    this.assetTypes.push(...this.proxy.assetTypes);
                },


                /*자산 컴포넌트 데이터 업데이트*/
                _updateAssetCompsData() {
                    this.assets2DCompObj = this.proxy.assetCompsInfo.comp2d;
                    this.assets3DCompObj = this.proxy.assetCompsInfo.comp3d;
                },

                /*타입 변경 */
                changeSelectedType() {
                    this._updateSelectedTypeComps();
                    this._updateSelectedTypeFields();
                    this._updateSelectedTypeAssetData();
                },


                /*update step1 선택한 타입의 컴포넌트 목록 업데이트 */
                _updateSelectedTypeComps() {
                    this.selectedCompItem = null;
                    this.selectedTypeComps.splice(0, this.selectedTypeComps.length);

                    let targetCompObj = this.assets3DCompObj;
                    if (this.activeLayer != AssetComponentPanel.LAYERS.THREE_LAYER) {
                        targetCompObj = this.assets2DCompObj;
                    }

                    if (targetCompObj[this.selectedType]) {
                        this.selectedTypeComps.push(...targetCompObj[this.selectedType]);
                    }

                    if (this.selectedTypeComps.length) {
                        this.selectedCompItem = this.selectedTypeComps[0];
                    }
                },

                /*update step2 */
                _updateSelectedTypeFields() {
                    let commonFields = this.proxy.visibleFields.common;
                    //commonFields = commonFields.filter(x => x.visible == true);
                    let typeFields = this.proxy.getTypeFields(this.selectedType, true);

                    this.typeFields.splice(0, this.typeFields.length);
                    this.typeFields.push(...typeFields);

                    this.checkedFields.splice(0, this.checkedFields.length);
                    this.checkedFields.push(...commonFields);
                    this.checkedFieldsName = this.checkedFields.map(x => x.name);

                    this.sort.field = this.typeFields[0] ? this.typeFields[0].name : '';
                    this.sort.state = "";
                    this.search.field = this.typeFields[0] ? this.typeFields[0].name : '';
                    this.search.keyword = "";
                },


                /*update step3 */
                _updateSelectedTypeAssetData() {
                    this.changeAssetsByOptions();
                },

                getAssetsDataManager() {
                    return this.proxy.assetsDataManager;
                },

                changeAssetsByOptions() {
                    clearTimeout(this.updateAssetListTimer);
                    this.updateAssetListTimer = setTimeout(() => {
                        this.updateAssetsByOptions();
                        this.changeCurrentPage(1);
                    }, 10);
                },

                refreshCurrentPageAssets() {
                    this.updateAssetsByOptions();
                    this.changeCurrentPage(this.pagination.page);
                },

                updateAssetsByOptions() {
                    this.optionsResult.splice(0, this.optionsResult.length);
                    this.optionsResult.push(...this.getAssetsDataManager().getDataByOptions(this.selectedType, {
                        search: this.search,
                        sort: this.sort
                    }));
                },

                changeCurrentPage(page) {
                    let currentPageData = this.getAssetsDataManager().getPaginationData(this.optionsResult, page);
                    this.currentPageAssets.splice(0, this.currentPageAssets.length);
                    this.currentPageAssets.push(...currentPageData.data);
                    this.pagination.page = currentPageData.page;
                    this.pagination.totalPage = currentPageData.totalPage;
                },


                nextPage() {
                    let page = Math.min(this.pagination.totalPage, this.pagination.page + 1);
                    if (this.pagination.page != page) {
                        this.changeCurrentPage(page);
                    }
                },

                prevPage() {
                    let page = Math.max(1, this.pagination.page - 1);
                    if (this.pagination.page != page) {
                        this.changeCurrentPage(page);
                    }
                },

                onDragStart(event, assetData) {
                    if (!assetData || !this.selectedCompItem) {
                        return;
                    }
                    /*  1. 컴포넌트 기본 정보
                              2. asset 정보
                              3. 컴포넌트 환경 정보*/

                    /* 1. 컴포넌트 기본 정보
                              : 선택한 컴포넌트에 이미 존재 */

                    /* 2018.12.12 (ckkim) name(Component)에서 id로 수정 */
                    //var comInstanceInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("3D", this.selectedCompItem.data.name);


                    try {
                        var comInstanceMetaInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("three_layer", this.selectedCompItem.data.file_id);

                        if (!comInstanceMetaInfo) {
                            alert(this.lang.assetComponentPanel.noComponent);
                            return;
                        }

                        comInstanceMetaInfo = $.extend(true, {}, comInstanceMetaInfo);
                        comInstanceMetaInfo.category = "3D";

                        wemb.globalStore.set("__drag_asset_data__", assetData);


                        event.dataTransfer.effectAllowed = 'move';
                        if (CPUtil.getInstance().browserDetection() != "Internet Explorer") {
                            event.dataTransfer.setData('text/plane', JSON.stringify(comInstanceMetaInfo));
                        }

                        // 3. 컴포넌트 환경 정보
                        window["dragData"] = JSON.stringify(comInstanceMetaInfo);

                    } catch (error) {
                        console.log("Error, The asset id does not exist.", error);
                    }
                }
            }
        })

        this.bindAppEvent();
        return this._app;
    }

    bindAppEvent() {
        this._app.$on(AssetComponentPanel.EVENT_CHANGE_ACTIVE_LAYER, (layerName) => {
            this.assetComponentProxy.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, layerName);
        });
    }

    get notificationList() {
        return [
            EditorProxy.NOTI_OPEN_PAGE,
            EditorProxy.NOTI_CLOSED_PAGE,
            EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
            AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST
        ]
    }

    setAssetData() {
        this._app.setAssetList();
    }

    /*
      noti가 온 경우 실행
      call : mediator에서 실행
       */
    handleNotification(note) {
        switch (note.name) {
            case EditorProxy.NOTI_OPEN_PAGE:
                console.log("info_, AssetComponentPanel NOTI_OPEN_PAGE");
                if (!this.isLoaded) {
                    this.setAssetData();
                }
                this.isLoaded = true;
                this._app.changeActiveLayer(this.activeLayer);
                break;
            case EditorProxy.NOTI_CLOSED_PAGE:
                this._app.changeActiveLayer("");
                break;
            case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER:
                switch (note.body) {
                    case AssetComponentPanel.LAYERS.THREE_LAYER:
                        this.activeLayer = AssetComponentPanel.LAYERS.THREE_LAYER;
                        break;
                    case AssetComponentPanel.LAYERS.TWO_LAYER:
                    case AssetComponentPanel.LAYERS.MASTER_LAYER:
                        this.activeLayer = AssetComponentPanel.LAYERS.TWO_LAYER;
                        break;
                    default:
                        this.activeLayer = "";
                }
                this._app.changeActiveLayer(this.activeLayer);
                break;
            case AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST:
                this._app.refreshCurrentPageAssets();
                break;
        }
    }
}

AssetComponentPanel.LAYERS = {
    MASTER_LAYER: "masterLayer",
    TWO_LAYER: "twoLayer",
    THREE_LAYER: "threeLayer"
}

AssetComponentPanel.EVENT_CHANGE_ACTIVE_LAYER = "event/changeActiveLayer";
