"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/***
 * 자산 컴포넌트 맵핑 패널
 * 자산관리자에서 설정된 자산타입별/컴포넌트 노출
 * assetComponentProxy == dcimManager.assetManager 에서 자산정보를 모두 가지고 시작
 * 배치여부를 실시간 반영, NOTI_UPDATE_ASSET_ALLOCATION_LIST Noti를 통해 실시간으로 배치여부에 따라 refresh 처리함
 *  - 자산 정보에 instance_id(컴포넌트와 연결) / parent_id(실장자산에 연결) 값이 있는지에 따라 배치여부를 구분함
 * 2D/3D 선택레이어를 변경할수 있는 버튼도 포함하는데, 기본 2D 레이어가 선택돼있는 상태에서 패널 접근시 컴포넌트가 없는것처럼 보이고 이 점이 금방 인지되지 않아 같은 기능의 버튼을 제공함
 *
 * 자산목록은 assetComponentProxy == dcimManager.assetManager 객체의 .assetsDataManager() 메소드를 통해 AssetsDataManager객체를 리턴받아 사용
 * 프로젝트에서 공유하는 자산 데이터를 참조. 페이지네이션/검색/정렬/필터 결과를 얻을 수 있음 *
 *
 * TODO :
 * 1. 자산 동기화시 자산 정보가 업데이트 되는데 컴포넌트 정보는 동기화되지 않음.
 * 2. 자산 패널에서 표출하는 필드를 설정하는 기능이 있는데, 자산 타입을 변경할 경우 이 정보가 없어짐. localstorage 저장처리가 있어야할듯..
 **/
var AssetComponentPanel =
      /*#__PURE__*/
      function (_ExtensionPanelCore) {
            _inherits(AssetComponentPanel, _ExtensionPanelCore);

            function AssetComponentPanel() {
                  var _this;

                  _classCallCheck(this, AssetComponentPanel);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(AssetComponentPanel).call(this));
                  _this.activeLayer = AssetComponentPanel.LAYERS.TWO_LAYER;
                  _this.assetComponentProxy = null;
                  _this._app = null;
                  _this.isLoaded = false;
                  return _this;
            }
            /* call :인스턴스 생성 후 실행  단, 아직 화면에 붙지 않은 상태임. */


            _createClass(AssetComponentPanel, [{
                  key: "create",
                  value: function create() {
                        var template = "\n             <div style=\"overflow: auto;\" data-id=\"dcim-assets-panel\" v-if=\"activeLayer\">\n                <!--\uD0C0\uC785\uC158\uD0DD-->        \n                <div style=\"display: flex;\">\n                        <el-select class=\"mini select-asset-type\" \n                                    :disabled=\"assetTypes.length==0\" \n                                    v-model=\"selectedType\" placeholder=\"Select Asset Type\" size=\"mini\"\n                                    @change=\"changeSelectedType\">\n                              <el-option\n                                    v-for=\"item in assetTypes\"\n                                    :key=\"item.id\"\n                                    :label=\"item.name\"\n                                    :value=\"item.id\">\n                              </el-option>\n                        </el-select>\n                        <el-radio-group v-model=\"activeLayerModel\" @change=\"onChangeActiveLayer\" type=\"primary\" size=\"mini\">\n                              <el-radio-button type=\"primary\" size=\"mini\" label=\"twoLayer\">2D</el-radio-button>\n                              <el-radio-button type=\"primary\" size=\"mini\" label=\"threeLayer\">3D</el-radio-button>\n                        </el-radio-group>\n                </div>   \n                <!--//\uD0C0\uC785\uC158\uD0DD-->    \n                <split class=\"flex\" direction=\"vertical\">\n                    <split-area  :size=\"40\">\n                        <!--\uCEF4\uD3EC\uB10C\uD2B8 \uBAA9\uB85D-->   \n                        <div class=\"comps-wrap\">\n                        \n                            <template v-if=\"selectedTypeComps && selectedTypeComps.length\">\n                          \n                                <div class=\"preview-frame\">\n                                    <img :src=\"compPreviewPath\" @error=\"onErrorPreviewImage\" style=\"position:absolute;visibility: hidden;\">\n                                    <span class=\"preview\" v-show=\"!isPreviewError\" :style=\"{ 'background-image': 'url(' + compPreviewPath + ')' }\"></span>\n                                    <span class=\"preview-default\" v-show=\"isPreviewError\"></span>\n                                </div>\n                                \n                                <div class=\"comp-list\">\n                                    <ul>\n                                    <template v-for=\"(item, index) in selectedTypeComps\">\n                                        <li v-if=\"index%2==0\">\n                                            <label :for=\"selectedTypeComps[index].id\">\n                                                <input type=\"radio\"\n                                                v-model=\"selectedCompItem\" \n                                                :id=\"selectedTypeComps[index].id\"\n                                                :value=\"selectedTypeComps[index]\"\n                                                name=\"renobtAssetComps\">\n                                                <span :style=\"{cursor: selectedTypeComps.length === 1 ? 'default' : 'pointer'}\">{{ firstLetterToUpperCase(selectedTypeComps[index].label) }}</span>\n                                            </label>\n                                            <label v-if=\"selectedTypeComps[index+1]\"\n                                                :for=\"selectedTypeComps[index+1].id\">\n                                                <input type=\"radio\"\n                                                v-model=\"selectedCompItem\" \n                                                :id=\"selectedTypeComps[index+1].id\"\n                                                :value=\"selectedTypeComps[index+1]\"\n                                                name=\"renobtAssetComps\">\n                                                <span>{{selectedTypeComps[index+1].label}}</span>\n                                            </label>\n                                        </li>\n                                        </template>\n                                    </ul>\n                                </div>\n                            </template>\n                            <template v-else>\n                            <span class=\"no-data\">NO DATA</span>\n                            </template>\n                        </div>\n\n                        <!--//\uCEF4\uD3EC\uB10C\uD2B8 \uBAA9\uB85D-->   \n                    </split-area>\n                    <split-area  :size=\"60\">\n                        <!--\uC790\uC0B0 \uBAA9\uB85D-->                        \n                        <div class=\"assets-wrap\">\n                            <template v-if=\"selectedCompItem\">\n                                <div class=\"assets-filter\">\n                                    \n                                    <!--\uAC80\uC0C9 \uD544\uB4DC-->\n                                    <div class=\"assets-search\">\n                                         <el-select class=\"mini select-search-type\" \n                                            v-model=\"search.field\" placeholder=\"searh field\" size=\"mini\"\n                                            @change=\"onChangeSearchField\"\n                                        >\n                                            <el-option\n                                                    v-for=\"item in typeFields\"\n                                                    :key=\"item.name\"\n                                                    :label=\"item.label\"\n                                                    :value=\"item.name\">\n                                            </el-option>\n                                        </el-select>\n                                        <el-input class=\"search-field\" v-model=\"search.keyword\" placeholder=\"search keyword\">\n                                            <i v-if=\"search.keyword.length\" @click=\"clearSearchStr()\" class=\"clear-search-str el-icon-error el-input__icon\" slot=\"suffix\"></i>\n                                        </el-input>\n                                    </div>\n                                    <!--//\uAC80\uC0C9 \uD544\uB4DC-->\n                                    <!--\uC815\uB82C/\uD544\uD130 \uD544\uB4DC-->\n                                    <div class=\"assets-sort\">\n                                         <el-select class=\"mini select-sort-type\" \n                                                 v-model=\"sort.field\" \n                                                 placeholder=\"sort field\" size=\"mini\"\n                                                 @change=\"onChangeSortField\"\n                                        >\n                                            <el-option\n                                                    v-for=\"item in typeFields\"\n                                                    :key=\"item.name\"\n                                                    :label=\"item.label\"\n                                                    :value=\"item.name\">\n                                            </el-option>\n                                        </el-select>\n\n                                        <el-radio-group class=\"flex sort-radio\" v-model=\"sort.state\">\n                                            <el-radio label=\"ascending\"><i class=\"el-icon-sort-up\"></i></el-radio>\n                                            <el-radio label=\"descending\"><i class=\"el-icon-sort-down\"></i></el-radio>\n                                        </el-radio-group>\n\n                                        <div>\n                                            <el-checkbox  class=\"unpositioned-checkbox\" v-model=\"search.onlyUnpositioned\">{{lang.common.unpositioned}}</el-checkbox>                                        \n                                        </div>                                      \n                                    </div>\n                                   <!--//\uC815\uB82C/\uD544\uD130 \uD544\uB4DC-->\n                                </div>\n                                <div class=\"assets-list\">\n                                    <ul v-if=\"optionsResult.length\">\n                                        <template v-for=\"item in currentPageAssets\">\n                                        <li v-if=\"!item.instance_id && !item.parent_id\" @dragstart=\"onDragStart($event, item)\" draggable=\"true\">\n                                            <span v-for=\"field in checkedFields\" :key=\"field.name\">\n                                                <em>{{field.label}}</em> : <em>{{item[field.name]}}</em>\n                                            </span>                                            \n                                        </li>  \n                                        <li v-else class=\"disabled\">\n                                            <span v-for=\"field in checkedFields\" :key=\"field.name\">\n                                                <em>{{field.label}}</em> : <em>{{item[field.name]}}</em>\n                                            </span>                                            \n                                        </li>  \n                                        </template>                                                                       \n                                    </ul>\n                                    <span v-else class=\"no-data\">NO DATA</span>\n                                </div>\n                                <div class=\"assets-pagination\">\n                                    <el-button size=\"mini\" @click=\"showModal\" type=\"primary\" class=\"icon-btn\"><i class=\"el-icon-edit-outline\"></i></el-button>\n                                    <el-button size=\"mini\" @click=\"prevPage\" :disabled=\"1 >= pagination.page\">prev</el-button>\n                                    <span>{{pagination.page}}/{{pagination.totalPage}}</span>\n                                    <el-button size=\"mini\" type=\"primary\" @click=\"nextPage\" :disabled=\"pagination.totalPage == pagination.page\">next</el-button>\n                                </div>\n                            </template>\n                            <template v-else>\n                                <p class=\"no-data\">NO DATA</p>\n                            </template>    \n                        </div>                    \n                        <!--//\uC790\uC0B0 \uBAA9\uB85D-->\n                    </split-area>\n                </split>\n\n\n\n                <!---\uD544\uB4DC \uAD6C\uC131 \uC124\uC815--->\n                <modal\n                    class=\"w-modal\"\n                    id=\"asset-panel-field-settings\"\n                    name=\"asset-panel-field-settings\"\n                    :click-to-close=\"false\"\n                    @closed=\"saveFieldSettings\"\n                    :draggable=\"false\"\n                    :resizable=\"true\"\n                    :width=\"480\"\n                    :height=\"280\"\n                    :min-width=\"480\"\n                    :min-height=\"280\"\n\n                >\n                    <div class=\"modal-header\"><h4>Asset Fields</h4><a class=\"close-modal-btn\" role=\"button\"\n                                                                                    @click=\"hideModal\"><i\n                        class=\"el-icon-error\"></i></a></div>\n                    <div class=\"modal-body\">\n                        <div class=\"cont-wrap\">\n                            <el-checkbox :indeterminate=\"isIndeterminate\" v-model=\"assetFieldsCheckAll\" @change=\"handleCheckAllChange\" style=\"margin-bottom: 16px;\">Check all</el-checkbox>\n                            <el-checkbox-group class=\"group-box\" v-model=\"checkedFieldsName\" @change=\"handleCheckedFieldsChange\">\n                                <el-checkbox v-for=\"field in typeFields\" :label=\"field.name\" :key=\"field.name\">{{field.label}}</el-checkbox>\n                            </el-checkbox-group>\n                        </div>\n                    </div>\n                </modal>\n                <!---//\uD544\uB4DC \uAD6C\uC131 \uC124\uC815--->\n                \n            </div>\n            ";
                        this._$element = $(template);

                        if (this._$parentView == null) {
                              console.log("The parentView must be set before calling the create() method in the Extension Panel.");
                              return;
                        }

                        this._$parentView.append(this._$element);

                        this.assetComponentProxy = this._facade.retrieveProxy(AssetComponentProxy.NAME); //==dcimManager.assetManager
                        // 트리 목록에서 그룹 목록만 구해오기

                        this._app = new Vue({
                              el: this._$element[0],
                              data: {
                                    lang: Vue.$i18n.messages.wv.dcim_pack,
                                    activeLayer: this.activeLayer,
                                    activeLayerModel: this.activeLayer,
                                    proxy: this.assetComponentProxy,
                                    assets2DCompObj: {},
                                    assets3DCompObj: {},
                                    currentComps: [],
                                    selectedType: "",
                                    selectedTypeComps: [],
                                    pagination: {
                                          page: 1,
                                          totalPage: 1
                                    },
                                    sort: {
                                          field: "",
                                          state: ""
                                    },
                                    search: {
                                          field: "",
                                          keyword: "",
                                          onlyUnpositioned: true
                                    },
                                    assetTypes: [],
                                    selectedCompItem: null,
                                    parent: this,
                                    optionsResult: [],
                                    ///검색 결과 전체 자산
                                    currentPageAssets: [],
                                    //검색결과 전체에서 현재 페이지부분만
                                    isPreviewError: false,
                                    updateAssetListTimer: null,
                                    typeFields: [],
                                    ///선택한 타입의 필드 정보
                                    assetFieldsCheckAll: false,
                                    checkedFieldsName: [],
                                    checkedFields: [],
                                    isIndeterminate: true
                              },
                              computed: {
                                    //컴포넌트 미리보기 이미지 경로 가져오기
                                    compPreviewPath: function compPreviewPath() {
                                          var url = "";

                                          switch (this.activeLayer) {
                                                case AssetComponentPanel.LAYERS.TWO_LAYER:
                                                      try {
                                                            var previewPath;

                                                            if (this.selectedCompItem.data.props.preview_path) {
                                                                  previewPath = this.selectedCompItem.data.props.preview_path;
                                                            } else {
                                                                  previewPath = this.selectedCompItem.file_path;
                                                                  var tempAry = previewPath.split("/");
                                                                  tempAry.pop();
                                                                  url = tempAry.join("/") + "/preview.png";
                                                            }
                                                      } catch (error) {}

                                                      break;

                                                case AssetComponentPanel.LAYERS.THREE_LAYER:
                                                      try {
                                                            var rootPath = this.selectedCompItem.data.props.initProperties.props.resource.modeling["default"];
                                                            var objPath = rootPath.path || rootPath.objPath;
                                                            url = objPath.slice(0, objPath.lastIndexOf(".obj")) + "-P.png";
                                                            url = window.wemb.configManager.context + url;
                                                      } catch (error) {}

                                                      break;

                                                default:
                                          }

                                          this.isPreviewError = false;
                                          return url;
                                    }
                              },
                              watch: {
                                    search: {
                                          handler: function handler(newValue, oldValue) {
                                                this.changeAssetsByOptions();
                                          },
                                          deep: true
                                    },
                                    sort: {
                                          handler: function handler() {
                                                this.changeAssetsByOptions();
                                          },
                                          deep: true
                                    }
                              },
                              mounted: function mounted() {},
                              methods: {
                                    //자산 타입(파일명) 첫번째 글자 대문자로 변경하기
                                    firstLetterToUpperCase: function firstLetterToUpperCase(assetTypeFileName) {
                                          return assetTypeFileName.charAt(0).toUpperCase() + assetTypeFileName.slice(1);
                                    },

                                    /*필드설정 모달 관련*/
                                    handleCheckAllChange: function handleCheckAllChange(val) {
                                          this.checkedFieldsName.splice(0, this.checkedFieldsName.length);
                                          var typeFieldsName = this.typeFields.map(function (x) {
                                                return x.name;
                                          });

                                          if (val) {
                                                var _this$checkedFieldsNa;

                                                (_this$checkedFieldsNa = this.checkedFieldsName).push.apply(_this$checkedFieldsNa, _toConsumableArray(typeFieldsName));
                                          }

                                          this.isIndeterminate = false;
                                    },
                                    handleCheckedFieldsChange: function handleCheckedFieldsChange(value) {
                                          var checkedCount = value.length;
                                          this.assetFieldsCheckAll = checkedCount === this.typeFields.length;
                                          this.isIndeterminate = checkedCount > 0 && checkedCount < this.typeFields.length;
                                    },

                                    /*
                                    2018.11.09(ckkim)
                                    데이터 다시 부르기
                                    현재 사용하지 않음.
                                    handleClickRefreshAssetData() {
                                        wemb.dcimManager.refreshAssetAllData();
                                    },
                                     */
                                    saveFieldSettings: function saveFieldSettings() {
                                          var _this2 = this,
                                                _this$checkedFields;

                                          this.checkedFields.splice(0, this.checkedFields.length);
                                          var list = this.typeFields.filter(function (x) {
                                                return _this2.checkedFieldsName.indexOf(x.name) != -1;
                                          });

                                          (_this$checkedFields = this.checkedFields).push.apply(_this$checkedFields, _toConsumableArray(list));
                                    },
                                    showModal: function showModal() {
                                          this.$modal.show('asset-panel-field-settings');
                                    },
                                    hideModal: function hideModal() {
                                          if (!this.checkedFieldsName.length) {
                                                this.$message.error(this.lang.assetComponentPanel.selectField);
                                          } else {
                                                this.$modal.hide('asset-panel-field-settings');
                                          }
                                    },

                                    /*//필드설정 모달 관련*/
                                    onErrorPreviewImage: function onErrorPreviewImage() {
                                          this.isPreviewError = true;
                                    },
                                    clearSearchStr: function clearSearchStr() {
                                          this.search.keyword = "";
                                    },
                                    onChangeActiveLayer: function onChangeActiveLayer() {
                                          this.$emit(AssetComponentPanel.EVENT_CHANGE_ACTIVE_LAYER, this.activeLayerModel);
                                    },
                                    onChangeSortField: function onChangeSortField() {
                                          this.sort.state = "";
                                    },
                                    onChangeSearchField: function onChangeSearchField() {
                                          this.search.keyword = "";
                                    },
                                    changeActiveLayer: function changeActiveLayer(layer) {
                                          if (layer != this.activeLayer) {
                                                this.activeLayer = layer;
                                                this.activeLayerModel = this.activeLayer;

                                                this._updateSelectedTypeComps(this.selectedType);
                                          }
                                    },
                                    setAssetList: function () {
                                          var _setAssetList = _asyncToGenerator(
                                                /*#__PURE__*/
                                                regeneratorRuntime.mark(function _callee() {
                                                      return regeneratorRuntime.wrap(function _callee$(_context) {
                                                            while (1) {
                                                                  switch (_context.prev = _context.next) {
                                                                        case 0:
                                                                              this._updateAssetTypes();

                                                                              this._updateAssetCompsData();

                                                                              this.initSelectedType();
                                                                              this.changeSelectedType();

                                                                        case 4:
                                                                        case "end":
                                                                              return _context.stop();
                                                                  }
                                                            }
                                                      }, _callee, this);
                                                }));

                                          function setAssetList() {
                                                return _setAssetList.apply(this, arguments);
                                          }

                                          return setAssetList;
                                    }(),
                                    initSelectedType: function initSelectedType() {
                                          if (this.assetTypes.length) {
                                                this.selectedType = this.assetTypes[0].id;
                                          }
                                    },

                                    /*자산타입 업데이트 */
                                    _updateAssetTypes: function _updateAssetTypes() {
                                          var _this$assetTypes;

                                          this.assetTypes.splice(0, this.assetTypes.length);

                                          (_this$assetTypes = this.assetTypes).push.apply(_this$assetTypes, _toConsumableArray(this.proxy.assetTypes));
                                    },

                                    /*자산 컴포넌트 데이터 업데이트*/
                                    _updateAssetCompsData: function _updateAssetCompsData() {
                                          this.assets2DCompObj = this.proxy.assetCompsInfo.comp2d;
                                          this.assets3DCompObj = this.proxy.assetCompsInfo.comp3d;
                                    },

                                    /*타입 변경 */
                                    changeSelectedType: function changeSelectedType() {
                                          this._updateSelectedTypeComps();

                                          this._updateSelectedTypeFields();

                                          this._updateSelectedTypeAssetData();
                                    },

                                    /*update step1 선택한 타입의 컴포넌트 목록 업데이트 */
                                    _updateSelectedTypeComps: function _updateSelectedTypeComps() {
                                          this.selectedCompItem = null;
                                          this.selectedTypeComps.splice(0, this.selectedTypeComps.length);
                                          var targetCompObj = this.assets3DCompObj;

                                          if (this.activeLayer != AssetComponentPanel.LAYERS.THREE_LAYER) {
                                                targetCompObj = this.assets2DCompObj;
                                          }

                                          if (targetCompObj[this.selectedType]) {
                                                var _this$selectedTypeCom;

                                                (_this$selectedTypeCom = this.selectedTypeComps).push.apply(_this$selectedTypeCom, _toConsumableArray(targetCompObj[this.selectedType]));
                                          }

                                          if (this.selectedTypeComps.length) {
                                                this.selectedCompItem = this.selectedTypeComps[0];
                                          }
                                    },

                                    /*update step2 */
                                    _updateSelectedTypeFields: function _updateSelectedTypeFields() {
                                          var _this$typeFields, _this$checkedFields2;

                                          var commonFields = this.proxy.visibleFields.common; //commonFields = commonFields.filter(x => x.visible == true);

                                          var typeFields = this.proxy.getTypeFields(this.selectedType, true);
                                          this.typeFields.splice(0, this.typeFields.length);

                                          (_this$typeFields = this.typeFields).push.apply(_this$typeFields, _toConsumableArray(typeFields));

                                          this.checkedFields.splice(0, this.checkedFields.length);

                                          (_this$checkedFields2 = this.checkedFields).push.apply(_this$checkedFields2, _toConsumableArray(commonFields));

                                          this.checkedFieldsName = this.checkedFields.map(function (x) {
                                                return x.name;
                                          });
                                          this.sort.field = this.typeFields[0] ? this.typeFields[0].name : '';
                                          this.sort.state = "";
                                          this.search.field = this.typeFields[0] ? this.typeFields[0].name : '';
                                          this.search.keyword = "";
                                    },

                                    /*update step3 */
                                    _updateSelectedTypeAssetData: function _updateSelectedTypeAssetData() {
                                          this.changeAssetsByOptions();
                                    },
                                    getAssetsDataManager: function getAssetsDataManager() {
                                          return this.proxy.assetsDataManager;
                                    },
                                    changeAssetsByOptions: function changeAssetsByOptions() {
                                          var _this3 = this;

                                          clearTimeout(this.updateAssetListTimer);
                                          this.updateAssetListTimer = setTimeout(function () {
                                                _this3.updateAssetsByOptions();

                                                _this3.changeCurrentPage(1);
                                          }, 10);
                                    },
                                    refreshCurrentPageAssets: function refreshCurrentPageAssets() {
                                          this.updateAssetsByOptions();
                                          this.changeCurrentPage(this.pagination.page);
                                    },
                                    updateAssetsByOptions: function updateAssetsByOptions() {
                                          var _this$optionsResult;

                                          this.optionsResult.splice(0, this.optionsResult.length);

                                          (_this$optionsResult = this.optionsResult).push.apply(_this$optionsResult, _toConsumableArray(this.getAssetsDataManager().getDataByOptions(this.selectedType, {
                                                search: this.search,
                                                sort: this.sort
                                          })));
                                    },
                                    changeCurrentPage: function changeCurrentPage(page) {
                                          var _this$currentPageAsse;

                                          var currentPageData = this.getAssetsDataManager().getPaginationData(this.optionsResult, page);
                                          this.currentPageAssets.splice(0, this.currentPageAssets.length);

                                          (_this$currentPageAsse = this.currentPageAssets).push.apply(_this$currentPageAsse, _toConsumableArray(currentPageData.data));

                                          this.pagination.page = currentPageData.page;
                                          this.pagination.totalPage = currentPageData.totalPage;
                                    },
                                    nextPage: function nextPage() {
                                          var page = Math.min(this.pagination.totalPage, this.pagination.page + 1);

                                          if (this.pagination.page != page) {
                                                this.changeCurrentPage(page);
                                          }
                                    },
                                    prevPage: function prevPage() {
                                          var page = Math.max(1, this.pagination.page - 1);

                                          if (this.pagination.page != page) {
                                                this.changeCurrentPage(page);
                                          }
                                    },
                                    onDragStart: function onDragStart(event, assetData) {
                                          if (!assetData || !this.selectedCompItem) {
                                                return;
                                          }
                                          /*  1. 컴포넌트 기본 정보
                                                    2. asset 정보
                                                    3. 컴포넌트 환경 정보*/

                                          /* 1. 컴포넌트 기본 정보
                                                    : 선택한 컴포넌트에 이미 존재 */

                                          /* 2018.12.12 (ckkim) name(Component)에서 id로 수정 */
                                          //var comInstanceInfo = wemb.componentLibraryManager.getComponentPanelInfoByName("3D", this.selectedCompItem.data.name);


                                          try {
                                                var comInstanceMetaInfo = wemb.componentLibraryManager.getComponentPanelInfoByLabel("three_layer", this.selectedCompItem.data.file_id);

                                                if (!comInstanceMetaInfo) {
                                                      alert(this.lang.assetComponentPanel.noComponent);
                                                      return;
                                                }

                                                comInstanceMetaInfo = $.extend(true, {}, comInstanceMetaInfo);
                                                comInstanceMetaInfo.category = "3D";
                                                wemb.globalStore.set("__drag_asset_data__", assetData);
                                                event.dataTransfer.effectAllowed = 'move';

                                                if (CPUtil.getInstance().browserDetection() != "Internet Explorer") {
                                                      event.dataTransfer.setData('text/plane', JSON.stringify(comInstanceMetaInfo));
                                                } // 3. 컴포넌트 환경 정보


                                                window["dragData"] = JSON.stringify(comInstanceMetaInfo);
                                          } catch (error) {
                                                console.log("Error, The asset id does not exist.", error);
                                          }
                                    }
                              }
                        });
                        this.bindAppEvent();
                        return this._app;
                  }
            }, {
                  key: "bindAppEvent",
                  value: function bindAppEvent() {
                        var _this4 = this;

                        this._app.$on(AssetComponentPanel.EVENT_CHANGE_ACTIVE_LAYER, function (layerName) {
                              _this4.assetComponentProxy.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, layerName);
                        });
                  }
            }, {
                  key: "setAssetData",
                  value: function setAssetData() {
                        this._app.setAssetList();
                  }
                  /*
                    noti가 온 경우 실행
                    call : mediator에서 실행
                     */

            }, {
                  key: "handleNotification",
                  value: function handleNotification(note) {
                        switch (note.name) {
                              case EditorProxy.NOTI_OPEN_PAGE:
                                    console.log("info_, AssetComponentPanel NOTI_OPEN_PAGE");

                                    if (!this.isLoaded) {
                                          this.setAssetData();
                                    }

                                    this.isLoaded = true;

                                    this._app.changeActiveLayer(this.activeLayer);

                                    break;

                              case EditorProxy.NOTI_CLOSED_PAGE:
                                    this._app.changeActiveLayer("");

                                    break;

                              case EditorProxy.NOTI_CHANGE_ACTIVE_LAYER:
                                    switch (note.body) {
                                          case AssetComponentPanel.LAYERS.THREE_LAYER:
                                                this.activeLayer = AssetComponentPanel.LAYERS.THREE_LAYER;
                                                break;

                                          case AssetComponentPanel.LAYERS.TWO_LAYER:
                                          case AssetComponentPanel.LAYERS.MASTER_LAYER:
                                                this.activeLayer = AssetComponentPanel.LAYERS.TWO_LAYER;
                                                break;

                                          default:
                                                this.activeLayer = "";
                                    }

                                    this._app.changeActiveLayer(this.activeLayer);

                                    break;

                              case AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST:
                                    this._app.refreshCurrentPageAssets();

                                    break;
                        }
                  }
            }, {
                  key: "notificationList",
                  get: function get() {
                        return [EditorProxy.NOTI_OPEN_PAGE, EditorProxy.NOTI_CLOSED_PAGE, EditorProxy.NOTI_CHANGE_ACTIVE_LAYER, AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST];
                  }
            }]);

            return AssetComponentPanel;
      }(ExtensionPanelCore);

AssetComponentPanel.LAYERS = {
      MASTER_LAYER: "masterLayer",
      TWO_LAYER: "twoLayer",
      THREE_LAYER: "threeLayer"
};
AssetComponentPanel.EVENT_CHANGE_ACTIVE_LAYER = "event/changeActiveLayer";
