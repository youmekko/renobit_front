/***
 * 자산 배치 상태를 관리하는 allocationProxy와 연동하여 outline구성
 * outline특성상 페이지네이션이 어울리지 않아 전체 목록을 리스트로 구성하는데, 이 때문에 하나의 속성정보만으로 표기한다.
 *    기본 표기 속성은 ID인데 이것을 타입별로 변경할 수 있다.(타입별로 필드 구성이 다르기 때문에..)
 *
 * TODO : 컴포넌트 패널과 마찬가지로 표기하고자 하는 속성 정보를 사용자가 변경하면 이를 localstorage로 저장하여 관리해줄 필요가 있음.
 **/
class AssetOutlinePanel extends ExtensionPanelCore {

    constructor() {
        super();
        this.assetComponentProxy = null;
        this._app = null;
        this.isLoaded = false;
    }

    /*
          call :
                인스턴스 생성 후 실행
                단, 아직 화면에 붙지 않은 상태임.

           */
    create() {

        let template =
            `
      <div data-id="dcim-assets-outline-panel" v-if="isActive">     
            <!--타입션택-->        
            <div class="asset-types">
                  <el-select class="mini flex select-asset-type" 
                              :disabled="assetTypes.length==0" 
                              v-model="selectedType" placeholder="Select Asset Type" size="mini"
                              @change="changeSelectedType">
                        <el-option
                              label="All"
                              value="">
                        </el-option>
                        <el-option
                              v-for="item in assetTypes"
                              :key="item.id"
                              :label="item.name"
                              :value="item.id">
                        </el-option>
                  </el-select>
                  <el-button size="mini" @click="showModal" type="primary" class="icon-btn"><i class="el-icon-edit-outline"></i></el-button>
            </div>   
            <!--//타입션택-->   
            <!--검색 필드-->
            <div class="asset-search">
                  <el-select class="mini select-search-type" 
                        v-model="search.field" placeholder="searh field" size="mini"
                  >
                        <el-option
                              v-for="item in typeFields"
                              :key="item.name"
                              :label="item.label"
                              :value="item.name">
                        </el-option>
                  </el-select>
                  <el-input class="search-field" v-model="search.keyword" placeholder="search keyword">
                        <i v-if="search.keyword.length" @click="clearSearchStr()" class="clear-search-str el-icon-error el-input__icon" slot="suffix"></i>
                  </el-input>
            </div>
            <!--//검색 필드-->

            <div class="asset-list flex-scroll" v-if="!selectedType && assetSearchResult.length">
                  <el-collapse v-model="activeNames" @change="handleChange">
                        <template v-for="type in assetTypes">
                              <el-collapse-item :name="type.id" v-if="typeAssetObj[type.id] && typeAssetObj[type.id].length">
                                    <template slot="title">{{type.name}}<em class="tag">{{typeAssetObj[type.id].length}}</em></template>                                    
                                    <dl v-for="assetCompositionInfo in typeAssetObj[type.id]"                                           
                                          :class="getStateClass(assetCompositionInfo.selected, assetCompositionInfo.assetInfo.valid_state)"
                                          @click="onClickAssetItem(assetCompositionInfo)">
                                          <dt>
                                          <el-tooltip class="item" effect="dark" content="중복 배치된 자산입니다." placement="top-start">
                                                <i class="el-icon-warning icon double-asset"></i>
                                          </el-tooltip>
                                          <el-tooltip class="item" effect="dark" content="삭제된 자산입니다." placement="top-start">
                                                <i class="el-icon-warning icon none-asset"></i>
                                          </el-tooltip>
                                          {{showTypeFieldId[type.id].label}} : </dt>
                                          <dd>{{getDataByFieldName(assetCompositionInfo.assetInfo, type.id )}}</dd>
                                    </dl>                                    
                              </el-collapse-item>
                        </template>
                  </el-collapse>
            </div>
            <div class="asset-list flex-scroll" v-if="selectedType && selectedTypeAssets.length">
                  <dl   v-for="assetCompositionInfo in selectedTypeAssets"
                        :class="getStateClass(assetCompositionInfo.selected, assetCompositionInfo.assetInfo.valid_state)"
                        @click="onClickAssetItem(assetCompositionInfo)"
                        >
                        <dt>
                          <el-tooltip class="item" effect="dark" content="중복 배치된 자산입니다." placement="top-start">
                              <i class="el-icon-warning icon double-asset"></i>
                        </el-tooltip>
                        <el-tooltip class="item" effect="dark" content="삭제된 자산입니다." placement="top-start">
                              <i class="el-icon-warning icon none-asset"></i>
                        </el-tooltip>
                        {{showTypeFieldId[selectedType].label}} : </dt><dd>{{getDataByFieldName(assetCompositionInfo.assetInfo, selectedType )}}</dd></dl>
            </div>
            <div class="no-data" v-if="(!selectedType && !assetSearchResult.length) || selectedType && !selectedTypeAssets.length">
                  NO DATA
            </div>

             <!---필드 구성 설정--->
            <modal
                  class="w-modal"
                  id="asset-outline-field-settings"
                  name="asset-outline-field-settings"
                  :click-to-close="false"
                  @opened="openedModal"
                  @closed="cloasedModal"
                  :draggable="false"
                  :resizable="true"
                  :width="480"
                  :height="280"
                  :min-width="480"
                  :min-height="280"

            >
                  <div class="modal-header"><h4>Set up asset fields visible by type</h4><a class="close-modal-btn" role="button"
                                                                              @click="hideModal"><i
                  class="el-icon-error"></i></a></div>
                  <div class="modal-body">
                        <div class="cont-wrap">
                              <div v-for="type in assetTypes" class="radio-wrap">
                                    <h5>{{type.name}}</h5>
                                    <el-radio-group v-model="showTypeFieldId[type.id]">
                                          <el-radio v-for="item in typesFields[type.id]" :label="item">{{item.label}}</el-radio>
                                    </el-radio-group>
                              </div>
                        </div>
                  </div>
            </modal>
            <!---//필드 구성 설정--->
       </div>
      `;
        /*`
		<div style="overflow: auto;padding:8px;">
		<div>자산 아웃라인 패널</div>
		<div class="component-thumb-list">
			<ul class="list">
				<li class="component" v-for="item in assetCompositionInfoList2" >
					<div class="img-wrap"draggable="true">
						{{item.name}}
					</div>
				</li>
			</ul>
		</div>
		</div>`*/

        this._$element = $(template);

        if (this._$parentView == null) {
            console.log("The parentView must be set before calling the create () method in the Extension Panel.")
            return;
        }


        /*
		중요:
		Extension Panel에서 create() 메서드 호출 전 parentView가 설정되어 있어야 합니다.
		*/
        this._$parentView.append(this._$element);
        this.assetComponentProxy = this._facade.retrieveProxy(AssetComponentProxy.NAME);
        // this.allocationProxy = this._facade.retrieveProxy(AssetAllocationProxy.NAME);

        // 트리 목록에서 그룹 목록만 구해오기
        var that = this;
        this._app = new Vue({
            el: this._$element[0],
            data: {
                parent: this,
                proxy: this.assetComponentProxy,
                allocationProxy: this.allocationProxy,
                search: { field: "", keyword: "" },
                selectedType: "",
                assetTypes: [],
                typeFields: [], ///선택한 타입의 필드 정보
                typesFields: {}, //{sensor: []}타입별 전체 필드
                showTypeFieldId: {}, // { cctv: "id", pdu: "id", sensor: "id" }, //{sensor : "id"}타입별 노출할 필드 정보
                commonFields: [],
                isActive: false,
                assetCompositionInfoList: [],
                assetSearchResult: [],
                typeAssetObj: {}, //타입별 자산 데이터
                selectedTypeAssets: [], //선택한 타입의 자산 데이터(전체 선택시 데이터 없음)
                activeNames: [],
                searchTimer: null
            },

            watch: {
                search: {
                    handler: function(newValue, oldValue) {
                        this.onChangeSearchOptions();
                    },
                    deep: true
                }
            },

            mounted() {},

            methods: {
                getStateClass(selected, validState) {
                    //ok, none, double
                    //validState = "double";
                    let className = selected ? "active " : '';

                    switch (validState) {
                        case "none":
                            className += "none";
                            break;
                        case "double":
                            className += "double";
                            break;
                    }

                    return className;
                },

                getDataByFieldName(asset, type) {
                    let field = this.showTypeFieldId[type].name;
                    return asset[field] || "";
                },

                handleChange(val) {
                    console.log(val);
                },

                initAssetData() {
                    this._updateSelectedTypeFields();
                    this.updateAssetList();

                },

                active(bool) {
                    if (bool) {
                        this._updateAssetTypes();
                        this._updateTypesFields();
                    }
                    this.isActive = bool;
                },

                showModal() {
                    this.$modal.show('asset-outline-field-settings');
                },

                openedModal() {},

                hideModal() {
                    this.$modal.hide('asset-outline-field-settings');
                },

                cloasedModal() {

                },

                clearSearchStr() {
                    this.search.keyword = "";
                },

                getTypeFields(type) {
                    return this.proxy.getTypeFields(type, true);
                },



                /*자산타입 업데이트 */
                _updateAssetTypes() {
                    this.assetTypes.splice(0, this.assetTypes.length);
                    this.assetTypes.push(...this.proxy.assetTypes);
                    this.activeNames.splice(0, this.activeNames.length);
                    this.activeNames.push(...this.assetTypes.map(x => x.id));
                },

                _updateTypesFields() {
                    this.typesFields = {};
                    this.showTypeFieldId = {};
                    for (let i in this.assetTypes) {
                        let type = this.assetTypes[i].id;
                        let fields = this.getTypeFields(type);
                        let defaultField = fields.find(x => x.name == "id");
                        if (!this.showTypeFieldId[type]) {
                            Vue.set(this.showTypeFieldId, type, defaultField);
                        }

                        this.typesFields[type] = fields;
                    }
                },

                /*타입 변경 */
                changeSelectedType() {
                    this._updateSelectedTypeFields();
                    this.updateSelectedTypeAsset();
                },

                /*update step2 */
                _updateSelectedTypeFields() {
                    this.commonFields = this.proxy.visibleFields.common;
                    //this.commonFields = this.commonFields.filter(x => x.visible == true);
                    this.typeFields.splice(0, this.typeFields.length);

                    if (this.selectedType == "") {
                        this.typeFields.push(...this.commonFields);
                    } else {
                        let typeFields = this.getTypeFields(this.selectedType);
                        this.typeFields.push(...typeFields);
                    }

                    this.search.field = this.typeFields[0].name;
                    this.search.keyword = "";
                },

                updateAssetList() {
                      let list = that._facade.retrieveProxy(AssetAllocationProxy.NAME).assetCompositionInfoList || [];
                      let converted = list.map((data)=>(
                            {
                                  assetId : data.assetId,
                                  assetInfo : data.assetInfo,
                                  assetType : data.assetType,
                                  comInstanceId : data.comInstanceId,
                                  comInstanceName : data.comInstanceName,
                                  selected : data.comInstance.selected
                            }
                      ));

                    // 배열을 초기화 하는 기능
                    this.assetCompositionInfoList = [...converted];
                    this.searchAssets();
                },

                sortTypeAsset() {
                    this.typeAssetObj = {};
                    for (let i in this.assetTypes) {
                        let type = this.assetTypes[i].id;
                        let typeAsset = this.assetSearchResult.filter(x => x.assetInfo.asset_type == type);
                        this.typeAssetObj[type] = typeAsset;
                    }
                },

                updateSelectedTypeAsset() {
                    this.selectedTypeAssets.splice(0, this.selectedTypeAssets.length);
                    if (this.selectedType) {
                        this.selectedTypeAssets.push(...this.typeAssetObj[this.selectedType]);
                    }
                },

                onChangeSearchOptions() {
                    clearTimeout(this.searchTimer);
                    this.searchTimer = setTimeout(() => {
                        this.searchAssets();
                    }, 30);
                },

                searchAssets() {
                    //this.search.keyword = "";
                    let list = this.assetCompositionInfoList;
                    let keword = this.search.keyword.trim();
                    if (keword) {
                        list = list.filter((assetCompositionInfo) => {
                            let assetInfo = assetCompositionInfo.assetInfo;
                            let fieldValue = assetInfo[this.search.field];
                            if (fieldValue && fieldValue.indexOf(this.search.keyword) != -1) {
                                return assetCompositionInfo;
                            }
                        });
                    }


                    this.assetSearchResult.splice(0, this.assetSearchResult.length);
                    this.assetSearchResult.push(...list);
                    this.sortTypeAsset();
                    this.updateSelectedTypeAsset();
                },

                onClickAssetItem(assetCompositionInfo) {

                    if (wemb.editorProxy.activeLayerInfo.name != "threeLayer") {
                        this.parent._facade.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, "threeLayer");
                    }
                     let component_instance = wemb.mainPageComponent.getComInstanceByName(assetCompositionInfo.comInstanceName);

                    try {
                        this.parent._facade.sendNotification(EditorStatic.CMD_NEW_SELECTED_COMPONENT_INSTANCE, component_instance);
                    } catch (error) {
                        console.log("AssetOutlinePanel, Error when moving to asset component by asset click", component_instance)
                    }
                }
            }
        })


        return this._app;

    }


    get notificationList() {
        return [
            EditorProxy.NOTI_OPEN_PAGE,
            EditorProxy.NOTI_CLOSED_PAGE,
            EditorProxy.NOTI_CHANGE_ACTIVE_LAYER,
            AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST,
            EditorProxy.NOTI_UPDATE_COM_INSTANCE
        ]
    }

    setAssetData() {
        this._app.initAssetData();
    }

    /*
	noti가 온 경우 실행
	call : mediator에서 실행
	*/
    handleNotification(note) {
        switch (note.name) {
            case EditorProxy.NOTI_OPEN_PAGE:
                this._app.active(true);
                if (!this.isLoaded) {
                    this.setAssetData();
                    this.isLoaded = true;
                }

                break;
            case EditorProxy.NOTI_CLOSED_PAGE:
                this._app.active(false);
                break;
            case AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST:
            case EditorProxy.NOTI_UPDATE_COM_INSTANCE:
                this.noti_UpdateAssetAllocationList();
                break;
        }
    }


    noti_UpdateAssetAllocationList() {
        if (this.isLoaded) {
            this._app.updateAssetList();
        }
    }
}
