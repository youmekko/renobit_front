"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DCIMComponentPropertyTemplateRegister =
      /*#__PURE__*/
      function () {
            function DCIMComponentPropertyTemplateRegister() {
                  _classCallCheck(this, DCIMComponentPropertyTemplateRegister);
            }

            _createClass(DCIMComponentPropertyTemplateRegister, null, [{
                  key: "regist",
                  value: function regist() {
                        if (DCIMComponentPropertyTemplateRegister.init == true) return;
                        DCIMComponentPropertyTemplateRegister.init = true;

                        DCIMComponentPropertyTemplateRegister._createAssetTypeGroupListTemplate();

                        DCIMComponentPropertyTemplateRegister._createAssetIdTemplate();

                        DCIMComponentPropertyTemplateRegister._createAssetPopupInfoTemplate();

                        DCIMComponentPropertyTemplateRegister._createRackMountTemplate();

                        DCIMComponentPropertyTemplateRegister._createPduMountTemplate();
                  }
            }, {
                  key: "_createAssetTypeGroupListTemplate",
                  value: function _createAssetTypeGroupListTemplate() {
                        wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-asset-typeset");
                        Vue.component('dcim-asset-typeset-template', {
                              "extends": CoreTemplate,
                              template: " \n                  <div class=\"prop-group\">\n                        <h5 class=\"header\">Asset Type Info</h5>\n                        <div class=\"body\">\n                              <div class=\"d-flex row\">\n                                    <span class=\"label\">Group Name</span>\n                                    <div class=\"flex d-flex\" v-if=\"groupList && groupList.length\">\n                                          <el-select class=\"select flex\" \n                                          v-model=\"typeId\" \n                                          @change=\"onChangeTypeGroupName\">\n                                          <el-option v-for=\"item of groupList\" \n                                                :value=\"item.name\" :label=\"item.name\"\n                                                :key=\"item.name\">                                         \n                                          </el-option>\n                                          </el-select>\n                                    </div>\n                              \n                                    <div v-else>\n                                          {{lang.componentPropertyTemplateRegister.noTypeInfo}}\n                                    </div>\n                              </div>\n                        </div>\n                  </div>\n            ",
                              mounted: function mounted() {},
                              data: function data() {
                                    return {
                                          lang: Vue.$i18n.messages.wv.dcim_pack,
                                          typeId: ""
                                    };
                              },
                              computed: {
                                    groupList: function groupList() {
                                          return window.wemb.dcimManager.typesetManager.getAssetTypeGroupList();
                                    }
                              },
                              methods: {
                                    updatePropertyInfo: function updatePropertyInfo() {
                                          if (!this.model.typeInfo.typeGroupName && this.groupList.length) {
                                                this.typeId = this.groupList[0].id;
                                          } else {
                                                this.typeId = this.model.typeInfo.typeGroupName;
                                          }
                                    },
                                    onChangeTypeGroupName: function onChangeTypeGroupName(value) {
                                          this.dispatchChangeEvent("typeInfo", "typeGroupName", value);
                                    }
                              }
                        });
                  }
                  /*자산 팝업*/

            }, {
                  key: "_createAssetPopupInfoTemplate",
                  value: function _createAssetPopupInfoTemplate() {
                        wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-asset-popup");
                        Vue.component('dcim-asset-popup-template', {
                              "extends": CoreTemplate,
                              template: " \n                  <div class=\"prop-group\">\n                        <h5 class=\"header\">Asset Popup Info</h5>\n                        <div class=\"body\">\n                              <div class=\"row\">\n                                        <el-select style=\"margin-bottom: 4px; display:block;\"\n                                            v-model=\"useCustomPopup\" \n                                            placeholder=\"Select option\"\n                                            @change=\"onChangeUseCustomPopup\">\n                                            <el-option\n                                            v-for=\"item in options\"\n                                            :key=\"item.value\"\n                                            :label=\"item.label\"\n                                            :value=\"item.value\">\n                                            </el-option>\n                                        </el-select>\n\n                                        <el-select v-if=\"useCustomPopup\" \n                                            style=\"display:block;\"\n                                            v-model=\"override_settings.id\" \n                                            placeholder=\"Select Page\"\n                                            @change=\"onChangeCustomPopup\"\n                                            filterable\n                                            >\n                                            \n                                            <el-option\n                                            v-for=\"item in pages\"\n                                            :key=\"item.id\"\n                                            :label=\"item.name\"\n                                            :value=\"item.id\">\n                                            </el-option>\n                                        </el-select>\n                                         <el-input                                  \n                                         v-else v-model=\"settings.name\" :disabled=\"true\" :readonly=\"true\"></el-input>\n                                    </div>\n                              </div>\n                        </div>\n                  </div>\n            ",
                              mounted: function mounted() {},
                              data: function data() {
                                    return {
                                          options: [{
                                                value: false,
                                                label: 'Reference Popup'
                                          }, {
                                                value: true,
                                                label: 'Custom Popup'
                                          }],
                                          useCustomPopup: false,
                                          settings: {
                                                id: '',
                                                name: ''
                                          },
                                          override_settings: {
                                                id: '',
                                                name: ''
                                          },
                                          pages: []
                                    };
                              },
                              computed: {},
                              created: function created() {
                                    this.updatePropertyInfo();
                              },
                              methods: {
                                    updatePropertyInfo: function updatePropertyInfo() {
                                          /*{
                                              override_settings:{
                                                "popupId": ''
                                              }
                                          }*/
                                          this.pages = window.wemb.pageManager.getPageInfoList();
                                          this.override_settings.id = this.model.override_settings.popupId;
                                          this.override_settings.name = this._getPageName(this.override_settings.id);

                                          if (this.override_settings.id) {
                                                this.useCustomPopup = true;
                                          } else {
                                                this.useCustomPopup = false;
                                          }

                                          this.settings.id = this.model.settings.popupId;
                                          this.settings.name = this._getPageName(this.settings.id);
                                    },
                                    _getPageName: function _getPageName(pageId) {
                                          var name = '';
                                          var page = this.pages.find(function (x) {
                                                return x.id == pageId;
                                          });

                                          if (page) {
                                                name = page.name;
                                          }

                                          return name;
                                    },
                                    onChangeUseCustomPopup: function onChangeUseCustomPopup() {
                                          if (this.useCustomPopup && this.pages && this.pages.length) {
                                                this.override_settings.id = this.pages[0].id;
                                                this.override_settings.name = this.pages[0].name;
                                          } else {
                                                this.override_settings.id = '';
                                                this.override_settings.name = '';
                                          }

                                          this.onChangeCustomPopup();
                                    },
                                    onChangeCustomPopup: function onChangeCustomPopup(value) {
                                          this.dispatchChangeEvent("override_settings", "popupId", this.override_settings.id);
                                    }
                              }
                        });
                  }
                  /*자산 ID*/

            }, {
                  key: "_createAssetIdTemplate",
                  value: function _createAssetIdTemplate() {
                        wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-asset-id");
                        Vue.component('dcim-asset-id-template', {
                              "extends": CoreTemplate,
                              template: " \n                  <div class=\"prop-group\">\n                        <h5 class=\"header\">Asset ID</h5>\n                        <div class=\"body\">\n                              <div class=\"d-flex row\">\n                                    <div class=\"flex d-flex\">\n                                          <el-input class=\"flex\" \n                                            :readonly=\"true\"\n                                            :disabled=\"true\"\n                                            v-model=\"assetId\"></el-input>\n                                             <button class=\"icon-btn asset-mapping-btn\" v-if=\"!assetId\"\n                                                @click=\"openAssetMappingManager()\">Open\n                                          </button>\n                                          <button class=\"icon-btn cir-close-btn\" v-if=\"assetId\"\n                                                @click=\"removeAssetMappingData()\">Delete\n                                          </button>\n                                    </div>\n                              </div>\n                        </div>\n                  </div>\n            ",
                              created: function created() {
                                    wemb.$globalBus.$on(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, function () {
                                          this.$forceUpdate();
                                    }.bind(this));
                              },
                              mounted: function mounted() {
                                    this.updatePropertyInfo();
                              },
                              data: function data() {
                                    return {
                                          assetId: ''
                                    };
                              },
                              computed: {},
                              updated: function updated() { this.updatePropertyInfo(); },
                              methods: {
                                    openAssetMappingManager: function openAssetMappingManager() {
                                          var path = window.wemb.configManager.context + "/custom/packs/dcim_pack/manager/view/asset_mapping/DCIMAssetMappingComponent.vue";
                                          var component_instance = wemb.mainPageComponent.getComInstanceByName(this.primary.name);
                                          window.wemb.showExternalModal("AssetMappingManager", path, component_instance);
                                    },
                                    removeAssetMappingData: function removeAssetMappingData() {
                                          // 인스턴스는 그대로고
                                          // false == 인스턴스가 삭제되지 않는 의미
                                          var component_instance = wemb.mainPageComponent.getComInstanceByName(this.primary.name);
                                          wemb.dcimManager.assetAllocationProxy.removeAssetAssetAllocation(component_instance, true, false);
                                    },
                                    updatePropertyInfo: function updatePropertyInfo() {
                                          this.assetId = wemb.mainPageComponent.getComInstanceByName(this.primary.name).assetId;
                                    }
                              },
                              destroyed: function destroyed() {
                                    wemb.$globalBus.$off(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST);
                              }
                        });
                  }
                  /*랙 실장 팝업*/

            }, {
                  key: "_createRackMountTemplate",
                  value: function _createRackMountTemplate() {
                        wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-rack-mount");
                        Vue.component('dcim-rack-mount-template', {
                              "extends": CoreTemplate,
                              template: " \n                  <div class=\"prop-group\">\n                        <h5 class=\"header\">Rack Mount</h5>\n                        <div class=\"body\">\n                              <div class=\"d-flex row\">\n                                    <el-button size=\"small\" class=\"flex\" @click=\"onClickHandler\" type=\"primary\">Open</el-button>\n                              </div>\n                        </div>\n                  </div>\n            ",
                              mounted: function mounted() {},
                              data: function data() {
                                    return {};
                              },
                              computed: {},
                              methods: {
                                    onClickHandler: function onClickHandler() {
                                          var param = {
                                                "modalName": "dcim-rack-mount-manager-popup",
                                                "fileName": window.wemb.configManager.context + "/custom/packs/dcim_pack/manager/view/mount/rack/DCIMRackMountManagerComponent.vue",
                                                "data": wemb.mainPageComponent.getComInstanceByName(this.primary.name).assetId
                                          };
                                          window.wemb.editorFacade.sendNotification(EditorStatic.CMD_SHOW_MODAL, param);
                                    }
                              }
                        });
                  }
                  /*분전반 실장 팝업*/

            }, {
                  key: "_createPduMountTemplate",
                  value: function _createPduMountTemplate() {
                        wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-pdu-mount");
                        Vue.component('dcim-pdu-mount-template', {
                              "extends": CoreTemplate,
                              template: " \n                  <div class=\"prop-group\">\n                        <h5 class=\"header\">PDU Mount</h5>\n                        <div class=\"body\">\n                              <div class=\"d-flex row\">\n                                    <el-button size=\"small\" class=\"flex\" @click=\"onClickHandler\" type=\"primary\">Open</el-button>\n                              </div>\n                        </div>\n                  </div>\n            ",
                              mounted: function mounted() {},
                              data: function data() {
                                    return {};
                              },
                              computed: {},
                              methods: {
                                    onClickHandler: function onClickHandler() {
                                          var param = {
                                                "modalName": "dcim-pdu-mount-manager-popup",
                                                "fileName": window.wemb.configManager.context + "/custom/packs/dcim_pack/manager/view/mount/pdu/DCIMPduMountManagerComponent.vue",
                                                "data": wemb.mainPageComponent.getComInstanceByName(this.primary.name).assetId
                                          };
                                          window.wemb.editorFacade.sendNotification(EditorStatic.CMD_SHOW_MODAL, param);
                                    }
                              }
                        });
                  }
            }]);

            return DCIMComponentPropertyTemplateRegister;
      }();

DCIMComponentPropertyTemplateRegister.init = false;
