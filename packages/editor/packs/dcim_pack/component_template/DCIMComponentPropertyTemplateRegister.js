
class DCIMComponentPropertyTemplateRegister {
      static regist() {
            if (DCIMComponentPropertyTemplateRegister.init == true)
                  return;

            DCIMComponentPropertyTemplateRegister.init = true;
            DCIMComponentPropertyTemplateRegister._createAssetTypeGroupListTemplate();
            DCIMComponentPropertyTemplateRegister._createAssetIdTemplate();
            DCIMComponentPropertyTemplateRegister._createAssetPopupInfoTemplate();
            DCIMComponentPropertyTemplateRegister._createRackMountTemplate();
            DCIMComponentPropertyTemplateRegister._createPduMountTemplate();
      }


      static _createAssetTypeGroupListTemplate() {
            wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-asset-typeset");
            Vue.component('dcim-asset-typeset-template', {
                  extends: CoreTemplate,
                  template: ` 
                  <div class="prop-group">
                        <h5 class="header">Asset Type Info</h5>
                        <div class="body">
                              <div class="d-flex row">
                                    <span class="label">Group Name</span>
                                    <div class="flex d-flex" v-if="groupList && groupList.length">
                                          <el-select class="select flex" 
                                          v-model="typeId" 
                                          @change="onChangeTypeGroupName">
                                          <el-option v-for="item of groupList" 
                                                :value="item.name" :label="item.name"
                                                :key="item.name">                                         
                                          </el-option>
                                          </el-select>
                                    </div>
                              
                                    <div v-else>
                                          {{lang.componentPropertyTemplateRegister.noTypeInfo}}
                                    </div>
                              </div>
                        </div>
                  </div>
            `,

                  mounted: function () {
                  },
                  data: function () {
                        return {
                              lang: Vue.$i18n.messages.wv.dcim_pack,
                              typeId: ""
                        };
                  },

                  computed: {
                        groupList: function () {
                              return window.wemb.dcimManager.typesetManager.getAssetTypeGroupList()
                        }
                  },

                  methods: {
                        updatePropertyInfo() {
                              if (!this.model.typeInfo.typeGroupName && this.groupList.length) {
                                    this.typeId = this.groupList[0].id;
                              } else {
                                    this.typeId = this.model.typeInfo.typeGroupName;
                              }
                        },

                        onChangeTypeGroupName: function (value) {
                              this.dispatchChangeEvent("typeInfo", "typeGroupName", value);
                        }
                  }
            })
      }

      /*자산 팝업*/
      static _createAssetPopupInfoTemplate() {
            wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-asset-popup");
            Vue.component('dcim-asset-popup-template', {
                  extends: CoreTemplate,
                  template: ` 
                  <div class="prop-group">
                        <h5 class="header">Asset Popup Info</h5>
                        <div class="body">
                              <div class="row">
                                        <el-select style="margin-bottom: 4px; display:block;"
                                            v-model="useCustomPopup" 
                                            placeholder="Select option"
                                            @change="onChangeUseCustomPopup">
                                            <el-option
                                            v-for="item in options"
                                            :key="item.value"
                                            :label="item.label"
                                            :value="item.value">
                                            </el-option>
                                        </el-select>

                                        <el-select v-if="useCustomPopup" 
                                            style="display:block;"
                                            v-model="override_settings.id" 
                                            placeholder="Select Page"
                                            @change="onChangeCustomPopup"
                                            filterable
                                            >
                                            
                                            <el-option
                                            v-for="item in pages"
                                            :key="item.id"
                                            :label="item.name"
                                            :value="item.id">
                                            </el-option>
                                        </el-select>
                                         <el-input                                  
                                         v-else v-model="settings.name" :disabled="true" :readonly="true"></el-input>
                                    </div>
                              </div>
                        </div>
                  </div>
            `,

                  mounted: function () {
                  },
                  data: function () {
                        return {
                              options: [{
                                    value: false,
                                    label: 'Reference Popup'
                              }, {
                                    value: true,
                                    label: 'Custom Popup'
                              }],
                              useCustomPopup: false,

                              settings: {
                                    id: '',
                                    name: ''
                              },

                              override_settings: {
                                    id: '',
                                    name: ''
                              },

                              pages: []
                        };
                  },

                  computed: {},

                  created: function () {
                        this.updatePropertyInfo();
                  },

                  methods: {

                        updatePropertyInfo() {
                              /*{
                                  override_settings:{
                                    "popupId": ''
                                  }
                              }*/
                              this.pages = window.wemb.pageManager.getPageInfoList();

                              this.override_settings.id = this.model.override_settings.popupId;
                              this.override_settings.name = this._getPageName(this.override_settings.id);

                              if (this.override_settings.id) {
                                    this.useCustomPopup = true;
                              } else {
                                    this.useCustomPopup = false;
                              }


                              this.settings.id = this.model.settings.popupId;
                              this.settings.name = this._getPageName(this.settings.id);
                        },

                        _getPageName(pageId) {
                              let name = '';
                              let page = this.pages.find(x => x.id == pageId);
                              if (page) {
                                    name = page.name;
                              }
                              return name;
                        },

                        onChangeUseCustomPopup: function () {
                              if (this.useCustomPopup && this.pages && this.pages.length) {
                                    this.override_settings.id = this.pages[0].id;
                                    this.override_settings.name = this.pages[0].name;
                              } else {
                                    this.override_settings.id = '';
                                    this.override_settings.name = '';
                              }

                              this.onChangeCustomPopup();
                        },

                        onChangeCustomPopup: function (value) {
                              this.dispatchChangeEvent("override_settings", "popupId", this.override_settings.id);
                        }
                  }
            })
      }

      /*자산 ID*/
      static _createAssetIdTemplate() {
            wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-asset-id");
            Vue.component('dcim-asset-id-template', {
                  extends: CoreTemplate,
                  template: ` 
                  <div class="prop-group">
                        <h5 class="header">Asset ID</h5>
                        <div class="body">
                              <div class="d-flex row">
                                    <div class="flex d-flex">
                                          <el-input class="flex" 
                                            :readonly="true"
                                            :disabled="true"
                                            v-model="assetId"></el-input>
                                             <button class="icon-btn asset-mapping-btn" v-if="!assetId"
                                                @click="openAssetMappingManager()">Open
                                          </button>
                                          <button class="icon-btn cir-close-btn" v-if="assetId"
                                                @click="removeAssetMappingData()">Delete
                                          </button>
                                    </div>
                              </div>
                        </div>
                  </div>
            `,
                  created(){
                        wemb.$globalBus.$on(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, function(){
                              this.$forceUpdate();
                        }.bind(this));
                  },
                  mounted: function () {
                        this.updatePropertyInfo()
                  },
                  data: function () {
                        return {
                              assetId : ''
                        };
                  },

                  computed: {
                  },
                  updated(){
                        this.updatePropertyInfo()
                  },

                  methods: {
                        openAssetMappingManager() {
                              let path = window.wemb.configManager.context + "/custom/packs/dcim_pack/manager/view/asset_mapping/DCIMAssetMappingComponent.vue";
                              let component_instance = wemb.mainPageComponent.getComInstanceByName(this.primary.name);
                              window.wemb.showExternalModal("AssetMappingManager", path, component_instance);
                        },

                        removeAssetMappingData() {
                              // 인스턴스는 그대로고
                              // false == 인스턴스가 삭제되지 않는 의미
                              let component_instance = wemb.mainPageComponent.getComInstanceByName(this.primary.name);
                              wemb.dcimManager.assetAllocationProxy.removeAssetAssetAllocation(component_instance, true, false);

                        },
                        updatePropertyInfo() {
                              this.assetId = wemb.mainPageComponent.getComInstanceByName(this.primary.name).assetId;
                        },
                  },
                  destroyed(){
                        wemb.$globalBus.$off(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST);
                  }
            })
      }


      /*랙 실장 팝업*/
      static _createRackMountTemplate() {
            wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-rack-mount");
            Vue.component('dcim-rack-mount-template', {
                  extends: CoreTemplate,
                  template: ` 
                  <div class="prop-group">
                        <h5 class="header">Rack Mount</h5>
                        <div class="body">
                              <div class="d-flex row">
                                    <el-button size="small" class="flex" @click="onClickHandler" type="primary">Open</el-button>
                              </div>
                        </div>
                  </div>
            `,

                  mounted: function () {
                  },
                  data: function () {
                        return {};
                  },

                  computed: {
                  },

                  methods: {
                        onClickHandler() {
                              let param = {
                                    "modalName": "dcim-rack-mount-manager-popup",
                                    "fileName": window.wemb.configManager.context + "/custom/packs/dcim_pack/manager/view/mount/rack/DCIMRackMountManagerComponent.vue",
                                    "data": wemb.mainPageComponent.getComInstanceByName(this.primary.name).assetId
                              };

                              window.wemb.editorFacade.sendNotification(EditorStatic.CMD_SHOW_MODAL, param);
                        }
                  }
            })
      }

      /*분전반 실장 팝업*/
      static _createPduMountTemplate() {
            wemb.componentLibraryManager.addComponentPropertyTemplate("dcim-pdu-mount");
            Vue.component('dcim-pdu-mount-template', {
                  extends: CoreTemplate,
                  template: ` 
                  <div class="prop-group">
                        <h5 class="header">PDU Mount</h5>
                        <div class="body">
                              <div class="d-flex row">
                                    <el-button size="small" class="flex" @click="onClickHandler" type="primary">Open</el-button>
                              </div>
                        </div>
                  </div>
            `,

                  mounted: function () {
                  },
                  data: function () {
                        return {};
                  },

                  computed: {
                  },

                  methods: {
                        onClickHandler() {
                              let param = {
                                    "modalName": "dcim-pdu-mount-manager-popup",
                                    "fileName": window.wemb.configManager.context+"/custom/packs/dcim_pack/manager/view/mount/pdu/DCIMPduMountManagerComponent.vue",
                                    "data": wemb.mainPageComponent.getComInstanceByName(this.primary.name).assetId
                              };

                              window.wemb.editorFacade.sendNotification(EditorStatic.CMD_SHOW_MODAL, param);
                        }
                  }
            })
      }
}

DCIMComponentPropertyTemplateRegister.init = false;
