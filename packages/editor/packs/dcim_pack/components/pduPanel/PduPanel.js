class PduPanel extends WV3DAssetComponent {

      constructor() {
            super();
      }

      _onCreateProperties() {
            super._onCreateProperties();
            this.door = null;
      }

      _onChildMouseOver(event) {
            event.selectItem.toggleOutlineElement(true);
            super._onChildMouseOver(event);
      }

      _onChildMouseOut(event) {
            event.selectItem.toggleOutlineElement(false);
            super._onChildMouseOut(event);
      }

      composeResource(loadedObj) {
            let boxSize = ThreeUtil.getObjectSize(loadedObj);
            let max = loadedObj.children.length;
            for (let i = 0; i < max; i++) {
                  let child = loadedObj.children[i];
                  if (child instanceof THREE.Mesh) {
                        if (child.name == "doorRight") {
                              child.geometry.center();
                              child.geometry.translate(-boxSize.x / 2, 0, 0);
                              child.position.set(boxSize.x / 2, boxSize.y / 2, boxSize.z / 2);
                              this.door = child;
                        }
                        if (child.name.indexOf("port") > -1) {
                              child.geometry.computeBoundingSphere();
                              let ce = child.geometry.boundingSphere.center;
                              child.position.set(ce.x, ce.y, ce.z);
                              child.geometry.center();
                              child.visible = false;
                              child.material.visible = false;
                        }
                  }
            }
            super.composeResource(loadedObj);
      }

      /*
      _onValidateResource() {
            let boxSize = ThreeUtil.getObjectSize(this.element);
            let max = this.element.children.length;
            for (let i = 0; i < max; i++) {
                  let child = this.element.children[i];
                  if (child instanceof THREE.Mesh) {
                        if (child.name == "doorRight") {
                              child.geometry.center();
                              child.geometry.translate(-boxSize.x / 2, 0, 0);
                              child.position.set(boxSize.x / 2, boxSize.y / 2, boxSize.z / 2);
                              this.door = child;
                        }
                        if (child.name.indexOf("port") > -1) {
                              child.geometry.computeBoundingSphere();
                              let ce = child.geometry.boundingSphere.center;
                              child.position.set(ce.x, ce.y, ce.z);
                              child.geometry.center();
                              child.visible = false;
                              child.material.visible = false;
                        }
                  }
            }
            ;
            super._onValidateResource();
      }*/


      releaseChildren() {
            if (!this.isViewerMode) return;
            let child;
            for (let i = 0; i < this._children.length; i++) {
                  child = this._children[i];
                  child.destroy();
            }
            this.appendElement.removeEventListener("itemClick", this.childClickHandler);
            this.appendElement.removeEventListener("itemDblClick", this.childDoubleClickHandler);
            this.appendElement.removeEventListener("itemMouseOver", this.childMouseOverHandler);
            this.appendElement.removeEventListener("itemMouseOut", this.childMouseOutHandler);
            this._children = [];
      }

      hasChildAsset(){
            return true;
      }

      buildChildren() {
            if(this.isAssetComp() && +this.data.unit_size>1){
                  // 포트 수
                  let count = this.data.unit_size * 2;
                  let isLeft;
                  // 기본 연결 포트 정보
                  let leftPort = this.element.getObjectByName("port_l");
                  let rightPort = this.element.getObjectByName("port_r");
                  let i;
                  let portSize = ThreeUtil.getObjectSize(leftPort);

                  let port, targetPort, rowIndex;
                  for (i = 0; i < count; i++) {
                        isLeft = (i % 2 == 0) ? true : false;
                        targetPort = isLeft ? leftPort : rightPort;
                        rowIndex = parseInt(i / 2);
                        port = new THREE.Mesh(targetPort.geometry.clone(), targetPort.material.clone());
                        port.name = (isLeft ? "LEFT" : "RIGHT") + "_" + (rowIndex+1);
                        port.material.visible = true;
                        port.position.copy(targetPort.position);
                        port.position.y -= portSize.y * rowIndex;
                        let comp = new PduInnerMeshDecorator(port);
                        comp.draw(this);
                        this._children.push(comp);
                  }
                  this.appendElement.addEventListener("itemClick", this.childClickHandler);
                  this.appendElement.addEventListener("itemDblClick", this.childDoubleClickHandler);
                  this.appendElement.addEventListener("itemMouseOver", this.childMouseOverHandler);
                  this.appendElement.addEventListener("itemMouseOut", this.childMouseOutHandler);
                  // 포트와 연관자산 데이터 설정
                  (async()=>{
                        let [error, res] = await window.wemb.dcimManager.getRelationData(this.assetId);
                        if(error){
                              CPLogger.log("pud-buildChildren", error );
                              return;
                        }
                        // 데이터에 해당하는 포트 찾기
                        let portList      = res.data;
                        let reducer = (source, vo ) => {
                              let result = this._children.filter((item)=>{
                                    const [direction, portIndex] = item.name.split("_");
                              //      let props = vo.props;
                                    let prosDirection = vo.direction.toUpperCase();
                                    let prosIndex = vo.index;
                                    return (direction == prosDirection && portIndex == +prosIndex) ? true : false;
                              });
                              if(result.length){
                                    // 포트에 데이터 설정
                                    result[0].data = vo;
                                    source = source.concat(result);
                              }
                              return source;

                        }
                        // 연관 포트 마우스 이벤트 등록
                        let connectPort  = portList.reduce( reducer, [] ) ;
                        connectPort.forEach((comp)=>{
                              comp.registEventListener();
                        });
                  })();

            }
      }


      transitionAfter() {
            this._validateOutline();
      }

      transitionInEffect() {
            if(this.isViewerMode){
                  this.buildChildren();
            }
            return new Promise(async(resolve) => {
                  this.focusTween = TweenMax.to(this.door.rotation, 0.5, {
                        y: Math.PI / 2,
                        onComplete: () => {
                              this.transitionAfter();
                              resolve();
                        }
                  });
            });
      }

      transitionOutEffect() {

            return new Promise(async(resolve) => {
                  this.focusTween = TweenMax.to(this.door.rotation, 0.5, {
                        y: 0,
                        onComplete: () => {
                              this.releaseChildren();
                              this.transitionAfter();
                              resolve();
                        }
                  });
            });
      }


      _onDestroy() {
            this.door = null;
            super._onDestroy();
      }
}

WV3DPropertyManager.attach_default_component_infos(PduPanel, {
      "setter": {
            "assetId": "",
            "popupId": "",
            "state": "normal"
      },

      "label": {
            "label_text": "PduPanel",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "PduPanel",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});


WVPropertyManager.add_property_panel_group_info(PduPanel, {
      template: "dcim-asset-id"
})

WVPropertyManager.add_property_panel_group_info(PduPanel, {
      template: 'dcim-asset-popup'
})

WVPropertyManager.add_property_panel_group_info(PduPanel, {
      template: 'dcim-pdu-mount'
});

WV3DPropertyManager.add_event(PduPanel, {
      name: "itemMouseOver",
      label: "Child Element Mouseover Event",
      description: "Child Element Mouseover Event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});

WV3DPropertyManager.add_event(PduPanel, {
      name: "itemMouseOut",
      label: "Child Element Mouseout Event",
      description: "Child Element Mouseout Event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});

WV3DPropertyManager.add_event(PduPanel, {
      name: "itemClick",
      label: "Child Element Mouseclick Event",
      description: "Child Element Mouseclick Event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});


WV3DPropertyManager.add_event(PduPanel, {
      name: "itemDblClick",
      label: "Child Element Mousedbclick Event",
      description: "Child Element Mousedbclick Event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
