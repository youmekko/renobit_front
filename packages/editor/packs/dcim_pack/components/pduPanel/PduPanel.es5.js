function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var PduPanel =
      /*#__PURE__*/
      function (_WV3DAssetComponent) {
            "use strict";

            _inherits(PduPanel, _WV3DAssetComponent);

            function PduPanel() {
                  _classCallCheck(this, PduPanel);

                  return _possibleConstructorReturn(this, _getPrototypeOf(PduPanel).call(this));
            }

            _createClass(PduPanel, [{
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(PduPanel.prototype), "_onCreateProperties", this).call(this);

                        this.door = null;
                  }
            }, {
                  key: "_onChildMouseOver",
                  value: function _onChildMouseOver(event) {
                        event.selectItem.toggleOutlineElement(true);

                        _get(_getPrototypeOf(PduPanel.prototype), "_onChildMouseOver", this).call(this, event);
                  }
            }, {
                  key: "_onChildMouseOut",
                  value: function _onChildMouseOut(event) {
                        event.selectItem.toggleOutlineElement(false);

                        _get(_getPrototypeOf(PduPanel.prototype), "_onChildMouseOut", this).call(this, event);
                  }
            }, {
                  key: "composeResource",
                  value: function composeResource(loadedObj) {
                        var boxSize = ThreeUtil.getObjectSize(loadedObj);
                        var max = loadedObj.children.length;

                        for (var i = 0; i < max; i++) {
                              var child = loadedObj.children[i];

                              if (_instanceof(child, THREE.Mesh)) {
                                    if (child.name == "doorRight") {
                                          child.geometry.center();
                                          child.geometry.translate(-boxSize.x / 2, 0, 0);
                                          child.position.set(boxSize.x / 2, boxSize.y / 2, boxSize.z / 2);
                                          this.door = child;
                                    }

                                    if (child.name.indexOf("port") > -1) {
                                          child.geometry.computeBoundingSphere();
                                          var ce = child.geometry.boundingSphere.center;
                                          child.position.set(ce.x, ce.y, ce.z);
                                          child.geometry.center();
                                          child.visible = false;
                                          child.material.visible = false;
                                    }
                              }
                        }

                        _get(_getPrototypeOf(PduPanel.prototype), "composeResource", this).call(this, loadedObj);
                  }
                  /*
                  _onValidateResource() {
                        let boxSize = ThreeUtil.getObjectSize(this.element);
                        let max = this.element.children.length;
                        for (let i = 0; i < max; i++) {
                              let child = this.element.children[i];
                              if (child instanceof THREE.Mesh) {
                                    if (child.name == "doorRight") {
                                          child.geometry.center();
                                          child.geometry.translate(-boxSize.x / 2, 0, 0);
                                          child.position.set(boxSize.x / 2, boxSize.y / 2, boxSize.z / 2);
                                          this.door = child;
                                    }
                                    if (child.name.indexOf("port") > -1) {
                                          child.geometry.computeBoundingSphere();
                                          let ce = child.geometry.boundingSphere.center;
                                          child.position.set(ce.x, ce.y, ce.z);
                                          child.geometry.center();
                                          child.visible = false;
                                          child.material.visible = false;
                                    }
                              }
                        }
                        ;
                        super._onValidateResource();
                  }*/

            }, {
                  key: "releaseChildren",
                  value: function releaseChildren() {
                        if (!this.isViewerMode) return;
                        var child;

                        for (var i = 0; i < this._children.length; i++) {
                              child = this._children[i];
                              child.destroy();
                        }

                        this.appendElement.removeEventListener("itemClick", this.childClickHandler);
                        this.appendElement.removeEventListener("itemDblClick", this.childDoubleClickHandler);
                        this.appendElement.removeEventListener("itemMouseOver", this.childMouseOverHandler);
                        this.appendElement.removeEventListener("itemMouseOut", this.childMouseOutHandler);
                        this._children = [];
                  }
            }, {
                  key: "hasChildAsset",
                  value: function hasChildAsset() {
                        return true;
                  }
            }, {
                  key: "buildChildren",
                  value: function buildChildren() {
                        var _this = this;

                        if (this.isAssetComp() && +this.data.unit_size > 1) {
                              // 포트 수
                              var count = this.data.unit_size * 2;
                              var isLeft; // 기본 연결 포트 정보

                              var leftPort = this.element.getObjectByName("port_l");
                              var rightPort = this.element.getObjectByName("port_r");
                              var i;
                              var portSize = ThreeUtil.getObjectSize(leftPort);
                              var port, targetPort, rowIndex;

                              for (i = 0; i < count; i++) {
                                    isLeft = i % 2 == 0 ? true : false;
                                    targetPort = isLeft ? leftPort : rightPort;
                                    rowIndex = parseInt(i / 2);
                                    port = new THREE.Mesh(targetPort.geometry.clone(), targetPort.material.clone());
                                    port.name = (isLeft ? "LEFT" : "RIGHT") + "_" + (rowIndex + 1);
                                    port.material.visible = true;
                                    port.position.copy(targetPort.position);
                                    port.position.y -= portSize.y * rowIndex;
                                    var comp = new PduInnerMeshDecorator(port);
                                    comp.draw(this);

                                    this._children.push(comp);
                              }

                              this.appendElement.addEventListener("itemClick", this.childClickHandler);
                              this.appendElement.addEventListener("itemDblClick", this.childDoubleClickHandler);
                              this.appendElement.addEventListener("itemMouseOver", this.childMouseOverHandler);
                              this.appendElement.addEventListener("itemMouseOut", this.childMouseOutHandler); // 포트와 연관자산 데이터 설정

                              _asyncToGenerator(
                                    /*#__PURE__*/
                                    regeneratorRuntime.mark(function _callee() {
                                          var _ref2, _ref3, error, res, portList, reducer, connectPort;

                                          return regeneratorRuntime.wrap(function _callee$(_context) {
                                                while (1) {
                                                      switch (_context.prev = _context.next) {
                                                            case 0:
                                                                  _context.next = 2;
                                                                  return window.wemb.dcimManager.getRelationData(_this.assetId);

                                                            case 2:
                                                                  _ref2 = _context.sent;
                                                                  _ref3 = _slicedToArray(_ref2, 2);
                                                                  error = _ref3[0];
                                                                  res = _ref3[1];

                                                                  if (!error) {
                                                                        _context.next = 9;
                                                                        break;
                                                                  }

                                                                  CPLogger.log("pud-buildChildren", error);
                                                                  return _context.abrupt("return");

                                                            case 9:
                                                                  // 데이터에 해당하는 포트 찾기
                                                                  portList = res.data.data;

                                                                  reducer = function reducer(source, vo) {
                                                                        var result = _this._children.filter(function (item) {
                                                                              var _item$name$split = item.name.split("_"),
                                                                                    _item$name$split2 = _slicedToArray(_item$name$split, 2),
                                                                                    direction = _item$name$split2[0],
                                                                                    portIndex = _item$name$split2[1]; //      let props = vo.props;


                                                                              var prosDirection = vo.direction.toUpperCase();
                                                                              var prosIndex = vo.index;
                                                                              return direction == prosDirection && portIndex == +prosIndex ? true : false;
                                                                        });

                                                                        if (result.length) {
                                                                              // 포트에 데이터 설정
                                                                              result[0].data = vo;
                                                                              source = source.concat(result);
                                                                        }

                                                                        return source;
                                                                  }; // 연관 포트 마우스 이벤트 등록


                                                                  connectPort = portList.reduce(reducer, []);
                                                                  connectPort.forEach(function (comp) {
                                                                        comp.registEventListener();
                                                                  });

                                                            case 13:
                                                            case "end":
                                                                  return _context.stop();
                                                      }
                                                }
                                          }, _callee, this);
                                    }))();
                        }
                  }
            }, {
                  key: "transitionAfter",
                  value: function transitionAfter() {
                        this._validateOutline();
                  }
            }, {
                  key: "transitionInEffect",
                  value: function transitionInEffect() {
                        var _this2 = this;

                        if (this.isViewerMode) {
                              this.buildChildren();
                        }

                        return new Promise(
                              /*#__PURE__*/
                              function () {
                                    var _ref4 = _asyncToGenerator(
                                          /*#__PURE__*/
                                          regeneratorRuntime.mark(function _callee2(resolve) {
                                                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                                      while (1) {
                                                            switch (_context2.prev = _context2.next) {
                                                                  case 0:
                                                                        _this2.focusTween = TweenMax.to(_this2.door.rotation, 0.5, {
                                                                              y: Math.PI / 2,
                                                                              onComplete: function onComplete() {
                                                                                    _this2.transitionAfter();

                                                                                    resolve();
                                                                              }
                                                                        });

                                                                  case 1:
                                                                  case "end":
                                                                        return _context2.stop();
                                                            }
                                                      }
                                                }, _callee2, this);
                                          }));

                                    return function (_x) {
                                          return _ref4.apply(this, arguments);
                                    };
                              }());
                  }
            }, {
                  key: "transitionOutEffect",
                  value: function transitionOutEffect() {
                        var _this3 = this;

                        return new Promise(
                              /*#__PURE__*/
                              function () {
                                    var _ref5 = _asyncToGenerator(
                                          /*#__PURE__*/
                                          regeneratorRuntime.mark(function _callee3(resolve) {
                                                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                                      while (1) {
                                                            switch (_context3.prev = _context3.next) {
                                                                  case 0:
                                                                        _this3.focusTween = TweenMax.to(_this3.door.rotation, 0.5, {
                                                                              y: 0,
                                                                              onComplete: function onComplete() {
                                                                                    _this3.releaseChildren();

                                                                                    _this3.transitionAfter();

                                                                                    resolve();
                                                                              }
                                                                        });

                                                                  case 1:
                                                                  case "end":
                                                                        return _context3.stop();
                                                            }
                                                      }
                                                }, _callee3, this);
                                          }));

                                    return function (_x2) {
                                          return _ref5.apply(this, arguments);
                                    };
                              }());
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.door = null;

                        _get(_getPrototypeOf(PduPanel.prototype), "_onDestroy", this).call(this);
                  }
            }]);

            return PduPanel;
      }(WV3DAssetComponent);

WV3DPropertyManager.attach_default_component_infos(PduPanel, {
      "setter": {
            "assetId": "",
            "popupId": "",
            "state": "normal"
      },
      "label": {
            "label_text": "PduPanel",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "PduPanel",
            "version": "1.0.0"
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      }
});
WVPropertyManager.add_property_panel_group_info(PduPanel, {
      template: "dcim-asset-id"
});
WVPropertyManager.add_property_panel_group_info(PduPanel, {
      template: 'dcim-asset-popup'
});
WVPropertyManager.add_property_panel_group_info(PduPanel, {
      template: 'dcim-pdu-mount'
});
WV3DPropertyManager.add_event(PduPanel, {
      name: "itemMouseOver",
      label: "Child Element Mouseover Event",
      description: "Child Element Mouseover Event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
WV3DPropertyManager.add_event(PduPanel, {
      name: "itemMouseOut",
      label: "Child Element Mouseout Event",
      description: "Child Element Mouseout Event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
WV3DPropertyManager.add_event(PduPanel, {
      name: "itemClick",
      label: "Child Element Mouseclick Event",
      description: "Child Element Mouseclick Event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
WV3DPropertyManager.add_event(PduPanel, {
      name: "itemDblClick",
      label: "Child Element Mousedbclick Event",
      description: "Child Element Mousedbclick Event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
