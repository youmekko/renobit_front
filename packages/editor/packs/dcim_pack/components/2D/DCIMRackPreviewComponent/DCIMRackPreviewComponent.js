class DCIMRackPreviewComponent extends WVDOMComponent {
    constructor() {
        super();
        this._rackId = null;
        this.app = null;
    }


    _onCreateElement() {
        var self = this;
        var str =
            `<div class="rack-warp">
                <ul>
                    <li v-for="(shelfNo, i) in unitAry" :key="i"
                        @click="onClick(null)"
                    >
                        <span>{{shelfNo}}</span>                    
                        <div class="item-wrap" :data-unit-no="shelfNo">       
                            <template v-if="shelfMountData[shelfNo]">
                                <div v-for="(item, j) in shelfMountData[shelfNo]"                                     
                                        :key="j" 
                                        :style="getItemStyle(item)" 
                                        @click.stop="onClick(item.id)"
                                        class="item tooltip"
                                        :class="getClass(item)">
                                        <el-tooltip :content="item.name" placement="top" class="rack-tooltip">
                                            <div class="cont"><span :class="{rotate: item.mountData.rotate}">{{item.name}}</span></div>
                                        </el-tooltip>   
                                    </div>     
                            </template>
                        </div>
                    </li>                      
                </ul>
            </div>`;

        let temp = $(str);
        $(this._element).append(temp);

        this.app = new Vue({
            el: temp.get(0),
            data: function() {
                return {
                    defaultUnitLen: 42,
                    unitLen: 42,
                    unitDirBT: true,
                    unitHeight: 13,
                    unitAry: [],
                    shelfMountData: {},
                    focusId: null
                }
            },

            mounted: function() {
                this.setData(null);
            },
            methods: {
                getClass(item) {
                    return {
                        'active': this.focusId == item.id,
                        'overlap': item.mountData.error == 'oversize' || item.mountData.error == 'overlap_item'
                    }
                },

                getItemStyle: function(item) {
                    let style = "width:" + item.mountData.width + "%;left:" + item.mountData.left + "%;height:" + this.unitHeight * item.mountData.row_weight + "px;";
                    return style;
                },

                setData: function(data) {
                    this.setFocusId(null);
                    if (data) {
                        this.setUnitDir(data.unitDirBT);
                        this.setUnitLen(data.unitLen);
                        this.setMountData(data.mounts.rows);
                    } else {
                        this.setUnitDir(true);
                        this.setUnitLen(this.defaultUnitLen);
                        this.setMountData({});
                    }
                },

                setFocusId(id) {
                    let tempId = this.focusId;
                    this.focusId = id;
                    if (tempId != id) {
                        if (id) {
                            self.dispatchWScriptEvent("focus", {
                                targetId: this.focusId
                            })
                        } else {
                            self.dispatchWScriptEvent("blur");
                        }
                    }
                },

                onClick: function(id) {
                    if (this.focusId != id) {
                        this.setFocusId(id);
                    } else {
                        this.setFocusId(null);
                    }
                },

                setUnitLen: function(len) {
                    this.unitLen = len;
                    this.unitAry = [];
                    if (this.unitDirBT) {
                        for (let i = this.unitLen; i > 0; i--) {
                            this.unitAry.push(i);
                        }
                    } else {
                        for (let i = 1, iLen = this.unitLen; i <= iLen; i++) {
                            this.unitAry.push(i);
                        }
                    }
                },

                setMountData: function(data) {
                    this.shelfMountData = data;
                },

                setUnitDir: function(dirBT) {
                    this.unitDirBT = dirBT;
                    this.setUnitLen(this.unitLen);
                }
            }
        })
    }

    bindEvent() {
        if (this.isEditorMode) { return; }

    }

    _onDestroy() {
        this.app.$destroy();
        this.app = null;
        super._onDestroy();
    }

    set rackId(rackId) {
        this._rackId = rackId;
        let data = window.wemb.dcimManager.mountDataManager.getMountDataByRackId(rackId);
        this.app.setData(data);
    }

    get rackId() {
        return this._rackId;
    }

    focus(id) {
        this.app.setFocusId(id);
    }

    blur() {
        this.app.setFocusId(null);
    }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(DCIMRackPreviewComponent, {
    "info": {
        "componentName": "DCIMRackPreviewComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 190,
        "height": 570
    },

    "label": {
        "label_using": "N",
        "label_text": "Rack Preview"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
DCIMRackPreviewComponent.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    },
    {
        template: "label",
        label: "label"
    }
];


//  추후 추가 예정
DCIMRackPreviewComponent.method_info = [];
WVPropertyManager.remove_event(DCIMRackPreviewComponent, "dblclick");
WVPropertyManager.remove_event(DCIMRackPreviewComponent, "click");
// 이벤트 정보
WVPropertyManager.add_event(DCIMRackPreviewComponent, {
    name: "focus",
    label: "Mounted asset focus event",
    description: "Mounted asset focus event",
    properties: [{
        name: "targetId",
        type: "string",
        default: "",
        description: "focused target ID"
    }]
});

// 이벤트 정보
WVPropertyManager.add_event(DCIMRackPreviewComponent, {
    name: "blur",
    label: "Mounted asset blur event",
    description: "Mounted asset blur event"
});