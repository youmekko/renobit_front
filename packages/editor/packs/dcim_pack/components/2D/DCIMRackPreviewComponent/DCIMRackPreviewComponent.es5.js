'use strict';

var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DCIMRackPreviewComponent = function(_WVDOMComponent) {
    _inherits(DCIMRackPreviewComponent, _WVDOMComponent);

    function DCIMRackPreviewComponent() {
        _classCallCheck(this, DCIMRackPreviewComponent);

        var _this = _possibleConstructorReturn(this, (DCIMRackPreviewComponent.__proto__ || Object.getPrototypeOf(DCIMRackPreviewComponent)).call(this));

        _this._rackId = null;
        _this.app = null;
        return _this;
    }

    _createClass(DCIMRackPreviewComponent, [{
        key: '_onCreateElement',
        value: function _onCreateElement() {
            var self = this;
            var str = '<div class="rack-warp">\n                <ul>\n                    <li v-for="(shelfNo, i) in unitAry" :key="i"\n                        @click="onClick(null)"\n                    >\n                        <span>{{shelfNo}}</span>                    \n                        <div class="item-wrap" :data-unit-no="shelfNo">       \n                            <template v-if="shelfMountData[shelfNo]">\n                                <div v-for="(item, j) in shelfMountData[shelfNo]"                                     \n                                        :key="j" \n                                        :style="getItemStyle(item)" \n                                        @click.stop="onClick(item.id)"\n                                        class="item tooltip"\n                                        :class="getClass(item)">\n                                        <el-tooltip :content="item.name" placement="top" class="rack-tooltip">\n                                            <div class="cont"><span :class="{rotate: item.mountData.rotate}">{{item.name}}</span></div>\n                                        </el-tooltip>   \n                                    </div>     \n                            </template>\n                        </div>\n                    </li>                      \n                </ul>\n            </div>';

            var temp = $(str);
            $(this._element).append(temp);

            this.app = new Vue({
                el: temp.get(0),
                data: function data() {
                    return {
                        defaultUnitLen: 42,
                        unitLen: 42,
                        unitDirBT: true,
                        unitHeight: 13,
                        unitAry: [],
                        shelfMountData: {},
                        focusId: null
                    };
                },

                mounted: function mounted() {
                    this.setData(null);
                },
                methods: {
                    getClass: function getClass(item) {
                        return {
                            'active': this.focusId == item.id,
                            'overlap': item.mountData.error == 'oversize' || item.mountData.error == 'overlap_item'
                        };
                    },


                    getItemStyle: function getItemStyle(item) {
                        var style = "width:" + item.mountData.width + "%;left:" + item.mountData.left + "%;height:" + this.unitHeight * item.mountData.row_weight + "px;";
                        return style;
                    },

                    setData: function setData(data) {
                        this.setFocusId(null);
                        if (data) {
                            this.setUnitDir(data.unitDirBT);
                            this.setUnitLen(data.unitLen);
                            this.setMountData(data.mounts.rows);
                        } else {
                            this.setUnitDir(true);
                            this.setUnitLen(this.defaultUnitLen);
                            this.setMountData({});
                        }
                    },

                    setFocusId: function setFocusId(id) {
                        var tempId = this.focusId;
                        this.focusId = id;
                        if (tempId != id) {
                            if (id) {
                                self.dispatchWScriptEvent("focus", {
                                    targetId: this.focusId
                                });
                            } else {
                                self.dispatchWScriptEvent("blur", {
                                    targetId: tempId
                                });
                            }
                        }
                    },


                    onClick: function onClick(id) {
                        if (this.focusId != id) {
                            this.setFocusId(id);
                        } else {
                            this.setFocusId(null);
                        }
                    },

                    setUnitLen: function setUnitLen(len) {
                        this.unitLen = len;
                        this.unitAry = [];
                        if (this.unitDirBT) {
                            for (var i = this.unitLen; i > 0; i--) {
                                this.unitAry.push(i);
                            }
                        } else {
                            for (var _i = 1, iLen = this.unitLen; _i <= iLen; _i++) {
                                this.unitAry.push(_i);
                            }
                        }
                    },

                    setMountData: function setMountData(data) {
                        this.shelfMountData = data;
                    },

                    setUnitDir: function setUnitDir(dirBT) {
                        this.unitDirBT = dirBT;
                        this.setUnitLen(this.unitLen);
                    }
                }
            });
        }
    }, {
        key: 'bindEvent',
        value: function bindEvent() {
            if (this.isEditorMode) {
                return;
            }
        }
    }, {
        key: '_onDestroy',
        value: function _onDestroy() {
            this.app.$destroy();
            this.app = null;
            _get(DCIMRackPreviewComponent.prototype.__proto__ || Object.getPrototypeOf(DCIMRackPreviewComponent.prototype), '_onDestroy', this).call(this);
        }
    }, {
        key: 'focus',
        value: function focus(id) {
            this.app.setFocusId(id);
        }
    }, {
        key: 'blur',
        value: function blur() {
            this.app.setFocusId(null);
        }
    }, {
        key: 'rackId',
        set: function set(rackId) {
            this._rackId = rackId;
            var data = window.wemb.dcimManager.mountDataManager.getMountDataByRackId(rackId);
            this.app.setData(data);
        },
        get: function get() {
            return this._rackId;
        }
    }]);

    return DCIMRackPreviewComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(DCIMRackPreviewComponent, {
    "info": {
        "componentName": "DCIMRackPreviewComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 190,
        "height": 570
    },

    "label": {
        "label_using": "N",
        "label_text": "Rack Preview"
    }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
DCIMRackPreviewComponent.property_panel_info = [{
    template: "primary",
    label: "primary"
}, {
    template: "pos-size-2d",
    label: "pos-size-2d"
}, {
    template: "label",
    label: "label"
}];

//  추후 추가 예정
DCIMRackPreviewComponent.method_info = [];
WVPropertyManager.remove_event(DCIMRackPreviewComponent, "dblclick");
WVPropertyManager.remove_event(DCIMRackPreviewComponent, "click");
// 이벤트 정보
WVPropertyManager.add_event(DCIMRackPreviewComponent, {
    name: "focus",
    label: "Mounted asset focus event",
    description: "Mounted asset focus event",
    properties: [{
        name: "targetId",
        type: "string",
        default: "",
        description: "focused target ID"
    }]
});

// 이벤트 정보
WVPropertyManager.add_event(DCIMRackPreviewComponent, {
    name: "blur",
    label: "Mounted asset blur Event",
    description: "Mounted asset blur Event"
});