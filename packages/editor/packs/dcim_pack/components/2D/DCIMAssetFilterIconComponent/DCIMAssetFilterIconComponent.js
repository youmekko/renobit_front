class DCIMAssetFilterIconComponent extends WVDOMComponent {

      constructor() {
            super();
            this.executeViewerModeHandler = this._executeViewer.bind(this);
            this.arrowCompList = [];
      }

      _onDestroy() {
            super._onDestroy();
            wemb.$globalBus.$off(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, this.executeViewerModeHandler);
            this.executeViewerModeHandler = null;
      }

      executeViewerMode() {
            super.executeViewerMode();
            wemb.$globalBus.$on(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, this.executeViewerModeHandler);
      }

      _executeViewer() {
            // 비상구 인스턴스
            window.wemb.mainPageComponent.$threeLayer.forEach((comInstance) => {
                  if (typeof DirectionArrow !== "undefined" && comInstance instanceof DirectionArrow) {
                        this.arrowCompList.push(comInstance);
                  }
            });

            this._setAssetIcon();
            this._bindEvent();
            this.allocatedAssetMap = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoGroupMap;
      }

      _onCreateElement() {
            $(this._element).addClass("dcim-asset-filter-icon");
            let tmpEle = '<div class="container">' +
                  '<div class="btn btn-all"></div>' +
                  '</div>';

            this._element.innerHTML = tmpEle;
      }

      _setAssetIcon() {
            this.typeList = wemb.dcimManager.assetManager.getAssetTypesInPage(wemb.pageManager.currentPageInfo.id);
            let $container = $(this._element).find(".container");
            let elem = '';
            let styleStr = '<style>';

            this.typeList.map(item => {
                  elem += '<div class="btn '+item+ '"type="' + item + '"></div>';
                  styleStr += this.createTypeClass(item);
            });

            // 비상구 인스턴스가 있는 경우 아이콘 추가
            if (this.arrowCompList.length > 0) {
                  elem += '<div class="btn exit" type="exit"></div>';
                  styleStr += this.createTypeClass('exit');
            }

            styleStr += '</style>';
            $container.append(styleStr, elem);
      }

      createTypeClass(type) {
            let url = DCIMManager.PACK_PATH + this.componentName + "/resource/";

            return ".btn."+type+ "{ background-image: url('" + url +type+".png'); }" +
                  ".btn."+type+ ":hover, ."+type+ ".active { background-image: url('"+ url + type+"_active.png'); }"
      };

      _bindEvent() {
            let $typeIcons = $(this._element).find(".btn").not(".btn-all");
            let $allBtn = $(this._element).find(".btn-all");

            $allBtn.click(event => {
                  $(event.currentTarget).toggleClass("active");

                  $typeIcons.map((index, item) => {
                        $(item).removeClass('active')
                  });

                  this.dispatchActiveType();
            })

            $typeIcons.click(event => {
                  let $target = $(event.currentTarget);
                  $typeIcons.map((index, item) => {
                        if (item != $target.get(0)) {
                              $(item).removeClass('active')
                        }
                  });

                  $allBtn.removeClass("active");
                  $target.toggleClass("active");

                  this.dispatchActiveType();
            })
      }

      dispatchActiveType() {
            let activeType = null;
            let $allBtn = $(this._element).find(".btn-all");

            if ($allBtn.hasClass('active')) {
                  activeType = 'all';
            } else {
                  let $typeIcons = $(this._element).find(".btn.active").not(".btn-all");
                  $typeIcons.map((index, item) => {
                        activeType = item.getAttribute("type");
                  });
            }

            this.arrowCompList.map((comInstance) => {
                  let isActive = 'exit' == activeType ? true : false;
                  comInstance.visible = isActive;
            });

            // opacity 처리
            this.allocatedAssetMap.forEach((assetList, type) => {
                  assetList.forEach(asset => {
                        let comInstance = asset.comInstance;

                        // 전체보기 버튼 클릭 or 선택된 타입에 대해서만 active
                        let isActive = (activeType == 'all' || type == activeType) ? true : false;
                        comInstance.usePolling = isActive;
                        if (activeType != null) {
                              comInstance.opacity = isActive ? 100 : 30;
                        } else {
                              comInstance.opacity = 100;
                        }
                  });
            });

            wemb.dcimManager._eventManager.clearHistoryTimer();

            // api 호출
            if (activeType && activeType != 'exit') {
                  switch (activeType) {
                        case AssetComponentProxy.TYPE.ACCESS:
                        case AssetComponentProxy.TYPE.LEAK:
                              this.activeInfo = { types: [activeType], assetIds: [] };
                              this.activeInfo.assetIds = this.allocatedAssetMap.get(activeType).map(row => { return row.assetId });
                              wemb.dcimManager._eventManager.activeHistoryInfo = this.activeInfo;
                              wemb.dcimManager._eventManager.procHistoryData();
                              break;
                  }
            }
      }

      clearActive() {
            $(this._element).find(".btn").removeClass("active");
            this.dispatchActiveType();
      }
}

WVPropertyManager.attach_default_component_infos(DCIMAssetFilterIconComponent, {
      "info": {
            "componentName": "DCIMAssetFilterIconComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 500,
            "height": 100
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIMAssetFilterIconComponent"
      }
});

DCIMAssetFilterIconComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}]
