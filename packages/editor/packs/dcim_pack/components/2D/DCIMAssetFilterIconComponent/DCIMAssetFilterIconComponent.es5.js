"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var DCIMAssetFilterIconComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(DCIMAssetFilterIconComponent, _WVDOMComponent);

            function DCIMAssetFilterIconComponent() {
                  var _this;

                  _classCallCheck(this, DCIMAssetFilterIconComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(DCIMAssetFilterIconComponent).call(this));
                  _this.executeViewerModeHandler = _this._executeViewer.bind(_assertThisInitialized(_this));
                  _this.arrowCompList = [];
                  return _this;
            }

            _createClass(DCIMAssetFilterIconComponent, [{
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        _get(_getPrototypeOf(DCIMAssetFilterIconComponent.prototype), "_onDestroy", this).call(this);

                        wemb.$globalBus.$off(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, this.executeViewerModeHandler);
                        this.executeViewerModeHandler = null;
                  }
            }, {
                  key: "executeViewerMode",
                  value: function executeViewerMode() {
                        _get(_getPrototypeOf(DCIMAssetFilterIconComponent.prototype), "executeViewerMode", this).call(this);

                        wemb.$globalBus.$on(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, this.executeViewerModeHandler);
                  }
            }, {
                  key: "_executeViewer",
                  value: function _executeViewer() {
                        var _this2 = this;

                        // 비상구 인스턴스
                        window.wemb.mainPageComponent.$threeLayer.forEach(function (comInstance) {
                              if (typeof DirectionArrow !== "undefined" && _instanceof(comInstance, DirectionArrow)) {
                                    _this2.arrowCompList.push(comInstance);
                              }
                        });

                        this._setAssetIcon();

                        this._bindEvent();

                        this.allocatedAssetMap = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoGroupMap;
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        $(this._element).addClass("dcim-asset-filter-icon");
                        var tmpEle = '<div class="container">' + '<div class="btn btn-all"></div>' + '</div>';
                        this._element.innerHTML = tmpEle;
                  }
            }, {
                  key: "_setAssetIcon",
                  value: function _setAssetIcon() {
                        var _this3 = this;

                        this.typeList = wemb.dcimManager.assetManager.getAssetTypesInPage(wemb.pageManager.currentPageInfo.id);
                        var $container = $(this._element).find(".container");
                        var elem = '';
                        var styleStr = '<style>';
                        this.typeList.map(function (item) {
                              elem += '<div class="btn ' + item + '"type="' + item + '"></div>';
                              styleStr += _this3.createTypeClass(item);
                        }); // 비상구 인스턴스가 있는 경우 아이콘 추가

                        if (this.arrowCompList.length > 0) {
                              elem += '<div class="btn exit" type="exit"></div>';
                              styleStr += this.createTypeClass('exit');
                        }

                        styleStr += '</style>';
                        $container.append(styleStr, elem);
                  }
            }, {
                  key: "createTypeClass",
                  value: function createTypeClass(type) {
                        var url = DCIMManager.PACK_PATH + this.componentName + "/resource/";
                        return ".btn." + type + "{ background-image: url('" + url + type + ".png'); }" + ".btn." + type + ":hover, ." + type + ".active { background-image: url('" + url + type + "_active.png'); }";
                  }
            }, {
                  key: "_bindEvent",
                  value: function _bindEvent() {
                        var _this4 = this;

                        var $typeIcons = $(this._element).find(".btn").not(".btn-all");
                        var $allBtn = $(this._element).find(".btn-all");
                        $allBtn.click(function (event) {
                              $(event.currentTarget).toggleClass("active");
                              $typeIcons.map(function (index, item) {
                                    $(item).removeClass('active');
                              });

                              _this4.dispatchActiveType();
                        });
                        $typeIcons.click(function (event) {
                              var $target = $(event.currentTarget);
                              $typeIcons.map(function (index, item) {
                                    if (item != $target.get(0)) {
                                          $(item).removeClass('active');
                                    }
                              });
                              $allBtn.removeClass("active");
                              $target.toggleClass("active");

                              _this4.dispatchActiveType();
                        });
                  }
            }, {
                  key: "dispatchActiveType",
                  value: function dispatchActiveType() {
                        var activeType = null;
                        var $allBtn = $(this._element).find(".btn-all");

                        if ($allBtn.hasClass('active')) {
                              activeType = 'all';
                        } else {
                              var $typeIcons = $(this._element).find(".btn.active").not(".btn-all");
                              $typeIcons.map(function (index, item) {
                                    activeType = item.getAttribute("type");
                              });
                        }

                        this.arrowCompList.map(function (comInstance) {
                              var isActive = 'exit' == activeType ? true : false;
                              comInstance.visible = isActive;
                        }); // opacity 처리

                        this.allocatedAssetMap.forEach(function (assetList, type) {
                              assetList.forEach(function (asset) {
                                    var comInstance = asset.comInstance; // 전체보기 버튼 클릭 or 선택된 타입에 대해서만 active

                                    var isActive = activeType == 'all' || type == activeType ? true : false;
                                    comInstance.usePolling = isActive;

                                    if (activeType != null) {
                                          comInstance.opacity = isActive ? 100 : 30;
                                    } else {
                                          comInstance.opacity = 100;
                                    }
                              });
                        });

                        wemb.dcimManager._eventManager.clearHistoryTimer(); // api 호출


                        if (activeType && activeType != 'exit') {
                              switch (activeType) {
                                    case AssetComponentProxy.TYPE.ACCESS:
                                    case AssetComponentProxy.TYPE.LEAK:
                                          this.activeInfo = {
                                                types: [activeType],
                                                assetIds: []
                                          };
                                          this.activeInfo.assetIds = this.allocatedAssetMap.get(activeType).map(function (row) {
                                                return row.assetId;
                                          });
                                          wemb.dcimManager._eventManager.activeHistoryInfo = this.activeInfo;

                                          wemb.dcimManager._eventManager.procHistoryData();

                                          break;
                              }
                        }
                  }
            }, {
                  key: "clearActive",
                  value: function clearActive() {
                        $(this._element).find(".btn").removeClass("active");
                        this.dispatchActiveType();
                  }
            }]);

            return DCIMAssetFilterIconComponent;
      }(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DCIMAssetFilterIconComponent, {
      "info": {
            "componentName": "DCIMAssetFilterIconComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 500,
            "height": 100
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIMAssetFilterIconComponent"
      }
});
DCIMAssetFilterIconComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}];
