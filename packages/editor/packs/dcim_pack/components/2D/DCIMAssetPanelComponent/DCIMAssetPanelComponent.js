class DCIMAssetPanelComponent extends WVDOMComponent {

      constructor() {
            super();
            this.executeViewerModeHandler = this._executeViewer.bind(this);
      }

      _onDestroy() {
            // this.$container.tooltip("destroy");
            wemb.$globalBus.$off(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, this.executeViewerModeHandler);
            this.executeViewerModeHandler = null;
            super._onDestroy();
      }

      executeViewerMode(){
            super.executeViewerMode();
            wemb.$globalBus.$on(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, this.executeViewerModeHandler);
      }

      _executeViewer() {
            this.getAssetData();
      }

      _onCreateElement() {
            let str = `<div style="height: 100%;">
                              <div class="container">
                                    <div class="border-label">{{ initTitle }}</div>
                                    <div class="tree-bg">
                                         <div class="tree" v-if="treeData">
                                               <div style="padding: 10px;"> <span><input type="text" v-model="searchStr" @keyup="filterTreeData"></span></div>
                                               <ul style="overflow: auto">
                                                  <li v-for="item in treeData">
                                                      <span class="type-item">
                                                          <div class="icon icon-closed" @click="slideAnimation($event)"><i class="fa fa-angle-down"></i></div>
                                                          <label :type-name="item" :type-id="item">{{item.name}} ({{item.length}})</label>
                                                      </span>
                                                      <ul style="display: none;">
                                                          <li v-for="asset in item.children" :data-name="asset.assetInfo.name"
                                                                    v-if="asset.assetInfo.name.toLowerCase().includes(searchStr.toLowerCase())">
                                                              <span class="tree-item"
                                                                        @click="clickAsset(asset.assetInfo)" @dblclick="dblClickAsset(asset.assetInfo)" 
                                                                        @mouseover="hoverAsset(asset.assetInfo)" @mouseout="hoverAsset(asset.assetInfo)">
                                                                  <div class="icon-bullet"></div><label>{{asset.assetInfo.name}}</label>
                                                              </span>
                                                          </li>
                                                      </ul>
                                                  </li>
                                               </ul>
                                         </div>
                                         <div v-else class='empty-msg'>
                                              <span>{{lang.assetPanel.noMountedAsset}}</span>
                                          </div>
                                    </div>
                              </div>
                              <div class="icon-bg" @click="foldAnimation($event)">
                                   <div class="bullet"></div>
                              </div>
                        </div>
                        `;
            let temp = $(str);
            $(this._element).append(temp);
            
            let self = this;
            let assetTitle = this.getGroupPropertyValue('title', 'list_title'); // 처음 DB의 Setting 값으로 되어있는 title
            
            this.app = new Vue({
                  el: temp.get(0),
                  data: function() {
                        return {
                              lang : Vue.$i18n.messages.wv.dcim_pack,
                              searchStr: '',
                              treeData: null,
                              typeInfoMap: {},
                              title : assetTitle
                        }
                  },
                  computed: {
                        initTitle: function(){
                              return this.title.length === 0 ? this.lang.assetPanel.assetList : this.title;
                        }
                  },
                  methods: {
                        filterTreeData:function() {
                              if(this.last_asset) this.last_asset.comInstance.hideCurrentState();
                              this.treeData.map(data => {
                                    data.length = data.children.filter(x=> x.assetInfo.name.toUpperCase().indexOf(this.searchStr.toUpperCase()) >= 0).length;
                              })
                        },
                        _setTreeData:function(data) {
                              this.treeData = data;
                        },

                        dblClickAsset:function(asset) {
                              window.wemb.dcimManager.actionController.gotoAsset3DComponent(asset.id);
                              self.dispatchWScriptEvent("dblClickItem", asset);
                        },

                        clickAsset:function(asset) {
                              self.dispatchWScriptEvent("clickItem", asset);
                        },

                        hoverAsset:function(asset){
                              let assetInfo = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(asset.id);
                              if(event.type == 'mouseout') {
                                    this.last_asset = null;
                                    assetInfo.comInstance.hideCurrentState();
                              } else {
                                    this.last_asset = assetInfo;
                                    assetInfo.comInstance.displayCurrentState();
                              }
                        },

                        foldAnimation:function($event) {
                              // 패널 위치 변경
                              let target = $($event.currentTarget);

                              self.$container = $(self._element).find(".container");
                              self.$container.toggle("slide", {direction: "right"}, 800);

                              // 화살표 위치 변경
                              target.toggleClass("icon-bg-close");

                              // 화살표 모양 변경
                              if (target.hasClass("icon-bg-close")) {
                                    target.animate({left: (self.width - 40) + 'px'}, 800);
                                    target.find(".bullet").attr("class", "bullet-open");
                              } else {
                                    target.find(".bullet-open").attr("class", "bullet");
                                    target.animate({left: '-20px'}, 800);
                              }
                        },

                        slideAnimation:function(event) {
                              var target = $(event.currentTarget);
                              var submenu = target.parent().next();
                              if (submenu.is(":visible")) {
                                    target.removeClass("icon-opened").addClass("icon-closed");
                                    target.find("i").attr("class", "fa fa-angle-right");
                              } else {
                                    target.removeClass("icon-closed").addClass("icon-opened");
                                    target.find("i").attr("class", "fa fa-angle-down");
                              }

                              submenu.slideToggle();
                        }
                  }

            });

            $(this._element).addClass("asset-tree-panel");
            this.$container = $(this._element).find(".container");
            // this.$container.tooltip({
            //       items       :"li",
            //       content     :function(){
            //             let msg = $(this).attr("data-name");
            //             return msg;
            //       }
            // })
      }
      _onCommitProperties() {
            super._onCommitProperties();
            if (this._updatePropertiesMap.has("title")) {
                  this.app.title = this.getGroupPropertyValue('title', 'list_title');
                  this.app.$forceUpdate();
            }
      }
      getAssetData() {
            let assetList = [];
            let keys = [ ...wemb.dcimManager.assetAllocationProxy.assetCompositionInfoGroupMap.keys() ];
            let typeInfo = Object.create(wemb.dcimManager.assetManager.assetTypes);

            keys.map(key=>{
                  let children = $.extend(true,[],wemb.dcimManager.assetAllocationProxy.assetCompositionInfoGroupMap.get(key));
                  let item = {
                        id: key,
                        name: typeInfo.find(x=>x.id == key).name,
                        length: children.length,
                        children: children.slice(0)
                  }
                  assetList.push(item);
            })

            assetList = assetList.map(function(row) {
                  row.children = row.children.map(function(child) {
                        return {
                              assetId : child.assetId,
                              assetInfo : child.assetInfo,
                              assetType : child.assetType,
                              comInstance : Object.create(child.comInstance),
                              comInstanceId : child.comInstanceId,
                              comInstanceName : child.comInstanceName
                        }
                  })
                  return row;
            })

            this.app._setTreeData(assetList);
      }
}

WVPropertyManager.attach_default_component_infos(DCIMAssetPanelComponent, {
      "info": {
            "componentName": "DCIMAssetPanelComponent",
            "version": "1.1.0"
      },
      "setter": {
            "width": 230,
            "height": 550
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIMAssetPanelComponent"
      },
      "title" : {
            "list_title" : ''
      }
});

WVPropertyManager.add_event(DCIMAssetPanelComponent, {
      name: "dblClickItem",
      label: "dblClickItem Event",
      description: "dblClickItem Event",
      properties: []
});


WVPropertyManager.add_event(DCIMAssetPanelComponent, {
      name: "clickItem",
      label: "clickItem Event",
      description: "clickItem Event",
      properties: []
});



DCIMAssetPanelComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}]

WVPropertyManager.add_property_panel_group_info(DCIMAssetPanelComponent, {
      label : "List Title",
      template : "vertical",
      children : [
            {
                  owner : "title",
                  name : "list_title",
                  type : "string",
                  label : "Title",
                  show : true,
                  writable : true,
                  description : "타이틀"
            }
      ]
});
