"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var DCIMAssetPanelComponent =
/*#__PURE__*/
function (_WVDOMComponent) {
  _inherits(DCIMAssetPanelComponent, _WVDOMComponent);

  function DCIMAssetPanelComponent() {
    var _this;

    _classCallCheck(this, DCIMAssetPanelComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DCIMAssetPanelComponent).call(this));
    _this.executeViewerModeHandler = _this._executeViewer.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(DCIMAssetPanelComponent, [{
    key: "_onDestroy",
    value: function _onDestroy() {
      // this.$container.tooltip("destroy");
      wemb.$globalBus.$off(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, this.executeViewerModeHandler);
      this.executeViewerModeHandler = null;

      _get(_getPrototypeOf(DCIMAssetPanelComponent.prototype), "_onDestroy", this).call(this);
    }
  }, {
    key: "executeViewerMode",
    value: function executeViewerMode() {
      _get(_getPrototypeOf(DCIMAssetPanelComponent.prototype), "executeViewerMode", this).call(this);

      wemb.$globalBus.$on(AssetAllocationProxy.NOTI_UPDATE_ASSET_ALLOCATION_LIST, this.executeViewerModeHandler);
    }
  }, {
    key: "_executeViewer",
    value: function _executeViewer() {
      this.getAssetData();
    }
  }, {
    key: "_onCreateElement",
    value: function _onCreateElement() {
      var str = "<div style=\"height: 100%;\">\n                              <div class=\"container\">\n                                    <div class=\"border-label\">{{ initTitle }}</div>\n                                    <div class=\"tree-bg\">\n                                         <div class=\"tree\" v-if=\"treeData\">\n                                               <div style=\"padding: 10px;\"> <span><input type=\"text\" v-model=\"searchStr\" @keyup=\"filterTreeData\"></span></div>\n                                               <ul style=\"overflow: auto\">\n                                                  <li v-for=\"item in treeData\">\n                                                      <span class=\"type-item\">\n                                                          <div class=\"icon icon-closed\" @click=\"slideAnimation($event)\"><i class=\"fa fa-angle-down\"></i></div>\n                                                          <label :type-name=\"item\" :type-id=\"item\">{{item.name}} ({{item.length}})</label>\n                                                      </span>\n                                                      <ul style=\"display: none;\">\n                                                          <li v-for=\"asset in item.children\" :data-name=\"asset.assetInfo.name\"\n                                                                    v-if=\"asset.assetInfo.name.toLowerCase().includes(searchStr.toLowerCase())\">\n                                                              <span class=\"tree-item\"\n                                                                        @click=\"clickAsset(asset.assetInfo)\" @dblclick=\"dblClickAsset(asset.assetInfo)\" \n                                                                        @mouseover=\"hoverAsset(asset.assetInfo)\" @mouseout=\"hoverAsset(asset.assetInfo)\">\n                                                                  <div class=\"icon-bullet\"></div><label>{{asset.assetInfo.name}}</label>\n                                                              </span>\n                                                          </li>\n                                                      </ul>\n                                                  </li>\n                                               </ul>\n                                         </div>\n                                         <div v-else class='empty-msg'>\n                                              <span>{{lang.assetPanel.noMountedAsset}}</span>\n                                          </div>\n                                    </div>\n                              </div>\n                              <div class=\"icon-bg\" @click=\"foldAnimation($event)\">\n                                   <div class=\"bullet\"></div>\n                              </div>\n                        </div>\n                        ";
      var temp = $(str);
      $(this._element).append(temp);
      var self = this;
      var assetTitle = this.getGroupPropertyValue('title', 'list_title'); // 처음 DB의 Setting 값으로 되어있는 title

      this.app = new Vue({
        el: temp.get(0),
        data: function data() {
          return {
            lang: Vue.$i18n.messages.wv.dcim_pack,
            searchStr: '',
            treeData: null,
            typeInfoMap: {},
            title: assetTitle
          };
        },
        computed: {
          initTitle: function initTitle() {
            return this.title.length === 0 ? this.lang.assetPanel.assetList : this.title;
          }
        },
        methods: {
          filterTreeData: function filterTreeData() {
            var _this2 = this;

            if (this.last_asset) this.last_asset.comInstance.hideCurrentState();
            this.treeData.map(function (data) {
              data.length = data.children.filter(function (x) {
                return x.assetInfo.name.toUpperCase().indexOf(_this2.searchStr.toUpperCase()) >= 0;
              }).length;
            });
          },
          _setTreeData: function _setTreeData(data) {
            this.treeData = data;
          },
          dblClickAsset: function dblClickAsset(asset) {
            window.wemb.dcimManager.actionController.gotoAsset3DComponent(asset.id);
            self.dispatchWScriptEvent("dblClickItem", asset);
          },
          clickAsset: function clickAsset(asset) {
            self.dispatchWScriptEvent("clickItem", asset);
          },
          hoverAsset: function hoverAsset(asset) {
            var assetInfo = wemb.dcimManager.assetAllocationProxy.assetCompositionInfoMap.get(asset.id);

            if (event.type == 'mouseout') {
              this.last_asset = null;
              assetInfo.comInstance.hideCurrentState();
            } else {
              this.last_asset = assetInfo;
              assetInfo.comInstance.displayCurrentState();
            }
          },
          foldAnimation: function foldAnimation($event) {
            // 패널 위치 변경
            var target = $($event.currentTarget);
            self.$container = $(self._element).find(".container");
            self.$container.toggle("slide", {
              direction: "right"
            }, 800); // 화살표 위치 변경

            target.toggleClass("icon-bg-close"); // 화살표 모양 변경

            if (target.hasClass("icon-bg-close")) {
              target.animate({
                left: self.width - 40 + 'px'
              }, 800);
              target.find(".bullet").attr("class", "bullet-open");
            } else {
              target.find(".bullet-open").attr("class", "bullet");
              target.animate({
                left: '-20px'
              }, 800);
            }
          },
          slideAnimation: function slideAnimation(event) {
            var target = $(event.currentTarget);
            var submenu = target.parent().next();

            if (submenu.is(":visible")) {
              target.removeClass("icon-opened").addClass("icon-closed");
              target.find("i").attr("class", "fa fa-angle-right");
            } else {
              target.removeClass("icon-closed").addClass("icon-opened");
              target.find("i").attr("class", "fa fa-angle-down");
            }

            submenu.slideToggle();
          }
        }
      });
      $(this._element).addClass("asset-tree-panel");
      this.$container = $(this._element).find(".container"); // this.$container.tooltip({
      //       items       :"li",
      //       content     :function(){
      //             let msg = $(this).attr("data-name");
      //             return msg;
      //       }
      // })
    }
  }, {
    key: "_onCommitProperties",
    value: function _onCommitProperties() {
      _get(_getPrototypeOf(DCIMAssetPanelComponent.prototype), "_onCommitProperties", this).call(this);

      if (this._updatePropertiesMap.has("title")) {
        this.app.title = this.getGroupPropertyValue('title', 'list_title');
        this.app.$forceUpdate();
      }
    }
  }, {
    key: "getAssetData",
    value: function getAssetData() {
      var assetList = [];

      var keys = _toConsumableArray(wemb.dcimManager.assetAllocationProxy.assetCompositionInfoGroupMap.keys());

      var typeInfo = Object.create(wemb.dcimManager.assetManager.assetTypes);
      keys.map(function (key) {
        var children = $.extend(true, [], wemb.dcimManager.assetAllocationProxy.assetCompositionInfoGroupMap.get(key));
        var item = {
          id: key,
          name: typeInfo.find(function (x) {
            return x.id == key;
          }).name,
          length: children.length,
          children: children.slice(0)
        };
        assetList.push(item);
      });
      assetList = assetList.map(function (row) {
        row.children = row.children.map(function (child) {
          return {
            assetId: child.assetId,
            assetInfo: child.assetInfo,
            assetType: child.assetType,
            comInstance: Object.create(child.comInstance),
            comInstanceId: child.comInstanceId,
            comInstanceName: child.comInstanceName
          };
        });
        return row;
      });

      this.app._setTreeData(assetList);
    }
  }]);

  return DCIMAssetPanelComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DCIMAssetPanelComponent, {
  "info": {
    "componentName": "DCIMAssetPanelComponent",
    "version": "1.1.0"
  },
  "setter": {
    "width": 230,
    "height": 550
  },
  "label": {
    "label_using": "N",
    "label_text": "DCIMAssetPanelComponent"
  },
  "title": {
    "list_title": ''
  }
});
WVPropertyManager.add_event(DCIMAssetPanelComponent, {
  name: "dblClickItem",
  label: "dblClickItem Event",
  description: "dblClickItem Event",
  properties: []
});
WVPropertyManager.add_event(DCIMAssetPanelComponent, {
  name: "clickItem",
  label: "clickItem Event",
  description: "clickItem Event",
  properties: []
});
DCIMAssetPanelComponent.property_panel_info = [{
  template: "primary"
}, {
  template: "pos-size-2d"
}, {
  template: "cursor"
}, {
  template: "label"
}];
WVPropertyManager.add_property_panel_group_info(DCIMAssetPanelComponent, {
  label: "List Title",
  template: "vertical",
  children: [{
    owner: "title",
    name: "list_title",
    type: "string",
    label: "Title",
    show: true,
    writable: true,
    description: "타이틀"
  }]
});