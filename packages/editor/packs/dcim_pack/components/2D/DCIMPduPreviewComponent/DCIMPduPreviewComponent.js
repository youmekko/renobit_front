class DCIMPduPreviewComponent extends WVDOMComponent {
      constructor() {
            super();

            this.PDU_COL = "unit_size";
      }

      _onDestroy() {
            if (this.app) {
                  this.app.$destroy();
                  this.app = null;
            }

            super._onDestroy();
      }

      _onCreateElement() {
            let str = `<div class="pdu-wrap">
                              <div class="item-wrap" v-for="index in pduUnitSize">
                                    <div class="mount-item" @click="clickContent('LEFT_' + index)">
                                          <div class="mount-item-content" :class="{active: active == 'LEFT_' + index,mounted: mountList['LEFT_' + index]}">
                                               <el-tooltip v-if="mountList['LEFT_' + index]" :content="mountList['LEFT_' + index].name" placement="top">
                                                    <div class="mounted-label" v-text="mountList['LEFT_' + index].name"></div>
                                               </el-tooltip>
                                          </div>
                                          <span class="mount-item-index">{{index}}</span>
                                    </div>
                                    <div class="mount-partition"></div>
                                    <div class="mount-item" @click="clickContent('RIGHT_' + index)">
                                          <span class="mount-item-index">{{index}}</span>
                                          <div class="mount-item-content" :class="{active: active == 'RIGHT_' + index, mounted: mountList['RIGHT_' + index]}">
                                            <el-tooltip v-if="mountList['RIGHT_' + index]" :content="mountList['RIGHT_' + index].name" placement="top">
                                                <div class="mounted-label" v-text="mountList['RIGHT_' + index].name"></div>
                                            </el-tooltip>
                                          </div>
                                    </div>
                              </div>
                       </div>`;
            let temp = $(str);
            let self = this;
            $(this._element).addClass("pdu-preview-component").append(temp);
            this.app = new Vue({
                  el: temp.get(0),
                  data: function () {
                        return {
                              pduUnitSize: 1,
                              mountList: {},
                              asset_id: 'test',
                              active: ''
                        }
                  },
                  mounted() {
                        if (!self.isEditorMode) {
                              this.callMountList();
                        }
                  },
                  methods: {
                        clickContent(info) {
                              this.active = info;
                              var data = {
                                    asset_id: self.assetId,
                                    port_id: info
                              }

                              if(this.mountList[info]) {
                                    data.child_id = this.mountList[info].child_id;
                                    data.index = this.mountList[info].index;
                                    data.direction = this.mountList[info].direction;
                              }

                              self.dispatchWScriptEvent("clickPort", data);
                        },

                        callMountList() {
                              window.wemb.dcimManager.getRelationData(self.assetId).then(res=>{
                                    var result = res[1].data.data;
                                    this.mountList = {};
                                    result.map((item, index)=> {
                                          var asset_info = wemb.dcimManager.assetManager.getAssetDataByAssetId(item.child_id);
                                          item.name = asset_info.name;
                                          this.mountList[item.direction + "_" + item.index] = item;
                                    });
                              }).catch(function (err) {
                                    console.log(err);
                              })
                        }
                  }
            })
      }

      _onCommitProperties() {
            if (this._invalidateAssetId) {
                  this.validateCallLater(this._validateAssetId);
                  this._invalidateAssetId = false;
            }

            if (this._invalidateAssetData) {
                  this.validateCallLater(this._validateAssetData);
                  this._invalidateAssetData = false;
            }

            if (this._invalidateActivePort) {
                  this.validateCallLater(this._validateActivePort);
                  this._invalidateActivePort = false;
            }
      }

      _validateAssetId() {
            this.app.callMountList();
      }

      _validateAssetData() {
            this.app.pduUnitSize = this.assetData[this.PDU_COL];
      }

      _validateActivePort() {
            this.app.clickContent(this.activePort);
      }

      set activePort(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "activePort", value)) {
                  this._invalidateActivePort = true;
            }
      }

      get activePort() {
            return this.getGroupPropertyValue("setter", "activePort");
      }

      set assetId(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "assetId", value)) {
                  this._invalidateAssetId = true;
            }
      }

      get assetId() {
            return this.getGroupPropertyValue("setter", "assetId");
      }

      set assetData(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "assetData", value)) {
                  this._invalidateAssetData = true;
            }
      }

      get assetData() {
            return this.getGroupPropertyValue("setter", "assetData");
      }
}

WVPropertyManager.attach_default_component_infos(DCIMPduPreviewComponent, {
      "info": {
            "componentName": "DCIMPduPreviewComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 240,
            "height": 520
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIMPduPreviewComponent"
      },
      "setter": {
            "assetId": '',
            "assetData": '',
            "activePort": ''
      }
});

DCIMPduPreviewComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}];

WVPropertyManager.add_event(DCIMPduPreviewComponent, {
      name: "clickPort",
      label: "clickPort Event",
      description: "clickPort Event",
      properties: []
});
