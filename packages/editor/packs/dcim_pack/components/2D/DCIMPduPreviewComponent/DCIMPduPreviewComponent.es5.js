"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var DCIMPduPreviewComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(DCIMPduPreviewComponent, _WVDOMComponent);

            function DCIMPduPreviewComponent() {
                  var _this;

                  _classCallCheck(this, DCIMPduPreviewComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(DCIMPduPreviewComponent).call(this));
                  _this.PDU_COL = "unit_size";
                  return _this;
            }

            _createClass(DCIMPduPreviewComponent, [{
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        if (this.app) {
                              this.app.$destroy();
                              this.app = null;
                        }

                        _get(_getPrototypeOf(DCIMPduPreviewComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        var str = "<div class=\"pdu-wrap\">\n                              <div class=\"item-wrap\" v-for=\"index in pduUnitSize\">\n                                    <div class=\"mount-item\" @click=\"clickContent('LEFT_' + index)\">\n                                          <div class=\"mount-item-content\" :class=\"{active: active == 'LEFT_' + index,mounted: mountList['LEFT_' + index]}\">\n                                               <el-tooltip v-if=\"mountList['LEFT_' + index]\" :content=\"mountList['LEFT_' + index].name\" placement=\"top\">\n                                                    <div class=\"mounted-label\" v-text=\"mountList['LEFT_' + index].name\"></div>\n                                               </el-tooltip>\n                                          </div>\n                                          <span class=\"mount-item-index\">{{index}}</span>\n                                    </div>\n                                    <div class=\"mount-partition\"></div>\n                                    <div class=\"mount-item\" @click=\"clickContent('RIGHT_' + index)\">\n                                          <span class=\"mount-item-index\">{{index}}</span>\n                                          <div class=\"mount-item-content\" :class=\"{active: active == 'RIGHT_' + index, mounted: mountList['RIGHT_' + index]}\">\n                                            <el-tooltip v-if=\"mountList['RIGHT_' + index]\" :content=\"mountList['RIGHT_' + index].name\" placement=\"top\">\n                                                <div class=\"mounted-label\" v-text=\"mountList['RIGHT_' + index].name\"></div>\n                                            </el-tooltip>\n                                          </div>\n                                    </div>\n                              </div>\n                       </div>";
                        var temp = $(str);
                        var self = this;
                        $(this._element).addClass("pdu-preview-component").append(temp);
                        this.app = new Vue({
                              el: temp.get(0),
                              data: function data() {
                                    return {
                                          pduUnitSize: 1,
                                          mountList: {},
                                          asset_id: 'test',
                                          active: ''
                                    };
                              },
                              mounted: function mounted() {
                                    if (!self.isEditorMode) {
                                          this.callMountList();
                                    }
                              },
                              methods: {
                                    clickContent: function clickContent(info) {
                                          this.active = info;
                                          var data = {
                                                asset_id: self.assetId,
                                                port_id: info
                                          };

                                          if (this.mountList[info]) {
                                                data.child_id = this.mountList[info].child_id;
                                                data.index = this.mountList[info].index;
                                                data.direction = this.mountList[info].direction;
                                          }

                                          self.dispatchWScriptEvent("clickPort", data);
                                    },
                                    callMountList: function callMountList() {
                                          var _this2 = this;

                                          window.wemb.dcimManager.getRelationData(self.assetId).then(function (res) {
                                                var result = res[1].data.data;
                                                _this2.mountList = {};
                                                result.map(function (item, index) {
                                                      var asset_info = wemb.dcimManager.assetManager.getAssetDataByAssetId(item.child_id);
                                                      item.name = asset_info.name;
                                                      _this2.mountList[item.direction + "_" + item.index] = item;
                                                });
                                          }).catch(function (err) {
                                                console.log(err);
                                          });
                                    }
                              }
                        });
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._invalidateAssetId) {
                              this.validateCallLater(this._validateAssetId);
                              this._invalidateAssetId = false;
                        }

                        if (this._invalidateAssetData) {
                              this.validateCallLater(this._validateAssetData);
                              this._invalidateAssetData = false;
                        }

                        if (this._invalidateActivePort) {
                              this.validateCallLater(this._validateActivePort);
                              this._invalidateActivePort = false;
                        }
                  }
            }, {
                  key: "_validateAssetId",
                  value: function _validateAssetId() {
                        this.app.callMountList();
                  }
            }, {
                  key: "_validateAssetData",
                  value: function _validateAssetData() {
                        this.app.pduUnitSize = this.assetData[this.PDU_COL];
                  }
            }, {
                  key: "_validateActivePort",
                  value: function _validateActivePort() {
                        this.app.clickContent(this.activePort);
                  }
            }, {
                  key: "activePort",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("setter", "activePort", value)) {
                              this._invalidateActivePort = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "activePort");
                  }
            }, {
                  key: "assetId",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("setter", "assetId", value)) {
                              this._invalidateAssetId = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "assetId");
                  }
            }, {
                  key: "assetData",
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("setter", "assetData", value)) {
                              this._invalidateAssetData = true;
                        }
                  },
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "assetData");
                  }
            }]);

            return DCIMPduPreviewComponent;
      }(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DCIMPduPreviewComponent, _defineProperty({
      "info": {
            "componentName": "DCIMPduPreviewComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 240,
            "height": 520
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIMPduPreviewComponent"
      }
}, "setter", {
      "assetId": '',
      "assetData": '',
      "activePort": ''
}));
DCIMPduPreviewComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}];
WVPropertyManager.add_event(DCIMPduPreviewComponent, {
      name: "clickPort",
      label: "clickPort Event",
      description: "clickPort Event",
      properties: []
});
