class DCIMAssetCounterComponent extends WVDOMComponent {
      constructor() {
            super();
            this.app;
            this.requestIntervalTime = DCIMManager.EVENT_POLLING_INTERVAL_TIME;
            this.requestTimer = null;
            this.isDestroyed = false;
            this.tempEventCountData = '';
            this._typeList = [];
            this._typeIds = [];
            this._pageId = wemb.pageManager.currentPageInfo.id;

      }

      _onDestroy() {
            clearTimeout(this.requestTimer);
            this.isDestroyed = true;
            this.app = null;
            this._typeList = null;
            this._typeIds = null;
            super._onDestroy();
      }

      _onCreateElement() {
            this.createVueElement();
            this.updateTypeset();
            if (!this.isEditorMode) {
                  this.requestEventCount();
                  this.pollingRequestEventCount();
            }
      }

      createVueElement() {
            var str = `
            <div class="container">
                  <div class="cont-wrap" style="width: 100%; height: 100%;overflow: hidden;position: absolute;top:0;left:0;">
                       <div style="display:inline-block;">
                              <div class="border-label">{{ initTitle }}</div>
                                    <div class="asset-info-back">
                                          <div class="asset-wrap" v-for="typeId in typeIds">
                                                <div class="asset-count">{{assetData[typeId]}}</div>
                                                <div class="asset-event-count">{{getEventCount(typeId)}}</div>                                                      
                                                <el-tooltip effect="light" placement="top">
                                                      <div slot="content">{{ typeId }}</div>
                                                      <img class="asset-image" :src="getImagePath(typeId)">
                                                </el-tooltip>
                                          </div>
                                    </div>
                              </div>
                        </div>                        
                        <div class="icon-bg" @click="toggleState($event)">
                            <div class="bullet"></div>
                        </div>
                  </div>
            </div>
            `;


            let $temp = $(str);
            $(this._element).append($temp);

            var imageRootPath = DCIMManager.PACK_PATH + this.componentName + "/resource/";
            let assetTitle = this.getGroupPropertyValue('title', 'asset_list_title'); // 처음 DB의 Setting 값으로 되어있는 title

            var self = this;
            this.app = new Vue({
                  el: $temp.get(0),
                  data: function () {
                        return {
                              lang: Vue.$i18n.messages.wv.dcim_pack,
                              $bgEl: null,
                              $container: null,
                              assetData: null,
                              eventsData: {},
                              imgRootPath: imageRootPath,
                              typeIds: [],
                              testCount: 0,
                              title: assetTitle
                        }
                  },
                  computed: {
                        initTitle: function(){
                              return this.title.length === 0 ? this.lang.common.assetStatusInfo : this.title;
                        }
                  },
                  mounted: function () {
                        this.$bgEl = $(this.$el).find(".icon-bg");
                        this.$container = $(this.$el).find(".cont-wrap");
                  },

                  methods: {
                        getImagePath(asset) {
                              return this.imgRootPath + "/" + asset + ".png";
                        },

                        getEventCount(typeId) {
                              if (this.eventsData[typeId] == undefined) {
                                    return "0";
                              }

                              return this.eventsData[typeId];
                        },

                        updateEventCounts(dataObj) {
                              this.testCount++;
                              this.eventsData = dataObj;
                        },

                        updateTypeCounts(data) {
                              let obj = {}
                              this.typeIds.forEach((typeId) => {
                                    let typeData = data[typeId];
                                    let count = 0;

                                    if (typeData) {
                                          count = typeData.length;
                                    }

                                    obj[typeId] = count;
                              })

                              this.assetData = obj;
                        },

                        updateTypeIds(data) {
                              this.typeIds = data;
                        },

                        toggleState($event) {

                              let target = $($event.currentTarget);

                              this.$container.toggle("slide", {direction: "right"}, 800);
                              // 화살표 위치 변경
                              target.toggleClass("icon-bg-close");

                              // 화살표 모양 변경
                              if (target.hasClass("icon-bg-close")) {
                                    target.animate({left: (self.width - 40) + 'px'}, 800);
                                    target.find(".bullet").attr("class", "bullet-open");
                              } else {
                                    target.find(".bullet-open").attr("class", "bullet");
                                    target.animate({left: '-20px'}, 800);
                              }
                        },

                  }
            });
      }

      pollingRequestEventCount() {
            if (this.isDestroyed) {
                  return;
            }
            this.requestTimer = setTimeout(() => {
                  this.requestEventCount()
            }, this.requestIntervalTime);
      }

      requestEventCount() {
            if (this.isDestroyed) {
                  return;
            }
            clearTimeout(this.requestTimer);

            let pageIds = window.wemb.dcimManager.pagesetManager.getAllPageIds();
            window.wemb.dcimManager.eventManager.requestSeverityCount(pageIds, this.typeIds).then((result) => {
                  if (this.isDestroyed) {
                        return;
                  }
                  /*result = {
                      "4c1ea0db-6688-4b28-af61-801dd0d44ac0": {
                          "FireSensor": {
                              "minor": 3,
                              "major": 5,
                              "normal": 6
                          }
                      },

                      "e3796fc4-1a68-474c-90c8-05466e10f3bc": {
                          "FireSensor": {
                              "minor": 1,
                              "major": 2,
                              "normal": 3
                          },
                          "pdu": {
                              "minor": 3,
                              "major": 2,
                              "critical": 1
                          }
                      }
                  }*/

                  let data = result || {};
                  let jsonStr = JSON.stringify(data);

                  if (this.tempEventCountData != jsonStr) {
                        this.updateAssetsEventCount(data);
                  }
                  this.tempEventCountData = jsonStr;
            }).finally(() => {
                  this.pollingRequestEventCount();
            });
      }

      getMergeEventCountData(dataObj) {
            let mergeData = {};
            $.each(dataObj, (key, pageEventObj) => {
                  this._typeList.forEach((typeId) => {
                        let typeCountObj = pageEventObj[typeId];
                        if (typeCountObj) {
                              let count = 0;
                              $.each(typeCountObj, (key, value) => {
                                    count += value;
                              });

                              if (mergeData[typeId]) {
                                    mergeData[typeId] += count;
                              } else {
                                    mergeData[typeId] = count;
                              }
                        }
                  })
            })
            return mergeData;
      }

      updateAssetsEventCount(data) {
            let eventCountObj = this.getMergeEventCountData(data.data);
            this.app.updateEventCounts(eventCountObj);
      }

      updateTypeset() {
            this.updateTypeList();
            this.updateAssetCountList();
      }

      _onCommitProperties() {
            super._onCommitProperties();
            if (this._updatePropertiesMap.has("title")) {
                  this.app.title = this.getGroupPropertyValue('title', 'asset_list_title');
                  this.app.$forceUpdate();
            }
      }

      /*자산 타입 구성 업데이트*/
      updateTypeList() {
            this._typeList = [];
            let tmpList = [];
            // 페이지셋에 설정된 배치된 모든 자산 타입리스트 가져오기.
            let pageIds = window.wemb.dcimManager.pagesetManager.getAllPageIds();
            pageIds.map(id => {
                  let types = wemb.dcimManager.assetManager.getAssetTypesInPage(id) || [];
                  tmpList.push(...types)
            });

            // 중복 타입 제거
            this._typeList = tmpList.reduce((a, b) => {
                  if (a.indexOf(b) < 0) a.push(b);
                  return a;
            }, [])
            
            this.app.updateTypeIds(this._typeList);
      }

      /*자산 배치 개수 업데이트*/
      updateAssetCountList() {
            var assetCounts = wemb.dcimManager.assetManager.getDeployedAssets();
            this.app.updateTypeCounts(assetCounts);
      }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(DCIMAssetCounterComponent, {
      "info": {
            "componentName": "DCIMAssetCounterComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 340,
            "height": 140
      },

      "label": {
            "label_using": "N",
            "label_text": "Asset Counter Component"
      },

      "font": {
            "font_type": "inherit"
      },
      "title": {
            "asset_list_title": ''
      }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
DCIMAssetCounterComponent.property_panel_info = [{
      template: "primary",
      label: "primary"
},
      {
            template: "pos-size-2d",
            label: "pos-size-2d"
      },
      {
            template: "label",
            label: "label"
      }
];

WVPropertyManager.add_property_panel_group_info(DCIMAssetCounterComponent, {
      label : "Asset List Title",
      template : "vertical",
      children : [
            {
                  owner : "title",
                  name : "asset_list_title",
                  type : "string",
                  label : "Title",
                  show : true,
                  writable : true,
                  description : "전체 자산 리스트 타이틀"
            }
      ]
});
