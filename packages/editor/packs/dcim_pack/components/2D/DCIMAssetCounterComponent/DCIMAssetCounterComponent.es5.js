"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DCIMAssetCounterComponent = function (_WVDOMComponent) {
      _inherits(DCIMAssetCounterComponent, _WVDOMComponent);

      function DCIMAssetCounterComponent() {
            _classCallCheck(this, DCIMAssetCounterComponent);

            var _this = _possibleConstructorReturn(this, (DCIMAssetCounterComponent.__proto__ || Object.getPrototypeOf(DCIMAssetCounterComponent)).call(this));

            _this.app;
            _this.requestIntervalTime = DCIMManager.EVENT_POLLING_INTERVAL_TIME;
            _this.requestTimer = null;
            _this.isDestroyed = false;
            _this.tempEventCountData = '';
            _this._typeList = [];
            _this._typeIds = [];
            _this._pageId = wemb.pageManager.currentPageInfo.id;

            return _this;
      }

      _createClass(DCIMAssetCounterComponent, [{
            key: "_onDestroy",
            value: function _onDestroy() {
                  clearTimeout(this.requestTimer);
                  this.isDestroyed = true;
                  this.app = null;
                  this._typeList = null;
                  this._typeIds = null;
                  _get(DCIMAssetCounterComponent.prototype.__proto__ || Object.getPrototypeOf(DCIMAssetCounterComponent.prototype), "_onDestroy", this).call(this);
            }
      }, {
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  this.createVueElement();
                  this.updateTypeset();
                  if (!this.isEditorMode) {
                        this.requestEventCount();
                        this.pollingRequestEventCount();
                  }
            }
      }, {
            key: "createVueElement",
            value: function createVueElement() {
                  var str = "\n            <div class=\"container\">\n                  <div class=\"cont-wrap\" style=\"width: 100%; height: 100%;overflow: hidden;position: absolute;top:0;left:0;\">\n                       <div style=\"display:inline-block;\">\n                              <div class=\"border-label\">{{ initTitle }}</div>\n                                    <div class=\"asset-info-back\">\n                                          <div class=\"asset-wrap\" v-for=\"typeId in typeIds\">\n                                                <div class=\"asset-count\">{{assetData[typeId]}}</div>\n                                                <div class=\"asset-event-count\">{{getEventCount(typeId)}}</div>                                                      \n                                                <el-tooltip effect=\"light\" placement=\"top\">\n                                                      <div slot=\"content\">{{ typeId }}</div>\n                                                      <img class=\"asset-image\" :src=\"getImagePath(typeId)\">\n                                                </el-tooltip>\n                                          </div>\n                                    </div>\n                              </div>\n                        </div>                        \n                        <div class=\"icon-bg\" @click=\"toggleState($event)\">\n                            <div class=\"bullet\"></div>\n                        </div>\n                  </div>\n            </div>\n            ";

                  var $temp = $(str);
                  $(this._element).append($temp);

                  var imageRootPath = DCIMManager.PACK_PATH + this.componentName + "/resource/";
                  var assetTitle = this.getGroupPropertyValue('title', 'asset_list_title'); // 처음 DB의 Setting 값으로 되어있는 title

                  var self = this;
                  this.app = new Vue({
                        el: $temp.get(0),
                        data: function data() {
                              return {
                                    lang: Vue.$i18n.messages.wv.dcim_pack,
                                    $bgEl: null,
                                    $container: null,
                                    assetData: null,
                                    eventsData: {},
                                    imgRootPath: imageRootPath,
                                    typeIds: [],
                                    testCount: 0,
                                    title: assetTitle
                              };
                        },
                        computed: {
                              initTitle: function initTitle() {
                                return this.title.length === 0 ? this.lang.common.assetStatusInfo : this.title;
                              }
                        },

                        mounted: function mounted() {
                              this.$bgEl = $(this.$el).find(".icon-bg");
                              this.$container = $(this.$el).find(".cont-wrap");
                        },

                        methods: {
                              getImagePath: function getImagePath(asset) {
                                    return this.imgRootPath + "/" + asset + ".png";
                              },
                              getEventCount: function getEventCount(typeId) {
                                    if (this.eventsData[typeId] == undefined) {
                                          return "0";
                                    }

                                    return this.eventsData[typeId];
                              },
                              updateEventCounts: function updateEventCounts(dataObj) {
                                    this.testCount++;
                                    this.eventsData = dataObj;
                              },
                              updateTypeCounts: function updateTypeCounts(data) {
                                    var obj = {};
                                    this.typeIds.forEach(function (typeId) {
                                          var typeData = data[typeId];
                                          var count = 0;

                                          if (typeData) {
                                                count = typeData.length;
                                          }

                                          obj[typeId] = count;
                                    });

                                    this.assetData = obj;
                              },
                              updateTypeIds: function updateTypeIds(data) {
                                    this.typeIds = data;
                              },
                              toggleState: function toggleState($event) {

                                    var target = $($event.currentTarget);

                                    this.$container.toggle("slide", { direction: "right" }, 800);
                                    // 화살표 위치 변경
                                    target.toggleClass("icon-bg-close");

                                    // 화살표 모양 변경
                                    if (target.hasClass("icon-bg-close")) {
                                          target.animate({ left: self.width - 40 + 'px' }, 800);
                                          target.find(".bullet").attr("class", "bullet-open");
                                    } else {
                                          target.find(".bullet-open").attr("class", "bullet");
                                          target.animate({ left: '-20px' }, 800);
                                    }
                              }
                        }
                  });
            }
      }, {
            key: "pollingRequestEventCount",
            value: function pollingRequestEventCount() {
                  var _this2 = this;

                  if (this.isDestroyed) {
                        return;
                  }
                  this.requestTimer = setTimeout(function () {
                        _this2.requestEventCount();
                  }, this.requestIntervalTime);
            }
      }, {
            key: "requestEventCount",
            value: function requestEventCount() {
                  var _this3 = this;

                  if (this.isDestroyed) {
                        return;
                  }
                  clearTimeout(this.requestTimer);

                  var pageIds = window.wemb.dcimManager.pagesetManager.getAllPageIds();
                  window.wemb.dcimManager.eventManager.requestSeverityCount(pageIds, this.typeIds).then(function (result) {
                        if (_this3.isDestroyed) {
                              return;
                        }
                        /*result = {
                            "4c1ea0db-6688-4b28-af61-801dd0d44ac0": {
                                "FireSensor": {
                                    "minor": 3,
                                    "major": 5,
                                    "normal": 6
                                }
                            },
                             "e3796fc4-1a68-474c-90c8-05466e10f3bc": {
                                "FireSensor": {
                                    "minor": 1,
                                    "major": 2,
                                    "normal": 3
                                },
                                "pdu": {
                                    "minor": 3,
                                    "major": 2,
                                    "critical": 1
                                }
                            }
                        }*/

                        var data = result || {};
                        var jsonStr = JSON.stringify(data);

                        if (_this3.tempEventCountData != jsonStr) {
                              _this3.updateAssetsEventCount(data);
                        }
                        _this3.tempEventCountData = jsonStr;
                  }).finally(function () {
                        _this3.pollingRequestEventCount();
                  });
            }
      }, {
            key: "getMergeEventCountData",
            value: function getMergeEventCountData(dataObj) {
                  var _this4 = this;

                  var mergeData = {};
                  $.each(dataObj, function (key, pageEventObj) {
                        _this4._typeList.forEach(function (typeId) {
                              var typeCountObj = pageEventObj[typeId];
                              if (typeCountObj) {
                                    var count = 0;
                                    $.each(typeCountObj, function (key, value) {
                                          count += value;
                                    });

                                    if (mergeData[typeId]) {
                                          mergeData[typeId] += count;
                                    } else {
                                          mergeData[typeId] = count;
                                    }
                              }
                        });
                  });
                  return mergeData;
            }
      }, {
            key: "updateAssetsEventCount",
            value: function updateAssetsEventCount(data) {
                  var eventCountObj = this.getMergeEventCountData(data.data);
                  this.app.updateEventCounts(eventCountObj);
            }
      }, {
            key: "updateTypeset",
            value: function updateTypeset() {
                  this.updateTypeList();
                  this.updateAssetCountList();
            }
      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {
              _get(_getPrototypeOf(DCIMAssetCounterComponent.prototype), "_onCommitProperties", this).call(this);
        
              if (this._updatePropertiesMap.has("title")) {
                this.app.title = this.getGroupPropertyValue('title', 'asset_list_title');
                this.app.$forceUpdate();
              }
            }
      }, {
            /*자산 타입 구성 업데이트*/
            
            key: "updateTypeList",
            value: function updateTypeList() {
                  this._typeList = [];
                  var tmpList = [];
                  // 페이지셋에 설정된 배치된 모든 자산 타입리스트 가져오기.
                  var pageIds = window.wemb.dcimManager.pagesetManager.getAllPageIds();
                  pageIds.map(function (id) {
                        var types = wemb.dcimManager.assetManager.getAssetTypesInPage(id) || [];
                        tmpList.push.apply(tmpList, _toConsumableArray(types));
                  });

                  // 중복 타입 제거
                  this._typeList = tmpList.reduce(function (a, b) {
                        if (a.indexOf(b) < 0) a.push(b);
                        return a;
                  }, []);

                  this.app.updateTypeIds(this._typeList);
            }

            /*자산 배치 개수 업데이트*/

      }, {
            key: "updateAssetCountList",
            value: function updateAssetCountList() {
                  var assetCounts = wemb.dcimManager.assetManager.getDeployedAssets();
                  this.app.updateTypeCounts(assetCounts);
            }
      }]);

      return DCIMAssetCounterComponent;
}(WVDOMComponent);

// 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(DCIMAssetCounterComponent, {
      "info": {
            "componentName": "DCIMAssetCounterComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 340,
            "height": 140
      },

      "label": {
            "label_using": "N",
            "label_text": "Asset Counter Component"
      },

      "font": {
            "font_type": "inherit"
      },
      "title": {
            "asset_list_title": ''
      }
});

// 프로퍼티 패널에서 사용할 정보 입니다.
DCIMAssetCounterComponent.property_panel_info = [{
      template: "primary",
      label: "primary"
}, {
      template: "pos-size-2d",
      label: "pos-size-2d"
}, {
      template: "label",
      label: "label"
}];

WVPropertyManager.add_property_panel_group_info(DCIMAssetCounterComponent, {
      label: "Asset List Title",
      template: "vertical",
      children: [{
        owner: "title",
        name: "asset_list_title",
        type: "string",
        label: "Title",
        show: true,
        writable: true,
        description: "전체 자산 리스트 타이틀"
      }]
    });