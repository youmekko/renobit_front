class DCIMAllAssetCounterComponent extends WVDOMComponent {
      constructor() {
            super();
            this.requestIntervalTime = DCIMManager.EVENT_POLLING_INTERVAL_TIME;
            this.requestTimer = null;
            this.isDestroyed = false;
            this.tempEventCountData = '';
            this._pageIds = [];
            this._typeIds = [];
            this._locationList = null;
            this._floorList = [];
            this.app;
      }

      _onDestroy() {
            clearTimeout(this.requestTimer);
            this.isDestroyed = true;
            this.app = null;
            this._pageIds = null;
            this._typeIds = null;
            this._locationList = null;
            this._floorList = null;
            super._onDestroy();
      }

      _onCreateElement() {
            this.createVueElement();
            this.updatePagesetList();
            this.updateTypeset();
            this.bindEvent();
            if (!this.isEditorMode) {
                  this.requestEventCount();
                  this.pollingRequestEventCount();
            }
      }

      pollingRequestEventCount() {
            if (this.isDestroyed) {
                  return;
            }
            this.requestTimer = setTimeout(() => {
                  this.requestEventCount()
            }, this.requestIntervalTime);
      }

      requestEventCount() {
            if (this.isDestroyed) {
                  return;
            }
            clearTimeout(this.requestTimer);
            window.wemb.dcimManager.eventManager.requestSeverityCount(this._pageIds, this.typeIds).then((result) => {
                  if (this.isDestroyed) {
                        return;
                  }
                  /*result = {
                      "4c1ea0db-6688-4b28-af61-801dd0d44ac0": {
                          "FireSensor": {
                              "minor": 3,
                              "major": 5,
                              "normal": 6
                          }
                      },

                      "e3796fc4-1a68-474c-90c8-05466e10f3bc": {
                          "FireSensor": {
                              "minor": 1,
                              "major": 2,
                              "normal": 3
                          },
                          "pdu": {
                              "minor": 3,
                              "major": 2,
                              "critical": 1
                          }
                      }
                  }*/
                  let jsonStr = JSON.stringify(result);
                  if (this.tempEventCountData != jsonStr) {
                        this.updateFloorsEventCount(result);
                  }
                  this.tempEventCountData = jsonStr;
            }).finally(() => {
                  this.pollingRequestEventCount();
            });
      }


      createVueElement() {
            var str = `
            <div class="container">            
                <div class="cont-fir-wrap">
                    <div class="cont-sec-wrap">                    
                        <span class="border-label">{{ initTitle }}</span>                        
                        <div class="asset-info-back">                            
                            <div class="asset-info-wrap">                            
                                <div class="asset-icon-wrap">
                                    <div class="asset-icon-blank"></div>                                              
                                    <el-tooltip effect="light" placement="top" v-for="type in typeIds">
                                          <div slot="content">{{ type }}</div>
                                          <img class="asset-img" :src="getImagePath(type)">
                                    </el-tooltip>
                                </div>
                              <div class="asset-counts-wrap" v-for="(floor, index) in floorList">
                                  <div class="asset-count-location">{{ floor.name }}</div>                                                        
                                  <div class="asset-count-wrap">
                                      <div class="asset-count" v-for="type in typeIds">
                                          <div class="asset-counter">{{ getAssetCount(floor.id, type) }}</div>
                                          <div class="asset-event-counter">{{ getEventCount(floor.id, type) }}</div>
                                      </div>
                                  </div>  
                              </div>  
                            </div>
                        </div>                             
                    </div>
                </div>
                <div class="icon-bg" @click="toggleState($event)">
                    <div class="bullet"></div>
                </div>            
            </div>
            `;


            let $temp = $(str);
            let self = this;
            $(this._element).append($temp);

            var imageRootPath = DCIMManager.PACK_PATH + this.componentName + "/resource/";
            let assetAllTitle = this.getGroupPropertyValue('title', 'floor_asset_list_title'); // 처음 DB의 Setting 값으로 되어있는 title

            this.app = new Vue({
                  el: $temp.get(0),

                  data: function () {
                        return {
                              lang: Vue.$i18n.messages.wv.dcim_pack,
                              assetData: {},
                              eventsData: {},
                              typeIds: [],
                              floorList: null,
                              $bgEl: null,
                              $container: null,
                              imageRootPath: imageRootPath,
                              title: assetAllTitle
                        }
                  },

                  computed: {
                        initTitle: function(){
                              return this.title.length === 0 ? this.lang.common.assetStatusInfo : this.title;
                        }
                  },

                  mounted: function () {
                        this.$bgEl = $(this.$el).find(".icon-bg");
                        this.$container = $(this.$el).find(".cont-fir-wrap");
                  },

                  methods: {
                        getAssetCount(floorId, type) {
                              let count = 0;
                              try {
                                    count = this.assetData[floorId][type] || 0;
                              } catch (error) {
                                    count = "0";
                              }
                              return count;
                        },

                        getEventCount(floorId, type) {
                              //eventsData
                              let count = 0;
                              try {
                                    count = this.eventsData[floorId][type] || 0;
                              } catch (error) {
                                    count = "0";
                              }

                              return count;
                        },

                        updateEventCounts(data) {
                              this.eventsData = data;
                        },


                        getImagePath(asset) {
                              return this.imageRootPath + "/" + asset + ".png";
                        },

                        updateTypeCounts(data) {
                              this.assetData = data;
                        },

                        updateFloorData(data) {
                              this.floorList = data;
                        },

                        updateTypeIds(data) {
                              this.typeIds = data;
                        },

                        toggleState($event) {
                              var target = $(event.currentTarget);

                              this.$container.toggle("slide", {direction: "right"}, 800);
                              // 화살표 위치 변경
                             target.toggleClass("icon-bg-close");

                              // 화살표 모양 변경
                              if (target.hasClass("icon-bg-close")) {
                                    target.animate({left: (self.width - 40) + 'px'}, 800);
                                    target.find(".bullet").attr("class", "bullet-open");
                              } else {
                                    target.find(".bullet-open").attr("class", "bullet");
                                    target.animate({left: '-20px'}, 800);
                              }
                        }

                  }
            })
      }

      bindEvent() {
            window.wemb.dcimManager.pagesetManager.$on(DCIMManager.UPDATE_PAGESET_LIST, () => {
                  this.updatePagesetList();
            })
      }

      updatePagesetList() {
            this._locationList = wemb.dcimManager.pagesetManager.getPagesetBuildings();
            this.updateBuildingFloors();
      }

      _onImmediateUpdateDisplay() {

      }

      _onCommitProperties() {
            if (this._updatePropertiesMap.get("typeInfo") || this._updatePropertiesMap.get("pageInfo")) {
                  this.updateTypeset();
            }

            if(this._updatePropertiesMap.has("title")) {
                  this.app.title = this.getGroupPropertyValue('title', 'floor_asset_list_title');
                  this.app.$forceUpdate();
            }
      }

      getCurrentBuilding() {
            if (!this._locationList) {
                  return '';
            }
            let pageId = wemb.pageManager.currentPageInfo.id;
            let currentLocation = this._locationList.find(x => x.id == pageId); //building/동 리스트 검사
            if (!currentLocation) {
                  let list = [];
                  this._locationList.forEach((location) => {
                        let floors = location.children;
                        if (floors) {
                              let findItem = floors.find(x => x.id == pageId); //층리스트 검사
                              if (findItem) {
                                    currentLocation = location;
                              } else if (!this.currentLocation) {
                                    floors.forEach((floor) => {
                                          let rooms = floor.children; //룸 리스트 검사
                                          if (rooms) {
                                                let findItem = rooms.find(x => x.id == pageId);
                                                if (findItem) {
                                                      currentLocation = location;
                                                }
                                          }
                                    })
                              }
                        }
                  })
            }
            return currentLocation;
      }

      updateBuildingFloors() {
            let building = this.getCurrentBuilding();
            if (building) {
                  var buildingId = building.id;
                  this._pageIds = window.wemb.dcimManager.pagesetManager.getFloorRoomIds(building);
                  // 그룹 이름이 선택되지 않는 경우 첫 번째 그룹으로
                  this._floorList = null;
                  if (buildingId === "") {
                        this._floorList = wemb.dcimManager.pagesetManager.getFirstBuildiingFloorList() || [];
                  } else {
                        this._floorList = wemb.dcimManager.pagesetManager.getFloorList(buildingId) || [];
                  }

                  this.app.updateFloorData(this._floorList);
            }

      }

      updateTypeset() {
            this.updateTypeList();
            this.updateAssetCountList();
      }

      updateTypeList() {
            this._typeIds = [];
            let tmpList = [];
            // 페이지셋에 설정된 배치된 모든 자산 타입리스트 가져오기.
            let pageIds = this._floorList;
            pageIds.map(floor => {
                  if (wemb.pageManager.hasPageInfoBy(floor.id)) {
                        tmpList.push(...wemb.dcimManager.assetManager.getAssetTypesInPage(floor.id))
                  }

            });

            // 중복 타입 제거
            this._typeIds = tmpList.reduce((a, b) => {
                  if (a.indexOf(b) < 0) a.push(b);
                  return a;
            }, []);

            this.app.updateTypeIds(this._typeIds);
      }

      updateAssetCountList() {
            //타입에 있는 자산 목록
            let asseetCountObj = {};

            this._floorList.forEach((floor) => {
                  let floorCountObj = wemb.dcimManager.assetManager.getAssetCountListInPage(this._typeIds, floor.id);
                  asseetCountObj[floor.id] = floorCountObj;

                  /*{sensor: 0, cctv:7}*/
                  let rooms = this._floorList.children;
                  if (rooms) {
                        rooms.forEach((room) => {
                              let roomCountObj = wemb.dcimManager.assetManager.getAssetCountListInPage(this._typeIds, room.id);
                              typeIds.forEach((type) => {
                                    floorCountObj[type] += roomCountObj[type];
                              });
                        });
                  }
            })

            this.app.updateTypeCounts(asseetCountObj);
      }


      updateFloorsEventCount(data) {
            /*{
            Type:{
                    “cricical” :0
                }
            }
            }*/
            let obj = {}

            this._floorList.forEach((floor) => {
                  obj[floor.id] = this.getFloorAssetEventCount(floor, data);
            })

            this.app.updateEventCounts(obj);
      }

      getFloorAssetEventCount(floor, data) {
            let eventCountObj = {};
            let floorEventData = data.data[floor.id];
            if (floorEventData) {
                  this.addAssetEventCount(floorEventData, eventCountObj);
            }

            let rooms = floor.children;
            if (rooms) {
                  rooms.forEach((room) => {
                        let roomEventData = data[room.id];
                        if (roomEventData) {
                              this.addAssetEventCount(roomEventData, eventCountObj);
                        }
                  })
            }

            return eventCountObj;
      }

      addAssetEventCount(data, eventCountObj) {
            this._typeIds.forEach((typeId) => {
                  eventCountObj[typeId] = eventCountObj[typeId] || 0;
                  let typeCountObj = data[typeId];
                  if (typeCountObj) {
                        let count = 0;
                        $.each(typeCountObj, (key, value) => {
                              count += value;
                        });

                        eventCountObj[typeId] += count;
                  }
            })
      }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(DCIMAllAssetCounterComponent, {
      "info": {
            "componentName": "DCIMAllAssetCounterComponent",
            "version": "1.0.0"
      },

      "setter": {
            "width": 340,
            "height": 140
      },

      "label": {
            "label_using": "N",
            "label_text": "All Asset Counter Component"
      },

      "font": {
            "font_type": "inherit"
      },

      "title": {
            "floor_asset_list_title": ''
      }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
DCIMAllAssetCounterComponent.property_panel_info = [{
      template: "primary",
      label: "primary"
},
      {
            template: "pos-size-2d",
            label: "pos-size-2d"
      },
      {

            template: "label",
            label: "label"
      }
];

WVPropertyManager.add_property_panel_group_info(DCIMAllAssetCounterComponent, {
      label : "Floor Asset List Title",
      template : "vertical",
      children : [
            {
                  owner : "title",
                  name : "floor_asset_list_title",
                  type : "string",
                  label : "Title",
                  show : true,
                  writable : true,
                  description : "층별 자산 리스트 타이틀"
            }
      ]
});
