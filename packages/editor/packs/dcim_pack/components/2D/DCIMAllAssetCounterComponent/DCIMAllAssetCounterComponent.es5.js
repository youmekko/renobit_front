"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var DCIMAllAssetCounterComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(DCIMAllAssetCounterComponent, _WVDOMComponent);

            function DCIMAllAssetCounterComponent() {
                  var _this;

                  _classCallCheck(this, DCIMAllAssetCounterComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(DCIMAllAssetCounterComponent).call(this));
                  _this.requestIntervalTime = DCIMManager.EVENT_POLLING_INTERVAL_TIME;
                  _this.requestTimer = null;
                  _this.isDestroyed = false;
                  _this.tempEventCountData = '';
                  _this._pageIds = [];
                  _this._typeIds = [];
                  _this._locationList = null;
                  _this._floorList = [];
                  _this.app;
                  return _this;
            }

            _createClass(DCIMAllAssetCounterComponent, [{
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        clearTimeout(this.requestTimer);
                        this.isDestroyed = true;
                        this.app = null;
                        this._pageIds = null;
                        this._typeIds = null;
                        this._locationList = null;
                        this._floorList = null;

                        _get(_getPrototypeOf(DCIMAllAssetCounterComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this.createVueElement();
                        this.updatePagesetList();
                        this.updateTypeset();
                        this.bindEvent();

                        if (!this.isEditorMode) {
                              this.requestEventCount();
                              this.pollingRequestEventCount();
                        }
                  }
            }, {
                  key: "pollingRequestEventCount",
                  value: function pollingRequestEventCount() {
                        var _this2 = this;

                        if (this.isDestroyed) {
                              return;
                        }

                        this.requestTimer = setTimeout(function () {
                              _this2.requestEventCount();
                        }, this.requestIntervalTime);
                  }
            }, {
                  key: "requestEventCount",
                  value: function requestEventCount() {
                        var _this3 = this;

                        if (this.isDestroyed) {
                              return;
                        }

                        clearTimeout(this.requestTimer);
                        window.wemb.dcimManager.eventManager.requestSeverityCount(this._pageIds, this.typeIds).then(function (result) {
                              if (_this3.isDestroyed) {
                                    return;
                              }
                              /*result = {
                                  "4c1ea0db-6688-4b28-af61-801dd0d44ac0": {
                                      "FireSensor": {
                                          "minor": 3,
                                          "major": 5,
                                          "normal": 6
                                      }
                                  },
                                   "e3796fc4-1a68-474c-90c8-05466e10f3bc": {
                                      "FireSensor": {
                                          "minor": 1,
                                          "major": 2,
                                          "normal": 3
                                      },
                                      "pdu": {
                                          "minor": 3,
                                          "major": 2,
                                          "critical": 1
                                      }
                                  }
                              }*/


                              var jsonStr = JSON.stringify(result);

                              if (_this3.tempEventCountData != jsonStr) {
                                    _this3.updateFloorsEventCount(result);
                              }

                              _this3.tempEventCountData = jsonStr;
                        }).finally(function () {
                              _this3.pollingRequestEventCount();
                        });
                  }
            }, {
                  key: "createVueElement",
                  value: function createVueElement() {
                        var str = "\n            <div class=\"container\">            \n                <div class=\"cont-fir-wrap\">\n                    <div class=\"cont-sec-wrap\">                    \n                        <span class=\"border-label\">{{ initTitle }}</span>                        \n                        <div class=\"asset-info-back\">                            \n                            <div class=\"asset-info-wrap\">                            \n                                <div class=\"asset-icon-wrap\">\n                                    <div class=\"asset-icon-blank\"></div>                                              \n                                    <el-tooltip effect=\"light\" placement=\"top\" v-for=\"type in typeIds\">\n                                          <div slot=\"content\">{{ type }}</div>\n                                          <img class=\"asset-img\" :src=\"getImagePath(type)\">\n                                    </el-tooltip>\n                                </div>\n                              <div class=\"asset-counts-wrap\" v-for=\"(floor, index) in floorList\">\n                                  <div class=\"asset-count-location\">{{ floor.name }}</div>                                                        \n                                  <div class=\"asset-count-wrap\">\n                                      <div class=\"asset-count\" v-for=\"type in typeIds\">\n                                          <div class=\"asset-counter\">{{ getAssetCount(floor.id, type) }}</div>\n                                          <div class=\"asset-event-counter\">{{ getEventCount(floor.id, type) }}</div>\n                                      </div>\n                                  </div>  \n                              </div>  \n                            </div>\n                        </div>                             \n                    </div>\n                </div>\n                <div class=\"icon-bg\" @click=\"toggleState($event)\">\n                    <div class=\"bullet\"></div>\n                </div>            \n            </div>\n            ";
                        var $temp = $(str);
                        var self = this;
                        $(this._element).append($temp);
                        var imageRootPath = DCIMManager.PACK_PATH + this.componentName + "/resource/";
                        var assetAllTitle = this.getGroupPropertyValue('title', 'floor_asset_list_title'); // 처음 DB의 Setting 값으로 되어있는 title
                        this.app = new Vue({
                              el: $temp.get(0),
                              data: function data() {
                                    return {
                                          lang: Vue.$i18n.messages.wv.dcim_pack,
                                          assetData: {},
                                          eventsData: {},
                                          typeIds: [],
                                          floorList: null,
                                          $bgEl: null,
                                          $container: null,
                                          imageRootPath: imageRootPath,
                                          title: assetAllTitle
                                    };
                              },
                              mounted: function mounted() {
                                    this.$bgEl = $(this.$el).find(".icon-bg");
                                    this.$container = $(this.$el).find(".cont-fir-wrap");
                              },
                              computed: {
                                    initTitle: function initTitle() {
                                      return this.title.length === 0 ? this.lang.common.assetStatusInfo : this.title;
                                    }
                              },
                              methods: {
                                    getAssetCount: function getAssetCount(floorId, type) {
                                          var count = 0;

                                          try {
                                                count = this.assetData[floorId][type] || 0;
                                          } catch (error) {
                                                count = "0";
                                          }

                                          return count;
                                    },
                                    getEventCount: function getEventCount(floorId, type) {
                                          //eventsData
                                          var count = 0;

                                          try {
                                                count = this.eventsData[floorId][type] || 0;
                                          } catch (error) {
                                                count = "0";
                                          }

                                          return count;
                                    },
                                    updateEventCounts: function updateEventCounts(data) {
                                          this.eventsData = data;
                                    },
                                    getImagePath: function getImagePath(asset) {
                                          return this.imageRootPath + "/" + asset + ".png";
                                    },
                                    updateTypeCounts: function updateTypeCounts(data) {
                                          this.assetData = data;
                                    },
                                    updateFloorData: function updateFloorData(data) {
                                          this.floorList = data;
                                    },
                                    updateTypeIds: function updateTypeIds(data) {
                                          this.typeIds = data;
                                    },
                                    toggleState: function toggleState($event) {
                                          var target = $(event.currentTarget);
                                          this.$container.toggle("slide", {
                                                direction: "right"
                                          }, 800); // 화살표 위치 변경

                                          target.toggleClass("icon-bg-close"); // 화살표 모양 변경

                                          if (target.hasClass("icon-bg-close")) {
                                                target.animate({
                                                      left: self.width - 40 + 'px'
                                                }, 800);
                                                target.find(".bullet").attr("class", "bullet-open");
                                          } else {
                                                target.find(".bullet-open").attr("class", "bullet");
                                                target.animate({
                                                      left: '-20px'
                                                }, 800);
                                          }
                                    }
                              }
                        });
                  }
            }, {
                  key: "bindEvent",
                  value: function bindEvent() {
                        var _this4 = this;

                        window.wemb.dcimManager.pagesetManager.$on(DCIMManager.UPDATE_PAGESET_LIST, function () {
                              _this4.updatePagesetList();
                        });
                  }
            }, {
                  key: "updatePagesetList",
                  value: function updatePagesetList() {
                        this._locationList = wemb.dcimManager.pagesetManager.getPagesetBuildings();
                        this.updateBuildingFloors();
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {}
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._updatePropertiesMap.get("typeInfo") || this._updatePropertiesMap.get("pageInfo")) {
                              this.updateTypeset();
                        }
                        if (this._updatePropertiesMap.has("title")) {
                              this.app.title = this.getGroupPropertyValue('title', 'floor_asset_list_title');
                              this.app.$forceUpdate();
                        }
                  }
            }, {
                  key: "getCurrentBuilding",
                  value: function getCurrentBuilding() {
                        var _this5 = this;

                        if (!this._locationList) {
                              return '';
                        }

                        var pageId = wemb.pageManager.currentPageInfo.id;

                        var currentLocation = this._locationList.find(function (x) {
                              return x.id == pageId;
                        }); //building/동 리스트 검사


                        if (!currentLocation) {
                              var list = [];

                              this._locationList.forEach(function (location) {
                                    var floors = location.children;

                                    if (floors) {
                                          var findItem = floors.find(function (x) {
                                                return x.id == pageId;
                                          }); //층리스트 검사

                                          if (findItem) {
                                                currentLocation = location;
                                          } else if (!_this5.currentLocation) {
                                                floors.forEach(function (floor) {
                                                      var rooms = floor.children; //룸 리스트 검사

                                                      if (rooms) {
                                                            var _findItem = rooms.find(function (x) {
                                                                  return x.id == pageId;
                                                            });

                                                            if (_findItem) {
                                                                  currentLocation = location;
                                                            }
                                                      }
                                                });
                                          }
                                    }
                              });
                        }

                        return currentLocation;
                  }
            }, {
                  key: "updateBuildingFloors",
                  value: function updateBuildingFloors() {
                        var building = this.getCurrentBuilding();

                        if (building) {
                              var buildingId = building.id;
                              this._pageIds = window.wemb.dcimManager.pagesetManager.getFloorRoomIds(building); // 그룹 이름이 선택되지 않는 경우 첫 번째 그룹으로

                              this._floorList = null;

                              if (buildingId === "") {
                                    this._floorList = wemb.dcimManager.pagesetManager.getFirstBuildiingFloorList() || [];
                              } else {
                                    this._floorList = wemb.dcimManager.pagesetManager.getFloorList(buildingId) || [];
                              }

                              this.app.updateFloorData(this._floorList);
                        }
                  }
            }, {
                  key: "updateTypeset",
                  value: function updateTypeset() {
                        this.updateTypeList();
                        this.updateAssetCountList();
                  }
            }, {
                  key: "updateTypeList",
                  value: function updateTypeList() {
                        this._typeIds = [];
                        var tmpList = []; // 페이지셋에 설정된 배치된 모든 자산 타입리스트 가져오기.

                        var pageIds = this._floorList;
                        pageIds.map(function (floor) {
                              if (wemb.pageManager.hasPageInfoBy(floor.id)) {
                                    tmpList.push.apply(tmpList, _toConsumableArray(wemb.dcimManager.assetManager.getAssetTypesInPage(floor.id)));
                              }
                        }); // 중복 타입 제거

                        this._typeIds = tmpList.reduce(function (a, b) {
                              if (a.indexOf(b) < 0) a.push(b);
                              return a;
                        }, []);
                        this.app.updateTypeIds(this._typeIds);
                  }
            }, {
                  key: "updateAssetCountList",
                  value: function updateAssetCountList() {
                        var _this6 = this;

                        //타입에 있는 자산 목록
                        var asseetCountObj = {};

                        this._floorList.forEach(function (floor) {
                              var floorCountObj = wemb.dcimManager.assetManager.getAssetCountListInPage(_this6._typeIds, floor.id);
                              asseetCountObj[floor.id] = floorCountObj;
                              /*{sensor: 0, cctv:7}*/

                              var rooms = _this6._floorList.children;

                              if (rooms) {
                                    rooms.forEach(function (room) {
                                          var roomCountObj = wemb.dcimManager.assetManager.getAssetCountListInPage(_this6._typeIds, room.id);
                                          typeIds.forEach(function (type) {
                                                floorCountObj[type] += roomCountObj[type];
                                          });
                                    });
                              }
                        });

                        this.app.updateTypeCounts(asseetCountObj);
                  }
            }, {
                  key: "updateFloorsEventCount",
                  value: function updateFloorsEventCount(data) {
                        var _this7 = this;

                        /*{
                        Type:{
                                “cricical” :0
                            }
                        }
                        }*/
                        var obj = {};

                        this._floorList.forEach(function (floor) {
                              obj[floor.id] = _this7.getFloorAssetEventCount(floor, data);
                        });

                        this.app.updateEventCounts(obj);
                  }
            }, {
                  key: "getFloorAssetEventCount",
                  value: function getFloorAssetEventCount(floor, data) {
                        var _this8 = this;

                        var eventCountObj = {};
                        var floorEventData = data.data[floor.id];

                        if (floorEventData) {
                              this.addAssetEventCount(floorEventData, eventCountObj);
                        }

                        var rooms = floor.children;

                        if (rooms) {
                              rooms.forEach(function (room) {
                                    var roomEventData = data[room.id];

                                    if (roomEventData) {
                                          _this8.addAssetEventCount(roomEventData, eventCountObj);
                                    }
                              });
                        }

                        return eventCountObj;
                  }
            }, {
                  key: "addAssetEventCount",
                  value: function addAssetEventCount(data, eventCountObj) {
                        this._typeIds.forEach(function (typeId) {
                              eventCountObj[typeId] = eventCountObj[typeId] || 0;
                              var typeCountObj = data[typeId];

                              if (typeCountObj) {
                                    var count = 0;
                                    $.each(typeCountObj, function (key, value) {
                                          count += value;
                                    });
                                    eventCountObj[typeId] += count;
                              }
                        });
                  }
            }]);

            return DCIMAllAssetCounterComponent;
      }(WVDOMComponent); // 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(DCIMAllAssetCounterComponent, {
      "info": {
            "componentName": "DCIMAllAssetCounterComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 340,
            "height": 140
      },
      "label": {
            "label_using": "N",
            "label_text": "All Asset Counter Component"
      },
      "font": {
            "font_type": "inherit"
      },
      "title": {
        "floor_asset_list_title": ''
      }
}); // 프로퍼티 패널에서 사용할 정보 입니다.

DCIMAllAssetCounterComponent.property_panel_info = [{
      template: "primary",
      label: "primary"
}, {
      template: "pos-size-2d",
      label: "pos-size-2d"
}, {
      template: "label",
      label: "label"
}];

WVPropertyManager.add_property_panel_group_info(DCIMAllAssetCounterComponent, {
      label: "Floor Asset List Title",
      template: "vertical",
      children: [{
        owner: "title",
        name: "floor_asset_list_title",
        type: "string",
        label: "Title",
        show: true,
        writable: true,
        description: "층별 자산 리스트 타이틀"
      }]
    });