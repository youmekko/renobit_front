var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }

    return function(Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor;
    };
}();

function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
        for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
            arr2[i] = arr[i];
        }
        return arr2;
    } else {
        return Array.from(arr);
    }
}

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function _possibleConstructorReturn(self, call) {
    if (!self) {
        throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
        throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
        constructor: {
            value: subClass,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DCIMDashboardComponent = function(_WVDOMComponent) {
    _inherits(DCIMDashboardComponent, _WVDOMComponent);

    function DCIMDashboardComponent() {
        _classCallCheck(this, DCIMDashboardComponent);

        var _this = _possibleConstructorReturn(this, (DCIMDashboardComponent.__proto__ || Object.getPrototypeOf(DCIMDashboardComponent)).call(this));

        _this.app;
        return _this;
    }

    _createClass(DCIMDashboardComponent, [{
        key: "onLoadPage",
        value: function onLoadPage() {}
    }, {
        key: "_onImmediateUpdateDisplay",
        value: function _onImmediateUpdateDisplay() {
            this.updateTypeList();
        }
    }, {
        key: "_onCreateProperties",
        value: function _onCreateProperties() {}
    }, {
        key: "_onDestroy",
        value: function _onDestroy() {
            if (this.app) {
                this.app.clear();
                this.app = null;
            }
        }
    }, {
        key: "_onCreateElement",
        value: function _onCreateElement() {
            var isEditorMode = this.isEditorMode;
            var str = "       \n            <div data-name=\"dcim-dashboard-comp\">\n                <div class=\"header\">\n                    <div class=\"select-location\">\n                        <div class=\"select_box\">\n                            <label for=\"dcim-location\">\n                                <template v-if=\"currentLocation\">\n                                    {{currentLocation.name}}\n                                </template>\n                            </label>\n                            <select v-if=\"currentLocation\" v-model=\"currentLocation\" id=\"dcim-location\" title=\"select color\" @change=\"onChangeCurrentLocation\">\n                                <option v-for=\"item in locationList\" :key=\"item.page_name\" :value=\"item\">{{item.name}}</option>\n                            </select>\n                        </div>\n                    </div>\n                    <template v-if=\"assetTypes\">\n                        <div  v-for=\"item in assetTypes\" :key=\"item.id\" :value=\"item.id\" class=\"label\">{{item.name}}</div>\n                    </template>\n                </div>\n                <div class=\"contents\" v-if=\"!isEditorMode\">\n                    <template  v-if=\"currentLocation\">\n                         <div class=\"floor-row\" v-for=\"(floor, index) in currentLocation.children\" :key=\"index\">\n                            <div class=\"label\"><span>{{floor.name}}</span></div>\n                            <div class=\"column\" v-for=\"item in assetTypes\" :key=\"item.id\">                           \n                                <div class=\"symbol\" :class=\"getSeverity(floor.id, item.id)\" @dblclick=\"onClickSymbol($event, getSeverity(floor.id, item.id), floor.id, item.id)\">\n                                    <img class=\"bg\" :src=\"svgPath+'bg/bg-'+getSeverity(floor.id, item.id)+'.svg'\">\n                                    <img class=\"icon\" :src=\"svgPath+'icon/'+item.id+'.svg'\"  @load=\"loadedIconImage($event)\">\n                                </div>\n                            </div>\n                        </div>                        \n                    </template>\n\n                    <!--event list popup-->\n                    <div class=\"event-list-popup\" v-show=\"openPopupData\">\n                        <div class=\"header\">\n                            <h5>Event List</h5>\n                            <button @click=\"openPopupData = null\"><i class=\"el-icon-circle-close-outline\"></i></button>\n                        </div>\n                        <div class=\"contents\">\n                            <ul v-if=\"selectedEventList && selectedEventList.length\">\n                                <li v-for=\"event in selectedEventList\">\n                                    <i class=\"severity-icon\" :class=\"event.severity\"></i><span class=\"msg-txt\" @dblclick=\"moveToPage(event)\"><em>[ {{event.page_name}} ]</em>{{event.msg}}</span></li>\n                                </li>\n                            </ul>\n                            <p v-else class=\"no-data\" style=\"color:#fff;\">NO DATA</p>\n                        </div>\n                    </div>\n                    <!--//event list popup-->                    \n                </div>\n                <div class=\"contents\" v-else>\n                    <template  v-if=\"currentLocation\">\n                         <div class=\"floor-row\" v-for=\"(floor, index) in currentLocation.children\" :key=\"index\">\n                            <div class=\"label\"><span>{{floor.name}}</span></div>\n                            <div class=\"column\" v-for=\"item in assetTypes\" :key=\"item.id\">\n                                <div class=\"symbol\">\n                                    <img class=\"bg\" :src=\"svgPath+'bg/bg-none.svg'\">\n                                    <img class=\"icon\" :src=\"svgPath+'icon/'+item.id+'.svg'\"  @load=\"loadedIconImage($event)\">\n                                </div>\n                            </div>\n                        </div>                        \n                    </template>      \n                </div>\n            </div>\n        ";

            var self = this;

            var temp = $(str);
            $(this._element).append(temp);
              var svgPath = DCIMManager.PACK_PATH + this.componentName + "/resource/";

            var self = this;
            this.app = new Vue({
                el: temp.get(0),
                data: function data() {
                    return {
                        lang: Vue.$i18n.messages.wv.dcim_pack,
                        requestTimer: null,
                        isEditorMode: isEditorMode,
                        tempSeverityWithCountData: null,
                        responseEventSeverity: null,
                        openPopupData: null,
                        eventDataObj: {},
                        svgPath: svgPath,
                        value: '',
                        currentLocation: null,
                        locationList: null,
                        selectedEventList: [],
                        typeIds: [],
                        assetTypes: [
                            /*{ id: "TempHumiSensor", name: "온습도 센서" },
                            { id: "LeakDetectorSensor", name: "누설 감지기 센서" },
                            { id: "AccessSensor", name: "출입문 센서" },
                            { id: "cctv", name: "카메라" },
                            { id: "FireSensor", name: "화재 센서" },
                            { id: "pdu", name: "PDU" }*/
                        ],

                        eventState: {},
                        isDestroyed: false
                    };
                },

                created: function created() {
                    this.bindEvent();
                    this.updatePagesetList();
                    //this.requestEventSeverity();
                },
                mounted: function mounted() {},
                destroyed: function destroyed() {
                    this.clear();
                },


                methods: {
                    clear: function clear() {
                        this.isDestroyed = true;
                        clearTimeout(this.requestTimer);
                    },
                    setPopupPosition: function setPopupPosition($target) {
                        var top = $target.position().top + $target.parent().position().top + Math.floor($target.height() - 26);
                        var left = $target.position().left + $target.parent().position().left + Math.floor($target.width() * 0.5) + parseInt($target.css("padding-left"));

                        var $el = $(this.$el);
                        top = Math.min(top, ($el.height() - 5) - $el.find(".event-list-popup").height());
                        left = Math.min(left, ($el.width() - 5) - $el.find(".event-list-popup").width());

                        $el.find(".event-list-popup").css({
                            top: top,
                            left: left
                        });
                    },
                    pollingRequestSeverity: function pollingRequestSeverity() {
                        var _this2 = this;

                        if (this.isDestroyed) {
                            return;
                        }
                        this.requestTimer = setTimeout(function() {
                            _this2.requestEventSeverity();
                        }, DCIMManager.EVENT_POLLING_INTERVAL_TIME);
                    },
                    requestEventSeverity: function requestEventSeverity() {
                        var _this3 = this;

                        clearTimeout(this.requestTimer);
                        var pageIds = this.getCurrentLocationPageIds();
                        window.wemb.dcimManager.eventManager.requestSeverityCount(pageIds, this.typeIds).then(function(result) {
                            if (_this3.isDestroyed) {
                                return;
                            }
                            var jsonStr = JSON.stringify(result.data);
                            if (_this3.tempSeverityWithCountData != jsonStr) {
                                _this3.responseEventSeverity = result.data;
                                _this3.updateEventSeverity();
                            }
                            _this3.tempSeverityWithCountData = jsonStr;
                        }).finally(function() {
                            _this3.pollingRequestSeverity();
                        });
                    },
                    openEventListPopup: function openEventListPopup(floorId, typeId) {
                        this.openPopupData = { "floorId": floorId, "typeId": typeId };
                        this.selectedEventList = [];
                        this.requestEventList(floorId, typeId);
                    },
                    requestEventList: function requestEventList(floorId, typeId) {
                        var _this4 = this;

                        if (!this.eventDataObj[floorId]) {
                            return;
                        }
                        var pages = this.eventDataObj[floorId]["pages"];
                        var pageIds = [];
                        pageIds = pages.map(function(x) {
                            return x.id;
                        });
                        window.wemb.dcimManager.eventManager.requestEventList(pageIds, [typeId]).then(function(result) {
                            if (_this4.isDestroyed) {
                                return;
                            }
                            _this4.updateEventList(pages, result);
                        });
                    },
                    updateEventList: function updateEventList(pages, eventList) {
                        var list = [];
                        eventList.data.list.forEach(function(event) {
                            var findItem = pages.find(function(x) {
                                return x.id == event.page_id;
                            });
                            if (findItem) {
                                event.page_name = findItem.label;
                                list.push(event);
                            }
                        });

                        this.selectedEventList = list;
                    },
                    moveToPage: function moveToPage(event) {
                        var pageId = event.page_id || '';

                          /*
                            2019.02.14(ckkim)
                            자신이 존재하는 경우 자산으로 직접이동할 수 있게 파라메터 추가
                             */
                          let assetId = event.asset_id ||'';
                          if(assetId)
                                window.wemb.pageManager.openPageById(pageId,{assetId:assetId});
                          else {
                                window.wemb.pageManager.openPageById(pageId);
                          }
                    },
                    bindEvent: function bindEvent() {
                        var _this5 = this;

                        window.wemb.dcimManager.pagesetManager.$on(DCIMManager.UPDATE_PAGESET_LIST, function() {
                            _this5.updatePagesetList();
                        });
                    },
                    updatePagesetList: function updatePagesetList() {
                        this.locationList = wemb.dcimManager.pagesetManager.getPagesetBuildings();
                        this.currentLocation = this.locationList ? this.locationList[0] : null;
                    },
                    onChangeCurrentLocation: function onChangeCurrentLocation() {
                        this.openPopupData = null;
                        this.requestEventSeverity();
                    },
                    onClickSymbol: function onClickSymbol(e, severity, floorId, typeId) {
                        if (severity == "none") {
                            this.$message(this.lang.dashboard.noEvent);
                            return;
                        }
                        this.setPopupPosition($(e.currentTarget).parent());
                        this.openEventListPopup(floorId, typeId);
                    },
                    getSeverity: function getSeverity(floorId, typeId) {
                        var severity = "none";
                        if (this.isEditorMode) {
                            return severity;
                        }

                        try {
                            if (this.eventDataObj) {
                                severity = this.eventDataObj[floorId]["types"][typeId] || "none";
                            }
                        } catch (error) {}

                        return severity;
                    },
                    updateAssetTypes: function updateAssetTypes(types) {
                        this.openPopupData = null;
                        this.assetTypes = types;
                        this.typeIds = this.assetTypes.map(function(x) {
                            return x.id;
                        });
                        this.requestEventSeverity();
                    },
                    getCurrentLocationPageIds: function getCurrentLocationPageIds() {
                        if (this.isEditorMode || !this.locationList || !this.currentLocation) {
                            return [];
                        }
                        return window.wemb.dcimManager.pagesetManager.getFloorRoomIds(this.currentLocation);
                    },
                    setHighLevelSeverity: function setHighLevelSeverity(dataObj, severity) {
                        var eventLevel = {
                            "none": 0,
                            "normal": 1,
                            "minor": 2,
                            "warning": 3,
                            "major": 4,
                            "critical": 5
                        };

                        $.each(dataObj, function(status, count) {
                            if (eventLevel[status] > severity.level) {
                                severity.level = eventLevel[status];
                                severity.name = status;
                            }
                        });
                    },
                    updateEventSeverity: function updateEventSeverity() {
                        var _this6 = this;

                        if (this.isEditorMode || !this.locationList || !this.currentLocation) {
                            return;
                        }
                        var dataObj = this.responseEventSeverity || {};
                        var eventData = {};
                        var floorList = this.currentLocation.children;

                        floorList.forEach(function(floor) {
                            var floorObj = {};
                            var floorPageId = floor.id;
                            var pagesData = [{ "id": floorPageId, "label": floor.name }];

                            if (floor.children && floor.children.length) {
                                var childrenPageData = floor.children.map(function(x) {
                                    return { "id": x.id, "label": x.name };
                                });
                                pagesData.push.apply(pagesData, _toConsumableArray(childrenPageData));
                            }

                            _this6.typeIds.forEach(function(typeId) {
                                var severity = { level: 0, name: "none" };
                                if (dataObj[floorPageId] && dataObj[floorPageId][typeId]) {
                                    _this6.setHighLevelSeverity(dataObj[floorPageId][typeId], severity);
                                }

                                //층이 가지고 있는 룸에 대한 정보 합치기
                                if (floor.children && floor.children.length) {
                                    floor.children.forEach(function(child) {
                                        if (dataObj[child.id] && dataObj[child.id][typeId]) {
                                            _this6.setHighLevelSeverity(dataObj[child.id][typeId], severity);
                                        }
                                    });
                                }

                                floorObj[typeId] = severity.name;
                            });

                            eventData[floorPageId] = { "types": floorObj, "pages": pagesData };
                        });

                        this.eventDataObj = eventData;
                        if (this.openPopupData) {
                            this.openEventListPopup(this.openPopupData.floorId, this.openPopupData.typeId);
                        }
                    },
                    loadedIconImage: function loadedIconImage(event) {
                        var $img = jQuery(event.target);
                        var imgClass = $img.attr('class');
                        var imgURL = $img.attr('src');

                        jQuery.get(imgURL, function(data) {
                            var $svg = jQuery(data).find('svg');
                            $svg = $svg.attr('id', Math.random());
                            $svg = $svg.attr('class', imgClass + ' replaced-svg');
                            $svg = $svg.removeAttr('xmlns:a');
                            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
                            }

                            $img.replaceWith($svg);
                        }, 'xml');
                    }
                }
            });
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            if (this._updatePropertiesMap.has("typeInfo")) {
                this.validateCallLater(this.updateTypeList);
            }
        }
    }, {
        key: "updateTypeList",
        value: function updateTypeList() {
            var groupId = this.getGroupPropertyValue("typeInfo", "typeGroupName");

            // 그룹 이름이 선택되지 않는 경우 첫 번째 그룹으로
            var typeList = null;
            if (!groupId) {
                typeList = window.wemb.dcimManager.typesetManager.getFirstAssetTypeGroupData();
            } else {
                typeList = window.wemb.dcimManager.typesetManager.getAssetTypesByGroupName(groupId);
            }

            this.app.updateAssetTypes(typeList);
        }
    }]);

    return DCIMDashboardComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DCIMDashboardComponent, {
    "info": {
        "componentName": "DCIMDashboardComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 1140,
        "height": 640
    },

    "style": {},

    "typeInfo": {
        "typeGroupName": ""
    }
});

DCIMDashboardComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "dcim-asset-typeset"
}];
