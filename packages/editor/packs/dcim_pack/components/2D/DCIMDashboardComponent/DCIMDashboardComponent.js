/**
 * 종합현황, 심볼을 SVG를 사용하고, 상태별 컬러는 css에서 정의하여 표기한다.
 * img에서 svg로드시 style적용이 어렵기 때문에 로드 완료 후 svg 태그를 꺼내와 img태그를 삭제하고 해당 위치에 다시 넣는다.
 * 심볼은 해당 층의 가장 높은 등급의 이벤트 상태를 표현하고 더블클릭시 이벤트 리스트를 팝업으로 보여준다. 
 * 이벤트 polling은 eventManager를 통해 한다.
 */
class DCIMDashboardComponent extends WVDOMComponent {
    constructor() {
        super();
        this.app;
    }

    onLoadPage() {}

    _onImmediateUpdateDisplay() {
        this.updateTypeList();
    }

    _onCreateProperties() {}

    _onDestroy() {
        if (this.app) {
            this.app.clear();
            this.app = null;
        }
    }

    _onCreateElement() {
        let isEditorMode = this.isEditorMode;
        var str = `       
            <div data-name="dcim-dashboard-comp">
                <div class="header">
                    <div class="select-location">
                        <div class="select_box">
                            <label for="dcim-location">
                                <template v-if="currentLocation">
                                    {{currentLocation.name}}
                                </template>
                            </label>
                            <select v-if="currentLocation" v-model="currentLocation" id="dcim-location" title="select color" @change="onChangeCurrentLocation">
                                <option v-for="item in locationList" :key="item.page_name" :value="item">{{item.name}}</option>
                            </select>
                        </div>
                    </div>
                    <template v-if="assetTypes">
                        <div  v-for="item in assetTypes" :key="item.id" :value="item.id" class="label">{{item.name}}</div>
                    </template>
                </div>
                <div class="contents" v-if="!isEditorMode">
                    <template  v-if="currentLocation">
                         <div class="floor-row" v-for="(floor, index) in currentLocation.children" :key="index">
                            <div class="label"><span>{{floor.name}}</span></div>
                            <div class="column" v-for="item in assetTypes" :key="item.id">                           
                                <div class="symbol" :class="getSeverity(floor.id, item.id)" @dblclick="onClickSymbol($event, getSeverity(floor.id, item.id), floor.id, item.id)">
                                    <img class="bg" :src="svgPath+'bg/bg-'+getSeverity(floor.id, item.id)+'.svg'">
                                    <img class="icon" :src="svgPath+'icon/'+item.id+'.svg'"  @load="loadedIconImage($event)">
                                </div>
                            </div>
                        </div>                        
                    </template>

                    <!--event list popup-->
                    <div class="event-list-popup" v-show="openPopupData">
                        <div class="header">
                            <h5>Event List</h5>
                            <button @click="openPopupData = null"><i class="el-icon-circle-close-outline"></i></button>
                        </div>
                        <div class="contents">
                            <ul v-if="selectedEventList && selectedEventList.length">
                                <li v-for="event in selectedEventList">
                                    <i class="severity-icon" :class="event.severity"></i><span class="msg-txt" @dblclick="moveToPage(event)"><em>[ {{event.page_name}} ]</em>{{event.msg}}</span></li>
                                </li>
                            </ul>
                            <p v-else class="no-data" style="color:#fff;">NO DATA</p>
                        </div>
                    </div>
                    <!--//event list popup-->                    
                </div>
                <div class="contents" v-else>
                    <template  v-if="currentLocation">
                         <div class="floor-row" v-for="(floor, index) in currentLocation.children" :key="index">
                            <div class="label"><span>{{floor.name}}</span></div>
                            <div class="column" v-for="item in assetTypes" :key="item.id">
                                <div class="symbol">
                                    <img class="bg" :src="svgPath+'bg/bg-none.svg'">
                                    <img class="icon" :src="svgPath+'icon/'+item.id+'.svg'"  @load="loadedIconImage($event)">
                                </div>
                            </div>
                        </div>                        
                    </template>      
                </div>
            </div>
        `;

        var self = this;


        let temp = $(str);
        $(this._element).append(temp);
        var svgPath = DCIMManager.PACK_PATH + this.componentName + "/resource/";
        var self = this;
        this.app = new Vue({
            el: temp.get(0),
            data: function() {
                return {
                    lang: Vue.$i18n.messages.wv.dcim_pack,
                    requestTimer: null,
                    isEditorMode: isEditorMode,
                    tempSeverityWithCountData: null,
                    responseEventSeverity: null,
                    openPopupData: null,
                    eventDataObj: {},
                    svgPath: svgPath,
                    value: '',
                    currentLocation: null,
                    locationList: null,
                    selectedEventList: [],
                    typeIds: [],
                    assetTypes: [
                        /*{ id: "TempHumiSensor", name: "온습도 센서" },
                        { id: "LeakDetectorSensor", name: "누설 감지기 센서" },
                        { id: "AccessSensor", name: "출입문 센서" },
                        { id: "cctv", name: "카메라" },
                        { id: "FireSensor", name: "화재 센서" },
                        { id: "pdu", name: "PDU" }*/
                    ],

                    eventState: {},
                    isDestroyed: false
                }
            },

            created() {
                this.bindEvent();
                this.updatePagesetList();
                //this.requestEventSeverity();
            },

            mounted() {

            },

            destroyed() {
                this.clear();
            },

            methods: {
                clear() {
                    this.isDestroyed = true;
                    clearTimeout(this.requestTimer);
                },

                setPopupPosition($target) {
                    let top = $target.position().top + $target.parent().position().top + Math.floor($target.height() - 26);
                    let left = $target.position().left + $target.parent().position().left + Math.floor($target.width() * 0.5) + parseInt($target.css("padding-left"));

                    let $el = $(this.$el);
                    top = Math.min(top, ($el.height() - 5) - $el.find(".event-list-popup").height());
                    left = Math.min(left, ($el.width() - 5) - $el.find(".event-list-popup").width());

                    $el.find(".event-list-popup").css({
                        top: top,
                        left: left
                    })
                },

                pollingRequestSeverity() {
                    if (this.isDestroyed) {
                        return;
                    }
                    this.requestTimer = setTimeout(() => {
                        this.requestEventSeverity();
                    }, DCIMManager.EVENT_POLLING_INTERVAL_TIME);
                },

                requestEventSeverity() {
                    clearTimeout(this.requestTimer);
                    let pageIds = this.getCurrentLocationPageIds();
                    window.wemb.dcimManager.eventManager.requestSeverityCount(pageIds, this.typeIds).then((result) => {
                        if (this.isDestroyed) {
                            return;
                        }
                        let jsonStr = JSON.stringify(result.data);
                        if (this.tempSeverityWithCountData != jsonStr) {
                            this.responseEventSeverity = result.data;
                            this.updateEventSeverity();
                        }
                        this.tempSeverityWithCountData = jsonStr;
                    }).finally(() => {
                        this.pollingRequestSeverity();
                    });
                },

                openEventListPopup(floorId, typeId) {
                    this.openPopupData = { "floorId": floorId, "typeId": typeId };
                    this.selectedEventList = [];
                    this.requestEventList(floorId, typeId);
                },

                //event polling
                requestEventList(floorId, typeId) {
                    if (!this.eventDataObj[floorId]) {
                        return;
                    }
                    let pages = this.eventDataObj[floorId]["pages"];
                    let pageIds = [];
                    pageIds = pages.map(x => x.id);
                    window.wemb.dcimManager.eventManager.requestEventList(pageIds, [typeId]).then((result) => {
                        if (this.isDestroyed) {
                            return;
                        }
                        this.updateEventList(pages, result);
                    });
                },


                updateEventList(pages, eventList) {
                    let list = [];
                    eventList.data.list.forEach((event) => {
                        let findItem = pages.find(x => x.id == event.page_id);
                        if (findItem) {
                            event.page_name = findItem.label;
                            list.push(event);
                        }
                    })

                    this.selectedEventList = list;
                },

                moveToPage(event) {
                    let pageId = event.page_id || '';

                    /*
                    2019.02.14(ckkim)
                    자신이 존재하는 경우 자산으로 직접이동할 수 있게 파라메터 추가
                     */
                    let assetId = event.asset_id || '';
                    if (assetId)
                        window.wemb.pageManager.openPageById(pageId, { assetId: assetId });
                    else {
                        window.wemb.pageManager.openPageById(pageId);
                    }
                },

                bindEvent() {
                    window.wemb.dcimManager.pagesetManager.$on(DCIMManager.UPDATE_PAGESET_LIST, () => {
                        this.updatePagesetList();
                    })
                },

                updatePagesetList() {
                    this.locationList = wemb.dcimManager.pagesetManager.getPagesetBuildings();
                    this.currentLocation = this.locationList ? this.locationList[0] : null;
                },

                onChangeCurrentLocation() {
                    this.openPopupData = null;
                    this.requestEventSeverity();
                },

                onClickSymbol(e, severity, floorId, typeId) {
                    if (severity == "none") {
                        this.$message(this.lang.dashboard.noEvent);
                        return;
                    }
                    this.setPopupPosition($(e.currentTarget).parent());
                    this.openEventListPopup(floorId, typeId);
                },

                getSeverity(floorId, typeId) {
                    let severity = "none";
                    if (this.isEditorMode) {
                        return severity;
                    }

                    try {
                        if (this.eventDataObj) {
                            severity = this.eventDataObj[floorId]["types"][typeId] || "none";
                        }
                    } catch (error) {}

                    return severity;
                },

                updateAssetTypes(types) {
                    this.openPopupData = null;
                    this.assetTypes = types;
                    this.typeIds = this.assetTypes.map(x => x.id);
                    this.requestEventSeverity();
                },


                getCurrentLocationPageIds() {
                    if (this.isEditorMode || !this.locationList || !this.currentLocation) {
                        return [];
                    }
                    return window.wemb.dcimManager.pagesetManager.getFloorRoomIds(this.currentLocation);
                },

                setHighLevelSeverity(dataObj, severity) {
                    let eventLevel = {
                        "none": 0,
                        "normal": 1,
                        "minor": 2,
                        "warning": 3,
                        "major": 4,
                        "critical": 5
                    }


                    $.each(dataObj, (status, count) => {
                        if (eventLevel[status] > severity.level) {
                            severity.level = eventLevel[status];
                            severity.name = status;
                        }
                    })
                },

                updateEventSeverity() {
                    if (this.isEditorMode || !this.locationList || !this.currentLocation) {
                        return;
                    }
                    let dataObj = this.responseEventSeverity || {};
                    let eventData = {};
                    let floorList = this.currentLocation.children;

                    floorList.forEach((floor) => {
                        let floorObj = {};
                        let floorPageId = floor.id;
                        let pagesData = [{ "id": floorPageId, "label": floor.name }];

                        if (floor.children && floor.children.length) {
                            let childrenPageData = floor.children.map((x) => {
                                return { "id": x.id, "label": x.name }
                            });
                            pagesData.push(...childrenPageData);
                        }

                        this.typeIds.forEach((typeId) => {
                            let severity = { level: 0, name: "none" };
                            if (dataObj[floorPageId] && dataObj[floorPageId][typeId]) {
                                this.setHighLevelSeverity(dataObj[floorPageId][typeId], severity);
                            }

                            //층이 가지고 있는 룸에 대한 정보 합치기
                            if (floor.children && floor.children.length) {
                                floor.children.forEach((child) => {
                                    if (dataObj[child.id] && dataObj[child.id][typeId]) {
                                        this.setHighLevelSeverity(dataObj[child.id][typeId], severity);
                                    }
                                });
                            }

                            floorObj[typeId] = severity.name;
                        })

                        eventData[floorPageId] = { "types": floorObj, "pages": pagesData };
                    })

                    this.eventDataObj = eventData;
                    if (this.openPopupData) {
                        this.openEventListPopup(this.openPopupData.floorId, this.openPopupData.typeId);
                    }
                },


                loadedIconImage(event) {
                    let $img = jQuery(event.target);
                    let imgClass = $img.attr('class');
                    let imgURL = $img.attr('src');

                    jQuery.get(imgURL, function(data) {
                        let $svg = jQuery(data).find('svg');
                        $svg = $svg.attr('id', Math.random());
                        $svg = $svg.attr('class', imgClass + ' replaced-svg');
                        $svg = $svg.removeAttr('xmlns:a');
                        if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                        }

                        $img.replaceWith($svg);
                    }, 'xml');
                }
            }
        })
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("typeInfo")) {
            this.validateCallLater(this.updateTypeList);
        }
    }


    updateTypeList() {
        var groupId = this.getGroupPropertyValue("typeInfo", "typeGroupName");

        // 그룹 이름이 선택되지 않는 경우 첫 번째 그룹으로
        let typeList = null;
        if (!groupId) {
            typeList = window.wemb.dcimManager.typesetManager.getFirstAssetTypeGroupData();
        } else {
            typeList = window.wemb.dcimManager.typesetManager.getAssetTypesByGroupName(groupId);
        }

        this.app.updateAssetTypes(typeList);
    }
}

WVPropertyManager.attach_default_component_infos(DCIMDashboardComponent, {
    "info": {
        "componentName": "DCIMDashboardComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 1140,
        "height": 640
    },

    "style": {},

    "typeInfo": {
        "typeGroupName": ""
    }
});

DCIMDashboardComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "dcim-asset-typeset"
}];
