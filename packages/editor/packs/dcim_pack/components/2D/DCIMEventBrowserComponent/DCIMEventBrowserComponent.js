class DCIMEventBrowserComponent extends WVDOMComponent {

      constructor() {
            super();
            this.isAll = false;
      }

      _onDestroy() {

            if(this.app) {
                  this.app.$destroy();
                  this.app = null;
            }

            if(this.popup) {
                  this.popup.$destroy();
                  this.popup = null;
            }

            if(this.$audio) {
                  this.$audio.get(0).pause();
            }

            if(this.$temp) {
                  this.$temp.remove();
            }


            super._onDestroy();
      }

      _onCreateElement() {
            $(this._element).addClass("dcim-event-browser");
            this.$audio = $('<audio id="evt_browser_audio" loop></audio>');
            $(this._element).append(this.$audio);

            let str = `<div class="container">
                              <div class="tool-area">
                                    <div class="border-label"><label>{{ initTitle }}</label>
                                          <div class="btn-sound" :class="{off: !hasSoundEffect}" @click="_btnSoundClick"></div>
                                          <div class="auto-btn checks"><input type="checkbox" :id="_uid" @change="isAuto = !isAuto;"> <label :for="_uid">Auto</label>
                                          </div>
                                          <div class="fold-btn" @click="_btnFoldClick">
                                                <div :class="{up: isFolded, down: !isFolded}"></div>
                                          </div>
                                    </div>
                                    <div class="ack-btn-area">
                                          <div v-for="(v, sItem) in severityInfo" class="btn" :class="{active: severityInfo[sItem].active}" :key="sItem"
                                                @click="_btnSeverityClick(sItem)">
                                                <div class="bullet" :class="sItem"></div>
                                                <div>{{severityInfo[sItem].count}}</div>
                                          </div>
                                          
                                          <div class="btn btn-ack" @click="_btnAckClick">Ack</div>
                                          <div class="btn btn-ack" @click="_btnAllClick">ALL</div>
                                    </div>
                              </div>
                              <div class="grid-area">
                                    <div class="header">
                                          <div class="severity">{{lang.common.class}}</div>
                                          <div class="asset_name">{{lang.common.equipmentName}}</div>
                                          <div class="stime">{{lang.common.time}}</div>
                                          <div class="asset_type">{{lang.common.eventType}}</div>
                                          <div class="dupl_cnt">{{lang.common.overlap}}</div>
                                          <div class="msg">{{lang.common.message}}</div>
                                    </div>
                                    <div style="flex:1; overflow: auto;">
                                          <div class="contents">
                                                <div v-for="item in dataProvider" class="content" :data-id="item.evt_id" 
                                                            :data-asset-id="item.asset_id" :key="item.evt_id"
                                                            @click="_contentClick" @dblclick="_contentDbClick">
                                                    <div class="severity" :class="item.severity">{{item.severity.substring(0,2).toUpperCase()}}</div>
                                                    <div class="asset_name">{{item.asset_name}}</div>
                                                    <div class="stime">{{item.stime}}</div>
                                                    <div class="asset_type">{{item.asset_type}}</div>
                                                    <div class="dupl_cnt">{{item.dupl_cnt}}</div>
                                                    <div class="msg" style="text-align: left;" :data-tooltip="item.msg">{{item.msg}}</div>
                                                </div>
                                                <div v-if="!dataProvider || dataProvider.length == 0" class='no-data'>No data.</div>
                                          </div>
                                    </div>
                              </div>
                        </div>`;
            let temp = $(str);
            $(this._element).append(temp);
            let self = this;
            let eventBrowserTitle = this.getGroupPropertyValue('title', 'event_browser_title'); // 처음 DB의 Setting 값으로 되어있는 title
            this.app = new Vue({
                  el: temp.get(0),
                  data: function() {
                        return {
                              lang : Vue.$i18n.messages.wv.dcim_pack,
                              dataProvider: [],
                              beforeTime: new Date(),
                              isAuto: false,
                              isFolded: false,
                              hasSoundEffect: true,
                              foldedHeight : 45,
                              severityInfo: {
                                    critical: {count: 0, active: false},
                                    major: {count: 0, active: false},
                                    minor: {count: 0, active: false},
                                    warning: {count: 0, active: false},
                                    normal: {count: 0, active: false}
                              },
                              criticalList: [],
                              title: eventBrowserTitle
                        }
                  },
                  computed: {
                        initTitle: function(){
                              return this.title.length === 0 ? this.lang.common.eventStatus : this.title;
                        }
                  },
                  mounted() {
                        if(!self.isEditorMode) {
                              window.wemb.dcimManager.eventManager.$on(DCIMManager.GET_EVENT_LIST,    this.dataProviderHanlder);
                              this._setDataProvider(window.wemb.dcimManager.eventManager.eventBrowserList);
                        }

                        $(self._element).find(".container").tooltip({
                              items       : ".msg",
                              show        : {delay: 1000},
                              content     : function(){
                                    let msg = $(this).attr("data-tooltip");
                                    return msg;
                              }
                        })
                  },

                  created() {
                        this.dataProviderHanlder = this._setDataProvider.bind(this);
                        this.requestHandler = this.requestEventData.bind(this);
                  },
                  destroyed() {
                        window.wemb.dcimManager.eventManager.$off(DCIMManager.GET_EVENT_LIST, this.dataProviderHanlder);

                        this.dataProviderHanlder = null;
                        this.requestHandler = null;

                        $(self._element).find(".container").tooltip("destroy");
                  },

                  methods: {
                        _setDataProvider(event = null) {
                              this.beforeTime = new Date().getTime() - 1000 * 60 * 60 * 24;
                              this.dataProvider = event.list;
                              this.criticalList = event.critical;

                              Object.keys(this.severityInfo).map(item=>{
                                    this.severityInfo[item].count = event.count[item] || 0
                              })

                              // auto 여부 판별
                              if (this.isAuto && this.isFolded) {
                                    this._btnFoldClick();
                              }

                              this.soundEffect();
                        },

                        requestEventData(data) {
                              this.call();
                        },

                        call() {
                              try {
                                    let activeList = Object.keys(this.severityInfo).filter(key=> this.severityInfo[key].active);
                                    window.wemb.dcimManager.eventManager.procEventBrowserData(activeList);
                              } catch(err) {
                                    CPLogger.log( this.name + " :: " + err );
                              }
                        },

                        _contentClick(event) {
                              let $target = $(event.currentTarget);
                              let event_id = $target.attr("data-id");
                              let isMulti = event.ctrlKey;

                              if(isMulti) {
                                    $target.toggleClass('active');
                              } else {
                                    $(self._element).find(".contents > .active").not("[data-id="+event_id+"]").removeClass("active");
                                    $target.toggleClass('active');
                              }
                        },

                        _contentDbClick(event) {
                              try {
                                    let $target = $(event.currentTarget);
                                    let event_id = $target.attr("data-id");
                                    let item = this.dataProvider.find(item => item.evt_id == event_id);
                                    let currentPage = wemb.pageManager.currentPageInfo.id;

                                    if (item) {
                                          if(item.page_id == currentPage) {
                                                window.wemb.dcimManager.actionController.gotoAsset3DComponent(item.asset_id);
                                          } else {
                                                window.wemb.pageManager.openPageById(item.page_id, { assetId: item.asset_id });
                                          }

                                          // NEW mark 제거
                                          let data = {ackList: [item], confirm: "Y"};
                                          self.ackEvent(data);
                                    }
                              } catch(err) {
                                    CPLogger.log( this.name + " :: " + err );
                              }
                        },

                        _btnSeverityClick(severity) {
                              this.severityInfo[severity].active = !this.severityInfo[severity].active;
                              this.call();
                        },

                        _btnFoldClick() {
                              this.isFolded = !this.isFolded;

                              if (this.isFolded) {
                                    $(self._element).animate({
                                          "height": this.foldedHeight,
                                          "top": self.y + self.height - this.foldedHeight
                                    }).find(".grid-area").animate({"padding": 0});
                              } else {
                                    $(self._element).animate({"height": self.height, "top": self.y})
                                          .find(".grid-area").animate({"padding": 10});
                              }
                        },

                        _btnAckClick() {
                              let ackList = $(self._element).find(".content.active");
                              if (ackList.length > 0 || (self.isAll && this.dataProvider.length > 0)) {
                                    this.$modal.show('ack-popup');
                              } else {
                                    Vue.$message({message: this.lang.eventBrowser.selectEventToAck, type: 'info'})
                              }
                        },

                        _btnSoundClick() {
                              this.hasSoundEffect = !this.hasSoundEffect;
                              this.soundEffect();
                        },

                        _btnAllClick() {
                              self.isAll = true;
                              this._btnAckClick();
                        },

                        _btnPlayClick(item) {
                              console.log("play click", item);
                              window.wemb.dcimManager.playQENXViewer(item);
                        },

                        soundEffect() {
                              if(this.hasSoundEffect) {
                                    if(this.criticalList && this.criticalList.length > 0) {
                                          try {
                                                self.$audio.get(0).src = './custom/packs/dcim_pack/components/2D/DCIMEventBrowserComponent/resource/sound/' + this.criticalList[0].asset_type + '.mp3';
                                                self.$audio.get(0).play();
                                          } catch(err) {
                                                console.log(err);
                                          }
                                    } else {
                                          self.$audio.get(0).pause();
                                    }
                              } else {
                                    self.$audio.get(0).pause();
                              }
                        }
                  }
            })

            this._createAckPopup();
      }

      _createAckPopup() {
            let str = `<modal name="ack-popup"
                         id="dcim-ack-popup"
                         class="ack-popup"
                         :height="150"
                         :width="300"
                         :click-to-close="false"
                         @before-open="beforeOpen">
                    <div class="modal-header">
                          <h4>Ack Message Popup</h4>
                          <a class="close-modal-btn" role="button" @click="closedPopup">
                              <i class="el-icon-close"></i>
                          </a>
                    </div>
                    <div class="modal-body">
                        <el-input size="small" v-model="value"></el-input>
                    </div>
                    <div class="modal-footer">
                            <el-button class="bottom-btn" size="small" @click="procAck">Save</el-button>
                            <el-button class="bottom-btn" size="small" @click="closedPopup">Cancel</el-button>
                        </div>
                 </modal>`;
            this.$temp = $(str);
            let self = this;
            $("body").append(this.$temp);
            this.popup = new Vue({
                  el: self.$temp.get(0),
                  data() {
                        return {value: null}
                  },
                  methods: {
                        beforeOpen(event) {
                              this.value = null;
                        },
                        closedPopup() {
                              this.$modal.hide('ack-popup');
                              self.isAll = false;
                        },
                        procAck() {
                              if (this.value) {
                                    self._procAck(this.value);
                                    this.closedPopup();
                              } else {
                                    var lang = Vue.$i18n.messages.wv.dcim_pack;
                                    Vue.$message({message: lang.eventBrowser.ackMessage, type: 'info'});
                              }
                        }
                  }
            });
      }

      _procAck(value) {
            let user_id = window.wemb.configManager.userId;
            let activeList = $(this._element).find(".content.active");
            let ackList = [];
            if(this.isAll) {
                  ackList = this.app.dataProvider;
            } else {
                  activeList.map((i,item)=>{
                        let evt_id = $(item).attr("data-id");
                        ackList.push(this.app.dataProvider.find(x=> x.evt_id == evt_id))
                  });
            }

            let item = {
                  ackList: ackList,
                  ack_info: value,
                  ack: 'Y',
                  ack_user: user_id,
                  confirm: "Y"
            };

            this.ackEvent(item);
            this.isAll = false;
            $(self._element).find(".content").removeClass("active");
      }

      ackEvent(item) {
            try {
                  window.wemb.dcimManager.eventManager.procAckEventData(item);
            } catch(err) {
                  CPLogger.log( this.name + " :: " + err );
            }
      }

      _onCommitProperties() {
            if (this._updatePropertiesMap.has("title")) {
                  this.app.title = this.getGroupPropertyValue('title', 'event_browser_title');
                  this.app.$forceUpdate();
            }
      }
}

WVPropertyManager.attach_default_component_infos(DCIMEventBrowserComponent, {
      "info": {
            "componentName": "DCIMEventBrowserComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 900,
            "height": 230
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIMEventBrowserComponent"
      },
      "title": {
            "event_browser_title": ''
      }
});

DCIMEventBrowserComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}]

WVPropertyManager.add_property_panel_group_info(DCIMEventBrowserComponent, {
      label : "Event Browser Title",
      template : "vertical",
      children : [
            {
                  owner : "title",
                  name : "event_browser_title",
                  type : "string",
                  label : "Title",
                  show : true,
                  writable : true,
                  description : "타이틀"
            }
      ]
});
