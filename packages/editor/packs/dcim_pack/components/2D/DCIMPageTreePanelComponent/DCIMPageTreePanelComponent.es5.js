"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var DCIMPageTreePanelComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(DCIMPageTreePanelComponent, _WVDOMComponent);

            function DCIMPageTreePanelComponent() {
                  var _this;

                  _classCallCheck(this, DCIMPageTreePanelComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(DCIMPageTreePanelComponent).call(this));
                  _this.title = Vue.$i18n.messages.wv.dcim_pack.common.area;
                  _this.clickTreeItemHandler = _this.clickTreeItem.bind(_assertThisInitialized(_this));
                  _this.updatePageSetHandler = _this.updatePageSet.bind(_assertThisInitialized(_this));
                  _this.flatData = [];
                  return _this;
            }

            _createClass(DCIMPageTreePanelComponent, [{
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        var initTitle = this.getGroupPropertyValue('page', 'title'); // DB에 저장 된 Page Title

                        if (initTitle.length !== 0) {
                              this.title = initTitle;
                        }
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.clickTreeItemHandler = null;
                        window.wemb.dcimManager.pagesetManager.$off(DCIMManager.UPDATE_PAGESET_LIST, this.updatePageSetHandler);
                        this.updatePageSetHandler = null;

                        _get(_getPrototypeOf(DCIMPageTreePanelComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        $(this._element).addClass("page-tree-panel");
                        var str = '<div class="container">' + '      <div class="border-label">' + this.title + '</div>' + '      <div class="tree-bg">' + '            <div class="tree"></div>' + '      </div>' + '</div>' + '<div class="icon-bg">' + '      <div class="bullet"></div>' + '</div>';
                        this._element.innerHTML = str;
                        this.$container = $(this._element).find(".container");
                        $(this._element).find(".icon-bg").css("left", this.width - 40 + "px");
                  }
            }, {
                  key: "onLoadPage",
                  value: function onLoadPage() {
                        this.activeTreeItem(wemb.pageManager.currentPageInfo.id);
                  }
            }, {
                  key: "onOpenPage",
                  value: function onOpenPage() {
                        this.activeTreeItem(wemb.pageManager.currentPageInfo.id);
                  }
            }, {
                  key: "bindEvent",
                  value: function bindEvent() {
                        window.wemb.dcimManager.pagesetManager.$on(DCIMManager.UPDATE_PAGESET_LIST, this.updatePageSetHandler);
                  }
            }, {
                  key: "updatePageSet",
                  value: function updatePageSet() {
                        this._setDataProvider();
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._updatePropertiesMap.has("extension")) {
                              this.validateCallLater(this._updateFoldFlag);
                        }

                        if (this._updatePropertiesMap.has("page")) {
                              this._element.querySelector('div.border-label').innerText = this.getGroupPropertyValue('page', 'title');
                        }
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        this.bindEvent();

                        this._updateFoldFlag();

                        this._setDataProvider();

                        this._setBtnEvent();
                  }
            }, {
                  key: "_setDataProvider",
                  value: function _setDataProvider() {
                        var pageset = window.wemb.dcimManager.pagesetManager.getPageset();

                        if (!pageset) {
                              this._setEmptyMsg();
                        } else {
                              this.dataProvider = pageset.structure;
                              $(this._element).find(".tree").empty();

                              this._setTreeData(this.dataProvider, $(this._element).find(".tree"));
                        }
                  }
            }, {
                  key: "_setEmptyMsg",
                  value: function _setEmptyMsg() {
                        var emptyMsg = $("<span class='empty-msg'>NO DATA</span>");
                        $(this._element).find(".tree").empty().append(emptyMsg);
                  }
            }, {
                  key: "_setTreeData",
                  value: function _setTreeData(list, parent) {
                        var _this2 = this;

                        var $ul = $("<ul></ul>");
                        list.map(function (item) {
                              var $li = $("<li data-id='" + item.id + "'></li>");
                              var $treeItem = $('<span class="tree-item"><label>' + item.name + '</label></span>');

                              _this2.bindTreeItem($treeItem);

                              $li.append($treeItem);

                              _this2.flatData.push(item);

                              if (item.children && item.children.length > 0) {
                                    var icon = $("<div class='icon icon-minus'></div>");
                                    $li.find(".tree-item").prepend(icon);
                                    $ul.append($li);

                                    _this2._setTreeData(item.children, $li);
                              } else {
                                    var icon = $("<div class='icon-bullet'></div>");
                                    $li.find(".tree-item").prepend(icon);
                                    $ul.append($li);
                              }
                        });
                        parent.append($ul);
                  }
                  /**
                   * 이벤트 바인딩 메소드
                   * @private
                   */

            }, {
                  key: "_setBtnEvent",
                  value: function _setBtnEvent() {
                        var self = this;

                        if (this.useFold) {
                              $(this._element).find(".icon-bg").click(function () {
                                    // 패널 위치 변경
                                    self.$container.toggle("slide", 800); // 화살표 위치 변경

                                    $(this).toggleClass("icon-bg-close"); // 화살표 모양 변경

                                    if ($(this).hasClass("icon-bg-close")) {
                                          $(this).find(".bullet").attr("class", "bullet-open");
                                          $(this).animate({
                                                left: "-38px"
                                          }, 800);
                                    } else {
                                          $(this).animate({
                                                left: self.width - 40 + "px"
                                          }, 800);
                                          $(this).find(".bullet-open").attr("class", "bullet");
                                    }
                              });
                        }

                        this.$container.find(".icon-minus").click(function (event) {
                              event.preventDefault();
                              var submenu = $(this).parent().next();

                              if (submenu.is(":visible")) {
                                    $(this).removeClass("icon-minus").addClass("icon-plus");
                                    submenu.slideUp();
                              } else {
                                    $(this).removeClass("icon-plus").addClass("icon-minus");
                                    submenu.slideDown();
                              }
                        });
                  }
            }, {
                  key: "bindTreeItem",
                  value: function bindTreeItem($treeItem) {
                        $treeItem.on('dblclick', this.clickTreeItemHandler);
                        $treeItem.on('click', this.clickTreeItemHandler);
                  }
            }, {
                  key: "clickTreeItem",
                  value: function clickTreeItem(event) {
                        var itemId = $(event.currentTarget).parent().attr("data-id"); // dispath 이벤트

                        var type = event.type.charAt(0).toUpperCase() + event.type.slice(1);
                        if (!wemb.pageManager.hasPageInfoBy(itemId)) return;
                        this.dispatchWScriptEvent("treeItem" + type, {
                              page_id: itemId
                        }); // 페이지 이동

                        if (this.link == event.type) {
                              var info = wemb.pageManager.getPageInfoBy(itemId);
                              var findPage = window.wemb.pageManager.getPageInfoList().find(function (x) {
                                    return x.id == info.id;
                              });

                              if (!findPage) {
                                    Vue.$message.error("Invalid Page Info");
                              }

                              if (!info.popup) {
                                    wemb.pageManager.openPageById(findPage.id);
                              } else {
                                    if (info.popup) {
                                          var _param = {};

                                          try {
                                                _param = JSON.parse(info.param);
                                          } catch (error) {
                                                console.log(error);
                                          }

                                          wemb.popupManager.open(findPage.name, _param);
                                    }
                              }
                        }
                  }
            }, {
                  key: "activeTreeItem",
                  value: function activeTreeItem(page_id) {
                        $(this._element).find(".tree-item").removeClass('active');
                        $(this._element).find('li[data-id="' + page_id + '"] > .tree-item').addClass('active');
                  }
            }, {
                  key: "_updateFoldFlag",
                  value: function _updateFoldFlag() {
                        var val = this.useFold ? "flex" : "none";
                        $(this._element).find(".icon-bg").css("display", val);
                  }
            }, {
                  key: "link",
                  get: function get() {
                        return this.getGroupPropertyValue("extension", "link");
                  }
            }, {
                  key: "useFold",
                  get: function get() {
                        return this.getGroupPropertyValue("extension", "useFold");
                  },
                  set: function set(bol) {
                        this._checkUpdateGroupPropertyValue("extension", "useFold", bol);
                  }
            }]);

            return DCIMPageTreePanelComponent;
      }(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DCIMPageTreePanelComponent, {
      "info": {
            "componentName": "DCIMPageTreePanelComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 230,
            "height": 550
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIMPageTreePanelComponent"
      },
      "extension": {
            "link": "dblclick",
            "useFold": true
      },
      "page": {
            "title": ''
      }
});
DCIMPageTreePanelComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}, {
      label: "Extension",
      owner: "vertical",
      children: [{
            owner: "extension",
            name: "link",
            type: "select",
            options: {
                  items: [{
                        label: "none",
                        value: "none"
                  }, {
                        label: "click",
                        value: "click"
                  }, {
                        label: "dblclick",
                        value: "dblclick"
                  }]
            },
            label: "Move Event",
            show: true,
            writable: true,
            description: "Status"
      }, {
            owner: "extension",
            name: "useFold",
            type: "checkbox",
            label: "Use Fold",
            show: true,
            writable: true
      }]
}];
WVPropertyManager.add_event(DCIMPageTreePanelComponent, {
      name: "treeItemClick",
      label: "treeItemClick Event",
      description: "treeItemClick Event",
      properties: []
});
WVPropertyManager.add_event(DCIMPageTreePanelComponent, {
      name: "treeItemDblclick",
      label: "dblclick 이벤트",
      description: "DblClick Event",
      properties: []
});
WVPropertyManager.add_property_panel_group_info(DCIMPageTreePanelComponent, {
      label: "Page List Title",
      template: "vertical",
      children: [{
        owner: "page",
        name: "title",
        type: "string",
        label: "Title",
        show: true,
        writable: true,
        description: "타이틀"
      }]
    });