class DCIMPageTreePanelComponent extends WVDOMComponent {

      constructor() {
            super();
            this.title = Vue.$i18n.messages.wv.dcim_pack.common.area;
            this.clickTreeItemHandler = this.clickTreeItem.bind(this);
            this.updatePageSetHandler = this.updatePageSet.bind(this);
            this.flatData = [];
      }

      _onCreateProperties(){
            const initTitle = this.getGroupPropertyValue('page', 'title'); // DB에 저장 된 Page Title

            if(initTitle.length !== 0) {
                  this.title = initTitle;
            } 
      }

      _onDestroy() {
            this.clickTreeItemHandler = null;
            window.wemb.dcimManager.pagesetManager.$off(DCIMManager.UPDATE_PAGESET_LIST, this.updatePageSetHandler);
            this.updatePageSetHandler = null;
            super._onDestroy();
      }

      _onCreateElement() {
            $(this._element).addClass("page-tree-panel");
            var str = '<div class="container">' +
                  '      <div class="border-label">' + this.title + '</div>' +
                  '      <div class="tree-bg">' +
                  '            <div class="tree"></div>' +
                  '      </div>' +
                  '</div>' +
                  '<div class="icon-bg">' +
                  '      <div class="bullet"></div>' +
                  '</div>';

            this._element.innerHTML = str;
            this.$container = $(this._element).find(".container");

            $(this._element).find(".icon-bg").css("left", (this.width - 40) + "px");
      }

      onLoadPage() {
            this.activeTreeItem(wemb.pageManager.currentPageInfo.id);
      }

      onOpenPage(){
            this.activeTreeItem(wemb.pageManager.currentPageInfo.id);
      }

      bindEvent() {
            window.wemb.dcimManager.pagesetManager.$on(DCIMManager.UPDATE_PAGESET_LIST, this.updatePageSetHandler);
      }

      updatePageSet() {
            this._setDataProvider();
      }

      _onCommitProperties() {
            if (this._updatePropertiesMap.has("extension")) {
                  this.validateCallLater(this._updateFoldFlag);
            }

            if (this._updatePropertiesMap.has("page")) {
                  this._element.querySelector('div.border-label').innerText = this.getGroupPropertyValue('page', 'title');
            }
      }

      _onImmediateUpdateDisplay() {
            this.bindEvent();
            this._updateFoldFlag();
            this._setDataProvider();
            this._setBtnEvent();
      }
      
      _setDataProvider() {
            var pageset = window.wemb.dcimManager.pagesetManager.getPageset();

            if (!pageset) {
                  this._setEmptyMsg();
            } else {
                  this.dataProvider = pageset.structure;
                  $(this._element).find(".tree").empty();
                  this._setTreeData(this.dataProvider, $(this._element).find(".tree"));
            }
      }

      _setEmptyMsg() {
            var emptyMsg = $("<span class='empty-msg'>NO DATA</span>");
            $(this._element).find(".tree").empty().append(emptyMsg);
      }

      _setTreeData(list, parent) {
            var $ul = $("<ul></ul>");

            list.map(item => {
                  var $li = $("<li data-id='" + item.id + "'></li>");
                  var $treeItem = $('<span class="tree-item"><label>' + item.name + '</label></span>');
                  this.bindTreeItem($treeItem);

                  $li.append($treeItem);
                  this.flatData.push(item);

                  if (item.children && item.children.length > 0) {
                        var icon = $("<div class='icon icon-minus'></div>");
                        $li.find(".tree-item").prepend(icon);
                        $ul.append($li);
                        this._setTreeData(item.children, $li);
                  } else {
                        var icon = $("<div class='icon-bullet'></div>");
                        $li.find(".tree-item").prepend(icon);
                        $ul.append($li);
                  }
            })

            parent.append($ul);
      }

      /**
       * 이벤트 바인딩 메소드
       * @private
       */
      _setBtnEvent() {
            var self = this;
            if(this.useFold) {

                  $(this._element).find(".icon-bg").click(function () {
                        // 패널 위치 변경
                        self.$container.toggle("slide", 800);

                        // 화살표 위치 변경
                        $(this).toggleClass("icon-bg-close");

                        // 화살표 모양 변경
                        if ($(this).hasClass("icon-bg-close")) {
                              $(this).find(".bullet").attr("class", "bullet-open");
                              $(this).animate({ left: "-38px"}, 800);
                        } else {
                              $(this).animate({ left: (self.width - 40) + "px"}, 800);
                              $(this).find(".bullet-open").attr("class", "bullet");
                        }
                  });
            }

            this.$container.find(".icon-minus").click(function (event) {
                  event.preventDefault();
                  var submenu = $(this).parent().next();
                  if (submenu.is(":visible")) {
                        $(this).removeClass("icon-minus").addClass("icon-plus");
                        submenu.slideUp();
                  } else {
                        $(this).removeClass("icon-plus").addClass("icon-minus");
                        submenu.slideDown();
                  }
            })
      }

      bindTreeItem($treeItem) {
            $treeItem.on('dblclick', this.clickTreeItemHandler);
            $treeItem.on('click', this.clickTreeItemHandler);
      }

      clickTreeItem(event) {
            let itemId = $(event.currentTarget).parent().attr("data-id");
            // dispath 이벤트
            let type = event.type.charAt(0).toUpperCase() + event.type.slice(1);
            if (!wemb.pageManager.hasPageInfoBy(itemId)) return;
            this.dispatchWScriptEvent("treeItem" + type, {page_id: itemId});
            // 페이지 이동
            if(this.link == event.type) {
                  let info = wemb.pageManager.getPageInfoBy(itemId);
                  let findPage = window.wemb.pageManager.getPageInfoList().find(x => x.id == info.id);

                  if (!findPage) {
                        Vue.$message.error("Invalid Page Info");
                  }

                  if (!info.popup) {
                        wemb.pageManager.openPageById(findPage.id);
                  } else {
                        if (info.popup) {
                              let _param = {};
                              try {
                                    _param = JSON.parse(info.param);
                              } catch (error) {
                                    console.log(error);
                              }

                              wemb.popupManager.open(findPage.name, _param);
                        }
                  }
            }
      }

      activeTreeItem(page_id) {
            $(this._element).find(".tree-item").removeClass('active');
            $(this._element).find('li[data-id="'+page_id+'"] > .tree-item').addClass('active');
      }

      _updateFoldFlag() {
            let val = this.useFold ? "flex" : "none";
            $(this._element).find(".icon-bg").css("display", val);
      }

      get link() {
            return this.getGroupPropertyValue("extension", "link");
      }

      get useFold() {
            return this.getGroupPropertyValue("extension", "useFold");
      }

      set useFold(bol) {
            this._checkUpdateGroupPropertyValue("extension", "useFold", bol);
      }
}

WVPropertyManager.attach_default_component_infos(DCIMPageTreePanelComponent, {
      "info": {
            "componentName": "DCIMPageTreePanelComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 230,
            "height": 550
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIMPageTreePanelComponent"
      },
      "extension": {
            "link": "dblclick",
            "useFold" : true
      },
      "page": {
            "title": ''
      }
});

DCIMPageTreePanelComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "cursor"
}, {
      template: "label"
}, {
      label: "Extension",
      owner: "vertical",
      children: [{
            owner: "extension",
            name: "link",
            type: "select",
            options: {
                  items: [
                        {label: "none", value: "none"},
                        {label: "click", value: "click"},
                        {label: "dblclick", value: "dblclick"},
                  ]
            },
            label: "Move Event",
            show: true,
            writable: true,
            description: "Status"
      }, {
            owner: "extension",
            name: "useFold",
            type: "checkbox",
            label: "Use Fold",
            show: true,
            writable: true
      }]
}]
WVPropertyManager.add_event(DCIMPageTreePanelComponent, {
      name: "treeItemClick",
      label: "treeItemClick Event",
      description: "treeItemClick Event",
      properties: []
});

WVPropertyManager.add_event(DCIMPageTreePanelComponent,  {
      name: "treeItemDblclick",
      label: "dblclick 이벤트",
      description: "DblClick Event",
      properties: []
});

WVPropertyManager.add_property_panel_group_info(DCIMPageTreePanelComponent, {
      label : "Page List Title",
      template : "vertical",
      children : [
            {
                  owner : "page",
                  name : "title",
                  type : "string",
                  label : "Title",
                  show : true,
                  writable : true,
                  description : "타이틀"
            }
      ]
});
