class DCIMVideoPlayerComponent extends WVDOMComponent {

    constructor() {
        super();
    }

    _onCreateProperties() {

    }

    //element 생성
    _onCreateElement() {
        this.$canvas = $('<canvas style="width: 100%;height:100%;"></canvas>');
        $(this._element).append(this.$canvas);
    }

    ///페이지 로드가 다 끝났을때
    onLoadPage() {
        var ws = new WebSocket("ws://"+location.hostname + ":" + (parseInt(location.port) + 1));
        this.player = new jsmpeg(ws, {canvas:this.$canvas[0], autoplay:true, audio:true, loop:true});
    }

    ///화면에 붙였을때
    _onImmediateUpdateDisplay() {

    }

    //properties 변경시
    _onCommitProperties() {

    }



    _onDestroy() {
        super._onDestroy();
    }
}

WVPropertyManager.attach_default_component_infos(DCIMVideoPlayerComponent, {
    "info": {
        "componentName": "DCIMVideoPlayerComponent",
        "version": "1.0.0"
    },

    ///외부에서 접근하는 property는 이곳에 선언
    "setter": {
        "width": 600,
        "height": 400,
        "value": "",
        "placeHolder": ""
    },

    "label": {
        "label_using": "N",
        "label_text": "Video Player Component"
    },

    "background": {
        "type": "",
        "direction": "left", //top, left, diagonal1, diagonal2, radial
        "color1": "#eee",
        "color2": "#000",
        "text": ''
    },
});

InputFieldComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "background-gradient",
    owner: "background",
    label: "Background"
}, {
    template: "label"
}]
