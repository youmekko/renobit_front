"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DCIMVideoPlayerComponent = function (_WVDOMComponent) {
      _inherits(DCIMVideoPlayerComponent, _WVDOMComponent);

      function DCIMVideoPlayerComponent() {
            _classCallCheck(this, DCIMVideoPlayerComponent);

            return _possibleConstructorReturn(this, (DCIMVideoPlayerComponent.__proto__ || Object.getPrototypeOf(DCIMVideoPlayerComponent)).call(this));
      }

      _createClass(DCIMVideoPlayerComponent, [{
            key: "_onCreateProperties",
            value: function _onCreateProperties() {}

            //element 생성

      }, {
            key: "_onCreateElement",
            value: function _onCreateElement() {
                  this.$canvas = $('<canvas style="width: 100%;height:100%;"></canvas>');
                  $(this._element).append(this.$canvas);
            }

            ///페이지 로드가 다 끝났을때

      }, {
            key: "onLoadPage",
            value: function onLoadPage() {
                  var ws = new WebSocket("ws://" + location.hostname + ":" + (parseInt(location.port) + 1));
                  this.player = new jsmpeg(ws, { canvas: this.$canvas[0], autoplay: true, audio: true, loop: true });
            }

            ///화면에 붙였을때

      }, {
            key: "_onImmediateUpdateDisplay",
            value: function _onImmediateUpdateDisplay() {}

            //properties 변경시

      }, {
            key: "_onCommitProperties",
            value: function _onCommitProperties() {}
      }, {
            key: "_onDestroy",
            value: function _onDestroy() {
                  _get(DCIMVideoPlayerComponent.prototype.__proto__ || Object.getPrototypeOf(DCIMVideoPlayerComponent.prototype), "_onDestroy", this).call(this);
            }
      }]);

      return DCIMVideoPlayerComponent;
}(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DCIMVideoPlayerComponent, {
      "info": {
            "componentName": "DCIMVideoPlayerComponent",
            "version": "1.0.0"
      },

      ///외부에서 접근하는 property는 이곳에 선언
      "setter": {
            "width": 600,
            "height": 400,
            "value": "",
            "placeHolder": ""
      },

      "label": {
            "label_using": "N",
            "label_text": "Video Player Component"
      },

      "background": {
            "type": "",
            "direction": "left", //top, left, diagonal1, diagonal2, radial
            "color1": "#eee",
            "color2": "#000",
            "text": ''
      }
});

InputFieldComponent.property_panel_info = [{
      template: "primary"
}, {
      template: "pos-size-2d"
}, {
      template: "background-gradient",
      owner: "background",
      label: "Background"
}, {
      template: "label"
}];
