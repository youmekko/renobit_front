class DCIMBreadcrumbsComponent extends WVDOMComponent {
    constructor() {
        super();
        this.app = null;
    }


    _onCreateElement() {
        this.createVueElement();
        this.updatePageDepth();
    }

    onOpenPage() {
        if (this.app) {
            this.updatePageDepth();
        }
    }

    updatePageDepth() {
        let depth = window.wemb.dcimManager.pagesetManager.getPageBreadcrumbs(window.wemb.pageManager.currentPageInfo.id);
        this.app.updatePageDepth(depth);
    }

    createVueElement() {
        var str = `
            <div class="dcim-breadcrumbs-component">
                 <ul :style="fontStyleStr" v-if="depth && depth.length">
                    <li v-for="(item, index) in depth">
                        <template v-if="index != depth.length-1">
                            <a :style="'color:'+normalColor+';'"
                            @click.prevent="movePage(item.id)" href="#">{{item.name}}</a>
                            <i class="el-icon-arrow-right"></i>
                        </template>
                        <template v-else>
                            <span>{{item.name}}</span>
                        </template>
                    </li>
                </ul>                
            </div>
            `;

        let $temp = $(str);
        let self = this;
        $(this._element).append($temp);
        this.app = new Vue({
            el: $temp.get(0),
            data: function() {
                return {
                    depth: [],
                    fontStyleStr: '',
                    normalColor: '#cbcbcb'
                }
            },

            mounted: function() {

            },

            methods: {
                getFontStyleStr(data) {
                    let style = "font-family:" + data.font_type;
                    style += ";font-size:" + data.font_size;
                    style += ";font-weight:" + data.font_weight;
                    style += ";color:" + data.font_color;
                    return style;
                },

                updateFontStyle(style) {
                    this.fontStyleStr = this.getFontStyleStr(style);
                    this.normalColor = style.font_color;
                },

                updatePageDepth: function(list) {
                    this.depth.splice(0, this.depth.length);
                    this.depth.push(...list);
                },

                movePage: function(pageId) {
                      if (window.wemb.pageManager.hasPageInfoBy(pageId)) {
                            window.wemb.pageManager.openPageById(pageId);
                      }
                }
            }
        });
    }

    _onImmediateUpdateDisplay() {
        this._updateFontProperty();
    }

    _onCommitProperties() {
        if (this._updatePropertiesMap.has("font")) {
            this.validateCallLater(this._updateFontProperty);
        }
    }


    _updateFontProperty() {
        var fontProps = this.getGroupProperties("font");
        if (this.app) {
            this.app.updateFontStyle(fontProps);
        }
    }

    _onDestroy() {
        this.app = null;
    }
}


// 기본 프로퍼티 정보입니다.
WVPropertyManager.attach_default_component_infos(DCIMBreadcrumbsComponent, {
    "info": {
        "componentName": "DCIMBreadcrumbsComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 190,
        "height": 15,
        "imagePath": "custom/packs/posod_pack/components/2D/DCIMBreadcrumbsComponent/src/"
    },

    "label": {
        "label_using": "N",
        "label_text": "DCIM Breadcrumbs"
    },

    "font": {
        "font_type": "inherit",
        "font_color": "#cbcbcb",
        "font_size": 12,
        "font_weight": "normal"
    }
});


// 프로퍼티 패널에서 사용할 정보 입니다.
DCIMBreadcrumbsComponent.property_panel_info = [{
        template: "primary",
        label: "primary"
    },
    {
        template: "pos-size-2d",
        label: "pos-size-2d"
    }, {
        template: "font"
    }
];
