"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var DCIMBreadcrumbsComponent =
      /*#__PURE__*/
      function (_WVDOMComponent) {
            _inherits(DCIMBreadcrumbsComponent, _WVDOMComponent);

            function DCIMBreadcrumbsComponent() {
                  var _this;

                  _classCallCheck(this, DCIMBreadcrumbsComponent);

                  _this = _possibleConstructorReturn(this, _getPrototypeOf(DCIMBreadcrumbsComponent).call(this));
                  _this.app = null;
                  return _this;
            }

            _createClass(DCIMBreadcrumbsComponent, [{
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        this.createVueElement();
                        this.updatePageDepth();
                  }
            }, {
                  key: "onOpenPage",
                  value: function onOpenPage() {
                        if (this.app) {
                              this.updatePageDepth();
                        }
                  }
            }, {
                  key: "updatePageDepth",
                  value: function updatePageDepth() {
                        var depth = window.wemb.dcimManager.pagesetManager.getPageBreadcrumbs(window.wemb.pageManager.currentPageInfo.id);
                        this.app.updatePageDepth(depth);
                  }
            }, {
                  key: "createVueElement",
                  value: function createVueElement() {
                        var str = "\n            <div class=\"dcim-breadcrumbs-component\">\n                 <ul :style=\"fontStyleStr\" v-if=\"depth && depth.length\">\n                    <li v-for=\"(item, index) in depth\">\n                        <template v-if=\"index != depth.length-1\">\n                            <a :style=\"'color:'+normalColor+';'\"\n                            @click.prevent=\"movePage(item.id)\" href=\"#\">{{item.name}}</a>\n                            <i class=\"el-icon-arrow-right\"></i>\n                        </template>\n                        <template v-else>\n                            <span>{{item.name}}</span>\n                        </template>\n                    </li>\n                </ul>                \n            </div>\n            ";
                        var $temp = $(str);
                        var self = this;
                        $(this._element).append($temp);
                        this.app = new Vue({
                              el: $temp.get(0),
                              data: function data() {
                                    return {
                                          depth: [],
                                          fontStyleStr: '',
                                          normalColor: '#cbcbcb'
                                    };
                              },
                              mounted: function mounted() {},
                              methods: {
                                    getFontStyleStr: function getFontStyleStr(data) {
                                          var style = "font-family:" + data.font_type;
                                          style += ";font-size:" + data.font_size;
                                          style += ";font-weight:" + data.font_weight;
                                          style += ";color:" + data.font_color;
                                          return style;
                                    },
                                    updateFontStyle: function updateFontStyle(style) {
                                          this.fontStyleStr = this.getFontStyleStr(style);
                                          this.normalColor = style.font_color;
                                    },
                                    updatePageDepth: function updatePageDepth(list) {
                                          var _this$depth;

                                          this.depth.splice(0, this.depth.length);

                                          (_this$depth = this.depth).push.apply(_this$depth, _toConsumableArray(list));
                                    },
                                    movePage: function movePage(pageId) {
                                          if (window.wemb.pageManager.hasPageInfoBy(pageId)) {
                                                window.wemb.pageManager.openPageById(pageId);
                                          }
                                    }
                              }
                        });
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        this._updateFontProperty();
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        if (this._updatePropertiesMap.has("font")) {
                              this.validateCallLater(this._updateFontProperty);
                        }
                  }
            }, {
                  key: "_updateFontProperty",
                  value: function _updateFontProperty() {
                        var fontProps = this.getGroupProperties("font");

                        if (this.app) {
                              this.app.updateFontStyle(fontProps);
                        }
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.app = null;
                  }
            }]);

            return DCIMBreadcrumbsComponent;
      }(WVDOMComponent); // 기본 프로퍼티 정보입니다.


WVPropertyManager.attach_default_component_infos(DCIMBreadcrumbsComponent, {
      "info": {
            "componentName": "DCIMBreadcrumbsComponent",
            "version": "1.0.0"
      },
      "setter": {
            "width": 190,
            "height": 15,
            "imagePath": "custom/packs/posod_pack/components/2D/DCIMBreadcrumbsComponent/src/"
      },
      "label": {
            "label_using": "N",
            "label_text": "DCIM Breadcrumbs"
      },
      "font": {
            "font_type": "inherit",
            "font_color": "#cbcbcb",
            "font_size": 12,
            "font_weight": "normal"
      }
}); // 프로퍼티 패널에서 사용할 정보 입니다.

DCIMBreadcrumbsComponent.property_panel_info = [{
      template: "primary",
      label: "primary"
}, {
      template: "pos-size-2d",
      label: "pos-size-2d"
}, {
      template: "font"
}];
