class DCIMTopNavComponent extends WVDOMComponent {
    constructor() {
        super();
        this.$editorBtn;
        this.$helpBtn;
        this.$logoutBtn;

        this.helpType;
        this.helpPage;
        this.helpPopupOption;
        this.lang = Vue.$i18n.messages.wv.dcim_pack;


    }

    onLoadPage() {

    }


    _onCreateElement() {
        let $el = $(this._element);
    //    let imagePath = "custom/assets/dcim_pack/DCIMTopNavComponent/";
        let imagePath = DCIMManager.PACK_PATH + this.componentName + "/resource/";

        let temp = `<div class='comp-wrap'>
                              <div class="img-btn editor-btn" title=` + this.lang.topNav.editor + `>
                                    <img class="up" src="` + imagePath + `btn_editor.png">
                                    <img class="hover" src="` + imagePath + `btn_editor_select.png">
                              </div>
                              <div class="img-btn help-btn"  title=` + this.lang.topNav.help + `>
                                    <img class="up" src="` + imagePath + `btn_help.png">
                                    <img class="hover" src="` + imagePath + `btn_help_select.png">
                              </div>
                              <div class="img-btn logout-btn"  title=` + this.lang.topNav.logout + `>
                                    <img class="up" src="` + imagePath + `btn_logout.png">
                                    <img class="hover" src="` + imagePath + `btn_logout_select.png">
                              </div>
                        </div>`;

        $el.append(temp);

        this.$editorBtn = $el.find(".editor-btn");
        this.$helpBtn = $el.find(".help-btn");
        this.$logoutBtn = $el.find(".logout-btn");


        if (!window.wemb.userInfo.hasAdminRole) {
            this.$editorBtn.hide();
        }

        if (this.isViewerMode === true) {
            this._bindEvent();
            this._bindHelpBtnEvent();
        }
    }


    _bindEvent() {
        this.$editorBtn.on("click", () => { window.wemb.gotoEditor() });
        this.$logoutBtn.on("click", () => { window.wemb.logout() });
    }

    _bindHelpBtnEvent() {
        this._validateHelp();

        this.$helpBtn.on("click", () => {
            if (this.helpPage !== "") {
                if (this.helpType === 'link') {
                    wemb.pageManager.openPageById(this.helpPage);
                } else {
                    let pageName = wemb.pageManager.getPageNameById(this.helpPage);
                    if (this.helpPopupOption !== "") {
                        let popupOption = JSON.parse(this.helpPopupOption);
                        wemb.popupManager.open(pageName, popupOption);
                    }
                }
            }
        });
    }

    _validateHelp() {
        this.helpType = this.getGroupPropertyValue("help", "selectType");
        this.helpPage = this.getGroupPropertyValue("help", "selectPage");
        this.helpPopupOption = this.getGroupPropertyValue("help", "popupOption");

    }

    _onDestroy() {
        this.$editorBtn.remove();
        this.$helpBtn.remove();
        this.$logoutBtn.remove();

        this.$editorBtn = null;
        this.$helpBtn = null;
        this.$logoutBtn = null;

        this.helpType = null;
        this.helpPage = null;
        this.helpPopupOption = null;

        super._onDestroy();
    }

}


//기본 프로퍼티 정보
WVPropertyManager.attach_default_component_infos(DCIMTopNavComponent, {
    "info": {
        "componentName": "DCIMTopNavComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 100,
        "height": 26
    },
    "help": {
        "selectType": "link",
        "selectPage": "",
        "popupOption": ""
    }

});


DCIMTopNavComponent.property_panel_info = [{
    template: "primary",
}, {
    template: "pos-size-2d",
}, {
    template: "page-select-input",
    owner: "help",
    label: "Help"
}];



WVPropertyManager.add_event(DCIMTopNavComponent, {
    name: "select",
    label: "Button",
    description: "Button select event",
    properties: [{
        name: "name",
        type: "string",
        default: "",
        description: "favorite button select"
    }]
});
