"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var DCIMMainNavComponent =
    /*#__PURE__*/
    function(_WVDOMComponent) {
        _inherits(DCIMMainNavComponent, _WVDOMComponent);

        function DCIMMainNavComponent() {
            var _this;

            _classCallCheck(this, DCIMMainNavComponent);

            _this = _possibleConstructorReturn(this, _getPrototypeOf(DCIMMainNavComponent).call(this));
            _this.app = null;
            return _this;
        }

        _createClass(DCIMMainNavComponent, [{
            key: "onLoadPage",
            value: function onLoadPage() {}
        }, {
            key: "_onCreateElement",
            value: function _onCreateElement() {
                this.createVueElement();
            }
        }, {
            key: "onOpenPage",
            value: function onOpenPage() {
                if (!this.isEditorMode && this.app) {
                    this.app.opendPage();
                }
            }
        }, {
            key: "createVueElement",
            value: function createVueElement() {
                var str = "\n                <ul class=\"dcim-main-nav-component\">\n                    <template v-if=\"!isEditorMode\">\n                        <li class=\"icon-site\" v-if=\"!isSiteType\"><a href=\"#\" @click.prevent=\"onClickSiteBtn\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_site.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_site_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-dashboard\"><a href=\"#\" @click.prevent=\"onClickDasbboardBtn()\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_dashboard.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_dashboard_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-position\"><a href=\"#\" @click.prevent=\"onClickInitPositionBtn()\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_position.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_position_hover.png'\">\n                        </a></li>\n                    </template>\n                    <template v-else>\n                        <li class=\"icon-site\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_site.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_site_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-dashboard\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_dashboard.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_dashboard_hover.png'\">\n                        </a></li>\n                        <li class=\"icon-position\"><a href=\"#\" @click.prevent=\"\">\n                            <img class=\"up\" :src=\"imagePath + 'icon_position.png'\">\n                            <img class=\"hover\" :src=\"imagePath + 'icon_position_hover.png'\">\n                        </a></li>\n                    </template>\n                </ul>\n            ";
                var $temp = $(str);
                var self = this;
                var isEditorMode = this.isEditorMode;
                var imagePath = DCIMManager.PACK_PATH + this.componentName + "/resource/";
                $(this._element).append($temp);
                this.app = new Vue({
                    el: $temp.get(0),
                    data: function data() {
                        return {
                            lang: Vue.$i18n.messages.wv.dcim_pack,
                            imagePath: imagePath,
                            comp: self,
                            isEditorMode: isEditorMode,
                            assetWatchOn: false,
                            pagesetManager: window.wemb.dcimManager.pagesetManager,
                            isRoomType: true,
                            isSiteType: false,
                            isAnimate: false
                        };
                    },
                    created: function created() {
                        if (!this.isEditorMode) {
                            this.opendPage();
                        }
                    },
                    mounted: function mounted() {},
                    methods: {
                        opendPage: function opendPage() {
                            if (this.isEditorMode) {
                                return;
                            }

                            var pageId = window.wemb.pageManager.currentPageInfo.id;
                            var parents = this.pagesetManager.getParents(pageId);
                            this.isRoomType = parents.length == 4;
                            var siteData = this.pagesetManager.getSite();
                            this.isSiteType = siteData && siteData.id == pageId;
                        },
                        onClickInitPositionBtn: function onClickInitPositionBtn() {
                            var _this2 = this;

                            if (this.isAnimate) {
                                return;
                            }

                            this.isAnimate = true;

                            try {
                                window.wemb.dcimManager.actionController.resetController();
                                this.isAnimate = false;
                            } catch (error) {
                                window.wemb.mainControls.transitionFocusInTarget(wemb.mainControls.position0.clone(), wemb.mainControls.target0.clone(), 1, function() {
                                    _this2.isAnimate = false;
                                });
                            }
                            wemb.$globalBus.$emit(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION);
                        },
                        onClickDasbboardBtn: function onClickDasbboardBtn() {
                            var dashboardType = self.getGroupPropertyValue("dashboard", "selectType");
                            var dashboardPage = self.getGroupPropertyValue("dashboard", "selectPage");
                            var dashboardPopupOption = self.getGroupPropertyValue("dashboard", "popupOption");

                            if (dashboardPage !== "") {
                                if (dashboardType === 'link') {
                                    wemb.pageManager.openPageById(dashboardPage);
                                } else {
                                    var pageName = wemb.pageManager.getPageNameById(dashboardPage);
                                    var options = {};

                                    if (dashboardPopupOption !== "") {
                                        try {
                                            options = JSON.parse(dashboardPopupOption);
                                        } catch (error) {}
                                    }

                                    wemb.popupManager.open(pageName, options);
                                }
                            }
                        },
                        onClickSiteBtn: function onClickSiteBtn() {
                            try {
                                var siteData = this.pagesetManager.getSite();
                                var validPageId = window.wemb.pageManager.getPageInfoBy(siteData.id).id;
                                window.wemb.pageManager.openPageById(validPageId);
                            } catch (error) {
                                Vue.$message.error(this.lang.mainNav.checkSiteInfo);
                            }
                        }
                    }
                });
            }
        }]);

        return DCIMMainNavComponent;
    }(WVDOMComponent);

WVPropertyManager.attach_default_component_infos(DCIMMainNavComponent, {
    "info": {
        "componentName": "DCIMMainNavComponent",
        "version": "1.0.0"
    },
    "setter": {
        "width": 180,
        "height": 64
    },
    "label": {
        "label_using": "N",
        "label_text": "DCIMMainNavComponent"
    },
    "dashboard": {
        "selectType": "popup",
        "selectPage": "",
        "popupOption": ""
    }
});
DCIMMainNavComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "page-select-input",
    owner: "dashboard",
    label: "Dashboard"
}]; //  추후 추가 예정

DCIMMainNavComponent.method_info = [];
WVPropertyManager.remove_event(DCIMMainNavComponent, "dblclick");
WVPropertyManager.remove_event(DCIMMainNavComponent, "click"); // 이벤트 정보

WVPropertyManager.add_event(DCIMMainNavComponent, {
    name: "select",
    label: "Button Click Event",
    description: "Button Click Event",
    properties: [{
        name: "name",
        type: "Number",
        default: "",
        description: "Clicked button name"
    }]
});
