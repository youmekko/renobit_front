class DCIMMainNavComponent extends WVDOMComponent {
    constructor() {
        super();
        this.app = null;
    }

    onLoadPage() {}

    _onCreateElement() {
        this.createVueElement();
    }

    onOpenPage() {
        if (!this.isEditorMode && this.app) {
            this.app.opendPage();
        }
    }

    createVueElement() {
        var str = `
                <ul class="dcim-main-nav-component">
                    <template v-if="!isEditorMode">
                        <li class="icon-site" v-if="!isSiteType"><a href="#" @click.prevent="onClickSiteBtn">
                            <img class="up" :src="imagePath + 'icon_site.png'">
                            <img class="hover" :src="imagePath + 'icon_site_hover.png'">
                        </a></li>
                        <li class="icon-dashboard"><a href="#" @click.prevent="onClickDasbboardBtn()">
                            <img class="up" :src="imagePath + 'icon_dashboard.png'">
                            <img class="hover" :src="imagePath + 'icon_dashboard_hover.png'">
                        </a></li>
                        <li class="icon-position"><a href="#" @click.prevent="onClickInitPositionBtn()">
                            <img class="up" :src="imagePath + 'icon_position.png'">
                            <img class="hover" :src="imagePath + 'icon_position_hover.png'">
                        </a></li>
                    </template>
                    <template v-else>
                        <li class="icon-site"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_site.png'">
                            <img class="hover" :src="imagePath + 'icon_site_hover.png'">
                        </a></li>
                        <li class="icon-dashboard"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_dashboard.png'">
                            <img class="hover" :src="imagePath + 'icon_dashboard_hover.png'">
                        </a></li>
                        <li class="icon-position"><a href="#" @click.prevent="">
                            <img class="up" :src="imagePath + 'icon_position.png'">
                            <img class="hover" :src="imagePath + 'icon_position_hover.png'">
                        </a></li>
                    </template>
                </ul>
            `;

        let $temp = $(str);
        let self = this;
        let isEditorMode = this.isEditorMode;
        let imagePath = DCIMManager.PACK_PATH + this.componentName + "/resource/";
        $(this._element).append($temp);
        this.app = new Vue({
            el: $temp.get(0),
            data: function() {
                return {
                    lang: Vue.$i18n.messages.wv.dcim_pack,
                    imagePath: imagePath,
                    comp: self,
                    isEditorMode: isEditorMode,
                    assetWatchOn: false,
                    pagesetManager: window.wemb.dcimManager.pagesetManager,
                    isRoomType: true,
                    isSiteType: false,
                    isAnimate: false
                }
            },

            created() {
                if (!this.isEditorMode) {
                    this.opendPage();
                }

            },


            mounted: function() {},

            methods: {
                opendPage() {
                    if (this.isEditorMode) {
                        return;
                    }
                    let pageId = window.wemb.pageManager.currentPageInfo.id;
                    let parents = this.pagesetManager.getParents(pageId);

                    this.isRoomType = parents.length == 4;
                    let siteData = this.pagesetManager.getSite();
                    this.isSiteType = siteData && siteData.id == pageId;
                },

                onClickInitPositionBtn() {
                    if (this.isAnimate) {
                        return;
                    }
                    this.isAnimate = true;

                    try {
                        window.wemb.dcimManager.actionController.resetController();
                        this.isAnimate = false;
                    } catch (error) {
                        window.wemb.mainControls.transitionFocusInTarget(wemb.mainControls.position0.clone(), wemb.mainControls.target0.clone(), 1, () => {
                            this.isAnimate = false;
                        })
                    }
                    //wemb.viewerFacade.sendNotification(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION);
                    wemb.$globalBus.$emit(DCIMManager.NOTI_SELECT_INIT_CAMERA_POSITION);
                },


                onClickDasbboardBtn() {
                    let dashboardType = self.getGroupPropertyValue("dashboard", "selectType");
                    let dashboardPage = self.getGroupPropertyValue("dashboard", "selectPage");
                    let dashboardPopupOption = self.getGroupPropertyValue("dashboard", "popupOption");

                    if (dashboardPage !== "") {
                        if (dashboardType === 'link') {
                            wemb.pageManager.openPageById(dashboardPage);
                        } else {
                            let pageName = wemb.pageManager.getPageNameById(dashboardPage);
                            var options = {};
                            if (dashboardPopupOption !== "") {
                                try {
                                    options = JSON.parse(dashboardPopupOption);
                                } catch (error) {}
                            }

                            wemb.popupManager.open(pageName, options);
                        }
                    }
                },

                onClickSiteBtn() {
                    try {
                        let siteData = this.pagesetManager.getSite();
                        let validPageId = window.wemb.pageManager.getPageInfoBy(siteData.id).id;
                        window.wemb.pageManager.openPageById(validPageId);
                    } catch (error) {
                        Vue.$message.error(this.lang.mainNav.checkSiteInfo)
                    }
                }
            }
        });
    }

}

WVPropertyManager.attach_default_component_infos(DCIMMainNavComponent, {
    "info": {
        "componentName": "DCIMMainNavComponent",
        "version": "1.0.0"
    },

    "setter": {
        "width": 180,
        "height": 64
    },

    "label": {
        "label_using": "N",
        "label_text": "DCIMMainNavComponent"
    },

    "dashboard": {
        "selectType": "popup",
        "selectPage": "",
        "popupOption": ""
    }
});

DCIMMainNavComponent.property_panel_info = [{
    template: "primary"
}, {
    template: "pos-size-2d"
}, {
    template: "page-select-input",
    owner: "dashboard",
    label: "Dashboard"
}];

//  추후 추가 예정
DCIMMainNavComponent.method_info = [];
WVPropertyManager.remove_event(DCIMMainNavComponent, "dblclick");
WVPropertyManager.remove_event(DCIMMainNavComponent, "click");

// 이벤트 정보
WVPropertyManager.add_event(DCIMMainNavComponent, {
    name: "select",
    label: "Button Click Event",
    description: "Button Click Event",
    properties: [{
        name: "name",
        type: "Number",
        default: "",
        description: "Clicked button name"
    }]
});
