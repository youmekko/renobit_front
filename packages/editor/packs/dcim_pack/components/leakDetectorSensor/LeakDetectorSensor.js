class LeakDetectorSensor extends WV3DAssetComponent {

      constructor() {
            super();
      }

      _onCreateProperties() {
            super._onCreateProperties();
            this._points = [];
            this._leakPoints = [];
            this._controlChangeHandler = this._onChangeControlHandler.bind(this);
            this._controlClickHandler = this._onClickControlHandler.bind(this);
            this._controls = null;
      }

      _onCreateElement() {
            super._onCreateElement();
            if (this.isEditorMode) {
                  let boxSize = 10;
                  this._pointerHolder = new THREE.Object3D();
                  this._pointerHolder.frustumCulled = false;
                  this._boxPointerGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
                  this._boxPointerMaterial = new THREE.MeshBasicMaterial({color: 0x0000ff});
                  this.appendElement.add(this._pointerHolder);
            }
      }

      _onCommitProperties() {
            super._onCommitProperties();
            if (this._updatePropertiesMap.has("syncPoint.point") || this._updatePropertiesMap.has("setter.radius") || this._updatePropertiesMap.has("setter.pointCount")) {
                  this.validateCallLater(this.updateSplineOutline);
            }
            if (this._updatePropertiesMap.has("syncPoint.scale")) {
                  this.validateCallLater(this.updateControlPoints);
            }
      }


      serializeMetaProperties() {
            let properties = super.serializeMetaProperties();
            let pointer = [];
            for (let i = 1; i < this._points.length; i++) {
                  pointer.push(this._points[i]);
            }
            properties.props.points = JSON.stringify(pointer);
            return properties;
      }


      _onChangeControlHandler() {
            if (this._controls.object.name.indexOf(LeakDetectorSensor.CONTROL_NAME) > -1) {
                  this.updateSplineOutline();
            }
      }

      _onClickControlHandler(e) {
            e.stopPropagation();
            e.origDomEvent.stopPropagation();
            this._controls.attach(e.target);
            this._controls.showY = false;
      }

      updateControlPoints() {
            let point;
            let scale = this.getGroupPropertyValue("syncPoint", "scale");
            for (let i = 0; i < this._pointerHolder.children.length; i++) {
                  point = this._pointerHolder.children[i];
                  point.scale.set(scale, scale, scale);
            }

      }

      updateSplineOutline() {
            let gradientLine = this.appendElement.getObjectByName("gradientLine");
            let splineLine = this.appendElement.getObjectByName("splineLine");
            let max = this._spline.arcLengthDivisions - 1;
            let t, p, i;
            if (splineLine != null) {
                  for ( i = 0; i < this._spline.arcLengthDivisions; i++) {
                        p = splineLine.geometry.vertices[i];
                        t = i / max;
                        this._spline.getPoint(t, p);
                  }
                  splineLine.geometry.verticesNeedUpdate = true;
            }
            if (gradientLine != null) {
                  gradientLine.geometry.dispose();
                  gradientLine.geometry = this.getGeometry();
                  gradientLine.geometry.attributes.position.needsUpdate = true;
            }
      }

      get radius() {
            return this.getGroupPropertyValue("setter", "radius");
      }

      set radius(value) {
            if (this._checkUpdateGroupPropertyValue("setter", "radius", value)) {
            }
      }

      get pointCount() {
            return this.getGroupPropertyValue("setter", "pointCount");
      }

      set pointCount(value) {
            // 시작점인 index 0 은 제외
            let currentCount = this.pointCount;
            let count = Math.max(this.getDefaultProperties().setter.pointCount, value);
            if (this._checkUpdateGroupPropertyValue("setter", "pointCount", count)) {
                  let idx = 0;
                  if (count != currentCount) {
                        if (count > currentCount) {
                              count -= currentCount;
                              while (idx < count) {
                                    this.insertPoint();
                                    idx++;
                              }
                        } else {
                              idx = currentCount;
                              while (idx > count) {
                                    this.removePoint();
                                    idx--;
                              }
                        }
                  }
            }
            //this._spline.arcLengthDivisions = count;
      }


      insertPoint(point) {
            let max = this.pointCount;
            let insert;
            if (max > 3) {
                  let lastIndex = this._points.length - 1;
                  let prevIndex = lastIndex - 1;
                  let prevPoint = this._points[prevIndex].clone();
                  let endPoint = this._points[lastIndex];
                  insert = this.addPointer(point || prevPoint.lerp(endPoint, 0.5));
                  this._points.splice(lastIndex, 0, insert.position);
            } else {
                  insert = this.addPointer(point || new THREE.Vector3(1 * 100 - 100, 0, 1 * 100 - 100));
                  this._points.push(insert.position);
            } // 컨트롤 추가를 위한 훅 호출
            return insert;
      }

      removePoint(point) {
            let nIndex = -1;
            let rPoint = point;
            if (point != null) {
                  nIndex = this._points.indexOf(point);
            } else {
                  nIndex = this._points.length - 2;
            }
            if (nIndex > 0) {
                  rPoint = this._points.splice(nIndex, 1)[0];
                  this.removePointer(rPoint);
            }
            return rPoint
      }


      addPointer(point) {
            let pointer = new THREE.Mesh(this._boxPointerGeometry, this._boxPointerMaterial);
            let pointerScale = this.getGroupPropertyValue("syncPoint", "scale");
            pointer.position.set(point.x, point.y, point.z);
            pointer.scale.set(pointerScale, pointerScale, pointerScale);
            pointer.name = LeakDetectorSensor.CONTROL_NAME + this._pointerHolder.children.length;
            if (this.isEditorMode) {
                  this._domEvents.addEventListener(pointer, "click", this._controlClickHandler, false);
                  this._pointerHolder.add(pointer);
            }
            return pointer;
      }

      removePointer(point) {
            let max = this._pointerHolder.children.length;
            for (let idx = 0; idx < max; idx++) {
                  let pointer = this._pointerHolder.children[idx];
                  if (pointer.position.equals(point)) {
                        if (this.isEditorMode) {
                              this._domEvents.removeEventListener(pointer, "click", this._controlClickHandler, false);
                              this._pointerHolder.remove(pointer);
                        }
                        return;
                  }
            }
      }

      _createDefaultPoints() {
            var arr = [
                  new THREE.Vector3(100, 0, 0),
                  new THREE.Vector3(100, 0, -100),
                  new THREE.Vector3(0, 0, -100)
            ];
            return arr;
      }

      initSpline() {
            let geometry = new THREE.Geometry();
            for (var i = 0; i < this._spline.arcLengthDivisions; i++) {
                  geometry.vertices.push(new THREE.Vector3);
            }
            let outline = new THREE.Line(geometry, new THREE.LineBasicMaterial({
                  color: 0xff0000,
                  opacity: 1,
                  linewidth: 1
            }));
            outline.name = "splineLine";
            this.appendElement.add(outline);
      }

      generateTexture() {
            let size = 124;
            let canvas = document.createElement('canvas');
            canvas.width = size;
            canvas.height = size;
            let context = canvas.getContext('2d');
            context.rect(0, 0, size, size);
            let gradient = context.createLinearGradient(size / 2, size / 2, size / 2 + Math.cos(Math.PI / 2) * size / 2, size / 2 + Math.sin(Math.PI / 2) * size / 2);
            gradient.addColorStop(0, 'rgba(0, 200, 255, 0.3)'); // light blue
            gradient.addColorStop(1, 'rgba(0, 112, 255, 0.6)'); // dark blue
            context.fillStyle = gradient;
            context.fill();
            return canvas;
      }

      getGeometry() {

            console.log("geometry", this.radius );
            return new THREE.TubeBufferGeometry(this._spline, this._spline.arcLengthDivisions-1, this.radius, 2, false);
      }

      initMeshLine() {
            // texture 상용 시 빛에 따른 컬러 변경이 생김 그냥 color값만 적용
            let material = new THREE.MeshLambertMaterial({color: "#00ccff", transparent: true, opacity: 0.5});
            let geometry = this.getGeometry();
            let gradientLine = new THREE.Mesh(geometry, material);
            gradientLine.name = "gradientLine";
            gradientLine.renderOrder = 2;
            this.appendElement.add(gradientLine);
      }

      initPoints() {
            this._points.unshift(new THREE.Vector3);
            let restorePoints, point, idx;
            if (this.properties.points) {
                  restorePoints = JSON.parse(this.properties.points);
            } else {
                  restorePoints = this._createDefaultPoints();
            }
            for (idx = 0; idx < restorePoints.length; idx++) {
                  point = restorePoints[idx];
                  if (this.isEditorMode) {
                        let pointer = this.addPointer(point);
                        point = pointer.position;
                  } else {
                        point = new THREE.Vector3(point.x, point.y, point.z);
                  }
                  this._points.push(point);
            }
      }

      initSplineCurve() {
            this._spline = new THREE.CatmullRomCurve3(this._points);
            this._spline.curveType = "catmullrom";
            this._spline.tension = 0.01;
            this._spline.arcLengthDivisions = 50;
      }


      getDistance() {
            let max = this._spline.points.length;
            let d = 0;
            for (let i = 1; i < max; i++) {
                  let p1 = this._spline.points[i - 1];
                  let p2 = this._spline.points[i];
                  let dis = Math.abs(p1.distanceTo(p2));
                  d += dis;
            }
            return d;
      }


      getLeakPointFromPoint(point) {
            let searchPoint = new WV3DLabel();
            searchPoint.name = LeakDetectorSensor.LEAK_POINT_ID + this._leakPoints.length;
            searchPoint.draw(this);
            searchPoint.container.add(new THREE.Mesh(new THREE.SphereGeometry(this.radius / 4, 8, 8), new THREE.MeshBasicMaterial({
                  color: "#86E57F",
                  wireframe: true
            })));
            searchPoint.css({
                  "border-radius": "0px"
            })
            searchPoint.labelPosition = {x: 0, y: this.radius * 1.4, z: 0};
            searchPoint.position = point;
            return searchPoint;
      }

      getTotalDistance(){
            let max = this._spline.points.length;
            let d = 0;
            for (let i = 1; i < max; i++) {
                  let p1 = this._spline.points[i - 1];
                  let p2 = this._spline.points[i];
                  let dis = Math.abs(p1.distanceTo(p2));
                  d += dis;
            }
            return d;
      }

      _findPositionAtDistance(distance) {
            let max = this._spline.points.length;
            let start = 0;
            let p1, p2, dis, end;
            for (let i = 1; i < max; i++) {
                  p1 = this._spline.points[i - 1];
                  p2 = this._spline.points[i];
                  dis = Math.abs(p1.distanceTo(p2));
                  end = start + dis;
                  if (distance >= start && end > distance) {
                        let pos = end - distance;
                        let rate = pos / dis;
                        let point = p2.clone();
                        point.lerp(p1, rate);
                        let leakPoint = this.getLeakPointFromPoint(point);
                        leakPoint.text = distance;
                        this._leakPoints.push(leakPoint);
                        break;
                  }
                  start = end;
            }
      }


      clearLeakPoints() {
            let max = this._leakPoints.length;
            let label;
            for (let i = 0; i < max; i++) {
                  label = this._leakPoints[i];
                  label.clear();
            }
            this._leakPoints = [];
      }


      displayEventState(info) {
            this.toggleLeakLine(true);
            if (info.status == "Y") {
                  this._findPositionAtDistance(info.distance);
            } else {
                  this.clearLeakPoints();
            }
      }

      hideEventState(){
            this.toggleLeakLine(false);
            this.clearLeakPoints();
      }

      /* 누수라인 토글 처리 */
      toggleLeakLine(bValue) {
            let gradientLine = this.appendElement.getObjectByName("gradientLine");
            if(gradientLine){
                  gradientLine.visible = bValue;
            } else if (bValue) {
                  this.initPoints();
                  this.initSplineCurve();

                  this.initMeshLine();
            }

      }

      _validateSize() {
            super._validateSize();
            if (this._pointerHolder) {
                  this._pointerHolder.scale.set(this.scaleRate.x, this.scaleRate.y, this.scaleRate.z);
            }
      }


      _onValidateResource() {

            if(this.isEditorMode && this._points.length === 0)
            {
                  this.initPoints();
                  this.initSplineCurve();
                  this._controls = window.wemb.mainPageComponent.threeLayer._transformControls;
                  this._controls.addEventListener("objectChange", this._controlChangeHandler);
                  this.initSpline();
                  this.initMeshLine();
                  this.updateSplineOutline();
            }
            super._onValidateResource();
      }

      _onDestroy() {

            if (this.isEditorMode) {
                  let max = this._pointerHolder.children.length;
                  for (let i = 0; i < max; i++) {
                        let pointer = this._pointerHolder.children[i];
                        this._domEvents.removeEventListener(pointer, "click", this._controlClickHandler, false);
                  }
                  this._controls.removeEventListener("objectChange", this._controlChangeHandler);
                  this._controls.removeEventListener("mouseUp", this._controlMouseUpHandler);
                  this.appendElement.remove(this._pointerHolder);
                  MeshManager.disposeMesh(this._pointerHolder);
                  this._pointerHolder = null;
                  this._controls = null;
                  this._boxPointerGeometry.dispose();
                  this._boxPointerMaterial.dispose();
                  this._boxPointerMaterial = null;
                  this._boxPointerGeometry = null;
            }
            this._points = [];
            this._spline = null;
            super._onDestroy();
      }




}

LeakDetectorSensor.LEAK_POINT_ID = "leak_point";
LeakDetectorSensor.CONTROL_NAME = "control_point_";
WV3DPropertyManager.attach_default_component_infos(LeakDetectorSensor, {
      "setter": {
            "pointCount": 3,
            "radius": 10
      },

      "label": {
            "label_text": "LeakDetectorSensor",
            "label_line_size": 15,
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "syncPoint": {
            "point": "",
            "scale": 1
      },
      "info": {
            "componentName": "LeakDetectorSensor",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});


//WVPropertyManager.removePropertyGroupByName(LeakDetectorSensor.property_panel_info, "label", "color");
WVPropertyManager.removePropertyGroupByName(LeakDetectorSensor.property_panel_info, "label", "Original Size");


WVPropertyManager.add_property_panel_group_info(LeakDetectorSensor, {
      template: "dcim-asset-id"
})

WVPropertyManager.add_property_panel_group_info(LeakDetectorSensor, {
      template: 'dcim-asset-popup'
})


WVPropertyManager.add_property_panel_group_info(LeakDetectorSensor, {
      label: "Radius",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "radius",
            type: "number",
            label: "Radius",
            show: true,
            writable: true,
            description: "radius"
      }]
});


WVPropertyManager.add_property_panel_group_info(LeakDetectorSensor, {
      label: "controlPoint",
      template: "vector",
      children: [{
            owner: "syncPoint",
            name: "point",
            type: "object",
            label: "Length",
            show: true,
            writable: true,
            description: "point"
      }]
});

WVPropertyManager.add_property_group_info(LeakDetectorSensor, {
      label: "Setting Info",
      name: "pointCount",
      children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "PointCount",
            show: true,
            writable: true,
            defaultValue: 3,
            description: "Set the leak sensor control point."
      }, {
            owner: "setter",
            name: "radius",
            type: "number",
            label: "Radius",
            show: true,
            writable: true,
            defaultValue: 10,
            description: "Sets the radius of the line drawn when a leak occurs."
      }]
});

// 사이즈 정보 writable을 false로 설정
let sizeInfo = WVPropertyManager.getPropertyGroupChildrenByName(LeakDetectorSensor.property_panel_info, "display", "size");
sizeInfo.writable = false;

WVPropertyManager.removePropertyGroupChildrenByName(LeakDetectorSensor.property_info, "display", "size");
