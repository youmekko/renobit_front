"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var LeakDetectorSensor =
      /*#__PURE__*/
      function (_WV3DAssetComponent) {
            _inherits(LeakDetectorSensor, _WV3DAssetComponent);

            function LeakDetectorSensor() {
                  _classCallCheck(this, LeakDetectorSensor);

                  return _possibleConstructorReturn(this, _getPrototypeOf(LeakDetectorSensor).call(this));
            }

            _createClass(LeakDetectorSensor, [{
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(LeakDetectorSensor.prototype), "_onCreateProperties", this).call(this);

                        this._points = [];
                        this._leakPoints = [];
                        this._controlChangeHandler = this._onChangeControlHandler.bind(this);
                        this._controlClickHandler = this._onClickControlHandler.bind(this);
                        this._controls = null;
                  }
            }, {
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        _get(_getPrototypeOf(LeakDetectorSensor.prototype), "_onCreateElement", this).call(this);

                        if (this.isEditorMode) {
                              var boxSize = 10;
                              this._pointerHolder = new THREE.Object3D();
                              this._pointerHolder.frustumCulled = false;
                              this._boxPointerGeometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
                              this._boxPointerMaterial = new THREE.MeshBasicMaterial({
                                    color: 0x0000ff
                              });
                              this.appendElement.add(this._pointerHolder);
                        }
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        _get(_getPrototypeOf(LeakDetectorSensor.prototype), "_onCommitProperties", this).call(this);

                        if (this._updatePropertiesMap.has("syncPoint.point") || this._updatePropertiesMap.has("setter.radius") || this._updatePropertiesMap.has("setter.pointCount")) {
                              this.validateCallLater(this.updateSplineOutline);
                        }

                        if (this._updatePropertiesMap.has("syncPoint.scale")) {
                              this.validateCallLater(this.updateControlPoints);
                        }
                  }
            }, {
                  key: "serializeMetaProperties",
                  value: function serializeMetaProperties() {
                        var properties = _get(_getPrototypeOf(LeakDetectorSensor.prototype), "serializeMetaProperties", this).call(this);

                        var pointer = [];

                        for (var i = 1; i < this._points.length; i++) {
                              pointer.push(this._points[i]);
                        }

                        properties.props.points = JSON.stringify(pointer);
                        return properties;
                  }
            }, {
                  key: "_onChangeControlHandler",
                  value: function _onChangeControlHandler() {
                        if (this._controls.object.name.indexOf(LeakDetectorSensor.CONTROL_NAME) > -1) {
                              this.updateSplineOutline();
                        }
                  }
            }, {
                  key: "_onClickControlHandler",
                  value: function _onClickControlHandler(e) {
                        e.stopPropagation();
                        e.origDomEvent.stopPropagation();

                        this._controls.attach(e.target);

                        this._controls.showY = false;
                  }
            }, {
                  key: "updateControlPoints",
                  value: function updateControlPoints() {
                        var point;
                        var scale = this.getGroupPropertyValue("syncPoint", "scale");

                        for (var i = 0; i < this._pointerHolder.children.length; i++) {
                              point = this._pointerHolder.children[i];
                              point.scale.set(scale, scale, scale);
                        }
                  }
            }, {
                  key: "updateSplineOutline",
                  value: function updateSplineOutline() {
                        var gradientLine = this.appendElement.getObjectByName("gradientLine");
                        var splineLine = this.appendElement.getObjectByName("splineLine");
                        var max = this._spline.arcLengthDivisions - 1;
                        var t, p, i;

                        if (splineLine != null) {
                              for (i = 0; i < this._spline.arcLengthDivisions; i++) {
                                    p = splineLine.geometry.vertices[i];
                                    t = i / max;

                                    this._spline.getPoint(t, p);
                              }

                              splineLine.geometry.verticesNeedUpdate = true;
                        }

                        if (gradientLine != null) {
                              gradientLine.geometry.dispose();
                              gradientLine.geometry = this.getGeometry();
                              gradientLine.geometry.attributes.position.needsUpdate = true;
                        }
                  }
            }, {
                  key: "insertPoint",
                  value: function insertPoint(point) {
                        var max = this.pointCount;
                        var insert;

                        if (max > 3) {
                              var lastIndex = this._points.length - 1;
                              var prevIndex = lastIndex - 1;

                              var prevPoint = this._points[prevIndex].clone();

                              var endPoint = this._points[lastIndex];
                              insert = this.addPointer(point || prevPoint.lerp(endPoint, 0.5));

                              this._points.splice(lastIndex, 0, insert.position);
                        } else {
                              insert = this.addPointer(point || new THREE.Vector3(1 * 100 - 100, 0, 1 * 100 - 100));

                              this._points.push(insert.position);
                        } // 컨트롤 추가를 위한 훅 호출


                        return insert;
                  }
            }, {
                  key: "removePoint",
                  value: function removePoint(point) {
                        var nIndex = -1;
                        var rPoint = point;

                        if (point != null) {
                              nIndex = this._points.indexOf(point);
                        } else {
                              nIndex = this._points.length - 2;
                        }

                        if (nIndex > 0) {
                              rPoint = this._points.splice(nIndex, 1)[0];
                              this.removePointer(rPoint);
                        }

                        return rPoint;
                  }
            }, {
                  key: "addPointer",
                  value: function addPointer(point) {
                        var pointer = new THREE.Mesh(this._boxPointerGeometry, this._boxPointerMaterial);
                        var pointerScale = this.getGroupPropertyValue("syncPoint", "scale");
                        pointer.position.set(point.x, point.y, point.z);
                        pointer.scale.set(pointerScale, pointerScale, pointerScale);
                        pointer.name = LeakDetectorSensor.CONTROL_NAME + this._pointerHolder.children.length;

                        if (this.isEditorMode) {
                              this._domEvents.addEventListener(pointer, "click", this._controlClickHandler, false);

                              this._pointerHolder.add(pointer);
                        }

                        return pointer;
                  }
            }, {
                  key: "removePointer",
                  value: function removePointer(point) {
                        var max = this._pointerHolder.children.length;

                        for (var idx = 0; idx < max; idx++) {
                              var pointer = this._pointerHolder.children[idx];

                              if (pointer.position.equals(point)) {
                                    if (this.isEditorMode) {
                                          this._domEvents.removeEventListener(pointer, "click", this._controlClickHandler, false);

                                          this._pointerHolder.remove(pointer);
                                    }

                                    return;
                              }
                        }
                  }
            }, {
                  key: "_createDefaultPoints",
                  value: function _createDefaultPoints() {
                        var arr = [new THREE.Vector3(100, 0, 0), new THREE.Vector3(100, 0, -100), new THREE.Vector3(0, 0, -100)];
                        return arr;
                  }
            }, {
                  key: "initSpline",
                  value: function initSpline() {
                        var geometry = new THREE.Geometry();

                        for (var i = 0; i < this._spline.arcLengthDivisions; i++) {
                              geometry.vertices.push(new THREE.Vector3());
                        }

                        var outline = new THREE.Line(geometry, new THREE.LineBasicMaterial({
                              color: 0xff0000,
                              opacity: 1,
                              linewidth: 1
                        }));
                        outline.name = "splineLine";
                        this.appendElement.add(outline);
                  }
            }, {
                  key: "generateTexture",
                  value: function generateTexture() {
                        var size = 124;
                        var canvas = document.createElement('canvas');
                        canvas.width = size;
                        canvas.height = size;
                        var context = canvas.getContext('2d');
                        context.rect(0, 0, size, size);
                        var gradient = context.createLinearGradient(size / 2, size / 2, size / 2 + Math.cos(Math.PI / 2) * size / 2, size / 2 + Math.sin(Math.PI / 2) * size / 2);
                        gradient.addColorStop(0, 'rgba(0, 200, 255, 0.3)'); // light blue

                        gradient.addColorStop(1, 'rgba(0, 112, 255, 0.6)'); // dark blue

                        context.fillStyle = gradient;
                        context.fill();
                        return canvas;
                  }
            }, {
                  key: "getGeometry",
                  value: function getGeometry() {
                        console.log("geometry", this.radius);
                        return new THREE.TubeBufferGeometry(this._spline, this._spline.arcLengthDivisions - 1, this.radius, 2, false);
                  }
            }, {
                  key: "initMeshLine",
                  value: function initMeshLine() {
                        // texture 상용 시 빛에 따른 컬러 변경이 생김 그냥 color값만 적용
                        var material = new THREE.MeshLambertMaterial({
                              color: "#00ccff",
                              transparent: true,
                              opacity: 0.5
                        });
                        var geometry = this.getGeometry();
                        var gradientLine = new THREE.Mesh(geometry, material);
                        gradientLine.name = "gradientLine";
                        gradientLine.renderOrder = 2;
                        this.appendElement.add(gradientLine);
                  }
            }, {
                  key: "initPoints",
                  value: function initPoints() {
                        this._points.unshift(new THREE.Vector3());

                        var restorePoints, point, idx;

                        if (this.properties.points) {
                              restorePoints = JSON.parse(this.properties.points);
                        } else {
                              restorePoints = this._createDefaultPoints();
                        }

                        for (idx = 0; idx < restorePoints.length; idx++) {
                              point = restorePoints[idx];

                              if (this.isEditorMode) {
                                    var pointer = this.addPointer(point);
                                    point = pointer.position;
                              } else {
                                    point = new THREE.Vector3(point.x, point.y, point.z);
                              }

                              this._points.push(point);
                        }
                  }
            }, {
                  key: "initSplineCurve",
                  value: function initSplineCurve() {
                        this._spline = new THREE.CatmullRomCurve3(this._points);
                        this._spline.curveType = "catmullrom";
                        this._spline.tension = 0.01;
                        this._spline.arcLengthDivisions = 50;
                  }
            }, {
                  key: "getDistance",
                  value: function getDistance() {
                        var max = this._spline.points.length;
                        var d = 0;

                        for (var i = 1; i < max; i++) {
                              var p1 = this._spline.points[i - 1];
                              var p2 = this._spline.points[i];
                              var dis = Math.abs(p1.distanceTo(p2));
                              d += dis;
                        }

                        return d;
                  }
            }, {
                  key: "getLeakPointFromPoint",
                  value: function getLeakPointFromPoint(point) {
                        var searchPoint = new WV3DLabel();
                        searchPoint.name = LeakDetectorSensor.LEAK_POINT_ID + this._leakPoints.length;
                        searchPoint.draw(this);
                        searchPoint.container.add(new THREE.Mesh(new THREE.SphereGeometry(this.radius / 4, 8, 8), new THREE.MeshBasicMaterial({
                              color: "#86E57F",
                              wireframe: true
                        })));
                        searchPoint.css({
                              "border-radius": "0px"
                        });
                        searchPoint.labelPosition = {
                              x: 0,
                              y: this.radius * 1.4,
                              z: 0
                        };
                        searchPoint.position = point;
                        return searchPoint;
                  }
            }, {
                  key: "getTotalDistance",
                  value: function getTotalDistance() {
                        var max = this._spline.points.length;
                        var d = 0;

                        for (var i = 1; i < max; i++) {
                              var p1 = this._spline.points[i - 1];
                              var p2 = this._spline.points[i];
                              var dis = Math.abs(p1.distanceTo(p2));
                              d += dis;
                        }

                        return d;
                  }
            }, {
                  key: "_findPositionAtDistance",
                  value: function _findPositionAtDistance(distance) {
                        var max = this._spline.points.length;
                        var start = 0;
                        var p1, p2, dis, end;

                        for (var i = 1; i < max; i++) {
                              p1 = this._spline.points[i - 1];
                              p2 = this._spline.points[i];
                              dis = Math.abs(p1.distanceTo(p2));
                              end = start + dis;

                              if (distance >= start && end > distance) {
                                    var pos = end - distance;
                                    var rate = pos / dis;
                                    var point = p2.clone();
                                    point.lerp(p1, rate);
                                    var leakPoint = this.getLeakPointFromPoint(point);
                                    leakPoint.text = distance;

                                    this._leakPoints.push(leakPoint);

                                    break;
                              }

                              start = end;
                        }
                  }
            }, {
                  key: "clearLeakPoints",
                  value: function clearLeakPoints() {
                        var max = this._leakPoints.length;
                        var label;

                        for (var i = 0; i < max; i++) {
                              label = this._leakPoints[i];
                              label.clear();
                        }

                        this._leakPoints = [];
                  }
            }, {
                  key: "displayEventState",
                  value: function displayEventState(info) {
                        this.toggleLeakLine(true);

                        if (info.status == "Y") {
                              this._findPositionAtDistance(info.distance);
                        } else {
                              this.clearLeakPoints();
                        }
                  }
            }, {
                  key: "hideEventState",
                  value: function hideEventState() {
                        this.toggleLeakLine(false);
                        this.clearLeakPoints();
                  }
                  /* 누수라인 토글 처리 */

            }, {
                  key: "toggleLeakLine",
                  value: function toggleLeakLine(bValue) {
                        var gradientLine = this.appendElement.getObjectByName("gradientLine");

                        if (gradientLine) {
                              gradientLine.visible = bValue;
                        } else if (bValue) {
                              this.initPoints();
                              this.initSplineCurve();

                              this.initMeshLine();
                        }
                  }
            }, {
                  key: "_validateSize",
                  value: function _validateSize() {
                        _get(_getPrototypeOf(LeakDetectorSensor.prototype), "_validateSize", this).call(this);

                        if (this._pointerHolder) {
                              this._pointerHolder.scale.set(this.scaleRate.x, this.scaleRate.y, this.scaleRate.z);
                        }
                  }
            }, {
                  key: "_onValidateResource",
                  value: function _onValidateResource() {
                        if (this.isEditorMode && this._points.length === 0) {
                              this.initPoints();
                              this.initSplineCurve();
                              this._controls = window.wemb.mainPageComponent.threeLayer._transformControls;

                              this._controls.addEventListener("objectChange", this._controlChangeHandler);

                              this.initSpline();
                              this.initMeshLine();
                              this.updateSplineOutline();
                        }

                        _get(_getPrototypeOf(LeakDetectorSensor.prototype), "_onValidateResource", this).call(this);
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        if (this.isEditorMode) {
                              var max = this._pointerHolder.children.length;

                              for (var i = 0; i < max; i++) {
                                    var pointer = this._pointerHolder.children[i];

                                    this._domEvents.removeEventListener(pointer, "click", this._controlClickHandler, false);
                              }

                              this._controls.removeEventListener("objectChange", this._controlChangeHandler);

                              this._controls.removeEventListener("mouseUp", this._controlMouseUpHandler);

                              this.appendElement.remove(this._pointerHolder);
                              MeshManager.disposeMesh(this._pointerHolder);
                              this._pointerHolder = null;
                              this._controls = null;

                              this._boxPointerGeometry.dispose();

                              this._boxPointerMaterial.dispose();

                              this._boxPointerMaterial = null;
                              this._boxPointerGeometry = null;
                        }

                        this._points = [];
                        this._spline = null;

                        _get(_getPrototypeOf(LeakDetectorSensor.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "radius",
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "radius");
                  },
                  set: function set(value) {
                        if (this._checkUpdateGroupPropertyValue("setter", "radius", value)) {}
                  }
            }, {
                  key: "pointCount",
                  get: function get() {
                        return this.getGroupPropertyValue("setter", "pointCount");
                  },
                  set: function set(value) {
                        // 시작점인 index 0 은 제외
                        var currentCount = this.pointCount;
                        var count = Math.max(this.getDefaultProperties().setter.pointCount, value);

                        if (this._checkUpdateGroupPropertyValue("setter", "pointCount", count)) {
                              var idx = 0;

                              if (count != currentCount) {
                                    if (count > currentCount) {
                                          count -= currentCount;

                                          while (idx < count) {
                                                this.insertPoint();
                                                idx++;
                                          }
                                    } else {
                                          idx = currentCount;

                                          while (idx > count) {
                                                this.removePoint();
                                                idx--;
                                          }
                                    }
                              }
                        } //this._spline.arcLengthDivisions = count;

                  }
            }]);

            return LeakDetectorSensor;
      }(WV3DAssetComponent);

LeakDetectorSensor.LEAK_POINT_ID = "leak_point";
LeakDetectorSensor.CONTROL_NAME = "control_point_";
WV3DPropertyManager.attach_default_component_infos(LeakDetectorSensor, {
      "setter": {
            "pointCount": 3,
            "radius": 10
      },
      "label": {
            "label_text": "LeakDetectorSensor",
            "label_line_size": 15,
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "syncPoint": {
            "point": "",
            "scale": 1
      },
      "info": {
            "componentName": "LeakDetectorSensor",
            "version": "1.0.0"
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      }
}); //WVPropertyManager.removePropertyGroupByName(LeakDetectorSensor.property_panel_info, "label", "color");

WVPropertyManager.removePropertyGroupByName(LeakDetectorSensor.property_panel_info, "label", "Original Size");
WVPropertyManager.add_property_panel_group_info(LeakDetectorSensor, {
      template: "dcim-asset-id"
});
WVPropertyManager.add_property_panel_group_info(LeakDetectorSensor, {
      template: 'dcim-asset-popup'
});
WVPropertyManager.add_property_panel_group_info(LeakDetectorSensor, {
      label: "Radius",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "radius",
            type: "number",
            label: "Radius",
            show: true,
            writable: true,
            description: "radius"
      }]
});
WVPropertyManager.add_property_panel_group_info(LeakDetectorSensor, {
      label: "controlPoint",
      template: "vector",
      children: [{
            owner: "syncPoint",
            name: "point",
            type: "object",
            label: "Length",
            show: true,
            writable: true,
            description: "point"
      }]
});
WVPropertyManager.add_property_group_info(LeakDetectorSensor, {
      label: "Setting Info",
      name: "pointCount",
      children: [{
            owner: "setter",
            name: "pointCount",
            type: "number",
            label: "PointCount",
            show: true,
            writable: true,
            defaultValue: 3,
            description: "Set the leak sensor control point."
      }, {
            owner: "setter",
            name: "radius",
            type: "number",
            label: "Radius",
            show: true,
            writable: true,
            defaultValue: 10,
            description: "Sets the radius of the line drawn when a leak occurs."
      }]
}); // 사이즈 정보 writable을 false로 설정

var sizeInfo = WVPropertyManager.getPropertyGroupChildrenByName(LeakDetectorSensor.property_panel_info, "display", "size");
sizeInfo.writable = false;
WVPropertyManager.removePropertyGroupChildrenByName(LeakDetectorSensor.property_info, "display", "size");
