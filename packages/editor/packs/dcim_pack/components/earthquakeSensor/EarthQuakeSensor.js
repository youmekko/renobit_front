class EarthQuakeSensor extends WV3DAssetComponent{
      constructor(){ super(); }

      hasAssetIcon() {
            return true;
      }

}


WV3DPropertyManager.attach_default_component_infos(EarthQuakeSensor, {

      "label": {
            "label_text": "EarthQuakeSensor",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "EarthQuakeSensor",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});



WVPropertyManager.add_property_panel_group_info(EarthQuakeSensor, {
      template: "dcim-asset-id"
})

WVPropertyManager.add_property_panel_group_info(EarthQuakeSensor, {
      template: 'dcim-asset-popup'
})
