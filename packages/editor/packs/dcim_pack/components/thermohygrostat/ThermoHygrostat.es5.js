"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ThermoHygrostat = function (_WV3DAssetComponent) {
      _inherits(ThermoHygrostat, _WV3DAssetComponent);

      function ThermoHygrostat() {
            _classCallCheck(this, ThermoHygrostat);

            return _possibleConstructorReturn(this, (ThermoHygrostat.__proto__ || Object.getPrototypeOf(ThermoHygrostat)).call(this));
      }

      return ThermoHygrostat;
}(WV3DAssetComponent);

WV3DPropertyManager.attach_default_component_infos(ThermoHygrostat, {
      "label": {
            "label_using": "N",
            "line_connect": false,
            "y": 10
      },
      "info": {
            "componentName": "ThermoHygrostat",
            "version": "1.0.0"
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(ThermoHygrostat, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(ThermoHygrostat, {
      template: 'dcim-asset-popup'
});

WVPropertyManager.removePropertyGroupByName(ThermoHygrostat.property_panel_info, "label", "label");
WVPropertyManager.removePropertyGroupByName(ThermoHygrostat.property_info, "label", "Label Attributes");
