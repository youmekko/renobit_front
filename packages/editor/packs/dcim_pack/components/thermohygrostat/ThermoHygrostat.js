class ThermoHygrostat extends WV3DAssetComponent {

      constructor(){ super(); }

}


WV3DPropertyManager.attach_default_component_infos(ThermoHygrostat, {
      "label": {
            "label_using": "N",
            "line_connect":false,
            "y":10
      },
      "info": {
            "componentName": "ThermoHygrostat",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(ThermoHygrostat, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(ThermoHygrostat, {
      template: 'dcim-asset-popup'
});

WVPropertyManager.removePropertyGroupByName(ThermoHygrostat.property_panel_info, "label", "label");
WVPropertyManager.removePropertyGroupByName(ThermoHygrostat.property_info, "label", "Label Attributes");
