"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Storage = function (_WV3DAssetComponent) {
      _inherits(Storage, _WV3DAssetComponent);

      function Storage() {
            _classCallCheck(this, Storage);

            return _possibleConstructorReturn(this, (Storage.__proto__ || Object.getPrototypeOf(Storage)).call(this));
      }

      return Storage;
}(WV3DAssetComponent);

WV3DPropertyManager.attach_default_component_infos(Storage, {

      "label": {
            "label_text": "Storage",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "Storage",
            "version": "1.0.0"
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(Storage, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(Storage, {
      template: 'dcim-asset-popup'
});
