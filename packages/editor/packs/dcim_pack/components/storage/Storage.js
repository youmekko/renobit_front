class Storage extends WV3DAssetComponent {
      constructor(){super();}
}

WV3DPropertyManager.attach_default_component_infos(Storage, {

      "label": {
            "label_text": "Storage",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "Storage",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(Storage, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(Storage, {
      template: 'dcim-asset-popup'
});
