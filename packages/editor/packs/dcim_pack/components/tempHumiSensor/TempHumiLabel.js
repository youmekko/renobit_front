class TempHumiLabel extends WV3DLabel {

      constructor(){
            super();
      }

      _createElements() {
            let template       =    '<div id="temp-humi-label">'+
                                          '<span class="temp label"></span>'+
                                          '<span class="humi label"></span>'+
                                    '</div>';
            let labelContainer = $(template);
            $(labelContainer).css({
                  "position" : "absolute",
                  "pointer-events":"none",
                  "width"    : "140px",
                  "overflow" : "hidden",
                  "border-radius":"10px"
            });

            let labels =$(labelContainer).find(".label");
            labels.css({
                  "width"           : "70px",
                  "height"          : "26px",
                  "display"         : "inline-block",
                  "line-height"     : "26px",
                  "padding"         : "2px 0 2px 6px"
            });

            this.label = new THREE.CSS2DObject(labelContainer[0]);
            this.container.add(this.label);
            this.tempLabel = $(labelContainer).find(".temp");
            this.humiLabel = $(labelContainer).find(".humi");
      }


      css( styleInfo ) {
            if(styleInfo.hasOwnProperty("temp_background_color")){
                  let tempBG = styleInfo.temp_background_color;
                  this.tempLabel.css({"background-color":tempBG });
                  delete styleInfo.temp_background_color;
            }
            super.css(styleInfo);
      }

      set tempText(str){
            this.tempLabel.html(str+"℃");
      }

      get tempText(){
            return this.tempLabel.text();
      }

      set humiText(str){
            this.humiLabel.html(str+"%");
      }

      get humiText(){
            return this.humiLabel.text();
      }


      clear(){
            this.tempLabel.empty();
            this.humiLabel.empty();
            this.humiLabel = this.tempLabel = null;
            super.clear();
      }




}
