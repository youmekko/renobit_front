"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TempHumiSensor = function (_WV3DAssetComponent) {
      _inherits(TempHumiSensor, _WV3DAssetComponent);

      function TempHumiSensor() {
            _classCallCheck(this, TempHumiSensor);

            return _possibleConstructorReturn(this, (TempHumiSensor.__proto__ || Object.getPrototypeOf(TempHumiSensor)).call(this));
      }

      _createClass(TempHumiSensor, [{
            key: "_createDomComponentLabel",
            value: function _createDomComponentLabel() {
                  this._label = new TempHumiLabel();
                  this._label.draw(this);
            }
      }, {
            key: "_validateLabel",
            value: function _validateLabel() {
                  if (!this.visible || !this.hasLabel()) return null;
                  var labelVO = this.getGroupProperties("label");
                  if (labelVO.label_using == "Y") {
                        this._label.css({
                              "color": labelVO.label_color,
                              "font-size": labelVO.label_font_size + "px",
                              "background-color": labelVO.label_background_color,
                              "font-weight": labelVO.label_font_weight,
                              "temp_background_color": labelVO.temp_background_color,
                              "font-family": labelVO.label_font_type });
                        this._label.labelPosition = { x: labelVO.x * this.scaleRate.x, y: labelVO.y * this.scaleRate.y, z: 0 };
                        this._label.lineConnect = labelVO.line_connect;
                        this._label.show();
                  } else {
                        this._label.hide();
                  }
            }
      }, {
            key: "displayEventState",
            value: function displayEventState(info) {}
      }]);

      return TempHumiSensor;
}(WV3DAssetComponent);

WV3DPropertyManager.attach_default_component_infos(TempHumiSensor, {
      "label": {
            "label_using": "N",
            "line_connect": true,
            "label_text": "",
            "label_color": "#ffffff",
            "label_background_color": "#00a716",
            "temp_background_color": "#0070ff",
            "y": 10
      },
      "info": {
            "componentName": "TempHumiSensor",
            "version": "1.0.0"
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(TempHumiSensor, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(TempHumiSensor, {
      template: 'dcim-asset-popup'
});

WVPropertyManager.removePropertyGroupByName(TempHumiSensor.property_panel_info, "label", "label");
WVPropertyManager.removePropertyGroupByName(TempHumiSensor.property_info, "label", "Label Attributes ");
