class TempHumiSensor extends WV3DAssetComponent {

      constructor() { super(); }

      _createDomComponentLabel() {
            this._label = new TempHumiLabel;
            this._label.draw(this);
      }

      _validateLabel(){
            if(!this.visible || !this.hasLabel() ) return null;
            let labelVO = this.getGroupProperties("label");
            if(labelVO.label_using == "Y"){
                  this._label.css({
                        "color"           :    labelVO.label_color,
                        "font-size"       :    labelVO.label_font_size+"px",
                        "background-color":    labelVO.label_background_color,
                        "font-weight"     :    labelVO.label_font_weight,
                        "temp_background_color" : labelVO.temp_background_color,
                        "font-family"     :    labelVO.label_font_type });
                  this._label.labelPosition = {x:labelVO.x*this.scaleRate.x, y:labelVO.y*this.scaleRate.y, z:0};
                  this._label.lineConnect   = labelVO.line_connect;
                  this._label.show();
            } else {
                  this._label.hide();
            }
      }

      displayEventState(info) {

      }
}


WV3DPropertyManager.attach_default_component_infos(TempHumiSensor, {
      "label": {
            "label_using": "N",
            "line_connect":true,
            "label_text":"",
            "label_color":"#ffffff",
            "label_background_color":"#00a716",
            "temp_background_color" :"#0070ff",
            "y":10
      },
      "info": {
            "componentName": "TempHumiSensor",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(TempHumiSensor, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(TempHumiSensor, {
      template: 'dcim-asset-popup'
});

WVPropertyManager.removePropertyGroupByName(TempHumiSensor.property_panel_info, "label", "label");
WVPropertyManager.removePropertyGroupByName(TempHumiSensor.property_info, "label", "Label Attributes ");
