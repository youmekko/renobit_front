class GasSensor extends WV3DAssetComponent{
      constructor(){ super(); }
      hasAssetIcon() {
            return true;
      }
}


WV3DPropertyManager.attach_default_component_infos(GasSensor, {

      "label": {
            "label_text": "GasSensor",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "GasSensor",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});


WVPropertyManager.add_property_panel_group_info(GasSensor, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(GasSensor, {
      template: 'dcim-asset-popup'
});
