function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// 객체를 인스턴스로 생성하지 않고 코드 상에서 인스턴스처럼 다루기 위한 프록시
var MeshInstanceProxy =
      /*#__PURE__*/
      function () {
            "use strict";

            function MeshInstanceProxy(properties) {
                  _classCallCheck(this, MeshInstanceProxy);

                  this._parent = null;
                  this._position = null;
                  this._rotation = null;
                  this._color = 0xffffff;
                  this._scale = null;
                  this._props = null;
                  this._instance = null;
                  this._data = null;

                  if (properties) {
                        this.initFromProperties(properties);
                  }
            }

            _createClass(MeshInstanceProxy, [{
                  key: "initFromProperties",
                  value: function initFromProperties(info) {
                        this._props = info.props;
                        var toRadian = Math.PI / 180;
                        var rotate = this._props.setter.rotation || new THREE.Vector3();
                        var position = this._props.setter.position || new THREE.Vector3();
                        var scale = this._props.setter.size || new THREE.Vector3();
                        var elementSize = this._props.elementSize || new THREE.Vector3();
                        this._data = this._props.setter.data;
                        this.rotation = new THREE.Vector3(rotate.x * toRadian, rotate.y * toRadian, rotate.z * toRadian);
                        this.position = new THREE.Vector3(position.x, position.y, position.z);
                        this.scale = new THREE.Vector3(scale.x / elementSize.x, scale.y / elementSize.y, scale.z / elementSize.z);
                        this.selected = false;
                  }
            }, {
                  key: "getWorldPosition",
                  value: function getWorldPosition() {
                        return this.position.clone().applyMatrix4(this.parent.appendElement.matrixWorld);
                  }
            }, {
                  key: "onDestroy",
                  value: function onDestroy() {}
            }, {
                  key: "destroy",
                  value: function destroy() {
                        this.onDestroy();
                        this._parent = null;
                        this._data = null;
                  }
            }, {
                  key: "getResourceFileName",
                  value: function getResourceFileName(info) {
                        return info.substring(info.lastIndexOf("/") + 1);
                  }
            }, {
                  key: "getResourceInfo",
                  value: function getResourceInfo() {
                        var resourceInfo = this.resource.modeling;

                        if (resourceInfo) {
                              var key = this.resource.mappingKey;

                              if (this.data != null && this.data.hasOwnProperty(key)) {
                                    key = this.data[key];
                              } else {
                                    key = "default";
                              }

                              resourceInfo = resourceInfo[key];
                        } else {
                              CPLogger.log("##WV3DAssetComponent", "No resource.modeling Info");
                              return null;
                        }

                        if (resourceInfo) {
                              if (!resourceInfo.name) {
                                    resourceInfo.name = this.getResourceFileName(resourceInfo.path);
                              }

                              if (resourceInfo.mapPath && resourceInfo.mapPath.substr(-1) != "/") {
                                    resourceInfo.mapPath += "/";
                              }
                        }

                        return resourceInfo;
                  }
            }, {
                  key: "elementSize",
                  get: function get() {
                        return this._props.elementSize;
                  }
            }, {
                  key: "size",
                  get: function get() {
                        return new THREE.Vector3(this.scale.x * this.elementSize.x, this.scale.y * this.elementSize.y, this.scale.z * this.elementSize.z);
                  }
            }, {
                  key: "resource",
                  get: function get() {
                        return this._props.resource;
                  }
            }, {
                  key: "selected",
                  get: function get() {
                        return this._selected;
                  },
                  set: function set(bValue) {
                        if (this._selected != bValue) {
                              this._selected = bValue;

                              if (!this._selected && this.instance) {
                                    this.instance = null;
                              }
                        }
                  } // 프록시 정보를 통해 생성되는 진짜 instance참조
                  // null값이 들어오면 destroy처리

            }, {
                  key: "instance",
                  set: function set(instance) {
                        this._instance = instance;
                  },
                  get: function get() {
                        return this._instance;
                  }
            }, {
                  key: "name",
                  set: function set(strName) {
                        this._name = strName;
                  },
                  get: function get() {
                        return this._name;
                  }
            }, {
                  key: "parent",
                  set: function set(parent) {
                        this._parent = parent;
                  },
                  get: function get() {
                        return this._parent;
                  }
            }, {
                  key: "assetId",
                  get: function get() {
                        return this._data.id;
                  }
            }, {
                  key: "data",
                  get: function get() {
                        return this._data;
                  }
            }, {
                  key: "visible",
                  set: function set(value) {
                        this._visible = value;
                  },
                  get: function get() {
                        return this._visible;
                  }
            }, {
                  key: "scale",
                  set: function set(value) {
                        this._scale = value;
                  },
                  get: function get() {
                        return this._scale;
                  }
            }, {
                  key: "rotation",
                  set: function set(value) {
                        this._rotation = value;
                  },
                  get: function get() {
                        return this._rotation;
                  }
            }, {
                  key: "position",
                  set: function set(value) {
                        this._position = value;
                  },
                  get: function get() {
                        return this._position;
                  }
            }, {
                  key: "color",
                  set: function set(value) {
                        this._color = value;
                  },
                  get: function get() {
                        return this._color;
                  }
            }]);

            return MeshInstanceProxy;
      }();
