function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var RackInnerMeshDecorator =
      /*#__PURE__*/
      function (_WVMeshDecorator) {
            "use strict";

            _inherits(RackInnerMeshDecorator, _WVMeshDecorator);

            function RackInnerMeshDecorator(source) {
                  _classCallCheck(this, RackInnerMeshDecorator);

                  return _possibleConstructorReturn(this, _getPrototypeOf(RackInnerMeshDecorator).call(this, source));
            }

            _createClass(RackInnerMeshDecorator, [{
                  key: "validateData",
                  value: function validateData() {
                        if (this.data.rotateY) {
                              this.appendElement.geometry.rotateZ(90 * Math.PI / 180);
                        } // 회전값에 따라 x, y값을 서로 변경


                        var sx = this.data.scale.x;
                        var sy = this.data.scale.y;
                        var sz = this.data.scale.z;
                        var tx = this.data.rotateY ? this.size.x : this.size.x / 2;
                        var ty = this.data.rotateY ? -this.size.y / 2 : -this.size.y;
                        this.appendElement.geometry.translate(tx, ty, 0);
                        this.position.copy(this.data.position);
                        this.scale.set(sx, sy, sz);
                  }
            }, {
                  key: "transitionAfter",
                  value: function transitionAfter() { this.appendElement.updateMatrixWorld(true) }
            }, {
                  key: "transitionInEffect",
                  value: function transitionInEffect() {
                        var _this = this;

                        this.clearTween();
                        return new Promise(function (resolve) {
                              _this._focusTween = TweenMax.to(_this.position, .5, {
                                    z: _this.size.z / 2,
                                    onComplete: function onComplete() {
                                          _this.transitionAfter();

                                          resolve();
                                    }
                              });
                        });
                  }
            }, {
                  key: "transitionOutEffect",
                  value: function transitionOutEffect() {
                        var _this2 = this;

                        this.clearTween();
                        return new Promise(function (resolve) {
                              _this2._focusTween = TweenMax.to(_this2.position, .5, {
                                    z: 0,
                                    onComplete: function onComplete() {
                                          _this2.transitionAfter();

                                          resolve();
                                    }
                              });
                        });
                  }
            }]);

            return RackInnerMeshDecorator;
      }(WVMeshDecorator);
