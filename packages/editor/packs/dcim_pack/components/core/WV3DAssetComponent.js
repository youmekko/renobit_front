class WV3DAssetComponent extends WV3DResourceComponent {

      constructor() {
            super();
      }

      _onCreateProperties() {
            this._data = this.properties.setter.data;
            this._usePolling = false;
            this.properties.setter.data = null;
            this.properties.setter.assetId = null;
            delete this.properties.setter.data;
            delete this.properties.setter.assetId;
            this._children = [];
            if (this.isViewerMode) {
                  this.initViewerProperties();
            }
            super._onCreateProperties();
      }

      getIconLabelImageList() {
            let list = [];
            for (let prop in WV3DAssetComponent.STATE) {
                  let severity = WV3DAssetComponent.STATE[prop];
                  if (severity == WV3DAssetComponent.STATE.DEFAULT) {continue};
                  let path = `${WV3DAssetComponent.ICON_PATH}${this.assetType}_${severity}.png`;
                  list.push({key: severity, path: path});
            }
            return list;
      }

      hasAssetIcon() {
            return false;
      }

      initViewerProperties() {
            this._useToolTip = false;
            this.focusTween = null;
            this._iconLabel = null;
            this._stateColors = {};
            this._stateIcons = {};
            this._state = WV3DAssetComponent.STATE.DEFAULT;
            this.registStateColor(WV3DAssetComponent.STATE.DEFAULT, "#ffffff");
            this.registStateColor(WV3DAssetComponent.STATE.NORMAL, "#3ca0c5");
            this.registStateColor(WV3DAssetComponent.STATE.MINOR, "#e7c33b");
            this.registStateColor(WV3DAssetComponent.STATE.MAJOR, "#cd821d");
            this.registStateColor(WV3DAssetComponent.STATE.WARNING, "#40bf56");
            this.registStateColor(WV3DAssetComponent.STATE.CRITICAL, "#c73f45");
            //데이터가 설정되어 있으면 자산 아이콘의 유효성을 체크
            if (this.isAssetComp() && this.hasAssetIcon()) {
                  (async () => {
                        let response = await CPUtil.getInstance().getValidateImageList(this.getIconLabelImageList());
                        response.forEach((info) => {
                              this.registStateIconPath(info.key, info.path);
                              if (info.key == WV3DAssetComponent.STATE.NORMAL) {
                                    this.registStateIconPath(WV3DAssetComponent.STATE.DEFAULT, info.path);
                              }
                        });
                  })();
            }
            // 관련 자산을 배열로 할지 오브젝트로 할지는 타입에 따라 다를 듯.
            this.childMouseOverHandler          = this._onChildMouseOver.bind(this);
            this.childMouseOutHandler           = this._onChildMouseOut.bind(this);
            this.childClickHandler              = this._onChildClickHandler.bind(this);
            this.childDoubleClickHandler        = this._onChildDoubleClickHandler.bind(this);
            this.iconLabelClickHandler          = this._onIconLabelClickHandler.bind(this);
            this.iconLabelDoubleClickHandler    = this._onIconLabelDoubleClickHandler.bind(this);
      }

      registStateColor(state, value) {
            this._stateColors[state] = value;
      }

      registStateIconPath(state, value) {
            this._stateIcons[state] = value;
      }

      invalidateAssetData() {
            // 자산 데이터 변경에 따른 업데이트 처리
            this._resourceInfo = this.getResourceInfo();
            (async () => {
                  let loadedObj = await this._validateResource();
                  this.composeResource(loadedObj);
                  this._onValidateResource();
            })();
      }

      set usePolling(bValue) {
            if (this._usePolling != bValue) {
                  this._usePolling = bValue;
                  this.deferredCall(this._validatePolling);
            }
      }

      get usePolling() {
            return this._usePolling;
      }

      set data(info) {
            if (this._data != info) {
                  this._data = info;
                  if (this._data != null) {
                        //this.invalidateAssetData();
                  }
            }
      }

      get data() {
            return this._data;
      }

      get assetId() {
            return (this.data) ? this.data.id : "";
      }

      get assetType() {
            return (this.data) ? this.data.asset_type : "";
      }

      get popupId() {
            let user = this.getGroupPropertyValue("override_settings", "popupId");
            if (user) {
                  return user;
            }
            let config = this.getGroupPropertyValue("settings", "popupId");
            return config;
      }



      _onCommitProperties() {
            super._onCommitProperties();
            if (this._resourceLoaded) {
                  this.validateCallLater(this._validateState);
            }
      }

      set toolTipMessage(value) {
            this._toolTipMessage = value;
      }

      get toolTipMessage() {
            return this._toolTipMessage;
      }

      get toolTipTarget() {
            return this._outlineElement;
      }

      set useToolTip(bValue) {
            this._useToolTip = bValue;
      }

      get useToolTip() {
            return this._useToolTip;
      }


      /*
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
             상태 아이콘 및 컬러 반환 관련 처리
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
      */
      getAssetIconFromState(state) {
            if (this._stateIcons.hasOwnProperty(state)) {
                  return this._stateIcons[state];
            } else {
                  return this._stateIcons[WV3DAssetComponent.STATE.DEFAULT];
            }
      }

      getAssetColorFromState(state) {
            if (this._stateColors.hasOwnProperty(state)) {
                  return this._stateColors[state];
            } else {
                  return this._stateColors[WV3DAssetComponent.STATE.DEFAULT]
            }
      }



      /*
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
             상태에 따른 아이콘 라벨 관련 처리
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
      */
      get iconLabel() {
            if (this._iconLabel == null) {
                  this._iconLabel = new WV3DIconLabel();
                  this._iconLabel.label.css({
                        "color": "white",
                        "padding": "2px 4px",
                        "background-color": "rgba(0,0,0,0.5)"
                  });
                  $(this._iconLabel.element).css({"text-align": "center"});
                  $(this._iconLabel.element).on("click", this.iconLabelClickHandler);
                  $(this._iconLabel.element).on("dblclick", this.iconLabelDoubleClickHandler);
                  this.setIconLabelPosition();
                  this.iconLabel.text = this.data.name;
            }
            return this._iconLabel;
      }

      setIconLabelPosition() {
            this.iconLabel.position = {x: 0, y: this.elementSize.y + 6, z: 0};
      }

      updateIconLabel() {
            this.iconLabel.iconPath = this.getAssetIconFromState(this._state);
      }

      isAssetComp() {
            return Boolean(this.assetId);
      }


      isCriticalState() {
            return this._state == WV3DAssetComponent.STATE.CRITICAL;
      }


      toggleIconLabel(bValue) {
            if (!this.isAssetComp()) return;
            if (bValue) {
                  if (!this.hasElement(this.iconLabel.appendElement)) {
                        this.iconLabel.draw(this);
                  }
            } else {
                  if (this.hasElement(this.iconLabel.appendElement)) {
                        this.iconLabel.clear();
                  }
            }
      }


      /**
       * selected속성이 false일 경우
       * 현재 상태를 나타내는 아이콘을 화면에서 제거
       * */
      toggleSelectedOutline(bValue) {
            super.toggleSelectedOutline(bValue);
            if (!bValue) {
                  this.hideCurrentState();
            }
      }


      /**
       * 데이터에 따른 자산 모델링 리소스 반환
       * 설정은 자산관라자 패널에 컴포넌트 설정패널에서
       * 지정된 key가 없으면 기본 모델링을 반환
       **/
      getResourceInfo() {
            if(this.resource == null || !this.resource.hasOwnProperty("modeling")) {
                  CPLogger.log("##WV3DAssetComponent", "No resource.modeling Info");
                  return null;
            }
            let resourceInfo = this.resource.modeling;
            let defaultkey = "default";

            if (resourceInfo) {
                  let key = this.resource.mappingKey || defaultkey;
                  if (this.data != null && this.data.hasOwnProperty(key)) {
                        key = this.data[key] || defaultkey;
                  }
                  resourceInfo = resourceInfo[key] || resourceInfo[defaultkey];
            } else {
                  CPLogger.log("##WV3DAssetComponent", "No resource.modeling Info");
                  return null;
            }

            if (resourceInfo.mapPath.substr(-1) != "/") {
                  resourceInfo.mapPath += "/";
            }
            return resourceInfo;
      }


      createGroupEdge(object) {
            if (!this.usingRenderer) {
                  return super.createGroupEdge(object);
            }
            // json 용 아웃라인 박스 처리
            object.updateMatrixWorld(true);
            let box = ThreeUtil.getObjectBox(object);
            // 원점을 기준으로 위치를 계산하기 위해 translate를 통해 position값을 offset처리
            let inverse = new THREE.Matrix4().getInverse(object.matrixWorld);
            box = box.applyMatrix4(inverse);
            // 회전은 유지
            let rMatrix = new THREE.Matrix4();
            rMatrix.makeRotationX(-Math.PI / 2);
            box = box.applyMatrix4(rMatrix);
            let boxSize = box.getSize(new THREE.Vector3());
            let geometry = new THREE.BoxGeometry(boxSize.x, boxSize.y, boxSize.z);
            let offset = box.getCenter();
            geometry.translate(offset.x, offset.y, offset.z);
            return this.createEdge(geometry);
      }


      get usingRenderer() {
            if (this._resourceInfo.hasOwnProperty("path")) {
                  return !this.getResourceFileName(this._resourceInfo.path).indexOf(".obj") <= -1;
            } else {
                  return false;
            }
      }

      render() {
            if (this.mixer != undefined) {
                  this.mixer.update(this.clock.getDelta());
            }
      }

      play() {
            this.animation.play();
      }

      stop() {
            this.animation.stop();
      }

      composeJsonAnimation() {
            this.clock = new THREE.Clock();
            this.mixer = new THREE.AnimationMixer(this._element);
            this.animation = this.mixer.clipAction(this._element.geometry.animations[0]);
            this.animation.play();
      }

      // 상태에 따른 컬러 적용
      _validateState() {
            if (this.isViewerMode && this.isAssetComp()) {
                  this.color = this.getAssetColorFromState(this._state);
                  this.updateIconLabel();
                  this.toggleCriticalDisplay(this._usePolling || this.isCriticalState());
            }
      }

      clearTween() {
            if (this.focusTween != null && this.focusTween.isActive()) {
                  this.focusTween.pause();
                  this.focusTween.kill();
            }
      }

      transitionIn(data) {
            return new Promise(async (resolve) => {
                  this.clearTween();
                  await this.transitionInEffect()
                  this._useToolTip = false;
                  resolve();
            });
      }

      transitionOut(data) {
            return new Promise(async (resolve) => {
                  //this.selected = false;
                  this.clearTween();
                  await this.transitionOutEffect();
                  this._useToolTip = true;
                  resolve();
            });
      }

      /*_validateOutline() {
            this._setOutlineElement(MeshManager.requestMergeMesh(this.element));
      }*/


      _onValidateResource() {
            super._onValidateResource();
            this._validateState();
      }


      onLoadPage() {
            super.onLoadPage();
            if (this.isViewerMode && this.usingRenderer) {
                  this.composeJsonAnimation();
            }
      }


      /*
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
            오버라이드 메서드 정의 - 실장 요소 마우스 이벤트 처리
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
      */
      _preventEvent(event) {
            event.stopPropagation();
            event.origDomEvent.preventDefault();
            event.origDomEvent.stopPropagation();
            event.origDomEvent.stopImmediatePropagation();
      }

      _onChildMouseOver(event) {
            if (this.isViewerMode) {
                  this.dispatchWScriptEvent("itemMouseOver", {
                        target: this,
                        selectItem: event.selectItem,
                        type: "itemMouseOver"
                  });
            }
      }

      _onChildMouseOut(event) {
            if (this.isViewerMode) {
                  this.dispatchWScriptEvent("itemMouseOut", {
                        target: this,
                        selectItem: event.selectItem,
                        type: "itemMouseOut"
                  });
            }
      }

      _onChildClickHandler(event) {
            if (this.isViewerMode) {
                  this.dispatchWScriptEvent("itemClick", {
                        target: this,
                        selectItem: event.selectItem,
                        type: "itemClick"
                  });
            }
      }

      _onChildDoubleClickHandler(event) {
            if (this.isViewerMode) {
                  this.dispatchWScriptEvent("itemDblClick", {
                        target: this,
                        selectItem: event.selectItem,
                        type: "itemDblClick"
                  });
            }
      }

      _onIconLabelClickHandler(event) {
            //console.log(event);
      }

      _onIconLabelDoubleClickHandler(event) {
            //console.log(event);
      }


      /*
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
         컴포넌트 상태 표현 처리 / 이벤트 상태 표현 처리
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
      */
      async changePropsData(info) {

      }

      changeNotification(noti) {
            let type = noti.type;
            let body = noti.body;
            switch (type) {

                  case DCIMManager.NOTI_CHANGE_SEVERITY:
                        this._state = body;
                        this._validateState();
                        break;

                  case DCIMManager.NOTI_CHANGE_ASSET_DATA:
                        console.log("NOTI_CHANGE_ASSET_DATA", body);
                        this._data = body;
                        this.invalidateAssetData();
                        break;
                  case DCIMManager.NOTI_CHANGE_PROPS_DATA:
                        this.setGroupPropertyValue("settings", "popupId", body.popupId);
                        this.setGroupProperties("resource", body.resource);
                        this.data = body.data;
                        this.invalidateAssetData();
                        break;

                  case DCIMManager.NOTI_CHANGE_EVENT_DATA:
                        if (body != null) {
                              this.displayEventState(body);
                        }
                        break;

                  default:

                        break;
            }
      }

      /**
       * 컴포넌트 고정상태 여부 반환
       * 이벤트 데이터 폴링 중 or critical 상태 or 선택 상태
       * */
      isFixedState() {
            return this._usePolling || this.isCriticalState() || this.selected;
      }

      /**
       * 컴포넌트 선택 여부 설정
       * 값에 따라 outline을 toggleSelectOutline으로 노출
       * */
      set selected(bValue) {
            super.selected = bValue;
            this.toggleSelectedOutline(bValue);
      }

      get selected() {
            return super.selected;
      }

      displayCurrentState() {
            if (this.isFixedState()) return;
            this.toggleStateDisplay(true);
      }

      hideCurrentState() {
            if (this.isFixedState()) return;
            this.toggleStateDisplay(false);
      }

      /**
       *  fixed상태를 무시한 state값에 따른 자산상태 반영 토글
       * */
      toggleStateDisplay(bValue) {
            if (!this.isAssetComp()) return;
            this.toggleIconLabel(bValue);
            this.toggleExtraState(bValue);
      }


      /**
       * critical상태 일 경우 디스플레이 처리
       * */
      toggleCriticalDisplay( bValue ) {
            this.toggleStateDisplay(bValue);
      }


      /**
       * 컴포넌트 별 추가적인 상태반영이 필요할 경우 override 하여 사용
       * */
      toggleExtraState(bValue) { };


      /**
       * 자산 이벤트 데이터 정보 처리
       * changeNotification에서 호출
       * */
      displayEventState(info) { }


      /**
       * 노출된 자산 이벤트 데이터 정보 제거
       * _usePolling값이 false일 경우 호출
       * */
      hideEventState() { }

      // 폴링 상태 변경에 따른 갱신 처리
      // 이벤트 상태는 changeNotification으로 상태를 보여줌.
      // 폴링 상태가 끝날 경우만 이벤트 상태 제거 처리
      _validatePolling() {
            if (this._usePolling) {
                  this.toggleStateDisplay(true);
            } else {
                  this.hideCurrentState();
                  this.hideEventState();
            }
      }


      /*
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
         자산 상태 표현을 위한 마우스 이벤트 오버라이드
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
      */
      _onMouseOverHandler(event) {
            if (this.isViewerMode) {
                  this.displayCurrentState();
            }
            super._onMouseOverHandler(event);
      }

      _onMouseOutHandler(event) {
            if (this.isViewerMode) {
                  this.hideCurrentState();
            }
            super._onMouseOutHandler(event);
      }


      /*
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
            오버라이드 메서드 정의
                  - 실장 구성 및 애니메이션 처리
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
      */
      hasChildAsset() {
            //실장 데이터 여부를 반환
            //필요 컴포넌트에서 override하여 사용
            return false;
      }


      /**
       * 실장 자산 컴포넌트 return
       * @param assetID 실장 자산 assetId;
       * */
      getChildByAssetID(assetID) {
            for (let i = 0; i < this._children.length; i++) {
                  let asset = this._children[i];
                  if (asset.data.id == assetID) {
                        return asset;
                  }
            }
            return null;
      }


      /**
       * 실장 자산 컴포넌트 return
       * @param strName 찾으려는 자산인스턴스 이름;
       * */
      getChildByAssetName(strName) {
            for (let i = 0; i < this._children.length; i++) {
                  let asset = this._children[i];
                  if (asset.name == strName) {
                        return asset;
                  }
            }
            return null;
      }

      buildChildren() {
            //console.log("실장 구성은 여기를 오버라이드");
      }

      releaseChildren() {
            //console.log("실장 정보 해지는 여기를 오버라이드");
      }

      transitionInEffect() {
            return Promise.resolve("");
      }

      transitionOutEffect() {
            return Promise.resolve("");
      }

      _onDestroy() {
            if (this.usingRenderer) {
                  this.stop();
                  this.animation = null;
                  this.mixer = null;
            }
            this.releaseChildren();
            this.iconLabelClickHandler = null;
            this.iconLabelDoubleClickHandler = null;
            this.childMouseOverHandler = null;
            this.childMouseOutHandler = null;
            this.childClickHandler = null;
            this.childDoubleClickHandler = null;
            this._data = null;
            this.focusTween = null;
            super._onDestroy();
      }

}


WV3DAssetComponent.STATE = {
      DEFAULT: "default",
      NORMAL: "normal",
      MINOR: "minor",
      MAJOR: "major",
      WARNING: "warning",
      CRITICAL: "critical"
};
// WV3DAssetComponent.ICON_PATH = "./custom/assets/dcim_pack/WV3DAssetComponent/";
WV3DAssetComponent.ICON_PATH = "./custom/packs/dcim_pack/resource/";

WV3DPropertyManager.attach_default_component_infos(WV3DAssetComponent, {

      "setter": {
            "fixed":false
      },
      "bindingProperties": {},
      "label": {
            "label_text": "WV3DAssetComponent",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "WV3DAssetComponent",
            "version": "1.0.0",
      }

});


WVPropertyManager.add_property_panel_group_info(WV3DAssetComponent, {
      label: "popupId",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "popupId",
            type: "string",
            label: "popupId",
            show: true,
            writable: true,
            description: "popup id"
      }]
});
