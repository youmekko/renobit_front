class RackInnerMeshDecorator extends WVMeshDecorator {

      constructor(source) {
            super(source);
      }

      validateData() {
            if (this.data.rotateY) {
                  this.appendElement.geometry.rotateZ(90 * Math.PI/180);
            }
            // 회전값에 따라 x, y값을 서로 변경
            let sx = this.data.scale.x;
            let sy = this.data.scale.y;
            let sz = this.data.scale.z;
            let tx = (this.data.rotateY) ? this.size.x : this.size.x/2;
            let ty = (this.data.rotateY) ? -this.size.y/2 : -this.size.y;
            this.appendElement.geometry.translate(tx, ty, 0);
            this.position.copy(this.data.position);
            this.scale.set(sx, sy, sz);
      }

      transitionAfter(){
            this.appendElement.updateMatrixWorld(true);
      }

      transitionInEffect() {
            this.clearTween();
            return new Promise((resolve) => {
                  this._focusTween = TweenMax.to(
                        this.position, .5, {
                              z: this.size.z / 2,
                              onComplete: () => {
                                    this.transitionAfter();
                                    resolve();
                              }
                        }
                  );
            });
      }

      transitionOutEffect() {
            this.clearTween();
            return new Promise((resolve) => {
                  this._focusTween = TweenMax.to(
                        this.position, .5, {
                              z: 0,
                              onComplete: () => {
                                    this.transitionAfter();
                                    resolve();
                              }
                        }
                  );
            });
      }

}
