// 객체를 인스턴스로 생성하지 않고 코드 상에서 인스턴스처럼 다루기 위한 프록시
class MeshInstanceProxy {
      constructor(properties){
            this._parent  =null;
            this._position=null;
            this._rotation=null;
            this._color   =0xffffff;
            this._scale   =null;
            this._props   =null;
            this._instance=null;
            this._data    =null;
            if(properties){
                  this.initFromProperties(properties);
            }
      }

      initFromProperties( info ){
            this._props       = info.props;
            let toRadian      = Math.PI/180;
            let rotate        = this._props.setter.rotation || new THREE.Vector3;
            let position      = this._props.setter.position || new THREE.Vector3;
            let scale         = this._props.setter.size || new THREE.Vector3;
            let elementSize   = this._props.elementSize || new THREE.Vector3;
            this._data        = this._props.setter.data;
            this.rotation     = new THREE.Vector3( rotate.x*toRadian, rotate.y*toRadian, rotate.z*toRadian);
            this.position     = new THREE.Vector3(position.x, position.y, position.z );
            this.scale        = new THREE.Vector3(scale.x/elementSize.x, scale.y/elementSize.y, scale.z/elementSize.z);
            this.selected     = false;
      }

      get elementSize(){ return this._props.elementSize; }

      get size() { return new THREE.Vector3(this.scale.x*this.elementSize.x, this.scale.y*this.elementSize.y, this.scale.z*this.elementSize.z); }
      get resource(){ return this._props.resource; }
      get selected(){ return this._selected; }

      set selected( bValue ){
            if(this._selected != bValue){
                  this._selected = bValue;
                  if(!this._selected && this.instance ){
                        this.instance = null;
                  }
            }
      }

      // 프록시 정보를 통해 생성되는 진짜 instance참조
      // null값이 들어오면 destroy처리
      set instance(instance) { this._instance = instance; }

      get instance() { return this._instance; }
      

      set name(strName) { this._name = strName; }
      get name() { return this._name; }

      set parent( parent ){ this._parent = parent; }
      get parent(){ return this._parent; }

      get assetId(){
            return this._data.id;
      }

      get data(){
            return this._data;
      }

      set visible( value ) { this._visible =value; }
      get visible(){ return this._visible; }

      set scale( value ) { this._scale = value; }
      get scale() { return this._scale; }

      set rotation( value ) { this._rotation = value; }
      get rotation() { return this._rotation; }

      set position( value ) { this._position = value; }
      get position() { return this._position; }

      getWorldPosition(){
            return this.position.clone().applyMatrix4(this.parent.appendElement.matrixWorld);
      }

      set color( value ) { this._color = value; }
      get color() { return this._color; }

      onDestroy(){}

      destroy(){
            this.onDestroy();
            this._parent = null;
            this._data   = null;
      }


      getResourceFileName(info) {
            return info.substring(info.lastIndexOf("/")+1);
      }

      getResourceInfo(){
            let resourceInfo = this.resource.modeling;
            if(resourceInfo) {
                  let key = this.resource.mappingKey;
                  if(this.data != null && this.data.hasOwnProperty(key)) {
                        key = this.data[key];
                  } else {
                        key = "default";
                  }
                  resourceInfo = resourceInfo[key];
            } else {
                  CPLogger.log("##WV3DAssetComponent", "No resource.modeling Info" );
                  return null;
            }
            if(resourceInfo){
                  if (!resourceInfo.name) {
                        resourceInfo.name = this.getResourceFileName(resourceInfo.path);
                  }
                  if ( resourceInfo.mapPath && resourceInfo.mapPath.substr(-1) != "/") {
                        resourceInfo.mapPath += "/";
                  }
            }

            return resourceInfo;
      }
}
