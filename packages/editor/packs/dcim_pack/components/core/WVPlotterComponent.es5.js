"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var WVPlotterComponent =
      /*#__PURE__*/
      function () {
            function WVPlotterComponent() {
                  _classCallCheck(this, WVPlotterComponent);

                  this.grayTexture;
                  this.colorTexture;
                  this.element;
                  this.dataProvider = [];
                  this.mapGenerator;
                  this._useBackGround = false;
                  this.divisor = 10;
                  this.space;
                  this.type = "";
                  this.mapInfo = {
                        temperature: {
                              colorGradient: {
                                    0.2: '#00FFFF',
                                    0.4: '#0099FF',
                                    0.6: '#00FF00',
                                    0.8: '#ffff00',
                                    1.0: '#ff0000'
                              },
                              grayGradient: {
                                    0.2: '#333333',
                                    0.4: '#666666',
                                    0.6: '#999999',
                                    0.8: '#CCCCCC',
                                    1.0: '#ffffff'
                              }
                        },
                        background: "black",
                        humidity: {
                              colorGradient: {
                                    0.2: '#58b1cb',
                                    0.4: '#02D5F9',
                                    0.6: '#067acb',
                                    0.8: '#0566cb',
                                    1.0: '#004AFA'
                              },
                              grayGradient: {
                                    0.2: '#333333',
                                    0.4: '#666666',
                                    0.6: '#999999',
                                    0.8: '#CCCCCC',
                                    1.0: '#ffffff'
                              }
                        }
                  };
                  this.tempSections = [0, 100];
                  this.humiSections = [0, 100];
                  this.displacementScale = 140;
            } // 맵 생성 요청 처리


            _createClass(WVPlotterComponent, [{
                  key: "createMapWithSpace",
                  value: function createMapWithSpace(space) {
                        this.space = space;
                        this.parentBoundingBox = new THREE.Box3().setFromObject(this.space.appendElement);
                        var spaceSize = this.space.size;
                        var rw = this.getBestPowerOf2(+spaceSize.x >> 1) / 2;
                        var rh = this.getBestPowerOf2(+spaceSize.z >> 1) / 2;
                        var minSide = Math.min(rw, rh);
                        var radius = minSide / this.divisor / 2;
                        var blur = radius * 1.4;
                        this.mapGenerator = new MapGenerator({
                              width: rw,
                              height: rh,
                              radius: radius,
                              blur: blur
                        });
                        this.changeType("temperature");
                        var modelRate = spaceSize.z / spaceSize.x;
                        var geometry = new THREE.PlaneGeometry(spaceSize.x, spaceSize.z, 128, 128 * modelRate);
                        this.colorTexture = new THREE.Texture(this.mapGenerator.colorLayer);
                        this.colorTexture.needsUpdate = true;
                        this.grayTexture = new THREE.Texture(this.mapGenerator.grayLayer);
                        this.grayTexture.needsUpdate = true;
                        var material = new THREE.MeshPhongMaterial();
                        material.side = THREE.DoubleSide;
                        material.map = this.colorTexture;
                        material.normapMap = this.colorTexture;
                        material.displacementMap = this.grayTexture;
                        material.normalMapScale = new THREE.Vector2(1, -1);
                        material.displacementScale = this.displacementScale;
                        material.displacementBias = -0.5;
                        material.transparent = true;
                        this.element = new THREE.Mesh(geometry, material);
                        this.element.rotation.x = -Math.PI / 2;
                  }
                  /*convertSpacePosition( objPosition, areaInfo ){
                        var xpos = (objPosition.x+(areaInfo.x/2))/areaInfo.x;
                        var zpos = (objPosition.z+(areaInfo.z/2))/areaInfo.z;
                        return {x:xpos, y:zpos};
                  }*/

            }, {
                  key: "remove",
                  value: function remove() {
                        this.parent.remove(this.element);
                  }
            }, {
                  key: "draw",
                  value: function draw(parent) {
                        this.parent = parent;
                        this.parent.add(this.element);
                  }
            }, {
                  key: "calculate",
                  value: function calculate(min, max, value) {
                        return (value - min) / (max - min);
                  }
            }, {
                  key: "changeType",
                  value: function changeType(strType) {
                        if (this.type != strType) {
                              this.type = strType;
                              var colors = this.mapInfo[strType].colorGradient;
                              var grays = this.mapInfo[strType].grayGradient;
                              this.mapGenerator.setGradient(colors, grays, this.useBackGround);
                        }
                  }
            }, {
                  key: "setTempSections",
                  value: function setTempSections(objProvider) {
                        this.tempSections = objProvider;
                  }
            }, {
                  key: "setHumiSections",
                  value: function setHumiSections(objProvider) {
                        this.humiSections = objProvider;
                  }
            }, {
                  key: "setTempColorGradient",
                  value: function setTempColorGradient(objColors) {
                        this.mapInfo.temperature.colorGradient = objColors;
                  }
            }, {
                  key: "setHumiColorGradient",
                  value: function setHumiColorGradient(objColors) {
                        this.mapInfo.humidity.colorGradient = objColors;
                  }
            }, {
                  key: "paint",
                  value: function paint(info) {
                        var isTemp = info.type == "temperature";
                        var min = isTemp ? this.tempSections[0] : this.humiSections[0];
                        var max = isTemp ? this.tempSections[1] : this.humiSections[1];
                        var value = isTemp ? info.temp : info.humi;
                        var force = this.calculate(min, max, value);
                        var spacePosition = this.parentBoundingBox.getParameter(info.object.appendElement.position); //let spacePosition = this.convertSpacePosition( this.space.appendElement.worldToLocal( info.object.appendElement.position.clone() ) , this.space.size );

                        var px = spacePosition.x * this.colorTexture.image.width;
                        var py = spacePosition.z * this.colorTexture.image.height;
                        this.mapGenerator.add(px, py, force);
                  }
            }, {
                  key: "addItem",
                  value: function addItem(item) {
                        this.dataProvider.push(item);
                  }
            }, {
                  key: "setDataProvider",
                  value: function setDataProvider(objProvider) {
                        if (this.dataProvider != objProvider) {
                              this.dataProvider = objProvider;
                              this.update();
                        }
                  }
            }, {
                  key: "getBestPowerOf2",
                  value: function getBestPowerOf2(value) {
                        var MAX_SIZE = 4096;
                        var p = 1;

                        while (p < value) {
                              p <<= 1;
                        }

                        if (p > MAX_SIZE) p = MAX_SIZE;
                        return p;
                  }
            }, {
                  key: "adjustSize",
                  value: function adjustSize(size) {
                        var results = {
                              size: size,
                              scale: 1
                        };

                        while (results.size > 2000) {
                              results.size >>= 1;
                              results.scale *= 2;
                        }

                        return results;
                  }
            }, {
                  key: "clear",
                  value: function clear() {
                        this.mapGenerator.clear();
                        this.mapGenerator.draw();
                        this.colorTexture.needsUpdate = true;
                        this.grayTexture.needsUpdate = true;
                  }
            }, {
                  key: "update",
                  value: function update() {
                        this.clear();
                        var max = this.dataProvider.length;
                        var sensorVO, idx;

                        for (idx = 0; idx < max; idx++) {
                              sensorVO = this.dataProvider[idx];
                              sensorVO.type = this.type;
                              this.paint(sensorVO);
                        }

                        this.mapGenerator.draw();
                        this.colorTexture.needsUpdate = true;
                        this.grayTexture.needsUpdate = true;
                  }
            }, {
                  key: "destroy",
                  value: function destroy() {
                        // scene에서 element제거
                        this.parent.remove(this.element);
                        MeshManager.disposeMesh(this.element);
                        this.grayTexture.dispose();
                        this.colorTexture.dispose();
                        this.mapGenerator.destroy();
                        this.mapGenerator = null;
                        this.colorTexture = null;
                        this.grayTexture = null;
                        this.dataProvider = null;
                        this.parent = null;
                  }
            }, {
                  key: "position",
                  get: function get() {
                        return this.element.position;
                  },
                  set: function set(value) {
                        this.element.position.set(value.x, value.y, value.z);
                  }
            }, {
                  key: "rotation",
                  get: function get() {
                        return this.element.rotation;
                  },
                  set: function set(value) {
                        return this.element.rotation.set(value.x, value.y, value.z);
                  }
            }, {
                  key: "useBackGround",
                  set: function set(value) {
                        if (this._useBackGround != value) {
                              this._useBackGround = value;
                        }
                  },
                  get: function get() {
                        return this._useBackGround;
                  }
            }, {
                  key: "visible",
                  set: function set(value) {
                        this.element.material.visible = value;
                  },
                  get: function get() {
                        return this.element.material.visible;
                  }
            }]);

            return WVPlotterComponent;
      }();

var MapGenerator =
      /*#__PURE__*/
      function () {
            function MapGenerator(opt) {
                  _classCallCheck(this, MapGenerator);

                  this.blur = opt.blur;
                  this.radius = opt.radius;
                  this.colorLayer = document.createElement("canvas");
                  this.colorLayer.width = opt.width;
                  this.colorLayer.height = opt.height;
                  this.grayLayer = this.cloneCanvas(this.colorLayer);
                  this.heatmap = simpleheat(this.colorLayer);
                  this.graymap = simpleheat(this.grayLayer);
                  this.heatmap.radius(opt.radius, opt.blur).max(1);
                  this.graymap.radius(opt.radius, opt.blur).max(1);
            }

            _createClass(MapGenerator, [{
                  key: "setGradient",
                  value: function setGradient(colorGradient, grayGradient, useBackGround) {
                        this.heatmap.gradient(colorGradient);
                        this.graymap.gradient(grayGradient); // 배경 사용 시 가장 낮은 컬러값을 적용

                        if (useBackGround) {
                              var prop = Object.keys(colorGradient).sort();
                              this.heatmap.bgColor(colorGradient[prop[0]]);
                              this.graymap.bgColor(grayGradient[prop[0]]);
                        }
                  }
            }, {
                  key: "cloneCanvas",
                  value: function cloneCanvas(origin) {
                        var clone = document.createElement("canvas");
                        clone.width = origin.width;
                        clone.height = origin.height;
                        var context = clone.getContext("2d");
                        context.drawImage(origin, 0, 0);
                        return clone;
                  }
            }, {
                  key: "add",
                  value: function add(x, y, value) {
                        this.heatmap.add([x, y, value]);
                        this.graymap.add([x, y, value]);
                  }
            }, {
                  key: "clear",
                  value: function clear() {
                        this.heatmap.clear();
                        this.graymap.clear();
                  }
            }, {
                  key: "draw",
                  value: function draw() {
                        this.heatmap.draw();
                        this.graymap.draw();
                  }
            }, {
                  key: "destroy",
                  value: function destroy() {
                        this.heatmap = null;
                        this.graymap = null;
                        this.colorLayer = null;
                        this.grayLayer = null;
                  }
            }]);

            return MapGenerator;
      }();
