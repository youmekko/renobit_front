"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function set(target, property, value, receiver) { if (typeof Reflect !== "undefined" && Reflect.set) { set = Reflect.set; } else { set = function set(target, property, value, receiver) { var base = _superPropBase(target, property); var desc; if (base) { desc = Object.getOwnPropertyDescriptor(base, property); if (desc.set) { desc.set.call(receiver, value); return true; } else if (!desc.writable) { return false; } } desc = Object.getOwnPropertyDescriptor(receiver, property); if (desc) { if (!desc.writable) { return false; } desc.value = value; Object.defineProperty(receiver, property, desc); } else { _defineProperty(receiver, property, value); } return true; }; } return set(target, property, value, receiver); }

function _set(target, property, value, receiver, isStrict) { var s = set(target, property, value, receiver || target); if (!s && isStrict) { throw new Error('failed to set property'); } return value; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var WV3DAssetComponent =
      /*#__PURE__*/
      function (_WV3DResourceComponen) {
            _inherits(WV3DAssetComponent, _WV3DResourceComponen);

            function WV3DAssetComponent() {
                  _classCallCheck(this, WV3DAssetComponent);

                  return _possibleConstructorReturn(this, _getPrototypeOf(WV3DAssetComponent).call(this));
            }

            _createClass(WV3DAssetComponent, [{
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        this._data = this.properties.setter.data;
                        this._usePolling = false;
                        this.properties.setter.data = null;
                        this.properties.setter.assetId = null;
                        delete this.properties.setter.data;
                        delete this.properties.setter.assetId;
                        this._children = [];

                        if (this.isViewerMode) {
                              this.initViewerProperties();
                        }

                        _get(_getPrototypeOf(WV3DAssetComponent.prototype), "_onCreateProperties", this).call(this);
                  }
            }, {
                  key: "getIconLabelImageList",
                  value: function getIconLabelImageList() {
                        var list = [];

                        for (var prop in WV3DAssetComponent.STATE) {
                              var severity = WV3DAssetComponent.STATE[prop];

                              if (severity == WV3DAssetComponent.STATE.DEFAULT) {
                                    continue;
                              }

                              ;
                              var path = "".concat(WV3DAssetComponent.ICON_PATH).concat(this.assetType, "_").concat(severity, ".png");
                              list.push({
                                    key: severity,
                                    path: path
                              });
                        }

                        return list;
                  }
            }, {
                  key: "hasAssetIcon",
                  value: function hasAssetIcon() {
                        return false;
                  }
            }, {
                  key: "initViewerProperties",
                  value: function initViewerProperties() {
                        var _this = this;

                        this._useToolTip = false;
                        this.focusTween = null;
                        this._iconLabel = null;
                        this._stateColors = {};
                        this._stateIcons = {};
                        this._state = WV3DAssetComponent.STATE.DEFAULT;
                        this.registStateColor(WV3DAssetComponent.STATE.DEFAULT, "#ffffff");
                        this.registStateColor(WV3DAssetComponent.STATE.NORMAL, "#3ca0c5");
                        this.registStateColor(WV3DAssetComponent.STATE.MINOR, "#e7c33b");
                        this.registStateColor(WV3DAssetComponent.STATE.MAJOR, "#cd821d");
                        this.registStateColor(WV3DAssetComponent.STATE.WARNING, "#40bf56");
                        this.registStateColor(WV3DAssetComponent.STATE.CRITICAL, "#c73f45"); //데이터가 설정되어 있으면 자산 아이콘의 유효성을 체크

                        if (this.isAssetComp() && this.hasAssetIcon()) {
                              _asyncToGenerator(
                                    /*#__PURE__*/
                                    regeneratorRuntime.mark(function _callee() {
                                          var response;
                                          return regeneratorRuntime.wrap(function _callee$(_context) {
                                                while (1) {
                                                      switch (_context.prev = _context.next) {
                                                            case 0:
                                                                  _context.next = 2;
                                                                  return CPUtil.getInstance().getValidateImageList(_this.getIconLabelImageList());

                                                            case 2:
                                                                  response = _context.sent;
                                                                  response.forEach(function (info) {
                                                                        _this.registStateIconPath(info.key, info.path);

                                                                        if (info.key == WV3DAssetComponent.STATE.NORMAL) {
                                                                              _this.registStateIconPath(WV3DAssetComponent.STATE.DEFAULT, info.path);
                                                                        }
                                                                  });

                                                            case 4:
                                                            case "end":
                                                                  return _context.stop();
                                                      }
                                                }
                                          }, _callee);
                                    }))();
                        } // 관련 자산을 배열로 할지 오브젝트로 할지는 타입에 따라 다를 듯.


                        this.childMouseOverHandler = this._onChildMouseOver.bind(this);
                        this.childMouseOutHandler = this._onChildMouseOut.bind(this);
                        this.childClickHandler = this._onChildClickHandler.bind(this);
                        this.childDoubleClickHandler = this._onChildDoubleClickHandler.bind(this);
                        this.iconLabelClickHandler = this._onIconLabelClickHandler.bind(this);
                        this.iconLabelDoubleClickHandler = this._onIconLabelDoubleClickHandler.bind(this);
                  }
            }, {
                  key: "registStateColor",
                  value: function registStateColor(state, value) {
                        this._stateColors[state] = value;
                  }
            }, {
                  key: "registStateIconPath",
                  value: function registStateIconPath(state, value) {
                        this._stateIcons[state] = value;
                  }
            }, {
                  key: "invalidateAssetData",
                  value: function invalidateAssetData() {
                        var _this2 = this;

                        // 자산 데이터 변경에 따른 업데이트 처리
                        this._resourceInfo = this.getResourceInfo();

                        _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee2() {
                                    var loadedObj;
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                          while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                      case 0:
                                                            _context2.next = 2;
                                                            return _this2._validateResource();

                                                      case 2:
                                                            loadedObj = _context2.sent;

                                                            _this2.composeResource(loadedObj);

                                                            _this2._onValidateResource();

                                                      case 5:
                                                      case "end":
                                                            return _context2.stop();
                                                }
                                          }
                                    }, _callee2);
                              }))();
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        _get(_getPrototypeOf(WV3DAssetComponent.prototype), "_onCommitProperties", this).call(this);

                        if (this._resourceLoaded) {
                              this.validateCallLater(this._validateState);
                        }
                  }
            }, {
                  key: "getAssetIconFromState",

                  /*
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
						 상태 아이콘 및 컬러 반환 관련 처리
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
				  */
                  value: function getAssetIconFromState(state) {
                        if (this._stateIcons.hasOwnProperty(state)) {
                              return this._stateIcons[state];
                        } else {
                              return this._stateIcons[WV3DAssetComponent.STATE.DEFAULT];
                        }
                  }
            }, {
                  key: "getAssetColorFromState",
                  value: function getAssetColorFromState(state) {
                        if (this._stateColors.hasOwnProperty(state)) {
                              return this._stateColors[state];
                        } else {
                              return this._stateColors[WV3DAssetComponent.STATE.DEFAULT];
                        }
                  }
                  /*
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
						 상태에 따른 아이콘 라벨 관련 처리
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
				  */

            }, {
                  key: "setIconLabelPosition",
                  value: function setIconLabelPosition() {
                        this.iconLabel.position = {
                              x: 0,
                              y: this.elementSize.y + 6,
                              z: 0
                        };
                  }
            }, {
                  key: "updateIconLabel",
                  value: function updateIconLabel() {
                        this.iconLabel.iconPath = this.getAssetIconFromState(this._state);
                  }
            }, {
                  key: "isAssetComp",
                  value: function isAssetComp() {
                        return Boolean(this.assetId);
                  }
            }, {
                  key: "isCriticalState",
                  value: function isCriticalState() {
                        return this._state == WV3DAssetComponent.STATE.CRITICAL;
                  }
            }, {
                  key: "toggleIconLabel",
                  value: function toggleIconLabel(bValue) {
                        if (!this.isAssetComp()) return;

                        if (bValue) {
                              if (!this.hasElement(this.iconLabel.appendElement)) {
                                    this.iconLabel.draw(this);
                              }
                        } else {
                              if (this.hasElement(this.iconLabel.appendElement)) {
                                    this.iconLabel.clear();
                              }
                        }
                  }
                  /**
                   * selected속성이 false일 경우
                   * 현재 상태를 나타내는 아이콘을 화면에서 제거
                   * */

            }, {
                  key: "toggleSelectedOutline",
                  value: function toggleSelectedOutline(bValue) {
                        _get(_getPrototypeOf(WV3DAssetComponent.prototype), "toggleSelectedOutline", this).call(this, bValue);

                        if (!bValue) {
                              this.hideCurrentState();
                        }
                  }
                  /**
                   * 데이터에 따른 자산 모델링 리소스 반환
                   * 설정은 자산관라자 패널에 컴포넌트 설정패널에서
                   * 지정된 key가 없으면 기본 모델링을 반환
                   **/

            }, {
                  key: "getResourceInfo",
                  value: function getResourceInfo() {
                        if(this.resource == null || !this.resource.hasOwnProperty("modeling")) {
                              CPLogger.log("##WV3DAssetComponent", "No resource.modeling Info");
                              return null;
                        }
                        var resourceInfo = this.resource.modeling;
                        var defaultkey = "default";

                        if (resourceInfo) {
                              var key = this.resource.mappingKey || defaultkey;

                              if (this.data != null && this.data.hasOwnProperty(key)) {
                                    key = this.data[key] || defaultkey;
                              }

                              resourceInfo = resourceInfo[key] || resourceInfo[defaultkey];
                        } else {
                              CPLogger.log("##WV3DAssetComponent", "No resource.modeling Info");
                              return null;
                        }

                        if (resourceInfo.mapPath.substr(-1) != "/") {
                              resourceInfo.mapPath += "/";
                        }

                        return resourceInfo;
                  }
            }, {
                  key: "createGroupEdge",
                  value: function createGroupEdge(object) {
                        if (!this.usingRenderer) {
                              return _get(_getPrototypeOf(WV3DAssetComponent.prototype), "createGroupEdge", this).call(this, object);
                        } // json 용 아웃라인 박스 처리


                        object.updateMatrixWorld(true);
                        var box = ThreeUtil.getObjectBox(object); // 원점을 기준으로 위치를 계산하기 위해 translate를 통해 position값을 offset처리

                        var inverse = new THREE.Matrix4().getInverse(object.matrixWorld);
                        box = box.applyMatrix4(inverse); // 회전은 유지

                        var rMatrix = new THREE.Matrix4();
                        rMatrix.makeRotationX(-Math.PI / 2);
                        box = box.applyMatrix4(rMatrix);
                        var boxSize = box.getSize(new THREE.Vector3());
                        var geometry = new THREE.BoxGeometry(boxSize.x, boxSize.y, boxSize.z);
                        var offset = box.getCenter();
                        geometry.translate(offset.x, offset.y, offset.z);
                        return this.createEdge(geometry);
                  }
            }, {
                  key: "render",
                  value: function render() {
                        if (this.mixer != undefined) {
                              this.mixer.update(this.clock.getDelta());
                        }
                  }
            }, {
                  key: "play",
                  value: function play() {
                        this.animation.play();
                  }
            }, {
                  key: "stop",
                  value: function stop() {
                        this.animation.stop();
                  }
            }, {
                  key: "composeJsonAnimation",
                  value: function composeJsonAnimation() {
                        this.clock = new THREE.Clock();
                        this.mixer = new THREE.AnimationMixer(this._element);
                        this.animation = this.mixer.clipAction(this._element.geometry.animations[0]);
                        this.animation.play();
                  } // 상태에 따른 컬러 적용

            }, {
                  key: "_validateState",
                  value: function _validateState() {
                        if (this.isViewerMode && this.isAssetComp()) {
                              this.color = this.getAssetColorFromState(this._state);
                              this.updateIconLabel();
                              this.toggleCriticalDisplay(this._usePolling || this.isCriticalState());
                        }
                  }
            }, {
                  key: "clearTween",
                  value: function clearTween() {
                        if (this.focusTween != null && this.focusTween.isActive()) {
                              this.focusTween.pause();
                              this.focusTween.kill();
                        }
                  }
            }, {
                  key: "transitionIn",
                  value: function transitionIn(data) {
                        var _this3 = this;

                        return new Promise(
                              /*#__PURE__*/
                              function () {
                                    var _ref3 = _asyncToGenerator(
                                          /*#__PURE__*/
                                          regeneratorRuntime.mark(function _callee3(resolve) {
                                                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                                      while (1) {
                                                            switch (_context3.prev = _context3.next) {
                                                                  case 0:
                                                                        _this3.clearTween();

                                                                        _context3.next = 3;
                                                                        return _this3.transitionInEffect();

                                                                  case 3:
                                                                        _this3._useToolTip = false;
                                                                        resolve();

                                                                  case 5:
                                                                  case "end":
                                                                        return _context3.stop();
                                                            }
                                                      }
                                                }, _callee3);
                                          }));

                                    return function (_x) {
                                          return _ref3.apply(this, arguments);
                                    };
                              }());
                  }
            }, {
                  key: "transitionOut",
                  value: function transitionOut(data) {
                        var _this4 = this;

                        return new Promise(
                              /*#__PURE__*/
                              function () {
                                    var _ref4 = _asyncToGenerator(
                                          /*#__PURE__*/
                                          regeneratorRuntime.mark(function _callee4(resolve) {
                                                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                                      while (1) {
                                                            switch (_context4.prev = _context4.next) {
                                                                  case 0:
                                                                        //this.selected = false;
                                                                        _this4.clearTween();

                                                                        _context4.next = 3;
                                                                        return _this4.transitionOutEffect();

                                                                  case 3:
                                                                        _this4._useToolTip = true;
                                                                        resolve();

                                                                  case 5:
                                                                  case "end":
                                                                        return _context4.stop();
                                                            }
                                                      }
                                                }, _callee4);
                                          }));

                                    return function (_x2) {
                                          return _ref4.apply(this, arguments);
                                    };
                              }());
                  }
                  /*_validateOutline() {
						this._setOutlineElement(MeshManager.requestMergeMesh(this.element));
				  }*/

            }, {
                  key: "_onValidateResource",
                  value: function _onValidateResource() {
                        _get(_getPrototypeOf(WV3DAssetComponent.prototype), "_onValidateResource", this).call(this);

                        this._validateState();
                  }
            }, {
                  key: "onLoadPage",
                  value: function onLoadPage() {
                        _get(_getPrototypeOf(WV3DAssetComponent.prototype), "onLoadPage", this).call(this);

                        if (this.isViewerMode && this.usingRenderer) {
                              this.composeJsonAnimation();
                        }
                  }
                  /*
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
						오버라이드 메서드 정의 - 실장 요소 마우스 이벤트 처리
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
				  */

            }, {
                  key: "_preventEvent",
                  value: function _preventEvent(event) {
                        event.stopPropagation();
                        event.origDomEvent.preventDefault();
                        event.origDomEvent.stopPropagation();
                        event.origDomEvent.stopImmediatePropagation();
                  }
            }, {
                  key: "_onChildMouseOver",
                  value: function _onChildMouseOver(event) {
                        if (this.isViewerMode) {
                              this.dispatchWScriptEvent("itemMouseOver", {
                                    target: this,
                                    selectItem: event.selectItem,
                                    type: "itemMouseOver"
                              });
                        }
                  }
            }, {
                  key: "_onChildMouseOut",
                  value: function _onChildMouseOut(event) {
                        if (this.isViewerMode) {
                              this.dispatchWScriptEvent("itemMouseOut", {
                                    target: this,
                                    selectItem: event.selectItem,
                                    type: "itemMouseOut"
                              });
                        }
                  }
            }, {
                  key: "_onChildClickHandler",
                  value: function _onChildClickHandler(event) {
                        if (this.isViewerMode) {
                              this.dispatchWScriptEvent("itemClick", {
                                    target: this,
                                    selectItem: event.selectItem,
                                    type: "itemClick"
                              });
                        }
                  }
            }, {
                  key: "_onChildDoubleClickHandler",
                  value: function _onChildDoubleClickHandler(event) {
                        if (this.isViewerMode) {
                              this.dispatchWScriptEvent("itemDblClick", {
                                    target: this,
                                    selectItem: event.selectItem,
                                    type: "itemDblClick"
                              });
                        }
                  }
            }, {
                  key: "_onIconLabelClickHandler",
                  value: function _onIconLabelClickHandler(event) {//console.log(event);
                  }
            }, {
                  key: "_onIconLabelDoubleClickHandler",
                  value: function _onIconLabelDoubleClickHandler(event) {} //console.log(event);

                  /*
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
					 컴포넌트 상태 표현 처리 / 이벤트 상태 표현 처리
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
				  */

            }, {
                  key: "changePropsData",
                  value: function () {
                        var _changePropsData = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee5(info) {
                                    return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                          while (1) {
                                                switch (_context5.prev = _context5.next) {
                                                      case 0:
                                                      case "end":
                                                            return _context5.stop();
                                                }
                                          }
                                    }, _callee5);
                              }));

                        function changePropsData(_x3) {
                              return _changePropsData.apply(this, arguments);
                        }

                        return changePropsData;
                  }()
            }, {
                  key: "changeNotification",
                  value: function changeNotification(noti) {
                        var type = noti.type;
                        var body = noti.body;

                        switch (type) {
                              case DCIMManager.NOTI_CHANGE_SEVERITY:
                                    this._state = body;

                                    this._validateState();

                                    break;

                              case DCIMManager.NOTI_CHANGE_ASSET_DATA:
                                    console.log("NOTI_CHANGE_ASSET_DATA", body);
                                    this._data = body;
                                    this.invalidateAssetData();
                                    break;

                              case DCIMManager.NOTI_CHANGE_PROPS_DATA:
                                    this.setGroupPropertyValue("settings", "popupId", body.popupId);
                                    this.setGroupProperties("resource", body.resource);
                                    this.data = body.data;
                                    this.invalidateAssetData();
                                    break;

                              case DCIMManager.NOTI_CHANGE_EVENT_DATA:
                                    if (body != null) {
                                          this.displayEventState(body);
                                    }

                                    break;

                              default:
                                    break;
                        }
                  }
                  /**
                   * 컴포넌트 고정상태 여부 반환
                   * 이벤트 데이터 폴링 중 or critical 상태 or 선택 상태
                   * */

            }, {
                  key: "isFixedState",
                  value: function isFixedState() {
                        return this._usePolling || this.isCriticalState() || this.selected;
                  }
                  /**
                   * 컴포넌트 선택 여부 설정
                   * 값에 따라 outline을 toggleSelectOutline으로 노출
                   * */

            }, {
                  key: "displayCurrentState",
                  value: function displayCurrentState() {
                        if (this.isFixedState()) return;
                        this.toggleStateDisplay(true);
                  }
            }, {
                  key: "hideCurrentState",
                  value: function hideCurrentState() {
                        if (this.isFixedState()) return;
                        this.toggleStateDisplay(false);
                  }
                  /**
                   *  fixed상태를 무시한 state값에 따른 자산상태 반영 토글
                   * */

            }, {
                  key: "toggleStateDisplay",
                  value: function toggleStateDisplay(bValue) {
                        if (!this.isAssetComp()) return;
                        this.toggleIconLabel(bValue);
                        this.toggleExtraState(bValue);
                  }
                  /**
                   * critical상태 일 경우 디스플레이 처리
                   * */

            }, {
                  key: "toggleCriticalDisplay",
                  value: function toggleCriticalDisplay(bValue) {
                        this.toggleStateDisplay(bValue);
                  }
                  /**
                   * 컴포넌트 별 추가적인 상태반영이 필요할 경우 override 하여 사용
                   * */

            }, {
                  key: "toggleExtraState",
                  value: function toggleExtraState(bValue) {}
            }, {
                  key: "displayEventState",

                  /**
                   * 자산 이벤트 데이터 정보 처리
                   * changeNotification에서 호출
                   * */
                  value: function displayEventState(info) {}
                  /**
                   * 노출된 자산 이벤트 데이터 정보 제거
                   * _usePolling값이 false일 경우 호출
                   * */

            }, {
                  key: "hideEventState",
                  value: function hideEventState() {} // 폴링 상태 변경에 따른 갱신 처리
                  // 이벤트 상태는 changeNotification으로 상태를 보여줌.
                  // 폴링 상태가 끝날 경우만 이벤트 상태 제거 처리

            }, {
                  key: "_validatePolling",
                  value: function _validatePolling() {
                        if (this._usePolling) {
                              this.toggleStateDisplay(true);
                        } else {
                              this.hideCurrentState();
                              this.hideEventState();
                        }
                  }
                  /*
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
					 자산 상태 표현을 위한 마우스 이벤트 오버라이드
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
				  */

            }, {
                  key: "_onMouseOverHandler",
                  value: function _onMouseOverHandler(event) {
                        if (this.isViewerMode) {
                              this.displayCurrentState();
                        }

                        _get(_getPrototypeOf(WV3DAssetComponent.prototype), "_onMouseOverHandler", this).call(this, event);
                  }
            }, {
                  key: "_onMouseOutHandler",
                  value: function _onMouseOutHandler(event) {
                        if (this.isViewerMode) {
                              this.hideCurrentState();
                        }

                        _get(_getPrototypeOf(WV3DAssetComponent.prototype), "_onMouseOutHandler", this).call(this, event);
                  }
                  /*
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
						오버라이드 메서드 정의
							  - 실장 구성 및 애니메이션 처리
				  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
				  */

            }, {
                  key: "hasChildAsset",
                  value: function hasChildAsset() {
                        //실장 데이터 여부를 반환
                        //필요 컴포넌트에서 override하여 사용
                        return false;
                  }
                  /**
                   * 실장 자산 컴포넌트 return
                   * @param assetID 실장 자산 assetId;
                   * */

            }, {
                  key: "getChildByAssetID",
                  value: function getChildByAssetID(assetID) {
                        for (var i = 0; i < this._children.length; i++) {
                              var asset = this._children[i];

                              if (asset.data.id == assetID) {
                                    return asset;
                              }
                        }

                        return null;
                  }
                  /**
                   * 실장 자산 컴포넌트 return
                   * @param strName 찾으려는 자산인스턴스 이름;
                   * */

            }, {
                  key: "getChildByAssetName",
                  value: function getChildByAssetName(strName) {
                        for (var i = 0; i < this._children.length; i++) {
                              var asset = this._children[i];

                              if (asset.name == strName) {
                                    return asset;
                              }
                        }

                        return null;
                  }
            }, {
                  key: "buildChildren",
                  value: function buildChildren() {//console.log("실장 구성은 여기를 오버라이드");
                  }
            }, {
                  key: "releaseChildren",
                  value: function releaseChildren() {//console.log("실장 정보 해지는 여기를 오버라이드");
                  }
            }, {
                  key: "transitionInEffect",
                  value: function transitionInEffect() {
                        return Promise.resolve("");
                  }
            }, {
                  key: "transitionOutEffect",
                  value: function transitionOutEffect() {
                        return Promise.resolve("");
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        if (this.usingRenderer) {
                              this.stop();
                              this.animation = null;
                              this.mixer = null;
                        }

                        this.releaseChildren();
                        this.iconLabelClickHandler = null;
                        this.iconLabelDoubleClickHandler = null;
                        this.childMouseOverHandler = null;
                        this.childMouseOutHandler = null;
                        this.childClickHandler = null;
                        this.childDoubleClickHandler = null;
                        this._data = null;
                        this.focusTween = null;

                        _get(_getPrototypeOf(WV3DAssetComponent.prototype), "_onDestroy", this).call(this);
                  }
            }, {
                  key: "usePolling",
                  set: function set(bValue) {
                        if (this._usePolling != bValue) {
                              this._usePolling = bValue;
                              this.deferredCall(this._validatePolling);
                        }
                  },
                  get: function get() {
                        return this._usePolling;
                  }
            }, {
                  key: "data",
                  set: function set(info) {
                        if (this._data != info) {
                              this._data = info;

                              if (this._data != null) {//this.invalidateAssetData();
                              }
                        }
                  },
                  get: function get() {
                        return this._data;
                  }
            }, {
                  key: "assetId",
                  get: function get() {
                        return this.data ? this.data.id : "";
                  }
            }, {
                  key: "assetType",
                  get: function get() {
                        return this.data ? this.data.asset_type : "";
                  }
            }, {
                  key: "popupId",
                  get: function get() {
                        var user = this.getGroupPropertyValue("override_settings", "popupId");

                        if (user) {
                              return user;
                        }

                        var config = this.getGroupPropertyValue("settings", "popupId");
                        return config;
                  }
            }, {
                  key: "toolTipMessage",
                  set: function set(value) {
                        this._toolTipMessage = value;
                  },
                  get: function get() {
                        return this._toolTipMessage;
                  }
            }, {
                  key: "toolTipTarget",
                  get: function get() {
                        return this._outlineElement;
                  }
            }, {
                  key: "useToolTip",
                  set: function set(bValue) {
                        this._useToolTip = bValue;
                  },
                  get: function get() {
                        return this._useToolTip;
                  }
            }, {
                  key: "iconLabel",
                  get: function get() {
                        if (this._iconLabel == null) {
                              this._iconLabel = new WV3DIconLabel();

                              this._iconLabel.label.css({
                                    "color": "white",
                                    "padding": "2px 4px",
                                    "background-color": "rgba(0,0,0,0.5)"
                              });

                              $(this._iconLabel.element).css({
                                    "text-align": "center"
                              });
                              $(this._iconLabel.element).on("click", this.iconLabelClickHandler);
                              $(this._iconLabel.element).on("dblclick", this.iconLabelDoubleClickHandler);
                              this.setIconLabelPosition();
                              this.iconLabel.text = this.data.name;
                        }

                        return this._iconLabel;
                  }
            }, {
                  key: "usingRenderer",
                  get: function get() {
                        if (this._resourceInfo.hasOwnProperty("path")) {
                              return !this.getResourceFileName(this._resourceInfo.path).indexOf(".obj") <= -1;
                        } else {
                              return false;
                        }
                  }
            }, {
                  key: "selected",
                  set: function set(bValue) {
                        _set(_getPrototypeOf(WV3DAssetComponent.prototype), "selected", bValue, this, true);

                        this.toggleSelectedOutline(bValue);
                  },
                  get: function get() {
                        return _get(_getPrototypeOf(WV3DAssetComponent.prototype), "selected", this);
                  }
            }]);

            return WV3DAssetComponent;
      }(WV3DResourceComponent);

WV3DAssetComponent.STATE = {
      DEFAULT: "default",
      NORMAL: "normal",
      MINOR: "minor",
      MAJOR: "major",
      WARNING: "warning",
      CRITICAL: "critical"
}; // WV3DAssetComponent.ICON_PATH = "./custom/assets/dcim_pack/WV3DAssetComponent/";

WV3DAssetComponent.ICON_PATH = "./custom/packs/dcim_pack/resource/";
WV3DPropertyManager.attach_default_component_infos(WV3DAssetComponent, {
      "setter": {
            "fixed": false
      },
      "bindingProperties": {},
      "label": {
            "label_text": "WV3DAssetComponent",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "WV3DAssetComponent",
            "version": "1.0.0"
      }
});
WVPropertyManager.add_property_panel_group_info(WV3DAssetComponent, {
      label: "popupId",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "popupId",
            type: "string",
            label: "popupId",
            show: true,
            writable: true,
            description: "popup id"
      }]
});
