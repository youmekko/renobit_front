class WVPlotterComponent  {

      constructor(){
            this.grayTexture;
            this.colorTexture;
            this.element;
            this.dataProvider =[];
            this.mapGenerator;
            this._useBackGround = false;
            this.divisor        = 10;
            this.space;
            this.type = "";
            this.mapInfo = {
                  temperature:{
                        colorGradient:{
                              0.2: '#00FFFF',
                              0.4: '#0099FF',
                              0.6: '#00FF00',
                              0.8: '#ffff00',
                              1.0: '#ff0000'
                        },
                        grayGradient:{
                              0.2: '#333333',
                              0.4: '#666666',
                              0.6: '#999999',
                              0.8: '#CCCCCC',
                              1.0: '#ffffff'
                        }
                  },
                  background:"black",
                  humidity:{
                        colorGradient:{
                              0.2: '#58b1cb',
                              0.4: '#02D5F9',
                              0.6: '#067acb',
                              0.8: '#0566cb',
                              1.0: '#004AFA'
                        },
                        grayGradient:{
                              0.2: '#333333',
                              0.4: '#666666',
                              0.6: '#999999',
                              0.8: '#CCCCCC',
                              1.0: '#ffffff'
                        }
                  }
            };
            this.tempSections = [0, 100];
            this.humiSections = [0, 100];
            this.displacementScale = 140;
      }

      // 맵 생성 요청 처리
      createMapWithSpace( space ) {

            this.space     = space;
            this.parentBoundingBox = new THREE.Box3().setFromObject(this.space.appendElement);
            let spaceSize  = this.space.size;
            let rw = this.getBestPowerOf2(+spaceSize.x>>1)/2;
            let rh = this.getBestPowerOf2(+spaceSize.z>>1)/2;
            let minSide = Math.min(rw, rh);
            let radius = (minSide/this.divisor)/2;
            let blur                = radius*1.4;
            this.mapGenerator       = new MapGenerator({width:rw, height:rh, radius:radius, blur:blur});
            this.changeType("temperature");

            let modelRate                 = spaceSize.z/spaceSize.x;
            const geometry                = new THREE.PlaneGeometry( spaceSize.x, spaceSize.z, 128, 128*modelRate);
            this.colorTexture             = new THREE.Texture( this.mapGenerator.colorLayer );
            this.colorTexture.needsUpdate = true;

            this.grayTexture              = new THREE.Texture( this.mapGenerator.grayLayer );
            this.grayTexture.needsUpdate  = true;

            const material                = new THREE.MeshPhongMaterial();
            material.side                 = THREE.DoubleSide;
            material.map                  = this.colorTexture;
            material.normapMap            = this.colorTexture;
            material.displacementMap      = this.grayTexture;
            material.normalMapScale       = new THREE.Vector2(1, -1);
            material.displacementScale    = this.displacementScale;
            material.displacementBias     =-0.5;
            material.transparent          = true;

            this.element                 = new THREE.Mesh( geometry, material );
            this.element.rotation.x      = -Math.PI/2;


      }

      /*convertSpacePosition( objPosition, areaInfo ){
            var xpos = (objPosition.x+(areaInfo.x/2))/areaInfo.x;
            var zpos = (objPosition.z+(areaInfo.z/2))/areaInfo.z;
            return {x:xpos, y:zpos};
      }*/

      remove(){
            this.parent.remove(this.element);
      }


      draw( parent ){
            this.parent = parent;
            this.parent.add( this.element );
      }

      get position(){
            return this.element.position;
      }

      set position( value ){
            this.element.position.set( value.x, value.y, value.z);
      }

      get rotation(){
            return this.element.rotation;
      }

      set rotation( value ){
            return this.element.rotation.set( value.x, value.y, value.z );
      }

      set useBackGround( value ){
            if(this._useBackGround != value ){
                  this._useBackGround = value;
            }
      }

      get useBackGround(){
            return this._useBackGround;
      }

      calculate( min, max, value ){
            return (value-min)/(max-min);
      }

      changeType(strType){
            if(this.type != strType){
                  this.type = strType;
                  let colors  = this.mapInfo[strType].colorGradient;
                  let grays   = this.mapInfo[strType].grayGradient;
                  this.mapGenerator.setGradient(colors, grays, this.useBackGround);
            }
      }

      setTempSections( objProvider ){
            this.tempSections = objProvider;
      }

      setHumiSections( objProvider ){
            this.humiSections = objProvider;
      }

      setTempColorGradient( objColors ){
            this.mapInfo.temperature.colorGradient = objColors;
      }

      setHumiColorGradient( objColors ){
            this.mapInfo.humidity.colorGradient = objColors;
      }

      paint( info ){
            let isTemp  = (info.type == "temperature" );
            let min     = isTemp ? this.tempSections[0] : this.humiSections[0];
            let max     = isTemp ? this.tempSections[1] : this.humiSections[1];
            let value   = isTemp ? info.temp : info.humi;
            let force   = this.calculate( min, max, value );
            let spacePosition = this.parentBoundingBox.getParameter(info.object.appendElement.position );
            //let spacePosition = this.convertSpacePosition( this.space.appendElement.worldToLocal( info.object.appendElement.position.clone() ) , this.space.size );
            let px = spacePosition.x * this.colorTexture.image.width;
            let py = spacePosition.z * this.colorTexture.image.height;
            this.mapGenerator.add( px, py, force);
      }

      addItem( item ){
            this.dataProvider.push(item);
      }

      setDataProvider( objProvider ){
            if(this.dataProvider != objProvider ){
                  this.dataProvider = objProvider;
                  this.update();
            }
      }

      getBestPowerOf2( value ){
            let MAX_SIZE = 4096;
            var p = 1;
            while (p < value)
                  p <<= 1;

            if (p > MAX_SIZE)
                  p = MAX_SIZE;
            return p;
      }

      adjustSize( size ){
            let results = {size:size, scale:1};
            while( results.size>2000){
                  results.size>>=1;
                  results.scale*=2;
            }
            return results;
      }

      clear(){
            this.mapGenerator.clear();
            this.mapGenerator.draw();
            this.colorTexture.needsUpdate=true;
            this.grayTexture.needsUpdate=true;
      }

      update(){
            this.clear();
            let max = this.dataProvider.length;
            let sensorVO, idx;
            for(idx=0; idx<max; idx++){
                  sensorVO = this.dataProvider[idx];
                  sensorVO.type = this.type;
                  this.paint( sensorVO );
            }

            this.mapGenerator.draw();
            this.colorTexture.needsUpdate=true;
            this.grayTexture.needsUpdate=true;
      }

      set visible( value ) {
            this.element.material.visible=value;
      }

      get visible() {
            return this.element.material.visible;
      }

      destroy(){
            // scene에서 element제거
            this.parent.remove(this.element);
            MeshManager.disposeMesh(this.element);
            this.grayTexture.dispose();
            this.colorTexture.dispose();
            this.mapGenerator.destroy();
            this.mapGenerator = null;
            this.colorTexture = null;
            this.grayTexture  = null;
            this.dataProvider = null;
            this.parent       = null;
      }

}


class MapGenerator {

      constructor(opt){
            this.blur               =opt.blur;
            this.radius             =opt.radius;
            this.colorLayer         =document.createElement("canvas");
            this.colorLayer.width   =opt.width;
            this.colorLayer.height  =opt.height;
            this.grayLayer          =this.cloneCanvas(this.colorLayer);
            this.heatmap            =simpleheat(this.colorLayer);
            this.graymap            =simpleheat(this.grayLayer);
            this.heatmap.radius(opt.radius, opt.blur).max(1);
            this.graymap.radius(opt.radius, opt.blur).max(1);
      }

      setGradient( colorGradient, grayGradient, useBackGround ){
            this.heatmap.gradient(colorGradient);
            this.graymap.gradient(grayGradient);
            // 배경 사용 시 가장 낮은 컬러값을 적용
            if(useBackGround){
                  let prop = Object.keys(colorGradient).sort();

                  this.heatmap.bgColor(colorGradient[prop[0]]);
                  this.graymap.bgColor(grayGradient[prop[0]]);
            }
      }

      cloneCanvas( origin ){
            var clone = document.createElement("canvas");
            clone.width  =origin.width;
            clone.height =origin.height;
            var context =clone.getContext("2d");
            context.drawImage(origin, 0, 0);
            return clone;
      }

      add( x, y, value ){
            this.heatmap.add([x, y, value]);
            this.graymap.add([x, y, value]);
      }

      clear(){
            this.heatmap.clear();
            this.graymap.clear();
      }

      draw(){
            this.heatmap.draw();
            this.graymap.draw();
      }

      destroy(){
            this.heatmap = null;
            this.graymap = null;
            this.colorLayer = null;
            this.grayLayer  = null;
      }

}
