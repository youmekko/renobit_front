class DirectionArrow extends WV3DResourceComponent {

      constructor(){super();}

      get direction() {
            return this.getGroupPropertyValue("setter", "direction");
      }

      set direction( value ){
            this._checkUpdateGroupPropertyValue("setter", "direction", value );
      }

}



WV3DPropertyManager.attach_default_component_infos(DirectionArrow, {
      "setter": {
            "state"  : "normal"
      },
      "label": {
            "label_text": "DirectionArrow",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "DirectionArrow",
            "version": "1.0.0",
      }
});

WV3DPropertyManager.removePropertyGroupByName(DirectionArrow.property_panel_info, "label", "label");
