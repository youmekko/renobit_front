"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var CCTV =
      /*#__PURE__*/
      function (_WV3DAssetComponent) {
            _inherits(CCTV, _WV3DAssetComponent);

            function CCTV() {
                  _classCallCheck(this, CCTV);

                  return _possibleConstructorReturn(this, _getPrototypeOf(CCTV).call(this));
            }

            _createClass(CCTV, [{
                  key: "_onCreateElement",
                  value: function _onCreateElement() {
                        _get(_getPrototypeOf(CCTV.prototype), "_onCreateElement", this).call(this);

                        this.camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, -1, -10);
                        this.sightMaterial = new THREE.MeshPhongMaterial({
                              color: 0xffff00,
                              transparent: true,
                              opacity: 0.5
                        });

                        if (this.isEditorMode) {
                              this.cameraHelper = new WVCameraHelper(this.camera);
                        }
                  }
            }, {
                  key: "hasAssetIcon",
                  value: function hasAssetIcon() {
                        return true;
                  }
            }, {
                  key: "_createCameraSightGeometry",
                  value: function _createCameraSightGeometry() {
                        var toRadian = Math.PI / 180;
                        var nearH = 2 * Math.tan(this.camera.fov * toRadian / 2) * this.camera.near;
                        var farH = 2 * Math.tan(this.camera.fov * toRadian / 2) * this.camera.far;
                        var narW = farH * this.camera.aspect;
                        var farR = Math.sqrt(narW * narW + farH * farH) / 2;
                        var geometry = new THREE.ConeGeometry(farR / this.camera.zoom, farH - nearH, 16, 1, true, 0, Math.PI * 2);
                        geometry.computeBoundingBox();
                        geometry.translate(0, geometry.boundingBox.max.y, 0);
                        return geometry;
                  }
            }, {
                  key: "_onCommitProperties",
                  value: function _onCommitProperties() {
                        _get(_getPrototypeOf(CCTV.prototype), "_onCommitProperties", this).call(this);

                        if (this._updatePropertiesMap.has("cameraInfo")) {
                              this.validateCallLater(this._validateCameraInfo);
                        }
                  }
            }, {
                  key: "_onImmediateUpdateDisplay",
                  value: function _onImmediateUpdateDisplay() {
                        _get(_getPrototypeOf(CCTV.prototype), "_onImmediateUpdateDisplay", this).call(this);

                        this._validateCameraInfo();
                  }
            }, {
                  key: "_validateCameraInfo",
                  value: function _validateCameraInfo() {
                        var cameraInfo = this.getGroupProperties("cameraInfo");
                        if(cameraInfo.far === 0)
                              cameraInfo.far = 1;
                        if(cameraInfo.zoom === 0)
                              cameraInfo.zoom = 1;
                        this.camera.rotation.x = cameraInfo.tilt * Math.PI / 180;
                        this.camera.far = cameraInfo.far * -1;
                        this.camera.zoom = cameraInfo.zoom;
                        this.render();
                  }
            }, {
                  key: "composeResource",
                  value: function composeResource(loadedObject) {
                        _get(_getPrototypeOf(CCTV.prototype), "composeResource", this).call(this, loadedObject);

                        if (this.isEditorMode) {
                              this.appendElement.add(this.cameraHelper);
                        }
                  }
            }, {
                  key: "getExtensionProperties",
                  value: function getExtensionProperties() {
                        return this.isAssetComp();
                  }
                  /*
                  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
                      컴포넌트 상태 표현 처리 / 이벤트 상태 표현 처리
                  ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
                   */

            }, {
                  key: "toggleExtraState",
                  value: function toggleExtraState(bValue) {
                        if (bValue) {
                              this.displayCameraRange();
                        } else {
                              this.hideCameraRange();
                        }
                  } // object내부 mesh컬러 변경 처리 함수

            }, {
                  key: "_validateOpacity",
                  value: function _validateOpacity() {
                        _get(_getPrototypeOf(CCTV.prototype), "_validateOpacity", this).call(this);

                        var cameraSight = this.element.getObjectByName(CCTV.SIGHT_ID);

                        if (cameraSight) {
                              cameraSight.material.transparent = true;
                              cameraSight.material.opacity = 0.5;
                        }
                  }
            }, {
                  key: "createCameraRange",
                  value: function createCameraRange() {
                        var cameraSight = this.element.getObjectByName(CCTV.SIGHT_ID);

                        if (!cameraSight) {
                              cameraSight = new THREE.Mesh(this._createCameraSightGeometry(), this.sightMaterial);
                              cameraSight.rotation.x = this.camera.rotation.x + 90 * Math.PI / 180;
                              cameraSight.name = CCTV.SIGHT_ID;
                              this.element.add(cameraSight);
                        }

                        return cameraSight;
                  }
            }, {
                  key: "displayCameraRange",
                  value: function displayCameraRange() {
                        var severity = this._state; //기본상태일 경우 불빛을 파란색으로 설정

                        if (severity == WV3DAssetComponent.STATE.DEFAULT) {
                              severity = WV3DAssetComponent.STATE.NORMAL;
                        }

                        this.changeCameraRangeSeverity(severity);
                  }
            }, {
                  key: "hideCameraRange",
                  value: function hideCameraRange() {
                        var cameraSight = this.element.getObjectByName(CCTV.SIGHT_ID);

                        if (cameraSight) {
                              this.element.remove(cameraSight);
                              cameraSight.geometry.dispose();
                        }
                  }
            }, {
                  key: "changeCameraRangeSeverity",
                  value: function changeCameraRangeSeverity(severity) {
                        var cameraSight = this.createCameraRange();
                        var color = this.getAssetColorFromState(severity);
                        cameraSight.material.color.set(color);
                  }
            }, {
                  key: "render",
                  value: function render() {
                        this.camera.updateMatrixWorld();
                        this.camera.updateProjectionMatrix();

                        if (this.isEditorMode) {
                              this.cameraHelper.update();
                        }
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        if (this.isEditorMode) {
                              this.appendElement.remove(this.cameraHelper);
                              MeshManager.disposeMesh(this.cameraHelper);
                        }

                        this.sightMaterial.dispose();
                        this.sightMaterial = null;
                        this.camera = null;

                        _get(_getPrototypeOf(CCTV.prototype), "_onDestroy", this).call(this);
                  }
            }]);

            return CCTV;
      }(WV3DAssetComponent);

CCTV.SIGHT_ID = "sight";
WV3DPropertyManager.attach_default_component_infos(CCTV, {
      "cameraInfo": {
            "tilt": 0,
            "zoom": 2,
            "far": 10
      },
      "label": {
            "label_text": "CCTV",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "CCTV",
            "version": "1.0.0"
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      },
      "extension": {
            "associatedAsset": []
      }
});
WVPropertyManager.add_property_panel_group_info(CCTV, {
      template: "dcim-asset-id"
});
WVPropertyManager.add_property_panel_group_info(CCTV, {
      template: 'dcim-asset-popup'
});
WV3DPropertyManager.add_property_panel_group_info(CCTV, {
      label: "Camera Properties",
      template: "vertical",
      children: [{
            owner: "cameraInfo",
            name: "tilt",
            type: "number",
            label: "Tilt",
            show: true,
            writable: true,
            description: "tilt"
      }, {
            owner: "cameraInfo",
            name: "zoom",
            type: "number",
            label: "Zoom",
            show: true,
            writable: true,
            description: "zoom"
      }, {
            owner: "cameraInfo",
            name: "far",
            type: "number",
            label: "Far",
            show: true,
            writable: true,
            description: "far"
      }]
});
