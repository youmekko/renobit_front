class CCTV extends WV3DAssetComponent {

      constructor() {
            super();
      }

      _onCreateElement() {
            super._onCreateElement();
            this.camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, -1, -10);
            this.sightMaterial = new THREE.MeshPhongMaterial({color: 0xffff00, transparent: true, opacity: 0.5});
            if (this.isEditorMode) {
                  this.cameraHelper = new WVCameraHelper(this.camera);
            }
      }

      hasAssetIcon() {
            return true;
      }

      _createCameraSightGeometry() {
            let toRadian = Math.PI / 180;
            let nearH = 2 * Math.tan(this.camera.fov * toRadian / 2) * this.camera.near;
            let farH = 2 * Math.tan(this.camera.fov * toRadian / 2) * this.camera.far;
            let narW = farH * this.camera.aspect;
            let farR = Math.sqrt(narW * narW + farH * farH) / 2;
            let geometry = new THREE.ConeGeometry(farR / this.camera.zoom, farH - nearH, 16, 1, true, 0, Math.PI*2);
            geometry.computeBoundingBox();
            geometry.translate(0, geometry.boundingBox.max.y, 0);
            return geometry;
      }

      _onCommitProperties() {
            super._onCommitProperties();
            if (this._updatePropertiesMap.has("cameraInfo")) {
                  this.validateCallLater(this._validateCameraInfo);
            }
      }

      _onImmediateUpdateDisplay() {
            super._onImmediateUpdateDisplay();
            this._validateCameraInfo();
      }

      _validateCameraInfo() {
            let cameraInfo = this.getGroupProperties("cameraInfo");

            //cone geometry 에서 far, zoom 값 중 하나라도 0이면 에러가 난다. 0으로 입력했다면 1로 강제(vertical template에서 처리 불가)
            if(cameraInfo.far === 0)
                  cameraInfo.far = 1;
            if(cameraInfo.zoom === 0)
                  cameraInfo.zoom = 1;
            this.camera.rotation.x = (cameraInfo.tilt * Math.PI / 180);
            this.camera.far = cameraInfo.far * -1;
            this.camera.zoom = cameraInfo.zoom;
            this.render();
      }

      composeResource(loadedObject) {
            super.composeResource(loadedObject);
            if (this.isEditorMode) {
                  this.appendElement.add(this.cameraHelper);
            }

      }


      getExtensionProperties() {
            return this.isAssetComp();
      }

      /*
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
          컴포넌트 상태 표현 처리 / 이벤트 상태 표현 처리
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       */
      toggleExtraState(bValue) {
            if (bValue) {
                  this.displayCameraRange();
            } else {
                  this.hideCameraRange();
            }
      }

      // object내부 mesh컬러 변경 처리 함수
      _validateOpacity() {
            super._validateOpacity();
            let cameraSight = this.element.getObjectByName(CCTV.SIGHT_ID);
            if(cameraSight){
                  cameraSight.material.transparent=true;
                  cameraSight.material.opacity = 0.5;
            }
      }



      createCameraRange(){
            let cameraSight = this.element.getObjectByName(CCTV.SIGHT_ID);
            if(!cameraSight){
                  cameraSight = new THREE.Mesh(this._createCameraSightGeometry(), this.sightMaterial);
                  cameraSight.rotation.x = this.camera.rotation.x + (90 * Math.PI / 180);
                  cameraSight.name = CCTV.SIGHT_ID;
                  this.element.add(cameraSight);
            }
            return cameraSight;
      }

      displayCameraRange() {
            let severity = this._state;
            //기본상태일 경우 불빛을 파란색으로 설정
            if(severity == WV3DAssetComponent.STATE.DEFAULT) {
                  severity = WV3DAssetComponent.STATE.NORMAL;
            }
            this.changeCameraRangeSeverity(severity);
      }

      hideCameraRange() {
            let cameraSight = this.element.getObjectByName(CCTV.SIGHT_ID);
            if (cameraSight) {
                  this.element.remove(cameraSight);
                  cameraSight.geometry.dispose();
            }
      }

      changeCameraRangeSeverity( severity ){
            let cameraSight = this.createCameraRange();
            let color = this.getAssetColorFromState(severity);
            cameraSight.material.color.set(color);
      }

      render() {
            this.camera.updateMatrixWorld();
            this.camera.updateProjectionMatrix();
            if (this.isEditorMode) {
                  this.cameraHelper.update();
            }
      }

      _onDestroy() {
            if (this.isEditorMode) {
                  this.appendElement.remove(this.cameraHelper);
                  MeshManager.disposeMesh(this.cameraHelper);
            }
            this.sightMaterial.dispose();
            this.sightMaterial = null;
            this.camera = null;
            super._onDestroy();
      }


}

CCTV.SIGHT_ID = "sight"

WV3DPropertyManager.attach_default_component_infos(CCTV, {

      "cameraInfo": {
            "tilt": 0,
            "zoom": 2,
            "far": 10,
      },
      "label": {
            "label_text": "CCTV",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "CCTV",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      },
      "extension": {
            "associatedAsset": []
      }
});

WVPropertyManager.add_property_panel_group_info(CCTV, {
      template: "dcim-asset-id"
})

WVPropertyManager.add_property_panel_group_info(CCTV, {
      template: 'dcim-asset-popup'
})


WV3DPropertyManager.add_property_panel_group_info(CCTV, {
      label: "Camera Properties",
      template: "vertical",
      children: [{
            owner: "cameraInfo",
            name: "tilt",
            type: "number",
            label: "Tilt",
            show: true,
            writable: true,
            description: "tilt"
      },
            {
                  owner: "cameraInfo",
                  name: "zoom",
                  type: "number",
                  label: "Zoom",
                  show: true,
                  writable: true,
                  description: "zoom"
            },
            {
                  owner: "cameraInfo",
                  name: "far",
                  type: "number",
                  label: "Far",
                  show: true,
                  writable: true,
                  description: "far"
            }
      ]
});
