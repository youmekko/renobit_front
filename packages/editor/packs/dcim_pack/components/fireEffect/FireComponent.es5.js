"use strict";

var _createClass = function() {
    function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor); } } return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _asyncToGenerator(fn) { return function() { var gen = fn.apply(this, arguments); return new Promise(function(resolve, reject) {
            function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function(value) { step("next", value); }, function(err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); }
    subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FireComponent = function(_WV3DResourceComponen) {
    _inherits(FireComponent, _WV3DResourceComponen);

    function FireComponent() {
        _classCallCheck(this, FireComponent);

        return _possibleConstructorReturn(this, (FireComponent.__proto__ || Object.getPrototypeOf(FireComponent)).call(this));
    }

    _createClass(FireComponent, [{
        key: "_onCreateProperties",
        value: function _onCreateProperties() {
            _get(FireComponent.prototype.__proto__ || Object.getPrototypeOf(FireComponent.prototype), "_onCreateProperties", this).call(this);
            this._elementSize = this.properties.elementSize || { x: 1, y: 1, z: 1 };
        }
    }, {
        key: "_onCommitProperties",
        value: function _onCommitProperties() {
            _get(FireComponent.prototype.__proto__ || Object.getPrototypeOf(FireComponent.prototype), "_onCommitProperties", this).call(this);
            if (this._resourceLoaded && this.isEditorMode) {
                this.validateCallLater(this.refreshRender);
            }
        }
    }, {
        key: "_validateSize",
        value: function _validateSize() {
            var value = this.size;
            if (this._resourceLoaded) {
                this.element.scale.set(value.x, value.y, value.z);
                var fireSize = ThreeUtil.getObjectSize(this.element);
                this.element.position.y = 0;
                this.element.position.y += fireSize.y / 2;
            }
        }

        /*
        일반적으로 controller에 의해서 움직인 값을 setter에 적용할 때 사용.
         */

    }, {
        key: "syncLocationSizeElementPropsComponentProps",
        value: function syncLocationSizeElementPropsComponentProps() {
            var degree = 180 / Math.PI;
            this._properties.setter.position.x = parseInt(this.appendElement.position.x);
            this._properties.setter.position.y = parseInt(this.appendElement.position.y);
            this._properties.setter.position.z = parseInt(this.appendElement.position.z);

            this._properties.setter.rotation.x = (this.appendElement.rotation.x * degree).toFixed(0);
            this._properties.setter.rotation.y = (this.appendElement.rotation.y * degree).toFixed(0);
            this._properties.setter.rotation.z = (this.appendElement.rotation.z * degree).toFixed(0);

            // elementSize정보가 없을 경우 size값을 이용
            var sizeInfo = this.size;
            this._properties.setter.size.x = +this.appendElement.scale.x * +sizeInfo.x;
            this._properties.setter.size.y = +this.appendElement.scale.y * +sizeInfo.y;
            this._properties.setter.size.z = +this.appendElement.scale.z * +sizeInfo.z;
            this._validateLabel();
            this.refreshRender();
        }
    }, {
        key: "validateResource",
        value: function() {
            var _ref = _asyncToGenerator( /*#__PURE__*/ regeneratorRuntime.mark(function _callee() {
                var resourceInfo, path, texture;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;

                                this.removePrevResource();
                                resourceInfo = this.getGroupPropertyValue("setter", "resource");
                                path = resourceInfo.texturePath;
                                texture = NLoaderManager.textureLoader.load(path);

                                this._element = new THREE.Fire(texture);
                                this._element.material.uniforms.magnitude.value = 1.3;
                                this._element.material.uniforms.gain.value = 0.4;
                                this._element.material.uniforms.lacunarity.value = 1.4;
                                this._element.material.uniforms.noiseScale.value = new THREE.Vector4(1, 2, 1, 0.3);
                                this._element.renderOrder = 5000;
                                this.appendElement.add(this._element);
                                _context.next = 17;
                                break;

                            case 14:
                                _context.prev = 14;
                                _context.t0 = _context["catch"](0);
                                throw _context.t0;

                            case 17:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [
                    [0, 14]
                ]);
            }));

            function validateResource() {
                return _ref.apply(this, arguments);
            }

            return validateResource;
        }()
    }, {
        key: "_onValidateResource",
        value: function _onValidateResource() {
            this._resourceLoaded = true;
            this._validateSize();
        }
    }, {
        key: "refreshRender",
        value: function refreshRender() {
            if (this.clock) {
                this.clock.getDelta();
                this._element.update(this.clock.elapsedTime);
            }
        }
    }, {
        key: "render",
        value: function render() {
            if (this.clock && this.isViewerMode && this.visible) {
                this.clock.getDelta();
                this._element.update(this.clock.elapsedTime * 1.4);
            }
        }
    }, {
        key: "startLoadResource",
        value: function() {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/ regeneratorRuntime.mark(function _callee2() {
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                _context2.next = 3;
                                return this.validateResource();

                            case 3:
                                this._onValidateResource();
                                _context2.next = 9;
                                break;

                            case 6:
                                _context2.prev = 6;
                                _context2.t0 = _context2["catch"](0);

                                CPLogger.log(CPLogger.getCallerInfo(this, "startLoadResource"), [_context2.t0.toString()]);

                            case 9:
                                _context2.prev = 9;

                                this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
                                return _context2.finish(9);

                            case 12:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [
                    [0, 6, 9, 12]
                ]);
            }));

            function startLoadResource() {
                return _ref2.apply(this, arguments);
            }

            return startLoadResource;
        }()
    }, {
        key: "onLoadPage",
        value: function onLoadPage() {
            _get(FireComponent.prototype.__proto__ || Object.getPrototypeOf(FireComponent.prototype), "onLoadPage", this).call(this);
            this.clock = new THREE.Clock();
            if (this.isEditorMode) {
                this.clock.getDelta();
                this._element.update(this.clock.elapsedTime);
            }
        }
    }, {
        key: "usingRenderer",
        get: function get() {
            return true;
        }
    }]);

    return FireComponent;
}(WV3DResourceComponent);

WV3DPropertyManager.attach_default_component_infos(FireComponent, {
    "setter": {
        "size": { x: 50, y: 50, z: 50 }
    },
    "label": {
        "label_text": "FireComponent",
        "label_using": false
    },

    "info": {
        "componentName": "FireComponent",
        "version": "1.0.0"
    }
});