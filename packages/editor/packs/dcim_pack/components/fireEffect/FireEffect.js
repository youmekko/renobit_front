class FireEffect {
      constructor(){
            this._parent      = null;
            this._element     = null;
            this._clock       = new THREE.Clock();
            this._animateID   = null;
            this._fire        = null;
            this.create();
      }

      create(){
            let texture   = NLoaderManager.textureLoader.load("./client/common/assets/effect/Fire.png");
            this._element =new THREE.Fire(texture);
            this._element.material.uniforms.magnitude.value = 1.3;
            this._element.material.uniforms.gain.value      = 0.4;
            this._element.material.uniforms.lacunarity.value      = 1.4;
            this._element.material.uniforms.noiseScale.value = new THREE.Vector4(1, 2, 1, 0.3);
            this._element.material.side = THREE.DoubleSide;
            //this._element.material.depthTest=true;
            this._element.renderOrder = 10;
      }

      draw( parent ) {
            this._parent = parent;
            this._parent.appendElement.add(this._element);
            let size = Math.floor( Math.min(300, Math.max(parent.size.x, parent.size.z)) );
            // 3D Object 1/10 처리에 대한 Fire Effect 조정. 
            if(size >= 300){
                  this._element.scale.set(size, size*2.2, size); // Origin
                  this._element.material.uniforms.rayLen.value = 0.0288;
            }else{
                  this._element.scale.set(300, 660, 300);
                  this._element.material.uniforms.rayLen.value = 0.00288;
            }         
            this._element.position.y = this._element.scale.y/2;
      }

      get size(){
            return this._element.scale;
      }

      set size( value ){
            this._element.scale.set( value.x, value.y, value.z );
      }

      get position(){
            return this._element.position;
      }

      set position( value ){
            for( let prop in value ){
                  this._element.position[prop] = value[prop];
            }
      }

      animate(){
            this._clock.getDelta();
            this._element.update(this._clock.elapsedTime);
            this._animateID = requestAnimationFrame(()=>{
                  this.animate();
            });
      }

      destroy(){
            cancelAnimationFrame(this._animateID);
            this._parent.appendElement.remove(this._element);
            MeshManager.disposeMesh(this._element);
            this._parent  = null;
            this._element = null;
            this._fire    = null;
      }

}
