"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var FireEffect =
/*#__PURE__*/
function () {
  function FireEffect() {
    _classCallCheck(this, FireEffect);

    this._parent = null;
    this._element = null;
    this._clock = new THREE.Clock();
    this._animateID = null;
    this._fire = null;
    this.create();
  }

  _createClass(FireEffect, [{
    key: "create",
    value: function create() {
      var texture = NLoaderManager.textureLoader.load("./client/common/assets/effect/Fire.png");
      this._element = new THREE.Fire(texture);
      this._element.material.uniforms.magnitude.value = 1.3;
      this._element.material.uniforms.gain.value = 0.4;
      this._element.material.uniforms.lacunarity.value = 1.4;
      this._element.material.uniforms.noiseScale.value = new THREE.Vector4(1, 2, 1, 0.3);
      this._element.material.side = THREE.DoubleSide; //this._element.material.depthTest=true;

      this._element.renderOrder = 10;
    }
  }, {
    key: "draw",
    value: function draw(parent) {
      this._parent = parent;

      this._parent.appendElement.add(this._element);

      var size = Math.floor(Math.min(300, Math.max(parent.size.x, parent.size.z))); // 3D Object 1/10 처리에 대한 Fire Effect 조정. 

      if (size >= 300) {
        this._element.scale.set(size, size * 2.2, size); // Origin


        this._element.material.uniforms.rayLen.value = 0.0288;
      } else {
        this._element.scale.set(300, 660, 300);

        this._element.material.uniforms.rayLen.value = 0.00288;
      }

      this._element.position.y = this._element.scale.y / 2;
    }
  }, {
    key: "animate",
    value: function animate() {
      var _this = this;

      this._clock.getDelta();

      this._element.update(this._clock.elapsedTime);

      this._animateID = requestAnimationFrame(function () {
        _this.animate();
      });
    }
  }, {
    key: "destroy",
    value: function destroy() {
      cancelAnimationFrame(this._animateID);

      this._parent.appendElement.remove(this._element);

      MeshManager.disposeMesh(this._element);
      this._parent = null;
      this._element = null;
      this._fire = null;
    }
  }, {
    key: "size",
    get: function get() {
      return this._element.scale;
    },
    set: function set(value) {
      this._element.scale.set(value.x, value.y, value.z);
    }
  }, {
    key: "position",
    get: function get() {
      return this._element.position;
    },
    set: function set(value) {
      for (var prop in value) {
        this._element.position[prop] = value[prop];
      }
    }
  }]);

  return FireEffect;
}();