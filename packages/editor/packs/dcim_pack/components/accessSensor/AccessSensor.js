class AccessSensor extends WV3DAssetComponent {

      constructor() {
            super()
      }

      _onCreateProperties() {
            super._onCreateProperties();
            this.focusTween = null;
      }

      hasAssetIcon() {
            return true;
      }

      clearTween() {
            if (this.focusTween != null && this.focusTween.isActive()) {
                  this.focusTween.pause();
                  this.focusTween.kill();
            }
      }

      transitionIn(data) {
            return Promise.resolve();
      }

      transitionOut(data) {
            return Promise.resolve();
      }


      get isLeftDoor() {
            return this.door.name.toLowerCase().indexOf("left") > -1;
      }

      composeResource(loadedObject) {
            super.composeResource(loadedObject);
            try {
                  this.door = loadedObject.getObjectByName("doorLeft_A") || loadedObject.getObjectByName("doorRight_A");
                  this.door.geometry.computeBoundingBox();
            } catch(error) {
                  console.log('access door modeling error');
            }
      }

      transitionInEffect() {
            this.clearTween();
            if(this.door) {
                  let doorBox = this.door.geometry.boundingBox;
                  let targetValue = doorBox.max.x;
                  if (this.isLeftDoor) targetValue *= -1;
                  return new Promise((resolve) => {
                        this.focusTween = TweenMax.to(this.door.position, 0.5, {
                              x: targetValue,
                              onComplete: () => {
                                    resolve();
                              }
                        });
                  });
            } else {
                  return new Promise((resolve) => {
                        resolve();
                  })
            }
      }

      transitionOutEffect() {
            this.clearTween();
            return new Promise( (resolve) => {
                  if(this.door) {
                        this.focusTween = TweenMax.to(this.door.position, 0.5, {
                              x: 0,
                              onComplete: () => {
                                    resolve();
                              }
                        });
                  } else {
                        resolve();
                  }
            });
      }

      displayEventState(info) {
            if (info.status == "Y") {
                  this.transitionInEffect();
                  this._label.text = info.msg;
                  this._label.labelPosition = new THREE.Vector3(0, parseInt(this.size.y*1.2), 0);
                  this._label.show();
            } else {
                  this.transitionOutEffect();
                  this._label.hide();
            }
      }

      hideEventState() {
            this.transitionOutEffect();
            this._label.hide();
      }

      getExtensionProperties() {
            return this.isAssetComp();
      }

}


WV3DPropertyManager.attach_default_component_infos(AccessSensor, {

      "label": {
            "label_text": "AccessSensor",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "AccessSensor",
            "version": "1.0.0",
      },

      "extension": {
            "associatedAsset": []
      },

      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(AccessSensor, {
      template: "dcim-asset-id"
})

WVPropertyManager.add_property_panel_group_info(AccessSensor, {
      template: 'dcim-asset-popup'
})
