function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var AccessSensor =
      /*#__PURE__*/
      function (_WV3DAssetComponent) {
            "use strict";

            _inherits(AccessSensor, _WV3DAssetComponent);

            function AccessSensor() {
                  _classCallCheck(this, AccessSensor);

                  return _possibleConstructorReturn(this, _getPrototypeOf(AccessSensor).call(this));
            }

            _createClass(AccessSensor, [{
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(AccessSensor.prototype), "_onCreateProperties", this).call(this);

                        this.focusTween = null;
                  }
            }, {
                  key: "hasAssetIcon",
                  value: function hasAssetIcon() {
                        return true;
                  }
            }, {
                  key: "clearTween",
                  value: function clearTween() {
                        if (this.focusTween != null && this.focusTween.isActive()) {
                              this.focusTween.pause();
                              this.focusTween.kill();
                        }
                  }
            }, {
                  key: "transitionIn",
                  value: function transitionIn(data) {
                        return Promise.resolve();
                  }
            }, {
                  key: "transitionOut",
                  value: function transitionOut(data) {
                        return Promise.resolve();
                  }
            }, {
                  key: "composeResource",
                  value: function composeResource(loadedObject) {
                        _get(_getPrototypeOf(AccessSensor.prototype), "composeResource", this).call(this, loadedObject);
                        try {
                              this.door = loadedObject.getObjectByName("doorLeft_A") || loadedObject.getObjectByName("doorRight_A");
                              this.door.geometry.computeBoundingBox();
                        } catch (error) {
                              console.log('error access modeling');
                        }
                  }
            }, {
                  key: "transitionInEffect",
                  value: function transitionInEffect() {
                        var _this = this;

                        this.clearTween();
                        if(this.door) {
                              var doorBox = this.door.geometry.boundingBox;
                              var targetValue = doorBox.max.x;
                              if (this.isLeftDoor) targetValue *= -1;
                              return new Promise(function (resolve) {
                                    _this.focusTween = TweenMax.to(_this.door.position, 0.5, {
                                          x: targetValue,
                                          onComplete: function onComplete() {
                                                resolve();
                                          }
                                    });
                              });
                        } else {
                              return new Promise(function(resolve) {
                                    resolve();
                              })
                        }
                  }
            }, {
                  key: "transitionOutEffect",
                  value: function transitionOutEffect() {
                        var _this2 = this;

                        this.clearTween();

                        if(_this2.door) {
                              return new Promise(function (resolve) {
                                    _this2.focusTween = TweenMax.to(_this2.door.position, 0.5, {
                                          x: 0,
                                          onComplete: function onComplete() {
                                                resolve();
                                          }
                                    });
                              });
                        } else {
                              return new Promise(function(resolve) {
                                    resolve();
                              })
                        }
                  }
            }, {
                  key: "displayEventState",
                  value: function displayEventState(info) {
                        if (info.status == "Y") {
                              this.transitionInEffect();
                              this._label.text = info.msg;
                              this._label.labelPosition = new THREE.Vector3(0, parseInt(this.size.y * 1.2), 0);

                              this._label.show();
                        } else {
                              this.transitionOutEffect();

                              this._label.hide();
                        }
                  }
            }, {
                  key: "hideEventState",
                  value: function hideEventState() {
                        this.transitionOutEffect();

                        this._label.hide();
                  }
            }, {
                  key: "getExtensionProperties",
                  value: function getExtensionProperties() {
                        return this.isAssetComp();
                  }
            }, {
                  key: "isLeftDoor",
                  get: function get() {
                        return this.door.name.toLowerCase().indexOf("left") > -1;
                  }
            }]);

            return AccessSensor;
      }(WV3DAssetComponent);

WV3DPropertyManager.attach_default_component_infos(AccessSensor, {
      "label": {
            "label_text": "AccessSensor",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "AccessSensor",
            "version": "1.0.0"
      },
      "extension": {
            "associatedAsset": []
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      }
});
WVPropertyManager.add_property_panel_group_info(AccessSensor, {
      template: "dcim-asset-id"
});
WVPropertyManager.add_property_panel_group_info(AccessSensor, {
      template: 'dcim-asset-popup'
});
