class Rack extends WV3DAssetComponent {

      constructor() {
            super();
      }

      getExtensionProperties() {
            return null;
      }

      _onCreateProperties() {
            super._onCreateProperties();
            this.door = null;
            this.unitArea = null;
            this.cover = null;
            this.assetData = null;
            this.errorData = null;
      }

      composeResource(loadedObj) {

            let rackSize      = ThreeUtil.getObjectSize(loadedObj);
            

            // element 변경 처리
            this._element = loadedObj;
            this._element.name = this.name + "_rack";
            this.appendElement.add(this._element);

            // 문 위치 조정
            try {
                  this.door         = loadedObj.getObjectByName("doorLeft");
                  this.unitArea     = loadedObj.getObjectByName("area");
                  this.unitArea.material.visible = false;
                  this.cover        = loadedObj.getObjectByName("cover");
                  this.unitArea.geometry.computeBoundingBox();
                  this.door.geometry.center();
                  this.door.geometry.computeBoundingBox();
                  this.door.geometry.translate(rackSize.x / 2, 0, 0);
                  this.door.position.x = -rackSize.x / 2;
                  this.door.position.y = rackSize.y / 2;
                  this.door.position.z = rackSize.z / 2;
            } catch (error) {
                  console.log('rack comp : modeling error');
            }

            this._validateElementSize(rackSize);
      }

      getResourceUnits() {
            return this.resource.units;
      }

      transitionAfter() {
            this._validateOutline();
      }

      validateUnitBuild(response) {
            this.assetData = response.assetData;
            this.errorData = response.errorData;
            if (this.errorData.length > 0) {
                  CPLogger.log("Mounting data error", this.errorData);
                  return;
            };
            if (this.assetData) {
                  this.buildChildren();
            }
      }

      prepareInnerAssetData() {
            let response = window.wemb.dcimManager.mountDataManager.getMountDataByRackId(this.assetId);
            let unitResource = this.getResourceUnits();
            
            if(response.mounts.hasOwnProperty("error") && response.mounts.error.length ){
                  console.warn("There is an error in the asset mounting information, so do not configure the asset implementation.", this.data, response.mounts.error );
            } else if (response.mounts.items.length) {
                  window.wemb.dcimManager.mountDataManager.filterChildAssetData(response.mounts.items, unitResource ).then(this.validateUnitBuild.bind(this));
            } else {
                  console.log("No asset information available");
            }
      }

      ascendingType() {
            return this.data.order_type == "ASC" ? true : false;
      }

      // provider을 이용해 내부 유닛 구성
      composeUnitData() {
            if(this.unitArea) {
                  let boxSize = this.unitArea.geometry.boundingBox.getSize(new THREE.Vector3());
                  let units = this.assetData;
                  let rows = units.length;
                  let colUnit = boxSize.x / 100; //가로 스케일 단위
                  let rowUnit = (boxSize.y / parseInt(this.data.unit_size)); //세로 스케일 단위
                  let i, j, vo, unit, cols, rowIndex;
                  let colProvider;
                  let ascendingType = this.ascendingType();
                  let orderValue = ascendingType ? -1 : 1;
                  let startX = -boxSize.x / 2;
                  let startY = ascendingType ? this.unitArea.geometry.boundingBox.max.y : this.unitArea.geometry.boundingBox.min.y;
                  for (i = 0; i < rows; i++) {
                        colProvider = units[i];
                        cols = colProvider.length;
                        for (j = 0; j < cols; j++) {
                              vo = colProvider[j];
                              if(vo.resource){
                                    let unitMesh = NLoaderManager.getCloneLoaderPool(wemb.configManager.serverUrl + vo.resource.path);
                                    unit = new RackInnerMeshDecorator(unitMesh.children[0]);
                                    unit.name = "unit_" + vo.type + "_" + i + "_" + j;
                                    rowIndex = vo.rowIndex;
                                    if(ascendingType){ rowIndex-=1;}
                                    let scaleRateX = vo.rotateY ? colUnit / unit.elementSize.y : colUnit / unit.elementSize.x;
                                    let scaleRateY = vo.rotateY ? rowUnit / unit.elementSize.x : rowUnit / unit.elementSize.y;
                                    let posY = (rowIndex * rowUnit);
                                    vo.scale = new THREE.Vector3(scaleRateX * vo.scaleX, scaleRateY * vo.scaleY, 1);
                                    vo.position = new THREE.Vector3(
                                          startX + (colUnit * vo.spaceX),
                                          startY + (posY * orderValue), 0);
                                    unit.data = vo;
                                    unit.draw(this);
                                    unit.registEventListener();
                                    this._children.push(unit);
                              }
                        }
                  }
            }
      }

      _onChildMouseOver(event) {
            event.selectItem.toggleOutlineElement(true);
            super._onChildMouseOver(event);
      }

      _onChildMouseOut(event) {
            event.selectItem.toggleOutlineElement(false);
            super._onChildMouseOut(event);
      }

      hasChildAsset() {
            return true;
      }

      buildChildren() {
            this.composeUnitData();
            this.appendElement.addEventListener("itemClick", this.childClickHandler);
            this.appendElement.addEventListener("itemDblClick", this.childDoubleClickHandler);
            this.appendElement.addEventListener("itemMouseOver", this.childMouseOverHandler);
            this.appendElement.addEventListener("itemMouseOut", this.childMouseOutHandler);
      }

      releaseChildren() {
            for (let i = 0; i < this._children.length; i++) {
                  let unit = this._children[i];
                  unit.destroy();
            }
            this._children = [];
            this.appendElement.removeEventListener("itemClick", this.childClickHandler);
            this.appendElement.removeEventListener("itemDblClick", this.childDoubleClickHandler);
            this.appendElement.removeEventListener("itemMouseOver", this.childMouseOverHandler);
            this.appendElement.removeEventListener("itemMouseOut", this.childMouseOutHandler);
      }

      transitionInEffect() {
            let DOOR_OPEN_ANGLE = -135;
            return new Promise((resolve) => {
                  if (this.isViewerMode) {
                        this.prepareInnerAssetData();
                        if(this.door) {
                              this.focusTween = TweenMax.to(this.door.rotation, 0.5, {
                                    y: DOOR_OPEN_ANGLE * Math.PI / 180,
                                    onComplete: () => {
                                          this.transitionAfter();
                                          resolve();
                                    }
                              });
                        } else {
                              resolve();
                        }
                  }
            });
      }

      childrenTransitionOut(){
            let child;
            for( let i=0; i<this._children.length;i++){
                  child = this._children[i];
                  if(child.selected){
                        child.selected=false;
                        child.transitionOut();
                  }
            }
      }

      transitionOutEffect() {
            return new Promise((resolve) => {
                  if (this.isViewerMode) {
                        this.childrenTransitionOut();
                        if(this.door) {
                              this.focusTween = TweenMax.to(this.door.rotation, 0.5, {
                                    y: 0,
                                    onComplete: () => {
                                          this.releaseChildren();
                                          this.transitionAfter();
                                          resolve();
                                    }
                              });
                        } else {
                              resolve();
                        }
                  }
            });
      }

      _onDestroy() {
            this.door = null;
            this.unitArea = null;
            this.cover = null;
            super._onDestroy();
      }


}


/**
 *  실장구성에 필요한 정보를 가공해서 반환해주는 builder
*
UnitVOBulder = {
      build: function (info, unitParams) {
            let product = {};
            product.rotateY = info.mountData.rotate ;  //유닛 회전 여부
            product.name = info.name; //유닛 이름
            product.type = info.asset_type //유닛 타입
            product.rowIndex = +info.mountData.row_index; //유닛 로우 정보
            product.spaceX = +(info.mountData.left); //유닛 가로 공백
            product.scaleX = +info.mountData.width; //유닛 가로 크기
            let unitScaleY = +info.mountData.row_weight; //유닛 세로 크기
            product.scaleY = unitScaleY;
            let hasFixUnit = (unitScaleY == 1 || unitScaleY == 2 || unitScaleY == 4 || unitScaleY == 10 || unitScaleY == 20);
            if (!hasFixUnit) {
                  if (product.scaleY >= 20) {
                        unitScaleY = 20;
                  } else if (product.scaleY >= 10 && product.scaleY < 20) {
                        unitScaleY = 10;
                  } else if (product.scaleY >= 4 && product.scaleY < 10) {
                        unitScaleY = 4;
                  } else {
                        unitScaleY = 1;
                  }
            }

            //실장 데이터의 타입 정보가 있어야 세부 모델링을 data를 통해 결정할 수 있음
            let unitTypeResource = unitParams[product.type];
            let unitModeling = unitTypeResource.modeling;
            let key = unitTypeResource.mappingKey;
            if (key != "" && info.hasOwnProperty(key)) {
                  key = info[key];
            } else {
                  key = "default";
            }
            // 매칭되는 모델링 정보 추출
            let resourceInfo = unitModeling[key];

            function findMatchingResource(source, searchScale) {
                  let results = source.find((resource) => {
                        let path = resource.path;
                        let name = path.substring(path.lastIndexOf("/") + 1);
                        return name.indexOf(searchScale.toString()) > -1;
                  });
                  return results;
            }

            let selectItem = findMatchingResource(resourceInfo, unitScaleY);
            if (selectItem == undefined) {
                  unitScaleY = 1;
                  selectItem = findMatchingResource(resourceInfo, unitScaleY);
            }

            return new Promise(async (resolve) => {
                  product.origin = info;
                  product.id = info.id;
                  if (selectItem) {
                        try {
                              let hasUnit = NLoaderManager.hasLoaderPool(selectItem.name);
                              if (!hasUnit) {
                                    await NLoaderManager.composeResource(selectItem);
                              }
                              product.resource = selectItem;
                              resolve([null, product]);
                        } catch (error) {
                              product.origin.mountData.error = "resource-load-error";
                              resolve([error, null]);
                              CPLogger.log("Mounting modeling load error", selectItem, error);
                        }
                  } else {
                        product.origin.mountData.error = "resource-type-undefined";
                        resolve([null, product]);
                  }

            });
      }


}*/


WV3DPropertyManager.attach_default_component_infos(Rack, {
      "label": {
            "label_text": "RackComponent",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "RackComponent",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(Rack, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(Rack, {
      template: 'dcim-asset-popup'
});

WVPropertyManager.add_property_panel_group_info(Rack, {
      template: 'dcim-rack-mount'
});


WV3DPropertyManager.add_event(Rack, {
      name: "itemMouseOver",
      label: "Child Element mouse over event",
      description: "Child Element mouse over event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});

WV3DPropertyManager.add_event(Rack, {
      name: "itemMouseOut",
      label: "Child Element mouse out event",
      description: "Child Element mouse out event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});

WV3DPropertyManager.add_event(Rack, {
      name: "itemClick",
      label: "Child Element mouse click event",
      description: "Child Element mouse click event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});

WV3DPropertyManager.add_event(Rack, {
      name: "itemDblClick",
      label: "Child Element mouse dbclick event",
      description: "Child Element mouse dbclick event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
