function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Rack =
      /*#__PURE__*/
      function (_WV3DAssetComponent) {
            "use strict";

            _inherits(Rack, _WV3DAssetComponent);

            function Rack() {
                  _classCallCheck(this, Rack);

                  return _possibleConstructorReturn(this, _getPrototypeOf(Rack).call(this));
            }

            _createClass(Rack, [{
                  key: "getExtensionProperties",
                  value: function getExtensionProperties() {
                        return null;
                  }
            }, {
                  key: "_onCreateProperties",
                  value: function _onCreateProperties() {
                        _get(_getPrototypeOf(Rack.prototype), "_onCreateProperties", this).call(this);

                        this.door = null;
                        this.unitArea = null;
                        this.cover = null;
                        this.assetData = null;
                        this.errorData = null;
                  }
            }, {
                  key: "composeResource",
                  value: function composeResource(loadedObj) {
                        var rackSize = ThreeUtil.getObjectSize(loadedObj);
                        
                        /*loadedObj.getObjectByName("base").material.visible = false;
                        wemb.threeElements.boxHelper.setFromObject(this.unitArea);
                        wemb.threeElements.boxHelper.material.visible = true;
                        */

                        this._element = loadedObj;
                        this._element.name = this.name + "_rack";
                        this.appendElement.add(this._element); // 문 위치 조정

                        try {
                              this.cover = loadedObj.getObjectByName("cover");
                              this.door = loadedObj.getObjectByName("doorLeft");
                              this.unitArea = loadedObj.getObjectByName("area");
                              this.unitArea.material.visible = false;
                              this.unitArea.geometry.computeBoundingBox(); // element 변경 처리
                              this.door.geometry.center();
                              this.door.geometry.computeBoundingBox();
                              this.door.geometry.translate(rackSize.x / 2, 0, 0);
                              this.door.position.x = -rackSize.x / 2;
                              this.door.position.y = rackSize.y / 2;
                              this.door.position.z = rackSize.z / 2;
                        } catch (error) {
                              console.log('rack comp : modeling error');
                        }

                        this._validateElementSize(rackSize);
                  }
            }, {
                  key: "getResourceUnits",
                  value: function getResourceUnits() {
                        return this.resource.units;
                  }
            }, {
                  key: "transitionAfter",
                  value: function transitionAfter() {
                        this._validateOutline();
                  }
                  /*
                  async filterChildAssetData(objProvider) {
                        let assetData = [];
                        let errorData = [];
                        objProvider.forEach((item) => {
                              if (item.mountData.error == "") {
                                    assetData.push(item);
                              } else {
                                    errorData.push(item);
                              }
                        });
                         assetData = assetData.sort((a, b) => {
                              return parseInt(a.mountData.row_index) - parseInt(b.mountData.row_index);
                        });
                         let i;
                        let info;
                        let limit = assetData.length;
                        let unitResource = this.getResourceUnits();
                        let sorted = [];
                        for (i = 0; i < limit; i++) {
                              info = assetData[i];
                              const [error, results] = await window.wemb.dcimManager.mountDataManager.buildRackInnerResourceData(info, unitResource);
                              if (error) {
                                    continue;
                              }
                              sorted.push(results);
                        }
                        if (sorted.length == 0) return sorted;
                        // 들어온 데이터를 row순으로 정렬해 이중 배열로 만듬.
                        let startIndex = parseInt(sorted[0].rowIndex);
                        let endIndex = parseInt(sorted[limit - 1].rowIndex) || 0;
                        let provider = [];
                        for (i = startIndex; i <= endIndex; i++) {
                              let rowIdxArr = sorted.filter((vo) => {
                                    return vo.rowIndex == i;
                              });
                              if (rowIdxArr.length > 0) {
                                    provider.push(rowIdxArr);
                              }
                        }
                        return {assetData: provider, errorData: errorData};
                  }
                  */

            }, {
                  key: "validateUnitBuild",
                  value: function validateUnitBuild(response) {
                        this.assetData = response.assetData;
                        this.errorData = response.errorData;

                        if (this.errorData.length > 0) {
                              CPLogger.log("Mounting data error", this.errorData);
                              return;
                        }

                        ;

                        if (this.assetData) {
                              this.buildChildren();
                        }
                  }
            }, {
                  key: "prepareInnerAssetData",
                  value: function prepareInnerAssetData() {
                        var response = window.wemb.dcimManager.mountDataManager.getMountDataByRackId(this.assetId);
                        var unitResource = this.getResourceUnits();

                        if (response.mounts.hasOwnProperty("error") && response.mounts.error.length) {
                              console.warn("There is an error in the asset mounting information, so do not configure the asset implementation.", this.data, response.mounts.error);
                        } else if (response.mounts.items.length) {
                              window.wemb.dcimManager.mountDataManager.filterChildAssetData(response.mounts.items, unitResource).then(this.validateUnitBuild.bind(this)); //this.filterChildAssetData(response.mounts.items));
                        } else {
                              console.log("No asset information available");
                        }
                  }
            }, {
                  key: "ascendingType",
                  value: function ascendingType() {
                        return this.data.order_type == "ASC" ? true : false;
                  } // provider을 이용해 내부 유닛 구성

            }, {
                  key: "composeUnitData",
                  value: function composeUnitData() {
                        if(this.unitArea) {
                              var boxSize = this.unitArea.geometry.boundingBox.getSize(new THREE.Vector3());
                              var units = this.assetData;
                              var rows = units.length;
                              var colUnit = boxSize.x / 100; //가로 스케일 단위

                              var rowUnit = boxSize.y / parseInt(this.data.unit_size); //세로 스케일 단위

                              var i, j, vo, unit, cols, rowIndex;
                              var colProvider;
                              var ascendingType = this.ascendingType();
                              var orderValue = ascendingType ? -1 : 1;
                              var startX = -boxSize.x / 2;
                              var startY = ascendingType ? this.unitArea.geometry.boundingBox.max.y : this.unitArea.geometry.boundingBox.min.y;

                              for (i = 0; i < rows; i++) {
                                    colProvider = units[i];
                                    cols = colProvider.length;

                                    for (j = 0; j < cols; j++) {
                                          vo = colProvider[j];

                                          if (vo.resource) {
                                                var unitMesh = NLoaderManager.getCloneLoaderPool(wemb.configManager.serverUrl + vo.resource.path);
                                                unit = new RackInnerMeshDecorator(unitMesh.children[0]);
                                                unit.name = "unit_" + vo.type + "_" + i + "_" + j;
                                                rowIndex = vo.rowIndex;

                                                if (ascendingType) {
                                                      rowIndex -= 1;
                                                }

                                                var scaleRateX = vo.rotateY ? colUnit / unit.elementSize.y : colUnit / unit.elementSize.x;
                                                var scaleRateY = vo.rotateY ? rowUnit / unit.elementSize.x : rowUnit / unit.elementSize.y;
                                                var posY = rowIndex * rowUnit;
                                                vo.scale = new THREE.Vector3(scaleRateX * vo.scaleX, scaleRateY * vo.scaleY, 1);
                                                vo.position = new THREE.Vector3(startX + colUnit * vo.spaceX, startY + posY * orderValue, 0);
                                                unit.data = vo;
                                                unit.draw(this);
                                                unit.registEventListener();

                                                this._children.push(unit);
                                          }
                                    }
                              }
                        }
                  }
            }, {
                  key: "_onChildMouseOver",
                  value: function _onChildMouseOver(event) {
                        event.selectItem.toggleOutlineElement(true);

                        _get(_getPrototypeOf(Rack.prototype), "_onChildMouseOver", this).call(this, event);
                  }
            }, {
                  key: "_onChildMouseOut",
                  value: function _onChildMouseOut(event) {
                        event.selectItem.toggleOutlineElement(false);

                        _get(_getPrototypeOf(Rack.prototype), "_onChildMouseOut", this).call(this, event);
                  }
            }, {
                  key: "hasChildAsset",
                  value: function hasChildAsset() {
                        return true;
                  }
            }, {
                  key: "buildChildren",
                  value: function buildChildren() {
                        this.composeUnitData();
                        this.appendElement.addEventListener("itemClick", this.childClickHandler);
                        this.appendElement.addEventListener("itemDblClick", this.childDoubleClickHandler);
                        this.appendElement.addEventListener("itemMouseOver", this.childMouseOverHandler);
                        this.appendElement.addEventListener("itemMouseOut", this.childMouseOutHandler);
                  }
            }, {
                  key: "releaseChildren",
                  value: function releaseChildren() {
                        for (var i = 0; i < this._children.length; i++) {
                              var unit = this._children[i];
                              unit.destroy();
                        }

                        this._children = [];
                        this.appendElement.removeEventListener("itemClick", this.childClickHandler);
                        this.appendElement.removeEventListener("itemDblClick", this.childDoubleClickHandler);
                        this.appendElement.removeEventListener("itemMouseOver", this.childMouseOverHandler);
                        this.appendElement.removeEventListener("itemMouseOut", this.childMouseOutHandler);
                  }
            }, {
                  key: "transitionInEffect",
                  value: function transitionInEffect() {
                        var _this = this;

                        var DOOR_OPEN_ANGLE = -135;
                        return new Promise(function (resolve) {
                              if (_this.isViewerMode) {
                                    _this.prepareInnerAssetData();

                                    if(_this.door) {
                                          _this.focusTween = TweenMax.to(_this.door.rotation, 0.5, {
                                                y: DOOR_OPEN_ANGLE * Math.PI / 180,
                                                onComplete: function onComplete() {
                                                      _this.transitionAfter();
      
                                                      resolve();
                                                }
                                          });
                                    } else {
                                          resolve();
                                    }
                              }
                        });
                  }
            }, {
                  key: "childrenTransitionOut",
                  value: function childrenTransitionOut() {
                        var child;

                        for (var i = 0; i < this._children.length; i++) {
                              child = this._children[i];

                              if (child.selected) {
                                    child.selected = false;
                                    child.transitionOut();
                              }
                        }
                  }
            }, {
                  key: "transitionOutEffect",
                  value: function transitionOutEffect() {
                        var _this2 = this;

                        return new Promise(function (resolve) {
                              if (_this2.isViewerMode) {
                                    _this2.childrenTransitionOut();

                                    if(_this2.door) {
                                          _this2.focusTween = TweenMax.to(_this2.door.rotation, 0.5, {
                                                y: 0,
                                                onComplete: function onComplete() {
                                                      _this2.releaseChildren();
      
                                                      _this2.transitionAfter();
      
                                                      resolve();
                                                }
                                          });
                                    } else {
                                          resolve();
                                    }
                              }
                        });
                  }
            }, {
                  key: "_onDestroy",
                  value: function _onDestroy() {
                        this.door = null;
                        this.unitArea = null;
                        this.cover = null;

                        _get(_getPrototypeOf(Rack.prototype), "_onDestroy", this).call(this);
                  }
            }]);

            return Rack;
      }(WV3DAssetComponent);
/**
 *  실장구성에 필요한 정보를 가공해서 반환해주는 builder
 *
 UnitVOBulder = {
      build: function (info, unitParams) {
            let product = {};
            product.rotateY = info.mountData.rotate ;  //유닛 회전 여부
            product.name = info.name; //유닛 이름
            product.type = info.asset_type //유닛 타입
            product.rowIndex = +info.mountData.row_index; //유닛 로우 정보
            product.spaceX = +(info.mountData.left); //유닛 가로 공백
            product.scaleX = +info.mountData.width; //유닛 가로 크기
            let unitScaleY = +info.mountData.row_weight; //유닛 세로 크기
            product.scaleY = unitScaleY;
            let hasFixUnit = (unitScaleY == 1 || unitScaleY == 2 || unitScaleY == 4 || unitScaleY == 10 || unitScaleY == 20);
            if (!hasFixUnit) {
                  if (product.scaleY >= 20) {
                        unitScaleY = 20;
                  } else if (product.scaleY >= 10 && product.scaleY < 20) {
                        unitScaleY = 10;
                  } else if (product.scaleY >= 4 && product.scaleY < 10) {
                        unitScaleY = 4;
                  } else {
                        unitScaleY = 1;
                  }
            }

            //실장 데이터의 타입 정보가 있어야 세부 모델링을 data를 통해 결정할 수 있음
            let unitTypeResource = unitParams[product.type];
            let unitModeling = unitTypeResource.modeling;
            let key = unitTypeResource.mappingKey;
            if (key != "" && info.hasOwnProperty(key)) {
                  key = info[key];
            } else {
                  key = "default";
            }
            // 매칭되는 모델링 정보 추출
            let resourceInfo = unitModeling[key];

            function findMatchingResource(source, searchScale) {
                  let results = source.find((resource) => {
                        let path = resource.path;
                        let name = path.substring(path.lastIndexOf("/") + 1);
                        return name.indexOf(searchScale.toString()) > -1;
                  });
                  return results;
            }

            let selectItem = findMatchingResource(resourceInfo, unitScaleY);
            if (selectItem == undefined) {
                  unitScaleY = 1;
                  selectItem = findMatchingResource(resourceInfo, unitScaleY);
            }

            return new Promise(async (resolve) => {
                  product.origin = info;
                  product.id = info.id;
                  if (selectItem) {
                        try {
                              let hasUnit = NLoaderManager.hasLoaderPool(selectItem.name);
                              if (!hasUnit) {
                                    await NLoaderManager.composeResource(selectItem);
                              }
                              product.resource = selectItem;
                              resolve([null, product]);
                        } catch (error) {
                              product.origin.mountData.error = "resource-load-error";
                              resolve([error, null]);
                              CPLogger.log("Mounting modeling load error", selectItem, error);
                        }
                  } else {
                        product.origin.mountData.error = "resource-type-undefined";
                        resolve([null, product]);
                  }

            });
      }


}*/


WV3DPropertyManager.attach_default_component_infos(Rack, {
      "label": {
            "label_text": "RackComponent",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },
      "info": {
            "componentName": "RackComponent",
            "version": "1.0.0"
      },
      "settings": {
            "popupId": ""
      },
      "override_settings": {
            "popupId": ""
      }
});
WVPropertyManager.add_property_panel_group_info(Rack, {
      template: "dcim-asset-id"
});
WVPropertyManager.add_property_panel_group_info(Rack, {
      template: 'dcim-asset-popup'
});
WVPropertyManager.add_property_panel_group_info(Rack, {
      template: 'dcim-rack-mount'
});
WV3DPropertyManager.add_event(Rack, {
      name: "itemMouseOver",
      label: "Child Element mouse over event",
      description: "Child Element mouse over event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
WV3DPropertyManager.add_event(Rack, {
      name: "itemMouseOut",
      label: "Child Element mouse out event",
      description: "Child Element mouse out event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
WV3DPropertyManager.add_event(Rack, {
      name: "itemClick",
      label: "Child Element mouse click event",
      description: "Child Element mouse click event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
WV3DPropertyManager.add_event(Rack, {
      name: "itemDblClick",
      label: "Child Element mouse dbclick event",
      description: "Child Element mouse dbclick event",
      properties: [{
            name: "event",
            type: "any",
            default: "",
            description: "Event Obejct"
      }]
});
