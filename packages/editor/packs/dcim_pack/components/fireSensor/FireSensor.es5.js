"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FireSensor = function (_WV3DAssetComponent) {
      _inherits(FireSensor, _WV3DAssetComponent);

      function FireSensor() {
            _classCallCheck(this, FireSensor);

            return _possibleConstructorReturn(this, (FireSensor.__proto__ || Object.getPrototypeOf(FireSensor)).call(this));
      }

      _createClass(FireSensor, [{
            key: "hasAssetIcon",
            value: function hasAssetIcon() {
                  return true;
            }
      }, {
            key: "getExtensionProperties",
            value: function getExtensionProperties() {
                  return this.isAssetComp();
            }
      }]);

      return FireSensor;
}(WV3DAssetComponent);

WV3DPropertyManager.attach_default_component_infos(FireSensor, {

      "label": {
            "label_text": "FireSensor",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "FireSensor",
            "version": "1.0.0"
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(FireSensor, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(FireSensor, {
      template: 'dcim-asset-popup'
});
