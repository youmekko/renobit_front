class MDM extends WV3DAssetComponent {
      constructor(){ super(); }

      hasAssetIcon() {
            return true;
      }

}



WV3DPropertyManager.attach_default_component_infos(MDM, {

      "label": {
            "label_text": "MDM",
            "label_using": "N",
            "label_background_color": "#3351ED"
      },

      "info": {
            "componentName": "MDM",
            "version": "1.0.0",
      },
      "settings": {
            "popupId": ""
      },

      "override_settings": {
            "popupId": ""
      }
});

WVPropertyManager.add_property_panel_group_info(MDM, {
      template: "dcim-asset-id"
});

WVPropertyManager.add_property_panel_group_info(MDM, {
      template: 'dcim-asset-popup'
});
