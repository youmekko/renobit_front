module.exports = function (renobit) {
      'use strict';
      const _ = require('lodash');
      const TB_ASSET_MOUNT_RACK = 'TB_ASSET_MOUNT_RACK';
      const TB_ASSET_COMMON = 'TB_ASSET_COMMON';
      const SCHEMA = {
            "id": {"type": "string", "defaultValue": null, "maxLength": 50, "nullable": false},
            "row_index": {"type": "integer", "defaultValue": 0, "nullable": true},
            "column_index": {"type": "integer", "defaultValue": 0, "nullable": true},
            "row_weight": {"type": "integer", "defaultValue": 1, "nullable": true},
            "column_weight": {"type": "string", "defaultValue": 'flex', "maxLength": 50, "nullable": true},
            "padding": {"type": "integer", "defaultValue": 0, "nullable": true},
            "parent_id": {"type": "string", "defaultValue": null, "maxLength": 50, "nullable": false}
      };

      const mount = {
            init: function() {
                  var promise = new Promise(function(resolve,reject){
                        renobit.database.schema.hasTable(TB_ASSET_MOUNT_RACK).then(function(exists) {
                              if(!exists) {
                                    renobit.database.schema.createTable(TB_ASSET_MOUNT_RACK, function (table) {
                                          var pk_arr = [];
                                          _.each(SCHEMA, (detail, column_name) => {
                                                var column = detail.type === 'string' ? table[detail.type](column_name, detail.maxLength) : table[detail.type](column_name);
                                                if(detail.type === "timestamp") column.defaultTo(RENOBIT.database.fn.now())
                                                if(detail.defaultValue !== null) column.defaultTo(detail.defaultValue);
                                                else column.notNullable();
                                                if(detail.nullable) column.nullable();
                                                if(detail.pk) pk_arr.push(column_name);
                                          })
                                          if(pk_arr.length > 0) table.primary(pk_arr);
                                    }).then(function (res) {
                                          resolve();
                                    }).catch(function (err) {
                                          reject(err);
                                    })
                              } else {
                                    resolve();
                              }
                        })
                  });
                  return promise;

            },
            uninit: function() {
                  return renobit.database.schema.dropTableIfExists(TB_ASSET_MOUNT_RACK);
            },

            select: function(param) {
                  return new Promise(function(resolve, reject) {
                        let queryBuilder = renobit.database(TB_ASSET_MOUNT_RACK).innerJoin(TB_ASSET_COMMON, TB_ASSET_MOUNT_RACK + '.id', TB_ASSET_COMMON + '.id').select("*");

                        if (param) {
                              _.each(param, (v,k) => {
                                    queryBuilder.andWhere(k, v);
                              })
                        }

                        queryBuilder.then((result) => {
                              resolve(result);
                        }).catch((err) => {
                              reject(err);
                        })
                  })
            },

            delete: function(param) {
                  return new Promise(function(resolve, reject) {
                        let queryBuilder = renobit.database(TB_ASSET_MOUNT_RACK).del();
                        if (param) {
                              let _param = _.clone(param);
                              delete _param.mountList;

                              _.each(_param, (v,k) => {
                                    queryBuilder.andWhere(k, v);
                              })
                        }

                        queryBuilder.then(() => {
                              resolve();
                        }).catch((err) => {
                              reject(err)
                        })
                  })
            },

            insert: function(param) {
                  return new Promise(function(resolve, reject) {
                        let _param = _.clone(param);
                        if(_param.mountList) {
                              _param.mountList.map(item=>{
                                    item.parent_id = _param.parent_id;
                              })
                        }

                        renobit.database.batchInsert(TB_ASSET_MOUNT_RACK, _param.mountList).then(()=>{
                              resolve();
                        }).catch(err =>{
                              reject(err);
                        })
                  })
            }
      }

      function ensureAuth(req, res, next) {
            if (req.isAuthenticated()) {
                  next();
            } else {
                  res.status(401).send({code:'AUTH_001'});
            }
      }

      return {
            init: function(param) {
                  return mount.init();
            },
            uninit: function() {
                  mount.uninit().then(() => {
                        console.log("mount rack table drop success");
                  });
            },
            regist: function() {
                  renobit.app.get("/asset/mount/rack", ensureAuth, function(req, res, next) {
                        mount.select(req.query).then((rows) => {
                              res.status(200).send(rows);
                        }).catch((err) => {
                              RENOBIT.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });
                  renobit.app.post("/asset/mount/rack", ensureAuth, function(req, res, next) {
                        var rows = [];
                        // delete
                        mount.delete(req.body, rows).then(()=>{
                              // insert
                              mount.insert(req.body, rows).then(() => {
                                    res.status(200).send();
                              }).catch((err) => {
                                    RENOBIT.logger.error(err);
                                    res.status(500).send({code:'ETC_001'});
                              })

                        }).catch(err =>{
                              RENOBIT.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        });
                  });

            },

            unregist: function() {
                  try {
                        var remove_route = [];
                        var stack = renobit.app._router.stack;
                        stack.forEach(removeMiddlewares);

                        function removeMiddlewares(layer, i) {
                              if (layer.handle.name === "bound dispatch") {
                                    if (layer.route.path.includes("/asset/mount/rack")) {
                                          remove_route.push(layer);
                                    }
                              }
                        }

                        remove_route.forEach((v, k) => {
                              var idx = stack.indexOf(v);
                              stack.splice(idx, 1);
                        })
                  } catch (error) {
                        RENOBIT.logger.error(error);
                  }
            },
            backup : function() {
                  var promise = new Promise((resolve, reject) => {
                        var result = {};
                        renobit.database(TB_ASSET_MOUNT_RACK).select('*').then((rows) => {
                              result[TB_ASSET_MOUNT_RACK] = {
                                    schema:SCHEMA,
                                    rows:rows
                              }
                              resolve(result);
                        })
                  })
                  return promise;
            }
      }
}
