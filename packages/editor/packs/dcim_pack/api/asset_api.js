module.exports = function(renobit) {
    'use strict';
    var _ = require('lodash');
    var Excel = require('exceljs');
    var path = require('path');

    const TB_PREFIX = 'A_';
    const TB_ASSET_TYPE = 'TB_ASSET_TYPE';
    const TB_ASSET_COMMON = 'TB_ASSET_COMMON';
    const TB_ASSET_COMPONENT = 'TB_ASSET_COMPONENT';

    const TB_PACKAGES = 'TB_PACKAGES';
    const TB_INSTANCE = 'TB_INSTANCE';

    // schema define
    const TYPE_SCHEMA = {
        id: {
            type: "string",
            defaultValue: null,
            maxLength: 50,
            nullable: false,
            pk: true
        },
        name: {
            type: "string",
            defaultValue: null,
            maxLength: "50",
            nullable: false,
            index: true
        },
        fields: {
            type: "json",
            defaultValue: null,
            maxLength: null,
            nullable: false
        }
    };
    const COMMON_SCHEMA = {
        id: {
            type: "string",
            defaultValue: null,
            maxLength: 50,
            nullable: false,
            pk: true
        },
        name: {
            type: "string",
            defaultValue: null,
            maxLength: "50",
            nullable: false,
            index: true
        },
        asset_type: {
            type: 'string',
            defaultValue: null,
            maxLength: "50",
            nullable: false,
            index: true
        }
    };
    const COMP_SCHEMA = {
        file_id: {
            type: "string",
            defaultValue: null,
            maxLength: "50",
            nullable: false,
            index: true,
            pk: true
        },
        name: {
            type: "string",
            defaultValue: null,
            maxLength: "50",
            nullable: false,
            index: true
        },
        asset_type: {
            type: 'string',
            defaultValue: null,
            maxLength: "50",
            nullable: false,
            index: false
        },
        batch: {
            type: 'boolean',
            defaultValue: null,
            maxLength: "50",
            nullable: false,
            index: false
        },
        props: {
            type: 'json',
            defaultValue: null,
            nullable: false,
            index: false
        }
    };
    // install logic

    // uninstall logic

    function ensureAuth(req, res, next) {
        if (req.isAuthenticated()) {
            next();
        } else {
            res.status(401).send({code:'AUTH_001'});
        }
    }

    const asset = {
        type: {
            uninit: function() {
                var promises = [];
                return renobit.database(TB_ASSET_TYPE).select('*').map(row => {
                    if (row.id !== 'common') {
                        var table_name = TB_PREFIX + row.id.toUpperCase();
                        promises.push(renobit.database.schema.dropTableIfExists(table_name));
                        promises.push(renobit.database.schema.dropTableIfExists(table_name + "_H"));
                    }
                }).then(() => {
                    promises.push(renobit.database.schema.dropTableIfExists(TB_ASSET_TYPE));
                    promises.push(renobit.database.schema.dropTableIfExists(TB_ASSET_COMMON));
                    promises.push(renobit.database.schema.dropTableIfExists(TB_ASSET_COMPONENT));
                    return Promise.all(promises);
                })
            },
            init: function(param) {
                var promise = new Promise(function(resolve, reject) {
                    renobit.database.schema.hasTable(TB_ASSET_TYPE).then(function(exists) {
                        if (!exists) {
                            renobit.database.schema.createTable(TB_ASSET_TYPE, function(table) {
                                table.string('id', 50).primary();
                                table.string('name', 50);
                                table.json('fields');
                                table.index('name');
                            }).then(function(res) {
                                if (param) {
                                    var promises = [];
                                    var common_data = {
                                        id: "common",
                                        name: "common",
                                        fields: param.common_fields
                                    }
                                    promises.push(asset.type.insert(common_data));
                                    param.types.forEach(item => {
                                        promises.push(asset.type.insert(item));
                                    });
                                    Promise.all(promises).then(() => {
                                        resolve();
                                    }).catch((err) => {
                                        reject();
                                    })
                                } else {
                                    resolve();
                                }
                            }).catch(function(err) {
                                reject(err);
                            })
                        } else {
                            resolve();
                        }
                    })
                });
                return promise;
            },
            insert: function(param) {
                var promise = new Promise(function(resolve, reject) {
                    if (param) {
                        var value = _.clone(param);
                        value.fields = JSON.stringify(value.fields);
                        renobit.database.insert(value).into(TB_ASSET_TYPE).then((result) => {
                            resolve();
                        }).catch((err) => {
                            reject(err);
                        })
                    } else {
                        resolve();
                    }
                });
                return promise;
            },
            update: function(param) {
                var promise = new Promise(function(resolve, reject) {
                    renobit.database.transaction(function(trx) {
                            Promise.all([
                                renobit.database(TB_ASSET_TYPE).transacting(trx).where({ id: "common" }).update({ fields: JSON.stringify(param.common_fields) }),
                                renobit.database(TB_ASSET_TYPE).transacting(trx).where({ id: param.id }).update({
                                    name: param.name,
                                    fields: JSON.stringify(param.fields)
                                })
                            ]).then(() => {
                                trx.commit();
                            }).catch((err) => {
                                trx.rollback();
                            })
                        })
                        .then(() => {
                            resolve();
                        }).catch((err) => {
                            reject();
                        })
                })
                return promise;
            },
            selectQuery: function(query) {
                return renobit.database.select('*').from(TB_ASSET_TYPE).where(query);
            },
            select: function(query) {
                var promise = new Promise(function(resolve, reject) {
                    var result = {
                        "common_fields": [],
                        "types": []
                    };
                    renobit.database.select('*').from(TB_ASSET_TYPE).where({ id: 'common' }).map((common_row) => {
                        result.common_fields = typeof common_row.fields === "object" ? common_row.fields : JSON.parse(common_row.fields);
                        renobit.database.select('*').from(TB_ASSET_TYPE).where(query).map(function(row) {
                            if (row.id !== "common") {
                                row.fields = typeof row.fields === "object" ? row.fields : JSON.parse(row.fields);
                                result.types.push(row);
                            }
                        }).then((rows) => {
                            resolve(result);
                        }).catch((err) => {
                            reject();
                        })
                    });
                });
                return promise;
            },
            delete: function(query) {
                var promise = new Promise(function(resolve, reject) {
                    if (query.id === "common") {
                        reject();
                    } else {
                        renobit.database(TB_ASSET_TYPE).where({ id: query.id }).del().then(() => {
                            resolve();
                        }).catch((err) => {
                            reject()
                        })
                    }
                });
                return promise;
            }
        },
        data: {
            init: function(param) {
                var promise = new Promise(function(resolve, reject) {
                    renobit.database.schema.hasTable(TB_ASSET_COMMON).then(function(exists) {
                        if (!exists) {
                            renobit.database.schema.createTable(TB_ASSET_COMMON, function(table) {
                                _.each(param.common_fields, (detail, index) => {
                                    var column;
                                    if(detail.type.toLowerCase() === 'string') {
                                        column = table[detail.type.toLowerCase()](detail.name, 50);
                                    } else if(detail.type.toLowerCase() === 'number') {
                                        column = table.integer(detail.name);
                                    } else {
                                        column = table[detail.type.toLowerCase()](detail.name);
                                    }
                                    if (detail.type.toLowerCase() === "timestamp") column.defaultTo(RENOBIT.database.fn.now())
                                    if (detail.type.toLowerCase() === "string") {
                                        table.index(detail.name);
                                    }
                                    if (detail.name === 'id') column.primary();
                                })
                            }).then(function(res) {
                                var promises = [];
                                param.types.forEach(function(type) {
                                    promises.push(asset.data.create(type));
                                })
                                Promise.all(promises).then(() => {
                                    resolve();
                                })
                            }).catch(function(err) {
                                reject(err);
                            })
                        } else {
                            resolve();
                        }
                    })
                });
                return promise;
            },
            insert: function(param) {
                return renobit.database.transaction(function(trx) {
                    if (param) {
                        var promises = [];
                        var value = _.clone(param);
                        if (value.type_data) {
                            delete value.type_data;
                            param.type_data['id'] = value.id;
                            promises.push(renobit.database.insert(param.type_data).into(TB_PREFIX + param.asset_type.toUpperCase()).transacting(trx));
                        }
                        if (value.history_data) {
                            delete value.history_data;
                            param.history_data['id'] = value.id;
                            promises.push(renobit.database.insert(param.history_data).into(TB_PREFIX + param.asset_type.toUpperCase() + "_H"));
                        }
                        promises.push(renobit.database.insert(value).into(TB_ASSET_COMMON).transacting(trx));
                        Promise.all(promises).then(() => {
                            trx.commit();
                        }).catch((err) => {
                            trx.rollback();
                        })
                    } else {
                        trx.commit();
                    }
                });
            },
            update: function(param) {
                var promise = new Promise(function(resolve, reject) {
                    var promises = [];
                    if (Array.isArray(param)) {
                        _.each(param, (v, k) => {
                            promises.push(renobit.database(TB_ASSET_COMMON).where({ id: v.id }).update(v))
                        })
                    } else {
                        var value = _.clone(param);
                        if (value.type_data) delete value.type_data;
                        if (value.history_data) delete value.history_data;
                        promises.push(renobit.database(TB_ASSET_COMMON).where({ id: param.id }).update(value))
                        if (param.type_data && Object.keys(param.type_data).length > 0) {
                            promises.push(renobit.database(TB_PREFIX + param.asset_type.toUpperCase()).where({ id: param.id }).update(param.type_data));
                        }
                    }
                    Promise.all(promises).then(() => {
                        resolve();
                    }).catch((err) => {
                        reject();
                    })
                })
                return promise;
            },
            select: function(query, fields = []) {
                var pass_fields = [];
                var json_fields = [];
                fields.forEach((field) => {
                    if (field.type === 'Json') {
                        json_fields.push(field.name);
                    } else if(field.type === 'Password') {
                        pass_fields.push(field.name);
                    }
                })
                var promise = new Promise(function(resolve, reject) {
                    var response = {
                        count: 0,
                        result: []
                    };
                    var whereQuery = _.clone(query);
                    if (query.limit) delete whereQuery.limit;
                    if (query.offset) delete whereQuery.offset;
                    if (query.orderby) delete whereQuery.orderby;

                    var queryBuilder = renobit.database(TB_ASSET_COMMON);
                    if (query.asset_type) {
                        var type_table = TB_PREFIX + query.asset_type.toUpperCase()
                        queryBuilder.innerJoin(type_table, 'TB_ASSET_COMMON.id', type_table + '.id')
                            .leftJoin('TB_INSTANCE', 'TB_ASSET_COMMON.id', 'TB_INSTANCE.ASSET_ID')
                        if (query.id) {
                            queryBuilder.andWhere({ "TB_ASSET_COMMON.id": query.id })
                            delete whereQuery.id;
                        }
                        delete whereQuery.asset_type;
                        _.each(whereQuery, (v, k) => {
                            queryBuilder.andWhere(k, 'like', '%' + v + '%');
                        })
                    } else {
                        queryBuilder.leftJoin('TB_INSTANCE', 'TB_ASSET_COMMON.id', 'TB_INSTANCE.ASSET_ID');
                        if (query.id) {
                            queryBuilder.andWhere({ "TB_ASSET_COMMON.id": query.id })
                            delete whereQuery.id;
                        }
                        delete whereQuery.asset_type;
                        _.each(whereQuery, (v, k) => {
                            queryBuilder.andWhere(k, 'like', '%' + v + '%');
                        })
                    }

                    queryBuilder.clone().count({ count: '*' }).then((rows) => {
                        if (rows.length > 0) {
                            response.count = rows[0].count;
                            if (query.orderby) {
                                try {
                                    var orderby = JSON.parse(query.orderby);
                                    _.each(orderby, (v, k) => {
                                        var key = k === 'id' ? TB_ASSET_COMMON + '.id' : k;
                                        var order = v;
                                        queryBuilder.orderBy(key, order);
                                    })
                                } catch (error) {
                                    reject();
                                }
                            }
                            if (query.limit) queryBuilder.limit(parseInt(query.limit));
                            if (query.offset) queryBuilder.offset(parseInt(query.offset));
                            queryBuilder.select('*').map((row) => {
                                row['instance_id'] = row.INST_ID;
                                row['page_id'] = row.PAGE_ID;
                                delete row.INST_ID;
                                delete row.PAGE_ID;
                                delete row.LAYER_NAME;
                                delete row.CATEGORY;
                                delete row.COMP_NAME;
                                delete row.NAME;
                                delete row.GROUP_ID;
                                delete row.PROPS;
                                delete row.ASSET_ID;
                                json_fields.forEach((id) => {
                                    if (row.hasOwnProperty(id)) {
                                        try {
                                            row[id] = JSON.parse(row[id]);
                                        } catch (err) {
                                            // json 형태가 아니면 문제됨.
                                            console.log(err);
                                        }
                                    }
                                })
                                pass_fields.forEach((id) => {
                                    if(row.hasOwnProperty(id) && row[id]) {
                                        row[id] = RENOBIT.utils.base64.decode(row[id]);
                                    }
                                })
                                return row;
                            }).then((rows) => {
                                response.result = rows;
                                resolve(response);
                            })
                        }
                    })
                    return null;
                }).catch((err) => {
                    reject();
                })

                return promise;
            },
            delete: function(asset_type, id_arr) {
                var promise = new Promise(function(resolve, reject) {
                    Promise.all([renobit.database(TB_ASSET_COMMON).whereIn('id', id_arr).del(),
                        renobit.database(TB_PREFIX + asset_type.toUpperCase()).whereIn('id', id_arr).del()
                    ]).then(() => {
                        resolve();
                    }).catch((err) => {
                        reject();
                    })
                });
                return promise;
            },
            alter: function(type) {
                var promise = new Promise(function(resolve, reject) {
                    var promises = []
                    var table_name = TB_PREFIX + type.id.toUpperCase();

                    function compare_columns(origin, target) {
                        var add_columns = [];
                        var remove_columns = [];
                        _.each(target, (v, k) => {
                            if (!origin[v.name]) {
                                add_columns.push(v.name);
                            }
                        })
                        _.each(origin, (v, k) => {
                            if (!target.find(function(d) { return d.name == k })) {
                                if (k !== 'id' && k !== 'created_at' && k !== 'name' && k !== 'asset_type') remove_columns.push(k);
                            }
                        })
                        return {
                            add_columns: add_columns,
                            remove_columns: remove_columns
                        }
                    }
                    // common fields
                    promises.push(renobit.database(TB_ASSET_COMMON).columnInfo().then((info) => {
                        return { "table": TB_ASSET_COMMON, "column": compare_columns(info, type.common_fields) };
                    }));

                    // history fields
                    promises.push(renobit.database(table_name + '_H').columnInfo().then((info) => {
                        return { "table": table_name + '_H', "column": compare_columns(info, type.fields.history_fields) };
                    }))

                    // static fields
                    promises.push(renobit.database(table_name).columnInfo().then(function(info) {
                        return { "table": table_name, "column": compare_columns(info, type.fields.type_fields) };
                    }))
                    Promise.all(promises).then((result) => {
                        var last_promises = []
                        _.each(result, (v, k) => {
                            last_promises.push(renobit.database.schema.table(v.table, function(table) {
                                if (v.column.add_columns.length > 0) v.column.add_columns.forEach(function(d) { table.string(d) });
                                if (v.column.remove_columns.length > 0) v.column.remove_columns.forEach(function(d) { table.dropColumn(d) });
                            }))
                        })
                        Promise.all(last_promises).then(() => {
                            resolve();
                        }).catch(() => {
                            reject();
                        })
                    }).catch(() => {
                        reject();
                    })

                });
                return promise;
            },
            drop: function(query) {
                var promise = new Promise((resovle, reject) => {
                    var promises = [];
                    var table_name = TB_PREFIX + query.id.toUpperCase();
                    promises.push(renobit.database.schema.dropTableIfExists(table_name));
                    promises.push(renobit.database.schema.dropTableIfExists(table_name + "_H"));
                    promises.push(renobit.database(TB_ASSET_COMMON).whereIn('asset_type', query.id).del());

                    Promise.all(promises).then(() => {
                        resovle();
                    }).catch((err) => {
                        reject();
                    })
                })
                return promise;
            },
            create: function(type) {
                var promise = new Promise(function(resolve, reject) {
                    var table_name = TB_PREFIX + type.id.toUpperCase();
                    renobit.database.schema.hasTable(table_name).then(function(exists) {
                        if (!exists) {
                            Promise.all([
                                renobit.database.schema.createTable(table_name, function(table) {
                                    table.string('id', 50).primary();
                                    if (type.fields && type.fields.type_fields) {
                                        type.fields.type_fields.forEach(function(d) {
                                            table.string(d.name);
                                        });
                                    }
                                }),
                                renobit.database.schema.createTable(table_name + '_H', function(table) {
                                    table.string('id', 50);
                                    table.timestamp('created_at').defaultTo(RENOBIT.database.fn.now());
                                    table.unique(['id', 'created_at'])
                                    if (type.fields && type.fields.history_fields) {
                                        type.fields.history_fields.forEach(function(d) {
                                            table.string(d.name);
                                        });
                                    }
                                })
                            ]).then(() => {
                                resolve()
                            }).catch((err) => {
                                console.log(err);
                                reject();
                            })
                        } else {
                            resolve();
                        }
                    });
                });
                return promise;
            },
            columnInfo: function(type) {
                var promise = new Promise(function(resolve, reject) {
                    renobit.database(TB_ASSET_COMMON).columnInfo().then((info) => {
                        resolve(info);
                    })
                });
                return promise;
            },
            exportMapping: function(workbook) {
                var promise = new Promise(function(resolve, reject) {
                    var columns = [{
                        header: 'INST_ID',
                        key: 'INST_ID',
                        width: 20
                    }, {
                        header: 'ASSET_ID',
                        key: 'ASSET_ID',
                        width: 20
                    }];
                    var worksheet = workbook.addWorksheet('AssetMappingInfo');
                    worksheet.columns = columns;
                    renobit.database(TB_INSTANCE).select('INST_ID', 'ASSET_ID').whereNotNull('ASSET_ID').then((rows) => {
                        worksheet.addRows(rows);
                        resolve(workbook);
                    }).catch((err) => {
                        reject(err);
                    })
                });

                return promise;
            },
            export: function(type_info) {
                var self = this;
                var promise = new Promise(function(resolve, reject) {
                    var workbook = new Excel.Workbook();
                    workbook.creator = 'wemb';

                    var columns = [];
                    _.each(type_info.common_fields, (v, k) => {
                        columns.push({
                            header: v.name,
                            key: v.name,
                            width: 20
                        })
                    })

                    var count = 0;
                    _.each(type_info.types, (v, k) => {
                        var type_columns = _.clone(columns);
                        var worksheet = workbook.addWorksheet(v.id);
                        _.each(v.fields.type_fields, (item, index) => {
                            type_columns.push({
                                header: item.name,
                                key: item.name,
                                width: 20
                            })
                        })
                        worksheet.columns = type_columns;
                        var fields = type_info.common_fields.concat(v.fields.type_fields)
                        self.select({asset_type:v.id}, fields).then((result) => {
                            worksheet.addRows(result.result);
                            count++;
                            if (type_info.types.length == count) {
                                resolve(workbook);
                            }
                        }).catch((err) => {
                            reject(err);
                        })
                    })
                });
                return promise;
            },
            import: function(stream, type_info) {
                var promise = new Promise(function(resolve, reject) {
                    var import_data = {
                        types: {},
                        mapping_data: []
                    };

                    var workbook = new Excel.Workbook();
                    workbook.xlsx.read(stream).then(function(book) {
                        var common_header = [];
                        _.each(type_info.common_fields, (v, k) => {
                            if (v.name !== 'id' || v.name !== 'asset_type') common_header.push(v.name);
                        })
                        book.eachSheet(function(worksheet, id) {
                            var header = [];
                            var column_count = worksheet.columnCount;
                            if (worksheet.name === 'AssetMappingInfo') {
                                worksheet.eachRow({ includeEmpty: true }, function(row, rowNum) {
                                    var mapping_row = {}
                                    for (var i = 0; i < column_count; i++) {
                                        var cell = row.getCell(i + 1);
                                        if (rowNum === 1) {
                                            header.push(cell.value);
                                        } else {
                                            mapping_row[header[i]] = cell.value;
                                        }
                                    }
                                    if (rowNum !== 1) {
                                        import_data.mapping_data.push(mapping_row);
                                    }
                                });
                            } else {
                                var asset_type = worksheet.name;
                                import_data.types[asset_type] = [];
                                worksheet.eachRow({ includeEmpty: true }, function(row, rowNum) {
                                    var type_row = {}
                                    for (var i = 0; i < column_count; i++) {
                                        var cell = row.getCell(i + 1);
                                        if (rowNum === 1) {
                                            header.push(cell.text);
                                        } else {
                                            type_row[header[i]] = cell.text;
                                        }
                                    }
                                    if (rowNum !== 1) {
                                        import_data.types[asset_type].push(type_row);
                                    }
                                });

                            }
                        })
                        resolve(import_data);
                    });
                });
                return promise;
            }
        },
        components: {
            init: function(param) {
                var promise = new Promise(function(resolve, reject) {
                    renobit.database.schema.hasTable(TB_ASSET_COMPONENT).then(function(exists) {
                        if (!exists) {
                            renobit.database.schema.createTable(TB_ASSET_COMPONENT, function(table) {
                                table.string('file_id', 50).primary();
                                table.string('name', 50);
                                table.string('asset_type', 50);
                                table.boolean('batch');
                                table.json('props');
                            }).then(function(res) {
                                if (param) {
                                    asset.components.insert(param.comps).then((result) => {
                                        resolve();
                                    })
                                } else {
                                    resolve();
                                }
                            }).catch(function(err) {
                                console.log('why!!??')
                                reject(err);
                            })
                        } else {
                            resolve();
                        }
                    })
                });
                return promise;
            },
            insert: function(param) {
                var rows = param.map(function(d) {
                    d.props = JSON.stringify(d.props)
                    return d;
                })
                console.log(rows);
                return renobit.database.batchInsert(TB_ASSET_COMPONENT, rows)
            },
            update: function(param) {
                return renobit.database.transaction(function(trx) {
                    renobit.database(TB_ASSET_COMPONENT).transacting(trx).where({ file_id: param.file_id }).update({
                        props: JSON.stringify(param.props)
                    }).then(trx.commit).catch(trx.rollback);
                })
            },
            selectQuery: function() {
                return RENOBIT.database(TB_ASSET_COMPONENT).select('*');
            },
            select: function(param) {
                var promise = new Promise(function(resolve, reject) {
                    var result = [];
                    RENOBIT.database(TB_ASSET_COMPONENT).join(TB_PACKAGES + ' as packages', 'packages.FILE_ID', TB_ASSET_COMPONENT + '.file_id')
                        .rightJoin(TB_ASSET_TYPE + ' as asset_type', 'asset_type.id', TB_ASSET_COMPONENT + '.asset_type')
                        .where('id', '!=', 'common')
                        .select('*').map((row) => {
                            var item = result.find((v) => { return v.id === row.id });
                            var asset_props = typeof row.props === "object" ? row.props : JSON.parse(row.props);
                            var comp_props = typeof row.PROPS === "object" ? row.PROPS : JSON.parse(row.PROPS);
                            var test = _.merge(comp_props, asset_props);
                            console.log(test);
                            if (item) {
                                item.children.push({
                                    id: row.file_id,
                                    label: row.file_id,
                                    type: 'asset_comp',
                                    data: {
                                        file_id: row.file_id,
                                        name: row.name,
                                        asset_type: row.asset_type,
                                        pack_name: row.PACK_NAME,
                                        layer: row.LAYER,
                                        fields: typeof row.fields === "object" ? row.fields : JSON.parse(row.fields),
                                        props: test,
                                    }

                                })
                            } else {
                                item = {
                                    id: row.id,
                                    label: row.name,
                                    type: 'asset_type',
                                    data: {
                                        batch: row.batch
                                    },
                                    children: []
                                }
                                if (row.file_id) {
                                    item.children.push({
                                        id: row.file_id,
                                        label: row.file_id,
                                        type: 'asset_comp',
                                        data: {
                                            file_id: row.file_id,
                                            name: row.name,
                                            asset_type: row.asset_type,
                                            pack_name: row.PACK_NAME,
                                            layer: row.LAYER,
                                            fields: typeof row.fields === "object" ? row.fields : JSON.parse(row.fields),
                                            props: test,
                                        }
                                    });
                                }
                                result.push(item);
                            }
                            return row;
                        }).then((rows) => {
                            resolve(result);
                        })
                });
                return promise;
            },
            delete: function(param) {
                var promise = new Promise(function(resolve, reject) {

                });
                return promise;
            }
        }
    }

    return {
        init: function(param) {
            var init_data = {
                "common_fields": [{
                    "name": "id",
                    "label": "자산 아이디",
                    "type": "String",
                    "required": true,
                    "visible": true
                }, {
                    "name": "name",
                    "label": "자산 이름",
                    "type": "String",
                    "required": true,
                    "visible": true
                }, {
                    "name": "asset_type",
                    "label": "자산 유형",
                    "type": "String",
                    "required": true,
                    "visible": true
                }, {
                    "name": "parent_id",
                    "label": "부모 자산",
                    "type": "String",
                    "required": false,
                    "visible": false
                }, {
                    "name": "mng_id",
                    "label": "정담당자",
                    "type": "String",
                    "required": false,
                    "visible": false
                }, {
                    "name": "ip",
                    "label": "IP 주소",
                    "type": "String",
                    "required": false,
                    "visible": false
                }, {
                    "name": "serial_no",
                    "label": "시리얼번호",
                    "type": "String",
                    "required": false,
                    "visible": false
                }, {
                    "name": "props",
                    "label": "자산 속성",
                    "type": "Json",
                    "required": false,
                    "visible": false
                }, {
                    "name": "row_index",
                    "label": "Row Index",
                    "type": "Number",
                    "required": false,
                    "visible": false
                }, {
                    "name": "column_index",
                    "label": "Column Index",
                    "type": "Number",
                    "required": false,
                    "visible": false
                }, {
                    "name": "row_weight",
                    "label": "Row Weight",
                    "type": "Number",
                    "required": false,
                    "visible": false
                }, {
                    "name": "column_weight",
                    "label": "Column Weight",
                    "type": "String",
                    "required": false,
                    "visible": false
                }, {
                    "name": "padding",
                    "label": "Padding",
                    "type": "Number",
                    "required": false,
                    "visible": false
                }, {
                    "name": "rotate",
                    "label": "Rotate",
                    "type": "Number",
                    "required": false,
                    "visible": false
                }]
            };
            var options;
            if (param) {
                options = { types: param[TB_ASSET_TYPE], common_fields: init_data.common_fields, comps: param[TB_ASSET_COMPONENT] };
            }
            return asset.type.init(options).then(() => {
                return asset.data.init(options)
            }).then(() => {
                return asset.components.init(options)
            })
        },
        uninit: function() {
            asset.type.uninit().then(() => {
                console.log('uninit success')
            }).catch((err) => {
                RENOBIT.logger.error(err);
            })
        },
        regist: function(param) {
            renobit.app.get("/renobit/custom/packs/dcim_pack/main", ensureAuth, function(req, res, next) {
                res.sendFile(path.resolve(process.env.root_path, './custom/packs/dcim_pack/main/index.html'));
            })

            /* -----------------------------------asset type ----------------------------------------- */
            renobit.app.post("/asset/types", ensureAuth, function(req, res, next) {
                asset.data.create(req.body).then(() => {
                    asset.type.insert(req.body).then((result) => {
                        res.status(200).send();
                    }).catch(() => {
                        res.status(500).send({code:'ETC_001'});
                    })
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'});
                })
            })
            renobit.app.put("/asset/types", ensureAuth, function(req, res, next) {
                asset.data.alter(req.body).then((result) => {
                    asset.type.update(req.body).then((result) => {
                        res.status(200).send();
                    })
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'});
                })
            })
            renobit.app.delete("/asset/types", ensureAuth, function(req, res, next) {
                asset.type.delete(req.query).then((result) => {
                    // type 삭제시 테이블 삭제
                    asset.data.drop(req.query).then(() => {
                        res.status(200).send();
                    })
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'});
                })
            })
            renobit.app.get("/asset/types", ensureAuth, function(req, res, next) {
                    asset.type.select(req.query).then((result) => {
                        res.status(200).send(result);
                    }).catch((err) => {
                        res.status(500).send({code:'ETC_001'});
                    })
                })
                /* -----------------------------------asset data ----------------------------------------- */
            renobit.app.post("/asset/data", ensureAuth, function(req, res, next) {
                asset.type.select({id:req.body.asset_type}).then((schema) => {
                    var param = {
                        type_data:{}
                    }
                    if(schema.types.length > 0) {
                        _.each(schema.common_fields, (v,k) => {
                            switch(v.type) {
                                case 'Password' :
                                    param[v.name] = RENOBIT.utils.base64.encode(req.body[v.name]);
                                break;
                                case 'Number' :
                                    if(!isNaN(parseFloat(req.body[v.name]))) {
                                        param[v.name] = req.body[v.name]
                                    }
                                break;
                                case 'String' :
                                    param[v.name] = req.body[v.name]
                                break;
                            }
                        });
                        _.each(schema.types[0].fields.type_fields, (v,k) => {
                            switch(v.type) {
                                case 'Password' :
                                    param.type_data[v.name] = RENOBIT.utils.base64.encode(req.body.type_data[v.name]);
                                break;
                                case 'Number' :
                                    if(!isNaN(parseFloat(req.body.type_data[v.name]))) {
                                        param.type_data[v.name] = req.body.type_data[v.name]
                                    }
                                break;
                                case 'String' :
                                    param.type_data[v.name] = req.body.type_data[v.name]
                                break;
                            }
                        })
                    }
                    asset.data.insert(param).then(() => {
                        res.status(200).send();
                        process.send({event: "asset/data", data:""});
                    }).catch((err) => {
                        renobit.logger.error(err);
                        res.status(500).send({code:'ETC_001'})
                    })
                });
            })
            renobit.app.put("/asset/update", function(req, res, next) {
                asset.type.select({ id: req.body.asset_type }).then((result) => {
                    var common_data = {};
                    var type_data = {};
                    if(result.types.length > 0) {
                        _.each(result.common_fields, (v,k) => {
                            switch(v.type) {
                                case 'Password' :
                                    common_data[v.name] = RENOBIT.utils.base64.encode(req.body[v.name]);
                                break;
                                case 'Number' :
                                    if(!isNaN(parseFloat(req.body[v.name]))) {
                                        common_data[v.name] = req.body[v.name]
                                    }
                                break;
                                case 'String' :
                                    common_data[v.name] = req.body[v.name]
                                break;
                            }
                        });
                        _.each(result.types[0].fields.type_fields, (v,k) => {
                            switch(v.type) {
                                case 'Password' :
                                    type_data[v.name] = RENOBIT.utils.base64.encode(req.body[v.name]);
                                break;
                                case 'Number' :
                                    if(!isNaN(parseFloat(req.body[v.name]))) {
                                        type_data[v.name] = req.body[v.name]
                                    }
                                break;
                                case 'String' :
                                    type_data[v.name] = req.body[v.name]
                                break;
                            }
                        })
                        renobit.database.transaction(function(trx) {
                            renobit.database(TB_ASSET_COMMON).transacting(trx).where({ id: req.body.id }).update(common_data).then(() => {
                                renobit.database(TB_PREFIX + req.body.asset_type.toUpperCase()).transacting(trx).where({ id: req.body.id }).update(type_data).then(trx.commit).catch(trx.rollback);
                            }).catch(trx.rollback)
                        }).then(() => {
                            res.status(200).send();
                        }).catch(() => {
                            res.status(500).send({code:'ETC_001'});
                        })
                    }
                })
            })
            renobit.app.post("/asset/push", ensureAuth, function(req, res, next) {
                asset.data.insert(req.body).then(() => {
                    // if(renobit.socket.subscribe["asset/data/" + req.body.id]) {
                    //     renobit.socket.subscribe["asset/data/" + req.body.id].forEach((socket) => {
                    //         socket.emit('publish', req.body);
                    //     })
                    // }
                    res.status(200).send()
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'})
                })
            })
            renobit.app.post("/asset/data/import", ensureAuth, function(req, res, next) {
                if (req.busboy) {
                    req.busboy.on("file", function(fieldName, fileStream, fileName, encoding, mimeType) {
                        asset.type.select({}).then((result) => {
                            asset.data.import(fileStream, result).then((import_data) => {
                                var count = 0;
                                var insert_types = 0;
                                renobit.database.transaction(function(trx) {
                                    _.each(import_data.types, (item, type) => {
                                        var table_name = TB_PREFIX + type.toUpperCase();
                                        renobit.database.schema.hasTable(table_name).then(function(exists) {
                                            if (exists) {
                                                insert_types++;
                                                var type_data = [];
                                                var common_data = [];
                                                var type_schema = result.types.find((a) => { return a.id === type });
                                                var id_arr = item.map((d) => {
                                                    var type_row = {}
                                                    var common_row = {}
                                                    _.each(result.common_fields, (v, k) => {
                                                        if (d.hasOwnProperty(v.name)) {
                                                            if (v.name === 'id') { type_row[v.name] = d[v.name] };
                                                            switch(v.type) {
                                                                case 'Password' :
                                                                common_row[v.name] = RENOBIT.utils.base64.encode(d[v.name]);
                                                                break;
                                                                case 'Number' :
                                                                    if(!isNaN(parseFloat(d[v.name]))) {
                                                                        common_row[v.name] = d[v.name]
                                                                    }
                                                                break;
                                                                case 'String' :
                                                                common_row[v.name] = d[v.name]
                                                                break;
                                                            }
                                                        }
                                                    });
                                                    _.each(type_schema.fields.type_fields, (v, k) => {
                                                        if (d.hasOwnProperty(v.name)) {
                                                            switch(v.type) {
                                                                case 'Password' :
                                                                type_row[v.name] = RENOBIT.utils.base64.encode(d[v.name]);
                                                                break;
                                                                case 'Number' :
                                                                    if(!isNaN(parseFloat(d[v.name]))) {
                                                                        type_row[v.name] = d[v.name]
                                                                    }
                                                                break;
                                                                case 'String' :
                                                                type_row[v.name] = d[v.name]
                                                                break;
                                                            }
                                                        }
                                                    })
                                                    type_data.push(type_row);
                                                    common_data.push(common_row);
                                                    return d.id;
                                                });

                                                renobit.database(TB_ASSET_COMMON).transacting(trx).whereIn('id', id_arr).del()
                                                    .then(() => {
                                                        return renobit.database(table_name).transacting(trx).whereIn('id', id_arr).del().then(() => {
                                                            return renobit.database.batchInsert(TB_ASSET_COMMON, common_data).transacting(trx).then(() => {
                                                                return renobit.database.batchInsert(table_name, type_data).transacting(trx).then(() => {
                                                                    count++;
                                                                    if (count === insert_types) {
                                                                        trx.commit();
                                                                    }
                                                                })
                                                            })
                                                        })
                                                    }).catch((err) => {
                                                        trx.rollback();
                                                    })
                                            }
                                        })
                                    });
                                }).then(() => {
                                    if (import_data.mapping_data.length > 0) {
                                        var promises = []
                                        var update_arr = import_data.mapping_data.map((row) => {
                                            promises.push(renobit.database(TB_INSTANCE).where({ INST_ID: row.INST_ID }).update({ ASSET_ID: row.ASSET_ID }));
                                            return row.ASSET_ID;
                                        })
                                        renobit.database(TB_INSTANCE).whereIn("ASSET_ID", update_arr).update({ ASSET_ID: null }).then(() => {
                                            Promise.all(promises).then(() => {
                                                res.status(200).send();
                                                process.send({ event: "asset/data", data: "" });
                                            })
                                        })
                                    } else {
                                        res.status(200).send();
                                        process.send({ event: "asset/data", data: "" });
                                    }
                                }).catch((err) => {
                                    renobit.logger.error(err);
                                    res.status(500).send({code:'ETC_001'});
                                })
                            })
                        }).catch((err) => {
                            renobit.logger.error(err);
                            res.status(500).send({code:'ETC_001'});
                        })
                    });
                    return req.pipe(req.busboy);
                }
            })
            renobit.app.put("/asset/data", ensureAuth, function(req, res, next) {
                asset.type.select({id:req.body.asset_type}).then((schema) => {
                    var param = {
                        type_data:{}
                    }
                    if(schema.types.length > 0) {
                        _.each(schema.common_fields, (v,k) => {
                            switch(v.type) {
                                case 'Password' :
                                    param[v.name] = RENOBIT.utils.base64.encode(req.body[v.name]);
                                break;
                                case 'Number' :
                                    if(!isNaN(parseFloat(req.body[v.name]))) {
                                        param[v.name] = req.body[v.name]
                                    }
                                break;
                                case 'String' :
                                    param[v.name] = req.body[v.name]
                                break;
                            }
                        });
                        _.each(schema.types[0].fields.type_fields, (v,k) => {
                            switch(v.type) {
                                case 'Password' :
                                    param.type_data[v.name] = RENOBIT.utils.base64.encode(req.body.type_data[v.name]);
                                break;
                                case 'Number' :
                                    if(!isNaN(parseFloat(req.body.type_data[v.name]))) {
                                        param.type_data[v.name] = req.body.type_data[v.name]
                                    }
                                break;
                                case 'String' :
                                    param.type_data[v.name] = req.body.type_data[v.name]
                                break;
                            }
                        })
                    }
                    asset.data.update(param).then(() => {
                        res.status(200).send();
                        process.send({event: "asset/data", data:""});
                    }).catch((err) => {
                        renobit.logger.error(err);
                        res.status(500).send({code:'ETC_001'})
                    })
                })
            })
            renobit.app.delete("/asset/data", ensureAuth, function(req, res, next) {
                var id_arr = req.query.id.split(',');
                var asset_type = req.query.asset_type;
                renobit.database(TB_INSTANCE).whereIn("ASSET_ID", id_arr).then((rows) => {
                    if (rows.length > 0) {
                        res.status(500).send({code:'ETC_001'})
                    } else {
                        asset.data.delete(asset_type, id_arr).then(() => {
                            res.status(200).send();
                            process.send({ event: "asset/data", data: "" });
                        }).catch((err) => {
                            renobit.logger.error(err);
                            res.status(500).send({code:'ETC_001'})
                        })
                    }
                })
            })
            renobit.app.get("/asset/data", ensureAuth, function(req, res, next) {
                var type_query = req.query.asset_type ? { id: req.query.asset_type } : {};
                asset.type.select(type_query).then((schema) => {
                    var fields = schema.common_fields.concat(schema.types[0].fields.type_fields)
                    asset.data.select(req.query, fields).then((result) => {
                        result["schema"] = schema;
                        res.status(200).send(result)
                    }).catch((err) => {
                        renobit.logger.error(err);
                        res.status(500).send({code:'ETC_001'})
                    })
                })
            })
            renobit.app.delete("/asset/all", ensureAuth, function(req, res, next) {
                asset.type.select({}).then((schema) => {
                    renobit.database.transaction(function(trx) {
                        var del_arr = [renobit.database(TB_ASSET_COMMON).transacting(trx).del()]
                        _.each(schema.types, (v,k) => {
                            del_arr.push(renobit.database(TB_PREFIX + v.id.toUpperCase()).transacting(trx).del())
                        })
                        Promise.all(del_arr).then(trx.commit).catch(trx.rollback);
                    }).then(() => {
                        res.status(200).send();
                        process.send({ event: "asset/data", data: "" });
                    }).catch((err) => {
                        res.status(500).send({code:'ETC_001'});
                    })
                }).catch((err) => {
                    res.status(200).send();
                })
            })
            renobit.app.get("/asset/all", ensureAuth, function(req, res, next) {
                var response_data = {
                    count: 0,
                    result: {},
                    schema: {}
                }
                asset.type.select({}).then((schema) => {
                    var count = 0;
                    _.each(schema.types, (v, k) => {
                        var fields = schema.common_fields.concat(v.fields.type_fields);
                        asset.data.select({ asset_type: v.id }, fields).then((data) => {
                            response_data.count += data.count;
                            if (req.query.page_id) {
                                data.result = data.result.filter((d) => { return d.page_id === req.query.page_id })
                            }
                            response_data.result[v.id] = data.result;
                            count--;
                            if (count === 0) {
                                res.status(200).send(response_data);
                            }
                        });
                        count++;
                    })
                    response_data.schema = schema;
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            })
            renobit.app.get("/asset/data/export", ensureAuth, function(req, res, next) {
                    asset.type.select({}).then((result) => {
                        asset.data.export(result).then((_workbook) => {
                            asset.data.exportMapping(_workbook).then((workbook) => {
                                workbook.xlsx.writeBuffer().then((buffer) => {
                                    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                                    res.status(200).send(buffer);
                                })
                            })
                        }).catch((err) => {
                            renobit.logger.error(err);
                            res.status(500).send({code:'ETC_001'})
                        })
                    })
                })
                /* -----------------------------------asset component ----------------------------------------- */
            renobit.app.post("/asset/components", ensureAuth, function(req, res, next) {
                var rows = req.body.components.map((comp) => {
                    return {
                        file_id: comp.FILE_ID,
                        name: comp.NAME,
                        asset_type: req.body.asset_type,
                        batch: 1,
                        props: comp.PROPS
                    }
                })
                renobit.database(TB_ASSET_COMPONENT).where({ asset_type: req.body.asset_type }).del().then(() => {
                    renobit.database.batchInsert(TB_ASSET_COMPONENT, rows).then(() => {
                        res.status(200).send();
                    }).catch(() => {
                        res.status(500).send({code:'ETC_001'});
                    })
                })

            })
            renobit.app.delete("/asset/components", ensureAuth, function(req, res, next) {
                res.send('Not implemented.')
            })
            renobit.app.put("/asset/components", ensureAuth, function(req, res, next) {
                asset.components.update(req.body).then(() => {
                    res.status(200).send();
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'})
                })
            })
            renobit.app.get("/asset/components", ensureAuth, function(req, res, next) {
                asset.components.select().then((result) => {
                    res.status(200).send(result);
                })
            })
            renobit.app.get("/asset/packages", ensureAuth, function(req, res, next) {
                    asset.components.selectQuery().map((row) => {
                        row.props = typeof row.props === "object" ? row.props : JSON.parse(row.props);
                        return row;
                    }).then((result) => {
                        res.status(200).send(result);
                    })
                })
                /* ----------------------------------------------------------------------------------------------- */

            renobit.app.post("/asset/batch", ensureAuth, function(req, res, next) {
                var update_list = req.body;
                renobit.database.transaction(function(trx) {
                    var initialize_assets = [];
                    var promises = [];
                    _.each(update_list, (v, k) => {
                        if (initialize_assets.includes(v.asset_id)) {
                            throw new Error("Duplicate")
                        }
                        initialize_assets.push(v.asset_id);
                        promises.push(renobit.database(TB_INSTANCE).transacting(trx).where({ INST_ID: v.instance_id }).update({ ASSET_ID: v.asset_id }));
                    });
                    renobit.database(TB_INSTANCE).transacting(trx).whereIn("ASSET_ID", initialize_assets).update({ ASSET_ID: null }).then(() => {
                        Promise.all(promises).then(trx.commit).catch(trx.rollback);
                    })
                }).then(() => {
                    res.status(200).send();
                    process.send({ event: "asset/data", data: "" });
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });
            renobit.app.post("/asset/mount", ensureAuth, function(req, res, next) {
                asset.type.select({ id: req.body.asset_type }).then((result) => {
                    var common_data = {};
                    var type_data = {};
                    if (result.types.length > 0) {
                        _.each(result.common_fields, (v, k) => {
                            if (req.body[v.name]) common_data[v.name] = req.body[v.name];
                        });
                        _.each(result.types[0].fields.type_fields, (v, k) => {
                            if (req.body[v.name]) type_data[v.name] = req.body[v.name];
                        })
                        renobit.database.transaction(function(trx) {
                            renobit.database(TB_ASSET_COMMON).transacting(trx).where({ id: req.body.id }).update(common_data).then(() => {
                                renobit.database(TB_PREFIX + req.body.asset_type.toUpperCase()).transacting(trx).where({ id: req.body.id }).update(type_data).then(() => {
                                    renobit.database(TB_ASSET_COMMON).transacting(trx).where({ parent_id: req.body.id }).update({ parent_id: null }).then(() => {
                                        var children_update = [];
                                        _.each(req.body.children, (child, i) => {
                                            child["parent_id"] = req.body.id;
                                            children_update.push(renobit.database(TB_ASSET_COMMON).transacting(trx).where({ id: child.id }).update(child));
                                        })
                                        Promise.all(children_update).then(trx.commit).catch(trx.rollback);
                                    });
                                }).catch(trx.rollback);
                            }).catch(trx.rollback)
                        }).then(() => {
                            res.status(200).send();
                        }).catch(() => {
                            res.status(500).send({code:'ETC_001'});
                        })
                    }
                })
                res.status(200).send();
            });
        },

        unregist: function() {
            try {
                var remove_route = [];
                var stack = renobit.app._router.stack;
                stack.forEach(removeMiddlewares);

                function removeMiddlewares(layer, i) {
                    if (layer.handle.name === "bound dispatch") {
                        if (layer.route.path.includes("/asset/") || layer.route.path.includes("/renobit/custom/packs/dcim_pack/main")) {
                            remove_route.push(layer);
                        }
                    }
                }
                remove_route.forEach((v, k) => {
                    var idx = stack.indexOf(v);
                    stack.splice(idx, 1);
                })
            } catch (error) {
                RENOBIT.logger.error(error);
            }
        },
        backup: function() {
            var promise = new Promise((resolve, reject) => {
                var result = {};
                RENOBIT.database(TB_ASSET_TYPE).select('*').map((row) => {
                    var fields = typeof row.fields === "object" ? row.fields : JSON.parse(row.fields);
                    if (row.id !== 'common') {
                        var table_name = TB_PREFIX + row.id.toUpperCase();
                        var history_table_name = table_name + "_H";
                        result[history_table_name] = {
                            schema: {
                                id: {
                                    type: "string",
                                    defaultValue: null,
                                    maxLength: 50,
                                    nullable: false,
                                    pk: true
                                },
                                created_at: {
                                    type: "timestamp",
                                    defaultValue: null,
                                    maxLength: null,
                                    nullable: false,
                                    pk: true
                                }
                            },
                            rows: []
                        }
                        result[table_name] = {
                            schema: {
                                id: {
                                    type: "string",
                                    defaultValue: null,
                                    maxLength: 50,
                                    nullable: false,
                                    pk: true
                                }
                            },
                            rows: []
                        }
                        fields.type_fields.forEach((d) => {
                                result[table_name].schema[d.name] = {
                                    type: "string",
                                    defaultValue: null,
                                    maxLength: 255,
                                    nullable: true
                                }
                            })
                            // fields.history_fields.forEach((d) => {
                            //     result[history_table_name].schema[d.name] = {
                            //         type:"string",
                            //         defaultValue:null,
                            //         maxLength:255,
                            //         nullable:true
                            //     }
                            // })
                        RENOBIT.database(table_name).select('*').then((rows) => {
                                result[table_name].rows = rows;
                            })
                            // RENOBIT.database(history_table_name).select('*').map((row) => {
                            //     if(row.created_at) {
                            //         row.created_at = new Date(row.created_at).format('yyyy-MM-dd HH:mm:ss.fff');
                            //     }
                            //     return row;
                            // }).then((rows) => {
                            //     result[history_table_name].rows = rows;
                            // })
                    } else {
                        result[TB_ASSET_COMMON] = {
                            schema: {},
                            rows: []
                        };
                        fields.forEach((d) => {
                            result[TB_ASSET_COMMON].schema[d.name] = {
                                type: d.type.toLowerCase(),
                                defaultValue: null,
                                maxLength: 50,
                                nullable: true,
                                pk: d.name === 'id'
                            }
                        })
                    }
                    return row;
                }).then((type_rows) => {
                    result[TB_ASSET_TYPE] = {
                        schema: TYPE_SCHEMA,
                        rows: type_rows
                    }
                    RENOBIT.database(TB_ASSET_COMMON).select('*').then((common_rows) => {
                        result[TB_ASSET_COMMON].rows = common_rows;
                        RENOBIT.database(TB_ASSET_COMPONENT).select('*').then((comp_rows) => {
                            result[TB_ASSET_COMPONENT] = {
                                schema: COMP_SCHEMA,
                                rows: comp_rows
                            }
                            resolve(result);
                        })
                    })
                })
            })
            return promise;
        }
    }
};