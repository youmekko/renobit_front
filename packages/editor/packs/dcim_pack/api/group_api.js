module.exports = function(renobit) {
    'use strict';
    const _ = require('lodash');
    const TB_ASSET_GROUP = 'TB_ASSET_GROUP';
    const TB_ASSET_TYPE = 'TB_ASSET_TYPE';
    const SCHEMA = {"group_name":{"type":"string","defaultValue":null,"maxLength":50,"nullable":false},"asset_type":{"type":"string","defaultValue":null,"maxLength":50,"nullable":false},"seq":{"type":"integer","defaultValue":null,"maxLength":null,"nullable":false}};

    const group = {
        init: function() {
            var promise = new Promise(function(resolve,reject){
                renobit.database.schema.hasTable(TB_ASSET_GROUP).then(function(exists) {
                    if(!exists) {
                        renobit.database.schema.createTable(TB_ASSET_GROUP, function(table){
                            table.string('group_name', 50);
                            table.string('asset_type', 50);
                            table.integer('seq');
                        }).then(function(res) {
                            resolve();
                        }).catch(function(err) {
                            reject(err);
                        })
                    } else {
                        resolve();
                    }
                })
            });
            return promise;
        },
        uninit: function() {
            return renobit.database.schema.dropTableIfExists(TB_ASSET_GROUP);
        },
        getGroups: function() {
            return renobit.database(TB_ASSET_GROUP).join(TB_ASSET_TYPE + ' as asset_type','asset_type.id', TB_ASSET_GROUP+'.asset_type').orderBy('seq', 'asc');
        },
        setGroup: function(group_name,rows) {
            var promise = new Promise(function(resolve,reject){
                group.delGroup(group_name).then(() => {
                    renobit.database.batchInsert(TB_ASSET_GROUP, rows).then(() => {
                        resolve();
                    })
                }).catch((err) => {
                    reject();
                })
            })
            return promise;
        },
        delGroup: function(group_name) {
            return renobit.database(TB_ASSET_GROUP).where("group_name", group_name).del()
        }
    }

    function ensureAuth(req, res, next) {
        if (req.isAuthenticated()) {
            next();
        } else {
            res.status(401).send({code:'AUTH_001'});
        }
    }

    return {
        init: function(param) {
            return group.init();
        },
        uninit: function() {
            group.uninit().then(() => {
                console.log("group table drop success");
            });
        },
        regist: function() {
            renobit.app.get("/asset/group", ensureAuth, function(req, res, next) {
                var result = [];
                group.getGroups().map((row) => {
                    var init_group = {
                        name:row.group_name,
                        types:[{
                            id:row.id,
                            name:row.name
                        }]
                    }
                    var group_info = result.find((d) => { return d.name === row.group_name});
                    if(group_info) {
                        group_info.types.push({
                            id:row.id,
                            name:row.name
                        })
                    } else {
                        result.push(init_group);
                    }
                }).then(() => {
                    res.status(200).send(result);
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });
            renobit.app.post("/asset/group", ensureAuth, function(req, res, next) {
                var rows = [];
                
                _.each(req.body.types, (v,k) => {
                    rows.push({
                        group_name:req.body.name,
                        asset_type:v,
                        seq:k
                    })
                });
                group.setGroup(req.body.name, rows).then(() => {
                    res.status(200).send();
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });

            renobit.app.delete("/asset/group", ensureAuth, function(req, res, next) {
                group.delGroup(req.query.group_name).then(() => {
                    res.status(200).send();
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });
        },

        unregist: function() {
            try {
                var remove_route = [];
                var stack = renobit.app._router.stack;
                stack.forEach(removeMiddlewares);

                function removeMiddlewares(layer, i) {
                    if (layer.handle.name === "bound dispatch") {
                        if (layer.route.path.includes("/asset/group")) {
                            remove_route.push(layer);
                        }
                    }
                }

                remove_route.forEach((v, k) => {
                    var idx = stack.indexOf(v);
                    stack.splice(idx, 1);
                })
            } catch (error) {
                RENOBIT.logger.error(error);
            }
        },
        backup : function() {
            var promise = new Promise((resolve, reject) => {
                var result = {};
                renobit.database(TB_ASSET_GROUP).select('*').then((rows) => {
                    result[TB_ASSET_GROUP] = {
                        schema:SCHEMA,
                        rows:rows
                    }
                    resolve(result);
                })
            })
            return promise;
        }
    }
}