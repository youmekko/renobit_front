module.exports = function(renobit) {
    'use strict';
    const _ = require('lodash');
    const TB_ASSET_RELATION = 'TB_ASSET_RELATION';
    const TB_ASSET_PREFIX = 'A_'
    const SCHEMA = {
          "parent_id":{"type":"string","defaultValue":null,"maxLength":50,"nullable":false},
          "child_id":{"type":"string","defaultValue":null,"maxLength":50,"nullable":false},
          "direction":{"type":"string","defaultValue":null,"maxLength":50,"nullable":true},
          "index":{"type": "integer", "defaultValue": null, "maxLength": null, "nullable": true}
    };

    const relation = {
        init: function() {
            var promise = new Promise(function(resolve,reject){
                renobit.database.schema.hasTable(TB_ASSET_RELATION).then(function(exists) {
                    if(!exists) {
                        renobit.database.schema.createTable(TB_ASSET_RELATION, function(table){
                              var pk_arr = [];
                              _.each(SCHEMA, (detail, column_name) => {
                                    var column = detail.type === 'string' ? table[detail.type](column_name, detail.maxLength) : table[detail.type](column_name);
                                    if(detail.type === "timestamp") column.defaultTo(RENOBIT.database.fn.now())
                                    if(detail.defaultValue !== null) column.defaultTo(detail.defaultValue);
                                    else column.notNullable();
                                    if(detail.nullable) column.nullable();
                                    if(detail.pk) pk_arr.push(column_name);
                              })
                              if(pk_arr.length > 0) table.primary(pk_arr);
                        }).then(function(res) {
                            resolve();
                        }).catch(function(err) {
                            reject(err);
                        })
                    } else {
                        resolve();
                    }
                })
            });
            return promise;
        },
        uninit: function() {
            return renobit.database.schema.dropTableIfExists(TB_ASSET_RELATION);
        },
        getRelation: function(parent_id) {
            return renobit.database(TB_ASSET_RELATION).select('*').where({parent_id:parent_id});
        },
        setRelation: function(parent_id,rows) {
            var promise = new Promise(function(resolve,reject){
                relation.delRelation(parent_id).then(() => {
                    renobit.database.batchInsert(TB_ASSET_RELATION, rows).then(() => {
                        resolve();
                    })
                }).catch((err) => {
                    reject();
                })
            })
            return promise;
        },
        delRelation: function(parent_id) {
            return renobit.database(TB_ASSET_RELATION).where("parent_id", parent_id).del();
        }
    }

    function ensureAuth(req, res, next) {
        if (req.isAuthenticated()) {
            next();
        } else {
            res.status(401).send({code:'AUTH_001'});
        }
    }

    return {
        init: function(param) {
            return relation.init();
        },
        uninit: function() {
            relation.uninit().then(() => {
                console.log("relation table drop success");
            });
        },
        regist: function() {
            renobit.app.get("/asset/relation", ensureAuth, function(req, res, next) {
                relation.getRelation(req.query.parent_id).then((rows) => {
                    res.status(200).send(rows);
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });
            renobit.app.post("/asset/relation", ensureAuth, function(req, res, next) {
                var rows = [];
                
                _.each(req.body.childs, (v,k) => {
                    rows.push({
                        parent_id:req.body.parent_id,
                        child_id:v.child_id,
                        index: v.index,
                        direction: v.direction
                    })
                });

                relation.setRelation(req.body.parent_id, rows).then(() => {
                    res.status(200).send();
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });

            renobit.app.delete("/asset/relation", ensureAuth, function(req, res, next) {
                relation.delRelation(req.query.parent_id).then(() => {
                    res.status(200).send();
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });
        },

        unregist: function() {
            try {
                var remove_route = [];
                var stack = renobit.app._router.stack;
                stack.forEach(removeMiddlewares);

                function removeMiddlewares(layer, i) {
                    if (layer.handle.name === "bound dispatch") {
                        if (layer.route.path.includes("/asset/relation")) {
                            remove_route.push(layer);
                        }
                    }
                }

                remove_route.forEach((v, k) => {
                    var idx = stack.indexOf(v);
                    stack.splice(idx, 1);
                })
            } catch (error) {
                RENOBIT.logger.error(error);
            }
        },
        backup : function() {
            var promise = new Promise((resolve, reject) => {
                var result = {};
                renobit.database(TB_ASSET_RELATION).select('*').then((rows) => {
                    result[TB_ASSET_RELATION] = {
                        schema:SCHEMA,
                        rows:rows
                    }
                    resolve(result);
                })
            })
            return promise;
        }
    }
}
