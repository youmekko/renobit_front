module.exports = function (renobit) {
    'use strict';
    var _ = require('lodash');
    var path = require('path');
    const TB_ASSET_GRADE = 'TB_ASSET_GRADE';
    const TB_GRADE = 'TB_GRADE';
    const TB_PREFIX = 'TB_ASSET_';
    const TB_ASSET_COMMON = 'TB_ASSET_COMMON';
    const SCHEMA = {
          "asset_id": {"type": "string", "defaultValue": null, "maxLength": 50, "nullable": false},
          "grade_id": {"type": "string", "defaultValue": null, "maxLength": 36, "nullable": false}
    }

    const grade = {
        uninit:function() {
            return renobit.database.schema.dropTableIfExists(TB_ASSET_GRADE);
        },
        init: function () {
              var promise = new Promise(function (resolve, reject) {
                    renobit.database.schema.hasTable(TB_ASSET_GRADE).then(function (exists) {
                          if (!exists) {
                                renobit.database.schema.createTable(TB_ASSET_GRADE, function (table) {
                                      var pk_arr = [];
                                      _.each(SCHEMA, (detail, column_name) => {
                                            var column = detail.type === 'string' ? table[detail.type](column_name, detail.maxLength) : table[detail.type](column_name);
                                            if(detail.type === "timestamp") column.defaultTo(RENOBIT.database.fn.now())
                                            if(detail.defaultValue !== null) column.defaultTo(detail.defaultValue);
                                            else column.notNullable();
                                            if(detail.nullable) column.nullable();
                                            if(detail.pk) pk_arr.push(column_name);
                                      })
                                      if(pk_arr.length > 0) table.primary(pk_arr);
                                }).then(function (res) {
                                      resolve();
                                }).catch(function (err) {
                                      reject(err);
                                })
                          } else {
                                resolve();
                          }
                    })
              });
              return promise;
        },
        gradeList: function () {
            var promise = new Promise((resolve, reject) => {
                renobit.database.select('GRADE_ID AS grade_id', 'NAME AS name', 'DESCRIPTION AS description', 'SEQ AS seq').from(TB_GRADE).orderBy("SEQ", 'asc')
                    .map(item => {
                        item.seq = item.seq.toString();
                        return item
                    }).then(result => {
                    resolve(result);
                }).catch(error => {
                    reject(error);
                })
            })

            return promise;
        },

        select: function (query) {
            var promise = new Promise((resolve, reject) => {
                var response = {
                    count: 0,
                    result: []
                };
                var queryBuilder = renobit.database(TB_ASSET_COMMON);

                var whereQuery = _.clone(query);
                if (query.limit) delete whereQuery.limit;
                if (query.offset) delete whereQuery.offset;
                if (query.orderby) delete whereQuery.orderby;
                if (query.grade_id) delete whereQuery.grade_id;

                if(query.asset_type) {
                    queryBuilder = queryBuilder.innerJoin(TB_PREFIX + query.asset_type.toUpperCase(),
                        TB_ASSET_COMMON + '.id', TB_PREFIX + query.asset_type.toUpperCase() + '.id').where({asset_type:query.asset_type});
                    if(query.id) {
                        queryBuilder.andWhere({"TB_ASSET_COMMON.id": query.id})
                        delete whereQuery.id;
                    }
                    delete whereQuery.asset_type;
                }

                _.each(whereQuery, (v,k) => {
                    queryBuilder.andWhere(k, 'like', '%' + v + '%');
                })

                queryBuilder.clone().count({count: 'TB_ASSET_COMMON.id'}).first().then((row) => {
                    response.count = row.count;
                    if(process.env.database === 'mysql') {
                        var subQuery = renobit.database(TB_ASSET_GRADE).select('asset_id').as('assigned')
                        .whereRaw('`asset_id` = `TB_ASSET_COMMON`.`id`')
                        .andWhere('grade_id', '=', query.grade_id)
                    } else {
                        var subQuery = renobit.database(TB_ASSET_GRADE).select('asset_id').as('assigned')
                        .whereRaw('"asset_id" = "TB_ASSET_COMMON"."id"')
                        .andWhere('grade_id', '=', query.grade_id)
                    }

                    if (query.limit) queryBuilder.limit(parseInt(query.limit));
                    if (query.offset) queryBuilder.offset(parseInt(query.offset));
                    if (query.orderby) {
                        try {
                            var orderby = JSON.parse(query.orderby);
                            _.each(orderby, (v, k) => {
                                var key = k === 'id' ? TB_ASSET_COMMON + '.id' : k;
                                var order = v;
                                queryBuilder.orderBy(key, order);
                            })
                        } catch (error) {
                            reject();
                        }
                    } else {
                        queryBuilder.orderBy('id', 'asc');
                    }

                    queryBuilder.select('TB_ASSET_COMMON.id', 'name', 'asset_type', subQuery).then(result => {
                        response.result = result;
                        resolve(response);
                    })
                })

            }).catch((err) => {
                reject();
            })

            return promise;
        },

        insert: function (query) {
            var promise = new Promise(function(resolve,reject){
                renobit.database(TB_ASSET_GRADE).del().where({grade_id: query.grade_id}).then(()=>{
                    renobit.database.batchInsert(TB_ASSET_GRADE, query.target).then(()=>{
                        resolve();
                    })
                }).catch(err=> {
                    reject(err);
                });
            })

            return promise;
        },

        delete:function(query) {
            var promise = new Promise(function(resolve,reject){
                renobit.database(TB_ASSET_GRADE).del().where({grade_id: query.grade_id}).then(()=>{
                    resolve()
                }).catch(err=>{
                    reject(err);
                })
            });
            return promise;
        }
    }

    function ensureAuth(req, res, next) {
        if (req.isAuthenticated()) {
            next();
        } else {
            res.status(401).send({code:'AUTH_001'});
        }
    }

    return {
        init: function (param) {
            return grade.init();
        },
        uninit : function() {
            grade.uninit().then(()=>{
                console.log("grade table drop success");
            });
        },
        regist: function (param) {
            renobit.app.get("/grade/list", ensureAuth, function (req, res, next) {
                grade.gradeList().then((result) => {
                    res.status(200).send(result);
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'});
                })
            });

            renobit.app.get("/grade/data", ensureAuth, function (req, res, next) {
                grade.select(req.query).then((result) => {
                    res.status(200).send(result);
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'});
                })
            });

            renobit.app.delete("/grade/data", ensureAuth, function (req, res, next) {
                grade.delete(req.body).then(() => {
                    res.status(200).send();
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'});
                })
            });

            renobit.app.post("/grade/data", ensureAuth, function (req, res, next) {
                grade.insert(req.body).then(() => {
                    res.status(200).send();
                }).catch((err) => {
                    renobit.logger.error(err);
                    res.status(500).send({code:'ETC_001'});
                })
            });
        },

        unregist: function () {
            try {
                var remove_route = [];
                var stack = renobit.app._router.stack;
                stack.forEach(removeMiddlewares);
                function removeMiddlewares(layer, i) {
                      if(layer.handle.name === "bound dispatch") {
                            if(layer.route.path.includes("/grade/")) {
                            remove_route.push(layer);
                            }
                      }
                }
                remove_route.forEach((v,k) => {
                      var idx = stack.indexOf(v);
                      stack.splice(idx, 1);
                })
          } catch (error) {
                RENOBIT.logger.error(error);
          }
        },
        backup : function() {
              var promise = new Promise((resolve, reject) => {
                    var result = {};
                    renobit.database(TB_ASSET_GRADE).select('*').then((rows) => {
                          result[TB_ASSET_GRADE] = {
                                schema:SCHEMA,
                                rows:rows
                          }
                          resolve(result);
                    })
              })
              return promise;
        }
    }
}
