module.exports = function(renobit) {
    'use strict';
    const _ = require('lodash');
    const TB_RESOURCE = 'TB_RESOURCE';
    const TB_ASSET_RESOURCE = 'TB_ASSET_RESOURCE';
    const SCHEMA = {"asset_type":{"type":"string","defaultValue":null,"maxLength":50,"nullable":false},"resource_id":{"type":"string","defaultValue":null,"maxLength":50,"nullable":false}};

    const resource = {
        init: function() {
            var promise = new Promise(function(resolve,reject){
                renobit.database.schema.hasTable(TB_ASSET_RESOURCE).then(function(exists) {
                    if(!exists) {
                        renobit.database.schema.createTable(TB_ASSET_RESOURCE, function(table){
                            table.string('asset_type', 50);
                            table.string('resource_id', 50);
                        }).then(function(res) {
                            resolve();
                        }).catch(function(err) {
                            reject(err);
                        })
                    } else {
                        resolve();
                    }
                })
            });
            return promise;
        },
        uninit: function() {
            return renobit.database.schema.dropTableIfExists(TB_ASSET_RESOURCE);
        },
        getTypeResource: function(asset_types) {
            return renobit.database(TB_ASSET_RESOURCE).innerJoin(TB_RESOURCE, 'TB_RESOURCE.RES_ID', TB_ASSET_RESOURCE + '.resource_id').whereIn('asset_type',asset_types);
        },
        delTypeResource: function(query, trx) {
            return renobit.database(TB_ASSET_RESOURCE).transacting(trx).where(query).del();
        },
        setTypeResource: function(rows, trx) {
            return renobit.database.batchInsert(TB_ASSET_RESOURCE, rows).transacting(trx);
        }
    }

    function ensureAuth(req, res, next) {
        if (req.isAuthenticated()) {
            next();
        } else {
            res.status(401).send({code:'AUTH_001'});
        }
    }

    return {
        init: function(param) {
            return resource.init();
        },
        uninit: function() {
            resource.uninit().then(() => {
                console.log("relation table drop success");
            });
        },
        regist: function() {
            renobit.app.get("/asset/resource", ensureAuth, function(req, res, next) {
                var asset_types = req.query.asset_type.split(",");
                resource.getTypeResource(asset_types).then((rows) => {
                    res.status(200).send(rows);
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });
            renobit.app.post("/asset/resource", ensureAuth, function(req, res, next) {
                renobit.database.transaction(function(trx) {
                    resource.delTypeResource({asset_type:req.body.asset_type}, trx).then(() => {
                        var rows = [];
                
                        _.each(req.body.resources, (v,k) => {
                            rows.push({
                                asset_type:req.body.asset_type,
                                resource_id:v
                            })
                        });
                        resource.setTypeResource(rows,trx).then(trx.commit).catch(trx.rollback)
                    }).catch(trx.rollback)
                }).then(() => {
                    res.status(200).send();
                }).catch((err) => {
                    res.status(500).send({code:'ETC_001'});
                })
            });
        },

        unregist: function() {
            try {
                var remove_route = [];
                var stack = renobit.app._router.stack;
                stack.forEach(removeMiddlewares);

                function removeMiddlewares(layer, i) {
                    if (layer.handle.name === "bound dispatch") {
                        if (layer.route.path.includes("/asset/resource")) {
                            remove_route.push(layer);
                        }
                    }
                }

                remove_route.forEach((v, k) => {
                    var idx = stack.indexOf(v);
                    stack.splice(idx, 1);
                })
            } catch (error) {
                RENOBIT.logger.error(error);
            }
        },
        backup : function() {
            var promise = new Promise((resolve, reject) => {
                var result = {};
                renobit.database(TB_ASSET_RESOURCE).select('*').then((rows) => {
                    result[TB_ASSET_RESOURCE] = {
                        schema:SCHEMA,
                        rows:rows
                    }
                    resolve(result);
                })
            })
            return promise;
        }
    }
}
