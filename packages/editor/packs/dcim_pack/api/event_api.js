module.exports = function (renobit) {
      'use strict';
      var _ = require('lodash');
      const moment = require('moment');
      const TB_ASSET_EVENT = 'TB_ASSET_EVENT';
      const TB_INSTANCE = 'TB_INSTANCE';
      const TB_ASSET_COMMON = 'TB_ASSET_COMMON';
      const TB_ASSET_RELATION = 'TB_ASSET_RELATION';
      const ASSET_PREFIX = 'A_';
      const SCHEMA = {
            "evt_id": {"type": "string", "defaultValue": null, "maxLength": 36, "nullable": true, "pk": true},
            "asset_type": {"type": "string", "defaultValue": null, "maxLength": 50, "nullable": true},
            "stime": {"type": "string", "defaultValue": null, "maxLength": 14, "nullable": true},
            "asset_id": {"type": "string", "defaultValue": null, "maxLength": 50, "nullable": true},
            "severity": {"type": "string", "defaultValue": null, "maxLength": 10, "nullable": true},
            "application": {"type": "string", "defaultValue": null, "maxLength": 255, "nullable": true},
            "object": {"type": "string", "defaultValue": null, "maxLength": 255, "nullable": true},
            "msg": {"type": "string", "defaultValue": null, "maxLength": 255, "nullable": true},
            "ack": {"type": "string", "defaultValue": null, "maxLength": 1, "nullable": true},
            "ack_user": {"type": "string", "defaultValue": null, "maxLength": 30, "nullable": true},
            "ack_info": {"type": "string", "defaultValue": null, "maxLength": 255, "nullable": true},
            "extra": {"type": "string", "defaultValue": null, "maxLength": 512, "nullable": true},
            "dupl_cnt": {"type": "integer", "defaultValue": null, "maxLength": null, "nullable": true},
            "ack_dt": {"type": "timestamp", "defaultValue": null, "maxLength": 50, "nullable": true},
            "confirm": {"type": "string", "defaultValue": "N", "maxLength": 1, "nullable": true}
      }

      const event = {
            uninit: function () {
                  return renobit.database.schema.dropTableIfExists(TB_ASSET_EVENT);
            },
            init: function () {
                  var promise = new Promise(function (resolve, reject) {
                        renobit.database.schema.hasTable(TB_ASSET_EVENT).then(function (exists) {
                              if (!exists) {
                                    renobit.database.schema.createTable(TB_ASSET_EVENT, function (table) {
                                          var pk_arr = [];
                                          _.each(SCHEMA, (detail, column_name) => {
                                                var column = detail.type === 'string' ? table[detail.type](column_name, detail.maxLength) : table[detail.type](column_name);
                                                if(detail.type === "timestamp") column.defaultTo(RENOBIT.database.fn.now())
                                                if(detail.defaultValue !== null) column.defaultTo(detail.defaultValue);
                                                else column.notNullable();
                                                if(detail.nullable) column.nullable();
                                                if(detail.pk) pk_arr.push(column_name);
                                          })
                                          if(pk_arr.length > 0) table.primary(pk_arr);
                                    }).then(function (res) {
                                          resolve();
                                    }).catch(function (err) {
                                          reject(err);
                                    })
                              } else {
                                    resolve();
                              }
                        })
                  });
                  return promise;
            },
            select: function (severity = null, count) {
                  var promise = new Promise((resolve, reject) => {
                        let queryBuilder = renobit.database(TB_ASSET_EVENT);
                        if (Object.keys(severity).length > 0) {
                              queryBuilder.whereIn('severity', severity);
                        }
                        if (count > 0) {
                              queryBuilder.limit(count);
                        }

                        let subQuery = renobit.database(TB_ASSET_RELATION).count("child_id").as('count').whereRaw('"parent_id" = "TB_ASSET_EVENT"."asset_id"');
                        queryBuilder.select(TB_ASSET_EVENT + '.*', TB_INSTANCE + ".PAGE_ID AS page_id", TB_ASSET_COMMON + ".name AS asset_name", subQuery)
                              .innerJoin(TB_ASSET_COMMON, TB_ASSET_EVENT + '.asset_id', TB_ASSET_COMMON + '.id')
                              .innerJoin(TB_INSTANCE, function(){
                                    this.on(TB_ASSET_COMMON + '.id', '=', TB_INSTANCE + '.ASSET_ID')
                                          .orOn(TB_ASSET_COMMON + '.parent_id', '=', TB_INSTANCE + '.ASSET_ID')
                              })
                              .whereBetween("stime", [moment().add(-1, 'days').format('YYYYMMDDHHmmss'), moment().format('YYYYMMDDHHmmss')])
                              .orderBy(TB_ASSET_EVENT + ".stime", 'desc').where('ack', 'N')
                              .map(row => {
                                    let event_time = moment(row.stime, "YYYYMMDDHHmmss");
                                    row.stime = event_time.format("YYYY-MM-DD HH:mm:ss");
                                    row.dt_stime = event_time.valueOf();
                                    row.extra = JSON.parse(row.extra);
                                    return row;
                              }).then(result => {
                              resolve(result);
                        }).catch(error => {
                              reject(error);
                        })
                  })

                  return promise;
            },
            update: function (query) {
                  var promise = new Promise(function (resolve, reject) {
                        let updateItem = JSON.parse(JSON.stringify(query));
                        delete updateItem.ackList;
                        delete updateItem.assetList;
                        if(updateItem.ack == 'Y'){
                              updateItem.ack_dt = RENOBIT.database.fn.now();
                        }
                        renobit.database.transaction(function (trx) {
                              const queries = [];
                              query.ackList.forEach(item => {
                                    const query = renobit.database(TB_ASSET_EVENT)
                                          .where('evt_id', item.evt_id)
                                          .update(updateItem)
                                          .transacting(trx);
                                    queries.push(query);
                              });
                              Promise.all(queries).then(() => {
                                    trx.commit();
                              }).catch((err) => {
                                    trx.rollback();
                              })
                        }).then(() => {
                              resolve();
                        }).catch((err) => {
                              reject(err);
                        })
                  })
                  return promise;

            },
            insert: function (query) {
                  var promise = new Promise(function (resolve, reject) {
                        renobit.database(TB_ASSET_EVENT).insert(query).then(() => {
                              resolve(result);
                        }).catch((err) => {
                              console.log(err);
                              reject();
                        })
                  })
                  return promise;
            },

            count: function () {
                  return new Promise(function (resolve, reject) {
                        var data = {};
                        renobit.database(TB_ASSET_EVENT).count("severity as count").select("severity")
                              .innerJoin(TB_ASSET_COMMON, TB_ASSET_EVENT + '.asset_id', TB_ASSET_COMMON + '.id')
                              .innerJoin(TB_INSTANCE, function(){
                                    this.on(TB_ASSET_COMMON + '.id', '=', TB_INSTANCE + '.ASSET_ID')
                                          .orOn(TB_ASSET_COMMON + '.parent_id', '=', TB_INSTANCE + '.ASSET_ID')
                              })
                              .where({'ack': 'N'})
                              .whereBetween("stime", [moment().add(-1, 'days').format('YYYYMMDDHHmmss'), moment().format('YYYYMMDDHHmmss')])
                              .groupBy("severity")
                              .map(row => {
                                    data[row.severity] = row.count;
                              })
                              .then(result => {
                                    resolve(data);
                              }).catch(err => {
                              reject(err);
                        })
                  })
            },

            selectList: function (severity, count) {
                  return new Promise(function (resolve, reject) {
                        let data = {list: null, count: null};
                        event.select(severity, count).then((result) => {
                              data.list = result;
                              event.count().then((result) => {
                                    data.count = result;
                                    event.select(["critical"], 0).then((result)=>{
                                          data.critical = result;
                                          resolve(data);
                                    })
                              }).catch(err => {
                                    reject(err);
                              })
                        }).catch(err => {
                              reject(err);
                        });
                  })
            },

            selectByType: function (query) {
                  return new Promise(function (resolve, reject) {
                        let queryBuilder = renobit.database(TB_ASSET_EVENT);
                        if (query.pageIds) {
                              queryBuilder.whereIn('PAGE_ID', query.pageIds);
                        }
                        if (query.typeIds) {
                              queryBuilder.whereIn(TB_ASSET_EVENT + '.asset_type', query.typeIds);
                        }

                        queryBuilder.select(TB_ASSET_EVENT + '.*', TB_INSTANCE + ".PAGE_ID AS page_id", TB_ASSET_COMMON + ".name AS asset_name")
                              .innerJoin(TB_ASSET_COMMON, TB_ASSET_EVENT + '.asset_id', TB_ASSET_COMMON + '.id')
                              .innerJoin(TB_INSTANCE, function(){
                                    this.on(TB_ASSET_COMMON + '.id', '=', TB_INSTANCE + '.ASSET_ID')
                                          .orOn(TB_ASSET_COMMON + '.parent_id', '=', TB_INSTANCE + '.ASSET_ID')
                              })
                              .whereBetween("stime", [moment().add(-1, 'days').format('YYYYMMDDHHmmss'), moment().format('YYYYMMDDHHmmss')])
                              .orderBy("stime", 'desc').where('ack', 'N')
                              .then(result => {
                                    resolve(result);
                              }).catch(error => {
                              reject(error);
                        })
                  })
            },

            selectSeverityCount: function (query) {
                  return new Promise(function (resolve, reject) {
                        let queryBuilder = renobit.database(TB_ASSET_EVENT).where({"ack": "N"});
                        if (query.pageIds) {
                              queryBuilder.whereIn('PAGE_ID', query.pageIds);
                        }
                        if (query.typeIds) {
                              queryBuilder.whereIn(TB_ASSET_EVENT + '.asset_type', query.typeIds);
                        }

                        queryBuilder.count("severity as count").select("severity", TB_ASSET_EVENT + ".asset_type", "PAGE_ID AS page_id")
                              .innerJoin(TB_ASSET_COMMON, TB_ASSET_EVENT + '.asset_id', TB_ASSET_COMMON + '.id')
                              .innerJoin(TB_INSTANCE, function(){
                                    this.on(TB_ASSET_COMMON + '.id', '=', TB_INSTANCE + '.ASSET_ID')
                                          .orOn(TB_ASSET_COMMON + '.parent_id', '=', TB_INSTANCE + '.ASSET_ID')
                              })
                              .whereBetween("stime", [moment().add(-1, 'days').format('YYYYMMDDHHmmss'), moment().format('YYYYMMDDHHmmss')])
                              .groupBy("severity", TB_ASSET_EVENT + ".asset_type", "PAGE_ID")
                              .then(result => {
                                    let obj = {};

                                    if (result) {
                                          result.forEach((data) => {
                                                let pageId = data.page_id;
                                                let assetType = data.asset_type;
                                                let item = obj[pageId];
                                                if (!item) {
                                                      item = obj[pageId] = {};
                                                }

                                                let typeData = item[assetType];
                                                if (!typeData) {
                                                      typeData = item[assetType] = {};
                                                }

                                                typeData[data.severity] = data.count;
                                          })
                                    }
                                    resolve(obj);
                              }).catch(err => {
                              reject(err);
                        })
                  })
            },

            selectHistory: function (query) {
                  return new Promise((resolve, reject) => {
                        let types = query.types;
                        let result = {};
                        let resolveList = [];
                        types.map(type=>{
                              let table_name = ASSET_PREFIX + type.toUpperCase() + "_H";
                              let queryBuilder = renobit.database(table_name);
                              result[type] = [];
                              if (query.assetIds) {
                                    queryBuilder.whereIn('id', query.assetIds);
                              }

                              // 최근 데이터 1건
                              if(query.current) {
                                    let subQuery = renobit.database(table_name).select("id").max("created_at").groupBy("id");
                                    queryBuilder.whereIn(["id", "created_at"], subQuery)
                                    resolveList.push(queryBuilder.then(res=> {
                                          result[type] = res
                                    }))
                              } else {
                                    if(query.s_time && query.e_time) {
                                          queryBuilder.whereBetween(table_name + '.created_at', [query.s_time, query.e_time]);
                                    }

                                    resolveList.push(queryBuilder.select('*').then(res=> {
                                          result[type] = res
                                    }));
                              }
                        })

                        Promise.all(resolveList).then(() => resolve(result)).catch((err) => reject(err));
                  })
            }
      }

      function ensureAuth(req, res, next) {
            if (req.isAuthenticated()) {
                  next();
            } else {
                  res.status(401).send({code:'AUTH_001'});
            }
      }

      return {
            init: function (param) {
                  return event.init();
            },
            uninit: function () {
                  event.uninit().then(() => {
                        console.log("event table drop success");
                  });
            },
            regist: function () {
                  renobit.app.post("/event/list", ensureAuth, function (req, res, next) {
                        event.selectList(req.body, 100).then((result) => {
                              res.status(200).send(result);
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });

                  renobit.app.get("/event/data", ensureAuth, function (req, res, next) {
                        event.select().then((result) => {
                              res.status(200).send(result);
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });

                  renobit.app.put("/event/data", ensureAuth, function (req, res, next) {
                        event.update(req.body).then((result) => {
                              res.status(200).send(result);
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });

                  renobit.app.post("/event/data", ensureAuth, function (req, res, next) {
                        event.insert(req.body).then(() => {
                              res.status(200).send();
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });

                  renobit.app.post("/event/data/type", ensureAuth, function (req, res, next) {
                        event.selectByType(req.body).then((result) => {
                              res.status(200).send(result);
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });

                  renobit.app.post("/event/data/count", ensureAuth, function (req, res, next) {
                        event.selectSeverityCount(req.body).then((result) => {
                              res.status(200).send(result);
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });

                  renobit.app.post("/event/history", ensureAuth, function (req, res, next) {
                        event.selectHistory(req.body).then((result) => {
                              res.status(200).send(result);
                        }).catch((err) => {
                              renobit.logger.error(err);
                              res.status(500).send({code:'ETC_001'});
                        })
                  });

                  renobit.app.post("/event/bypass", function(req,res,next) {
                        process.send({event: "/event/bypass", data: req.body});
                        res.status(200).send();
                  })
            },

            unregist: function () {
                  try {
                        var remove_route = [];
                        var stack = renobit.app._router.stack;
                        stack.forEach(removeMiddlewares);

                        function removeMiddlewares(layer, i) {
                              if (layer.handle.name === "bound dispatch") {
                                    if (layer.route.path.includes("/event/data")) {
                                          remove_route.push(layer);
                                    }
                              }
                        }

                        remove_route.forEach((v, k) => {
                              var idx = stack.indexOf(v);
                              stack.splice(idx, 1);
                        })
                  } catch (error) {
                        RENOBIT.logger.error(error);
                  }
            },
            backup : function() {
                  var promise = new Promise((resolve, reject) => {
                        var result = {};
                        renobit.database(TB_ASSET_EVENT).select('*').map((row) => {
                              if(row.ack_dt) {
                                    row.ack_dt = new Date(row.ack_dt).format("yyyy-MM-dd hh:mm:ss.fff");
                              }
                              return row;
                        }).then((rows) => {
                              // 일단 이벤트 테이블 복구 하지 않음.
                              result[TB_ASSET_EVENT] = {
                                    schema:SCHEMA,
                                    rows:[]
                              }
                              resolve(result);
                        })
                  })
                  return promise;
            }
      }
}
