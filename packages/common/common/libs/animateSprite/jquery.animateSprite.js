/*! jqueryanimatesprite - v1.3.5 - 2014-10-17
 * http://blaiprat.github.io/jquery.animateSprite/
 * Copyright (c) 2014 blai Pratdesaba; Licensed MIT */
(function($, window, undefined) {

    'use strict';
    var init = function(options) {

        return this.each(function() {
            var $this = $(this),
                data = $this.data('animateSprite');

            // ASYNC
            // If we don't specify the columns, we
            // can discover using the background size
            var discoverColumns = function(cb) {
                var imageSrc = $this.css('background-image').replace(/url\((['"])?(.*?)\1\)/gi, '$2');
                var image = new Image();

                image.onload = function() {
                    var width = image.width,
                        height = image.height;
                    cb(width, height);
                };
                image.src = imageSrc;
            };

            if (!data) {
                $this.data('animateSprite', {
                    settings: $.extend({
                        width: $this.width(),
                        height: $this.height(),
                        totalFrames: false,
                        columns: false,
                        fps: 12,
                        complete: function() {},
                        loop: false,
                        autoplay: true
                    }, options),
                    currentFrame: 0,
                    controlAnimation: function() {
                        if (!$this.data("animateSprite")) { return; }
                        var checkLoop = function(currentFrame, finalFrame) {
                            currentFrame++;
                            if (currentFrame >= finalFrame) {
                                if (this.settings.loop === true) {
                                    currentFrame = 0;
                                    data.controlTimer();
                                } else {
                                    this.settings.complete();
                                }
                            } else {
                                data.controlTimer();
                            }
                            return currentFrame;
                        };

                        if (this.settings.animations === undefined) {
                            $this.animateSprite('frame', this.currentFrame);
                            this.currentFrame = checkLoop.call(this, this.currentFrame, this.settings.totalFrames);

                        } else {
                            if (this.currentAnimation === undefined) {
                                for (var k in this.settings.animations) {
                                    this.currentAnimation = this.settings.animations[k];
                                    break;
                                }
                            }
                            var newFrame = this.currentAnimation[this.currentFrame];

                            $this.animateSprite('frame', newFrame);
                            this.currentFrame = checkLoop.call(this, this.currentFrame, this.currentAnimation.length);

                        }

                    },
                    controlTimer: function() {
                        // duration overrides fps
                        var speed = 1000 / data.settings.fps;

                        if (data.settings.duration !== undefined) {
                            speed = data.settings.duration / data.settings.totalFrames;
                        }

                        clearTimeout(data.interval);
                        data.interval = setTimeout(function() {
                            data.controlAnimation();
                        }, speed);

                    }
                });


                data = $this.data('animateSprite');

                // Setting up columns and total frames
                if (!data.settings.columns) {
                    // this is an async function
                    discoverColumns(function(width, height) {
                        // getting amount of columns
                        data.settings.columns = Math.round(width / data.settings.width);
                        // if totalframes are not specified
                        if (!data.settings.totalFrames) {
                            // total frames is columns times rows
                            var rows = Math.round(height / data.settings.height);
                            data.settings.totalFrames = data.settings.columns * rows;
                        }
                        if (data.settings.autoplay) {
                            data.controlTimer();
                        }
                    });
                } else {

                    // if everything is already set up
                    // we start the interval
                    if (data.settings.autoplay) {
                        data.controlTimer();
                    }
                }


            }

        });

    };

    var frame = function(frameNumber) {
        // frame: number of the frame to be displayed
        return this.each(function() {
            if ($(this).data('animateSprite') !== undefined) {
                var $this = $(this),
                    data = $this.data('animateSprite'),
                    row = Math.floor(frameNumber / data.settings.columns),
                    column = frameNumber % data.settings.columns;

                $this.css('background-position', (-data.settings.width * column) + 'px ' + (-data.settings.height * row) + 'px');
            }
        });
    };

    var stop = function() {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('animateSprite') || {};

            clearTimeout(data.interval);
        });
    };

    var destroy = function() {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('animateSprite') || {};
            clearTimeout(data.interval);
            $this.removeData('animateSprite');
        });
    }

    var resume = function() {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('animateSprite');

            // always st'op animation to prevent overlapping intervals
            $this.animateSprite('stopAnimation');
            data.controlTimer();
        });
    };

    var restart = function() {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('animateSprite');

            $this.animateSprite('stopAnimation');

            data.currentFrame = 0;
            data.controlTimer();
        });
    };

    var play = function(animationName) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('animateSprite');

            if (typeof animationName === 'string') {

                $this.animateSprite('stopAnimation');
                if (data.settings.animations[animationName] !== data.currentAnimation) {
                    data.currentFrame = 0;
                    data.currentAnimation = data.settings.animations[animationName];
                }
                data.controlTimer();
            } else {
                $this.animateSprite('stopAnimation');
                data.controlTimer();
            }

        });
    };

    var fps = function(val) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('animateSprite');
            // data.fps
            data.settings.fps = val;
        });
    };

    var methods = {
        init: init,
        frame: frame,
        stop: stop,
        resume: resume,
        restart: restart,
        play: play,
        stopAnimation: stop,
        resumeAnimation: resume,
        restartAnimation: restart,
        destroy: destroy,
        fps: fps
    };

    $.fn.animateSprite = function(method, flag) {

        if (flag == 'init') {
            return methods.init.apply(this, arguments);
        }

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.animateSprite');
        }

    };



})(jQuery, window);
/*
class AnimateSpriteClip {
    constructor(element, options) {
        this.$el = $(element);
        this.tween;
        this.frameObj = {
            frame: 1
        };

        this.options = $.extend(true, {}, AnimateSpriteClip.DefaultOptions);
        this.options = $.extend(true, this.options, options);

        this.options.totalFrames = parseInt(this.options.totalFrames);
        this.options.columns = parseInt(this.options.columns);
        this.options.width = parseInt(this.options.width);
        this.options.height = parseInt(this.options.height);
        this.options.fps = parseInt(this.options.fps);

        this.count = Math.random() + " " + Math.random();
        this.playDuration = this.options.totalFrames * (1 / this.options.fps);
        var width = this.options.width * this.options.columns;
        var height = this.options.height * Math.ceil(this.options.totalFrames / this.options.columns)
        this.$el.css({
            width: width,
            height: height
        })
    }

    frame() {
        var frameNumber = Math.ceil(this.frameObj.frame) - 1;
        var row = Math.floor(frameNumber / this.options.columns),
            column = frameNumber % this.options.columns;
        this.$el.css("transform", "translate3d(" + (-this.options.width * column) + "px, " + (-this.options.height * row) + "px, 0)");
        //this.$el.css('background-position', (-this.options.width * column) + 'px ' + (-this.options.height * row) + 'px');
    }

    stop() {
        if (this.tween) {
            this.tween.kill();
        }
    }

    play() {
        var self = this;
        this.frameObj.frame = 1;
        this.tween = TweenLite.to(this.frameObj, this.playDuration, {
            ease: SteppedEase.config(this.options.totalFrames),
            frame: this.options.totalFrames,
            onComplete: function() {
                if (self.options && self.options.loop) {
                    self.play();
                }
            },
            repeat: -1,
            onUpdate: this.frame.bind(self)
        });
    }

    destroy() {
        this.stop();
        this.$el = null;
        this.tween = null;
        this.frameObj = null;
        this.options = null;
    }
}

AnimateSpriteClip.DefaultOptions = {
    width: 0,
    height: 0,
    totalFrames: 0,
    columns: 0,
    fps: 60,
    complete: function() {},
    loop: false,
    autoplay: true
}*/


var _createClass = function() {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function(Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; };
}();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }


/**
 * 초기 jquery.animateSprite를 사용하였으나, 속도 성능이슈로 아래 클래스를 작성하여 대체함.
 * TODO : Frame컨트롤하는 메소드 추가 - gotoAndPlay() gotoAndStop(), playOnce() 등.. 
 *        라이브러리파일정리..
 */
var AnimateSpriteClip = function() {
    function AnimateSpriteClip(element, options) {
        _classCallCheck(this, AnimateSpriteClip);

        this.$el = $(element);
        this.tween;
        this.frameObj = {
            frame: 1
        };

        this.options = $.extend(true, {}, AnimateSpriteClip.DefaultOptions);
        this.options = $.extend(true, this.options, options);

        this.options.totalFrames = parseInt(this.options.totalFrames);
        this.options.columns = parseInt(this.options.columns);
        this.options.width = parseInt(this.options.width);
        this.options.height = parseInt(this.options.height);
        this.options.fps = parseInt(this.options.fps);

        this.count = Math.random() + " " + Math.random();
        this.playDuration = this.options.totalFrames * (1 / this.options.fps);
        var width = this.options.width * this.options.columns;
        var height = this.options.height * Math.ceil(this.options.totalFrames / this.options.columns)
        this.$el.css({
            width: width,
            height: height
        })
    }

    _createClass(AnimateSpriteClip, [{
        key: "frame",
        value: function frame() {
            var frameNumber = Math.ceil(this.frameObj.frame) - 1;
            var row = Math.floor(frameNumber / this.options.columns),
                column = frameNumber % this.options.columns;
            this.$el.css("transform", "translate3d(" + -this.options.width * column + "px, " + -this.options.height * row + "px, 0)");
            //this.$el.css('background-position', (-this.options.width * column) + 'px ' + (-this.options.height * row) + 'px');
        }
    }, {
        key: "stop",
        value: function stop() {
            if (this.tween) {
                this.tween.kill();
            }
        }
    }, {
        key: "play",
        value: function play() {
            var self = this;
            this.frameObj.frame = 1;
            this.tween = TweenLite.to(this.frameObj, this.playDuration, {
                ease: SteppedEase.config(this.options.totalFrames),
                frame: this.options.totalFrames,
                onComplete: function onComplete() {
                    if (self.options && self.options.loop) {
                        self.play();
                    }
                },
                repeat: -1,
                onUpdate: this.frame.bind(self)
            });
        }
    }, {
        key: "destroy",
        value: function destroy() {
            this.stop();
            this.$el = null;
            this.tween = null;
            this.frameObj = null;
            this.options = null;
        }
    }]);

    return AnimateSpriteClip;
}();

AnimateSpriteClip.DefaultOptions = {
    width: 0,
    height: 0,
    totalFrames: 0,
    columns: 0,
    fps: 60,
    complete: function complete() {},
    loop: false,
    autoplay: true
};