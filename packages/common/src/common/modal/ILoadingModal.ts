import {IProgressbar} from "../../wemb/wv/components/LoadingProgressbar";

interface ILoadingModal extends IProgressbar{
	reset():void;
	setTitle(string):void;
}

export default ILoadingModal;

