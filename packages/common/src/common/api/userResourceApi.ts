
import {http} from "../../wemb/http/Http";
import api = http.api;
import sendErrorMessage from "./sendErrorMessage";

export default {
    loadUserResourceList(type):Promise<any>{
        let url = window.wemb.configManager.serverUrl + "/editor.do";
        type = type || "";

        let request:any = {
            "id": "editorService.selectResourceInfo",
            "params": {"type": type}
        };

        return api.post(url, request).then((result) => {
            if (result.data.data && result.data.data.length > 0) {
                return result.data.data;
            } else {
                return [];
            }
        }, (error) => {
              sendErrorMessage.sendCriticalErrorCommand({
				message: error
			})
        });
    },

    exportResourceList(data): Promise<any> {
          let url = window.wemb.configManager.serverUrl + "/fileDownload";

          return api.post(url, data).then((result) => {
                if (result.data)  {
                      return result.data;
                } else {
                      return [];
                }
          }, (error) => {
                sendErrorMessage.sendCriticalErrorCommand({
                      message: error
                })
          });

    }

}


