import sendErrorMessage from "./sendErrorMessage";
import {http} from "../../wemb/http/Http";
import Vue from "vue";
import api = http.api;

export default {
      getApiUrl(){
            return window.wemb.configManager.workUrl;
      },

      updatePageInfo(pageInfoList, isDeleteCurrentPageAct:boolean=false){
            if(pageInfoList){
                  window.wemb.pageManager.attachJsonPageInfoListToPageInfoList(pageInfoList, isDeleteCurrentPageAct);
            }
      },

      loadPageTreeData() {
            if ( window.wemb.configManager.test) {
                  return this._test_loadPageTreeData();

            } else {
                  return this._loadPageTreeData();
            }
      },

      _test_loadPageTreeData(): Promise<any> {
            let fileName =  window.wemb.configManager.serverUrl + '/custom/data/treeData.json';

            return new Promise((resolve, reject) => {
                  api.get(fileName).then((result) => {
                        resolve(result.data);
                  }).catch((error) => {
                        sendErrorMessage.sendCriticalErrorCommand({
                              message: error
                        })
                  })
            })
      },


      _loadPageTreeData(): Promise<any> {
            let fileName = this.getApiUrl();
            let data = {
                  "id": "editorService.getPageTreeList",
                  "params": {}
            };

            return new Promise((resolve, reject) => {
                  api.post(fileName, data).then((result) => {
                        let response = result.data;
                        if (response.result == "ok") {
                              resolve(response.data);
                        }else {
                              let msg = Vue.$i18n.messages.wv.pageTree.failedLoadTreeInfo;
                              sendErrorMessage.sendCriticalErrorCommand({
                                    message: msg
                              })

                              reject();
                        }

                  }).catch((error) => {
                        sendErrorMessage.sendCriticalErrorCommand({
                              message: error
                        })

                        reject(error);
                  })
            });
      },

      createTreeItem(item:any){
            if (window.wemb.configManager.test) {
                  return new Promise((resolve, reject) => {
                        resolve({
                              data: {result: "ok"}
                        });
                  })
            } else {
                  return new Promise((resolve, reject) => {
                        let fileName = this.getApiUrl();
                        // 서버에 JSON 파일로 저장
                        let data = {
                              "id": "editorService.createTreeItem",
                              "params": {
                                    "name": item.name,
                                    "type": item.type,
                                    "parent": item.parent,
                                    "prev_id": item.prev_id
                              }
                        };

                        if(item.type == "page"){
                              data.params["pageInfo"] = item.pageInfo;
                        }
                        let errorMsg =  Vue.$i18n.messages.wv.common.errorSave;
                        //"페이지를 정상적으로 생성되지 않았습니다. 다시 시도해 주세요.";
                        api.post(fileName, data).then((result:any) => {
                              let response = result.data;
                              if( response.result == "ok" && response.data.pageTreeList ){
                                    resolve(response.data.pageTreeList);
                                    this.updatePageInfo(response.data.pageList);
                              }else {
                                    // this.errorMessage( response, errorMsg );
                                    reject(errorMsg);
                              }

                        }, (error) => {
                              console.error("RENOBIT pageTreeApi.createTreeItem() ", error);
                              // this.errorMessage( "", errorMsg );
                              reject(error);
                        });
                  })
            }
      },

      errorMessage(response, defaultMessage ){
            if( response && response.result == "error" ){
                  try{
                        let key = ERROR_CODES[ response.errorCode ].MSG;
                        let errorData = Vue.$i18n.messages.wv.pageTree[key];
                        alert( "[ "+response.errorCode+" ] "+errorData.MSG );
                  }catch(error){
                        alert( defaultMessage );
                  }
            }else{
                  alert( defaultMessage );
            }
      },

      doubleCheck(name){
            if (window.wemb.configManager.test) {
                  return new Promise((resolve, reject) => {
                        resolve(true);
                  })
            } else {
                  let fileName = this.getApiUrl();
                  let data = {
                        "id": "editorService.doubleCheckTreeName",
                        "params": {
                              "name": name
                        }
                  };

                  return new Promise((resolve, reject) => {
                        let errorMsg = Vue.$i18n.messages.wv.pageTree.failedTryNormally;
                        //"정상적으로 시도하지 못했습니다. 다시 시도해 주세요.";
                        api.post(fileName, data).then((result) => {
                              let response = result.data;
                              if (response.result == "ok") {
                                    resolve(response.data);
                              } else {
                                    this.errorMessage( '', errorMsg );
                                    reject('');
                                    //throw new Error("페이지가 정상적으로  삭제되지 않았습니다.");
                              }
                        }, (error) => {
                              this.errorMessage( '', errorMsg );
                              reject(error);
                        })
                  })
            }
      },

      /* 2018.12.05(ckkim) 추가*/
      doubleCheckId(id){
            if (window.wemb.configManager.test) {
                  return new Promise((resolve, reject) => {
                        resolve(true);
                  })
            } else {
                  let fileName = this.getApiUrl();
                  let data = {
                        "id": "editorService.doubleCheckTreeId",
                        "params": {
                              "id": id
                        }
                  };

                  return new Promise((resolve, reject) => {
                        let errorMsg = Vue.$i18n.messages.wv.pageTree.failedTryNormally;
                        //"정상적으로 시도하지 못했습니다. 다시 시도해 주세요.";
                        api.post(fileName, data).then((result) => {
                              let response = result.data;
                              if (response.result == "ok") {
                                    resolve(false);
                              } else {
                                    this.errorMessage( '', errorMsg );
                                    reject(true);
                                    //throw new Error("페이지가 정상적으로  삭제되지 않았습니다.");
                              }
                        }, (error) => {
                              this.errorMessage( '', errorMsg );
                              reject(false);
                        })
                  })
            }
      },

      removeTreeItem(items:Array<any>) {
            //[ { type: "", id: "" } ]
            if (window.wemb.configManager.test) {
                  return new Promise((resolve, reject) => {
                        resolve(true);
                  })
            } else {

                  return new Promise((resolve, reject) => {
                        let fileName = this.getApiUrl();
                        let data = {
                              "id": "editorService.removeTreeItem",
                              "params": {
                                    "items": items
                              }
                        };

                        let errorMsg = Vue.$i18n.messages.wv.pageTree.notDeleted;//"페이지가 삭제되지 않았습니다. 다시 시도해 주세요.";

                        api.post(fileName, data).then((result:any) => {
                              let response = result.data;
                              if( response.result == "ok" && response.data.pageTreeList ){
                                    resolve(response.data.pageTreeList);
                                    let currentId = window.wemb.pageManager.currentPageInfo.id;
                                    let findItem = items.find(x=>x.id==currentId);
                                    this.updatePageInfo(response.data.pageList, true);
                              }else{
                                    this.errorMessage( response, errorMsg );
                                    reject(response.errorCode);
                              }

                        }, (error) => {
                              console.error("RENOBIT pageTreeApi.removeTreeItem() ", error);
                              this.errorMessage( '', errorMsg );
                              reject(error);
                        });
                  });
            }
      },

      renameTreeItem(item: any) {
            //{ type: "", id: "", name: "" }
            console.log('renameTreeItem', item);
            if (window.wemb.configManager.test) {
                  console.log("RENOBIT, 테스트 모드에서는 페이지를 수정할 수 없습니다.");
            }else {


                  return new Promise((resolve, reject) => {
                        let fileName = this.getApiUrl();
                        let data = {
                              "id": "editorService.renameTreeItem",
                              "params": {
                                    "id": item.id,
                                    "name": item.name,
                                    "oldName": item.oldName,
                                    "type": item.type
                              }
                        };

                        let errorMsg = "페이지명이 수정되지 않았습니다. 다시 시도해 주세요.";
                        api.post(fileName, data).then((result) => {
                              let response = result.data;
                              if( response.result == "ok" && response.data.pageTreeList ){
                                    resolve(response.data.pageTreeList);
                                    this.updatePageInfo(response.data.pageList);
                              }else{
                                    this.errorMessage( response, errorMsg );
                                    reject(response.errorCode);
                              }

                        }, (error) => {
                              this.errorMessage( '', errorMsg );
                              reject(error);
                        })
                  });
            }
      },

      moveTreeItem(items: Array<any>) {
            //{ type: "", id: "", name: "" }
            if (window.wemb.configManager.test) {
                  console.log("RENOBIT, 테스트 모드에서는 페이지를 수정할 수 없습니다.");
            }else {
                  return new Promise((resolve, reject) => {
                        let fileName = this.getApiUrl();
                        let data = {
                              "id": "editorService.moveTreeItem",
                              "params": {
                                    "items": items
                              }
                        };

                        let errorMsg = Vue.$i18n.messages.wv.pageTree.notSavedTree;//"트리 항목이 저장되지 않았습니다. 다시 시도해 주세요.";
                        api.post(fileName, data).then((result) => {
                              let response = result.data;
                              if( response.result == "ok" && response.data.pageTreeList ){
                                    resolve(response.data.pageTreeList);
                                    this.updatePageInfo(response.data.pageList);
                              }else{
                                    this.errorMessage( response, errorMsg );
                                    reject(errorMsg);
                              }

                        }, (error) => {
                              console.error("RENOBIT pageTreeApi.moveTreeItem()", error);

                              this.errorMessage( '', errorMsg );
                              reject(error);
                        })
                  });

            }
      }
}


export namespace ERROR_CODES {
      //페이지 생성
      export enum TR001 {//페이지 생성이 정상적으로 되지 않았을 경우
            MSG = 'notCreated'//"페이지를 정상적으로 생성되지 않았습니다. 다시 시도해 주세요."
      }

      export enum TR002 {//페이지명이 중복일 경우
            MSG = 'existsName'//"중복된 이름이 있습니다. 다른 이름으로 저장해 주세요."
      }

      export enum TR009{//생성할 페이지가 DB에 추가 되지 않았을 경우
            MSG = 'notCreated'//"페이지를 정상적으로 생성되지 않았습니다. 다시 시도해 주세요."
      }

      //페이지 명 수정
      export enum TR003 {//페이지 명 수정이 정상적으로 되지 않았을 경우
            MSG = 'notModified'//"페이지명이 수정되지 않았습니다. 다시 시도해 주세요."
      }

      export enum TR008 {// 페이지의 이름이 DB에 수정 되지 않았을 경우
            MSG = 'notModified'//"페이지명이 수정되지 않았습니다. 다시 시도해 주세요."
      }

      export enum TR010{// 페이지명이 중복일 경우
            MSG = 'existsName'//"중복된 이름이 있습니다. 다른 이름으로 저장해 주세요."
      }

      //페이지 삭제
      export enum TR004 {//페이지 삭제가 정상적으로 되지 않았을 경우
            MSG = 'notDeleted'//"페이지가 삭제되지 않았습니다. 다시 시도해 주세요."
      }

      export enum TR005{//DB에 없는 페이지를 삭제 시도할 경우 (다중 페이지 삭제시 한 페이지라도 해당되면 에러 발생)
            MSG = 'notExist'//"조회되지 않는 항목이 있습니다. 새로고침 후 다시 시도해 주세요."
      }

      //페이지 이동
      export enum TR006 {//페이지 이동이 정상적으로 되지 않았을 경우
            MSG = 'notSavedTree'//"트리 항목이 저장되지 않았습니다. 다시 시도해 주세요."
      }

      export enum TR007{//DB에 없는 페이지를 이동 시도할 경우(다중 페이지 이동시 한 페이지라도 해당되면 에러 발생)
            MSG = 'notExist'//"조회되지 않는 항목이 있습니다. 새고로침 후 다시 시도해 주세요."
      }

}
