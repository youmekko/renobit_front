import sendErrorMessage from "./sendErrorMessage";
import {http} from "../../wemb/http/Http";
import api = http.api;
/*
서버 환경설정 정보 읽기

초기 시작시 사용됨.2

 */
export default {

    /*
    환경설정 정보 읽기
    call : WVConfigManager.load()
     */
    loadData(): Promise<any> {
        console.log("\n\n\n### static/mockup_data/config.json 정보 읽기 시작");
        let fileName = 'client/custom/lang/lang_list.json';
          return api.get(fileName).then((data) => {

                if (data.hasOwnProperty(("languages"))) {
                      return data.languages;
                } else {
                      sendErrorMessage.sendCriticalErrorCommand({
                            message:"읽어들인 정보에 languages 속성이 존재하지 않습니다."
                      })
                }
          }).catch((error)=>{
                sendErrorMessage.sendCriticalErrorCommand({
                      message:error
                })
          })
    }
}
