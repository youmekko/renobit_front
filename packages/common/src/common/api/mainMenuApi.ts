import sendErrorMessage from "./sendErrorMessage";
import {http} from "../../wemb/http/Http";
import api = http.api;
import ConfigManager from "../../wemb/wv/managers/ConfigManager";

export default {

      getServerPath(): string {
            return ConfigManager.getInstance().serverUrl;
      },

      loadFile(): Promise<object> {
            let fileName = 'custom/menu/menu.json';

            return api.get(fileName).then((data: any) => {
                  if (data.hasOwnProperty(("menuItems"))) {
                        return this.getPluginInfo().then((pluginData: any) => {
                              let ArrInPluginObj = data.menuItems.find(x=>x.key === "plugin").children;
                              for(var key in pluginData) {
                                    if(pluginData[key]) {
                                          ArrInPluginObj.push({
                                                key:key,
                                                label:key,
                                                disabled:false,
                                                show:true,
                                                children:pluginData[key]
                                          })
                                    }
                              }
                              return data.menuItems;
                        });
                  } else {

                        sendErrorMessage.sendCriticalErrorCommand({
                              message: "읽어들인 메뉴 파일(menu.json) 정보에 menuItem 속성이 존재하지 않습니다. 관리자에 문의해주세요."
                        })
                  }
            }).catch((error) => {
                  sendErrorMessage.sendCriticalErrorCommand({
                        message: error
                  })
            })
      },

      getPluginInfo(): Promise<any> {
            let SERVERPATH = this.getServerPath() + '/editor.do';
            let data = {
                  "id": "editorService.getPluginInfo",
                  "params": {}
            };

            return api.post(SERVERPATH, data).then((result) => {
                  let data = result.data || {};
                  data = data.data || {};
                  
                  return data;
            }, (error) => {
                  console.error("RENOBIT mainMenuApi.getPluginInfo() ", error);
                  return null;
            });
      }
}
