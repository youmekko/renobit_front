import sendErrorMessage from "./sendErrorMessage";
import Vue from "vue";
import {http} from "../../wemb/http/Http";
import {SavePageData} from "../../wemb/wv/page/Page";
import {CONFIG_MODE, ERROR_TYPE, EXE_MODE} from "../../wemb/wv/data/Data";
import ViewerPageComponent from "../../wemb/wv/components/ViewerPageComponent";
import api = http.api;


/*
서버 환경설정 정보 읽기

초기 시작시 사용됨.

 */
export default {

	/*
	환경설정 정보 읽기
	call : WVConfigManager.load()
	 */
	loadMasterPageInfo(): Promise<any> {

		console.log("\n\n\n### 마스터 페이지 정보 읽기 시작, 실행모드=", window.wemb.configManager.test);

		if (window.wemb.configManager.test) {
			return this._test_loadMasterPageInfoList()
		} else {
			return this._server_loadMasterPageInfoList();
		}
	},

	_test_loadMasterPageInfoList(): Promise<any> {
		let fileName = window.wemb.configManager.serverUrl + '/master_page_info.json';
		return new Promise((resolve, reject) => {
			api.get(fileName).then((data) => {
				console.log("RENOBIT 읽어들일 마스터 fileName = ", fileName);
				if (this._check_masterDataValidation(data.data) == true) {
					resolve(data.data);
				}

			}).catch((error) => {
				sendErrorMessage.sendCriticalErrorCommand({
					message: error
				})
			})
		});
	},


	_server_loadMasterPageInfoList(): Promise<any> {
		let fileName = window.wemb.configManager.workUrl;
		let dataID = "editorService.getEditorInitInfo";
		if (window.wemb.configManager.exeMode == EXE_MODE.VIEWER)
			dataID = "editorService.getViewerInitInfo";

		let data = {
			"id": dataID,
			"params": {}
		};
		return new Promise((resolve, reject) => {

			api.post(fileName, data).then((result) => {
				if (this._check_masterDataValidation(result.data.data) == true) {
					resolve(result.data.data);
				}

			}).catch((error) => {
				sendErrorMessage.sendCriticalErrorCommand({
					message: error
				})
			})
		});
	},

	_check_masterDataValidation(masterPageFormat) {

		if (masterPageFormat.hasOwnProperty("page_list") == false) {
			sendErrorMessage.sendCriticalErrorCommand({
				type: ERROR_TYPE.PARSING_ERROR,
				message: "읽어들인 page정보에 pageInfoList 속성이 존재하지 않습니다."
			})

			return false;
		}


		if (masterPageFormat.hasOwnProperty("master_info") == false) {
			sendErrorMessage.sendCriticalErrorCommand({
				type: ERROR_TYPE.PARSING_ERROR,
				message: "읽어들인 page정보에 masterInfo 속성이 존재하지 않습니다."
			})

			return false;
		}



		return true;
	},
	///////////////////////////////////////////////////////////////////////////////////////////////////////

      async checkPageGrade(pageID:string){
            let fileName = window.wemb.configManager.workUrl;
            let data = {
                  "id": "editorService.getPageInfo",
                  "params": {
                        "page_id": pageID,
                        "user_id" : window.wemb.userInfo.userId,
                        "isViewer" : !window.wemb.configManager.isEditorMode
                  }
            };

            let result:any = await api.post(fileName, data);

            return result.data.errorCode == "NO_GRADE" && window.wemb.configManager.configMode == CONFIG_MODE.LOCAL;
      },

      async openPageById(pageID:string){
            if (window.wemb.configManager.test) {

                  let fileName = `static/mockup_data/${pageID}.json`;
                  let result =  await api.get(fileName);

                  if (this.validatePageFormat(result.data) == false) {
                        alert(Vue.$i18n.messages.wv.common.errorFormat);
                        throw new Error("페이지 포맷이 정확하지 않습니다. 확인 해주세요.");
                  }

                  return result.data;

            } else {
                  /*
                        2018.10.19(ckkim), localstorage 에 페이지 내용을 저장한 부분 삭제
                  */

                  let fileName = window.wemb.configManager.workUrl;
                  let data = {
                        "id": "editorService.getPageInfo",
                        "params": {
                              "page_id": pageID,
                              "user_id" : window.wemb.userInfo.userId,
                              "isViewer" : !window.wemb.configManager.isEditorMode
                        }
                  };

                  let result:any = await api.post(fileName, data);

                  if (this.validatePageFormat(result.data.data) == false) {
                        alert(Vue.$i18n.messages.wv.common.errorFormat);
                        throw new Error("페이지 포맷이 정확하지 않습니다. 확인 해주세요.");
                  }



                  // 페이지 권한이 없는 경우
                  if(result.data.errorCode == "NO_GRADE" && window.wemb.configManager.configMode == CONFIG_MODE.LOCAL) {
                        window.wemb.$defaultLoadingModal.hide();
                        throw new Error("할당된 권한이 없습니다.\n관리자에게 문의하세요");
                  }

                  let pageData = result.data.data;

                  return this.defensiveCode(pageData);
            }
      },

      //Youme - 2.1.0 버전 페이지 프로퍼티 구조 변경으로 인한 방어 코드
      defensiveCode(pageData){
	      const { props } = pageData.page_info;

            if(props.background){
                  props.setter.background = props.background;
                  delete props.background;
            }

            if(props.settings_3d){
                  props.setter.threeLayer = props.settings_3d;
                  delete props.settings_3d;
                  props.setter.threeLayer.visible = false;
            }

            if(props.setter.cameraView){
                  delete props.setter.cameraView;
            }

            if(props.page_info){
                  props.setter.description = props.page_info.description;
                  delete props.page_info;
            }

            if(!props.script){
                  props.script = {fileName: ""};
            }

            if(!props.setter.visible){
                  props.setter.visible = true;
            }

            if(!props.setter.event){
                  props.setter.event = false;
            }

            if(!props.setter.popup){
                  props.setter.popup = {
                        customStyle: false,
                        instance: {
                              header: '',
                              close: ''
                        }
                  }
            }

            if(!props.setter.twoLayer){
                  props.setter.twoLayer = {visible: true};
            }

            if(!props.setter.mapLayer){
                  props.setter.mapLayer = {visible: false};
            }

            if(!props.setter.borderRadius){
                  props.setter.borderRadius = {
                        topLeft: 0,
                        topRight: 0,
                        bottomLeft: 0,
                        bottomRight: 0
                  }
            }

            if(!props.setter.masterLayer){
                  props.setter.masterLayer = {visible: false};
            }

            return pageData;
      },

	validatePageFormat(pageFormat) {
            if (pageFormat.hasOwnProperty("page_info") == false) {
                  console.log("RENOBIT, validatePageFormat error page_info");
                  return false;
            }

            if (pageFormat.content_info.hasOwnProperty("two_layer") == false) {
                  console.log("RENOBIT, validatePageFormat error edit.two_layer");
                  return false;
            }


            if (pageFormat.content_info.hasOwnProperty("three_layer") == false) {
                  console.log("RENOBIT, validatePageFormat error edit.three_layer");
                  return false;
            }


		return true;
	},


      // 신규 페이지 저장(빈페이지)
      // 참고: savepage로 변경해야함.
      saveNewPage(pageVO:SavePageData) {
            console.info("RENOBIT, 새페이지 저장 전, pageVO ", pageVO);
            let fileName = window.wemb.configManager.workUrl;
            let data = {
                  "id": "editorService.saveNewPageInfo",
                  "params": pageVO.data
            };

            return api.post(fileName, data).then((result) => {
                  return result;
            });
      },


      savePage(pageVO:SavePageData) {
            if (window.wemb.configManager.test) {
                  // 디버깅용
                  //console.log("마스터용 ", JSON.stringify({ master_info: pageVO.data.master_info }));
                  let temp = {
                        "data": {
                              "page_info": pageVO.data.page_info,
                              "content_info": pageVO.data.content_info
                        }
                  };

                  // 디버깅용
                  //console.log("savePage 저장할 페이지 내용", JSON.stringify(temp));

                  return new Promise((resolve, reject) => {
                        resolve({
                              data:{
                                    result:"ok"
                              }
                        })
                  })
            } else {
                  // 디버깅용
                  //console.log("savePage 저장할 데이터 = ", JSON.stringify(pageVO.data));
                  let fileName = window.wemb.configManager.workUrl;
                  let data = {
                        "id": "editorService.savePageInfo",
                        "params": pageVO.data
                  };

                  return api.post(fileName, data).then((result) => {
                        return result;
                  });
            }
      },


      exportPages(sendData){
            if (window.wemb.configManager.test) {
                  return new Promise((resolve, reject) => {
                        reject('');
                  })
            } else {
                  return new Promise((resolve, reject) => {
                        let fileName = window.wemb.configManager.workUrl;
                        let data = {
                                    "id": "editorService.exportPageList",
                                    "params": {
                                          "master" : sendData.master,
                                          "ids" : sendData.ids
                                    }
                        };

                        api.post(fileName, data).then((result:any) => {
                              let response = result.data;
                              if( response.result === "ok" && response.data){
                                    resolve(response.data);
                              }else{
                                    reject('');
                              }
                        }, (error) => {
                              reject(error);
                        });
                  });
            }
      },

      importPages(sendData){
            if (window.wemb.configManager.test) {
                  return new Promise((resolve, reject) => {
                        reject('');
                  })
            } else {
                  return new Promise((resolve, reject) => {
                        let fileName = window.wemb.configManager.workUrl;
                        let data = {
                              "id": "editorService.importPageList",
                              "params": sendData
                        };

                        api.post(fileName, data).then((result:any) => {
                              let response = result.data;
                              if( response.result === "ok" ){
                                    resolve();
                              }else{
                                    reject('');
                              }
                        }, (error) => {
                              reject(error);
                        });
                  });
            }

      },

      checkExistPage(id){
            if (window.wemb.configManager.test) {
                  return new Promise((resolve, reject) => {
                        resolve(true);
                  })
            } else {
                  return new Promise((resolve, reject) => {
                        let fileName = window.wemb.configManager.workUrl;
                        let data = {
                                    "id": "editorService.checkExistPage",
                                    "params": {
                                    "id": id
                                    }
                        };

                        api.post(fileName, data).then((result:any) => {
                              let response = result.data;
                              if( response.data === true ){
                                    resolve(true);
                              }else{
                                    reject(false);
                              }
                        }, (error) => {
                              reject(error);
                        });
                  });
            }
      },

      // [REN-319] v2.1.0 사용자 초기 페이지 설정
      // PageTree 에서 접속 User의 초기 페이지 설정
      updateUserInitPage(page_id){
            let fileName = window.wemb.configManager.workUrl;
            let data = {
                  "id": "editorService.updateUserInitPage",
                  "params": {
                        "init_page": page_id,
                        "user_id": window.wemb.userInfo.userId
                  }
            };

            return api.post(fileName, data);
      },

      // [REN-312] v2.1.0 참조 페이지/마스터 설정
      // 참조페이지 update api 호출
      updateReferPage(referPage){
            let fileName = window.wemb.configManager.workUrl;
            let data = {
                  "id": "editorService.updateReferPage",
                  "params": {
                        "type": referPage.type,
                        "master_id": referPage.master_id,
                        "page_id": referPage.page_id
                  }
            };

            return api.post(fileName, data);
      },

      async attachPageScriptFile(fileName:string, pageComponent:ViewerPageComponent){
	      let script =  await api.get(fileName);
            let func = new Function("",script);
            func.call(pageComponent);
            return func;
      },

      restoreSamplePage() {
            let fileName = window.wemb.configManager.workUrl;
            let data =  {
                  id: "editorService.insertSampleContent",
                  params: {}
            };

            return api.post(fileName, data);
      }
}
