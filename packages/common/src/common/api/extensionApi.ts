import sendErrorMessage from "./sendErrorMessage";
import {http} from "../../wemb/http/Http";
import api = http.api;
/*
서버 환경설정 정보 읽기

초기 시작시 사용됨.2

 */
export default {

    /*
    환경설정 정보 읽기
    call : WVConfigManager.load()
     */
    loadData(): Promise<any> {

        let fileName = 'custom/extension/extension_instance_list.json';
          return api.get(fileName).then((data) => {
                if (data.hasOwnProperty(("extension_instance_list"))) {
                      return data.extension_instance_list;
                } else {
                      sendErrorMessage.sendCriticalErrorCommand({
                            message:"읽어들인 정보에 extension_instance_list 속성이 존재하지 않습니다."
                      })
                }
          }).catch((error)=>{
                sendErrorMessage.sendCriticalErrorCommand({
                      message:error
                })
          })
    }
}
