import sendErrorMessage from "./sendErrorMessage";
import {http} from "../../wemb/http/Http";
import api = http.api;
import {dto} from "../../editor/windows/datasetManager/model/dto/DatasetDTO";
import DatasetItemDTO = dto.DatasetItemDTO;
import Vue from "vue";


/*
서버 환경설정 정보 읽기
초기 시작시 사용됨.
 */
export default {
      getServerPath() {
            let config: any = window.wemb.configManager;
            let path: string = "";

            if (window.__serverUrl__) {
                  path = window.__serverUrl__;
            } else if (config && config.serverUrl) {
                  path = config.serverUrl;
            }

            return path;
      },

      getPreviewData(param_data: any): Promise<any> {
            let locale_msg = Vue.$i18n.messages.wv;
            let fileName: string = this.getServerPath() + "/dataset.do"; //"http://192.168.0.8:8080/WebVisualizer/dataset.do"; //this.getServerPath() + "/dataset.do";
            let sendData: any = {
                  id: "dataSetService.getTestData",
                  params: param_data
            }

            sendData.params.dbType = param_data.dbType || param_data.db_type;

            return new Promise((resolve, reject) => {
                  api.post(fileName, sendData).then((result) => {
                        let data: any = result.data;

                        if (data.data.includes("error")) {
                              throw Error();
                        }

                        if(data.query == "not ok"){
                              Vue.$message({
                                    message : locale_msg.dataset.invalidQueryPreview,
                                    type : 'error'
                              })
                              return;
                        }

                        if (data.result != "ok") {
                              console.error(data.errorMsg);
                              resolve(null);
                              return;
                        }

                        let maxRowCount: number = 12;
                        if (data.data && data.data.length > maxRowCount) {
                              data.data = data.data.splice(0, maxRowCount);
                        }

                        data.data.dbType = data.dbType;

                        resolve(data.data);
                  }).catch((error) => {
                        sendErrorMessage.sendCriticalErrorCommand({
                              message: error
                        });
                        reject(error);
                  })
            });
      },

      getDatasetList(mode: string): Promise<any> {
            mode = mode || '';
            let fileName: string = this.getServerPath() + "/dataset.do";
            let sendData: any = {
                  id: "dataSetService.getDatasetList",
                  params: {mode: ""}
            }
            return new Promise((resolve, reject) => {
                  api.post(fileName, sendData).then((result) => {
                        resolve(result.data);
                  }).catch((error) => {
                        sendErrorMessage.sendCriticalErrorCommand({
                              message: error
                        })
                  })
            });
      },

      updateDatasetList(list: Array<DatasetItemDTO>): Promise<any> {
            let fileName: string = this.getServerPath() + "/dataset.do";
            let sendData: any = {
                  id: "dataSetService.updateDatasetList",
                  params: {list: list}
            };

            return new Promise((resolve, reject) => {
                  api.post(fileName, sendData).then((result) => {
                        resolve(result.data);
                  }).catch((error) => {
                        sendErrorMessage.sendCriticalErrorCommand({
                              message: error
                        })
                  })
            });
      },

      addDataset(sendData: DatasetItemDTO): Promise<any> {
            let fileName = this.getServerPath() + "/dataset.do";
            return api.post(fileName, {
                  id: "dataSetService.insertDataset",
                  params: sendData
            }).then((result) => {
                  return result.data;
            }, (error) => {
                  //console.log("error");
                  return error;
            })
      },

      removeDataset(datasetId: string): Promise<any> {
            let fileName = this.getServerPath() + "/dataset.do";
            return api.post(fileName, {
                  id: "dataSetService.deleteDataset",
                  params: {dataset_id: datasetId}
            }).then((result) => {
                  return result.data;
            }, (error) => {
                  return error;
            })
      },

      modifyDataset(sendData: DatasetItemDTO): Promise<any> {
            let fileName = this.getServerPath() + "/dataset.do";
            return api.post(fileName, {
                  id: "dataSetService.updateDataset",
                  params: sendData
            }).then((result) => {
                  return result.data;
            }, (error) => {
                  //console.log("error");
                  return error;
            })
      },

      /*******************************
       * TIM
       * ****************************/

      loadTimUrlList(): Promise<any> {
            let fileName = this.getServerPath() + "/loader.do";
            let data: any = {
                  "id": "loaderService.registerClass",
                  "params": {"class": "JSONFileParser", "method": "readJSON", "file_name": "output/timURL.json"}
            };

            return api.post(fileName, data).then((result) => {
                  return result.data;

            }, (error) => {
                  console.error("RENOBIT datasetApi.loadTimUrlList() ", error);
                  return error;
            });
      },

      updateTimUrlList(list): Promise<any> {
            let fileName = this.getServerPath() + "/loader.do";
            // 서버에 JSON 파일로 저장
            var jsonValue: string = JSON.stringify({"data": list});
            let data: any = {
                  "id": "loaderService.registerClass",
                  "params": {
                        "class": "JSONFileParser",
                        "method": "writeJSON",
                        "file_name": "output/timURL.json",
                        "value": jsonValue
                  }
            };

            return api.post(fileName, data).then((result) => {
                  return result.data;
            }, (error) => {
                  console.error("RENOBIT datasetApi.updateTimUrlList() ", error);
                  return error;
            });
      },

      loadTimWorkList(url: string): Promise<any> {
            /*
            return api.get(url).then((result) => {
                  return result;
            }, (error) => {
             //     error.type = "error";
                  console.error("RENOBIT datasetApi.loadTimWorkList() ", error);
                  return error;
            });
            */
            return api.get(url).then((result) => {
                  return result;
            }, (error) => {
                  console.error("RENOBIT datasetApi.loadTimWorkList() ", error);
                  return error;
            });
      },

      addDataSource(item): Promise<any> {
            let fileName = this.getServerPath() + "/dataset.do";
            return api.post(fileName, {
                  id: "dataSetService.insertDbConnInfo",
                  params: item
            }).then((result) => {
                  return result.data;
            }, (error) => {
                  //console.log("error");
                  return error;
            })
      },

      getDatasourceList(): Promise<any> {
            let fileName = this.getServerPath() + "/dataset.do";
            return api.post(fileName, {
                  id: "dataSetService.getDbConnInfo",
                  params: {}
            }).then((result) => {
                  return result.data;
            }, (error) => {
                  //console.log("error");
                  return error;
            })
      },

      removeDataSource(item):Promise<any> {
            let fileName = this.getServerPath() + "/dataset.do";
            return api.post(fileName, {
                  id: "dataSetService.deleteDbConnInfo",
                  params: item
            }).then((result) => {
                  return result.data;
            }, (error) => {
                  //console.log("error");
                  return error;
            })
      },

      testDataSource(item):Promise<any> {
            let fileName = this.getServerPath() + "/dataset.do";
            return api.post(fileName, {
                  id: "dataSetService.testDbConnInfo",
                  params: item
            }).then((result) => {
                  return result.data;
            }, (error) => {
                  //console.log("error");
                  return error;
            })
      },

      loadDynamicSQL(dataSouraces):Promise<any> {
            let fileName = this.getServerPath() + "/dataset.do";
            return api.post(fileName, {
                  id: "dataSetService.getDynamicList",
                  params: {
                        dataSouraces: dataSouraces
                  }
            }).then(result => {
                  return result.data;
            }, error => {
                  return error;
            });
      }
}


