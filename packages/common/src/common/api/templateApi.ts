import sendErrorMessage from "./sendErrorMessage";
import {http} from "../../wemb/http/Http";
import api = http.api;

export default {

      getJsonData(fileName: string): Promise<any> {
            let FILENAME = wemb.configManager.serverUrl + "/loader.do";
            let data = {
                  "id": "loaderService.registerClass",
                  "params": {"class": "JSONFileParser", "method": "readJSON", "file_name": fileName}
            };


            return api.post(FILENAME, data).then((result) => {
                  if (result.data.data) {
                        return JSON.parse(result.data.data).data;
                  }
            }, (error) => {
                  console.error("RENOBIT templateApi.getJsonData() ", error);
                  return null;
            });

      },

      setJsonData(filName: string, json: any): Promise<any> {
            let FILENAME = wemb.configManager.serverUrl + "/loader.do";
            // 서버에 JSON 파일로 저장
            var jsonValue: string = '{"data":' + JSON.stringify(json) + '}';

            let data = {
                  "id": "loaderService.registerClass",
                  "params": {"class": "JSONFileParser", "method": "writeJSON", "file_name": filName, "value": jsonValue}
            };

            return api.post(FILENAME, data).then((result) => {
                  // console.log("리소스 JSON 데이터 저장하기 result ===", result);
                  return result;
            }, (error) => {
                  console.error("RENOBIT templateApi.setJsonData() ", error);
                  return null;
            });

      },


      getTemplateDefaultData(): Promise<any> {
            return this.getJsonData("client/common/template/template.default.json");
      },

      getTemplateCustomData(): Promise<any> {
            return this.getJsonData("output/template/template.custom.json");
      },

      setTemplateCustomData(value): Promise<any> {
            return this.setJsonData("output/template/template.custom.json", value);
      }

}


