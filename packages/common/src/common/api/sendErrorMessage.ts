import EditorAppFacade from "../../editor/EditorAppFacade";
import CommonStatic from "../../editor/controller/common/CommonStatic";

import IFacade = puremvc.IFacade;
import {ERROR_TYPE, ErrorVO} from "../../wemb/wv/data/Data";



export default  {

    sendCriticalErrorCommand(vo: ErrorVO) {
          try {
                var facade: IFacade = window.wemb.currentFacade as IFacade;
                vo.type = ERROR_TYPE.SERVER_ERROR;
                facade.sendNotification(CommonStatic.CMD_CRITICAL_ERROR_COMMAND, vo);
          }catch(error){
                console.error("RENOBIT, facade 가 존재하지 않습니다.");
          }

    }

}
