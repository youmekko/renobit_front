import {http} from "../../wemb/http/Http";
import api = http.api;

/*
서버 환경설정 정보 읽기
초기 시작시 사용됨.
 */
export default {
      getServerPath(): string {
            let config: any = window.wemb.configManager;

            let path: string = "";

            if (window.__serverUrl__) {
                  path = window.__serverUrl__;
            } else if (config && config.serverUrl) {
                  path = config.serverUrl;
            }

            return path;
      },

      getJsonData(fileName: string): Promise<any> {
            let SERVERPATH = this.getServerPath() + '/loader.do';
            let data = {
                  "id": "loaderService.registerClass",
                  "params": {"class": "JSONFileParser", "method": "readJSON", "file_name": fileName}
            };

            return api.post(SERVERPATH, data).then((result) => {
                  return JSON.parse(result.data.data);
            }, (error) => {
                  console.error("RENOBIT codeSnippetsApi.getJsonData() ", error);
                  return null;
            });

      },

      setJsonData(filName: string, json: any): Promise<any> {
            let SERVERPATH = this.getServerPath() + '/loader.do';

            // 서버에 JSON 파일로 저장
            var jsonValue: string = JSON.stringify({"data": json});
            let data = {
                  "id": "loaderService.registerClass",
                  "params": {"class": "JSONFileParser", "method": "writeJSON", "file_name": filName, "value": jsonValue}
            };

            return api.post(SERVERPATH, data).then((result) => {
                  // console.log("리소스 JSON 데이터 저장하기 result ===", result);
                  return result;
            }, (error) => {
                  console.error("RENOBIT codeSnippetsApi.setJsonData() ", error);
                  return null;
            });

      },

      getCodeSnippetList(): Promise<any> {
            return this.getJsonData("client/common/wscript/global.snippets.json");
      },

      setCodeSnippetList(value): Promise<any> {
            return this.setJsonData("codeSnippets.json", value);
      },

      getUserCodeSnippetList(): Promise<any> {
            return this.getJsonData("output/wscript/userCodeSnippets.json");
      },

      setUserCodeSnippetList(value): Promise<any> {
            return this.setJsonData("output/wscript/userCodeSnippets.json", value);
      },

      getFavoriteList() {
            return this.getJsonData("output/wscript/userFavoriteCodeSnippets.json");
      },

      setFavoriteList(value): Promise<any> {
            return this.setJsonData("output/wscript/userFavoriteCodeSnippets.json", value);
      }
}


