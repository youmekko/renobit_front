import httpAPI from 'axios';

export default {
      backupProject():Promise<any> {
            let url = window.wemb.configManager.serverUrl + "/backup.do";
            let request:any = {
                  "id": "snapshotService.backup",
                  "params": {}
            };

            return httpAPI.post(url, request);
      },

      loadSnapshotList():Promise<any> {
            let url = window.wemb.configManager.serverUrl + "/backup.do";
            let request:any = {
                  "id": "snapshotService.getSnapshotList",
                  "params": {}
            };

            return httpAPI.post(url, request);
      },

      restoreProject(id:String):Promise<any> {
            let url = window.wemb.configManager.serverUrl + "/backup.do";
            let request:any = {
                  "id": "snapshotService.restore",
                  "params": {
                        "id" : id
                  }
            };

            return httpAPI.post(url, request);
      },

      saveSnapshot(id:String):Promise<any> {
            const url = window.wemb.configManager.serverUrl + "/snapshot/download.do";
            return httpAPI.post(url, { "id": id });
      },

      deleteSnapshot(id:String):Promise<any> {
            let url = window.wemb.configManager.serverUrl + "/backup.do";
            let request:any = {
                  "id": "snapshotService.deleteSnapshot",
                  "params": {
                        "id" : id
                  }
            };

            return httpAPI.post(url, request);
      },

      uploadSnapshot(url, formData, config):Promise<any> {
            return httpAPI.post(url, formData, config);
      }
}
