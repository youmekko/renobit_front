import httpAPI, {AxiosResponse} from 'axios';
import { SessionScheduler } from "../../wemb/wv/managers/SessionScheduler";

export default {
    prolongSession():Promise<any>{
        let url = window.wemb.configManager.serverUrl + "/config.do";
        let request:any = {
            "id": "configManageService.sessionExtension",
            "params": {}
        };

        return httpAPI.post(url, request).then((result) => {
            if( (result && result.data && result.data.result) && result.data.result == "success" ){
                SessionScheduler.getInstance().lastResponseTime = new Date().getTime();

                return {
                      success:true,
                      error:false
                }
            }


            return {
                  success:false,
                  error:true,
                  message:"result error"
            }
        }, (error) => {
              return {
                    success:false,
                    error:true,
                    message:error
              }
        });
    },

    destroySession():Promise<any>{
        let url = window.wemb.configManager.serverUrl + "/config.do";

        let request:any = {
            "id": "configManageService.sessionDestroy",
            "params": {}
        };

        return httpAPI.post(url, request).then((result) => {
            return result;
        }, (error) => {
            return error;
        });
    }
}


