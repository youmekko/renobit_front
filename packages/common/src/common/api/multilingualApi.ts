import {http} from "../../wemb/http/Http";
import api = http.api;
import ConfigManager from "../../wemb/wv/managers/ConfigManager";

/*
서버 환경설정 정보 읽기
초기 시작시 사용됨.
 */
export default {
	getServerPath():string{
		return ConfigManager.getInstance().serverUrl;
    },

    getUserMultilingualData(){
        let path = ConfigManager.getInstance().userMultilingualPath;
        return api.post(path).then((result) => {
            let data = result.data || {};
            return data;
         }, (error) => {
            console.error("RENOBIT multilingualApi.getUserMultilingualData() ",error);
            return null;
        });
    },

    async getPackageInfo():any {
        let SERVERPATH = this.getServerPath() + '/admin.do';
        let data = {
            id: "adminService.getPackageInfo",
            params: {}
        };
        var result = await api.post(SERVERPATH, data);
        return result.data.data;
    },
    
    async getPackLanguageData(code, packs):any {
        //var result = await this.getPackageInfo();
        let ret_data = {};
        for(var i in packs.packNames) {
            var packName = packs.packNames[i].pack_name;
            try {
                let res = await api.get('custom/packs/'+ packName + '/locales/' + code + '.json');
                ret_data[packName] = res;
            } catch (error) {
                ret_data[packName] = {};
            }
        }
        return ret_data;
    },

    getLanguageData(code, pack_data):Promise<any> {    
        let fileName = 'client/common/locales/'+code+'.json';
        return api.get(fileName).then((data:any) => {
            return $.extend(true, data, pack_data);
        });

        //   let SERVERPATH = this.getServerPath() + '/editor.do';
        //   let data = {
        //         "id": "editorService.getLocaleInfo",
        //         "params": {lang: code}
        //   };

        //   return api.post(SERVERPATH, data).then((result) => {
        //         let data = result.data || {};
        //         data = data.data || {};
        //         return data;
        //   }, (error) => {
        //         console.error("RENOBIT multilingualApi.getLanguageData() ",error);
        //         return null;
        //   });
    },
      /*
      node버전에서 사용함.
      일반 loclate 정보와 pack locale 정보 합하기
      추후 자바버전에 pack을 합치는 코드가 들어가는 경우
      getLanguageData()과 getLanguageDataToNode() 합쳐야 함.
       */
      getLanguageDataToNode(code):Promise<any> {

            let fileName = '/locale/info?lang='+code;
            return api.get(fileName).then((data:any) => {
                  return data;
            });
      },


	getMultilingualData():Promise<any>{
        let SERVERPATH = this.getServerPath() + '/loader.do';
        let fileName = "output/multilingual/multilingual.json";
        let data = {
            "id": "loaderService.registerClass",
            "params": { "class": "JSONFileParser", "method": "readJSON", "file_name": fileName }
        };

        return api.post(SERVERPATH, data).then((result) => {
            let data = result.data || {};
            data = result.data.data || '{}';
            return JSON.parse(data);
        }, (error) => {
              console.error("RENOBIT multilingualApi.getMultilingualData() ",error);
            return null;
        });

    },

    setMultilingualData(value:any):Promise<any>{
        let SERVERPATH = this.getServerPath() + '/loader.do';
        let fileName = "output/multilingual/multilingual.json";

        let jsonValue:string = JSON.stringify( value );
        let data = {
            "id": "loaderService.registerClass",
            "params": { "class": "JSONFileParser", "method": "writeJSON", "file_name": fileName, "value": jsonValue }
        };

        return api.post(SERVERPATH, data).then((result) => {
                    return result;
                }, (error) => {
                        console.error("RENOBIT multilingualApi.setMultilingualData() ",error);
                    return null;
                });

    }
}


