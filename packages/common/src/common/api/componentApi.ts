import sendErrorMessage from "./sendErrorMessage";
import {http} from "../../wemb/http/Http";
import api = http.api;


export default {

      loadPackInfo(): Promise<any> {
            let fileName = 'custom/packs/installed_packs.json';
            //console.log("@@ fileName loadPackInfo ", fileName);
            return api.get(fileName).then((data) => {
                  if (data.hasOwnProperty("packs")) {
                        if(data.packs.length==0)
                              return [];

                        return data.packs;
                  } else {
                        sendErrorMessage.sendCriticalErrorCommand({
                              message: "기본 Pack 정보에 packs 속성이 존재하지 않습니다. 관리자에 문의해주세요."
                        })

                        return [];
                  }

            }).catch((error) => {
                  sendErrorMessage.sendCriticalErrorCommand({
                        message: error
                  })
            })
      },
      loadPackages(): Promise<any> {
            let url = window.wemb.configManager.workUrl;
            let data = {
                  "id":"editorService.getPackages",
                  "params":{
                        "mode":wemb.configManager.exeMode
                  }
            }
            return api.post(url,data).then((data) => {
                  if (data.data.data.length == 0)
                        return [];

                  return data.data.data;
            }).catch((error) => {
                  console.error("RENOBIT  loadPackages() ", error);
			sendErrorMessage.sendCriticalErrorCommand({
				message: error
			})
            })
      },

	loadComponentFileListJson(packName:string): Promise<any> {
		let fileName = `custom/packs/${packName}/component_files.json`;
		return api.get(fileName).then((data) => {


                  try {
                        if (data.length == 0)
                              return [];

                        return data;


                  }catch(error){
				sendErrorMessage.sendCriticalErrorCommand({
					message: packName+"기본 컴포넌트 정보가 정상적이지 않습ㄴ디ㅏ. 관리자에 문의해주세요."
				})
			}

		}).catch((error) => {
		      console.error("RENOBIT loadComponentFileListJson() ", error);
			sendErrorMessage.sendCriticalErrorCommand({
				message: error
			})
		})
      },

      loadComponentFileListApi(): Promise<any> {
            let url = location.origin + "/component/search";
            let data = {
                  type:'file',
                  file_type:'component',
                  isViewer : !window.wemb.configManager.isEditorMode,
                  pageList:window.wemb.pageManager._pageInfoList
            }
            return api.post(url,data).then((data) => {
                  if (data.data.length == 0)
                        return [];

                  return data.data;
            }).catch((error) => {
                  console.error("RENOBIT  loadComponentFileListApi() ", error);
			sendErrorMessage.sendCriticalErrorCommand({
				message: error
			})
            })
      },

      /*
      2018.10.12(ckkim)
            - 수정되어 isViewer를 원상태로 변경
      2018.10.08 (ckkim)
            - isViewer true인 경우 컴포넌트 정보를 2번 가져오고 있음.
            - isViewer : !window.wemb.configManager.isEditorMode,

       */
      loadExtensionFileListApi(): Promise<any> {
            let url = location.origin + "/component/search";
            let data = {
                  type:'file',
                  file_type:'extension',

                  //isViewer : false,
                  isViewer : !window.wemb.configManager.isEditorMode,

                  pageList:window.wemb.pageManager._pageInfoList
            }
            return api.post(url,data).then((data) => {
                  if (data.data.length == 0)
                        return [];

                  return data.data;
            }).catch((error) => {
                  console.error("RENOBIT  ", error);
			sendErrorMessage.sendCriticalErrorCommand({
				message: error
			})
            })
      },

      loadExtensionFileList(packName:string): Promise<any> {
            let fileName = `custom/packs/${packName}/extension_files.json`;
            return api.get(fileName).then((data) => {
                  if(data){

                        return data;
                  } else {
                        sendErrorMessage.sendCriticalErrorCommand({
                              message: "커스텀 패널 정보에서 읽어들인 정보에 list 속성이 존재하지 않습니다. 관리자에 문의해주세요."
                        })
                  }

            }).catch((error) => {
                  sendErrorMessage.sendCriticalErrorCommand({
                        message: error
                  })
            })
      },



      // 컴포넌트 리스트 패널에 출력될 컴포넌트 정보 읽기
      loadComponentInfoListJson(packName:string): Promise<any>  {
            let fileName = `custom/packs/${packName}/component_panel_info.json`;
            return api.get(fileName).then((data) => {
                  return data;
            }, (error) => {
                  sendErrorMessage.sendCriticalErrorCommand({
                        message: error
                  })
            })
      },
      loadComponentInfoListApi(packList): Promise<any> {
            let url = location.origin + "/component/search";
            let data = {
                  type:'info',
                  file_type:'component',
                  isViewer : !window.wemb.configManager.isEditorMode,
                  packList:packList,
                  pageList:window.wemb.pageManager._pageInfoList
            }
            return api.post(url,data).then((data) => {
                  return data.data;
            }).catch((error) => {
                  console.error("RENOBIT loadComponentInfoListApi", error);
			sendErrorMessage.sendCriticalErrorCommand({
				message: error
			})
            })
      },


}
