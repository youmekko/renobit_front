import sendErrorMessage from "./sendErrorMessage";
import {http} from "../../wemb/http/Http";
import api = http.api;

export default {
	loadMenesetList():Promise<any> {
        return new Promise((resolve, reject)=>{
            let FILENAME = wemb.configManager.serverUrl + "/loader.do";
            let data = {
                "id": "loaderService.registerClass",
                "params": {
                    "class": "JSONFileParser",
                    "method": "readJSON",
                    "file_name": "output/menu/menuset.json"
                }
            };

            api.post(FILENAME, data).then((result)=>{
                if (!result || !result.data || !result.data.data) {
                    reject();
                }else{
                    let menuData = JSON.parse(result.data.data);
                    resolve( menuData );
                }
            }, (error)=>{
                console.error("RENOBIT menusetApi.loadMenesetList() 메뉴셋 JSON 데이터 읽어오기 에러", error);
                reject();
            });
        })
    },

    updateMenusetList(menuList){
        return new Promise((resolve, reject)=>{
            var jsonValue = JSON.stringify({"menu": menuList});
            var FILENAME = wemb.configManager.serverUrl + "/loader.do";
            var data = {
                "id": "loaderService.registerClass",
                "params": {
                    "class": "JSONFileParser",
                    "method": "writeJSON",
                    "file_name": "output/menu/menuset.json",
                    "value": jsonValue
                }
            };

            api.post(FILENAME, data).then((result)=>{

                resolve();
            }, (error)=>{
                  console.error("RENOBIT menusetApi.updateMenusetList() 메뉴셋 JSON 데이터 읽어오기 에러", error);
                reject();
            });
        })
    }

}


