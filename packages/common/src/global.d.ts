import { WVPointComponentEvent } from "./wemb/core/events/WVPointComponentEvent";
import WV3DIconLabel from "./wemb/core/component/3D/drawable/WV3DIconLabel";
import ScriptUtil from "./wemb/core/utils/ScriptUtil";
declare global {

	interface Window {
		Vue;
		wemb;
		WeMB;
		WV3DIconLabel;
		CPLogger;
		ThreeUtil;
		WVComponent;
		WVDOMComponent;
		WVSeverityComponent;
		WVPropertyManager;
		WVSVGComponent;
		WV3DLabel;
		WVPointComponent;
		WVSVGStreamLineComponent;
		WVComponentContainer;
		WV3DComponent;
		WV3DResourceComponent;
		WV3DPropertyManager;
		NWV3DComponent;
		WV3DSprite;
		WVMeshDecorator;
		WV3DInstanceProxy;
		NLoaderManager;
		WVDataGrid;
		clipboardData;
		__scriptWorkerChannel__;
		__scriptEditorChannel__;
		__serverUrl__;
		WVToolTipManager;
		CPUtil;
		Spinner;
		Point;
		WV2DComponentProperty;
		WVPointComponentEvent;
		MeshManager;
		LoaderManager;
		http;
		TopViewPanel;
		ExtensionPanelCore;
		ExtensionPluginCore;
		puremvc;
		CoreTemplate;
		HookManager;
		EditorStatic;
		ViewerStatic;
		ScriptUtil: ScriptUtil;
	}
	const wemb;
}

