import core from './wemb/core'
import { http } from './wemb/http/Http'
import * as util from './wemb/utils/Util'
import ws from './wemb/ws/Socket'
import wv from './wemb/wv'
import ErrorPageComponent from './ErrorPageComponent' //vue

const common = {
    core,
    http,
    util,
    ws,
    wv,
    ErrorPageComponent
}

export default common

