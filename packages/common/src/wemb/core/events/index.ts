import * as EventInterfaces from './interfaces/EventInterfaces'

import DatasetEvent from './DatasetEvent'
import EventDispatcher from './EventDispatcher'
import WScriptEventWatcher from './WScriptEventWatcher'
import WVComponentEvent from './WVComponentEvent'
import WVComponentEventDispatcher from './WVComponentEventDispatcher'
import WVEvent from './WVEvent'
import WVMouseEvent from './WVMouseEvent'
import WVPointComponentEvent from './WVPointComponentEvent'


const events = {
    interfaces: {
        EventInterfaces
    },
    DatasetEvent,
    WScriptEventWatcher,
    WVComponentEvent ,
    WVComponentEventDispatcher,
    WVEvent,
    WVMouseEvent,
    WVPointComponentEvent,
}

export default events