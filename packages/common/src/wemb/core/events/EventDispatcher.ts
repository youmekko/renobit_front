import {EventListenerTarget, IEventDispatcher, IWVEvent} from "./interfaces/EventInterfaces";

export default class EventDispatcher implements IEventDispatcher {

      private _listeners:EventListenerTarget[];
      constructor(){
            this._listeners =[];
      }

      hasEventListener( eventType:string, handler?:Function ):boolean {
            let exists:boolean = false;
            let listener:EventListenerTarget;
            let i;
            for( i = 0; i<this._listeners.length; i++ ){
                  listener = this._listeners[i];
                  if( listener.type == eventType ){
                        if( handler && listener.handler !== handler ){
                              return exists;
                        }
                        exists = true;
                        return exists;
                  }
            }
            return exists;
      }

      addEventListener( eventType:string, handler:Function ):boolean {
            if( this.hasEventListener(eventType, handler)){
                  return false;
            }
            this._listeners.push({type:eventType, handler:handler});
            return true;
      }

      removeEventListener( eventType:string, handler:Function ):boolean {
            if( this.hasEventListener(eventType, handler) ){
                  let listener:EventListenerTarget;
                  let i;
                  for( i=0; i<this._listeners.length; i++ ){
                        listener = this._listeners[i];
                        if( listener.type == eventType && listener.handler == handler ){
                              this._listeners.splice(i, 1);
                              return true;
                        }
                  }
            }
            return false;
      }

      /**
       scope값이 있을 경우는 사용 기본은 event.target을 이용
       * */
      dispatchEvent( event:IWVEvent, scope?:any ) {
            let listener:EventListenerTarget;
            let i;
            try {
                  for (i = 0; i < this._listeners.length; i++) {
                        listener = this._listeners[i];
                        if (listener.type == event.type) {
                              listener.handler.call(scope || event.target, event);
                        }
                  }
            }catch(error){
                  console.log("주의! dispatchEvent() 메서드에서 에러가 발생하지 말아야하는데 에러 발생 중 ", error);
            }

      }

      destroy():void {
            let i:number
            let listener:EventListenerTarget;
            for( i=0; i<this._listeners.length;i++ ){
                  listener = this._listeners[i];
                  this.removeEventListener(listener.type, listener.handler);
            }
            this._listeners = null;
      }

}
