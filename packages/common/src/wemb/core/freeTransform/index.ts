import CircleControlCursor from './controls/html/CircleControlCursor'
import ControlCursor from './controls/html/ControlCursor'
import RectControlCursor from './controls/html/RectControlCursor'
import TranslateCursor from './controls/html/TranslateCursor'

import SVGControlCursor from './controls/svg/SVGControlCursor'
import SVGLineTranslateCursor from './controls/svg/SVGLineTranslateCursor'
import SVGPointControlCursor from './controls/svg/SVGPointControlCursor'
import SVGPointGuide from './controls/svg/SVGPointGuide'

import TransformEvent from './event/TransformEvent'

import * as WVTransformInterfaces from './interfaces/WVTransformInterfaces'

import WVPointTransformTool from './WVPointTransformTool'
import WVTransformTool from './WVTransformTool'
import WVTransformToolMG from './WVTransformToolMG'

const freeTransform = {
    cotrols: {
        html: {
            CircleControlCursor,
            ControlCursor,
            RectControlCursor,
            TranslateCursor
        },
        svg: {
            SVGControlCursor,
            SVGLineTranslateCursor,
            SVGPointControlCursor,
            SVGPointGuide
        }
    },
    event: {
        TransformEvent
    },
    interfaces: {
        WVTransformInterfaces
    },
    WVPointTransformTool,
    WVTransformTool,
    WVTransformToolMG
}

export default freeTransform