import Point from "../../../../core/geom/Point";
import EventDispatcher from "../../../../core/events/EventDispatcher";
import TransformEvent from "../../event/TransformEvent";
import WVTransformTool from "../../WVTransformTool";
import {TransformCursor, TransformCursorType} from "../../interfaces/WVTransformInterfaces";
import CPNotification from "../../../utils/CPNotification";
import {IWVEvent} from "../../../events/interfaces/EventInterfaces";

export default class ControlCursor implements TransformCursor {

      set pivotCursor(cursor: TransformCursor) {
            this._pivotCursor = cursor;
      }

      get pivotCursor(): TransformCursor {
            return this._pivotCursor;
      }

      get type(): string {
            return this._type;
      }

      get position(): Point {
            return this._position
      }

      get size(): number {
            return this._size;
      }

      get offset(): number {
            return this._offset;
      }

      get element(): HTMLElement {
            return this._element;
      }

      get offsetPoint(): Point {
            return this._offsetPoint;
      }


      onMoveHandler: Function;
      protected _element: HTMLElement;
      protected _offset: number;
      protected _position: Point;
      protected _pivotCursor?: TransformCursor;

      protected _type: string;
      protected _size: number;
      protected _offsetPoint: Point;
      protected _style: any;

      constructor(type: string, size: number, offset: number, style?: string, pivotCursor?: TransformCursor) {
            this._type = type;
            this._size = size;
            this._offset = offset;
            this._style = $.extend({
                  position: "absolute",
                  tranformOrigin: "center",
                  transform: 'translate(-50%, -50%)',
                  width: this.size,
                  height: this.size,
                  backgroundColor: "#fff",
                  border: "1px solid black",
                  pointerEvents: "auto"
            }, style);
            this._offsetPoint = new Point();
            this._dispatcher = new EventDispatcher();
            this._pivotCursor = pivotCursor || null;
            this._position = new Point();
      }


      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       커서 사용 메서드 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       */
      protected registEventListener(): void {
            $(this.element).on("mousedown", this._onMouseDown_Control.bind(this));
      }


      protected _onMouseDown_Control(event: any): void {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            this.offsetPoint.x = this.offset + event.offsetX;
            this.offsetPoint.y = this.offset + event.offsetY;
            this.dispatchEvent(new TransformEvent(TransformEvent.DOWN_CURSOR, this, {
                  px: event.pageX - this.offsetPoint.x,
                  py: event.pageY - this.offsetPoint.y
            }));
      }

      protected _createCursorElement(style: any): HTMLElement {
            CPNotification.getInstance().notifyMessage("커서 표현을 위한 엘리먼트 생성 처리를 구현해 주세요");
            return null;
      }

      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IDrawable인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       */
      draw(parent: WVTransformTool): void {
            this._element = this._createCursorElement(this._style);
            $(this._element).addClass(`${TransformCursorType.DEFAULT} ${this.type}`);
            $(parent.container).append(this._element);
            this.registEventListener();
      }

      reDraw(info: any): void {
            let jQueryEle: any = $(this.element);
            if (info.position) {
                  jQueryEle.css({
                        left: info.position.x + "px",
                        top: info.position.y + "px"
                  });
                  this._position.x = info.position.x;
                  this._position.y = info.position.y;
            }


            (info.visible) ? jQueryEle.show() : jQueryEle.hide();
      }

      clear(): void {
            $(this._element).off();
            $(this._element).remove();
            this._dispatcher.destroy();
            this._type = null;
            this._size = null;
            this._offset = null;
            this._pivotCursor = null;
            this._offsetPoint = null;
            this._dispatcher = null;
            this._element = null;
      }

      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IEventDispatcher 인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       */
      protected _dispatcher: EventDispatcher;

      addEventListener(eventType: string, handler: Function): boolean {
            return this._dispatcher.addEventListener(eventType, handler);
      }

      removeEventListener(eventType: string, handler: Function): boolean {
            return this._dispatcher.removeEventListener(eventType, handler);
      }

      hasEventListener(eventType: string, handler?: Function): boolean {
            return this._dispatcher.hasEventListener(eventType, handler);
      }

      dispatchEvent(event: IWVEvent, scope?: any): void {
            this._dispatcher.dispatchEvent(event, scope);
      }

}
