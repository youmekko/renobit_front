import ControlCursor from "./ControlCursor";
import {TransformCursor} from "../../interfaces/WVTransformInterfaces";

export default class CircleControlCursor extends ControlCursor {
    constructor( type:string, size:number, offset:number, style?:any, pivotCursor?:TransformCursor ){
        super(type, size, offset, style, pivotCursor);
    }

    /**
     ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
     IDrawable인터페이스 구현
     ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
     */
    protected _createCursorElement( style?:any ):HTMLElement {
        let element = document.createElement("div");
        style.borderRadius = "50%";
        $(element).css(style);
        return element;
    }

}
