import ControlCursor from "./ControlCursor";
import {TransformCursor} from "../../interfaces/WVTransformInterfaces";

export default class TranslateCursor extends ControlCursor {

      constructor(type: string, size: number, offset: number, style?: any, pivotCursor?: TransformCursor) {
            super(type, size, offset, style, pivotCursor);
      }

      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IDrawable인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       */
      protected _createCursorElement(style: any): HTMLElement {
            let element: HTMLElement = document.createElement("div");
            style.transformOrigin = "0 0";
            style.backgroundColor = "rgba(0, 0, 0, 0)";

            $(element).css(style);
            return element;
      }

      reDraw(info: any): void {
            let jqueryEle: any = $(this.element);
            jqueryEle.css({
                  width: info.width + "px",
                  height: info.height + "px",
                  left: info.x + "px",
                  top: info.y + "px",
                  zIndex: "-1",
                  transform: `rotate(${info.rotate}deg)`
            });
            (info.visible) ? jqueryEle.show() : jqueryEle.hide();
      }

}
