import SVGControlCursor from "./SVGControlCursor";
import CPUtil from "../../../utils/CPUtil";
import {TransformCursorType} from "../../interfaces/WVTransformInterfaces";

export default class SVGLineTranslateCursor extends SVGControlCursor {

      constructor(type: string, size: number, offset: number, style?: any) {

            super(type, size, offset, style);
      }

      protected _createCursorElement(style?: any): SVGElement {
            return CPUtil.getInstance().createSVGElement("path", style);
      }

      draw(parent: SVGElement, style?: any): void {
            super.draw(parent, style );
            $(this._element).css({ cursor:"pointer",  pointerEvents:"stroke" });
      }

      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IDrawable인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       */

      reDraw(info: any): void {
            $(this.element).attr({d: info.path});
      }


}
