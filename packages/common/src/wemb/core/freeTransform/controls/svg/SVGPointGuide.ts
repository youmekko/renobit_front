import {IDrawDisplay, IReDrawDisplay} from "../../../../core/display/interfaces/DisplayInterfaces";
import CPUtil from "../../../utils/CPUtil";

export class SVGPointGuide implements IDrawDisplay , IReDrawDisplay {

    protected _style:any;
    protected _element:SVGGraphicsElement;
    constructor(style?:any){
        this._style = $.extend({
            "stroke":"black",
            "fill":"none",
            "fill-opacity":0,
            "stroke-width":1,
            "stroke-dasharray":"5 5 1 5"
        }, style);
    }

    draw( parent:SVGElement, style?:any ):void{
        this._element = CPUtil.getInstance().createSVGElement("path", this._style ) as SVGGraphicsElement;
        parent.appendChild(this._element);
    }

    clear():void{
        $(this._element).remove();
        this._style = null;
        this._element = null;
    }

    reDraw(info:any ):void{
        let jQueryEle = $(this._element);
        if(info.path){
            jQueryEle.attr({"d":info.path});
        }
        (info.visible) ? jQueryEle.attr({visibility: "visible"}) : jQueryEle.attr({visibility: "hidden"});
    }
}
