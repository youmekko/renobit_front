import {IEventDispatcher} from "../../../core/events/interfaces/EventInterfaces";
import Point from "../../../core/geom/Point";
import {IDrawDisplay, IReDrawDisplay} from "../../display/interfaces/DisplayInterfaces";
import WV2DComponent from "../../component/2D/WV2DComponent";

export interface IWVTransformTool extends IEventDispatcher, ITransformToolCalculator, IDrawDisplay {
      target: WV2DComponent;
      toggleCursor(bValue: boolean): void
      initGroupSync(): void
      syncFromTarget():void // 호출 시 타깃 정보로 툴을 재 설정

}

// 트랜스폼 툴에서 변경 되는 요청 처리에 사용하는 인터페이스
export interface ITransformToolCalculator {
      scale(sx: number, sy: number, type?: string): void

      rotate(angle: number): void

      translate(tx: number, ty: number): void
}


export interface TransformCursor extends IDrawDisplay, IReDrawDisplay, IEventDispatcher {
      readonly element: HTMLElement | SVGElement;
      readonly type: string;
      readonly size: number;
      readonly position: Point;
      readonly offsetPoint?: Point;
      pivotCursor?: TransformCursor;
      onMoveHandler?: Function;
}


export namespace TransformCursorType {

      export const CURVE_CONTROL: string = "curve-control";
      export const BROKEN_CONTROL: string = "broken-control";
      export const BROKEN_ANCHOR_POINT: string = "broken-anchor-point";


      export const POINT_ANCHOR: string = "point-anchor";
      export const POINT_BROKEN: string = "point-broken";

      export const TOP_LEFT_SCALE: string = "tl";
      export const TOP_MIDDLE_SCALE: string = "tm";
      export const TOP_RIGHT_SCALE: string = "tr";
      export const MIDDLE_LEFT_SCALE: string = "ml";
      export const MIDDLE_RIGHT_SCALE: string = "mr";
      export const BOTTOM_LEFT_SCALE: string = "bl";
      export const BOTTOM_MIDDLE_SCALE: string = "bm";
      export const BOTTOM_RIGHT_SCALE: string = "br";

      export const TRANSLATE: string = "trans";
      export const ROTATE: string = "rotate";
      export const REGIST: string = "regist";

      export const DEFAULT: string = "wvtransform-cursor";

}
