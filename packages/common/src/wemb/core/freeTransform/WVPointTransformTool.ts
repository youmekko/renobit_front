import EventDispatcher from "../../core/events/EventDispatcher";
import TransformEvent from "./event/TransformEvent";

import Point from "../../core/geom/Point";
import SVGPointControlCursor from "./controls/svg/SVGPointControlCursor";

import {IPointGuideDisplay} from "../../core/display/interfaces/DisplayInterfaces";
import SVGLineTranslateCursor from "./controls/svg/SVGLineTranslateCursor";
import {WVPointComponentEvent} from "../../core/events/WVPointComponentEvent";
import {SVGPointGuide} from "./controls/svg/SVGPointGuide";
import {IWVTransformTool, TransformCursor, TransformCursorType} from "./interfaces/WVTransformInterfaces";
import WVPointComponent from "../component/2D/WVPointComponent";
import CPUtil from "../utils/CPUtil";
import {GROUP_NAME_LIST} from "../component/WVComponent";
import {WVSVG_PROPERTIES} from "../component/interfaces/ComponentInterfaces";
import {$} from "../utils/reference";
/**
 * SVG라인 처리용 transform객체
 * 라인영역에 해당하는 부분을 두껍게 만들어서 컨트롤 가능한 형태로 제공.
 * transform과 translate만 공유하고 나머지는 무시.
 *
 * 커서는 컨트롤 포인트와 축점으로 처리?
 * 모든 컨트롤은 이동하고 가상 돔만 translate처리 ?
 * */
export default class WVPointTransformTool extends EventDispatcher implements IWVTransformTool {

      protected _target: WVPointComponent;
      set target(objTarget: WVPointComponent) {
            if (this._target != objTarget) {
                  if (this._target) {
                        this.resetTargetResource();
                  }
                  if (objTarget != null) {
                        this._target = objTarget;
                        this._init();
                  }
            }
      }

      toggleCursor(bValue:boolean):void{
            this._updateCursorState();
      }

      syncFromTarget():void{
            let cursor:TransformCursor;
            let point:Point;
            // 변경된 포인트 정보에 맞춰 커서 위치 갱신
            for( let idx:number = 0 ; idx<this.target.points.length; idx++) {
                  point = this.target.points[idx];
                  cursor = this.cursors[idx];
                  cursor.reDraw({
                        x:point.x,
                        y:point.y,
                        visible:true
                  });
            }
            this.translateControl.reDraw({path:this.target.paths});

            if(this.isGuideDisplay(this.target)) {
                  let guideTarget:IPointGuideDisplay = this.isGuideDisplay(this.target);
                  this.curveGuide.reDraw({path:guideTarget.controlPaths});
            }
      }

      initGroupSync():void{}



      protected _init():void{
            this.target.selected = true;
            this._initTranslateCursor();
            if(this.isGuideDisplay(this.target)) {
                  this._initCurveGuide();
            }
            this._initCursorWithPoint(this.target.points);
            this.target.addEventListener(WVPointComponentEvent.INSERT_POINT, this._onChangeComponent_Handler );
            this.target.addEventListener( WVPointComponentEvent.REMOVE_POINT, this._onChangeComponent_Handler);
      }


      get target():WVPointComponent { return this._target; }

      get container():any { return this._container; }

      private translateControl:SVGLineTranslateCursor;
      private selectControl:TransformCursor;
      private mouseLoc:Point;
      private startControlLoc:Point;
      private startMouseLoc:Point;
      private startPoints:Array<Point>;
      private moveLoc:Point;
      private _container:any;
      private _onMouseMove_Handler: EventListenerObject;
      private _onMouseUp_Handler: EventListenerObject;
      private _onMouseDown_Handler: Function;
      private _onMoveTranslate_Handler:Function;
      private _onChangeComponent_Handler:Function;
      private _onDoubleClick_Handler:Function;
      private option: any;
      private cursors:Array<TransformCursor>;

      constructor(opt?: any) {
            super();
            this.option = $.extend({}, opt);
            this._preInitialize();
      }

      protected _preInitialize(): void {
            this.cursors = [];
            this._container = CPUtil.getInstance().createSVGElement("svg");
            $(this._container).css({
                  position: "absolute",
                  left:"0",
                  top:"0",
                  width:"100%",
                  height:"100%",
                  pointerEvents:"none"
            });
            this.mouseLoc               = new Point();
            this.startMouseLoc          = this.mouseLoc.clone();
            this.startControlLoc        = this.mouseLoc.clone();
            this.moveLoc                = this.mouseLoc.clone();
            this._onMouseMove_Handler   = this._onMouseMove_Document.bind(this);
            this._onMouseUp_Handler     = this._onMouseUp_Document.bind(this);
            this._onMouseDown_Handler   = this._onMouseDown_Cursor.bind(this);
            this._onMoveTranslate_Handler = this._onMoveTranslate_Cursor.bind(this);
            this._onChangeComponent_Handler = this._onChangeComponent.bind(this);
            this._onDoubleClick_Handler = this._onDoubleClick_Cursor.bind(this);
      }

      protected _initPositionReference(mouseInfo:any):void{
            this.mouseLoc.x   = mouseInfo.px;
            this.mouseLoc.y   = mouseInfo.py;
            this.startMouseLoc = this.mouseLoc.clone();
            this.startControlLoc = this.selectControl.position.clone();
            this.startPoints     = this.target.points.concat();
      }

      // 초기화 시 타깃을 포인트 정보를 이용해 커서 위치를 설정해 준다.
      private _initCursorWithPoint( points:Array<Point>):void{
            let cursorList:Array<string>    = this.target.handleTransformList();
            if(points.length>=2){
                  let cursor  :TransformCursor;
                  let i       :number;
                  // 나머지 컨트롤 요소 생성
                  for( i = 0; i<points.length;i++ ){
                        let point : Point = points[i];
                        cursor = this.createCursorByType( cursorList[i] );
                        cursor.draw(this.container);
                        cursor.reDraw({ x:point.x,  y:point.y , visible:true});
                        this._registEventCursor( cursor, TransformEvent.DOWN_CURSOR, this._onMouseDown_Handler );
                        this.cursors.push(cursor);
                  }
            }
      }

      protected _initTranslateCursor():void {
            this.translateControl = new SVGLineTranslateCursor( TransformCursorType.TRANSLATE, 0, 0,{
                  "stroke-width": Math.max( 3,  this.target.getGroupPropertyValue( GROUP_NAME_LIST.STYLE, WVSVG_PROPERTIES.STROKE_WIDTH ) - 1  ) ,
                  "stroke-opacity":0,
                  "stroke":"#ff0000",
                  "fill":"#000000",
                  "fill-opacity":0
            });

            this.translateControl.onMoveHandler = this._onMoveTranslate_Handler;
            this.translateControl.draw( this.container );
            this.translateControl.reDraw({path:this.target.paths});
            this._registEventCursor(this.translateControl, TransformEvent.DOWN_CURSOR, this._onMouseDown_Handler );
            $(this.translateControl.element).on("dblclick", this._onDoubleClick_Handler );
      }

      protected _updateMousePosition( event:MouseEvent ):void{
            this.mouseLoc.x = event.pageX;
            this.mouseLoc.y = event.pageY;
            this.moveLoc    = this.mouseLoc.subtract(this.startMouseLoc);
      }

      protected _onChangeComponent( event:WVPointComponentEvent ):void{
            let point:Point = event.data.point;
            let type:string = event.data.type;
            let cursor:TransformCursor;
            if( event.type == WVPointComponentEvent.INSERT_POINT ){
                  cursor = this.createCursorByType( type );
                  cursor.draw(this.container);
                  cursor.reDraw({
                        x:point.x,
                        y:point.y
                  });
                  this._registEventCursor( cursor, TransformEvent.DOWN_CURSOR, this._onMouseDown_Handler );
                  this.cursors.splice( this.cursors.length-1, 0, cursor);
            } else {

                  let idx:number  = this.target.points.indexOf(point);
                  cursor          = this.cursors.splice( idx, 1 )[0];
                  this._removeEventCursor(cursor,  TransformEvent.DOWN_CURSOR, this._onMouseDown_Handler );
                  cursor.clear();
            }
           this.syncFromTarget();
      }



      protected _applyTransformValue():void{

            let results:Array<Point> = [];
            let point:Point;
            let prevx:number = this.target.x;
            let prevy:number = this.target.y;
            this.cursors.forEach((cursor:TransformCursor)=>{
                  point   = cursor.position.clone();
                  results.push(point);
                  cursor.reDraw({visible:false});
            });
            this.target.points = results;
            this.target.invalidateImmediateProperties();
            this.translateControl.reDraw({path:this.target.paths});

            if(this.isGuideDisplay(this.target)) {
                  let guideTarget:IPointGuideDisplay = this.isGuideDisplay(this.target);
                  this.curveGuide.reDraw({path:guideTarget.controlPaths});
            }

            if( this.selectControl && this.selectControl.type == TransformCursorType.TRANSLATE ){
                  this.dispatchEvent(new TransformEvent(TransformEvent.UPDATE_TRANSFORM, this, {
                        x: this.target.x - prevx,
                        y: this.target.y - prevy,
                        type : this.selectControl.type
                  }));
            }
      }


      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       커서 움직임에 따른 핸들러 함수 구현 시작
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       * */
      protected _onMoveControlCursor():void{
            this.selectControl.reDraw({
                  x:this.startControlLoc.x+this.moveLoc.x,
                  y:this.startControlLoc.y+this.moveLoc.y
            });
      }


      protected _onMoveTranslate_Cursor():void{
            let cursor:TransformCursor;
            let point:Point;
            for( let i:number = 0;i<this.cursors.length;i++) {
                  cursor  = this.cursors[i];
                  point   = this.startPoints[i];
                  cursor.reDraw({
                        x: point.x+this.moveLoc.x,
                        y: point.y+this.moveLoc.y
                  });
            }
      }


      // brokenline 포인트 커서 핸들러 함수
      protected _onMoveBrokenPointCursor():void{

            this._onMoveControlCursor(); // 커서 이동 처리
            let startCursor:TransformCursor  = this.cursors[0] as TransformCursor;
            let centerCursor:TransformCursor = this.cursors[1] as TransformCursor;
            let endCursor:TransformCursor    = this.cursors[2] as TransformCursor;

            let p1:Point = new Point( Math.min(startCursor.position.x, endCursor.position.x), Math.min(startCursor.position.y, endCursor.position.y) );

            let p2:Point = new Point(
                  Math.abs(startCursor.position.x - endCursor.position.x),
                  Math.abs(startCursor.position.y - endCursor.position.y) );
            p2.x = Math.max(1, p2.x );
            p2.y = Math.max(1, p2.y );
            centerCursor.reDraw({
                  x:p1.x + (p2.x)/2,
                  y:p1.y + (p2.y)/2
            });
      }

      // brokenline 컨트롤 커서 핸들러 함수
      protected _onMoveBrokenControlCursor():void{
            let startCursor:TransformCursor  = this.cursors[0] as TransformCursor;
            let endCursor:TransformCursor    = this.cursors[2] as TransformCursor;
            let diff = {x: Math.abs(startCursor.position.x - endCursor.position.x),
                  y: Math.abs(startCursor.position.y - endCursor.position.y) };
            let movePos = this.startControlLoc.clone();
            if (diff.x > diff.y) {
                  movePos.x += this.moveLoc.x;
            } else {
                  movePos.y += this.moveLoc.y;
            }
            this.selectControl.reDraw({x:movePos.x, y:movePos.y});
      }


      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       타입에 따른 커서 생성 관련 함수 시작
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       * */
      private createCursorByType( type:string ):TransformCursor{

            let cursorSize: number = 4;
            let cursorOffset: number = 0;
            let style:any ={};
            let cursor:TransformCursor;
            switch( type ){
                  case TransformCursorType.POINT_ANCHOR:
                  case TransformCursorType.BROKEN_ANCHOR_POINT:
                        style.fill  = "red";
                        break;

                  case TransformCursorType.BROKEN_CONTROL:
                        style.fill  = "blue";
                        break;

                  case TransformCursorType.CURVE_CONTROL:
                        style.fill  = "green";
                        break;
            }

            cursor = new SVGPointControlCursor( type, cursorSize, cursorOffset , style);
            switch( type ){
                  case TransformCursorType.POINT_ANCHOR:
                  case TransformCursorType.CURVE_CONTROL:
                  case TransformCursorType.BROKEN_ANCHOR_POINT:
                        cursor.onMoveHandler = this._onMoveControlCursor.bind(this);
                        break;
                  /* 멀티 포인트 제공을 위해 주석
                  case TransformCursorType.BROKEN_ANCHOR_POINT:
                      cursor.onMoveHandler = this._onMoveBrokenPointCursor.bind(this);
                      break;
                      */
                  case TransformCursorType.BROKEN_CONTROL:
                        cursor.onMoveHandler = this._onMoveBrokenControlCursor.bind(this);
                        break;
            }

            return cursor;
      }


      private curveGuide:SVGPointGuide;
      protected _initCurveGuide():void{
            let guideTarget:IPointGuideDisplay = this.isGuideDisplay(this.target);
            this.curveGuide = new SVGPointGuide();
            this.curveGuide.draw(this.container);
            this.curveGuide.reDraw({path:guideTarget.controlPaths, visible:true});
      }

      protected isGuideDisplay( objTarget:any ):IPointGuideDisplay {
            if(objTarget.controlPaths !== undefined ){
                  return objTarget as IPointGuideDisplay;
            }
            return null;
      }




      // 대상 상태에 따른 커서 상태 변경
      private _updateCursorState():void{
            this.cursors.forEach((cursor:TransformCursor)=>{
                  cursor.reDraw({visible:true});
            });
            if(this.curveGuide){this.curveGuide.reDraw({visible:true});}
      }
      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       document 이벤트 관련
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       * */
      protected _onMouseUp_Document(event:MouseEvent): void {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            this._removeDocumentEvent();
            this._updateCursorState();
            this.dispatchEvent(new TransformEvent(TransformEvent.COMPLETE_TRANSFORM, this));
            this.selectControl = null;
      }

      protected _registDocumentEvent(): void {

            document.addEventListener("mousemove", this._onMouseMove_Handler);
            document.addEventListener("mouseup", this._onMouseUp_Handler);
      }

      protected _removeDocumentEvent(): void {

            document.removeEventListener("mousemove", this._onMouseMove_Handler);
            document.removeEventListener("mouseup", this._onMouseUp_Handler);
      }


      protected _onMouseDown_Cursor(event: TransformEvent): void {
            let cursor:TransformCursor = event.target as TransformCursor;
            this.selectControl = cursor;
            this._initPositionReference( event.data );
            this._removeDocumentEvent();
            this._registDocumentEvent();
            this.dispatchEvent(new TransformEvent(TransformEvent.START_TRANSFORM, this));
      }

      protected _onDoubleClick_Cursor( event:any ):void{
            this.dispatchEvent(new TransformEvent(TransformEvent.DOUBLE_CLICK, this, {select:this.target}));
      }

      protected _onMouseMove_Document(event: MouseEvent): void {
            // 매트릭 초기화
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            if( this.selectControl != null ){
                  this._updateMousePosition( event );
                  this.selectControl.onMoveHandler();
                  this._applyTransformValue();
            }
      }

      protected _registEventCursor(cursor: TransformCursor, eventType: string, handler: Function): void {
            cursor.addEventListener(eventType, handler);
      }

      protected _removeEventCursor(cursor: TransformCursor, eventType: string, handler: Function): void {
            cursor.removeEventListener(eventType, handler);
      }


      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       ITransformToolCalculator 인터페이스 구현
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       * */
      scale(sx: number, sy: number, type?: string): void {}


      /*convert(target:Point, cp:Point, rate:number):Point {
          let tempx = target.x, tempy = target.y;
          return new Point(
              (tempx - cp.x) * Math.cos(rate) - ( tempy - cp.y) * Math.sin(rate) + cp.x,
              (tempx - cp.x) * Math.sin(rate) + ( tempy - cp.y) * Math.cos(rate) + cp.y
          );
      }*/

      rotate(angle: number): void {
            /*
            let points:Array<Point> = this.option.points.concat();
            let cp:Point = this.option.cp.clone();
            let cursor:TransformCursor;
            let point:Point;
            let radianAngle:number = angle;
            for( let i:number = 0;i<this.cursors.length;i++) {
                  cursor  = this.cursors[i];
                  point   = points[i].clone();
                  if(cp.equals(point))continue;
                  point = point.transformPoint( cp, radianAngle );
                  cursor.reDraw({
                        x: point.x,
                        y: point.y
                  });
            }
            this._applyTransformValue();*/
      }

      translate(tx: number, ty: number): void {
            let cursor:TransformCursor;
            let point:Point;
            let dx:number = tx - this.target.x;
            let dy:number = ty - this.target.y;
            for( let i:number = 0;i<this.cursors.length;i++) {
                  cursor  = this.cursors[i];
                  point   = this.target.points[i];
                  cursor.reDraw({
                        x: point.x+dx,
                        y: point.y+dy
                  });
            }
            this._applyTransformValue();
      }

      protected resetTargetResource():void{
            this.target.removeEventListener( WVPointComponentEvent.INSERT_POINT, this._onChangeComponent_Handler);
            this.target.removeEventListener( WVPointComponentEvent.REMOVE_POINT, this._onChangeComponent_Handler);
            this.cursors.forEach((cursor:TransformCursor)=>{
                  this._removeEventCursor(cursor, TransformEvent.DOWN_CURSOR, this._onMouseDown_Handler );
                  cursor.clear();
            });
            this._removeEventCursor(this.translateControl, TransformEvent.DOWN_CURSOR, this._onMouseDown_Handler );

            this.translateControl.clear();
            this.translateControl = null;
            this._target.selected = false;
            this._target          = null;
      }



      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       IDrawDisplay 인터페이스 구현
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       * */
      draw(parent: any, style?: any): void {
            parent.appendChild(this._container);
      }

      clear(): void {
            this.resetTargetResource();
            $(this._container).remove();
            this._container = null;
            this.cursors = null;
            this.selectControl = null;
            this.mouseLoc   = null;
            this.startControlLoc    = null;
            this.startMouseLoc  = null;
            this.startPoints    = null;
            this.moveLoc    = null;
            this._onMouseMove_Handler   = null;
            this._onMouseUp_Handler     = null;
            this._onMouseDown_Handler   = null;
            this._onMoveTranslate_Handler   =null;
            this._onChangeComponent_Handler =null;
            this._onDoubleClick_Handler =null;
            this.option = null;
      }
}
