import WVTransformTool from "./WVTransformTool";
import TransformEvent from "./event/TransformEvent";
import {IWVTransformTool, TransformCursorType} from "./interfaces/WVTransformInterfaces";
import WVPointTransformTool from "./WVPointTransformTool";
import WV2DComponent from "../component/2D/WV2DComponent";
import {IDrawDisplay} from "../display/interfaces/DisplayInterfaces";
import {IEventDispatcher, IWVEvent} from "../events/interfaces/EventInterfaces";
import EventDispatcher from "../events/EventDispatcher";
import {CPLogger} from "../utils/CPLogger";

/**
 transformTool을 관리하는 매니저 클래스
 instance에 오는 수 만큼 transformtool을 생성하여 관리
 **/
export default class WVTransformToolMG implements IDrawDisplay, IEventDispatcher {

      addEventListener(eventType: string, handler: Function): boolean {
            return this._dispatcher.addEventListener(eventType, handler);
      }

      removeEventListener(eventType: string, handler: Function): boolean {
            return this._dispatcher.removeEventListener(eventType, handler );
      }

      dispatchEvent(event: IWVEvent, scope?: any): void {
            this._dispatcher.dispatchEvent(event);
      }

      hasEventListener(eventType: string, handler?: Function): boolean {
            return this._dispatcher.hasEventListener(eventType, handler);
      }


      private _container: HTMLElement;
      private updateTransformHandler      : Function;
      private completeTransformHandler    : Function;
      private startTransformHandler       : Function;
      private doubleClickTransformHandler : Function;
      private _dispatcher:EventDispatcher;
      get target(): Array<WV2DComponent> {
            return this._target;
      }

      set target(value: Array<WV2DComponent>) {
            if (this._target) {
                  this.reset();
            }

            if (value.length >= 1) {
                  this._target = value;
                  this.initialize();
            }
      }

      private static _instance: WVTransformToolMG;

      private constructor() {
            if (!WVTransformToolMG._instance) {
                  WVTransformToolMG._instance = this;
                  this._dispatcher = new EventDispatcher();
                  this._transforms = new Map<WV2DComponent, WVTransformTool>();
                  this.startTransformHandler = this.onStartTransform.bind(this);
                  this.updateTransformHandler = this.onUpdateTransform.bind(this);
                  this.completeTransformHandler = this.onCompleteTransform.bind(this);
                  this.doubleClickTransformHandler = this.onDoubleClickTransform.bind(this)
            }
            return WVTransformToolMG._instance;
      }

      private onDoubleClickTransform( event:TransformEvent ):void{
            this.dispatchEvent(event);
      }

      private onStartTransform(event: TransformEvent): void {
            this._transforms.forEach((tool: WVTransformTool, component: WV2DComponent) => {
                  if (event.target != tool) {
                        tool.initGroupSync();
                  }
            });
            this.dispatchEvent(new TransformEvent(TransformEvent.START_TRANSFORM, this) );
      }

      private onCompleteTransform(event: TransformEvent): void {
            this._transforms.forEach((tool: WVTransformTool, component: WV2DComponent) => {
                  if (event.target != tool) {
                        tool.toggleCursor(true);
                  }
            });
            this.dispatchEvent(new TransformEvent(TransformEvent.COMPLETE_TRANSFORM, this) );
      }

      private onUpdateTransform(event: TransformEvent): void {
            this._transforms.forEach((tool: WVTransformTool, component: WV2DComponent) => {
                  if (event.target != tool) {
                        if (event.data.type == TransformCursorType.ROTATE) {
                              if (tool instanceof WVPointTransformTool) {
                                    tool.rotate(event.data.trotation);
                              } else {
                                    tool.rotate(event.data.rotation);
                              }
                        } else if (event.data.type == TransformCursorType.TRANSLATE) {
                              tool.translate(component.x + event.data.x, component.y + event.data.y);
                        } else {
                              tool.scale(event.data.scaleX, event.data.scaleY, event.data.pivotType);
                        }
                  }
            });
            this.dispatchEvent( new TransformEvent(TransformEvent.UPDATE_TRANSFORM, this) );
      }

      public static getInstance(): WVTransformToolMG {
            return WVTransformToolMG._instance || new WVTransformToolMG();
      }

      private _target: Array<WV2DComponent>;
      private _transforms: Map<WV2DComponent, IWVTransformTool>;

      private reset(): void {
            this._transforms.forEach((tool: IWVTransformTool, component: WV2DComponent) => {
                  tool.removeEventListener(TransformEvent.START_TRANSFORM, this.startTransformHandler);
                  tool.removeEventListener(TransformEvent.UPDATE_TRANSFORM, this.updateTransformHandler);
                  tool.removeEventListener(TransformEvent.COMPLETE_TRANSFORM, this.completeTransformHandler);
                  tool.removeEventListener(TransformEvent.DOUBLE_CLICK, this.doubleClickTransformHandler);
                  tool.clear();
                  this._transforms.delete(component);
            });

      }

      private isPointDisplay(objTarget: any): boolean {
            return objTarget.points !== undefined ? true : false;
      }

      // 무조건 가상 돔을 생성하여 제어 하고 그 값을
      // 대상에 적용하는 방식으로 처리함.
      private initialize(): void {

            let tool: IWVTransformTool;
            this._target.forEach((client: WV2DComponent) => {
                  tool = this.isPointDisplay(client) ? new WVPointTransformTool() : new WVTransformTool();
                  tool.draw(this._container);
                  tool.target = client;
                  this._transforms.set(client, tool);
                  tool.addEventListener(TransformEvent.START_TRANSFORM, this.startTransformHandler );
                  tool.addEventListener(TransformEvent.UPDATE_TRANSFORM, this.updateTransformHandler);
                  tool.addEventListener(TransformEvent.COMPLETE_TRANSFORM, this.completeTransformHandler);
                  tool.addEventListener(TransformEvent.DOUBLE_CLICK, this.doubleClickTransformHandler);
            });

      }


      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       IDrawable 인터페이스 구현
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      draw(parent: any): void {

            this._container = document.createElement("div");
            $(this._container).css({
                  position: "absolute",
                  top: 0,
                  left: 0,
                  width:"100%",
                  height:"100%",
                  "z-index": "39999",
            }).addClass("transform-layer");
            $(parent).append(this._container);
      }


      clear(): void {
            $(this._container).remove();
            this._dispatcher.destroy();
            this._dispatcher = null;
            this.updateTransformHandler = null;
            this.startTransformHandler = null;
            this.completeTransformHandler = null;
            this.doubleClickTransformHandler = null;
      }

      syncFromTarget():void{
            this._transforms.forEach((tool: IWVTransformTool, component: WV2DComponent) =>{
                  tool.syncFromTarget();
            });
      }



}

