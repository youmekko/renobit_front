import Point from "./Point";

export class Matrix {

    a:number;
    b:number;
    c:number;
    d:number;
    tx:number;
    ty:number;

    static readonly temp = new Matrix();
    static readonly TO_RADIAN = Math.PI/180;
    static readonly TO_DEGREE = 180/Math.PI;

    constructor( a?:number, b?:number, c?:number, d?:number, tx?:number, ty?:number ) {
        this.a = a || 1;
        this.b = b || 0;
        this.c = c || 0;
        this.d = d || 1;
        this.tx = tx || 0;
        this.ty = ty || 0;
    }

    scale( sx:number, sy:number ) :void {
        this.a *=sx;
        this.b *=sy;
        this.c *=sx;
        this.d *=sy;
        this.tx *=sx;
        this.ty *=sy;
    }

    translate( px:number,py:number ):void {
        this.tx +=px;
        this.ty +=py;
    }

    rotateDeg( angle:number ):void {
        this.rotate( angle * Matrix.TO_RADIAN );
    }

    rotate( angle:number ):void {
        let u = Math.cos(angle);
        let v = Math.sin(angle);
        let temp = this.a;
        this.a = u * this.a - v * this.b;
        this.b = v * temp + u * this.b;
        temp = this.c;
        this.c = u * this.c - v * this.d;
        this.d = v * temp + u * this.d;
        temp = this.tx;
        this.tx = u * this.tx - v * this.ty;
        this.ty = v * temp + u * this.ty;
    }

    getRotateX():number {
        return Math.atan2(this.b, this.a);
    }

    getRotateY():number {
        return Math.atan2(this.c, this.d);
    }

    getTransformedX( px:number, py:number ):number {
        return this.tx + this.a * px + this.c * py;
    }

    getTransformedY( px:number, py:number ):number {
        return this.ty + this.d * py + this.b * px;
    }

    getScaleX(){
        return Math.sqrt( this.a*this.a + this.b * this.b );
    }

    getScaleY(){
        return Math.sqrt( this.c*this.c + this.d * this.d );
    }


    transformPoint( point:Point ):Point {
        return new Point(
            this.getTransformedX(point.x, point.y),
            this.getTransformedY(point.x, point.y)
        );
    }

    containsPoint( px:number, py:number, w:number, h:number ):boolean {
        let invert = Matrix.temp;
        invert.copyFrom(this);
        invert.invert();

        let tempx = invert.getTransformedX(px, py);
        let tempy = invert.getTransformedY(px, py);
        if( tempx >= 0 && tempx <= w && tempy >= 0 && tempy <= h ){
            return true;
        }
        return false;
    }

    invert() : void {
        if (this.b === 0 && this.c === 0 && this.a !== 0 && this.d !== 0) {

            this.a = 1/this.a;
            this.d = 1/this.d;
            this.b = 0;
            this.c = 0;
            this.tx = -this.a*this.tx;
            this.ty = -this.d*this.ty;

        }else{

            let det = this.a*this.d - this.b*this.c;
            if (det === 0) {
                this.identity();
                return;
            }
            det = 1/det;

            let temp = this.a;
            this.a = this.d * det;
            this.b = -this.b * det;
            this.c = -this.c * det;
            this.d = temp * det;

            temp = this.ty;
            this.ty = -(this.b * this.tx + this.d * this.ty);
            this.tx = -(this.a * this.tx + this.c * temp);
        }
    }


    concat( m :Matrix ):void {
        let tempA = this.a * m.a;
        let tempB = 0;
        let tempC = 0;
        let tempD = this.d * m.d;
        let tempTx = this.tx * m.a + m.tx;
        let tempTy = this.ty * m.d + m.ty;

        if( this.b !== 0 || this.c !== 0 || m.b !== 0 || m.c !== 0 ) {
            tempA += this.b * m.c;
            tempD += this.c * m.b;
            tempB += this.a * m.b + this.b * m.d;
            tempC += this.c * m.a + this.d * m.c;
            tempTx += this.ty * m.c;
            tempTy += this.tx * m.b;
        }

        this.a = tempA;
        this.b = tempB;
        this.c = tempC;
        this.d = tempD;
        this.tx = tempTx;
        this.ty = tempTy;
    }

    copyFrom( m:Matrix ):void {
        this.a = m.a;
        this.b = m.b;
        this.c = m.c;
        this.d = m.d;
        this.tx = m.tx;
        this.ty = m.ty;
    }

    equals( m:Matrix ) :boolean {
        if( this.a === m.a &&
            this.b === m.b &&
            this.c === m.c &&
            this.d === m.d &&
            this.tx === m.tx &&
            this.ty === m.ty ){
            return true;
        }
        return false;
    }

    identity() : void {
        this.a = 1;
        this.b = 0;
        this.c = 0;
        this.d = 1;
        this.tx = 0;
        this.ty = 0;
    }

    clone():Matrix {
        return new Matrix( this.a, this.b, this.c, this.d, this.tx, this.ty );
    }



    toString():string {
        return "matrix("+this.a+","+this.b+","+this.c+","+this.d+"," +this.tx+","+this.ty+")";
    }



}