
import * as GeomInterfaces from './interfaces/GeomInterfaces'

import Matrix from './Matrix'
import Point from './Point'
import Transform from './Transform'

const geom = {
    interfaces: {
        GeomInterfaces
    },
    Matrix,
    Point,
    Transform
}

export default geom