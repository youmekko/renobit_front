import {ITransform} from "./interfaces/GeomInterfaces";
import {Matrix} from "./Matrix";
import Point from "./Point";
import WV2DComponent from "../component/2D/WV2DComponent";

export default class Transform implements ITransform {

      get matrix(): Matrix {
            return this._matrix;
      }

      set matrix(value: Matrix) {
            if (!this.matrix.equals(value)) {
                  this._matrix = value;
                  this.applyTransform();
            }
      }

      reset(): void {
            this.width  = this.owner.width;
            this.height = this.owner.height;
            this.x      = this.owner.x;
            this.y      = this.owner.y;
            this.rotation = this.owner.rotation;
            this._matrix = new Matrix();
            this._matrix.rotateDeg(this.rotation);
            this._matrix.translate(this.x, this.y);
      }

      destroy(): void {
            this.owner = null;
            this.x = this.y = this.width = this.height = null;
            this._matrix = null;
      }

      get ownerScale(): Point {
            return new Point(this.owner.width / this.width, this.owner.height / this.height);
      }

      private _matrix: Matrix;
      x: number;
      y: number;
      width: number;
      height: number;
      rotation: number;
      owner: WV2DComponent;

      constructor(owner: WV2DComponent, matrix?: Matrix) {


            this.owner = owner;
            this.x = owner.x;
            this.y = owner.y;
            this.width = owner.width;
            this.height = owner.height;
            this.rotation = owner.rotation;
            this._matrix = new Matrix();
            this._matrix.rotateDeg(this.rotation);
            this._matrix.translate(this.x, this.y);
            //this._matrix = matrix || new Matrix(1, 0, 0, 1, this.x, this.y);
      }


      // 현재 매트릭 값을 target에 적용
      applyTransform(): void {
            let nWidth: number = this.matrix.getScaleX() * this.width;
            let nHeight: number = this.matrix.getScaleY() * this.height;
            let nRotate: number = this.matrix.getRotateX() * Matrix.TO_DEGREE;
            let nX: number = this.matrix.tx;
            let nY: number = this.matrix.ty;
            this.owner.x = nX;
            this.owner.y = nY;
            this.owner.width = nWidth;
            this.owner.height = nHeight;
            this.owner.rotation = nRotate;
            this.owner.invalidateImmediateProperties();
      }

}
