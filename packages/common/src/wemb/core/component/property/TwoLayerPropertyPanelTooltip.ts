import TooltipGenerator from "../../utils/TooltipGenerator";

export default class TwoLayerPropertyPanelTooltip {
      private static twoLayerPropertyPanelTooltip: TwoLayerPropertyPanelTooltip;
      private constructor() {
      }

      public static getInstance() {
            if (!this.twoLayerPropertyPanelTooltip) {
                  this.twoLayerPropertyPanelTooltip = new TwoLayerPropertyPanelTooltip();
            }
            return this.twoLayerPropertyPanelTooltip;
      }

      private readonly TOOLTIP_DIRECTION_STYLE = {
            left : 'linear-gradient( 90deg',
            top : 'linear-gradient( 180deg',
            diagonal1 : 'linear-gradient( -45deg',
            diagonal2 : 'linear-gradient( 45deg',
            radial : 'radial-gradient(ellipse at center'
      }

      private usedTooltip(tooltip) {
            return tooltip && tooltip.description;
      }

      private getDrawingBackgroundMethod(type: String):Function {
            if (type === 'solid') {
                  return this.drawBackgroundWithSolid;
            } else if (type === 'gradient') {
                  return this.drawBackgroundWithGradient;
            } else {
                  return this.drawBackgroundWithCss;
            }
      }

      private drawBackgroundWithSolid(tooltip) {
            return {
                  backgroundKey: 'background-color',
                  backgroundStyle: `${tooltip.color1} !important`
            }
      }

      private drawBackgroundWithGradient(tooltip) {
            return {
                  backgroundKey: 'background',
                  backgroundStyle: `${this.TOOLTIP_DIRECTION_STYLE[tooltip.direction]}, ${tooltip.color1} 0%, ${tooltip.color2} 100%)`
            }
      }

      private drawBackgroundWithCss(tooltip) {
            return {
                  backgroundKey: 'background',
                  backgroundStyle: tooltip.text
            }
      }

      public createTooltip(component) {
            let tooltip = component.properties.tooltip;

            this.usedTooltip(tooltip) &&
                  TooltipGenerator.getInstance().initTooltip(component, tooltip.description, this.using(this, tooltip));
      }

      private using(self, tooltip) {
            return function(position) {
                  let {backgroundKey, backgroundStyle} = self.getDrawingBackgroundMethod(tooltip.type).call(self, tooltip);
                  let css = Object.assign({}, position, {
                        "border-radius": `${tooltip.border_radius}`,
                        "z-index": '40001',
                        border: `${tooltip.border} !important`,
                        [backgroundKey]: backgroundStyle,
                        "font-family": `${tooltip.font_type}`,
                        "color": `${tooltip.font_color}`,
                        "font-weight": `${tooltip.font_weight}`,
                        "font-size": `${tooltip.font_size}px`,
                        "line-height": `${tooltip.line_height}px`,
                        "text-align": `${tooltip.text_align}`
                  });

                  $(this).css(css);
            }
      }
}
