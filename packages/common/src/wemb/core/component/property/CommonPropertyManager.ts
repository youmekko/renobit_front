import WVComponent from "../WVComponent";

export default class CommonPropertyManager {
      private static commonPropertyManager:CommonPropertyManager;
      private readonly commonPropertyList = [
            {
                  label: 'tooltip',
                  template: 'tooltip'
            }
      ]

      public static getInstance() {
            if (!this.commonPropertyManager) {
                  this.commonPropertyManager = new CommonPropertyManager();
            }
            return this.commonPropertyManager;
      }

      public getDefaultProperty(component: WVComponent):Array<Object> {
            if (component.category === "3D") {
                  return [];
            }

            return this.commonPropertyList;
      }
}



