
import WVComponent from './WVComponent'
import WVComponentPropertyManager from './WVComponentPropertyManager'
import WVComponentPropertyManagerCore from './WVComponentPropertyManagerCore'

import WV2DComponent from './2D/WV2DComponent'
import WVDOMComponent from './2D/WVDOMComponent'
import WVPointComponent from './2D/WVPointComponent'
import WVSeverityComponent from './2D/WVSeverityComponent'
import WVSVGComponent from './2D/WVSVGComponent'
import WVSVGStreamLineComponent from './2D/WVSVGStreamLineComponent'

import WVDataGrid from './2D/drawable/WVDataGrid'
import WVDOMComponentStyleManager from './2D/manager/WVDOMComponentStyleManager'
import WVSVGComponentStyleManager from './2D/manager/WVSVGComponentStyleManager'

import WV3DIconLabel from './3D/drawable/WV3DIconLabel'
import WV3DLabel from './3D/drawable/WV3DLabel'
import WV3DSprite from './3D/drawable/WV3DSprite'
import WVMeshDecorator from './3D/drawable/WVMeshDecorator'

import LoaderManager from './3D/manager/LoaderManager'
import MeshManager from './3D/manager/MeshManager'
import NLoaderManager from './3D/manager/NLoaderManager'
import WV3DComponentPropertyManager from './3D/manager/WV3DComponentPropertyManager'
import WV3DComponentStyleManager from './3D/manager/WV3DComponentStyleManager'
import WVToolTipManager from './3D/manager/WVToolTipManager'

import WV3DInstanceProxy from './3D/proxy/WV3DInstanceProxy'

import WVSelectTransitionProvider from './3D/transition/WVSelectTransitionProvider'

import DirectionViewScene from './3D/DirectionViewScene'
import NWV3DComponent from './3D/NWV3DComponent'
import OrbitDecorator from './3D/OrbitDecorator'
import PersonViewControls from './3D/PersonViewControls'
import TopViewPanel from './3D/TopViewPanel'
import WV3DComponent from './3D/WV3DComponent'
import WV3DResourceComponent from './3D/WV3DResourceComponent'

import * as ComponentInterfaces from './interfaces/ComponentInterfaces'
import * as TransitionInterfaces from './interfaces/TransitionInterfaces'

import CommonPropertyManager from './property/CommonPropertyManager'
import TwoLayerPropertyPanelTooltip from './property/TwoLayerPropertyPanelTooltip'



const component = {
    two: {
        drawable: {
            WVComponent
        },
        manager: {
            WVComponentPropertyManager,
            WVComponentPropertyManagerCore
        },
        WV2DComponent,
        WVDOMComponent,
        WVPointComponent,
        WVSeverityComponent,
        WVSVGComponent,
        WVSVGStreamLineComponent
    },
    three: {
        drawable: {
            WV3DIconLabel,
            WV3DLabel,
            WV3DSprite,
            WVMeshDecorator
        },
        manager: {
            LoaderManager,
            MeshManager,
            NLoaderManager,
            WV3DComponentPropertyManager,
            WV3DComponentStyleManager,
            WVToolTipManager
        },
        proxy: {
            WV3DInstanceProxy
        },
        transition: {
            WVSelectTransitionProvider
        },
        DirectionViewScene,
        NWV3DComponent,
        OrbitDecorator,
        PersonViewControls,
        TopViewPanel,
        WV3DComponent,
        WV3DResourceComponent,
    },
    interfaces:{
        ComponentInterfaces,
        TransitionInterfaces
    },
    property: {
        CommonPropertyManager,
        TwoLayerPropertyPanelTooltip
    },
    WVDataGrid,
    WVDOMComponentStyleManager,
    WVSVGComponentStyleManager
}

export default component