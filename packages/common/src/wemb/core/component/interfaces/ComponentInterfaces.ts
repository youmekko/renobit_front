import {IMetaProperties, IWVDisplayObject} from "../../display/interfaces/DisplayInterfaces";
import {Rectangle} from "../../geom/interfaces/GeomInterfaces";
import Vue from "vue";
import {IEditorPageObject, IViewerPageObject} from "../../../wv/managers/InstanceManagerOnPageViewer";



export interface IExtensionElements {

}

export interface IThreeExtensionElements extends IExtensionElements{
      scene:any;
      camera:any;
      renderer:any;
      mainControls:any;
      personControls:any;
      raycaster:any;
      domEvents:any;
      /*comInstanceList:any;*/
      labelList:any;
      threeLayer:any;
      /*transformControls:any;*/
}

export interface IWVComponentProperties {
      primary: {
            id: string,
            name: string,
            type: string,
            master: string,
            updateDt: string,
            lastUser: string,
            layerName: string
      },

      setter?: {
            x?: number,
            y?: number,
            width?: number,
            height?: number,

            depth?: number,
            visible?: boolean
      },
      style?: {},
      info?: {
            componentName?: string,
            category?: string,
            version?: string
      }

}


export interface IComponentMetaProperties extends IMetaProperties {
      componentName: string,
      version: string,
      category: string,

      id: string,
      name: string,
      type: string,
      master: string,
      updateDt: string,
      lastUser: string,
      layerName: string,
      label:string,
      props?: any

}


/*
프로퍼티 패널을 통해 수정할때 사용하는 인터페이스
2018.02.08
ddandongne
*/
export interface IComponentPropertyEditable {
      getEditableProperties(): any;      // 수정가능 속성과 값이 담긴 오브젝트 객체
      getDefaultProperties(): any;

      getExtensionProperties(): any;

      getPropertyPanelDescription(): Array<any>;              // 프로퍼티 패널에서 사용하는 정보(속성 포맷 등등이 포함)
      getMethodDescription(): Array<any>;

      getEventDescription(): Array<any>;
}

////////////////////////////////






////////////////////////////////
// 리소스 상태 정보
export enum RESOURCE_STATE {
      READY="ready",
      LOADING="loading",
      LOADED="loaded"

}
export interface IResourceComponentAble {
      readonly isResourceComponent:boolean;     // 리소스 컴포넌트 유무
      readonly resourceState:RESOURCE_STATE;
      readonly resourceBus:Vue;
      setResourceBus($resourceBus:Vue);
      startLoadResource();
      stopLoadResource();

}


export interface IWVComponent extends IComponentPropertyEditable, IWVDisplayObject, IResourceComponentAble, Object {
      readonly id: string;
      readonly name: string;
      readonly componentName: string;
      readonly category: any;            // 카테고리
      readonly styleManager: IStyleManager;

      lock:boolean;
      layerName:string;
      selected:boolean;
      visible:boolean;

      // editor/viewer 실행 모드 유무
      exeMode:string;


      readonly  isEditorMode:boolean;
      readonly  isViewerMode:boolean;

      // 뷰어 모드에서 2d,3d(카메라 등등) 외부 요소를 주입받게 될때 사용.
      readonly extensionElements:IExtensionElements;
      setExtensionElements(extension:IExtensionElements);




      // viewer모드에서만 사용, InstanceManagerOnPageViewer.ts에서 주입됨.
      page:IViewerPageObject|IEditorPageObject;
      isCompleted:boolean;

      createStyleManager(): IStyleManager;

      serializeMetaProperties():IComponentMetaProperties;

      /*
      2018.09.12
            viewer에서만 사용.
       */
      onWScriptEvent(eventName:string, func:Function);
      offWScriptEvent(eventName:string, func:Function);
      emitWScriptEvent(eventName:string, data:any);
      offWScriptEventAll();
}

/**
 * component에서 아래 인터페이스 구현시 페이지가 로드완료했을때 해당 메소드가 실행된다.
 * grid, chart같이 페이지 시작시 dataService를 실행해야하는 컴포넌트에서 사용하게 된다.
 */
export interface OnLoadPage{
      onLoadPage():void;
}

export interface IWV2DComponent extends IWVComponent {
      x: number;
      y: number;
      width: number;
      height: number;
      rotation: number;

      readonly clientRect: Rectangle;    // 디스플레이 영역 정보
}


export interface IWVDOMComponent extends IWV2DComponent {

}


export interface IWVSVGComponent extends IWV2DComponent {

}


export interface IWV3DComponent extends IWVComponent {
      readonly usingRenderer:boolean;
      render():void;

      position: IWVVector;
      size: IWVVector;
      rotation: IWVVector;
}


export interface IStyleManager {
      invalidateProperty: boolean;
      immediateProperties(properties:any):void;
      setProperty(propertyName: string, value: any): boolean;

      getProperty(propertyName: string): any;

      setProperties(properties: any, override?: boolean): boolean;

      getProperties(): any;

      validateProperty(): void;

      clearInvalidate(): void;

      destroy():void;
}


export interface IWVDOMComponentStyleManager extends IStyleManager {
      addClass(className: string): void;

      removeClass(className: string): void;

      hasClass(className: string): boolean;

}

export interface IWVSVGComponentStyleManager extends IStyleManager {

}

export interface IWV3DComponentStyleManager extends IStyleManager {
}


export interface IWVVector {
      x: number;
      y: number;
      z: number;
}




export interface IBackgroundElement {
      setBackgroundPropertyValue(propertyName:string, value:any);
}





/**
 * 흐름선 관련 인터페이스 시작
 * */
export namespace StreamLineDirection {
      export const LEFT:string = "left";
      export const RIGHT:string = "right";
      export const BI_DIRECTIONAL:string = "biDirectional"
}

export namespace StreamLineState {
      export const NORMAL:string      = "normal";
      export const MINOR:string       = "minor";
      export const MAJOR:string       = "major";
      export const WARNING:string     = "warning";
      export const CRITICAL:string    = "critical"
}


export type StreamTweenVO = {
      value:number
      from:number
      to:number
      duration:number
      streamSize:number
}

export type StreamLineVO = {
      tweenInfo:StreamTweenVO
      stateInfo:StreamStateVO
}

export type StreamStateVO = {
      state:string
      direction:string
      normal:string
      warning:string
      critical:string
      minor:string
      major:string
}

export interface IStreamTweenAble {
      play():void
      stop():void
      render():void
}

export interface IWVStreamLine {
      readonly streamPath:SVGGraphicsElement
      streamVO:StreamLineVO
}




/*
WV3DComponent에서
$on() 메서드를 통해 이벤트 등록시
추후 제거하기 위해
이벤트 정보를 저장할때 사용.
 */
export type EventVO ={
      eventName:string,
      listener:Function
}





export enum WVCOMPONENT_PROPERTY_NAMES {
      ID                ="id",
      NAME              ="name",
      COMPONENT_NAME    ="componentName",
      LAYER_NAME        ="layerName",
      CATEGORY          ="category",
      X                 ="x",
      Y                 ="y",
      WIDTH             ="width",
      HEIGHT            ="height",
      LOCK              ="lock",

}

export enum WVCOMPONENT_EDITOR_POPERTY_NAMES {
      VISIBLE    ="visible",
      LOCK              ="lock"
}

export enum WVDISPLAY_PROPERTIES {
      OLD_X       = "prevX",
      OLD_Y       = "prevY",
      OLD_WIDTH   = "prevW",
      OLD_HEIGHT  = "prevH",
      OLD_ROTATION= "prevR",
      X           = "x",
      Y           = "y",
      WIDTH       = "width",
      HEIGHT      = "height",
      ROTATION    = "rotation",
      Z_INDEX     = "z_index",
      VISIBLE     = "visible",
      DEPTH       = "depth",
      PREVIEW_MODE   = "preview",
      BORDER_RADIUS = "borderRadius"
}

export enum WVSVG_PROPERTIES {
      POINT_COUNT       = "pointCount",
      START_MARKER      = "startMarker",
      END_MARKER        = "endMarker",
      FILL              = "fill",
      FILL_OPACITY      = "fill-opacity",
      STROKE            = "stroke",
      STROKE_WIDTH      = "stroke-width",
      STROKE_OPACITY    = "stroke-opacity",
      STROKE_DASHARRAY  = "stroke-dasharray",
      D                 = "d"
}

export enum WV3D_PROPERTIES {
      OLD_X       = "prevX",
      OLD_Y       = "prevY",
      OLD_WIDTH   = "prevW",
      OLD_HEIGHT  = "prevH",
      OLD_ROTATION= "prevR",
      POSITION    = "position",
      ROTATION    = "rotation",
      SIZE        = "size",
      VISIBLE     = "visible"
}


export enum WVCOMPONENT_CATEGORY {
      TWO         = "2D",
      THREE       ="3D",
      GROUP       ="GROUP",
      PAGE        ="PAGE",
}
export enum PRIMARY_WVCOMPONENT {
      VIRTUAL_GROUP_COMPONENT_NAME ="VirutalGroupComponent",
	PAGE_COMPONENT_NAME = "EditorPageComponent"

}

export enum WVCOMPONENT_METHOD_INFO {
      ON_LAOAD_PAGE="onLoadPage",

      ON_OPEN_PAGE="onOpenPage" // 마스터 레이어에 배치되어 있는 컴포넌트의 메서드 호출
}



