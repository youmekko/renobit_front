// 3d 포커스 요소를 위한 인터페이스
import {IEventDispatcher} from "../../events/interfaces/EventInterfaces";
import {IWVComponent} from "./ComponentInterfaces";

namespace WV {
      export namespace Transition {

            export interface ISelectTransitionProvider {
                  transitionInFromInstance( instance:ISelectTransitionProviderClient ):void  // 포커스 인   처리
                  transitionOut():void
                  registEventListener():void
                  removeEventListener():void
            }

            export interface ISelectTransitionProviderClient extends IEventDispatcher, Object {
                  transitionIn(data?:any):void
                  transitionOut(data?:any):void
                  selected:boolean;
                  transitionTarget:any;
            }
      }

      export namespace Core {
            export interface IDataRenderer {
                  data( data:any )
                  data():any
            }
      }
      
      export namespace Provider {
            export interface IToolTipClient extends IWVComponent {
                  toolTipMessage:string;
                  toolTipTarget:any;
                  useToolTip:boolean;
            }
            export interface IToolTipProvider {
                  add( client:IToolTipClient ):boolean
                  remove( client:IToolTipClient ):boolean
            }
      }

}
