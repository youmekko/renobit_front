import {$} from "../../../utils/reference";
import CPUtil from "../../../utils/CPUtil";

export default class WVDataGrid {

      protected _dataProvider:any;
      protected _columnProvider:any;
      protected _grid:any;
      protected _header:any;
      protected _headerTable:any;
      protected _body:any;
      protected _bodyTable:any;
      protected _bodyContainer:any;
      protected _enabled:boolean;
      protected _headerItemClickHandler:Function;
      protected _headerItemMouseOverHandler:Function;
      protected _headerItemMouseOutHandler:Function;
      protected _gridItemClickHandler:Function;
      protected _gridItemMouseOverHandler:Function;
      protected _gridItemMouseOutHandler:Function;


      protected _headerList:any;
      //protected _gridRowList:any;
      protected _parent:any = null;
      protected _rowHeight:number;
      protected _lastRenderPos:number;
      protected _rowCount:number = null;


      constructor(){
            this._enabled                    = true;
            this._headerItemClickHandler     = this._onClickHeaderItem.bind(this);
            this._headerItemMouseOverHandler = this._onMouseOverHeaderItem.bind(this);
            this._headerItemMouseOutHandler  = this._onMouseOutHeaderItem.bind(this);

            this._gridItemClickHandler       = this._onClickGridItem.bind(this);
            this._gridItemMouseOverHandler   = this._onMouseOverGridItem.bind(this);
            this._gridItemMouseOutHandler    = this._onMouseOutGridItem.bind(this);
            this._rowHeight                  = 0;
            //this._gridRowList                = [];
            this._rowCount                   = 14;
            this._lastRenderPos              = -1;
      }


      get columnProvider():any{
            return this._columnProvider;
      }

      set columnProvider( objProvider:any ){
            this._columnProvider = objProvider;
            if(this._parent){
                  this._updateGridHeaderDisplay();
            }
      }

      get dataProvider():any{
            return this._dataProvider;
      }

      set dataProvider( objProvider:any ){
            this._dataProvider = objProvider;
            this._lastRenderPos = -1;
            if(this._parent){
                  this._updateGridDataDisplay();
            }
      }





      _createHeader():string{
            let tempalte:string = '<div class="grid-header">'+
                  '<div class="content-container">'+
                  '<table><thead></thead></table>'+
                  '</div>'+
                  '</div>';
            return $(tempalte);
      }


      _createBody():string{
            let tempalte:string = '<div class="grid-body">'+
                  '<div class="content-container">'+
                  '<table><tbody></tbody></table>'+
                  '</div>'+
                  '</div>';
            return $(tempalte);
      }

      _getHeaderContainer():string{
            let template:string = '<th class="grid-header-cell grid-cell"></th >';
            return template;
      }



      _getGridRowContainer():string{
            let template = '<tr class="grid-row"></tr>';
            return template;
      }


      _getGridColumnItem():string{
            let template = '<td class="grid-cell"></td>';
            return template;
      }


      _getDefaultHeaderRenderer( columnLabel:string, parent:any, vo:any, context:any ){
            let template = '<div class="column-indicators"><i class="fa fa-sort-up sort-none"></i></div>'+
                  '<div class="text-content">'+vo[columnLabel]+'</div>';
            return template;
      }



      _getDefaultItemRenderer( columnLabel:string, parent:any, vo:any, context:any ){
            let template = '<div class="text-content">'+vo[columnLabel]+'</div>';
            return template;
      }


      draw( parent:any ) {
            this._grid = $('<div class="wv-grid">');
            $(parent).append(this._grid);
            this._header = this._createHeader();
            this._body	= this._createBody();
            this._grid.append(this._header);
            this._grid.append(this._body);
            this._headerTable = this._header.find("table");
            this._bodyTable = this._body.find("table");
            this._bodyContainer = this._body.find(".content-container");
            this._parent = parent;

            if(this.columnProvider == null ){
                  console.warn("columnProvider 설정이 필요합니다.");
                  return;
            }

            if(this.columnProvider && this.columnProvider.length > 0 ) {
                  this._updateGridHeaderDisplay();
            }


            if(this.dataProvider && this._dataProvider.length > 0) {
                  this._updateGridDataDisplay();
            }

            this._validateEnabled();
            this._registEvent();
      }




      protected _createHeaderItemEvent(target:any, eventName:string):any{
            let columnIndex=  target.data("columnIndex");
            let customEvent = $.Event(eventName);
            customEvent.columIndex = columnIndex;
            customEvent.selectData	  = this._columnProvider[columnIndex];
            return customEvent;
      }

      protected _createGridItemEvent(target:any, eventName:string):any{
            let columnIndex =  target.data("columnIndex");
            let customEvent = $.Event(eventName);
            let index 	   = target.parent().data("index");
            customEvent.columnIndex      = columnIndex;
            customEvent.rowIndex         = index;
            customEvent.selectData	     = this.dataProvider[index];
            customEvent.selectRow        = target.parent();
            return customEvent;
      }

      protected _onMouseOverGridItem(event:any){
            let target	 = $(event.currentTarget);
            let customEvent = this._createGridItemEvent(target, "wvGridItemMouseOver");
            $(this).trigger(customEvent);
      }
      protected _onMouseOutGridItem(event:any){
            let target	 = $(event.currentTarget);
            let customEvent = this._createGridItemEvent(target, "wvGridItemMouseOut");
            $(this).trigger(customEvent);
      }

      protected _onClickGridItem(event:any){
            let target	 = $(event.currentTarget);
            let customEvent = this._createGridItemEvent(target, "wvGridItemClick");
            $(this).trigger(customEvent);
      }

      protected _onMouseOverHeaderItem(event:any){
            let target	 = $(event.currentTarget);
            let customEvent = this._createHeaderItemEvent(target, "wvGridHeaderMouseOver");
            $(this).trigger(customEvent);
      }
      protected _onMouseOutHeaderItem(event:any){
            let target	 = $(event.currentTarget);
            let customEvent = this._createHeaderItemEvent(target, "wvGridHeaderMouseOut");
            $(this).trigger(customEvent);
      }

      protected _onClickHeaderItem(event:any){
            let target	 = $(event.currentTarget);
            let customEvent = this._createHeaderItemEvent(target, "wvGridHeaderClick");
            $(this).trigger(customEvent);
      }





      // grid 크기는 고정이고 내부 item의 크기 변경 시 호출 
      _validateGridHeaderDisplay(){
            let headerSize 	= this._headerTable.width();
            let baseSize	= $(this._grid.parent()).width();
            if( headerSize < baseSize ) {
                  let diff = (baseSize - headerSize);
                  let lastHeader	= this._headerList.last();
                  let itemWidth 	= lastHeader.outerWidth();
                  let value = itemWidth+diff;
                  this._updateGridItemWidth(lastHeader, value);
            }
            this._resizeGridRowDisplay();
      }


      getRowItemAtIndex( index:number ){
            let rows = this.getGridRowItems();
            let item = rows.filter(( idx, row )=>{
                  return ($(row).data("index") == index);
            });
            if(item.length > 0){ return item[0]; }
            return null;
      }


      // grid크기에 맞춘 디스플레이 변경
      getGridRowItems(){
            return this._bodyTable.find(".grid-row");
      }

      _validateItemRenderer(){

            let scrollPos:number = this._bodyContainer.scrollTop();
            if(this._lastRenderPos != scrollPos && !isNaN(this._rowHeight)  && this._rowHeight != 0 ){
                  this._lastRenderPos =  scrollPos;
                  let rowHeight:number = this._rowHeight;
                  let originRow:number  = Math.floor(scrollPos/rowHeight);
                  let dummyItem:number = Math.floor(this._rowCount/2);
                  let startRow:number  = originRow - dummyItem;
                  if(startRow<0){
                        startRow =0;
                  }
                  let endRow:number    = startRow+(this._rowCount+dummyItem*2);
                  this._clearGridItemByContainer(this._body, "tbody");
                  this._createGridItemRowRange(startRow, endRow);
                  this._resizeGridRowDisplay();
                  this._bodyTable.css({ "position":"relative",  top: startRow*rowHeight+"px"});
            }

      }

      setScrollTopByIndex( idx:number ) {
            let scrollPos:number = this._rowHeight*idx;
            let originRow:number  = Math.floor(scrollPos/this._rowHeight);
            let dummyItem:number = Math.floor(this._rowCount/2);
            let startRow:number  = originRow - dummyItem;
            if(startRow<0){
                  startRow =0;
            }
            this._bodyContainer.scrollTop( startRow*this._rowHeight);
            this._validateItemRenderer();
      }


      _createGridItemRowRange( startRow:number, endRow:number ){
            if( endRow > this.dataProvider.length ) endRow = this.dataProvider.length;
            let rendererContainer = this._body.find("tbody");
            let vo,itemRenderer;
            let fragment = document.createDocumentFragment();
            for( let i=startRow; i<endRow; i++ ) {
                  vo = this.dataProvider[i];
                  itemRenderer = this._createItemRenderer(vo);
                  itemRenderer.data("index", i );
                  itemRenderer.data("vo", vo );
                  itemRenderer.attr({"row-index":i});
                  $(fragment).append(itemRenderer);
            }
            rendererContainer.append($(fragment));
      }


      _resizeGridRowDisplay() {
            $(this.getGridRowItems()).each((idx, row)=>{
                  let rowItems = $(row).children();
                  let maxCol = rowItems.length;
                  for( let i=0; i<maxCol; i++){
                        let w = $(this._headerList[i]).outerWidth();
                        this._updateGridItemWidth(rowItems[i], w );
                  }
            });
      }

      _createVirtualScroller(){
            let virtualContents = $('<div class="vitual-contents"></div>');
            virtualContents.css({ position:"absolute",  width:1+"px",  top:0,  "z-index":-1 });
            return virtualContents;
      }


      _calculateScrollProperties(){

            //this._gridRowList = this.getGridRowItems();
            //this._rowHeight = parseInt(this._gridRowList.height());

            let rowListHeight = this._rowHeight * this._dataProvider.length;
            let virtualContents  = this._bodyContainer.find(".vitual-contents");
            if( virtualContents.length <= 0 ){
                  virtualContents = this._createVirtualScroller();
                  this._bodyContainer.append(virtualContents);
            }
            virtualContents.css("height", rowListHeight);
            this._rowCount =Math.ceil(this.getScrollHeight()/this._rowHeight);
      }


      // dataProvider를 이용한 뷰 갱신
      _updateGridDataDisplay(){
            this._clearGridItemByContainer(this._body, "tbody");
            // rowHeight를 위한 itemRenderer생성
            if(this._rowHeight == 0 || isNaN(this._rowHeight)){
                  this._createGridItemRowRange(0, 1);
                  this._rowHeight = parseInt(this.getGridRowItems().height());
            }
            this._calculateScrollProperties();
            this._validateItemRenderer();
      }


      // 이전 목록 제거
      _clearGridItemByContainer( container , selector){
            let rendererContainer = container.find(selector);
            let itemRendererList = this._getGridCellFromContainer( container );
            itemRendererList.each(( renderer )=>{
                  $(renderer).removeData();
                  $(renderer).off();
                  $(renderer).remove();
            });
            rendererContainer.empty();
      }

      _createGridItemFromDataProvider(){
            let rendererContainer = this._body.find("tbody");
            let vo,itemRenderer;
            let fragment = document.createDocumentFragment();
            for( let i=0; i<1; i++ ) {
                  vo = this.dataProvider[i];
                  itemRenderer = this._createItemRenderer(vo);
                  itemRenderer.data("index", i );
                  $(fragment).append(itemRenderer);
            }
            rendererContainer.append($(fragment));
      }



      _updateGridItemWidth( gridItem, value ){
            if(value != 0){
                  $(gridItem).css({
                        width:value+"px",
                        "max-width":value+"px",
                        "min-width":value+"px"
                  });
            }
      }




      _createItemRenderer( vo:any ){
            let container = $(this._getGridRowContainer());
            let gridColumn, itemRenderer, columnVO, strField;
            let headerRenderer, maxCols = this.columnProvider.length;
            for( let j=0; j<maxCols; j++ ) {
                  columnVO 		= this.columnProvider[j];
                  headerRenderer	= $(this._headerList[j]);
                  gridColumn		= $(this._getGridColumnItem());
                  strField 		= columnVO.field || columnVO.label;
                  let buildFunc	= this._getDefaultItemRenderer;
                  if(columnVO.itemRenderer){
                        buildFunc = columnVO.itemRenderer;
                  }
                  gridColumn.addClass('grid-cell-'+columnVO.field);
                  itemRenderer = $(buildFunc.apply(this, [strField, container, vo, this ] ));
                  gridColumn.append(itemRenderer);
                  gridColumn.data("columnIndex", j);
                  this.registEventByName( gridColumn, "click", this._gridItemClickHandler );
                  this.registEventByName( gridColumn, "mouseover", this._gridItemMouseOverHandler );
                  this.registEventByName( gridColumn, "mouseout", this._gridItemMouseOutHandler );
                  container.append(gridColumn);
            }
            return container;
      }


      registEventByName( target, eventName, handler ){
            $(target).on(eventName, handler);
      }

      _updateGridHeaderDisplay(){
            this._clearGridItemByContainer( this._header, "thead" );
            this._createGridHeader();
            this._headerList = this._getGridCellFromContainer(this._header);
            this._validateGridHeaderDisplay();
      }

      _createGridHeader(){
            let rendererContainer = this._header.find("thead");
            for( let i=0; i<this.columnProvider.length; i++ ) {
                  let vo = this.columnProvider[i];
                  let container = $(this._getHeaderContainer());
                  container.addClass('grid-header-cell-'+vo.field);
                  rendererContainer.append(container);

                  let itemRenderer;
                  if(vo.headerRenderer){
                        itemRenderer = $( vo.headerRenderer.apply( this, ["label", container, vo, this] ) );
                  } else {
                        itemRenderer = $( this._getDefaultHeaderRenderer( "label", container, vo, this ) );
                  }
                  container.append(itemRenderer);
                  container.data("columnIndex", i);
                  this.registEventByName( container, "click", this._headerItemClickHandler );
                  this.registEventByName( container, "mouseover", this._headerItemMouseOverHandler );
                  this.registEventByName( container, "mouseout", this._headerItemMouseOutHandler );
                  if(vo.css){
                        container.css(vo.css);
                        if(vo.css.width){
                              this._updateGridItemWidth(container, vo.css.width);
                        }
                  }
            }
      }

      _getGridCellFromContainer( container ){
            return container.find(".grid-cell");
      }

      invalidateGridBodyScrollPosition( event ){
            let headerContents = this._header.find(".content-container");
            headerContents.scrollLeft(event.target.scrollLeft);
            this._validateItemRenderer();
      }

      // 설정 정보로 화면 구성
      _registEvent() {


            this._bodyContainer.scrollbar();

            let callBack = this.invalidateGridBodyScrollPosition.bind(this);
            this._bodyContainer.on("scroll", CPUtil.getInstance().debounce(callBack,50) );

            this._headerList.resizable({
                  handles:"e",
                  minWidth:30,
                  resize:(event:any, ui:any)=>{
                        this._updateGridItemWidth(event.target, ui.size.width);
                  },
                  stop:(event:any, ui:any)=>{
                        this._updateGridItemWidth(event.target, ui.size.width);
                        this._validateGridHeaderDisplay();
                  }
            });
      }


      _validateGridBodySize(){
            let headerHeight = this._header.outerHeight();
            // grid의 height값은 css에 정의됨.
            let gridHeight	 = $(this._grid.parent()).height();
            let h              = gridHeight-headerHeight;
            this._bodyContainer.parent().css({height:h+"px"});
      }


      getScrollHeight(){
            let scrollHeight = this._bodyContainer.parent().height();
            if(isNaN(scrollHeight) || scrollHeight == 0 ){
                  this._validateGridBodySize();
                  scrollHeight = this._bodyContainer.parent().height();
            }
            return scrollHeight;
      }


      _validateSize(){
            this._validateGridBodySize();
            this._validateGridHeaderDisplay();
            this._updateGridDataDisplay();
      }

      _validateEnabled(){
            if(this._grid){
                  if(this.enabled){
                        this._grid.removeClass("disabled");
                  } else {
                        this._grid.addClass("disabled");
                  }
            }
      }

      set enabled( value ){
            this._enabled = value;
            this._validateEnabled();
      }

      get enabled(){
            return this._enabled;
      }

      destroy(){
            this._clearGridItemByContainer(this._header, "thead");
            this._clearGridItemByContainer(this._body, "tbody" );
            //this._bodyContainer.scrollbar("destroy");
            this._bodyContainer.off();
            this._bodyContainer.empty();
            this._headerTable = null;
            this._bodyTable  = null;
            this._headerList = null;
            this._header     = null;
            this._body       = null;
            this._dataProvider = null;
            this._columnProvider    = null;
            this._headerItemClickHandler = null;
            this._gridItemClickHandler   = null;
            this._grid.off();
            this._grid.remove();
            this._grid = null;
            this._parent = null;
            $(this).off();
      }
      
      
}
