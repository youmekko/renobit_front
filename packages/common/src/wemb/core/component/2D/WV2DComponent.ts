import WVComponent, {GROUP_NAME_LIST} from "../WVComponent";
import {ITransformClient, Rectangle} from "../../geom/interfaces/GeomInterfaces";
import {WVDISPLAY_PROPERTIES} from "../interfaces/ComponentInterfaces";
import {TransformCursorType} from "../../freeTransform/interfaces/WVTransformInterfaces";
import Transform from "../../geom/Transform";
import TwoLayerPropertyPanelTooltip from "../property/TwoLayerPropertyPanelTooltip";

export default abstract class WV2DComponent extends WVComponent implements ITransformClient{




      get clientRect():Rectangle {
            let info = {x: this.x, y: this.y, width: this.width, height: this.height};
            return info;
      }

      get x(){
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.X);
      }
      set x(value:number){
            if(this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.X, value)){
                  this.invalidatePosition=true;
            }
      }

      get y(){
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.Y);
      }

      set y(value:number){
            if(this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.Y, value)){
                  this.invalidatePosition=true;
            }
      }



      get width(){
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.WIDTH);
      }
      set width(value:number){
            if(this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.WIDTH, value)){
                  this.invalidateSize=true;
            }
      }

      get height(){
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.HEIGHT);
      }
      set height(value:number){
            if(this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.HEIGHT, value)){
                  this.invalidateSize=true;
            }
      }

      get borderRadius(){
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.BORDER_RADIUS);
      }

      set borderRadius(value:any){
            if(this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.BORDER_RADIUS, value)){
                  this.invalidateSize = true
            }
      }

      protected invalidateDepth:boolean;
      get depth():number{
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.DEPTH);
      }
      set depth(value:number){
            if(this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.DEPTH, value)){
                  this.invalidateDepth = true;
            }
      }

      protected invalidateRotation:boolean;
      get rotation():number {
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.ROTATION);
      }
      set rotation(value:number){
            if(this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.ROTATION, value)){
                  this.invalidateRotation = true;
            }
      }

	constructor() {
		super();
	}

      protected _onDestroy(){

            if(this.appendElement) {
                  let $appendElement = $(this.appendElement);
                  $appendElement.off();
                  $appendElement.remove();

            }

            if(this._transform != null ){
                  this._transform.destroy(); // transform초기화
                  this._transform = null;
            }

            if(this.element) {
                  let $el = $(this.element);
                  $el.off();
                  $el.remove(); // 2d엘리먼트 제거 추가
                  $el = null;

                  this._element = null;
            }

            super._onDestroy();
      }



      public $on(eventName:string, func:Function) {
            if(this.appendElement){
                  $(this.appendElement).on(eventName,func);

            }
      }


      public $off(eventName:string, func:Function) {
            if(this.appendElement){
                  $(this.appendElement).off(eventName,func);
            }
      }



      protected _validateAttribute(): void {
            try {
                  let primaryProperties = this.getGroupProperties(GROUP_NAME_LIST.PRIMARY);


                  /*
                  2018.04.24
                  DomToken 에러 발생
                  어떤 객체에서 발생하는 에러인지 찾아야함.
                  임시적으로 처리  해 놓음.

                   */
                  if(primaryProperties.id.length>0) {
                        // 스타일 클래스에 인스턴스 이름과 컴포넌트명을 추가
                        let tempName = primaryProperties.name.replace(/ /gi, "-");
                        this.element.classList.add(tempName);
                        this.element.classList.add(this.constructor.name);
                        this.element.classList.add("wv-component");

                        // editMode일때만 추가
                        if (this.isEditorMode)
                              this.element.classList.add("wv-edit");

                        if (this.isViewerMode) {
                              this.element.classList.add("wv-view");
                        }

                        // element id를 id 속성 값으로 설정
                        this.element.id = primaryProperties.id;
                        this.element["data-instanceName"] = primaryProperties.name;
                        this.element["data-id"] = primaryProperties.id;

                        this.appendElement.style.zIndex = this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.DEPTH);
                  }
            }
            catch (error) {
                  console.log("validateAttribute error = ", error);
            }
      }


      protected  _commitProperties():void{
            super._commitProperties();
            if(this.invalidateRotation ){
                  this.validateCallLater(this._validateRotation);
            }
            if(this.invalidateDepth){
                  this.validateCallLater(this._validateDepth);
            }
      }




      /*
            create()에서
            _createElement()
            _initWScriptEvent() -> 내부에서 호출

            이벤트 등록 및 자동 실행
      */
      protected _onInitWScriptEvent():void{
            if(this.events){
                  let keys = Object.keys(this.events);
                  let $element = $(this.element);


                  let eventInfos = this.getEventDescription();
                  if(eventInfos) {
                        eventInfos.forEach((eventInfo) => {
                              let eventName = eventInfo.name;
                              try{
                                    $element.on(eventName, ()=>{
                                          this.dispatchWScriptEvent(eventName);
                                    })
                              }catch(error){
                                    console.log("WScript 이벤트 등록 중 에러");
                              }
                        });
                  }


                  /*
                  keys.forEach((eventName)=>{
                        try{
                              $element.on(eventName, ()=>{
                                    this.dispatchWScriptEvent(eventName);
                              })
                        }catch(error){
                              console.log("WScript 이벤트 등록 중 에러");
                        }
                  });
                  */
            }

            TwoLayerPropertyPanelTooltip.getInstance().createTooltip(this);
      }

      protected _validateRotation():void {}

      protected _validateDepth():void {}

      _clearInvalidate(){
            super._clearInvalidate();
            this.invalidateDepth    = false;
            this.invalidateRotation = false;
      }

      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       ITransform 인터페이스 구현
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      protected _transform:Transform;
      get transform():Transform{
            return this._transform;
      }



      // 사용할 트랜스폼 커서 목록 반환
      handleTransformList():Array<string>{
            let handler:Array<string> = [
                  TransformCursorType.TOP_LEFT_SCALE,
                  TransformCursorType.TOP_MIDDLE_SCALE,
                  TransformCursorType.TOP_RIGHT_SCALE,
                  TransformCursorType.MIDDLE_LEFT_SCALE,
                  TransformCursorType.MIDDLE_RIGHT_SCALE,
                  TransformCursorType.BOTTOM_LEFT_SCALE,
                  TransformCursorType.BOTTOM_MIDDLE_SCALE,
                  TransformCursorType.BOTTOM_RIGHT_SCALE,
                  TransformCursorType.TRANSLATE,
                  /*TransformCursorType.ROTATE,*/
                  TransformCursorType.REGIST,
            ];
            if(this.transformMinimumScale() * this.transform.width >= this.width){
                  handler.splice(handler.indexOf(TransformCursorType.TOP_MIDDLE_SCALE), 1);
                  handler.splice(handler.indexOf(TransformCursorType.BOTTOM_MIDDLE_SCALE), 1);
                  //handler.splice(handler.indexOf(TransformCursorType.ROTATE), 1);
            }
            if(this.transformMinimumScale() * this.transform.height >= this.height){
                  //let hasRotateCursor:number = handler.indexOf(TransformCursorType.ROTATE);
                  handler.splice(handler.indexOf(TransformCursorType.MIDDLE_LEFT_SCALE), 1);
                  handler.splice(handler.indexOf(TransformCursorType.MIDDLE_RIGHT_SCALE), 1);
                  /*if(hasRotateCursor > -1){
                        handler.splice(hasRotateCursor, 1);
                  }*/
            }
            return handler;
      }

      // transform최소 사이즈
      transformMinimumScale(): number {
            let ow:number = this.getDefaultProperties().setter.width;
            return (10/ow);
      }

      createTransform(): void {
            this._transform = new Transform( this, this._properties.matrix || null );
      }
}
