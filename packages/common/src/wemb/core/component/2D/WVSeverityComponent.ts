import WVDOMComponent from "./WVDOMComponent";
import {CPLogger} from "../../utils/CPLogger";

export default class WVSeverityComponent extends WVDOMComponent {

      protected _severityVO:any;
      constructor(){
            super();
            this._severityVO =  null;
      }

      set severity(value) {
            this._checkUpdateGroupPropertyValue("setter", "severity", value);
      }

      get severity() {
            return this.getGroupPropertyValue("setter", "severity");
      }

      _onCreateProperties(){
            this._severityVO = {};
            this._severityVO.state = this.getGroupPropertyValue("setter", "severity");
            this._severityVO.colors = this.getGroupProperties("colors");
      }

      _onCommitProperties(){
            if(this._updatePropertiesMap.has("colors") || this._updatePropertiesMap.get("setter.severity") ){
                  this._severityVO.state = this.getGroupPropertyValue("setter", "severity");
                  this._severityVO.colors = this.getGroupProperties("colors");
                  this.validateCallLater(this._render);
            }
      }

      _onImmediateUpdateDisplay() {
            super._onImmediateUpdateDisplay();
            this._render();
      }

      protected _render() {
            CPLogger.log("상태갱신 처리", ["override가 필요"]);
      }

      _onDestroy(){
            this._severityVO =  null;
            super._onDestroy();
      }

}
