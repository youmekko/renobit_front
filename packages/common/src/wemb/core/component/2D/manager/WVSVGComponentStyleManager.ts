import WVComponent from "../../WVComponent";
import WVSVGComponent from "../WVSVGComponent";
import {IStyleManager} from "../../interfaces/ComponentInterfaces";

export default class WVSVGComponentStyleManager implements IStyleManager {

      private _styleList:any = {};
      private _classList:Array<String> = [];
      private _element:HTMLElement;
      private _comInstance:WVSVGComponent;
      private _styleProperties:any;

      private _invalidateProperty:boolean= false;
      get invalidateProperty():boolean {
            return this._invalidateProperty;
      }
      set invalidateProperty(value:boolean) {
            this._invalidateProperty = value;
      }

      constructor( comInstance:WVSVGComponent ){
            this._comInstance = comInstance;
            this._element = comInstance.element;

            // 컴포넌트의 properties.style를 _styleProperties로 사용
            if(this._comInstance.properties.hasOwnProperty("style"))
                  this._styleProperties = this._comInstance.properties.style;
            else
                  this._styleProperties = null;
      }

      immediateProperties(properties){
            /*
	      삭제 예정 immediateProperties에서 properties를 받지 않게 처리 해야함.

	      if(properties==null)
	            return;
	       */

            if(this._styleProperties==null)
                  return;

            let list = Object.getOwnPropertyNames(this._styleProperties);
            for (let i = 0; i < list.length; i++) {
                  let propertyName = list[i];
                  this._styleList[propertyName] = this._styleProperties[propertyName];

            }
            this.invalidateProperty = true;
            this.validateProperty();
      }


      setProperty(propertyName: string, value: any): boolean {
            this._styleList[propertyName]= value;
            if(this._styleProperties) {
                  this._styleProperties[propertyName] = value;
            }

            this._invalidateProperty = true;
            this._comInstance.invalidateProperties();
            return true;
      }

      getProperty(propertyName: string): any {
            if(this._styleList.hasOwnProperty(propertyName)==true){
                  return this._styleList[propertyName];
            }
            return null;
      }

      setProperties(properties: any, override?: boolean): boolean {

            if(override==true){
                  let list = Object.getOwnPropertyNames(this._styleList);
                  let change=false;
                  for(let i=0;i<list.length;i++){
                        delete this._styleList[list[i]];
                        change=true;
                  }

                  if(change){
                        this._invalidateProperty = true;
                        this._comInstance.invalidateProperties();
                  }

            }
            if(properties!=null) {
                  let list = Object.getOwnPropertyNames(properties);
                  for (let i = 0; i < list.length; i++) {
                        let propertyName = list[i];
                        this.setProperty(propertyName, properties[propertyName]);
                  }
                  return true;
            }

            return false;

      }

      getProperties(): any {
            return this._styleList;
      }


      public addClass(className:string):void{
            if(this._comInstance==null)
                  return;

            this._invalidateProperty = true;

            if(this._classList.indexOf(className)==-1) {
                  this._classList.push(className);
                  this._invalidateProperty = true;
                  this._comInstance.invalidateProperties();
            }

      }
      public removeClass(className:string):void{
            if(this._comInstance==null)
                  return;
            let index:number  =this._classList.indexOf(className);
            if(index!=-1) {
                  this._classList.splice(index,1);
                  this._invalidateProperty = true;
                  this._comInstance.invalidateProperties();
            }

      }
      public hasClass(className:string):boolean{
            return this._element.classList.contains(className);
      }

      public destroy(){
            this._styleList   = null;
            this._classList   = null;
            this._element     = null;
            this._comInstance = null;
            this._styleProperties = null;
      }

      validateProperty(): void {

            if(this.invalidateProperty){
                  if(this._styleList!=null){
                        if(this._styleList["rx"]){
                              this._styleList["ry"] = this._styleList["rx"];
                        }
                        delete this._styleList.backgroundColor;
                        delete this._styleList.borderRadius;
                        delete this._styleList.border;
                        $(this._element).attr(this._styleList);
                  }
                  if(this._classList.length>0){
                        $(this._element).addClass(this._classList.join(","));
                  }
            }
      }

      clearInvalidate(): void {
            this._invalidateProperty = false;
      }



}
