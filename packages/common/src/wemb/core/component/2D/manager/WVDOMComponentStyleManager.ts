import {IStyleManager} from "../../interfaces/ComponentInterfaces";
import WVComponent from "../../WVComponent";
import {tagProperty} from "inversify/dts/annotation/decorator_utils";

export default class WVDOMComponentStyleManager implements IStyleManager{

      // 컴포넌트에 적용되는 style 속성 이외에 속성을 저장
	private _styleList:any = {};
	private _classList:Array<String> = [];
	private _element:HTMLElement;
	private _comInstance:WVComponent;
	private _styleProperties:any;

	private _invalidateProperty:boolean= false;
	get invalidateProperty():boolean {
		return this._invalidateProperty;
	}
	set invalidateProperty(value:boolean) {
		this._invalidateProperty = value;
	}
	constructor(comInstance:WVComponent) {
		this._comInstance = comInstance;
		this._element = comInstance.element;

		// 컴포넌트의 properties.style를 _styleProperties로 사용
		if(this._comInstance.properties.hasOwnProperty("style"))
		      this._styleProperties = this._comInstance.properties.style;
		else
		      this._styleProperties = null;

	}

	public immediateProperties(properties:any){

	      /*
	      삭제 예정 immediateProperties에서 properties를 받지 않게 처리 해야함.

	      if(properties==null)
	            return;
	       */
	      if(this._styleProperties==null)
	            return;

            let list = Object.getOwnPropertyNames(this._styleProperties);
            for (let i = 0; i < list.length; i++) {
                  let propertyName = list[i];
                  this._styleList[propertyName] = this._styleProperties[propertyName];

            }

            // 스타일 정보를 컴포넌트에 바로 적용
            this.invalidateProperty = true;
            this.validateProperty();
      }


	public setProperty(propertyName:string, value:any):boolean{

		this._styleList[propertyName]= value;

            if(this._styleProperties) {
                  this._styleProperties[propertyName] = value;
            }

		this._invalidateProperty = true;
		this._comInstance.invalidateProperties();

		return true;
	}

	public getProperty(propertyName:string):any{
		if(this._styleList.hasOwnProperty(propertyName)==true){
			return this._styleList[propertyName];
		}
		return null;
	}

	public setProperties(properties:any, clear:boolean=false):boolean{

		if(clear==true){
			let list = Object.getOwnPropertyNames(this._styleList);
			let change=false;
			for(let i=0;i<list.length;i++){
				delete this._styleList[list[i]];
                        change=true;
			}

			if(change){
                        this._invalidateProperty = true;
                        this._comInstance.invalidateProperties();
                  }

		}
		if(properties!=null) {

                  let list = Object.getOwnPropertyNames(properties);
                  for (let i = 0; i < list.length; i++) {
                        let propertyName = list[i];
                        this.setProperty(propertyName, properties[propertyName]);
                  }

                  return true;
            }

            return false;
	}
	public getProperties():any{
		return this._styleList;
	}



	public addClass(className:string):void{
		if(this._comInstance==null)
			return;

		this._invalidateProperty = true;

		if(this._classList.indexOf(className)==-1) {
			this._classList.push(className);
			this._invalidateProperty = true;
			this._comInstance.invalidateProperties();
		}

	}
	public removeClass(className:string):void{
		if(this._comInstance==null)
			return;
		let index:number  =this._classList.indexOf(className);
		if(index!=-1) {
			this._classList.splice(index,1);
			this._invalidateProperty = true;
			this._comInstance.invalidateProperties();
		}

	}

	public hasClass(className:string):boolean{
		return this._element.classList.contains(className);
	}
      /////////////////////////////////////////////////


	public getLabelBackgroundStyle(bgData:any){
		let str = '';
		bgData.label_background_type = bgData.label_background_type || 'solid';
		bgData.label_background_color1 = bgData.label_background_color1 || 'rgba(0,0,0,0)';

		if (bgData.label_background_type == 'solid') {
			str = `${bgData.label_background_color1}`;
		} else if (bgData.label_background_type == 'gradient') {
			let dir = '';
			switch (bgData.label_background_direction) {
			case 'left':
				dir = 'linear-gradient( 90deg';
				break;
			case 'top':
				dir = 'linear-gradient( 180deg';
				break;
			case 'diagonal1':
				dir = 'linear-gradient( -45deg';
				break;
			case 'diagonal2':
				dir = 'linear-gradient( 45deg';
				break;
			case 'radial':
				dir = 'radial-gradient(ellipse at center';
				break;
			}

			str = `${dir}, ${bgData.label_background_color1} 0%, ${bgData.label_background_color2} 100%)`;
		} else {
			str = `${bgData.label_background_text}`;
		}

		return str;
	}

	
	public getBackgroundStyle(bgData:any, selector:string){
           let style = '';
		if (bgData.type == 'solid') {
			style = `<style>${selector}{background-color: ${bgData.color1} !important; }</style>`;
		} else if (bgData.type == 'gradient') {
			let dir = '';
			switch (bgData.direction) {
			case 'left':
				dir = 'linear-gradient( 90deg';
				break;
			case 'top':
				dir = 'linear-gradient( 180deg';
				break;
			case 'diagonal1':
				dir = 'linear-gradient( -45deg';
				break;
			case 'diagonal2':
				dir = 'linear-gradient( 45deg';
				break;
			case 'radial':
				dir = 'radial-gradient(ellipse at center';
				break;
			}

			style = `<style>${selector}{background: ${dir}, ${bgData.color1} 0%, ${bgData.color2} 100%);}</style>`;
		} else {
			style = `<style>${selector}{background: ${bgData.text};}</style>`;
		}

		return style;
	}


      /////////////////////////////////////////////////
	// 외부에서 호출
	public validateProperty():void{
		if(this.invalidateProperty){
			if(this._styleList!=null){
				$(this._element).css(this._styleList);
			}

			if(this._classList.length>0){
				$(this._element).addClass(this._classList.join(","));
			}
		}
	}

	public clearInvalidate():void{
	      this._invalidateProperty = false;
      }
      /////////////////////////////////////////////////

      destroy():void{
            this._styleList   = null;
            this._classList   = null;
            this._element     = null;
            this._comInstance = null;
            this._styleProperties = null;
      }


}
