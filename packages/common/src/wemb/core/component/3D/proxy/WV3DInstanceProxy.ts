import {THREE} from "../../../utils/reference";

// 컴포넌트 프록시
/*인스턴스화는 사용성을 위해 사용하는 곳에서 판단하여 구성하도록 함*/
export default class WV3DInstanceProxy {

      protected _parent:any;
      protected _position:any;
      protected _scale:any;
      protected _rotation:any;
      protected _size:any;
      protected _properties:any;
      protected _color:any;
      protected _instance:any;
      protected _visible:boolean;
      protected _name:string;
      protected _selected:boolean;
      protected _data:any;

      constructor( properties:any = null ){
            if(properties){
                  this.initFromProperties(properties);
            }
      }

      // 상황에 따라 override필요
      protected initFromProperties( props:any ) {
            let toRadian = Math.PI/180;
            let rotate      = props.setter.rotation || new THREE.Vector3;
            let position    = props.setter.position || new THREE.Vector3;
            let scale       = props.setter.size || new THREE.Vector3(1, 1, 1);
            let elementSize = props.elementSize || new THREE.Vector3(1, 1, 1);

            this.rotation = new THREE.Vector3( rotate.x*toRadian, rotate.y*toRadian, rotate.z*toRadian);
            this.position = new THREE.Vector3(position.x, position.y, position.z );
            this.scale    = new THREE.Vector3(scale.x/elementSize.x, scale.y/elementSize.y, scale.z/elementSize.z);
            this._properties   = props; //원본 참조
            this._properties.elementSize = elementSize;
      }

      get elementSize(){ return this._properties.elementSize; }

      get size() { return new THREE.Vector3(this.scale.x*this.elementSize.x, this.scale.y*this.elementSize.y, this.scale.z*this.elementSize.z); }
      get resource(){ return this._properties.resource; }
      get selected() { return this._selected; }

      set selected( bValue ){
            if(this._selected != bValue){
                  this._selected = bValue;
                  if(!this._selected && this.instance ){
                        this.instance = null;
                  }
            }
      }

      // 프록시 정보를 통해 생성되는 진짜 instance참조
      // null값이 들어오면 destroy처리
      set instance(instance) {
            if(instance == null ){
                  this._instance.destroy();
            }
            this._instance = instance;
      }
      get instance() { return this._instance; }


      set name(strName) { this._name = strName; }
      get name() { return this._name; }

      set parent( parent ){ this._parent = parent; }
      get parent(){ return this._parent; }

      get data(){ return this._data; }
      set data(objData:any) { this._data = objData; }

      set visible( value ) { this._visible =value; }
      get visible(){ return this._visible; }

      set scale( value ) { this._scale = value; }
      get scale() { return this._scale; }

      set rotation( value ) { this._rotation = value; }
      get rotation() { return this._rotation; }

      set position( value ) { this._position = value; }
      get position() { return this._position; }

      getWorldPosition(){
            return this.position.clone().applyMatrix4(this.parent.appendElement.matrixWorld);
      }

      getWorldRotation(){
            return this.rotation.clone().applyMatrix4(this.parent.appendElement.matrixWorld);
      }

      set color( value ) { this._color = value; }
      get color() { return this._color; }


      onDestroy(){}

      destroy(){
            this.onDestroy();
            this._parent = null;
            this._data  = null;
      }

}
