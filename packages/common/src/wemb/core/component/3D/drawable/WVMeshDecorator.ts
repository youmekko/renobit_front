// mesh를 컴포넌트 처럼 다룰 수 있게 하는 데코레이터
import ThreeUtil from "../../../utils/ThreeUtil";
import {THREE} from "../../../utils/reference";
import MeshManager from "../manager/MeshManager";
import CPUtil from "../../../utils/CPUtil";

export default class WVMeshDecorator {

      protected _container:THREE.Object3D;
      protected _outlineElement:THREE.Object3D;
      protected _parent:any;
      protected _outlineColor:any = 0x00ff00;
      protected _selectedOutlineColor:any = 0x00ff00;
      protected _onClickHandler:Function;
      protected _onDoubleClickHandler:Function;
      protected _onOverHandler:Function;
      protected _onOutHandler:Function;
      protected _outlineEnable:boolean;
      protected _data:any;
      protected _selected:boolean;
      protected _focusTween:any;
      protected _color:any;
      protected _emissive:any;
      protected _elementSize:THREE.Vector3;

      constructor( source:THREE.Object3D ){
            this._container  = source;
            this._onClickHandler = this.onClick.bind(this);
            this._onDoubleClickHandler = this.onDoubleClick.bind(this);
            this._onOverHandler = this.onOver.bind(this);
            this._onOutHandler = this.onOut.bind(this);
            this._elementSize  = ThreeUtil.getObjectSize(source);
            this._focusTween = null;
            this._data         = null;
            this._selected     = false;
            this._color        = 0xffffff;
            this._emissive     = 0x000000;
      }

      clearTween(){
            if(this._focusTween != null && this._focusTween.isActive() ){
                  this._focusTween.pause();
                  this._focusTween.kill();
            }
      }

      // scale이 1일 경우 size정보
      get elementSize():THREE.Vector3{
            return this._elementSize;
      }

      get size():THREE.Vector3{
            return ThreeUtil.getObjectSize(this.appendElement);
      }

      get selected():boolean{
            return this._selected;
      }

      set selected( bValue ){
            if(this._selected != bValue){
                  this._selected = bValue;
                  // selected아웃라인 활성화 시 마우스 오버/아웃 아웃라인은 제거
                  if(this._selected){
                        this.toggleOutlineElement(false);
                  }
                  this.toggleSelectedOutline(bValue);
            }
      }

      set data( info:any ){
            if(this._data != info ){
                  this._data = info;
                  this.validateData();
            }
      }

      get data():any{
            return this._data;
      }

      set outlineColor( value:any ){
            if(this._outlineColor != value ){
                  this._outlineColor = value;
            }
      }

      set outlineEnable( bValue:boolean ){
            this._outlineEnable = bValue;
      }

      set selectedOutlineColor(bValue:any){
            this._selectedOutlineColor = bValue;
      }

      get selectedOutlineColor(){
            return this._selectedOutlineColor;
      }

      get outlineEnable():boolean{
            return this._outlineEnable;
      }

      get outlineColor():any {
            return this._outlineColor;
      }

      get outlineElement(){
            return this._outlineElement.userData.boundingBox;
      }

      protected removePrevOutlineElement() {
            if (this.outlineElement != null) {
                  //this.removeEventByType(this._outlineElement, "mouseover", this._func_call_over_handler, false);
                  //this.removeEventByType(this._outlineElement, "mouseout", this._func_call_out_handler, false);
                  //this.appendElement.remove(this.outlineElement);
                  //MeshManager.disposeMesh(this.outlineElement);
                  this.outlineElement.geometry.dispose();
                  this.outlineElement.material.dispose();
                  this.appendElement.remove(this.outlineElement);
                  this._outlineElement.userData.boundingBox = null;
            }
      }

      _validateOutline(){
            if(this._outlineElement){
                  this.removePrevOutlineElement();
            } else {
                  this._outlineElement                            = this.appendElement;
            }
            this._outlineElement.userData.boundingBox       = this.createGroupEdge(this.appendElement);
            this._outlineElement.userData.boundingBox.name  = "outline";
            // 현재 선택아웃라인이 존재하면 제거 후 다시 추가
            let selectedOutline = this.getSelectOutline();

            if (selectedOutline) {
                  this.toggleSelectedOutline(false);
                  this.toggleSelectedOutline(true);
            }
      }


      //디스플레이 구성 초기화
      draw( parent ) {
            this._parent = parent;
            this._parent.appendElement.add(this.appendElement);
            this.composeChildren();
      }

      protected validateData(){
            console.warn("데이터 갱신에 따른 업데이트 처리가 필요하면 여기서 구현");
      }

      protected createEdge( geometry:THREE.Geometry ) {
            let edge = new THREE.EdgesGeometry(geometry);
            let line = new THREE.LineSegments( edge, new THREE.LineBasicMaterial({color:this.outlineColor, transparent:true}));
            return line;
      }

      protected createGroupEdge( object:THREE.Object3D ) {
            //true
            object.updateMatrix();
            let box:THREE.Box3                  = ThreeUtil.getObjectBox(object);
            // 원점을 기준으로 위치를 계산하기 위해 translate를 통해 position값을 offset처리
            let inverse:THREE.Matrix4           = new THREE.Matrix4().getInverse(object.matrixWorld);
            box                                 = box.applyMatrix4(inverse);
            let boxSize:THREE.Vector3           = box.getSize(new THREE.Vector3());
            let geometry:THREE.BoxGeometry      = new THREE.BoxGeometry(boxSize.x, boxSize.y, boxSize.z);
            let offset:THREE.Vector3            = box.getCenter();
            geometry.translate(offset.x, offset.y, offset.z);
            return this.createEdge(geometry);
      }

      protected createChildren(){}

      protected composeChildren() {
            this.createChildren();
            this._validateOutline();
      }

      get parent(){
            return this._parent;
      }


      get appendElement(){
            return this._container;
      }

      set name(strName:string) {
            this.appendElement.name = strName;
      }

      get name():string {
            return this.appendElement.name;
      }


      set visible( value ) {
            this.appendElement.visible =value;
      }

      get visible(){
            return this.appendElement.visible;
      }

      set scale( value:THREE.Vector3 ) {
            this.appendElement.scale.set( value.x, value.y, value.z );
      }

      get scale():THREE.Vector3 {
            return this.appendElement.scale;
      }

      set rotation( value:THREE.Euler ) {
            this.appendElement.rotation.set( value.x, value.y, value.z );
      }

      get rotation():THREE.Euler {
            return this.appendElement.rotation;
      }

      set position( value:THREE.Vector3 ){
            this.appendElement.position.set( value.x, value.y, value.z );
      }

      get position():THREE.Vector3 {
            return this.appendElement.position;
      }

      getWorldPosition(){
            return this.appendElement.getWorldPosition(new THREE.Vector3);
      }

      _validateColor(){
            let color = this.color;
            let emissive= this.emissive;
            function recursiveColor( parent ){
                  let i;
                  parent.traverse((child)=>{
                        if(child instanceof THREE.Mesh ){
                              if(child.material instanceof Array){
                                    for( i=0; i<child.material.length; i++ ){
                                          child.material[i].color.set(color);
                                          child.material[i].emissive.set(emissive);
                                    }
                              } else {
                                    child.material.color.set(color);
                                    child.material.emissive.set(emissive);
                              }

                        }
                        if(child.children != null){
                              for( let i=0; i<child.children.length;i++){
                                    if(child.children[i] != null){
                                          recursiveColor(child.children[i]);
                                    }
                              }
                        }
                  });
            }
            recursiveColor(this.appendElement);
      }

      set color( value:any ){
            if(this._color != value ){
                  this._color = value;
                  let callback = this._validateColor.bind(this);
                  CPUtil.getInstance().debounce( callback, 0);
            }
      }

      get color(){
            return this._color;
      }

      set emissive( value:any ){
            let callback = this._validateColor.bind(this);
            if( this._emissive != value ){
                  this._emissive = value;
                  CPUtil.getInstance().debounce( callback, 0);
            }
      }

      get emissive():any{
            return this._emissive;
      }


      protected _preventEvent( event ){
            event.stopPropagation();
            event.origDomEvent.preventDefault();
            event.origDomEvent.stopPropagation();
            event.origDomEvent.stopImmediatePropagation();
      }

      protected onClick( event ){
            this._preventEvent(event);
            this._parent.appendElement.dispatchEvent({type:"itemClick", selectItem:this});
      }

      protected onDoubleClick( event ){
            this._preventEvent(event);
            this._parent.appendElement.dispatchEvent({type:"itemDblClick", selectItem:this});
      }

      protected onOver( event ){
            this._preventEvent(event);
            if(this.outlineEnable){this.toggleOutlineElement(true);}
            this._parent.appendElement.dispatchEvent({type:"itemMouseOver", selectItem:this});
      }

      protected onOut( event ){
            this._preventEvent(event);
            if(this.outlineEnable){this.toggleOutlineElement(false);}
            this._parent.appendElement.dispatchEvent({type:"itemMouseOut", selectItem:this});
      }

      hasElement( element ) {
            let max = this.appendElement.children.length;
            for( let i = 0; i<max; i++) {
                  let child = this.appendElement.children[i];
                  if( child == element ){
                        return true;
                  }
            }
            return false;
      }

      toggleOutlineElement( bValue ) {
            if(this.getSelectOutline())return;
            let wireFrame = this._outlineElement.userData.boundingBox;
            if(bValue){
                  this.appendElement.add(wireFrame);
            } else {
                  this.appendElement.remove(wireFrame);
            }
      }

      getSelectOutline():any{
            return this.appendElement.getObjectByName("selectedOutline");
      }

      toggleSelectedOutline(bValue: boolean ): void {
            let wireFrame = this.getSelectOutline();
            if (bValue) {
                  if(!wireFrame){
                        wireFrame = this.outlineElement.clone();
                        wireFrame.name = "selectedOutline";
                        wireFrame.material.color.set(this.selectedOutlineColor);
                  }
                  this.appendElement.add(wireFrame);
            } else {
                  if(wireFrame){
                        this.appendElement.remove(wireFrame);
                        MeshManager.disposeMesh(wireFrame);
                  }
            }
      }



      registEventListener(){
            this.outlineEnable = true;
            wemb.threeElements.domEvents.addEventListener(this._outlineElement, "click", this._onClickHandler , true);
            wemb.threeElements.domEvents.addEventListener(this._outlineElement, "dblclick", this._onDoubleClickHandler , true);
            wemb.threeElements.domEvents.addEventListener(this._outlineElement, "mouseover", this._onOverHandler , true);
            wemb.threeElements.domEvents.addEventListener(this._outlineElement, "mouseout", this._onOutHandler , true);
      }

      removeEventListener(){
            this.outlineEnable = false;
            wemb.threeElements.domEvents.removeEventListener(this._outlineElement, "click", this._onClickHandler , true);
            wemb.threeElements.domEvents.removeEventListener(this._outlineElement, "dblclick", this._onDoubleClickHandler , true);
            wemb.threeElements.domEvents.removeEventListener(this._outlineElement, "mouseover", this._onOverHandler , true);
            wemb.threeElements.domEvents.removeEventListener(this._outlineElement, "mouseout", this._onOutHandler , true);
      }



      transitionInEffect(){
            return Promise.resolve();
      }

      transitionOutEffect(){
            return Promise.resolve();
      }

      async transitionIn(){
            await this.transitionInEffect();
      }

      async transitionOut(){
            await this.transitionOutEffect();
      }

      onDestroy(){}

      destroy(){
            this.onDestroy();
            this.removeEventListener();
            this._elementSize = null;
            this._data = null;
            this._outlineElement.userData = null;
            this._parent.appendElement.remove(this.appendElement);
            MeshManager.disposeMesh(this.appendElement);
            this._container = null;
            this._parent = null;
      }

}
