import WVComponent, {GROUP_NAME_LIST} from "../WVComponent";
import MeshManager from "./manager/MeshManager";
import {
      IComponentMetaProperties,
      IExtensionElements,
      IStyleManager,
      IThreeExtensionElements,
      IWV3DComponent,
      IWVVector,
      WV3D_PROPERTIES, WVCOMPONENT_EDITOR_POPERTY_NAMES, WVCOMPONENT_PROPERTY_NAMES
} from "../interfaces/ComponentInterfaces";

import {THREE, THREEx, $} from "../../utils/reference";
import WVComponentEvent from "../../events/WVComponentEvent";
import {Matrix} from "../../geom/Matrix";
import ThreeUtil from "../../utils/ThreeUtil";
import WV3DComponentStyleManager from "./manager/WV3DComponentStyleManager";
import WV3DLabel from "./drawable/WV3DLabel";
import WVMouseEvent from "../../events/WVMouseEvent";


export default abstract class NWV3DComponent extends WVComponent implements IWV3DComponent {

      protected _container: any = null;
      protected _label: WV3DLabel = null;
      protected _elementSize: any = null;
      protected _domEvents: any = null;
      protected _outlineElement: any = null;
      protected invalidateRotation: boolean = false;
      protected invalidOriginSize: boolean = false;

      protected _threeElements: IThreeExtensionElements;

      private _func_call_click_handler: Function = null;
      private _func_call_dblclick_handler: Function = null;
      private _func_call_over_handler: Function = null;
      private _func_call_out_handler: Function = null;

      private _connections : Map<string, string> = new Map();
      public setConnection(from : string, to : string){
            this.connections.set(from, to)
      }
      get connections(){
            return this._connections
      }
      public getConnectionSize(){
            return this.connections.size
      }
      public getConnectionKeys(){
            return this.connections.keys();
      }
      public getConnection(key : string){
            return this.connections.get(key);
      }
      public deleteConnection(key : string){
             this.connections.delete(key);
      }

      static readonly SELECTED_OUTLINE_NAME: string = "selectedOutline";
      static readonly OUTLINE_NAME: string = "outline";

      protected constructor() {
            super();
      }

      get elementSize(): any {
            return this._elementSize;
      }

      set selectedOutlineColor(value: any) {
            if (this._checkUpdateGroupPropertyValue("setter", "selectedOutlineColor", value)) {
                  let outline: any = this.getSelectOutline();
                  if (outline) {
                        outline.material.color.set(value);
                  }
            }
      }

      get selectedOutlineColor(): any {
            return this.properties.setter.selectedOutlineColor;
      }

      set outlineColor(value: any) {
            if (this._checkUpdateGroupPropertyValue("setter", "outlineColor", value)) {
                  if (this.outlineElement) {
                        this.outlineElement.material.color.set(value);
                  }
            }
      }

      get outlineColor(): any {
            return this.properties.setter.outlineColor;
      }

      protected createEdge(geometry: THREE.Geometry): any {
            let edgeColor: number = this.outlineColor;
            let edge: THREE.EdgesGeometry = new THREE.EdgesGeometry(geometry);
            let line: THREE.LineSegments = new THREE.LineSegments(edge, new THREE.LineBasicMaterial({color: edgeColor, transparent:true}));
            return line;
      }

      protected createGroupEdge(object: THREE.Object3D): any {
            // 변경 정보를 반영하기 위해 updatematrixworld를 실행해 줌.
            object.updateMatrixWorld(true);
            let box: THREE.Box3 = ThreeUtil.getObjectBox(object);
            // 원점을 기준으로 위치를 계산하기 위해 translate를 통해 position값을 offset처리
            let inverse: THREE.Matrix4 = new THREE.Matrix4().getInverse(object.matrixWorld);
            box = box.applyMatrix4(inverse);
            let boxSize: THREE.Vector3 = box.getSize(new THREE.Vector3());
            let geometry: THREE.Geometry = new THREE.BoxGeometry(boxSize.x, boxSize.y, boxSize.z);
            let offset: THREE.Vector3 = box.getCenter(new THREE.Vector3());
            geometry.translate(offset.x, offset.y, offset.z);
            return this.createEdge(geometry);
      }

      protected _createElement(template) {
            this._container = new THREE.Object3D();
            this._container.name = this.name;
            this._onCreateElement();
            this._initEventListener();
      }

      protected hasLabel(): boolean {
            return this._label != null ? true : false;
      }

      _onCreateProperties() {
            super._onCreateProperties();

            this._elementSize = this.properties.elementSize || null;
            //this.outlineEnable = this.properties.setter.outlineEnable || true;
            this._func_call_click_handler = this._onClickHandler.bind(this);
            this._func_call_dblclick_handler = this._onDblClickHandler.bind(this);
            this._func_call_over_handler = this._onMouseOverHandler.bind(this);
            this._func_call_out_handler = this._onMouseOutHandler.bind(this);
      }

      protected translateCenterByVector(vector: any): void {
            this._element.geometry.translate(vector.x, vector.y, vector.z);
      }


      protected _onDestroy() {
            if (this.hasLabel()) {
                  this._label.destroy();
                  this._label = null;
            }
            if (this._domEvents) {
                  this.removeEventByType(this.appendElement, "click", this._func_call_click_handler, false);
                  this.removeEventByType(this.appendElement, "dblclick", this._func_call_dblclick_handler, false);
                  //if (this._outlineElement != null) {
                  this.removeEventByType(this.appendElement, "mouseover", this._func_call_over_handler, false);
                  this.removeEventByType(this.appendElement, "mouseout", this._func_call_out_handler, false);
                  //}
                  this._domEvents = null;
            }
            MeshManager.disposeMesh(this.appendElement);
            this._threeElements = null;
            this._func_call_click_handler = null;
            this._func_call_dblclick_handler = null;
            this._func_call_over_handler = null;
            this._func_call_out_handler = null;
            this._label = null;

            if(this._outlineElement){
                  MeshManager.disposeMesh(this._outlineElement);
                  this._outlineElement = null;
            }
            this._container = null;
            super._onDestroy();
      }

      public serializeMetaProperties(): IComponentMetaProperties {
            let properties: any = super.serializeMetaProperties();
            properties.props.elementSize = this._elementSize;
            return properties;
      }


      public setExtensionElements(extension: IExtensionElements) {
            super.setExtensionElements(extension);
            /*
            2018.11.04(ckkim)
            threeElements가 존재하지 않을 수 있음.
             */
            this._threeElements = extension as IThreeExtensionElements;
            try {
                  this._domEvents = this._threeElements.domEvents;
            } catch (error) {
                  this._domEvents = null;
            }
      }

      get outlineElement(): any {
            let results = null;
            if(this._outlineElement){
                  results = this._outlineElement.userData.boundingBox;
            }
            return results;
      }


      public hasElement(element: any): boolean {
            let max: number = this.appendElement.children.length;
            for (let i: number = 0; i < max; i++) {
                  let child = this.appendElement.children[i];
                  if (child == element) {
                        return true;
                  }
            }
            return false;
      }

      public getObjectByName( strName:string ):any {
            return this.appendElement.getObjectByName(strName);
      }


      public toggleOutlineElement(bValue: boolean): void {
            if (this.getSelectOutline()) return;
            let wireFrame = this.outlineElement;
            if(wireFrame){
                  (bValue) ? this.appendElement.add(wireFrame) : this.appendElement.remove(wireFrame);
            }
      }

      getSelectOutline(): any {
            return this.appendElement.getObjectByName(NWV3DComponent.SELECTED_OUTLINE_NAME);
      }

      toggleSelectedOutline(bValue: boolean): void {
            let wireFrame = this.getSelectOutline();
            if (bValue) {
                  if (!wireFrame) {
                        if(this.outlineElement !== null){
                              wireFrame = this.outlineElement.clone();
                              wireFrame.gemetry = this.outlineElement.geometry.clone();
                              wireFrame.material = this.outlineElement.material.clone();
                              wireFrame.name = NWV3DComponent.SELECTED_OUTLINE_NAME;
                              wireFrame.material.color.set(this.selectedOutlineColor);
                              this.toggleOutlineElement(false);
                              this.appendElement.add(wireFrame);
                        }
                  }

            } else {
                  if (wireFrame) {
                        this.appendElement.remove(wireFrame);
                        MeshManager.disposeMesh(wireFrame);
                  }
            }
      }


      public createStyleManager(): IStyleManager {
            // 3D 스타일관리자로 변경해야함.
            this._styleManager = new WV3DComponentStyleManager(this);
            return this._styleManager;
      }

      get appendElement(): any {
            return this._container;
      }

      set position(value: WVVector) {
            if (this._checkUpdateGroupVectorPropertyValue(GROUP_NAME_LIST.SETTER, WV3D_PROPERTIES.POSITION, value)) {
                  this.invalidatePosition = true;
            }
      }

      get position(): WVVector {
            return this.setter.position;
      }

      set size(value: WVVector) {
            if (this._checkUpdateGroupVectorPropertyValue(GROUP_NAME_LIST.SETTER, WV3D_PROPERTIES.SIZE, value)) {
                  this.invalidateSize = true;
            }
      }


      get size(): WVVector {
            return this.setter.size;
      }


      set rotation(value: WVVector) {
            if (this._checkUpdateGroupVectorPropertyValue(GROUP_NAME_LIST.SETTER, WV3D_PROPERTIES.ROTATION, value)) {
                  this.invalidateRotation = true;
            }
      }

      get rotation(): WVVector {
            return this.setter.rotation;
      }

      get lock(): boolean {
            return this.getGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_PROPERTY_NAMES.LOCK);
      }

      set lock(value: boolean) {
            this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_PROPERTY_NAMES.LOCK, value);
      }


      set originSize(value: WVVector) {
            if (this._checkUpdateGroupPropertyValue("setter", "originSize", value)) {
                  this.size = this._elementSize;
            }
      }

      get originSize(): WVVector {
            return this.getGroupPropertyValue("setter", "originSize");
      }


      set mouseEnable(value: boolean) {
            if (this._checkUpdateGroupPropertyValue("setter", "mouseEnable", value)) {
            }
      }

      get mouseEnable(): boolean {
            return this.getGroupPropertyValue("setter", "mouseEnable");
      }

      set opacity(value: number) {
            if (this._checkUpdateGroupPropertyValue("setter", "opacity", value)) {
            }
      }

      get opacity(): number {
            return this.getGroupPropertyValue("setter", "opacity");
      }


      set color(value: string) {
            if (this._checkUpdateGroupPropertyValue("setter", "color", value)) {
            }
      }

      get color(): string {
            return this.getGroupPropertyValue("setter", "color");
      }


      set emissive(value: string) {
            if (this._checkUpdateGroupPropertyValue("setter", "emissive", value)) {
            }
      }

      get emissive(): string {
            return this.getGroupPropertyValue("setter", "emissive");
      }

      set boundingEnable(bValue: boolean) {
            console.warn("boudingEnable은 추후 사라질 수 있습니다. outlineEnable을 이용해 주세요");
            this.outlineEnable = bValue;
      }

      get boundingEnable(): boolean {
            console.warn("boudingEnable은 추후 사라질 수 있습니다. outlineEnable을 이용해 주세요");
            return this.outlineEnable
      }

      set outlineEnable(bValue: boolean) {
            if (this._checkUpdateGroupPropertyValue("setter", "outlineEnable", bValue)) {
            }
      }

      get outlineEnable(): boolean {
            return this.getGroupPropertyValue("setter", "outlineEnable");
      }

      /////////////


      /////////////
      /* 커스텀 렌더링 처리
      컴포넌트 자체에서 렌더링을 사용하는 경우
      1. usingRender()를 true 값을 리턴한다.
      2. renderer()를 구현한다.
      */

      // 렌더러를 사용하는 컴포넌트 인경우 true를 리턴후 render() 메서드를 재정의 하세요.
      get usingRenderer() {
            return false;
      }

      public render() {

      }

      /////////////


      _onImmediateUpdateDisplay(): void {
            this._validateRotation();
            this._validateLabel();
            this._validateLock();
            if (!this.isResourceComponent) {
                  this._validateColor();
                  this._validateOpacity();
                  this._validateOutline();
            }
      }


      _onCommitProperties() {
            if (this.invalidateRotation) {
                  this.validateCallLater(this._validateRotation);
            }

            if (this._updatePropertiesMap.has("setter.color") || this._updatePropertiesMap.has("setter.emissive")) {
                  this.validateCallLater(this._validateColor);
            }

            if (this._updatePropertiesMap.has("setter.opacity")) {
                  this.validateCallLater(this._validateOpacity);
            }

            if (this._updatePropertiesMap.has("setter.visible")) {
                  this.validateCallLater(this._validateVisible);
            }

            if(this._updatePropertiesMap.has("label"))
                  this.validateCallLater(this._validateLabel)
      }

      protected _validateAttribute(): void {
      }

      protected _validateLock(): void {
            /*lock처리는 editor 모드에서만 실행되어야 함.*/
            if (this.isEditorMode) {
                  if (this.lock) {
                        this.dispatchEvent(new WVComponentEvent(WVComponentEvent.LOCKED, this, {}));
                  } else {
                        this.dispatchEvent(new WVComponentEvent(WVComponentEvent.UN_LOCKED, this, {}));
                  }
            }
      }

      protected _validatePosition(): void {
            this.appendElement.position.set(+this.position.x, +this.position.y, +this.position.z);
      }

      protected _validateSize(): void {
            let value = this.size;
            let base = this.elementSize || this.size;
            let sx: number = +value.x / base.x;
            let sy: number = +value.y / base.y;
            let sz: number = +value.z / base.z;
            this.appendElement.scale.set(sx, sy, sz);
            this.appendElement.updateMatrixWorld();
      }


      /*
      *   resource를 사용하는 컴포넌트에서 사이즈 정보를 초기화 할 때 사용.
      *   기본 정보 초기화 처리
      * */
      protected _inValidateElementSize() {
            this._elementSize = null;
            this.size = this.getDefaultProperties().setter.size;
      }

      protected _validateElementSize(objSize: any) {
            if (this._elementSize == null) {
                  // obj 사이즈와 appendElement 사이즈 스위치 처리
                  this._elementSize = objSize;
                  let base = this.getDefaultProperties().setter.size;
                  if (this.size.x == base.x && this.size.y == base.y && this.size.z == base.z) {
                        this.syncSizeFromElementSize();
                  }
            }
      }


      // 모델링 사이즈와 속성 size동기화 처리
      protected syncSizeFromElementSize(): void {
            this.properties.setter.size.x = this._elementSize.x;
            this.properties.setter.size.y = this._elementSize.y;
            this.properties.setter.size.z = this._elementSize.z;
      }

      /**
       속성창에 size와 scale정보를 변경해 표시
       하위 추가 되는 요소는 스케일 표현을 위해 부모의 스케일 비율가 반영되야 함.
       */
      protected get scaleRate(): any {
            return {
                  x: 1 / this.appendElement.scale.x,
                  y: 1 / this.appendElement.scale.y,
                  z: 1 / this.appendElement.scale.z
            };
      }

      protected _validateRotation(): void {
            let value = this.rotation;
            //degree => radian으로 변경
            this.appendElement.rotation.set(+value.x * Matrix.TO_RADIAN, +value.y * Matrix.TO_RADIAN, +value.z * Matrix.TO_RADIAN);
      }

      protected _validateVisible(): void {
            let visible: boolean = this._properties.setter.visible;
            this.appendElement.visible = this.element.visible = visible;
            if (this._outlineElement) { this._outlineElement.visible = this.appendElement.visible; }
            this._validateLabel();
      }

      /* editor 모드에서만 실행 */
      protected _validateEditorModeVisible(): void {
            if (this.isEditorMode) {
                  let visible: boolean = this.getGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_EDITOR_POPERTY_NAMES.VISIBLE);
                  this.appendElement.visible = visible;
            }
      }

      protected _validateLabel(): void {
            if (!this.visible && this.hasLabel()) {
                  this._label.destroy();
                  this._label = null;
                  return null;
            }
            let labelVO = this.getGroupProperties("label");
            if (labelVO.label_using == "Y") {
                  if(!this.hasLabel()) { this._label = new WV3DLabel(); } //실제 라벨이 없으면 생성
                  this._label.text = wemb.localeManager.translatePrefixStr(labelVO.label_text);
                  this._label.css({
                        "opacity": labelVO.label_opacity,
                        "color": labelVO.label_color,
                        "background-color": labelVO.label_background_color,
                        "font-size": labelVO.label_font_size + "px",
                        "font-weight": labelVO.label_font_weight,
                        "border": labelVO.border,
                        "font-family": labelVO.label_font_type
                  });
                  this._label.labelPosition = new THREE.Vector3(labelVO.x * this.scaleRate.x,labelVO.y * this.scaleRate.y, 0);
                  if(this._label.line === null && labelVO.line_connect){
                        //이미 라인 쓴다고 했던 것들
                        console.log(labelVO);
                        this._label.createLine(labelVO.label_background_color);
                  }else if(this._label.line !== null && !labelVO.line_connect){
                        //라인 안쓴다고 했는데 바꾸는 것들
                        this._label.lineClear();
                  }
                  this._label.draw(this);
                  this._label.show();
            } else {
                  if(this.hasLabel()){
                        this._label.destroy();
                        this._label = null;
                  }
            }
      }

      protected _clearInvalidate() {
            super._clearInvalidate();
            this.invalidateRotation = false;
      }

      /*
      일반적으로 controller에 의해서 움직인 값을 setter에 적용할 때 사용.
       */
      syncLocationSizeElementPropsComponentProps() {
            this._properties.setter.position.x = this.appendElement.position.x;
            this._properties.setter.position.y = this.appendElement.position.y;
            this._properties.setter.position.z = this.appendElement.position.z;

            this._properties.setter.rotation.x = (this.appendElement.rotation.x * Matrix.TO_DEGREE).toFixed(0);
            this._properties.setter.rotation.y = (this.appendElement.rotation.y * Matrix.TO_DEGREE).toFixed(0);
            this._properties.setter.rotation.z = (this.appendElement.rotation.z * Matrix.TO_DEGREE).toFixed(0);

            // elementSize정보가 없을 경우 size값을 이용
            let sizeInfo = this._elementSize || this.size;
            this._properties.setter.size.x = (+this.appendElement.scale.x) * +sizeInfo.x;
            this._properties.setter.size.y = (+this.appendElement.scale.y) * +sizeInfo.y;
            this._properties.setter.size.z = (+this.appendElement.scale.z) * +sizeInfo.z;
            this._validateLabel();
      }


      /* vector 요소 변경 유무 */
      protected _checkUpdateGroupVectorPropertyValue(groupName: string, propertyName: string, value: IWVVector) {
            let _oldValue = this.getGroupPropertyValue(groupName, propertyName);
            _oldValue = new WVVector(_oldValue);
            if (_oldValue.equal(value) == false) {
                  let groupOwner = this._call_getGroup(groupName);
                  if (groupOwner == null)
                        return false;

                  let targetVector = groupOwner[propertyName];
                  if (targetVector == null) {
                        return false;
                  }

                  targetVector.x = value.x;
                  targetVector.y = value.y;
                  targetVector.z = value.z;
                  this._call_setInvalidateGroupPropertyValue(groupName, propertyName, value);

                  return true;
            }
            return false;
      }

      ///////////////////////////////////////////////////////


      ///////////////////////////////////////////////////////
      // 이벤트 처리
      protected _onClickHandler(event) {
            event.stopPropagation();
            // window.wemb.threeElements.threeLayer.mouseOveredComponent = this; cctv context click text do not erasse
            this.dispatchEvent(new WVMouseEvent(WVMouseEvent.CLICK, this, {
                  originEvent: event.origDomEvent,
                  intersect: event.intersect
            }));
            this.dispatchWScriptEvent("click", {originEvent: event.origDomEvent, intersect: event.intersect});
      }

      protected _onDblClickHandler(event) {
            event.stopPropagation();
            this.dispatchEvent(new WVMouseEvent(WVMouseEvent.DOUBLE_CLICK, this, {
                  originEvent: event.origDomEvent,
                  intersect: event.intersect
            }));
            this.dispatchWScriptEvent("dblclick", {originEvent: event.origDomEvent, intersect: event.intersect});

      }

      protected _onMouseOverHandler(event) {
            event.stopPropagation();
            if (this.outlineEnable) {
                  if(this.outlineElement !== null){
                        this.outlineElement.material.color = wemb.threeElements.threeLayer.mouseOverOutlineColor
                        this.outlineElement.material.needsUpdate = true;
                        this.toggleOutlineElement(true);
                  }
            }
            // window.wemb.threeElements.threeLayer.mouseOveredComponent = this; cctv context click text do not erasse
            this.dispatchEvent(new WVMouseEvent(WVMouseEvent.ROLL_OVER, this, {
                  originEvent: event.origDomEvent,
                  intersect: event.intersect
            }));
            this.dispatchWScriptEvent("mouseover", {originEvent: event.origDomEvent, intersect: event.intersect});
      }

      protected _onMouseOutHandler(event) {
            event.stopPropagation();
            if (this.outlineEnable) {
                  this.toggleOutlineElement(false);
            }
            // if(event.intersect === undefined)
            //       window.wemb.threeElements.threeLayer.mouseOveredComponent = null; cctv context click text do not erasse
            this.dispatchEvent(new WVMouseEvent(WVMouseEvent.ROLL_OUT, this, {
                  originEvent: event.origDomEvent,
                  intersect: event.intersect
            }));
            this.dispatchWScriptEvent("mouseout", {originEvent: event.origDomEvent, intersect: event.intersect});
      }

      protected _initEventListener(): void {
            this.registEventByType(this.appendElement, "click", this._func_call_click_handler, false);
            this.registEventByType(this.appendElement, "dblclick", this._func_call_dblclick_handler, false);
            this.registEventByType(this.appendElement, "mouseover", this._func_call_over_handler, false);
            this.registEventByType(this.appendElement, "mouseout", this._func_call_out_handler, false);
      }

      protected registEventByType(target: any, eventName: string, handler: Function, useCapture: boolean = false): void {
            if (this._domEvents)
                  this._domEvents.addEventListener(target, eventName, handler, useCapture)
      }

      protected removeEventByType(target: any, eventName: string, handler: Function, useCapture: boolean = false): void {
            if (this._domEvents)
                  this._domEvents.removeEventListener(target, eventName, handler, useCapture);
      }


      // object내부 mesh컬러 변경 처리 함수
      protected _validateColor() {
            let value = this.color;
            let emissive = this.emissive;

            function recursiveColor(parent) {
                  let i;
                  if(parent instanceof THREE.SkinnedMesh){
                        if (parent.material instanceof Array) {
                              for (i = 0; i < parent.material.length; i++) {
                                    parent.material[i].color.set(value);
                                    parent.material[i].emissive.set(emissive);
                              }
                        } else {
                              parent.material.color.set(value);
                              parent.material.emissive.set(emissive);
                        }
                  } else {
                        parent.traverse((child) => {
                              if (child instanceof THREE.Mesh) {
                                    if (child.material instanceof Array) {
                                          for (i = 0; i < child.material.length; i++) {
                                                child.material[i].color.set(value);
                                                child.material[i].emissive.set(emissive);
                                          }
                                    } else {
                                          child.material.color.set(value);
                                          child.material.emissive.set(emissive);
                                    }
                              }
                              if (child.children != null) {
                                    for (let i = 0; i < child.children.length; i++) {
                                          if (child.children[i] != null) {
                                                recursiveColor(child.children[i]);
                                          }
                                    }
                              }
                        });
                  }
            }

            recursiveColor(this.element);

      }


      // object내부 mesh컬러 변경 처리 함수
      protected _validateOpacity() {
            let nValue = this.opacity * 0.01;
            let hasTransparent = this.opacity >= 100 ? false : true;

            function recursiveOpacity(parent) {
                  let i;
                  parent.traverse((child) => {
                        if (child instanceof THREE.Mesh) {
                              let material;
                              if (child.material instanceof Array) {
                                    for (i = 0; i < child.material.length; i++) {
                                          material = child.material[i];
                                          material.transparent = hasTransparent;
                                          material.opacity = nValue;
                                          //texture에 alpha가 들어간 것은 예외
                                          if (child.name.indexOf("_A") > -1) {
                                                material.transparent = true;
                                          }
                                    }
                              } else {
                                    material = child.material;
                                    material.transparent = hasTransparent;
                                    //texture에 alpha가 들어간 것은 예외
                                    if (child.name.indexOf("_A") > -1) {
                                          material.transparent = true;
                                    }
                                    material.opacity = nValue;
                              }
                              //opacity에 따른 renderOrder를 조정
                              child.renderOrder = hasTransparent ? 2 : 1;
                        }
                        if (child.children != null) {
                              for (let i = 0; i < child.children.length; i++) {
                                    if (child.children[i] != null) {
                                          recursiveOpacity(child.children[i]);
                                    }
                              }
                        }
                  });
            }

            recursiveOpacity(this.element);
      }

      protected removePrevOutlineElement() {
            if (this.outlineElement != null) {
                  //this.removeEventByType(this._outlineElement, "mouseover", this._func_call_over_handler, false);
                  //this.removeEventByType(this._outlineElement, "mouseout", this._func_call_out_handler, false);
                  //this.appendElement.remove(this.outlineElement);
                  //MeshManager.disposeMesh(this.outlineElement);
                  this.outlineElement.geometry.dispose();
                  this.outlineElement.material.dispose();
                  this.appendElement.remove(this.outlineElement);
                  this._outlineElement.userData.boundingBox = null;
            }
      }

      // 아웃라인 생성에 사용 할 mesh설정
      protected _setOutlineElement() {
            this._outlineElement = this._element;
            //let reqAdd: boolean = this.element != this._outlineElement && this.appendElement != this._outlineElement;
            // element, appendElement를 그대로 사용할 경우 translate를 하지 않음.
            //this._updateOutlineGeometry();
            //this.registEventByType(this._outlineElement, "mouseover", this._func_call_over_handler, false);
            //this.registEventByType(this._outlineElement, "mouseout", this._func_call_out_handler, false);
            /*
            if (reqAdd) {
                  //outline이벤트 등록처리
                  //모델링 변경 시 아웃라인에 등록한 이벤트 혜지 필요.
                  //선택적인 등록은 안됨 > hover는 outline, click은 appendEelement append에서 click발생 시 동시에 outline에서 out이벤트 발생
                  this.outlineEnable = true;
                  this.registEventByType(this._outlineElement, "mouseover", this._func_call_over_handler, false);
                  this.registEventByType(this._outlineElement, "mouseout", this._func_call_out_handler, false);
                  this._outlineElement.material.visible = false;
                  this.appendElement.add(this._outlineElement);
            }*/
      }

      protected _updateOutlineGeometry() {
            // 이전 outline 정보 제거
            if (this.outlineElement) {
                  this.removePrevOutlineElement();
            }
            this._outlineElement.userData.boundingBox = this.createGroupEdge(this._outlineElement);
            this._outlineElement.userData.boundingBox.name = NWV3DComponent.OUTLINE_NAME;
            // 현재 선택아웃라인이 존재하면 제거 후 다시 추가
            let selectedOutline = this.getSelectOutline();
            if (selectedOutline) { 
                  this.toggleSelectedOutline(false);
                  this.toggleSelectedOutline(true);
            }
      }

      protected _validateOutline() {
            if (this._outlineElement == null) {
                  this._setOutlineElement();
            }
            this._updateOutlineGeometry();
      }

      onLoadPage() {

      }


}


export class WVVector implements IWVVector {

      _x: any = 0;
      _y: any = 0;
      _z: any = 0;

      get x():any { return this._x; }
      set x( nValue:any ){ this._x = +nValue; }

      get y():any{ return this._y; }
      set y( nValue:any ){ this._y = +nValue; }

      get z():any{ return this._z; }
      set z( nValue:any ){ this._z = +nValue; }

      constructor(x = 0, z = 0, y = 0) {
            if (arguments.length == 1 && typeof arguments[0] == "object") {
                  let tempObject: any = x;
                  this.x = tempObject.x;
                  this.y = tempObject.y;
                  this.z = tempObject.z;
            } else {
                  this.x = x;
                  this.y = y;
                  this.z = z;
            }

      }

      equal(value: WVVector): boolean {
            return value.x == this.x && value.y == this.y && value.z == this.z;
      }

      copy(value: WVVector): void {
            this.x = value.x;
            this.y = value.y;
            this.z = value.z;
      }

      sub(value: WVVector): void {
            this.x = this.x - value.x;
            this.y = this.y - value.y;
            this.z = this.z - value.z;
      }

      add(value: WVVector): void {
            this.x = this.x + value.x;
            this.y = this.y + value.y;
            this.z = this.z + value.z;
      }

      clone(): WVVector {
            return new WVVector(this.x, this.y, this.z);
      }

}


