import {THREE, TweenMax} from "../../utils/reference";

/**
 * OrbitControl 데코레이터 객체
 * Orbit의 모든 속성을 만들지는 않고
 * 현재 editor에 사용되는 속성만 get, set을 만들어 둠
 * 실제 속성을 제어하고 싶은 경우는 component로 접근하여 사용.
 * */
export default class OrbitDecorator {

      get staticMoving() {
            return this._controls.staticMoving;
      }

      set staticMoving(value) {
            this._controls.staticMoving = value;
      }


      /**
       *
       * @returns {any}
       */
      get panSpeed():number {
            return this._controls.panSpeed;
      }

      set panSpeed(value:number) {
            this._controls.panSpeed = value;
      }

      get zoomSpeed():number {
            return this._controls.zoomSpeed;
      }

      set zoomSpeed(value:number) {
            this._controls.zoomSpeed = value;
      }

      get rotateSpeed():number {
            return this._controls.rotateSpeed;
      }

      set rotateSpeed(value:number) {
            this._controls.rotateSpeed = value;
      }

      set screenSpacePanning( value:boolean ){
            this._controls.screenSpacePanning = value;
      }

      get screenSpacePanning():boolean {
            return this._controls.screenSpacePanning;
      }

      get hasChange():boolean {
            return this._hasChange;
      }

      set autoRotate( bValue:boolean ){
            this._controls.autoRotate = bValue;
      }

      get autoRotate():boolean{
            return this._controls.autoRotate;
      }

      set autoRotateSpeed( bValue:number ){
            this._controls.autoRotateSpeed = bValue;
      }

      get autoRotateSpeed():number {
            return this._controls.autoRotateSpeed;
      }

      private readonly _controls:any;
      private readonly _onStartControlHandler:Function;
      private readonly _onChangeControlHandler:Function;
      private _camera:THREE.Camera;
      private _hasChange:boolean;
      constructor( controls:any ){
            this._controls = controls;
            this._controls.minDistance = 50;
            this._controls.screenSpacePanning = true;
            this._onChangeControlHandler = this.onChangeControl.bind(this);
            this._onStartControlHandler = this.onStartControl.bind(this);
            this._controls.addEventListener("start", this._onStartControlHandler );
            this._controls.addEventListener("change", this._onChangeControlHandler );
            this._camera = controls.object;
            this.transitionInfo = {
                  prevCamera:null,
                  prevTarget:null
            };

      }

      onStartControl():void {
            this._hasChange = false;
      }

      onChangeControl():void {
            this._hasChange = true;
      }

      get component():any{
            return this._controls;
      }

      set target( objValue:THREE.Vector3 ){
            this._controls.target = objValue;
      }

      get target():THREE.Vector3{
            return this._controls.target;
      }

      set enabled( bValue:boolean ){
            this._controls.enabled = bValue;
      }

      get enabled():boolean {
            return this._controls.enabled;
      }

      set enablePan( bValue:boolean ){
            this._controls.enablePan = bValue;
      }

      get enablePan():boolean{
            return this._controls.enablePan;
      }

      set enableZoom( bValue:boolean ) {
            this._controls.enableZoom = bValue;
      }

      get enableZoom():boolean {
            return this._controls.enableZoom;
      }

      /**
       * control target 저장 위치 리턴
       * */
      get target0():THREE.Vector3 {
            return this._controls.target0;
      }

      /**
       * 카메라 저장 위치 리턴
       * */
      get position0():THREE.Vector3 {
            return this._controls.position0;
      }

      /**
       * 카메라 위치 리턴
       * */
      get position():THREE.Vector3{
            return this._controls.object.position;
      }

      get zoom0():THREE.Vector3 {
            return this._controls.zoom0;
      }

      set maxDistance( value:number ){
            this._controls.maxDistance = value;
      }

      get maxDistance(){
            return this._controls.maxDistance;
      }

      set minDistance( value:number ){
            this._controls.minDistance = value;
      }

      get minDistance(){
            return this._controls.minDistance;
      }

      reset(){
            this._controls.reset();
      }

      saveState(){
            this._controls.saveState();
      }


      dispose(){
            this._controls.removeEventListener("start", this._onStartControlHandler );
            this._controls.removeEventListener("change", this._onChangeControlHandler );
            this._controls.dispose();
      }



      private transitionTween:any;
      private transitionInfo:any;
      resetTransitionTween():void {
            if(this.transitionTween != null && this.transitionTween.isActive()){
                  this.transitionTween.pause();
                  this.transitionTween.kill();
            }
      }

      tweenTo( value:number, target:number, rate:number ):number {
            return value + ( target - value ) * rate;
      }

      transitionFocusInTarget( cameraPos:any, targetPos:any, duration:number = 1, completeCallback:Function = null, callbackParams:Array<any> = [] ){

            this.resetTransitionTween();
            let tweenObj:any = {t:0};
            let startCamera = this._camera.clone();
            let startControlTarget = this.target.clone();
            let time:number = duration;

            this.transitionTween = TweenMax.to( tweenObj, time, {
                  t:1,
                  onUpdate:()=>{
                        this._camera.position.set(
                              this.tweenTo( startCamera.position.x, cameraPos.x, tweenObj.t ),
                              this.tweenTo( startCamera.position.y, cameraPos.y, tweenObj.t ),
                              this.tweenTo( startCamera.position.z, cameraPos.z, tweenObj.t )
                        );

                        this.target.set(
                              this.tweenTo( startControlTarget.x, targetPos.x, tweenObj.t ),
                              this.tweenTo( startControlTarget.y, targetPos.y, tweenObj.t ),
                              this.tweenTo( startControlTarget.z, targetPos.z, tweenObj.t )
                        );

                  },
                  onComplete:() => {
                        if(completeCallback != null){
                              completeCallback.apply( null, callbackParams);
                        }
                  }

            });

      }

      transitionFocusOutTarget( cameraInfo:any, targetPos:any, duration?:number, completeCallback?:Function ){

            this.resetTransitionTween();
            if(!this._camera.position.equals(cameraInfo.position) && !this.target.equals( targetPos ) ) {

                  let tweenObj:any = {t:0};
                  let startControlTarget:THREE.Vector3 = this._controls.target;
                  let startCamera   = this._camera.clone();
                  let time:number   = duration;

                  this.transitionTween = TweenMax.to( tweenObj, time, {
                        t:1,
                        onUpdate:()=>{
                              this._camera.position.set(
                                    this.tweenTo( startCamera.position.x, cameraInfo.position.x, tweenObj.t ),
                                    this.tweenTo( startCamera.position.y, cameraInfo.position.y, tweenObj.t ),
                                    this.tweenTo( startCamera.position.z, cameraInfo.position.z, tweenObj.t )
                              );

                              this.target.set(
                                    this.tweenTo( startControlTarget.x, targetPos.x, tweenObj.t ),
                                    this.tweenTo( startControlTarget.y, targetPos.y, tweenObj.t ),
                                    this.tweenTo( startControlTarget.z, targetPos.z, tweenObj.t )
                              );
                        },
                        onComplete:() => {
                              if(completeCallback){
                                    completeCallback.apply( null, []);
                              }
                        }

                  });
            } else {
                  if(completeCallback){
                        completeCallback.apply(null, []);
                  }
            }

      }

      update() {
            this._controls.update();
      }



}
