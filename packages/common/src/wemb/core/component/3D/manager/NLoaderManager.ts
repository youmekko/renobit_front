import {THREE} from "../../../utils/reference";
import MeshManager from "./MeshManager";

export default class NLoaderManager {

      /*[ obj 로더 ]*/
      static objLoader: any = new THREE.OBJLoader();

      static mapPathDictionary : any = {};

      /* .obj 파일 로드한 것 재사용 위한 pool*/
      static MeshLoadedPool : Map<string, any> = new Map();
      static hasMeshLoaderPool(key:string) : boolean {
            return NLoaderManager.MeshLoadedPool.has(key);
      }
      static setMeshLoadedPool(key:string, value : any){
            NLoaderManager.MeshLoadedPool.set( key, value );
      }
      static getMeshLoadedPool(key:string){
            return NLoaderManager.MeshLoadedPool.get(key);
      }
      static getCloneMeshLoaderPool( key:string ):any {
            if(NLoaderManager.hasMeshLoaderPool(key)){
                  let source:any = NLoaderManager.getMeshLoadedPool(key);
                  if(source.type == "SkinnedMesh"){
                        return NLoaderManager.cloneMeshSkinned(source);
                  }
                  return NLoaderManager.clone( source.constructor, source );
            }
            return null;
      }
      static getMeshLoadedPoolKeys() : IterableIterator<string>{
            return NLoaderManager.MeshLoadedPool.keys();
      }

      /*[ MESH, TEXTURE 를 재사용하기 위한 풀 ] 위에꺼 두개 합쳐서 아래에 넣는다 로드 된 총 결과물*/
      static compLoaderPool: Map<String, any> = new Map();
      static hasLoaderPool( compName:string ):boolean {
            return NLoaderManager.compLoaderPool.has(compName);
      }
      static setLoaderPool( key:string, value:any ):void {
            if(!NLoaderManager.hasLoaderPool(key)){
                  if(value.type == "SkinnedMesh"){
                        NLoaderManager.compLoaderPool.set( key, NLoaderManager.cloneMeshSkinned(value) );
                  } else {
                        NLoaderManager.compLoaderPool.set( key, NLoaderManager.clone( value.constructor, value ) );
                  }
            }
      }
      static getLoaderPool( key:string ):any {
            return NLoaderManager.compLoaderPool.get(key);
      }
      static getCloneLoaderPool( key:string ):any {
            if(NLoaderManager.hasLoaderPool(key)){
                  let source:any = NLoaderManager.getLoaderPool(key);
                  if(source.type == "SkinnedMesh"){
                        return NLoaderManager.cloneMeshSkinned(source);
                  }
                  return NLoaderManager.clone( source.constructor, source );
            }
            return null;
      }

      /*[ texture 로더 ]*/
      static textureLoader: any = new THREE.TextureLoader();
      static texturePool:Map<String, any> =new Map();
      static registTexturePool( key:string, value:any ){
            if(NLoaderManager.hasTexture(key)) return;
            NLoaderManager.texturePool.set(key, value.clone());
      }

      static hasTexture( key:string):boolean{
            return NLoaderManager.texturePool.has(key);
      }

      static getTexturePool( key:string):any {
            let texture:any = NLoaderManager.texturePool.get(key);
            return texture.clone();
      }

      /*[ font 로더 ]*/
      static fontLoader: any = new THREE.FontLoader();
      static fontPool:Map<String, any> = new Map();

      /*[ json 로더 ]*/
      static jsonLoader: any = new THREE.JSONLoader();


      static clear() {

            NLoaderManager.compLoaderPool.forEach((poolObj: any) => {
                  MeshManager.disposeMesh(poolObj);
                  poolObj = null;
            });
            NLoaderManager.compLoaderPool.clear();
            NLoaderManager.texturePool.forEach((texture: any) => {
                  texture.dispose();
                  texture = null;
            });
            NLoaderManager.texturePool.clear();

            NLoaderManager.fontPool.forEach((font: any) => {
                  font = null;
            });
            NLoaderManager.fontPool.clear();
      }

      static clearMeshTextureLoaderPool(){
            NLoaderManager.texturePool.clear();
            NLoaderManager.MeshLoadedPool.forEach((poolObj: any) => {
                  MeshManager.disposeMesh(poolObj);
                  poolObj = null;
            });
            NLoaderManager.MeshLoadedPool.clear();
            NLoaderManager.mapPathDictionary = {};
      }



      /**
       * Mesh 복사 하기 전 타입 추출
       **/
      static getMeshContainerByType( type:string ) {
            return (type == "Group") ? new THREE.Group : new THREE.Mesh;
      }


      static cloneMeshSkinned( source:any ) {

            var sourceLookup = new Map();
            var cloneLookup = new Map();

            var clone = source.clone();

            parallelTraverse( source, clone, function ( sourceNode, clonedNode ) {

                  sourceLookup.set( clonedNode, sourceNode );
                  cloneLookup.set( sourceNode, clonedNode );

            } );

            clone.traverse( function ( node ) {

                  if ( ! node.isSkinnedMesh ) return;

                  var clonedMesh = node;
                  clonedMesh.material = cloneMatrial(node.material);
                  var sourceMesh = sourceLookup.get( node );
                  var sourceBones = sourceMesh.skeleton.bones;

                  clonedMesh.skeleton = sourceMesh.skeleton.clone();

                  clonedMesh.skeleton.bones = sourceBones.map( function ( bone ) {
                        return cloneLookup.get( bone );

                  } );
                  clonedMesh.bind( clonedMesh.skeleton, clonedMesh.bindMatrix );

            } );

            function cloneMatrial( material ){
                  let mat;
                  if(material instanceof Array){
                        mat = [];
                        for( let i=0; i<material.length;i++){
                              mat.push(material[i].clone());
                        }
                  } else{
                        mat = material.clone();
                  }
                  return mat;
            }

            function parallelTraverse( a, b, callback ) {

                  callback( a, b );

                  for ( var i = 0; i < a.children.length; i ++ ) {

                        parallelTraverse( a.children[ i ], b.children[ i ], callback );

                  }

            }

            return clone;

      }
      static cloneMeshSkinned2( source:any ) {

            var sourceLookup = new Map();
            var cloneLookup = new Map();

            var clone = source.clone();

            parallelTraverse( source, clone, function ( sourceNode, clonedNode ) {

                  sourceLookup.set( clonedNode, sourceNode );
                  cloneLookup.set( sourceNode, clonedNode );

            } );

            clone.traverse( function ( node ) {

                  if ( ! node.isSkinnedMesh ) return;

                  var clonedMesh = node;
                  clonedMesh.material = cloneMatrial(node.material);
                  var sourceMesh = sourceLookup.get( node );
                  var sourceBones = sourceMesh.skeleton.bones;

                  clonedMesh.skeleton = sourceMesh.skeleton.clone();

                  clonedMesh.skeleton.bones = sourceBones.map( function ( bone ) {
                        return cloneLookup.get( bone );

                  } );
                  clonedMesh.bind( clonedMesh.skeleton, clonedMesh.bindMatrix );

            } );

            function cloneMatrial( material ){
                  let mat;
                  if(material instanceof Array){
                        mat = [];
                        for( let i=0; i<material.length;i++){
                              mat.push(material[i].clone());
                        }
                  } else{
                        mat = material.clone();
                  }
                  return mat;
            }

            function parallelTraverse( a, b, callback ) {

                  callback( a, b );

                  for ( var i = 0; i < a.children.length; i ++ ) {

                        parallelTraverse( a.children[ i ], b.children[ i ], callback );

                  }

            }

            return clone;

      }


      /**
       * 반환된 Mesh를 재사용하기 위한 깊은 복사 함수
       **/
      static clone(constructor:any, copyObject: any) {
            let object;
            if (constructor === undefined) object = new THREE.Object3D();
            object = new constructor();
            object.name = copyObject.name;
            object.up.copy(copyObject.up);

            if (copyObject.material !== undefined && copyObject.geometry !== undefined) {

                  if (copyObject.material instanceof Array) {
                        object.material = [];
                        for (let i = 0; i < copyObject.material.length; i++) {
                              object.material.push(copyObject.material[i].clone());
                        }
                  } else {
                        object.material = copyObject.material.clone();
                  }

                  object.geometry          = copyObject.geometry.clone();
            }

            object.name = copyObject.name;
            object.up.copy( copyObject.up);

            object.position.copy(copyObject.position);
            object.quaternion.copy(copyObject.quaternion);
            object.scale.copy(copyObject.scale);
            object.renderDepth = copyObject.renderDepth;
            object.rotationAutoUpdate = copyObject.rotationAutoUpdate;
            object.matrix.copy(copyObject.matrix);
            object.matrixWorld.copy(copyObject.matrixWorld);
            object.matrixAutoUpdate = copyObject.matrixAutoUpdate;
            object.matrixWorldNeedsUpdate = copyObject.matrixWorldNeedsUpdate;
            object.type = copyObject.type;
            object.visible = copyObject.visible;
            object.castShadow = copyObject.castShadow;
            object.receiveShadow = copyObject.receiveShadow;
            object.frustumCulled = copyObject.frustumCulled;
            object.layers = copyObject.layers;
            object.renderOrder = copyObject.renderOrder;
            // object.userData = JSON.parse(JSON.stringify(copyObject.userData));

            if (copyObject.children.length > 0) {
                  let childSize = copyObject.children.length;
                  for (var i = 0; i < childSize; i++) {
                        let copyChild:any = copyObject.children[i];
                        let child = NLoaderManager.clone(  copyChild.constructor, copyChild);
                        //var child = NLoaderManager.clone(  copyChild.constructor, copyChild);
                        object.add(child);
                  }

            }

            return object;

      }


      static loadJson( jsonPath:string , mapPath:string):Promise<any>{
            function convertServerPath( str ){
                  if(str.indexOf("http")<= -1){
                        return wemb.configManager.serverUrl  + str;
                  }
            }

            return new Promise<any>((resolve, reject)=>{
                  if(NLoaderManager.hasLoaderPool(jsonPath)){
                        resolve(NLoaderManager.getCloneMeshLoaderPool(jsonPath));
                  }else{
                        NLoaderManager.jsonLoader.load( jsonPath, async (geometry, materials)=>{
                              NLoaderManager.setMeshLoadedPool(jsonPath, jsonPath);
                              geometry.computeFaceNormals();
                              geometry.computeVertexNormals();
                              let mesh = new THREE.SkinnedMesh( geometry, materials );
                              resolve(mesh);
                        }, undefined, (error)=>{
                              reject(error);
                        });
                  }
            });

      }

      static async loadObj( objPath:string, mapPath:string,):Promise<any> {
            return new Promise<any>(async (resolve, reject)=>{
                  let obj = NLoaderManager.getCloneMeshLoaderPool(objPath);
                  if(obj){
                        obj.children.forEach((child)=>{
                              let childName:string = child.name;
                              let pt2: string = mapPath + childName + ".png";
                              if(child instanceof THREE.Mesh){
                                    child.material = new THREE.MeshPhongMaterial();
                                    child.receiveShadow = true;
                                    child.castShadow = true;
                                    if( childName !== "base" && childName !== "area" ){
                                          if (childName.includes("_A")) {
                                                child.material.transparent = true;
                                                child.renderOrder = 2; // 뎁스 순서를 alpha가진 material을 크게
                                          }  else {
                                                child.renderOrder = 1;
                                          }
                                          try{
                                                child.material.map = NLoaderManager.getTexturePool(pt2);
                                                child.material.map.needsUpdate = true;
                                          }catch(e){

                                          }
                                    }else if( childName == "base"){
                                          child.visible =false;
                                    }
                              }
                        });
                        NLoaderManager.setLoaderPool(objPath, obj);
                        resolve(obj);
                  }
                  else{
                        console.log('not used cache')
                        NLoaderManager.objLoader.load( objPath, async (loadedObj) => {
                                    NLoaderManager.setMeshLoadedPool(objPath, loadedObj);
                                    const promise = loadedObj.children.map(async (child)=>{
                                          let childName:string = child.name;
                                          if( child instanceof THREE.Mesh ){
                                                child.material = new THREE.MeshPhongMaterial();
                                                child.receiveShadow = true;
                                                child.castShadow = true;
                                                if( childName !== "base" && childName !== "area" ){
                                                      if (childName.includes("_A")) {
                                                            child.material.transparent = true;
                                                            child.renderOrder = 2; // 뎁스 순서를 alpha가진 material을 크게
                                                      }  else {
                                                            child.renderOrder = 1;
                                                      }
                                                      try{
                                                            let path = mapPath + childName + ".png";
                                                            let texture = await NLoaderManager.loadTexture(path);
                                                            if(texture.image)
                                                                  NLoaderManager.registTexturePool(texture.image.src, texture);
                                                            child.material.map = texture;
                                                      } catch(e) {
                                                            console.log("texture-error",e );
                                                      }
                                                } else if( childName == "base"){
                                                      child.visible =false;
                                                }
                                          }
                                    });
                                    await Promise.all(promise);
                                    NLoaderManager.setLoaderPool(objPath, loadedObj);
                                    resolve(loadedObj);
                              }, undefined,
                              ( error )=>{
                                    reject(error);
                              });
                  }
            });
      }

      static loadTexture( path:string):Promise<any>{
            return new Promise<any>((resolve, reject )=>{
                  NLoaderManager.textureLoader.load( path,
                        (loadedTexture) => {
                              resolve(loadedTexture);
                        }, undefined,
                        (error) => {
                              resolve(new THREE.Texture());
                  });
            });

      }

      static generateTexture(color) {

            var canvas = document.createElement('canvas');
            canvas.width = 32;
            canvas.height = 32;

            var context = canvas.getContext('2d');

            context.fillStyle = color || "#ffffff";
            context.fillRect(0, 0, 32, 32);

            return canvas;

      }

      static loadImage(src, material) {

            var image = document.createElement('img');
            material.wireframe = false;
            material.opacity = 1;

            material.map.image = image;
            image.onload = function () {
                  material.map.needsUpdate = true;
            };

            image.src = src;
            return material;

      }
      static async loadGLTF(resourceVO: any, useCache:boolean = false) : Promise<any>{
            //테스트 로딩 되는거 확인 함, 캐싱처리 추가해야됨
            function convertServerPath( str ){
                  if(str.indexOf("http")<= -1){
                        return wemb.configManager.serverUrl  + str;
                  }
            }
            let obj = NLoaderManager.getCloneMeshLoaderPool(convertServerPath(resourceVO.path));
            if(obj){
                  return new Promise(((resolve, reject) => {
                     resolve(obj);
                  }))
            }else{
                  let loader = new THREE.GLTFLoader();
                  loader.setCrossOrigin('anonymous');
                  loader.setDRACOLoader(new THREE.DRACOLoader());

                  return new Promise(((resolve, reject) => {
                        loader.load(convertServerPath(resourceVO.path), function(data){
                              NLoaderManager.setMeshLoadedPool(convertServerPath(resourceVO.path), data.scene);
                              resolve(data.scene);
                        })
                  }))
            }


      }
      static async composeResource( resourceVO:any, useCache:boolean = false ):Promise<any> {

            let isObj:boolean = resourceVO.path.indexOf(".obj") > -1 ? true :  false;
            let loadFunc:Function         = isObj ? NLoaderManager.loadObj : NLoaderManager.loadJson;
            function convertServerPath( str ){
                  if(str.indexOf("http")<= -1){
                        return wemb.configManager.serverUrl  + str;
                  }
            }
            async function _loadTexture( path:string ):Promise<any>{
                  return new Promise(async (resolve, reject ) => {
                        let texture;
                        if(NLoaderManager.hasTexture(path)){
                              texture = NLoaderManager.getTexturePool(path);
                              //resolve(texture);
                        } else {
                              try{
                                    texture = await NLoaderManager.loadTexture( convertServerPath(path) );
                                    if(useCache){ NLoaderManager.registTexturePool(path, texture); }
                              }catch(e){
                                    reject(e);
                              }
                        }
                        resolve(texture);
                  });
            }
            async function setMaterialTexture( objMesh:any, prefix:string ) {
                  let material      = objMesh.material;
                  let numChildren   = material.length;
                  let materialName  = material[0].name;
                  let child;
                  for( let i=0; i<numChildren; i++) {
                        child = material[i];
                        child.skinning = true;
                        child.castShadow =true;
                        child.receiveShadow= true;
                        /* 투명 처리  */
                        if (materialName.includes("_A")) {
                              child.transparent = true;
                        }
                        try{
                              let path  =  prefix + materialName + ".png";
                              let texture = await _loadTexture(path);
                              child.map = texture;
                              child.map.needsUpdate=true;
                        }catch(error){
                              console.log("error", error);
                        }
                  }
            }
            if(!isObj){
                  //json은 NJSonLoader에서 자체적으로 로드한다. 일단 주석해둠

                  // return new Promise(async (resolve, reject)=>{
                  //       let loadedObj = NLoaderManager.getCloneLoaderPool(convertServerPath(resourceVO.path));
                  //       if (loadedObj == null ){
                  //             try{
                  //                   loadedObj = await loadFunc( convertServerPath(resourceVO.path) );
                  //                   if(loadedObj){
                  //                         await setMaterialTexture( loadedObj, resourceVO.mapPath );
                  //                   }
                  //                   if(useCache){ NLoaderManager.setLoaderPool(convertServerPath(resourceVO.path), loadedObj ); }
                  //             }catch(error){
                  //                   reject(error);
                  //             }
                  //       }
                  //       loadedObj.rotation.x = -Math.PI/2;
                  //       resolve(loadedObj);
                  // });
            }
            else {
                  let loadedObj;

                  //obj 로드 할 때
                  if(NLoaderManager.hasLoaderPool(convertServerPath(resourceVO.path))){
                        loadedObj = NLoaderManager.getCloneLoaderPool(convertServerPath(resourceVO.path));
                  }else
                        loadedObj = await loadFunc(convertServerPath(resourceVO.path), convertServerPath(resourceVO.mapPath));
                  return loadedObj;
            }
      }
       static loadFont( path:string ):Promise<any> {
            return new Promise<any>( (resolve, reject) =>{
                        NLoaderManager.fontLoader.load( path, (response)=>{
                              resolve(response);
                        }, undefined, (error)=>{
                              reject(error);
                        });
                  }
            );
      }

      static hasFont( key:string ):boolean {
            return NLoaderManager.fontPool.has(key);
      }

      static getFontPool( key:string ):any {
            if(NLoaderManager.hasFont(key)){
                  return NLoaderManager.fontPool.get(key);
            }
            return null;
      }

      static registFont( key:string, value:any ):void {
            if(!NLoaderManager.hasFont(key)){
                  NLoaderManager.fontPool.set(key, value);
            }
      }


}
