import IToolTipProvider = WV.Provider.IToolTipProvider;
import IToolTipClient = WV.Provider.IToolTipClient;
import {WORK_LAYERS} from "../../../../wv/layer/Layer";
import {CPLogger} from "../../../utils/CPLogger";
import {TweenMax} from "../../../utils/reference";

export class WVToolTipProvider  implements IToolTipProvider {

      static STYLE_SELECTOR:string = "wv_tooltip";
      private _clients:Array<IToolTipClient>
      private _currentClient:IToolTipProvider;
      private _onMouseOverHandler:Function;
      private _onMouseOutHandler:Function;
      private _onMouseMoveHandler:any;
      public customShowHandler:Function;
      public customHideHandler:Function;

      private _enabled:boolean;
      private _threes:any;
      private _toolTip:any;
      private _offset:any;
      private _effectTween:any;
      constructor(threes:any ){
            this._threes  = threes;
            this._clients = [];
            let skinItem:any =$("<span>");
            skinItem.addClass(WVToolTipProvider.STYLE_SELECTOR);
            skinItem.css({position:"absolute",
                  left:"0",
                  top:"0",
                  display:"inline-block",
                  pointerEvents:"none",
                  padding:"10",
                  backgroundColor:"#3351ED",
                  "white-space":"nowrap",
                  "z-index":"10000",
                  "border-radius":"3px",
                  color:"#ffffff" });
            this._toolTip     =skinItem;
            this._onMouseOverHandler = this.onMouseOver.bind(this);
            this._onMouseOutHandler  = this.onMouseOut.bind(this);
            this._onMouseMoveHandler = this.onMouseMove.bind(this);
            this.customHideHandler = this.onHideToolTip;
            this.customShowHandler = this.onShowToolTip;
            this._offset = {x:10, y:10};
            this._toolTip.hide();

      }

      set offset(offsetInfo:any) {
            this._offset = offsetInfo;
      }

      set skin( template:any ) {
            if(this._toolTip){
                  this._toolTip.off();
                  this._toolTip.remove();
            }
            this._toolTip = $(template);
      }

      get skin():any {
            return this._toolTip;
      }

      protected has( client:IToolTipClient ):boolean {
            if( !this.checkImplements(client, "toolTipTarget")){
                  CPLogger.log( CPLogger.getCallerInfo(this, "add") ,[ client.constructor.name ,"툴팁 사용을 위한 인터페이스 구현이 안되어 있습니다."] );
                  return false;
            }
            return this._clients.indexOf(client) > -1 ? true : false;
      }

      add( client:IToolTipClient ):boolean {
            if(this.has(client)){
                  return false;
            }
            this._clients.push(client);
            this.registEvent(client);
            return true;
      }

      remove( client:IToolTipClient ):boolean {
            if(this.has(client)){
                  let idx:number = this._clients.indexOf(client);
                  this._clients.splice(idx, 1);
                  this.removeEvent(client);
                  return true;
            }
            return false;
      }

      checkImplements( instance:any, methodName:string ):boolean {
            return (methodName in instance);
      }

      registEvent( client:IToolTipClient ):void {
            if( client.layerName == WORK_LAYERS.THREE_LAYER ){
                  this._threes.domEvents.addEventListener( client.toolTipTarget, "mouseover", this._onMouseOverHandler, false );
                  this._threes.domEvents.addEventListener( client.toolTipTarget, "mouseout", this._onMouseOutHandler, false );
            } else {
                  $(client.toolTipTarget).on("mouseover", this._onMouseOverHandler );
            }
      }


      removeEvent( client:IToolTipClient ):void {
            if( client.layerName == WORK_LAYERS.THREE_LAYER ){
                  this._threes.domEvents.removeEventListener( client.toolTipTarget, "mouseover", this._onMouseOverHandler, false );
                  this._threes.domEvents.removeEventListener( client.toolTipTarget, "mouseout", this._onMouseOutHandler, false );
            } else {
                  $(client.toolTipTarget).on("mouseout", this._onMouseOutHandler );
            }
      }

      draw( parent:any ){
            $(parent).append(this._toolTip);
      }

      onMouseOver( event:any ):void  {
            let eventTarget:any;
            if( event.target.type ){
                  eventTarget =  event.target.userData.owner
            } else {
                  eventTarget = event.currentTarget;
            }
            if(this._currentClient != eventTarget ){
                  this._currentClient = eventTarget;
            }
      }

      onMouseOut( event:any ):void  {
            let eventTarget:any;
            if( event.target.type ){
                  eventTarget =  event.target.userData.owner
            } else {
                  eventTarget = event.currentTarget;
            }
            if(this._currentClient == eventTarget ){
                  this._currentClient = null;
            }
      }

      onMouseMove( event:any ):void {
            if(!this._enabled) return;

            let vo:any = event.origDomEvent || event;
            vo.toolTip = this._toolTip;
            vo.offset  = this._offset;
            if(this._currentClient != null && this._currentClient.useToolTip ) {
                  if(this._currentClient.toolTipMessage.length < 1 ) return;
                  this._toolTip.html(this._currentClient.toolTipMessage);
                  this.customShowHandler.apply(this, [vo]);
            }  else {
                  this.customHideHandler.apply( this, [vo]);
            }
      }


      /*
      *     오버라이드 가능 함수
      *     등장 효과 변경 시 사용.
      * */
      onShowToolTip( info:any ){
            let tw:number = info.toolTip.outerWidth();
            let th:number = info.toolTip.outerHeight();
            if( info.toolTip.is(":hidden")){
                  info.toolTip.css({opacity:"0"}).show();
            }
            let off:any  =  this._toolTip.parent().offset();
            let ox:number = off.left;
            let oy:number = off.top;
            info.toolTip.css({"left": (info.pageX-ox) - tw/2, "top":(info.pageY-oy)-(th+info.offset.y)});
            if(this._effectTween != null){
                  this._effectTween.pause();
            }
            this._effectTween = TweenMax.to(info.toolTip, .3, {autoAlpha:1});
      }

      onHideToolTip( info:any ){
            this._effectTween = TweenMax.to(info.toolTip, .3, {autoAlpha:0, delay:.1});
      }

      destroy(){
            this._clients.forEach((client)=>{
                  this.removeEvent(client);
            });
            this._clients = null;
            this._onMouseOverHandler = null;
            this._onMouseOutHandler = null;
            this.customShowHandler = null;
            this.customHideHandler = null;
            if(this._effectTween){
                  this._effectTween.pause();
                  this._effectTween = null;
            }
            this._toolTip.empty();
            this._toolTip.off();
            this._toolTip.remove();
            this._toolTip = null;
      }

      set enabled( value:boolean ) {
            if(value != this._enabled){
                  this._enabled = value;
                  if(!this._enabled){
                        this.onHideToolTip({toolTip:this.skin});
                        window.document.removeEventListener( "mousemove", this._onMouseMoveHandler );
                  } else {
                        window.document.addEventListener( "mousemove", this._onMouseMoveHandler );
                  }
            }
      }

      get enabled():boolean{
            return this._enabled;
      }


}


export class WVToolTipManager {

      private static provider:WVToolTipProvider;
      static create( threes:any, container:HTMLDivElement ){
            if(WVToolTipManager.provider == null){
                  WVToolTipManager.provider = new WVToolTipProvider(threes);
                  WVToolTipManager.provider.draw(container);
            }
      }

      static add( client:any ):boolean{
            return WVToolTipManager.provider.add(client);
      }

      static remove( client:any ):boolean{
            return WVToolTipManager.provider.remove(client);
      }

      static setSkin( skin:any ):void {
            WVToolTipManager.provider.skin = skin;
      }

      static setStyle(styleObj:any):void{
            WVToolTipManager.provider.skin.css(styleObj);
      }

      static getSkin():any{
            return WVToolTipManager.provider.skin;
      }


      static setEnabled( bValue:boolean ):void {
            WVToolTipManager.provider.enabled = bValue;
      }

      static overrideShowToolTip( func:Function ):void {
            WVToolTipManager.provider.customShowHandler = func;
      }

      static overrideHideToolTip( func:Function ):void {
            WVToolTipManager.provider.customHideHandler = func;
      }

      static destroy():void {
            if(WVToolTipManager.provider != null ){
                  WVToolTipManager.provider.destroy();
                  WVToolTipManager.provider = null;
            }
      }

}
