import WVComponent from "../../WVComponent";
import {IWV3DComponentStyleManager} from "../../interfaces/ComponentInterfaces";
declare var THREE;

export default class WV3DComponentStyleManager implements IWV3DComponentStyleManager{

	protected _propertySetterMap: Map<string, Function> = new Map();
	private _styleList:any = {};
	private _element:any;
	private _comInstance:WVComponent;

	private _invalidateProperty:boolean= false;
	get invalidateProperty():boolean {
		return this._invalidateProperty;
	}
	set invalidateProperty(value:boolean) {
		this._invalidateProperty = value;
	}
	constructor(comInstance:WVComponent) {
		this._comInstance = comInstance;
		this._element = comInstance.element;
		this.onRegisterSetter();
	}

      public immediateProperties(properties:any){

      }

	private onRegisterSetter(){
		this._propertySetterMap.set("color", this._color.bind(this));
	}


	private _color(value:string){
		return new THREE.Color(value);
	}

	public setProperty(propertyName:string, value:any):boolean{
		this._styleList[propertyName]= value;

		this._invalidateProperty = true;
		this._comInstance.invalidateProperties();

		return true;
	}
	public getProperty(propertyName:string):any{
		if(this._styleList.hasOwnProperty(propertyName)==true){
			return this._styleList[propertyName];
		}
		return null;
	}

	public setProperties(properties:any, clear:boolean=false):boolean{
		if(clear==true){
			let list = Object.getOwnPropertyNames(this._styleList);
			for(let i=0;i<list.length;i++){
				delete this._styleList[list[i]];
			}
		}
		let list = Object.getOwnPropertyNames(properties);
		for(let i=0;i<list.length;i++){
			let propertyName = list[i];
			this.setProperty(propertyName, properties[propertyName]);
		}

		return true;
	}
	public getProperties():any{
		return this._styleList;
	}




	public validateProperty():void{
		if(this.invalidateProperty){
			if(this._styleList!=null){
				let styleNameList = Object.keys(this._styleList);
				styleNameList.forEach((styleName)=>{
					if(this._propertySetterMap.has(styleName)==true) {
						let setFunction:Function =  this._propertySetterMap.get(styleName);
						this._element.material[styleName] = setFunction(this._styleList[styleName]);
					}
				})
			}

		}
	}

	public clearInvalidate():void{

      }


      public destroy(){

      }




}
