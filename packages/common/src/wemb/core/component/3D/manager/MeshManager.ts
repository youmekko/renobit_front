declare var THREE;

export default class MeshManager {

      static geometryList: Map<String, any> = new Map()
            .set("Geometry", new THREE.Geometry())
            .set("BoxBufferGeometry", new THREE.BoxBufferGeometry(1, 1, 1))
            .set("BoxGeometry", new THREE.BoxGeometry(1, 1, 1))
            .set("CircleBufferGeometry", new THREE.CircleBufferGeometry(1, 8))
            .set("CircleGeometry", new THREE.CircleGeometry(1, 8))
            .set("ConeBufferGeometry", new THREE.ConeBufferGeometry(1, 1))
            .set("ConeGeometry", new THREE.ConeGeometry(1, 1))
            .set("CylinderBufferGeometry", new THREE.CylinderBufferGeometry(1, 1, 1, 8, 1, false))
            .set("CylinderGeometry", new THREE.CylinderGeometry(1, 1, 1, 1))
            .set("OctahedronBufferGeometry", new THREE.OctahedronBufferGeometry(1, 0))
            .set("OctahedronGeometry", new THREE.OctahedronGeometry(10, 0))
            .set("PlaneBufferGeometry", new THREE.PlaneBufferGeometry(1, 1, 8))
            .set("PlaneGeometry", new THREE.PlaneGeometry(1, 1))
            .set("RingBufferGeometry", new THREE.RingBufferGeometry(1, 1))
            .set("RingGeometry", new THREE.RingGeometry(1, 2, 8))
            .set("SphereGeometry", new THREE.SphereGeometry(1, 8, 8))
            .set("TorusGeometry", new THREE.TorusGeometry(1, 0.4, 8, 6));

      static materialList: Map<String, any> = new Map()
            .set("MeshBasicMaterial", new THREE.MeshBasicMaterial())
            .set("MeshPhongMaterial", new THREE.MeshPhongMaterial())
            .set("MeshNormalMaterial", new THREE.MeshNormalMaterial());

      static getGeometry(name: String) {

            if (MeshManager.geometryList.has(name)) {
                  return MeshManager.geometryList.get(name);
            } else {
                  return false;
            }

      }

      static getMaterial(name: String) {

            if (MeshManager.materialList.has(name)) {

                  return MeshManager.materialList.get(name);

            } else {

                  return false;

            }

      }

      static gridHelper(sizeX: number, stepX: number, sizeZ: number, stepZ: number) {

            var x = Math.round(sizeX / stepX);
            var y = Math.round(sizeZ / stepZ);

            sizeX = x * stepX;
            sizeZ = y * stepZ;


            var geometry = new THREE.Geometry();
            var material = new THREE.LineBasicMaterial({vertexColors: THREE.VertexColors, transparent:true, opacity:0.4});

            var color = new THREE.Color("#e2e2e2");

            for (var i = -1 * sizeX; i <= sizeX; i += stepX) {

                  geometry.vertices.push(
                        new THREE.Vector3(i, 0, -1 * sizeZ), //x Y z
                        new THREE.Vector3(i, 0, sizeZ) //x Y z
                  );

                  //var color = i === 0 ? color1 : color2;
                  geometry.colors.push(color, color, color, color);

            }

            for (var i = -1 * sizeZ; i <= sizeZ; i += stepZ) {

                  geometry.vertices.push(
                        new THREE.Vector3(-1 * sizeX, 0, i), //x Y z
                        new THREE.Vector3(sizeX, 0, i) //x Y z
                  );

                  //var color = i === 0 ? color1 : color2;
                  geometry.colors.push(color, color, color, color);

            }


            let oMesh = new THREE.LineSegments(geometry, material);
            oMesh.renderOrder = -1;
            oMesh.name = "gridHelper";
            return oMesh;

      }


      static requestMergeGeometry(source:any, exceptionList:any = []):any {
            function mergeGeometry( base:any, target:any ){
                  target.updateMatrix();
                  let geo:any = target.geometry;
                  if (geo instanceof THREE.BufferGeometry) {
                        geo = new THREE.Geometry().fromBufferGeometry(geo);
                  }
                  base.merge(geo, target.matrix);
                  return base;
            }

            function checkValidate(name){
                  let max = exceptionList.length;
                  let filter;
                  for( var i=0; i<max; i++){
                        filter=  exceptionList[i];
                        if(name.indexOf(filter) > -1){
                              return false;
                        }
                  }
                  return true;
            }

            let mergedGeometry = new THREE.Geometry;
            if(source instanceof THREE.SkinnedMesh ){
                  mergedGeometry = mergeGeometry(mergedGeometry, source);
            } else {
                  source.traverse((child: any) => {
                        if (child instanceof THREE.Mesh) {
                              if(checkValidate(child.name)){
                                    mergedGeometry = mergeGeometry(mergedGeometry, child);
                              }
                        }
                  });
            }
            return mergedGeometry;
      }

      static requestMergeMesh( source:any, exceptionList:any = [] ):any {
            let mergedGeometry  = MeshManager.requestMergeGeometry(source, exceptionList);
            let mesh:any = new THREE.Mesh(mergedGeometry, new THREE.MeshBasicMaterial({color:0xff0000}));
            mesh.name = "hit_area";
            return mesh;
      }


      static disposeMesh( object: any ) {

            if(object.dispose === "function"){
                  object.dispose();
            }

            if (object.geometry) {
                  object.geometry.dispose();
                  object.geometry = null;
            }
            if (object.material) {
                  if(object.material instanceof Array){
                        let m;
                        for(let i=0;i<object.material.length;i++){
                              m = object.material[i];
                              if(m.map) {
                                    m.map.dispose();
                              }
                              m.dispose();
                        }
                  } else {
                        if (object.material.map) {
                              object.material.map.dispose();
                        }
                        object.material.dispose();
                  }

                  object.material = null;
            }
            if (object.texture) {
                  object.texture.dispose();
                  object.texture = null;
            }
            if (object.boneTexture) {
                  object.boneTexture.dispose();
                  object.boneTexture = null;
            }
            if( object.children != null ) {
                  /*let max = object.children.length
                  for( let i=0; i<object.children.length;i++){
                        if(object.children[i] != null){
                              MeshManager.disposeMesh(object.children[i]);
                              object.children[i] = null;
                        }
                  }*/
                  while(object.children.length){
                        let child = object.children[0];
                        if(child instanceof THREE.Mesh ){
                              MeshManager.disposeMesh(child);
                        }
                        object.remove(child);
                  }
            }
      }


      /**
       * 해당 Mesh에 BoxHelper생성하는 함수
       **/
      static procCreateBoxHelper(obj: any) {

            var boxHelper = new THREE.BoxHelper(obj, 0xffff00);
            boxHelper.name = "BoxHelper";
            boxHelper.update();
            boxHelper.material.visible = 0;
            obj.add(boxHelper);

      }

      /**
       * BoxHelper에 사용하는 마우스 이벤트 바인딩 ( OVER, OUT )
       **/
      static mouseEvent(e: any, flag: String) {
            let target = e.target.getObjectByName("BoxHelper");
            target.update();
            target.material.visible = flag == "out" ? 0 : 1;

      }

}
