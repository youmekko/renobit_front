import NWV3DComponent from "./NWV3DComponent";
import NLoaderManager from "./manager/NLoaderManager";
import {THREE} from "../../utils/reference";
import MeshManager from "./manager/MeshManager";
import ThreeUtil from "../../utils/ThreeUtil";
import {CPLogger} from "../../utils/CPLogger";

/*
*  resource속성을 이용해 처음부터 모델링 데이터를 가져오는 컴포넌트에 사용
*  ex ) 자산 컴포넌트
* */
export default class WV3DResourceComponent extends NWV3DComponent {

      constructor() {
            super();
      }

      protected _resourceLoaded: boolean;
      protected _resourceInfo: any;

      _onCreateProperties() {
            super._onCreateProperties();
            this._isResourceComponent = true;
            this._resourceLoaded = false;
            this._resourceInfo = this.getResourceInfo();
      }

      _onCreateElement(): void {
            this._element = new THREE.Mesh(MeshManager.getGeometry("BoxGeometry").clone(), MeshManager.getMaterial("MeshBasicMaterial").clone());
            this._element.material.wireframe = true;
            // 뷰어에서 리소스 로드시 와이어 프레임이 보이는 것을 방지하기 위해 에디터에서만 추가
            if (this.isEditorMode) {
                  this.appendElement.add(this._element);
            }
      }


      get resource(): any {
            return this.getGroupProperties("resource");
      }

      protected removePrevResource() {
            MeshManager.disposeMesh(this._element);
            this.appendElement.remove(this._element);
            this.removePrevOutlineElement();
            this._resourceLoaded = false;
      }

      protected async setChildMeshTexture(objMesh: any, prefix: string) {

            let numChildren = objMesh.children.length;
            let child;
            for (let i = 0; i < numChildren; i++) {
                  child = objMesh.children[i];
                  if (child instanceof THREE.Mesh) {
                        let childName = child.name;
                        child.receiveShadow = true;
                        child.castShadow = true
                        if (childName !== "base" && childName !== "area") {
                              if (childName.includes("_A")) {
                                    child.material.transparent = true;
                                    child.material.depthWrite = false;
                              }
                              try {
                                    let path = prefix + childName + ".png";
                                    let texture;
                                    if (NLoaderManager.hasTexture(path)) {
                                          texture = NLoaderManager.getTexturePool(path);
                                    } else {
                                          texture = await NLoaderManager.loadTexture(this.convertServerPath(path));
                                          NLoaderManager.registTexturePool(path, texture);
                                    }
                                    child.material.map = texture;
                              } catch (e) {
                                    console.log("texture-error", e);
                              }
                        }
                  }
            }
      }

      protected convertServerPath(str) {
            if (str.indexOf("http") <= -1) {
                  return wemb.configManager.serverUrl + str;
            }
      }


      /*
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
            로드된 obj 구성 처리 확장 컴포넌트에서 각자 override 필요
      ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
      * */
      protected composeResource(loadedObj: any): void {
            let objSize: THREE.Vector3 = ThreeUtil.getObjectSize(loadedObj);
            this._element = loadedObj;
            this._element.name = this.name + "_resource";
            this.appendElement.add(this._element);
            this._validateElementSize(objSize);
      }


      getResourceFileName(info) {
            return info.substring(info.lastIndexOf("/") + 1);
      }

      getResourceInfo(): any {
            let resourceInfo = null;
            if (this.resource != null) {
                  if(!this.resource.hasOwnProperty("modeling") || this.resource.hasOwnProperty("mappingKey")) {
                        CPLogger.log("##WV3DAssetComponent", "No resource.modeling Info");
                        return null;
                  }
                  resourceInfo = this.resource.modeling;
                  if (resourceInfo) {
                        let key: string = this.resource.mappingKey || "default";
                        resourceInfo = resourceInfo[key];
                  }
            } else {
                  resourceInfo = this.getGroupPropertyValue("setter", "resource");
                  console.warn("setter를 통한 resource설정은 지원하지 않을 예정입니다. resource 그룹을 이용하세요.");
            }
            if(resourceInfo){
                  if (!resourceInfo.path) {
                        resourceInfo.path = resourceInfo.objPath;
                  }
                  if (!resourceInfo.name) {
                        resourceInfo.name = this.getResourceFileName(resourceInfo.path);
                  }
                  if (resourceInfo.mapPath.substr(-1) != "/") {
                        resourceInfo.mapPath += "/";
                  }
            }

            return resourceInfo;
      }

      async _validateResource() {
            // rack obj 로드 처리
            try {
                  this.removePrevResource();
                  let loadedObj = await NLoaderManager.composeResource(this._resourceInfo, true);
                  return loadedObj;
            } catch (error) {
                  throw error;
            }

      }


      async startLoadResource() {
            try {
                  let loadedObj = await this._validateResource();
                  this.composeResource(loadedObj);
                  this._onValidateResource();
            } catch (e) {
                  CPLogger.log("startLoadResource-error", e);
            } finally {
                  this.resourceBus.$emit(window.WeMB.WVComponentEvent.LOADED_RESOURCE, this);
            }
      }

      // object내부 mesh컬러 변경 처리 함수
      protected _validateColor() {
            if (this._resourceLoaded) {
                  super._validateColor();
            }
      }

      // object내부 mesh컬러 변경 처리 함수
      protected _validateOpacity() {
            if (this._resourceLoaded) {
                  super._validateOpacity();
            }
      }

      // resource사용 컴포넌트 로드 완료 후 초기화 처리
      protected _onValidateResource() {
            this._resourceLoaded = true;
            this._validateColor();
            this._validateOpacity();
            this._validateSize();
            this._validateOutline();
      }

      onLoadPage() {

      }

}
