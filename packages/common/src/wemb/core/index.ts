import component from './component'
import display from './display'
import events from './events'
import extension from './extension'
import freeTransform from './freeTransform'
import geom from './geom'
import utils from './utils'

const core = {
    component,
    display,
    events,
    extension,
    freeTransform,
    geom,
    utils
}

export default core