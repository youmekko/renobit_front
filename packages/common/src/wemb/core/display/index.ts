import * as DisplayInterfaces from './interfaces/DisplayInterfaces'
import WVDisplayObject from './WVDisplayObject'
import WVDisplayObjectContainer from './WVDisplayObjectContainer'

const display = {
    interfaces: {
        DisplayInterfaces
    },
    WVDisplayObject,
    WVDisplayObjectContainer
}

export default display