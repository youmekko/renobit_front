import {IEventDispatcher, IWVEvent} from "../../events/interfaces/EventInterfaces";
import Point from "../../geom/Point";

export type MethodQueueElement = {
      method: Function,
      args: Array<any>
}

/*
메타 정보 프로퍼티(빈 타입, 주로 타입을 맞추기 위해 사용)
 */
export interface IMetaProperties {

}

/*
실제 프로퍼티 목록을 가지게되는 타입(빈 타입, 주로 타입을 맞추기 위해 사용)
 */
export interface  IProperties {

}


export interface IDisplayObjectProperties extends  IProperties{
      id:string;
      name:string;
      type:string;
      master:string;
}


export interface IDeferredAble {
      invalidateProperty: boolean      // 속성 갱신 여부 참조

      invalidateProperties(): void
      invalidateImmediateProperties(): void;
      validateCallLater<T>(method: Function, args?: T[]): void
}
////////////////////////////////






////////////////////////////////
// 프로퍼티 변경
export interface IPropertyManager {
      setGroupProperties(groupName:string, properties:any):boolean;
      getGroupProperties(groupName:string):any;
      setGroupPropertyValue(groupName:string, propertyName:string, value:any):boolean;
      getGroupPropertyValue(groupName:string, propertyName:string):any;
}
////////////////////////////////





////////////////////////////////
// 웹비쥬얼 라이저에 사용 컴포넌트 인터페이스 정의
export interface IWVDisplayObject extends IDeferredAble, IEventDispatcher, IPropertyManager {

      properties: any            // properties
      readonly element: any;     // 돔 엘레먼트 참조
      readonly appendElement:any;

      readonly parent:IWVDisplayObjectContainer;
      readonly initialize: boolean // 컴포넌트 초기화 여부 참조

      readonly id:string;
      name:string;


      //toString(): string               // 객체 설명용
      //////////////

      create(initProperties: IMetaProperties):boolean;
      immediateUpdateDisplay(event:boolean):void;

      setParent(parent:IWVDisplayObjectContainer);    // 수동으로 parent를 설정하는 기능.
      /////////////////////////////


      onBeforeAdded(event:IWVEvent):void // 컨테이너 추가 전 실행
      onAdded(event: IWVEvent): void   // 컨테이너 추가 후( 화면에는 안보이고 컨테이너에 추가된 상태 )


      onRemoveBefore(): void   // 제거 전
      onRemove(event: IWVEvent): void   // 제거
      onDestroyBefore(): void   // 삭제 전( 컴포넌트 코어 제거 직전 호출 )

      toString(): string               // 객체 설명용



      // 마우스 이벤트
      setMouseEventEnabled(enabled:boolean);

}


// 웹 비쥬얼라이즈 컨테이너 컴포넌트 자식요소 추가,삭제,뎁스 처리 기능 추가됨.
export interface IWVDisplayObjectContainer extends IWVDisplayObject {

      numChildren: number  // 자식 요소 수
      addChild(displayObject: IWVDisplayObject): IWVDisplayObject                         // 추가
      addChildAt(displayObject: IWVDisplayObject, index: number): IWVDisplayObject         // 특정 index 추가
      removeChild(displayObject: IWVDisplayObject): IWVDisplayObject                      // 삭제
      removeChildAt(index: number): IWVDisplayObject      // 특정 index 삭제
      removeChildAll(): void                                                  // 자식 요소 모두 제거
      swapChildren(displayObjectA: IWVDisplayObject, displayObjectB: IWVDisplayObject): void   // 자식 요소 뎁스 변경
      setChildIndex(displayObject: IWVDisplayObject, index: number): IWVDisplayObject             // 자식 요소 인덱스 설정
      getChildIndex(displayObject: IWVDisplayObject): number                          // 자식 요소 인덱스 반환
      getChildByName(name: string): IWVDisplayObject                              // 자식 요소 이름으로 찾기
      getChildAt(nIndex: number): IWVDisplayObject                                // 자식 요소 index로 찾기
      hasChild(displayObject: IWVDisplayObject): boolean                              // 자식 요소 포함여부

}



/**
 WV컴폰넌트가 아닌 컴포넌트를 추가하고 심플한 속성 갱신을 위한 인터페이스
 WVTransformTool ControlCursor에 사용
 */
export interface IDrawDisplay {
      draw( parent:any, style?:any ):void     // 추가
      clear():void                            // 삭제
}

export interface IReDrawDisplay {
      reDraw( info?:any ):void             // 갱신
}

export interface IPointDisplay  {
      pointCount:number;
      points:Array<Point>
      readonly paths:string
      insertPoint?( point?:Point ):Point
      removePoint?( point?:Point ):Point
}

export interface IPointGuideDisplay {
      readonly controlPaths:string;
}


export enum BROWSER_TYPE {
      IE          = "Internet Explorer",
      CHORME      = "Chrome",
      FIRE_FOX    = "Firefox",
      OPERA       = "Opera",
      SAFARI      = "Safari"
}
