/**
 * 스피너
 * */
export default class Spinner {

      private static _instance: Spinner;
      private _skin:any;
      private constructor() {
            if (!Spinner._instance) {
                  Spinner._instance = this;
                  this._skin = $('<div class="cssload-container"> <div class="cssload-speeding-wheel"></div> </div>');
                  this._skin.hide();
            }
            return Spinner._instance;
      }

      public static getInstance(): Spinner {
            return Spinner._instance || new Spinner();
      }

      setStyle(styleInfo){
            $(".cssload-speeding-wheel").css(styleInfo);
      }

      show(parent:any){
            if(!$.contains(parent, this._skin[0])){
                  $(parent).append(this._skin);
            }
            this._skin.show();
      }

      hide(){
            this._skin.hide();
            this._skin.remove();
      }


}
window.Spinner = Spinner;
