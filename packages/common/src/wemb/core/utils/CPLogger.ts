export let CPLogger = {

      LOG_INDEX   :0,
      LOG_TYPE    :"log",
      LOG         :"log",
      WARN        :"warn",
      ERROR       :"error",
      TRACE       :"trace",
      LOG_ID      :"RENOBIT",
      debug       : true,
      log:function( title:string, ...args:any[] ){
            console.log( "[" + this.getCurrentTime() + " " + CPLogger.LOG_ID + "] " + title );
            if( args != null ){
                  let debugIndex = args.indexOf("debug");
                  if( debugIndex > -1 ) args.splice(debugIndex, 1);
                  if( CPLogger.debug || debugIndex > -1 ) {
                        console.log(args);
                  }
            }
            CPLogger.LOG_INDEX++;
            CPLogger.LOG_TYPE = CPLogger.LOG;

      },




      getCallerInfo: function( scope:any, methodName?:string ):string {
            return scope.constructor.name + " | method ::" + methodName;
      },

      /**
       * return currentTimeString ( yyyy-mm-dd hh:mm:ss )
       */
      getCurrentTime:function() {
            let year, month, day, hour, min, sec;
            let currentDate = new Date();
            year  = currentDate.getFullYear();
            month = currentDate.getMonth() + 1;
            month = month >= 10 ? month : "0" + month;
            day   = currentDate.getDate() >= 10 ? currentDate.getDate() : "0" + currentDate.getDate();
            hour  = currentDate.getHours() >= 10 ? currentDate.getHours() : "0" + currentDate.getHours();
            min   = currentDate.getMinutes() >= 10 ? currentDate.getMinutes() : "0" + currentDate.getMinutes();
            sec   = currentDate.getSeconds() >= 10 ? currentDate.getSeconds() : "0" + currentDate.getSeconds();

            return  [[year, month, day].join('-'), [hour, min, sec].join(':')].join(" ")
      }


}
