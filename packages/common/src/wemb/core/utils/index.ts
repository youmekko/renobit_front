import ComponentCounter from './ComponentCounter'
import CPLogger from './CPLogger'
import CPNotification from './CPNotification'
import CPUtil from './CPUtil'
import CssInfo from './CssInfo'
import DragAreaCalculator from './DragAreaCalculator'
import * as reference from './reference'
import ScriptUtil from './ScriptUtil'
import Spinner from './Spinner'
import ThreeUtil from './ThreeUtil'
import TooltipGenerator from './TooltipGenerator'


const utils = {
    ComponentCounter,
    CPLogger,
    CPNotification,
    CPUtil,
    CssInfo,
    DragAreaCalculator,
    reference,
    ScriptUtil,
    Spinner,
    ThreeUtil,
    TooltipGenerator
}

export default utils