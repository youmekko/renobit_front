/**
 * 유틸 함수 모음 객체
 * */
import {BROWSER_TYPE} from "../display/interfaces/DisplayInterfaces";
import Point from "../geom/Point";

export default class CPUtil {

      private static _instance: CPUtil;

      private constructor() {
            if (!CPUtil._instance) {
                  CPUtil._instance = this;
            }
            return CPUtil._instance;
      }

      public static getInstance(): CPUtil {
            return CPUtil._instance || new CPUtil();
      }


      //엘리먼트 현재 rotation값 반환
      getCssRotateValue(objElement: HTMLElement): number {
            let jqueryEle = $(objElement);
            let angle: number;
            var matrix = jqueryEle.css("-webkit-transform") ||
                  jqueryEle.css("-moz-transform") ||
                  jqueryEle.css("-ms-transform") ||
                  jqueryEle.css("-o-transform") ||
                  jqueryEle.css("transform");
            if (matrix !== 'none') {
                  let values: any = matrix.split('(')[1].split(')')[0].split(',');
                  let a = values[0];
                  let b = values[1];
                  angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
            } else {
                  angle = 0;
            }
            return (angle < 0) ? angle + 360 : angle;
      }


      debounce(func: any, wait: number, immediate?: boolean) {
            let timeout;
            return function () {
                  let context = this, args = arguments;
                  let later = function () {
                        timeout = null;
                        if (!immediate) func.apply(context, args);
                  };
                  let callNow = immediate && !timeout;
                  clearTimeout(timeout);
                  timeout = setTimeout(later, wait);
                  if (callNow) func.apply(context, args);
            };
      };


      randomIntFromInterval(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1) + min);
      }

      getRandomColor(): string {
            let letters: string = '0123456789ABCDEF';
            let color: string = '#';
            for (let i = 0; i < 6; i++) {
                  color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
      }

      toHex(int: any): string {
            let hex = int.toString(16);
            return hex.length == 1 ? "0" + hex : hex;
      }

      parseColor(color: string): any {
            let arr: any = [];
            color.replace(/[\d+\.]+/g, (v: string): any => {
                  arr.push(parseFloat(v));
            });
            return {
                  hex: "#" + arr.map(this.toHex).join("")
            };
      }

      hex2rgba(hexa: string): any {
            //let results = {};
            if (hexa.indexOf("#") <= -1) {
                  let info = this.parseColor(hexa);
                  hexa = info.hex;
            }
            let r = parseInt(hexa.slice(1, 3), 16);
            let g = parseInt(hexa.slice(3, 5), 16);
            let b = parseInt(hexa.slice(5, 7), 16);
            let results: any = {r: r, g: g, b: b};
            if (hexa.length > 6) {
                  let a = parseInt(hexa.slice(7, 9), 16) / 255;
                  results.a = a;
            }
            return results;
      }

      convertInt(strValue: string): number {
            return parseInt(strValue);
      }


      createSVGElement(tag: string, attrs?: any): SVGElement | SVGGraphicsElement {
            let el: SVGElement = document.createElementNS('http://www.w3.org/2000/svg', tag);
            if (tag == "svg") {
                  el.setAttribute("xmlns", "http://www.w3.org/2000/svg");
            }
            for (let k in attrs)
                  el.setAttribute(k, attrs[k]);
            return el;
      }


      lerp(p1: Point, p2: Point, t: number): any {
            var x = p1.x + t * (p2.x - p1.x);
            var y = p1.y + t * (p2.y - p1.y);
            return {x: x, y: y};
      }

      browserDetection(): string {
            var agent = navigator.userAgent, match;
            var app, version;

            if ((match = agent.match(/MSIE ([0-9]+)/)) || (match = agent.match(/Trident.*rv:([0-9]+)/))) app = 'Internet Explorer';
            else if (match = agent.match(/Chrome\/([0-9]+)/)) app = 'Chrome';
            else if (match = agent.match(/Firefox\/([0-9]+)/)) app = 'Firefox';
            else if (match = agent.match(/Safari\/([0-9]+)/)) app = 'Safari';
            else if ((match = agent.match(/OPR\/([0-9]+)/)) || (match = agent.match(/Opera\/([0-9]+)/))) app = 'Opera';
            else app = 'Unknown';

            if (app !== 'Unknown') version = match[1];

            if (app == BROWSER_TYPE.IE) {
                  return BROWSER_TYPE.IE;
            } else if (app == BROWSER_TYPE.FIRE_FOX) {
                  return BROWSER_TYPE.FIRE_FOX;
            } else if (app == BROWSER_TYPE.SAFARI) {
                  return BROWSER_TYPE.SAFARI;
            } else if (app == BROWSER_TYPE.OPERA) {
                  return BROWSER_TYPE.OPERA;
            }
            return BROWSER_TYPE.CHORME;
      }

      /**
       * promise를 이용한 유효 이미지목록 체크
       * @param resourceList 사용할 이미지 목록 배열, 아이템은 {key:구분 키, path:이미지경로}로 구성
       * */
      async getValidateImageList(resourceList) {
            function requestIconLoad(info) {
                  return new Promise((resolve, reject) => {
                        let img = new Image();
                        img.onload = () => { resolve(info); }
                        img.onerror = (error) => {
                              info.error = error;
                              resolve(info);
                        }
                        img.src = info.path;
                  });
            }

            let promises = [];
            resourceList.forEach((info) => {promises.push(requestIconLoad(info));});
            let promiseResults = await Promise.all(promises);
            let resources =[];
            promiseResults.forEach(element =>{
                  if(!element.hasOwnProperty("error")){resources.push(element);}
            });
            return resources;
      }

      destructAssignmentPromiseForNode(promise): Promise<any> {
            return promise.then(response => [null, response]).catch(error => [error, null]);
      }

      destructAssignmentPromise(promise): Promise<any> {
            return promise.then(response => [null, response.data]).catch(error => [error, null]);
      }
}
window.CPUtil = CPUtil;
