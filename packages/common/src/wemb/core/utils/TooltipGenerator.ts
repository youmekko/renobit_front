import WV2DComponent from "../component/2D/WV2DComponent";
import CssInfo from "./CssInfo";

export default class TooltipGenerator {
      private static tooltipGenerator:TooltipGenerator;
      private constructor() {
      }

      public static getInstance() {
            if (!this.tooltipGenerator) {
                  this.tooltipGenerator = new TooltipGenerator();
            }
            return this.tooltipGenerator;
      }

      public initTooltip(component: WV2DComponent, title:string, option: CssInfo | Function):void {
            let element = component.element;

            $(element).attr("title", title);
            $(element).tooltip({
                  track: true,
                  position: {
                        using: (typeof option) === 'function' ? option: this.defaultUsing(option)
                  }
            })
      }

      private defaultUsing(cssInfo) {
            return function(position) {
                  let css = { ...position, ...cssInfo.css };
                  $(this).css(css);
            }
      }
}
