import * as IExtensionPanel from './interfaces/IExtensionPanel'
import * as IWVExtension from './interfaces/IWVExtension'
import * as IWVExtensionPlugin from './interfaces/IWVExtensionPlugin'

import ExtensionPanelCore from './ExtensionPanelCore'
import ExtensionPluginCore from './ExtensionPluginCore'

const extension = {
    interfaces: {
        IExtensionPanel,
        IWVExtension,
        IWVExtensionPlugin
    },
    ExtensionPanelCore,
    ExtensionPluginCore
}

export default extension