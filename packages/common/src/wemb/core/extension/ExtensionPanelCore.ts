import {IExtensionPanel} from "./interfaces/IExtensionPanel";
import IFacade = puremvc.IFacade;
import {CreateDynamicPanelCommand} from "../../../editor/controller/main/CreateDynamicPanelCommand";

export default class ExtensionPanelCore implements IExtensionPanel{

      protected _facade:IFacade;
      protected _$element =null;

      protected _$parentView = null;

      // element
      get $element(){
            return this._$element;
      }






      constructor(){
            this._$element = null;
            this._facade = null;
      }


      create(){

      }



      setFacade(facade:IFacade){
            this._facade = facade;
      }

      setParentView(parentView:any){
            this._$parentView = $(parentView);
      }


      /*
      받을 노티 목록
      call : CreateDynamicPanelCommand에서 Mediator가 생성될 때 실행.

       */
      get notificationList():string[]{
            return [
            ]
      }


      /*
            noti가 온 경우 실행
            call : mediator에서 실행
       */
      handleNotification(note){
            //console.log("CustomPanel note2 = ", note);
      }


      /*
      데이터를 facade로 보내는 부분.
       */
      sendNotification(notificationName:string, data:any=null){
            if(this._facade==null)
                  return;

            this._facade.sendNotification(notificationName, data);
      }
}
