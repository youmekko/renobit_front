import {IExtensionPanel} from "./interfaces/IExtensionPanel";
import IFacade = puremvc.IFacade;
import {IWVExtensionPlugin} from "./interfaces/IWVExtensionPlugin";

export default class ExtensionPluginCore implements IWVExtensionPlugin{

      protected _facade:IFacade;

      constructor(){
            this._facade = null;
            this.constructor["_instance"] = this;
      }


      create(){

      }



      setFacade(facade:IFacade){
            this._facade = facade;
      }

      /*
      모든 준비가 마무리 된 후 실행되는 함수
      call :
            CreateExtensionCommand.ts
       */
      start(){

      }

      /*
      받을 노티 목록
      call : CreateDynamicPanelCommand에서 Mediator가 생성될 때 실행.

       */
      get notificationList():string[]{
            return [
            ]
      }


      /*
            noti가 온 경우 실행
            call : mediator에서 실행
       */
      handleNotification(note){
            //console.log("CustomPanel note2 = ", note);
      }


      /*
      데이터를 facade로 보내는 부분.
       */
      sendNotification(notificationName:string, data:any=null){
            if(this._facade==null)
                  return;

            this._facade.sendNotification(notificationName, data);
      }

}
