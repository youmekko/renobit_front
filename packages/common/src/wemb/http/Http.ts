
import httpAPI, {AxiosResponse} from 'axios';
import { SessionScheduler } from '../wv/managers/SessionScheduler'; 
export namespace http {

	export const api ={
		get(url, request={}) {
			let serverUrl:string;
			if( window.wemb && window.wemb.configManager ){				
				serverUrl = window.wemb.configManager.serverUrl;
			}	

			return httpAPI.get(url, request)
				.then((response:AxiosResponse<any>) =>{
					if( url.indexOf(serverUrl) != -1 ){
						SessionScheduler.getInstance().lastResponseTime = new Date().getTime();
					}

					return Promise.resolve(response.data);
				})
				.catch((error) => Promise.reject(error));
		},
	
		post(url, request={}) {
			let serverUrl:string;
			if( window.wemb && window.wemb.configManager ){				
				serverUrl = window.wemb.configManager.serverUrl;
			}	

			return httpAPI.post(url, request)
				.then((response:AxiosResponse<any>) => {					
					if( url.indexOf(serverUrl) != -1){
						SessionScheduler.getInstance().lastResponseTime = new Date().getTime();
					}

					return Promise.resolve(response)
				
				})
				.catch((error) => Promise.reject(error));
		},

		put(url, request={}) {
			let serverUrl:string;
			if( window.wemb && window.wemb.configManager ){				
				serverUrl = window.wemb.configManager.serverUrl;
			}	

			return httpAPI.put(url, request)
				.then((response:AxiosResponse<any>) => {					
					if( url.indexOf(serverUrl) != -1){
						SessionScheduler.getInstance().lastResponseTime = new Date().getTime();
					}

					return Promise.resolve(response)
				
				})
				.catch((error) => Promise.reject(error));
		},
	
		patch(url, request={}) {
			return httpAPI.patch(url, request)
				.then((response:AxiosResponse<any>) => Promise.resolve(response))
				.catch((error) => Promise.reject(error));
		},
	
		delete(url, request={}) {
			return  httpAPI.delete(url, request)
				.then((response:AxiosResponse<any>) => Promise.resolve(response))
				.catch((error) => Promise.reject(error));
		}
	}
}
