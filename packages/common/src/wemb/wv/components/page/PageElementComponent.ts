import WVComponent from "../../../core/component/WVComponent";
import WVComponentPropertyManager from "../../../core/component/WVComponentPropertyManager";

import {BackgroundLayer} from "../../layer/BackgroundLayer";
import {ViewerTwoLayer} from "../../layer/ViewerTwoLayer";
import {IThreeExtensionElements, IWVComponentProperties} from "../../../core/component/interfaces/ComponentInterfaces";
import {ViewerThreeLayer} from "../../layer/ViewerThreeLayer";
import {ObjectUtil} from "../../../utils/Util";
import InstanceManagerOnPageViewer from "../../managers/InstanceManagerOnPageViewer";
import {PageMetaProperties} from "../PageMetaProperties";
import {EXE_MODE} from "../../data/Data";
import {IComponentInstanceLoaderContainer} from "../../managers/ComponentInstanceLoader";
import {WORK_LAYERS} from "../../layer/Layer";
import PageComponentCore from "./PageComponentCore";
import {ILayer} from "../../layer/interfaces/LayerInterfaces";
declare var THREE;

/*


- 레이어 관린하는 컴포넌트 (컨테이너가 아님)
- 컨테이너가 아니지만 컨테이너 역할을 어느정도 하게됨.

- 해당 layer에 컴포넌트 추가, 삭제, 찾기



 */


export default class PageElementComponent extends PageComponentCore implements IComponentInstanceLoaderContainer{
      // WeMB에서 isPage()를 호출해 해당 컴포넌트가 페이지 컴포넌트인지 아닌지 판단하는 속성으로 사용됨.
      public __PAGE__:string="PAGE_TYPE";
      public static readonly NAME: string = "PageElementComponent";
      private _bgLayer: BackgroundLayer;
      private _threeLayer: ViewerThreeLayer;
      private _twoLayer: ViewerTwoLayer;



      get bgLayer(){
            return this._bgLayer;
      }


      // 인스턴스를 WScript를 통해 외부에서 접속할 수 있게 인스턴스 명을 만들어주는 기능.
      private _instanceManager:InstanceManagerOnPageViewer;

      constructor() {
            super();
            this.exeMode = EXE_MODE.VIEWER;
      }

      protected _onDestroy() {


            this._bgLayer = null;
            this._threeLayer = null;
            this._twoLayer = null;


            this._instanceManager.clear();
            this._instanceManager = null;

            super._onDestroy();


      }


      /*
	     레이어 초기화
		*/
      public resetPage(){
            this.isLoaded=false;
            this.clearPage();

            // 기존 등록 정보를 모두 지운다.
            if(this._instanceManager)
                  this._instanceManager.clear();
      }



      /*
      페이지 프로퍼티 정보를 모두 업데이트 해야하는 경우에 사용함.
      call : EditProxy

       */
      public resetProperties(metaProperties:any){
            let newProperties:IWVComponentProperties = this._createProperties(metaProperties);
            if(newProperties){
                  this._properties = newProperties;
                  // 페이지 정보가 즉, 페이지가 다시 열릴때마다 페이지 인스턴스 관리자 생성
                  this._instanceManager.create((metaProperties as PageMetaProperties).id, this);

                  // properties에 따라 화면을 다시 업데이터 해야함.
                  this.immediateUpdateDisplay();
            }else {
                  console.log("경고 컴포넌트 프로퍼티 스타일이 맞지 않습니다.");
            }
      }




      /*
      1. layer를 생성 한후
      2. 수동으로 layer를 붙여야 함.
      3. layer를 붙인 후 layer의 parent를 PageElementComponent로 적용해야함.
       */
      protected _onCreateElement() {

            if(this.isViewerMode) {
                  // 레이어 인스턴스만 생성하기.
                  this._createLayer();
                  //레이어를 화면에 붙이기
                  this.addLayer(WORK_LAYERS.BG_LAYER,  this._bgLayer, false);


                  if(this._threeLayer) {
                        this.addLayer(WORK_LAYERS.THREE_LAYER, this._threeLayer);
                        // 여러 개의 threeLayer가 생성될 수 있기 때문에 기본적으로 생성한 threeLayer를 메인 레이어로 만들기.
                        this.setMainThreeLayer(this._threeLayer);
                  }

                  this.addLayer(WORK_LAYERS.TWO_LAYER,  this._twoLayer);

                  // 기본적으로 PageElementComponent main element에서 이벤트가 발생하지 않게 만들기
                  // 중요!!!!
                  $(this.element).css("pointer-events", "none");

            }

      }


      // 레이어 인스턴스만 생성하기.
      // 아직 화면에 붙이지 않은 상태.
      private _createLayer() {
            ////////////
            this._bgLayer = new BackgroundLayer();
            this._bgLayer.create({
                  id: ObjectUtil.generateID(),
                  name: "_bgLayer",
                  x:0,
                  y:0
            });

            // 옵션에 따라 생성
            let usingThreeLayer:boolean = this.getGroupPropertyValue("createLayerInfo", "usingThreeLayer");
            if(usingThreeLayer) {
                  this._threeLayer = new ViewerThreeLayer();
                  this._threeLayer.create({
                        id: ObjectUtil.generateID(),
                        name: "_threeLayer",
                        x: 0,
                        y: 0
                  });
            }else {
                  this._threeLayer =null;
            }




            this._twoLayer = new ViewerTwoLayer();
            this._twoLayer.create({
                  id: ObjectUtil.generateID(),
                  name: "_twoLayer",
                  x:0,
                  y:0
            });


      }

      ////////////////////////////////////////////////////////////




      protected _onImmediateUpdateDisplay(): void {
            if(this._instanceManager==null){
                  this._instanceManager = new InstanceManagerOnPageViewer();
            }

            this._bgLayer.setBackgroundPropertyValue("", this.getGroupProperties("setter.background"));
      }
      ////////////////////////////////////////////////////////////////////////












      ////////////////////////////////////////////////////////////////////////


      public setActiveAllLayer(active:boolean){

            this.layerListMap.forEach((layerInstance:ILayer)=>{
                  layerInstance.setMouseEventEnabled(active);

            })


            /*
            3D 처리 활서화 비활성화 처리
            추후 visible대신 다른 스타일로 변경해야할것 같음.
             */
            if(this.getMainThreeLayer()){
                 let layerInstance:ILayer = this.getMainThreeLayer();


                  if(layerInstance.numChildren==0){
                        layerInstance.visible=false;
                  }else {
                        layerInstance.visible=true;
                  }


            }

      }


      /*
	레이어에 컴포넌트 추가.
	 */
      public addComInstance(comInstance: WVComponent) {
            let result = super.addComInstance(comInstance);
            if(result==true) {

                  this._instanceManager.register(comInstance, comInstance.layerName);
            }

            return result;
      }


      ////////////////////////////////////////////////////////////////








      ////////////////////////////////////////////////////////////////
      // 중요!
      // 로더에 의해서 컴포넌트가 모두 로드되면 실행.
      public loadCompleted(){
            this.setActiveAllLayer(this.isViewerMode);

      }





      public getThreeExtensionElements():IThreeExtensionElements{
            let mainThreeLayer = this.getMainThreeLayer();
            if(mainThreeLayer)
                  return mainThreeLayer.threeElements;
      }


      /*
      2018.12.28(ckkim)
      Viewer, Eidtor와 동일하게 한번에 로딩되게 나오는 효과 추가.

       */


      public hiddenAllLayer(){
            this.layerListMap.forEach((layer:ILayer)=>{
                  console.log("name 1", layer.name);
                  if(layer.name=="_bgLayer")
                        return;
                  $(layer.appendElement).css("opacity",0);
            })
      }


      public showAllLayer(){
            this.layerListMap.forEach((layer:ILayer)=>{
                  if(layer.name=="_bgLayer")
                        return;
                  $(layer.appendElement).css("opacity",1);
            })
      }

}


WVComponentPropertyManager.attach_default_component_infos(PageElementComponent, {name: PageElementComponent.NAME});

PageElementComponent["default_properties"] = {
      primary:{
            id: "",
            name: "",
            type:"page",
            secret:"N",
            layerName:"" /* 주의! 컴포넌트 _validateInitProperties() 호출에서 layerName이 반드시 존재해야함 */
      },
      setter: {
            width: 1920,
            height: 1080,
            borderRadius: {
                  topLeft:0,
                  topRight:0,
                  bottomLeft:0,
                  bottomRight:0
            },
            masterLayer: {
                  visible: true
            },
            mapLayer: {
                  visible: false
            },
            twoLayer: {
                  visible: true
            },
            threeLayer: {
                  visible: true,
                  using_shadow: false,
                  using_light: false,
                  camera: {
                        x: -100,
                        y: 100,
                        z: 100
                  },
                  cameraView: {
                        save: false,
                        cameraMatrix: "",
                        target: {
                              x: 0,
                              y: 0,
                              z: 0
                        }
                  },
                  grid: {
                        x: 100,
                        z: 100
                  }
            },
            background: {
                  using: false,
                  color: "rgba(255,255,255,0)",
                  path: "",
                  image: ""
            },
            visible : true,
            popup : {
                  customStyle: false,
                  instance: {
                        header: '',
                        close: ''
                  }
            },
            event: false,
            description: "",
      },
      events: {
            beforeLoad: "",
            loaded: "",
            beforeUnLoad: "",
            unLoaded: ""
      },
      info:{
            category:"2D",
            version:"1.0.0",
            componentName:"PageElementComponent"
      },
      createLayerInfo:{
            usingThreeLayer:false
      }
};
