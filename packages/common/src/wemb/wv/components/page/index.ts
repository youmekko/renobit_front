import * as pageComponentInterfaces from './interfaces/PageComponentInterfaces'
import PageComponentCore from './PageComponentCore'
import PageElementComponent from './PageElementComponent'

const page = {
    pageComponentInterfaces,
    PageComponentCore,
    PageElementComponent,
}

export default page