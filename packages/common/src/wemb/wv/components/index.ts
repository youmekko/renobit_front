import datasetPreview from './datasetPreview'
import page from './page'
import selectablePageTree from './selectablePageTree'
import table from './table'

import DynamicModalComponent from './DynamicModalComponent' //vue
import EditMainAreaResourceLoadingComponent from './EditMainAreaResourceLoadingComponent'//vue
import EditorPageComponent from './EditorPageComponent'
import LoadingProgressbar from './LoadingProgressbar'
import PageComponent from './PageComponent'
import PageInfoProperties from './PageInfoProperties'
import * as PageMetaProperties from './PageMetaProperties'
import PageResourceLoadingComponent from './PageResourceLoadingComponent' //vue
import PopupWindow from './PopupWindow'
import RollingPageComponent from './RollingPageComponent'
import ViewerPageComponent from './ViewerPageComponent'

const components = {
    datasetPreview,
    page,
    selectablePageTree,
    table,
    DynamicModalComponent,
    EditMainAreaResourceLoadingComponent,
    EditorPageComponent,
    LoadingProgressbar,
    PageComponent,
    PageInfoProperties,
    PageMetaProperties,
    PageResourceLoadingComponent,
    PopupWindow,
    RollingPageComponent,
    ViewerPageComponent,
}

export default components