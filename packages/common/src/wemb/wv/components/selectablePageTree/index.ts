import SelectablePageTreeCheckboxItemComponent from './SelectablePageTreeCheckboxItemComponent' // vue
import SelectablePageTreeComponent from './SelectablePageTreeComponent' //vue
import SelectablePageTreeRadioItemComponent from './SelectablePageTreeRadioItemComponent' //vue



const selectablePageTree = {
    SelectablePageTreeCheckboxItemComponent,
    SelectablePageTreeComponent,
    SelectablePageTreeRadioItemComponent
}

export default selectablePageTree