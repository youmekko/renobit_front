export interface IProgressbar {

      readonly element:any;
      usingProgress:boolean;
      show(string): void;
      hide(): void;
      setMaxLength(length: number);
      addMessage(message:string, progress:boolean):void;
      setProgress(index: number): void;
      completed(): void;


}

export default class LoadingProgressbar implements IProgressbar {
      private _$el = null;
      public maxLength: number = 100;

      private _usingProgress:boolean=true;
      public set usingProgress(value:boolean){
            this._usingProgress=value;

            if(value==false){
                  this._$el.hide();
            }else {
                  this._$el.show();
            }
      }
      constructor() {
            this._$el = $("<div style='width: 0%;height: 3px; background-color: #4690e6; position: absolute; top: 0; left: 0;'></div>");
            this.hide();
      }

      public reset(){

      }

      public setTitle(txt:string){
      }



      public get element(){
            return this._$el.get(0);
      }

      public show(message:string=""): void {
            if(this._usingProgress)
                  this._$el.show();
      }

      public hide(): void {
           this._$el.hide();
      }

      public setMaxLength(length: number) {
            this.maxLength = length;
      }

      public addMessage(message: string, progress:boolean = false): void {

      }

      public setProgress(index: number): void {
            let percent:string = (100*(index/this.maxLength)).toFixed(0);
            this._$el.width( percent+"%" );
      }

      public completed(): void {
            this._$el.width("100%" );
      }



}
