import WVComponent, {GROUP_NAME_LIST} from "../../core/component/WVComponent";
import WVComponentPropertyManager from "../../core/component/WVComponentPropertyManager";

import {EditThreeLayer} from "../layer/EditThreeLayer";
import {BackgroundLayer} from "../layer/BackgroundLayer";
import {EditTwoLayer} from "../layer/EditTwoLayer";
import {IWVEvent} from "../../core/events/interfaces/EventInterfaces";

import {ControlLayer} from "../layer/ControlLayer";

import {PageMetaProperties} from "./PageMetaProperties";
import {DragAreaLayer} from "../layer/DragAreaLayer";
import PageInfoProperties from "./PageInfoProperties";
import {ILayer} from "../layer/interfaces/LayerInterfaces";
import {LayerEvent, WORK_LAYERS} from "../layer/Layer";
import WVDisplayObject from "../../core/display/WVDisplayObject";
import PageComponentCore from "./page/PageComponentCore";
declare var THREE;

export class EditorPageComponentEvent implements IWVEvent {
      public static readonly COMPONENT_MOUSE_DOWN: string = "EditorPageComponentEvent.COMPONENT_MOUSE_DOWN";
      public static readonly BACKGROUND_MOUSE_DOWN: string = "EditorPageComponentEvent.BACKGROUND_MOUSE_DOWN";
      public type: string;
      public target: any;
      public originalEvent: any;
      public data?: {
            targetLayer: ILayer,
            targetInstance: WVComponent
      };

      constructor(type: string, target: any, originalEvent?: any, data?: any) {
            this.type = type;
            this.target = target;
            this.originalEvent = originalEvent;
            this.data = data;
      }

}

/*


- 레이어 관린하는 컴포넌트 (컨테이너가 아님)
- 컨테이너가 아니지만 컨테이너 역할을 어느정도 하게됨.

- 해당 layer에 컴포넌트 추가, 삭제, 찾기



 */
export default class EditorPageComponent extends PageComponentCore {
      public static readonly NAME: string = "EditorPageComponent";
      private _bgLayer: BackgroundLayer;
      private _threeLayer: EditThreeLayer;
      private _twoLayer: EditTwoLayer;
      private _masterLayer: EditTwoLayer;
      private _controlLayer:ControlLayer;
      private _dragAreaLayer:DragAreaLayer;

      private _invalidateGrid = false;

      get controlLayer(){
            return this._controlLayer;
      }

      get bgLayer(){
            return this._bgLayer;
      }

      get threeLayer(){
            return this._threeLayer;
      }

      get dragAreaLayer(){
            return this._dragAreaLayer;
      }


      set grid(value){
            if (this._checkUpdateGroupPropertyValue("setter.threeLayer", "grid", value)) {
                  this._invalidateGrid = true;
                  this._invalidateCameraFar =true;
            }
      }

      get grid(){
            return this.getGroupPropertyValue("setter.threeLayer", "grid");
      }

      private _activeLayer: ILayer;
      public get activeLayer(){
            return this._activeLayer;
      }
      private layerMap: Map<string, object> = new Map();


      constructor() {
            super();
      }


      public clearPage(){
            /*
            주의:
            clearPage() 전에 resetTransformToolMG() 호출하면 안됨.

             */
            this._controlLayer.resetTransformToolMG();
            super.clearPage();


            // 카메라 위치 초기화
            this.resetCameraPosition();


      }

      /*
      1. layer를 생성 한후
      2. 수동으로 layer를 붙여야 함.
      3. layer를 붙인 후 layer의 parent를 EditorPageComponent로 적용해야함.
       */
      protected _onCreateElement(template:string=null) {

            this._createLayer();

            if(window.wemb.configManager.using3D==true){
                  this.create3DLayer();
            }

            // clearlayer = true 인 경우 clearPage에 의해서 초기화 될 레이어를 나타냄.
            this.addLayer(WORK_LAYERS.BG_LAYER,  this._bgLayer,false);

            // 레이어 등록, 위치가 변경되면 안됨.
            if(window.wemb.configManager.using3D==true) {
                  this.addLayer(WORK_LAYERS.THREE_LAYER, this._threeLayer);
                  this.setMainThreeLayer(this._threeLayer);
            }

            this.addLayer(WORK_LAYERS.TWO_LAYER,  this._twoLayer);
            this.addLayer(WORK_LAYERS.MASTER_LAYER,  this._masterLayer);
            this.addLayer(WORK_LAYERS.CONTROL_LAYER,  this._controlLayer,false);
            this.addLayer(WORK_LAYERS.DRAG_AREA_LAYER,  this._dragAreaLayer,false);

      }

      private _componentMousedownHandler:Function;
      private _layerMouseDownHandler:Function;
      private _createLayer() {
            ////////////
            this._bgLayer = new BackgroundLayer();
            this._bgLayer.create({
                  id: "_bgLayer",
                  name: "_bgLayer",
                  x:0,
                  y:0
            });


            this._twoLayer = new EditTwoLayer();
            this._twoLayer.create({
                        id: "_twoLayer",
                        name: "_twoLayer",
                        x:0,
                        y:0,
                        visible:true
            });

            this._masterLayer = new EditTwoLayer();
            this._masterLayer.create({
                  id: "_masterLayer",
                  name: "_masterLayer",
                  x:0,
                  y:0,
                  visible:true
            });


            this._controlLayer = new ControlLayer();
            this._controlLayer.create({
                  id: "controlLayer",
                  name: "controlLayer",
                  x:0,
                  y:0
            });

            this._dragAreaLayer = new DragAreaLayer();
            this._dragAreaLayer.create({
                  id   : "dragAreaLayer",
                  name : "dragAreaLayer",
                  x : 0,
                  y : 0
            });
            ////////////


            ////////////
            this.layerMap.set(WORK_LAYERS.BG_LAYER, this._bgLayer);
            this.layerMap.set(WORK_LAYERS.TWO_LAYER, this._twoLayer);
            this.layerMap.set(WORK_LAYERS.MASTER_LAYER, this._masterLayer);
            this.layerMap.set(WORK_LAYERS.CONTROL_LAYER, this._controlLayer);
            this.layerMap.set(WORK_LAYERS.DRAG_AREA_LAYER, this._dragAreaLayer);
            ////////////


            ////////////
            // 레이어에서 발생하는 COMPONENT_MOUSE_DOWN 이벤트 등록
            this._componentMousedownHandler = this._onComponentMouseDown.bind(this);
            this._layerMouseDownHandler = this._onThreeLayerMouseDown.bind(this);
            this._registerLayerEvent( this._masterLayer, LayerEvent.COMPONENT_MOUSE_DOWN, this._componentMousedownHandler );
            this._registerLayerEvent( this._twoLayer,   LayerEvent.COMPONENT_MOUSE_DOWN, this._componentMousedownHandler );

            // 배경 처리
            /*
            2018.04. 24
            추후 _registerLayerEvent()를 적용해서 이벤트 처리해야함.
             */
            $(this._bgLayer.element).on("mousedown", this._layerMouseDownHandler );

            ////////////
      }


      /*
      2018.11.04
      3D Layer 처리  부분 추가
       */
      private create3DLayer(){
            this._threeLayer = new EditThreeLayer();
            this._threeLayer.create({
                  id: "_threeLayer",
                  name: "_threeLayer",
                  x:0,
                  y:0,
                  visible:true
            });


            this.layerMap.set(WORK_LAYERS.THREE_LAYER, this._threeLayer);

            ////////////
            // 레이어에서 발생하는 COMPONENT_MOUSE_DOWN 이벤트 등록
            this._registerLayerEvent( this._threeLayer, LayerEvent.COMPONENT_MOUSE_DOWN, this._componentMousedownHandler );
            this._registerLayerEvent( this._threeLayer, LayerEvent.LAYER_MOUSE_DOWN , this._layerMouseDownHandler );
      }

      ////////////////////////////////////////////////////////////

      protected _validateAttribute(): void {
            try {
                  let primaryProperties = this.getGroupProperties(GROUP_NAME_LIST.PRIMARY);

                  // 스타일 클래스에 인스턴스 이름과 컴포넌트명을 추가
                  this.element.classList.add(primaryProperties.id);
                  this.element.classList.add(this.constructor.name);
                  this.element.classList.add("wv-component");

                  // element id를 id 속성 값으로 설정
                  this.element.id = primaryProperties.id;
                  this.element["data-instanceName"] = primaryProperties.name;
                  this.element["data-id"] = primaryProperties.id;
            }
            catch (error) {
                  console.log("validateAttribute ", error);
            }
      }


      protected  _onCommitProperties():void{
            if(this.threeLayer==null)
                  return;

            // 그리드 정보 변경  유무
		if(this._updatePropertiesMap.has("setter.threeLayer.grid")){
		      this._invalidateCameraFar=true;
			this.validateCallLater(this._validateGrid);
		}

            for(let key in this.properties.setter.borderRadius){
                  if(this._updatePropertiesMap.has(`setter.borderRadius.${key}`)){
                        this.invalidateSize= true;
                        this.validateCallLater(this._validateSize);
                  }
            }
      }


      protected _validateGrid():void {
            if(this.threeLayer)
                  this.threeLayer.recreateGridHelper(this.grid);
      }


      public _onImmediateUpdateDisplay(): void {

            this._validateGrid();
      }

      ////////////////////////////////////////////////////////////////////////












      ////////////////////////////////////////////////////////////////////////

      private _registerLayerEvent( layer:WVDisplayObject, eventName:string, handler:Function ):void {
            if (layer.hasEventListener(eventName) == false) {
                  layer.addEventListener(eventName, handler );
            }
      }

      private _onThreeLayerMouseDown( event:LayerEvent ):void {

            /*
            2018.08.28
                  threeLayer가 활성화 된 상태에서 component property panel의 입력창이 활성화 되어 있을 때
                  일반적인 레이어의 경우 레이어 자체에서 이벤트 처리가 가능함.
                  threeLayer의 경우 threeLayer에서 마우스 이벤트 처리를 하지 못하기 때문에
                  blur 이벤트가 발생하지 못해 UPDATE_PROPERTY 이벤트가 mousedown 이벤트 발생 후 발생하게 되어
                  선택한 일반 컴포넌트 프로퍼티가 변경되는가 아니라
                  배경 컴포넌트로의 프로퍼티가 변경되는 문제 발생

                  이를 위해서 배경선택시
                  threeLayer가 선택되어 있는 경우
                  현재 입력 포커시를 가진 input element의  blur를 실행해야함.


             */
            if(this.activeLayer.name=="_threeLayer") {
                  try {
                        (document.activeElement as any).blur();
                  }catch(error){
                        console.log("활성화된 요소 중  input 요소가 아닌 경우 blur() 가 실행됨. error 아님")
                  }
            }

            // 이벤트 발생
            let newEvent: EditorPageComponentEvent = new EditorPageComponentEvent(EditorPageComponentEvent.BACKGROUND_MOUSE_DOWN, this, event.originalEvent);
            this.dispatchEvent(newEvent);
      }

      /*
            레이어에서 컴포넌트 클릭시 EditAreaMainMediator로 연결하기 위해 이벤트 발생 처리
      */
      private _onComponentMouseDown(event: LayerEvent) {
            let newEvent: EditorPageComponentEvent = new EditorPageComponentEvent(
                  EditorPageComponentEvent.COMPONENT_MOUSE_DOWN,
                  this,
                  event.originalEvent,
                  {
                        targetLayer: event.target,
                        targetInstance: event.data.targetInstance
                  }
            );
            this.dispatchEvent(newEvent);
      }



      ////////////////////////////////////////////////////////////////////////////////////////


      ////////////////////////////////////////////////////////////////////////////////////////

      /*
	특정 레이어만 활성화 시키기
	 */
      public setActiveLayer(layerName: WORK_LAYERS) {
            this._activeLayer = <ILayer>this.layerMap.get((layerName as string));

            // 모든 레이어 이벤트 비활성화
            this._masterLayer.setMouseEventEnabled(false);
            this._twoLayer.setMouseEventEnabled(false);

            if(this._threeLayer)
                  this._threeLayer.setMouseEventEnabled(false);

            this._controlLayer.setMouseEventEnabled(false);
            this._dragAreaLayer.setMouseEventEnabled(false);



            // 선택한 레이어만 마우스 이벤트 활성화
            if(this._activeLayer) {
                  this._activeLayer.setMouseEventEnabled(true);
                  // 삭제 예정
                  //this._activeLayer.visible=true;
            }
      }



      public updateLayerVisible(layerName: WORK_LAYERS, visible: boolean) {
            let targetLayer: WVComponent = <WVComponent>this.layerMap.get((layerName as string));
            if (targetLayer) {
                  targetLayer.visible = visible;
            }else{
                  return;
            }

            this.properties.setter[targetLayer.id.substring(1, targetLayer.id.length)].visible = visible;
      }


      public serializePageMetaProperties():PageMetaProperties{
            let pageInfoProperties:PageInfoProperties = new PageInfoProperties();
            pageInfoProperties.setPageInfoProperties(this.properties);
            return pageInfoProperties.serializeMetaProperties();
      }





      /*
      parent를 추가하기 위해 addComInstance override 시키기
       */
      public addComInstance(comInstance: WVComponent):boolean {
            let value = super.addComInstance(comInstance);
            if(value==true){
                  comInstance.page=this;
            }

            return value;
      }





      /*
      2018.09.14
     Mater, Two, Three Layer 숨기기
     페이지 로딩 후 모든 내용을 한번에 출력되는 효과를 만들기 위해 사용
	*/
      public hiddenMTTLayer(){
            /*
            visivility의 경우 부모/자식간의 독립적으로 처리되기 때문에 (즉, 부모가 hidden되어 있다해도 자식이 visible인 경우 활성화됨)
            visivility 사용하기 어려움.

            */

            // 2018.11.17(ckkim) 화면을 뒤덮는 로딩 효과 숨기기
            if(this._masterLayer) $(this._masterLayer.element).css("opacity",0);
            if(this._twoLayer) $(this._twoLayer.element).css("opacity",0);
            if(this._threeLayer) $(this._threeLayer.element).css("opacity",0);
      }

      /*
      2018.09.14
      Mater, Two, Three Layer 보이기
      페이지 로딩 후 모든 내용을 한번에 출력되는 효과를 만들기 위해 사용
       */
      public showMTTLayer(){
            // 2018.11.17(ckkim) 화면을 뒤덮는 로딩 효과 숨기기
            if(this._masterLayer) $(this._masterLayer.element).css("opacity",1);
            if(this._twoLayer) $(this._twoLayer.element).css("opacity",1);
            if(this._threeLayer) $(this._threeLayer.element).css("opacity",1);

      }


      /*
     2019.01.24(ckkim)
	     - 페이지에 저장된 카메라 위치로 이동 처리
	*/
      public resetCameraPosition(){
            if(this._threeLayer) {
                  try {
                        window.wemb.mainControls.reset();

                        // 카메러 원위치 시키기
                        window.wemb.mainControls.transitionFocusInTarget(
                              window.wemb.mainControls.position0.clone(),
                              window.wemb.mainControls.target0.clone(),
                              0
                        );
                  } catch(error){
                        console.warn("viewerPageCOmponent camera reset error ", error);
                  }
            }

      }

      ////////////////////////////////////////////////////////////////

}
WVComponentPropertyManager.attach_default_component_infos(EditorPageComponent, {name: EditorPageComponent.NAME});

EditorPageComponent["default_properties"] = {
      primary:{
            id: "",
            name: "",
            master: "",
            type:"page",
            secret:"N",
            updateDt: "",
            lastUser: ""
      },
      info:{
            category:"Page",
            version:"1.0.0",
            componentName:"EditorPageComponent"
      },
      setter: {
            visible: true,
            width: 1920,
            height: 1080,
            borderRadius: {
                  topLeft:0,
                  topRight:0,
                  bottomLeft:0,
                  bottomRight:0
            },
            masterLayer: {
                  visible: true
            },
            mapLayer: {
                  visible: false
            },
            twoLayer: {
                  visible: true
            },
            threeLayer: {
                  visible: true,
                  using_shadow: false,
                  using_light: false,
                  camera: {
                        x: 0,
                        y: 100,
                        z: 100.00000000000001
                  },
                  cameraView: {
                        save: false,
                        cameraMatrix: "",
                        target: {
                              x: 0,
                              y: 0,
                              z: 0
                        }
                  },
                  grid: {
                        x: 100,
                        z: 100
                  }
            },
            background: {
                  using: false,
                  color: "#ffffff",
                  path: "",
                  image: ""
            },
            event: false,
            popup : {
                  customStyle: false,
                  instance: {
                        header: '',
                        close: ''
                  }
            },
            description: ""
      },
      events: {
            beforeLoad: "",
            loaded: "",
            beforeUnLoad: "",
            unLoaded: "",
            ready: ""
      },
      script: {
            fileName: ""
      }
};


EditorPageComponent["property_panel_info"] = [{
      label: "기본  속성",
      template: "primary",
      children: [{
            owner: "setter",
            name: "id",
            label: "아이디",
            type: "string",
            writable: false,
            show: true,
            description: "페이지 아이디"
      }, {
            owner: "setter",
            name: "name",
            label: "이름",
            type: "string",
            writable: true,
            show: true,
            description: "페이지 이름"
      }, {
            owner: "setter",
            name: "type",
            label: "타입",
            type: "string",
            writable: false,
            show: false,
            description: "페이지 타입"
      }]
}, {
      label: "페이지 크기 속성",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "width",
            label: "width",
            type: "number",
            writable: true,
            show: true,
            description: "페이지 너비"
      }, {
            owner: "setter",
            name: "height",
            label: "height",
            type: "number",
            writable: true,
            show: true,
            description: "페이지 높이"
      },{
            owner: "setter.borderRadius",
            name: "topLeft",
            label: "TL",
            type: "number",
            writable: true,
            show: true,
            description: "페이지 border-radius"
      },{
            owner: "setter.borderRadius",
            name: "topRight",
            label: "TR",
            type: "number",
            writable: true,
            show: true,
            description: "페이지 border-radius"
      },{
            owner: "setter.borderRadius",
            name: "bottomLeft",
            label: "BL",
            type: "number",
            writable: true,
            show: true,
            description: "페이지 border-radius"
      },{
            owner: "setter.borderRadius",
            name: "bottomRight",
            label: "BR",
            type: "number",
            writable: true,
            show: true,
            description: "페이지 border-radius"
      }]
}, {
      label: "카메라 속성",
      template: "camera",
      children: [{
            owner: "setter.threeLayer",
            name: "camera",
            label: "카메라",
            type: "object",
            writable: true,
            show: true,
            description: "카메라 위치"
      }]
}, {
      label: "카메라 뷰",
      template: "cameraView",
      children: [{
            owner: "setter.threeLayer",
            name: "cameraView",
            label: "카메라뷰",
            type: "object",
            writable: true,
            show: true,
            description: "카메라 뷰"
      }]
}, {
      label: "그리드",
      template: "grid",
      children: [{
            owner: "setter.threeLayer",
            name: "grid",
            label: "grid",
            type: "object",
            writable: true,
            show: true,
            description: "그리드 정보"
      }]
}, {
      label: "page info",
      template: "vertical",
      children: [{
            owner: "setter",
            name: "description",
            label: "description",
            type: "textarea",
            writable: true,
            show: true,
            description: "페이지 설명",
            options:{
                  rows:5
            }
      }]
}
];

EditorPageComponent["event_info"]= [{
      name: "beforeLoad",
      label: "beforeLoad",
      description: "beforeLoad벤트입니다.",
      properties: [],

},{
      name: "loaded",
      label: "loaded",
      description: "loaded이벤트입니다.",
      properties: [],

},{
      name: "ready",
      label: "ready",
      description: "ready이벤트입니다.",
      properties: [],

}, {
      name: "beforeUnLoad",
      label: "beforeUnLoad",
      description: "beforeUnLoad 이벤트 입니다.",
      properties: []
}, {
      name: "unLoaded",
      label: "unLoaded",
      description: "unLoaded 이벤트 입니다.",
      properties: []
}];



EditorPageComponent["property_info"]= [{
            name: "primary",
            label: "General",
            children: [{
                  owner:"setter",
                  name: "name",
                  type: "string",
                  label: "name",
                  writable: false,
                  show: true,
                  description: "인스턴스명"
            }]
      }, {
            label: "Size",
            children: [{
                  owner:"",
                  name: "width",
                  type: "number",
                  label: "width",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "width  값"
            }, {
                  owner:"",
                  name: "height",
                  type: "number",
                  label: "height",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "height 위치 값"
            }]
      }
];
