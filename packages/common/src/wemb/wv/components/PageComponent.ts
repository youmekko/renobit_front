import WVComponentPropertyManager from "../../core/component/WVComponentPropertyManager";

import WVDOMComponent from "../../core/component/2D/WVDOMComponent";


import PageElementComponent from "./page/PageElementComponent";

import {OpenPageData} from "../page/Page";
import Vue from "vue";
import {ComponentInstanceLoader2} from "../managers/ComponentInstanceLoader";
import {IProgressbar, default as LoadingProgressbar} from "./LoadingProgressbar";
import IPageComponent from "./page/interfaces/PageComponentInterfaces";
import {EXE_MODE} from "../data/Data";
import {WVComponentScriptEvent} from "../../core/events/WVComponentEventDispatcher";
import WVComponent from "../../core/component/WVComponent";
import WVComponentEvent from "../../core/events/WVComponentEvent";
import {WVCOMPONENT_METHOD_INFO} from "../../core/component/interfaces/ComponentInterfaces";
import {ObjectUtil} from "../../utils/Util";
import WVEvent from "../../core/events/WVEvent";
import {PageComponentEvent} from "../events/PageComponentEvent";
import axios from 'axios';
import {ComponentResourceLoader, ComponentResourceLoaderEvent} from "../helpers/ComponentResourceLoader";
import ViewerProxy from "../../../viewer/model/ViewerProxy";
import HookManager from "../managers/HookManager";
declare var THREE;

/*

- 레이어 관린하는 컴포넌트 (컨테이너가 아님)
- 컨테이너가 아니지만 컨테이너 역할을 어느정도 하게됨.

- 해당 layer에 컴포넌트 추가, 삭제, 찾기


- PageComponent의 역할은
   EditorPageCOmponent의 OpenPageCommand 기능
   ViewerPageComponent의 OpenPageCommand 기능을 함.
   - 즉  페이지 정보 로드하고, 리소스 읽기 처리 하는 등의 작업을
     총괄함.

 */
export default class PageComponent extends WVDOMComponent implements IPageComponent{
      // WeMB에서 isPage()를 호출해 해당 컴포넌트가 페이지 컴포넌트인지 아닌지 판단하는 속성으로 사용됨.
      public __PAGE__:string="PAGE_TYPE";
      public static readonly NAME: string = "PageComponent";
      private static readonly ICON: string = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAkCAYAAACaJFpUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MkIxRTY0QUYzRDJCMTFFODlFMkRFMjQyRkRCMjQwMDEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MkIxRTY0QjAzRDJCMTFFODlFMkRFMjQyRkRCMjQwMDEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyQjFFNjRBRDNEMkIxMUU4OUUyREUyNDJGREIyNDAwMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyQjFFNjRBRTNEMkIxMUU4OUUyREUyNDJGREIyNDAwMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PicbV6AAAAFzSURBVHja7JfPK0RBHMDn8eTnxsHFQeIgbanVSolycXZwdRblV8rNv0A5oBTZdVUKuezNH8GRlJKbosW2ls/o++q1m7e9fW/WZb716dWbN/OZmTffacbZPb95Vkp1qdrDgUtYhid/wdJMsuJjFxJwB9fQGEL0DU0wAbPQAgvwGFRJCxsgR2/Wygv3Lm6rSXXdHeiHacjAPNwHVdDRWuN0tkE3PMAmjEMWBqsJo4Ru4w22YBVGZaRJU0IdzbJ4jmAdUnDMLxk2JfSkOg5gBbQsgzRlSugPPdJFmdYs0pG4hV/wUfbuBOagBw69kcYhLEk+dsqq7RDaSbUznhswJNK0G8MKTUga5KAoi8fL46JsELojadiPKizAFbxKo44I/NteSdJkEsaiCj/lX51Kw0HTvg29UYV6NHmhIvybN9NbMJkWf+3bdRUqK7RCK7RCK7RCK/xn4XsdXL8OhxtwXq5bYS+kYU/mU9DnyplyQDAdLz8CDACgwVKq1QTrkwAAAABJRU5ErkJggg==';

      private _isLoadingPage:boolean=false;
      private _pageElementComponent:PageElementComponent;
      get pageElementComponent() {
            return this._pageElementComponent;
      }

      private _progressBar:IProgressbar = new LoadingProgressbar();
      private _$errorPageInfo = null;


      //중요!: page정보 로딩 취소 용
      private _cancelSource = null;

      /*
      ComponentInstanceLoader2와 ComponentInstanceLoader와의 차이점
      ComponentInstanceLoader2에는
            - ajax을 통한 페이지 정보 읽기 취소
            - resource 로딩 읽기 취소 등의 기능이 추가되어 있음.
       */
      private _comInstanceLoader2:ComponentInstanceLoader2 = new ComponentInstanceLoader2();
      private _componentResourceLoader:ComponentResourceLoader = null;









      get progressBar(){
            return this._progressBar;
      }


      get pageId(){
            return this.getGroupPropertyValue("pageInfo", "pageId");
      }

      get usingThreeLayer(){
            /*
            2018.12.28(ckkim)
            PageELementComponent를 사용하는 페이지 컴포넌트 류에서는
            threeLayer를 사용하지 못하게 처리함.

            만약 일반  페이지 컴포넌트에서도 threeLayer를 사용하고자 한다면
            테스트를 좀 더 해야함.
            return this.getGroupPropertyValue("createLayerInfo", "usingThreeLayer");
            */

            return false;
      }


      get usingPopupPage(){
            return this.getGroupPropertyValue("pageInfo", "usingPopupPage");
      }

      get usingProgress(){
            return this.getGroupPropertyValue("pageInfo", "usingProgress");
      }

      get params(){
            return this.getGroupProperties("params");
      }


      set params(value){
            this.setGroupProperties("params", value);
      }

      get popupOwner(){
            // popupOwner는 고정.
            return this.getGroupPropertyValue("params", "popupOwner");

      }
      /* □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ */












      constructor() {
            super();
      }
      /* □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ */







      getExtensionProperties(){
            return true;
      }

      protected  _onDestroy() {

            if(this._comInstanceLoader2){
                  this._comInstanceLoader2.destroy();
                  this._comInstanceLoader2 = null;
            }


            if(this._cancelSource){
                  try {
                        if(this._cancelSource.clear)
                              this._cancelSource.clear();
                  }catch(error){
                        console.log("START 2 ___ error", error);
                  }

                  this._cancelSource = null;
            }


            if(this._pageElementComponent) {
                  this._pageElementComponent.destroy();
                  this._pageElementComponent = null;
            }




            this._progressBar = null;

            super._onDestroy();
      }


      public setMouseEventEnabled(enabled:boolean){
            if(this._pageElementComponent) {
                  this._pageElementComponent.setMouseEventEnabled(false);
            }
            super.setMouseEventEnabled(enabled);
      }

      /*
      1. 기본 Layer를 생성 관리하는 PageElementComponent 생성
            다음과 같이 3개의 레이어를 PageElementComponent에서 생성함.
            - bgLayer
            - twoLayer
            - threeLayer(옵션에 따라 생성)


      3. layer를 붙인 후 layer의 parent를 PageComponent로 적용해야함.
       */
      protected _onCreateElement(template:string=null) {
            let $element = document.createDocumentFragment();
            this.$frag = $element;
            if(this.isViewerMode) {
                  this._pageElementComponent = new PageElementComponent();

                  /*
                  페이지 컴포넌트로 넘어온 파라메터를 페이지ElementComponent에 넘기기
                  주의01:
                  왜? initProps로 넘기지 않는 이유는?
                  페이지를 읽게되면 초기화가 되니 때문


                  주의02:
                  create()호출 전에 설정해야함.


                   */
                  // params는 컴포넌트를 생성할 때와 openPageByName() 메서드를 호출할때
                  // 파라메터 정보가 넘어오게 될때 실행.
                  this._pageElementComponent.params=this.params;


                  this._pageElementComponent.executeViewerMode();
                  this._pageElementComponent.create({
                        id: ObjectUtil.generateID(),
                        name: "pageComponentElement",
                        x: 0,
                        y: 0,
                        props:{
                              createLayerInfo:{
                                    usingThreeLayer:this.usingThreeLayer
                              }
                        }
                  });



                  $($element).append(this._pageElementComponent.appendElement);
                  this._$errorPageInfo = $('<div style="display:none;width:100%;height:100%;position:absolute;text-align: center;margin-top: 50px;">페이지가 존재하지 않습니다.</div>');
                  $($element).append(this._$errorPageInfo);

                  $($element).append(this.progressBar.element);

                  this.progressBar.usingProgress = this.usingProgress;



                  // 수동으로 붙였기 때문에 이벤트를 발생시켜 줘야함.
                  this._pageElementComponent.dispatchEvent(new WVEvent(WVEvent.ADDED, this._pageElementComponent));
            }else{
                  $($element).attr("data-compname", "page-component");
                  $($element).append('<div style="width:100%;height:100%;display:flex;justify-content:center;align-items:center;"><img style="max-width:28px;width:100%;max-height:36px;height:100%;" src='+PageComponent.ICON+' /></div>');
            }
      }



      ////////////////////////////////////////////////////////////
      protected  _onCommitProperties():void{
            if(this._updatePropertiesMap.has("pageInfo.pageId")){
                  this.validateCallLater(this._validatePage);
            }
      }



      private _validatePage(){
            // editor모드에서는 실행되지 않음.
            if(this.isEditorMode==true)
                  return;

            let pageId=this.getGroupPropertyValue("pageInfo", "pageId");
            if(pageId.trim().length>0){
                  this._openPageById(pageId);
            }
      }


      /*
      컴포넌트가 container에 추가되면 자동으로 호출
       */
      public _onImmediateUpdateDisplay(): void {
            this._validatePage();
      }

      ////////////////////////////////////////////////////////////////////////











      ////////////////////////////////////////////////////////////////
      /*
      2018.09.26(ckkim)
      페이지가 로드되고 있는 시점에
            페이지를  로드하는 경우 모든 로딩을 취소 처리한다.
            1. 컴포넌트 인스턴스 생성 멈추기
            2. ajax을 이용한 페이지 정보 로딩 멈추기
            3. PageElementComponent의 기본 레이어에 생성된
               모든 컴포넌트 제거하기
       */
      private _resetPage():void{

            //1. 컴포넌트 인스턴스 생성 멈추기
            if(this._comInstanceLoader2){
                  this._comInstanceLoader2.stopClearInstanceLoader();
            }

            //2. ajax을 이용한 페이지 정보 로딩 멈추기
            if(this._cancelSource){
                  this._cancelSource.clear();
                  this._cancelSource = null;
            }

            //3. PageElementComponent의 기본 레이어에 생성된
            //모든 컴포넌트 제거하기
            // _pageElementComponent 인스턴스만 살리고 페이지 관련 내용을 모두 삭제
            if(this._pageElementComponent) {
                  this._pageElementComponent.resetPage();
            }
      }







      /*
      Viewer의 OpenPageCommand.ts와 거의 동일.
       */
      private async _openPageById(pageId:string){

            if(this.isEditorMode)
                  return false;

            var locale_msg = Vue.$i18n.messages.wv;


            this._$errorPageInfo.hide();

            if(pageId==null){
                  this._$errorPageInfo.show();
                  return false;
            }



            /*
            unload 되기 전 (before) 이벤트 발생
            console.log("1-3. 페이지 정보 로딩 전 unload wscript event 발생");
             */
            console.log("## PageComponent, 1-3. 페이지 정보 로딩 전 unload wscript event 발생");
            this._pageElementComponent.dispatchBeforeUnLoadScriptEvent();


            console.log("##페이지 컴포넌트, 1-2. 로딩전 레이어 숨기기");
            this._pageElementComponent.hiddenAllLayer();


            // 모든 내용 clear
            /*
            페이지가 로드되고 있는 시점에
            페이지를  로드하는 경우 모든 로딩을 취소 처리한다.
            1. 컴포넌트 인스턴스 생성 멈추기
            2. ajax을 이용한 페이지 정보 로딩 멈추기
            3. PageElementComponent의 기본 레이어에 생성된
               모든 컴포넌트 제거하기(레이어는 살려둔 상태에서 처리)

             */
            /*
            기존에 열린 정보 모두 제거하기
             PageElementComponent를 삭제하는것이 아님, 내부에 생성된 내용을  reset시키는 것임.
             */
            console.log("## PageComponent, 1-4. 페이지 정보 로딩 전 기존 열린 정보 모두 제거하기");
            this._resetPage();



            console.log("## PageComponent, 1-5. 페이지 닫은 후 unloaded wscript event 발생  ");
            this._pageElementComponent.dispatchUnLoadedScriptEvent();

            // flag값을 true
            this._isLoadingPage=true;


            let pageData: OpenPageData = null;
            try {
                  console.log("## PageComponent, 2-1. 페이지 정보 읽기 시작");
                  // 페이지 정보 로딩을 도중에 취소하기 위해 토큰 발생.
                  this._cancelSource = axios.CancelToken.source();
                  //this._cancelSource.cancel()을 이용해 취소 가능.
                  wemb.$editMainAreaResourceLoadingComponent.show(true);

                  // 페이지 정보 읽기
                  pageData = await window.wemb.pageManager.loadPageDataById(pageId);
                  console.log("## PageComponent, 2-2. 페이지 정보 읽기 완료");
            }catch(error){
                  this._cancelSource = null;
                  console.log("## PageComponent", error);
                  //alert(locale_msg.common.errorLoad+"An error occurred while reading the page information from the page component. Please check the page information.");
                  return;
            } finally {
                  wemb.$editMainAreaResourceLoadingComponent.hide();
            }


            try{
                  this._cancelSource = null;

                  /*
			resetProperties() 내부에서는 다음 내용이 호출됨.
			      immedateUpdateDisplay()
			      onAddedOnctainer()호출

			알림:
			     ViewerPageComponent, EditorPageComponent처럼 따로 배경설정을 할 필요는 없음.
			*/

                  console.log("## PageComponent 2-3. 페이지 정보 설정하기, ");
                  this._pageElementComponent.resetProperties(pageData.page_info);

                  console.log("## PageComponent 2-4. beforeload wscript event 발생");
                  this._pageElementComponent.dispatchBeforeLoadScriptEvent();




                  /*
                  2018.05.10(수)
                  - 팝업 호출 시
                    내부에 사용하는 Page2DComponent의 크기가 600*400으로 고정되는 문제
                    해결:
                    - Pag32DComponent가 독립적으로 사용하는 경우 페이지 크기를 에디터에서 변경하게 되지만
                      popup에서 사용하는 경우 크기를 지정하지 않기 때문에 600*400으로 사용됨.

                        일단, element의 크기를 페이지 크기로 늘리게 처리
                        추후 수정해야함.
                   */
                  if(this.usingPopupPage){
                        $(this._element).css({
                              width:this._pageElementComponent.width,
                              height:this._pageElementComponent.height
                        })
                  }
                  /* □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ */




                  console.log("## PageComponent 3-1. 리소스 로딩바 활성화"); // _comInstanceLoader2내부에서 로딩바 활성화 처리
                  console.log("## PageComponent 3-2. 화면에 붙일 컴포넌트 인스턴스 정보 생성, 시작");
                  // 레이어별 컴포넌트 정보 생성
                  let comInstanceInfoList: Array<any> = [];
                  comInstanceInfoList = comInstanceInfoList.concat(pageData.content_info.two_layer);

                  // threeLayer를 사용하는 경우에만 컴포넌트 정보 읽기
                  if(this.usingThreeLayer)
                        comInstanceInfoList = comInstanceInfoList.concat(pageData.content_info.three_layer);

                  console.log("## PageComponent 3-3, 화면에 붙일 컴포넌트 인스턴스 정보 생성 완료, 개수 = ", comInstanceInfoList.length);

                  // 프로그래스 처리를 위해 최대 length 설정
                  this.progressBar.setMaxLength(comInstanceInfoList.length);



                  // 화면에 인스턴스를 붙이기 전 DOM TREE 생성
                  this._element.appendChild(this.$frag);



                  /* □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ */
                  // 컴포넌트 인스턴스와 하기 시작
                  console.log("## PageComponent 3-4. 화면에 인스턴스 붙이기, 시작!");
                  let success = await this._comInstanceLoader2.startInstanceLoader(comInstanceInfoList, this.progressBar, this._pageElementComponent, this.exeMode as EXE_MODE)
                  if(success) {
                        // 정상적으로 이벤트가 로드된 경우
                        this._pageElementComponent.dispatchReadyScriptEvent();

                        this._startResourceLoading();
                  }else {
                        console.log("컴포넌트 생성이 정상적으로 이뤄지지 않았습니다.")
                  }

                  /* □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ */


            }catch(error){
                  console.log("## PageComponent",error);
                  //alert(locale_msg.common.errorLoad+"An error occurred while creating the component in the page component. Please check the page information.");
            }
      }



      private _startResourceLoading(){


            //let comInstanceListMap:Map<string, WVComponent> = new Map();
            let comInstanceList:Array<WVComponent> = this._pageElementComponent.comInstanceList;


            this._componentResourceLoader = new ComponentResourceLoader();
            this._componentResourceLoader.$on(ComponentResourceLoaderEvent.COMPLETED_ALL, ()=>{
                  this._completedLoadAllResource();
            })

            console.log("## PageComponent 4-1. PageComponent _startResourceLoading");
            this._componentResourceLoader.startResourceLoading(comInstanceList);
      }




      private _completedLoadAllResource(){
            console.log("## PageComponent 4-2. 컴포넌트 리소스 읽기, 완료");

            // 리로스 로더 파괴
            if(this._componentResourceLoader) {
                  this._componentResourceLoader.destroy();
                  this._componentResourceLoader = null;
            }


            this._isLoadingPage=false;

            this._pageElementComponent.isLoaded=true;
            console.log("## PageComponent 4-3. 화면에 배치된 모든 컴포넌트에 complete wscript 이벤트 발생");
            console.log("## PageComponent 4-4. 화면에 배치된 모든 컴포넌트의 onLoadPage 메서도 호출");
            let comInstanceList:Array<WVComponent> = this._pageElementComponent.comInstanceList;
            comInstanceList.forEach((instance:WVComponent)=>{
                  instance.dispatchWScriptEvent(WVComponentScriptEvent.COMPLETED);
                  // onLoadPage를 가지고 있는 컴포넌트만 onLoadPage()메서드 호출
                  if(instance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]){
                        instance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]();
                  }
            })

            // 최종 페이지 이벤트 발생
            console.log("## PageComponent 4-5. 페이지 이벤트 : LOADED 이벤트 발생");
            this._pageElementComponent.dispatchLoadedScriptEvent();



            //setTimeout(async ()=>{
            this._pageElementComponent.showAllLayer();

            this.dispatchEvent(new PageComponentEvent(PageComponentEvent.LOADED,this))
            this.dispatchWScriptEvent(WVComponentScriptEvent.COMPLETED);
            console.log("## PageComponent 4-4. 페이지 로딩, 리소스 로딩, 모든 처리 완료!");

            //},500)
      }

      ////////////////////////////////////////////////////////////////


}



WVComponentPropertyManager.attach_default_component_infos(PageComponent, {
      "info":{
            "version":"1.0.0",
            "componentName":"PageComponent"
      },

      "setter": {
            "width": 600,
            "height": 400,
      },

      "pageInfo":{
            "pageId":"",
            "usingPopupPage":false,    // 팝업 페이지 사용 유무 PopupWindow에서는 true로 사용 일반적인 경우는 false로 설정
            "usingProgress":false
      },

      "style": {
            "border": "0px solid #000000",
            "backgroundColor": "rgba(255, 255, 255, 0)",
            "borderRadius": 0,
            "overflow":"hidden"
      },
      createLayerInfo:{
            usingThreeLayer:false  // three레이어 사용 유무, popupWindow에서는 false로 사용.
      },
      // 팝업으로 사용되는 경우 외부에서 파라메터로 넘어온 값을 저장.
      params:{

      }


});




// 프로퍼티 패널에서 사용할 정보 입니다.
PageComponent["property_panel_info"] = [{
      template: "primary"
},
      {
            template: "pos-size-2d"
      }, {
            template: "border"
      }, {
            label: "Style",
            template: "vertical",
            children: [{
                  owner: "style",
                  name: "overflow",
                  label: "Overflow",
                  type: "select",
                  options: {
                        items: [
                              {label: "hidden", value: "hidden"},
                              {label: "auto", value: "auto"},
                              {label: "scroll", value: "scroll"}
                        ]
                  },
                  writable: true,
                  show: true,
                  description: "overflow"
            }]
      }
];

WVComponentPropertyManager.remove_property_group_info(PageComponent, "label");
