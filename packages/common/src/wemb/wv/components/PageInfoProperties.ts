import EditorPageComponent from "./EditorPageComponent";
/*
페이지 저장 또는 읽을때 사용됨.
 */
export default class PageInfoProperties {



      private _properties:any = {

      };
      public get properties(){
            return this._properties;
      }


      public get primary(){
            return this._properties.primary;
      }
      public get setter(){
            return this._properties.setter;
      }

      public get threeLayer(){
            return this._properties.setter.threeLayer;
      }

      public get background(){
            return this._properties.background;
      }

      public get events(){
            return this._properties.events;
      }

      /*
      2018.11.05(ckkim)
      script 속성 추가
       */
      public get script(){
            return this._properties.script;
      }





      public get id():string {
            return this._properties.primary.id;
      }
      public set id(value:string) {
            this._properties.primary.id =value;
      }

      public get name():string {
            return this._properties.primary.name;
      }
      public set name(value:string) {
            this._properties.primary.name =value;
      }

      public get secret():string {
            return this._properties.primary.secret;
      }
      public set secret(value:string) {
            this._properties.primary.secret = value;
      }
      public get type():string{
            return this._properties.primary.type;
      }
      public set type(value:string) {
            this._properties.primary.type = value;
      }

      public get master():string {
            return this._properties.primary.master;
      }
      public set master(value:string) {
            this._properties.primary.master = value;
      }



      public set width(value:number) {
            this._properties.setter.width=value;
      }
      public get width():number{
            return this._properties.setter.width;
      }

      public set height(value:number) {
            this._properties.setter.height=value;
      }
      public get height():number{
            return this._properties.setter.height;
      }


      public set grid(value:any){
            this.threeLayer.grid.x = value.x;
            this.threeLayer.grid.z = value.z;
      }
      public get grid(){
            return this.threeLayer.grid;
      }

      constructor() {
            // EditorPageComponent의 default_properties를 사용해 기본 속성을 만듦.
            // PageVO에서도 PageUIComponent가 사용됨
            $.extend(true,this._properties, EditorPageComponent["default_properties"]);
      }


      /*
      외부 페이지 정보를 바탕으로 값 설정하기.
       */
      public setPageInfoProperties(pageInfoProperties:any){

            $.extend(true,this._properties, pageInfoProperties);
      }



      /*
      페이지 저장 전 호출 되는 정보
      이전 페이지 정보와 호완성을 위해
      laeryName을 layer
       */
      public serializeMetaProperties():any {
            let properties:any = {
                  id:"",
                  name:"",
                  secret:"",
                  type:"",
                  master:"",
                  version:"",
                  props:{}
            };
            $.extend(true, properties.props, this._properties);



            /*
            primary 정보를 properties 로 만들기
             */
            properties.id = this.primary.id;
            properties.name = this.primary.name;
            properties.secret = this.primary.secret;
            properties.type = this.primary.type;
            properties.master = this.primary.master;

            delete properties.props.primary;


            /*
            info 정보를 properties로 만들기
             */
            properties.version = this._properties.info.version;
            delete properties.props.info;
            return properties;

      }

}
