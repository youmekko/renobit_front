import Vue from "vue";
import {IWVComponent} from "../../core/component/interfaces/ComponentInterfaces";
import DataService, { DatasetWorker } from "./DataService";
import { dto } from "../../../editor/windows/datasetManager/model/dto/DatasetDTO";
import DatasetInstDTO = dto.DatasetInstDTO;
import { TimersScheduler } from "./TimersScheduler";

/*
1. 뷰어에서만 사용함.
2. 뷰어 페이지에서 WScript에서 사용하는 컴포넌트 인스턴스를 변수로 만들기.
      인스턴스명은
      page._컴포넌트_인스턴스.name 이 됨.


3. page 객체에 생성하는 기능
      page._컴포넌트_인스턴스.name
      page.instanceList
      page.$masterLayer._컴포넌트_인스턴스.name
      page.$twoLayer._컴포넌트_인스턴스.name
      page.$threeLayer._컴포넌트_인스턴스.name



DataService.getInstance().execute(  pageId: string, dataset:any ):DatasetWorker
DataService.getInstance().call( pageId: string, datasetInst:DatasetInstDTO, param:any={} ):DatasetWorker
DataService.getInstance().callById( pageId: string, datasetInst:DatasetInstDTO, param:any={} ):DatasetWorker
DataService.getInstance().clear( pageId: string, param:any )
DataService.getInstance().clearAllPageWorkers( pageId: string )
DataService.getInstance().getPageServiceCount( pageId: string ):Number


TimersScheduler.getInstance().setTimeout( pageId:string, func:Function, time:number, execute:boolean = false):number;
TimersScheduler.getInstance().setInterval( pageId:string, func:Function, time:number, execute:boolean = false):number;
TimersScheduler.getInstance().clearTimeout( pageId:string, id:number );
TimersScheduler.getInstance().clearInterval(pageId:string, id:number );
TimersScheduler.getInstance().clearAllPageTimeout( pageId:string );
TimersScheduler.getInstance().clearAllPageInterval( pageId:string );
TimersScheduler.getInstance().clearAllByPageId( pageId:string );

 */


export interface IEditorPageObject{
      isLoaded:boolean;
}
export interface IViewerPageObject {
      isLoaded:boolean;
      dataService: DelegateDataService;
      $instanceList:Map<string, IWVComponent>;
      $masterLayer:Map<string, IWVComponent>;
      $twoLayer:Map<string, IWVComponent>;
      $threeLayer:Map<string, IWVComponent>;

      setTimeout(func:Function, time:number, execute:boolean):number;
      setInterval(func:Function, time:number, execute:boolean):number;
      clearTimeout(id:number):void;
      clearInterval(id:number):void;

}
export default class InstanceManagerOnPageViewer{

      private _pageId:string;
      private _instanceOwner:IViewerPageObject = null;
      constructor(){

      }

public create(pageId:string, instanceOwner:any){
      this.clear();

      this._pageId = pageId;
      this._instanceOwner = instanceOwner;

      // 내부에서 사용하는 객체 만들기
      this._attachInnerVariableElements();

      // 전역객체를 내부 변수로 만들기
      this._attachGlobalObjectToInnerVariable();
}

      /*
            기본 레이어  생성하기
      */
      private _attachInnerVariableElements(){
            if(this._instanceOwner==null){
                  console.log("######################인스턴스 owner가 존재하지 않습니다. ")
            }
           this._instanceOwner.$instanceList = new Map();

           this._instanceOwner.$masterLayer = new Map();
           this._instanceOwner.$twoLayer = new Map();
           this._instanceOwner.$threeLayer = new Map();
      }


      /*
      페이지로 접근해서 사용할 기능을 추가해주세요.
      _instanceOwner : IViewerPageObject이니
      IViewerPageObject에 속성을 선언 후 작성 해야함.
       */
      private _attachGlobalObjectToInnerVariable(){
            let timer = TimersScheduler.getInstance();

            this._instanceOwner.dataService = new DelegateDataService( this._pageId );
            this._instanceOwner.setTimeout=(func:Function, time:number, execute:boolean)=>{
                  return timer.setTimeout(this._pageId, func, time, execute );
            };

            this._instanceOwner.setInterval=(func:Function, time:number, execute:boolean)=>{
                  return timer.setInterval(this._pageId, func, time, execute );
            };

            this._instanceOwner.clearTimeout=(id:number)=>{
                  timer.clearTimeout(this._pageId, id );
            };

            this._instanceOwner.clearInterval=(id:number)=>{
                  timer.clearInterval(this._pageId, id );
            };
      }










      /*
	layerName에 해당하는 인스턴스 등록하기
	 */
      public register(instance:IWVComponent, layerName:string){
            if(this._instanceOwner==null){
                  console.log("######################인스턴스 owner가 존재하지 않습니다. ")
            }

            if(instance){

                  // 그룹과 페이지 객체 내부에 변수 만들기.
                 this._instanceOwner.$instanceList.set(instance.name, instance);
                 this._instanceOwner.$instanceList["_"+instance.name] = instance;
                 this._instanceOwner["_"+instance.name] = instance;

                 // 중요!
                 // 일반 컴포넌트에서 페이지의 전역 기능을 사용하는 용도로 사용.
                  // 컴포넌트 인스턴스에 페이지 객체를 설정하기.
                 instance.page = this._instanceOwner;
                  if(layerName!=""){
                       this._instanceOwner["$"+layerName].set(instance.name, instance);
                       this._instanceOwner["$"+layerName]["_"+instance.name] = instance;
                  }
            }
      }


      /*
	기존에 생성된 컴포넌트 인스턴스의 전역 인스턴스 삭제하기
	 */
      public clear(){
            if(this._instanceOwner==null){
                  //TODO 페이지 이동시마다 clear가 2번씩 호출 됨.. 확인 필요
                  return;
            }

            try {
                  // 2. two 영역에 있는 컴포넌트 제거하기
                  // 마스터 영역을 제외한 부분을 모두 삭제한다.
                  // 이때 instanceList에서도 two, three 내용을 모두 삭제한다..
                  if(this._instanceOwner.$twoLayer) {
                        this._instanceOwner.$twoLayer.forEach((value, key) => {
                              this._instanceOwner.$instanceList.delete(key);
                              delete this._instanceOwner.$instanceList["_" + key];
                              delete this._instanceOwner["_" + key];

                        });

                        this._instanceOwner.$twoLayer.clear();
                        this._instanceOwner.$twoLayer = null;
                        this._instanceOwner.$twoLayer = new Map();
                  }
                  if(this._instanceOwner.$threeLayer) {
                        // 3. three 영역 컴포넌트 인스턴스 제거하기
                        this._instanceOwner.$threeLayer.forEach((value, key) => {
                              this._instanceOwner.$instanceList.delete(key);
                              delete this._instanceOwner.$instanceList["_" + key];
                              delete this._instanceOwner["_" + key];

                        });

                        this._instanceOwner.$threeLayer.clear();
                        this._instanceOwner.$threeLayer = null;
                        this._instanceOwner.$threeLayer = new Map();
                  }

                  this._instanceOwner.dataService.clearAll();
                  TimersScheduler.getInstance().clearAllByPageId( this._pageId );

                  // 동적으로 추가한 항목 삭제.
                  delete this._instanceOwner.dataService;
                  delete this._instanceOwner.setTimeout;
                  delete this._instanceOwner.setInterval;
                  delete this._instanceOwner.clearTimeout;
                  delete this._instanceOwner.clearInterval;
                  this._instanceOwner = null
            }catch(error){
                  console.log("clear 도중 에러 발생", error);
            }
      }
}

/**
 * Dataservice가 pageId와 함께 호출돼야하는데, 이를 자동 처리해주는 delegate객체
 * dataService를 pageId 인자를 생략하여 호출 할 수 있게 한번 래핑하여 제공
 * */
export class DelegateDataService{
      private dataService: DataService;
      private pageId:string = "";
      constructor(pageId: string){
            this.pageId = pageId;
            this.dataService = DataService.getInstance();
      }

      public execute( dataset:any ):DatasetWorker{
            return  this.dataService.execute(  this.pageId, dataset );
      }

      public call( datasetName:string, param:any={} ):DatasetWorker{
            console.log("@@ call ");
            return this.dataService.call( this.pageId, datasetName, param );
      }

      public callById(id:string, param:any={} ):DatasetWorker{
            return  this.dataService.callById(  this.pageId, id, param );
      }

      public clear( param:any ){
            this.dataService.clear(  this.pageId, param );
      }

      public clearAll(){
            this.dataService.clearAllPageWorkers(this.pageId);
      }

      public get serviceCount():Number{
            return  this.dataService.getPageServiceCount(this.pageId);
      }
}
