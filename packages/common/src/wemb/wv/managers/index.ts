import ComponentInstanceFactory from './ComponentInstanceFactory'
import ComponentInstanceLoader from './ComponentInstanceLoader'
import ComponentLibraryManager from './ComponentLibraryManager'
import ConfigManager from './ConfigManager'
import DataService from './DataService'
import ExtensionInstanceManager from './ExtensionInstanceManager'
import ExternalWindowManager from './ExternalWindowManager'
import FileManager from './FileManager'
import FPSManager from './FPSManager'
import HookManager from './HookManager'
import InstanceManagerOnPageViewer from './InstanceManagerOnPageViewer'
import KeyboardCommandManager from './KeyboardCommandManager'
import LocaleManager from './LocaleManager'
import MainMenuManager from './MainMenuManager'
import MenusetDataManager from './MenusetDataManager'
import PageHistoryManager from './PageHistoryManager'
import PageManager from './PageManager'
import PageTreeDataManager from './PageTreeDataManager'
import PopupManager from './PopupManager'
import SessionCheckingManager from './SessionCheckingManager'
import SessionScheduler from './SessionScheduler'
import SoundManager from './SoundManager'
import TemplateDataManager from './TemplateDataManager'
import TimersScheduler from './TimersScheduler'
import UserInfoManager from './UserInfoManager'
import WindowsManager from './WindowsManager'


const managers = {
    ComponentInstanceFactory, 
    ComponentInstanceLoader, 
    ComponentLibraryManager, 
    ConfigManager, 
    DataService, 
    ExtensionInstanceManager, 
    ExternalWindowManager, 
    FileManager, 
    FPSManager, 
    HookManager, 
    InstanceManagerOnPageViewer, 
    KeyboardCommandManager,
    LocaleManager,
    MainMenuManager,
    MenusetDataManager, 
    PageHistoryManager,
    PageManager,
    PageTreeDataManager, 
    PopupManager,
    SessionCheckingManager,  
    SessionScheduler, 
    SoundManager,
    TemplateDataManager, 
    TimersScheduler,
    UserInfoManager,
    WindowsManager,
}

export default managers