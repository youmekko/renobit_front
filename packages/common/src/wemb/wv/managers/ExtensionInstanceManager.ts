import Vue from "vue";
import configApi from "../../../common/api/configApi";
import sendErrorMessage from "../../../common/api/sendErrorMessage";
import {WORK_LAYERS} from "../layer/Layer";
import {CONFIG_MODE, ERROR_TYPE, EXE_MODE} from "../data/Data";

import {http} from "../../../wemb/http/Http";
import api = http.api;
import extensionApi from "../../../common/api/extensionApi";

/*
만들고 나서 아직 적용하지 않음.
 */
type ExtensionInstanceListInfo = {
            using:boolean
            editor: {
                  panels: any[],
                  plugins: any[]
            }

            viewer: {
                  plugins: any[]
            }
}

export enum CONFIG_PROPERTY_GROUP_NAME {
      MODE = "mode",
      REMOTE = "remove",
      LOADING = "loading",
      VIEWER = "viewer",
      EXTENSION = "extension"
}


export default class ExtensionInstanceManager {
      private static _instance: ExtensionInstanceManager = null;
      private _exeMode:string="";
      public _data:ExtensionInstanceListInfo=null;
      constructor(exeMode = EXE_MODE.EDITOR) {
            /*
		삭제예정:  2018.05.31
		exeMode를 제거
		exeMode로 받을 예정
		 */
            this._exeMode = exeMode;
      }


      public static getInstance(): ExtensionInstanceManager {
            if (ExtensionInstanceManager._instance == null) {
                  ExtensionInstanceManager._instance = new ExtensionInstanceManager();
            }

            return ExtensionInstanceManager._instance;
      }


      public async startLoading():Promise<boolean> {
            this._data = await extensionApi.loadData();
            if(this._data==null){
                  return false;
            }

            return true;
      }

      public get usingExtension() {
            try {
                  return this._data.using;
            } catch(error){
                  return false;
            }
      }

      public get editorModeInstanceList(){
            try {
                  return this._data.editor;
            } catch(error){
                  return null;
            }
      }

      public get viewerModeInstanceList(){
            try {
                  return this._data.viewer;
            } catch(error){
                  return [];
            }
      }

}
