import Vue from "vue";
import componentApi from "../../../common/api/componentApi";


import WVComponent from "../../core/component/WVComponent";
import {event} from "../events/LoadingEvent";
import LoadingEvent = event.LoadingEvent;
import WVComPropertyManager from "../../core/component/WVComponentPropertyManager";

import WVDOMComponent from "../../core/component/2D/WVDOMComponent";
import WV3DComponent from "../../core/component/3D/WV3DComponent";
import WV3DComponentPropertyManager from "../../core/component/3D/manager/WV3DComponentPropertyManager";
import CPUtil from "../../core/utils/CPUtil";
import WVSVGComponent from "../../core/component/2D/WVSVGComponent";
import WVPointComponent from "../../core/component/2D/WVPointComponent";
import WVSVGStreamLineComponent from "../../core/component/2D/WVSVGStreamLineComponent";
import {WVPointComponentEvent} from "../../core/events/WVPointComponentEvent";
import {Matrix} from "../../core/geom/Matrix";
import Point from "../../core/geom/Point";

import LoaderManager from "../../core/component/3D/manager/LoaderManager";
import MeshManager from "../../core/component/3D/manager/MeshManager";

import ExtensionPanelCore from "../../core/extension/ExtensionPanelCore";
/* 내부 컴포넌트*/
import PageComponent from "../components/PageComponent";
import PopupWindow from "../components/PopupWindow";
import {EXE_MODE} from "../data/Data";
import RollingPageComponent from "../components/RollingPageComponent";
import TopViewPanel from "../../core/component/3D/TopViewPanel";
import NWV3DComponent from "../../core/component/3D/NWV3DComponent";
import ThreeUtil from "../../core/utils/ThreeUtil";
import NLoaderManager from "../../core/component/3D/manager/NLoaderManager";
import {CPLogger} from "../../core/utils/CPLogger";
import WV3DResourceComponent from "../../core/component/3D/WV3DResourceComponent";
import {EXTENSION_TYPE, ComponentLibraryFileInfo} from "./interfaces/ComponentLibraryInterfaces";
import ExtensionPluginCore from "../../core/extension/ExtensionPluginCore";
import {WVToolTipManager} from "../../core/component/3D/manager/WVToolTipManager";
import WVSeverityComponent from "../../core/component/2D/WVSeverityComponent";
import Spinner from "../../core/utils/Spinner";
import ConfigManager from "./ConfigManager";
import WV3DLabel from "../../core/component/3D/drawable/WV3DLabel";
import WVMeshDecorator from "../../core/component/3D/drawable/WVMeshDecorator";
import WV3DInstanceProxy from "../../core/component/3D/proxy/WV3DInstanceProxy";
import WVDataGrid from "../../core/component/2D/drawable/WVDataGrid";
import WV3DIconLabel from "../../core/component/3D/drawable/WV3DIconLabel";
import WV3DSprite from "../../core/component/3D/drawable/WV3DSprite";
export enum LibraryLoadingState {
      READY = "ready",
      LOADING = "loading",
      COMPLETED = "completed"
}



/*
컴포넌트 라이브러리는
viewer에서만 사용하는 라이브러리
editor에서만 사용하는 라이브러리
모두 에서 사용하는 라이브러리로 나뉜다.
 */
export enum ComponentLibraryExeMode {
      BOTH = "both",
      EDITOR = "editor",
      VIEWER = "viewer"
}


/*
파일 구조
{
    "list": [{
        "name": "SVGBrokenLineComponent",
        "file_name": "client/packs/2d_pack/components/SVGBrokenLineComponent/SVGBrokenLineComponent.js"
    }, {
        "name": "SVGCurveComponent",
        "file_name": "client/packs/2d_pack/components/SVGCurveComponent/SVGCurveComponent.js"
    }, {
        "name": "SVGCircleComponent",
        "file_name": "client/packs/2d_pack/components/SVGCircleComponent/SVGCircleComponent.js"
    }, {
        "name": "SVGLineComponent",
        "file_name": "client/packs/2d_pack/components/SVGLineComponent/SVGLineComponent.js"
    }, {
        "name": "SVGRectComponent",
        "file_name": "client/packs/2d_pack/components/SVGRectComponent/SVGRectComponent.js"
    }, {
        "name": "BasicGridComponent",
        "file_name": "client/packs/2d_pack/components/BasicGridComponent/BasicGridComponent.js",
        "style_name": "client/packs/2d_pack/components/BasicGridComponent/BasicGridComponent.css",
        "dependencies": {
            "libs": [{
                "file_name": "static/libs/jquery-fixed-header-table/jquery.CongelarFilaColumna.js",
                "mode": "both"
            }],
            "styles": [{
                "file_name": "static/libs/jquery-fixed-header-table/ScrollTabla.css",
                "mode": "both"
            }]
        }
    }]
}

 */

export default class ComponentLibraryManager {

      private _componentListMap = new Map();
      /*
	기본 인스턴스 이름이 등록되는 맵
	 */
      private _componentInstanceNameMap = new Map();
      /*
	컴포넌트 패스명이 등록되는 맵
	 */
      private _componentPathListMap = new Map();
      private _componentInfoList = null;
      private _componentPanelInfoList = null;
      private _2DcomponentPanelInfoMap = null;
      get twoComponentPanelInfoMap(){
            return this._2DcomponentPanelInfoMap;
      }
      private _3DcomponentPanelInfoMap = null;
      get threeComponentPanelInfoMap(){
            return this._3DcomponentPanelInfoMap;
      }

      private _$bus = new Vue();
      private _loaderManager: ComponentLibraryLoader = null;
      private _loadCompleted: boolean = false;
      private _libLoadingState: LibraryLoadingState = LibraryLoadingState.READY;



      private _panelListMap = new Map();
      private _panelInstanceNameMap = new Map();
      private _panelPathListMap = new Map();



      private _pluginListMap = new Map();
      private _pluginInstanceNameMap = new Map();
      private _pluginPathListMap = new Map();

      /*
      2018.10.20(ckkim)
      동적 컴포넌트 템플릿 추가를 위해 컴포넌트 프로퍼티 템플릿 리스트 관리 기능 추가

       */
      private _componentPropertyTemplateList = [];


      public $on(eventName: string, data: any) {
            this._$bus.$on(eventName, data);
      }


      constructor(exeMode: EXE_MODE) {
            if(ConfigManager.getInstance().serverType=="node")
                  this._loaderManager = new ComponentLibraryLoaderNode(exeMode, this._$bus);
            else
                  this._loaderManager = new ComponentLibraryLoaderJAVA(exeMode, this._$bus);



            this._initComponentPropertyTemplateList();
      }


      /*
	코어 컴포넌트 등록
	 */
      private _registerCoreComponentClass() {
            // 전역 컴포넌트 등록;
            window.WVComponent = WVComponent;
            window.WVDOMComponent = WVDOMComponent;
            window.WVSeverityComponent = WVSeverityComponent;
            window.WVPropertyManager = WVComPropertyManager;
            window.WV3DComponent = WV3DComponent;
            window.NWV3DComponent = NWV3DComponent;
            window.WV3DSprite     = WV3DSprite;
            window.WVToolTipManager = WVToolTipManager;
            window.WV3DResourceComponent = WV3DResourceComponent;
            window.WV3DPropertyManager = WV3DComponentPropertyManager;
            window.WVSVGComponent = WVSVGComponent;
            window.WVPointComponent = WVPointComponent;
            window.WVSVGStreamLineComponent = WVSVGStreamLineComponent;
            window.CPUtil = CPUtil;
            window.Spinner = Spinner;
            window.CPLogger = CPLogger;
            window.WV3DLabel = WV3DLabel;
            window.WV3DIconLabel = WV3DIconLabel;
            window.Point = Point;
            window.TopViewPanel = TopViewPanel;
            window.WVDataGrid = WVDataGrid;

            window.WVPointComponentEvent = WVPointComponentEvent;
            window.WVMeshDecorator  = WVMeshDecorator;
            window.WV3DInstanceProxy = WV3DInstanceProxy;

            window.MeshManager = MeshManager;
            window.LoaderManager = LoaderManager;
            window.NLoaderManager = NLoaderManager;
            window.ThreeUtil = ThreeUtil;


            window.ExtensionPanelCore = ExtensionPanelCore;
            window.ExtensionPluginCore = ExtensionPluginCore;


      }


      /*
	내부 지원 컴포넌트 등록.
	 */
      private _registerInnerComponentClass() {
            this._componentListMap.set(PageComponent.NAME, PageComponent);
            this._componentListMap.set(PopupWindow.NAME, PopupWindow);
            this._componentListMap.set(RollingPageComponent.NAME, RollingPageComponent);


            this._componentInstanceNameMap.set(PageComponent.NAME, "page");
            this._componentInstanceNameMap.set(PopupWindow.NAME, "popup");
            this._componentInstanceNameMap.set(RollingPageComponent.NAME, "rolling");

            this._componentPathListMap.set(RollingPageComponent.NAME, "custom/packs/2d_pack/components/basic/RollingPageComponent");
            this._componentPathListMap.set(PageComponent.NAME, "custom/packs/2d_pack/components/basic/PageComponent");

      }

      public startLoading() {
            var locale_msg = Vue.$i18n.messages.wv;
            if (this._libLoadingState == LibraryLoadingState.COMPLETED) {
                  Vue.$message('message code: []<br> ' + locale_msg.componentManager.loadMsg01, locale_msg.common.notification, {
                        confirmButtonText: 'OK',
                        type: "info",
                        dangerouslyUseHTMLString: true
                  });
                  return;
            }
            if (this._libLoadingState == LibraryLoadingState.LOADING) {
                  Vue.$message('message code: []<br> ' + locale_msg.componentManager.loadMsg01, locale_msg.common.notification, {
                        confirmButtonText: 'OK',
                        type: "info",
                        dangerouslyUseHTMLString: true
                  });
                  return;
            }

            this._libLoadingState = LibraryLoadingState.LOADING;


            // 코어 컴포넌트 클래스 등록하기
            this._registerCoreComponentClass();

            // 내부에서 생성되어 사용되는 컴포넌트 등록
            this._registerInnerComponentClass();


            // 이벤트 등록
            this._$bus.$on(LoadingEvent.PROGRESS, (event: LoadingEvent) => {

                  let componentInfo: ComponentLibraryFileInfo = event.data.componentInfo;
                  if (componentInfo.loadingFileLength == 0)
                        return;



                  if(componentInfo.type ==null || componentInfo.type==EXTENSION_TYPE.COMPONENT) {
                        /*
                        읽기 성공한 경우
                         */
                        // name을 키로 클래스 추가하기
                        this._addComponent(componentInfo.name, eval(componentInfo.name));

                        // 컴포넌트 인스턴스 명 추가하기(인스턴스명을 만들때 사용
                        this._addInstanceName(componentInfo.name, componentInfo.instanceName);
                        // name을 키로 클래스패스 추가하기
                        this._addComponentPath(componentInfo.name, componentInfo.file_name);

                        return;
                  }


                  if(componentInfo.type==EXTENSION_TYPE.PANEL) {

                        /*
                        읽기 성공한 경우
                         */

                        this._addExtensionElement(componentInfo);

                        return;
                  }




            })

            this._$bus.$on(LoadingEvent.COMPLETED, () => {
                  this._loadCompleted = true;
            })

            // 라이브러리 로딩 시작
            this._loaderManager.startLoading();


      }



      public getComponentByName(name) {
            return this._componentListMap.get(name);
      }


      getComponentPath(name) {
            return this._componentPathListMap.get(name);
      }

      private _addComponent(componentName, componentClass) {
            this._componentListMap.set(componentName, componentClass);
      }

      private _addInstanceName(componentName, instanceName) {
            this._componentInstanceNameMap.set(componentName, instanceName);
      }

      /*
	file_name에서 패스 정보 구하기
	예)
	"file_name": "client/packs/2d_pack/components/SVGRectComponent/SVGRectComponent.js"

	path = client/packs/2d_pack/components/SVGRectComponent;
	 */
      _addComponentPath(componentName, fullFileName) {
            let aryInfo = fullFileName.split("/");
            // 마지막인 파일이름 삭제하기
            aryInfo.pop();

            // 패스 다시 만들기
            let strPathName = aryInfo.join("/");
            this._componentPathListMap.set(componentName, strPathName);
      }


      /*
	name에 해당하는 컴포넌트 인스턴스 생성하기
	 */
      public createComponentInstance(componentName) {
            let ComponentClass = this._componentListMap.get(componentName);
            let instance = null;
            if (ComponentClass) {
                  instance = new ComponentClass();
            }
            return instance;
      }

      /*
	컴포넌트 명에 해당하는 기본 인스턴스 구분자 명 구하기
	기본 인ㅅ그턴스 구분자 명은 컴포넌트 라이브러리를 로딩할때 등록
	 */
      getInstanceSeparator(componentName) {

            return this._componentInstanceNameMap.get(componentName);
      }





      ////////////////////////////////////////////////






      _addExtensionElement(extensionInfo:ComponentLibraryFileInfo) {
            switch(extensionInfo.type) {


                  case EXTENSION_TYPE.PANEL :

                        // name을 키로 클래스 추가하기
                        this._panelListMap.set(extensionInfo.name, eval(extensionInfo.name));

                        // 컴포넌트 인스턴스 명 추가하기(인스턴스명을 만들때 사용
                        this._panelInstanceNameMap.set(extensionInfo.name, extensionInfo.instanceName);
                        // name을 키로 클래스패스 추가하기
                        /*
				file_name에서 패스 정보 구하기
				예)
					"file_name": "client/packs/2d_pack/components/SVGRectComponent/SVGRectComponent.js"
				     path = client/packs/2d_pack/components/SVGRectComponent;
				*/
                        this._panelPathListMap.set(extensionInfo.name, this._getExtensionElementPath(extensionInfo.file_name));
                        break;

                  case EXTENSION_TYPE.PLUGIN :

                        // name을 키로 클래스 추가하기
                        this._pluginListMap.set(extensionInfo.name, eval(extensionInfo.name));
                        // 컴포넌트 인스턴스 명 추가하기(인스턴스명을 만들때 사용

                        this._pluginInstanceNameMap.set(extensionInfo.name, extensionInfo.instanceName);
                        // name을 키로 클래스패스 추가하기
                        /*
				file_name에서 패스 정보 구하기
				예)
					"file_name": "client/packs/2d_pack/components/SVGRectComponent/SVGRectComponent.js"
				     path = client/packs/2d_pack/components/SVGRectComponent;
				*/
                        this._pluginPathListMap.set(extensionInfo.name, this._getExtensionElementPath(extensionInfo.file_name));
                        break;


            }
      }


      _getExtensionElementPath(fullFileName){
            let aryInfo = fullFileName.split("/");
            // 마지막인 파일이름 삭제하기
            aryInfo.pop();

            // 패스 다시 만들기
            let strPathName = aryInfo.join("/");
            return strPathName;
      }





      getExtentionPath(type, name) {
            switch(type) {
                  case EXTENSION_TYPE.PANEL :
                        return this._panelPathListMap.get(name);
                  case EXTENSION_TYPE.PLUGIN :
                        return this._pluginPathListMap.get(name);
            }


            return null;

      }


      /*
      외부 정보(DB또는 파일) 컴포넌트 패널 정보 생성하는 부분.
       */
      async  getComponentPanelInfoList(reload:boolean=false){
            if(this._componentPanelInfoList==null || reload==true) {
                  //console.log(this._componentFileInfoList);
                  this._componentPanelInfoList = {
                        "two_layer": {
                              "label": "2D Components",
                              "children": []
                        },
                        "three_layer": {
                              "label": "3D Components",
                              "children": []
                        }
                  }


                  try {
                        var tempInfoList =  await this._loaderManager.mixComponentPanelInfoList();


                        if(ConfigManager.getInstance().serverType=="java") {
                              this._componentPanelInfoList = tempInfoList;
                        } else {
                              this._componentPanelInfoList.two_layer.children = tempInfoList.two_layer.concat([]);
                              this._componentPanelInfoList.three_layer.children = tempInfoList.three_layer.concat([]);
                        }

                        //this._componentPanelInfoList = this._loaderManager._componentPanelInfoList

                        // 2D, 3D 패널 맵 생성하기
                        this._createComponentPanelInfoMap();

                  } catch (error) {
                        console.log("컴포넌트 패널 정보를 가져오는 도중 에러 발생 ", error)
                  }
            }

            return this._componentPanelInfoList;
      }

      /*
      2D, 3D 패널 정보 맵 생성

      key: label
       */
      private _createComponentPanelInfoMap(){
            if(this._2DcomponentPanelInfoMap==null)
                  this._2DcomponentPanelInfoMap = new Map();
            this._2DcomponentPanelInfoMap.clear();



            for(var i=0;i<this._componentPanelInfoList.two_layer.children.length;i++){
                  var group = this._componentPanelInfoList.two_layer.children[i];
                  try {

                        group.children.forEach((comInfo) => {
                              if(comInfo.hasOwnProperty("id")) {
                                    this._2DcomponentPanelInfoMap.set(comInfo.id, comInfo);
                              } else if(comInfo.hasOwnProperty("label")) {
                                    this._2DcomponentPanelInfoMap.set(comInfo.label, comInfo);
                              } else {
                                    console.warn("RENOBIT ", "컴포넌트 패널정보에서 label이 존재하지 않습니다.", comInfo);
                              }
                        })
                  }catch(error){
                        console.warn("RENOBIT ", "컴포넌트 패널 정보 맵을 만드는 도중 에러 발생 ", error);
                  }


            }

            if(this._3DcomponentPanelInfoMap==null)
                  this._3DcomponentPanelInfoMap = new Map();
            this._3DcomponentPanelInfoMap.clear();

            for(var i=0;i<this._componentPanelInfoList.three_layer.children.length;i++){
                  var group = this._componentPanelInfoList.three_layer.children[i];
                  try {

                        group.children.forEach((comInfo) => {
                              if(comInfo.hasOwnProperty("id"))
                                    this._3DcomponentPanelInfoMap.set(comInfo.id, comInfo);
                              else {
                                    console.warn("RENOBIT ", "컴포넌트 패널정보에서 label이 존재하지 않습니다.", comInfo);
                              }
                        })
                  }catch(error){
                        console.warn("RENOBIT ", "컴포넌트 패널 정보 맵을 만드는 도중 에러 발생 ", error);
                  }


            }

      }


      /*
      2018.09.21(컴포넌트 패널 정보 구하기) 추가
            - 주로 확장 요소에서 사용게 됨.
       */
      getComponentPanelInfoByName(category:string, componentName:string){
            var info = null;

            if(category=="2D" || category=="two_layer"){

                  for(var i=0;i<this._componentPanelInfoList.two_layer.children.length;i++){
                        var group = this._componentPanelInfoList.two_layer.children[i];
                        info = group.children.find((comInfo)=>{
                              return comInfo.name==componentName;
                        })

                        if(info!=null)
                              return info;
                  }

            }else if( category=="3D" || category=="three_layer" ){
                  for(var i=0;i<this._componentPanelInfoList.three_layer.children.length;i++){
                        var group = this._componentPanelInfoList.three_layer.children[i];
                        info = group.children.find((comInfo)=>{
                              return comInfo.name==componentName;
                        })

                        if(info!=null)
                              return info;
                  }
            }

            return null;

      }

      /*
      2018.12.13(ckkim)
      컴포넌트이름으로 label 구하기
       */
      getComponentLabelByName(category:string, componentName:string){
            var info = null;

            if(category=="2D" || category=="two_layer" || category=="twoLayer"){

                  for(var i=0;i<this._componentPanelInfoList.two_layer.children.length;i++){
                        var group = this._componentPanelInfoList.two_layer.children[i];
                        info = group.children.find((comInfo)=>{
                              return comInfo.name==componentName;
                        })

                        if(info!=null)
                              return info.id;

                  }

            }else if( category=="3D" || category=="three_layer" || category=="threeLayer"){
                  for(var i=0;i<this._componentPanelInfoList.three_layer.children.length;i++){
                        var group = this._componentPanelInfoList.three_layer.children[i];
                        info = group.children.find((comInfo)=>{
                              return comInfo.name==componentName;
                        })

                        if(info!=null)
                              return info.id;


                  }
            }



            return null;

      }



      /*
      2018.12.12 label로 컴포넌트 패널 정보 구하기
            - 주로 확장 요소에서 사용게 됨.
       */
      getComponentPanelInfoByLabel(layerName:string, id:string){

            if(layerName=="2D" || layerName=="two_layer" || layerName=="twoLayer"){
                  return this._2DcomponentPanelInfoMap.get(id);

            }else if( layerName=="3D" || layerName=="three_layer" || layerName=="threeLayer"){
                  return this._3DcomponentPanelInfoMap.get(id);
            }


            return null;

      }


      ////////////////////////////////////////////////














      ////////////////////////////////////////////////
      /*
      2018.10.20(ckkim)
      - 컴포넌트 프로퍼티 패널 템플릿 동적추가를 위한 작업 추가.

      */
            // 컴포넌트 프로퍼티 패널 템플릿 기본 설정
      private _initComponentPropertyTemplateList(){
                  this._componentPropertyTemplateList = ["page-select-input", "document", "primary", "font-type", "pos-size-2d", "cursor", "label", "background", "background-gradient", "border", "border-radius", "padding", "vertical", "pos-size-3d", "label-3d", "default-3d", "resource", "menuset", "template-manager", "font", "w-script", "vector", "text-info", 'color-3d', 'points', 'tooltip', 'text-shadow', 'gradient', 'stroke', 'event-border', 'table-background', 'stroke-gradient', 'shadow'];
      }


      public getComponentPropertyTemplateList(){
            return  this._componentPropertyTemplateList;
      }

      /*
    동적으로 컴포넌트 프로퍼티 템필릇 추가
     */
      public addComponentPropertyTemplate(name:string){
            this._componentPropertyTemplateList.push(name);
      }


}


abstract class ComponentLibraryLoader {


      protected _packList:string[] = [];
      private _componentListMap = new Map();
      protected _componentFileInfoList = null;
      protected _componentPanelInfoList = null;
      private _loadIndex = 0;
      private _loadFileManager: LoadFileManager;

      private _$bus = null;


      constructor(exeMode: EXE_MODE, $parentBus: Vue) {
            this._loadFileManager = new LoadFileManager(exeMode);
            this._$bus = $parentBus;
      }


      public $on(eventName: string, data: any) {
            this._$bus.$on(eventName, data);
      }


      public abstract async startLoading();
      public abstract async mixComponentPanelInfoList();




      /*
      실제 컴포넌트 정보 읽기 시작.
       */
      protected _executeStartLibraryLoading() {
            //console.log("@@ node _executeStartLibraryLoading() ", this._componentFileInfoList.length)
            let event = new LoadingEvent(LoadingEvent.LOAD_START, this, {
                  loadLength: this._componentFileInfoList.length,
                  currentIndex: 0,
            });
            this._$bus.$emit(LoadingEvent.LOAD_START, event);

            this._loadComponentAt(0);
      }

      /*
	load 순서
	  1. 의존 파일(dependencies)읽기
		  1-1. libs 읽기
		  1-2. styles 읽기
		  1-3. core 읽기
	  2. JS 파일 읽기
		- JS 파일이 없으면 에러
	  3. style 파일 읽기
	 */
      _loadComponentAt(index: number) {
            /* 디버깅 용 */
            let componentInfo: ComponentLibraryFileInfo = <ComponentLibraryFileInfo>this._componentFileInfoList[index];

            if(componentInfo==null){
                  this._nextLoadComponent();
                  return;
            }

            // type이 없는 경우 기본 값을 component로 설정
            if(componentInfo["type"]==null){
                  componentInfo.type = EXTENSION_TYPE.COMPONENT;
            }

            //console.log("\t\t\t### ", componentInfo.name + " 컴포넌트 관련 파일 읽기 시작,  ", this._loadIndex + 1, "/", this._componentFileInfoList.length);


            // 컴포넌트 파일 정보에서 로딩할 파일 정보 구하기
            let fileList: Array<any> = this._createLoadingFileList(componentInfo);

            if (fileList.length == 0) {
                  let event = new LoadingEvent(LoadingEvent.PROGRESS, this, {
                        loadLength: this._componentFileInfoList.length,
                        currentIndex: index + 1,
                        componentInfo: componentInfo,
                        loadingFileLength: 0
                  });
                  this._$bus.$emit(LoadingEvent.PROGRESS, event);


                  this._nextLoadComponent();
                  return;
            }


            this._loadFileManager.startFileLoading(fileList).then((result) => {
                  //console.log("\t\t\t### " + componentInfo.name + " 컴포넌트 관련 파일 읽기 완료,", this._loadIndex + 1, "/", this._componentFileInfoList.length);

                  this._componentListMap.set(componentInfo.name, true);
                  let event = new LoadingEvent(LoadingEvent.PROGRESS, this, {
                        loadLength: this._componentFileInfoList.length,
                        currentIndex: index + 1,
                        componentInfo: componentInfo,
                        loadingFileLength: fileList.length
                  });
                  this._$bus.$emit(LoadingEvent.PROGRESS, event);


                  setTimeout(() => {
                        this._nextLoadComponent();
                  }, 3);
            });
      }

      /*
	로딩할 파일 목록 만들기
	중요:
		라이브러리 읽는 순서가 중요
		1. 외존 파일 읽기
		2. js 파일 읽기
		3. style 파일 읽기

	 */
      private _createLoadingFileList(componentInfo: ComponentLibraryFileInfo): Array<any> {

            let fileList = [];


            // 컴포넌트 존재 유무 확인
            if (this._componentListMap.has(componentInfo.name) == true) {
                  console.log("step 03, " + componentInfo.name + "라는 컴포넌트 이름은 이미 등록된 컴포넌트 입니다. 다른 이름으로 변경해주세요.");
                  return [];
            }


            // 1. 의존 파일(dependencies)읽기
            if (componentInfo.hasOwnProperty("dependencies") == true && componentInfo["dependencies"]) {
                  let dependencies = componentInfo.dependencies;
                  // 라이브러리
                  if (dependencies.hasOwnProperty("libs") == true) {
                        fileList.push(...dependencies.libs);
                  }

                  if (dependencies.hasOwnProperty("styles") == true) {
                        fileList.push(...dependencies.styles);
                  }

                  if (dependencies.hasOwnProperty("core") == true) {
                        fileList.push(...dependencies.core);
                  }
            }
            // 2. JS 파일 읽기(필수)
            if (componentInfo.hasOwnProperty("file_name") == true && componentInfo["file_name"]) {
                  fileList.push({
                        "file_name": componentInfo.file_name
                  })
            } else {
                  console.log("step 03, #### " + componentInfo.name + "에 해당하는 컴포넌트 파일 정보(file_name)이 존재하지 않습니다. 확인 후 다시 실행해주세요.");
                  return [];
            }

            // 3. style 파일 읽기
            if (componentInfo.hasOwnProperty("style_name") == true && componentInfo["style_name"]) {
                  fileList.push({
                        "file_name": componentInfo.style_name
                  })
            }

            return fileList;

      }


      _nextLoadComponent() {
            this._loadIndex++;
            if (this._componentFileInfoList.length <= this._loadIndex) {
                  this._completedLoad();

            } else {

                  this._loadComponentAt(this._loadIndex);
            }
      }


      protected _completedLoad() {
            let event = new LoadingEvent(LoadingEvent.COMPLETED, this);
            this._$bus.$emit(LoadingEvent.COMPLETED, event);
            this._clearLoadingInfo();
      }


      //  로딩이 끝난 후 로딩 관련 프로퍼티를 제거하기
      private _clearLoadingInfo() {
            this._componentFileInfoList = null;
            this._componentListMap.clear();
            this._componentListMap = null;
      }
}




class ComponentLibraryLoaderJAVA extends ComponentLibraryLoader{

      public async startLoading(){
            /*
		  컴포넌트 파일 읽기 시작.
		  */

            this._componentFileInfoList = [];

            // this._packList = await componentApi.loadPackInfo();

            // if (this._packList.length <= 0) {
            //       this._completedLoad();
            // }

            try {
                  // pack 컴포넌트
                  // this._componentFileInfoList = await this.mixComponentFileList();

                  // if(window.wemb.extensionInstanceManager.usingExtension == true) {

                  //       // 확장 패널 컴포넌트
                  //       let extensionList = await this.mixExtensionFileList();
                  //       this._componentFileInfoList = this._componentFileInfoList.concat(extensionList);

                  // }
                  var me = this;
                  var packages =  await componentApi.loadPackages();
                  var extensionList = [];
                  packages.reduce(function(rv, x) {
                        if(rv[x["name"]]) {
                              rv[x["name"]] = rv[x["name"]];
                        } else {
                              rv[x["name"]] = [];
                              var row = {};
                              if(x.file_id) row["file_id"] = x.file_id;
                              if(x.label) row["label"] = x.label;
                              if(x.name) row["name"] = x.name;
                              if(x.instanceName) row["instanceName"] = x.instanceName;
                              if(x.file_name) row["file_name"] = x.file_name;
                              if(x.style_name) row["style_name"] = x.style_name;
                              if(x.dependencies) row["dependencies"] = x.dependencies;
                              if(x.file_name){
                                    if(x.type === 'extension') {
                                          extensionList.push(row);
                                    } else if(x.type === 'component') {
                                          me._componentFileInfoList.push(row)
                                    }
                              }
                        }
                        return rv;
                  }, {});
                  me._componentFileInfoList = me._componentFileInfoList.concat(extensionList);
            }catch(error){
                  console.warn("error _loadComponentFileList");
                  this._componentFileInfoList=[];
            }

            // 컴포넌트가 존재하지 않는 경우.
            if(this._componentFileInfoList.length==0){
                  this._completedLoad();
                  return;
            }


            this._executeStartLibraryLoading();

      }

      // pack에 컴포넌트 목록 합치기
      /*
      파일 정보를 합치는 도중에 에러가 나면(pack 정보가 없는 경우)
      에러 알림
       */
      private async mixComponentFileList(){
            return new Promise(async (resolve, reject)=>{

                  let mixedFileList: any[] = [];
                  let count = 0;

                  if (this._packList.length <= 0)
                        resolve([])

                  /*
                  팩 개수만큼 컴포넌트 정보 구하기
                   */
                  this._packList.forEach(async (packName) => {
                        let fileList = await componentApi.loadComponentFileListJson(packName);
                        //console.log(packName+"  컴포넌트 목록 =  ", fileList);
                        mixedFileList = mixedFileList.concat(fileList);
                        count++;
                        if (count >= this._packList.length) {
                              resolve(mixedFileList);
                        }
                  })
            })
      }



      protected async mixExtensionFileList(){
            return new Promise(async (resolve, reject)=>{
                  let mixedFileList:any[] = [];
                  let count=0;

                  if(this._packList.length<=0)
                        resolve([])


                  this._packList.forEach(async (packName)=>{
                        let fileList = await componentApi.loadExtensionFileList(packName);
                        mixedFileList = mixedFileList.concat(fileList);
                        count++;
                        if(count>=this._packList.length){
                              resolve(mixedFileList);
                        }
                  })
            })
      }








      public async mixComponentPanelInfoList(){
            return new Promise(async (resolve, reject)=>{
                  var packages =  await componentApi.loadPackages();
                  var _componentPanelInfoList = {
                        "two_layer": {
                              "label": "2D Components",
                              "children": []
                        },
                        "three_layer": {
                              "label": "3D Components",
                              "children": []
                        }
                  }
                  packages.forEach(function(d) {
                        if(d.layer) {
                              // java version의 경우 layer를 그대로 쓰도록...
                              var category = _componentPanelInfoList[d.layer === "2D" ? "two_layer" : "three_layer"]
                                    .children.find(function(a) {
                                          return a.label === d.category;
                                    })
                              if(category) {
                                    category.children.push(d.props);
                              } else {
                                    _componentPanelInfoList[d.layer === "2D" ? "two_layer" : "three_layer"].children.push({
                                          label:d.category,
                                          children:[d.props]
                                    })
                              }
                        } else {
                              if(d.type === 'extension') {
                                    window.wemb.extensionInstanceManager._data.using = true;
                                    var extension_info = $.extend(true, d.props, {
                                          "label": d.label,
                                          "extension_name": d.name
                                    });

                                    if(d.category === 'panel') {
                                          if(d.props.mode === 'both') {
                                                window.wemb.extensionInstanceManager._data.editor.panels.push(extension_info);
                                                window.wemb.extensionInstanceManager._data.viewer.panels.push(extension_info);
                                          } else {
                                                window.wemb.extensionInstanceManager._data[d.props.mode].panels.push(extension_info);
                                          }
                                    } else if(d.category === 'plugin') {
                                          if(d.props.mode === 'both') {
                                                window.wemb.extensionInstanceManager._data.editor.plugins.push(extension_info);
                                                window.wemb.extensionInstanceManager._data.viewer.plugins.push(extension_info);
                                          } else {
                                                window.wemb.extensionInstanceManager._data[d.props.mode].plugins.push(extension_info);
                                          }
                                    }
                              }
                        }
                  })
                  resolve(_componentPanelInfoList);
            })
      }


}

class ComponentLibraryLoaderNode extends ComponentLibraryLoader{
      public async startLoading(){

            /*
            2018.10.01(ckkim)

            Node 버전에서는 packList가 필요 없지만
            extensionFileList를 읽기 위해서 필요함.
            추후 수정해야함.
             */
            // this._packList = await componentApi.loadPackInfo();
            // console.log("@@ node 읽어드린 팩 개수 ", this._packList);
            // if (this._packList.length <= 0) {
            //       this._completedLoad();
            // }



            this._componentFileInfoList = [];

            try {
                  // pack 컴포넌트
                  this._componentFileInfoList = await componentApi.loadComponentFileListApi();

                  if(window.wemb.extensionInstanceManager.usingExtension == true) {

                        // 확장 패널 컴포넌트
                        let extensionList = await componentApi.loadExtensionFileListApi();


                        this._componentFileInfoList = this._componentFileInfoList.concat(extensionList);

                  }

            }catch(error){
                  console.warn("@@ node error loadComponentFileListApi()");
                  this._componentFileInfoList=[];
            }

            // 컴포넌트가 존재하지 않는 경우.
            if(this._componentFileInfoList.length==0){
                  this._completedLoad();
                  return;
            }


            this._executeStartLibraryLoading();

      }

      public async mixComponentPanelInfoList(){
            return new Promise(async (resolve, reject)=>{
                  let mixedComponentInfoList = {
                        two_layer:[],
                        three_layer:[]
                  }

                  let count=0;



                  let infoList = await componentApi.loadComponentInfoListApi([]);
                  resolve(infoList);

            })
      }


}



/////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////
// 컴포넌트 라이브러리 읽기


/*
component_file_list.json에서 하나의 파일 정보 노드에 정보 일기

예)
{
  "name": "BasicGridComponent",
  "file_name": "client/packs/2d_pack/components/BasicGridComponent/BasicGridComponent.js",
  "style_name": "client/packs/2d_pack/components/BasicGridComponent/BasicGridComponent.css",
  "dependencies": {
	"libs": [{
	    "file_name": "static/libs/jquery-fixed-header-table/jquery.CongelarFilaColumna.js",
	    "mode": "both"
	}],
	"styles": [{
	    "file_name": "static/libs/jquery-fixed-header-table/ScrollTabla.css",
	    "mode": "both"
	}]
  }
}
 */

/*
컴포넌트에서 사용하는 파일, 라이브러리, 스타일을 로드하는 메니저
css,js 모두 조합해서

 */
class LoadFileManager {

      static fileMap = new Map();

      private _currentIndex = 0;
      private _mode: EXE_MODE = EXE_MODE.EDITOR;
      private _promise = null;
      private _list = null;
      private _resolve = null;
      private _reject = null;
      private _typeCheckReg;

      constructor(mode: EXE_MODE) {
            var VALIDATION_TYPE_FORMAT = "\.(css|js)$";
            this._typeCheckReg = new RegExp(VALIDATION_TYPE_FORMAT, "i");

            this._mode = mode;
      }

      public startFileLoading(list) {
            this._currentIndex = 0;
            this._list = list;
            return new Promise((resolve, reject) => {

                  if (list.length == 0) {
                        resolve(true);

                  } else {
                        this._resolve = resolve;
                        this._reject = reject;
                        this._loadFileAt(0);
                  }
            })
      }

      private _loadFileAt(index: number) {
            let fileInfo = this._list[index];

            // 로딩할 파일 정보가 있는지  또는 현재 모드에서 로딩할 파일인지 확인하기
            if (this._checkLoadingInfo(fileInfo) == false) {
                  this._nextLoadFile();
                  return;
            }


            // 로딩할수 있는 파일인지 체크하기(파일 포맷 유무, 기존에 이미 읽어들인 파일인지 체크하기)
            if (this._checkLoadingFile(fileInfo.file_name) == false) {
                  this._nextLoadFile();
                  return;

            }


            // 지원하지 않는 타입인 경우 체크
            let fileType = this._getFileType(fileInfo.file_name);

            switch (fileType) {
                  case "js":
                        this._loadJSFile(fileInfo.file_name);
                        break;
                  case "css":
                        this._loadStyleFile(fileInfo.file_name);
                        break;
                  default:
                        this._nextLoadFile();
            }

      }


      /*
	JS File 읽기
	 */
      private _loadJSFile(fileName: string) {

            var script = document.createElement('script');
            script.onload = () => {
                  let tempFileName = fileName.toLowerCase();
                  if (LoadFileManager.fileMap.has(tempFileName) == false) {
                        LoadFileManager.fileMap.set(tempFileName, true);
                  }
                  script = null;
                  this._nextLoadFile();
            };
            script.onerror = () => {
                  console.log(`ERROR ${fileName} 파일을 로드하는 도중에 에러 발생`);
                  script = null;
                  this._nextLoadFile();
            };

            /*
            2018.10.20 (ckkim)
            자동 업데이트 처리를 위해 추가
            추후 수정해야할 수 있음.
             */
            script.src = fileName;
            //console.log("\t\t\t\t js fileName ", fileName);
            document.head.appendChild(script); //or something of the likes

      }


      /*
	css File 읽기
	 */
      private _loadStyleFile(fileName) {

            var link = document.createElement('link');
            link.rel = "stylesheet";
            link.type = "text/css";


            link.onload = () => {
                  let tempFileName = fileName.toLowerCase();
                  if (LoadFileManager.fileMap.has(tempFileName) == false) {
                        LoadFileManager.fileMap.set(tempFileName, true);
                  }
                  link = null;
                  this._nextLoadFile();
            };
            link.onerror = () => {
                  this._nextLoadFile();
            };

            link.href = fileName;
            //console.log("\t\t\t\t css fileName ", fileName);
            document.head.appendChild(link); //or something of the likes

            link = null;
      }


      private _nextLoadFile() {
            this._currentIndex++;
            if (this._currentIndex >= this._list.length) {
                  this._completed();
            } else {
                  this._loadFileAt(this._currentIndex);
            }
      }

      /*
	스크립트 또는 파일을 로드 할 수 있는 경우 인지 판단하기.
	1. file_name이 있는지 유무
	2. mode와 file_info.mode가 동일 한 경우인지
	    - file_info.mode가 없거나 both인 경우 로드하기

	 */
      private _checkLoadingInfo(fileInfo) {
            if (fileInfo.hasOwnProperty("file_name") == false) {
                  return false;
            }

            if (fileInfo.hasOwnProperty("mode") == false) {
                  return true;
            }

            let tempMode = fileInfo.mode.toLowerCase();
            if (tempMode == "both") {
                  return true;
            }

            return tempMode == this._mode;
      }

      // 로딩 가능한 파일인지 확인
      _checkLoadingFile(fileName) {

            // 로딩 가능 파일 포맷인지 체크하기
            fileName = fileName.toLowerCase();
            if (this._typeCheckReg.test(fileName) == false)
                  return false;

            // 기존에 로딩한 파일인지 확인하기
            return !LoadFileManager.fileMap.has(fileName);

      }


      private _getFileType(fileName) {
            let tempArray = fileName.split('.');
            return tempArray[tempArray.length - 1];
      }

      private _completed() {
            this._resolve(true);
      }

}
