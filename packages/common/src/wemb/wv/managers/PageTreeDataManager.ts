import Vue from "vue";
import PageManager from "./PageManager";
import pageTreeApi from "../../../common/api/pageTreeApi";
import PageTreeItemVO from "../../../editor/model/vo/PageTreeItemVO";
import EditorStatic from "../../../editor/controller/editor/EditorStatic";
/**
 * 페이지 트리 데이터 관리
 * 페이지 트리 목록 업데이트시마다 페이지 트리목록과 페이지목록을 새로 load하여 변경이 있을시 이에 대한 업데이트도 같이 한다.
 * 편집중인 페이지가 삭제된경우(다른 PC에서 ) 이를 export할 수 있는 로직이 있는데 이는 pageManager attachJsonPageInfoListToPageInfoList 참조
 * TODO: 페이지 이동시 위치가 잘 저장되지 않아 server쪽 로직 확인 필요
*/
export default class PageTreeDataManager {
	private _rootId:string;
	private _focusTargetId:string;
	private _$bus:Vue;
	private _treeData:Array<PageTreeItemVO>;
	get $bus() {
		return this._$bus;
	}

	get $on(){
		return this._$bus.$on;
	}

	get $emit(){
		return this._$bus.$emit;
	}

	set focusTargetId(target) {
		target = target || this._rootId;
		this._focusTargetId = target;
	}

	get focusTargetId() {
		return this._focusTargetId;
	}

	set treeData(data) {
		this.updatePageTree(data);
	}

	get treeData() {
		return this._treeData;
	}

	constructor() {
		this._$bus = new Vue();
		this._treeData = [];
	}


	private updatePageTree(data){
		//현재 편집중인 페이지의 이름이 변경된 경우 현재 이름을 유지하도록 한다.
		let currentPageInfo = window.wemb.pageManager.currentPageInfo;
		if( currentPageInfo ){
			let treeItem = data.find(x=>x.id == currentPageInfo.id);
			if( treeItem && currentPageInfo.name != treeItem.text ){
				treeItem.text = currentPageInfo.name;
			}
		}

		//항목 변경이 있으면 pageTree update를 한다
		if( this.compareTreeData( this.treeData.concat(), data.concat() ) ){
			this._treeData = data;
			this.$bus.$emit("updatePageTree", data);
		}
	}

	private compareTreeData(oldData, newData){
            let result = true;
            try{
                  if(oldData.length == newData.length){
                        let oldD = oldData.sort(function(a, b) {
                              let r = (a.id < b.id) ? -1 : (a.id > b.id) ? 1 : 0;
                              return r;
                        })

                        let newD = newData.sort(function(a, b) {
                              let r = (a.id < b.id) ? -1 : (a.id > b.id) ? 1 : 0;
                              return r;
                        })

                        /*
                        2019.11.06 Youme
                       기존에는 id만 조회해서 일치하는지 비교 했으나
                       순서가 변경된 경우에도 update 해주기 위해서 정렬한 후 문자열로 비교한다.
                        */
                        if(JSON.stringify(oldD) === JSON.stringify(newD)){
                              result = false;
                        }
                  }
            } catch(error){

            }

            return result;
	}

	// 페이지 정보 가져오기
	public startLoading() {
            return new Promise((resolve, reject) => {
                  pageTreeApi.loadPageTreeData().then((result) => {
                        if (!result) {
                              reject();
                        } else {
                              let treeData = result.pageTreeList;
                              let pageList = result.pageList;

                              treeData.filter(treeItem => treeItem.type === 'page').map(treeItem => {
                                    pageList.filter(page => page.id === treeItem.id).map(page => {
                                          return treeItem['description'] = page.props.setter.description;
                                    })
                              });

                              let rootItem = <PageTreeItemVO>treeData.find(x=>x.type=="root");
                              this.setRootItemData(rootItem);
                              this.treeData = treeData;
                              resolve(result);
                        }

                  }).catch((error) => {
                        console.log("getPageTreeData error", error);
                        //sendErrorMessage.sendCriticalErrorCommand({message: error});
                        reject(error);
                  });
            });
	}

	private setRootItemData(item:PageTreeItemVO){
		this._rootId = item.id;
		this._focusTargetId = this._rootId;
	}

	public addPage(name, pageInfo){
		let item = {
			name: name,
			type: "page",
			pageInfo: pageInfo
		}

		return this.createTreeItem(item);
	}

	public addPageGroup(name){
		let item = {
			name: name,
			type: "group"
		}

		return this.createTreeItem(item);
	}

      private createTreeItem(info){
            /**
             * 2019.06.20
             * parent, prev_id를 클라에서 계산해서 다 던져줌
             */
            let focusTargetItem = this.treeData.find(x => x.id === this._focusTargetId);
            if(focusTargetItem.type == 'group' || focusTargetItem.type === 'root'){
				  info.parent = focusTargetItem.id;
				  let childItems = this.treeData.filter(x => x.parent === focusTargetItem.id);
				  info.prev_id = childItems.length === 0 ? this._focusTargetId :childItems[childItems.length - 1].id;
            }else{
                  info.parent = focusTargetItem.parent;
                  let siblingItems = this.treeData.filter(x => x.parent === focusTargetItem.parent);
                  info.prev_id = siblingItems[siblingItems.length - 1].id;
            }

            return new Promise((resolve, reject) => {
                  pageTreeApi.createTreeItem( info ).then((treeData)=>{
                        //this.updatePageTree(data);
                        this.treeData = treeData;
                        resolve(treeData);
                  }).catch((error)=>{
                        reject(error);
                  });
            });
      }

      /* 2019.11.06 Youme 주석 처리
      private createTreeItem(info) {
            info.prevId =  this._focusTargetId;
            if( this._focusTargetId == this._rootId ){
                  info.parent = this._rootId;
            }else{
                  let item = this.treeData.find(x=>x.id == this._focusTargetId);
                  if(item){
                        if( item.type == "page" ){
                              info.parent = item.parent;
                        }else{
                              info.parent = item.id;
                        }
                  }else{
                        info.parent = this._rootId;
                  }
            }


            return new Promise((resolve, reject) => {
                  pageTreeApi.createTreeItem( info ).then((treeData)=>{
                        //this.updatePageTree(data);
                        this.treeData = treeData;
                        resolve(treeData);
                  }).catch((error)=>{
                        reject(error);
                  });
            });
      }*/

      private validCheckTreeItemName(name):boolean{
            name = name.trim();
            if(name.length<=0)
                  return false;

            return true;

      }

      private validCheckTreeItemId(pageId):boolean{
            //영문+숫자만 허용
            let nameReg = /^[_$A-Za-z0-9+;\-]*$/;
            return nameReg.test(pageId);
      }

      public renameTreeItem(id, newName, oldName, itemType):Promise<boolean> {
            return new Promise((resolve, reject) => {


                  if( !this.validCheckTreeItemName(newName) ){
                        var msg = Vue.$i18n.messages.wv.pageTree.itemNameErrorMsg;
                        Vue.$message({
                              type: 'error',
                              message: msg
                        });

                        reject('');
                        return;
                  }

                  // 2018.12.05(ckkim), 중복 이름 체크하지 않기, name 대신 id로 변경

                  /*this.doubleCheckTreeId(id).then((result)=>{
                        if(result == false){*/
					pageTreeApi.renameTreeItem({id:id, name: newName, oldName:oldName, type:itemType}).then((treeData)=>{
						let targetItem = this.treeData.find(x=>x.id == id);
						let oldName = targetItem.text;
						if( window.wemb.pageManager.currentPageInfo.id == id ){
							window.wemb.editorFacade.sendNotification(EditorStatic.CMD_CHANGE_PAGE_NAME, {
								oldName: oldName,
								newName: newName
							});
						}

						targetItem.text = newName;
						////text를 업데이트해주어서 다른 tree정보가 업데이트되지 않았다면 redraw 이벤트 발생하지 않게
						this.treeData = treeData;
						resolve();
					}).catch((error)=>{
						reject(error);
					})
				/*}else{
					let msg = Vue.$i18n.messages.wv.pageTree.existsName
					alert(msg);
					reject('');
				}
			}).catch((error)=>{
				//reject(error);
			})*/
		})
    }

	/*
	2018.12.05 (ckkim), 삭제 예정
	public doubleCheckTreeName(name){
		return new Promise((resolve, reject) => {
			if(this.hasTreeName(name)){
				resolve(true);
			}else{
				pageTreeApi.doubleCheck(name).then((result)=>{
					resolve(result);
				}).catch((error)=>{
					reject(error);
				})
			}
		})
	}*/

      public doubleCheckTreeId(id){
            return new Promise((resolve, reject) => {
                  if(this.hasTreeId(id)){
                        resolve(true);
                  }else{
                        pageTreeApi.doubleCheckId(id).then((result)=>{
                              resolve(result);
                        }).catch((error)=>{
                              reject(error);
                        })
                  }
            })
      }

	public getPageIds(ary):Array<string>{
		return this.treeData.map((x)=>{
			if( x.type == "page" ){
				return x.id;
			}
		});
	}

	getfilterPageTypeIds(ids) {
		let ary = [];
		for( let index in ids ){
			let id = ids[index];
			let findItem = this.treeData.find(x=> x.id == id && x.type == "page");

			if(findItem){
				ary.push(id);
			}
		}

		return ary;
	}



	public removeTreeItemsById(ids:Array<string>){
		let removeItems = [];
		for( let index in ids ){
			let id = ids[index];
			let item = this.treeData.find(x=>x.id == id);
			removeItems.push( {type: item.type, id: id} );
		}

		return this.removeTreeItems(removeItems);
	}

	public removeTreeItems(items:Array<PageTreeItemVO>){
		return new Promise((resolve, reject) => {
			pageTreeApi.removeTreeItem(items).then((treeData)=>{
				/*let currentId = window.wemb.pageManager.currentPageInfo.id;
				let findItem = items.find(x=>x.id == currentId);
				if(findItem){
					this.updatePageTree(treeData);
				}else{
					this.updatePageTree(treeData);
				}*/
				this.treeData = treeData;
				resolve(treeData);
			}).catch((error)=>{
				reject(error);
			})
		})
	}


	public moveTreeItems(items:Array<any>){
		pageTreeApi.moveTreeItem(items).then((treeData)=>{
			this.treeData = treeData;
		}).catch((error)=>{
			console.log("moveTreeItems error", error);
		});
	}


      // 2018.12.05 (ckkim), 삭제 예정
	/*public hasTreeName(name:string){
		let item = <PageTreeItemVO>this.treeData.find((x:any) => x.text == name);
		if(item){return true;}
		return false;
	}*/

      public hasTreeId(id:string){
            let item = <PageTreeItemVO>this.treeData.find((x:any) => x.id == id);
            if(item){return true;}
            return false;
      }
}


export enum PAGE_TREE_ITEM_TYPE {
	group="group",
	PAGE="page",
	MASTER="master"
}
