import Vue from "vue";
import {OpenPageData} from "../page/Page";
import pageApi from "../../../common/api/pageApi";
import {PageMetaProperties} from "../components/PageMetaProperties";
import ViewerStatic from "../../../viewer/controller/viewer/ViewerStatic";
import EditorStatic from "../../../editor/controller/editor/EditorStatic";


export default class PageManager {
      public static PAGE_PRE_ID:string="page_";
	private _pageInfoList:Array<PageInfo>;
      private _currentPageInfo:PageMetaProperties = null;
      // 페이지 정보
      get currentPageInfo():PageMetaProperties {
            return this._currentPageInfo;
      }

      set currentPageInfo(pageInfo) {
            this._currentPageInfo = pageInfo;
      }

	constructor(){
		this._pageInfoList = [];
		this._currentPageInfo = null;
	}

	public reset(){
		this._pageInfoList = [];
	}

	public attachJsonPageInfoListToPageInfoList(pageInfoList:Array<any>, isDeleteCurrentPageAct:boolean=false){
            //tree에서 최신 page정보 업데이트시, 현재 페이즈를 타인이 삭제하여 열려있는 페이지 정보가 누락될 수 있음
            //isCurrentPageDeleteAct << 사용자가 직접 삭제요청하여 update하는 경우는 제외

            let currentInfo;///현재페이지가 삭제된채 update될 수 있으므로 현재 페이지 정보를 캐시한다.
            if(!isDeleteCurrentPageAct){
                  if( this._pageInfoList && this._pageInfoList.length ){
                        let findItem = this._pageInfoList.find(x=>x.id == this.currentPageInfo.id);
                        if(findItem){
                              currentInfo = $.extend(true, {}, findItem);
                        }
                  }
            }


            /*
            2019.11.06 Youme
           기존에는 page만 넘어왔지만 group까지 넘어오게 변경하면서 분기 추가
           메서드도 map에서 filter로 변경
           */
            this._pageInfoList = pageInfoList.filter((info: any) => {
                  if (info.type === 'page' || info.type === 'master') {
                        let pageInfo = new PageInfo();
                        pageInfo.id = info.id;
                        pageInfo.name = info.name;
                        pageInfo.width = info.props.setter.width;
                        pageInfo.height = info.props.setter.height;
                        pageInfo.type = info.type;
                        return pageInfo;
                  }
            })


            ///아래 로직은 동적으로 페이지가 삭제 됐을때 처리를 위한것으로 에디터에서만 실행
            if(!wemb.configManager.isEditorMode){
                  return;
            }

            if(currentInfo){
                  let findInfo =  this._pageInfoList.find(x=>x.id == currentInfo.id);

                  if(findInfo){
                        findInfo.name = currentInfo.name;
                        findInfo.width = currentInfo.width;
                        findInfo.height = currentInfo.height;
                        findInfo.type = currentInfo.type;
                  }else{
                        window.wemb.editorFacade.sendNotification(EditorStatic.CMD_DELETED_CURRENT_PAGE);
                  }
            }else{
                  this.updatePageList( this._pageInfoList );
            }
	}

      /*
      페이지 삭제에 따른 다음 작업 진행
      1. 페이지가 0이 되는 경우

      2. 열려있는 페이지가 삭제되는 경우
      */
      private updatePageList(pageList) {
            var locale_msg = Vue.$i18n.messages.wv;
            let editorFacade = window.wemb.editorFacade;
            //window.wemb.pageTreeDataManager.getPageIds();
            // 페이지 삭제 후 페이지가 0인 경우
            if(pageList.length == 0){
                  this.currentPageInfo=null;
                  editorFacade.sendNotification(EditorStatic.CMD_CLOSE_PAGE);
                  setTimeout(()=>{
                        alert(locale_msg.page.noPage + locale_msg.page.newCreate);
                  },100);
                  return;
            }

            // 삭제 페이지가 포함되어 있는 경우!!!! 0번째 페이지 열기
            // 삭제 페이지가 현재 열린 페이지인 경우 마지막 페이지를 자동으로 열기
            if(this.currentPageInfo==null){
                  console.log("트리에서 페이지 목록 삭제 후 처리 부분, 열려있는 페이지가 삭제되는 경우 자동으로 0번째 페이지를 열게되는데, 이때 열려있는 페이지가 존재 하지 않아 처리 할 수 없음.");
                  return;
            }


            let findItem = pageList.find((x) => x.id == this.currentPageInfo.id);
            if (!findItem) {
                  let pageId = window.wemb.configManager.getStartPageId();
                  if (pageId != null) {
                        console.log("페이지 삭제 후 열게되는 페이지 ID= ", pageId);
                        editorFacade.sendNotification(EditorStatic.CMD_OPEN_PAGE, pageId);
                  } else {
                        editorFacade.sendNotification(EditorStatic.CMD_CLOSE_PAGE);
                        alert(Vue.$i18n.messages.wv.page.noPage);
                  }
            }
      }

      public getPageNameById(id:string) {
		let pages = this._pageInfoList.filter(x => x.type == "page");
		let page = pages.find((x) => {
			return x.id == id;
		});

		if (page && page.name) {
			return page.name;
		} else {
			return null;
		}
	}


	public getPageIdByName(name:string) {
		let pages = this._pageInfoList.filter(x => x.type == "page");
		let page = pages.find((x) => {
			return x.name == name;
		});

		if (page && page.id) {
			return page.id;
		} else {
			return null;
		}
	}

	public getPageIdByIndex(index:number){
		let pages = this._pageInfoList.filter(x => x.type == "page");
		let page = pages[index];
		if (page && page.id) {
			return page.id;
		} else {
			return null;
		}
	}

	/*
	비밀 페이지 목록 구하기
	 */

	public getSecretPageList(){
		let list = this._pageInfoList.filter((item)=>{
			if(item.secret=="Y"){
				return item;
			}
		});

		return list;
	}

	public getPageInfoList(){
		return this._pageInfoList;
	}

      addPageInfo(pageMetaProperties) {
            this._pageInfoList.push(pageMetaProperties);
      }


	public getPageLength(){
		return this._pageInfoList.length;
	}


      // index에 해당하는 페이지 정보 구하기
      // editor/viewer공통
      getPageInfoAt(index) {
            if (this._pageInfoList == null)
                  return null;
            // index에 해당하는 페이지 정보 구하기
            if (index < this._pageInfoList.length) {
                  return this._pageInfoList[index];
            }

            return null;
      }


      // 페이지 ID를 이용해 페이지 정보 읽기
      //  viewer에서만 사용.
      public getPageInfoBy(pageID) {
            if (this._pageInfoList.length > 0) {
                  for (let pageInfo of this._pageInfoList) {
                        if (pageInfo.id == pageID) {
                              return pageInfo;
                        }
                  }
            }

            return null;
      }

      public getPageInfoByName(pageName) {
            if (this._pageInfoList.length > 0) {
                  for (let pageInfo of this._pageInfoList) {
                        if (pageInfo.name == pageName) {
                              return pageInfo;
                        }
                  }
            }
            return null;
      }


      public hasPageInfoBy(pageID) {
            if (this.getPageInfoBy(pageID)) {
                  return true;
            }
            return false;
      }

      public hasPageInfoByName(pageName) {
            if (this.getPageInfoByName(pageName)) {
                  return true;
            }
            return false;
      }

      // 페이지 내에 페이지를 가지고 있는 컴포넌트인지 파악하기
      public isHavePage(pageName:string){
	      return false;
      }



      /*
      페이지 정보만 읽기
      page component등에서 사용.
       */
      public async loadPageDataById(pageId) {
            let pageData:OpenPageData = await pageApi.openPageById(pageId);
            return pageData;
      }


      /*
      2018.11.12(ckkim)
            params 추가
       */
      public openPageById(pageId:string, params:any = null):void{
            // 기존 열린 페이지가 있는 경우 저장된 파라메터 값 제거
            if(this.currentPageInfo){
                  window.wemb.globalStore.delete(this.currentPageInfo.id);
            }
            // 신규로 등록
            if(params==null){
                  window.wemb.globalStore.delete(pageId);
            }else {
                  window.wemb.globalStore.set(pageId, params);
            }

            // 페이지 유무 처리는 command에서 처리
            window.wemb.viewerFacade.sendNotification(ViewerStatic.CMD_OPEN_PAGE, pageId);
      }

      public openPageByName(pageName: string, params: any = null): void {
            let pageId: string = this.getPageIdByName(pageName);
            this.openPageById(pageId, params);

      }



      public updatePageName(pageID:string, pageName:string):Promise<any> {
            if(this.getPageIdByName(pageName)==null){
                  return pageApi.updatePageName(pageID, pageName);
            }else {
                  return Promise.reject("동일한 페이지 이름이 존재합니다.");
            }
      }
      ///////////////////////////////////////////////////







      ///////////////////////////////////////////////////
      /*
	에디터에서만 사용
	 */

      /*
	  페이지를 삭제한 후 페이지 목록에서 페이지 정보 삭제하기
	   */
      public deletePageInfoById(pageID) {
            let resultPageInfo = null;
            if (this._pageInfoList.length > 0) {
                  for (let i = 0; i < this._pageInfoList.length; i++) {
                        let pageInfo = this._pageInfoList[i];
                        if (pageInfo.id == pageID) {
                              resultPageInfo = pageInfo;
                              this._pageInfoList.splice(i, 1);
                              break;
                        }
                  }
            }
            return resultPageInfo;
      }
      ///////////////////////////////////////////////////



      /*
      openbyname, openById() 메서드 호출시 파라메터도 넘긴 값 구하기
       */
      public getParams(){
            return window.wemb.globalStore.get(this.currentPageInfo.id);
      }

      /*
      2018.12.05(ckkim)
      유효한 페이지 이름 생성하기
       */

      public generatePageId(pageId){

            let maxCounter =1;
            let sw=false;
            let newPageId:string="";

            // 페이지의 접두어 알아내기(일종의 그룹 이름)
            let prePageId:string=pageId;
            let pos = prePageId.lastIndexOf("_");
            if(pos!=-1){
                  prePageId = prePageId.slice(0, pos);
            }


            // max count 구하기
            this._pageInfoList.forEach((pageInfo)=>{
                  let tempPageId = pageInfo.id;
                  //  페이지 이름을 가지고 있는 경우
                  if(tempPageId.indexOf(prePageId)!=-1) {

                        // 최대 카운터 찾기
                        let pos = tempPageId.lastIndexOf("_");
                        if (pos != -1) {
                              sw = true;
                              let tempValue = tempPageId.slice(pos, tempPageId.length);
                              let tempCounter = parseInt(tempValue);
                              if (isNaN(tempCounter) == false) {
                                    maxCounter = Math.max(maxCounter, tempCounter + 1);
                              }
                        }
                  }
            })


            newPageId = prePageId+"_"+maxCounter;

            return newPageId;

      }


}

class PageInfo {
	public id:string;
	public name:string;
	public title:string;
	public width:number;
	public height:number;
	public type:PAGE_TYPE;
	public secret?:PAGE_SECRET;
	public description?:string
}


enum PAGE_SECRET {
	YES="Y",
	NO="N"
}

enum PAGE_TYPE {
	PAGE="page",
	POPUP="popup"
}


class StageInfo {
	public props:{
		background:{
			color:"#ff0000";
			image:"";
			fileMode:""
		}
	}
	public globalFunction:string=""
}
