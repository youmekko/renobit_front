import {PageMetaProperties} from "../components/PageMetaProperties";

export default class PageHistoryManager {

      public static PAGE_HISTORY_COUNT_MAX = 10;
      public static _instance:PageHistoryManager;
      public static GOTO_HISTORY_PAGE:boolean=false;

      public static getInstance():PageHistoryManager{
            if(PageHistoryManager._instance==null){
                  PageHistoryManager._instance = new PageHistoryManager();
            }

            return PageHistoryManager._instance;
      }

      private _pageInfoList:Array<PageMetaProperties>;
      public get pageInfoList(){
            return this._pageInfoList;
      }


      private _updateCallbackList:Map<string,Function> = new Map();
      private _pageHistoryCountMax:number =10;




      private _currentIndex = 0;
      public get currentIndex(){
            return this._currentIndex;
      }


      public get currentPageInfo(){
            return this._pageInfoList[this._currentIndex];
      }


      public get historyLength(){
            return this._pageInfoList.length;
      }


      public get isFirstHistory():boolean{
            if(this._currentIndex==0){
                  return true;
            }else {
                  return false;
            }
      }

      public get isLastHistory():boolean{
            let index = Math.max(0, this.historyLength-1);
            if(this._currentIndex==index){
                  return true;
            }else {
                  return false;
            }
      }

      public get isEmptyHistory(){
            return this._pageInfoList.length==0;
      }



      constructor(){
            this._pageInfoList = [];

      }
      public reset(){
            this._pageInfoList = [];
            this._currentIndex=0;
      }






      // 현재 열린 페이지 추가
      public pushCurrentPage(){
            if(PageHistoryManager.GOTO_HISTORY_PAGE==true) {
                  PageHistoryManager.GOTO_HISTORY_PAGE=false;
                  return;
            }


            let viewer = wemb.configManager.getConfigProperties("viewer");
            if(viewer && viewer.hasOwnProperty("page_history_count_max")==true){
                  this._pageHistoryCountMax = parseInt(viewer.page_history_count_max);
            }else {
                  this._pageHistoryCountMax = PageHistoryManager.PAGE_HISTORY_COUNT_MAX;
            }


            if(this._pageInfoList.length>=this._pageHistoryCountMax){
                  this._pageInfoList.shift();
            }

            // 마지막에 추가
            this._pageInfoList.push(wemb.pageManager.currentPageInfo);

            for(let i=0;i<this._pageInfoList.length;i++){
                  let item = this._pageInfoList[i];
                  item["idx"] = i;
            }


            this.dispathUpdate();
      }


      public gotoNextPage(){

            let index = this._currentIndex+1;


            if(index>=this._pageInfoList.length){
                  index=this._pageInfoList.length-1;
            }

            this.gotoPageAt(index);

      }


      public gotoPrevPage(){
            let index = this._currentIndex-1;
            if(index<0){
                  index=0;
            }

            this.gotoPageAt(index);
      }

      public gotoPageAt(index:number){
            this._currentIndex = index;
            let pageInfo = this._pageInfoList[this._currentIndex];
            if(pageInfo) {
                  PageHistoryManager.GOTO_HISTORY_PAGE = true;
                  wemb.pageManager.openPageByName(pageInfo.name);


                  this.dispathUpdate();
            }
      }

      /*
      페이지 배열 정보, currentIndex가 변경되는 경우 호출
       */
      public addUpdateCallback(name:string, func:Function){
            if(func!=null)
                  this._updateCallbackList.set(name,func);
      }

      public removeCallback(name:string){
            this._updateCallbackList.delete(name);
      }

      private dispathUpdate(){
            if(this._updateCallbackList.size>0) {
                  this._updateCallbackList.forEach((value) => {
                        try {
                              value["call"](this);
                        }
                        catch (error) {
                              console.log("PageHistoryManager dispatchUpdate error ", error);
                        }
                  })
            }
      }

}


class PageInfo {
      public id:string;
      public name:string;
      public title:string;
}
