/**
 * singleton 객체 
 * Timeout과 setInterval이 비동기 처리되므로 페이지 이동시마다 사용중인 타이머 객체를 해제하는 관리를 해주어야하는데,
 * 프로젝트팀에서 이를 디테일하게 관리하는데 시간이 소요되어 제품 기능으로 제공한다. 
 * DataService처럼 자동으로 페이지 이동시 페이지에서 동작중인 timer, interval이 모두 해제된다.
 * 기능은 native기능과 거의 동일하나 execute인자만이 추가되었다
 *   - execute : 
 *          - interval : 실행시점에서 실행 후 interval 실행
 *          - timer : 실행 시점에서 실행 후 설정 주기 후에 재 실행(2번 실행) 
 * 
 * InstanceManagerOnPageViewer객체에서 자동으로 생성된다.
 * pageId별로 객체가 관리되는데 자동으로 pageId를 인자로 넘겨주고 페이지 이동시 clearAll 메소드를 실행하는 기능을 한다.
 * 
*/
export class TimersScheduler {
    private static _instance = null;

    private timeoutListByPageId: any = {};
    private intervalListByPageId: any = {};

    constructor() {
    }

    public static getInstance():TimersScheduler {
        if (TimersScheduler._instance == null) {
            TimersScheduler._instance = new TimersScheduler();
        }

        return TimersScheduler._instance;
    }

    private getTimeoutListByPageId( id:string ):Array<number>{
        if( !this.timeoutListByPageId[id] ){
            this.timeoutListByPageId[id] = [];
        }

        return  this.timeoutListByPageId[id];
    }

    private getIntervalListByPageId( id:string ):Array<number>{
        if( !this.intervalListByPageId[id] ){
            this.intervalListByPageId[id] = [];
        }

        return  this.intervalListByPageId[id];
    }


    /** setTimeout을 pageId를 key로 등록하여 실행한다. execute를 true로 설정시 즉시 실행하고 설정 time뒤에 재실행한다(2번실행) */
    public setTimeout(pageId:string, func:Function, time:number, execute:boolean = false):number {
        let timeoutId;
        let self = this;
        let _func = function() {
            func();
            self.clearTimeout(pageId, timeoutId);
        }

        if (execute === true) { func() }

        timeoutId = setTimeout(_func, time);
        this.getTimeoutListByPageId( pageId ).push(timeoutId);
        return timeoutId;
    }


    /** setInterval을 pageId를 key로 등록하여 실행한다. execute를 true로 설정시 즉시 실행하고 설정 interval을 실행한다 */
    public setInterval(pageId:string, func:Function, time:number, execute:boolean = false):number {
        if (execute === true) { func() }
        let intervalId = setInterval(func, time);
        this.getIntervalListByPageId( pageId ).push(intervalId);
        return intervalId;
    }


    public clearTimeout(pageId:string, id:number) {
        clearTimeout(id);
        let timeoutIdList = this.getTimeoutListByPageId( pageId );
        let index = timeoutIdList.indexOf(id);
        if (index != -1) {
           timeoutIdList.splice(index, 1);
        }
    }

    public clearInterval(pageId:string, id:number) {
        clearInterval(id);
        let intervalIdList = this.getIntervalListByPageId( pageId );
        let index = intervalIdList.indexOf(id);
        if (index != -1) {
           intervalIdList.splice(index, 1);
        }
    }

    //페이지에 실행한 모든 timeout clear
    clearAllPageTimeout(pageId:string) {
        let timeoutList = this.getTimeoutListByPageId( pageId );        
        for(let i=0, len=timeoutList.length; i<len; i++){
            this.clearTimeout(pageId, timeoutList[0]);
        }
        delete this.timeoutListByPageId[pageId];
        //timoutList.splice(0, timoutList.length);
    }

    //페이지에 실행한 모든 interval clear
    clearAllPageInterval(pageId:string) {
        let intervalList= this.getIntervalListByPageId( pageId );
        for(let i=0, len=intervalList.length; i<len; i++){     
            this.clearInterval(pageId, intervalList[0]);
        }

        delete this.intervalListByPageId[pageId];
    }

    clearAllByPageId(pageId:string) {
        this.clearAllPageTimeout(pageId);
        this.clearAllPageInterval(pageId);
    }
}
