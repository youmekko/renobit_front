import Vue from "vue";
import menusetApi from "../../../common/api/menusetApi";
import FileManager from "./FileManager";
import {locale} from "moment";

export default class MenusetDataManager {
      private static _instance = null;
      private _menusetList: Array<any> = [];
      private _$bus: Vue;

      constructor() {
            this._$bus = new Vue();
      }

      public static getInstance() {
            if (MenusetDataManager._instance == null) {
                  MenusetDataManager._instance = new MenusetDataManager();
            }

            return MenusetDataManager._instance;
      }

      get $bus() {
            return this._$bus;
      }

      get $on() {
            return this._$bus.$on;
      }

      get $emit() {
            return this._$bus.$emit;
      }

      public startLoading(): Promise<boolean> {
            return new Promise((resolve, reject) => {
                  this.loadMenesetList().then(() => {
                        resolve(true);
                        return;
                  }, () => {
                        resolve(false);
                  })
            })
      }

      /**
       * 로드 시점시, menusetManager open시마다 load
       */
      loadMenesetList(): Promise<any> {
            return new Promise((resolve, reject) => {
                  this.menusetList.splice(0, this.menusetList.length);
                  menusetApi.loadMenesetList().then((data: any) => {
                        if (data.menu && data.menu.length) {
                              this.menusetList = data.menu;
                        }
                        resolve(this.menusetList);
                        return;
                  }, (error) => {
                        reject();
                  });
            })
      }

      get menusetList() {
            return this._menusetList;
      }

      set menusetList(list: Array<any>) {
            this.menusetList.splice(0, this.menusetList.length);
            this.menusetList.push(...list);

            this.$bus.$emit("update_menuset_list", this._menusetList.concat());
      }

      importMenusetListAsFile(file: any) {
            var locale_msg = Vue.$i18n.messages.wv;
            var reader = new FileReader();

            if (file.name.search(/.json$/) === -1) {
                  Vue.$message.error(locale_msg.common.errorFormat);
            } else {
                  reader.onloadend = (evt: any) => {
                        try {
                              let data = JSON.parse(evt.target.result).menu;
                              let valid = this.getValidMenusetList(data);
                              if (valid.length && valid.length !== 0) {
                                    this.mergeMenusetList(valid)
                                    Vue.$message.success(locale_msg.common.successImport);
                              } else {
                                    Vue.$message.error(locale_msg.common.errorValidate);
                              }
                        } catch (error) {
                              Vue.$message.error(locale_msg.common.failImport);
                        }
                  };

                  reader.readAsText(file);
            }
      }

      getValidMenusetList(list) {
            if (Array.isArray(list)) {
                  return list.filter((x) => {
                        if (x.id && x.name && x.structure) {
                              return x;
                        }
                  })
            } else {
                  return [];
            }
      }

      mergeMenusetList(ary) {
            let locale_msg = Vue.$i18n.messages.wv;

            let doubleCheckList = this._menusetList.filter((x) => {
                  if (!ary.find(y => y.id == x.id)) {//id가 겹치지 않는것만 수집
                        return x;
                  }
            })

            let mergeList;
            //같은 아이디의 셋이 있다면,
            if (this._menusetList.length > doubleCheckList.length) {
                  //덮어쓰기 OK?
                  if (confirm(locale_msg.common.overwrite)) {
                        mergeList = doubleCheckList.concat(ary);
                        this.updateMenusetList(mergeList);
                  }
                  //덮어쓰기   Cancel의 경우는 update하지 않음
            } else {
                  //같은 아이디 셋이 없을 때
                  let _list = ary.filter((x) => {
                        if (!this._menusetList.find(y => y.id == x.id)) {
                              return x;
                        }
                  })
                  mergeList = this._menusetList.concat(_list);
                  this.updateMenusetList(mergeList);
            }
      }

      exportMenusetListAsFile() {
            let jsonStr = JSON.stringify({"menu": this.menusetList});
            let fileManager = new FileManager();
            fileManager.saveTextFile("menusetList.json", jsonStr);
            fileManager.onDestroy();
      }

      getMenusetById(id: string) {
            if (this.menusetList && this.menusetList.find(x => x.id == id)) {
                  return this.menusetList.find(x => x.id == id);
            } else {
                  console.warn("메뉴셋이 조회되지 않습니다.");
                  return null;
            }
      }

      getMenusetByName(name: string) {
            if (this.menusetList && this.menusetList.find(x => x.name == name)) {
                  return this.menusetList.find(x => x.name == name);
            } else {
                  console.warn("메뉴셋이 조회되지 않습니다.");
                  return null;
            }
      }


      updateMenusetList(list): Promise<any> {
            return new Promise((resolve, reject) => {
                  menusetApi.updateMenusetList(list).then(() => {
                        this.menusetList = list;
                        resolve();
                        return;
                  }, (error) => {
                        reject();
                  })
            });
      }

}
