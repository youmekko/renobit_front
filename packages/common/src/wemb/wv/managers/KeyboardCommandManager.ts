import IFacade = puremvc.IFacade;
import EditorStatic from "../../../editor/controller/editor/EditorStatic";
import {WORK_LAYERS} from "../layer/Layer";
import CommonStatic from "../../../editor/controller/common/CommonStatic";
import EditorProxy from "src/editor/model/EditorProxy";

import Vue from "vue";

export default class KeyboardCommandManager {

	private _active:boolean =false;
	private _validation:boolean=false;
	public facade:IFacade = null;
	constructor(){
		this._active=false;
		this._validation=false;
	}

	start(facade:IFacade){

		this.facade = facade;
		if(this._checkGlobalProperty()==true){
			this._initEvent();
		}
	}

	_checkGlobalProperty(){
		this._validation =true;
		return true;

	}

	_initEvent(){

	}

	// 명령어에 의해서 키보드 입력 처리
	setActive(active){
		if(this._validation==false){
			throw new Error("초기화가 되지 않은  상태에서 KeyboardCommandManager를 실행했습니다.");
		}

		this._active=active;



		if(active){
			$(document).data("keyboard-command-manager", this);
			$(document).off("keydown", this._onKeyDown);
                  $(document).on("keydown", this._onKeyDown);
                  $(document).off("keyup", this._onKeyUp);
			$(document).on("keyup", this._onKeyUp);
		}else {
                  $(document).off("keydown", this._onKeyDown);
                  $(document).off("keyup", this._onKeyUp);
		}
      }
      
      _onKeyUp(event) {
            let that:KeyboardCommandManager = <KeyboardCommandManager>$(document).data("keyboard-command-manager");
            let inputKey = event.key.toLowerCase();
            if (inputKey == "v" && event.ctrlKey) {
			that.facade.sendNotification(EditorStatic.CMD_PASTE_COM_INSTANCE);
			return;
            }
            
            // 저장
            if (inputKey == "z" && event.ctrlKey) {
                  event.preventDefault();
                  event.stopPropagation();
                  var proxy = that.facade.retrieveProxy("EditorProxy") as EditorProxy;
                  proxy.undoComInstances()
                  return;
            }

            // 저장
            if (inputKey == "y" && event.ctrlKey) {
                  event.preventDefault();
                  event.stopPropagation();
                  var proxy = that.facade.retrieveProxy("EditorProxy") as EditorProxy;
                  proxy.redoComInstances()
                  return;
            }
      }


	_onKeyDown(event) {
		let that:KeyboardCommandManager = <KeyboardCommandManager>$(document).data("keyboard-command-manager");

		//console.log("position 입력된 키 = ", event.key, event.target, event.target.tagName, event.currentTarget);
            let inputKey = event.key.toLowerCase();

            	// 삭제 처리
		if (inputKey == "delete" || inputKey=="del") {
                  if (!document.activeElement.classList.contains('jstree-anchor')) {
                        that.facade.sendNotification(EditorStatic.CMD_REMOVE_SELECTED_COM_INSTANCE);
			      return;
                  }
            }
            
            /*
            2019.02.07(ckkim) IE에서 back
             */
            if (inputKey == "backspace"  && event.target.tagName != 'INPUT' && event.target.tagName != 'TEXTAREA') {
                  event.preventDefault();
                  event.stopPropagation();
                  return;
            }


		if (inputKey == "a" && event.ctrlKey) {
			that.facade.sendNotification(EditorStatic.CMD_ADD_ALL_SELECTED);
                  event.preventDefault();
                  event.stopPropagation();
			return;
		}


		if (inputKey == "c" && event.ctrlKey) {
			that.facade.sendNotification(EditorStatic.CMD_COPY_SELECTED_COM_INSTANCE);
			return;
		}


            if (inputKey == "x" && event.ctrlKey) {
                  that.facade.sendNotification(EditorStatic.CMD_CUT_SELECTED_COM_INSTANCE);
                  return;
            }


            // 저장
            if (inputKey == "s" && event.ctrlKey) {
                  event.preventDefault();
                  event.stopPropagation();
                  that.facade.sendNotification(EditorStatic.CMD_SAVE_PAGE);
                  return;
            }

            // 신규 페이지
            if (inputKey == "n" && event.altKey && event.shiftKey==false) {
                  event.preventDefault();
                  event.stopPropagation();
                  that.facade.sendNotification(EditorStatic.CMD_SHOW_NEW_PAGE_MODAL);
                  return;
            }

            // 신규 그룹
            if (inputKey == "n" && event.altKey && event.shiftKey) {
                  event.preventDefault();
                  event.stopPropagation();
                  that.facade.sendNotification(EditorStatic.CMD_SHOW_NEW_PAGE_MODAL, "group");
                  return;
            }

            // 스냅샷 팝업 오픈
            if (inputKey == "b" && event.ctrlKey && event.shiftKey) {
                  event.preventDefault();
                  event.stopPropagation();
                  that.facade.sendNotification(EditorStatic.CMD_PROJECT_RESTORE);
                  return;
            }
            ///////////////////////////////////////











            ///////////////////////////////////////

            // 레이어 전환
		if (inputKey == "1" && event.ctrlKey) {
			that.facade.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, WORK_LAYERS.MASTER_LAYER);
			event.preventDefault();
			return;
		}

		if (inputKey == "2" && event.ctrlKey) {
			that.facade.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, WORK_LAYERS.TWO_LAYER);
			event.preventDefault();
			return;
		}

		if (inputKey == "3" && event.ctrlKey) {
			that.facade.sendNotification(EditorStatic.CMD_CHANGE_ACTIVE_LAYER, WORK_LAYERS.THREE_LAYER);
			event.preventDefault();
                  return;
		}
            ///////////////////////////////////////







            ///////////////////////////////////////
            // 호출
            if (inputKey == "8" && event.ctrlKey) {

                  that.facade.sendNotification(EditorStatic.CMD_SHOW_DATATSET_MANAGER);
                  event.preventDefault();
                  return;
            }

            if (inputKey == "9" && event.ctrlKey) {
                  that.facade.sendNotification(EditorStatic.CMD_SHOW_WSCRIPT_EDITOR);
                  event.preventDefault();

            }

            if (inputKey == "0" && event.ctrlKey) {
                  that.facade.sendNotification(EditorStatic.CMD_SHOW_RESOURCE_MANAGER);
                  event.preventDefault();
                  return;
            }
            if (inputKey == "enter" && event.ctrlKey) {
                  if (window.wemb.pageManager.currentPageInfo.type != "page"){
                    alert(Vue.$i18n.messages.wv.extension.menu.openViewerMasterError);
                    return;
                  }
                  that.facade.sendNotification(EditorStatic.CMD_GOTO_VIEWER_BY_CURRENT_PAGE_NAME);
                  event.preventDefault();
                  return;
            }
            ///////////////////////////////////////




            ///////////////////////////////////////
            if(inputKey=="arrowright" || inputKey=="right" ){
		      let step=1;
                  if(event.shiftKey==true)
                        step=5;

                  that.facade.sendNotification(EditorStatic.CMD_MOVE_SELECTED_COMPONENT_INSTANCE, {propertyName:"x",step:step});
                  event.preventDefault();
                  event.stopPropagation();
                  return;

            }

            if(inputKey=="arrowleft" || inputKey=="left" ){
                  let step=-1;
                  if(event.shiftKey==true)
                        step=-5;

                  that.facade.sendNotification(EditorStatic.CMD_MOVE_SELECTED_COMPONENT_INSTANCE, {propertyName:"x",step:step});
                  event.preventDefault();
                  event.stopPropagation();
                  return;

            }

            if(inputKey=="arrowup" || inputKey=="up" ){
                  let step=-1;
                  if(event.shiftKey==true)
                        step=-5;

                  that.facade.sendNotification(EditorStatic.CMD_MOVE_SELECTED_COMPONENT_INSTANCE, {propertyName:"y",step:step});
                  event.preventDefault();
                  event.stopPropagation();
                  return;

            }
            if(inputKey=="arrowdown" || inputKey=="down" ){
                  let step=1;
                  if(event.shiftKey==true)
                        step=5;

                  that.facade.sendNotification(EditorStatic.CMD_MOVE_SELECTED_COMPONENT_INSTANCE, {propertyName:"y",step:step});
                  event.preventDefault();
                  event.stopPropagation();
                  return;

            }

            if((inputKey=="arrowup" || inputKey=="up") && event.ctrlKey==true){

                  let step = 1;
                  if(event.shiftKey==true)
                        step=5;

                  that.facade.sendNotification(EditorStatic.CMD_MOVE_SELECTED_COMPONENT_INSTANCE, {propertyName:"z",step:step});
                  event.preventDefault();
                  event.stopPropagation();
                  return;
            }

            if((inputKey=="arrowdown"  || inputKey=="down")  && event.ctrlKey==true){
                  let step = -1;
                  if(event.shiftKey==true)
                        step=-5;

                  that.facade.sendNotification(EditorStatic.CMD_MOVE_SELECTED_COMPONENT_INSTANCE, {propertyName:"z",step:step});
                  event.preventDefault();
                  event.stopPropagation();
                  return;
            }






            //FPS 호출
            if (inputKey == "4" && event.ctrlKey==true && event.altKey==true) {
                  that.facade.sendNotification(CommonStatic.CMD_ACTIVE_FPS);
                  event.preventDefault();
                  return;
            }
	}



}
