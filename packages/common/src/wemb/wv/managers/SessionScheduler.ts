import Vue from "vue";
import EditorStatic from "../../../editor/controller/editor/EditorStatic";
import sessionApi from "../../../common/api/sessionApi";
import ViewerStatic from "../../../viewer/controller/viewer/ViewerStatic";

/**
 * 세션 시간을 체크하는 스케줄러
 * RegisterGlobalVariableCommand에서 초기 세션 시간을 셋팅해준다. session시간은 configManager에서 조회 가능
 * 마지막 세션 체크 시간은 local 시간을 참조하여 처리한다. 사용자가 중간에 시간을 바꿀 경우 바로 세션이 종료되거나 할 수 있다. 이경우는 예외로 침.
 * viewer에서는 SessionCheckingManager를 이용하여 주기적으로 세션을 연장하는 객체가 실행된다. 실제로 editor에서만 유효한 스케줄러이다.
 * 
 * 마지막 세션 연장한 시간을 로컬스토리지에 저장하고 이 정보를 세션시간을 client에서 체크하게 되는데,
 * 세션 시간을 저장하는 메소드는 window에 선언되어있다. window._getSessionUpdateTime()
 * 이유는 자산관리자와 같은 별도의 window에서 작업할때에서 세션 시간 연장이 필요하는데, window.opener.으로 opener의 객체 정보를 조회할경우
 * ie에서 심각한 속도 저하 현상이 발생하여 이와 같은 방법으로 처리한다. 자산관리자에서는 API호출시 window.opener._getSessionUpdateTime()를 호출하여 마지막 세션 연장 시간을 업데이트 한다.
 * 
 * 세션 시간 관리는 SessionScheduler 객체가,
 * 세션 경고 팝업, 만료팝업은 SessionTimeoutDialog 객체가 처리한다.
 *  
*/
export class SessionScheduler {
    private static _instance = null;

    private _waringTime: number = 30*60*1000;//세션 경고 시간
    private _expireTime:number = 30*60*1000;//세션 만료 시간

    private _sessionCheckIntervalId:number = null;
    private _isDestroyed: boolean = false;
    private _isStart = false;

    constructor() {
        this.lastResponseTime = new Date().getTime();
        this.bindUserActionEvent();
    }


    //사용자 행동이(mousedown) 있을 경우 체크하여 세션시간을 연장함, 40초마다 체크함 
    bindUserActionEvent(){
        let  timer = null;
        $("body>#app-main").on("mousedown.sessionScheduler", ()=>{
            let time = new Date().getTime();
            if(timer){return;}            
            timer = setTimeout(()=>{
                //마지막 세션 갱신 시간보다 사용자가 편집한 시간이 나중일 경우, 세션을 연장한다.
               if(this.lastResponseTime < time){
                    this.prolongSession();
                }
                timer = null;
            }, 40*1000);//40초마다 체크            
        })
    }

   
    /*마지막 세션 연장 시간을 셋팅*/
    public set lastResponseTime(time:number){
        //localstorage에 세션 갱신 시간을 저장
        window._setSessionUpdateTime(time);
        //localStorage.setItem("__renobit_supdatetime__", time.toString());
    }

    /*마지막 세션 연장 시간을 반환*/
    public get lastResponseTime(){
        let time = window._getSessionUpdateTime();
        //localStorage.getItem("__renobit_supdatetime__");
        return parseInt(time);
    }

    public get isStart(){
        return this._isStart;
    }

    public setSessionTime(sec: number){
        if(this._isDestroyed || !sec){return;}
        this._isStart = true;
        this._expireTime =  Math.max((sec-60), 60)*1000;
        this._waringTime = this._expireTime - 60000;///세션 만료시간보다 1분 더 빠르게 noti
        this.reset();
    }

    public static getInstance():SessionScheduler {
        if (SessionScheduler._instance == null) {
            SessionScheduler._instance = new SessionScheduler();
        }
        return SessionScheduler._instance;
    }

    reset(){
        if(this._isDestroyed || !this._isStart){return;}
        this.setIntervalCheckSessionTime();
        SessionTimeoutDialog.getInstance().close();
    }

    // 세션 연장하는 api호출
    prolongSession(){
        if(this._isDestroyed || !this._isStart){return;}
        return sessionApi.prolongSession();
    }


    destroySession(){
        if(this._isDestroyed || !this._isStart){return;}   
        this.clearIntervalCheckSessionTime();
        this._isDestroyed = true;
        this._isStart = false;
        sessionApi.destroySession();
    }

    setIntervalCheckSessionTime(){
        if(this._isDestroyed || !this._isStart){return;}
        this.clearIntervalCheckSessionTime();
        this._sessionCheckIntervalId = setInterval(()=>{            
            let time = new Date().getTime() - this.lastResponseTime;
            if( time  > this._expireTime ){
                this.openExpiredSession();
            }else if( time > this._waringTime){
                this.openWaringSesionTimeDialog();
            }else{
                if( SessionTimeoutDialog.getInstance().isOpend ){
                    SessionTimeoutDialog.getInstance().close();
                }
            }
        }, 1000);
    }

    clearIntervalCheckSessionTime(){
        clearInterval(this._sessionCheckIntervalId);
    }

    /*세션 만료시간이 임박하여 팝업 호출*/
    openWaringSesionTimeDialog(){
        if(this._isDestroyed || !this._isStart){return;}
        SessionTimeoutDialog.getInstance().open();
    }

    /*세션 만료시 팝업에 expiredSession메소드 호출*/
    openExpiredSession(){
        if(this._isDestroyed || !this._isStart){return;}
        SessionTimeoutDialog.getInstance().expiredSession();
    }
}


export class SessionTimeoutDialog {
    private static _instance = null;
    private static count: number = 60;
    private intervalId: number;
    private _isOpend: boolean = false;
    private _isLogout: boolean = false;
    constructor() {

    }

    public static getInstance():SessionTimeoutDialog {
        if (SessionTimeoutDialog._instance == null) {
            SessionTimeoutDialog._instance = new SessionTimeoutDialog();
        }

        return SessionTimeoutDialog._instance;
    }

    public open(){
        if(this._isOpend || this._isLogout){return;}
        this._isOpend = true;
        this.drawDialog();
        this.startCountdown();
        this.bindEvent();
    }

    public close(){
        if(!this._isOpend){return;}
        this._isOpend = false;
        this.clearInterval();
        $("body").find("#session-popup").remove();
    }

    public expiredSession(){
        if(!this.isOpend){
            this.open();
        }
        
        this.clearInterval();
        this.setCount("00");
        SessionScheduler.getInstance().destroySession();
        $("#session-popup").attr("data-state", "after");
    }

    public get isOpend(){
        return this._isOpend;
    }

    drawDialog(){
        let locale:any = Vue.$i18n.messages.wv.sessionDialog;
        if(!locale){return;}
        let imgSrc = wemb.configManager.serverUrl + "/client/common/image/clock.png";
        let warnImg = wemb.configManager.serverUrl + "/client/common/image/warn.png";
        let tempStr:string =`
        <div id="session-popup" data-state="before">
            <div class="timeout-wrap">
                <div class="header">
                    <img src=${warnImg} alt="clock">
                    <h4>${locale.title}</h4>
                </div>
                <div class="time-wrap">
                    <img src=${imgSrc} alt="clock">
                    <span class="time">${SessionTimeoutDialog.count}</span>
                </div>
                <p class="before">
                    <strong>${locale.beforeMsg01}</strong> 
                    <br>${locale.beforeMsg02}
                </p>
                <p class="after">
                    <strong>${locale.afterMsg01}</strong>
                    <br><span data-only-edit>${locale.afterMsg02}</span></p>
                <div class="btn-wrap before">
                    <a data-act="logout" href="#">${locale.logout}</a>
                    <a data-act="prolong" href="#">${locale.keepSignedIn}</a>
                </div>
                <div class="btn-wrap after">
                    <a data-act="logout" href="#">${locale.login}</a>
                    <a data-act="export" href="#" data-only-edit>${locale.exportPage}</a>
                </div>
            </div>
        </div>
        `;

        let $body = $("body");
        $body.find("#session-popup").remove();
        $body.append(tempStr);

        if( !wemb.configManager.isEditorMode ){
            $body.find("#session-popup").find("[data-only-edit]").remove();
        }
    }

    private bindEvent(){
        let self = this;

        $("#session-popup").off("click.sessionDialog").on("click.sessionDialog", "[data-act]", function(e){
            e.preventDefault();
            let act = $(this).attr("data-act");
            switch( act ){
                case "logout":
                    self.logout();
                break;
                case "export":
                    self.exportPage();
                break;
                case "prolong":
                    self.prolongSession();
                break;
            }
        });
    }

    private logout(){
        this._isLogout = true;
        this.close();
        this.getProxy().sendNotification(this.logoutCMD);
    }

    private get logoutCMD(){
        if( wemb.configManager.isEditorMode ){
            return  EditorStatic.CMD_GOTO_LOGOUT;
        }else{
            return ViewerStatic.CMD_GOTO_LOGOUT;
        }
    }

    //세션 연장하기 버튼을 누른경우 세션 연장을 시도하고 실패시 세션 만료 상태로 처리한다.
    private prolongSession(){
        SessionScheduler.getInstance().prolongSession().then((r)=>{
            if(r.success == false || r.error) {
                this.expiredSession();
            }
        });
    }

    private exportPage(){
        if( wemb.configManager.isEditorMode ){
            this.getProxy().sendNotification( EditorStatic.CMD_EXPORT_PAGE );
        }
    }

    private getProxy(){
        if( wemb.configManager.isEditorMode ){
            return  wemb.editorProxy;
        }else{
            return wemb.viewerProxy;
        }
    }


    startCountdown(){
        let count = SessionTimeoutDialog.count;
        let current = count;
        this.clearInterval();
        this.intervalId = setInterval(()=>{
            current--;
            let time = this.getDigitNum(current);
            this.setCount(time);
            if(current==0){
                this.expiredSession();
            }
        }, 1000);
    }


    setCount(countStr){
        $("#session-popup .time").html(countStr);
    }
    

    clearInterval(){
        clearInterval(this.intervalId);
    }

    private getDigitNum(no:number):string {
        let str:string = no.toString();
        if (no < 10) {
            str = "0" + no;
        }

        return str;
    }
}
