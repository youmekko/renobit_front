import WindowManager from "./WindowsManager";
import ExternalWindowMediator from "../extension/ExternalWindowMediator";

export default class ExternalWindowManager {
      private static _instance:ExternalWindowManager = null;
      private windowManager:WindowManager = null;

      protected constructor() {
            this.windowManager = WindowManager.getInstance();
      }

      public static getInstance():ExternalWindowManager {
            if(ExternalWindowManager._instance==null){
                  ExternalWindowManager._instance = new ExternalWindowManager();
            }

            return ExternalWindowManager._instance;
      }


      public open(data:any) {
            if(data) {
                  let popup = this.windowManager.open(data);
                  let self = this;
                  let $popup = $(popup);
                  $popup.on("message",function(result){

                        let resultData = result.originalEvent.data;
                        console.log("@@ external 4, 서브로부터  메시지 받기 ", resultData);
                        if(resultData.command=="start")
                              self._createMediator(data.NAME, popup);

                        $popup.off();

                        $popup.on("closed",function(){
                              self._removeMediator(data.NAME);
                              $popup.off();
                        })
                  })
            }
      }



      private _createMediator(mediatorName, managerWindow:any){
            try {
                  console.log("@@ external 5, _createMediator() 메디에이더 등록 시작", mediatorName);
                  let mediator = new ExternalWindowMediator(mediatorName, managerWindow, managerWindow.getNotificationList());
                  let facade = window.wemb.editorFacade;
                  facade.registerMediator(mediator);

                  console.log("@@ external 5, _createMediator() 메디에이더 등록 완료", facade);

            }catch(error){
                  console.log("###################");
                  console.warn("@@ external error ", error);
                  console.log("###################");
            }
      }
      private _removeMediator(mediatorName){
            let facade = window.wemb.editorFacade;
            facade.removeMediator(mediatorName);
      }
}
