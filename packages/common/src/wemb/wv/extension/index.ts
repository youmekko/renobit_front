import CustomMediator from './CustomMediator'
import CustomPanel from './CustomPanel' //vue
import ExternalWindowMediator from './ExternalWindowMediator'

const extension = {
    CustomMediator,
    CustomPanel,
    ExternalWindowMediator
}

export default extension