import Vue from "vue";
import * as puremvc from "puremvc";
import CustomPanel from "./CustomPanel.vue";

export default class CustomMediator extends puremvc.Mediator {
      public static NAME: string = "CustomMediator";
      private static _count:number = 0;
      public static getCount(){
            CustomMediator._count++;
            return CustomMediator._count;
      }
      constructor(name: string, viewComponent: Vue, private notiList:string[]=[]) {
            super(name, viewComponent);
      }


      public getView(): CustomPanel {
            return this.viewComponent;
      }

      public onRegister() {



      }


      public listNotificationInterests(): string[] {
           return this.notiList;
      }


      /*
	배경 값의 경우 바인딩으로 사용하기 때문에 따로 뷰에 업데이트해줄 필요가 없읎ㅇ.ㅁ
	 */
      public handleNotification(note: puremvc.Notification): void {
            //console.log("CustomPanel note1 = ", note);
            try {
                  this.getView().handleNotification(note);
            }catch(error){

            }
      }

}
