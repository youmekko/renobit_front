import WVEvent from "../../core/events/WVEvent";
import WVDisplayObject from "../../core/display/WVDisplayObject";
import CPNotification from "../../core/utils/CPNotification";
import {IMetaProperties, IWVDisplayObject} from "../../core/display/interfaces/DisplayInterfaces";
import {
      IThreeExtensionElements, IWV3DComponent
} from "../../core/component/interfaces/ComponentInterfaces";

import PersonViewControls from "../../core/component/3D/PersonViewControls";
import {THREE, THREEx} from "../../core/utils/reference";
import {EXE_MODE} from "../data/Data";
import OrbitDecorator from "../../core/component/3D/OrbitDecorator";
import {LayerCore} from "./Layer";

import LoaderManager from "../../core/component/3D/manager/LoaderManager";
import MeshManager from "../../core/component/3D/manager/MeshManager";

import NLoaderManager from "../../core/component/3D/manager/NLoaderManager";
import {Mesh} from "three";

/*
주요 관리 내용
1. scene에 mesh 추가 삭제
2. meshListMap에 mesh를 키로 연관 컴포넌트 추가 삭제
3. 자체 렌더링을 사용하는  컴포넌트 관리, 컴포넌트 id를 키로 컴포넌트 추가, 삭제
4. 라벨 관리, mesh.uuid로 라벨(DOM) 관리
 */
export abstract class ThreeLayerCore extends LayerCore {

      protected _boxHelper:any;

      protected _scene: any = null;
      protected _sceneHelper: any = null;

      protected _controls: OrbitDecorator;
      protected _personControls:PersonViewControls;
      protected _isUsePersonView:boolean;
      protected _personControlTarget:any;

      protected _camera: any = null;
      protected _renderer: any = null;
      protected _labelRenderer:any = null;

      protected _transformControls: any = null;
      protected _isDownTransformControls:boolean = false;

      protected _requestAnimationID: number = -1;
      protected _enable: boolean = true;

      // 전역으로 사용하는 마우스 다운 위치
      protected _mouse = new THREE.Vector2();
      protected _raycaster = new THREE.Raycaster();

      protected _appendElementListMap:Map<any, IWVDisplayObject> = new Map();


      protected _componentListUsingRenderer:Map<any, IWV3DComponent> = new Map();


      //protected _$labelContainer:any = null;
      protected _labelListMap:Map<any, any> = new Map();



      protected _instanceContainer:THREE.Group;
      //static INSTANCE_CONTAINER_NAME:string = "WV3D_INSTANCE_CONTAINER";
      protected _threeElements:IThreeExtensionElements = null;
      public get threeElements(){
            this._threeElements.threeLayer = this;
            return this._threeElements;
      }

      //protected _directionViewScene:DirectionViewScene = null;

      /*
      2018.09.20(ckkim)


      public get directionViewScene(){
            return this._directionViewScene;
      }
      */







      protected constructor() {
            super();
      }

      protected _onDestroy(){

            this._stopRenderingLoop();

            this.clear();

            if(this.threeElements && this.threeElements.domEvents){
                  this.threeElements.domEvents.destroy();
                  this.threeElements.domEvents = null;
            }

            this._threeElements = null;
            /*
            if(this._directionViewScene){
                  this._directionViewScene.destroy();
                  this._directionViewScene = null;
            }*/

            if(this._controls){
                  this._controls.dispose();
                  this._controls = null;
            }

            if(this._personControls){
                  this._personControls.destroy();
                  this._personControls = null;
            }

            this._clearChildByContainer(this._instanceContainer);
            this._clearChildByContainer(this._sceneHelper, true);
            this._clearChildByContainer(this._scene, true);
            this._renderer.dispose();
            this._renderer = null;

            this._labelRenderer = null;

            this._mouse =null;
            this._raycaster = null;

            this._sceneHelper = null;
            this._scene = null;
            this._camera = null;


            $(this._element).off();
            $(this._element).empty();

            super._onDestroy();

      }


      /*
     레이어는 그대로 둔 상태에서 요소만을 제거하기
	*/

      public clear(){
            this._stopRenderingLoop();

            console.log("clear레이어 정보");
            // 1 컴포넌트 제거
            this.removeChildAll();

            // 2. 혹시 모르니까, mesh를 키로한 컴포넌트 목록을 다시 제거
            this._appendElementListMap.clear();

            // 3. label 삭제하기
            this._clearLabelArea();

            // 4. three 컨트롤 리셋
            this.resetControls();

            // 6. scene에 남아 있는 mesh 제거
            //this._clearChildByContainer(this._scene);
            this._clearChildByContainer(this._instanceContainer);

            // 7. 커스텀 렌더러 제거하기
            this._componentListUsingRenderer.clear();


            // 8. 로더매니저 풀 제거
            // NLoaderManager.clear();
            // THREE.Cache.clear();
            this._renderer.clear();
            this._renderer.renderLists.dispose();
            this._lastRenderList =[];
            /*
            이벤트 초기화
             */
            //if(this._exeMode==EXE_MODE.VIEWER) {
            /*if(this._threeElements.domEvents != null ) {
                  this._threeElements.domEvents = new THREEx.DomEvents(this._camera, this._renderer.domElement);
            }*/
            this._threeElements.domEvents = this._createDomEvents();
            this._startRenderingLoop();
      }

      protected _clearChildByContainer(objContainer:THREE.Object3D, clearAll?:boolean ):void{
            let i:number = objContainer.children.length;
            let child:THREE.Object3D;
            while(i--){
                  child = objContainer.children[i];
                  if(child instanceof THREE.Mesh ){
                        MeshManager.disposeMesh(child);
                        objContainer.remove(child);
                  } else {
                        if(clearAll){
                              objContainer.remove(child);
                        }
                  }
            }
      }


      protected _createElement(template: string = null) {
            let $element = $(`<div></div>`);
            $element.css({
                  position:"absolute"
            })

            //this._$labelContainer = $("<div class='label-container'></div>")
            //$element.append($("<div class='direction-view'></div>"));
            //$element.append(this._$labelContainer);

            this._element = $element[0];

            this._startLayer();

      }


      protected _createProperties(properties: IMetaProperties): any {
            try {
                  // 기본 값 생성하기
                  let newProperties: any = {
                        x:0,
                        y:0,
                        width:0,
                        height:0
                  };
                  // 기본 값 + 입력 값 조합하기
                  $.extend(true, newProperties, properties);

                  return newProperties;
            } catch (error) {
                  console.log("경고 컴포넌트 프로퍼티 스타일이 맞지 않습니다.", error);
                  return null;
            }
      }


      protected abstract _startLayer();
      // 컴포넌트 관련 내용 완료
      /////////////////////////////////////////////////////////////////
































      /////////////////////////////////////////////////////////////////
      // three관련 요소 생성
      protected _createThreeElements() {

            this._createScene();
            this._createHelperScene();
            this._createBoxHelperControls();
            this._createRenderer();
            this._createCamera();
            this._createMainController();
            this._createPersonController();
            this._createThreeElementsObject();
            //this._createDirectionViewScene();

      }

      protected _createPersonController():void{
            // person뷰 처리를 위한 컨트롤 설정
            this._personControlTarget = new THREE.Object3D();
            this._personControls = new PersonViewControls( this._camera, this._controls );
            this._personControls.icon.add(this._camera);
            this._scene.add(this._personControls.person);
      }

      private _createScene() {
            this._scene = new THREE.Scene();
            this._instanceContainer = new THREE.Group;
            this._instanceContainer.name = "WV3D_INSTANCE_CONTAINER";
            var light = new THREE.AmbientLight(0xffffff);
            this._scene.add(light);
            this._scene.add(this._instanceContainer);
      }

      private _createHelperScene() {
            this._sceneHelper = new THREE.Scene();
      }


      private _createRenderer() {

            this._labelRenderer = new THREE.CSS2DRenderer();
            this._labelRenderer.setSize(this.element.clientWidth, this.element.clientHeight);
            $(this._labelRenderer.domElement).addClass("label-container").css({"position":"absolute", "top":0, "pointer-events":"none"});
            this.element.appendChild(this._labelRenderer.domElement);

            ///////////////
            // 렌더러 생성
            this._renderer = new THREE.WebGLRenderer({alpha: true, antialias: true });
            // 멀티 scene처리를 위한 자동 갱신 방지
            this._renderer.autoClear = false;
            this._renderer.autoUpdateScene = false;
            this._renderer.setClearColor(0x000000, 0);
            this._renderer.setSize(this.element.clientWidth, this.element.clientHeight);
            // element에 렌더러 붙이기
            this.element.appendChild(this._renderer.domElement);



            this._renderer.context.canvas.addEventListener("webglcontextlost", (event) => {
                  alert("webgl context lost");
                  event.preventDefault();
                  this._stopRenderingLoop();
            }, false);

            this._renderer.context.canvas.addEventListener("webglcontextrestored", (event) =>{
                  // Do something
                  alert("webgl context restore");
                  event.preventDefault();
                  this._startRenderingLoop();
            }, false);


            ///////////////
      }

      private _createCamera() {
            ///////////////
            // 카메라 생성
            let aspectRatio = this._getAspectRatio();
            this._camera = new THREE.PerspectiveCamera(
                  60,
                  aspectRatio,
                  1,
                  10000
            );
            this._camera.position.set(0, 100, 100);
            ///////////////
      }

      protected _createMainController(){
            ///////////////
            // 컨트롤러 생성
            let controlComp = new THREE.OrbitControls(this._camera, this._renderer.domElement);
            this._controls = new OrbitDecorator( controlComp);
            this._controls.component.maxPolarAngle = Math.PI/2;
            this._controls.component.minPolarAngle = 0; // radians
            this._controls.enabled = true;
            ///////////////
            console.log("콘트롤 생성!!!");

      }


      protected _createBoxHelperControls() {
            this._boxHelper = new THREE.BoxHelper;
            this._boxHelper.material.depthTest = false;
            this._boxHelper.material.transparent = true;
            this._boxHelper.material.visible   = false;
            this._sceneHelper.add(this._boxHelper);
      }

      protected _createThreeElementsObject() {

            /*
            중요사항
            이 정보는 시작시 registerGlobalVariableCommand에서
            전역 데이터로 설정됨.
             */

            this._threeElements = {
                  threeLayer:null,
                  domEvents: this._createDomEvents(),
                  camera: this._camera,
                  mainControls: this._controls,
                  personControls: this._personControls,
                  renderer: this._renderer,
                  scene: this._scene,
                  raycaster: this._raycaster,
                  labelList: this._labelListMap,
                  boxHelper: this._boxHelper
            } as IThreeExtensionElements;
            /*
            if(this._exeMode==EXE_MODE.VIEWER) {
                  //this._threeElements.domEvents = new THREEx.DomEvents(this._camera, this._renderer.domElement);
                  //this._threeElements.rackController = new RackGroupController(this._threeElements);
                  console.log("레이어 정보", this);
            }*/

      }


      protected _createDomEvents(){

            let that = this;
            /*
            2018.06.12일
            DomEvents를 override 처리
            editAreaMainElement의 scroll 값을 추가해서 처리
            */
            THREEx.DomEvents.prototype._getRelativeMouseXY = function (domEvent) {
                  try {
                        let areaMainElement = that.element.parentNode.parentNode;

                        var element = domEvent.target || domEvent.srcElement;
                        if (element.nodeType === 3) {
                              element = element.parentNode; // Safari fix -- see http://www.quirksmode.org/js/events_properties.html
                        }

                        //get the real position of an element relative to the page starting point (0, 0)
                        //credits go to brainjam on answering http://stackoverflow.com/questions/5755312/getting-mouse-position-relative-to-content-area-of-an-element
                        var elPosition = {x: 0, y: 0};
                        var tmpElement = element;
                        //store padding
                        var style = getComputedStyle(tmpElement, null);
                        elPosition.y += parseInt(style.getPropertyValue("padding-top"), 10);
                        elPosition.x += parseInt(style.getPropertyValue("padding-left"), 10);
                        //add positions
                        do {
                              elPosition.x += tmpElement.offsetLeft;
                              elPosition.y += tmpElement.offsetTop;
                              style = getComputedStyle(tmpElement, null);

                              elPosition.x += parseInt(style.getPropertyValue("border-left-width"), 10);
                              elPosition.y += parseInt(style.getPropertyValue("border-top-width"), 10);
                        } while (tmpElement = tmpElement.offsetParent);

                        var elDimension = {
                              width: (element === window) ? window.innerWidth : element.offsetWidth,
                              height: (element === window) ? window.innerHeight : element.offsetHeight
                        };

                        return {
                              x: +(((domEvent.pageX + areaMainElement.scrollLeft) - elPosition.x) / elDimension.width) * 2 - 1,
                              y: -(((domEvent.pageY + areaMainElement.scrollTop) - elPosition.y) / elDimension.height) * 2 + 1
                        };
                  }catch(error){
                        console.log("ThreeLayerCore error_createDomEvents  ", error);
                  }
            };

            return new THREEx.DomEvents(this._camera, this._renderer.domElement);
      }

      /*
      private _createDirectionViewScene(){
            ///////////////////////////////////////
            // 나침반 (오리엔테이션툴) 생성
            ///////////////////////////////////////
            this._directionViewScene = new DirectionViewScene(this._camera, this._controls, {
                  container: this._element.querySelector(".direction-view"),
                  workHolder: null
            });
            this._directionViewScene.setTextureUrl();
      }
      */


      protected _getAspectRatio() {
            return this.element.clientWidth / this.element.clientHeight;
      }


      protected _startRenderingLoop() {
            let threeManager = this;

            if (this._requestAnimationID == -1) {
                  (function localRender() {
                        if (threeManager._enable == true) {
                              threeManager._requestAnimationID = requestAnimationFrame(localRender);
                              threeManager._render();

                        }
                  }());
            }

      }

      protected _stopRenderingLoop() {

            cancelAnimationFrame(this._requestAnimationID);
            this._requestAnimationID = -1;
      }

      protected _lastRenderList = [];

      protected arraysEqual(a, b) {
            if (a === b) return true;
            if (a == null || b == null) return true;
            if (a.length != b.length) return false;
            for (let i = 0; i < a.length; ++i) {
                  if (a[i] !== b[i]) return false;
            }
            return true;
      }


      /**
       * 카메라 줌 시 2d라벨이 화면에 보이는 것을 예외처리
       * renderer에 목록을 이용해 컴포넌트 목록중 scene에 보이는 것만 label을 보이게 처리
       * */
      protected _renderComponentLabel(){
            if(this.isViewerMode){
                  let renderList = this._renderer.renderLists.get(this._scene, this._camera);
                  let renderObjs = [].concat(renderList.opaque, renderList.transparent);
                  let parentList = renderObjs.reduce((list, currentObj)=>{
                        if(currentObj.object.type == "Mesh" || currentObj.object.type === "Line2"){
                              let objParent  = currentObj.object.parent;
                              while( objParent && objParent.name != "WV3D_INSTANCE_CONTAINER" && objParent.type != "Scene" ){
                                    let next = objParent.parent;
                                    if( next && next.name == "WV3D_INSTANCE_CONTAINER"){
                                          if( list.indexOf(objParent)==-1){
                                                list.push(objParent);
                                          }
                                    }
                                    objParent = objParent.parent;
                              }
                        }
                        return list;
                  }, []);
                  if(this.arraysEqual(parentList, this._lastRenderList))return;
                  this._lastRenderList = parentList;
                  this._appendElementListMap.forEach((value:any, key:any)=>{
                        if(value.getGroupPropertyValue("label", "label_using") == "Y"){
                              if(parentList.indexOf(key) > -1){
                                    value._label && value._label.show();
                              } else {
                                    value._label && value._label.notShow();
                              }
                        }
                  });

            }
      }


      protected _render() {

            ///page 컴포넌트가 있는 페이지에서 이동시 _controls null참조 에러로 임시 예외처리 추가
            if( !this._controls ){return;}
            this._controls.update();
            this._customComponentRender();
            this._updateLabelPosition();
            if(this._personControls.enabled){ this._personControls.update(); }
            if(this._boxHelper.object != null ){ this._boxHelper.update(); }
            this._renderer.clear(true, true, true);
            //this._renderer.renderLists.dispose();
            this._renderer.render( this._scene, this._camera );
            this._renderer.render( this._sceneHelper, this._camera );
            this._labelRenderer.render( this._scene, this._camera );
            if(this.isViewerMode){this._renderComponentLabel();}
      }

      // 렌더러를 가진 컴포넌트를 렌더링하기
      private _customComponentRender(){

            this._componentListUsingRenderer.forEach((instance:IWV3DComponent)=>{
                  instance.render( );
            })
      }
      //////////////////////////////////////////////////////////////








      //////////////////////////////////////////////////////////////
      /*
      크기 처리
      EditorPageComponent에서 실행.
       */
      public updateSize(width: number, height: number, borderRadius:object) {
            this.width = width;
            this.height = height;
            this.borderRadius = borderRadius;
            this._camera.aspect = width / height;
            this._camera.updateProjectionMatrix();
            this._renderer.setSize(width, height);
            this._labelRenderer.setSize(width, height);
      }

      /*
            마우스 좌표에 해당하는 컴포넌트를 반환
      */
      public getIntersectionObject( event:any ){
            let results = this._threeElements.domEvents._getIntersectionObject(event);
            if(results.length){
                  if(this._appendElementListMap.has(results[0])){
                        return this._appendElementListMap.get(results[0]);
                  }
            }
            return null;
      }

      /*
      마우스 이벤트 활성화/비활성화 처리
       */
      public setMouseEventEnabled(enabled:boolean){
            if(enabled){
                  $(this.element).css("pointer-events", "auto");
            }else {
                  $(this.element).css("pointer-events", "none");
            }
      }
      //////////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////
      public addChildAt(component: IWVDisplayObject, index: number): IWVDisplayObject {
            try{
                  if(!this.hasChild(component)) {
                        this.callLaterDispatch(component as WVDisplayObject, new WVEvent(WVEvent.ADDED, component));

                        this._childs.splice(index, 0, component);
                        this._instanceContainer.add(component.appendElement);
                        this._appendElementListMap.set(component.appendElement, component);
                        this.onAddedChild(component);
                        let threeComponent:IWV3DComponent = component as IWV3DComponent;
                        if(threeComponent.usingRenderer)
                              this._componentListUsingRenderer.set(threeComponent.id, threeComponent);

                        return component;
                  }
            }catch(e){
                  CPNotification.getInstance().throwErrorWithMessage( this.constructor.name + " " + e.toString() );
            }
            return null;
      }

      /*

            // 주요 제거 내용
            1. scene에 mesh 추가 삭제
            2. meshListMap에 mesh를 키로 연관 컴포넌트 추가 삭제
            3. 자체 렌더링을 사용하는  컴포넌트 관리, 컴포넌트 id를 키로 컴포넌트 추가, 삭제
            4. 라벨 관리, mesh.uuid로 라벨(DOM) 관리


            // 추가사항
            1. 이벤트 삭제하기
            2. 이벤트 처리를 위한 onRemoveChild 실행
       */
      public removeChildAt( index: number ): IWVDisplayObject {
            try{

                  // 1. scene에서 mesh 삭제하기
                  let comInstance:WVDisplayObject = this._childs.splice( index, 1 )[0] as WVDisplayObject;
                  this._instanceContainer.remove(comInstance.appendElement);
                  // 2. 맵에서 emsh를 키로 저장한 컴포넌트 인스턴스 제거
                  this._appendElementListMap.delete(comInstance.appendElement);
                  // label 삭제
                  this._removeLabelElement(comInstance.appendElement);

                  // 3. 랜더링 맵에서 삭제
                  this._componentListUsingRenderer.delete(comInstance.id);

                  // 4. 이벤트 발생
                  this.dispatchEvent(new WVEvent(WVEvent.REMOVE, comInstance));
                  comInstance.destroy();

                  return comInstance;

            } catch(e) {

                  CPNotification.getInstance().throwErrorWithMessage("ThreeLayer removeChildAt" + this.constructor.name + " " + e.toString());

            }

            return null;
      }

      private _removeLabelElement(mesh){
            //라벨 사용시 라벨 제거

            if(this._labelListMap.get(mesh.uuid) != undefined){
                  $(this._labelListMap.get(mesh.uuid).elementLabel).remove();
                  this._labelListMap.delete(mesh.uuid);
            }


      }



      //////////////////////////////////////////////////////////////










      //////////////////////////////////////////////////////////////


      protected _getUnLockComponentElementList():Array<any>{
            let valueList:Array<any> = Array.from(this._appendElementListMap.keys());
            return  valueList;
      }








      /////////////////////////////////////////////////////
      /*
      labelArea 영역 처리
       */
      private _clearLabelArea(){

            /* 라벨 맵에서 리스트 제거 */
            this._labelListMap.forEach((mesh:any)=>{
                  this._removeLabelElement(mesh);
            })

            // 속시 모르니까 labelcontainer에 남아 있는 쓰레기 label 제거하기
            //this._$labelContainer.off();
            $(this._labelRenderer.domElement).empty();
            this._labelListMap.clear();


      }


      private _updateLabelPosition(){
            this._labelListMap.forEach((labelElement:any)=>{
                  labelElement.updatePosition();
            });
      }
      /////////////////////////////////////////////////////




      public resetControls():void{
            if(this._isUsePersonView || this._personControls.enabled ){
                  this._personControls.deActiveHelper(.1);
                  this._personControls.enabled = false;
                  this._isUsePersonView = false;
            }
      }




      /* 워크 뷰 보기 토글 처리 */
      public toggleWorkView( bValue:boolean, callback?:Function ){
            if( this._isUsePersonView != bValue ){
                  this._isUsePersonView = bValue;
                  if(this._isUsePersonView){
                        this._personControls.activeHelper( this._personControlTarget, 1, () => {
                              if(callback){ callback.apply(null);}
                        } );
                  } else {
                        this._personControls.deActiveHelper( 1, () => {
                              if(callback){ callback.apply(null);}
                        });
                  }
            }
      }


}
