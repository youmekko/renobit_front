import {EXE_MODE} from "../data/Data";
import {TwoLayerCore} from "./TwoLayerCore";

export class EditTwoLayer extends TwoLayerCore {
      public static readonly NAME="EditTwoLayer";
      constructor() {
            super();
            this._exeMode=(EXE_MODE.EDITOR as string);
      }
}

EditTwoLayer["default_properties"] = {
      "category": "2D",
      "lock": false,
      "x": 0,
      "y": 0,
      "borderRadius": {
            topLeft:0,
            topRight:0,
            bottomLeft:0,
            bottomRight:0
      },
      "width": 100,
      "height": 100,
      "id": "",
      "name": "",
      "depth": 1,
      "visible": true
}

