import {ThreeLayerCore} from "./ThreeLayerCore";
import {THREE, THREEx} from "../../core/utils/reference";
import OrbitDecorator from "../../core/component/3D/OrbitDecorator";
import {EXE_MODE} from "../data/Data";
import {IWVComponent} from "../../core/component/interfaces/ComponentInterfaces";
import WVMouseEvent from "../../core/events/WVMouseEvent";
import {LayerEvent} from "./Layer";
import ViewerProxy from "../../../viewer/model/ViewerProxy";
/*
주요 관리 내용

1. scene에 mesh 추가 삭제
2. meshListMap에 mesh를 키로 연관 컴포넌트 추가 삭제
3. 자체 렌더링을 사용하는  컴포넌트 관리, 컴포넌트 id를 키로 컴포넌트 추가, 삭제
4. 라벨 관리, mesh.uuid로 라벨(DOM) 관리


 */
export class ViewerThreeLayer extends ThreeLayerCore {
      public static readonly NAME="ViewerThreeLayer";
      private _func_mouseDownSceneHandler:Function = null;
      // public mouseOveredComponent:any = null; cctv context click test do not erase;
      private mouseovercolor = null;
      set mouseOverOutlineColor(color){
            this.mouseovercolor = new THREE.Color(color);
      }
      get mouseOverOutlineColor(){
            return this.mouseovercolor;
      }
      constructor() {
            super();
            this._exeMode=EXE_MODE.VIEWER;
            this._func_mouseDownSceneHandler= this._onMouseDownScene.bind(this);
            this.mouseovercolor = new THREE.Color(0xff0000);
      }



      /*
      createElement()에서 호출
       */
      protected _startLayer(){
            this._createThreeElements();
            this._startRenderingLoop();

            this._initEventListener();
      }

      /*
      protected _createMainController() {
            ///////////////
            // 컨트롤러 생성
            let controlComp = new THREE.OrbitControls(this._camera, this._renderer.domElement);
            this._controls = new OrbitDecorator( controlComp);
            this._controls.staticMoving = true;
            this._controls.component.maxPolarAngle = Math.PI/2;
            this._controls.component.minPolarAngle = 0; // radians
            this._controls.enabled = true;
      }*/
     //////////////////////////////////////////////////////////////

      private _initEventListener() {
            /*요소 이외에 영역을 클릭하는 경우 선택한 요소를 취소 처리하기 위해 이벤트를 상위로 흘려줘야 한다.*/
            //this._renderer.domElement.addEventListener('click', this._func_mouseDownSceneHandler, false);
            this._controls.component.addEventListener('start', function(){
                  this._threeElements.domEvents.enabled = false;
            }.bind(this))
            this._controls.component.addEventListener('end', function(){
                  this._threeElements.domEvents.enabled = true;
            }.bind(this))
      }



      private _onMouseDownScene( event ){
            $("body").css("event");
            // window.wemb.threeElements.threeLayer.mouseOveredComponent = null; cctv context click test do not erase;
            let evt = event.data ? event.data.originEvent : event;
            evt.stopPropagation();
            let instance:IWVComponent = null;
            if(event instanceof WVMouseEvent ){
                  evt.stopImmediatePropagation();
                  instance = event.target;
            }

            if(instance && instance.selected==false) {
                  console.log("TEST2 1 ")
                  /*this.dispatchEvent(new LayerEvent(LayerEvent.COMPONENT_MOUSE_DOWN, this,  evt,{
                        targetInstance:instance
                  }));*/
            } else {
                  /*this.dispatchEvent(new LayerEvent(LayerEvent.LAYER_MOUSE_DOWN, this,  evt,{
                        targetInstance:this
                  }));*/
                  console.log("TEST2 2 ")
            }

      }


      //////////////////////////////////////////////////////////////

      protected _validateVisible():void{
            super._validateVisible();

            wemb.viewerProxy.sendNotification(ViewerProxy.NOTI_ACTIVE_THREE_LAYER, this.visible);

            // visible==true일때만 렌더링 루프 실행
            if(this.visible){
                  this._startRenderingLoop();
            }
            else {
                  this._stopRenderingLoop();
            }
      }


}






ViewerThreeLayer["default_properties"]= {
      "lock": false,
      "x": 0,
      "y": 0,

      "width": 100,
      "height": 100,
      "id": "",
      "name": "",
      "depth":1,
      "visible":true
}
