import MeshManager from "../../core/component/3D/manager/MeshManager";
import {THREE, THREEx} from "../../core/utils/reference";
import {ThreeLayerCore} from "./ThreeLayerCore";
import EditorStatic from "../../../editor/controller/editor/EditorStatic";

import {IWVComponent} from "../../core/component/interfaces/ComponentInterfaces";
import {LayerEvent} from "./Layer";
import {EXE_MODE} from "../data/Data";
import WV3DComponent from "../../core/component/3D/WV3DComponent";
import WVComponentEvent from "../../core/events/WVComponentEvent";
import {IWVDisplayObject} from "../../core/display/interfaces/DisplayInterfaces";
import WVMouseEvent from "../../core/events/WVMouseEvent";
import NWV3DComponent from "../../core/component/3D/NWV3DComponent";
import EditorPageComponent from "../components/EditorPageComponent";
import selectionBox from './selectionBox'
import DragAreaCalculator from "../../core/utils/DragAreaCalculator";
import Point from "../../core/geom/Point";
import {Rectangle} from "../../core/geom/interfaces/GeomInterfaces";
import SelectProxy from "../../../editor/model/SelectProxy";
/*
주요 관리 내용
1. scene에 mesh 추가 삭제
2. meshListMap에 mesh를 키로 연관 컴포넌트 추가 삭제
3. 자체 렌더링을 사용하는  컴포넌트 관리, 컴포넌트 id를 키로 컴포넌트 추가, 삭제
4. 라벨 관리, mesh.uuid로 라벨(DOM) 관리
*/
const THROTTLE_INTERVAL = 10;
export class EditThreeLayer extends ThreeLayerCore {

      public static readonly NAME = "EditThreeLayer";


      protected _func_call_onSelectedHandler: Function = null;
      protected _func_call_onUnSelectedHandler: Function = null;
      private _func_mouseDownSceneHandler:Function = null;
      private _func_mouseDownTransformControlHandler:Function = null;
      private _func_mouseUpTransformControlHandler:Function = null;
      private _func_controlChangeHandler:Function = null;
      private _func_componentDoubleClickHandler:Function = null;
      private _func_objectChangeHandler:Function = null;

      private _transfromControl_Start = null;
      private _transfromControl_end = null;
      private mouseovercolor = null;

      private selectionBox:selectionBox = null;
      private enableArea:boolean = false;
      private frustum:THREE.Frustum = null;
      private _func_mouseMoveHandler:Function = null;
      private _func_mouseUpHandler:Function = null;
      private _func_mouseDownHandler:Function = null;
      private _func_mouseLeaveHandler:Function = null;

      private shiftSelectArray : Array<NWV3DComponent> = [];

      private enableMouseMoveCall:boolean = true;

      set mouseOverOutlineColor(color){
            this.mouseovercolor = new THREE.Color(color);
      }
      get mouseOverOutlineColor(){
            return this.mouseovercolor;
      }
      constructor() {
            super();
            this._exeMode = EXE_MODE.EDITOR;
            this._func_call_onSelectedHandler = this.onSelected.bind(this);
            this._func_call_onUnSelectedHandler = this.onUnSelected.bind(this);
            this._func_mouseDownSceneHandler= this._onMouseDownScene.bind(this);
            this._func_mouseDownTransformControlHandler = this._onMouseDownTransformControl.bind(this);
            this._func_mouseUpTransformControlHandler = this._onMouseUpTransformControl.bind(this);
            this._func_controlChangeHandler = this._onControlChangeHandler.bind(this);
            this._func_componentDoubleClickHandler = this._onComponentDoubleClick.bind(this);
            this._func_objectChangeHandler = this.onObjectChange.bind(this);

            this._func_mouseMoveHandler = this._onMouseMoveHandler.bind(this);
            this._func_mouseUpHandler = this._onMouseUpHandler.bind(this);
            this._func_mouseDownHandler = this._onMouseDownHandler.bind(this);
            this._func_mouseLeaveHandler = this._onMouseLeaveHandler.bind(this);
            this.mouseovercolor = new THREE.Color(0xff0000);
      }

      protected _onDestroy() {
            this._removeEventListener();
            this._transformControls.dispose();
            this._func_call_onSelectedHandler = null;
            this._func_call_onUnSelectedHandler = null;
            super._onDestroy();
      }

      get TransformControls(){
            return this._transformControls;
      }
      protected _startLayer() {


            this._createThreeElements();
            //this._createBoxHelperControls();
            this._createTransformController();

            // 렌더러 실행
            this._startRenderingLoop();
            this._initEventListener();
      }


      protected _createTransformController() {
            ///////////////
            // 오브젝트 움직이는 트렌스폼컨트롤 생성
            this._transformControls = new THREE.TransformControls(this._camera, this._renderer.domElement);
            this._sceneHelper.add(this._transformControls);
            ///////////////
      }


      public addChildAt( component:IWVDisplayObject, index:number ):IWVDisplayObject {

            super.addChildAt(component, index);
            component.addEventListener(WVComponentEvent.SELECTED, this._func_call_onSelectedHandler);
            component.addEventListener(WVComponentEvent.UN_SELECTED, this._func_call_onUnSelectedHandler);
            component.addEventListener( WVMouseEvent.CLICK, this._func_mouseDownSceneHandler );
            component.addEventListener( WVMouseEvent.DOUBLE_CLICK, this._func_componentDoubleClickHandler );
            return component;

      }


      public removeChildAt( index: number ): IWVDisplayObject {
            let component:IWVDisplayObject = this.getChildAt(index);
            component.removeEventListener(WVComponentEvent.SELECTED, this._func_call_onSelectedHandler);
            component.removeEventListener(WVComponentEvent.UN_SELECTED, this._func_call_onUnSelectedHandler);
            component.removeEventListener( WVMouseEvent.CLICK, this._func_mouseDownSceneHandler );
            component.removeEventListener( WVMouseEvent.DOUBLE_CLICK, this._func_componentDoubleClickHandler );
            return super.removeChildAt(index);
      }

      public clear(){
            super.clear();
            this._removeEventListener();
            this._isDownTransformControls = false;
            this._transformControls.detach();
            this._initEventListener();
      }



      private _initEventListener() {
            /*요소 이외에 영역을 클릭하는 경우 선택한 요소를 취소 처리하기 위해 이벤트를 상위로 흘려줘야 한다.*/
            this._renderer.domElement.addEventListener('click', this._func_mouseDownSceneHandler, false);
            this._transformControls.addEventListener( "mouseDown", this._func_mouseDownTransformControlHandler, false  );
            this._transformControls.addEventListener( "mouseUp", this._func_mouseUpTransformControlHandler, false );
            this._transformControls.addEventListener("objectChange",this._func_objectChangeHandler, false);
            this._controls.component.addEventListener("change", this._func_controlChangeHandler);
            this._controls.component.addEventListener('start', function(){
                  this._threeElements.domEvents.enabled = false;
            }.bind(this));
            this._controls.component.addEventListener('end', function(){
                  this._threeElements.domEvents.enabled = true;
            }.bind(this));

            this._renderer.domElement.addEventListener('mousemove', this._func_mouseMoveHandler);
            this._renderer.domElement.addEventListener('mouseup', this._func_mouseUpHandler);
            this._renderer.domElement.addEventListener('mousedown', this._func_mouseDownHandler);
            this._renderer.domElement.addEventListener('mouseleave', this._func_mouseLeaveHandler);
      }

      private _removeEventListener(){
            this._renderer.domElement.removeEventListener('click', this._func_mouseDownSceneHandler, false);
            this._transformControls.removeEventListener( "mouseDown", this._func_mouseDownTransformControlHandler, false );
            this._transformControls.removeEventListener( "mouseUp", this._func_mouseUpTransformControlHandler, false );
            this._transformControls.removeEventListener("objectChange", this._func_objectChangeHandler, false);
            this._controls.component.removeEventListener("change", this._func_controlChangeHandler );

            this._controls.component.removeEventListener('start');
            this._controls.component.removeEventListener('end');

            this._renderer.domElement.removeEventListener('mousemove', this._func_mouseMoveHandler);
            this._renderer.domElement.removeEventListener('mouseup', this._func_mouseUpHandler);
            this._renderer.domElement.removeEventListener('mousedown', this._func_mouseDownHandler);
            this._renderer.domElement.removeEventListener('mouseleave', this._func_mouseLeaveHandler);

      }

      protected _onControlChangeHandler( e:any ){

            let vo = {
                  x: this._camera.position.x,
                  y: this._camera.position.y,
                  z: this._camera.position.z
            }

            if (window.wemb.editorFacade){
                  window.wemb.editorFacade.sendNotification(EditorStatic.CMD_SYNC_REAL_CAMERA_POSITION_TO_COMPONENT_POSITION, vo);
            }

      }


      private _onComponentDoubleClick( event ):void {
            window.wemb.editorFacade.sendNotification(EditorStatic.CMD_SHOW_EXTENSION_POPUP, event.target);
      }

      private _onMouseDownTransformControl():void {

            this._controls.enabled = false;
            this._isDownTransformControls = true;
      }

      private _onMouseUpTransformControl( event ):void {
            this._controls.enabled = true;
            window.wemb.editorFacade.sendNotification(EditorStatic.CMD_SYNC_LOCATION_AND_SIZE_ELEMENT_PROPS_TO_COMPONENT_PROPS);
            this._transfromControl_end = null;
            this._transfromControl_Start = null;
      }

      private _onMouseDownScene( event ){

            $("body").css("event");
            let evt = event.data ? event.data.originEvent : event;
            evt.stopPropagation();
            let instance:IWVComponent = null;
            if(event instanceof WVMouseEvent ){
                  evt.stopImmediatePropagation();
                  instance = event.target;
            }

            if(this._isDownTransformControls) {
                  this._isDownTransformControls = false;
                  return false;
            }

            if(instance) {
                  this.setTransformTarget(event.target);
                  if(!event.ctrlKey){
                        this.shiftSelectArray.forEach((instance)=>{
                              instance.toggleSelectedOutline(false);
                        });
                  }
                  this.dispatchEvent(new LayerEvent(LayerEvent.COMPONENT_MOUSE_DOWN, this,  evt,{
                        targetInstance:instance
                  }));
            } else {
                  if(!event.altKey){
                        this.shiftSelectArray.forEach((d)=>d.toggleSelectedOutline(false));
                        this.shiftSelectArray = [];
                        this.dispatchEvent(new LayerEvent(LayerEvent.LAYER_MOUSE_DOWN, this,  evt,{
                              targetInstance:this
                        }));
                  }
            }

      }

      //alt 키를 누르고 마우스 왼쪽 버튼 누르고 드래그 하면 영역 생성 한 후에 이를 바탕으로 camera의 정보를 가져와 frustum을 만들고 picking 처리를 해서 picking 된 것을 알아낸 후에
      //select proxy에 넣는다.
      // 후에 orthographic camera를 사용하게 되면 이 부분도 수정!
      private _onMouseDownHandler(event){

            if(this._isDownTransformControls) {
                  this._isDownTransformControls = false;
                  return false;
            }
            if(event.altKey && !this.enableArea){
                  let point = new Point(event.offsetX, event.offsetY);
                  var areaCalculator = DragAreaCalculator.getInstance();
                  areaCalculator.setStartPoint(point);
                  this._controls.enabled = false;
                  this.enableArea = true;
                  this.selectionBox = new selectionBox(this._camera, this._scene);
                  this.selectionBox.startPoint.set(
                        ( point.x / window.innerWidth ) * 2 - 1,
                        -( point.y / window.innerHeight ) * 2 + 1,
                        0.5
                  )
                  this.selectionBox.collection = [];
                  this.selectionBox.out_collection = [];

            }else{
                  this._controls.enabled = true;
            }

      }
      private _onMouseMoveHandler(event){
            if(!this.enableMouseMoveCall) return;
            this.enableMouseMoveCall = false;

            function groupOrMesh(comp){
                  let result = undefined;
                  for(let i = 0 ; i < comp.appendElement.children.length; i++){
                        if(comp.appendElement.children[i].isMesh || comp.appendElement.children[i].isGroup || comp.appendElement.children[i].type === "Scene"){
                              result = i;
                              break;
                        }
                  }
                  return result;
            }
            if(this._isDownTransformControls) {
                  this._isDownTransformControls = false;
                  return false;
            }
            if(event.altKey && this.enableArea){
                  this.selectionBox.collection = [];
                  this.selectionBox.out_collection = [];
                  let point = new Point(event.offsetX, event.offsetY);
                  var areaCalculator = DragAreaCalculator.getInstance();
                  let area:Rectangle = areaCalculator.getDragArea(point);
                  $(wemb.mainPageComponent.dragAreaLayer.dragView).css({
                        left : area.x,
                        top  : area.y,
                        width : area.width,
                        height : area.height,
                        border: "2px dotted red"
                  }).show();

                  this.selectionBox.endPoint.set(( point.x / window.innerWidth ) * 2 - 1,
                        -( point.y / window.innerHeight ) * 2 + 1,
                        0.5 );
                  this.frustum = this.selectionBox.createFrustum(this.selectionBox.startPoint, this.selectionBox.endPoint)
                  wemb.mainPageComponent.comInstanceList
                        .filter((comp)=> comp.layerName === 'threeLayer')
                        .forEach(function(comp){
                              const idx = groupOrMesh(comp);
                              if(idx!== undefined){
                                    var box = new THREE.Box3().setFromObject(comp.appendElement.children[idx]);
                                    var v = new THREE.Vector3();
                                    box.getCenter(v);
                                    if(this.frustum.containsPoint(v)){
                                          this.selectionBox.collection.push(comp);
                                    }else{
                                          this.selectionBox.out_collection.push(comp);
                                    }
                              }
                  }.bind(this));

                  if(this.selectionBox.collection.length){
                        //alt + shift 영역선택
                        //alt 영역선택은 바로 select proxy로 들어가지만 alt + shift는 mouseup 할 때 select proxy 로 들어간다.
                        if(event.shiftKey){
                              this.shiftSelectArray = Array.from(new Set(this.shiftSelectArray));
                              this.selectionBox.collection.forEach((instance)=>{
                                    if(!(this.shiftSelectArray.some((data)=> data.name === instance.name))){
                                          this.shiftSelectArray.push(instance);
                                    }
                              });
                              this.selectionBox.out_collection.forEach((instance)=>{
                                    let idx = this.shiftSelectArray.findIndex((data) => data.name === instance.name);
                                    if(idx > -1) {
                                          this.shiftSelectArray.splice(idx, 1);
                                          instance.toggleSelectedOutline(false);
                                          //alt + shift로 선택 안된 것 중에 이미 선택된 것이 있으면 선택 해제
                                          if(window.wemb.editorFacade.retrieveProxy(SelectProxy.NAME).currentPropertyManager.getInstanceList().some((data)=>data.name === instance.name)){
                                                window.wemb.editorFacade.sendNotification(EditorStatic.CMD_REMOVE_SELECTED, instance);
                                          }
                                    }
                              });
                              this.shiftSelectArray.forEach((instance)=>{
                                    instance.toggleSelectedOutline(true);
                              })
                        }else{
                              //alt 영역선택
                              if (window.wemb.editorFacade)
                                    window.wemb.editorFacade.sendNotification(EditorStatic.CMD_ADD_SELECTED_LIST, { list : this.selectionBox.collection, clear : true})
                        }
                  }
                  this.selectionBox.collection = [];
                  this.selectionBox.out_collection = [];
            }
            setTimeout(function(){
                  this.enableMouseMoveCall = true
            }.bind(this), THROTTLE_INTERVAL);
      }
      private _onMouseUpHandler(event){
            if(this.enableArea){
                  $(wemb.mainPageComponent.dragAreaLayer.dragView).hide();
                  this.enableArea = false;
                  this._controls.enabled = true;

                  if(event.shiftKey){
                        if (window.wemb.editorFacade)
                              window.wemb.editorFacade.sendNotification(EditorStatic.CMD_ADD_SELECTED_LIST, { list : this.shiftSelectArray, clear : false});
                        this.shiftSelectArray = [];
                  }
            }
      }

      private _onMouseLeaveHandler(event) {
            if(this.enableArea){
                  $(wemb.mainPageComponent.dragAreaLayer.dragView).hide();
                  this.enableArea = false;
                  if(event.shiftKey){
                        if (window.wemb.editorFacade)
                              window.wemb.editorFacade.sendNotification(EditorStatic.CMD_ADD_SELECTED_LIST, { list : this.shiftSelectArray, clear : false});
                        this.shiftSelectArray = [];
                  }
            }
      }

      /*
     call :
     EditAreaMainContainer.vue에서 noti_updateSelectedInfo() 메서드 호출
      */
      public setTransformTarget(targetComponent:WV3DComponent){
            this._transformControls.detach();
            if(this._transformControls && targetComponent){
                  this._transformControls.attach(targetComponent.appendElement);
                  if(!this._transformControls.showY){
                        this._transformControls.showY = true;
                  }
            }
      }


      public onObjectChange(event){
            var selectedList = window.wemb.currentFacade.retrieveProxy("SelectProxy").currentPropertyManager.getInstanceList();

            var moving_object_uuid = this._transformControls.object.uuid;
            if(this._transfromControl_Start === null && this._transfromControl_end  === null){
                  this._transfromControl_Start = this._transformControls.pointStart.clone();
                  this._transfromControl_end = this._transformControls.pointEnd.clone();
            }

            var pos, moving;
            if(selectedList[0] instanceof EditorPageComponent) return;
            selectedList.forEach(function(obj){
                  if(obj.getConnectionSize()){
                        Array.from(obj.connections)
                              .forEach(([key, value])=>{
                                    let line = wemb.mainPageComponent.getComInstanceByName(key.split('#$')[0]);
                                    let pointIdx = key.split('#$')[1];

                                    let target_pos = obj.appendElement.position.clone();
                                    line.appendElement.worldToLocal(target_pos);
                                    obj.outlineElement.geometry.computeBoundingSphere();
                                    line._localPoints[pointIdx]
                                          .set(target_pos.x, target_pos.y, target_pos.z)
                                          .add(new THREE.Vector3(obj.outlineElement.geometry.boundingSphere.center.x, obj.outlineElement.geometry.boundingSphere.center.y, obj.outlineElement.geometry.boundingSphere.center.z));
                                    line._syncLocalWorld();
                                    line._lineAdjustment();
                                    line._Update();
                              })
                  }
                  if(obj.appendElement.uuid !== moving_object_uuid && !(obj instanceof EditorPageComponent)){
                        pos = obj.appendElement.position.clone();
                        moving = this._transfromControl_end.clone().sub(this._transfromControl_Start);
                        obj.appendElement.position.copy(moving).add(pos);
                  }else{
                        if(obj.componentName === 'Line3DComponent'
                              || obj.componentName === 'Path3DComponent'
                              || obj.componentName === 'StreamLine3DComponent'
                              || obj.componentName === 'StreamPath3DComponent'
                        ){
                              //라인을 움직이면 모든 연결을 해제한다.
                              let targets = obj.getGroupPropertyValue('connection', 'targets');
                              let entries = Object.entries(targets);
                              for( let [key, value] of entries) window.wemb.mainPageComponent.getComInstanceByName(value).deleteConnection(key);
                              obj.setGroupPropertyValue('connection', 'targets', {});
                              obj.connections.clear();
                        }
                  }
            }.bind(this));

            this._transfromControl_Start.copy(this._transfromControl_end);
            this._transfromControl_end.copy(this._transformControls.pointEnd);
      }
      public onSelected(event:WVComponentEvent){
            // this.setTransformTarget(event.target.appendElement);
            this.setTransformTarget(event.target);
      }

      public onUnSelected(event:WVComponentEvent){
            this._transformControls.detach();
      }




      ////////////////////////////////////////////////////////////
      /*    그리드 새로 생성하기 외부에서 호출됨. */
      public recreateGridHelper(gridValue){

            let helper = MeshManager.gridHelper(parseInt(gridValue.x), 1, parseInt(gridValue.z), 1);
            let oldHelper = this._scene.getObjectByName("gridHelper");
            if (oldHelper != null) {
                  this._scene.remove(oldHelper);
                  //[ 메모리 할당 해제 ]
                  oldHelper.geometry.dispose();
                  oldHelper.material.dispose();
                  oldHelper = null;
            }
            helper.name = "gridHelper";
            this._scene.add(helper);
      }

      public getGridHelper(){
            return this._scene.getObjectByName("gridHelper");
      }


      public setTransformControlMode(mode:string){
            if(this._transformControls)
                  this._transformControls.setMode(mode);
      }
      ////////////////////////////////////////////////////////////






      ////////////////////////////////////////////////////////////

      protected _validateVisible():void{
            super._validateVisible();

            // visible==true일때만 렌더링 루프 실행
            if(this.visible){
                  this._startRenderingLoop();
            }
            else {
                  this._stopRenderingLoop();
            }
      }


      /*
      포커스가 다른 곳으로 가는 경우 커서 등이 동작하지 않게 처리 해야함.
       */
      public setLockControls(value:boolean){
            this._controls.enabled =value;


      }

      public updateTransformControl(){
            //this._transformControls.update();
      }

}








EditThreeLayer["default_properties"]= {
      "lock": false,
      "x": 0,
      "y": 0,
      "borderRadius": {
            topLeft:0,
            topRight:0,
            bottomLeft:0,
            bottomRight:0
      },
      "width": 100,
      "height": 100,
      "id": "",
      "name": "",
      "depth":1,
      "visible":true
}
