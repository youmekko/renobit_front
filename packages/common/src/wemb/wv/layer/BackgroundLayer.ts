import {IBackgroundElement} from "../../core/component/interfaces/ComponentInterfaces";
import {TwoLayerCore} from "./TwoLayerCore";

export class BackgroundLayer extends TwoLayerCore implements IBackgroundElement {

    constructor() {
        super();
    }

    public setBackgroundPropertyValue(propertyName: string, backgroundInfo: any) {
            // 기존과 달리 적용 할 배경 정보가 그대로 넘어 오게 됨.
          let targetBackgroundInfo = backgroundInfo;
          let $el =  $(this.element);

          if (targetBackgroundInfo) {
                  if(targetBackgroundInfo.using) {
                        if (targetBackgroundInfo.color) {
                              $el.css("backgroundColor", targetBackgroundInfo.color);
                        } else {
                              $el.css("backgroundColor", "rgba(255,255,255,0)");
                        }

                        if (targetBackgroundInfo.path) {
                              var serverPath = window.wemb.configManager.serverUrl;
                              $el.css("backgroundImage", 'url("' + serverPath + targetBackgroundInfo.path + '")');
                        } else {
                              $el.css("backgroundImage", "none");
                        }
                  }
          }

    }

    public getBackgroundPropertyValue(propertyName: string) {

    }

    public setBackgroundProperties(properties: any) {

    }

    public getBackgroundProperties(): any {

    }

}

BackgroundLayer["default_properties"] = {
    "lock": false,
    "x": 0,
    "y": 0,
    "borderRadius": {
          topLeft:0,
          topRight:0,
          bottomLeft:0,
          bottomRight:0
    },
    "id": "",
    "name": "",
    "depth": 1,
    "visible": true
}

