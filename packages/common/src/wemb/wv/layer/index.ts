import * as LayerInterfaces from './interfaces/LayerInterfaces'
import BackgroundLayer from './BackgroundLayer'
import ControlLayer from './ControlLayer'
import DragAreaLayer from './DragAreaLayer'
import EditThreeLayer from './EditThreeLayer'
import EditTwoLayer from './EditTwoLayer'
import * as Layer from './Layer'
import selectionBox from './selectionBox'
import ThreeLayerCore from './ThreeLayerCore'
import TwoLayerCore from './TwoLayerCore'
import ViewerThreeLayer from './ViewerThreeLayer'
import ViewerTwoLayer from './ViewerTwoLayer'


const layer = {
    LayerInterfaces,
    BackgroundLayer,
    ControlLayer,
    DragAreaLayer,
    EditThreeLayer,
    EditTwoLayer,
    Layer,
    selectionBox,
    ThreeLayerCore,
    TwoLayerCore,
    ViewerThreeLayer,
    ViewerTwoLayer
}

export default layer