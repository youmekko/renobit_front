import WVDOMComponent from "../../core/component/2D/WVDOMComponent";
import {IWVComponent} from "../../core/component/interfaces/ComponentInterfaces";

export default class ComponentArrangeHelper {
      //---------------------------------------------------------

      static execute(command:string, selectedListAry:Array<IWVComponent>, sourceListAry:Array<IWVComponent>){

            switch(command){

                  case "arrange_forward":
                        ComponentArrangeHelper.arrange_forward(selectedListAry, sourceListAry);
                        break;
                  case "arrange_front":
                        ComponentArrangeHelper.arrange_front(selectedListAry, sourceListAry);
                        break;
                  case "arrange_backward":
                        ComponentArrangeHelper.arrange_backward(selectedListAry, sourceListAry);
                        break;
                  case "arrange_back":
                        ComponentArrangeHelper.arrange_back(selectedListAry, sourceListAry);
                        break;
            }
      }

      static swapComponentIndex( compA:IWVComponent, compB:IWVComponent ){
            let newCompA = compA as WVDOMComponent;
            let newCompB = compB as WVDOMComponent;
            let tempIndex = newCompA.depth;
            newCompA.depth  = newCompB.depth;
            newCompB.depth  = tempIndex;
      }

      static sortMapByzIndex( sourceAry:Array<IWVComponent> ) {
            //return [...sourceMap.values()].sort( ( instanceA, instanceB ) => { return ( instanceA.z_index > instanceB.z_index ) ? 1 : -1; });
            return sourceAry.sort( ( instanceA:WVDOMComponent, instanceB:WVDOMComponent ) => { return ( instanceA.depth > instanceB.depth ) ? 1 : -1; });
      }


      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□

       sort처리
       z_index값 순서로 전체 컴포넌트 정렬
       1. 한 칸이동 처리
       -전체 컴포넌트 배열에서 선택된 컴포넌트의 index값 추출
       -index값을 기준으로 원하는 정렬에 따라 +-1 순서의 컴포넌트와 zindex를 swap
       2. 최상위 이동 처리
       -선택 컴포넌트의 index를 추출
       -전체 컴포넌트에서 index값을 이용해 원하는 정렬 옵션에 따라 전체컴포넌트배열 조정
       -재 정렬된 전체 컴포넌트 배열을 이용해 정렬옵션에 따른 z_index값 재설정

       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      static arrange_front(selectedListAry:Array<IWVComponent>, sourceListAry:Array<IWVComponent>){
            if( selectedListAry.length > 0 ){

                  let sortArray    = ComponentArrangeHelper.sortMapByzIndex(selectedListAry);
                  let totalComp    = ComponentArrangeHelper.sortMapByzIndex(sourceListAry);

                  let component    = sortArray[0] as WVDOMComponent;
                  // 선택 컴포넌트 중 가장 낮은 z_index값 기억
                  let startZIndex  = component.depth;
                  let start        = totalComp.indexOf(component);
                  sortArray.forEach((component)=>{
                        let compIndex = totalComp.indexOf(component);
                        totalComp.push(totalComp.splice( compIndex, 1)[0]);
                  });
                  let i;
                  for( i=start; i<totalComp.length;i++){
                        component = totalComp[i] as WVDOMComponent;
                        component.depth = startZIndex;
                        startZIndex++;
                  }
            }
      }


      static arrange_forward(selectedListAry:Array<IWVComponent>, sourceListAry:Array<IWVComponent>){
            if( selectedListAry.length > 0 ){

                  let sortArray    = ComponentArrangeHelper.sortMapByzIndex(selectedListAry);
                  let totalComp    = ComponentArrangeHelper.sortMapByzIndex(sourceListAry);
                  let maxIndex  = totalComp.length;
                  sortArray.forEach((component)=>{
                        if(totalComp.indexOf(component) < maxIndex - 1 ) {
                              let nIndex = totalComp.indexOf(component);
                              let swapComponent = totalComp[nIndex+1];
                              ComponentArrangeHelper.swapComponentIndex( component, swapComponent );
                        }
                  });
            }

      }



      static arrange_backward(selectedListAry:Array<IWVComponent>, sourceListAry:Array<IWVComponent>){
            if( selectedListAry.length > 0 ){
                  let sortArray    = ComponentArrangeHelper.sortMapByzIndex(selectedListAry);
                  let totalComp    = ComponentArrangeHelper.sortMapByzIndex(sourceListAry);
                  sortArray.forEach((component)=>{
                        if(totalComp.indexOf(component)>0){
                              let nIndex = totalComp.indexOf(component);
                              let swapComponent = totalComp[nIndex-1];
                              ComponentArrangeHelper.swapComponentIndex( component, swapComponent );
                        }
                  });
            }
      }


      static arrange_back(selectedListAry:Array<IWVComponent>, sourceListAry:Array<IWVComponent>){
            if( selectedListAry.length > 0 ) {

                  let sortArray    = ComponentArrangeHelper.sortMapByzIndex(selectedListAry).reverse();
                  let totalComp    = ComponentArrangeHelper.sortMapByzIndex(sourceListAry);
                  let component    = totalComp[0] as WVDOMComponent;
                  // 현재 사용하는 가장 낮은 값을 기억
                  let lowestIndex  = component.depth;

                  sortArray.forEach((component)=>{
                        let compIndex = totalComp.indexOf(component);
                        totalComp.unshift( totalComp.splice( compIndex, 1)[0] );
                  });
                  let i;
                  for( i = 0; i<totalComp.length; i++) {
                        component = totalComp[i] as WVDOMComponent;
                        component.depth = lowestIndex;
                        lowestIndex++;
                  }
            }
      }
}
