import WVDOMComponent from "../../core/component/2D/WVDOMComponent";
import {IWVComponent} from "../../core/component/interfaces/ComponentInterfaces";

export default class ComponentAlignHelper {
      //---------------------------------------------------------

      static execute(command:string, instanceListAry:Array<IWVComponent>){

            switch(command){
                  case "align_left":
                        ComponentAlignHelper.align_left(instanceListAry);
                        break;
                  case "align_right":
                        ComponentAlignHelper.align_right(instanceListAry);
                        break;
                  case "align_top":
                        ComponentAlignHelper.align_top(instanceListAry);
                        break;
                  case "align_bottom":
                        ComponentAlignHelper.align_bottom(instanceListAry);
                        break;

                  case "align_center" :
                        ComponentAlignHelper.align_center(instanceListAry);
                        break;

                  case "align_middle" :
                        ComponentAlignHelper.align_middle(instanceListAry);
                        break;
            }
      }
      static align_left(instanceListAry:Array<IWVComponent>){

            if(instanceListAry.length==0)
                  return;




            let min = 50000;
            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  let instanceLeft = instance.x;

                  if(min>instanceLeft){
                        min = instanceLeft;
                  }


            });


            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  instance.x = min;
            })

      }


      static align_right(instanceListAry:Array<IWVComponent>){
            if(instanceListAry.length<=0)
                  return;

            let max = 0;
            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  let instanceRight = instance.x+instance.width;
                  if(max<instanceRight){
                        max = instanceRight;
                  }
            });
            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  let tempLeft = max - instance.width;
                  instance.x = tempLeft;
            })
      }



      static align_top(instanceListAry:Array<IWVComponent>){
            if(instanceListAry.length<=0)
                  return;


            let min = 50000;
            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  let instanceTop = instance.y;

                  if(min>instanceTop){
                        min = instanceTop;
                  }
            });

            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  instance.y = min;
            })
      }


      static align_bottom(instanceListAry:Array<IWVComponent>){
            if(instanceListAry.length<=0)
                  return;

            let max = 0;
            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  let instanceBottom = instance.y+instance.height;
                  if(max<instanceBottom){
                        max = instanceBottom;
                  }
            });

            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  instance.y = max - instance.height;
            })
      }



      /*
  실행 순서
	1. 시작 위치 구하기
	2. 마지막 위치 구하기 = left + width
	3. 중간 지점 구하기
	4. 중간 지점을 기준으로 컴포넌트 위치 설정하기  = 중간지점 - (컴포넌트.width)/2
   */

      static align_center(instanceListAry:Array<IWVComponent>){
            if(instanceListAry.length>=1){
                  let max = 0;
                  instanceListAry.forEach((instance:WVDOMComponent)=>{
                        let instanceLeft = instance.x+instance.width;
                        if(max<instanceLeft){
                              max = instanceLeft;
                        }
                  });

                  let min = 50000;
                  instanceListAry.forEach((instance:WVDOMComponent)=>{
                        let instanceLeft = instance.x;

                        if(min>instanceLeft){
                              min = instanceLeft;
                        }
                  });



                  let centerLeft = min+((max - min)/2);
                  instanceListAry.forEach((instance:WVDOMComponent)=>{
                        let width = instance.width;
                        let left = centerLeft - (width/2);
                        instance.x= left;
                  })
            }
      }



      /*
	실행 순서
	    1. 시작 위치 구하기
	    2. 마지막 위치 구하기 = left + width
	    3. 중간 지점 구하기
	    4. 중간 지점을 기준으로 컴포넌트 위치 설정하기  = 중간지점 - (컴포넌트.width)/2
	 */
      static align_middle(instanceListAry:Array<IWVComponent>){
            if(instanceListAry.length>=1){
                  let max = 0;
                  instanceListAry.forEach((instance:WVDOMComponent)=>{
                        let instanceBottom = instance.y+instance.height;
                        if(max<instanceBottom){
                              max = instanceBottom;
                        }
                  });

                  let min = 50000;
                  instanceListAry.forEach((instance:WVDOMComponent)=>{
                        let instanceTop = instance.y;

                        if(min>instanceTop){
                              min = instanceTop;
                        }
                  });

                  let centerTop = min+((max - min)/2);
                  instanceListAry.forEach((instance:WVDOMComponent)=>{
                        let height = instance.height;
                        let top = centerTop - (height/2);
                        instance.y = top;
                  })
            }
      }

}
