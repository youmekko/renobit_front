import ComponentAlignHelper from './ComponentAlignHelper'
import ComponentArrangeHelper from './ComponentArrangeHelper'
import ComponentDistributeHelper from './ComponentDistributeHelper'
import * as ComponentResourceLoader from './ComponentResourceLoader'
import ComponentSizeHelper from './ComponentSizeHelper'

const helpers = {
    ComponentAlignHelper,
    ComponentArrangeHelper,
    ComponentDistributeHelper,
    ComponentResourceLoader,
    ComponentSizeHelper
}

export default helpers