import WVDOMComponent from "../../core/component/2D/WVDOMComponent";
import {IWVComponent} from "../../core/component/interfaces/ComponentInterfaces";
import {Rectangle} from "../../core/geom/interfaces/GeomInterfaces";

export default class ComponentDistributeHelper {
      //---------------------------------------------------------

      static execute(command:string, selectedListAry:Array<IWVComponent>, sourceListAry:Array<IWVComponent>, size:Rectangle){

            switch(command){

                  case "distribute_horizontally":
                        ComponentDistributeHelper.distribute_horizontally(selectedListAry, sourceListAry, size );
                        break;
                  case "distribute_vertically":
                        ComponentDistributeHelper.distribute_vertically(selectedListAry, sourceListAry,  size );
                        break;

            }
      }



      static sortHorizontalPosition( sourceListAry:Array<IWVComponent> ) {
            return sourceListAry.sort((compA:WVDOMComponent, compB:WVDOMComponent)=>{
                  let ax = compA.x;
                  let bx = compB.x;
                  return ax > bx ? 1 :-1;
            });
      }
      static sortVerticalPosition( sourceListAry:Array<IWVComponent> ){
            return sourceListAry.sort((compA:WVDOMComponent, compB:WVDOMComponent)=>{
                  let ay = compA.y;
                  let by = compB.y;
                  return ay > by ? 1 :-1;
            });
      }


      /**
       *    1. x위치에 따른 간격 정렬
       *    2. 선택한 객체들 중 가장 넓은 간격을 추출.
       *    3. 나머지 컴포넌트들이 그 간격안에 들어 갈 수 있는지를 추출
       *    4. 컴포넌트들이 들어 갈 수 없다면 다음 2번을 반복
       *    5. 추출한 간격에 나머지 컴포넌트를 위치 시킴.
       * */
      static distribute_horizontally(selectedListAry:Array<IWVComponent>, sourceListAry:Array<IWVComponent>, size:Rectangle){
            if(selectedListAry.length>=1) {

                  let limitSize = size.width || 1000;
                  let sortPositionArr = ComponentDistributeHelper.sortHorizontalPosition(selectedListAry);
                  let max = sortPositionArr.length;
                  let startComp = sortPositionArr[0] as WVDOMComponent;
                  let endComp = sortPositionArr[max-1] as WVDOMComponent;
                  // 선택 컴포넌트 넓이 참조
                  let startX    = (startComp.x + startComp.width);
                  if( endComp.x + endComp.width > limitSize ){
                        endComp.x = limitSize - endComp.width;
                  }

                  let sortAreaWidth = (endComp.x - startX ) ;
                  let selectCompWidth = 0;
                  let i, comp;
                  for( i=1; i<max-1;i++){
                        comp = sortPositionArr[i];
                        selectCompWidth += comp.width;
                  }

                  // 간격 값 추출
                  let overlayGap = (sortAreaWidth-selectCompWidth)/(max-1);
                  let prevComp, nValue;
                  let repeat = overlayGap > 0 ? max-1 : max;
                  for( i=1; i<repeat;i++){
                        comp        = sortPositionArr[i];
                        prevComp    = sortPositionArr[i-1];
                        nValue      = prevComp.x + prevComp.width;
                        if(overlayGap>0){
                              nValue += overlayGap;
                        }

                        comp.x = ( nValue >= limitSize ) ? limitSize - comp.width: nValue;

                  }
                  // 시작 컴포넌트와 마지막 컴포넌트 간격은 이동하지 않는 다는 가정으로 정렬
            }
      }

      static distribute_vertically(selectedListAry:Array<IWVComponent>, sourceListAry:Array<IWVComponent>, size:Rectangle){
            if(selectedListAry.length>=1) {

                  let limitSize = size.height || 600;
                  let sortPositionArr = ComponentDistributeHelper.sortVerticalPosition(selectedListAry);
                  let max = sortPositionArr.length;
                  let startComp = sortPositionArr[0] as WVDOMComponent;
                  let endComp = sortPositionArr[max-1] as WVDOMComponent;
                  // 선택 컴포넌트 넓이 참조
                  let startY    = (startComp.y + startComp.height);
                  if( endComp.y + endComp.height > limitSize ){
                        endComp.y = limitSize - endComp.height;
                  }
                  let sortAreaHeight = (endComp.y) - startY;

                  let selectCompHeight = 0;
                  let i, comp;
                  for( i=1; i<max-1;i++){
                        comp = sortPositionArr[i];
                        selectCompHeight += comp.height;
                  }

                  // 간격 값 추출
                  let overlayGap = (sortAreaHeight-selectCompHeight)/(max-1);
                  let prevComp, nValue;
                  let repeat = overlayGap > 0 ? max-1 : max;
                  for( i=1; i<repeat;i++){
                        comp        = sortPositionArr[i];
                        prevComp    = sortPositionArr[i-1];
                        nValue      = prevComp.y + prevComp.height;
                        if(overlayGap>0){
                              nValue += overlayGap;
                        }
                        comp.y = ( nValue >= limitSize ) ? limitSize - comp.height: nValue;
                  }
                  // 시작 컴포넌트와 마지막 컴포넌트 간격은 이동하지 않는 다는 가정으로 정렬
            }
      }

}
