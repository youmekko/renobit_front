import Vue from "vue";
import WVComponent from "../../core/component/WVComponent";
import ViewerProxy from "../../../viewer/model/ViewerProxy";
import {WVComponentScriptEvent} from "../../core/events/WVComponentEventDispatcher";
import WVComponentEvent from "../../core/events/WVComponentEvent";
import NLoaderManager from "../../core/component/3D/manager/NLoaderManager";

/*
순차적 컴포넌트 리소스 호출
 */
export class ComponentResourceLoader {
      private _comInstanceList:WVComponent[] = [];
	private _isLoading:boolean =false;

      private _currentIndex = 0;
      private _currentComponent:WVComponent = null;

      // 컴포넌트에서 발생한 이벤트 받기
      private _$resourceEventBus = new Vue();
      private _$loaderEventBus:Vue = new Vue();

      private _funcCompletedLoading:Function = null;

      constructor(){
            this._funcCompletedLoading = this._onCompletedLoading.bind(this);
            this.reset();
      }
	public destroy(){
            this._isLoading=false;
            this._currentIndex=0;
	      this._comInstanceList = null;
            if(this._$resourceEventBus) {

                  this._$resourceEventBus.$off();
                  this._$resourceEventBus = null;

            }

            this._funcCompletedLoading = null;

            if(this._$loaderEventBus){
                  this._$loaderEventBus.$off();
                  this._$loaderEventBus = null;
            }

      }



      public $on(eventName:string, func:Function){
            this._$loaderEventBus.$on(eventName, func);
      }


      /*
      사용할 수 잇게 기존 내용을 모두 삭제 하고 초기화 상태로 만들기
       */
      public reset(){

            this._isLoading=false;
            this._currentIndex=0;
            this._comInstanceList = [];
            if(this._$resourceEventBus)
                  this._$resourceEventBus.$off();
            else {
                  this._$resourceEventBus = new Vue();
            }
            this._$resourceEventBus.$on(WVComponentEvent.LOADED_RESOURCE, this._funcCompletedLoading);

            if(this._$loaderEventBus){
                  this._$loaderEventBus.$off();
            }else {
                  this._$resourceEventBus = new Vue();
            }


      }


      /*
      컴포넌트 목록에서 리소스를 사용하는 컴포넌트만 걸러내어 맵으로 만들기.
       */
      private _createResourceMap(comInstanceList:WVComponent[]){
            if(this._isLoading==false) {

                  let $resourceEventBus = new Vue();
                  let comInstanceListMap: Map<string, WVComponent> = new Map();


                  // 리소스를 사용하는 컴포넌트만 알아내기.
                  comInstanceList.forEach((instance: WVComponent) => {
                        if (instance.isResourceComponent) {
                              this._comInstanceList.push(instance);
                        }
                  })
            }else {
                  console.warn("컴포넌트 리소스 로딩시 Resource를 변경 할 수 없습니다.");
            }
      }

      private async _firstLoading(){
            if(this._comInstanceList.length<=0){
                  this._completedAllLoading();
                  return;
            }
            this._isLoading = true;
            this._currentIndex=0;
            // this._loadResourceAt(this._currentIndex);
            // this._comInstanceList.forEach(async (comInstance)=>{
            //       if (comInstance) {
            //             //console.log("리소스 로딩 시작 ", comInstance.name, this._comInstanceList.length, this._currentIndex + 1);
            //             comInstance.setResourceBus(this._$resourceEventBus);
            //             await comInstance.startLoadResource();
            //       } else {
            //             console.warn("컴포넌트 인스턴스가 존재하지 않습니다.");
            //       }
            // })
            const promises = this._comInstanceList.map(this._loading.bind(this));
            await Promise.all(promises);
      }
      private async _loading(comInstance){
            try {

                  if (comInstance) {
                        //console.log("리소스 로딩 시작 ", comInstance.name, this._comInstanceList.length, this._currentIndex + 1);
                        comInstance.setResourceBus(this._$resourceEventBus);
                        await comInstance.startLoadResource();
                  } else {
                        console.warn("컴포넌트 인스턴스가 존재하지 않습니다.");
                  }
            }catch(error){
                  console.log("### resource load error ", comInstance.name, error );
                  this._$loaderEventBus.$emit(ComponentResourceLoaderEvent.RESOURCE_LOADED, comInstance);
            }
      }
      private async _loadResourceAt(index){
            let comInstance = this._comInstanceList[this._currentIndex];
            try {

                  if (comInstance) {
                        //console.log("리소스 로딩 시작 ", comInstance.name, this._comInstanceList.length, this._currentIndex + 1);
                        comInstance.setResourceBus(this._$resourceEventBus);
                        await comInstance.startLoadResource();
                  } else {
                        console.warn("컴포넌트 인스턴스가 존재하지 않습니다.");
                  }
            }catch(error){
                  console.log("### resource load error ", comInstance.name, error );
                  this._$loaderEventBus.$emit(ComponentResourceLoaderEvent.RESOURCE_LOADED, comInstance);
            }
      }

      /*
      컴포넌트에서 리소스가 로드되면 호출
       */

      private _onCompletedLoading(comInstance:WVComponent){
            if(comInstance) {
                  //console.log("리소스 로딩 완료 ", this._comInstanceList.length, this._currentIndex+1);
            }else {
                  console.warn("리소스 완료시 반드시 컴포넌트 인스턴스를 넘겨줘야 합니다.");
            }


            this._$loaderEventBus.$emit(ComponentResourceLoaderEvent.RESOURCE_LOADED, comInstance);

            this._currentIndex++;
            if(this._currentIndex>=this._comInstanceList.length){
                  this._completedAllLoading();
            }else {
                  // this._loadResourceAt(this._currentIndex);
            }

      }

      private _completedAllLoading(){
            console.log("모든 리소스 로딩 완료", this._comInstanceList.length+"/"+this._currentIndex);
            this._$loaderEventBus.$emit(ComponentResourceLoaderEvent.COMPLETED_ALL);
            this.reset();
      }




      public startResourceLoading(comInstanceList:WVComponent[]){
            if(this._isLoading==false) {
                  this._createResourceMap(comInstanceList);
                  this._firstLoading();
            }else {

            }
      }


      public stopResourceLoading(){
            if(this._isLoading==true){
                  this.reset();
            }
      }

      /* 배치된 컴포넌트 중 3D 컴포넌트의 리소스 정보만 빼내 THREE.ObjLoader2의 실행 데이터를 만드는 함수*/
      public prepObjLoad(instanceList :Array<WVComponent>){
            let resourceCompArray = instanceList
                  .map((comp)=>{
                        if(comp.layerName === 'threeLayer' && comp.componentName !== 'NJsonLoaderComponent'){
                              let resource_information = comp.componentName === 'NObjLoaderComponent' ? comp.selectItem : (comp.hasOwnProperty('_resourceInfo') ? comp._resourceInfo : null);
                              if(resource_information === null || typeof resource_information !== 'object') return null;
                              return {
                                    name : resource_information.name,
                                    mapPath : resource_information.mapPath,
                                    path : resource_information.path.includes('http') ? resource_information.path : wemb.configManager.serverUrl + resource_information.path
                              }
                        }else{
                              return null;
                        }
                  })
                  .filter((data)=> data !== null);

            // 같은 Rack이어도 자산 정보에 따라 resource가 바뀔수도 있으므로, 리소스의 경로 값을
            // 이용하여 중복을 제거한다.
            let filteredResourceCompArray = resourceCompArray.filter((data, i)=>{
                  return resourceCompArray.findIndex((data2, j)=>{
                        return (data.path === data2.path);
                  }) === i;
            });
            // ObjLoader2에서 사용할 prep data를 만든다.
            return filteredResourceCompArray.map((data)=>{
                  if(!NLoaderManager.hasLoaderPool(data.path)){
                        let prepData = new THREE.LoaderSupport.PrepData(data.path);
                        prepData.addResource(new THREE.LoaderSupport.ResourceDescriptor(data.path, 'OBJ'));
                        prepData.setLogging(false, false);
                        // 과거에는 오브젝트 로드 -> 텍스터 로드 순으로 진행되었기 때문에
                        // 따로 경로를 연결할 필요가 없지만, 지금은 그 과정이 분리되었기 때문에
                        // 로드 된 오브젝트와 그 텍스처에 맞는 map path를 연결해두어야 한다.
                        NLoaderManager.mapPathDictionary[prepData.modelName] = data.mapPath;
                        return prepData;
                  }else{
                        return null;
                  }
            }).filter((prep)=> prep !== null);
      }

      /* 텍스처 선 로드 Pool을 만들어 놓는 함수 */
      public createTexturePool(){
            let keys = Array.from(NLoaderManager.getMeshLoadedPoolKeys()).filter((key)=> key.includes(".obj"));
            let texturePromises = keys
                  .map((key)=>{
                        let comp = NLoaderManager.getMeshLoadedPool(key);
                        let keyArr = comp.children.map((child)=>{
                              if(child.name !== 'base' && child.name !== 'area')
                                    return NLoaderManager.mapPathDictionary[key] + child.name + '.png';
                              else
                                    return null;
                        }).filter((data)=> data !== null);
                        return [...keyArr]
                  })
                  .reduce((a,b) => a.concat(b), [])
                  .map((address)=>{
                        let path;
                        if(address.indexOf("http")<= -1){
                              path =  wemb.configManager.serverUrl  + address;
                        }
                        return NLoaderManager.loadTexture(path).then((texture)=>{
                              // console.log("텍스처 로드 완료~!");
                              return texture;
                        })
                  });
            return Promise.all(texturePromises);
      }

}


export class ComponentResourceLoaderEvent {
      public static readonly RESOURCE_LOADED: string = "resourceLoaded";
      public static readonly COMPLETED_ALL: string = "completedAll";
}
