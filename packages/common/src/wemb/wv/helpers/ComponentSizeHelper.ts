import WVDOMComponent from "../../core/component/2D/WVDOMComponent";
import {IWVComponent} from "../../core/component/interfaces/ComponentInterfaces";

export default class ComponentSizeHelper {
      //---------------------------------------------------------

      static execute(command:string, instanceListAry:Array<IWVComponent>){

            switch(command){
                  case "match_width":
                        ComponentSizeHelper.match_width(instanceListAry);
                        break;
                  case "match_height":
                        ComponentSizeHelper.match_height(instanceListAry);
                        break;

            }
      }

      // max_width으로 설정
      static match_width(instanceListAry:Array<IWVComponent>){
            if(instanceListAry.length==0)
                  return;

            let max = 0;
            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  let instanceWidth = instance.width;

                  if(max<instanceWidth){
                        max = instanceWidth;
                  }
            });


            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  instance.width = max;
            })

      }


      static match_height(instanceListAry:Array<IWVComponent>){
            if(instanceListAry.length==0)
                  return;

            let max = 0;
            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  let instanceHeight = instance.height;

                  if(max<instanceHeight){
                        max = instanceHeight;
                  }
            });


            instanceListAry.forEach((instance:WVDOMComponent)=>{
                  instance.height = max;
            })
      }



}
