import { LoadingEvent } from './LoadingEvent'
import { PageComponentEvent } from './PageComponentEvent'

const events = {
    LoadingEvent,
    PageComponentEvent
}

export default events