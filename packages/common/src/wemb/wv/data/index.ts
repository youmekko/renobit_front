import * as Data from './Data'
import { virtualData } from './VirtualData'

const data = {
    Data,
    virtualData
}

export default data