	export enum EXE_MODE {
		EDITOR="editor",
		VIEWER="viewer"
	}
	export enum ERROR_TYPE {
		PARSING_ERROR = "parsingError",
		SERVER_ERROR = "serverError"
	}

	export enum EDITOR_APP_STATE {
		READY_START="readyStart",
		CONFIG_LOADING="configLoading",
		READY_COMPLETED="readyCompleted",
		LOCALE_CHANGED="localeChanged"
	}




	export class ErrorVO {
		public errorID?:string;
		public type?:ERROR_TYPE;
		public message:string|object;
	}


	export enum CONFIG_MODE {
		REMOTE="remote",
		LOCAL="local"
	}
