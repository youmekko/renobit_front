namespace WV {
	export namespace virtualData {

		export interface  IVirtualContainer {

			getPropertyInfo();
			getProperties();
		}


		export class VirtualTwoContainer implements IVirtualContainer {
			constructor(){

			}

			getProperties(){

			}

			getPropertyInfo(){

			}
		}

		export class VirtualThreeContainer implements IVirtualContainer {
			constructor(){

			}

			getProperties(){

			}

			getPropertyInfo(){

			}
		}
	}
}