import components from './components'
import data from './data'
import events from './events'
import extension from './extension'
import helper from './helpers'
import layer from './layer'
import managers from './managers'
import page from './page'
import vue from './vue'

const wv = {
    components,
    data,
    events,
    extension,
    helper,
    layer,
    managers,
    page,
    vue
}

export default wv