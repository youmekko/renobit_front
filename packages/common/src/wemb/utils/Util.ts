import http, {AxiosResponse} from 'axios';
import Vue from "vue"
	export class ObjectUtil {
		static generateID() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
				var r = Math.random() * 16 | 0,
					v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		}

		static copyToClipboard( dataStr:string ){
			if(!dataStr){return;}
			let IE = ObjectUtil.detectIE();
			if( IE ){
				window.clipboardData.setData("Text", dataStr);
			}else{
				let handler:any = function(event:any) {
					event.clipboardData.setData('text/plain', dataStr);
					event.preventDefault();
					document.removeEventListener('copy', handler, true);
				}

				document.addEventListener('copy', handler, true);
				document.execCommand('copy');
			}
		}

		static detectIE(){
			var ua = window.navigator.userAgent;
			// Test values; Uncomment to check result …

			// IE 10
			// ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

			// IE 11
			// ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

			// Edge 12 (Spartan)
			// ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

			// Edge 13
			// ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

			var msie = ua.indexOf('MSIE ');
			if (msie > 0) {
				// IE 10 or older => return version number
				return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
			}

			var trident = ua.indexOf('Trident/');
			if (trident > 0) {
				// IE 11 => return version number
				var rv = ua.indexOf('rv:');
				return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
			}

			var edge = ua.indexOf('Edge/');
			if (edge > 0) {
				// Edge (IE 12+) => return version number
				return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
			}

			// other browser
			return false;
		}
	}

	export class ArrayUtil {
		constructor(){
		}

		/*
		info = {
			property
			value
		}
		 */
		static getItem(ary, propertyName, value){

			for(var i=0;i<ary.length;i++){
				var item = ary[i];
                        try {
                              var propertyValue = item[propertyName];
                              if (propertyValue == value) {
                                    return item;
                              }
                        }catch(error){
                              console.log("ArrayUtil.getItem error");
                        }
			}
			return null;
		}
		static getItemIndex(ary, propertyName, value ){
			for(var i=0;i<ary.length;i++){
				var item = ary[i];
				if(item) {
                              try {
                                    var propertyValue = item[propertyName];
                                    if (propertyValue == value) {
                                          return i;
                                    }
                              }catch(error){
                                    console.log("ArrayUtil.getItemIndex error");
                              }
                        }

			}
			return -1;
		}

		static has(ary, propertyName, value){

			let item = ArrayUtil.getItem(ary, propertyName, value);
			return item==null ? false : true;
		}

		static delete(ary, propertyName, value){
			let index = ArrayUtil.getItemIndex(ary, propertyName, value);
			if(index!=-1){
				ary.splice(index, 1);
			}
		}


		static clear(ary){
			while(ary.length){
				ary.splice(0,1);
			}

		}
	}

	export class ValidationUtil {
	      constructor() {

            }

            /**
             * 문자열이 길이값을 초과여부 확인
             * 1. 객체를 받을때(다중 확인)
             *    - target이 객체 : {label : value} 형식
             *    - value 에는 비교할 길이값
             * 2. 문자열을 받을때(하나 확인)
             *    - target이 label을 의미
             *    - value가 체크할 문자열
             *    - length가 길이값
             * @param target
             * @param value
             * @param length
             */
            static exeedMaxLength(target, value, length) {
	            if (!this.isObject(target)) {
	                  var obj = {};
	                  obj[target] = value;
                        target = obj;
	            } else {
	                  length = value;
                  }

	            return Object.keys(target).find(column => {
	                  return target[column].trim().length > length;
                  });
            }

            static isObject(obj) {
	            return typeof obj === "object";
            }
      }
