var path = require('path')
const vueLoaderConfig = require('./vue-loader.conf');

var root_path = path.resolve(__dirname, './bundle');

var webpack_config = {
    context: path.resolve(__dirname, '.'),
    entry: {
        app: ['babel-polyfill', '../src/main.ts']
    },
    output: {
        path: root_path,
        publicPath: './',
        libraryTarget: 'umd',
        filename: 'common/js/[name].js',
        chunkFilename: 'common/js/[id].js'
    },
    resolve: {
        extensions: ['.js', '.vue', '.json', '.ts', '.css'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@': path.resolve(__dirname, '../src'),
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: vueLoaderConfig
            },
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader',
                    options: {
                        appendTsSuffixTo: [/\.vue$/],
                        transpileOnly: true
                    }
                }
            }
        ]
    }
};

module.exports = webpack_config
