{
    "config": {
        "mode": "remote",
        "remote": {
            "server_path": "http://192.168.10.152:6277/renobit",
            "data_manager_socket_url": "ws://127.0.0.1",
            "data_manager_socket_port": "18530"
        },
        "loading": {
            "text_display": false
        },
        "viewer": {
            "main_menu_display": true,
            "page_history_count_max": 10
        },
        "languages": [{
            "label": "KOREA",
            "locale": "ko-KR",
            "user_locale": "KOR"
        }, {
            "label": "ENGLISH",
            "locale": "en-US",
            "user_locale": "ENG"
        }, {
            "label": "CHINA",
            "locale": "zh-CN",
            "user_locale": "CNS"
        }, {
            "label": "HONGKONG",
            "locale": "zh-HK",
            "user_locale": "CNT"
        }],

        "fonts": [{
            "label": "NotoSansCJKkr-B",
            "value": "NotoSansCJKkrBold"
        }, {
            "label": "NotoSansCJKkr-M",
            "value": "NotoSansCJKkrMedium"
        }, {
            "label": "NotoSansCJKkr-R",
            "value": "NotoSansCJKkrRegular"
        }]
    }
}



loading
	- text_display
		에디터/뷰어에서  컴포넌트 정보  또는 라이브러리 정보, 페이지 정보를 읽을때
		리스트 방식으로 로딩 정보를 출력할 것인지 유무 판단.
viewer
	- main_menu_display
		뷰어의 메인 메뉴 활성화 유무
	- page_history_count_max
		스크립트 또는 메뉴 등의 컴포넌트에 의해서 페이지가 이동하게 되는데
		이때 페이지 이동 정보가 히스토리에 담기게 됨.
		이때 히스토리 최대 개수를 설정하는 변수
		히스트리는 항상 최신 히스토리를 기준으로 하며
		최신 히스토리를 기준으로 + page_history_count_max 만큼 저장됨.
