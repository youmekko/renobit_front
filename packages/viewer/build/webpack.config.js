var _ = require('lodash');
var fs = require('fs');
var path = require('path')
var webpack = require('webpack')
const vueLoaderConfig = require('./vue-loader.conf');
const utils = require('./utils');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const childProcess = require('child_process');

var project_path = path.resolve(__dirname);
var root_path = path.resolve(__dirname, './bundle');
var build_path = path.resolve(root_path, './viewer');

var project_config = JSON.parse(fs.readFileSync(path.resolve(project_path, '../setup/index.json'), 'utf8'));
process.env.root_path = path.resolve(__dirname, './bundle');
var path_config = {
    production: {
        index: {
            path: './jsp/viewer/viewer.jsp',
            options: {
                jsp: '<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>',
                custom: project_config.css,
                trialExpire: '<%String trialExpire = (String) session.getAttribute("EXPIRATION_DATE_APPROACH");%><%if(trialExpire != null && trialExpire.equals("Y")){ %><%@ include file="../lib/licenseNoti.jsp" %><%}%>'
            }
        }
    },
    development: {
        index: {
            path: 'index.html',
            options: {
                custom: project_config.css
            }
        }
    }
}

var copy_plugin_config = [
    {
        from: path.resolve(__dirname, '../common'),
        to: path.resolve(build_path, 'common'),
        ignore: ['.*']
    },
    {
        from: path.resolve(project_path, '../setup/index.json'),
        to: path.resolve(root_path, 'custom/packs/installed_packs.json')
    }
];


var webpack_config = {
    watch: process.env.NODE_ENV === 'development',
    context: path.resolve(__dirname, '.'),
    entry: {
        app: process.env.NODE_ENV !== 'development' ? ['babel-polyfill', '../src/main.ts'] : ['babel-polyfill', 'core-js/stable', 'regenerator-runtime/runtime', "babel-runtime/regenerator", "webpack-hot-middleware/?reload=true", '../src/main.ts']
    },
    output: {
        path: root_path,
        publicPath: './',
        filename: process.env.NODE_ENV === 'development' ? 'viewer/js/[name].js' : 'viewer/js/[name].js',
        chunkFilename: process.env.NODE_ENV === 'development' ? 'viewer/js/[id].js' : 'viewer/js/[id].js'
    },
    resolve: {
        extensions: ['.js', '.vue', '.json', '.ts', '.css'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@': path.resolve(__dirname, '../src'),
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: vueLoaderConfig
            },
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader',
                    options: {
                        appendTsSuffixTo: [/\.vue$/],
                        transpileOnly: true
                    }
                }
            },

            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: 'viewer/common/img/[name].[hash:7].[ext]'
                }
            },
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: 'viewer/common/media/[name].[hash:7].[ext]'
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: 'viewer/common/fonts/[name].[hash:7].[ext]'
                }
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            config: path_config[process.env.NODE_ENV].index.options,
            filename: path_config[process.env.NODE_ENV].index.path,
            template: path.resolve(__dirname, '../index.ejs'),
            inject: true
        }),
        new ExtractTextPlugin({
            filename: 'viewer/common/css/[name].css',
            allChunks: true,
        }),
        new OptimizeCSSPlugin({
            cssProcessorOptions: { safe: true, map: { inline: false } }
        }),
        new CopyWebpackPlugin(copy_plugin_config),
        new webpack.HashedModuleIdsPlugin(),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks(module) {
                return (
                    module.resource &&
                    /\.js$/.test(module.resource) &&
                    module.resource.indexOf(
                        path.join(__dirname, '../node_modules')
                    ) === 0
                )
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'manifest',
            minChunks: Infinity
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'app',
            async: 'vendor-async',
            children: true,
            minChunks: 3
        }),
        new webpack.DefinePlugin({
            VERSION: JSON.stringify(require("../package.json").version),
            MODE : JSON.stringify(process.env.mode)
        }),
        new webpack.BannerPlugin({
              banner: `
                Build Date : ${new Date().toLocaleString()}
                Commit Version : ${childProcess.execSync('git rev-parse --short HEAD')}
                Author: ${childProcess.execSync('git config user.name')}
              `
        })
    ]
};

if (process.env.NODE_ENV === 'development') {
    webpack_config["devtool"] = 'eval-source-map'
}

module.exports = merge(webpack_config, { module: { rules: utils.styleLoaders({ sourceMap: true, usePostCSS: true }) } });
