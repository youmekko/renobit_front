import Vue from "vue";
import * as puremvc from "puremvc";

import ViewerQuickMenuComponent from "./ViewerQuickMenuComponent.vue";
import ViewerStatic from "../../controller/viewer/ViewerStatic";


export default class ViewerQuickMenuMediator extends puremvc.Mediator {
	public static NAME: string = "ViewerQuickMenuMediator";
	private view: ViewerQuickMenuComponent = null;

	constructor(name: string, viewComponent: Vue) {

		super(name, viewComponent);
		this.view = <ViewerQuickMenuComponent>viewComponent;
	}

	public getView(): ViewerQuickMenuComponent {
		return this.viewComponent;
	}

	public onRegister() {
        this.view.$on(ViewerQuickMenuComponent.EVENT_ACTIVE_360VIEW,()=>{
            this.sendNotification(ViewerStatic.CMD_ACTIVE_3D_360VIEW);
        })

        this.view.$on(ViewerQuickMenuComponent.EVENT_ACTIVE_WALKVIEW,()=>{
            this.sendNotification(ViewerStatic.CMD_ACTIVE_3D_WALKVIEW);
        })

        this.view.$on(ViewerQuickMenuComponent.EVENT_GOTO_EDITOR,()=>{
            this.sendNotification(ViewerStatic.CMD_GOTO_EDITOR);
        })

        this.view.$on(ViewerQuickMenuComponent.EVENT_GOTO_ADMIN,()=>{
            this.sendNotification(ViewerStatic.CMD_GOTO_ADMIN);
        })

        this.view.$on(ViewerQuickMenuComponent.EVENT_LOGOUT,()=>{
            this.sendNotification(ViewerStatic.CMD_GOTO_LOGOUT);
        })
	}


	public listNotificationInterests(): string[] {
		return [
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		switch (note.name) {

		}
	}
}
