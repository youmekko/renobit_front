import Vue from "vue";
import * as puremvc from "puremvc";

import ViewerAreaMainContainer from "./ViewerAreaMainContainer.vue";
import ViewerProxy from "../../model/ViewerProxy";


export default class ViewerAreaMainContainerMediator extends puremvc.Mediator {
	public static NAME: string = "ViewerAreaMainContainerMediator";
	private view: ViewerAreaMainContainer = null;

	constructor(name: string, viewComponent: Vue) {
		super(name, viewComponent);
		this.view = <ViewerAreaMainContainer>viewComponent;
	}

	public getView(): ViewerAreaMainContainer {
		return this.viewComponent;
	}

	public onRegister() {



	}


	public listNotificationInterests(): string[] {
		return [
			ViewerProxy.NOTI_ADD_COM_INSTANCE,
			ViewerProxy.NOTI_CLOSED_PAGE,
                  ViewerProxy.NOTI_OPEN_PAGE,
                  ViewerProxy.NOTI_ACTIVE_THREE_LAYER,

		]
	}


	public handleNotification(note: puremvc.Notification): void {
		switch (note.name) {
			case ViewerProxy.NOTI_CLOSED_PAGE:
				this.view.noti_closedPage();
				break;
                  case ViewerProxy.NOTI_OPEN_PAGE :
                        this.view.noti_openPage();
                        break;
			case ViewerProxy.NOTI_ADD_COM_INSTANCE :
				this.view.noti_addComInstance(note.getBody());
				break;
                  case ViewerProxy.NOTI_ACTIVE_THREE_LAYER :
                        this.view.noti_activeThreeLayer(note.getBody());
                        break;
		}
	}
}
