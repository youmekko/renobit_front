import * as puremvc from "puremvc";
import ViewerMainComponent from "./ViewerMainComponent.vue";
import ViewerProxy from "../model/ViewerProxy";

import ViewerStatic from "../controller/viewer/ViewerStatic";
import {VIEWER_APP_STATE} from "../model/data/Data";


export class ViewerMainMediator extends puremvc.Mediator {
	public static NAME: string = "ViewerMainMediator";
	private view: ViewerMainComponent = null;

	constructor(name: string, viewComponent: ViewerMainComponent) {
		super(name, viewComponent);
		this.view = viewComponent;
	}
 
	public getView(): ViewerMainComponent {
		return this.viewComponent;
	}


	public listNotificationInterests(): string[] {
		return [
			ViewerProxy.NOTI_UPDATE_VIEWER_STATE
		]
	}


	public handleNotification(note: puremvc.Notification): void {
		//console.log("실행 노티, ViewerMainMediator handleNotification note = ", note.name, "body = ", note.body);
		switch (note.name) {
			case ViewerProxy.NOTI_UPDATE_VIEWER_STATE :
				this.executeUpdateEditorState(note.getBody());
		}
	}


	private executeUpdateEditorState(state:VIEWER_APP_STATE){
		switch(state){
			case VIEWER_APP_STATE.READY_START :
				break;
			case VIEWER_APP_STATE.READY_COMPLETED :
				this.view.noti_readyCompleted();
		}
	}
}
