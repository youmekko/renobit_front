import * as puremvc from "puremvc";
import StartUpCommand from "./controller/StartUpCommand";
import ViewerMainComponent from "./view/ViewerMainComponent.vue";
import ReadyStatic from "./controller/ready/ReadyStatic";

export default class ViewerAppFacade extends puremvc.Facade {
    public static CMD_STARTUP: string = 'command/startup';
    public static NAME:string = "VIEWER_APP";
    constructor(key:string){
        super(key);
    }

    /** @override */
    initializeController() {
        super.initializeController();
        this.registerCommand(ViewerAppFacade.CMD_STARTUP, StartUpCommand);


    }

    /** @override */
    initializeModel() {
        super.initializeModel();
    }

    /** @override */
    initializeView() {
        super.initializeView();
    }

    /*

     */
    startup(app:ViewerMainComponent) {
        console.log("step 02, startup() 메서드 호출 ");
        this.sendNotification(ViewerAppFacade.CMD_STARTUP, app);
        // 전역 변수 등록을 시작으로!
        this.sendNotification(ReadyStatic.CMD_REGISTER_GLOBAL_VARIABLE_VIEWER, app);
    }

    static getInstance(multitonKey:string):ViewerAppFacade {
        const instanceMap:any = puremvc.Facade.instanceMap;
        if (!instanceMap[multitonKey]) {
            instanceMap[multitonKey] = new ViewerAppFacade(multitonKey);
            console.log("step 02, ViewerAppFacade 인스턴스 생성");
        }
        return instanceMap[multitonKey] as ViewerAppFacade;
    }
}
