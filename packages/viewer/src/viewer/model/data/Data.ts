export enum VIEWER_APP_STATE {
      READY_START="readyStart",
      CONFIG_LOADING="configLoading",
      READY_COMPLETED="readyCompleted"
}