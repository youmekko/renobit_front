import Vue from "vue";
import axios from "axios";

import ComponentLibaryManager from "../../../wemb/wv/managers/ComponentLibraryManager";
import PageManager from "../../../wemb/wv/managers/PageManager";
import ConfigManager from "../../../wemb/wv/managers/ConfigManager";

import MainMenuManager from "../../../wemb/wv/managers/MainMenuManager";


import WindowManager from "../../../wemb/wv/managers/WindowsManager";

import DataService from "../../../wemb/wv/managers/DataService";
import LocaleManager from "../../../wemb/wv/managers/LocaleManager";


declare function httpVueLoader(url: string, name: string);


import {IBackgroundElement, IThreeExtensionElements} from "../../../wemb/core/component/interfaces/ComponentInterfaces";
import ViewerAppFacade from "../../ViewerAppFacade";
import {EXE_MODE} from "../../../wemb/wv/data/Data";
import WScriptEventWatcher from "../../../wemb/core/events/WScriptEventWatcher";
import PopupManager from "../../../wemb/wv/managers/PopupManager";
import ViewerPageComponent from "../../../wemb/wv/components/ViewerPageComponent";
import WVComponentEvent from "../../../wemb/core/events/WVComponentEvent";
import { SoundManager } from "../../../wemb/wv/managers/SoundManager";
import PageHistoryManager from "../../../wemb/wv/managers/PageHistoryManager";
import ExtensionInstanceManager from "../../../wemb/wv/managers/ExtensionInstanceManager";
import PageTreeDataManager from "../../../wemb/wv/managers/PageTreeDataManager";


import DynamicModalComponent from "../../../wemb/wv/components/DynamicModalComponent.vue";

import ILoadingModal from "../../../common/modal/ILoadingModal";
import UserInfoManager from "../../../wemb/wv/managers/UserInfoManager";
import MenusetDataManager from "../../../wemb/wv/managers/MenusetDataManager";
import TemplateDataManager from "../../../wemb/wv/managers/TemplateDataManager";
import ViewerStatic from "../../controller/viewer/ViewerStatic";
import HookManager from "../../../wemb/wv/managers/HookManager";
import EditorProxy from "../../../editor/model/EditorProxy";

import EditMainAreaResourceLoadingComponent from "../../../wemb/wv/components/EditMainAreaResourceLoadingComponent.vue";

declare function httpVueLoader(url: string, name: string);

export default class WeMB{

      public static WVComponentEvent = WVComponentEvent;
      public $http: any;
      public $globalBus: Vue;
      public $componentBus: Vue;
      public $httpVueLoader = httpVueLoader;
      public $modalManager:DynamicModalComponent;
      public $defaultLoadingModal: ILoadingModal;
      public $editMainAreaResourceLoadingComponent: EditMainAreaResourceLoadingComponent;

      public componentLibraryManager: ComponentLibaryManager;
      public pageManager: PageManager;
      public pageTreeDataManager: PageTreeDataManager;
      public configManager: ConfigManager;
      public extensionInstanceManager:ExtensionInstanceManager;
      public windowsManager: WindowManager;
      public menusetDataManager:MenusetDataManager;
      public templateDataManager:TemplateDataManager;
      public dataService: DataService;
      public localeManager: LocaleManager;
      public soundManager: SoundManager;
      public userInfo: UserInfoManager;

      public pageHistoryManager:PageHistoryManager;


      public mainMenuManager: MainMenuManager;
      public viewComponentMap: Map<string, Vue> = new Map();
      public viewerFacade:ViewerAppFacade;


      public mainControls:any;
      public personControls:any;

      public mainPageComponent:ViewerPageComponent;
      public stageBackgroundElement:IBackgroundElement;
      public pageBackgroundElement:IBackgroundElement;
      public scriptEventWatcher:WScriptEventWatcher;
      public popupManager:PopupManager;



      public domEvents:any;
      public camera:any;
      public renderer:any;
      public scene:any;
      public raycaster:any;

      /*
      중요
      전체 페이지의 인스턴스 정보가 담길 인스턴스
       */
      public page:any = {};

      /*timer*/
      public setInterval: Function;
      public setTimeout: Function;
      public clearTimeout: Function;
      public clearInterval: Function;

      private _threeCoreElements:IThreeExtensionElements = null;

      public globalStore:Map<string,any> = null;
      public editorProxy: EditorProxy;

      // 전역으로  사용할 정보가 담길 변수
      public global:object = {
            queryParams:{}
      };

      get threeElements(){
            return this._threeCoreElements;
      }

      get hookManager():HookManager{
            return HookManager.getInstance();
      }


      constructor() {
            this.$http = axios;
            this.$globalBus = new Vue();
            this.$componentBus = new Vue();
            this.$httpVueLoader = httpVueLoader;

            this.pageManager = new PageManager();
            //this.configManager = new ConfigManager(EXE_MODE.VIEWER);
            this.configManager = ConfigManager.getInstance();
            this.configManager.exeMode = EXE_MODE.VIEWER;
            this.userInfo = UserInfoManager.getInstance();

            this.extensionInstanceManager = ExtensionInstanceManager.getInstance();

            this.pageTreeDataManager = new PageTreeDataManager();


            this.menusetDataManager = MenusetDataManager.getInstance();
            this.templateDataManager = TemplateDataManager.getInstance();

            // 컴포넌트 라이브러리 관리자
            this.componentLibraryManager = new ComponentLibaryManager(this.configManager.exeMode);


            // 스크립트 이벤트 실행기
            this.scriptEventWatcher = WScriptEventWatcher.getInstance();
            // 데이터 서비스 생성
            this.dataService = DataService.getInstance();
            this.localeManager = LocaleManager.getInstance();
            this.soundManager = SoundManager.getInstance();



            // 툴팁 관리 객체 생성
            //this.toolTopManager = WVToolTipManager.create()

            // 팝업 관리자 생성
            this.popupManager = new PopupManager();

            this.globalStore = new Map();


            this.pageHistoryManager = PageHistoryManager.getInstance();

            console.log("전역객체 Wemb 생성 완료!!");


            /*
		viewer에서만 실행
		세션 연장 기능 실행.
		 */
            ConfigManager.getInstance().startSessionCheckingManager();




      }




      /*
      중요.
      call : 3d container에서 호출, 추후 호출 위치를 획일화 시켜야 함.
       */
      public setThreeElements(threeElements:IThreeExtensionElements){
            this.camera = threeElements.camera;
            this.mainControls = threeElements.mainControls;
            this.personControls = threeElements.personControls;
            this.domEvents = threeElements.domEvents;
            this.renderer = threeElements.renderer;
            this.scene = threeElements.scene;
            this.raycaster = threeElements.raycaster;
            this._threeCoreElements = threeElements;
      }



      /*
      컴포넌트가 페이지인지 아닌지 파악하는 기능
      JS에는 인터페이스를 사용할 수 없기 때문에
      컴포넌트 프로퍼티에 __PAGE__라는 프로퍼티가 존재하는 경우 페이지로 간주
       */
      isPage(component:any):boolean{
            // instanceof에서 클래스만 사용할 수 있을 뿐 interface 타입을 사용할 없기 때문에 아래와 같애 특정 프로퍼티가 존재하는지에 따라
            // 페이지인지 아닌지 처리
            if(component==null)
                  return false;

            if(component.hasOwnProperty("__PAGE__"))
                  return true;

            return false;
      }

      logout(){
            this.viewerFacade.sendNotification(ViewerStatic.CMD_GOTO_LOGOUT);
      }

      gotoEditor(){
            this.viewerFacade.sendNotification(ViewerStatic.CMD_GOTO_EDITOR);
      }

      /*
      2018.11.05(ckkim)
      외부 모달을 동적으로 활성화 처리


      @modalName: 활성화 모달에 붙일 이름
      @fileName: 모달이 구현된 파일
      @opener: 모달을 활성화 시킨 대상
      @data: 데이터
      @closedCallback: 모달이 닫힐때 실행할 callback function

      call : editor에서만 사용.

       */
      public showExternalModal(modalName:string, fileName:string, opener:any=null, data:any=null, closedCallback:Function=null){
            this.viewerFacade.sendNotification(ViewerStatic.CMD_SHOW_MODAL, {
                  modalName:modalName,
                  fileName:fileName,
                  opener:opener,
                  data:data,
                  closed:closedCallback
            })
      }



}
