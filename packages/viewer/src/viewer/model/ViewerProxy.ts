import * as puremvc from "puremvc";
import {VIEWER_APP_STATE} from "./data/Data";


import {MasterBackgroundInfo, PageInfo, StageBackgroundInfo} from "../../wemb/wv/page/Page";
import WVComponent from "../../wemb/core/component/WVComponent";
import ComponentInstanceLoader from "../../wemb/wv/managers/ComponentInstanceLoader";
import {IThreeExtensionElements} from "../../wemb/core/component/interfaces/ComponentInterfaces";
import {EXE_MODE} from "../../wemb/wv/data/Data";
import {IComponentInstanceLoaderContainer} from "../../wemb/wv/components/page/interfaces/PageComponentInterfaces";
import {WORK_LAYERS} from "../../wemb/wv/layer/Layer";



class ViewerProxy extends puremvc.Proxy  implements IComponentInstanceLoaderContainer{
	public static readonly NAME: string = "ViewerProxy";

	public static readonly NOTI_UPDATE_VIEWER_STATE:string="notification/updateViewerState";
	public static readonly NOTI_ADD_COM_INSTANCE:string = "notification/addComponentInstance";
	public static readonly NOTI_OPEN_PAGE:string = "notification/openPage";
      // 페이지가 열린 후 발생하는 이벤트(리소스 로드까지 완료된 상태)
      public static readonly NOTI_OPENED_PAGE:string = "notification/openedPage";
      public static readonly NOTI_BEFORE_CLOSE_PAGE:string = "notification/beforeClosePage";
	public static readonly NOTI_CLOSED_PAGE:string = "notification/closedPage";

      public static readonly NOTI_CHANGE_PAGE_LOADING_STATE:string="notification/changPageLoadingState";
	/////////////////////////////////////////////////
      public static readonly NOTI_ACTIVE_THREE_LAYER:string = "notification/activeThreeLayer";
      public static readonly NOTI_UPDATE_EDITOR_STATE:string = "notification/updateEditorState"




	/////////////////////////////////////////////////
	private _appState:VIEWER_APP_STATE;

	// 환경설정 정보까지 준비 완료가 되면 true가 됨. (오직 한번만 초기화 됨.
	private _readyCompleted:boolean = false;


	private _masterBackgroundInfo:MasterBackgroundInfo =new MasterBackgroundInfo();
	get masterBackgroundInfo():MasterBackgroundInfo{
	      return this._masterBackgroundInfo;
      }

	private _stageBackgroundInfo:StageBackgroundInfo = new StageBackgroundInfo();
      get stageBackgroundInfo():StageBackgroundInfo{
            return this._stageBackgroundInfo;
      }

	/*
	값이 closed가 되는 경우 모든 내용을 삭제
	 */
	private _isOpenPage:boolean = false;

	public get isOpenPage(){
	      return this._isOpenPage;
      }


      private _masterLayerInstanceList:WVComponent[] = [];
      public get masterLayerInstanceList(){
            return this._masterLayerInstanceList;
      }

      private _threeLayerInstanceList:WVComponent[] = [];
      public get threeLayerInstanceList(){
            return this._threeLayerInstanceList;
      }


      private _twoLayerInstanceList:WVComponent[] = [];
      public get twoLayerInstanceList(){
            return this._twoLayerInstanceList;
      }


      private _comInstanceList:WVComponent[] = [];
	public get comInstanceList(){
	      return this._comInstanceList;
      }



      /*
      2018.11.05(ckkim)
            동적 페이지 스크립트 객체
            call : OpenPageCommand
       */
      private _pageScript:Function;
      public set pageScriptFunction(pageScript){
	      this._pageScript = pageScript;
      }
	constructor() {

		super(ViewerProxy.NAME);

	}



	public onRegister() {

	}
	/////////////////////////////////////////////////////////////////////




	/////////////////////////////////////////////////////////////////////
	public updateAppState(state:VIEWER_APP_STATE){
		this._appState = state;

            //console.log("@@ test33 #2 viewrProxy updateAppState ", state);
		if(state==VIEWER_APP_STATE.READY_COMPLETED){
			this._readyCompleted = true;
		}


		this.sendNotification(ViewerProxy.NOTI_UPDATE_VIEWER_STATE, state);
	}



	public getReadyCompleted():boolean{
		return this._readyCompleted;
	}






      public setPageOpenState(isState:boolean){
            this._isOpenPage = isState;
      }
	/*
	현재 열려 있는 페이지가 있다면 닫기
		단축키 비활성화
		편집 비활성화
		수정 상태를 false로
		기존 편접 정보 무두 삭제
		선택 정보 삭제
		복사 정보 삭제

	 */
	public closedPage(){
	      if(this._isOpenPage==true) {

                  this.sendNotification(ViewerProxy.NOTI_BEFORE_CLOSE_PAGE, window.wemb.pageManager.currentPageInfo.id);

                  this.setPageOpenState(false);
                  this._comInstanceList = [];
                  // this._comInstanceList.push(...this._masterLayerInstanceList);
                  this._twoLayerInstanceList = [];
                  this._threeLayerInstanceList= [];

                  /*
                  2018.11.05(ckkim)
                  페이지 스크립트 객체 초기화
                   */
                  this._pageScript = null;

                  this.sendNotification(ViewerProxy.NOTI_CLOSED_PAGE, window.wemb.pageManager.currentPageInfo.id);
            }
	}



	/*
	페이지 프로퍼티 정보 설정 및 다시  그리기
            1. 페이지 정보 재 설정
            2. 페이지 정보 즉서 적용
            3. resize 이벤트 발생
	 */
	public setPagePropertyInfo(pageInfo:any){
            /*
		페이지 정보 설정 후
		immedateUpdateDisplay()
		onAddedOnctainer()호출
		 */
            window.wemb.mainPageComponent.resetProperties(pageInfo);

            let event;
            if(typeof(Event) === 'function') {
                  event = new Event('resize');
            }else{
                  event = document.createEvent('Event');
                  event.initEvent('resize', true, true);
            }

            window.dispatchEvent(event);


	}

	/////////////////////////////////////////////


      /*
		* 컴포넌트 정보 생성
			* 마스터 레이어 정보 추가
			* 2D 레이어 정보 추가
			* 3D 레이어 정보 추가
		* 컴포넌트 인스턴스 생성
			* 마스터 정보 출력(단, 마스터 정바가 출력이 안된 경우)
			* 2D 정보 출력
			* 3D 정보 출력
		 */
      public async startConvertingComponentInfoToComponentInstance(comInstanceInfoList:Array<any>, flag:boolean):Promise<any>{

            // 인스턴스 생성 후 레이어에 추가 하기
            // 모든 인스턴스가 레이어에 추가되면 두 번째 정보로 넘어가는 window.wemb.$defaultLoadingModal==progressBar의 hide() 메서드가 호출되 로딩화면이 사라짐.

            let success=   await ComponentInstanceLoader.startInstanceLoader(comInstanceInfoList, window.wemb.$defaultLoadingModal, this, EXE_MODE.VIEWER);

            if(success) {
                  this.completePageReady();
                  return true;
            }

            return false;
      }




      /*
      페이지 열기 준비 완료
       */
      public completePageReady(){
            this.setPageOpenState(true)

            this.sendNotification(ViewerProxy.NOTI_OPEN_PAGE);
      }

      public addComInstance(comInstance:WVComponent){

            if(comInstance.layerName==null)
                  return;

            this._comInstanceList.push(comInstance);

            // 마스터 레이어의 컴포넌트인경우 마스터 레이어에 추가
            // onOpenPage() 메서드 호출을 위해

            switch(comInstance.layerName){
                  case WORK_LAYERS.MASTER_LAYER:
                        this._masterLayerInstanceList.push(comInstance);
                        break;
                  case WORK_LAYERS.TWO_LAYER:
                        this._twoLayerInstanceList.push(comInstance);
                        break;
                  case WORK_LAYERS.THREE_LAYER:
                        this._threeLayerInstanceList.push(comInstance);
                        break;
            }



            this.sendNotification(ViewerProxy.NOTI_ADD_COM_INSTANCE, comInstance);
            return true;
      }






      ////////////////////////////////////////////////////////////////////////////////
      // ComponentInstanceLoader에 의해서 모두 로드되고 난 후 실행되는 메서드
      // 여기에서는 사용하지 않음.
      public loadCompleted(){
      }


      // ComponentInstanceFactory 에서 호출됨.
      public getThreeExtensionElements():IThreeExtensionElements{
            return window.wemb.threeElements;
      }



      ////////////////////////////////////////////////////////////////////////////////


}


window.ViewerProxy = ViewerProxy;
export default ViewerProxy;
