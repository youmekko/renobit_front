import * as puremvc from "puremvc";
import {BackgroundInfo} from "../../wemb/wv/page/Page";

export default class StageProxy extends puremvc.Proxy {
    public static readonly NAME: string = "StageProxy";

      public backgroundInfo: any = {
            "stage": {"color": "#ff0000", "path": "", "image": ""},
            "master": {"color": "#ffffff", "path": "", "image": ""},
            "page": {"color": "#eeeeee", "path": "", "image": "", using: false},
      };

    constructor() {
        super(StageProxy.NAME);

    }

    public onRegister() {

    }

    public setPageBackgroundInfo(pageBGInfo: any) {
        this.backgroundInfo.page = pageBGInfo;
          this.updatePageBackgroundInfo();
    }

      public setMasterBackgroundInfo(backgroundInfo: BackgroundInfo) {
            this.backgroundInfo.master = backgroundInfo;
      }


      public setStageBackgroundInfo(backgroundInfo: BackgroundInfo) {
            this.backgroundInfo.stage = backgroundInfo;
            this.updateStageBackgroundInfo();
      }


      public updatePageBackgroundInfo() {
            if (this.backgroundInfo.page.using == true) {
                  window.wemb.pageBackgroundElement.setBackgroundPropertyValue("pageBackground", this.backgroundInfo.page);
            } else {
                  this.backgroundInfo.master.using = true;
                  window.wemb.pageBackgroundElement.setBackgroundPropertyValue("masterBackground", this.backgroundInfo.master);
            }
      }

      public updateStageBackgroundInfo() {
            this.backgroundInfo.stage.using = true;
            window.wemb.stageBackgroundElement.setBackgroundPropertyValue("stageBackground", this.backgroundInfo.stage);
      }



}
