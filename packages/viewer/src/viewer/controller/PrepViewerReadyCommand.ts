import * as puremvc from "puremvc";


import {RegisterGlobalVariableCommand} from "./ready/RegisterGlobalVariableCommand";
import {ReadyViewerCommand} from "./ready/ReadyViewerCommand";
import ReadyStatic from "./ready/ReadyStatic";
import {StartViewerCommand} from "./ready/StartViewerCommand";

export class PrepViewerReadyCommand extends puremvc.SimpleCommand {


    execute(note: puremvc.INotification) {

	  console.log("step 02, PrepViewerReadyCommand 실행");
        this.facade.registerCommand(ReadyStatic.CMD_REGISTER_GLOBAL_VARIABLE_VIEWER, RegisterGlobalVariableCommand);
        this.facade.registerCommand(ReadyStatic.CMD_READY_VIEWER, ReadyViewerCommand);
        this.facade.registerCommand(ReadyStatic.CMD_START_VIEWER, StartViewerCommand);

    }
}
