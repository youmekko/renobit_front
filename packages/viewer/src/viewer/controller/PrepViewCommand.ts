import * as puremvc from "puremvc";


import {ViewerMainMediator} from "../view/ViewerMainMediator";
import ViewerAreaMainContainerMediator from "../view/work-area/ViewerAreaMainContainerMediator";
import ViewerQuickMenuMediator from "../view/quickmenu/ViewerQuickMenuMediator";


export class PrepViewCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {

		console.log("step 02, preViewCommand");
		let viewerMainMediator: puremvc.IMediator = new ViewerMainMediator(ViewerMainMediator.NAME, note.getBody());

		this.facade.registerMediator(viewerMainMediator);		
        this.facade.registerMediator(new ViewerAreaMainContainerMediator(ViewerAreaMainContainerMediator.NAME, window.wemb.viewComponentMap.get(ViewerAreaMainContainerMediator.NAME)));
		this.facade.registerMediator(new ViewerQuickMenuMediator(ViewerQuickMenuMediator.NAME, window.wemb.viewComponentMap.get(ViewerQuickMenuMediator.NAME)));
	}
}