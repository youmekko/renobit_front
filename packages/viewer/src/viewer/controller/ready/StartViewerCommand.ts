import * as puremvc from "puremvc";
import Vue from "vue";
import ViewerStatic from "../viewer/ViewerStatic";
import CommanderStatic from "../common/CommonStatic";



export class StartViewerCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {

            console.log("step 04, StartEditorCommand 실행", "초기 페이지 읽기 시작");
            var locale_msg = Vue.$i18n.messages.wv;

            //1. 페이지가 없는 경우
		if (window.wemb.pageManager.getPageLength() == 0) {
                  this.sendNotification(CommanderStatic.CMD_CRITICAL_404_ERROR_COMMAND, locale_msg.page.noPage);

            //2. 페이지가 있는 경우
		}else {
                  let pageId : string = "";
                  let pageName : string="";

                  // 1순위 : pname 으로 오는 경우 처리
                  if(window.wemb.configManager.startPageName.length){

				// 페이지 열기
                        pageId = window.wemb.pageManager.getPageIdByName(window.wemb.configManager.startPageName);
                        pageName = window.wemb.configManager.startPageName;
                  }else {

                        //2순위 : admin 에서 설정한 로그인 사용자 시작 페이지 ID
                        pageId = window.wemb.configManager.getStartPageId();
                        if (pageId) {
                              pageName = window.wemb.pageManager.getPageInfoBy(pageId).name;
                        }

                  }

                  console.log("step 04, StartEditorCommand 실행", "초기 페이지 ", pageId, pageName);

                  if (!pageId) {
                        window.wemb.$defaultLoadingModal.hide();
                        this.sendNotification(CommanderStatic.CMD_CRITICAL_404_ERROR_COMMAND, locale_msg.page.noPage);
				return;
                  }

                  this.sendNotification(ViewerStatic.CMD_OPEN_PAGE, pageId);
		}
	}
}
