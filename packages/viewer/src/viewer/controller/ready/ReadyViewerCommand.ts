import * as puremvc from "puremvc";
import ViewerProxy from "../../model/ViewerProxy";
import pageApi from "../../../common/api/pageApi";
import {event} from "../../../wemb/wv/events/LoadingEvent";
import LoadingEvent = event.LoadingEvent;
import {ProjectInfoData} from "../../../wemb/wv/page/Page";
import StageProxy from "../../model/StageProxy";
import {VIEWER_APP_STATE} from "../../model/data/Data";
import ReadyStatic from "./ReadyStatic";
// import PublicProxy from "../../../editor/model/PublicProxy";
import ViewerStatic from "../viewer/ViewerStatic";


export class ReadyViewerCommand extends puremvc.SimpleCommand {
      private viewerProxy: ViewerProxy;
      // private publicProxy:PublicProxy;
      private stageProxy: StageProxy;

      execute(note: puremvc.INotification) {
            console.log("step 03, ReadyViewerCommand, 1. 준비 시작");

            this.viewerProxy = <ViewerProxy>this.facade.retrieveProxy(ViewerProxy.NAME);
            // this.publicProxy = <PublicProxy>this.facade.retrieveProxy(PublicProxy.NAME);
            this.stageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);

            this.viewerProxy.updateAppState(VIEWER_APP_STATE.READY_START);
            window.wemb.$defaultLoadingModal.setMaxLength(5);
            window.wemb.$defaultLoadingModal.show(window.wemb.$defaultLoadingModal.$i18n.messages.wv.common.preparing);
            this._startConfigLoad();
      }


      _startConfigLoad() {
            console.log("step 03, ReadyViewerCommand, 2. 환경설정 정보 읽기 시작");
            window.wemb.$defaultLoadingModal.addMessage("환경설정 정보 읽기 시작", false);
            window.wemb.$defaultLoadingModal.setProgress(1);
            window.wemb.$defaultLoadingModal.addMessage("환경설정 정보 읽기 완료");
            this._startMasterPageInfoLoad();
            /*
            삭제 예정
		window.wemb.configManager.startLoading().then((success) => {
			window.wemb.$defaultLoadingModal.setProgress(1);
			window.wemb.$defaultLoadingModal.addMessage("환경설정 정보 읽기 완료");
			//if (success) {
				this._startMasterPageInfoLoad();
			}
		})
		*/


      }

      _startMasterPageInfoLoad() {
            /*
            마스터 페이지 정보 읽기
                  - 페이지 리스트 정보
                  - 스테이지 정보
                  - 마스터 정보
                        - 마스터 프로퍼티 정보
                        - 마스터 레이어 정보
             */
            console.log("step 03, ReadyViewerCommand, 3.마스터페이지 정보 읽기 시작");
            pageApi.loadMasterPageInfo().then((projectInfo:ProjectInfoData)=>{
                  /*
                   json 페이지 정보를 파싱해서  PageInfo 정보로 만들기.
                   */
                  window.wemb.pageManager.attachJsonPageInfoListToPageInfoList(projectInfo.page_list);

                  this.stageProxy.setMasterBackgroundInfo(projectInfo.master_info.background);
                  this.stageProxy.setStageBackgroundInfo(projectInfo.stage_info.background);


                  // this.publicProxy.setComInstanceInfoListInMasterLayer(projectInfo.master_info.master_layer);

                  window.wemb.$defaultLoadingModal.setProgress(2);
                  window.wemb.$defaultLoadingModal.addMessage("페이지 정보 로딩 완료");
                  this._startPageTreeInfoLoad();

            })
      }


      _startPageTreeInfoLoad() {
            console.log("step 03, ReadyViewerCommand, 4.트리 정보 읽기 시작");
            window.wemb.$defaultLoadingModal.addMessage("페이지 트리정보 로딩 시작",false);

            window.wemb.pageTreeDataManager.startLoading().then((success) => {
                  window.wemb.$defaultLoadingModal.setProgress(3);
                  window.wemb.$defaultLoadingModal.addMessage("페이지 트리정보 로딩 완료");
                  this._startComponentPanelInfo();
            })
      }



      async _startComponentPanelInfo(){

            console.log("step 03, ReadyEditorCommand, 5.컴포넌트 패널 정보 로딩 시작");
            window.wemb.$defaultLoadingModal.addMessage("컴포넌트 패널 정보 로딩 시작",false);

            try {
                  let devComponentPanelInfoList = await window.wemb.componentLibraryManager.getComponentPanelInfoList();
                  window.wemb.$defaultLoadingModal.setProgress(4);
                  window.wemb.$defaultLoadingModal.addMessage("컴포넌트 패널 정보 로딩 완료");
                  this._startComponentLoad();

            }catch(error){

            }


      }


      _startComponentLoad() {

            console.log("step 03, ReadyViewerCommand, 6.컴포넌트 라이브러리 로딩 시작 ");

            window.wemb.$defaultLoadingModal.addMessage("컴포넌트 라이브러리 로딩 시작  ", false);


            window.wemb.componentLibraryManager.$on(LoadingEvent.LOAD_START, (event)=>{
                  window.wemb.$defaultLoadingModal.addMessage("\t\t   -라이브러리 로딩 시작", false);
                  window.wemb.$defaultLoadingModal.addMessage(`    -라이브러리 로딩 개수  ${event.data.loadLength}`, false);
            });

            window.wemb.componentLibraryManager.$on(LoadingEvent.PROGRESS, (event)=>{
                  window.wemb.$defaultLoadingModal.addMessage(`    -${event.data.currentIndex}. ${event.data.componentInfo.name} 로딩 완료`, false);
            });

            window.wemb.componentLibraryManager.$on(LoadingEvent.COMPLETED, (event)=>{
                  console.log("step 03, ReadyViewerCommand, 6.컴포넌트 라이브러리 로딩 완료 ");
                  window.wemb.$defaultLoadingModal.completed();
                  window.wemb.$defaultLoadingModal.addMessage("컴포넌트 라이브러리 로딩 완료");
                  console.log("########################################뷰어 초기화 완료 ############################\n\n\n");


                  /*
		      2018.11.21(ckkim)

		      주의!!!!!!!!!!!!
		      RENOBIT 상태 업데이트 처리가 동적패널, 동적플러그인보다 앞서서 실행되면 안됨.

		     */
                  // extension 생성하기
                  this.sendNotification(ViewerStatic.CMD_CREATE_EXTENSION);

                  /* 2018.12.19(ckkim)
		     확장요소(plugin,  panel)이 모두 로드 완료 된 후 실행
			*/
                  this.sendNotification(ViewerStatic.CMD_COMPLETED_EXTENSION);



                  // 뷰어 상태 업데이트 처리
                  this.viewerProxy.updateAppState(VIEWER_APP_STATE.READY_COMPLETED);



                  // 뷰어 시작
                  setTimeout((()=>{
                        // 2018.11.17(ckkim) 화면을 뒤덮는 로딩 효과 숨기기
                        window.wemb.$defaultLoadingModal.hide();
                        this.sendNotification(ReadyStatic.CMD_START_VIEWER);
                  }),500);
            });


            window.wemb.componentLibraryManager.startLoading();
      }

}
