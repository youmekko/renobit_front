import * as puremvc from "puremvc";
import Vue from "vue";

import ReadyStatic from "./ReadyStatic";
import ILoadingModal from "../../../common/modal/ILoadingModal";
import IMediator = puremvc.IMediator;
import ViewerAppFacade from "../../ViewerAppFacade";
import ViewerProxy from "../../model/ViewerProxy";
import ViewerAreaMainContainerMediator from "../../view/work-area/ViewerAreaMainContainerMediator";
import { SessionScheduler } from "../../../wemb/wv/managers/SessionScheduler";
import {CONFIG_MODE} from "../../../wemb/wv/data/Data";
import MenusetDataManager from "../../../wemb/wv/managers/MenusetDataManager";
import TemplateDataManager from "../../../wemb/wv/managers/TemplateDataManager";

export class RegisterGlobalVariableCommand extends puremvc.SimpleCommand {
	async execute(note: puremvc.INotification) {
		console.log("step 02, RegisterGlobalVariableCommand 실행", "전역 변수 등록");

            let viewerMainComponent:any = <Vue>note.getBody();

            // 환경설정 정보 설정하기
            // test, pid, layer 정보 설정하기
            window.wemb.configManager.initConfigValue(viewerMainComponent.$route.query);
            // 전역 정보중 queryParams에 넣음
            /*
            2018.05.11(ddandongne)
            configManager.queryParams는 삭제 예정
            전역 데이터는 모두 global에 넣을 예정
             */
            window.wemb.global.queryParams = viewerMainComponent.$route.query;


            Vue.$router = viewerMainComponent.$router;
            Vue.$route = viewerMainComponent.$route;


            /*
		DefaultLoadingModal 대신 ILoadingModal을 사용하는 경우 에러
		 */
            window.wemb.$defaultLoadingModal = <ILoadingModal>viewerMainComponent.$refs["defaultLoadingModal"];
            window.wemb.$editMainAreaResourceLoadingComponent = viewerMainComponent.$refs["editMainAreaResourceLoadingComponent"];
            window.wemb.$pageResourceLoadingBar = <ILoadingModal>viewerMainComponent.$refs["pageResourceLoadingBar"];

            window.wemb.viewerFacade = <ViewerAppFacade>ViewerAppFacade.getInstance(ViewerAppFacade.NAME);
            window.wemb.currentFacade =window.wemb.viewerFacade;

            window.wemb.viewerProxy = window.wemb.viewerFacade.retrieveProxy(ViewerProxy.NAME) as ViewerProxy;
            window.wemb.$modalManager = viewerMainComponent.$refs["modalManager"];

            let viewAreaMainContainerMediator:IMediator = window.wemb.viewerFacade.retrieveMediator(ViewerAreaMainContainerMediator.NAME);
            window.wemb.mainPageComponent =  viewAreaMainContainerMediator.getViewComponent().mainPageComponent;


            /////////////////////
            // main 영역의 three 정보 설정하기
            // 이 값은 three component에 주입됨.

            /*
	            2018.05.08, ddandongne
	            3D layer 사용 유무를 config로 변경해야함.
		*/
            let mainThreeLayer = window.wemb.mainPageComponent.getMainThreeLayer();
            if(mainThreeLayer!=null){
                  if(mainThreeLayer.threeElements==null) {
                        alert(Vue.$i18n.messages.wv.etc.etc01);
                        return;
                  }
                  window.wemb.setThreeElements(mainThreeLayer.threeElements);

            }else {
                  console.log("3D Layer를 사용하지 않음.")
            }
            /////////////////////
            /////////////////////
            // 배경 처리용 요소들
            window.wemb.stageBackgroundElement = viewAreaMainContainerMediator.getViewComponent();
            window.wemb.pageBackgroundElement = window.wemb.mainPageComponent.bgLayer;
            /////////////////////

            console.log("sessionTime :", window.wemb.configManager.sessionTime );
            if(window.wemb.configManager.configMode == CONFIG_MODE.LOCAL) {
                  let result = await window.wemb.userInfo.setUserConfigInfo();
                  if(!result) {
                        alert("setUserConfigInfo error");
                        return;
                  }
            }

            let menusetLoadSuccess = await MenusetDataManager.getInstance().startLoading();
            if(menusetLoadSuccess==false){
                  console.log("메뉴셋 정보 로드 실패.");
            }


            let templateLoadSuccess = await TemplateDataManager.getInstance().startLoading();
            if(templateLoadSuccess==false){
                  console.warn("RENOBIT RegisterGlobalVariableCommand, Loading template manager information failed.");
            }

            SessionScheduler.getInstance().setSessionTime(window.wemb.configManager.sessionTime);
            // 페이지 열기 시작
		this.sendNotification(ReadyStatic.CMD_READY_VIEWER);
	}

}


