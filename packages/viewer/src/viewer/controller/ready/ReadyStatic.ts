export default class ReadyStatic {
	/*
	실행순서
	CMD_REGISTER_GLOBAL_VARIABLE_VIEWER
	CMD_READY_VIEWER
	CMD_START_VIEWER

	 */
	public static CMD_REGISTER_GLOBAL_VARIABLE_VIEWER: string = "command/globalVariableViewer";
	public static CMD_READY_VIEWER: string = "command/readyViewer";
	public static CMD_START_VIEWER: string = "command/startViewer";



}
