export default class CommonStatic {
	public static CMD_CRITICAL_ERROR_COMMAND: string = "command/criticalErrorCommand";
      public static CMD_CRITICAL_404_ERROR_COMMAND: string = "command/critical404ErrorCommand";
      public static CMD_CRITICAL_403_ERROR_COMMAND: string = "command/critical403ErrorCommand";


      public static readonly  CMD_ACTIVE_FPS="command/activeFPS";
}
