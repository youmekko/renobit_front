import * as puremvc from "puremvc";
import CommonStatic from "./CommonStatic";
import Vue from "vue";

/*
페이지 권한이 없는 경우
 */
export class Critical403ErrorCommand  extends puremvc.SimpleCommand{
    execute( note:puremvc.INotification )
    {
        //   alert(note.getBody());
        var locale_msg = Vue.$i18n.messages.wv;
        alert(locale_msg.error.forbidden)
        // Vue.$alert(note.getBody(), locale_msg.error.forbidden, {
        //     confirmButtonText: 'OK',
        //     type: "error",
        //     dangerouslyUseHTMLString: true
        // });
        // location.href=window.wemb.configManager.serverUrl+"/403Error.do";
    }


}
