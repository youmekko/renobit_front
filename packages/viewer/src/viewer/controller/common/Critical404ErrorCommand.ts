import * as puremvc from "puremvc";
import CommonStatic from "./CommonStatic";
import Vue from "vue";

/*
페이지가 없는 경우
 */
export class Critical404ErrorCommand  extends puremvc.SimpleCommand{
    execute( note:puremvc.INotification )
    {
          //console.log("Critical404ErrorCommand 에러 페이지로 이동", note.getBody());
          alert(note.getBody());
          location.href=window.wemb.configManager.serverUrl+"/404Error.do";


        /*Vue.$alert('error code: []<br>설정값이 remote인 경우 config.json에 remote 정보가 있어야 합니다. 관리자에 문의해주세요.', '서버 에러', {
            confirmButtonText: 'OK',
            type: "error",
            dangerouslyUseHTMLString: true
        });*/

        //Vue.$router.push("/error");
    }


}
