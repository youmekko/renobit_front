import * as puremvc from "puremvc";
import ViewerProxy from "../model/ViewerProxy";
import StageProxy from "../model/StageProxy";
import PublicProxy from "../model/PublicProxy";

export class PrepModelCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {

		this.facade.registerProxy(new ViewerProxy());
            this.facade.registerProxy(new PublicProxy());
		this.facade.registerProxy(new StageProxy());


	}
}
