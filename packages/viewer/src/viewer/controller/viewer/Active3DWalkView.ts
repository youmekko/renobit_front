import * as puremvc from "puremvc";

export class Active3DWalkView extends puremvc.SimpleCommand {
    execute(note: puremvc.INotification) {
          wemb.threeElements.threeLayer.toggleWorkView(true,()=>{
                wemb.threeElements.personControls.enabled =true;
          })
    }
}
