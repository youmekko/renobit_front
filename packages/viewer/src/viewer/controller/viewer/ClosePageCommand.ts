import * as puremvc from "puremvc";
import ViewerProxy from "../../../viewer/model/ViewerProxy";

/*
viewer에서는 사용하지 않음.
 */
export class ClosePageCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {
            console.log("ClosePageCommand 실행, page id = ");
            window.wemb.mainPageComponent.isLoaded=false;
            let viewerProxy: ViewerProxy = <ViewerProxy>this.facade.retrieveProxy(ViewerProxy.NAME);
            viewerProxy.closedPage();

            console.log("페이지 닫힘 완료.")
      }
}
