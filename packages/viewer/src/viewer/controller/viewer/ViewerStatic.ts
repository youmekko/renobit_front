export default class ViewerStatic {

      public static readonly  CMD_CHANGE_PAGE_NAME:string = "command/changePageName";
      public static readonly CMD_DELETED_CURRENT_PAGE:string = "command/deletedCurrentPage";
      public static readonly CMD_EXPORT_PAGE:string = "command/exportPage";
      public static readonly  SELECTED_CLASS_NAME:string = "wv-component-selected";
      

	public static readonly  CMD_OPEN_PAGE:string = "command/openPage";
      public static readonly  CMD_CLOSE_PAGE:string = "command/closePage";

	public static readonly  CMD_CHANGE_ACTIVE_LAYER:string = "command/changeActvieLayer";
	public static readonly  CMD_ADD_ALL_SELECTED:string = "command/addAllSelected";
	public static readonly  CMD_REMOVE_SELECTED:string = "command/removeSelected";


	public static readonly  CMD_COPY_SELECTED_COM_INSTANCE:string = "command/copySelectedComponentInstance";
	public static readonly  CMD_CUT_SELECTED_COM_INSTANCE:string = "command/cutSelectedComponentInstance";
	public static readonly  CMD_PASTE_COM_INSTANCE:string = "command/pasteComponentInstance";


      public static readonly  CMD_ALIGN_SELECTED_COM_INSTANCE:string = "command/alignSelecedComponentInstance";
      public static readonly  CMD_ARRANGE_SELECTED_COM_INSTANCE:string = "command/arrangeSelecedComponentInstance";
      public static readonly  CMD_SIZE_SELECTED_COM_INSTANCE:string  = "command/sizeSelectedComponentInstance";









	public static readonly CMD_SAVE_PAGE:string = "command/savePage";
      public static readonly CMD_SHOW_NEW_PAGE_MODAL:string = "command/showNewPageModal";
      public static readonly CMD_SHOW_SAVE_AS_PAGE_MODAL:string = "command/showSaveAsPageModal";



      public static readonly CMD_SHOW_RESOURCE_MANAGER:string = "command/showResourceManager";



      public static readonly CMD_CREATE_EXTENSION:string = "command/createExtension";

      public static readonly CMD_GOTO_LOGOUT:string="command/gotoLogout";
      public static readonly CMD_GOTO_ADMIN:string="command/gotoAdmin";
      public static readonly CMD_GOTO_EDITOR:string="command/gotoEditor";

      public static readonly CMD_ACTIVE_3D_360VIEW:string="command/active3D360View";
      public static readonly CMD_ACTIVE_3D_WALKVIEW:string="command/active3DWalkView";

      /*
      2018.11.23(ckkim)
      동적 모달 활성화
       */
      public static readonly  CMD_SHOW_MODAL:string ="command/showModal";


      /*
      2018.12.19(ckkim)
      plugin, panel 정보가 모두 생성 완료된 상태
       */

      public static readonly  CMD_COMPLETED_EXTENSION:string ="command/completedExtension";
}

/*
2018.08.25
      - 자바스크립트에서도 사용할 숭 ㅣㅆㄲ ㅔ추가
 */
window.ViewerStatic = ViewerStatic;
