import Vue from "vue";
import * as puremvc from "puremvc";
import pageApi from "../../../common/api/pageApi";
import StageProxy from "../../../viewer/model/StageProxy";
import PublicProxy from "../../../viewer/model/PublicProxy";
import ViewerProxy from "../../../viewer/model/ViewerProxy";
import {OpenPageData} from "../../../wemb/wv/page/Page";
import WVComponent from "../../../wemb/core/component/WVComponent";
import {WVComponentScriptEvent, WVPageComponentScriptEvent} from "../../../wemb/core/events/WVComponentEventDispatcher";
import {WVCOMPONENT_METHOD_INFO} from "../../../wemb/core/component/interfaces/ComponentInterfaces";
import {ComponentResourceLoader, ComponentResourceLoaderEvent} from "../../../wemb/wv/helpers/ComponentResourceLoader";
import CommanderStatic from "../common/CommonStatic";
import HookManager from "../../../wemb/wv/managers/HookManager";
import {WORK_LAYERS} from "../../../wemb/wv/layer/Layer";
import NLoaderManager from "../../../wemb/core/component/3D/manager/NLoaderManager";

/*
실행 순서
1. 읽어들일 페이지 존재 유무 판단.
2. 로딩 모달 활성화
3. 페이지 닫기 전 WScript 이벤트 발생
4. 페이지 닫기
      4-1. 뷰어 모드 비활성화
      4-2. 레이어 정보 초기화(ViewerPageContainer에서 생성한 컴포넌트 모두 삭제

5. 페이지 닫기 WScript 이벤트 발생
6. 페이지 열기 전 WScript 이벤트 발생
7. 페이지 열기
8. 생성할 컴포넌트 인스턴스 정보 생성
      7-1. 마스터 컴포넌트 생성 유무 판단 후 인스턴스 정보 추가
      7-2. two 영역 컴포넌트 인스턴스 정보 생성
      7-3. three 영역 컴포넌트 인스턴스 정보 생성
9. 모달에 생성할 컴포넌트 인스턴스 정보 개수 설정

10. 페이지 정보 설정
      10-1. 페이지 프로퍼티 정보 신규 생성
      10-2. 인스턴스 관리자 신규 생성
      10-3. 페이지 다시 그리기(immediatrUpdateDisplay)
      10-4. 페이지 다시 그리기2(onAddContainer());

11.인스턴스 정보를 가지고 컴포넌트 인스턴스 생성
      11-1. 컴포넌트인스턴스로더를 이용해 컴포넌트 인스턴스 생성시작
            11-1-1.  컴포넌트 언스턴스 팩토리 생성
            11-1-2. 인스턴스 생성 시작
                  11-1-2-1. 인스턴스 생성
                  11-1-2-2. 컴포넌트 기능 추가(3D인 경우 threeElements가 이때 추가됨)
                  11-1-2-3. 컴포넌트를 뷰모드로 실행( executeViewerMode(이벤트버스), 이때 WScript 이벤트 버스연결
                  11-1-2-4. properties와 element 생성


            11-1-3. 컴포넌트에서 create이벤트가 발생하면 인스턴스를 뷰에 전송(화면에 붙음)
                  11-1-3-1. 해당 layer에 인스턴스 추가
                  11-1-3-2. 인스턴스 관리자에 등록(window.wemb.page."인스턴스명"으로 접근할 수 있는 기능)
------------------------
11.
 */
export class OpenPageCommand extends puremvc.SimpleCommand {
      private _componentResourceLoader:ComponentResourceLoader = null;
      private publicProxy             :PublicProxy;
      private isCreateMasterLayer     :boolean = false;
      private workerDirector          :any;
      private running                 :boolean = false;
      private cumilative              :number = 0;


      public async execute(note: puremvc.INotification) {

            let isGrade = await pageApi.checkPageGrade(note.getBody());
            if (isGrade) {
                  alert(Vue.$i18n.messages.wv.page.notHaveAssigned);
                  return;
            }

            console.log("#1 , OpenPageCommand = ", note.getBody());

            if(window.wemb.mainPageComponent.isLoading==true){
                  console.warn("You are already loading the page. It can be executed after loading is completed.");
                  return;
            }


            // 페이지 로딩 상태를 true로 만든다.
            window.wemb.mainPageComponent.setLoadingState(true);
            // 메인 페이지 컴포넌트 로딩 완료 유무 처리하기
            window.wemb.mainPageComponent.isLoaded=false;

            console.log("3-1. 리소스 로딩바 활성화");
            window.wemb.$editMainAreaResourceLoadingComponent.show();
            window.wemb.$editMainAreaResourceLoadingComponent.increase(0, 'Init Loading...');


            // 2018.11.21(ckkim)
            if(await window.wemb.hookManager.executeAction(HookManager.HOOK_BEFORE_LOAD_PAGE, note.getBody())==false){
                  console.warn("페이지 오픈 전 취소 되었습니다.");
                  window.wemb.mainPageComponent.setLoadingState(false);
                  window.wemb.$editMainAreaResourceLoadingComponent.hide();

                  console.warn("페이지 오픈 전 취소 되었습니다.");
                  return;
            }




            let viewerProxy: ViewerProxy = <ViewerProxy>this.facade.retrieveProxy(ViewerProxy.NAME);
            this.publicProxy = <PublicProxy>this.facade.retrieveProxy(PublicProxy.NAME);
            let stageProxy:StageProxy = <StageProxy>this.facade.retrieveProxy(StageProxy.NAME);
            let pageId = note.getBody();

            ////////////
            // 페이지가 없는 경우
            if(window.wemb.pageManager.hasPageInfoBy(pageId)==false){
                  this.sendNotification(CommanderStatic.CMD_CRITICAL_404_ERROR_COMMAND,"open page command, no page");
                  return;
            }


            console.log("1-1. 로딩 모달 활성화 (화면 뒤덮음)");
            //로딩 모달 활성화
            //window.wemb.$defaultLoadingModal.show(window.wemb.$defaultLoadingModal.$i18n.messages.wv.common.pageLoading);

            console.log("1-2. 로딩전 레이어 숨기기");
            window.wemb.mainPageComponent.hiddenMTTLayer();


            console.log("1-3. 페이지 정보 로딩 전 unload wscript event 발생");
            window.wemb.mainPageComponent.dispatchBeforeUnLoadScriptEvent();
            this.sendNotification(ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE, WVPageComponentScriptEvent.BEFORE_UNLOAD);

            /*
            페이지 닫기

                  - 에디트 모드 비활성화
                  - 레이어 정보 초기화
                  - 선택 정보 비활성화
                  - 복사 정보 모두 삭제


                  - 데이터셋 실행 모두  삭제
                  - wemb.setInterval (타이머 관리자) 실행 모두 삭제
         */
            console.log("1-4. 팝업 정보 제거");
            window.wemb.popupManager.clear();
            console.log("1-5. 페이지 정보 제거");
            viewerProxy.closedPage();
            console.log("1-6. 페이지 닫은 후 unloaded wscript event 발생  ");
            window.wemb.mainPageComponent.dispatchUnLoadedScriptEvent();
            this.sendNotification(ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE, WVPageComponentScriptEvent.UNLOADED);
            console.log("1-7. 페이지 배치 컴포넌트가 모두 사라짐.");
            ////////////////







            ////////////////

            let pageData:OpenPageData = null;
            try{
                  console.log("2-1. 페이지 정보 읽기 시작");
                  await wemb.dataService.initialize();
                  pageData = await pageApi.openPageById(pageId);
                  console.log("2-2. 페이지 정보 읽기 완료");

            }catch(error){

                  this.sendNotification(CommanderStatic.CMD_CRITICAL_403_ERROR_COMMAND, error);
                  return;
            };


            try {

                  /*
                  2018.11.05
                  동적 스크립트 파일 로드하기
                  editor OpenPageCommand와 다름.
                   */
                  window.wemb.mainPageComponent.offWScriptEventAll();
                  if (pageData.page_info.props.script) {

                        if (pageData.page_info.props.script.hasOwnProperty("fileName")) {
                              let fileName = pageData.page_info.props.script.fileName || "";
                              if (fileName.length > 0) {
                                    let pageScript = await pageApi.attachPageScriptFile("./custom/page-scripts/" + fileName, window.wemb.mainPageComponent);
                                    viewerProxy.pageScriptFunction = pageScript;
                              }
                        }
                  }
            }catch(error) {
                  this.sendNotification(CommanderStatic.CMD_CRITICAL_403_ERROR_COMMAND, error);
                  return;
            }


            try {
                  console.log("2-3. 페이지 정보 설정하기1: proxy에");
                  viewerProxy.setPagePropertyInfo(pageData.page_info);

                  console.log("2-4. 페이지 정보 설정하기2. 페이지 관리자에");
                  window.wemb.pageManager.currentPageInfo = pageData.page_info;

                  console.log("2-5. 페이지 배경 정보 설정하기 =  페이지 정보가 변경되어야 함.");
                  let bgInfo=pageData.page_info.props.setter.background;
                  stageProxy.setPageBackgroundInfo(bgInfo);




                  console.log("2-4. beforeload wscript event 발생");
                  /* 20118.11.12(ckkim)
                  팝업메니저, 페이지메니져에 의해서 페이지를 활성화 하는 경우
                  openPageById(),openPageByName() 호출시 파라메터를 넘긴 경우
                  mainPageComponent의 params에 파라메터 설정하기
                  즉,
                  this.params로 접근 가능함.
                  this.params로 파라메터를 접근할 수 있는 경우는

                   */
                  window.wemb.mainPageComponent.params = window.wemb.pageManager.getParams();


                  window.wemb.mainPageComponent.dispatchBeforeLoadScriptEvent();
                  this.sendNotification(ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE, WVPageComponentScriptEvent.BEFORE_LOAD);







                  // 로딩 바 활성화
                  console.log("3-1. 리소스 로딩바 활성화");
                  console.log("3-2. 화면에 붙일 컴포넌트 인스턴스 정보 생성, 시작");



                  // 레이어별 컴포넌트 정보 생성
                  let comInstanceInfoList: Array<any> = [];

                  this.isCreateMasterLayer = this.publicProxy.getCreateMasterLayer();
                  if (this.isCreateMasterLayer == false) {
                        //comInstanceInfoList = comInstanceInfoList.concat(this.publicProxy.getComInstanceInfoListInMasterLayer());
                        this.publicProxy.completeCreateMasterLayer();
                  }

                  if(pageData.page_info.type !== 'master'){
                        this.publicProxy.setComInstanceInfoListInMasterLayer(pageData.content_info.master_layer);
                        comInstanceInfoList = comInstanceInfoList.concat(this.publicProxy.getComInstanceInfoListInMasterLayer());
                  }

                  comInstanceInfoList = comInstanceInfoList.concat(pageData.content_info.two_layer);
                  comInstanceInfoList = comInstanceInfoList.concat(pageData.content_info.three_layer);

                  console.log("3-3. 화면에 붙일 컴포넌트 인스턴스 정보 생성, 완료, 개수 = ", comInstanceInfoList.length);
                  // 레이어별 컴포넌트 생성
                  window.wemb.$defaultLoadingModal.setMaxLength(comInstanceInfoList.length);




                  /*

		     2018.11.21(ckkim)
		     hook 위치
		     예)
		     if(await window.wemb.hookManager.executeAction(EditorStatic.CMD_SAVE_PAGE)==false){
			     console.warn("페이지 저장이 취소 되었습니다.");
			     //alert("페이지 저장이 취소 되었습니다.");
			     return;
		     }
			*/

                  if(await window.wemb.hookManager.executeAction(HookManager.HOOK_AFTER_LOAD_PAGE,comInstanceInfoList)==false){
                        console.warn("페이지 오픈이 취소 되었습니다.");
                        return;
                  }


                  window.wemb.$editMainAreaResourceLoadingComponent.increase(30, 'Open Page Loading...');


                  /*
                  컴포넌트 인스턴스 생성 후
                  layer에 컴포넌트 인스턴스가 추가된 상태.
                  단, 이때 리소스는 로드되지 않은 시점
                  즉, READY 상태임.
                   */
                  const loadObj = function loadObj(objs){
                        console.time("OBJ")
                        if(objs.length === 0 ){
                              // 로드해 둘게 없으면 그냥 리소스 로딩을 진행한다.
                              this._startResourceLoading();
                        }else{
                              this.workerDirector = new THREE.LoaderSupport.WorkerDirector(THREE.OBJLoader2);
                              this.workerDirector.setLogging( false, false );
                              this.workerDirector.setCrossOrigin( 'anonymous' );
                              this.workerDirector.setForceWorkerDataCopy( true );

                              // 로드 될 때마다 불려지는 콜백
                              var callbackOnLoad = async function ( event ) {
                                    NLoaderManager.setMeshLoadedPool(event.detail.modelName, event.detail.loaderRootNode);

                                    window.wemb.$editMainAreaResourceLoadingComponent.increase(0, `Resource Loading...\n${this.cumilative}/${objs.length}`);
                                    this.cumilative += 1;

                                    if ( this.workerDirector.objectsCompleted + 1 === objs.length ) {
                                          this.workerDirector.tearDown();
                                          this.workerDirector = null;
                                          console.timeEnd("OBJ");
                                          console.time("OBJtexture")
                                          // 여기서 다음 과정인 텍스처 로드를 진행한다.
                                          let textures = await this._componentResourceLoader.createTexturePool();
                                          textures
                                                .filter((texture)=> texture.image !== undefined)
                                                .forEach((texture)=>{
                                                      NLoaderManager.registTexturePool(texture.image.src, texture);
                                                });

                                          this.cumilative = 0;
                                          console.timeEnd("OBJtexture")
                                          this._startResourceLoading();
                                    }
                              }.bind(this);

                              var callbacks = new THREE.LoaderSupport.Callbacks();
                              callbacks.setCallbackOnLoad(callbackOnLoad);

                              this.workerDirector.prepareWorkers(callbacks, objs.length, 16);

                              objs.forEach((pr)=>{
                                    this.workerDirector.enqueueForRun(pr);
                              });
                              this.workerDirector.processQueue();
                        }
                  }.bind(this);

                  console.log("3-4. 화면에 인스턴스 붙이기, 시작!");
                  let success = await viewerProxy.startConvertingComponentInfoToComponentInstance(comInstanceInfoList);
                  console.log("3-5. 화면에 인스턴스 붙이기, 완료 = ", success);
                  window.wemb.$editMainAreaResourceLoadingComponent.increase(35, 'Resource Loading Start...');

                  if(success) {
                        this._componentResourceLoader = new ComponentResourceLoader();

                        // 정상적으로 이벤트가 로드된 경우
                        window.wemb.mainPageComponent.dispatchReadyScriptEvent();
                        this.sendNotification(ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE, WVPageComponentScriptEvent.READY);

                        let preps = this._componentResourceLoader.prepObjLoad(viewerProxy.comInstanceList);
                        let objs = preps.filter((prep)=> prep.modelName.includes('.obj'))
                        let gltf = preps.filter((prep)=> prep.modelName.includes('.gltf'))
                        let gltfCnt = 0;

                        if(!gltf.length) loadObj(objs);
                        else{
                              console.time("GLTF");
                              for(let i = 0 ; i < gltf.length; i++){
                                    let loader = new THREE.GLTFLoader();
                                    loader.setCrossOrigin('anonymous');
                                    loader.setDRACOLoader(new THREE.DRACOLoader());
                                    const url =  gltf[i].resources[0].url;
                                    loader.load(url, function(data){
                                          NLoaderManager.setMeshLoadedPool(url, data.scene);
                                          gltfCnt++;
                                          if(gltfCnt === gltf.length) {
                                                console.timeEnd('GLTF');
                                                loadObj(objs);
                                          }
                                    }.bind(this))
                              }
                        }
                  }else {
                        console.log("컴포넌트 생성이 정상적으로 이뤄지지 않았습니다.")
                  }

            }catch(error){

                  this.sendNotification(CommanderStatic.CMD_CRITICAL_403_ERROR_COMMAND, error);
            }
      }

      private _startResourceLoading(){
            let viewerProxy: ViewerProxy = <ViewerProxy>this.facade.retrieveProxy(ViewerProxy.NAME);
            let $resourceEventBus = new Vue();

            this._componentResourceLoader = new ComponentResourceLoader();
            this._componentResourceLoader.$on(ComponentResourceLoaderEvent.COMPLETED_ALL, ()=>{
                  this._completedLoadAllResource();
                  NLoaderManager.clearMeshTextureLoaderPool();
            })
            console.log("4-1. 컴포넌트 리소스 읽기, 시작");
            this._componentResourceLoader.startResourceLoading(viewerProxy.comInstanceList);
      }




      private async _completedLoadAllResource(){
            console.log("4-2. 컴포넌트 리소스 읽기, 완료");


            // 리로스 로더 파괴
            if(this._componentResourceLoader) {
                  this._componentResourceLoader.destroy();
                  this._componentResourceLoader = null;
            }


            // 모든 정보를 읽었다는 의미로 isLoaded = true로 설정
            window.wemb.mainPageComponent.isLoaded=true;


            console.log("4-3. 화면에 배치된 모든 컴포넌트에 complete wscript 이벤트 발생");
            console.log("4-4. 화면에 배치된 모든 컴포넌트의 onLoadPage 메서도 호출");
            let viewerProxy: ViewerProxy = <ViewerProxy>this.facade.retrieveProxy(ViewerProxy.NAME);

            viewerProxy.comInstanceList.forEach((instance:WVComponent)=>{
                  // 마스터가 로드되지 않은 상태인경우 마스터 실행
                  if(this.isCreateMasterLayer==true)
                        if(instance.layerName==WORK_LAYERS.MASTER_LAYER)
                              return;

                  instance.dispatchWScriptEvent(WVComponentScriptEvent.COMPLETED);
                  // onLoadPage를 가지고 있는 컴포넌트만 onLoadPage()메서드 호출
                  if(instance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]){
                        instance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]();
                  }
            })


            // 최종 페이지 이벤트 발생
            console.log("4-5. 페이지 이벤트 : LOADED 이벤트 발생");
            window.wemb.mainPageComponent.dispatchLoadedScriptEvent();
            this.sendNotification(ViewerProxy.NOTI_CHANGE_PAGE_LOADING_STATE, WVPageComponentScriptEvent.LOADED);



            /*
		2018.10.29(ckkim)
	     마스터 컴포넌트의 onLoadPage() 메서드 호출 하기
	     마스터 컴포넌트가 이미 생성된 경우이기 때문에 onOpenPage()만 호출
		*/
            if(this.isCreateMasterLayer==true){
                  let viewerProxy: ViewerProxy = <ViewerProxy>this.facade.retrieveProxy(ViewerProxy.NAME);
                  viewerProxy.masterLayerInstanceList.forEach((instance:WVComponent)=>{

                        // onLoadPage를 가지고 있는 컴포넌트만 onOpenPage()메서드 호출
                        if(instance[WVCOMPONENT_METHOD_INFO.ON_OPEN_PAGE]){
                              instance[WVCOMPONENT_METHOD_INFO.ON_OPEN_PAGE]();
                        }
                  });
            }


            /*
					2018.11.21(ckkim)
					컴포넌트 활성화(보이게) 전에 훅 호출

				 */
            if(await window.wemb.hookManager.executeAction(HookManager.HOOK_BEFORE_ACITVE_PAGE)==false){
                  console.warn("페이지 활성화 전에 에러 발생");
                  return;
            }


            /*
		    2018.09.14
		    Master, Two, Three Layer 보이기
		     페이지 로딩 후 모든 내용을 한번에 출력되는 효과를 만들기 위해 사용
		     주의:
		      타이머를 사용한 이유는?
		            - 컴포넌트가 보이는 도중에 layer가 활성화 되는 경우 "틱!" 튀기는 현상이 발생함.
		            - 이를 막기 위해 타이머 사용.

	     */
            window.wemb.$editMainAreaResourceLoadingComponent.increase(35, 'Completed!');

            setTimeout(async ()=> {
                  // 메인 페이지 컴포넌트 로딩 완료 유무 처리하기
                  window.wemb.mainPageComponent.isLoaded = true;
                  window.wemb.mainPageComponent.setLoadingState(false);

                  window.wemb.mainPageComponent.showMTTLayer();
                  window.wemb.$editMainAreaResourceLoadingComponent.hide();

                  this.sendNotification(ViewerProxy.NOTI_OPENED_PAGE, window.wemb.pageManager.currentPageInfo.id);

                  if (await window.wemb.hookManager.executeAction(HookManager.HOOK_AFTER_ACITVE_PAGE) == false) {
                        console.warn("페이지 활성화 후 에러 발생");

                  }

                  // 현재 열린 페이지를 히스토리에 저장.
                  console.log("4-6. 열린 페이지를 히스토리에 저장");
                  window.wemb.pageHistoryManager.pushCurrentPage();

            },500);

            console.log("모든 처리 완료!");


      }
}
