import * as puremvc from "puremvc";

import ViewerProxy from "../../../viewer/model/ViewerProxy";
import {CONFIG_PROPERTY_GROUP_NAME} from "../../../wemb/wv/managers/ConfigManager";
import {ViewerMainMediator} from "../../view/ViewerMainMediator";
import CustomMediator from "@/wemb/wv/extension/CustomMediator";


/*
페이지를 닫고 비활성화 상태로만 만들기.
 */
export class CreateExtensionCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {


            if(window.wemb.extensionInstanceManager.usingExtension !=true)
                  return;



            let extensionPluginList = window.wemb.extensionInstanceManager.viewerModeInstanceList;
	      if(extensionPluginList) {

                  /*
                  1. 메디에티더 생성
                  2. 메디에이터에 view 연결
                        - view는 CreateDynamicPanelCommand()로 넘어온 데이터 또는 config 정보가 됨.
                  3. 메디에이터에서 노티로 받을 정보는 view에 등록되어 있어야 한다.

                   */

                  //
                  let pluginList = extensionPluginList["plugins"];
                  let count = 1;
                  console.log("@@ EXTENSION extension start, maxcount= ", pluginList.length);
                  console.log(pluginList);
                  if(pluginList && pluginList.length>0) {
                        pluginList.forEach((extensionInfo) => {
                              try {
                                    let PluginClass:any = eval(extensionInfo.extension_name);
                                    let mediatorView: any = new PluginClass();
                                    let mediatorName = extensionInfo.label;
                                    let mediator = new CustomMediator(mediatorName, mediatorView, mediatorView.notificationList);
                                    this.facade.registerMediator(mediator);


                                    mediatorView.setFacade(this.facade);
                                    console.log("@@ EXTENSION extension create ", (count), mediatorName);


                              }catch(error){
                                    console.log("###################");
                                    console.log("@@ error CreateExtensionCommand ", extensionInfo.extension_name +" 이라는 확장 정보가 존재하지 않습니다.");
                                    console.log("###################");
                              }

                              count++;
                        });

                        console.log("@@ EXTENSION extension = end ");
                  }
            }else {
	            console.log("###################");
	            console.log("확장 정보가 존재하지 않습니다.");
                  console.log("###################");
            }
	}
}
