import * as puremvc from "puremvc";

export class GotoLogoutCommand extends puremvc.SimpleCommand {
    execute(note: puremvc.INotification) {
          window.location.href = window.wemb.configManager.serverUrl + "/logout.do?mode=VIEWER";
    }
}
