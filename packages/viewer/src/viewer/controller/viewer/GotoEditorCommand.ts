import * as puremvc from "puremvc";

export class GotoEditorCommand extends puremvc.SimpleCommand {
    execute(note: puremvc.INotification) {
	    let viewerURL = location.origin + location.pathname + "#/";
        window.open(viewerURL, "_blank");
    }
}
