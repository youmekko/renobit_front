import * as puremvc from "puremvc";

export class Active3D360View extends puremvc.SimpleCommand {
    execute(note: puremvc.INotification) {

          wemb.threeElements.threeLayer.toggleWorkView(false,()=>{
                wemb.threeElements.personControls.enabled =false;
          })
    }
}
