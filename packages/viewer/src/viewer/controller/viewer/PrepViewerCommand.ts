import * as puremvc from "puremvc";
import ViewerStatic from "./ViewerStatic";
import {OpenPageCommand} from "./OpenPageCommand";
import {ClosePageCommand} from "./ClosePageCommand";
import {CreateExtensionCommand} from "./CreateExtensionCommand";
import {GotoLogoutCommand} from "./GotoLogoutCommand";
import { Active3D360View } from "./Active3D360View";
import { Active3DWalkView } from "./Active3DWalkView";
import { GotoEditorCommand } from "./GotoEditorCommand";
import { GotoAdminCommand } from "./GotoAdminCommand";
import {ShowModalCommand} from "./ShowModalCommand";
import {CompletedExtensionCommand} from "./CompletedExtensionCommand";




export class PrepViewerCommand extends puremvc.SimpleCommand {


	execute(note: puremvc.INotification) {

		console.log("step 02, PrepViewerCommand");
            this.facade.registerCommand(ViewerStatic.CMD_OPEN_PAGE, OpenPageCommand);
            this.facade.registerCommand(ViewerStatic.CMD_CLOSE_PAGE, ClosePageCommand);
            this.facade.registerCommand(ViewerStatic.CMD_CREATE_EXTENSION, CreateExtensionCommand);
            this.facade.registerCommand(ViewerStatic.CMD_GOTO_LOGOUT, GotoLogoutCommand);

            this.facade.registerCommand(ViewerStatic.CMD_GOTO_EDITOR, GotoEditorCommand);
            this.facade.registerCommand(ViewerStatic.CMD_GOTO_ADMIN, GotoAdminCommand);
            this.facade.registerCommand(ViewerStatic.CMD_ACTIVE_3D_360VIEW, Active3D360View);
            this.facade.registerCommand(ViewerStatic.CMD_ACTIVE_3D_WALKVIEW, Active3DWalkView);
            this.facade.registerCommand(ViewerStatic.CMD_SHOW_MODAL, ShowModalCommand);

            /*
            2018.12.19(ckkim)
             */
            this.facade.registerCommand(ViewerStatic.CMD_COMPLETED_EXTENSION, CompletedExtensionCommand);
	}
}
