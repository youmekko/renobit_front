import * as puremvc from "puremvc";

/*
2018.11.05(ckkim)
정리
 */
export class ShowModalCommand extends puremvc.SimpleCommand {

      execute(note: puremvc.INotification) {
            let info = note.getBody();
            window.wemb.$modalManager.loadModal(info.modalName, info.fileName).then((result:any)=>{
                  //

                  if(info.opener)
                        result.modal.setOpener(info.opener);
                  if(info.data)
                        result.modal.setData(info.data);

                  if(info.closed)
                        result.modal.setClosedCallback(info.closed);


                  result.modal.show();

                  result.modal.$on("closed", (updateState)=>{

                  })

            }).catch ((error)=>{
                  console.log("Viewer ShowModalCommand errror", error);
            });
      }
}
