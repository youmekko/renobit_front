import * as puremvc from "puremvc";


/*
페이지를 닫고 비활성화 상태로만 만들기.
 */
export class CompletedExtensionCommand extends puremvc.SimpleCommand {
	execute(note: puremvc.INotification) {

            let extensionPluginList = window.wemb.extensionInstanceManager.viewerModeInstanceList;
            if(extensionPluginList) {

                  let pluginList = extensionPluginList["plugins"];
                  if (pluginList) {
                        pluginList.forEach((extensionInfo)=>{
                              try {
                                    let mediatorName = extensionInfo.label;
                                    let mediator:any = this.facade.retrieveMediator(mediatorName);
                                    mediator.viewComponent.start();

                              }catch(error){

                              }
                        })
                  }
            }
	}
}
