import * as puremvc from "puremvc";
import CommonStatic from "./common/CommonStatic";
import {CriticalErrorCommand} from "./common/CriticalErrorCommand";
import {Critical404ErrorCommand} from "./common/Critical404ErrorCommand";
import {Critical403ErrorCommand} from "./common/Critical403ErrorCommand";
import {ActiveFPSCommand} from "./common/ActiveFPSCommand";


export class PrepCommonCommand extends puremvc.SimpleCommand {


    execute(note: puremvc.INotification) {
        this.facade.registerCommand(CommonStatic.CMD_CRITICAL_ERROR_COMMAND, CriticalErrorCommand);
          this.facade.registerCommand(CommonStatic.CMD_CRITICAL_404_ERROR_COMMAND, Critical404ErrorCommand);
          this.facade.registerCommand(CommonStatic.CMD_CRITICAL_403_ERROR_COMMAND, Critical403ErrorCommand);

          this.facade.registerCommand(CommonStatic.CMD_ACTIVE_FPS, ActiveFPSCommand);
    }
}
