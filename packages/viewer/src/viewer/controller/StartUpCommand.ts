import * as puremvc from "puremvc";
import {PrepViewCommand} from "./PrepViewCommand";
import {PrepModelCommand} from "./PrepModelCommand";
import {PrepViewerReadyCommand} from "./PrepViewerReadyCommand";
import {PrepViewerCommand} from "./viewer/PrepViewerCommand";
import {PrepCommonCommand} from "./PrepCommonCommand";


export default class StartUpCommand extends puremvc.MacroCommand {
	initializeMacroCommand() {
		console.log("step 02, StartUpCommand 실행 view, model, command 등록");
		this.addSubCommand(PrepViewerCommand);

		this.addSubCommand(PrepViewCommand);
		this.addSubCommand(PrepModelCommand);

		this.addSubCommand(PrepCommonCommand);
		this.addSubCommand(PrepViewerReadyCommand);
		console.log("step 02, StartUpCommand 실행 view, model, command 완료");
	}
}
