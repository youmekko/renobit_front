
/**
 * 서버와 규약된 데이터 정보, 
 * MODE는 최초 global데이터셋과 page데이터셋을 구분하는데 사용되었으나 현재는 사용하지 않음.
*/
export namespace Dataset {
	export enum MODE {
		GLOBAL = '0',
        PAGE = '1',
        ALL = ''
	}

    export enum DATA_TYPE {
        SQL ='0',
        REST ='1',
        TIM ='2'
    }

    export enum DELIVERY_TYPE {
        POLLING ='0',
        PUSH ='1'
    };

    export enum SETTING_MODE {
        CREATE ='0',
        EDIT ='1'
    };

    export enum DELIVERY_MIN_TIME {
        PUSH_TYPE = 3,
        POLLING_TYPE = 1
    };

    export enum DYNAMIC_TYPE {
          STATIC = '0',
          DYNAMIC = '1'
    }
}
