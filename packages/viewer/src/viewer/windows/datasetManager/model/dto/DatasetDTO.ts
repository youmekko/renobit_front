export namespace dto {
    //데이터셋 관리자에서 데이터셋 등록시
    export class DatasetItemDTO {
          public dataset_id: string = "";
          public name: string = "";
          public mode: string = "";
          public interval: string = "";
          public data_type: string = "";
          public delivery_type: string = "";
          public query: string = "";
          public rest_api: string = "";
          public param_info: Array<any> = [];
          public page_id: string = "";
          public tim: string = ""
          public description: string = "";
          public query_type: string="";
          public datasource: string = "";
          public query_detail_info: any = {};
          public db_type:string = "";
          public dbType:string = "";
    }

    //실제 데이터셋 호출시  DataService에서 사용
    export class DatasetInstDTO {
          public id: string = "";
          public interval: number = 0;
          public data_type: string = "";
          public delivery_type: string = "";
          public query: string = "";
          public rest_api: string = "";
          public tim: string = ""
          public param: any = {};
          public delay: number = 0;
          public datasource:string = "";
          public dbType: string = "";
          public query_type: string="";
    }

    //데이터셋 관리자에서 데이터셋 파라메터 등록시
    export class DatasetParamDTO {
          param_name: string = "";
          param_type: string = "";
          default_value: string = "";
    }

    //데이터셋 관리자 환경설정에서 TIM Worker List URL 등록시
    export class TimItemDTO {
          public id: string = "";
          public url: string = "";
          public title: string = "";
    }

    export class DataSourceDTO {
          public id: string = "";
          public name: string;
          public db_type: string;
          public description: string;
          public class_name: string;
          public user_id: string;
          public pwd: string;
          public url: string;
          public query:string;
    }
}
