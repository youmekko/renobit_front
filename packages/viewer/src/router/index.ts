import Vue from 'vue'
import Router from 'vue-router'

import ViewerMainComponent from '../viewer/view/ViewerMainComponent';

Vue.use(Router);

const router = new Router({
      routes: [            
            {
                  path: '/',
                  component: ViewerMainComponent
            },
            {
                  path: '/pid=:pid',
                  component: ViewerMainComponent
            },
            {
                  path: '/pname=:pname',
                  component: ViewerMainComponent
            }
      ]
})
export default router