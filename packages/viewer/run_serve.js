'use strict';

const dotenv = require('dotenv');
dotenv.config();
process.env.NODE_ENV='development';

const webpack = require('webpack');
const express = require('express');
const http = require('http');
const proxy = require('express-http-proxy');
const httpProxy = require('http-proxy');
const apiProxy = httpProxy.createProxyServer();
const webpackConfig = require('./build/webpack.config.js');
const compiler = webpack(webpackConfig);
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require("webpack-hot-middleware");
const chalk = require('chalk');
const ora = require('ora');
const log = console.log

log(chalk.cyan('renobit_client Serve 모드로 실행 중 입니다.\n'));

if (process.env.API_PROXY_PATH == null) {
      log(chalk.red('.env 파일의 작성을 확인 해 주세요. 아래와 같은 내용이 필요 합니다.'));
      log(chalk.gray(`
      # 접속포트
      SERVE_PORT=4200
      # 경로
      PUBLIC_PATH=/renobit
      # API 서버 주소
      API_PROXY_PATH=http://localhost:6277
      # DCIM 개발서버 주소(미기입시 자체 파일 사용)
      DCIM_PROXY_PATH=http://localhost:8080
      # 핫리로딩 사용여부
      USE_HOT_RELOAD=true      
      `));
      process.exit();
}

const webpackInstance = webpackDevMiddleware(compiler, {
      hot: true,
      inline: true,
      historyApiFallback: true,
      stats: 'errors-only',
      publicPath: process.env.PUBLIC_PATH,
      overlay: true
});

let app = express();
app.use('/', webpackInstance);

if (process.env.USE_HOT_RELOAD == 'true') {
      app.use('/', webpackHotMiddleware(compiler));
}

if (process.env.DCIM_PROXY_PATH !== 'false') {
      app.use(process.env.PUBLIC_PATH + '/custom/packs/dcim_pack', proxy(process.env.DCIM_PROXY_PATH));
}

app.use('/', proxy(process.env.API_PROXY_PATH));


const spinner = ora('웹팩이 준비중 입니다. 보통 1분 내에 완료 됩니다...');

let server = http.createServer(app);

server.listen(process.env.SERVE_PORT, function () {
      log(chalk.yellow('개발 서버 기동이 시작 되었습니다. 포트는 ' + process.env.SERVE_PORT + ' 입니다.'));
      spinner.start();
}).on('error', function (err) {
      console.log(err.message);
});

server.on('upgrade', function (req, socket, head) {
      apiProxy.ws(req, socket, head, { target: process.env.API_PROXY_PATH });
});

webpackInstance.waitUntilValid(function() {
      spinner.stop();
      log(chalk.yellow(`
      ______    _______  __    _  _______  _______  ___   _______  _______ 
      |    _ |  |       ||  |  | ||       ||  _    ||   | |       ||       |
      |   | ||  |    ___||   |_| ||   _   || |_|   ||   | |_     _||____   |
      |   |_||_ |   |___ |       ||  | |  ||       ||   |   |   |   ____|  |
      |    __  ||    ___||  _    ||  |_|  ||  _   | |   |   |   |  | ______|
      |   |  | ||   |___ | | |   ||       || |_|   ||   |   |   |  | |_____ 
      |___|  |_||_______||_|  |__||_______||_______||___|   |___|  |_______|    
      `))
      log(chalk.green('개발 서버 기동이 완료 되었습니다!\n아래 링크를 Ctrl + click 하여 바로 브라우저를 실행시킬 수 있습니다.'));
      log(chalk.white('http://localhost:'+process.env.SERVE_PORT+process.env.PUBLIC_PATH+"/login.do"));
      log(chalk.gray('중단하시려면 Ctrl+C를 눌러주세요.'));
      log(chalk.cyan('\n\n부디 작업이 잘 되셨으면 좋겠네요!'));
})
